package kr.co.homeplus.claim.claim.sql;


import java.util.Arrays;
import java.util.HashMap;
import kr.co.homeplus.claim.claim.model.mp.ClaimChgPaymentRefundSetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimSearchListSetDto;
import kr.co.homeplus.claim.constants.CustomConstants;
import kr.co.homeplus.claim.constants.SqlPatternConstants;
import kr.co.homeplus.claim.entity.claim.ClaimAdditionEntity;
import kr.co.homeplus.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claim.entity.claim.ClaimExchShippingEntity;
import kr.co.homeplus.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claim.entity.claim.ClaimMstEntity;
import kr.co.homeplus.claim.entity.claim.ClaimOptEntity;
import kr.co.homeplus.claim.entity.claim.ClaimPaymentEntity;
import kr.co.homeplus.claim.entity.claim.ClaimPickShippingEntity;
import kr.co.homeplus.claim.entity.claim.ClaimRefundEntity;
import kr.co.homeplus.claim.entity.claim.ClaimReqEntity;
import kr.co.homeplus.claim.entity.dms.ItmPartnerSellerEntity;
import kr.co.homeplus.claim.entity.dms.ItmPartnerSellerShopEntity;
import kr.co.homeplus.claim.entity.order.BundleEntity;
import kr.co.homeplus.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claim.entity.order.OrderOptEntity;
import kr.co.homeplus.claim.entity.order.PaymentEntity;
import kr.co.homeplus.claim.entity.order.PaymentInfoEntity;
import kr.co.homeplus.claim.entity.order.PaymentMethodEntity;
import kr.co.homeplus.claim.entity.order.PurchaseGiftEntity;
import kr.co.homeplus.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claim.enums.MysqlFunction;
import kr.co.homeplus.claim.factory.EntityFactory;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ClaimSearchSql {

    // 공통코드 조회.
    private static final String escrowMngCode = "(SELECT IFNULL(imc.mc_nm, '''') AS mc_cd FROM dms.itm_mng_code imc where imc.gmc_cd = ''{0}'' AND imc.mc_cd = {1})";

    /**
     * 클레임 리스트 조회
     * **/
    public static String  selectClaimSearchList(ClaimSearchListSetDto claimSearchListSetDto){

        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class, Boolean.TRUE);
        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimMst.purchaseOrderNo,
                purchaseOrder.orgPurchaseOrderNo,
                claimMst.claimNo,
                claimReq.requestDt,
                purchaseOrder.orderDt,
                claimBundle.claimBundleNo,
                claimBundle.partnerId,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    claimBundle.claimType,
                    ObjectUtils.toArray(
                        "'C'", "'취소'",
                        "'R'", "'반품'",
                        "'X'", "'교환'"
                    ),
                    claimBundle.claimType
                ),
                claimBundle.claimType.concat(" as claimTypeCode"),
                "cs.mc_nm as claim_status",
                claimBundle.claimStatus.concat(" as claimStatusCode"),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(claimBundle.purchaseOrderNo,  claimBundle.bundleNo, "'M'"), "ship_type"),
                claimMst.claimPartYn,
                bundle.shipMethod,
                claimPickShipping.claimPickShippingNo,
                claimExchShipping.claimExchShippingNo,
                claimPickShipping.pickStatus,
                claimExchShipping.exchStatus,
                claimExchShipping.exchD3Dt,
                claimPickShipping.pickP3Dt,
                claimPickShipping.pickShipType,
                claimPickShipping.pickDlvCd,
                claimPickShipping.pickInvoiceNo,
                claimExchShipping.exchShipType,
                claimExchShipping.exchDlvCd,
                claimExchShipping.exchInvoiceNo
            ).FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo }, claimBundle )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimReq),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo, claimBundle.claimBundleNo }, claimReq )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.purchaseOrderNo }, purchaseOrder )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(bundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.bundleNo , claimMst.purchaseOrderNo}, bundle )),
                    null
                )
            )
            .INNER_JOIN("(SELECT * FROM "
                + "dms.itm_mng_code imc "
                + "WHERE "
                + "imc.gmc_cd = 'claim_status') cs on cs.mc_cd = cb.claim_status")
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimPickShipping),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimPickShipping )),
                    null
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimExchShipping),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimExchShipping )),
                    null
                )
            )
            .WHERE(
                SqlUtils.betweenDay(claimReq.requestDt, claimSearchListSetDto.getSchStartDt(), claimSearchListSetDto.getSchEndDt()),
                SqlUtils.equalColumnByInput(claimMst.siteType, claimSearchListSetDto.getSiteType()),
                SqlUtils.equalColumnByInput(claimMst.userNo, claimSearchListSetDto.getUserNo())
            )
            .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimBundle.claimBundleNo)))
            .LIMIT(claimSearchListSetDto.getPerPage())
            .OFFSET((claimSearchListSetDto.getPage() - 1) * claimSearchListSetDto.getPerPage());

            if(!claimSearchListSetDto.getClaimType().equalsIgnoreCase("ALL") && !claimSearchListSetDto.getClaimType().equalsIgnoreCase("")) {
                query.WHERE(
                    SqlUtils.equalColumnByInput(claimBundle.claimType, claimSearchListSetDto.getClaimType())
                );
            }
        log.debug("selectClaimSearchList Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimListPartnerInfo(@Param("claimBundleNo")long claimBundleNo, @Param("partnerId")String partnerId){
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ItmPartnerSellerEntity itmPartnerSeller = EntityFactory.createEntityIntoValue(ItmPartnerSellerEntity.class, Boolean.TRUE);


        SQL query = new SQL()
            .SELECT(
                claimItem.storeType,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimItem.mallType, "TD"),
                        claimItem.storeId,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray("NOW()", "%s%i")), itmPartnerSeller.sellerSeq, SqlUtils.sqlFunction(MysqlFunction.RIGHT, ObjectUtils.toArray("RAND()", 2)))))
                            .FROM(EntityFactory.getTableNameWithAlias("dms", itmPartnerSeller))
                            .WHERE(
                                SqlUtils.equalColumn(itmPartnerSeller.partnerId, claimItem.partnerId)
                            )
                        )
                    ), claimItem.partnerId
                ),
                "CASE "
                    + "WHEN ci.mall_type = 'TD' THEN UF_GET_STORE_PARTNER_NAME(ci.origin_store_id, 'S') "
                    + "ELSE UF_GET_STORE_PARTNER_NAME(ci.partner_id, 'P') "
                    + "END as partner_nm"
            ).FROM(EntityFactory.getTableNameWithAlias(claimItem))
            .WHERE(
                SqlUtils.equalColumnByInput(claimItem.claimBundleNo, claimBundleNo),
                SqlUtils.equalColumnByInput(claimItem.partnerId, partnerId)
            ).LIMIT(1);;
        log.debug("selectClaimListPartnerInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }


    /**
     * 클레임 리스트 수 조회
     * **/
    public static String selectClaimSearchListCnt(ClaimSearchListSetDto claimSearchListSetDto){

        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, "*")
            ).FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo }, claimBundle )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimReq),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo, claimBundle.claimBundleNo }, claimReq )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.purchaseOrderNo }, purchaseOrder )),
                    null
                )
            )
            .WHERE(
                SqlUtils.betweenDay(claimReq.requestDt, claimSearchListSetDto.getSchStartDt(), claimSearchListSetDto.getSchEndDt()),
                SqlUtils.equalColumnByInput(claimMst.userNo, claimSearchListSetDto.getUserNo()),
                SqlUtils.equalColumnByInput(claimMst.siteType, claimSearchListSetDto.getSiteType())
            );

            if(!claimSearchListSetDto.getClaimType().equalsIgnoreCase("ALL") && !claimSearchListSetDto.getClaimType().equalsIgnoreCase("")) {
                query.WHERE(
                    SqlUtils.equalColumnByInput(claimBundle.claimType, claimSearchListSetDto.getClaimType())
                );
            }
        log.debug("selectClaimSearchList Query :: \n [{}]", query.toString());
        return query.toString();
    }


     /**
     * 클레임 리스트 조회시 상품정보 claimBundleNo 단위
     * **/
    public static String selectClaimListSearchItemList(@Param("claimBundleNo")long claimBundleNo) {

        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimItem.claimItemNo,
                claimItem.itemNo,
                claimItem.itemName,
                claimItem.itemPrice,
                claimItem.claimItemQty,
                "CASE "
                + " WHEN "
                + " (select count(*) from order_promo_item_buy opib where opib.item_no = ci.item_no and opib.purchase_order_no= ci.purchase_order_no) = 0 "
                + " AND (select count(*) from order_promo_item_get opig where opig.item_no = ci.item_no and opig.purchase_order_no= ci.purchase_order_no) = 0 "
                + " THEN 'N' "
                + " ELSE 'Y' "
                + "END as promo_yn",
                "case when oi.org_item_no is null then 'N' else 'Y' end sub_item_yn",
                "case when oi.org_item_no is null then ''"
                + "else (select item_nm1 "
                + "from purchase_order poo "
                + "left join order_item oii "
                + "on poo.purchase_order_no = oii.purchase_order_no "
                + "where poo.org_purchase_order_no = ci.purchase_order_no "
                + "and oii.org_item_no = oi.item_no limit 1) end".concat(" as sub_item_nm"),
                shippingItem.shipNo,
                shippingItem.shipMethod
            ).FROM(EntityFactory.getTableNameWithAlias(claimItem)
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderItem),
                    Arrays
                        .asList(SqlUtils.onClause(new Object[]{claimItem.orderItemNo}, orderItem)),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.purchaseOrderNo, claimItem.orderItemNo, claimItem.bundleNo }, shippingItem )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimItem.claimBundleNo, claimBundleNo)
            );
        log.debug("selectClaimListSearchItemList Query :: \n [{}]", query.toString());
        return query.toString();
    }




    /**
     * 클레임 상세조회 상품정보 리스트 claimNo 단위
     * **/
    public static String selectClaimSearchItemList(@Param("claimNo")long claimNo){
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ItmPartnerSellerEntity itmPartnerSeller = EntityFactory.createEntityIntoValue(ItmPartnerSellerEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimBundle.claimNo,
                claimBundle.claimBundleNo,
                claimItem.itemNo,
                claimItem.itemName,
                claimItem.itemPrice,
                SqlUtils.aliasToRow(claimOpt.claimOptQty, "claim_item_qty"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimItem.mallType, "TD"),
                        claimItem.storeId,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray("NOW()", "%s%i")), itmPartnerSeller.sellerSeq, SqlUtils.sqlFunction(MysqlFunction.RIGHT, ObjectUtils.toArray("RAND()", 2)))))
                            .FROM(EntityFactory.getTableNameWithAlias("dms", itmPartnerSeller))
                            .WHERE(
                                SqlUtils.equalColumn(itmPartnerSeller.partnerId, claimItem.partnerId)
                            )
                        )
//                        SqlUtils.subQuery(itmPartnerSellerShop.shopUrl, EntityFactory.getTableNameWithAlias("dms", itmPartnerSellerShop), ObjectUtils.toArray(SqlUtils.equalColumn(itmPartnerSellerShop.partnerId, claimItem.partnerId)))
                    ), claimItem.partnerId
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimItem.originStoreId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimItem.partnerId, "P"))
                    ),
                    "partner_nm"
                ),
                claimItem.storeType,
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(claimBundle.purchaseOrderNo,  claimBundle.bundleNo, "'M'"), "ship_type"),
                SqlPatternConstants.getConvertPattern(escrowMngCode, SqlUtils.nonAlias(claimBundle.claimStatus), SqlUtils.nonAlias(claimBundle.claimStatus), claimBundle.claimStatus),
                claimBundle.claimStatus.concat(" as claimStatusCode"),
                "CASE "
                + " WHEN "
                + " (select count(*) from order_promo_item_buy opib where opib.item_no = ci.item_no and opib.purchase_order_no= ci.purchase_order_no) = 0 "
                + " AND (select count(*) from order_promo_item_get opig where opig.item_no = ci.item_no and opig.purchase_order_no= ci.purchase_order_no) = 0 "
                + " THEN 'N' "
                + " ELSE 'Y' "
                + "END as promo_yn",
                "case when oi.org_item_no is null then 'N' else 'Y' end sub_item_yn",
                "case when oi.org_item_no is null then ''"
                + "else (select item_nm1 "
                + "from purchase_order poo "
                + "left join order_item oii "
                + "on poo.purchase_order_no = oii.purchase_order_no "
                + "where poo.org_purchase_order_no = ci.purchase_order_no "
                + "and oii.org_item_no = oi.item_no limit 1) end".concat(" as sub_item_nm"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.selOpt1Title, orderOpt.selOpt1Title),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.selOpt1Val, orderOpt.selOpt1Val),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.selOpt2Title, orderOpt.selOpt2Title),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.selOpt2Val, orderOpt.selOpt2Val),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.txtOpt1Title, orderOpt.txtOpt1Title),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.txtOpt1Val, orderOpt.txtOpt1Val),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.txtOpt2Title, orderOpt.txtOpt2Title),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.txtOpt2Val, orderOpt.txtOpt2Val),
                shippingItem.shipNo,
                shippingItem.shipMethod
            ).FROM(EntityFactory.getTableNameWithAlias(claimItem)
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.claimNo, claimItem.claimBundleNo }, claimBundle )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.orderItemNo }, orderItem )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimOpt),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.claimItemNo }, claimOpt )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderOpt),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimOpt.orderOptNo }, orderOpt )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.purchaseOrderNo, claimItem.orderItemNo, claimItem.bundleNo }, shippingItem )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimItem.claimNo, claimNo)
            );
            query.ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimItem.purchaseOrderNo, claimItem.mallType, claimItem.bundleNo, claimItem.orderItemNo)));
        log.debug("selectClaimSearchItemList Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
     * 클레임 상세정보 조회
     * **/
    public static String selectClaimDetail(@Param("userNo")long userNo, @Param("claimNo")long claimNo){

        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class, Boolean.TRUE);
        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimMst.claimNo,
                claimMst.purchaseOrderNo,
                claimReq.requestDt,
                claimReq.completeDt,
                SqlPatternConstants.getConvertPattern(escrowMngCode, SqlUtils.nonAlias(claimReq.claimReasonType), SqlUtils.nonAlias(claimReq.claimReasonType), claimReq.claimReasonType),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, null, ObjectUtils.toArray(claimReq.claimReasonDetail, "''")).concat(" as claim_reason_detail"),
                SqlPatternConstants.getConvertPattern(escrowMngCode, SqlUtils.nonAlias(claimReq.pendingReasonType), SqlUtils.nonAlias(claimReq.claimReasonType), claimReq.pendingReasonType),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, null, ObjectUtils.toArray(claimReq.pendingReasonDetail, "''")).concat(" as pending_reason_detail"),
                SqlPatternConstants.getConvertPattern(escrowMngCode, SqlUtils.nonAlias(claimReq.rejectReasonType), SqlUtils.nonAlias(claimReq.claimReasonType), claimReq.rejectReasonType),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, null, ObjectUtils.toArray(claimReq.rejectReasonDetail, "''")).concat(" as reject_reason_detail"),
                claimReq.uploadFileName,
                claimReq.uploadFileName2,
                claimReq.uploadFileName3,
                SqlPatternConstants.getConvertPattern(escrowMngCode, SqlUtils.nonAlias(claimBundle.claimType), SqlUtils.nonAlias(claimBundle.claimType), claimBundle.claimType),
                claimBundle.claimType.concat(" as claimTypeCode"),
                claimPickShipping.claimPickShippingNo,
                claimExchShipping.claimExchShippingNo,
                claimPickShipping.pickStatus,
                claimExchShipping.exchStatus,
                claimBundle.claimShipFeeEnclose,
                claimExchShipping.exchD3Dt,
                claimPickShipping.pickP3Dt,
                claimPickShipping.pickShipType,
                claimPickShipping.pickDlvCd,
                claimPickShipping.pickInvoiceNo,
                claimExchShipping.exchShipType,
                claimExchShipping.exchDlvCd,
                claimExchShipping.exchInvoiceNo
            ).FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimReq),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo }, claimReq )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo }, claimBundle )),
                    null
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimPickShipping),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimPickShipping )),
                    null
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimExchShipping),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimExchShipping )),
                    null
                )
            )

            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.userNo, userNo),
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            )
            .LIMIT(1);
        log.debug("selectClaimDetail Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
     * 클레임 상세내역 - 환불 금액 정보
     *
     * @param claimNo
     * @return String 클레임 상세정보 Query
     * @throws Exception 오류처리.
     * **/
    public String selectRegisterPreRefundAmt(@Param("claimNo")long claimNo){

        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimMst.claimNo,
                SqlUtils.aliasToRow(claimMst.completeAmt, "refund_amt"),
                claimMst.dgvAmt,
                claimMst.mhcAmt,
                claimMst.mileageAmt,
                claimMst.ocbAmt,
                claimMst.pgAmt,
                SqlUtils.aliasToRow(claimMst.totAddIslandShipPrice, "add_island_ship_price"),
                SqlUtils.aliasToRow(claimMst.totAddShipPrice, "add_ship_price"),
                SqlUtils.aliasToRow(claimMst.totCartDiscountAmt, "cart_discount_amt"),
                SqlUtils.aliasToRow(claimMst.totDiscountAmt, "discount_amt"),
                SqlUtils.aliasToRow(claimMst.totEmpDiscountAmt, "emp_discount_amt"),
                SqlUtils.aliasToRow(claimMst.totIslandShipPrice, "island_ship_price"),
                SqlUtils.aliasToRow(claimMst.totItemDiscountAmt, "item_discount_amt"),
                SqlUtils.aliasToRow(claimMst.totItemPrice, "item_price"),
                SqlUtils.aliasToRow(claimMst.totShipDiscountAmt, "ship_discount_amt"),
                SqlUtils.aliasToRow(claimMst.totShipPrice, "ship_price"),
                SqlUtils.aliasToRow(claimMst.totPromoDiscountAmt, "promo_discount_amt"),
                SqlUtils.aliasToRow(claimMst.totCardDiscountAmt, "card_discount_amt"),
                SqlUtils.aliasToRow(claimMst.totShipPrice, "ship_price"),
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(claimMst.totItemPrice, claimMst.totShipPrice, claimMst.totIslandShipPrice),
                    "purchase_amt"
                ),
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(claimMst.dgvAmt, claimMst.mhcAmt),
                    "point_amt"
                ),
                "(select sum(cp.payment_amt - cp.substitution_amt) from claim_payment cp where cp.claim_no = cm.claim_no) as out_of_stock_refund_amt",
                claimMst.oosYn,
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(claimMst.totAddTdDiscountAmt, claimMst.totAddDsDiscountAmt),
                    "add_coupon_discount_amt"
                )
            ).FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            );
        log.debug("selectRegisterPreRefundAmt Query :: \n [{}]", query.toString());
        return query.toString();
    }
    public String selectClaimReason(@Param("claimType") String claimType) {
        StringBuilder query = new StringBuilder();
        query.append(
                "SELECT IFNULL(imc.mc_nm, '''') AS claimReasonTypeDetail, "
                + "imc.mc_cd as claimReasonType, "
                + "imc.ref_1 as claimType, "
                + "ref_2 as whoReason, "
                + "imc.ref_3 as cancelType, "
                + "imc.ref_4 as detailReasonRequired "
            + "FROM dms.itm_mng_code imc "
            + "where imc.gmc_cd = 'claim_reason_type' "
            + "AND imc.ref_1 = '" + claimType + "'"
            + "AND imc.use_yn = 'Y'"

            // FO에 노출안할 사유 제외
            + "AND imc.mc_cd not in('C001','C002','C006','C007','C009','R001','R007','R008','R011','R012','R008','X004')"
        );

        log.debug("selectClaimReason Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
    * 클레임 환불수단 조회
    *
    * @param claimNo
    * @return String 클레임 상세정보 Query
    * @throws Exception 오류처리.
    */
    public static String selectRefundTypeList(@Param("claimNo") long claimNo){

        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        PaymentMethodEntity paymentMethod = EntityFactory.createEntityIntoValue(PaymentMethodEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimPayment.claimPaymentNo,
                SqlPatternConstants.getConvertPattern(escrowMngCode, "refund_type", "payment_type", claimPayment.parentPaymentMethod),
                SqlUtils.aliasToRow(claimPayment.parentPaymentMethod, "refund_type_code"),
                SqlUtils.subQuery(
                    paymentMethod.methodNm,
                    EntityFactory.getTableNameWithAlias(paymentMethod),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(paymentMethod.methodCd, claimPayment.methodCd),
                        SqlUtils.equalColumn(paymentMethod.siteType, claimMst.siteType)
                    ),
                    "method_nm"
                ),
                SqlUtils.caseWhenElse(
                    claimPayment.parentPaymentMethod,
                    ObjectUtils.toArray(
                        "'CARD'", "(select card_no from payment_info pi where payment_no = cp.payment_no and pi.parent_method_cd ='CARD')"
                    ),
                    "''",
                    "card_no"
                ),
                SqlUtils.caseWhenElse(
                    claimPayment.parentPaymentMethod,
                    ObjectUtils.toArray(
                        "'CARD'", "(select card_installment_month from payment_info pi where payment_no = cp.payment_no and pi.parent_method_cd ='CARD')"
                    ),
                    "''",
                    "card_installment_month"
                ),
                claimPayment.paymentStatus,
                "cp.payment_amt - cp.substitution_amt as payment_amt",
                claimPayment.regDt
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimPayment))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimMst),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimPayment.claimNo }, claimMst )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.claimNo, claimNo)
            );
        log.debug("selectRefundTypeList Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
    * 환불예정금액 조회 시 결제수단 조회
    *
    * @param purchaseOrderNo
    * @return String 클레임 상세정보 Query
    * @throws Exception 오류처리.
    */
    public static String selectPaymentTypeList(@Param("purchaseOrderNo") long purchaseOrderNo){

        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        PaymentInfoEntity paymentInfo = EntityFactory.createEntityIntoValue(PaymentInfoEntity.class, Boolean.TRUE);
        PaymentMethodEntity paymentMethod = EntityFactory.createEntityIntoValue(PaymentMethodEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                payment.paymentNo,
                SqlPatternConstants.getConvertPattern(escrowMngCode, "payment_type", "payment_type", paymentInfo.parentMethodCd),
                SqlUtils.aliasToRow(paymentInfo.parentMethodCd, "payment_type_code"),
                SqlUtils.subQuery(
                    paymentMethod.methodNm,
                    EntityFactory.getTableNameWithAlias(paymentMethod),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(paymentMethod.methodCd, paymentInfo.methodCd),
                        SqlUtils.equalColumn(paymentMethod.siteType, purchaseOrder.siteType)
                    ),
                    "method_nm"
                ),
                SqlUtils.caseWhenElse(
                    paymentInfo.parentMethodCd,
                    ObjectUtils.toArray(
                        "'CARD'", paymentInfo.cardNo
                    ),
                    "''",
                    "card_no"
                ),
                SqlUtils.caseWhenElse(
                    paymentInfo.parentMethodCd,
                    ObjectUtils.toArray(
                        "'CARD'", paymentInfo.cardInstallmentMonth
                    ),
                    "''",
                    "card_installment_month"
                ),
                payment.paymentStatus,
                purchaseOrder.userNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(payment))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(paymentInfo),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ payment.paymentNo }, paymentInfo )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ payment.paymentNo }, purchaseOrder )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectPaymentTypeList Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
    * 환불수단 등록여부 확인
    *
    * @param claimChgPaymentRefundSetDto
    * @return String 등록여부 확인 Query
    * @throws Exception 오류처리.
    */
    public String selectAlreadyRegClaimRefundCnt(ClaimChgPaymentRefundSetDto claimChgPaymentRefundSetDto){

        ClaimRefundEntity claimRefund = EntityFactory.createEntityIntoValue(ClaimRefundEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, "*")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimRefund))
            .WHERE(
                SqlUtils.equalColumnByInput(claimRefund.claimNo, claimChgPaymentRefundSetDto.getClaimNo()),
                SqlUtils.equalColumnByInput(claimRefund.purchaseOrderNo, claimChgPaymentRefundSetDto.getPurchaseOrderNo())
            );
        log.debug("selectAlreadyRegClaimRefundCnt Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
    * 환불수단 변경될 결제정보 조회
    *
    * @param claimChgPaymentRefundSetDto
    * @return String 등록여부 확인 Query
    * @throws Exception 오류처리.
    */
    public String selectClaimPhonePaymentInfo(ClaimChgPaymentRefundSetDto claimChgPaymentRefundSetDto){

        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                    claimPayment.claimPaymentNo,
                    claimPayment.paymentNo,
                    SqlPatternConstants.getConvertPattern(escrowMngCode, "bank_name", "bank_code", claimChgPaymentRefundSetDto.getBankCode()),
                    claimPayment.paymentAmt
                )
                .FROM(EntityFactory.getTableNameWithAlias(claimPayment))
                .INNER_JOIN(
                    SqlUtils.createJoinCondition(
                        EntityFactory.getTableNameWithAlias(claimMst),
                        Arrays.asList( SqlUtils.onClause(new Object[]{ claimPayment.claimNo }, claimMst )),
                        null
                    )
                )
                .WHERE(
                    SqlUtils.equalColumnByInput((claimPayment.parentPaymentMethod), "PHONE"),
                    SqlUtils.equalColumnByInput((claimPayment.paymentStatus), "P4"),
                    SqlUtils.equalColumnByInput((claimPayment.purchaseOrderNo), claimChgPaymentRefundSetDto.getPurchaseOrderNo()),
                    SqlUtils.equalColumnByInput((claimPayment.claimNo), claimChgPaymentRefundSetDto.getClaimNo()),
                    SqlUtils.equalColumnByInput((claimMst.userNo), claimChgPaymentRefundSetDto.getUserNo())
                );
        log.debug("selectClaimPhonePaymentInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }


    public static String selectClaimPurchaseGift(@Param("claimNo")long claimNo){

        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        PurchaseGiftEntity purchaseGift = EntityFactory.createEntityIntoValue(PurchaseGiftEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimAddition.purchaseOrderNo,
                purchaseGift.giftNo,
                claimAddition.giftMsg
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimAddition))
            .INNER_JOIN(
                   "purchase_gift pg on ca.addition_discount_no = pg.purchase_gift_no "
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimAddition.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimAddition.additionType, "G")
            );
        log.debug("selectClaimGiftList Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
     * 환불예정금액 조회 - call procedure
     *
     * @param parameterMap 조회 파라미터
     * @return 환불예정금액
     */
    public static String callByClaimPreRefundPriceCalculation(HashMap<String, Object> parameterMap){
        log.info("환불예정금 조회(단건) :: \n[{}]", SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION, parameterMap.values().toArray()));
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION, parameterMap.values().toArray());
    }

    /**
     * 환불예정금액 다건 조회 - call procedure
     *
     * @param parameterMap 조회 파라미터
     * @return 환불예정금액
     */
    public static String callByClaimPreRefundPriceMultiCalculation(HashMap<String, Object> parameterMap){
        log.info("환불예정금 조회(다건) :: \n[{}]", SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_MULTI, parameterMap.values().toArray()));
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_MULTI, parameterMap.values().toArray());
    }

    /**
     * 환불예정금액 결품/대체 조회 - call procedure
     *
     * @param parameterMap 조회 파라미터
     * @return 환불예정금액
     */
    public static String callByClaimPreRefundPriceOosCalculation(HashMap<String, Object> parameterMap){
        log.info("환불예정금 조회(다건) :: \n[{}]", SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_OOS, parameterMap.values().toArray()));
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_OOS, parameterMap.values().toArray());
    }

    /**
     * 환불예정금액 선물세트(다중배송) 조회 - call procedure
     *
     * @param parameterMap 조회 파라미터
     * @return 환불예정금액
     */
    public static String callByClaimMultiPreRefundPriceCalculation(HashMap<String, Object> parameterMap){
        log.info("환불예정금 조회(다중배송) :: \n[{}]", SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_MULTI_REFUND_CALCULATION_MULTI, parameterMap.values().toArray()));
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_MULTI_REFUND_CALCULATION_MULTI, parameterMap.values().toArray());
    }

}
