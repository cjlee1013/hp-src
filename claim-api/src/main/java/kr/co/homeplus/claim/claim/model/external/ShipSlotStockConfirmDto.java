package kr.co.homeplus.claim.claim.model.external;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ShipSlotStockConfirmDto {

    @ApiModelProperty(value = "주문번호")
    private long purchaseOrderNo;

    @ApiModelProperty(value = "고객번호")
    private String userNo;

    @ApiModelProperty(value = "슬롯재고등록유형(주문:ORD,배송지변경:DLVCHG,반품:RTN,교환:EXCH)")
    private String slotStockRegType;

    @ApiModelProperty(value = "변경할 Ship Dt", reference = "YYYY-MM-DD")
    private String chgShipDt;

    @ApiModelProperty(value = "변경할 Shift ID")
    private String chgShiftId;

    @ApiModelProperty(value = "변경할 Slot ID")
    private String chgSlotId;
}
