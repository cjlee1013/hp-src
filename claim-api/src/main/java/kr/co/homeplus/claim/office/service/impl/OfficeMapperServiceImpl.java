package kr.co.homeplus.claim.office.service.impl;

import java.util.LinkedHashMap;
import kr.co.homeplus.claim.office.mapper.OfficeWriteMapper;
import kr.co.homeplus.claim.office.service.OfficeMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OfficeMapperServiceImpl implements OfficeMapperService {

    private final OfficeWriteMapper writeMapper;

    @Override
    public LinkedHashMap<String, Object> getClaimData(long bundleNo) {
        return writeMapper.selectClaimData(bundleNo);
    }
}
