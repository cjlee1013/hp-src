package kr.co.homeplus.claim.claim.model.mp;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimItemListDto {

    @ApiModelProperty(value = "그룹클레임번호", position = 1)
    private String claimNo;

    @ApiModelProperty(value = "클레임번호", position = 2)
    private String claimBundleNo;

    @ApiModelProperty(value = "상품 번호", position = 3)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 4)
    private String itemName;

    @ApiModelProperty(value="상품가격", position = 5)
    private String itemPrice;

    @ApiModelProperty(value="클레임 수량", position = 6)
    private String claimItemQty;

    @ApiModelProperty(value="행사상품여부", position = 7)
    private String promoYn;

    @ApiModelProperty(value = "대체상품여부", position = 8)
    private String subItemYn;

    @ApiModelProperty(value="대체상품명", position = 9)
    private String subItemNm;

    @ApiModelProperty(value = "파트너ID", position = 10)
    private String partnerId;

    @ApiModelProperty(value = "파트너명", position = 11)
    private String partnerNm;

    @ApiModelProperty(value = "점포유형(HYPER,CLUB,AURORA,EXP,DS", position = 12)
    private String storeType;

    @ApiModelProperty(value = "배송타입 (TD,DS,AURORA,PICK,RESERVE,MULTI,SUM)", position = 13)
    private String shipType;

    @ApiModelProperty(value = "클레임 상태", position = 14)
    private String claimStatus;

    @ApiModelProperty(value = "클레임 상태코드", position = 15)
    private String claimStatusCode;

    @ApiModelProperty(value = "선택옵션1타이틀", position = 16)
    private String selOpt1Title;

    @ApiModelProperty(value = "선택옵션2내용", position = 17)
    private String selOpt1Val;

    @ApiModelProperty(value = "선택옵션2타이틀", position = 18)
    private String selOpt2Title;

    @ApiModelProperty(value = "선택옵션2내용", position = 19)
    private String selOpt2Val;

    @ApiModelProperty(value = "텍스트옵션1타이틀", position = 20)
    private String txtOpt1Title;

    @ApiModelProperty(value = "텍스트옵션1내용", position = 21)
    private String txtOpt1Val;

    @ApiModelProperty(value = "텍스트옵션2타이틀", position = 22)
    private String txtOpt2Title;

    @ApiModelProperty(value = "텍스트옵션2내용", position = 23)
    private String txtOpt2Val;

    @ApiModelProperty(value = "배송방법(DS_DLV:택배배송,DS_DRCT:직접배송,DS_PICK:방문수령/픽업,DS_POST:우편배송,DS_QUICK:퀵배송,TD_DLV:택배배송,TD_DRCT:직접배송,TD_PICK:방문수령,TD_POST:우편배송,TD_QUICK:퀵배송)", position = 24)
    private String shipMethod;

    @ApiModelProperty(value = "운송번호", position = 25)
    private long shipNo;

}
