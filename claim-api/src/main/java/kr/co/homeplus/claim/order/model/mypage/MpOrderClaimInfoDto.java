package kr.co.homeplus.claim.order.model.mypage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마이페이지 > 주문/배송 - 주문조회 응답(클레임정보)")
public class MpOrderClaimInfoDto {

    @ApiModelProperty(value = "주문번호", position = 1)
    private Long purchaseOrderNo;

    @ApiModelProperty(value = "클레임번호", position = 2)
    private Long claimNo;

    @ApiModelProperty(value = "클레임번들번호", position = 3)
    private Long claimBundleNo;

    @ApiModelProperty(value = "클레임번들수량", position = 4)
    private Integer claimBundleQty;

    @ApiModelProperty(value = "클레입타입(C:취소,R:반품,X:교환)", position = 5)
    private String claimType;

    @ApiModelProperty(value = "클레임가능여부", position = 6)
    private String claimYn;

    @ApiModelProperty(value = "클레임진행여부", position = 7)
    private String claimProcessYn;


}
