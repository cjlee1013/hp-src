package kr.co.homeplus.claim.order.model.ship;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@ApiModel(description = "다중배송 안심번호 요청 정보")
@Data
@RequiredArgsConstructor
public class MultiSafetyIssueRequest {

    @ApiModelProperty(value = "다중번들번호", position = 1)
    private long multiBundleNo;

    @ApiModelProperty(value = "요청연락처번호", position = 2)
    private String reqPhoneNo;

}
