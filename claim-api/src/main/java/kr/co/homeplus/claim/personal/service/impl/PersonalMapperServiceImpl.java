package kr.co.homeplus.claim.personal.service.impl;

import java.util.List;
import kr.co.homeplus.claim.personal.mapper.PersonalReadMapper;
import kr.co.homeplus.claim.personal.model.PersonalGetDto;
import kr.co.homeplus.claim.personal.model.PersonalSetDto;
import kr.co.homeplus.claim.personal.service.PersonalMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PersonalMapperServiceImpl implements PersonalMapperService {

    private final PersonalReadMapper readMapper;

    @Override
    public List<PersonalGetDto> getPersonalInfoUsingList(PersonalSetDto personalSetDto) {
        return readMapper.selectPersonalInfoUsingList(personalSetDto);
    }
}
