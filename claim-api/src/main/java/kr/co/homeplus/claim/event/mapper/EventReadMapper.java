package kr.co.homeplus.claim.event.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.core.db.annotation.SecondaryConnection;
import kr.co.homeplus.claim.event.model.PayCompleteByAmtCheckSetDto;
import kr.co.homeplus.claim.event.model.PurchaseByShipCompleteGetDto;
import kr.co.homeplus.claim.event.model.PurchaseByShipCompleteSetDto;
import kr.co.homeplus.claim.event.model.PurchaseEventCheckSetDto;
import kr.co.homeplus.claim.event.model.PurchaseMileagePaybackGetDto.PurchasePaymentList;
import kr.co.homeplus.claim.event.model.PurchaseMileagePaybackSetDto;
import kr.co.homeplus.claim.event.model.ShipCompleteByAmtCheckSetDto;
import kr.co.homeplus.claim.event.sql.EventSql;
import org.apache.ibatis.annotations.SelectProvider;

@SecondaryConnection
public interface EventReadMapper {
    @SelectProvider(type = EventSql.class, method = "selectEventPurchaseOrderCheck")
    LinkedHashMap<String, Object> selectEventPurchaseOrderCheck(PurchaseEventCheckSetDto setDto);

    @SelectProvider(type = EventSql.class, method = "selectEventPurchasePaymentInfo")
    List<PurchasePaymentList> selectEventPurchasePaymentInfo(PurchaseMileagePaybackSetDto setDto);

    @SelectProvider(type = EventSql.class, method = "selectShipCompleteByBasicAmt")
    List<LinkedHashMap<String, Object>> selectShipCompleteByBasicAmt(ShipCompleteByAmtCheckSetDto setDto);

    @SelectProvider(type = EventSql.class, method = "selectShipCompleteByDayLimit")
    List<LinkedHashMap<String, Object>> selectShipCompleteByDayLimit(ShipCompleteByAmtCheckSetDto setDto);

    @SelectProvider(type = EventSql.class, method = "selectNotStoreKindShipCompleteList")
    List<PurchaseByShipCompleteGetDto> selectNotStoreKindShipCompleteList(PurchaseByShipCompleteSetDto setDto);

    @SelectProvider(type = EventSql.class, method = "selectPayCompleteByBasicAmt")
    List<LinkedHashMap<String, Object>> selectPayCompleteByBasicAmt(PayCompleteByAmtCheckSetDto setDto);

    @SelectProvider(type = EventSql.class, method = "selectPayCompleteByDayLimit")
    List<LinkedHashMap<String, Object>> selectPayCompleteByDayLimit(PayCompleteByAmtCheckSetDto setDto);
}
