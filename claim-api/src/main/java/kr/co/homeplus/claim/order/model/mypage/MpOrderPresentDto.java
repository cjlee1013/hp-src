package kr.co.homeplus.claim.order.model.mypage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "선물하기정보")
public class MpOrderPresentDto {
    @ApiModelProperty(value = "선물수락기한", position = 1)
    private String presentExpireDay;
    @ApiModelProperty(value = "선물받는사람", position = 2)
    private String presentReceiverNm;
    @ApiModelProperty(value = "선물수락여부", position = 3)
    private String presentReceiveYn;
    @ApiModelProperty(value = "선물메시지", position = 4)
    private String presentMsg;
    @ApiModelProperty(value = "선물수락일", position = 5)
    private String presentAcpDt;
    @ApiModelProperty(value = "선물자동취소여부", position = 6)
    private String presentAutoCancelYn;

    private void setPresentExpireDay(String presentExpireDay){
        if(Integer.parseInt(presentExpireDay) > 0){
            this.presentExpireDay = presentExpireDay;
        } else {
            this.presentExpireDay = "0";
        }
    }
}
