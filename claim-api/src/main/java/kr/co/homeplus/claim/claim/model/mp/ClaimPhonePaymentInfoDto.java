package kr.co.homeplus.claim.claim.model.mp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "환불수단 변경 DTO")
public class ClaimPhonePaymentInfoDto {

    @ApiModelProperty(value= "클레임 결제번호", position = 1)
    long claimPaymentNo;

    @ApiModelProperty(value= "은행명", position = 2)
    String bankName;

    @ApiModelProperty(value= "환불금액", position = 3)
    long paymentAmt;

    @ApiModelProperty(value= "결제번호", position = 4)
    long paymentNo;
}
