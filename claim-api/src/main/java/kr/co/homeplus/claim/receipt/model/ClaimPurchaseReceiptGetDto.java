package kr.co.homeplus.claim.receipt.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.claim.receipt.model.purchase.ClaimPaymentReceiptInfo;
import kr.co.homeplus.claim.receipt.model.purchase.ClaimReceiptItemInfo;
import kr.co.homeplus.claim.receipt.model.purchase.ClaimReceiptMainInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임_구매영수증")
public class ClaimPurchaseReceiptGetDto {
    @ApiModelProperty(value = "클레임정보", position = 1)
    ClaimReceiptMainInfo claimReceiptMainInfo;
    @ApiModelProperty(value = "클레임아이템정보", position = 2)
    List<ClaimReceiptItemInfo> claimReceiptItemInfoList;
    @ApiModelProperty(value = "클레임결제정보", position = 3)
    List<ClaimPaymentReceiptInfo> claimPaymentReceiptInfoList;
}