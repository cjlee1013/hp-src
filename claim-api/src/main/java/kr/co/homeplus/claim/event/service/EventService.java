package kr.co.homeplus.claim.event.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.claim.enums.ResponseCode;
import kr.co.homeplus.claim.event.model.PayCompleteByAmtCheckGetDto;
import kr.co.homeplus.claim.event.model.PayCompleteByAmtCheckSetDto;
import kr.co.homeplus.claim.event.model.PurchaseByShipCompleteGetDto;
import kr.co.homeplus.claim.event.model.PurchaseByShipCompleteSetDto;
import kr.co.homeplus.claim.event.model.PurchaseEventCheckDto;
import kr.co.homeplus.claim.event.model.PurchaseEventCheckSetDto;
import kr.co.homeplus.claim.event.model.PurchaseMileagePaybackGetDto;
import kr.co.homeplus.claim.event.model.PurchaseMileagePaybackGetDto.PurchasePaymentList;
import kr.co.homeplus.claim.event.model.PurchaseMileagePaybackSetDto;
import kr.co.homeplus.claim.event.model.ShipCompleteByAmtCheckGetDto;
import kr.co.homeplus.claim.event.model.ShipCompleteByAmtCheckSetDto;
import kr.co.homeplus.claim.factory.ResponseFactory;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class EventService {
    private final EventMapperService mapperService;

    public ResponseObject<PurchaseEventCheckDto> getPurchaseEventCheck(PurchaseEventCheckSetDto setDto){
        LinkedHashMap<String, Object> getPurchaseCheckMap = mapperService.getEventPurchaseOrderCheck(setDto);
        if(getPurchaseCheckMap == null){
            return ResponseFactory.getResponseObject(ResponseCode.SUCCESS_CODE, PurchaseEventCheckDto.builder().validPurchaseOrderNo(Boolean.FALSE).build());
        }
        long orderCnt = ObjectUtils.toLong(getPurchaseCheckMap.get("order_cnt"));
        log.info("주문번호({}) 이벤트 체크 : {}", setDto.getPurchaseOrderNo(), getPurchaseCheckMap);
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS_CODE,
            PurchaseEventCheckDto.builder()
                .validPurchaseOrderNo(orderCnt > 0 ? Boolean.TRUE : Boolean.FALSE)
                .build()
        );
    }

    public ResponseObject<PurchaseMileagePaybackGetDto> getEventMileagePayback(PurchaseMileagePaybackSetDto setDto) {
        List<PurchasePaymentList> paymentList = mapperService.getEventPurchasePaymentList(setDto);
        log.info("고객번호({})에 대한 주문금액 리스트 : {}", setDto.getUserNo(), paymentList);
        PurchaseMileagePaybackGetDto responseDto = new PurchaseMileagePaybackGetDto();
        responseDto.setPurchaseSumAmt(0);
        for(PurchasePaymentList list : paymentList){
            responseDto.setPurchaseSumAmt(responseDto.getPurchaseSumAmt() + (list.getPaymentAmt() - list.getClaimAmt()));
        }
        responseDto.setPaymentList(paymentList);
        log.info("고객번호({})에 대한 주문금액 결과 : {}", setDto.getUserNo(), responseDto);
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS_CODE, responseDto);
    }

    public ResponseObject<ShipCompleteByAmtCheckGetDto> getEventShipComplete(ShipCompleteByAmtCheckSetDto setDto) {
        List<LinkedHashMap<String, Object>> shipCompleteList = mapperService.getEventShipCompleteList(setDto);
        ShipCompleteByAmtCheckGetDto responseDto = ShipCompleteByAmtCheckGetDto.builder().completeQty(0).userNo(setDto.getUserNo()).build();
        if(shipCompleteList != null) {
            List<Long> purchaseOrderList = shipCompleteList.stream().filter(map -> ObjectUtils.toInt(map.get("payment_qty")) > 0).filter(map -> ObjectUtils.toInt(map.get("payment_amt")) >= setDto.getBasicAmt()).map(map -> ObjectUtils.toLong(map.get("purchase_order_no"))).collect(Collectors.toList());
            responseDto.setCompleteQty(purchaseOrderList.size());
        }
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS_CODE, responseDto);
    }

    public ResponseObject<ShipCompleteByAmtCheckGetDto> getEventShipCompleteDayLimit(ShipCompleteByAmtCheckSetDto setDto) {
        List<LinkedHashMap<String, Object>> shipCompleteList = mapperService.getEventShipCompleteDayLimitList(setDto);
        ShipCompleteByAmtCheckGetDto responseDto = ShipCompleteByAmtCheckGetDto.builder().completeQty(0).userNo(setDto.getUserNo()).build();
        if(shipCompleteList != null) {
            log.info("유저번호({}) 일제한 이벤트 조회 결과 : {}", setDto.getUserNo(), shipCompleteList);
            List<String> purchaseOrderList = shipCompleteList.stream().filter(map -> ObjectUtils.toInt(map.get("payment_qty")) > 0).filter(map -> ObjectUtils.toInt(map.get("payment_amt")) >= setDto.getBasicAmt()).map(map -> ObjectUtils.toString(map.get("complete_dt"))).distinct().collect(Collectors.toList());
            responseDto.setCompleteQty(purchaseOrderList.size());
        }
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS_CODE, responseDto);
    }

    public ResponseObject<List<PurchaseByShipCompleteGetDto>> getNotStoreKindPurchaseByShipComplete(PurchaseByShipCompleteSetDto setDto) {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS_CODE, mapperService.getNotStoreKindPurchaseByShipComplete(setDto));
    }

    public ResponseObject<PayCompleteByAmtCheckGetDto> getEventPayComplete(PayCompleteByAmtCheckSetDto setDto) {
        List<LinkedHashMap<String, Object>> shipCompleteList = mapperService.getEventPayCompleteList(setDto);
        PayCompleteByAmtCheckGetDto responseDto = PayCompleteByAmtCheckGetDto.builder().completeQty(0).userNo(setDto.getUserNo()).build();
        if(shipCompleteList != null) {
            List<Long> purchaseOrderList = shipCompleteList.stream().filter(map -> ObjectUtils.toInt(map.get("payment_qty")) > 0).filter(map -> ObjectUtils.toInt(map.get("payment_amt")) >= setDto.getBasicAmt()).map(map -> ObjectUtils.toLong(map.get("purchase_order_no"))).collect(Collectors.toList());
            responseDto.setCompleteQty(purchaseOrderList.size());
        }
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS_CODE, responseDto);
    }

    public ResponseObject<PayCompleteByAmtCheckGetDto> getEventPayCompleteDayLimit(PayCompleteByAmtCheckSetDto setDto) {
        List<LinkedHashMap<String, Object>> shipCompleteList = mapperService.getEventPayCompleteDayLimitList(setDto);
        PayCompleteByAmtCheckGetDto responseDto = PayCompleteByAmtCheckGetDto.builder().completeQty(0).userNo(setDto.getUserNo()).build();
        if(shipCompleteList != null) {
            log.info("유저번호({}) 일제한 이벤트 조회 결과 : {}", setDto.getUserNo(), shipCompleteList);
            List<String> purchaseOrderList = shipCompleteList.stream().filter(map -> ObjectUtils.toInt(map.get("payment_qty")) > 0).filter(map -> ObjectUtils.toInt(map.get("payment_amt")) >= setDto.getBasicAmt()).map(map -> ObjectUtils.toString(map.get("payment_fsh_dt"))).distinct().collect(Collectors.toList());
            responseDto.setCompleteQty(purchaseOrderList.size());
        }
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS_CODE, responseDto);
    }
}