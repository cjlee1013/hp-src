package kr.co.homeplus.claim.entity.order;

public class PurchaseStoreInfoEntity {
    // 구매점포번호
    public String purchaseStoreInfoNo;
    // 결제번호
    public String paymentNo;
    // 구매주문번호
    public String purchaseOrderNo;
    // 원천점포ID(매출용실제점포)
    public String originStoreId;
    // 점포ID(주문서에서 넘어온 가상점포ID)
    public String storeId;
    // 점포종류(NOR:일반점포,DLV:택배점포)
    public String storeKind;
    // 거래수량(Pine전송용 상품별 주문수량 SUM, 배송비발생시 1추가, DS는 N개 주문해도 1로 추가)
    public String tradeQty;
    // 거래일자(YYYYMMDD)
    public String tradeDt;
    // POS번호
    public String posNo;
    // 거래번호(영수증번호 Seq)
    public String tradeNo;
    // 배송일자
    public String shipDt;
    // SHIFT ID
    public String shiftId;
    // SLOT ID
    public String slotId;
    // SHIFT마감시간 (DMS실행시간)
    public String shiftCloseTime;
    // SLOT마감시간
    public String slotCloseTime;
    // 총상품가격(점포별)
    public String totItemPrice;
    // 총배송가격(점포별)(총 배송비 가격:고객지불금액)
    public String totShipPrice;
    // 총도서산간배송가격(점포별)(총 도서산간 추가 배송비:고객지불금액)
    public String totIslandShipPrice;
    // 총상품할인금액(점포별)(제휴즉시,상품즉시,상품쿠폰)
    public String totItemDiscountAmt;
    // 총배송비할인금액(점포별)(배송비즉시,배송비쿠폰)
    public String totShipDiscountAmt;
    // 총프로모션할인금액(점포별)
    public String totPromoDiscountAmt;
    // 총임직원할인금액(점포별)
    public String totEmpDiscountAmt;
    // 총카드사할인금액(점포별)(총 카드사즉시할인 금액)
    public String totCardDiscountAmt;
    // 총장바구니할인금액(점포별)(총 장바구니쿠폰)
    public String totCartDiscountAmt;
    // PG결제금액(점포별)
    public String pgAmt;
    // 마일리지결제금액(점포별)
    public String mileageAmt;
    // MHC결제금액(점포별)
    public String mhcAmt;
    // DGV결제금액(점포별)
    public String dgvAmt;
    // OCB결제금액(점포별)
    public String ocbAmt;
    // 임직원남은금액
    public String empRemainAmt;
    // 배송방법 (DS_DLV:택배배송,DS_DRCT:직접배송,DS_PICK:방문수령/픽업,DS_POST:우편배송,DS_QUICK:퀵배송,TD_DLV:택배배송,TD_DRCT:직접배송,TD_PICK:방문수령,TD_QUICK:퀵배송)
    public String shipMethod;
    // 합배송여부(합배송:Y,일반주문:N)
    public String cmbnYn;
    // 픽업여부(점포픽업:Y,자차:N)
    public String pickupYn;
    // 락커여부(락커사용여부 Y:사용,N:미사용)
    public String lockerYn;
    // FC점포ID
    public String fcStoreId;
    // Slot 주문차감 번호(order_slot_stock 테이블 연결고리)
    public String orderSlotStockNo;
    // 원구매주문번호(대체주문발생시)
    public String orgPurchaseOrderNo;
    // 원거래일자(대체주문발생시)
    public String orgTradeDt;
    // 원POS번호(대체주문발생시)
    public String orgPosNo;
    // 원거래번호(대체주문발생시)
    public String orgTradeNo;
    // 원구매점포정보번호(대체주문발생시)
    public String orgPurchaseStoreInfoNo;
    // 슬롯변경횟수
    public String slotChgCnt;
    // 총추가중복TD할인금액(점포별)
    public String totAddTdDiscountAmt;
    // 총추가중복DS할인금액(점포별)
    public String totAddDsDiscountAmt;
    // 애니타임 슬롯 여부
    public String anytimeSlotYn;
}
