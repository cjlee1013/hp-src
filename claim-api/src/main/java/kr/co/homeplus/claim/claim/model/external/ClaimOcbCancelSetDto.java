package kr.co.homeplus.claim.claim.model.external;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "OCB 결제 취소 DTO")
public class ClaimOcbCancelSetDto {
    @ApiModelProperty(notes = "가맹점고유거래ID", required = true, position = 1)
    private String mctTransactionId;
    @ApiModelProperty(notes = "사용거래시 응답받은 OK캐쉬백 고유거래ID", required = true, position = 2)
    private String orgOcbTransactionId;
    @ApiModelProperty(notes = "부분취소여부(기본값 N 세팅)", required = true, position = 3)
    private String partCancelYn = "N";
    @ApiModelProperty(notes = "부분취소할 포인트", position = 4)
    private long cancelOcbPoint;
    @ApiModelProperty(notes = "클레임번", position = 5)
    private long claimNo;
    @ApiModelProperty(notes = "유저번호", required = true, position = 6)
    private String userNo;
}
