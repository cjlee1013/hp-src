package kr.co.homeplus.claim.response;

/**
 * kr.co.homeplus.sampledomain.{업무도메인} 하위 패키지 들에서 공통으로 사용할 VO 클래스를 이 패키지에 담습니다.
 * 그 때 Dto라는 post fix는 붙이지 않습니다.
 */
public class SampleResponse {
}
