package kr.co.homeplus.claim.order.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.claim.claim.model.external.forward.ClaimForwardSetDto;
import kr.co.homeplus.claim.claim.model.register.ClaimProcessHistoryRegDto;
import kr.co.homeplus.claim.claim.service.ExternalService;
import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.enums.ExternalInfo;
import kr.co.homeplus.claim.order.model.inquiry.InquiryItemDto;
import kr.co.homeplus.claim.order.model.inquiry.OrderInquiryGetDto;
import kr.co.homeplus.claim.order.model.inquiry.OrderInquirySetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderProductListGetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderDetailInfoGetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderDetailInfoSetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderDetailListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderTicketDetailGetDto;
import kr.co.homeplus.claim.order.model.mypage.MultiOrderShipAddrEditDto;
import kr.co.homeplus.claim.order.model.mypage.MultiOrderShipBasicInfoEditDto;
import kr.co.homeplus.claim.order.model.ship.MultiOrderShipAddrInfoDto;
import kr.co.homeplus.claim.order.model.ship.MultiSafetyIssueRequest;
import kr.co.homeplus.claim.order.model.ship.SafetyIssueRequest;
import kr.co.homeplus.claim.enums.ResponseCode;
import kr.co.homeplus.claim.factory.ResponseFactory;
import kr.co.homeplus.claim.order.model.mypage.MpOrderDetailListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderDetailListSetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListSetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPaymentListDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPickupStoreInfoDto;
import kr.co.homeplus.claim.order.model.mypage.OrderShipAddrEditDto;
import kr.co.homeplus.claim.order.model.ship.ShipChangeOmniSetDto;
import kr.co.homeplus.claim.order.model.subMain.MpOrderMainDto;
import kr.co.homeplus.claim.order.model.subMain.MpOrderMainDto.MyPageMainOrderCount;
import kr.co.homeplus.claim.order.model.subMain.MpOrderMainDto.MyPageMainOrderList;
import kr.co.homeplus.claim.order.model.user.CustomerGradeInfoGetDto;
import kr.co.homeplus.claim.order.model.user.CustomerGradeInfoGetDto.CustomerGradeOrderListDto;
import kr.co.homeplus.claim.order.model.user.CustomerGradeInfoSetDto;
import kr.co.homeplus.claim.order.model.user.GradeInfoDto;
import kr.co.homeplus.claim.order.model.user.GradeOrderInfoDto;
import kr.co.homeplus.claim.order.model.user.UserGradeDto;
import kr.co.homeplus.claim.order.model.user.UserGradeGetDto;
import kr.co.homeplus.claim.order.model.user.UserGradeInfoDto;
import kr.co.homeplus.claim.utils.LogUtils;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SetParameter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class MpOrderService {

    private final MpOrderMapperService mapperService;
    private final ExternalService externalService;

    public ResponseObject<List<MpOrderListGetDto>> getMyPageOrderList(MpOrderListSetDto setDto) throws Exception {
        List<MpOrderListGetDto> orderList = mapperService.getMpOrderList(setDto);
        for(MpOrderListGetDto dto : orderList){
//            dto.setItemList(mapperService.getMpOrderListInfo(dto.getPurchaseOrderNo(), dto.getBundleNo()));
            Boolean isSubYn = dto.getShipType().equals("TD");
            dto.setItemList(mapperService.getMpOrderListInfo(dto.getPurchaseOrderNo(), dto.getBundleNo(), setDto.getUserNo(), false, isSubYn).stream().filter(itemDto -> itemDto.getItemQty() > 0).collect(Collectors.toList()));

            if(dto.getCmbnYn().equalsIgnoreCase("Y")){
                dto.setCombineItemList(mapperService.getMpOrderListInfo(dto.getPurchaseOrderNo(), dto.getBundleNo(), setDto.getUserNo(), true, isSubYn).stream().filter(itemDto -> itemDto.getItemQty() > 0).collect(Collectors.toList()));
            }

            //
            dto.setClaimInfo(mapperService.getOrderClaimBundleInfo(dto.getPurchaseOrderNo(), dto.getBundleNo(), (dto.getCmbnYn().equals("Y") || isSubYn)));

            if(dto.getShipType().equals("PRESENT")){
                dto.setPresentInfo(mapperService.getMpOrderPresentInfo(dto.getPurchaseOrderNo()));
            }
//            if(dto.getCmbnYn().equalsIgnoreCase("Y") || dto.getSubstitutionOrderYn().equalsIgnoreCase("Y")){
//                // 합배송/대체 일 경우만
//                dto.setClaimInfo(mapperService.getOrderClaimBundleInfo(dto.getPurchaseOrderNo()));
//            } else {
//                dto.setClaimInfo(mapperService.getOrderClaimBundleInfo(dto.getPurchaseOrderNo(), dto.getBundleNo()));
//            }
        }

        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, orderList);
    }

    public ResponseObject<MpOrderDetailListGetDto> getMyPageOrderDetailInfo(MpOrderDetailListSetDto detailSetDto) throws Exception {
        long purchaseOrderNo = ObjectUtils.toLong(detailSetDto.getPurchaseOrderNo());
        long userNo =  ObjectUtils.toLong(detailSetDto.getUserNo());
        String siteType = detailSetDto.getSiteType();
        // 주문조회 상세
        MpOrderDetailListGetDto detailGetDto = mapperService.getOrderDetailInfo(purchaseOrderNo, userNo, siteType, Boolean.FALSE);
        if(detailGetDto == null){
            throw new LogicException(ResponseCode.CLAIM_REG_ERR05);
        }
        if("ORD_NOR,ORD_COMBINE,ORD_SUB,ORD_CALL,ORD_TICKET".contains(detailGetDto.getOrderType())){
            // 상품상세정보 - 합배송 제외 (원주문 + 대체만 포함)
            detailGetDto.setOrderDetailInfo(mapperService.getMpOrderProductList(purchaseOrderNo, detailSetDto.getUserNo(), Boolean.FALSE));
            // 배송정보
            detailGetDto.setShipInfo(mapperService.getMpOrderShipInfo(detailSetDto));
            // 클레임 정보
            detailGetDto.setClaimInfoList(mapperService.getOrderDetailClaimInfo(purchaseOrderNo));
            // 다중 배송 조회.
        } else if("ORD_MULTI".contains(detailGetDto.getOrderType())){
            throw new LogicException(ResponseCode.ORDER_SEARCH_DETAIL_ERR_02);
        }
        // 결제정보
        detailGetDto.setOrderPaymentInfo(
            MpOrderPaymentListDto.builder()
                .orderPaymentInfo(mapperService.getPaymentInfoList(purchaseOrderNo, detailSetDto.getUserNo()))
                .claimPaymentInfo(mapperService.getClaimPaymentInfoList(purchaseOrderNo, detailSetDto.getUserNo()))
                .build()
        );

        // 합배송여부 주문 조회
        if(detailGetDto.getCmbnYn().equals("Y")) {
            MpOrderDetailListGetDto cmbnPurchaseOrderInfo = mapperService.getOrderDetailInfo(purchaseOrderNo, userNo, siteType, Boolean.TRUE);
            detailGetDto.setCombineDetailList(mapperService.getMpOrderProductList(cmbnPurchaseOrderInfo.getPurchaseOrderNo(), detailSetDto.getUserNo(), Boolean.TRUE));
            // 결제정보
            detailGetDto.setCombinePaymentInfo(
                MpOrderPaymentListDto.builder()
                    .orderPaymentInfo(mapperService.getPaymentInfoList(cmbnPurchaseOrderInfo.getPurchaseOrderNo(), detailSetDto.getUserNo()))
                    .claimPaymentInfo(mapperService.getClaimPaymentInfoList(cmbnPurchaseOrderInfo.getPurchaseOrderNo(), detailSetDto.getUserNo()))
                    .build()
            );
        }
        detailGetDto.setOrderGiftList(mapperService.getOrderGiftInfo(purchaseOrderNo));
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, detailGetDto);
    }


    public ResponseObject<MpMultiOrderDetailListGetDto> getMyPageMultiOrderDetailList(MpOrderDetailListSetDto detailSetDto) throws Exception {
        long purchaseOrderNo = ObjectUtils.toLong(detailSetDto.getPurchaseOrderNo());
        long userNo =  ObjectUtils.toLong(detailSetDto.getUserNo());
        String siteType = detailSetDto.getSiteType();
        // 주문조회 상세
        MpMultiOrderDetailListGetDto detailGetDto = mapperService.getMultiOrderDetailList(purchaseOrderNo, userNo, siteType);
        detailGetDto.setShipInfo(mapperService.getMpMultiOrderShipList(detailSetDto));
        // 결제정보
        detailGetDto.setOrderPaymentInfo(
            MpOrderPaymentListDto.builder()
                .orderPaymentInfo(mapperService.getPaymentInfoList(purchaseOrderNo, detailSetDto.getUserNo()))
                .claimPaymentInfo(mapperService.getClaimPaymentInfoList(purchaseOrderNo, detailSetDto.getUserNo()))
                .build()
        );
        detailGetDto.setOrderGiftList(mapperService.getOrderGiftInfo(purchaseOrderNo));
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, detailGetDto);
    }

    public ResponseObject<MpMultiOrderDetailInfoGetDto> getMyPageMultiOrderDetailItemInfo(MpMultiOrderDetailInfoSetDto detailSetDto) throws Exception {
        long purchaseOrderNo = ObjectUtils.toLong(detailSetDto.getPurchaseOrderNo());
        long multiBundleNo = ObjectUtils.toLong(detailSetDto.getMultiBundleNo());
        long userNo =  ObjectUtils.toLong(detailSetDto.getUserNo());
        String siteType = detailSetDto.getSiteType();
        // 주문조회 상세
        MpMultiOrderDetailListGetDto detailGetDto = mapperService.getMultiOrderDetailList(purchaseOrderNo, userNo, siteType);
        if(detailGetDto == null){
            throw new LogicException(ResponseCode.CLAIM_REG_ERR05);
        }
        MpMultiOrderDetailInfoGetDto responseDto = new MpMultiOrderDetailInfoGetDto();
        if("ORD_MULTI".contains(detailGetDto.getOrderType())){
            // 상품상세정보 - 합배송 제외 (원주문 + 대체만 포함)
            responseDto.setOrderDetailInfo(mapperService.getMpOrderMultiProductList(purchaseOrderNo, multiBundleNo, detailSetDto.getUserNo()));
            // 배송정보
            responseDto.setShipInfo(mapperService.getMpOrderMultiShipInfo(detailSetDto));
            // 클레임 정보
//            responseDto.setClaimInfoList(mapperService.getMultiOrderDetailClaimInfo(purchaseOrderNo));
        } else {
            throw new LogicException(ResponseCode.ORDER_SEARCH_DETAIL_ERR_03);
        }
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, responseDto);
    }

    public ResponseObject<List<MpOrderProductListGetDto>> getMypageMultiOrderItemList(MpOrderDetailListSetDto detailSetDto) throws Exception {
        long purchaseOrderNo = ObjectUtils.toLong(detailSetDto.getPurchaseOrderNo());
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, mapperService.getMpOrderProductList(purchaseOrderNo, detailSetDto.getUserNo(), Boolean.FALSE));
    }

    public ResponseObject<MpOrderMainDto> getMyPageMainOrderInfo(long userNo, String siteType) throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            MpOrderMainDto.builder().orderCount(getMyPageMainOrderCount(userNo, siteType)).orderList(getMyPageMainOrderList(userNo, siteType)).build()
        );
    }

    public ResponseObject<List<MyPageMainOrderCount>> getMainOrderCnt(long userNo, String siteType) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, getMyPageMainOrderCount(userNo, siteType));
    }

    public ResponseObject<List<MyPageMainOrderList>> getMainOrderList(long userNo, String siteType) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, getMyPageMainOrderList(userNo, siteType));
    }

    private List<MyPageMainOrderCount> getMyPageMainOrderCount(long userNo, String siteType) throws Exception {
        return mapperService.getMyPageMainOrderCount(userNo, siteType);
    }

    private List<MyPageMainOrderList> getMyPageMainOrderList(long userNo, String siteType) throws Exception {
        return mapperService.getMyPageMainOrderList(userNo, siteType);
    }

    /**
     * 주문관리 상세 - 주문 > 주소지변경
     *
     * @param shipAddrEditDto 주소지변경 데이터
     * @return 주소지 변경 결과
     */
    @Transactional(rollbackFor = {Exception.class, LogicException.class}, propagation = Propagation.NESTED, isolation = Isolation.READ_COMMITTED)
    public ResponseObject<String> modifyShipAddrChange(OrderShipAddrEditDto shipAddrEditDto) throws Exception {
        // 주소지변경시 D2인 경우에 주소지변경실패로 리턴.
        // 현재 상태 확인.
        if(shipAddrEditDto.getShipMsg() != null) {
            if (shipAddrEditDto.getShipMsg().length() > 30) {
                throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_06);
            }
        }
        if(shipAddrEditDto.getAuroraShipMsg() != null){
            if(shipAddrEditDto.getAuroraShipMsg().length() > 30){
                throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_06);
            }
        }

        List<LinkedHashMap<String, Object>> shipPresentInfo = mapperService.getShippingPresentStatus(shipAddrEditDto.getPurchaseOrderNo());
        if(shipPresentInfo.size() <= 0){
            throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_05);
        }
        if(!shipPresentInfo.stream().map(map -> map.get("ship_status")).map(ObjectUtils::toString).allMatch(status -> status.equalsIgnoreCase("D1"))){
            if(shipPresentInfo.stream().map(map -> map.get("ship_status")).map(ObjectUtils::toString).allMatch(status -> status.equalsIgnoreCase("NN"))){
                throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_03);
            } else {
                throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_02);
            }
        }

        boolean isTdOnly = shipPresentInfo.stream().map(map -> map.get("ship_type")).map(ObjectUtils::toString).allMatch(status -> status.equalsIgnoreCase("TD"));
        boolean isDsOnly = shipPresentInfo.stream().map(map -> map.get("ship_type")).map(ObjectUtils::toString).allMatch(status -> status.equalsIgnoreCase("DS"));
        boolean isCombine = shipPresentInfo.stream().map(map -> map.get("order_type")).map(ObjectUtils::toString).anyMatch(status -> status.equalsIgnoreCase("ORD_COMBINE"));
        boolean isTd = shipPresentInfo.stream().map(map -> map.get("ship_type")).map(ObjectUtils::toString).anyMatch(status -> status.equalsIgnoreCase("TD"));

        // 리스트로 처리
        List<Long> bundleList = new ArrayList<>();
        // 전부 TD가 아니거나 DS가 아닌 경우 복합으로처리.
        if((!isDsOnly && !isTdOnly) || (isTdOnly && isCombine) || isDsOnly){
            for(LinkedHashMap<String, Object> map : shipPresentInfo){
                bundleList.add(ObjectUtils.toLong(map.get("bundle_no")));
            }
        } else {
            bundleList.add(shipAddrEditDto.getBundleNo());
        }

        for(long bundleNo : bundleList){
            StringBuilder beforeDesc = new StringBuilder();
            StringBuilder afterDesc = new StringBuilder();
            shipAddrEditDto.setBundleNo(bundleNo);
            LinkedHashMap<String, Object> safetyIssueInfo = mapperService.getSafetyIssueInfo(shipAddrEditDto.getBundleNo());
            if(!ObjectUtils.toString(safetyIssueInfo.get("ship_mobile_no")).equals(shipAddrEditDto.getShipMobileNo())){
                // 전화번호 변경시
                beforeDesc.append(safetyIssueInfo.get("ship_mobile_no"));
                afterDesc.append(shipAddrEditDto.getShipMobileNo());
                // 전화번호 안신번호 변경 요청.
                // 안심번호 신청여부 판단
                if(ObjectUtils.toString(safetyIssueInfo.get("safety_use_yn")).equalsIgnoreCase("Y")){
                    try {
                        externalService.postTransfer(
                            ExternalInfo.ESCROW_ORDER_SAFETY_ISSUE_UPDATE,
                            createSafetyUpdateDto(
                                shipAddrEditDto.getBundleNo(),
                                ObjectUtils.toLong(safetyIssueInfo.get("purchase_order_no")),
                                shipAddrEditDto.getShipMobileNo()
                            ),
                            Integer.class
                        );
                    } catch (Exception e){
                        log.error("safetyIssue Update Error ::: {}", e.getMessage());
                        throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_04);
                    }
                }
            };

            if(!ObjectUtils.toString(safetyIssueInfo.get("road_base_addr")).equals(shipAddrEditDto.getRoadBaseAddr()) ||
                !ObjectUtils.toString(safetyIssueInfo.get("road_detail_addr")).equals(shipAddrEditDto.getRoadDetailAddr()) ){
                beforeDesc.append("\n".concat(ObjectUtils.toString(safetyIssueInfo.get("zipcode"))).concat("\n").concat(ObjectUtils.toString(safetyIssueInfo.get("road_base_addr"))).concat(" ").concat(ObjectUtils.toString(safetyIssueInfo.get("road_detail_addr"))));
                afterDesc.append("\n".concat(shipAddrEditDto.getZipCode()).concat("\n").concat(shipAddrEditDto.getRoadBaseAddr()).concat(" ").concat(shipAddrEditDto.getRoadDetailAddr()));
            }

            if(!ObjectUtils.toString(safetyIssueInfo.get("receiver_nm")).equals(shipAddrEditDto.getReceiverNm())) {
                beforeDesc.append("\n".concat(ObjectUtils.toString(safetyIssueInfo.get("receiver_nm"))));
                afterDesc.append("\n".concat(shipAddrEditDto.getReceiverNm()));
            }

            if(shipAddrEditDto.getShipType().equalsIgnoreCase("TD")) {
                if(!ObjectUtils.toString(safetyIssueInfo.get("store_ship_msg"), "").equals(shipAddrEditDto.getShipMsg())) {
                    beforeDesc.append("\n".concat(ObjectUtils.toString(safetyIssueInfo.get("store_ship_msg"), "")));
                    afterDesc.append("\n".concat(shipAddrEditDto.getShipMsg()));
                }
            } else {
                if(!ObjectUtils.toString(safetyIssueInfo.get("dlv_ship_msg"), "").equals(shipAddrEditDto.getShipMsg())) {
                    beforeDesc.append("\n".concat(ObjectUtils.toString(safetyIssueInfo.get("dlv_ship_msg"), "")));
                    afterDesc.append("\n".concat(shipAddrEditDto.getShipMsg()));
                }
            }

            if(!ObjectUtils.toString(safetyIssueInfo.get("aurora_ship_msg"), "").equals(shipAddrEditDto.getAuroraShipMsg())) {
                beforeDesc.append("\n".concat(ObjectUtils.toString(safetyIssueInfo.get("aurora_ship_msg"), "")));
                afterDesc.append("\n".concat(ObjectUtils.toString(shipAddrEditDto.getAuroraShipMsg(), "")));
            }

            if(!StringUtils.isEmpty(ObjectUtils.toString(safetyIssueInfo.get("reserve_ship_dt")))){
                if(!ObjectUtils.toString(safetyIssueInfo.get("reserve_ship_dt"), "").equals(shipAddrEditDto.getReserveShipDt())) {
                    LogUtils.addLogHistory(shipAddrEditDto, "희망배송일 수정", shipAddrEditDto.getReserveShipDt(), ObjectUtils.toString(safetyIssueInfo.get("reserve_ship_dt"), ""));
                }
            }

            if(beforeDesc.toString().length() > 0) {
                LogUtils.addLogHistory(shipAddrEditDto, "배송정보 수정", afterDesc.toString(), beforeDesc.toString());
            }

            if(!mapperService.modifyShipAddrChange(shipAddrEditDto)){
                throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_01);
            }
        }
        // 옴니전송.
        if(isTd){
            LinkedHashMap<String, Object> paramMap = shipPresentInfo.stream().filter(map -> ObjectUtils.isEquals("ORD_NOR", map.get("order_type")) && ObjectUtils.isEquals("TD", map.get("ship_type"))).collect(Collectors.toList()).get(0);
            List<Object> combineList = shipPresentInfo.stream().filter(map -> ObjectUtils.isEquals("ORD_COMBINE", map.get("order_type")) && ObjectUtils.isEquals("TD", map.get("ship_type"))).map(map -> map.get("purchase_store_info_no")).collect(Collectors.toList());
            if(combineList.size() > 0) {
                paramMap.put("combine_purchase_store_info_no", combineList.get(0));
            }
            //ShipChangeOmniSetDto 을 만들어야 함.
            try {
                paramMap.putAll(ObjectUtils.getConvertMap(shipAddrEditDto));
                ResponseObject<Object> result = externalService.externalToOmni(createShipChangeOmniDto(paramMap));
                log.info("주소지 변경에 따른 옴니 전송결과 :: [purchaseOrderNo : {}, returnCode : {}, returnMsg : {}]", shipAddrEditDto.getPurchaseOrderNo(), result.getReturnCode(), result.getReturnMessage());
                if(!"0000,SUCCESS".contains(result.getReturnCode())){
                    throw new LogicException(result.getReturnCode(), result.getReturnMessage());
                }
            } catch (Exception e) {
                log.error("주문번호({}) 주소지 변경 옴니 요청중 오류가 발생했습니다. ::: {}", shipAddrEditDto.getPurchaseOrderNo(), e.getMessage());
                throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_01);
            }
            if(shipAddrEditDto.getAuroraSaveYn() != null && shipAddrEditDto.getAuroraSaveYn().equals("Y")) {
                if(!mapperService.modifyOrderUserRecentlyInfo(shipAddrEditDto.getAuroraShipMsgCd(), shipAddrEditDto.getAuroraShipMsg(), ObjectUtils.toLong(paramMap.get("user_no")))){
                    log.error("주문번호({}) 출입현관정보 저장 중 오류가 발생했습니다.", shipAddrEditDto.getPurchaseOrderNo());
                }
            }
        }
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS);
    }

    public ResponseObject<MpOrderPickupStoreInfoDto> getOrderPickupStoreInfo(long purchaseOrderNo) {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, mapperService.getOrderPickupStoreInfo(purchaseOrderNo));
    }

    public ResponseObject<MpOrderTicketDetailGetDto> getOrderTicketDetailInfo(MpOrderDetailListSetDto detailSetDto) {
        MpOrderTicketDetailGetDto returnDto = mapperService.getOrderTicketDetailInfo(detailSetDto.getPurchaseOrderNo(), detailSetDto.getUserNo());
        returnDto.setTicketItemList(mapperService.getOrderTicketDetailItemInfo(detailSetDto.getPurchaseOrderNo(), detailSetDto.getUserNo()));
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            returnDto
        );
    }

    public ResponseObject<GradeOrderInfoDto> getUserGradeOrderInfo(long userNo, String siteType) {
        String searchDt = new SimpleDateFormat("yyyy-MM-01").format(new Date());
        List<CustomerGradeOrderListDto> infoDto = mapperService.getUserGradeOrderInfo(userNo, searchDt, siteType);
        List<CustomerGradeOrderListDto> claimInfoDto = mapperService.getUserGradeOrderInfoByClaim(userNo, searchDt, siteType);

        int orderPrice = infoDto.stream().mapToInt(CustomerGradeOrderListDto::getOrderPrice).sum();
        int orderCnt = infoDto.stream().mapToInt(CustomerGradeOrderListDto::getOrderCnt).sum();
        int claimPrice = claimInfoDto.stream().mapToInt(CustomerGradeOrderListDto::getOrderPrice).sum();
        int claimCnt = claimInfoDto.stream().mapToInt(CustomerGradeOrderListDto::getOrderCnt).sum();

        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            GradeOrderInfoDto.builder().orderCnt((orderCnt + claimCnt)).orderPrice((orderPrice + claimPrice)).userNo(userNo).build()
        );
    }

    public ResponseObject<CustomerGradeInfoGetDto> getCustomerGradeOrderInfo(CustomerGradeInfoSetDto setDto) {
        String searchDt = setDto.getSearchYear().concat("-").concat(setDto.getSearchMonth()).concat("-01");

        List<CustomerGradeOrderListDto> orderList = mapperService.getUserGradeOrderInfo(setDto.getUserNo(), searchDt, null);
        List<CustomerGradeOrderListDto> claimInfoList = mapperService.getUserGradeOrderInfoByClaim(setDto.getUserNo(), searchDt, null);

        int orderPrice = orderList.stream().mapToInt(CustomerGradeOrderListDto::getOrderPrice).sum();
        int orderCnt = orderList.stream().mapToInt(CustomerGradeOrderListDto::getOrderCnt).sum();
        int claimPrice = claimInfoList.stream().mapToInt(CustomerGradeOrderListDto::getOrderPrice).sum();
        int claimCnt = claimInfoList.stream().mapToInt(CustomerGradeOrderListDto::getOrderCnt).sum();

        if(claimInfoList.size() > 0){
            orderList.addAll(claimInfoList);
        }

        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            CustomerGradeInfoGetDto.builder()
                .orderInfo(GradeOrderInfoDto.builder().orderCnt((orderCnt + claimCnt)).orderPrice((orderPrice + claimPrice)).userNo(setDto.getUserNo()).build())
                .orderList(orderList)
                .build()
        );
    }

    public ResponseObject<UserGradeGetDto> getCustomerGradeInfo(long userNo) throws Exception {
        UserGradeInfoDto userGradeInfo = externalService.getTransfer(ExternalInfo.USER_API_USER_GRADE_INFO, new ArrayList<SetParameter>() { { add(SetParameter.create("userNo", userNo)); }}, UserGradeInfoDto.class);
        // 고객 등급 sort
        List<UserGradeDto> gradeList = userGradeInfo.getGradeDefinitionList().stream().sorted(Comparator.comparing(UserGradeDto::getGradeSeq).reversed()).collect(Collectors.toList());

        String searchDt = new SimpleDateFormat("yyyy-MM-01").format(new Date());
        List<CustomerGradeOrderListDto> orderList = mapperService.getUserGradeOrderInfo(userNo, searchDt, null);
        List<CustomerGradeOrderListDto> claimInfoList = mapperService.getUserGradeOrderInfoByClaim(userNo, searchDt, null);

        int orderPrice = orderList.stream().mapToInt(CustomerGradeOrderListDto::getOrderPrice).sum();
        int orderCnt = orderList.stream().mapToInt(CustomerGradeOrderListDto::getOrderCnt).sum();
        int claimPrice = claimInfoList.stream().mapToInt(CustomerGradeOrderListDto::getOrderPrice).sum();
        int claimCnt = claimInfoList.stream().mapToInt(CustomerGradeOrderListDto::getOrderCnt).sum();
        int selected = 0;

        for(UserGradeDto dto : gradeList) {
            //조건1 : 주문건수
            Boolean condition1 = dto.getOrderCnt() <= (orderCnt + claimCnt);
            //조건2 : 최저금액 이상 최저금액 이하
            Boolean condition2 = (dto.getOrderMinAmount() <= (orderPrice + claimPrice) && dto.getOrderMaxAmount() >= (orderPrice + claimPrice));

            if(dto.getComparisonRule().equalsIgnoreCase("OR")){
                if(condition1 || condition2){
                    break;
                }
            } else {
                if(condition1 && condition2){
                    break;
                }
            }
            // check
            selected++;
        }
        UserGradeDto next = gradeList.get(selected);

        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            UserGradeGetDto.builder()
                .current(userGradeInfo.getMyGradeInfo())
                .next(GradeInfoDto.builder().gradeSeq(next.getGradeSeq()).gradeNm(next.getGradeNm()).orderAmount((orderPrice + claimPrice)).orderCnt((orderCnt + claimCnt)).build())
                .expect(gradeList.get((selected <= 0) ? 0 : (selected - 1)))
                .build()
        );
    }

    public ResponseObject<List<OrderInquiryGetDto>> getInquiryOrderItemList(OrderInquirySetDto setDto) {
        List<OrderInquiryGetDto> orderInquiryInfoList = mapperService.getInquiryOrderItemInfo(setDto);
        for(OrderInquiryGetDto dto : orderInquiryInfoList){
            List<InquiryItemDto> itemList = mapperService.getInquiryOrderItemList(dto.getBundleNo());

            if(itemList.size() == 0){
                orderInquiryInfoList.removeIf(infoList -> infoList.getBundleNo() == dto.getBundleNo());
            } else {
                dto.setItemList(itemList);
            }
        }
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, orderInquiryInfoList);
    }

    public ResponseObject<String> modifyMultiShipAddrChange(MultiOrderShipAddrEditDto multiOrderShipAddrEditDto) throws Exception{
        List<LinkedHashMap<String, Object>> shipPresentInfo = mapperService.getShippingPresentStatus(multiOrderShipAddrEditDto.getPurchaseOrderNo());
        if(shipPresentInfo.size() <= 0){
            throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_05);
        }
        if(!shipPresentInfo.stream().map(map -> map.get("ship_status")).map(ObjectUtils::toString).allMatch(status -> status.equalsIgnoreCase("D1"))){
            if(shipPresentInfo.stream().map(map -> map.get("ship_status")).map(ObjectUtils::toString).allMatch(status -> status.equalsIgnoreCase("NN"))){
                throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_03);
            } else {
                throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_02);
            }
        }
        // 멀티번들용 전화번호 안심번호 설정은 추루.
        StringBuilder beforeDesc = new StringBuilder();
        StringBuilder afterDesc = new StringBuilder();
        List<MultiOrderShipAddrInfoDto> shipAddrInfo = mapperService.getMultiOrderShipAddrInfo(multiOrderShipAddrEditDto.getMultiBundleNo());
        if(!shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getShipMobileNo).allMatch(str -> str.equals(multiOrderShipAddrEditDto.getShipMobileNo()))){
            // 전화번호 변경여부
            beforeDesc.append(shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getShipMobileNo).collect(Collectors.toList()).get(0));
            afterDesc.append(multiOrderShipAddrEditDto.getShipMobileNo());
            // 안심번호 신청여부 판단
            if(shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getSafetyUseYn).anyMatch(str -> str.equals("Y"))){
                try {
                    externalService.postTransfer(
                        ExternalInfo.ESCROW_ORDER_MULTI_SAFETY_ISSUE_UPDATE,
                        createMultiSafetyUpdateDto(
                            multiOrderShipAddrEditDto.getMultiBundleNo(),
                            multiOrderShipAddrEditDto.getShipMobileNo()
                        ),
                        Integer.class
                    );
                } catch (Exception e) {
                    log.error("safetyIssue Update Error ::: {}", e.getMessage());
                    throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_04);
                }
            }
        }

        if(!shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getReceiverNm).allMatch(str -> str.equals(multiOrderShipAddrEditDto.getReceiverNm()))){
            beforeDesc.append("\n".concat(shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getReceiverNm).collect(Collectors.toList()).get(0)));
            afterDesc.append("\n".concat(multiOrderShipAddrEditDto.getReceiverNm()));
        }

        if(!shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getRoadBaseAddr).allMatch(str -> str.equals(multiOrderShipAddrEditDto.getRoadBaseAddr()))
            || !shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getRoadDetailAddr).allMatch(str -> str.equals(multiOrderShipAddrEditDto.getRoadDetailAddr()))
        ){
            beforeDesc.append("\n".concat(shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getZipCode).collect(Collectors.toList()).get(0)).concat("\n").concat(shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getRoadBaseAddr).collect(Collectors.toList()).get(0)).concat("\n".concat(shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getRoadDetailAddr).collect(Collectors.toList()).get(0))));
            afterDesc.append("\n".concat(multiOrderShipAddrEditDto.getZipCode()).concat("\n").concat(multiOrderShipAddrEditDto.getRoadBaseAddr()).concat(" ").concat(multiOrderShipAddrEditDto.getRoadDetailAddr()));
        }

        if(beforeDesc.toString().length() > 0) {
            LogUtils.addLogHistory(multiOrderShipAddrEditDto, "배송정보 수정", afterDesc.toString(), beforeDesc.toString());
        }

        // 멀티번들 주소 변경
        if(!mapperService.modifyMultiOrderShipAddrInfo(multiOrderShipAddrEditDto)){
            throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_01);
        }

        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS);
    }

    public ResponseObject<String> modifyMultiShipBasicInfoChange(MultiOrderShipBasicInfoEditDto multiOrderShipBasicInfoEditDto) throws Exception{
        List<LinkedHashMap<String, Object>> shipPresentInfo = mapperService.getShippingPresentStatus(multiOrderShipBasicInfoEditDto.getPurchaseOrderNo());
        if(shipPresentInfo.size() <= 0){
            throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_05);
        }
        if(!shipPresentInfo.stream().map(map -> map.get("ship_status")).map(ObjectUtils::toString).allMatch(status -> status.equalsIgnoreCase("D1"))){
            if(shipPresentInfo.stream().map(map -> map.get("ship_status")).map(ObjectUtils::toString).allMatch(status -> status.equalsIgnoreCase("NN"))){
                throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_03);
            } else {
                throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_02);
            }
        }

        List<MultiOrderShipAddrInfoDto> shipAddrInfo = mapperService.getMultiOrderShipAddrInfo(multiOrderShipBasicInfoEditDto.getMultiBundleNo());
        if(!shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getReserveShipDt).allMatch(str -> str.equals(multiOrderShipBasicInfoEditDto.getReserveShipDt()))){
            LogUtils.addLogHistory(multiOrderShipBasicInfoEditDto, "희망배송일 수정", multiOrderShipBasicInfoEditDto.getReserveShipDt(), shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getReserveShipDt).collect(Collectors.toList()).get(0));
        }

        log.info("주문번호({})에 대한 다중배송정보 변경 요청 : {}", multiOrderShipBasicInfoEditDto.getPurchaseOrderNo(), multiOrderShipBasicInfoEditDto);

        // 멀티번들 주소 변경
        if(!mapperService.modifyMultiOrderShipBasicInfo(multiOrderShipBasicInfoEditDto)){
            throw new LogicException(ResponseCode.ORDER_SHIP_ADDR_CHANGE_ERR_01);
        }

        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS);
    }

    private ClaimForwardSetDto createShipChangeOmniDto(LinkedHashMap<String, Object> paramMap) {
        return ClaimForwardSetDto.builder()
            .transactionId("omni")
            .transactionUrl("/modifyRcptInfoFull")
            .httpMethod(HttpMethod.POST)
            .data(
                ShipChangeOmniSetDto.builder()
                    .dlvMsg(ObjectUtils.toString(paramMap.get("shipMsg")))
                    .rcptName(ObjectUtils.toString(paramMap.get("receiverNm")))
                    .rcptAddr(ObjectUtils.toString(paramMap.get("roadDetailAddr")))
                    .rcptPhone(ObjectUtils.toString(paramMap.get("shipMobileNo")))
                    .storeId(StringUtils.leftPad(ObjectUtils.toString(paramMap.get("origin_store_id")), 4, "0"))
                    .ordNo(ObjectUtils.toString(paramMap.get("purchase_store_info_no")))
                    .combineOrdNo(ObjectUtils.toString(paramMap.get("combine_purchase_store_info_no")))
                    .doorPasswordCode(ObjectUtils.toString(paramMap.get("auroraShipMsgCd")))
                    .doorPassword(ObjectUtils.toString(paramMap.get("auroraShipMsg")))
                    .build()
            )
            .build();
    }

    private SafetyIssueRequest createSafetyUpdateDto(long bundleNo, long purchaseOrderNo, String changeMobileNo){
        SafetyIssueRequest safetyIssueRequest = new SafetyIssueRequest();
        safetyIssueRequest.setBundleNo(bundleNo);
        safetyIssueRequest.setPurchaseOrderNo(purchaseOrderNo);
        safetyIssueRequest.setReqPhoneNo(changeMobileNo);
        return safetyIssueRequest;
    }

    private MultiSafetyIssueRequest createMultiSafetyUpdateDto(long multiBundleNo, String changeMobileNo){
        MultiSafetyIssueRequest safetyIssueRequest = new MultiSafetyIssueRequest();
        safetyIssueRequest.setMultiBundleNo(multiBundleNo);
        safetyIssueRequest.setReqPhoneNo(changeMobileNo);
        return safetyIssueRequest;
    }
}
