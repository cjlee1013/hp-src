package kr.co.homeplus.claim.receipt.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.claim.enums.StoreType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임_결제수단별영수증")
public class ClaimPurchaseMethodReceiptSetDto {
    @ApiModelProperty(value = "클레임번호", position = 1)
    private long claimNo;
    @ApiModelProperty(value = "고객번호", position = 2)
    private long userNo;
    @ApiModelProperty(value = "파트너아이디", position = 3)
    private String partnerId;
    @ApiModelProperty(value = "점포타입", position = 4)
    private StoreType storeType;
    @ApiModelProperty(value = "배송번호", position = 5)
    private long bundleNo;
}
