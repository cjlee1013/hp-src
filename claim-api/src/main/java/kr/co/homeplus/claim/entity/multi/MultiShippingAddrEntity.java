package kr.co.homeplus.claim.entity.multi;

public class MultiShippingAddrEntity {
    // 다중배송주소번호
    public String multiShipAddrNo;
    // 구매주문번호
    public String purchaseOrderNo;
    // 번들번호
    public String bundleNo;
    // 다중번들번호
    public String multiBundleNo;
    // 주문자명
    public String buyerNm;
    // 주문자모바일번호(휴대폰번호)
    public String buyerMobileNo;
    // 주문자 주소지 우편번호
    public String buyerZipcode;
    // 주문자 배송지 기본주소
    public String buyerBaseAddr;
    // 주문자 배송지 상세주소
    public String buyerDetailAddr;
    // 주문자 배송지 도로명기본주소
    public String buyerRoadBaseAddr;
    // 주문자 배송지 도로명상세주소
    public String buyerRoadDetailAddr;
    // 받는사람명
    public String receiverNm;
    // 우편번호
    public String zipcode;
    // 기본주소
    public String baseAddr;
    // 상세주소
    public String detailAddr;
    // 도로명기본주소
    public String roadBaseAddr;
    // 도로명상세주소
    public String roadDetailAddr;
    // 배송모바일번호(휴대폰번호)
    public String shipMobileNo;
    // 안심번호사용여부(Y:사용,N:미사용)
    public String safetyUseYn;
    // 선물메시지
    public String giftMsg;
    // 등록일시
    public String regDt;
    // 수정일시
    public String chgDt;
    // 수정자ID
    public String chgId;

}
