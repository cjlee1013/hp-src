package kr.co.homeplus.claim.entity.order;

public class OrderItemEntity {
    // 주문상품번호
    public String orderItemNo;
    // 구매주문번호
    public String purchaseOrderNo;
    // 번들번호
    public String bundleNo;
    // 원천점포ID(매출용실제점포)
    public String originStoreId;
    // 점포ID (주문서에서 넘어온 점포ID)
    public String storeId;
    // FC상품여부
    public String fcItemYn;
    // 상품유형(B:반품,N:새상품,R:리퍼,U:중고)
    public String itemType;
    // 예약배송방법(RESERVE_TD_DRCT:예약자차계열, RESERVE_TD_DLV:예약택배계열)
    public String reserveShipMethod;
    // 점포유형(HYPER, CLUB, AURORA, EXP, DS)
    public String storeType;
    // 점포종류(NOR:일반점, DLV:택배점)
    public String storeKind;
    // 몰유형(DS:업체상품,TD:매장상품)
    public String mallType;
    // 성인 유형 (NORMAL: 일반, ADULT: 성인, LOCAL_LIQUOR: 전통주)
    public String adultType;
    // 소스코드(일반상품:2000+상품번호, 저울상품:별도상품코드존재)
    public String sourceCd;
    // 원상품번호(대체주문시 결품처리된 원 상품번호)
    public String orgItemNo;
    // 상품번호
    public String itemNo;
    // 상품명1
    public String itemNm1;
    // 구매최소수량
    public String purchaseMinQty;
    // 심플상품가격(심플 적용전 상품가격), 심플프로모션 미적용시에는 item_price와 동일함.
    public String simpOrgItemPrice;
    // 매입가격(매입가)
    public String purchasePrice;
    // 원금액(할인 미적용금액)
    public String orgPrice;
    // 단위리테일(TD Only)
    public String unitRetail;
    // 상품금액(상품 판매가)
    public String itemPrice;
    // 상품수량 (SUM(상품옵션수량))
    public String itemQty;
    // 주문금액 ((상품가격+옵션가격N)*옵션수량N) + (추가상품가격*추가상품수량)
    public String orderPrice;
    // 할인금액(주문상품번호별 할인받은 전체금액, 상품기준 할인금액)
    public String discountAmt;
    // 상품할인금액(상품할인 적용금액)
    public String itemDiscountAmt;
    // 상품쿠폰금액(상품쿠폰 적용금액)
    public String itemCouponAmt;
    // 프로모션번호행사(심플,믹스,Threshold)
    public String promoNo;
    // 프로모션상세번호행사(심플,믹스,Threshold) 상세번호
    public String promoDetailNo;
    // 프로모션적용여부(Y:행사적용, N:행사 미적용)
    public String promoApplyYn;
    // 프로모션할인금액(믹스앤매치, Threshold 할인금액),(SIMPLE 행사 할인금액은 미포함됨 판가에 바로 적용됨)
    public String promoDiscountAmt;
    // 믹스매치유형(Pine용, 믹스앤매치 유형 1:BUY,2:GET,3:ALL)
    public String mixMatchType;
    // 임직원할인율
    public String empDiscountRate;
    // 임직원할인금액
    public String empDiscountAmt;
    // 장바구니쿠폰할인금액
    public String cartItemDiscountAmt;
    // 카드상품할인금액
    public String cardItemDiscountAmt;
    // 판매측정단위(판매상품에 측정 단위 EA,HG등)
    public String saleUom;
    // 공급자(TD상품 납품업체)
    public String supplier;
    // 파트너ID
    public String partnerId;
    // 세금여부(Y:과세,N:면세,Z:영세)
    public String taxYn;
    // 세금부가세금액(주문금액-상품별할인금액 기준)
    public String taxVatAmt;
    // 업체상품코드
    public String sellerItemCd;
    // MD임직원번호(미사용-향후 삭제 예정)
    public String mdEmpNo;
    // 수수료유형(R:정액,A:정률,N:없음(TD상품))
    public String commissionType;
    // 수수료율
    public String commissionRate;
    // 수수료가격
    public String commissionPrice;
    // 대체여부(상품별 대체가능여부 Y:대체불가능,N:대체가능)
    public String substitutionYn;
    // 옵션선택사용여부
    public String optSelUseYn;
    // 옵션텍스트사용여부
    public String optTxtUseYn;
    // (대대카테고리가 없으면 -1로 들어와서 unsigned 해제합니다)
    public String gcateCd;
    // 대카테고리코드
    public String lcateCd;
    // 중카테고리코드
    public String mcateCd;
    // 소카테고리코드
    public String scateCd;
    // 세카테고리코드
    public String dcateCd;
    // 레거시카테고리 DIVISION
    public String division;
    // 레거시카테고리 GroupNo
    public String groupNo;
    // 레거시카테고리 DeptNo
    public String deptNo;
    // 레거시카테고리 Class_no
    public String classNo;
    // 레거시카테고리 subClassNo
    public String subClassNo;
    // ISBN코드(ISBN 10자리 또는 13자리코드)
    public String isbnCd;
    // 리뷰여부(리뷰작성여부 Y:작성,N:미작성)
    public String reviewYn;
    //
    public String hplusAnalytics;
    // 재고구입번호
    public String stockBuyNo;
    // 등록일시
    public String regDt;
    // 등록자ID
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정자ID
    public String chgId;
    // 판매단위
    public String saleUnit;
    // 믹스매치기준값(믹스앤매치 픽 기준수량)
    public String mixMatchStdVal;
    // 믹스매치픽여부(믹스앤매치 픽 여부 Y/N)
    public String mixMatchPickYn;
    // 믹스매치상품수량(믹스앤매치 적용시 상품수량) (item_qty와 동일)
    public String mixMatchItemQty;
    // 원상품명1(대체주문시 결품처리된 원상품명1)
    public String orgItemNm1;
    // 원상품수량(대체주문시 결품처리된 원상품수량)
    public String orgItemQty;
    // 프로모션(행사) 비례배분 대상 수량
    public String promoDivideItemQty;
    // 추가중복쿠폰TD할인금액
    public String addTdItemDiscountAmt;
    // 추가중복쿠폰DS할인금액
    public String addDsItemDiscountAmt;
}
