package kr.co.homeplus.claim.claim.model.external;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "(외부용) 점포 픽업장소정보 & slot 리스트")
@Setter
@Getter
public class FrontStoreSlot {

    @ApiModelProperty(value = "점포 slot shipDt 정보(헤더노출용)", position = 1)
    private Map<String, String> frontSlotHeader;

    @ApiModelProperty(value = "점포 slot 정보", position = 2)
    private Map<String, List<StoreSlotMng>> frontSlotMap;

    @ApiModelProperty(value = "픽업장소정보 리스트", position = 3)
    private List<StorePickupPlace> storePickupPlaceList;

}