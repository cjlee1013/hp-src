package kr.co.homeplus.claim.order.sql;

import kr.co.homeplus.claim.constants.CustomConstants;
import kr.co.homeplus.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claim.entity.claim.ClaimMstEntity;
import kr.co.homeplus.claim.entity.claim.ClaimOosItemEntity;
import kr.co.homeplus.claim.entity.claim.ClaimOosManageEntity;
import kr.co.homeplus.claim.entity.dms.ItmPartnerSellerEntity;
import kr.co.homeplus.claim.entity.order.BundleEntity;
import kr.co.homeplus.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claim.entity.order.PaymentEntity;
import kr.co.homeplus.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claim.entity.ship.ShippingAddrEntity;
import kr.co.homeplus.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claim.enums.MysqlFunction;
import kr.co.homeplus.claim.factory.EntityFactory;
import kr.co.homeplus.claim.order.model.inquiry.OrderInquirySetDto;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class OrderDataSql {

    public static String selectPurchaseOrderInfo( @Param("purchaseOrderNo") long purchaseOrderNo ) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class);
        return new SQL()
            .SELECT(
                purchaseOrder.paymentNo,
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.siteType,
                purchaseOrder.userNo
            )
            .FROM(EntityFactory.getTableName(PurchaseOrderEntity.class))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            ).toString();
    }

    public static String selectOrderItemInfo( @Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderItemNo") long orderItemNo ) {
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class);
        return new SQL()
            .SELECT(
                orderItem.purchaseOrderNo,
                orderItem.bundleNo,
                orderItem.orderItemNo,
                orderItem.itemNm1,
                orderItem.itemPrice,
                orderItem.storeType,
                orderItem.mallType,
                orderItem.taxYn,
                orderItem.partnerId
            )
            .FROM(EntityFactory.getTableName(OrderItemEntity.class))
            .WHERE(
                SqlUtils.equalColumnByInput(orderItem.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.orderItemNo, orderItemNo)
            ).toString();
    }

    /**
     * 클레임에 필요한 데이터 생성
     *  -> 클레임 아이템 데이터 생성을 위한 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param orderItemNo 상품주문번호
     * @return 클레임 아이템 데이터 생성을 위한 데이터 쿼리
     */
    public static String selectClaimItemInfo( @Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderItemNo") long orderItemNo ) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                orderItem.orderItemNo,
                orderItem.itemNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemNm1, "item_name"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemQty, orderItem.itemQty),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemPrice, orderItem.itemPrice),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.storeType, orderItem.storeType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.mallType, orderItem.mallType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.originStoreId, orderItem.originStoreId),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.storeId, orderItem.storeId),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.taxYn, orderItem.taxYn),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.empDiscountRate, orderItem.empDiscountRate),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.empDiscountAmt, orderItem.empDiscountAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderType, purchaseOrder.orderType),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimItem.claimItemQty, claimItem.claimItemQty),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.marketOrderNo, purchaseOrder.marketOrderNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo, orderItem.purchaseOrderNo, orderItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.orderItemNo, orderItemNo)
            )
            .GROUP_BY(orderItem.orderItemNo, orderItem.itemNo);
        log.debug("selectClaimItemInfo ::: \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 클레임에 필요한 데이터 생성
     *  -> 클레임 번들 생성을 위한 데이터.
     *
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return 클레임 번들 생성을 위한 데이터 쿼리
     */
    public static String selectCreateClaimDataInfo( @Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo, @Param("userNo") long userNo  ) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.paymentNo,
                purchaseOrder.siteType,
                purchaseOrder.userNo,
                bundle.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderType, purchaseOrder.orderType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shipStatus, shippingItem.shipStatus),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.partnerId, shippingItem.partnerId),
                SqlUtils.sqlFunction(MysqlFunction.FN_PAYMENT_TYPE, purchaseOrder.paymentNo, "payment_type"),
                SqlUtils.sqlFunction(MysqlFunction.FN_PAYMENT_TYPE, purchaseOrder.paymentNo, "refund_method"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.invoiceNo, shippingItem.invoiceNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.empNo, purchaseOrder.empNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.reserveYn, purchaseOrder.reserveYn),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo,  bundle.bundleNo, "'C'"), "order_category"),
                SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo,  bundle.bundleNo, "'M'")), "TD"), "close_send_yn"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.shipMobileNo, shippingAddr.shipMobileNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.safetyUseYn, shippingAddr.safetyUseYn),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.marketOrderNo, purchaseOrder.marketOrderNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(bundle.bundleNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                SqlUtils.equalColumnByInput(bundle.bundleNo, bundleNo)
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo, purchaseOrder.paymentNo, purchaseOrder.siteType, purchaseOrder.userNo, bundle.bundleNo);
        log.debug("selectCreateClaimDataInfo ::: {}", query.toString());
        return query.toString();
    }

    public static String selectCreateClaimMstDataInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo ) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.paymentNo,
                purchaseOrder.siteType,
                purchaseOrder.userNo,
                purchaseOrder.empNo,
                purchaseOrder.gradeSeq,
                purchaseOrder.marketType,
                purchaseOrder.marketOrderNo
            )
            .FROM(EntityFactory.getTableName(purchaseOrder))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo)
            );
        log.debug("selectClaimMstDtaInfo ::: {}", query.toString());
        return query.toString();
    }

    public static String selectInquiryOrderInfo(OrderInquirySetDto setDto) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ItmPartnerSellerEntity itmPartnerSeller = EntityFactory.createEntityIntoValue(ItmPartnerSellerEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderDt, purchaseOrder.orderDt),
                bundle.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.storeType, orderItem.storeType),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.mallType), "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.storeId),
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray("NOW()", "%s%i")), itmPartnerSeller.sellerSeq, SqlUtils.sqlFunction(MysqlFunction.RIGHT, ObjectUtils.toArray("RAND()", 2)))))
                                .FROM(EntityFactory.getTableNameWithAlias("dms", itmPartnerSeller))
                                .WHERE(
                                    SqlUtils.equalColumn(itmPartnerSeller.partnerId, SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.partnerId))
                                )
                        )
                    ),
                    orderItem.partnerId
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.purchaseOrderNo), bundle.bundleNo, "'M'"), "ship_type")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(orderItem.bundleNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(orderItem.orderItemNo, claimBundle.claimNo)))
            .WHERE(
                SqlUtils.betweenDay(purchaseOrder.orderDt, setDto.getSchStartDt(), setDto.getSchEndDt()),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, setDto.getUserNo()),
                SqlUtils.equalColumnByInput(purchaseOrder.siteType, setDto.getSiteType()),
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, purchaseOrder.orderType, "'ORD_SUB'")
            )
            .GROUP_BY(bundle.bundleNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.nonAlias(purchaseOrder.orderDt), CustomConstants.YYYYMMDD_COMMA), purchaseOrder.orderDt),
                SqlUtils.nonAlias(bundle.bundleNo),
                SqlUtils.nonAlias(orderItem.storeType),
                SqlUtils.nonAlias(orderItem.partnerId),
                "purchase_order_no as org_purchase_order_no"
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "orderData"))
            .ORDER_BY(
                SqlUtils.orderByesc(
                    SqlUtils.nonAlias(purchaseOrder.orderDt),
                    SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo),
                    "ship_type",
                    SqlUtils.nonAlias(bundle.bundleNo)
                )
            )
            .LIMIT(setDto.getPerPage())
            .OFFSET((setDto.getPage() - 1) * setDto.getPerPage());;
        log.debug("selectInquiryOrderInfo ::: {}", query.toString());
        return query.toString();
    }

    public static String selectInquiryOrderItemList(@Param("bundleNo") long bundleNo) {
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimOosManageEntity claimOosManage = EntityFactory.createEntityIntoValue(ClaimOosManageEntity.class, Boolean.TRUE);
        ClaimOosItemEntity claimOosItem = EntityFactory.createEntityIntoValue(ClaimOosItemEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                orderItem.purchaseOrderNo,
                orderItem.orderItemNo,
                orderItem.itemNo,
                orderItem.itemNm1,
                purchaseOrder.orderDt,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimMst.oosYn, "Y"),
                        claimOosItem.subQty,
                        0
                    ),
                    "claim_qty"
                ),
                orderItem.itemQty
            )
            .FROM(EntityFactory.getTableNameWithAlias(bundle))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(bundle.bundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(orderItem.purchaseOrderNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimBundle.claimNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(orderItem.orderItemNo, claimMst.claimNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimOosManage, ObjectUtils.toArray(claimBundle.claimNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimOosItem, ObjectUtils.toArray(claimOosManage.oosManageSeq)))
            .WHERE(
                SqlUtils.OR(
                    SqlUtils.equalColumnByInput(bundle.bundleNo, bundleNo),
                    SqlUtils.AND(
                        SqlUtils.equalColumnByInput(bundle.orgBundleNo, bundleNo),
                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB")
                    )
                )
            );
        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(orderItem.purchaseOrderNo),
                SqlUtils.nonAlias(orderItem.orderItemNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(orderItem.itemNo), orderItem.itemNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(orderItem.itemNm1), orderItem.itemNm1),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(purchaseOrder.orderDt)), CustomConstants.YYYYMMDD_COMMA), purchaseOrder.orderDt),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(orderItem.itemQty)),
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, "claim_qty")
                    ),
                    "qty"
                )
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "ZZ"))
            .GROUP_BY(SqlUtils.nonAlias(orderItem.purchaseOrderNo), SqlUtils.nonAlias(orderItem.orderItemNo));

        log.debug("selectInquiryOrderItemList ::: [{}]", query.toString());
        return query.toString();
    }

}
