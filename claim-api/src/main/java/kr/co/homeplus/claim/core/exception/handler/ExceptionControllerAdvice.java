package kr.co.homeplus.claim.core.exception.handler;

import java.nio.charset.StandardCharsets;
import java.sql.SQLSyntaxErrorException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collections;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.enums.ExceptionCode;
import kr.co.homeplus.claim.utils.LogUtils;
import kr.co.homeplus.plus.api.support.client.exception.ResourceClientException;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.servlet.NoHandlerFoundException;

@Slf4j
@ControllerAdvice
public class ExceptionControllerAdvice {

    private HttpHeaders getDefaultHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        MediaType mediaType = new MediaType(MediaType.APPLICATION_JSON_UTF8,
            StandardCharsets.UTF_8);
        httpHeaders.setContentType(mediaType);
        return httpHeaders;
    }

    /**
     * {@link Throwable}로 발생되는 예외를 처리하는 메소드로 명시적이지 않는 모든 예외를 처리한다.
     * <p>응답 내용은 에러내용을 담은 {@link ResponseObject} 와 {@link HttpStatus} 를 전달한다.
     *
     * @param req HttpServletRequest
     * @param ex  Exception
     * @return json 타입으로 return
     * @see ResponseObject
     */
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ResponseObject<String>> handleServerError(final HttpServletRequest req, final Exception ex) {
        // 응답할 ResponseObject 설정
        ResponseObject<String> responseObject = setResponseObjectForException(ex, HttpStatus.INTERNAL_SERVER_ERROR);
        log.error("Throwable Error!!! uri: {}, trace: {}", req.getRequestURI(), ExceptionUtils.getStackTrace(ex));

        return new ResponseEntity<>(responseObject, getDefaultHttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Resttemplate 사용 시 예외를 처리하는 핸들러<p> <p/> 전달되는 {@link Exception}의 type에 따라 리턴되는 값이 변경된다. 세부내용은
     * 하단의 내용을 참조한다. <p/>
     * <pre>
     * 1. {@link RestClientResponseException}이 발생 할 경우
     *  - 리턴 : status, ResponseBody 그대로 전달
     *
     * 2. 기타 그외의 exception
     *  - 리턴 : status만 변경(500), {@link ResponseObject}를 전달
     *   * unknownHostException, IOException 등의 기타 Exception 처리
     *   * 에러 내용을 {@link ResponseObject}로 담아 전달
     * </pre>
     *
     * @param req HttpServletRequest
     * @param res HttpServletResponse
     * @param ex  ResourceClientException
     * @return responseEntity
     * @see RestClientResponseException
     * @see kr.co.homeplus.plus.web.client.ResourceClient;
     * @see ResponseObject
     */
    @ExceptionHandler(ResourceClientException.class)
    public HttpEntity<?> handleResourceClientException(final HttpServletRequest req,
        final HttpServletResponse res,
        final ResourceClientException ex) {

        Throwable cause = ex.getCause();
        /*
         * 1. RestClientResponseException이 발생 할 경우
         * 리턴 : status, ResponseBody 그대로 전달
         */
        if (cause instanceof RestClientResponseException) {
            res.setStatus(ex.getHttpStatus());

            log.error(
                "ResourceClientException Error!!! uri:{}, resourceUrl:{}, httpStatus:{}, getResponseBody:{}",
                req.getRequestURI(),
                ex.getResourceUrl(), ex.getHttpStatus(),
                ((RestClientResponseException) cause).getResponseBodyAsString());
            /*
             * 2. 기타 그외의 exception
             * 리턴 : status 지정(500), 에러 내용은 ResponseObject로 담아 전달
             */
        } else {
            res.setStatus(500);
            ResponseObject<String> responseObject = setResponseObjectForException(ex,
                HttpStatus.INTERNAL_SERVER_ERROR);

            log.error(
                "ResourceClientException Error!!! uri:{}, resourceUrl:{}, httpStatus:{}, getResponseObject:{}, trace:{}",
                req.getRequestURI(),
                ex.getResourceUrl(), ex.getHttpStatus(), responseObject,
                ExceptionUtils.getStackTrace(ex));

            return new HttpEntity<>(responseObject, getDefaultHttpHeaders());
        }

        // 추후 해당부분 세부분석 필요
        return new HttpEntity<>(ex.getResponseBody(), getDefaultHttpHeaders());
    }


    /**
     * 404 error not found 예외 처리<p>
     *
     * @param req HttpServletRequest
     * @param ex  예외
     * @return json 타입으로 return
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ResponseObject<String>> NoHandlerFoundException(
        final HttpServletRequest req, final Exception ex) {
        log.warn("404 Not Found Error!!! <uri:{}>, <RequestMethod:{}>, <HttpStatus:{}>",
            req.getRequestURI(), req.getMethod(),
            HttpStatus.NOT_FOUND.value());
        ResponseObject<String> responseObject = setResponseObjectForException(ex,
            HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(responseObject, getDefaultHttpHeaders(), HttpStatus.NOT_FOUND);
    }

    /**
     * 에러 발생 시 리턴 할 {@link ResponseObject}를 spec에 맞춰 생성해주는 메소드
     *
     * @param ex         Exception
     * @param httpStatus ResponseObject 내에 입력할 HttpStatus
     * @return ResponseObject
     */
    private ResponseObject<String> setResponseObjectForException(final Exception ex,
        final HttpStatus httpStatus) {
        Exception processedException = new Exception(ex.getMessage());
        processedException.setStackTrace(Arrays.copyOf(ex.getStackTrace(), 3));

        return ResponseObject.Builder.<String>builder().status(httpStatus)
            .data(ExceptionUtils.getStackTrace(processedException)).returnCode("9001")
            .returnMessage(ExceptionUtils.getMessage(processedException)).build();
    }

    @ExceptionHandler(LogicException.class)
    public ResponseEntity<ResponseObject<?>> handleClaimLogicException(LogicException cle) {
        LogUtils.error("LogicException [\nExceptionInfo :: {}\nReturnCode :: {}\nReturnMessage :: {}]", convertExceptionMessage(cle.getStackTrace()[0]), cle.getResponseCode(), cle.getResponseMsg());
        return new ResponseEntity<>(ResponseObject.Builder.builder()
            .status(HttpStatus.OK)
            .returnCode(cle.getResponseCode())
            .returnMessage(cle.getResponseMsg())
            .data(cle.getData())
            .build(), getDefaultHttpHeaders(), HttpStatus.OK);
    }

    @ExceptionHandler(SQLSyntaxErrorException.class)
    public ResponseEntity<ResponseObject<?>> handleSQLSyntaxErrorException(SQLSyntaxErrorException e) {
        log.error("SQLSyntax ERROR[\nsourceInfo : {} \nErrorMessage : {}\n]", this.convertExceptionMessage(e.getStackTrace()[0]), e.getMessage());
        return new ResponseEntity<>(ResponseObject.Builder.builder()
            .status(HttpStatus.OK)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9004.getResponseCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9004.getResponseMessage())
            .data(null)
            .build(), getDefaultHttpHeaders(), HttpStatus.OK);
    }

    @ExceptionHandler(BadSqlGrammarException.class)
    public ResponseEntity<ResponseObject<String>> handleMethodBadSqlGrammarException(BadSqlGrammarException e, HttpServletRequest request) {
        log.error("SQL BadSqlGrammar ERROR[\nsourceInfo : {} \nErrorMessage : {}\n]", this.convertExceptionMessage(e.getStackTrace()[0]), e.getMessage());
        return new ResponseEntity<>(ResponseObject.Builder.<String>builder()
            .status(HttpStatus.OK)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9004.getResponseCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9004.getResponseMessage())
            .data(null)
            .build(), getDefaultHttpHeaders(), HttpStatus.OK);
    }

    private String convertExceptionMessage(StackTraceElement ste){
        return MessageFormat.format("{0}::{1}:{2}", ste.getFileName(), ste.getMethodName(), ste.getLineNumber());
    }
}
