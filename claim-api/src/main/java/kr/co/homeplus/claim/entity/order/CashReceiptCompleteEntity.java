package kr.co.homeplus.claim.entity.order;

public class CashReceiptCompleteEntity {
    // 현금영수증발급완료순번
    public String cashReceiptCompleteSeq;
    // 현금영수증순번(cash_receipt_v1.seq)
    public String cashReceiptSeq;
    // 구분 (1:승인, 2:취소)
    public String gubun;
    // 현금영수증발급PGMID
    public String pgMid;
    // PG결과코드
    public String pgResultCd;
    // PG결과메시지
    public String pgResultMsg;
    // 현금영수증TID
    public String cashReceiptTid;
    // 현금영수증발급일시
    public String cashReceiptIssueDt;
    // 현금영수증발급결과코드
    public String cashReceiptIssueResultCd;
    // 현금영수증승인번호
    public String cashReceiptApprovalNo;
    // 현금영수증금액
    public String cashReceiptAmt;
    // 현금영수증공급금액
    public String cashReceiptSuplyAmt;
    // 현금영수증부가세금액
    public String cashReceiptVatAmt;
    // 현금영수증용도(0:소득공제용, 1:지출증빙)
    public String cashReceiptUsage;
    // 현금영수증취소일시
    public String cashReceiptCancelDt;
    // 현금영수증원ID
    public String cashReceiptOrgTid;
    // 현금영수증부분취소TID
    public String cashReceiptPartCancelTid;
    // 현금영수증취소금액
    public String cashReceiptCancelAmt;
    // 현금영수증남은금액
    public String cashReceiptRemainAmt;
    // 등록일시
    public String regDt;
    // 등록자ID
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정자ID
    public String chgId;
}
