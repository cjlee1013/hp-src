package kr.co.homeplus.claim.receipt.service;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseMethodReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.card.ClaimCardReceiptGetDto;
import kr.co.homeplus.claim.receipt.model.cash.ClaimCashReceiptGetDto;
import kr.co.homeplus.claim.receipt.model.order.ClaimRefundReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.purchase.ClaimPaymentReceiptInfo;
import kr.co.homeplus.claim.receipt.model.purchase.ClaimReceiptItemInfo;
import kr.co.homeplus.claim.receipt.model.purchase.ClaimReceiptMainInfo;

public interface ReceiptMapperService {

    ClaimReceiptMainInfo getClaimPurchaseReceiptMainInfo(ClaimPurchaseReceiptSetDto setDto);

    List<ClaimReceiptItemInfo> getClaimPurchaseReceiptItemInfo(ClaimPurchaseReceiptSetDto setDto);

    List<ClaimPaymentReceiptInfo> getClaimPurchaseReceiptPaymentInfo(ClaimPurchaseReceiptSetDto setDto);

    ClaimCardReceiptGetDto getClaimCardPurchaseReceiptInfo(ClaimPurchaseMethodReceiptSetDto setDto);

    ClaimCashReceiptGetDto getClaimCashPurchaseReceiptInfo(ClaimPurchaseMethodReceiptSetDto setDto);

    List<LinkedHashMap<String, Object>> getOrderRefundReceiptInfo(ClaimRefundReceiptSetDto setDto);
}
