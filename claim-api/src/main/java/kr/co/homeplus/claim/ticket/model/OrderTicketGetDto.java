package kr.co.homeplus.claim.ticket.model;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderTicketGetDto {

    @ApiModelProperty(value = "주문티켓번호(발급거래번호)", position = 1)
    public String orderTicketNo;

    @ApiModelProperty(value = "구매주문번호", position = 2)
    public String purchaseOrderNo;

    @ApiModelProperty(value = "번들번호", position = 3)
    public String bundleNo;

    @ApiModelProperty(value = "주문상품번호", position = 4)
    public String orderItemNo;

    @ApiModelProperty(value = "업체상품코드", position = 5)
    public String sellerItemCd;

    @ApiModelProperty(value = "티켓코드(발급시스템을 통해 부여)", position = 6)
    public String ticketCd;

    @ApiModelProperty(value = "티켓상태미발급(T0) / 사용가능(T1) / 사용완료(T2) / 취소요청(T3) / 취소완료(T4)", position = 7)
    public String ticketStatus;

    @ApiModelProperty(value = "티켓유효시작일시", position = 8)
    public String ticketValidStartDt;

    @ApiModelProperty(value = "티켓유효종료일시", position = 9)
    public String ticketValidEndDt;

    @ApiModelProperty(value = "티켓사용일시", position = 10)
    public String ticketUseDt;

    @ApiModelProperty(value = "티켓전송횟수(SMS 재전송 횟수)", position = 11)
    public String ticketSendCnt;

    @ApiModelProperty(value = "미사용환불율(미사용 환불 적용율 : 0~100 / 1부터 미사용환불대상)", position = 12)
    public String noUseRefundRate;

    @ApiModelProperty(value = "발급유형(DIRECT:즉시발급, RESERVE:예약발급)", position = 13)
    public String issueType;

    @ApiModelProperty(value = "발급상태 발급대기(I1) / 발급중(I2) / 발급완료(I3) / 발급취소(I4) / 발급실패(I8) / 취소실패(I9)", position = 14)
    public String issueStatus;

    @ApiModelProperty(value = "발급리턴코드", position = 15)
    public String issueReturnCd;

    @ApiModelProperty(value = "발급리턴메시지", position = 16)
    public String issueReturnMsg;

    @ApiModelProperty(value = "발급요청횟수", position = 17)
    public String issueReqCnt;

    @ApiModelProperty(value = "발급채널(COOP:쿠프마케팅, ETC:미발급업체)", position = 18)
    public String issueChannel;

    @ApiModelProperty(value = "발급예약일시(발급유형이 예약발급인 경우 사용)", position = 19)
    public String issueReserveDt;

    @ApiModelProperty(value = "발급일자", position = 20)
    public String issueDt;

    @ApiModelProperty(value = "상품유형(사용미정), (E:E티켓,그외 정의해서 추가 사용 예정)", position = 21)
    public String itemType;

    @ApiModelProperty(value = "연동유형(사용미정), (INTERNAL:내부티켓, PARTNER:업체티켓, EXTERNAL:외부연동티켓, DIRECT:직접연동,MANUAL:수동발급)", position = 22)
    public String linkType;

    @ApiModelProperty(value = "등록일시", position = 23)
    public String regDt;

    @ApiModelProperty(value = "등록ID", position = 24)
    public String regId;

    @ApiModelProperty(value = "수정일시", position = 25)
    public String chgDt;

    @ApiModelProperty(value = "수정ID", position = 26)
    public String chgId;

}
