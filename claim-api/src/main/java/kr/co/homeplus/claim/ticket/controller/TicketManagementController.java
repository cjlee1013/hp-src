package kr.co.homeplus.claim.ticket.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.ticket.model.CoopTicketUpdateSetDto;
import kr.co.homeplus.claim.ticket.service.TicketManagementService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ticket")
@RequiredArgsConstructor
@Slf4j
@Api(tags = "티켓 관리")
public class TicketManagementController {

    private final TicketManagementService ticketManagementService;

    @PostMapping("/coop/setTicketStatus")
    @ApiOperation(value = "쿠프 티켓 상태 변경")
    public ResponseObject<String> updateCoopTicket(@RequestBody CoopTicketUpdateSetDto coopTicketUpdateSetDto) throws LogicException {
      return ticketManagementService.updateCoopTicketStatus(coopTicketUpdateSetDto);
    }

}
