package kr.co.homeplus.claim.omni.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claim.claim.model.register.ClaimRegDetailDto;
import kr.co.homeplus.claim.core.db.annotation.SecondaryConnection;
import kr.co.homeplus.claim.omni.model.OmniOriginOrderDto;
import kr.co.homeplus.claim.omni.sql.OmniOrderSql;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

@SecondaryConnection
public interface OmniReadMapper {

    @SelectProvider(type = OmniOrderSql.class, method = "selectOriginOrderListByOmni")
    List<OmniOriginOrderDto> selectOriginOrderListByOmni(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OmniOrderSql.class, method = "selectOriginPurchaseOrderNoFromOrdNo")
    Long selectOriginPurchaseOrderNoFromOrdNo(@Param("ordNo") long ordNo);

    @SelectProvider(type = OmniOrderSql.class, method = "selectClaimShipInfoByOmni")
    ClaimRegDetailDto selectClaimShipInfoByOmni(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = OmniOrderSql.class, method = "selectClaimReasonType")
    LinkedHashMap<String, String> selectClaimReasonType(@Param("claimCd") String claimCd);

    @SelectProvider(type = OmniOrderSql.class, method = "selectExpTotalCancelBundleNo")
    List<LinkedHashMap<String, Long>> selectExpTotalCancelBundleNo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OmniOrderSql.class, method = "selectOrderItemInfoExp")
    List<ClaimPreRefundItemList>  selectOrderItemInfoExp(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = OmniOrderSql.class, method = "selectOriginClaimNoFromOrdNo")
    Long selectOriginClaimNoFromOrdNo(@Param("ordNo") long ordNo);

    @SelectProvider(type = OmniOrderSql.class, method = "selectClaimStatusInfo")
    LinkedHashMap<String, Object> selectClaimStatusInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = OmniOrderSql.class, method = "selectOmniShipPresentCheck")
    LinkedHashMap<String, Object> selectOmniShipPresentCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

}
