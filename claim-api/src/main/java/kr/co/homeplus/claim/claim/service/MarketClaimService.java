package kr.co.homeplus.claim.claim.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.enums.ClaimResponseCode;
import kr.co.homeplus.claim.enums.ExternalInfo;
import kr.co.homeplus.claim.enums.MarketClaimAction;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.StringUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MarketClaimService {

    private final ExternalService externalService;

    private final ClaimMapperService claimMapperService;

    public boolean isOrderMarketCheck(long claimNo) {
        return this.isOrderMarketCheck(0, claimNo);
    }

    public boolean isOrderMarketCheck(String purchaseOrderNo) {
        return this.isOrderMarketCheck(ObjectUtils.toLong(purchaseOrderNo), 0);
    }

    private boolean isOrderMarketCheck(long purchaseOrderNo, long claimNo) {
        return claimMapperService.isOrderMarketCheck(purchaseOrderNo, claimNo);
    }

    public void setMarketCancelReg(String purchaseOrderNo, List<String> orderOptNos, String claimReasonType) throws Exception {
        for(String orderOptNo : orderOptNos) {
            LinkedHashMap<String, Object> parameter = this.createMarketParameter(ObjectUtils.toLong(purchaseOrderNo), ObjectUtils.toLong(orderOptNo));
            parameter.put("claimReasonType", claimReasonType);
            if(!this.isActionCheck(ObjectUtils.toString(parameter.get("marketType")), claimReasonType, "C1")){
                throw new LogicException(ClaimResponseCode.CLAIM_MARKET_REG_ERR02);
            }
            log.info("marketCanceling parameter : {}", parameter);
            log.info("{}", externalService.postTransfer(getTransfer(ObjectUtils.toString(parameter.get("marketType")), claimReasonType, "REG"), parameter, String.class));
        }
    }

    public void setMarketStatusChange(long claimNo, String claimChangeStatus, String claimReasonDetail) throws Exception {
        List<LinkedHashMap<String, Object>> resultData = claimMapperService.getClaimStatusChangeDataForMarket(claimNo);
        if(resultData != null && resultData.size() > 0) {
            long purchaseOrderNo = resultData.stream().map(map -> map.get("purchase_order_no")).mapToLong(ObjectUtils::toLong).distinct().toArray()[0];
            String claimType = resultData.stream().map(map -> ObjectUtils.toString(map.get("claim_type"))).distinct().collect(Collectors.toList()).get(0);
            String claimStatus = resultData.stream().map(map -> ObjectUtils.toString(map.get("claim_status"))).distinct().collect(Collectors.toList()).get(0);
            String marketType = resultData.stream().map(map -> ObjectUtils.toString(map.get("market_type"))).distinct().collect(Collectors.toList()).get(0);
            List<Long> orderOptNos = resultData.stream().map(map -> map.get("order_opt_no")).map(ObjectUtils::toLong).collect(Collectors.toList());

            if(!this.isActionCheck(marketType, claimType, claimChangeStatus)){
                throw new LogicException(ClaimResponseCode.CLAIM_MARKET_REG_ERR02);
            }
            for(long orderOptNo : orderOptNos) {
                LinkedHashMap<String, Object> parameter = this.createMarketParameter(purchaseOrderNo, orderOptNo);
                parameter.put("claimReasonType", getClaimType(claimType));
                parameter.put("claimDetailReason", claimReasonDetail);
                // 보류 해제가 필요.
                if(claimStatus.equals("C8") && claimChangeStatus.equals("C2")) {
                    claimChangeStatus = "C4";
                }
                log.info("{}", externalService.postTransfer(getTransfer(ObjectUtils.toString(parameter.get("marketType")), "", this.getClaimStatusType(claimChangeStatus)), parameter, String.class));
            }
        }
    }

    private LinkedHashMap<String, Object> createMarketParameter(long purchaseOrderNo, long orderOptNo) throws Exception {
        // 주문번호와 상품주문번호로 마켓연동 상품주문번호를 취득한다.
        try {
            return ObjectUtils.getConvertMap(claimMapperService.getClaimBasicDataForMarketOrder(purchaseOrderNo, orderOptNo));
        } catch (Exception e) {
            log.error("마켓연동 주문 취소 파라미터 생성중 오류 :: {}", e.getMessage());
            throw new LogicException("9999", "시스템오류");
        }
    }

    private ExternalInfo getTransfer(String marketType, String claimReasonType, String type) {

        log.info("{}/{}/{}", marketType, claimReasonType, type);
        return Stream.of(ExternalInfo.values())
            .filter(e -> e.name().contains("OUTBOUND_MARKET"))
            .filter(e -> e.name().contains(type))
            .filter(e -> e.name().contains(marketType))
            .filter(e -> e.name().contains(this.getClaimType(claimReasonType)))
            .iterator()
            .next();
    }

    private String getClaimStatusType(String claimChangeStatus) {
        switch (claimChangeStatus) {
            case "C1" :
                return "REG";
            case "C2" :
                return "APPROVE";
            case "C3" :
                return "COMPLETE";
            case "C4" :
                return "WITHDRAW";
            case "C8" :
                return "HOLD";
            case "C9" :
                return "REJECT";
            default :
                return "";
        }
    }

    private String getClaimType(String claimReasonType) {
        try {
            if(StringUtil.isNotEmpty(claimReasonType)) {
                claimReasonType = "OOS1,OOS2".contains(claimReasonType) ? "C" : "OOS3".equals(claimReasonType) ? "R" : claimReasonType;
            }
            switch (claimReasonType.substring(0, 1)) {
                case "C":
                    return "CANCEL";
                case "R" :
                    return "RETURN";
                case "X" :
                    return "EXCHANGE";
                default:
                    return "CLAIM";
            }
        } catch (Exception e) {
            return "";
        }

    }

    private boolean isActionCheck(String marketType, String claimType, String claimChangeStatus) {
        // N마트
        // 취소 : 신청, 승인
        // 반품 : 신청, 승인, 거부, 보류, 보류해제
        // 교환 : 거부, 보류, 보류해제, 교환수거완료, 교환재발송
        // 11번가(eleven)
        // 취소 : 신청, 승인, 거부
        // 반품 : 신청, 승인 거부, 보류
        // 교환 : 거부, 보류, 보류해제, 교환수거완료, 교환재발송
        log.info("isActionCheck : {}", MarketClaimAction.isActionCheck(marketType, getClaimType(claimType), claimChangeStatus));
        return MarketClaimAction.isActionCheck(marketType, getClaimType(claimType), claimChangeStatus);
    }

}
