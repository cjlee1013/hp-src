package kr.co.homeplus.claim.order.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.core.db.annotation.SecondaryConnection;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListSetDto;
import kr.co.homeplus.claim.order.model.order.MpOrderBundleShipInfoDto;
import kr.co.homeplus.claim.order.model.order.OrderClaimInfoDto;
import kr.co.homeplus.claim.order.model.order.MpOrderItemDto;
import kr.co.homeplus.claim.order.model.order.MypageOrderBundleListDto;
import kr.co.homeplus.claim.order.model.order.MypageOrderListGetDto;
import kr.co.homeplus.claim.order.sql.MypageOrderShipSql;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

@SecondaryConnection
public interface OrderRefactoringMapper {

    @SelectProvider(type = MypageOrderShipSql.class, method = "selectMypageOrderList")
    List<MypageOrderListGetDto> selectMypageOrderList(MpOrderListSetDto setDto);

    @SelectProvider(type = MypageOrderShipSql.class, method = "selectMypageOrderBundleList")
    List<MypageOrderBundleListDto> selectMypageOrderBundleList(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderShipSql.class, method = "selectMypageOrderBundleShipInfo")
    MpOrderBundleShipInfoDto selectMypageOrderBundleShipInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = MypageOrderShipSql.class, method = "selectMypageOrderItemList")
    List<MpOrderItemDto> selectMypageOrderItemList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo, @Param("searchType") String searchType);

    @SelectProvider(type = MypageOrderShipSql.class, method = "selectMypageCombineOrderSubstitutionCheck")
    LinkedHashMap<String, Object> selectMypageCombineOrderSubstitutionCheck(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderShipSql.class, method = "selectMypageOrderClaimInfo")
    List<OrderClaimInfoDto> selectMypageOrderClaimInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = MypageOrderShipSql.class, method = "selectAutoCancelTicketInfo")
    List<LinkedHashMap<String, Object>> selectAutoCancelTicketInfo(@Param("bundleNo") long bundleNo);

    @SelectProvider(type = MypageOrderShipSql.class, method = "selectClaimRefundFailCheck")
    String selectClaimRefundFailCheck(@Param("claimNo") long claimNo);
}
