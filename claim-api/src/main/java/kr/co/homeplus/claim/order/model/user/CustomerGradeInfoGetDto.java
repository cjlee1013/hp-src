package kr.co.homeplus.claim.order.model.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "고객등급 조회 리스트")
public class CustomerGradeInfoGetDto {
    private GradeOrderInfoDto orderInfo;
    private List<CustomerGradeOrderListDto> orderList;

    @Data
    public static class CustomerGradeOrderListDto{
        @ApiModelProperty(value = "구분", position = 1)
        private String shipType;
        @ApiModelProperty(value = "점포구분", position = 2)
        private String storeType;
        @ApiModelProperty(value = "주문번호", position = 3)
        private String purchaseOrderNo;
        @ApiModelProperty(value = "상품명", position = 4)
        private String itemNm1;
        @ApiModelProperty(value = "적용일자", position = 5)
        private String orderDt;
        @ApiModelProperty(value = "구매건수", position = 6)
        private int orderCnt;
        @ApiModelProperty(value = "결제금액", position = 7)
        private int paymentAmt;
        @ApiModelProperty(value = "제외금액", position = 8)
        private int claimPrice;
        @ApiModelProperty(value = "구매금액", position = 9)
        private int orderPrice;
        @ApiModelProperty(value = "마일리지", position = 10)
        private int resultPoint;
        @ApiModelProperty(value = "적립여부", position = 11)
        private String resultCd;
        @ApiModelProperty(value = "적립/차감일자", position = 12)
        private String resultDt;
    }
}
