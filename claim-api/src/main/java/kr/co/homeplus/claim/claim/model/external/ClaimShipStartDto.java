package kr.co.homeplus.claim.claim.model.external;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@ApiModel(description = "옴니 교환/반품시 배송중 처리 DTO")
public class ClaimShipStartDto {
    @ApiModelProperty(value = "")
    private String purchaseStoreInfoNo;
}
