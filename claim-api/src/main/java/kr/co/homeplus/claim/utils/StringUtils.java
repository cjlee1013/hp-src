package kr.co.homeplus.claim.utils;

import java.util.regex.Pattern;
import kr.co.homeplus.claim.constants.Constants;
import kr.co.homeplus.claim.constants.PatternConstants;
import org.springframework.util.ObjectUtils;

public class StringUtils {
    /**
     * str 값이 NULL, str 이 공백으로만 이루어진 경우 ""로 치환해서 리턴
     * @param str 체크할 문자열
     * @return "" 또는 원문자열
     * */
    public static String NVL(String str) {
        if (str == null || str.isBlank()) {
            return "";
        } else {
            return str;
        }
    }

    /**
     * str 값이 NULL, str 이 공백으로만 이루어진 경우 rtnStr 로 치환해서 리턴
     * @param str 체크할 문자열
     * @param rtnStr null 일 경우 돌려줄 문자열
     * @return rtnStr 또는 원문자열
     */
    public static String NVL(String str, String rtnStr) {
        if (str == null || str.isBlank()) {
            return rtnStr;
        } else {
            return str;
        }
    }

    /**
     * Object 값이 NULL 일경우 T 에 해당 하는 형식으로 리턴
     * Long : 0
     * Integer : 0
     * String : ""
     * @return T
     */
    public static <T> T NVL(Object obj, Class<T> tClass) {

        if (obj == null) {
            if (tClass.getName().equals(Long.class.getName())) {
                return tClass.cast(0L);
            } else if (tClass.getName().equals(Integer.class.getName())) {
                return tClass.cast(0);
            } else if (tClass.getName().equals(String.class.getName())) {
                return tClass.cast("");
            }
        }
        return tClass.cast(obj);
    }

    /** Object 공백 check
     * @param object 체크할 객체
     * @return boolean
     * */
    public static boolean isEmpty(Object object){
        return ObjectUtils.isEmpty(object);
    }
    public static boolean isNotEmpty(Object object){
        return !ObjectUtils.isEmpty(object);
    }

    /** String 공백 check
     * @param str 체크할 문자열
     * @return boolean
     * */
    public static boolean isEmpty(String str) {
        return org.springframework.util.StringUtils.isEmpty(str);
    }
    public static boolean isNotEmpty(String str) {
        return !org.springframework.util.StringUtils.isEmpty(str);
    }

    /** long 공백 check
     * @param number 체크할 Long 객체
     * @return boolean
     * */
    public static boolean isEmpty(Long number){
        return number == null;
    }
    public static boolean isNotEmpty(Long number){
        return number != null;
    }

    /** string equals */
    public static boolean equals(String firstStr, String secondStr){
        if(isEmpty(firstStr) || isEmpty(secondStr)) return false;
        return firstStr.equals(secondStr);
    }

    /**
     * 휴대번호 포맷 변경
     * @param num 휴대번호
     * @param mask 구분자
     * @return String 포맷팅된 휴대번호
     */
    public static String phoneNumberHyphenAdd(String num, String mask) {

        String formatNum = "";
        if (StringUtils.NVL(num).equals("")) {
            return formatNum;
        }
        num = num.replaceAll("-","");

        if (num.length() == 11) {
            if (mask.equals("Y")) {
                formatNum = num.replaceAll("(\\d{3})(\\d{3,4})(\\d{4})", "$1-****-$3");
            } else {
                formatNum = num.replaceAll("(\\d{3})(\\d{3,4})(\\d{4})", "$1-$2-$3");
            }
        }else if(num.length()==12){
            if(mask.equals("Y")){
                formatNum = num.replaceAll("(\\d{4})(\\d{4})(\\d{4})", "$1-****-$3");
            }else{
                formatNum = num.replaceAll("(\\d{4})(\\d{4})(\\d{4})", "$1-$2-$3");
            }
        }else if(num.length()==8){
            formatNum = num.replaceAll("(\\d{4})(\\d{4})", "$1-$2");
        }else{
            if(num.indexOf("02")==0){
                if(mask.equals("Y")){
                    formatNum = num.replaceAll("(\\d{2})(\\d{3,4})(\\d{4})", "$1-****-$3");
                }else{
                    formatNum = num.replaceAll("(\\d{2})(\\d{3,4})(\\d{4})", "$1-$2-$3");
                }
            }else{
                if(mask.equals("Y")){
                    formatNum = num.replaceAll("(\\d{3})(\\d{3,4})(\\d{4})", "$1-****-$3");
                }else{
                    formatNum = num.replaceAll("(\\d{3})(\\d{3,4})(\\d{4})", "$1-$2-$3");
                }
            }
        }
        return formatNum;
    }

    /**
     * 전체 숫자 길이(zeroLength)에서 num길이를 제외한만큼 0을 채워준다
     * ex) num = 10, zeroLength = 5
     * output : 00010 리턴
     * @param num .
     * @param zeroLength .
     * @return .
     */
    public static String zeroFill(int num, int zeroLength) {
        String rtnStr = "";
        if (zeroLength == 0) {
            zeroLength = 1;
        }
        rtnStr = String.format("%0"+zeroLength+"d",num);
        return rtnStr;

    }

    /**
     * 개행문자(\r\n) 빈 값으로 치환
     * @param str .
     * @return String .
     */
    public static String spaceEscape(String str) {
        if (StringUtils.isEmpty(str)) {
            return "";
        }
        return str.replaceAll("(\r\n|\r|\n|\n\r)","");
    }

    /**
     * 입력가능 문자에 대한 최소 / 최대 입력길이 체크
     * 정상일경우 true 반환
     * @param str String
     * @param minLength int
     * @param maxLength int
     * @return boolean
     */
    public static boolean isLengthCheck(String str, int minLength, int maxLength) {
        if (StringUtils.isEmpty(str)) return false;
        boolean rtnBool = true;
        int strLength = str.length();
        if (strLength < minLength) {
            rtnBool = false;
        }
        if (strLength > maxLength) {
            rtnBool = false;
        }
        return rtnBool;
    }
    /**
     * 입력가능 문자에 대한 패턴 확인
     * 정상일경우 true 리턴
     * @param str String
     * @param types String
     * @return boolean
     * // TODO: 30/01/2020 SPC_1, SPC_1, SPC_3 Constants.java 사용여부 수정 필요.
     */
    public static boolean isPattern(String str, String... types) {
        if (StringUtils.isEmpty(str)) return false;
        boolean isBool = true;

        if (!org.apache.commons.lang3.StringUtils.isBlank(str)) {
            String regx = "^[";
            if (types != null && types.length > 0) {
                for (String p : types) {
                    switch (p) {
                        case PatternConstants.KOR:
                            regx += PatternConstants.KOR_FORMAT;
                            break;
                        case PatternConstants.ENG:
                            regx += PatternConstants.ENG_FORMAT;
                            break;
                        case PatternConstants.NUM:
                            regx += PatternConstants.NUM_FORMAT;
                            break;
                        case Constants.SPC_1:
                            regx += Constants.SPC_1_FORMAT;
                            break;
                        case Constants.SPC_2:
                            regx += Constants.SPC_2_FORMAT;
                            break;
                        case Constants.SPC_3:
                            regx += Constants.SPC_3_FORMAT;
                            break;
                        default:
                            break;
                    }
                }
                regx += "\\s]*$";
            }
            isBool = Pattern.matches(regx, str);
        }

        return isBool;
    }

    // 엑셀 개행문자(줄바꿈) split
    public static String[] getExcelEnterSplit(final String str) {
        return str.split(PatternConstants.ENTER_STR);
    }
}
