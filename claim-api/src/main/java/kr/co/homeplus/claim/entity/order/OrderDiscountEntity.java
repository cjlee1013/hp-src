package kr.co.homeplus.claim.entity.order;

public class OrderDiscountEntity {
    // 주문할인번호
    public String orderDiscountNo;
    // 구매주문번호
    public String purchaseOrderNo;
    // 번들번호
    public String bundleNo;
    // 주문상품번호
    public String orderItemNo;
    // 상품번호
    public String itemNo;
    // 고객번호
    public String userNo;
    // 상점ID
    public String storeId;
    // 할인종류(상품할인:item_discount, 상품쿠폰:item_coupon, 카드상품할인:card_discount, 배송비쿠폰:ship_coupon, 장바구니쿠폰:cart_coupon)
    public String discountKind;
    // 할인번호
    public String discountNo;
    // 쿠폰번호
    public String couponNo;
    // 쿠폰명
    public String couponNm;
    // 쿠폰유형
    public String couponType;
    // 발급번호(쿠폰발급번호)
    public String issueNo;
    // 바코드(레거시 쿠폰번호와 비슷한 값)
    public String barcode;
    // 상품수량(고객이 구매한 상품 수량)
    public String itemQty;
    // 쿠폰적용수량(쿠폰이 적용된 상품 구매 수량)
    public String couponApplyQty;
    // 할인유형(1:정률, 2:정액) - 카드상품할인은 미입력(order_discount_cart 테이블에 입력)
    public String discountType;
    // 할인율
    public String discountRate;
    // 할인금액
    public String discountAmt;
    // 할인최대(최대할인금액)
    public String discountMax;
    // 구매최소구매금액
    public String purchaseMin;
    //
    public String deptCd;
    // 카드그룹코드(Pine에 전송할 레거시 카드그룹코드)
    public String cardGroupCd;
    // 쿠폰유효시작일시
    public String couponValidStartDt;
    // 쿠폰유효종료일시
    public String couponValidEndDt;
    // 홈플러스부담율
    public String homeChargeRate;
    // 홈플러스부담가격
    public String homeChargePrice;
    // 판매자부담율
    public String sellerChargeRate;
    // 판매자부담가격
    public String sellerChargePrice;
    // 카드사부담율
    public String cardChargeRate;
    // 카드사부담가격
    public String cardChargePrice;
    // 등록일시
    public String regDt;
}
