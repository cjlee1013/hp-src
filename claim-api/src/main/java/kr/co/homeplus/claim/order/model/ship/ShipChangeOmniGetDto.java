package kr.co.homeplus.claim.order.model.ship;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주소지변경에 따른 옴니정보 전송결과")
public class ShipChangeOmniGetDto {
    @ApiModelProperty(value = "배송메시지")
    private String dlvMsg;
    @ApiModelProperty(value = "주문번호")
    private String ordNo;
    @ApiModelProperty(value = "주소")
    private String rcptAddr;
    @ApiModelProperty(value = "이름")
    private String rcptName;
    @ApiModelProperty(value = "전화번호")
    private String rcptPhone;
    @ApiModelProperty(value = "안심번호")
    private String rcptSafenNumber;
    @ApiModelProperty(value = "상점번호")
    private String storeId;
    @ApiModelProperty(value = "결과")
    private int updatedCnt;
}
