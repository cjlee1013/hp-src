package kr.co.homeplus.claim.entity.claim;

public class ClaimMultiItemEntity {
    // 다중배송 클레임 아이템 번호
    public String claimMultiItemNo;
    // 다중배송 클레임 번들 번호
    public String claimMultiBundleNo;
    // 클레임 아이템 번호
    public String claimItemNo;
    // 클레임 그룹 번호
    public String claimNo;
    // 클레임 번들 번호
    public String claimBundleNo;
    // 다중배송번호
    public String multiBundleNo;
    // 상품주문번호
    public String orderItemNo;
    // 다중배송 아이템 번호(주문)
    public String multiOrderItemNo;
    // 아이템금액
    public String itemPrice;
    // 클레임 수량(다중배송별)
    public String claimItemQty;
    // 등록일
    public String regDt;
}
