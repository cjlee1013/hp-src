package kr.co.homeplus.claim.office.service;

import java.util.LinkedHashMap;

public interface OfficeMapperService {

    LinkedHashMap<String, Object> getClaimData(long bundleNo);

}
