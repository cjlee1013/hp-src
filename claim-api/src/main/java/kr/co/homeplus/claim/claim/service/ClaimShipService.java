package kr.co.homeplus.claim.claim.service;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import kr.co.homeplus.claim.claim.model.register.ClaimProcessHistoryRegDto;
import kr.co.homeplus.claim.enums.DateType;
import kr.co.homeplus.claim.enums.ResponseCode;
import kr.co.homeplus.claim.factory.ResponseFactory;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipEditDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipInfoGetDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipInfoSetDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipSetGto;
import kr.co.homeplus.claim.utils.DateUtils;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimShipService {

    private final ClaimMapperService mapperService;
    // 클레임프로세스 서비스
    private final ClaimProcessService processService;
    /**
     * 클레임 - 미수취 처리
     *
     * @param noRcvShipEditDto 미수취 처리 데이터
     * @return 미수취 처리 결과
     */
    public ResponseObject<String> modifyNoRcvShipInfo(NoRcvShipEditDto noRcvShipEditDto) {
        noRcvShipEditDto.setChgId("MYPAGE");
        if(!mapperService.modifyNoRcvShipInfo(noRcvShipEditDto)){
            return ResponseFactory.getResponseObject(ResponseCode.CLAIM_NO_RCV_ERR01);
        }
        try {
            mapperService.setClaimProcessHistory(
                ClaimProcessHistoryRegDto.builder()
                    .purchaseOrderNo(noRcvShipEditDto.getPurchaseOrderNo())
                    .bundleNo(noRcvShipEditDto.getBundleNo())
                    .historyReason(noRcvShipEditDto.getNoRcvProcessType().equals("W") ? "미수취신고 철회" : "미수취신고 철회요청")
                    .regId(noRcvShipEditDto.getChgId())
                    .build()
            );
        } catch (Exception e) {
            log.error("미수취 {} 로그저장 중 오류 발생 :: {}", noRcvShipEditDto.getNoRcvProcessType().equals("W") ? "미수취신고 철회" : "미수취신고 철회요청", e.getMessage());
        }

        if(noRcvShipEditDto.getNoRcvProcessType().equals("W")){
            // 구매확정여부를 판단해야함.
            // D3이면 배송완료를, D4일 경우은 complete_dt 에 8일지났는지 여부를 판단.
            try {
                LinkedHashMap<String, Object> shipCompleteInfo = mapperService.getNonShipCompleteInfo(noRcvShipEditDto.getPurchaseOrderNo(), noRcvShipEditDto.getBundleNo());
                log.info("미수취 철회에 따른 배송상태 확인 :: {}", shipCompleteInfo);
                String shipStatus = ObjectUtils.toString(shipCompleteInfo.get("ship_status"));
                if("D3,D4".contains(shipStatus)){
                    if("D3".equals(shipStatus)){
                        processService.setClaimShipComplete(String.valueOf(noRcvShipEditDto.getBundleNo()), "CLAIM");
                    } else {
                        log.info("미수취 철회에 따른 배송상태 확인(확정) :: {}", DateUtils.isAfter(ObjectUtils.toString(shipCompleteInfo.get("complete_dt")), DateType.DAY, 8));
                        if(DateUtils.isAfter(ObjectUtils.toString(shipCompleteInfo.get("complete_dt")), DateType.DAY, 8)){
                            processService.setClaimShipDecision(String.valueOf(noRcvShipEditDto.getBundleNo()), "CLAIM");
                        }
                    }
                }
            } catch (Exception e) {
                log.error("미수취 철회에 따른 배송상태 업데이트 중 오류발생 ::: {}", e.getMessage());
            }
        }
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS);
    }

    /**
     * 클레임 - 미수취 등록
     *
     * @param noRcvShipSetGto 미수취 등록 데이터
     * @return 미수취 등록 결과
     */
    public ResponseObject<String> addNoRcvShipInfo(NoRcvShipSetGto noRcvShipSetGto) throws Exception {
        noRcvShipSetGto.setRegId("MYPAGE");
        try {
            if(!mapperService.addNoRcvShipInfo(noRcvShipSetGto)){
                return ResponseFactory.getResponseObject(ResponseCode.CLAIM_NO_RCV_ERR02);
            }
            mapperService.setClaimProcessHistory(
                ClaimProcessHistoryRegDto.builder()
                    .purchaseOrderNo(noRcvShipSetGto.getPurchaseOrderNo())
                    .bundleNo(noRcvShipSetGto.getBundleNo())
                    .historyReason("미수취신고")
                    .regId(noRcvShipSetGto.getRegId())
                    .build()
            );
        } catch (SQLException se) {
            log.debug("{} ::: {}", se.getErrorCode(), se.getMessage());
        }
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS);
    }

    /**
     * 클레임 - 미수취 조회
     *
     * @param infoSetDto 미수취 조회 데이터
     * @return 미수취 조회 결과
     */
    public ResponseObject<NoRcvShipInfoGetDto> getNoRcvShipInfo(NoRcvShipInfoSetDto infoSetDto) {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, mapperService.getNoRcvShipInfo(infoSetDto));
    }


}
