package kr.co.homeplus.claim.claim.service;

import java.util.LinkedHashMap;
import kr.co.homeplus.claim.claim.model.ClaimRequestSetDto;
import kr.co.homeplus.claim.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.enums.ClaimCode;
import kr.co.homeplus.claim.enums.ClaimResponseCode;
import kr.co.homeplus.claim.enums.ResponseCode;
import kr.co.homeplus.claim.factory.OrderResponseFactory;
import kr.co.homeplus.claim.factory.ResponseFactory;
import kr.co.homeplus.claim.utils.DateUtils;
import kr.co.homeplus.claim.utils.MessageSendUtil;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClaimOrderCancelService {

    private final ClaimMapperService claimMapperService;

    private final ClaimManagementService claimManagementService;

    private final ClaimProcessService claimProcessService;

    public ResponseObject<Long> orderCancelAction(ClaimRegSetDto claimRegSetDto) throws Exception {

        if(!this.getClaimPossible(claimRegSetDto)){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR09);
        }

        if("R,X".contains(claimRegSetDto.getClaimType())) {
            if(claimRegSetDto.getClaimDetail().getIsPick().equals("Y")){
                if(claimRegSetDto.getClaimDetail().getPickDlvCd().isEmpty() || claimRegSetDto.getClaimDetail().getPickInvoiceNo().isEmpty()){
                    throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR13);
                }
            }
        }
        // 주문건수, 클레임건수 데이터 가지고 옴
        LinkedHashMap<String, Object> getOrderClaimPartInfoMap = this.getOrderByClaimPartInfo(claimRegSetDto.getPurchaseOrderNo(), claimRegSetDto.getBundleNo(), claimRegSetDto.getUserNo());
        // 클레임신청된 건수 + 이미 클레임 신청된 건수 비교
        int totalClaimItemQty = ObjectUtils.toInt(getOrderClaimPartInfoMap.get("claimItemQty")) + claimRegSetDto.getClaimItemList().stream().mapToInt(itemList -> Integer.parseInt(itemList.getClaimQty())).sum();
        // 부분취소 로직 여부
        boolean isPartCancel = Boolean.TRUE;
        // 주문건수 = 클레임건수(기클레임건수 + 신청클레임건수)이면 전체취소
        // 단, 기 클레임건수가 있는 경우 마지막 건수만 부분취소가 아닌 전체 취소로 표시
        if(ObjectUtils.toInt(getOrderClaimPartInfoMap.get("orderItemQty")) == totalClaimItemQty){
            // 전체취소 건수 set
            claimRegSetDto.setClaimPartYn("N");
            // 기 등록된 건이 없으면 전체취소로직
            if(ObjectUtils.toInt(getOrderClaimPartInfoMap.get("claimItemQty")) == 0 && Long.parseLong(claimRegSetDto.getBundleNo()) == 0){
                isPartCancel = Boolean.FALSE;
            }
        } else if (ObjectUtils.toInt(getOrderClaimPartInfoMap.get("orderItemQty")) < totalClaimItemQty) {
            // 취소건(기취소 + 취소요청건) 이 주문 건수보다 클 경우 오류.
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR06);
        } else if(claimRegSetDto.getClaimItemList().stream().mapToInt(itemList -> Integer.parseInt(itemList.getClaimQty())).sum() == 0){
            // 요청 취소건이 0건이면 오류
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR07);
        }else {
            claimRegSetDto.setClaimPartYn("Y");
        }

        // 응답데이터
        LinkedHashMap<String, Object> claimDataMap = new LinkedHashMap<>();
        //클레임 등록된 claim_bundle_no response 시 전달 값
        long claimNo = 0;
        // 주문취소(전체주문취소, 주문취소)
        if(claimRegSetDto.getOrderCancelYn().equals("Y") && claimRegSetDto.getClaimType().equalsIgnoreCase("C")){
            // 전체주문취소
            if(isPartCancel){
                // 주문취소
                claimDataMap = claimManagementService.orderPartCancel(claimRegSetDto);
                //response object에 claimBundleNo 전달
                claimNo = ObjectUtils.toLong(claimDataMap.get("claim_no"));
            } else {
                claimDataMap = claimManagementService.orderTotalCancel(claimRegSetDto);
                claimNo = ObjectUtils.toLong(claimDataMap.get("claim_no"));
            }
        } else {
            // 취소/반품/교환 신청
            claimDataMap = claimManagementService.cancelRegister(claimRegSetDto);
            claimNo = ObjectUtils.toLong(claimDataMap.get("claim_no"));
            MessageSendUtil.sendClaimApplication(claimNo);
        }
        return ResponseFactory.getResponseObject(ResponseCode.CLAIM_REG_SUCCESS, claimNo);
    }

    public ResponseObject<Object> requestChangeByClaim(ClaimRequestSetDto reqDto) throws Exception {
        // 클레임 요청 타입에 따른 클레임 상태값
        String claimChangeStatus = getClaimStatus(reqDto.getClaimReqType());
        // 클레임 상태 변경값이 C2(클레임승인) 이면 결제 취소 로직을 수행한다.
        if(claimChangeStatus.equalsIgnoreCase(ClaimCode.CLAIM_STATUS_C2.getCode())){
            // 상태 변경 C1 -> C2
            if (!claimProcessService.setClaimStatus(reqDto.getClaimNo(), reqDto.getClaimBundleNo(), reqDto.getRegId(), ClaimCode.CLAIM_STATUS_C2)) {
                throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
            }
//            if(!claimManagementService.paymentCancel(reqDto.getClaimNo(), reqDto.getClaimBundleNo(), reqDto.getRegId())){
//                log.error("클레임 결제 취소 중 오류가 발생됨. {}", reqDto.getClaimNo());
//                throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR02);
//            }
//            if("C,R".contains(reqDto.getClaimType())){
//                MessageSendUtil.sendClaimCompleteWithoutExchange(reqDto.getClaimNo());
//            } else {
//                MessageSendUtil.sendExchangeClaimComplete(reqDto.getClaimNo());
//            }
        } else {
            // 클레임 상태 업데이트
            if(!claimProcessService.setClaimStatus(reqDto.getClaimNo(), reqDto.getClaimBundleNo(), reqDto.getRegId(), claimChangeStatus)){
                throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
            }
        }
        return OrderResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_REG_SUCCESS);
    }

    private String getClaimStatus(String claimReqType) {
        String claimChangeStatus = null;
        // 상태에 대한 확인
        switch (claimReqType){
            // 승인
            case "CA" :
            case "ca" :
                claimChangeStatus = ClaimCode.CLAIM_STATUS_C2.getCode();
                break;
            // 철회
            case "CW" :
            case "cw" :
                claimChangeStatus = ClaimCode.CLAIM_STATUS_C4.getCode();
                break;
            // 보류
            case "CH" :
            case "ch" :
                claimChangeStatus = ClaimCode.CLAIM_STATUS_C8.getCode();
                break;
            // 거부
            case "CD" :
            case "cd" :
                claimChangeStatus = ClaimCode.CLAIM_STATUS_C9.getCode();
                break;
            default:
                claimChangeStatus = "";
                break;
        }
        return claimChangeStatus;
    }

    // 클레임 가능 여부 체크
    private Boolean getClaimPossible(ClaimRegSetDto claimRegSetDto) throws Exception {
        // R | X 요청 일 경우에는 거래가 D3, D4, D5 만 가능
        // C 일 경우에는 D1, D2일 때만 가능.
        // 원주문의 배송상태를 조회
        LinkedHashMap<String, Object> claimPossibleDataMap = claimMapperService.getClaimPossibleCheck(Long.parseLong(claimRegSetDto.getPurchaseOrderNo()), Long.parseLong(claimRegSetDto.getBundleNo()));
        log.info("claim getClaimPossible ::: {}", claimPossibleDataMap);
        // 조회 건수가 없으면 오류.
        if(claimPossibleDataMap.size() == 0){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR08);
        }
        String shipStatus = ObjectUtils.toString(claimPossibleDataMap.getOrDefault("ship_status", "NN"));
        String orderType = ObjectUtils.toString(claimPossibleDataMap.get("order_type"));
        // 클레임 타입
        String claimType = claimRegSetDto.getClaimType();
        String shipMethod = ObjectUtils.toString(claimPossibleDataMap.get("ship_method"));

        if(claimPossibleDataMap.get("slot_close_time") != null && "TD_QUICK,TD_DRCT,TD_PICK".contains(shipMethod) && claimType.equals("C")){
            if(DateUtils.isAfter(ObjectUtils.toString(claimPossibleDataMap.get("slot_close_time")))){
                throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR17);
            }
        }
        // ORD_TICKET 일 경우 별도 체크!
        if("ORD_TICKET".equals(orderType)){
            if(!claimType.equals("C")){
                claimType = "C";
            }
        }
        String claimShipStatus = "";
        switch (claimType) {
            case "C" :
            case "c" :
                //클레임 타입이 C 가 아니면 튕김.
                claimShipStatus = orderType.contains("TICKET") ? "D1,D4,D5" : orderType.contains("MULTI") ? "D1" : claimRegSetDto.getOrderCancelYn().equals("Y") ? "D0,D1" : "D2";
                break;
            case "R" :
            case "r" :
            case "X" :
            case "x" :
                claimShipStatus = orderType.contains("MULTI") ? "D4,D5" : "D3,D4,D5";
                break;
        }

        return claimShipStatus.contains(shipStatus);
    }

    private LinkedHashMap<String, Object> getOrderByClaimPartInfo(String purchaseOrderNo, String bundleNo, long userNo) throws Exception {
        LinkedHashMap<String, Object> returnMap = claimMapperService.getOrderByClaimPartInfo(Long.parseLong(purchaseOrderNo), Long.parseLong(bundleNo), userNo);
        if(ObjectUtils.toInt(returnMap.get("orderItemQty")) == 0){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR05);
        }
        return returnMap;
    }

    private void isException(boolean isException) throws Exception {
        if(isException) {
            throw new LogicException("9999", "잠시후 이용부탁드립니다.");
        }
    }
}
