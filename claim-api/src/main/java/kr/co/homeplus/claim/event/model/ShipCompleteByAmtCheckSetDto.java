package kr.co.homeplus.claim.event.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "이벤트 배송완료(기준금액) 체크 요청 DTO")
public class ShipCompleteByAmtCheckSetDto {
    @ApiModelProperty(value = "조회시작일")
    private String validStartDt;
    @ApiModelProperty(value = "조회종료일")
    private String validEndDt;
    @ApiModelProperty(value = "기준금액")
    private int basicAmt;
    @ApiModelProperty(value = "고객번호")
    private long userNo;
    @ApiModelProperty(value = "점포구분(HOME|EXP)")
    private String storeKind;
}
