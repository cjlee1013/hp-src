package kr.co.homeplus.claim.entity.order;

public class BundleEntity {
    // 번들번호
    public String bundleNo;
    // 구매주문번호
    public String purchaseOrderNo;
    //
    public String purchaseStoreInfoNo;
    // 원번들번호(합주문:TD상품에 원번들번호, 대체주문:대체상품에 원번들)
    public String orgBundleNo;
    // 원천점포ID(매출용실제점포)
    public String originStoreId;
    // 점포ID(주문서에서넘어온 가상점포ID)
    public String storeId;
    // 배송정책번호(대체주문 0 입력됨)
    public String shipPolicyNo;
    // 배송비 할인금액
    public String shipDiscountPrice;
    // 배송금액(원배송비금액 - 배송할인금액)
    public String shipPrice;
    // 도서산간배송가격(고객결제금액)
    public String islandShipPrice;
    // 상품 배송가능지역(ALL:전국,EXCLUDE:제주/도서산간 제외)
    public String shipArea;
    // 고객 주소지 배송가능지역 유형
    public String shipAreaType;
    // 배송유형(ITEM:상품별배송,BUNDLE:번들배송)
    public String shipType;
    // 배송방법(DS_DLV:택배배송,DS_DRCT:직접배송,DS_PICK:방문수령/픽업,DS_POST:우편배송,DS_QUICK:퀵배송,TD_DLV:택배배송,TD_DRCT:직접배송,TD_PICK:방문수령,TD_POST:우편배송,TD_QUICK:퀵배송)
    public String shipMethod;
    // 배송종류(FREE:무료,FIXED:유료,COND:조건부무료)
    public String shipKind;
    // 배송정책비(일반배송상품 배송비 기준)
    public String shipFee;
    // 도서산간배송정책비(도서산간 제주권역 추가배송비 기준)
    public String islandShipFee;
    // 배송가격상품(Pine 전송용 배송가격상품)
    public String shipPriceItemNo;
    // 무료조건비
    public String freeCondition;
    // 선결제여부(Y:선불,N:착불)
    public String prepaymentYn;
    // 출고기한(-1:미정)
    public String releaseDay;
    // 출고시간(출고기한이 1일인 경우 1-24 정의)
    public String releaseTime;
    // 휴일제외여부
    public String holidayExceptYn;
    // 차등여부
    public String diffYn;
    // 차등수량
    public String diffQty;
    // 세금부가세금액(배송비 부가세 계산)
    public String taxVatAmt;
    // 클레임배송정책비
    public String claimShipFee;
    // 다중주문여부(다중배송지여부 Y:다중, N:일반)
    public String multiOrderYn;
    // FC점포ID(FC점포ID)
    public String fcStoreId;
    // VAN번호
    public String vanNo;
    // 배송기사명
    public String driverNm;
    // 배송기사전화번호
    public String driverTelNo;
    // 배송완료여부
    public String shipComplete;
    // 등록일시
    public String regDt;
    // 등록자ID
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정자ID
    public String chgId;

}