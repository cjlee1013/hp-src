package kr.co.homeplus.claim.order.model.user;

import lombok.Data;

@Data
public class CustomerGradeInfoSetDto {
    private long userNo;
    private String searchYear;
    private String searchMonth;
}
