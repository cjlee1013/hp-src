package kr.co.homeplus.claim.utils;

import com.google.gson.Gson;
import java.util.Collections;
import java.util.LinkedHashMap;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.plus.api.support.client.header.LoginDto;
import kr.co.homeplus.plus.api.support.client.header.WebHeaderKey;
import kr.co.homeplus.plus.api.support.header.WebHeaders;
import kr.co.homeplus.plus.util.ServletUtils;
import kr.co.homeplus.plus.util.WebUriUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class RequestUtils {

    private static HttpServletRequest getRequest() {
        return ServletUtils.currentRequest();
    }

    private static String uriDecode(String uri) {
        return WebUriUtils.decode(uri);
    }

    private static String uriEncode(String uri) {
        return WebUriUtils.encode(uri);
    }

    public static String getLoginUserInfo(long userNo, String userId){
        LinkedHashMap<String, Object> parameter = new LinkedHashMap<>();
        parameter.put("userNo", StringUtil.toBlank(userNo));
        parameter.put("userId", StringUtil.toBlank(userId));
        return uriEncode(new Gson().toJson(parameter));
    }

    public static LoginDto getLoginUser(long userNo) {
        return WebHeaders.getLoginUser(getLoginUserInfo(userNo, null));
    }

    private static LinkedHashMap<String, Object> createUserTokenHash(){
        LinkedHashMap<String, Object> headerMap = new LinkedHashMap<>();
        headerMap.put(WebHeaderKey.X_AUTH_TOKEN, getRequest().getHeader(WebHeaderKey.X_AUTH_TOKEN));
        headerMap.put(WebHeaderKey.LOGIN_ACCESS_HASH, getRequest().getHeader(WebHeaderKey.LOGIN_ACCESS_HASH));
        return headerMap;
    }

    public static HttpHeaders createUserLoginHeaders(){
        return createHttpHeaders(createUserTokenHash());
    }

    public static HttpHeaders createUserInfoHeaders(long userNo) {
        LinkedHashMap<String, Object> headerMap = createUserTokenHash();
        headerMap.put(WebHeaderKey.LOGIN_USER_INFO, getLoginUserInfo(userNo, null) );
        return createHttpHeaders(headerMap);
    }

    public static HttpHeaders createHttpHeaders(LinkedHashMap<String, Object> webHeaderMap){
        HttpHeaders httpHeaders = createHttpHeaders();
        webHeaderMap.forEach((key, value) -> {
            httpHeaders.set(key, String.valueOf(value));
        });
        return httpHeaders;
    }

    public static HttpHeaders createHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return httpHeaders;
    }

}
