package kr.co.homeplus.claim.receipt.model.card;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임_카드구매영수증")
public class ClaimCardReceiptGetDto {

    @ApiModelProperty(value = "승인번호", position = 1)
    private String claimApprovalNo;

    @ApiModelProperty(value = "클레임번호", position = 2)
    private long claimNo;

    @ApiModelProperty(value = "카드명", position = 3)
    private String cardNm;

    @ApiModelProperty(value = "카드번호", position = 4)
    private String cardNo;

    @ApiModelProperty(value = "거래유형", position = 5)
    private String claimType;

    @ApiModelProperty(value = "카드할부", position = 6)
    private String cardInstallmentMonth;

    @ApiModelProperty(value = "승일인시", position = 7)
    private String completeDt;

    @ApiModelProperty(value = "승일인시", position = 8)
    private String paymentFshDt;

    @ApiModelProperty(value = "상품명", position = 9)
    private String itemNm;

    @ApiModelProperty(value = "금액", position = 10)
    private long payAmt;

    @ApiModelProperty(value = "부가세", position = 11)
    private long totVatAmt;

    @ApiModelProperty(value = "합계", position = 12)
    private long totPayAmt;

    @ApiModelProperty(value = "파트너명", position = 13)
    private String partnerNm;

    @ApiModelProperty(value = "대표자명", position = 14)
    private String partnerOwner;

    @ApiModelProperty(value = "사업자등록번호", position = 15)
    private String partnerNo;

    @ApiModelProperty(value = "고객센터", position = 16)
    private String partnerInfoCenter;

    @ApiModelProperty(value = "주문번호", position = 17)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "배송번호", position = 19)
    private long bundleNo;
}
