package kr.co.homeplus.claim.order.service.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.claim.order.model.inquiry.InquiryItemDto;
import kr.co.homeplus.claim.order.model.inquiry.OrderInquiryGetDto;
import kr.co.homeplus.claim.order.model.inquiry.OrderInquirySetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderDetailInfoSetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderDetailListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderClaimPaymentInfoDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderDetailListGetDto.MpOrderDetailClaimDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPresentDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderTicketDetailGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderTicketDetailGetDto.MpOrderTicketItemDto;
import kr.co.homeplus.claim.order.mapper.OrderPrimaryMapper;
import kr.co.homeplus.claim.order.mapper.OrderSecondaryMapper;
import kr.co.homeplus.claim.order.model.mypage.MpOrderClaimInfoDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderDetailListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderDetailListSetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderGiftListDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderItemListDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListSetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPickupStoreInfoDto;
import kr.co.homeplus.claim.order.model.mypage.MultiOrderShipAddrEditDto;
import kr.co.homeplus.claim.order.model.mypage.MultiOrderShipBasicInfoEditDto;
import kr.co.homeplus.claim.order.model.mypage.OrderShipAddrEditDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderProductListGetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderShipInfoDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderShipListDto;
import kr.co.homeplus.claim.order.model.ship.MultiOrderShipAddrInfoDto;
import kr.co.homeplus.claim.order.model.subMain.MpOrderMainDto.MyPageMainOrderCount;
import kr.co.homeplus.claim.order.model.subMain.MpOrderMainDto.MyPageMainOrderList;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPaymentInfoDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderProductListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderShipInfoDto;
import kr.co.homeplus.claim.order.model.user.CustomerGradeInfoGetDto.CustomerGradeOrderListDto;
import kr.co.homeplus.claim.order.service.MpOrderMapperService;
import kr.co.homeplus.claim.utils.SqlUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MpOrderMapperServiceImpl implements MpOrderMapperService {

    private final OrderSecondaryMapper orderSecondaryMapper;

    private final OrderPrimaryMapper primaryMapper;

    @Override
    public List<MpOrderListGetDto> getMpOrderList(MpOrderListSetDto mpOrderListSetDto) {
        return orderSecondaryMapper.selectMpOrderList(mpOrderListSetDto);
    }

    @Override
    public List<MpOrderItemListDto> getMpOrderListInfo(long purchaseOrderNo, long bundleNo, long userNo, Boolean isCmbn, Boolean isSubYn) {
        return orderSecondaryMapper.selectMpOrderListInfo(purchaseOrderNo, bundleNo, userNo, isCmbn, isSubYn);
    }

    @Override
    public List<MpOrderShipInfoDto> getMpOrderShipInfo(MpOrderDetailListSetDto mpOrderDetailListSetDto) {
        return orderSecondaryMapper.selectMpOrderShipInfo(mpOrderDetailListSetDto);
    }

    @Override
    public List<MpOrderProductListGetDto> getMpOrderProductList(long purchaseOrderNo, long userNo, Boolean isCmbn) {
        List<MpOrderProductListGetDto> productListGetDtoList = orderSecondaryMapper.selectMpOrderProductList(purchaseOrderNo, userNo, isCmbn);
        if(ObjectUtils.isNotEmpty(productListGetDtoList)){
            productListGetDtoList.stream().filter(dto -> ObjectUtils.isNotEmpty(dto.getClaimNo())).forEach(
                dto -> {
                    LinkedHashMap<String, String> claimShipInfo = this.getClaimShippingStatus(dto.getClaimNo());
                    if(ObjectUtils.isNotEmpty(claimShipInfo)){
                        dto.setPickShipStatus(claimShipInfo.get("pick_ship_status"));
                        dto.setExchShipStatus(claimShipInfo.get("exch_ship_status"));
                    }
                }
            );

            productListGetDtoList.forEach(dto ->{
                dto.setIsTicketFromCoop(this.getTicketFromCoopCheck(dto.getPurchaseOrderNo()));
            });

            List<Long> bundleList = productListGetDtoList.stream().map(MpOrderProductListGetDto::getBundleNo).distinct().collect(Collectors.toList());
            for(Long bundleNo : bundleList){
                List<MpOrderProductListGetDto> productListGetDtoListFromBundle = productListGetDtoList.stream().filter(dto -> dto.getBundleNo() == bundleNo).collect(Collectors.toList());
                boolean isPartClaimN = productListGetDtoListFromBundle.stream().anyMatch(dto -> dto.getClaimYn().equals("N"));
                boolean isAllClaimN = productListGetDtoListFromBundle.stream().allMatch(dto -> dto.getClaimYn().equals("N"));
                if(!isAllClaimN && isPartClaimN){
                    for(MpOrderProductListGetDto productListGetDto : productListGetDtoListFromBundle){
                        if("A".equals(productListGetDto.getClaimYn())){
                            productListGetDto.setClaimYn("Y");
                        }
                    }
                }
            }
        }
        return productListGetDtoList;
    }

    @Override
    public MpOrderDetailListGetDto getOrderDetailInfo(long purchaseOrderNo, long userNo, String siteType, Boolean isCmbn) {
        return orderSecondaryMapper.selectOrderDetailInfo(purchaseOrderNo, userNo, siteType, isCmbn);
    }

    @Override
    public MpMultiOrderDetailListGetDto getMultiOrderDetailList(long purchaseOrderNo, long userNo, String siteType) {
        return orderSecondaryMapper.selectMultiOrderDetailList(purchaseOrderNo, userNo, siteType);
    }

    @Override
    public MpOrderPaymentInfoDto getPaymentInfoList(long purchaseOrderNo, long userNo) {
        MpOrderPaymentInfoDto paymentInfoDto = orderSecondaryMapper.selectPurchaseInfoList(purchaseOrderNo, userNo);
        if(paymentInfoDto != null){
            // 결제정보 - 쿠폰할인정보
            paymentInfoDto.setCouponDiscountList(orderSecondaryMapper.selectCouponDiscountInfoList(purchaseOrderNo));
            // 결제정보 - 프로모션할인정보
            paymentInfoDto.setPromoDiscountList(orderSecondaryMapper.selectPromoDiscountInfoList(purchaseOrderNo));
            // 결제정보 - 결제수단 정보
            paymentInfoDto.setOrderPaymentListList(orderSecondaryMapper.selectPaymentInfoList(purchaseOrderNo));
            // 결제정보 - 혜택/적립 정보
            paymentInfoDto.setOrderBenefitList(orderSecondaryMapper.selectOrderBenefitInfoList(purchaseOrderNo));
        }
        return paymentInfoDto;
    }

    @Override
    public MpOrderClaimPaymentInfoDto getClaimPaymentInfoList(long purchaseOrderNo, long userNo) {
        MpOrderClaimPaymentInfoDto claimPaymentInfoDto = orderSecondaryMapper.selectMpOrderClaimPaymentInfo(purchaseOrderNo, userNo);
        if(claimPaymentInfoDto != null){
            // 결제정보 - 쿠폰할인정보
            claimPaymentInfoDto.setPromoDiscountList(orderSecondaryMapper.selectClaimPromoDiscountInfoList(purchaseOrderNo));
            // 결제정보 - 쿠폰할인정보
            claimPaymentInfoDto.setCouponDiscountList(orderSecondaryMapper.selectClaimCouponDiscountInfoList(purchaseOrderNo));
            // 결제정보 - 결제수단 정보
            claimPaymentInfoDto.setOrderPaymentListList(orderSecondaryMapper.selectClaimPaymentInfoList(purchaseOrderNo));
            // 결제정보 - 혜택/적립 정보
            claimPaymentInfoDto.setOrderBenefitList(orderSecondaryMapper.selectClaimBenefitInfoList(purchaseOrderNo));
            // 대체주문금액
            claimPaymentInfoDto.setSubstitutionAmt(orderSecondaryMapper.selectSubstitutionAmt(purchaseOrderNo));
            // 대체금액 제외처리.
            claimPaymentInfoDto.setTotPaymentAmt(claimPaymentInfoDto.getTotPaymentAmt() - claimPaymentInfoDto.getSubstitutionAmt());
            claimPaymentInfoDto.setTotDiscountPrice(claimPaymentInfoDto.getTotDiscountPrice() + claimPaymentInfoDto.getSubstitutionAmt());
        }
        return claimPaymentInfoDto;
    }

    @Override
    public List<MyPageMainOrderCount> getMyPageMainOrderCount(long userNo, String siteType) {
        return orderSecondaryMapper.selectMpMainOrderCount(userNo, siteType);
    }

    @Override
    public List<MyPageMainOrderList> getMyPageMainOrderList(long userNo, String siteType) {
        return orderSecondaryMapper.selectMpMainOrderList(userNo, siteType);
    }

    @Override
    public Boolean modifyShipAddrChange(OrderShipAddrEditDto shipAddrEditDto) {
        return SqlUtils.isGreaterThanZero(primaryMapper.updateShippingAddrEditCheck(shipAddrEditDto));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getShippingPresentStatus(long purchaseOrderNo) {
        return primaryMapper.selectShippingPresentStatus(purchaseOrderNo);
    }

    @Override
    public List<MpOrderClaimInfoDto> getOrderClaimBundleInfo(long purchaseOrderNo, long bundleNo, Boolean isOrigin) {
        return orderSecondaryMapper.selectMpOrderClaimInfo(purchaseOrderNo, bundleNo, isOrigin);
    }

    @Override
    public List<MpOrderClaimInfoDto> getOrderClaimBundleInfo(long purchaseOrderNo) {
        return orderSecondaryMapper.selectMpOrderClaimOrgInfo(purchaseOrderNo);
    }

    @Override
    public List<MpOrderGiftListDto> getOrderGiftInfo(long purchaseOrderNo) {
        return orderSecondaryMapper.selectOrderGiftList(purchaseOrderNo);
    }

    @Override
    public MpOrderPickupStoreInfoDto getOrderPickupStoreInfo(long purchaseOrderNo) {
        return orderSecondaryMapper.selectPickStoreInfo(purchaseOrderNo);
    }

    @Override
    public List<MpMultiOrderProductListGetDto> getMpOrderMultiProductList(long purchaseOrderNo, long multiBundleNo, long userNo) {
        List<MpMultiOrderProductListGetDto> multiOrderProductListGetDtoList = orderSecondaryMapper.selectMpOrderMultiProductList(purchaseOrderNo, multiBundleNo, userNo);
        if(ObjectUtils.isNotEmpty(multiOrderProductListGetDtoList)) {
            multiOrderProductListGetDtoList.stream().filter(dto -> ObjectUtils.isNotEmpty(dto.getClaimNo())).forEach(
                    dto -> {
                        LinkedHashMap<String, String> claimShipInfo = this.getClaimShippingStatus(dto.getClaimNo());
                        if (ObjectUtils.isNotEmpty(claimShipInfo)) {
                            dto.setPickShipStatus(claimShipInfo.get("pick_ship_status"));
                            dto.setExchShipStatus(claimShipInfo.get("exch_ship_status"));
                        }
                    }
                );

        }
        return multiOrderProductListGetDtoList;
    }

    @Override
    public List<MpMultiOrderShipListDto> getMpMultiOrderShipList(MpOrderDetailListSetDto mpOrderDetailListSetDto) {
        return orderSecondaryMapper.selectMpMultiOrderShipList(mpOrderDetailListSetDto);
    }

    @Override
    public MpMultiOrderShipInfoDto getMpOrderMultiShipInfo(MpMultiOrderDetailInfoSetDto mpMultiOrderDetailInfoSetDto) {
        return orderSecondaryMapper.selectMpOrderMultiShipInfo(mpMultiOrderDetailInfoSetDto);
    }

    @Override
    public MpOrderTicketDetailGetDto getOrderTicketDetailInfo(String purchaseOrderNo, long userNo) {
        return orderSecondaryMapper.selectOrderTicketDetailInfo(Long.parseLong(purchaseOrderNo), userNo);
    }

    @Override
    public List<MpOrderTicketItemDto> getOrderTicketDetailItemInfo(String purchaseOrderNo, long userNo) {
        return orderSecondaryMapper.selectOrderTicketDetailItemInfo(Long.parseLong(purchaseOrderNo), userNo);
    }

    @Override
    public List<MpOrderDetailClaimDto> getOrderDetailClaimInfo(long purchaseOrderNo) {
        return orderSecondaryMapper.selectOrderDetailClaimInfo(purchaseOrderNo);
    }

    @Override
    public List<MpOrderDetailClaimDto> getMultiOrderDetailClaimInfo(long purchaseOrderNo) {
        return orderSecondaryMapper.selectMultiOrderDetailClaimInfo(purchaseOrderNo);
    }

    @Override
    public LinkedHashMap<String, Object> getSafetyIssueInfo(long bundleNo) {
        return orderSecondaryMapper.selectSafetyIssueInfo(bundleNo);
    }

    @Override
    public MpOrderPresentDto getMpOrderPresentInfo(long purchaseOrderNo){
        return orderSecondaryMapper.selectOrderPresentInfo(purchaseOrderNo);
    }

    @Override
    public LinkedHashMap<String, String> getClaimShippingStatus(long claimNo){
        return orderSecondaryMapper.selectClaimShippingStatus(claimNo);
    }

    @Override
    public List<CustomerGradeOrderListDto> getUserGradeOrderInfo(long userNo, String searchDt, String siteType){
        return orderSecondaryMapper.selectUserGradeUserInfo(userNo, searchDt, siteType);
    }

    @Override
    public String getTicketFromCoopCheck(long purchaseOrderNo) {
        return SqlUtils.isGreaterThanZero(orderSecondaryMapper.selectTicketFromCoopCheck(purchaseOrderNo)) ? "Y" : "N";
    }

    @Override
    public int getClaimSubstitutionAmt(long purchaseOrderNo) {
        return orderSecondaryMapper.selectSubstitutionAmt(purchaseOrderNo);
    }

    @Override
    public List<OrderInquiryGetDto> getInquiryOrderItemInfo(OrderInquirySetDto setDto) {
        return orderSecondaryMapper.selectInquiryOrderInfo(setDto);
    }

    @Override
    public List<InquiryItemDto> getInquiryOrderItemList(long bundleNo) {
        return orderSecondaryMapper.selectInquiryOrderItemList(bundleNo);
    }

    @Override
    public boolean modifyMultiOrderShipAddrInfo(MultiOrderShipAddrEditDto multiOrderShipAddrEditDto) {
        return SqlUtils.isGreaterThanZero(primaryMapper.updateMultiOrderShippingAddrInfo(multiOrderShipAddrEditDto));
    }

    @Override
    public boolean modifyMultiOrderShipBasicInfo(MultiOrderShipBasicInfoEditDto multiOrderShipBasicInfoEditDto) {
        return SqlUtils.isGreaterThanZero(primaryMapper.updateMultiOrderShippingAddrBasicInfo(multiOrderShipBasicInfoEditDto));
    }

    @Override
    public List<MultiOrderShipAddrInfoDto> getMultiOrderShipAddrInfo(long bundleNo) {
        return orderSecondaryMapper.selectMultiOrderShipAddrInfo(bundleNo);
    }

    @Override
    public List<CustomerGradeOrderListDto> getUserGradeOrderInfoByClaim(long userNo, String searchDt, String siteType){
        return orderSecondaryMapper.selectUserGradeUserInfoByClaim(userNo, searchDt, siteType);
    }

    @Override
    public Boolean modifyOrderUserRecentlyInfo(String auroraShipMsgCd, String auroraShipMsg, long userNo) {
        return SqlUtils.isGreaterThanZero(primaryMapper.updateOrderUserRecentlyInfo(auroraShipMsgCd, auroraShipMsg, userNo));
    }
}
