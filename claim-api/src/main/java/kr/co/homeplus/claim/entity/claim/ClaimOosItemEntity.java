package kr.co.homeplus.claim.entity.claim;

public class ClaimOosItemEntity {
    // 결품/대체거래 아이템 SEQ
    public String oosItemSeq;
    // 결품/대체거래 SEQ
    public String oosManageSeq;
    // 주문아이템번호
    public String itemNo;
    // 픽킹수량
    public String pickingQty;
    // 대체아이템번호
    public String subItemNo;
    // 대체수량
    public String subQty;
    // 결품수량
    public String oosQty;
    // 등록일
    public String regDt;
}
