package kr.co.homeplus.claim.order.model.mypage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 주문/배송 - 주문 상세")
public class MpOrderDetailListGetDto {

    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "주문일자", position = 2)
    private String orderDt;

    @ApiModelProperty(value = "합배송여부", position = 3)
    private String cmbnYn;

    @ApiModelProperty(value = "대체주문여부", position = 4)
    private String substitutionOrderYn;

    @ApiModelProperty(value = "배송정보", position = 6)
    private List<MpOrderShipInfoDto> shipInfo;

    @ApiModelProperty(value = "상품상세리스트", position = 7)
    private List<MpOrderProductListGetDto> orderDetailInfo;

    @ApiModelProperty(value = "합배송상품리스트", position = 8)
    private List<MpOrderProductListGetDto> combineDetailList;

    @ApiModelProperty(value = "결제정보", position = 9)
    private MpOrderPaymentListDto orderPaymentInfo;

    @ApiModelProperty(value = "결제정보(합배송)", position = 10)
    private MpOrderPaymentListDto combinePaymentInfo;

    @ApiModelProperty(value = "사은품정보", position = 11)
    private List<MpOrderGiftListDto> orderGiftList;

    @ApiModelProperty(value = "클레임정보", position = 12)
    private List<MpOrderDetailClaimDto> claimInfoList;

    @ApiModelProperty(value = "주문타입", position = 13, hidden = true)
    private String orderType;

    @Data
    public static class MpOrderDetailClaimDto {
        @ApiModelProperty(value = "번들번호")
        private long bundleNo;
        @ApiModelProperty(value = "다중번들번호")
        private long multiBundleNo;
        @ApiModelProperty(value = "클레임가능여부")
        private String claimYn;
    }
}
