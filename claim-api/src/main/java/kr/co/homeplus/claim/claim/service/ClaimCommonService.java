package kr.co.homeplus.claim.claim.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.claim.claim.model.external.ClaimCouponReIssueSetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimCouponReIssueSetDto.CouponReIssueDto;
import kr.co.homeplus.claim.claim.model.external.ClaimDgvCancelSetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimDgvReceiptResponseDto;
import kr.co.homeplus.claim.claim.model.external.ClaimMhcCancelSetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimMileageCancelSetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimOcbCancelSetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimPgCancelSetDto;
import kr.co.homeplus.claim.claim.model.external.MhcCardInfoDto;
import kr.co.homeplus.claim.claim.model.external.RestoreSlotStockDto;
import kr.co.homeplus.claim.claim.model.external.message.MessageSendTalkSetDto;
import kr.co.homeplus.claim.claim.model.external.message.MessageSendTalkSetDto.MessageSendBodyDto;
import kr.co.homeplus.claim.claim.model.external.mhc.SaveCancelMhcPartSetDto;
import kr.co.homeplus.claim.claim.model.external.mhc.SaveCancelMhcSetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claim.claim.model.register.ClaimBundleRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimExchShippingRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimItemRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimPaymentRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimRegDetailDto;
import kr.co.homeplus.claim.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.claim.claim.model.register.ClaimReqRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimShippingRegDto;
import kr.co.homeplus.claim.claim.model.register.ticket.ClaimTicketRegDto;
import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claim.entity.claim.ClaimMstEntity;
import kr.co.homeplus.claim.entity.claim.ClaimPaymentEntity;
import kr.co.homeplus.claim.entity.claim.ClaimReqEntity;
import kr.co.homeplus.claim.enums.ClaimCode;
import kr.co.homeplus.claim.enums.ClaimResponseCode;
import kr.co.homeplus.claim.enums.ExchangeShipType;
import kr.co.homeplus.claim.enums.ExternalInfo;
import kr.co.homeplus.claim.factory.EntityFactory;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SetParameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimCommonService {

    // 클레임 맵퍼 서비스
    private final ClaimMapperService mapperService;

    private final ExternalService externalService;

    /**
     * 클레임 데이터 맵 생성.
     *
     * @param regSetDto 클레임 등록 데이터
     * @return 클레임 데이터 맵
     */
    public LinkedHashMap<String, Object> createClaimDataMap(ClaimRegSetDto regSetDto) {
        // 기존 orderItemNo -> bundleNo 로 변경. (2020.05.21)
        LinkedHashMap<String, Object> createClaimData = mapperService.getCreateClaimBundleInfo(Long.parseLong(regSetDto.getPurchaseOrderNo()), Long.parseLong(regSetDto.getBundleNo()), regSetDto.getUserNo());

        // Claim 타입 설정.
        createClaimData.put("claim_type", regSetDto.getClaimType());
        createClaimData.put("claim_reason_type", regSetDto.getClaimReasonType());
        createClaimData.put("claim_reason_detail", regSetDto.getClaimReasonDetail());
        createClaimData.put("request_id", regSetDto.getRegId());
        createClaimData.put("claim_bundle_qty", regSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getClaimQty).mapToInt(Integer::parseInt).sum());

        return createClaimData;
    }

    /**
     * 환불 예정금액 조회
     *
     * @param regSetDto 등록 DTO
     * @return ClaimPreRefundCalcSetDto 환불예정금액 조회 데이터.
     */
    public ClaimPreRefundCalcSetDto convertCalcSetDto(ClaimRegSetDto regSetDto){
        return ClaimPreRefundCalcSetDto.builder()
            .claimItemList(regSetDto.getClaimItemList())
            .purchaseOrderNo(regSetDto.getPurchaseOrderNo())
            .cancelType(regSetDto.getCancelType())
            .whoReason(regSetDto.getWhoReason())
            .claimPartYn(regSetDto.getClaimPartYn())
            .userNo(regSetDto.getUserNo())
            .isOmni(regSetDto.getIsOmni())
            .claimType(regSetDto.getClaimType())
            .encloseYn(regSetDto.getClaimDetail() == null ? "N" : regSetDto.getClaimDetail().getIsEnclose())
            .build();
    }

    /**
     * 클레임 마스터 등록을 위한 DTO 생성.
     *
     * @param regSetDto 환불예정금액 조회 데이터
     * @return ClaimMstRegDto 클레임 마스터 등록 DTO 정보
     * @throws Exception 오류 처리.
     */
    public ClaimMstRegDto getFusionForClaimMst(ClaimRegSetDto regSetDto) throws Exception {
        ClaimPreRefundCalcSetDto calcSetDto = this.convertCalcSetDto(regSetDto);
        // claimMstEntity
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class);
        // 아이템별 최소 구매 수량 미만 요청 체크
        // 클레임 환불 예정 금액 조회
        ClaimPreRefundCalcDto claimPreRefundCalcDto;
        if (regSetDto.getIsMultiOrder()){
            claimPreRefundCalcDto = mapperService.getClaimMultiPreRefundCalculation(calcSetDto);
        } else {
            claimPreRefundCalcDto = mapperService.getClaimPreRefundCalculation(calcSetDto);
        }

        LinkedHashMap<String, Object> refundInfo = ObjectUtils.getConvertMap(claimPreRefundCalcDto);

        log.info("주문번호({}) 에 대한 환불예정금액 ::: {}", calcSetDto.getPurchaseOrderNo(), refundInfo);

        if((int)refundInfo.get("returnValue") != 0){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR03);
        }
        // paymentNo, purchaseOrderNo,siteType,userNo,empNo
        LinkedHashMap<String, Object> createClaimMstData = mapperService.getCreateClaimMstInfo(Long.parseLong(calcSetDto.getPurchaseOrderNo()), calcSetDto.getUserNo());

        // 클레임 마스터 등록을 위한 purchaseOrder 테이블 조회.
        ClaimMstRegDto claimMstRegDto = ClaimMstRegDto.builder()
            .paymentNo(Long.parseLong(createClaimMstData.get(claimMst.paymentNo).toString()))
            .purchaseOrderNo(Long.parseLong(createClaimMstData.get(claimMst.purchaseOrderNo).toString()))
            .siteType(createClaimMstData.get(claimMst.siteType).toString())
            .userNo(createClaimMstData.get(claimMst.userNo).toString())
            .claimPartYn(calcSetDto.getClaimPartYn())
            .empNo(ObjectUtils.toString(createClaimMstData.get("emp_no")))
            .empRemainAmt(0)
            .oosYn(calcSetDto.getIsOmni() ? "Y" : "N")
            .gradeSeq(ObjectUtils.toString(createClaimMstData.get("grade_seq")))
            .marketType(ObjectUtils.toString(createClaimMstData.get("market_type")))
            .marketOrderNo(ObjectUtils.toString(createClaimMstData.get("market_order_no")))
            .build();

        // 클레임 등록 정보 map 화
        LinkedHashMap<String, Object> claimRegInfo = ObjectUtils.getConvertMap(claimMstRegDto);

        if(refundInfo.isEmpty() || claimRegInfo.isEmpty()){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR02);
        }

        claimRegInfo.forEach((key, value) -> {
            String getkey = key;
            if(getkey.equals("completeAmt")){
                value = refundInfo.get("refundAmt");
            } else {
                if(getkey.startsWith("tot")){
                    getkey = getkey.replace("tot", "");
                }
                if(refundInfo.get(StringUtils.uncapitalize(getkey)) != null){
                    value = refundInfo.getOrDefault(StringUtils.uncapitalize(getkey), 0);
                }
            }
            claimRegInfo.replace(key, value);
        });
        // 데이터 넣기
        claimMstRegDto.putMap(claimRegInfo);
        // 귀책사유
        claimMstRegDto.setWhoReason(calcSetDto.getWhoReason());

        return claimMstRegDto;
    }

    /**
     * 클레임 번들 등록 DTO 생성
     *
     * @param claimData 클레임 생성 데이터 맵
     * @return ClaimBundleRegDto 클레임 번들 등록 DTO
     */
    public ClaimBundleRegDto createClaimBundleRegDto(HashMap<String, Object> claimData) {

        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class);
        return ClaimBundleRegDto.builder()
            .purchaseOrderNo(Long.parseLong(claimData.get(claimBundle.purchaseOrderNo).toString()))
            .paymentNo(Long.parseLong(claimData.get(claimBundle.paymentNo).toString()))
            .userNo(Long.parseLong(claimData.get(claimBundle.userNo).toString()))
            .bundleNo(Long.parseLong(claimData.get(claimBundle.bundleNo).toString()))
            .partnerId(claimData.get(claimBundle.partnerId).toString())
            .claimType(claimData.get(claimBundle.claimType).toString())
            .claimStatus(ClaimCode.CLAIM_STATUS_C1.getCode())
            .claimNo(Long.parseLong(claimData.get(claimBundle.claimNo).toString()))
            // TODO: 27/04/2020
            // 임시, 필드 속성 여부에 따른 처리 필요.
            .originClaimBundleNo(0)
            .claimBundleQty(Long.parseLong(claimData.get(claimBundle.claimBundleQty).toString()))
            .claimShipFeeBank("N")
            .claimShipFeeEnclose(ObjectUtils.toString(claimData.getOrDefault("isEnclose", "N")))
            .warehouse("N")
            .isNoDiscountDeduct("N")
            .isNoShipDeduct("N")
            .reserveYn(ObjectUtils.toString(claimData.get(claimBundle.reserveYn)))
            .orderCategory(ObjectUtils.toString(claimData.get(claimBundle.orderCategory)))
            .closeSendYn(ObjectUtils.toString(claimData.get(claimBundle.closeSendYn)))
            .shipMobileNo(ObjectUtils.toString(claimData.get(claimBundle.shipMobileNo)))
            .marketOrderNo(ObjectUtils.toString(claimData.get(claimBundle.marketOrderNo)))
            .build();
    }

    /**
     * 클레임 요청 등록 DTO 생성
     *
     * @param claimData 클레임 생성 데이터 맵
     * @return ClaimReqRegDto 클레임 요청 등록
     */
    public ClaimReqRegDto createClaimReqRegDto(HashMap<String, Object> claimData) {
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class);
        return ClaimReqRegDto.builder()
            .claimNo(Long.parseLong(claimData.get(claimReq.claimNo).toString()))
            .claimBundleNo(Long.parseLong(claimData.get(claimReq.claimBundleNo).toString()))
            .claimReasonType(claimData.get(claimReq.claimReasonType).toString())
            .claimReasonDetail(ObjectUtils.toString(claimData.get(claimReq.claimReasonDetail)))
            .requestId(claimData.get(claimReq.requestId).toString())
            .requestDt("NOW()")
            .uploadFileName(ObjectUtils.toString(claimData.get(claimReq.uploadFileName)))
            .uploadFileName2(ObjectUtils.toString(claimData.get(claimReq.uploadFileName2)))
            .uploadFileName3(ObjectUtils.toString(claimData.get(claimReq.uploadFileName3)))
            .deductPromoYn(ObjectUtils.toString(claimData.get("deduct_promo_yn")))
            .deductDiscountYn(ObjectUtils.toString(claimData.get("deduct_discount_yn")))
            .deductShipYn(ObjectUtils.toString(claimData.get("deduct_ship_yn")))
            .build();
    }

    /**
     * 클레임 아이템 등록 DTO 생성
     *
     * @param claimData 클레임 생성 데이터 맵
     * @return ClaimItemRegDto 클레임 아이템 등록 DTO
     */
    public ClaimItemRegDto createClaimItemRegDto(LinkedHashMap<String, Object> claimData) {
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class);
        return ClaimItemRegDto.builder()
            .claimNo(ObjectUtils.toLong(claimData.get(claimItem.claimNo)))
            .claimBundleNo(ObjectUtils.toLong(claimData.get(claimItem.claimBundleNo)))
            .paymentNo(ObjectUtils.toLong(claimData.get(claimItem.paymentNo)))
            .purchaseOrderNo(ObjectUtils.toLong(claimData.get(claimItem.purchaseOrderNo)))
            .bundleNo(ObjectUtils.toLong(claimData.get(claimItem.bundleNo)))
            .originStoreId(ObjectUtils.toString(claimData.get(claimItem.originStoreId)))
            .storeId(ObjectUtils.toString(claimData.get(claimItem.storeId)))
            .fcStoreId(ObjectUtils.toString(claimData.get(claimItem.fcStoreId)))
            .orderItemNo(ObjectUtils.toLong(claimData.get(claimItem.orderItemNo)))
            .itemNo(ObjectUtils.toString(claimData.get(claimItem.itemNo)))
            .itemName(ObjectUtils.toString(claimData.get(claimItem.itemName)))
            .itemPrice(ObjectUtils.toLong(claimData.get(claimItem.itemPrice)))
            .claimItemQty(0)
            .storeType(ObjectUtils.toString(claimData.get(claimItem.storeType)))
            .mallType(ObjectUtils.toString(claimData.get(claimItem.mallType)))
            .taxYn(ObjectUtils.toString(claimData.get(claimItem.taxYn)))
            .partnerId(ObjectUtils.toString(claimData.get(claimItem.partnerId)))
            .empDiscountRate(0)
            .empDiscountAmt(0)
            .regId(ObjectUtils.toString(claimData.get("request_id")))
            .marketOrderNo(ObjectUtils.toString(claimData.get(claimItem.marketOrderNo)))
            .build();
    }

    /**
     * 클레임 - 클레임 결제 수단 등록 로직
     *
     * @param claimData 클레임 데이터
     * @param paymentType 결제 타입
     * @param paymentAmt 취소금액
     * @return 클레임 결제 수단 등록 DTO
     */
    public ClaimPaymentRegDto createClaimPaymentRegDto(LinkedHashMap<String, Object> claimData, String paymentType, long paymentAmt) throws Exception {
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class);
        // 결제 번호
        long paymentNo = Long.parseLong(claimData.get(claimPayment.paymentNo).toString());

        // 원주문 결제 정보 가지고 오기
        LinkedHashMap<String, Object> originPaymentInfo = mapperService.getOriPaymentInfoForClaim(paymentNo, paymentType);

        if(originPaymentInfo == null){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR04);
        }

        return ClaimPaymentRegDto.builder()
            .claimNo(Long.parseLong(claimData.get(claimPayment.claimNo).toString()))
            .paymentNo(Long.parseLong(claimData.get(claimPayment.paymentNo).toString()))
            .purchaseOrderNo(Long.parseLong(claimData.get(claimPayment.purchaseOrderNo).toString()))
            .pgKind(ObjectUtils.toString(originPaymentInfo.get(claimPayment.pgKind)))
            .paymentType(this.getClaimPaymentTypeCode(paymentType))
            .parentPaymentMethod(ObjectUtils.toString(originPaymentInfo.get(claimPayment.parentPaymentMethod)))
            .methodCd(ObjectUtils.toString(originPaymentInfo.get(claimPayment.methodCd)))
            .easyMethodCd(ObjectUtils.toString(originPaymentInfo.get(claimPayment.easyMethodCd)))
            .pgMid(ObjectUtils.toString(originPaymentInfo.get(claimPayment.pgMid)))
            .pgOid(ObjectUtils.toString(originPaymentInfo.get(claimPayment.pgOid)))
            .paymentTradeId(ObjectUtils.toString(originPaymentInfo.get(claimPayment.paymentTradeId)))
            .originApprovalCd(ObjectUtils.toString(originPaymentInfo.get(claimPayment.originApprovalCd)))
            .paymentStatus(ClaimCode.CLAIM_PAYMENT_STATUS_P0.getCode())
            .paymentAmt(paymentAmt)
            .payToken(ObjectUtils.toString(originPaymentInfo.get(claimPayment.payToken)))
            .substitutionAmt(0)
            .pgCancelRequestCnt(0)
            .regId(claimData.get("request_id").toString())
            .chgId(claimData.get("request_id").toString())
            .lastCancelYn("N")
            .build();
    }

    /**
     * 클레임 결제 수단을 등록하기 위한 로직
     * 클레임 발생시 취소금액을 처리하기 위하여
     * 환불예정금액에서 계산된 결제 수단에 따라 분리.
     *
     * @param claimPaymentMap 클레임 마스터정보
     * @return LinkedHashMap<String, Long> 클레임 결제수단 등록 결제타입 정보
     */
    public LinkedHashMap<String, Long> getClaimPaymentTypeList(LinkedHashMap<String, Object> claimPaymentMap){

        LinkedHashMap<String, Long> paymentTypeMap = new LinkedHashMap<>();

        if(ObjectUtils.toLong(claimPaymentMap.get("pgAmt")) != 0){
            paymentTypeMap.put("PG", ObjectUtils.toLong(claimPaymentMap.get("pgAmt")));
        }
        if(ObjectUtils.toLong(claimPaymentMap.get("mhcAmt")) != 0){
            paymentTypeMap.put("MHC", ObjectUtils.toLong(claimPaymentMap.get("mhcAmt")));
        }
        if(ObjectUtils.toLong(claimPaymentMap.get("ocbAmt")) != 0){
            paymentTypeMap.put("OCB", ObjectUtils.toLong(claimPaymentMap.get("ocbAmt")));
        }
        if(ObjectUtils.toLong(claimPaymentMap.get("mileageAmt")) != 0){
            paymentTypeMap.put("MILEAGE", ObjectUtils.toLong(claimPaymentMap.get("mileageAmt")));
        }
        if(ObjectUtils.toLong(claimPaymentMap.get("dgvAmt")) != 0){
            paymentTypeMap.put("DGV", ObjectUtils.toLong(claimPaymentMap.get("dgvAmt")));
        }

        if(paymentTypeMap.size() == 0){
            return null;
        }
        return paymentTypeMap;
    }

    public String getClaimPaymentTypeCode(String type){
        return Stream.of(ClaimCode.values())
            .filter(e -> e.name().contains("CLAIM_CONVERT_PAYMENT_TYPE"))
            .collect(Collectors.toMap(ClaimCode::name, ClaimCode::getCode))
            .get("CLAIM_CONVERT_PAYMENT_TYPE_".concat(type.toUpperCase(Locale.KOREA)));
    }

    public String getClaimPaymentTypeDescription(String type){
        return Stream.of(ClaimCode.values())
            .filter(e -> e.name().contains("CLAIM_CONVERT_PAYMENT_TYPE"))
            .collect(Collectors.toMap(ClaimCode::getCode, ClaimCode::getExternalNm))
            .get(type.toUpperCase(Locale.KOREA));
    }

    // ====================================================================================
    // payment cancel dto
    // ====================================================================================

    public ClaimPgCancelSetDto createPaymentPgCancelSetDto(LinkedHashMap<String, Object> paymentData) {

        int orderPaymentAmt = ObjectUtils.toInt(paymentData.get("order_payment_amt"));
        int pgCancelAmt = ObjectUtils.toInt(paymentData.get("payment_amt")) + ObjectUtils.toInt(paymentData.get("prev_payment_amt"));
        int partRemainPrice = orderPaymentAmt - pgCancelAmt;

        if(orderPaymentAmt < pgCancelAmt){
            partRemainPrice = 0;
        }

        return ClaimPgCancelSetDto.builder()
            .pgKind(ObjectUtils.toString(paymentData.get("pg_kind")))
            .pgOid(ObjectUtils.toString(paymentData.get("pg_oid")))
            .pgMid(ObjectUtils.toString(paymentData.get("pg_mid")))
            .pgTid(ObjectUtils.toString(paymentData.get("payment_trade_id")))
            .userNo(ObjectUtils.toLong(paymentData.get("user_no")))
            .parentMethodCd(ObjectUtils.toString(paymentData.get("parent_payment_method")))
            .cancelReason(paymentData.getOrDefault("cancel_message", "").toString())
            .clientIp(paymentData.getOrDefault("client_ip", "0.0.0.0").toString())
            .partCancelPrice(ObjectUtils.toLong(paymentData.getOrDefault("payment_amt", null)))
            .partRemainPrice(partRemainPrice)
            .cancelType(isTotalCancel(paymentData) ? "ALL" : "PART")
            .payToken(ObjectUtils.toString(paymentData.get("pay_token")))
            .build();
    }

    /**
     * MHC 포인트 취소 로직.
     * @param paymentData pg_kind, payment_type, pg_mid, pg_oid, payment_trade_id, origin_approval_cd, payment_amt
     *                    pg_cancel_request_cnt, parent_payment_method, purchase_order_no
     *                    claim_payment_no, order_payment_amt, prev_payment_amt
     * @return ClaimMhcCancelSetDto MHC취소 요청 DTO
     * @throws Exception 오류시 오류처리.
     */
    public ClaimMhcCancelSetDto createPaymentCancelForMhc(LinkedHashMap<String, Object> paymentData) throws Exception {
        // externalService 로 user-api 조회
        MhcCardInfoDto mhcCardInfo = getUserMhcCardInfo(ObjectUtils.toLong(paymentData.get("user_no")));

        ClaimMhcCancelSetDto claimMhcCancelSetDto = ClaimMhcCancelSetDto.builder()
            .userNo(ObjectUtils.toLong(paymentData.get("user_no")))
            .siteType(ObjectUtils.toString(paymentData.get("site_type")))
            .saleDt(new SimpleDateFormat("yyyyMMdd").format(new Date().getTime()))
            .mhcCardNoEnc(mhcCardInfo.getMhcCardnoEnc())
            .mhcCardNoHmac(mhcCardInfo.getMhcCardnoHmac())
            .claimNo(ObjectUtils.toLong(paymentData.get("claim_no")))
            .cancelAmt(ObjectUtils.toLong(paymentData.get("payment_amt")))
            .orgMhcAprDate(ObjectUtils.toString(paymentData.get("payment_trade_id")))
            .orgMhcAprNumber(ObjectUtils.toString(paymentData.get("origin_approval_cd")))
            .partCancelYn(isTotalCancel(paymentData) ? "N" : "Y")
            .build();
        log.debug("createPaymentCancelForMhc :: {} // isTotalCancel :: {}", claimMhcCancelSetDto, isTotalCancel(paymentData));
        return claimMhcCancelSetDto;
    }

    public ClaimOcbCancelSetDto createPaymentCancelForOcb(LinkedHashMap<String, Object> paymentData) throws Exception {
        List<String> mctTransactionIdList = new ArrayList<>();
        mctTransactionIdList.add("CANCEL");
        mctTransactionIdList.add(ObjectUtils.toString(paymentData.get("pg_mid")).split("-")[1]);
        mctTransactionIdList.add(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
        mapperService.modifyClaimPaymentOcbTransactionId(ObjectUtils.toLong(paymentData.get("claim_payment_no")), String.join("-", mctTransactionIdList));
        return ClaimOcbCancelSetDto.builder()
            .cancelOcbPoint(ObjectUtils.toLong(paymentData.get("payment_amt")))
            .mctTransactionId(String.join("-", mctTransactionIdList))
            .orgOcbTransactionId(ObjectUtils.toString(paymentData.get("payment_trade_id")))
            .partCancelYn(isTotalCancel(paymentData) ? "N" : "Y")
            .claimNo(ObjectUtils.toLong(paymentData.get("claim_no")))
            .userNo(ObjectUtils.toString(paymentData.get("user_no")))
            .build();
    }

    public ClaimMileageCancelSetDto createPaymentCancelForMileageDto(LinkedHashMap<String, Object> paymentData){
        return ClaimMileageCancelSetDto.builder()
            .userNo(ObjectUtils.toString(paymentData.get("payment_trade_id")))
            .tradeNo(Long.parseLong(ObjectUtils.toString(paymentData.get("purchase_order_no"))))
            .cancelMessage(paymentData.getOrDefault("cancel_message", "").toString())
            .regId(ObjectUtils.toString(paymentData.get("reg_id")))
            .requestCancelAmt(ObjectUtils.toString(paymentData.get("payment_amt")))
            .build();
    }

    public ClaimDgvCancelSetDto createPaymentCancelForDgv(LinkedHashMap<String, Object> paymentData) throws Exception {
        ClaimDgvReceiptResponseDto responseDto = externalService.getTransfer(
            ExternalInfo.PAYMENT_CANCEL_DGV_RECEIPT,
            new ArrayList<SetParameter>(){{
                add(SetParameter.create("siteType", ObjectUtils.toString(paymentData.get("site_type"))));
                add(SetParameter.create("tradeDt", new SimpleDateFormat("yyyyMMdd").format(new Date().getTime())));
            }},
            ClaimDgvReceiptResponseDto.class
        );
        return ClaimDgvCancelSetDto.builder()
            .dgvCardNo(ObjectUtils.toString(paymentData.get("pg_mid")))
            .posNo(responseDto.getPosNo())
            .tradeNo(responseDto.getTradeNo())
            .saleDt(new SimpleDateFormat("yyyyMMdd").format(new Date().getTime()))
            .partCancelYn(isTotalCancel(paymentData) ? "N" : "Y")
            .tradeAmount(ObjectUtils.toLong(paymentData.get("payment_amt")))
            .claimNo(ObjectUtils.toLong(paymentData.get("claim_no")))
            .orgAprNumber(ObjectUtils.toString(paymentData.get("origin_approval_cd")))
            .orgAprDate(ObjectUtils.toString(paymentData.get("pay_token")))
            .orgPosNo(ObjectUtils.toString(paymentData.get("pg_oid")))
            .orgTradeNo(ObjectUtils.toString(paymentData.get("payment_trade_id")))
            .userNo(ObjectUtils.toString(paymentData.get("user_no")))
            .build();
    }

    public RestoreSlotStockDto createRestoreStockSlotDto(LinkedHashMap<String, Object> claimData) {
        return RestoreSlotStockDto.builder()
            .purchaseOrderNo(ObjectUtils.toLong(claimData.get("purchase_order_no")))
            .bundleNo(ObjectUtils.toLong(claimData.get("bundle_no")))
            .claimNo(ObjectUtils.toLong(claimData.get("claim_no")))
            .userNo(ObjectUtils.toLong(claimData.get("user_no")))
            .build();
    }

    public ClaimShippingRegDto createClaimPickShippingDto(ClaimRegDetailDto regDetailDto, LinkedHashMap<String, Object> claimData) {
        LinkedHashMap<String, Object> pickInfo = mapperService.getClaimPickIsAutoInfo(ObjectUtils.toLong(claimData.get("purchase_order_no")), ObjectUtils.toLong(claimData.get("bundle_no")));

        String shipType = "TD,AURORA,PICK,SUM".contains(ObjectUtils.toString(pickInfo.get("ship_type")))? "TD" : "DS";
        String shipMethod = ObjectUtils.toString(pickInfo.get("ship_method"));
        // 기본 데이터 생성.(필수데이터)
        ClaimShippingRegDto shippingRegDto = ClaimShippingRegDto.builder()
            .claimBundleNo(ObjectUtils.toLong(claimData.get("claim_bundle_no")))
            .isPick(regDetailDto.getIsPick())
            .userNo(ObjectUtils.toLong(claimData.get("user_no")))
            .pickTrackingYn("N")
            .partnerId(ObjectUtils.toString(claimData.get("partner_id")))
            .orgInvoiceNo(ObjectUtils.toString(claimData.get("invoice_no")))
            .pickReceiverNm(regDetailDto.getPickReceiverNm())
            .pickZipcode(regDetailDto.getPickZipCode())
            .pickBaseAddr(regDetailDto.getPickBaseAddr())
            .pickDetailAddr(regDetailDto.getPickBaseDetailAddr())
            .pickRoadBaseAddr(regDetailDto.getPickRoadBaseAddr())
            .pickRoadDetailAddr(regDetailDto.getPickRoadDetailAddr())
            .pickMobileNo(regDetailDto.getPickMobileNo())
            .regId(ObjectUtils.toString(claimData.get("request_id")))
            .chgId(ObjectUtils.toString(claimData.get("request_id")))
            .isAuto(ObjectUtils.toString(pickInfo.get("return_delivery_yn")))
            .isPickProxy(ObjectUtils.toString(pickInfo.get("return_delivery_yn")))
            .pickStatus(ObjectUtils.toString(pickInfo.get("return_delivery_yn")).equalsIgnoreCase("Y") ? "NN" : "N0")
            .safetyUseYn(ObjectUtils.toString(claimData.get("safety_use_yn")))
            .shipMethod(ObjectUtils.toString(pickInfo.get("ship_method")))
            .build();

        if(shipType.equalsIgnoreCase("TD")){
            shippingRegDto.setPickShipType("D");
            shippingRegDto.setShipDt(regDetailDto.getShipDt());
            shippingRegDto.setShiftId(regDetailDto.getShiftId());
            shippingRegDto.setSlotId(regDetailDto.getSlotId());
        } else {
            // DS
            // isAuto 가 Y 면 NN / N 면 N0
            if(regDetailDto.getIsPick().equalsIgnoreCase("Y")) {
                shippingRegDto.setIsAuto("N");
                shippingRegDto.setPickDlvCd(regDetailDto.getPickDlvCd());
                shippingRegDto.setPickShipType("C");
                shippingRegDto.setPickInvoiceNo(regDetailDto.getPickInvoiceNo());
                shippingRegDto.setPickInvoiceRegId(ObjectUtils.toString(claimData.get("user_no")));
                shippingRegDto.setPickStatus("P1");
            } else {
                shippingRegDto.setPickDlvCd(ObjectUtils.toString(pickInfo.get("dlvCd")));
                shippingRegDto.setPickShipType(shipMethod.contains("DRCT") ? "D" : shipMethod.contains("POST") ? "P" : "C");
            }
        }

        // 수거안함
        if(regDetailDto.getIsPickReq().equalsIgnoreCase("N")) {
            shippingRegDto.setPickShipType("N");
            shippingRegDto.setPickStatus("P3");
        }
        return shippingRegDto;
    }

    public ClaimExchShippingRegDto createClaimExchShippingRegDto(ClaimShippingRegDto shippingRegDto, ClaimRegDetailDto regDetailDto) {
        return ClaimExchShippingRegDto.builder()
            .claimPickShippingNo(shippingRegDto.getClaimPickShippingNo())
            .claimBundleNo(shippingRegDto.getClaimBundleNo())
            .exchShipType(getExchPickType(shippingRegDto))
            .exchStatus("D0")
            .exchTrackingYn("N")
            .exchReceiverNm(regDetailDto.getExchReceiverNm())
            .exchZipcode(regDetailDto.getExchZipCode())
            .exchRoadBaseAddr(regDetailDto.getExchRoadBaseAddr())
            .exchRoadDetailAddr(regDetailDto.getExchRoadDetailAddr())
            .exchBaseAddr(regDetailDto.getExchBaseAddr())
            .exchDetailAddr(regDetailDto.getExchBaseDetailAddr())
            .exchMobileNo(regDetailDto.getExchMobileNo())
            .userNo(shippingRegDto.getUserNo())
            .partnerId(shippingRegDto.getPartnerId())
            .regId(shippingRegDto.getRegId())
            .chgId(shippingRegDto.getChgId())
            .safetyUseYn(shippingRegDto.getSafetyUseYn())
            .build();
    }

    public ClaimCouponReIssueSetDto createClaimCouponReIssueDto(List<CouponReIssueDto> issueList, long userNo, long purchaseOrderNo) {
        return ClaimCouponReIssueSetDto.builder()
            .couponReIssueList(issueList)
            .userNo(userNo)
            .purchaseOrderNo(purchaseOrderNo)
            .build();
    }

    public MessageSendTalkSetDto createMessageSendAlimTalkDto(LinkedHashMap<String, Object> claimSendDataMap) {
        MessageSendTalkSetDto returnDto;
//        MessageSendBasicDto basicDto = new MessageSendBasicDto();
        switch (ObjectUtils.toString(claimSendDataMap.get("claim_type"))){
            default:
                returnDto = MessageSendTalkSetDto.builder()
                    .templateCode("T10988")
                    .workName("클레임 취소 완료")
                    .description("반품클레임 취소에 따른 알람톡 발송")
                    .regId("SYSTEM")
                    .bodyArgument(
                        new ArrayList<MessageSendBodyDto>() {{
                            add( MessageSendBodyDto.builder()
                                .identifier("1")
                                .toToken(ObjectUtils.toString(claimSendDataMap.get("ship_mobile_no")))
                                .mappingData(new HashMap<>(){{
                                    put("취소상품내용", ObjectUtils.toString(claimSendDataMap.get("item_name")));
                                    put("취소금액", ObjectUtils.toString(claimSendDataMap.get("payment_amt")));
                                }})
                                .build()
                            );
                        }}
                    )
                    .build();
                break;
        }

//        basicDto.setIdentifier("1");
//        basicDto.setToToken(ObjectUtils.toString(claimSendDataMap.get("ship_mobile_no")));
//        basicDto.setMappingData();
        log.debug("createMessageSendAlimTalkDto ::: {}", returnDto);
        return returnDto;
    }

    public ClaimTicketRegDto createClaimTicketDto(LinkedHashMap<String, Object> ticketInfo) {
        return ClaimTicketRegDto.builder()
            .claimNo(ObjectUtils.toLong(ticketInfo.get("claim_no")))
            .claimBundleNo(ObjectUtils.toLong(ticketInfo.get("claim_bundle_no")))
            .claimItemNo(ObjectUtils.toLong(ticketInfo.get("claim_item_no")))
            .orderItemNo(ObjectUtils.toLong(ticketInfo.get("order_item_no")))
            .orderTicketNo(ObjectUtils.toLong(ticketInfo.get("order_ticket_no")))
            .regDt("NOW()")
            .chgDt("NOW()")
            .build();
    }

    public SaveCancelMhcSetDto createSaveCancelMhcDto(LinkedHashMap<String, Object> pointInfo) {
        return SaveCancelMhcSetDto.builder()
            .userNo(ObjectUtils.toString(pointInfo.get("user_no")))
            .saleDt(ObjectUtils.toString(pointInfo.get("claim_dt")))
            .mhcCardNoEnc(ObjectUtils.toString(pointInfo.get("mhc_card_no_enc")))
            .mhcCardNoHmac(ObjectUtils.toString(pointInfo.get("mhc_card_no_hmac")))
            .claimNo(ObjectUtils.toString(pointInfo.get("claim_no")))
            .orgPurchaseOrderNo(ObjectUtils.toString(pointInfo.get("purchase_order_no")))
            .build();
    }

    public SaveCancelMhcPartSetDto createSaveCancelMhcPartDto(LinkedHashMap<String, Object> pointInfo) {
        return SaveCancelMhcPartSetDto.builder()
            .userNo(ObjectUtils.toString(pointInfo.get("user_no")))
            .saleDt(ObjectUtils.toString(pointInfo.get("claim_dt")))
            .mhcCardNoEnc(ObjectUtils.toString(pointInfo.get("mhc_card_no_enc")))
            .mhcCardNoHmac(ObjectUtils.toString(pointInfo.get("mhc_card_no_hmac")))
            .claimNo(ObjectUtils.toString(pointInfo.get("claim_no")))
            .orgPurchaseOrderNo(ObjectUtils.toString(pointInfo.get("purchase_order_no")))
            .mhcBaseAccAmt(ObjectUtils.toInt(pointInfo.get("claim_base_acc_amt")))
            .mhcAddAccAmt(ObjectUtils.toInt(pointInfo.get("claim_add_acc_amt")))
            .build();
    }


    private String getExchPickType(ClaimShippingRegDto shippingRegDto){
        if(shippingRegDto.getPickShipType().equals("N")){
            // DB 조회후 원배송에 따른 타입으로 설정.
            String[] shipMethod = shippingRegDto.getShipMethod().split("_");
            if("DRCT,PICK".contains(shipMethod[1])){
                return ExchangeShipType.DIRECT.getExchangeShipType();
            } else if ("POST".contains(shipMethod[1])){
                return ExchangeShipType.POST.getExchangeShipType();
            } else {
                return ExchangeShipType.DELIVERY.getExchangeShipType();
            }
        }
        return shippingRegDto.getPickShipType();
    }

    private MhcCardInfoDto getUserMhcCardInfo(long userNo) throws Exception {
        try {
            return externalService.getTransfer(ExternalInfo.USER_MHC_CARD_INFO, new ArrayList<SetParameter>(){{ add(SetParameter.create("userNo", userNo)); }}, MhcCardInfoDto.class);
        } catch (Exception e) {
            throw new LogicException(ClaimResponseCode.CLAIM_PAYMENT_CANCEL_MHC_ERR01);
        }
    }

    /**
     * 전체 취소 여부 체크
     * @param paymentData
     * @return
     */
    private Boolean isTotalCancel(LinkedHashMap<String, Object> paymentData) {
        if(ObjectUtils.toLong(paymentData.get("prev_payment_amt")) > 0) {
            return Boolean.FALSE;
        }
        if(!ObjectUtils.toLong(paymentData.get("payment_amt")).equals(ObjectUtils.toLong(paymentData.get("order_payment_amt")))){
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}
