package kr.co.homeplus.claim.claim.mapper;

import java.util.LinkedHashMap;
import kr.co.homeplus.claim.core.db.annotation.PrimaryConnection;
import kr.co.homeplus.claim.core.db.annotation.SecondaryConnection;
import kr.co.homeplus.claim.claim.sql.ClaimMessageSql;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

@PrimaryConnection
public interface ClaimMessageMapper {
    @SelectProvider(type = ClaimMessageSql.class, method = "selectClaimBasicMessageInfo")
    LinkedHashMap<String, Object> selectClaimBasicMessageInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimMessageSql.class, method = "selectClaimPaymentMessageInfo")
    LinkedHashMap<String, Object> selectClaimPaymentMessageInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimMessageSql.class, method = "selectOrderSubstitutionInfo")
    LinkedHashMap<String, Object> selectOrderSubstitutionInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimMessageSql.class, method = "selectPresentAutoCancelInfo")
    LinkedHashMap<String, Object> selectPresentAutoCancelInfo(@Param("claimNo") long claimNo);
}
