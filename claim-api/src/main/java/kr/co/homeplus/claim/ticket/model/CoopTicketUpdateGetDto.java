package kr.co.homeplus.claim.ticket.model;


import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "쿠프티켓 상태 변경 요청 파라미터")
public class CoopTicketUpdateGetDto {
    private String resultCode;
    private String resultMessage;
}
