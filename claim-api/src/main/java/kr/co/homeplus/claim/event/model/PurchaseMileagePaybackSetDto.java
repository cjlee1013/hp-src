package kr.co.homeplus.claim.event.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "이벤트 결제금액 누계에 따른 마일리지 페이백 요청 DTO")
public class PurchaseMileagePaybackSetDto {
    @ApiModelProperty(value = "조회시작일")
    private String validStartDt;
    @ApiModelProperty(value = "조회종료일")
    private String validEndDt;
    @ApiModelProperty(value = "고객번호")
    private long userNo;
}
