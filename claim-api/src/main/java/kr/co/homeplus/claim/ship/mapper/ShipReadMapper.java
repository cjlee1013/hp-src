package kr.co.homeplus.claim.ship.mapper;

import java.util.List;
import kr.co.homeplus.claim.claim.sql.ClaimSearchSql;
import kr.co.homeplus.claim.core.db.annotation.SecondaryConnection;
import kr.co.homeplus.claim.ship.model.DlvCdGetDto;
import kr.co.homeplus.claim.ship.model.ShipMsgCdGetDto;
import kr.co.homeplus.claim.ship.sql.ShipSearchSql;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;

@SecondaryConnection
public interface ShipReadMapper {

    @ResultType(value = ShipMsgCdGetDto.class)
    @SelectProvider(type = ShipSearchSql.class, method = "selectShipMsg")
    List<ShipMsgCdGetDto> selectShipMsg ();

    @ResultType(value = DlvCdGetDto.class)
    @SelectProvider(type = ShipSearchSql.class, method = "selectDlvCd")
    List<DlvCdGetDto> selectDlvCd ();
}
