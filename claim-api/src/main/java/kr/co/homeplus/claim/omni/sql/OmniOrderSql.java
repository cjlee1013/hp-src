package kr.co.homeplus.claim.omni.sql;

import java.util.LinkedHashMap;
import kr.co.homeplus.claim.constants.CustomConstants;
import kr.co.homeplus.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claim.entity.claim.ClaimOosItemEntity;
import kr.co.homeplus.claim.entity.claim.ClaimOosManageEntity;
import kr.co.homeplus.claim.entity.claim.ClaimOptEntity;
import kr.co.homeplus.claim.entity.claim.ClaimPaymentEntity;
import kr.co.homeplus.claim.entity.claim.ClaimReqEntity;
import kr.co.homeplus.claim.entity.claim.ClaimStoreInfoEntity;
import kr.co.homeplus.claim.entity.dms.ItmMngCodeEntity;
import kr.co.homeplus.claim.entity.order.BundleEntity;
import kr.co.homeplus.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claim.entity.order.OrderOptEntity;
import kr.co.homeplus.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claim.entity.order.PurchaseStoreInfoEntity;
import kr.co.homeplus.claim.entity.ship.ShippingAddrEntity;
import kr.co.homeplus.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claim.enums.MysqlFunction;
import kr.co.homeplus.claim.factory.EntityFactory;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class OmniOrderSql {

    public static String selectOriginOrderListByOmni(@Param("purchaseOrderNo") long purchaseOrderNo ) {
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                orderItem.itemNo,
                SqlUtils.aliasToRow(orderOpt.optQty, orderItem.itemQty),
                orderItem.orderItemNo,
                orderItem.bundleNo,
                orderOpt.orderOptNo,
                shippingItem.userNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL_ZERO,
                    SqlUtils.subTableQuery(
                        new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.SUM, claimItem.claimItemQty))
                            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                            .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                            .WHERE(
                                SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                                SqlUtils.equalColumn(claimItem.orderItemNo, orderItem.orderItemNo),
                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'"))
                            )
                    ),
                    claimItem.claimItemQty
                ),
                orderItem.substitutionYn,
                purchaseOrder.orderType,
                purchaseOrder.marketType
            )
            .FROM(EntityFactory.getTableNameWithAlias(orderItem))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(orderItem.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.orderItemNo), ObjectUtils.toArray(SqlUtils.sqlFunction(
                MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")))))
            .WHERE(
                SqlUtils.equalColumnByInput(orderItem.purchaseOrderNo, purchaseOrderNo)
            )
            .ORDER_BY(SqlUtils.orderByesc(orderItem.itemNo, orderOpt.optQty))
            ;
        log.debug("selectOriginOrderListByOmni :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateClaimSubstitutionResult(LinkedHashMap<String, Object> parameterMap){
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimPayment))
            .SET(
                SqlUtils.equalColumnByInput(claimPayment.substitutionAmt, ObjectUtils.toLong(parameterMap.get("substitution_amt"))),
                SqlUtils.equalColumn(claimPayment.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(claimPayment.chgId, "SYSTEM")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.claimNo, ObjectUtils.toLong(parameterMap.get("claim_no"))),
                SqlUtils.equalColumnByInput(claimPayment.paymentType, ObjectUtils.toString(parameterMap.get("payment_type")))
            );
        log.debug("updateClaimSubstitutionResult \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOriginPurchaseOrderNoFromOrdNo(@Param("ordNo") long ordNo){
        PurchaseStoreInfoEntity purchaseStoreInfo = EntityFactory.createEntityIntoValue(PurchaseStoreInfoEntity.class);
        SQL query = new SQL()
            .SELECT(purchaseStoreInfo.purchaseOrderNo)
            .FROM(EntityFactory.getTableName(purchaseStoreInfo))
            .WHERE(SqlUtils.equalColumnByInput(purchaseStoreInfo.purchaseStoreInfoNo, ordNo));
        log.debug("selectOriginPurchaseOrderNoFromOrdNo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOriginClaimNoFromOrdNo(@Param("ordNo") long ordNo){
        ClaimStoreInfoEntity claimStoreInfo = EntityFactory.createEntityIntoValue(ClaimStoreInfoEntity.class);
        SQL query = new SQL()
            .SELECT(claimStoreInfo.claimNo)
            .FROM(EntityFactory.getTableName(claimStoreInfo))
            .WHERE(SqlUtils.equalColumnByInput(claimStoreInfo.claimStoreInfoNo, ordNo));
        log.debug("selectOriginClaimNoFromOrdNo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimShipInfoByOmni(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo){
        PurchaseStoreInfoEntity purchaseStoreInfo = EntityFactory.createEntityIntoValue(PurchaseStoreInfoEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                purchaseStoreInfo.shipDt,
                purchaseStoreInfo.shiftId,
                purchaseStoreInfo.slotId,
                SqlUtils.aliasToRow(shippingAddr.receiverNm, "pick_receiver_nm"),
                SqlUtils.aliasToRow(shippingAddr.zipcode, "pick_zip_code"),
                SqlUtils.aliasToRow(shippingAddr.roadBaseAddr, "pick_road_base_addr"),
                SqlUtils.aliasToRow(shippingAddr.roadDetailAddr, "pick_road_detail_addr"),
                SqlUtils.aliasToRow(shippingAddr.baseAddr, "pick_base_addr"),
                SqlUtils.aliasToRow(shippingAddr.detailAddr, "pick_base_detail_addr"),
                SqlUtils.aliasToRow(shippingAddr.shipMobileNo, "pick_mobile_no"),
                SqlUtils.aliasToRow(shippingAddr.receiverNm, "exch_receiver_nm"),
                SqlUtils.aliasToRow(shippingAddr.zipcode, "exch_zip_code"),
                SqlUtils.aliasToRow(shippingAddr.roadBaseAddr, "exch_road_base_addr"),
                SqlUtils.aliasToRow(shippingAddr.roadDetailAddr, "exch_road_detail_addr"),
                SqlUtils.aliasToRow(shippingAddr.baseAddr, "exch_base_addr"),
                SqlUtils.aliasToRow(shippingAddr.detailAddr, "exch_base_detail_addr"),
                SqlUtils.aliasToRow(shippingAddr.shipMobileNo, "exch_mobile_no"),
                shippingItem.userNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseStoreInfo))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(purchaseStoreInfo.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseStoreInfo.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo)
            );
        log.debug("selectClaimShipInfoByOmni \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimReasonType(@Param("claimCd") String claimCd){
        ItmMngCodeEntity itmMngCode = EntityFactory.createEntityIntoValue(ItmMngCodeEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                itmMngCode.mcCd,
                itmMngCode.mcNm,
                "imc.ref_1",
                "imc.ref_2",
                "imc.ref_3",
                "imc.ref_4"
            )
            .FROM(EntityFactory.getTableNameWithAlias("dms", itmMngCode))
            .WHERE(
                SqlUtils.equalColumnByInput(itmMngCode.gmcCd, "claim_reason_type"),
                SqlUtils.equalColumnByInput(itmMngCode.mcCd, claimCd)
            );
        log.debug("selectClaimReasonType \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectExpTotalCancelBundleNo(@Param("purchaseOrderNo") long purchaseOrderNo){
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.userNo,
                bundle.bundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectExpTotalCancelBundleNo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderItemInfoExp(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                orderItem.itemNo,
                orderOpt.orderOptNo,
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderOpt.optQty),
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.MAX, claimOpt.claimOptQty))
                    ),
                    "claim_qty"
                ),
                orderItem.orderItemNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(orderItem.purchaseOrderNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo, orderItem.purchaseOrderNo, orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimItemNo, orderOpt.orderOptNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo),
                SqlUtils.equalColumnByInput(orderItem.storeType, "EXP")
            )
            .GROUP_BY(orderItem.itemNo, orderItem.orderItemNo, orderOpt.orderOptNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(orderItem.itemNo),
                SqlUtils.nonAlias(orderOpt.orderOptNo),
                "claim_qty",
                SqlUtils.nonAlias(orderItem.orderItemNo)
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "data"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, "claim_qty", "0")
            );
        log.debug("selectOrderItemInfoExp Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String updateOmniClaimComplete(@Param("claimNo") long claimNo, @Param("claimStatus") String claimStatus) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);

        String updateIdColumn, updateDtColumn, prevClaimStatus;
        switch (claimStatus){
            case "C2" :
                updateDtColumn = claimReq.approveDt;
                updateIdColumn = claimReq.approveId;
                prevClaimStatus = "C1";
                break;
            case "C3" :
                updateDtColumn = claimReq.completeDt;
                updateIdColumn = claimReq.completeId;
                prevClaimStatus = "C2";
                break;
            default:
                updateDtColumn = "";
                updateIdColumn = "";
                prevClaimStatus = "";
                break;
        }

        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableNameWithAlias(ClaimBundleEntity.class))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(claimReq, ObjectUtils.toArray(claimBundle.claimNo))
            )
            .SET(
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, claimStatus),
                SqlUtils.nonSingleQuote(claimBundle.chgDt, "NOW()"),
                SqlUtils.equalColumn(updateDtColumn, "NOW()"),
                SqlUtils.equalColumnByInput(updateIdColumn, "OMNI")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, prevClaimStatus)
            );
        log.debug("updateOmniClaimComplete Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String insertClaimOosManage(LinkedHashMap<String, Object> parameterMap) {
        ClaimOosManageEntity claimOosManage = EntityFactory.createEntityIntoValue(ClaimOosManageEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(claimOosManage))
            .INTO_COLUMNS(
                claimOosManage.purchaseStoreInfoNo, claimOosManage.purchaseOrderNo, claimOosManage.shipDt,
                claimOosManage.shiftId, claimOosManage.slotId, claimOosManage.sordNo, claimOosManage.regDt,
                claimOosManage.chgDt
            )
            .INTO_VALUES(
                SqlUtils.convertInsertIntoValue(
                    ObjectUtils.toArray(
                        parameterMap,
                        "ordNo", "purchaseOrderNo", "deliveryDt", "shiftId", "slotId", "sordNo"
                    )
                )
            )
            .INTO_VALUES(
                "NOW()", "NOW()"
            );
        log.debug("insertClaimOosManage :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String insertClaimOosItem(LinkedHashMap<String, Object> parameterMap) {
        ClaimOosItemEntity claimOosItem = EntityFactory.createEntityIntoValue(ClaimOosItemEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(claimOosItem))
            .INTO_COLUMNS(
                claimOosItem.oosManageSeq, claimOosItem.itemNo, claimOosItem.pickingQty,
                claimOosItem.subItemNo, claimOosItem.subQty, claimOosItem.oosQty, claimOosItem.regDt
            )
            .INTO_VALUES(
                SqlUtils.convertInsertIntoValue(
                    ObjectUtils.toArray(
                        parameterMap,
                        "oosManageSeq", "goodId", "pickingQty", "changeGoodId", "changeQty", "waitingQty"
                    )
                )
            )
            .INTO_VALUES(
                "NOW()"
            );
        log.debug("insertClaimOosItem :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateClaimOosManage(LinkedHashMap<String, Object> updateParamMap) {
        ClaimOosManageEntity claimOosManage = EntityFactory.createEntityIntoValue(ClaimOosManageEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimOosManage))
            .SET(
                SqlUtils.equalColumn(claimOosManage.chgDt, "NOW()")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimOosManage.oosManageSeq, updateParamMap.get("oosManageSeq"))
            );
        if(updateParamMap.get("claimNo") != null){
            query.SET(
                SqlUtils.equalColumnByInput(claimOosManage.claimNo, updateParamMap.get("claimNo"))
            );
        }

        if(updateParamMap.get("subPurchaseOrderNo") != null){
            query.SET(
                SqlUtils.equalColumnByInput(claimOosManage.subPurchaseOrderNo, updateParamMap.get("subPurchaseOrderNo"))
            );
        }

        if(updateParamMap.get("returnCode") != null){
            query.SET(
                SqlUtils.equalColumnByInput(claimOosManage.returnCd, updateParamMap.get("returnCode")),
                SqlUtils.equalColumnByInput(claimOosManage.returnMessage, updateParamMap.get("returnMessage"))
            );
        }
        log.debug("updateClaimOosInfo :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimStatusInfo(@Param("claimNo") long claimNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class);
        SQL query = new SQL()
            .SELECT(
                claimBundle.claimNo,
                claimBundle.claimStatus
            )
            .FROM(EntityFactory.getTableName(claimBundle))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimBundle.orderCategory, "TD")
            );
        log.debug("selectClaimStatusInfo :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOmniShipPresentCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                shippingItem.purchaseOrderNo,
                shippingItem.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shipStatus, shippingItem.shipStatus),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.purchaseStoreInfoNo, bundle.purchaseStoreInfoNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingItem))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo)
            )
            .GROUP_BY(shippingItem.purchaseOrderNo, shippingItem.bundleNo)
            ;
        log.debug("selectOmniShipPresentCheck :: \n[{}]", query.toString());
        return query.toString();
    }
}
