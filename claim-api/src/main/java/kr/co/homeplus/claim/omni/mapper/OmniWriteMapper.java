package kr.co.homeplus.claim.omni.mapper;

import java.util.LinkedHashMap;
import kr.co.homeplus.claim.core.db.annotation.PrimaryConnection;
import kr.co.homeplus.claim.omni.sql.OmniOrderSql;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

@PrimaryConnection
public interface OmniWriteMapper {

    @UpdateProvider(type = OmniOrderSql.class, method = "updateClaimSubstitutionResult")
    int updateClaimSubstitutionResult(LinkedHashMap<String, Object> parameterMap);

    @UpdateProvider(type = OmniOrderSql.class, method = "updateOmniClaimComplete")
    int updateOmniClaimComplete(@Param("claimNo") long claimNo, @Param("claimStatus") String claimStatus);

    @Options(useGeneratedKeys = true, keyProperty="oosManageSeq")
    @InsertProvider(type = OmniOrderSql.class, method = "insertClaimOosManage")
    int insertClaimOosManage(LinkedHashMap<String, Object> parameterMap);

    @InsertProvider(type = OmniOrderSql.class, method = "insertClaimOosItem")
    int insertClaimOosItem(LinkedHashMap<String, Object> parameterMap);

    @UpdateProvider(type = OmniOrderSql.class, method = "updateClaimOosManage")
    int updateClaimOosManage(LinkedHashMap<String, Object> updateParamMap);
}
