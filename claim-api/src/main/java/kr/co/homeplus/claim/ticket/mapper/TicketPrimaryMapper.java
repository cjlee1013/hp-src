package kr.co.homeplus.claim.ticket.mapper;

import kr.co.homeplus.claim.core.db.annotation.PrimaryConnection;
import kr.co.homeplus.claim.ticket.model.CoopTicketUpdateSetDto;
import kr.co.homeplus.claim.ticket.model.OrderTicketHistorySetDto;

@PrimaryConnection
public interface TicketPrimaryMapper {
    int updateOrderTicketStatus(CoopTicketUpdateSetDto coopTicketUpdateSetDto);

    int insertTicketHistory(OrderTicketHistorySetDto orderTicketHistorySetDto);
}
