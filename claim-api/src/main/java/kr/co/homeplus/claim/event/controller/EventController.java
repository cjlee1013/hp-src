package kr.co.homeplus.claim.event.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.claim.event.model.PayCompleteByAmtCheckGetDto;
import kr.co.homeplus.claim.event.model.PayCompleteByAmtCheckSetDto;
import kr.co.homeplus.claim.event.model.PurchaseByShipCompleteGetDto;
import kr.co.homeplus.claim.event.model.PurchaseByShipCompleteSetDto;
import kr.co.homeplus.claim.event.model.PurchaseEventCheckDto;
import kr.co.homeplus.claim.event.model.PurchaseEventCheckSetDto;
import kr.co.homeplus.claim.event.model.PurchaseMileagePaybackGetDto;
import kr.co.homeplus.claim.event.model.PurchaseMileagePaybackSetDto;
import kr.co.homeplus.claim.event.model.ShipCompleteByAmtCheckGetDto;
import kr.co.homeplus.claim.event.model.ShipCompleteByAmtCheckSetDto;
import kr.co.homeplus.claim.event.service.EventService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/event")
@RequiredArgsConstructor
@Slf4j
@Api(tags = "이벤트 관련 API")
public class EventController {
    private final EventService eventService;

    @PostMapping("/validPurchaseOrderNo")
    @ApiOperation(value = "이벤트 주문체크", response = PurchaseEventCheckDto.class)
    public ResponseObject<PurchaseEventCheckDto> getValidPurchaseOrderNo(@RequestBody PurchaseEventCheckSetDto setDto) throws Exception {
        return eventService.getPurchaseEventCheck(setDto);
    }

    @PostMapping("/purchaseMileagePayback")
    @ApiOperation(value = "이벤트 결제금 누계액에 따른 마일리지 지급", response = PurchaseEventCheckDto.class)
    public ResponseObject<PurchaseMileagePaybackGetDto> getPurchaseMileagePayback(@RequestBody PurchaseMileagePaybackSetDto setDto) throws Exception {
        return eventService.getEventMileagePayback(setDto);
    }

    @PostMapping("/shipCompleteEvent")
    @ApiOperation(value = "이벤트 기간별 배송완료 건수", response = ShipCompleteByAmtCheckGetDto.class)
    public ResponseObject<ShipCompleteByAmtCheckGetDto> getEventShipComplete(@RequestBody ShipCompleteByAmtCheckSetDto setDto) throws Exception {
        return eventService.getEventShipComplete(setDto);
    }

    @PostMapping("/shipCompleteDayLimitEvent")
    @ApiOperation(value = "이벤트 기간별 배송완료 일제한 건수", response = ShipCompleteByAmtCheckGetDto.class)
    public ResponseObject<ShipCompleteByAmtCheckGetDto> getEventShipCompleteDayLimit(@RequestBody ShipCompleteByAmtCheckSetDto setDto) throws Exception {
        return eventService.getEventShipCompleteDayLimit(setDto);
    }

    @PostMapping("/purchaseByShipComplete")
    @ApiOperation(value = "구매리스트(배송완료)", response = PurchaseByShipCompleteGetDto.class)
    public ResponseObject<List<PurchaseByShipCompleteGetDto>> getNotStoreKindPurchaseByShipComplete(@RequestBody PurchaseByShipCompleteSetDto setDto) throws Exception {
        return eventService.getNotStoreKindPurchaseByShipComplete(setDto);
    }

    @PostMapping("/payCompleteEvent")
    @ApiOperation(value = "이벤트 기간별 주문완료 건수", response = PayCompleteByAmtCheckGetDto.class)
    public ResponseObject<PayCompleteByAmtCheckGetDto> getEventPayComplete(@RequestBody PayCompleteByAmtCheckSetDto setDto) throws Exception {
        return eventService.getEventPayComplete(setDto);
    }

    @PostMapping("/payCompleteDayLimitEvent")
    @ApiOperation(value = "이벤트 기간별 주문완료 일제한 건수", response = PayCompleteByAmtCheckGetDto.class)
    public ResponseObject<PayCompleteByAmtCheckGetDto> getEventPayCompleteDayLimit(@RequestBody PayCompleteByAmtCheckSetDto setDto) throws Exception {
        return eventService.getEventPayCompleteDayLimit(setDto);
    }
}
