package kr.co.homeplus.claim.entity.claim;

public class ClaimReqEntity {
    // 쿨래암 요청번호
    public String claimReqNo;
    // 클레임 묶음 배송번호
    public String claimBundleNo;
    // 클레임번호(환불:클레임번호)
    public String claimNo;
    // 클레임사유코드(claim_code.claim_depth2 = request )
    public String claimReasonType;
    // 클레임사유상세
    public String claimReasonDetail;
    // 클레임 보류 사유(claim_code.claim_depth2 = pending)
    public String pendingReasonType;
    // 클레임 보류 사유 상세
    public String pendingReasonDetail;
    // 클레임 거절 사유(claim_code.claim_depth2 = reject)
    public String rejectReasonType;
    // 클레임 거절 사유 상세
    public String rejectReasonDetail;
    // 클레임 요청자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)
    public String requestId;
    // 클레임 승인자 정보(mypage : user_no 참조하여 구매자정보 노출, \npartner : partner_id 참조하여 파트너 정보 노출, \nbatch : 배치작업, \n그외에는 사번으로 인식하여 사원정보 노출)
    public String approveId;
    // 클레임 보류자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)
    public String pendingId;
    // 클레임 철회 요청자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)
    public String withdrawId;
    // 클레임 거부자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)
    public String rejectId;
    // 클레임 완료자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)
    public String completeId;
    // 요청일자
    public String requestDt;
    // 승인일자
    public String approveDt;
    // 보류일자
    public String pendingDt;
    // 철회일자
    public String withdrawDt;
    // 거부일자
    public String rejectDt;
    // 완료일자
    public String completeDt;
    // 첨부파일
    public String uploadFileName;
    // 첨부파일명2
    public String uploadFileName2;
    // 첨부파일명3
    public String uploadFileName3;
    // 등록일
    public String regDt;
    // 수정일
    public String chgDt;
    // 행사미차감여부
    public String deductPromoYn;
    // 할인미차감여부
    public String deductDiscountYn;
    // 배송비미차감여부
    public String deductShipYn;
}
