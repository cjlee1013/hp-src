package kr.co.homeplus.claim.core.config;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import kr.co.homeplus.plus.api.support.client.ReconfigurableHttpRequestFactory;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceRouteConfigure;
import lombok.RequiredArgsConstructor;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * plus-web ResourceClient 를 사용하기 위한 설정.
 *
 * application.yml 에 아래와 같이 추가 필요.
 *
 * plus.resource-routes:
    product:
        url: http://10.200.208.106:8080

 * product 이 ID가 되어서 호출 시 apiId 가 되며,
 * url 은 ResourceRoute 의 인자로 mapping 되어 사용한다.
 */
@Configuration
@RequiredArgsConstructor
public class ClientConfig {

    private static final int DEFAULT_MAX_CONNECTION = 200;
    private static final int DEFAULT_MAX_CONNECTION_PER_ROUTE = DEFAULT_MAX_CONNECTION / 20;

    private final ResourceRouteConfigure resourceRouteConfigure;

    @Bean
    @Qualifier("reconfigurableHttpRequestFactorySsoIgnoreSsl")
    public ClientHttpRequestFactory reconfigurableHttpRequestFactorySsoIgnoreSsl()
        throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        SSLConnectionSocketFactory scsf = new SSLConnectionSocketFactory(
            SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build(),
            NoopHostnameVerifier.INSTANCE);

        final HttpClient httpClient = HttpClients.custom()
            .setMaxConnTotal(DEFAULT_MAX_CONNECTION)
            .setMaxConnPerRoute(DEFAULT_MAX_CONNECTION_PER_ROUTE)
            .setSSLSocketFactory(scsf)
            .disableRedirectHandling()
            .build();

        return new ReconfigurableHttpRequestFactory(httpClient);
    }

    @Bean
    @Qualifier("restTemplateSsoIgnoreSsl")
    public RestTemplate restTemplateSsoIgnoreSsl()
        throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        return new RestTemplate(reconfigurableHttpRequestFactorySsoIgnoreSsl());
    }

    @Bean
    @Qualifier("resourceClientSsoIgnoreSsl")
    public ResourceClient resourceClientSsoIgnoreSsl()
        throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        return new ResourceClient(reconfigurableHttpRequestFactorySsoIgnoreSsl(),
            resourceRouteConfigure,
            restTemplateSsoIgnoreSsl());
    }
}
