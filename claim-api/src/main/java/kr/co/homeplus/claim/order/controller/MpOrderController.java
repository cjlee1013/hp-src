package kr.co.homeplus.claim.order.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.claim.order.model.inquiry.OrderInquiryGetDto;
import kr.co.homeplus.claim.order.model.inquiry.OrderInquirySetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderProductListGetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderDetailInfoGetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderDetailInfoSetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderDetailListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderDetailListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderDetailListSetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListSetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPickupStoreInfoDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderTicketDetailGetDto;
import kr.co.homeplus.claim.order.model.mypage.MultiOrderShipAddrEditDto;
import kr.co.homeplus.claim.order.model.mypage.MultiOrderShipBasicInfoEditDto;
import kr.co.homeplus.claim.order.model.mypage.OrderShipAddrEditDto;
import kr.co.homeplus.claim.order.model.subMain.MpOrderMainDto;
import kr.co.homeplus.claim.order.model.subMain.MpOrderMainDto.MyPageMainOrderCount;
import kr.co.homeplus.claim.order.model.subMain.MpOrderMainDto.MyPageMainOrderList;
import kr.co.homeplus.claim.order.model.user.CustomerGradeInfoGetDto;
import kr.co.homeplus.claim.order.model.user.CustomerGradeInfoSetDto;
import kr.co.homeplus.claim.order.model.user.GradeOrderInfoDto;
import kr.co.homeplus.claim.order.model.user.UserGradeGetDto;
import kr.co.homeplus.claim.order.service.MpOrderService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/mp/order")
@Api(tags = "마이페이지 > 주문/배송 ")
public class MpOrderController {

    private final MpOrderService orderService;

    @PostMapping("/getOrderList")
    @ApiOperation(value = "주문내역조회", response = MpOrderListGetDto.class)
    public ResponseObject<List<MpOrderListGetDto>> getMyPageOrderList(@RequestBody MpOrderListSetDto setDto) throws Exception {
        return orderService.getMyPageOrderList(setDto);
    }

    @PostMapping("/getOrderDetailList")
    @ApiOperation(value = "주문내역상세조회", response = MpOrderDetailListGetDto.class)
    public ResponseObject<MpOrderDetailListGetDto> getMyPageOrderDetailInfo(@RequestBody MpOrderDetailListSetDto setDto) throws Exception {
        return orderService.getMyPageOrderDetailInfo(setDto);
    }

    @PostMapping("/getMultiOrderDetailList")
    @ApiOperation(value = "다중배송배송리스트조회", response = MpMultiOrderDetailListGetDto.class)
    public ResponseObject<MpMultiOrderDetailListGetDto> getMyPageMultiOrderDetailList(@RequestBody MpOrderDetailListSetDto setDto) throws Exception {
        return orderService.getMyPageMultiOrderDetailList(setDto);
    }

    @PostMapping("/getMultiOrderDetailInfo")
    @ApiOperation(value = "다중배송번호주문상세조회", response = MpMultiOrderDetailInfoGetDto.class)
    public ResponseObject<MpMultiOrderDetailInfoGetDto> getMyPageMultiOrderDetailInfo(@RequestBody MpMultiOrderDetailInfoSetDto setDto) throws Exception {
        return orderService.getMyPageMultiOrderDetailItemInfo(setDto);
    }

    @PostMapping("/getMultiOrderItemList")
    @ApiOperation(value = "다중배송아이템그룹조회", response = MpMultiOrderDetailInfoGetDto.class)
    public ResponseObject<List<MpOrderProductListGetDto>> getMyPageMultiOrderItemList(@RequestBody MpOrderDetailListSetDto setDto) throws Exception {
        return orderService.getMypageMultiOrderItemList(setDto);
    }

    @GetMapping("/getMainOrderInfo")
    @ApiOperation(value = "마이페이지메인-주문건수/리스트조회", response = MpOrderMainDto.class)
    public ResponseObject<MpOrderMainDto> getMainOrderInfo(@RequestParam("userNo") long userNo, @RequestParam("siteType") String siteType) throws Exception {
        return orderService.getMyPageMainOrderInfo(userNo, siteType);
    }

    @GetMapping("/getMainOrderCnt")
    @ApiOperation(value = "마이페이지메인-주문건수조회", response = MyPageMainOrderCount.class)
    public ResponseObject<List<MyPageMainOrderCount>> getMainOrderCnt(@RequestParam("userNo") long userNo, @RequestParam("siteType") String siteType) throws Exception {
        return orderService.getMainOrderCnt(userNo, siteType);
    }

    @GetMapping("/getMainOrderList")
    @ApiOperation(value = "마이페이지메인-리스트조회", response = MyPageMainOrderList.class)
    public ResponseObject<List<MyPageMainOrderList>> getMainOrderList(@RequestParam("userNo") long userNo, @RequestParam("siteType") String siteType) throws Exception {
        return orderService.getMainOrderList(userNo, siteType);
    }

    @PostMapping("/modifyOrderShipChange")
    @ApiOperation(value = "마이페이지-주문/배송지변경", response = String.class)
    public ResponseObject<String> modifyShipAddrChange(@RequestBody OrderShipAddrEditDto shipAddrEditDto) throws Exception {
        return orderService.modifyShipAddrChange(shipAddrEditDto);
    }

    @GetMapping("/getPickupStoreInfo/{purchaseOrderNo}")
    @ApiOperation(value = "픽업시점포정보조회", response = MpOrderPickupStoreInfoDto.class)
    public ResponseObject<MpOrderPickupStoreInfoDto> getOrderPickupStoreInfo(@PathVariable("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        return orderService.getOrderPickupStoreInfo(purchaseOrderNo);
    }

    @PostMapping("/getOrderTicketDetailInfo")
    @ApiOperation(value = "티켓주문내역상세조회", response = MpOrderTicketDetailGetDto.class)
    public ResponseObject<MpOrderTicketDetailGetDto> getMyPageOrderTicketDetailInfo(@RequestBody MpOrderDetailListSetDto setDto) throws Exception {
        return orderService.getOrderTicketDetailInfo(setDto);
    }

    @GetMapping("/getUserGradeOrderInfo")
    @ApiOperation(value = "월별주문정보(등급)", response = GradeOrderInfoDto.class)
    public ResponseObject<GradeOrderInfoDto> getUserGradeOrderInfo(@RequestParam("userNo") long userNo, @RequestParam("siteType") String siteType) throws Exception {
        return orderService.getUserGradeOrderInfo(userNo, siteType);
    }

    @GetMapping("/getCustomerGradeOrderInfo")
    @ApiOperation(value = "월별주문정보-고객센터", response = CustomerGradeInfoGetDto.class)
    public ResponseObject<CustomerGradeInfoGetDto> getCustomerGradeOrderInfo(@ModelAttribute CustomerGradeInfoSetDto setDto) throws Exception {
        return orderService.getCustomerGradeOrderInfo(setDto);
    }

    @GetMapping("/getCustomerGradeInfo")
    @ApiOperation(value = "등급정보-고객센터", response = UserGradeGetDto.class)
    public ResponseObject<UserGradeGetDto> getCustomerGradeInfo(@RequestParam("userNo") long userNo) throws Exception {
        return orderService.getCustomerGradeInfo(userNo);
    }

    @PostMapping("/getInquiryOrderList")
    @ApiOperation(value = "1:1문의-주문상품리스트", response = OrderInquiryGetDto.class)
    public ResponseObject<List<OrderInquiryGetDto>> getInquiryOrderList(@RequestBody OrderInquirySetDto setDto) throws Exception {
        return orderService.getInquiryOrderItemList(setDto);
    }

    @PostMapping("/modifyMultiOrderShipInfo")
    @ApiOperation(value = "마이페이지-다중배송주문 주소지변경", response = String.class)
    public ResponseObject<String> modifyMultiShipAddrChange(@RequestBody MultiOrderShipAddrEditDto multiOrderShipAddrEditDto) throws Exception {
        return orderService.modifyMultiShipAddrChange(multiOrderShipAddrEditDto);
    }

    @PostMapping("/modifyMultiOrderShipBasicInfo")
    @ApiOperation(value = "마이페이지-다중배송주문 공통정보 변경(예약일,선물메시지)", response = String.class)
    public ResponseObject<String> modifyMultiShipBasicInfoChange(@RequestBody MultiOrderShipBasicInfoEditDto multiOrderShipBasicInfoEditDto) throws Exception {
        return orderService.modifyMultiShipBasicInfoChange(multiOrderShipBasicInfoEditDto);
    }
}
