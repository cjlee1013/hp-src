package kr.co.homeplus.claim.claim.mapper;

import java.util.LinkedHashMap;
import java.util.List;

import kr.co.homeplus.claim.claim.model.external.safety.ClaimSafetySetDto;
import kr.co.homeplus.claim.claim.model.register.ClaimBundleRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimExchShippingRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimInfoRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimItemRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimMultiBundleRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimMultiItemRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimPaymentRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimProcessHistoryRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimRefundRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimReqRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimShippingRegDto;
import kr.co.homeplus.claim.claim.model.register.ticket.ClaimTicketModifyDto;
import kr.co.homeplus.claim.claim.model.register.ticket.ClaimTicketRegDto;
import kr.co.homeplus.claim.claim.sql.ClaimManagementSql;
import kr.co.homeplus.claim.claim.sql.ClaimOrderCancelSql;
import kr.co.homeplus.claim.claim.sql.ClaimOrderTicketSql;
import kr.co.homeplus.claim.core.db.annotation.PrimaryConnection;
import kr.co.homeplus.claim.enums.ClaimCode;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.mapping.StatementType;

@PrimaryConnection
public interface ClaimWriteMapper {

    @Options(useGeneratedKeys = true, keyProperty="claimNo")
    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimMst")
    int insertClaimMst(ClaimMstRegDto claimMstRegDto);

    @Options(useGeneratedKeys = true, keyProperty="claimBundleNo")
    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimBundle")
    int insertClaimBundle(ClaimBundleRegDto claimBundleRegDto);

    @Options(useGeneratedKeys = true, keyProperty="claimMultiBundleNo")
    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimMultiBundle")
    int insertClaimMultiBundle(ClaimMultiBundleRegDto claimMultiBundleRegDto);

    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimReq")
    int insertClaimReq(ClaimReqRegDto claimReqRegDto);

    @Options(useGeneratedKeys = true, keyProperty="claimItemNo")
    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimItemReq")
    int insertClaimItemReq(ClaimItemRegDto claimItemRegDto);

    @Options(useGeneratedKeys = true, keyProperty="claimItemNo")
    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimMultiItem")
    int insertClaimMultiItem(ClaimMultiItemRegDto claimMultiItemRegDto);

    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimPayment")
    int insertClaimPayment(ClaimPaymentRegDto claimPaymentRegDto);

    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimOpt")
    int insertClaimOpt(ClaimOptRegDto claimOptRegDto);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectPaymentCancelInfo")
    List<LinkedHashMap<String, Object>> selectPaymentCancelInfo(@Param("claimNo") long claimNo);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimStatus")
    int updateClaimStatus(LinkedHashMap<String, Object> parameter);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimPaymentStatus")
    int updateClaimPaymentStatus(@Param("claimPaymentNo") long claimPaymentNo, @Param("claimApprovalCd") String claimApprovalCd, ClaimCode claimCode);

    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimProcessHistory")
    int insertClaimProcessHistory(ClaimProcessHistoryRegDto claimProcessHistoryRegDto);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimManagementSql.class, method = "callByEndClaimFlag")
    LinkedHashMap<String, Object> callByClaimReg(ClaimInfoRegDto infoRegDto);

    @Options(useGeneratedKeys = true, keyProperty="claimPickShippingNo")
    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimPickShipping")
    int insertClaimPickShipping(ClaimShippingRegDto shippingRegDto);

    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimExchShipping")
    int insertClaimExchShipping(ClaimExchShippingRegDto exchShippingRegDto);

    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimRefund")
    int insertClaimRefund(ClaimRefundRegDto claimRefundRegDto);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimManagementSql.class, method = "callByClaimAdditionReg")
    LinkedHashMap<String, Object> callByClaimAdditionReg(@Param("claimNo") long claimNo);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimManagementSql.class, method = "callByClaimItemEmpDiscount")
    void callByClaimItemEmpDiscount(@Param("claimNo")long claimNo, @Param("orderItemNo") long orderItemNo);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimCouponReIssueResult")
    int updateClaimCouponReIssueResult(List<Long> claimAdditionNo);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimBundleSlotResult")
    int updateClaimBundleSlotResult(@Param("claimBundleNo") long claimBundleNo);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimPaymentOcbTransactionId")
    int updateClaimPaymentOcbTransactionId(@Param("claimPaymentNo") long claimPaymentNo, @Param("pgMid") String pgMid);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimPaymentPgCancelTradeId")
    int updateClaimPaymentPgCancelTradeId(@Param("claimPaymentNo") long claimPaymentNo, @Param("cancelTradeId") String cancelTradeId);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimSafetyIssueData")
    ClaimSafetySetDto selectClaimSafetyIssueData(@Param("claimBundleNo") long claimBundleNo, @Param("issueType") String issueType);

    @Options(useGeneratedKeys = true, keyProperty="claimTicketNo")
    @InsertProvider(type = ClaimOrderTicketSql.class, method = "insertClaimTicket")
    int insertClaimTicket(ClaimTicketRegDto claimTicketRegDto);

    @UpdateProvider(type = ClaimOrderTicketSql.class, method = "updateOrderTicketStatus")
    int updateOrderTicketStatus(ClaimTicketModifyDto modifyDto);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateTotalClaimPaymentStatus")
    int updateTotalClaimPaymentStatus(@Param("claimNo") long claimNo, ClaimCode claimCode);

    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimAccumulate")
    int insertClaimAccumulate(LinkedHashMap<String, Object> parameterMap);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimAccumulateReqSend")
    int updateClaimAccumulateReqSend(@Param("claimNo") long claimNo, @Param("reqSendYn") String reqSendYn, @Param("pointKind") String pointKind);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimAccumulate")
    int updateClaimAccumulate(LinkedHashMap<String, Object> parameterMap);

    @UpdateProvider(type = ClaimOrderCancelSql.class, method = "updateMileagePaybackResult")
    int updateMileagePaybackResult(@Param("paybackNo") long paybackNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimMultiItemCreateInfo")
    List<ClaimMultiItemRegDto> selectClaimMultiItemCreateInfo(@Param("claimNo") long claimNo, @Param("multiBundleNo") long multiBundleNo);
}
