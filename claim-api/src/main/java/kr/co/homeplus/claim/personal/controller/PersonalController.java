package kr.co.homeplus.claim.personal.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.claim.personal.model.PersonalGetDto;
import kr.co.homeplus.claim.personal.model.PersonalSetDto;
import kr.co.homeplus.claim.personal.service.PersonalService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/mp/customer")
@Api(tags = "마이페이지 > 개인정보 이용내역 > 제3자 제공내역 ")
public class PersonalController {

    private final PersonalService personalService;

    @PostMapping("/getUsePersonalInfo")
    @ApiOperation(value = "주문내역조회", response = PersonalGetDto.class)
    public ResponseObject<List<PersonalGetDto>> getUsePersonalInfo(@RequestBody PersonalSetDto setDto) throws Exception {
        return personalService.getPersonalUsingInfo(setDto);
    }
}
