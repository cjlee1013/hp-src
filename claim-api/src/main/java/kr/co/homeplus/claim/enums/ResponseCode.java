package kr.co.homeplus.claim.enums;

import kr.co.homeplus.claim.enums.impl.EnumImpl;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ResponseCode implements EnumImpl {

    // 오류
    // 9000
    SUCCESS("0000", "성공하였습니다."),
    SUCCESS_CODE("SUCCESS", "성공하였습니다."),
    // 주문
    // 2000 ~ 4000
    ORDER_SEARCH_ERR_01("2101", "조회에 실패 하였습니다."),
    ORDER_SEARCH_DETAIL_ERR_01("2301", "상세조회에 실패 하였습니다."),
    ORDER_SEARCH_DETAIL_ERR_02("2302", "다중배송 주문은 조회할 수 없습니다."),
    ORDER_SEARCH_DETAIL_ERR_03("2303", "일반주문은 조회할 수 없습니다."),

    ORDER_SHIP_ADDR_CHANGE_ERR_01("2401", "배송지 변경에 실패하였습니다."),
    ORDER_SHIP_ADDR_CHANGE_ERR_02("2402", "배송정보 변경가능 시간이 초과되었습니다."),
    ORDER_SHIP_ADDR_CHANGE_ERR_03("2403", "주소지변경 대상이 아닙니다."),
    ORDER_SHIP_ADDR_CHANGE_ERR_04("2404", "안심번호 변경 실패입니다.\n잠시후 재시도 부탁드립니다."),
    ORDER_SHIP_ADDR_CHANGE_ERR_05("2405", "주소지 변경대상이 없습니다.\r관리자에게 문의 부탁드립니다."),
    ORDER_SHIP_ADDR_CHANGE_ERR_06("2406", "30자 이내로 작성해주세요"),
    // 클레임
    // 5000 ~ 8000
    CLAIM_VALID_ERROR("5001", "유효성 검사 실패하였습니다."),
    CLAIM_VALID_ERROR_USERNO("5002", "유효성 검사 실패 [userNo : 회원번호]가 없습니다. "),
    CLAIM_VALID_ERROR_CLAIMTYPE("5003", "유효성 검사 실패 [claimType : 클레임타입]이 없습니다. "),
    CLAIM_REG_SUCCESS("0000", "클레임 등록에 성공하였습니다."),
    CLAIM_REG_ERR05("5005", "주문 내역을 확인할 수 없습니다."),

    CLAIM_DETAIL_SEARCH_ERR01("5101", "조회된 클레임 상세정보 없습니다."),

    CLAIM_NO_RCV_ERR01("6101", "미수취 정보 수정 중 오류가 발생하였습니다."),
    CLAIM_NO_RCV_ERR02("6102", "미수취 정보 등록 중 오류가 발생하였습니다."),

    CLAIM_SUBSTITUTION_ERR01("7011", "대체주문 생성할 주문번호가 없습니다."),
    CLAIM_SUBSTITUTION_ERR02("7012", "대체주문 생성할 수량이 없습니다."),
    CLAIM_SUBSTITUTION_ERR03("7013", "기존 주문이 없는 요청 건 입니다."),
    CLAIM_SUBSTITUTION_ERR04("7014", "결품/대체 수량이 없습니다."),
    CLAIM_SUBSTITUTION_ERR05("7015", "결품 결제취소중 오류가 발생했습니다."),
    CLAIM_SUBSTITUTION_ERR06("7016", "대체 결제취소중 오류가 발생했습니다."),
    CLAIM_SUBSTITUTION_ERR07("7017", "(itemNo)에 취소할 수량이 존재하지 않습니다."),
    CLAIM_SUBSTITUTION_ERR08("7018", "요청하신 주문에 (itemNo)는 없는 상품입니다."),
    CLAIM_SUBSTITUTION_ERR09("7019", "중복된 아이템요청(itemNo) 건 입니다."),
    CLAIM_SUBSTITUTION_ERR10("7020", "대체안함 상품으로 대체요청(itemNo)할 수 없습니다."),
    CLAIM_SUBSTITUTION_ERR11("7021", "(itemNo)은 전체수량만 취소가능합니다."),

    CLAIM_REFUND_CHG_SUCCESS("0000", "환불수단 변경이 변경됐습니다."),
    CLAIM_REFUND_CHG_ERR01("5201", "이미 등록된 환불수단이 있습니다."),
    CLAIM_REFUND_CHG_ERR02("5202", "환불수단 변경 가능한 결제정보가 없습니다."),
    CLAIM_REFUND_CHG_ERR03("5203", "환불정보 등록 중 오류가 발생하였습니다."),

    TICKET_STATUS_CHG_ERR01("3201", "조회된 티켓정보가 없습니다."),
    TICKET_STATUS_CHG_ERR02("3202", "티켓상태변경에 실패하였습니다."),
    TICKET_STATUS_CHG_ERR03("3203", "티켓변경 불가능한 상태값입니다."),
    TICKET_STATUS_CHG_ERR04("3204", "이미 상태변경 된 티켓입니다.")
    ;




    private final String code;
    private final String description;

    @Override
    public String getResponseCode() {
        return this.code;
    }

    @Override
    public String getResponseMessage() {
        return this.description;
    }

}
