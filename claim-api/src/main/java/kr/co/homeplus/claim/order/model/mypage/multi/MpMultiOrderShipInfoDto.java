package kr.co.homeplus.claim.order.model.mypage.multi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마이페이지 > 주문/배송 > 상세 > 배송정보")
public class MpMultiOrderShipInfoDto {
    @ApiModelProperty(value = "받는분", position = 1)
    private String receiverNm;
    @ApiModelProperty(value = "멀티번들번호", position = 2)
    private long multiBundleNo;
    @ApiModelProperty(value = "받는사람전화번호", position = 3)
    private String shipMobileNo;
    @ApiModelProperty(value = "우편번호", position = 4)
    private String zipCode;
    @ApiModelProperty(value = "도로명주소", position = 5)
    private String roadBaseAddr;
    @ApiModelProperty(value = "도로명주소상세", position = 6)
    private String roadDetailAddr;
    @ApiModelProperty(value = "안심번호구분", position = 7)
    private String safetyNm;
    @ApiModelProperty(value = "안심번호신청여부", position = 8)
    private String safetyUseYn;
    @ApiModelProperty(value = "고객 주소지 배송가능지역 유형", position = 16)
    private String shipAreaType;
    @ApiModelProperty(value = "배송기사명", position = 24)
    private String driverNm;
    @ApiModelProperty(value = "배송기사연락처", position = 25)
    private String driverTelNo;
    @ApiModelProperty(value = "예약배송방법(RESERVE_DRCT_DLV:명절선물세트,RESERVE_TD_DRCT:절임배추,RESERVE_DRCT_NOR/RESERVE_DS_DLV:일반상품)", position = 26)
    private String reserveShipMethod;

    private void setSafetyNm(String safetyNm){
        boolean isNumeric =  safetyNm.matches("[+-]?\\d*(\\.\\d+)?");
        if(isNumeric){
            this.safetyNm = safetyNm.replaceFirst("(^[0-9]{4})([0-9]{4})([0-9]{4})$", "$1-$2-$3");
        } else {
            this.safetyNm = safetyNm;
        }
    }


}
