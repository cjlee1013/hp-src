package kr.co.homeplus.claim.order.model.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "주문리스트 - 번들별 주문정보")
public class MpOrderBundleShipInfoDto {
    @ApiModelProperty(value = "배송상태", position = 1)
    private String shipStatus;
    @ApiModelProperty(value = "배송방법", position = 2)
    private String shipMethod;
    @ApiModelProperty(value = "배송일", position = 3)
    private String shipDt;
    @ApiModelProperty(value = "배송시작일(송장입력,원배송)", position = 4)
    private String shippingDt;
    @ApiModelProperty(value = "배송완료일", position = 5)
    private String completeDt;
    @ApiModelProperty(value = "배송시작시간", position = 6)
    private String shipStartTime;
    @ApiModelProperty(value = "배송종료시간", position = 7)
    private String shipEndTime;
    @ApiModelProperty(value = "배송주문마감시간", position = 8)
    private String shipCloseTime;
    @ApiModelProperty(value = "지연배송코드", position = 9)
    private String delayShipCd;
    @ApiModelProperty(value = "지연배송메시지", position = 10)
    private String delayShipMsg;
    @ApiModelProperty(value = "착불여부", position = 11)
    private String prepaymentYn;
    @ApiModelProperty(value = "합배송가능요일및시간", position = 12)
    private String cmcnPossibleTime;
    @ApiModelProperty(value = "미수취신고여부", position = 13)
    private String noRcvYn;
    @ApiModelProperty(value = "애니타임슬롯여부", position = 16)
    private String anytimeSlotYn;
    @JsonIgnore
    @ApiModelProperty(value = "배송완료등록", position = 14)
    private String completeRegId;
    @JsonIgnore
    @ApiModelProperty(value = "주문일시", position = 15)
    private String orderDt;
}
