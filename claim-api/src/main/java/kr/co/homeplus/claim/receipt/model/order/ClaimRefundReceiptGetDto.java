package kr.co.homeplus.claim.receipt.model.order;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClaimRefundReceiptGetDto {
    private String cancelCompleteDt;
    private List<ClaimRefundDetailReceiptDto> cancelPaymentDetail;
    private Long cancelCompleteAmt;
    private Long purchaseOrderNo;
    private Long claimNo;

    @Data
    @Builder
    public static class ClaimRefundDetailReceiptDto {
        private String paymentType;
        private Long paymentAmt;
    }
}
