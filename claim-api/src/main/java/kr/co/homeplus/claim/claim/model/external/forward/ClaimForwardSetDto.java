package kr.co.homeplus.claim.claim.model.external.forward;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpMethod;

@Data
@Builder
public class ClaimForwardSetDto {
    private String transactionId;
    private String transactionUrl;
    private HttpMethod httpMethod;
    private Object data;
}
