package kr.co.homeplus.claim.entity.order;

public class PaymentInfoEntity {
    // 결제정보순번
    public String paymentInfoSeq;
    // 결제번호
    public String paymentNo;
    // 결제유형(PG:PG결제,MILEAGE:마일리지,MHC:MHC,DGV:상품권,OCB:OKCashBag)
    public String paymentType;
    // 상위결제수단코드(CARD:카드,PHONE:휴대폰결제,RBANK:실시간계좌이체,KAKAOPAY:카카오페이,NAVERPAY:네이버페이,SAMSUNGPAY:삼성페이)
    public String parentMethodCd;
    // 결제수단코드
    public String methodCd;
    // 간편결제수단코드(간편결제시 사용한 결제수단 CARD:카드,RBANK:계좌이체 등)
    public String easyMethodCd;
    // 결제상태(P1:결제요청,P3:결제완료(입금확인),P4:결제실패,P5:가상결제(Pine전송용 PG결제건 아님))
    public String paymentStatus;
    // 결제금액
    public String amt;
    // PG종류(INICIS,KICC)
    public String pgKind;
    // PG 상점ID
    public String pgMid;
    // PG 인증Key값
    public String pgOid;
    // 카드번호
    public String cardNo;
    // 카드Prefix번호 (신용카드결제일때만)
    public String cardPrefixNo;
    // 카드할부개월(신용카드 할부개월수 1:일시불,2:2개월,3:3개월…)
    public String cardInstallmentMonth;
    // 카드할부개월입력값(신용카드 할부개월수 화면값)
    public String cardInstallmentMonthIpt;
    // 카드이자여부(Y:유이자,N:무이자)
    public String cardInterestYn;
    // 카드이자여부입력값(Y:유이자,N:무이자) 화면 입력값
    public String cardInterestYnIpt;
    // 카드유형코드(CREDIT:신용카드, CHECK:체크카드)
    public String cardTypeCd;
    // 승인번호(카드,간편결제,상품권 등)
    public String approvalNo;
    // 모바일결제 이동통신사유형
    public String mpayMcomType;
    // 모바일결제 모바일번호
    public String mpayMobileNo;
    // 모바일결제 VAN코드
    public String mpayVanCd;
    // 가상계좌 만료일자
    public String paymentReqDt;
    // 결제완료일자(승인완료일자, 무통장은 입금완료일자)
    public String paymentFshDt;
    // MHC승인일자(MHC 포인트 승인 날짜)
    public String mhcApprovalDt;
    // MHC사용포인트
    public String mhcUsePoint;
    // PG 승인Key값
    public String pgTid;
    // DGV카드번호(DGV 카드번호암호화)
    public String dgvCardNo;
    // DGV승인일자(YYYYMMDD)
    public String dgvApprovalDt;
    // DGV승인시간(HHMMSS)
    public String dgvApprovalTime;
    // DGVPOS번호(HOME:781,CLUB:791 고정)
    public String dgvPosNo;
    // DGV거래번호(영수증번호)
    public String dgvTradeNo;
    // DGV남은금액(사용 후 남은 금액)
    public String dgvRemainAmt;
    // 결제토큰(TOSS)
    public String payToken;
    // OCB고유거래id(OCB:mctTransactionId, 홈플러스고유거래번호)
    public String ocbUniqTradeId;
    // OCB트랜잭션토큰(OCB:ocbUseTrAuthToken)
    public String ocbTranToken;
    // OCB트랜잭션id(OCB:ocbTransactionId)
    public String ocbTranId;
    // PG수수료관리순번
    public String pgCommissionMngSeq;
    // 소수점처리(UP:올림,DOWN:버림,ROUND:반올림)
    public String decpntProcess;
    // 신용카드수수료율(신용카드결제, 간편결제신용카드)
    public String creditCommissionRate;
    // 체크카드수수료율(체크카드, 간편결제머니)
    public String debitCommissionRate;
    // 수수료금액(실시간계좌이체,휴대폰결제), 신용카드,간편결제 수수율에 따른 금액 입력
    public String commissionAmt;
    // 모바일환불수수료금액(휴대폰결제 환불수수료 금액)
    public String mobileRefundCommissionAmt;
    // 실시간계좌이체최저수수료금액(토스,이니시스 실시간계좌이체 최저수수료금액)
    public String rbankLowCommissionAmt;
    // TOSS실시간계좌이체최초수수료금액(토스 10만원 초과 수수료금액)
    public String tossRbankFstCommissionAmt;
    // TOSS실시간계좌이체다음수수료금액(토스 100만원 초과 수수료금액)
    public String tossRbankNextCommissionAmt;
    // 부가세여부(PG수수료 부가세여부)
    public String vatYn;
    // 리턴메시지(주문생성 사유 저장)
    public String returnMsg;
    // 승인리턴코드(0000:정상,그외:비정상)
    public String returnCd;
    // 취소승인번호(결제오류로 인한 승인 취소승인번호)
    public String cancelApprovalNo;
    // 취소승인일자(결제 오류로 인한 취소승인날짜) (YYYYMMDD) or (YYYY-MM-dd HH:mm:ss) 2가지 형식 입력됨
    public String cancelApprovalDt;
    // 취소DGV남은금액(결제 오류로 인한 DGV 취소 후 잔액)
    public String cancelDgvRemainAmt;
    // 취소결제금액(결제 오류로 인한 취소 결제금액)
    public String cancelAmt;
    // 취소요청tid(결제취소 요청 Tid값) (홈플러스 취소요청 Transaction ID)
    public String cancelReqTid;
    // 취소tid(결제취소 Tid값) (Transaction ID)
    public String cancelTid;
    // 취소리턴메시지(결제수단 취소 사유 저장)
    public String cancelReturnMsg;
    // 취소리턴코드(결제수단 취소 사유 코드 결과코드,0000:정상,그이외 비정상)
    public String cancelReturnCd;
    // DGV인터페이스여부(DGV 결제내역 LGW 전송여부)
    public String dgvIfYn;
    // 등록일시
    public String regDt;
    // 등록자ID
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정자ID
    public String chgId;
    // OCB 적립 카드번호 (ocb 제휴 진입시)
    public String ocbSaveCardNo;
}
