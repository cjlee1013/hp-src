package kr.co.homeplus.claim.claim.model.external;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "점포픽업장소")
@Getter
@Setter
public class StorePickupPlace {

    @ApiModelProperty(value = "점포ID", position = 1)
    @JsonIgnore
    private int storeId;

    @ApiModelProperty(value = "점포명", position = 1)
    private String storeNm;

    @ApiModelProperty(value = "장소 번호", position = 2)
    private String placeNo;

    @ApiModelProperty(value = "사용여부", position = 3)
    @JsonIgnore
    private String useYn;

    @ApiModelProperty(value = "장소명", position = 4)
    private String placeNm;

    @ApiModelProperty(value = "장소설명", position = 5)
    private String placeExpln;

    @ApiModelProperty(value = "장소이미지", position = 6)
    private String imageUrl;

    @ApiModelProperty(value = "전화번호", position = 7)
    private String telNo;

    @ApiModelProperty(value = "운영시작시간1", position = 8)
    @JsonIgnore
    private String startTime1;

    @ApiModelProperty(value = "운영종료시간1", position = 9)
    @JsonIgnore
    private String endTime1;

    @ApiModelProperty(value = "장소주소", position = 10)
    private String placeAddr;

    @ApiModelProperty(value = "락커사용여부", position = 11)
    private String lockerUseYn;

    @ApiModelProperty(value = "픽업장소주소", position = 12)
    private String placeAddrTxt;
}
