package kr.co.homeplus.claim.entity.ship;

public class StoreShiftMngEntity {
    // 점포유형(Hyper, Club, Exp, DS)
    public String storeType;
    // 점포ID
    public String storeId;
    // 배송일자(년-월-일 00:00:00)
    public String shipDt;
    // 배송요일(1:일요일,2:월요일~7:토요일)
    public String shipWeekday;
    // SHIFTID
    public String shiftId;
    // VAN명(VAN A,B,C조)
    public String vanNm;
    // SHIFT배송시작시간(1000,2330)
    public String shiftShipStartTime;
    // SHIFT배송종료시간(11:00,17:00)
    public String shiftShipEndTime;
    // SHIFT실행일(SHIFT실행 D-DAY) (0:배송일, 1:배송전일)
    public String shiftExecDay;
    // SHIFT마감시간(DMS실행시간)
    public String shiftCloseTime;
    // SHIFT사전마감시간(1차전송시간)
    public String shiftBeforeCloseTime;
    // ASSIGNDRV여부(사용용도 모름)
    public String assigndrvYn;
    // 반품/교환가능여부
    public String returnYn;
    // 수정자ID
    public String chgId;
    // 수정일자
    public String chgDt;

}
