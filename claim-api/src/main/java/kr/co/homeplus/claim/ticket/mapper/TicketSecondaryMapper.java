package kr.co.homeplus.claim.ticket.mapper;

import kr.co.homeplus.claim.core.db.annotation.SecondaryConnection;
import kr.co.homeplus.claim.ticket.model.OrderTicketGetDto;

@SecondaryConnection
public interface TicketSecondaryMapper {

    OrderTicketGetDto selectTicketInfo(String orderTicketNo);

    int selectTicketStatus(String ticketStatus);
}
