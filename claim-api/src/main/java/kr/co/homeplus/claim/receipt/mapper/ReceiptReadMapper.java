package kr.co.homeplus.claim.receipt.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.core.db.annotation.SecondaryConnection;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseMethodReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.card.ClaimCardReceiptGetDto;
import kr.co.homeplus.claim.receipt.model.cash.ClaimCashReceiptGetDto;
import kr.co.homeplus.claim.receipt.model.order.ClaimRefundReceiptGetDto.ClaimRefundDetailReceiptDto;
import kr.co.homeplus.claim.receipt.model.order.ClaimRefundReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.purchase.ClaimPaymentReceiptInfo;
import kr.co.homeplus.claim.receipt.model.purchase.ClaimReceiptItemInfo;
import kr.co.homeplus.claim.receipt.model.purchase.ClaimReceiptMainInfo;
import kr.co.homeplus.claim.receipt.sql.ClaimReceiptSql;
import org.apache.ibatis.annotations.SelectProvider;

@SecondaryConnection
public interface ReceiptReadMapper {

    @SelectProvider(type = ClaimReceiptSql.class, method = "selectClaimPurchaseReceiptMainInfo")
    ClaimReceiptMainInfo selectClaimPurchaseReceiptMainInfo(ClaimPurchaseReceiptSetDto setDto);

    @SelectProvider(type = ClaimReceiptSql.class, method = "selectClaimPurchaseReceiptItemInfo")
    List<ClaimReceiptItemInfo> selectClaimPurchaseReceiptItemInfo(ClaimPurchaseReceiptSetDto setDto);

    @SelectProvider(type = ClaimReceiptSql.class, method = "selectClaimPurchaseReceiptPaymentInfo")
    List<ClaimPaymentReceiptInfo> selectClaimPurchaseReceiptPaymentInfo(ClaimPurchaseReceiptSetDto setDto);

    @SelectProvider(type = ClaimReceiptSql.class, method = "selectClaimCardPurchaseReceiptInfo")
    ClaimCardReceiptGetDto selectClaimCardPurchaseReceiptInfo(ClaimPurchaseMethodReceiptSetDto setDto);

    @SelectProvider(type = ClaimReceiptSql.class, method = "selectClaimCashPurchaseReceiptInfo")
    ClaimCashReceiptGetDto selectClaimCashPurchaseReceiptInfo(ClaimPurchaseMethodReceiptSetDto setDto);

    @SelectProvider(type = ClaimReceiptSql.class, method = "selectOrderRefundReceiptInfo")
    List<LinkedHashMap<String, Object>> selectOrderRefundReceiptInfo(ClaimRefundReceiptSetDto setDto);
}
