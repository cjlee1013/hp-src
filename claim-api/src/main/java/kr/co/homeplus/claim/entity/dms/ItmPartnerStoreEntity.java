package kr.co.homeplus.claim.entity.dms;

public class ItmPartnerStoreEntity {
    //
    public String storeId;
    //
    public String partnerId;
    //
    public String storeNm;
    //
    public String storeNm2;
    //
    public String storeType;
    //
    public String mgrNm;
    //
    public String storeKind;
    //
    public String fcStoreId;
    //
    public String originStoreId;
    //
    public String openDate;
    //
    public String costCenter;
    //
    public String region;
    //
    public String zipcode;
    //
    public String addr1;
    //
    public String addr2;
    //
    public String phone;
    //
    public String fax;
    //
    public String closedInfo;
    //
    public String oprTime;
    //
    public String useYn;
    //
    public String onlineYn;
    //
    public String pickupYn;
    //
    public String latitude;
    //
    public String longitude;
    //
    public String regId;
    //
    public String regDt;
    //
    public String chgId;
    //
    public String chgDt;
    //
    public String onlineCostCenter;
    //
    public String storeCostCenter;
}
