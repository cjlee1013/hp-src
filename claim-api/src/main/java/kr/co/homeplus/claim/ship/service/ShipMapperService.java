package kr.co.homeplus.claim.ship.service;

import java.util.List;
import kr.co.homeplus.claim.ship.model.DlvCdGetDto;
import kr.co.homeplus.claim.ship.model.ShipMsgCdGetDto;

public interface ShipMapperService {

    /**
     * 택배사 코드 리스트
     * **/
    List<DlvCdGetDto> getDlvCd();

    /**
     * 배송메시지 리스트
     * **/
    List<ShipMsgCdGetDto> getShipMsg();

}
