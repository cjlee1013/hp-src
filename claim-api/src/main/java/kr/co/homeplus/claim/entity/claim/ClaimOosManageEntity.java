package kr.co.homeplus.claim.entity.claim;

public class ClaimOosManageEntity {
    // 결품/대체거래 SEQ
    public String oosManageSeq;
    // 구매점포번호
    public String purchaseStoreInfoNo;
    // 주문번호
    public String purchaseOrderNo;
    // 클레임번호
    public String claimNo;
    // 대체주문번호
    public String subPurchaseOrderNo;
    // 배송일자
    public String shipDt;
    // SHIFT ID
    public String shiftId;
    // SLOT ID
    public String slotId;
    // 응답코드
    public String returnCd;
    // 응답메시지
    public String returnMessage;
    // 등록일
    public String regDt;
    // 수정일
    public String chgDt;
    // 단축번호
    public String sordNo;
}
