package kr.co.homeplus.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ExchangeShipType {
    DIRECT("D"),
    NONE("N"),
    POST("P"),
    DELIVERY("C");

    private final String exchangeShipType;
}
