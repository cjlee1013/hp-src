package kr.co.homeplus.claim.receipt.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.claim.enums.ResponseCode;
import kr.co.homeplus.claim.factory.ResponseFactory;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseMethodReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseReceiptGetDto;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.card.ClaimCardReceiptGetDto;
import kr.co.homeplus.claim.receipt.model.cash.ClaimCashReceiptGetDto;
import kr.co.homeplus.claim.receipt.model.order.ClaimRefundReceiptGetDto;
import kr.co.homeplus.claim.receipt.model.order.ClaimRefundReceiptGetDto.ClaimRefundDetailReceiptDto;
import kr.co.homeplus.claim.receipt.model.order.ClaimRefundReceiptSetDto;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimReceiptService {

    private final ReceiptMapperService mapperService;

    // 주문_구매영수증
    public ResponseObject<ClaimPurchaseReceiptGetDto> getClaimPurchaseReceiptInfo(ClaimPurchaseReceiptSetDto claimPurchaseReceiptSetDto) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, this.createClaimPurchaseReceiptInfo(claimPurchaseReceiptSetDto));
    }

    public ResponseObject<ClaimCardReceiptGetDto> getClaimCardPurchaseReceiptInfo(ClaimPurchaseMethodReceiptSetDto claimPurchaseMethodReceiptSetDto) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, mapperService.getClaimCardPurchaseReceiptInfo(claimPurchaseMethodReceiptSetDto));
    }

    public ResponseObject<ClaimCashReceiptGetDto> getClaimCashPurchaseReceiptInfo(ClaimPurchaseMethodReceiptSetDto claimPurchaseMethodReceiptSetDto) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, mapperService.getClaimCashPurchaseReceiptInfo(claimPurchaseMethodReceiptSetDto));
    }

    public ResponseObject<List<ClaimRefundReceiptGetDto>> getOrderRefundReceipt(ClaimRefundReceiptSetDto setDto){
        List<LinkedHashMap<String, Object>> data = mapperService.getOrderRefundReceiptInfo(setDto);
        List<Long> claimNoList = data.stream().map(map -> ObjectUtils.toLong(map.get("claim_no"))).distinct().collect(Collectors.toList());
        List<ClaimRefundReceiptGetDto> resultDto = new ArrayList<>();
        for(Long claimNo : claimNoList) {
            resultDto.add(
                ClaimRefundReceiptGetDto.builder()
                    .purchaseOrderNo(setDto.getPurchaseOrderNo())
                    .cancelPaymentDetail(
                        data.stream()
                            .filter(map -> ObjectUtils.toLong(map.get("claim_no")).equals(claimNo))
                            .map(map -> ClaimRefundDetailReceiptDto.builder().paymentType(ObjectUtils.toString(map.get("payment_type"))).paymentAmt(ObjectUtils.toLong(map.get("payment_amt"))).build())
                            .collect(Collectors.toList())
                    )
                    .claimNo(claimNo)
                    .cancelCompleteAmt(
                        data.stream()
                            .filter(map -> ObjectUtils.toLong(map.get("claim_no")).equals(claimNo))
                            .mapToLong(map -> ObjectUtils.toLong(map.get("payment_amt")))
                            .sum()
                    )
                    .cancelCompleteDt(
                        data.stream()
                            .filter(map -> ObjectUtils.toLong(map.get("claim_no")).equals(claimNo))
                            .map(map -> ObjectUtils.toString(map.get("pay_complete_dt")))
                            .collect(Collectors.toList())
                            .get(0)
                    )
                    .build()
            );
        }

        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, resultDto);
    }

    private ClaimPurchaseReceiptGetDto createClaimPurchaseReceiptInfo(ClaimPurchaseReceiptSetDto setDto) {
        return ClaimPurchaseReceiptGetDto.builder()
            .claimReceiptMainInfo(mapperService.getClaimPurchaseReceiptMainInfo(setDto))
            .claimReceiptItemInfoList(mapperService.getClaimPurchaseReceiptItemInfo(setDto))
            .claimPaymentReceiptInfoList(mapperService.getClaimPurchaseReceiptPaymentInfo(setDto))
            .build();
    }

}
