package kr.co.homeplus.claim.claim.model.mp;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimPaymentType {

    @ApiModelProperty(value = "결제번호", position = 1)
     private String paymentNo;

     @ApiModelProperty(value = "결제수단", position = 2)
     private String paymentType;

     @ApiModelProperty(value = "결제수단코드", position = 3)
     private String paymentTypeCode;

     @ApiModelProperty(value="결제수단명", position = 5)
     private String methodNm;

     @ApiModelProperty(value="카드번호", position = 6)
     private String cardNo;

     @ApiModelProperty(value="카드 할부개월스", position = 7)
     private String cardInstallmentMonth;

     @ApiModelProperty(value = "결제상태", position = 8)
     private String paymentStatus;

     @ApiModelProperty(value = "pg결제금액", position = 9)
     private long pgAmt;

     @ApiModelProperty(value = "마일리지 금액", position = 10)
     private long mileageAmt;

     @ApiModelProperty(value = "마이홈플러스 포인트 금액", position = 11)
     private long mhcAmt;

     @ApiModelProperty(value = "홈플러스 상품권 금액", position = 12)
     private long dgvAmt;

     @ApiModelProperty(value = "오케이캐시백 금액", position = 13)
     private long ocbAmt;

     @ApiModelProperty(value = "회원번호", position = 14, hidden = true)
     private String userNo;

}
