package kr.co.homeplus.claim.omni.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claim.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.claim.claim.service.ClaimCommonService;
import kr.co.homeplus.claim.claim.service.ClaimMapperService;
import kr.co.homeplus.claim.claim.service.ClaimProcessService;
import kr.co.homeplus.claim.claim.service.ExternalService;
import kr.co.homeplus.claim.claim.service.MarketClaimService;
import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.enums.ClaimCode;
import kr.co.homeplus.claim.enums.ClaimMessageInfo;
import kr.co.homeplus.claim.enums.ClaimResponseCode;
import kr.co.homeplus.claim.enums.ExternalInfo;
import kr.co.homeplus.claim.enums.ResponseCode;
import kr.co.homeplus.claim.enums.impl.EnumImpl;
import kr.co.homeplus.claim.factory.ResponseFactory;
import kr.co.homeplus.claim.omni.model.OmniOrderSetDto;
import kr.co.homeplus.claim.omni.model.OmniOriginOrderDto;
import kr.co.homeplus.claim.omni.model.order.OmniSubstitutionCompleteDto;
import kr.co.homeplus.claim.omni.model.order.OmniSubstitutionGetDto;
import kr.co.homeplus.claim.omni.model.order.OmniSubstitutionItemDto;
import kr.co.homeplus.claim.omni.model.order.OmniSubstitutionSetDto;
import kr.co.homeplus.claim.utils.LogUtils;
import kr.co.homeplus.claim.utils.MessageSendUtil;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class OmniOrderService {

    private final OmniOrderMapperService omniMapperService;
    private final ClaimMapperService claimMapperService;
    private final ClaimProcessService processService;
    private final ClaimCommonService commonService;
    private final ExternalService externalService;
    private final MarketClaimService marketClaimService;

    public ResponseObject<String> setOmniOrderProcess(List<OmniOrderSetDto> omniOrderSetDto) throws Exception {
        log.info("대체/결품 요청 파라미터 :::: \n[{}]", omniOrderSetDto);
        long oosManageSeq = this.addOmniRequestInfo(omniOrderSetDto);
        if(oosManageSeq == 0L){
            return ResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_OMNI_ERR02);
        }
        // ordNo를 purchaseOrderNo 로 변경한다.
        this.convertOrdNoToOos(omniOrderSetDto);
        // 그룹 재처리
        LinkedHashMap<Long, List<OmniOrderSetDto>> orderListMap = createOmniOrderData(omniOrderSetDto);
        if(orderListMap.size() == 0){
            modifyOmniRequestInfoByComplete(oosManageSeq, ClaimResponseCode.CLAIM_OMNI_ERR01);
            return ResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_OMNI_ERR01);
        }
        // 파라미터 체크
        this.isCheckOmniRequest(orderListMap);
        // 정보처리
        for(Long ordNo : orderListMap.keySet()){
            List<OmniOrderSetDto> ordNoOrderList = new ArrayList<>(orderListMap.get(ordNo));
            Boolean isChange = ordNoOrderList.stream().anyMatch(dto -> dto.getChangeYn().equals("Y")) ? Boolean.TRUE : Boolean.FALSE;
            List<OmniOrderSetDto> changeOrderList = orderListMap.get(ordNo).stream().filter(dto -> dto.getChangeYn().equals("Y")).collect(Collectors.toList());
            if(changeOrderList.size() > 0){
                if(changeOrderList.stream().anyMatch(dto -> StringUtils.isEmpty(dto.getChangeGoodId()))){
                    modifyOmniRequestInfoByComplete(oosManageSeq, ResponseCode.CLAIM_SUBSTITUTION_ERR01);
                    throw new LogicException(ResponseCode.CLAIM_SUBSTITUTION_ERR01);
                }
                if(changeOrderList.stream().anyMatch(dto -> dto.getChangeQty() == 0)){
                    modifyOmniRequestInfoByComplete(oosManageSeq, ResponseCode.CLAIM_SUBSTITUTION_ERR02);
                    throw new LogicException(ResponseCode.CLAIM_SUBSTITUTION_ERR02);
                }
            }
            try {
                log.info("주문번호 :: {} 에 결품/대체 처리.", ordNo);
                // 결품/대체 프로세스 변경.
                // 대체도 결국 클레임에 계산되어야 하므로 결품과 같이 클레임 등록.
                ClaimRegSetDto regSetDto = getOrderPreRefundItemList(ordNoOrderList, Boolean.FALSE);
                if(isChange){
                    regSetDto.setClaimReasonType("OOS2");
                }
                LinkedHashMap<String, Object> omniOrderMap = processService.addClaimRegister(regSetDto);
                log.info("setOmniOrderProcess :: {}", omniOrderMap);
                long claimNo = ObjectUtils.toLong(omniOrderMap.get("claim_no"));
                long claimBundleNo = ObjectUtils.toLong(omniOrderMap.get("claim_bundle_no"));
                String orderType = ObjectUtils.toString(omniOrderMap.get("order_type"));
                // 클레임 번호 저장
                this.modifyOmniRequestInfoByClaimNo(oosManageSeq, omniOrderMap.get("claim_no"));
                // 대체만 따로 계산하여 주문생성.
                // 그후 금액 업데이트.
                // 대체가 있으면 T11117 인데 환불금액 발생시에만
                // 원주문의 상품에 대체여부를 조회하여서 문자.
                if(isChange){
                    // 대체주문 생성.
                    OmniSubstitutionSetDto substitutionSetDto = createSubstitutionProcess(omniOrderMap, changeOrderList);
                    if(substitutionSetDto == null){
                        processService.setClaimStatus(claimNo, claimBundleNo, "SYSTEM", "C9");
                        modifyOmniRequestInfoByComplete(oosManageSeq, ClaimResponseCode.CLAIM_OMNI_ERR03);
                        return ResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_OMNI_ERR03);
                    }
                    // 대체주문 요청/응답
                    OmniSubstitutionGetDto resultDto = this.requestSubstitutionOrder(substitutionSetDto);

                    if(resultDto == null){
                        // 거절 처리.
                        processService.setClaimStatus(claimNo, claimBundleNo, "SYSTEM", "C9");
                        modifyOmniRequestInfoByComplete(oosManageSeq, ClaimResponseCode.CLAIM_OMNI_ERR02);
                        return ResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_OMNI_ERR02);
                    }
                    // 대체주문 번호 저장.
                    this.modifyOmniRequestInfoBySub(oosManageSeq, resultDto.getSubstitutionPurchaseOrderNo());
                    // 취소결제수단 전환
                    LinkedHashMap<String, Long> paymentTypeMap = commonService.getClaimPaymentTypeList(ObjectUtils.getConvertMap(resultDto));
                    // 실패여부
                    boolean isFail = Boolean.FALSE;
                    // 취소결제수단 전환이 없으면 오류
                    if(paymentTypeMap == null || paymentTypeMap.size() == 0) {
                        isFail = Boolean.TRUE;
                    } else {
                        for(String key : paymentTypeMap.keySet()){
                            LinkedHashMap<String, Object> substitutionMap = new LinkedHashMap<>();
                            substitutionMap.put("payment_type", commonService.getClaimPaymentTypeCode(key));
                            substitutionMap.put("substitution_amt", paymentTypeMap.get(key));
                            substitutionMap.put("claim_no", claimNo);

                            omniMapperService.claimSubstitutionResultModify(substitutionMap);
                        }

                        // 금액비교 해야함.
                        if("ORD_MARKET".equals(orderType)) {
                            if(!this.paymentCancel(omniOrderMap)){
                                isFail = Boolean.TRUE;
                            }
                        }
                        // 실패가 아닐경우
                        if(!isFail) {
                            String completeResult = this.requestSubstitutionOrderComplete(
                                OmniSubstitutionCompleteDto.builder()
                                    .substitutionPurchaseOrderNo(resultDto.getSubstitutionPurchaseOrderNo())
                                    .purchaseOrderNo(ObjectUtils.toLong(omniOrderMap.get("purchase_order_no")))
                                    .userNo(ObjectUtils.toLong(omniOrderMap.get("user_no")))
                                    .build()
                            );
                            log.info("completeResult :: {}", completeResult);
                        }
                    }

                    if(isFail){
                        String failResult = this.requestSubstitutionOrderFail(
                            OmniSubstitutionCompleteDto.builder()
                                .substitutionPurchaseOrderNo(resultDto.getSubstitutionPurchaseOrderNo())
                                .purchaseOrderNo(ObjectUtils.toLong(omniOrderMap.get("purchase_order_no")))
                                .userNo(ObjectUtils.toLong(omniOrderMap.get("user_no")))
                                .build()
                        );
                        log.info("대체주문 실패 결과 ::: {}", failResult);
                        processService.setClaimStatus(claimNo, claimBundleNo, "SYSTEM", "C9");
                        modifyOmniRequestInfoByComplete(oosManageSeq, ResponseCode.CLAIM_SUBSTITUTION_ERR06);
                        return ResponseFactory.getResponseObject(ResponseCode.CLAIM_SUBSTITUTION_ERR06);
                    }
                    // 승인상태 처리.
                    // 대체 일때 비용발생하는 여부 체크 해야함.
                    // 대체 비용이 발생하면 알림톡
                    MessageSendUtil.sendOmniSubstitution(ObjectUtils.toLong(omniOrderMap.get("claim_no")));
                } else {
                    // 마켓여부를 확인 하여 마켓이면 해당 거래에 대한 취소를 요청한다.
                    if(marketClaimService.isOrderMarketCheck(regSetDto.getPurchaseOrderNo())){
                        if("C,R".contains(regSetDto.getClaimType())){
                            try {
                                marketClaimService.setMarketCancelReg(regSetDto.getPurchaseOrderNo(), regSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderOptNo).collect(Collectors.toList()), regSetDto.getClaimReasonType());
                            } catch (LogicException le) {
                                le.setResponseCode(ClaimResponseCode.CLAIM_MARKET_REG_ERR03.getResponseCode());
                                throw new LogicException(le.getResponseCode(), le.getResponseMsg());
                            }

                        } else {
                            throw new LogicException(ClaimResponseCode.CLAIM_MARKET_REG_ERR01);
                        }
                    }

                    boolean isItemSubstitution = this.getOriginOrderItemList(ordNo).stream().map(OmniOriginOrderDto::getSubstitutionYn).anyMatch(key -> key.equals("N"));

                    if(isItemSubstitution){
                        MessageSendUtil.sendOmniOutOfStock(ObjectUtils.toLong(omniOrderMap.get("claim_no")), ClaimMessageInfo.CLAIM_MESSAGE_HOME_OOS_SUB);
                    } else {
                        MessageSendUtil.sendOmniOutOfStock(ObjectUtils.toLong(omniOrderMap.get("claim_no")), ClaimMessageInfo.CLAIM_MESSAGE_HOME_OOS_NOT_SUB);
                    }
                }
                if(!("ORD_MARKET".equals(orderType) && isChange)) {
                    if (!processService.setClaimStatus(claimNo, claimBundleNo, "OMNI", ClaimCode.CLAIM_STATUS_C2)) {
                        throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
                    }
                }
            } catch (LogicException le){
                modifyOmniRequestInfoByComplete(oosManageSeq, le.getResponseCode(), le.getResponseMsg());
                throw new LogicException(le.getResponseCode(), le.getResponseMsg());
            }
        }
        // 환불예정금액 조회
        modifyOmniRequestInfoByComplete(oosManageSeq, ResponseCode.SUCCESS);
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS);
    }

    /**
     * 결품/대체 환불예정금액 생성
     * @param omniOrderSetDto
     * @param isChange
     * @return
     */
    private ClaimRegSetDto getOrderPreRefundItemList (List<OmniOrderSetDto> omniOrderSetDto, Boolean isChange) throws Exception {
        // 주문번호
        Long ordNo = omniOrderSetDto.stream().map(OmniOrderSetDto::getOrdNo).distinct().collect(Collectors.toList()).get(0);
        // 주문에 따른 원주문 상품정보 취득
        List<OmniOriginOrderDto> originOrderList = this.getOriginOrderItemList(ordNo);

        if(originOrderList == null){
            throw new LogicException(ResponseCode.CLAIM_SUBSTITUTION_ERR03);
        }
        String bundleNo = originOrderList.stream().map(OmniOriginOrderDto::getBundleNo).distinct().collect(Collectors.joining());
        String userNo = originOrderList.stream().map(OmniOriginOrderDto::getUserNo).distinct().collect(Collectors.joining());
        String orderType = originOrderList.stream().map(OmniOriginOrderDto::getOrderType).collect(Collectors.toList()).get(0);

        // 환불예정금액 상품 리스트 생성.
        List<ClaimPreRefundItemList> refundItemList = new ArrayList<>();

        for(OmniOrderSetDto dto : omniOrderSetDto) {
            int claimQty = dto.getWaitingQty();

            if(claimQty <= 0){
                throw new LogicException(ResponseCode.CLAIM_SUBSTITUTION_ERR04);
            }
            if(originOrderList.stream().map(OmniOriginOrderDto::getItemNo).noneMatch(str -> str.equals(dto.getGoodId()))){
                LogUtils.error("주문번호 :: {} || 오류 :: {}", dto.getOrdNo(), ResponseCode.CLAIM_SUBSTITUTION_ERR08.getResponseMessage());
                throw new LogicException(ResponseCode.CLAIM_SUBSTITUTION_ERR08, "itemNo", dto.getGoodId());
            }
            int checkPoint = 0;
            for(OmniOriginOrderDto originDto : originOrderList.stream().filter(originDto -> originDto.getItemNo().equals(dto.getGoodId())).collect(Collectors.toList())){
                if(originDto.getSubstitutionYn().equals("Y") && isChange){
                    log.info("대체안함 선택 상품({})을 대체요청하였습니다. ", dto.getGoodId());
                    throw new LogicException(ResponseCode.CLAIM_SUBSTITUTION_ERR10, "itemNo", dto.getGoodId());
                }
                int itemQty = Integer.parseInt(originDto.getItemQty());
                int claimItemQty = Integer.parseInt(originDto.getClaimItemQty());
                int optTotalQty = originOrderList.stream().filter(optDto -> optDto.getItemNo().equals(dto.getGoodId())).map(OmniOriginOrderDto::getItemQty).mapToInt(Integer::parseInt).sum();
                log.info("[ordNo :: {} 에 대한 환불예정금액 조회 originDto :: {} || checkPoint :: {} - itemQty :: {} ||  claimItemQty :: {}]", ordNo, originDto, checkPoint, itemQty, claimItemQty);
                if(!isChange){
                    if((optTotalQty - (claimQty + claimItemQty)) < 0){
                        throw new LogicException(ResponseCode.CLAIM_SUBSTITUTION_ERR07, "itemNo", originDto.getItemNo());
                    }
                }
                if("ORD_MARKET".equals(orderType) || "ORD_SUB_MARKET".equals(orderType)) {
                    if(claimQty != optTotalQty) {
                        log.error("마켓주문에 대한 대체주문 및 결품시도중 수량에 따른 오류 발생 :: 취소수량 ({}) ::: 주문수량 ({})", claimQty, optTotalQty);
                        throw new LogicException(ResponseCode.CLAIM_SUBSTITUTION_ERR11, "itemNo", originDto.getItemNo());
                    }
                }

                if(claimQty > checkPoint){
                    if((checkPoint + itemQty) > claimQty){
                        itemQty = claimQty - checkPoint;
                    }
                    refundItemList.add(ClaimPreRefundItemList.builder().itemNo(dto.getGoodId()).orderOptNo(originDto.getOrderOptNo()).claimQty(String.valueOf(itemQty)).orderItemNo(originDto.getOrderItemNo()).build());
                    checkPoint += itemQty;
                } else {
                    break;
                }
            }
        }
        return ClaimRegSetDto.builder()
            .purchaseOrderNo(String.valueOf(ordNo))
            .bundleNo(bundleNo)
            .cancelType("O")
            .claimItemList(refundItemList)
            .claimDetail(null)
            .orderCancelYn("Y")
            .claimPartYn("N")
            .claimReasonType("OOS1")
            .claimReasonDetail("")
            .whoReason("S")
            .claimType("C")
            .regId("OMNI")
            .userNo(Long.parseLong(userNo))
            .isOmni(Boolean.TRUE)
            .isMultiOrder(Boolean.FALSE)
            .build();
    }

    private OmniSubstitutionGetDto requestSubstitutionOrder(OmniSubstitutionSetDto setDto) throws Exception {
        try {
            return externalService.postTransfer(
                ExternalInfo.ESCROW_SUBSTITUTION_PROCESS,
                setDto,
                OmniSubstitutionGetDto.class
            );
        } catch (LogicException cle) {
            log.error("requestSubstitutionOrder Error ::: {}", cle.getResponseCode());
            return null;
        } catch (Exception e){
            log.error("requestSubstitutionOrder Error ::: {}", e.getMessage());
            return null;
        }
    }

    private String requestSubstitutionOrderComplete(OmniSubstitutionCompleteDto completeDto) throws Exception {
        try {
            return externalService.getTransfer(
                ExternalInfo.ESCROW_SUBSTITUTION_COMPLETE,
                externalService.convertDtoToSetParameter(completeDto),
                String.class
            );
        } catch (Exception e) {
            log.error("requestSubstitutionOrderComplete Error ::: {}", e.getMessage());
            // 오류 발생시 해당 거래 실패처리.
            this.requestSubstitutionOrderFail(completeDto);
            return null;
        }
    }

    private String requestSubstitutionOrderFail(OmniSubstitutionCompleteDto completeDto) throws Exception {
        try {
            return externalService.getTransfer(
                ExternalInfo.ESCROW_SUBSTITUTION_FAIL,
                externalService.convertDtoToSetParameter(completeDto),
                String.class
            );
        } catch (Exception e) {
            log.error("requestSubstitutionOrderFail Error ::: {}", e.getMessage());
            return null;
        }
    }

    private OmniSubstitutionSetDto createSubstitutionProcess(LinkedHashMap<String, Object> omniOrderMap, List<OmniOrderSetDto> changeOrderList) throws Exception {
        try {
            ClaimPreRefundCalcDto calcDto = claimMapperService.getClaimPreRefundCalculation(commonService.convertCalcSetDto(getOrderPreRefundItemList(changeOrderList, Boolean.TRUE)));
            return OmniSubstitutionSetDto.builder()
                .pgAmt(calcDto.getPgAmt())
                .mileageAmt(calcDto.getMileageAmt())
                .mhcAmt(calcDto.getMhcAmt())
                .dgvAmt(calcDto.getDgvAmt())
                .ocbAmt(calcDto.getOcbAmt())
                .originPurchaseOrderNo(ObjectUtils.toLong(omniOrderMap.get("purchase_order_no")))
                .userNo(ObjectUtils.toLong(omniOrderMap.get("user_no")))
                .substitutionOrderItemParamList(this.createSubstitutionDto(changeOrderList))
                .build();
        } catch (LogicException cle) {
            log.error("클레임그룹번호({}) 대체주문 클레임 생성시 오류 발생 : {}:{}", omniOrderMap.get("claim_no"), cle.getResponseCode(), cle.getResponseMsg());
            return null;
        } catch (Exception e) {
            log.error("클레임그룹번호({}) 대체주문 클레임 생성시 오류 발생 : {}", omniOrderMap.get("claim_no"), e.getMessage());
            return null;
        }

    }

    private List<OmniSubstitutionItemDto> createSubstitutionDto(List<OmniOrderSetDto> omniOrderSetDto) {
        if(omniOrderSetDto.stream().noneMatch(dto -> dto.getChangeYn().equals("Y"))){
            return null;
        }
        List<OmniSubstitutionItemDto> returnList = new ArrayList<>();
        for(OmniOrderSetDto dto : omniOrderSetDto) {
            if(StringUtils.isEmpty(dto.getChangeGoodId())){
                return null;
            }
            returnList.add(
                OmniSubstitutionItemDto.builder()
                    .originItemNo(dto.getGoodId())
                    .originItemQty(dto.getWaitingQty())
                    .substitutionItemNo(dto.getChangeGoodId())
                    .substitutionItemQty(dto.getChangeQty())
                    .build()
            );
        }
        return returnList;
    }

    private LinkedHashMap<Long, List<OmniOrderSetDto>> createOmniOrderData(List<OmniOrderSetDto> setDto){
        List<Long> ordNoList = setDto.stream().map(OmniOrderSetDto::getOrdNo).distinct().collect(Collectors.toList());
        LinkedHashMap<Long, List<OmniOrderSetDto>> orderList = new LinkedHashMap<>();

        for (Long ordNo : ordNoList) {
            orderList.put(ordNo, setDto.stream().filter(dto -> dto.getOrdNo().equals(ordNo) && (dto.getChangeYn().equals("Y") || dto.getOosYn().equals("Y"))).collect(Collectors.toList()));
        }

        return orderList;
    }

    private Boolean paymentCancel(LinkedHashMap<String, Object> paymentMap) throws Exception {
        long claimNo = ObjectUtils.toLong(paymentMap.get("claim_no"));
        long claimBundleNo = ObjectUtils.toLong(paymentMap.get("claim_bundle_no"));
        // 상태 변경 C1 -> C2
        // 상태 변경 C2-> C3
        if (!processService.setClaimStatus(claimNo, claimBundleNo, "OMNI", ClaimCode.CLAIM_STATUS_C2)) {
            throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
        }
        // P1 상태로 업데이트
        processService.setClaimPaymentStatus(claimNo, ClaimCode.CLAIM_PAYMENT_STATUS_P1);
        // 취소 처리.
        try{
            if (!processService.marketPaymentCancelProcess(claimNo)) {
                log.error("클레임그룹번호({}) 결제 취소 중 오류가 발생됨.", claimNo);
//                return Boolean.FALSE;
                throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR02);
            }
        } catch (Exception e) {
            log.error("클레임 결제 취소 중 오류가 발생됨. [{} :: {}]", claimNo, e.getMessage());
            return Boolean.FALSE;
        }

        // 상태 변경 C2-> C3
        if (!processService.setClaimStatus(claimNo, claimBundleNo, "OMNI", ClaimCode.CLAIM_STATUS_C3)) {
            throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
        }
        processService.callingClaimReg(claimNo);
        // 쿠폰 깨진거
        Boolean isCouponAndSlot = processService.isClaimCouponAndSlotProcess(claimNo);

        return Boolean.TRUE;
    }

    private List<OmniOrderSetDto> convertOrdNoToOos(List<OmniOrderSetDto> omniOrderSetDto) throws Exception {
        try{
            for(OmniOrderSetDto dto : omniOrderSetDto){
                dto.setOrdNo(omniMapperService.convertOrdNoToPurchaseOrderNo(dto.getOrdNo()));
            }
            return omniOrderSetDto;
        } catch (Exception e) {
            log.error("옴니 결품/대체 주문번호({}) refit 주문번호 변경시 오류 :: {}", omniOrderSetDto.get(0).getOrdNo(), e.getMessage());
            throw new LogicException(ClaimResponseCode.CLAIM_OMNI_ERR10);
        }
    }

    // 원주문 상품리트슽 조회.
    private List<OmniOriginOrderDto> getOriginOrderItemList(long ordNo) {
        return omniMapperService.getOmniOriginOrderList(ordNo);
    }

    private long addOmniRequestInfo(List<OmniOrderSetDto> omniOrderSetDto) throws Exception {
        //
        LinkedHashMap<Long, List<OmniOrderSetDto>> orderListMap = createOmniOrderData(omniOrderSetDto);
        for(long ordNo : orderListMap.keySet()){
            LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
            parameterMap.put("ordNo", ordNo);
            parameterMap.put("purchaseOrderNo", ObjectUtils.toLong(this.getConvertOrdNo(ordNo)));
            parameterMap.put("deliveryDt", orderListMap.get(ordNo).stream().map(OmniOrderSetDto::getDeliveryDt).distinct().map(String::valueOf).collect(Collectors.joining()));
            parameterMap.put("shiftId", orderListMap.get(ordNo).stream().mapToLong(OmniOrderSetDto::getShiftId).distinct().toArray()[0]);
            parameterMap.put("slotId", orderListMap.get(ordNo).stream().mapToLong(OmniOrderSetDto::getShiftId).distinct().toArray()[0]);
            parameterMap.put("sordNo", orderListMap.get(ordNo).stream().map(OmniOrderSetDto::getSordNo).distinct().toArray()[0]);

            if(omniMapperService.addClaimOosInfo(parameterMap)){
                log.debug("addClaimOosInfo :: {}", parameterMap);
                for(OmniOrderSetDto data : orderListMap.get(ordNo)){
                    LinkedHashMap<String, Object> oosItemParam = ObjectUtils.getConvertMap(data);
                    oosItemParam.put("oosManageSeq", parameterMap.get("oosManageSeq"));

                    if(!omniMapperService.addClaimOosItem(oosItemParam)){
                        throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR02);
                    }
                }
            }
            return ObjectUtils.toLong(parameterMap.get("oosManageSeq"));
        }
        throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR02);
    }

    private Long getConvertOrdNo(long ordNo) throws Exception {
        try{
            return omniMapperService.convertOrdNoToPurchaseOrderNo(ordNo);
        } catch (Exception e) {
            return 0L;
        }
    }

    private void modifyOmniRequestInfoByClaimNo(long oosManageSeq, Object claimNo) throws Exception {
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        parameterMap.put("oosManageSeq", oosManageSeq);
        parameterMap.put("claimNo", claimNo);
        if(!omniMapperService.setClaimOosMangeModify(parameterMap)){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR02);
        }
    }

    private void modifyOmniRequestInfoBySub(long oosManageSeq, long substitutionPurchaseOrderNo) throws Exception {
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        parameterMap.put("oosManageSeq", oosManageSeq);
        parameterMap.put("subPurchaseOrderNo", substitutionPurchaseOrderNo);
        if(!omniMapperService.setClaimOosMangeModify(parameterMap)){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR02);
        }
    }

    private void modifyOmniRequestInfoByComplete(long oosManageSeq, EnumImpl enumImpl) throws Exception {
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        parameterMap.put("oosManageSeq", oosManageSeq);
        parameterMap.put("returnCode", enumImpl.getResponseCode());
        parameterMap.put("returnMessage", enumImpl.getResponseMessage());
        if(!omniMapperService.setClaimOosMangeModify(parameterMap)){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR02);
        }
    }

    private void modifyOmniRequestInfoByComplete(long oosManageSeq, String returnCode, String returnMessage) throws Exception {
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        parameterMap.put("oosManageSeq", oosManageSeq);
        parameterMap.put("returnCode", returnCode);
        parameterMap.put("returnMessage", returnMessage);
        if(!omniMapperService.setClaimOosMangeModify(parameterMap)){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR02);
        }
    }

    private void isCheckOmniRequest(LinkedHashMap<Long, List<OmniOrderSetDto>> orderListMap) throws Exception {
        for(Long ordNo : orderListMap.keySet()){
            List<OmniOrderSetDto> requestData = orderListMap.get(ordNo);
            log.info("옴니 주문번호({})결품/대체 파라미터 검증 :: {}", ordNo, requestData);
            for(OmniOrderSetDto dto : requestData){
                if(requestData.stream().filter(crl -> crl.getGoodId().equals(dto.getGoodId())).count() > 1){
                    throw new LogicException(ResponseCode.CLAIM_SUBSTITUTION_ERR09, "itemNo", dto.getGoodId());
                }
            }
        }
    }

    /**
     * List<DTO>> 형식 내 중복키 제거 유틸.
     *
     * @param keyExtractor
     * @param <T>
     * @return
     */
    private <T> Predicate<T> distinctByKey( Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new HashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

}
