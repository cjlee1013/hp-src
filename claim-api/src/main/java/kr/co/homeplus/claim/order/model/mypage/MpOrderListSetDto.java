package kr.co.homeplus.claim.order.model.mypage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마일페이지 > 주문/배송조회 - 배송조회 리스트")
public class MpOrderListSetDto {

    @ApiModelProperty(value = "조회시작일", reference = "YYYY-MM-DD", required = true, position = 1)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", reference = "YYYY-MM-DD", required = true, position = 2)
    private String schEndDt;

    @ApiModelProperty(value = "유저번호", position = 3)
    private Long userNo;

    @ApiModelProperty(value = "페이지", position = 4)
    private Integer page;

    @ApiModelProperty(value = "페이지조회수", position = 5)
    private Integer perPage;

    @ApiModelProperty(value = "사이트타입", position = 6)
    private String siteType;
}
