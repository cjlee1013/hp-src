package kr.co.homeplus.claim.claim.model.external;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "DGV 취소 요청")
public class ClaimDgvCancelSetDto {
    @ApiModelProperty(notes = "DGV카드번호", required = true, position = 1)
    private String dgvCardNo;

    @ApiModelProperty(notes = "POS 번호", required = true, position = 2)
    private String posNo;

    @ApiModelProperty(notes = "영수증번호", required = true, position = 3)
    private String tradeNo;

    @ApiModelProperty(notes = "매출일자(YYYYMMDD)", required = true, position = 4)
    private String saleDt;

    @ApiModelProperty(notes = "부분취소여부('Y':부분취소,'N':전체취소)", required = true, position = 5)
    private String partCancelYn;

    @ApiModelProperty(notes = "취소금액", required = true, position = 6)
    private long tradeAmount;

    @ApiModelProperty(notes = "클레임번호", required = true, position = 7)
    private long claimNo;

    @ApiModelProperty(notes = "원거래 승인번호", required = true, position = 8)
    private String orgAprNumber;

    @ApiModelProperty(notes = "원거래 승인일자", required = true, position = 9)
    private String orgAprDate;

    @ApiModelProperty(notes = "원거래 POS 번호", required = true, position = 10)
    private String orgPosNo;

    @ApiModelProperty(notes = "원거래 영수증번호", required = true, position = 11)
    private String orgTradeNo;

    @ApiModelProperty(notes = "유저번호", required = true, position = 12)
    private String userNo;
}
