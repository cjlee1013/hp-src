package kr.co.homeplus.claim.receipt.model.purchase;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ClaimReceiptItemInfo {

    @JsonIgnore
    @ApiModelProperty(value = "클레임번호", position = 1)
    private long claimNo;

    @ApiModelProperty(value = "상품명", position = 2)
    private String itemName;

    @ApiModelProperty(value = "수량", position = 3)
    private int completeQty;

    @ApiModelProperty(value = "구매금액(취소금액)", position = 4)
    private long completeAmt;

    @ApiModelProperty(value = "파트너아이디", position = 5)
    private String partnerId;

    @ApiModelProperty(value = "파트너명", position = 6)
    private String partnerName;

    @ApiModelProperty(value = "점포타입", position = 7)
    private String storeType;
}