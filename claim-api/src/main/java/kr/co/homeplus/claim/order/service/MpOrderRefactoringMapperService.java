package kr.co.homeplus.claim.order.service;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListSetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPresentDto;
import kr.co.homeplus.claim.order.model.order.MpOrderBundleShipInfoDto;
import kr.co.homeplus.claim.order.model.order.MpOrderItemDto;
import kr.co.homeplus.claim.order.model.order.MypageOrderBundleListDto;
import kr.co.homeplus.claim.order.model.order.MypageOrderListGetDto;
import kr.co.homeplus.claim.order.model.order.OrderClaimInfoDto;

public interface MpOrderRefactoringMapperService {

    List<MypageOrderListGetDto> getMypageOrderList(MpOrderListSetDto setDto);

    List<MypageOrderBundleListDto> getMypageOrderBundleList(long purchaseOrderNo);

    MpOrderBundleShipInfoDto getShipInfoByBundle(long purchaseOrderNo, long bundleNo);

    List<MpOrderItemDto> getMypageOrderItemList(long purchaseOrderNo, long bundleNo);

    List<MpOrderItemDto> getMypageOrderItemListByCombine(long purchaseOrderNo, long bundleNo);

    List<MpOrderItemDto> getMypageOrderItemListBySubstitution(long purchaseOrderNo, long bundleNo);

    List<OrderClaimInfoDto> getOrderClaimInfoByBundle(long purchaseOrderNo, long bundleNo);

    MpOrderPresentDto getMpOrderPresentInfo(long purchaseOrderNo);

    List<LinkedHashMap<String, Object>> getAutoCancelTicketCheck(long bundleNo);

    String getClaimRefundFailCheck(long claimNo);
}
