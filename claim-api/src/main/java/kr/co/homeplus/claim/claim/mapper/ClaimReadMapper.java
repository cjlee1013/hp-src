package kr.co.homeplus.claim.claim.mapper;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import kr.co.homeplus.claim.claim.model.external.ClaimCouponReIssueSetDto.CouponReIssueDto;
import kr.co.homeplus.claim.claim.model.market.MarketClaimBasicDataDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimAddCouponDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimChgPaymentRefundSetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPhonePaymentInfoDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claim.claim.model.register.ClaimInfoRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.claim.claim.sql.ClaimManagementSql;
import kr.co.homeplus.claim.claim.sql.ClaimOrderCancelSql;
import kr.co.homeplus.claim.claim.sql.ClaimOrderTicketSql;
import kr.co.homeplus.claim.claim.sql.ClaimSearchSql;
import kr.co.homeplus.claim.core.db.annotation.SecondaryConnection;
import kr.co.homeplus.claim.order.sql.OrderDataSql;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.mapping.StatementType;

@SecondaryConnection
public interface ClaimReadMapper {

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "callByClaimPreRefundPriceCalculation")
    ClaimPreRefundCalcGetDto callByClaimPreRefundPriceCalculation(HashMap<String, Object> parameterMap);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "callByClaimPreRefundPriceMultiCalculation")
    ClaimPreRefundCalcGetDto callByClaimPreRefundPriceMultiCalculation(HashMap<String, Object> parameterMap);

    @SelectProvider(type = OrderDataSql.class, method = "selectPurchaseOrderInfo")
    ClaimMstRegDto selectPurchaseOrderInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OrderDataSql.class, method = "selectCreateClaimDataInfo")
    LinkedHashMap<String, Object> selectCreateClaimDataInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo, @Param("userNo") long userNo);

    @SelectProvider(type = OrderDataSql.class, method = "selectClaimItemInfo")
    LinkedHashMap<String, Object> selectClaimItemInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderItemNo") long orderItemNo);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectOriPaymentForClaim")
    LinkedHashMap<String, Object> selectOriPaymentForClaim(@Param("paymentNo") long paymentNo, @Param("paymentType") String paymentType);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectOrderOptInfoForClaim")
    ClaimOptRegDto selectOrderOptInfoForClaim(@Param("orderItemNo") long orderItemNo, @Param("orderOptNo") String orderOptNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectOrderBundleByClaim")
    List<LinkedHashMap<String, Object>> selectOrderBundleByClaim(@Param("purchaseOrderNo") long purchaseOrderNo);

    @ResultType(value = ClaimPreRefundItemList.class)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectOrderItemInfoByClaim")
    List<ClaimPreRefundItemList> selectOrderItemInfoByClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectOrderByClaimPartInfo")
    LinkedHashMap<String, Object> selectOrderByClaimPartInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo, @Param("userNo") long userNo);

    @SelectProvider(type = ClaimSearchSql.class, method = "selectAlreadyRegClaimRefundCnt")
    int selectAlreadyRegClaimRefundCnt(ClaimChgPaymentRefundSetDto claimChgPaymentRefundSetDto);

    @SelectProvider(type = ClaimSearchSql.class, method = "selectClaimPhonePaymentInfo")
    ClaimPhonePaymentInfoDto selectClaimPhonePaymentInfo(ClaimChgPaymentRefundSetDto claimChgPaymentRefundSetDto);

    @SelectProvider(type = OrderDataSql.class, method = "selectCreateClaimMstDataInfo")
    LinkedHashMap<String, Object> selectCreateClaimMstDataInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectClaimPossibleCheck")
    LinkedHashMap<String, Object> selectClaimPossibleCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimCouponReIssue")
    List<CouponReIssueDto> selectClaimCouponReIssue(@Param("claimNo") long claimNo, @Param("userNo") long userNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimInfo")
    List<LinkedHashMap<String, Object>> selectClaimInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimPickIsAutoInfo")
    LinkedHashMap<String, Object> selectClaimPickIsAutoInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimSendDataInfo")
    LinkedHashMap<String, Object> selectClaimSendDataInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimRegDataInfo")
    ClaimInfoRegDto selectClaimRegDataInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderTicketSql.class, method = "selectOrderTicketInfo")
    List<LinkedHashMap<String, Object>> selectOrderTicketInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderItemNo") long orderItemNo, @Param("cancelCnt") int cancelCnt);

    @SelectProvider(type = ClaimOrderTicketSql.class, method = "selectClaimTicketInfo")
    List<LinkedHashMap<String, Object>> selectClaimTicketInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimOrderTypeCheck")
    LinkedHashMap<String, String> selectClaimOrderTypeCheck(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectCreateClaimAccumulateInfo")
    List<LinkedHashMap<String, Object>> selectCreateClaimAccumulateInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectAccumulateCancelInfo")
    LinkedHashMap<String, Object> selectAccumulateCancelInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("pointKind") String pointKind);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectOriginAndCombineClaimCheck")
    List<LinkedHashMap<String, Object>> selectOriginAndCombineClaimCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("searchType") String searchType);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimBundleTdInfo")
    LinkedHashMap<String, String> selectClaimBundleTdInfo(@Param("claimNo") long claimNo);
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectMileagePaybackCheck")
    LinkedHashMap<String, Object> selectMileagePaybackCheck(@Param("bundleNo") long bundleNo);
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectMileagePaybackInfo")
    LinkedHashMap<String, Object> selectMileagePaybackInfo(@Param("purchaseOrderNo") long purchaseOrderNo);
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectMileagePaybackCancelInfo")
    LinkedHashMap<String, Object> selectMileagePaybackCancelInfo(@Param("bundleNo") long bundleNo, @Param("orgBundleNo") long orgBundleNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectAddCouponCheck")
    List<ClaimAddCouponDto> selectAddCouponCheck(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectMultiBundleInfoForClaim")
    List<Long> selectMultiBundleInfoForClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectIsMarketOrderCheckForClaim")
    String selectIsMarketOrderCheckForClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimBasicDataForMarketOrder")
    MarketClaimBasicDataDto selectClaimBasicDataForMarketOrder(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderOptNo") long orderOptNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimStatusChangeDataForMarketOrder")
    List<LinkedHashMap<String, Object>> selectClaimStatusChangeDataForMarketOrder(@Param("claimNo") long claimNo);
}