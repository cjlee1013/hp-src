package kr.co.homeplus.claim.claim.service.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.claim.claim.mapper.ClaimPrimaryMapper;
import kr.co.homeplus.claim.claim.mapper.ClaimReadMapper;
import kr.co.homeplus.claim.claim.mapper.ClaimSecondaryMapper;
import kr.co.homeplus.claim.claim.mapper.ClaimWriteMapper;
import kr.co.homeplus.claim.claim.model.external.ClaimCouponReIssueSetDto.CouponReIssueDto;
import kr.co.homeplus.claim.claim.model.external.safety.ClaimSafetySetDto;
import kr.co.homeplus.claim.claim.model.market.MarketClaimBasicDataDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimAddCouponDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimChgPaymentRefundSetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimDetailGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimItemListDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimListItemListDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPaymentType;
import kr.co.homeplus.claim.claim.model.mp.ClaimPhonePaymentInfoDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claim.claim.model.mp.ClaimPurchaseGift;
import kr.co.homeplus.claim.claim.model.mp.ClaimReasonSearchGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimRefundType;
import kr.co.homeplus.claim.claim.model.mp.ClaimRegisterPreRefundInfoDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimSearchListGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimSearchListSetDto;
import kr.co.homeplus.claim.claim.model.register.ClaimExchShippingRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimBundleRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimInfoRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimItemRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimMultiBundleRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimMultiItemRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimPaymentRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimProcessHistoryRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimRefundRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimReqRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimShippingRegDto;
import kr.co.homeplus.claim.claim.model.register.ticket.ClaimTicketModifyDto;
import kr.co.homeplus.claim.claim.model.register.ticket.ClaimTicketRegDto;
import kr.co.homeplus.claim.claim.service.ClaimMapperService;
import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.enums.ClaimCode;
import kr.co.homeplus.claim.enums.ExceptionCode;
import kr.co.homeplus.claim.order.mapper.OrderSecondaryMapper;
import kr.co.homeplus.claim.order.model.mypage.MpOrderGiftItemListDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPaymentInfoDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipEditDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipInfoGetDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipInfoSetDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipSetGto;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SqlUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimMapperServiceImpl implements ClaimMapperService {

    private final ClaimSecondaryMapper secondaryMapper;
    private final ClaimPrimaryMapper primaryMapper;
    private final ClaimReadMapper readMapper;
    private final ClaimWriteMapper writeMapper;
    private final OrderSecondaryMapper orderSecondaryMapper;

    @Override
    public List<ClaimSearchListGetDto> getClaimSearchList(ClaimSearchListSetDto claimSearchListSetDto){

        List<ClaimSearchListGetDto> responseClaimList = secondaryMapper.selectClaimSearchList(claimSearchListSetDto);

        //claimItem - partner_id, partner_nm, store_type 정보 조회
        for(ClaimSearchListGetDto claimDto : responseClaimList){
            LinkedHashMap<String, Object>  partnerInfo = secondaryMapper.selectClaimListPartnerInfo(claimDto.getClaimBundleNo(), claimDto.getPartnerId());
            claimDto.setPartnerId(ObjectUtils.toString(partnerInfo.get("partner_id")));
            claimDto.setPartnerNm(ObjectUtils.toString(partnerInfo.get("partner_nm")));
            claimDto.setStoreType(ObjectUtils.toString(partnerInfo.get("store_type")));
        }

        return responseClaimList;
    }

    @Override
    public List<ClaimListItemListDto> getClaimListItemList(long claimBundleNo) {
        return secondaryMapper.selectClaimListSearchItemList(claimBundleNo);
    }

    @Override
    public List<ClaimItemListDto> getClaimItemList(long claimNo){
        return secondaryMapper.selectClaimSearchItemList(claimNo);
    }

    @Override
    public int getClaimSearchListCnt(ClaimSearchListSetDto claimSearchListSetDto) {
        return secondaryMapper.selectClaimSearchListCnt(claimSearchListSetDto);
    }

    @Override
    public ClaimDetailGetDto getClaimDetail(long userNo, long claimNo){
        return secondaryMapper.selectClaimDetail(userNo, claimNo);
    }

    @Override
    public List<ClaimReasonSearchGetDto> getClaimReason(String claimType){
        return secondaryMapper.selectClaimReason(claimType);
    }

    @Override
    public Boolean addNoRcvShipInfo(NoRcvShipSetGto noRcvShipSetGto) throws Exception {
        try {
            return SqlUtils.isGreaterThanZero(primaryMapper.insertNoRcvShip(noRcvShipSetGto));
        } catch (Exception e){
            log.debug("addNoRcvShipInfo SQL Exception ::: [{}]", e.getMessage());
            throw new LogicException(ExceptionCode.SYS_ERROR_CODE_9004);
        }
    }

    @Override
    public Boolean modifyNoRcvShipInfo(NoRcvShipEditDto noRcvShipEditDto) {
        return SqlUtils.isGreaterThanZero(primaryMapper.updateNoRcvShip(noRcvShipEditDto));
    }

    @Override
    public NoRcvShipInfoGetDto getNoRcvShipInfo(NoRcvShipInfoSetDto noRcvShipInfoSetDto) {
        return secondaryMapper.selectNoRcvShipInfo(noRcvShipInfoSetDto);
    }
    @Override
    public ClaimRegisterPreRefundInfoDto getRegisterPreRefundAmt (long claimNo){
        return secondaryMapper.selectRegisterPreRefundAmt(claimNo);
    }

    @Override
    public List<ClaimRefundType> getRefundTypeList (long claimNo){
        return secondaryMapper.selectRefundTypeList(claimNo);
    }

    @Override
    public List<ClaimPurchaseGift> selectClaimPurchaseGift(long claimNo){
        return secondaryMapper.selectClaimPurchaseGift(claimNo);
    }

    @Override
    public List<ClaimPaymentType> getPaymentTypeList(long purchaseOrderNo) {
        return secondaryMapper.selectPaymentTypeList(purchaseOrderNo);
    }

    @Override
    public MpOrderPaymentInfoDto getPurchaseOrderInfo(long purchaseOrderNo, long userNo) {
        return orderSecondaryMapper.selectPurchaseInfoList(purchaseOrderNo, userNo);
    }

    @Override
    public ClaimPreRefundCalcDto getClaimPreRefundCalculation(ClaimPreRefundCalcSetDto claimPreRefundCalcSetDto) {
        log.debug("ClaimPreRefundCalcSetDto :: {}", claimPreRefundCalcSetDto.getClaimType());
        if(claimPreRefundCalcSetDto.getIsOmni() && claimPreRefundCalcSetDto.getClaimType().equals("C")){
            return getClaimPreRefundOosCalculation(claimPreRefundCalcSetDto);
        }
        
        // 다중 클레임 건 여부 확인
        boolean isMulti = Boolean.FALSE;

        if(claimPreRefundCalcSetDto.getClaimItemList().size() > 1) {
            isMulti = Boolean.TRUE;
        }
        // 파라미터 용 맵 선언.
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        // 주문번호
        parameterMap.put("purchaseOrderNo", claimPreRefundCalcSetDto.getPurchaseOrderNo());
        // 상품주문번호
        parameterMap.put("orderItemNo", claimPreRefundCalcSetDto.getClaimItemList().stream().map(
            ClaimPreRefundItemList::getOrderItemNo).collect(Collectors.joining(":")));
        // 상품번호
        parameterMap.put("itemNo", claimPreRefundCalcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getItemNo).collect(Collectors.joining(":")));
        // 옵션번호
        parameterMap.put("orderOptNo", claimPreRefundCalcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderOptNo).collect(Collectors.joining(":")));
        // 클레임 수량
        parameterMap.put("claimQty", claimPreRefundCalcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getClaimQty).collect(Collectors.joining(":")));
        // 귀책사유
        parameterMap.put("whoReason", claimPreRefundCalcSetDto.getWhoReason());
        // 취소타입
        parameterMap.put("cancelType", claimPreRefundCalcSetDto.getCancelType());
        //단건 클레임만 결품 여부 값 추가
        if(!isMulti){
            //결품여부
            parameterMap.put("piOosYn", "N");
        }
        //행사미차감 여부
        parameterMap.put("piDeductPromoYn", "N");
        //할인 미차감 여부
        parameterMap.put("piDeductDiscountYn", "N");
        //배송비 미차감 여부
        parameterMap.put("piDeductShipYn", "N");
        // 동봉 여부
        parameterMap.put("encloseYn", claimPreRefundCalcSetDto.getEncloseYn());
        if(isMulti){
            log.debug("Claim Pre Refund Calculation is Multi Action....");
            return secondaryMapper.callByClaimPreRefundPriceMultiCalculation(parameterMap);
        }
        return secondaryMapper.callByClaimPreRefundPriceCalculation(parameterMap);
    }

    public ClaimPreRefundCalcDto getClaimPreRefundOosCalculation(ClaimPreRefundCalcSetDto claimPreRefundCalcSetDto) {
        // 파라미터 용 맵 선언.
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        // 주문번호
        parameterMap.put("purchaseOrderNo", claimPreRefundCalcSetDto.getPurchaseOrderNo());
        // 상품주문번호
        parameterMap.put("orderItemNo", claimPreRefundCalcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderItemNo).collect(Collectors.joining(":")));
        // 상품번호
        parameterMap.put("itemNo", claimPreRefundCalcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getItemNo).collect(Collectors.joining(":")));
        // 옵션번호
        parameterMap.put("orderOptNo", claimPreRefundCalcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderOptNo).collect(Collectors.joining(":")));
        // 클레임 수량
        parameterMap.put("claimQty", claimPreRefundCalcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getClaimQty).collect(Collectors.joining(":")));
        // 귀책사유
        parameterMap.put("whoReason", claimPreRefundCalcSetDto.getWhoReason());
        // 취소타입
        parameterMap.put("cancelType", claimPreRefundCalcSetDto.getCancelType());

        return secondaryMapper.callByClaimPreRefundPriceOosCalculation(parameterMap);
    }

    @Override
    public ClaimPreRefundCalcDto getClaimMultiPreRefundCalculation(ClaimPreRefundCalcSetDto calcSetDto) throws Exception {
        // 파라미터 용 맵 선언.
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        // 주문번호
        parameterMap.put("purchaseOrderNo", calcSetDto.getPurchaseOrderNo());
        // 상품주문번호
        parameterMap.put("orderItemNo", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderItemNo).collect(Collectors.joining(":")));
        // 상품번호
        parameterMap.put("itemNo", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getItemNo).collect(Collectors.joining(":")));
        // 옵션번호
        parameterMap.put("orderOptNo", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderOptNo).collect(Collectors.joining(":")));
        // 클레임 수량
        parameterMap.put("claimQty", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getClaimQty).collect(Collectors.joining(":")));
        // 귀책사유
        parameterMap.put("whoReason", calcSetDto.getWhoReason());
        // 취소타입
        parameterMap.put("cancelType", calcSetDto.getCancelType());
        //행사미차감 여부
        parameterMap.put("piDeductPromoYn", "N");
        //할인 미차감 여부
        parameterMap.put("piDeductDiscountYn", "N");
        //배송비 미차감 여부
        parameterMap.put("piDeductShipYn", "N");
        // 동봉 여부
        parameterMap.put("encloseYn", "N");
        // 멀티번들번호리스트(전체취소)
        parameterMap.put("piMultiBundleNo", "0");

        return secondaryMapper.callByClaimMultiPreRefundPriceCalculation(parameterMap);
    }

    /**
     * 클레임 번들 생성을 위한 데이터 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return HashMap<String, Object> 클레임번들 기본정보
     */
    @Override
    public LinkedHashMap<String, Object> getCreateClaimBundleInfo(long purchaseOrderNo, long bundleNo, long userNo) {
        return readMapper.selectCreateClaimDataInfo(purchaseOrderNo, bundleNo, userNo);
    }

    /**
     * 클레임 아이템 생성을 위한 데이터 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param orderItemNo 상품주문번호
     * @return HashMap<String, Object> 클레임 아이템 기본정보
     */
    @Override
    public LinkedHashMap<String, Object> getClaimItemInfo(long purchaseOrderNo, long orderItemNo) {
        return readMapper.selectClaimItemInfo(purchaseOrderNo, orderItemNo);
    }

    /**
     * 클레임 마스터 등록
     *
     * @param claimMstRegDto 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimMstInfo(ClaimMstRegDto claimMstRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimMst(claimMstRegDto));
    }


    /**
     * 클레임 번들 등록
     *
     * @param claimBundleRegDto 클레임 번들 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimBundleInfo(ClaimBundleRegDto claimBundleRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimBundle(claimBundleRegDto));
    }

    /**
     * 클레임 다중배송 번들 등록
     *
     * @param claimMultiBundleRegDto 클레임 번들 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimMultiBundleInfo(ClaimMultiBundleRegDto claimMultiBundleRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimMultiBundle(claimMultiBundleRegDto));
    }

    /**
     * 클레임 요청 등록
     *
     * @param claimReqRegDto 클레임 요청 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimReqInfo(ClaimReqRegDto claimReqRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimReq(claimReqRegDto));
    }

    /**
     * 클레임 아이템 정보 등록
     *
     * @param claimItemRegDto 클레임 아이템 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimItemInfo(ClaimItemRegDto claimItemRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimItemReq(claimItemRegDto));
    }

    @Override
    public Boolean addClaimMultiItemInfo(ClaimMultiItemRegDto claimMultiItemRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimMultiItem(claimMultiItemRegDto));
    }

    /**
     * 클레임 결제 취소를 위한 원주문 결제 정보 조회
     *
     * @param paymentNo 결제 번호
     * @param paymentType 결제 타입
     * @return 클레임 결제 취소를 위한 원주문 결제 정보 맵
     */
    @Override
    public LinkedHashMap<String, Object> getOriPaymentInfoForClaim(long paymentNo, String paymentType){
        if(paymentType.equals("FREE")){
            paymentType = null;
        }
        return readMapper.selectOriPaymentForClaim(paymentNo, paymentType);
    }

    /**
     * 클레임 결제 취소 정보 등록
     *
     * @param claimPaymentRegDto 클레임 결제 취소 정보
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimPaymentInfo(ClaimPaymentRegDto claimPaymentRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimPayment(claimPaymentRegDto));
    }

    /**
     * 클레임 아이템 옵션별 등록을 위한 데이터 조회
     *
     * @param orderItemNo 아이템주문번호
     * @param orderOptNo 옵션번호
     * @return 클레임 옵션 등록 정보
     */
    @Override
    public ClaimOptRegDto getOriginOrderOptInfoForClaim(long orderItemNo, String orderOptNo){
        return readMapper.selectOrderOptInfoForClaim(orderItemNo, orderOptNo);
    }

    /**
     * 클레임 옵션별 등록
     *
     * @param claimOptRegDto 클레임 옵션별 데이터 DTO
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimOptInfo(ClaimOptRegDto claimOptRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimOpt(claimOptRegDto));
    }

    /**
     * 클레임 - 결제취소를 위한 데이터 조회
     *
     * @param claimNo 클레임 번호
     * @return LinkedHashMap<String, Object> 결제취소용 데이터
     */
    @Override
    public List<LinkedHashMap<String, Object>> getPaymentCancelInfo(long claimNo) {
        return writeMapper.selectPaymentCancelInfo(claimNo);
    }

    /**
     * 클레임 - 클레임상태 변경 업데이트
     *
     *
     * @param parameter@return Boolean 클레임상태변경 업데이트 결과
     */
    @Override
    public Boolean setClaimStatus(LinkedHashMap<String, Object> parameter) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimStatus(parameter));
    }

    /**
     * 클레임 - 클레임 결제 취소 상태 업데이트
     *
     * @param claimPaymentNo 클레임 페이먼트 번호
     * @param claimCode 클레임 코드
     * @return Boolean 클레임 결제취소상태 업데이트 결과
     */
    @Override
    public Boolean setClaimPaymentStatus(long claimPaymentNo, String claimApprovalCd, ClaimCode claimCode) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimPaymentStatus(claimPaymentNo, claimApprovalCd, claimCode));
    }

    @Override
    public LinkedHashMap<String, Object> callByEndClaimFlag(ClaimInfoRegDto claimInfoRegDto) {
        return writeMapper.callByClaimReg(claimInfoRegDto);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getOrderBundleByClaim(long purchaseOrderNo) {
        return readMapper.selectOrderBundleByClaim(purchaseOrderNo);
    }

    @Override
    public List<ClaimPreRefundItemList> getClaimPreRefundItemInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectOrderItemInfoByClaim(purchaseOrderNo, bundleNo);
    }

    @Override
    public LinkedHashMap<String, Object> getOrderByClaimPartInfo(long purchaseOrderNo, long bundleNo, long userNo) {
        return readMapper.selectOrderByClaimPartInfo(purchaseOrderNo, bundleNo, userNo);
    }


    /**
     * 클레임 처리내역/히스토리 등록
     *
     * @param  claimProcessHistoryRegDto 클레임 처리내역/히스토리 등록 dto
     * @return Boolean 클레임 히스토리 등록 결과
     */
    @Override
    public Boolean setClaimProcessHistory(ClaimProcessHistoryRegDto claimProcessHistoryRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimProcessHistory(claimProcessHistoryRegDto));
    }

    @Override
    public Boolean addClaimPickShippingInfo(ClaimShippingRegDto shippingRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimPickShipping(shippingRegDto));
    }

    @Override
    public Boolean addClaimExchShipping(ClaimExchShippingRegDto exchShippingRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimExchShipping(exchShippingRegDto));
    }

    @Override
    public Boolean setClaimRefund(ClaimRefundRegDto claimRefundRegDtoo) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimRefund(claimRefundRegDtoo));
    }

    @Override
    public int getAlreadyRegClaimRefundCnt(ClaimChgPaymentRefundSetDto claimChgPaymentRefundSetDto) {
        return readMapper.selectAlreadyRegClaimRefundCnt(claimChgPaymentRefundSetDto);
    }

    @Override
    public ClaimPhonePaymentInfoDto getClaimPhonePaymentInfo(ClaimChgPaymentRefundSetDto claimChgPaymentRefundSetDto) {
        return readMapper.selectClaimPhonePaymentInfo(claimChgPaymentRefundSetDto);
    }

    @Override
    public List<MpOrderGiftItemListDto> getOrderGiftItemList(long purchaseOrderNo, long bundleNo) {
        return orderSecondaryMapper.selectOrderGiftItemList(purchaseOrderNo, bundleNo);
    }

    @Override
    public void callByClaimAdditionReg(long claimNo) {
        LinkedHashMap<String, Object> returnMap = writeMapper.callByClaimAdditionReg(claimNo);
        log.debug("callByClaimAdditionReg ::: {}", returnMap);
    }

    @Override
    public void callByClaimItemEmpDiscount(long claimNo, long orderItemNo) {
        writeMapper.callByClaimItemEmpDiscount(claimNo, orderItemNo);
    }

    @Override
    public LinkedHashMap<String, Object> getCreateClaimMstInfo(long purchaseOrderNo, long userNo) {
        return readMapper.selectCreateClaimMstDataInfo(purchaseOrderNo, userNo);
    }

    @Override
    public LinkedHashMap<String, Object> getClaimPossibleCheck(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectClaimPossibleCheck(purchaseOrderNo, bundleNo);
    }

    @Override
    public List<CouponReIssueDto> getClaimCouponReIssueInfo(long claimNo, long userNo) {
        return readMapper.selectClaimCouponReIssue(claimNo, userNo);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getClaimInfo(long claimNo) {
        return readMapper.selectClaimInfo(claimNo);
    }

    @Override
    public LinkedHashMap<String, Object> getClaimPickIsAutoInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectClaimPickIsAutoInfo(purchaseOrderNo, bundleNo);
    }

    @Override
    public Boolean modifyClaimCouponReIssueResult(List<Long> claimAdditionNo) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimCouponReIssueResult(claimAdditionNo));
    }

    @Override
    public Boolean modifyClaimBundleSlotResult(long claimBundleNo){
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimBundleSlotResult(claimBundleNo));
    }

    @Override
    public LinkedHashMap<String, Object> getClaimSendDataInfo(long claimNo){
        return readMapper.selectClaimSendDataInfo(claimNo);
    }

    @Override
    public ClaimSafetySetDto getClaimSafetyData(long claimBundleNo, String issueType) {
        return writeMapper.selectClaimSafetyIssueData(claimBundleNo, issueType);
    }

    @Override
    public ClaimInfoRegDto getClaimRegData(long claimNo) {
        return readMapper.selectClaimRegDataInfo(claimNo);
    }

    @Override
    public Boolean modifyClaimPaymentOcbTransactionId(long claimPaymentNo, String pgMid) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimPaymentOcbTransactionId(claimPaymentNo, pgMid));
    }

    @Override
    public Boolean modifyClaimPaymentPgCancelTradeId(long claimPaymentNo, String cancelTradeId) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimPaymentPgCancelTradeId(claimPaymentNo, cancelTradeId));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getOrderTicketInfo(long purchaseOrderNo, long orderItemNo, int cancelCnt) {
        return readMapper.selectOrderTicketInfo(purchaseOrderNo, orderItemNo, cancelCnt);
    }

    @Override
    public Boolean addClaimTicketInfo(ClaimTicketRegDto claimTicketRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimTicket(claimTicketRegDto));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getClaimTicketInfo(long claimNo) {
        return readMapper.selectClaimTicketInfo(claimNo);
    }

    @Override
    public LinkedHashMap<String, String> getClaimOrderTypeCheck(long claimNo) {
        return readMapper.selectClaimOrderTypeCheck(claimNo);
    }

    @Override
    public Boolean modifyOrderTicketInfo(ClaimTicketModifyDto modifyDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateOrderTicketStatus(modifyDto));
    }

    @Override
    public Boolean setTotalClaimPaymentStatus(long claimNo, ClaimCode claimCode){
        return SqlUtils.isGreaterThanZero(writeMapper.updateTotalClaimPaymentStatus(claimNo, claimCode));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getCreateClaimAccumulateInfo(long claimNo){
        return readMapper.selectCreateClaimAccumulateInfo(claimNo);
    }

    @Override
    public LinkedHashMap<String, Object> getAccumulateCancelInfo(long purchaseOrderNo, String pointKind){
        return readMapper.selectAccumulateCancelInfo(purchaseOrderNo, pointKind);
    }

    @Override
    public Boolean addClaimAccumulate(LinkedHashMap<String, Object> parameterMap) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimAccumulate(parameterMap));
    }

    @Override
    public Boolean modifyClaimAccumulateReqSend(long claimNo, String reqSendYn, String pointKind){
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimAccumulateReqSend(claimNo, reqSendYn, pointKind));
    }

    @Override
    public Boolean modifyClaimAccumulateReqResult(LinkedHashMap<String, Object> parameterMap){
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimAccumulate(parameterMap));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getOrderCombineClaimCheck(String purchaseOrderNo, String type) {
        return readMapper.selectOriginAndCombineClaimCheck(Long.parseLong(purchaseOrderNo), type);
    }

    @Override
    public LinkedHashMap<String, String> getClaimTdBundleInfo(long claimNo) {
        return readMapper.selectClaimBundleTdInfo(claimNo);
    }

    @Override
    public LinkedHashMap<String, Object> getMileagePaybackCheck(long bundleNo) {
        return readMapper.selectMileagePaybackCheck(bundleNo);
    }

    @Override
    public LinkedHashMap<String, Object> getOrderCombinePaybackCheck(long purchaseOrderNo) {
        return readMapper.selectMileagePaybackInfo(purchaseOrderNo);
    }

    @Override
    public LinkedHashMap<String, Object> getClaimCombinePaybackCheck(long bundleNo, long orgBundleNo) {
        return readMapper.selectMileagePaybackCancelInfo(bundleNo, orgBundleNo);
    }

    @Override
    public boolean setShipPaybackResult(long paybackNo) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateMileagePaybackResult(paybackNo));
    }

    @Override
    public List<ClaimAddCouponDto> isAddCouponPossible(long purchaseOrderNo) {
        return readMapper.selectAddCouponCheck(purchaseOrderNo);
    }

    @Override
    public List<Long> getMultiBundleInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectMultiBundleInfoForClaim(purchaseOrderNo, bundleNo);
    }

    @Override
    public List<ClaimMultiItemRegDto> getClaimMultiItemCreateInfo(long claimNo, long multiBundleNo) {
        return writeMapper.selectClaimMultiItemCreateInfo(claimNo, multiBundleNo);
    }

    @Override
    public LinkedHashMap<String, Object> getNonShipCompleteInfo(long purchaseOrderNo, long bundleNo) {
        return secondaryMapper.selectNonShipCompleteInfo(purchaseOrderNo, bundleNo);
    }

    @Override
    public boolean isOrderMarketCheck(long purchaseOrderNo, long claimNo) {
        String isOrderMarket = readMapper.selectIsMarketOrderCheckForClaim(purchaseOrderNo, claimNo);
        return StringUtils.isNotEmpty(isOrderMarket) && isOrderMarket.equals("Y");
    }

    @Override
    public MarketClaimBasicDataDto getClaimBasicDataForMarketOrder(long purchaseOrderNo, long orderOptNo) {
        return readMapper.selectClaimBasicDataForMarketOrder(purchaseOrderNo, orderOptNo);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getClaimStatusChangeDataForMarket(long claimNo) {
        return readMapper.selectClaimStatusChangeDataForMarketOrder(claimNo);
    }
}
