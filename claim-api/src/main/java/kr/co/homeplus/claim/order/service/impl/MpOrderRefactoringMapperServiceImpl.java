package kr.co.homeplus.claim.order.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.order.mapper.OrderRefactoringMapper;
import kr.co.homeplus.claim.order.mapper.OrderSecondaryMapper;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListSetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPresentDto;
import kr.co.homeplus.claim.order.model.order.MpOrderBundleShipInfoDto;
import kr.co.homeplus.claim.order.model.order.MpOrderItemDto;
import kr.co.homeplus.claim.order.model.order.MypageOrderBundleListDto;
import kr.co.homeplus.claim.order.model.order.MypageOrderListGetDto;
import kr.co.homeplus.claim.order.model.order.OrderClaimInfoDto;
import kr.co.homeplus.claim.order.service.MpOrderRefactoringMapperService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MpOrderRefactoringMapperServiceImpl implements MpOrderRefactoringMapperService {

    private final OrderRefactoringMapper mapper;
    private final OrderSecondaryMapper readMapper;


    @Override
    public List<MypageOrderListGetDto> getMypageOrderList(MpOrderListSetDto setDto) {
        return mapper.selectMypageOrderList(setDto);
    }

    @Override
    public List<MypageOrderBundleListDto> getMypageOrderBundleList(long purchaseOrderNo) {
        return mapper.selectMypageOrderBundleList(purchaseOrderNo);
    }

    @Override
    public MpOrderBundleShipInfoDto getShipInfoByBundle(long purchaseOrderNo, long bundleNo){
        return mapper.selectMypageOrderBundleShipInfo(purchaseOrderNo, bundleNo);
    }

    @Override
    public List<MpOrderItemDto> getMypageOrderItemList(long purchaseOrderNo, long bundleNo){
        return this.getOrderItemList(purchaseOrderNo, bundleNo, "NONE");
    }

    @Override
    public List<MpOrderItemDto> getMypageOrderItemListByCombine(long purchaseOrderNo, long bundleNo) {
        List<MpOrderItemDto> itemList = this.getOrderItemList(purchaseOrderNo, bundleNo, "COMBINE");
        if(itemList != null){
            long combinePurchaseOrderNo = itemList.stream().mapToLong(MpOrderItemDto::getPurchaseOrderNo).distinct().toArray()[0];
            long combineBundleNo = itemList.stream().mapToLong(MpOrderItemDto::getBundleNo).distinct().toArray()[0];
            List<MpOrderItemDto> substitutionItemList = this.getMypageOrderItemListBySubstitution(combinePurchaseOrderNo, combineBundleNo);
            if(substitutionItemList != null){
                itemList.addAll(substitutionItemList);
            }
        }
        return itemList;
    }

    @Override
    public List<MpOrderItemDto> getMypageOrderItemListBySubstitution(long purchaseOrderNo, long bundleNo) {
        return this.getOrderItemList(purchaseOrderNo, bundleNo, "SUB");
    }

    @Override
    public List<OrderClaimInfoDto> getOrderClaimInfoByBundle(long purchaseOrderNo, long bundleNo){
        log.info("getOrderClaimInfoByBundle :: purchaseOrderNo : {} bundleNo : {}", purchaseOrderNo, bundleNo);
        List<OrderClaimInfoDto> resultDto = mapper.selectMypageOrderClaimInfo(purchaseOrderNo, bundleNo);
        if(resultDto.size() == 0){
            OrderClaimInfoDto claimInfoDto = new OrderClaimInfoDto();
            claimInfoDto.setClaimProcessYn("N");
            claimInfoDto.setIsRefundFail("N");
            return new ArrayList<>(){{ add(claimInfoDto); }};
        }
        return resultDto;
    }

    @Override
    public MpOrderPresentDto getMpOrderPresentInfo(long purchaseOrderNo){
        return readMapper.selectOrderPresentInfo(purchaseOrderNo);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getAutoCancelTicketCheck(long bundleNo){
        return mapper.selectAutoCancelTicketInfo(bundleNo);
    }

    @Override
    public String getClaimRefundFailCheck(long claimNo) {
        return mapper.selectClaimRefundFailCheck(claimNo);
    }

    private List<MpOrderItemDto> getOrderItemList(long purchaseOrderNo, long bundleNo, String searchType) {
        return mapper.selectMypageOrderItemList(purchaseOrderNo, bundleNo, searchType);
    }
}
