package kr.co.homeplus.claim.order.model.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Builder
@ApiModel(description = "번들별 버튼정보 DTO")
public class OrderButtonDto {
    @ApiModelProperty(value = "배송관련버튼정보")
    private OrderShipButtonDto shipButtonInfo;
    @ApiModelProperty(value = "클레임관련버튼정보")
    private OrderClaimButtonDto claimButtonInfo;

    @Data
    @Builder
    @Slf4j
    @ApiModel(description = "클레임 버튼정보 DTO")
    public static class OrderClaimButtonDto {
        @ApiModelProperty(value = "주문취소", position = 1)
        private String isPossibleOrderCancel;
        @ApiModelProperty(value = "취소신청", position = 2)
        private String isPossibleCancel;
        @ApiModelProperty(value = "반품신청", position = 3)
        private String isPossibleReturn;
        @ApiModelProperty(value = "교환신청", position = 4)
        private String isPossibleExchange;
    }

    @Data
    @Builder
    @ApiModel(description = "배송 버튼정보 DTO")
    public static class OrderShipButtonDto {
        @ApiModelProperty(value = "합배송", position = 1)
        private String isPossibleCombine;
        @ApiModelProperty(value = "배송시간변경", position = 2)
        private String isPossibleChangeShipDt;
        @ApiModelProperty(value = "배송조회", position = 3)
        private String isPossibleSearchShip;
        @ApiModelProperty(value = "배송예정일", position = 4)
        private String isPossibleExpectedDate;
        @ApiModelProperty(value = "배송안내", position = 5)
        private String isPossibleShipInfo;
        @ApiModelProperty(value = "배송지연안내", position = 6)
        private String isPossibleDelayShip;
        @ApiModelProperty(value = "미수취신고", position = 7)
        private String isPossibleNotReceiveShip;
        @ApiModelProperty(value = "수취확인", position = 8)
        private String isPossibleCompleteShip;
        @JsonIgnore
        @ApiModelProperty(value = "리뷰쓰기", position = 9, hidden = true)
        private String isPossibleReview;
        @ApiModelProperty(value = "미수취내역", position = 10)
        private String isPossibleNotReceiverShipList;
        @ApiModelProperty(value = "미수취철회", position = 11)
        private String isPossibleNotReceiverReject;
    }
}
