package kr.co.homeplus.claim.order.model.mypage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MultiOrderShipAddrEditDto {
    @ApiModelProperty(value = "배송번호", position = 1)
    private long bundleNo;
    @ApiModelProperty(value = "주문번호", position = 2)
    private long purchaseOrderNo;
    @ApiModelProperty(value = "멀티번들번호", position = 3)
    private long multiBundleNo;
    @ApiModelProperty(value = "이름", position = 4)
    private String receiverNm;
    @ApiModelProperty(value = "연락처", position = 6)
    private String shipMobileNo;
    @ApiModelProperty(value = "우편번호", position = 7)
    private String zipCode;
    @ApiModelProperty(value = "도로명", position = 8)
    private String roadBaseAddr;
    @ApiModelProperty(value = "도로명상세", position = 9)
    private String roadDetailAddr;
    @ApiModelProperty(value = "지번", position = 10)
    private String baseAddr;
    @ApiModelProperty(value = "지번상세", position = 11)
    private String detailAddr;
}
