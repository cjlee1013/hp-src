package kr.co.homeplus.claim.entity.dms;

public class ItmPartnerSellerDeliveryEntity {
    //
    public String partnerId;
    //
    public String dlvCd;
    //
    public String creditCd;
    //
    public String useYn;
    //
    public String regDt;
    //
    public String regId;
    //
    public String chgDt;
    //
    public String chgId;
}
