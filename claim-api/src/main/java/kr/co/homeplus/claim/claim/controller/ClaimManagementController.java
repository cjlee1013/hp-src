package kr.co.homeplus.claim.claim.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import kr.co.homeplus.claim.claim.model.ClaimRequestSetDto;
import kr.co.homeplus.claim.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.claim.claim.service.ClaimOrderCancelService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/claim")
@RequiredArgsConstructor
@Slf4j
@Api(tags = "클레임 관리(취소/반품/교환) ")
public class ClaimManagementController {

    private final ClaimOrderCancelService orderCancelService;

    @PostMapping("/claimCancelReg")
    @ApiOperation(value = "클레임 등록", response = String.class)
    public ResponseObject<Long> getClaimPrevOrderCancelInfo(@RequestBody @Valid ClaimRegSetDto claimRegSetDto) throws Exception {
        claimRegSetDto.setIsMultiOrder(Boolean.FALSE);
        return orderCancelService.orderCancelAction(claimRegSetDto);
    }

    @PostMapping("/reqChange")
    @ApiOperation(value = "클레임 상태 변경 요청", response = Object.class)
    public ResponseObject<Object> reqChange(@RequestBody @Valid ClaimRequestSetDto reqDto) throws Exception {
        return orderCancelService.requestChangeByClaim(reqDto);
    }

    @PostMapping("/multiClaimCancelReg")
    @ApiOperation(value = "클레임 등록(다중배송주문용)", response = String.class)
    public ResponseObject<Long> getClaimPrevMultiOrderCancelInfo(@RequestBody @Valid ClaimRegSetDto claimRegSetDto) throws Exception {
        claimRegSetDto.setIsMultiOrder(Boolean.TRUE);
        return orderCancelService.orderCancelAction(claimRegSetDto);
    }
}
