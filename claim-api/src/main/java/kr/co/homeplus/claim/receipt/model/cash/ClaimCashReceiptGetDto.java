package kr.co.homeplus.claim.receipt.model.cash;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "주문_현금구매영수증")
public class ClaimCashReceiptGetDto {

    @ApiModelProperty(value = "현금영수증 유형(PHONE:휴대폰번호,CARD:현금영수증카드번호)", position = 1)
    private String cashReceiptType;

    @ApiModelProperty(value = "현금영수증 신청번호", position = 2)
    private String cashReceiptReqNo;

    @ApiModelProperty(value = "현금영수증 용도(PERS:개인소득공제용,EVID:지출증빙용,NONE:미발행)", position = 3)
    private String cashReceiptUsage;

    @ApiModelProperty(value = "클레임번호", position = 4)
    private long claimNo;

    @ApiModelProperty(value = "취소시당초거래일", position = 5)
    private String paymentFshDt;

    @ApiModelProperty(value = "취소일시", position = 6)
    private String completeDt;

    @ApiModelProperty(value = "결제수단", position = 7)
    private String methodNm;

    @ApiModelProperty(value = "승인번호", position = 8)
    private String approvalNo;

    @ApiModelProperty(value = "상품명", position = 9)
    private String itemNm;

    @ApiModelProperty(value = "금액", position = 10)
    private long payAmt;

    @ApiModelProperty(value = "부가세", position = 11)
    private long totVatAmt;

    @ApiModelProperty(value = "합계", position = 12)
    private long totPayAmt;

    /** 전표 판매자 정보 노출 시 사용 **/
    @ApiModelProperty(value = "파트너명", position = 13)
    private String partnerNm;

    @ApiModelProperty(value = "대표자명", position = 14)
    private String partnerOwner;

    @ApiModelProperty(value = "사업자등록번호", position = 15)
    private String partnerNo;

    @ApiModelProperty(value = "고객센터", position = 16)
    private String partnerInfoCenter;

    @ApiModelProperty(value = "주문번호", position = 17)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "현금영수증발행일", position = 18)
    private String cashReceiptIssueDt;

    @ApiModelProperty(value = "배송번호", position = 19)
    private long bundleNo;
}
