package kr.co.homeplus.claim.review.service;

import java.util.List;
import kr.co.homeplus.claim.review.model.ReviewSearchGetDto;
import kr.co.homeplus.claim.review.model.ReviewSearchSetDto;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.client.model.ResponsePagination;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReviewService {

    private final ReviewMapperService mapperService;

    public ResponseObject<List<ReviewSearchGetDto>> getReviewInfo(ReviewSearchSetDto setDto) {
        List<ReviewSearchGetDto> reviewList = mapperService.getReviewInfoList(setDto);
        for(ReviewSearchGetDto dto : reviewList) {
            long bundleNo = ObjectUtils.toLong(dto.getBundleNo());
            if(dto.getStoreType().equalsIgnoreCase("GHS")){
                dto.setReviewItem(mapperService.getGhsReviewItemList(bundleNo));
                dto.setStoreType("HYPER");
            } else {
                dto.setReviewItem(mapperService.getReviewItemList(dto.getPurchaseOrderNo(), bundleNo));
            }
        }
        int page = setDto.getPage();
        setDto.setPage(0);
        int totalCount = mapperService.getReviewInfoList(setDto).size();
        return ResourceConverter.toResponseObject(
            reviewList,
            ResponsePagination.Builder.builder().setAsPage(page, setDto.getPerPage(), getTotalPage(totalCount, setDto.getPerPage())).totalCount(totalCount).build()
        );
    }

    private Integer getTotalPage(int totalCount, int perPage) {
        int totalPage = totalCount / perPage;
        if(totalCount % perPage > 0){
            totalPage ++;
        }
        return totalPage;
    }

}