package kr.co.homeplus.claim.entity.order;

public class OrderUserRecentlyInfoEntity {
    // 고객번호
    public String userNo;
    // 점포배송메시지코드
    public String storeShipMsgCd;
    // 점포배송메시지
    public String storeShipMsg;
    // 택배배송메시지코드
    public String dlvShipMsgCd;
    // 택배배송메시지
    public String dlvShipMsg;
    // 공동현관 출입방법 코드
    public String auroraShipMsgCd;
    // 공동현관 출입방법 메세지
    public String auroraShipMsg;
    // 상위결제수단코드(CARD,RBANK…)
    public String parentMethodCd;
    // 결제수단코드(H010…)
    public String methodCd;
    // 현금영수증 용도(PERS:개인소득공제용,EVID:지출증빙용,NONE:미발행)
    public String cashReceiptUsage;
    // 현금영수증 유형(PHONE:휴대폰번호,CARD:현금영수증카드번호,BUSINESSNO:사업자번호)
    public String cashReceiptType;
    // 현금영수증 신청번호
    public String cashReceiptReqNo;
    // 현금영수증 사용여부
    public String cashReceiptUseYn;
    // 메타필드1
    public String metaField1;
    // 메타필드2
    public String metaField2;
    // 메타필드3
    public String metaField3;
    // 메타필드4
    public String metaField4;
    // 등록일시
    public String regDt;
    // 수정일시
    public String chgDt;
}
