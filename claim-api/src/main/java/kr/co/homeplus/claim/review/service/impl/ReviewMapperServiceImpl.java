package kr.co.homeplus.claim.review.service.impl;

import java.util.List;
import kr.co.homeplus.claim.review.mapper.ReviewReadMapper;
import kr.co.homeplus.claim.review.model.ReviewItemDto;
import kr.co.homeplus.claim.review.model.ReviewSearchGetDto;
import kr.co.homeplus.claim.review.model.ReviewSearchSetDto;
import kr.co.homeplus.claim.review.service.ReviewMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ReviewMapperServiceImpl implements ReviewMapperService {

    private final ReviewReadMapper reviewReadMapper;

    @Override
    public List<ReviewSearchGetDto> getReviewInfoList(ReviewSearchSetDto setDto) {
        return reviewReadMapper.selectReviewPossibleList(setDto);
    }

    @Override
    public List<ReviewItemDto> getReviewItemList(long purchaseOrderNo, long bundleNo) {
        return reviewReadMapper.selectReviewPossibleItemList(purchaseOrderNo, bundleNo);
    }

    @Override
    public List<ReviewItemDto> getGhsReviewItemList(long ordNo) {
        return reviewReadMapper.selectGhsReviewPossibleItemList(ordNo);
    }
}
