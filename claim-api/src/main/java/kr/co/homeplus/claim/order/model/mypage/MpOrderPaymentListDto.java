package kr.co.homeplus.claim.order.model.mypage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "주문/배송 상세 - 결제 정보 리스트 DTO")
public class MpOrderPaymentListDto {

    @ApiModelProperty(value = "결제정보", position = 1)
    private MpOrderPaymentInfoDto orderPaymentInfo;

    @ApiModelProperty(value = "클레임정보", position = 2)
    private MpOrderClaimPaymentInfoDto claimPaymentInfo;

}
