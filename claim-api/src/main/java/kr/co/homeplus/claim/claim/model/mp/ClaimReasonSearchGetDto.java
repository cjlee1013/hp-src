package kr.co.homeplus.claim.claim.model.mp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "클레임 사유 조회 응답 DTO")
public class ClaimReasonSearchGetDto {

    @ApiModelProperty(value = "클레임사유코드", position = 1)
    private String claimReasonType;

    @ApiModelProperty(value = "클레임사유설명", position = 2)
    private String claimReasonTypeDetail;

    @ApiModelProperty(value = "귀책사유(B:고객/S:판매자/H:홈플러스)", position = 3)
    private String whoReason;

    @ApiModelProperty(value = "취소타입(O:주문취소/R:반품/X:교환)", position = 4)
    @Pattern(regexp = "O|R|X", message = "취소타입")
    private String cancelType;

    @ApiModelProperty(value = "클레임타입(C:취소/R:반품/X:교환)", position = 5)
    private String claimType;

    @ApiModelProperty(value = "상세사유 필수 여부(Y:필수/N:필수아님)", position = 6)
    private String detailReasonRequired;

}
