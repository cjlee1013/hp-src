package kr.co.homeplus.claim.entity.ship;

public class ShippingAddrEntity {
    // 배송주소번호
    public String shipAddrNo;
    //
    public String bundleNo;
    // 받는사람명
    public String receiverNm;
    // 우편번호
    public String zipcode;
    // 기본주소
    public String baseAddr;
    // 상세주소
    public String detailAddr;
    // 도로명 기본주소
    public String roadBaseAddr;
    // 도로명 상세주소
    public String roadDetailAddr;
    // 배송모바일번호(휴대폰번호)
    public String shipMobileNo;
    // 법인주문여부 (화면 체크박스 체크여부 (Y:체크,N:체크안함))
    public String corpOrderYn;
    // 점포배송 메시지코드
    public String storeShipMsgCd;
    // 점포배송 메시지
    public String storeShipMsg;
    // 택배배송 메시지코드
    public String dlvShipMsgCd;
    // 택배배송 메시지
    public String dlvShipMsg;
    // 새벽배송메시지코드
    public String auroraShipMsgCd;
    // 새벽배송메시지
    public String auroraShipMsg;
    // 안심번호사용여부(Y:사용,N:미사용)
    public String safetyUseYn;
    // 개인통관고유부호
    public String personalOverseaNo;
    // 선물메시지
    public String giftMsg;
    // 픽업장소주소텍스트(화면노출용)
    public String placeAddrTxt;
    // DS선물메시지
    public String dsGiftMsg;
    // 등록일시
    public String regDt;
    // 수정일시
    public String chgDt;
    // 수정자ID
    public String chgId;
}
