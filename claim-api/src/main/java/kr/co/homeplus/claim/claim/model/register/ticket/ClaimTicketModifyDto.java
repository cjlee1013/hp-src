package kr.co.homeplus.claim.claim.model.register.ticket;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClaimTicketModifyDto {
    private String ticketStatus;
    private String issueStatus;
    private long claimTicketNo;
}
