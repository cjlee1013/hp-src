package kr.co.homeplus.claim.claim.model.mp;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "취소/반품/교환 내역 확인 리스트 응답 DTO")
public class ClaimSearchListGetDto {

    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "원주문번호", position = 2)
    private String orgPurchaseOrderNo;

    @ApiModelProperty(value = "신청일자", position = 2)
    private String requestDt;

    @ApiModelProperty(value = "주문일시", position = 3)
    private String orderDt;

    @ApiModelProperty(value = "클레임번들번호", position = 4)
    private long claimBundleNo;

    @ApiModelProperty(value = "점포유형(HYPER,CLUB,AURORA,EXP,DS", position = 5)
    private String storeType;

    @ApiModelProperty(value = "파트너ID", position = 6)
    private String partnerId;

    @ApiModelProperty(value = "파트너명", position = 7)
    private String partnerNm;

    @ApiModelProperty(value = "배송방법 (DS_DLV:택배배송,DS_DRCT:직접배송,DS_PICK:방문수령/픽업,DS_POST:우편배송,DS_QUICK:퀵배송,TD_DLV:택배배송,TD_DRCT:직접배송,TD_PICK:방문수령,TD_POST:우편배송,TD_QUICK:퀵배송", position = 8)
    private String shipMethod;

    @ApiModelProperty(value = "클레임타입", position = 9)
    private String claimType;

    @ApiModelProperty(value = "클레임타입코드", position = 10)
    private String claimTypeCode;

    @ApiModelProperty(value = "클레임상태", position = 11)
    private String claimStatus;

    @ApiModelProperty(value = "클레임상태코드", position = 12)
    private String claimStatusCode;

    @ApiModelProperty(value = "배송타입 (TD,DS,AURORA,PICK,RESERVE,MULTI,SUM)", position = 13)
    private String shipType;

    @ApiModelProperty(value = "전체('N')/부분취소('Y') 여부", position = 14)
    private String claimPartYn;

    @ApiModelProperty(value = "클레임 번호", position = 15)
    private long claimNo;

    @ApiModelProperty(value = "반품수거상태", position = 16)
    private String pickStatus;

    @ApiModelProperty(value = "교환배송상태", position = 17)
    private String exchStatus;

    @ApiModelProperty(value = "클레임 반품수거 번호", position = 18)
    private String claimPickShippingNo;

    @ApiModelProperty(value = "클레임 교환배송 번호", position = 19)
    private String claimExchShippingNo;

    @ApiModelProperty(value = "교환배송 완료일", position = 20)
    private String exchD3Dt;

    @ApiModelProperty(value = "수거 완료일", position = 21)
    private String pickP3Dt;

    @ApiModelProperty(value = "반품수거방법", position = 22)
    private String pickShipType;

    @ApiModelProperty(value = "반품수거 택배사 코드", position = 23)
    private String pickDlvCd;

    @ApiModelProperty(value = "반품수거 송장번호", position = 24)
    private String pickInvoiceNo;

    @ApiModelProperty(value = "교환배송방법", position = 25)
    private String exchShipType;

    @ApiModelProperty(value = "교환배송 택배사 코드", position = 26)
    private String exchDlvCd;

    @ApiModelProperty(value = "교환배송 송장번호", position = 27)
    private String exchInvoiceNo;

    @ApiModelProperty(value="클레임 상품 리스트", position = 28)
    private List<ClaimListItemListDto> claimItemList;

    @ApiModelProperty(value="환불수단 리스트", position = 29)
    public List<ClaimRefundType> claimRefundTypeList;



}
