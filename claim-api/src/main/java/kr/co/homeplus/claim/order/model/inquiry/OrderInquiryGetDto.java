package kr.co.homeplus.claim.order.model.inquiry;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
public class OrderInquiryGetDto {
    @ApiModelProperty(value = "원주문번호", position = 1)
    private String orgPurchaseOrderNo;
    @ApiModelProperty(value = "배송번호", position = 2)
    private long bundleNo;
    @ApiModelProperty(value = "주문일시", position = 3)
    private String orderDt;
    @ApiModelProperty(value = "점포유형(HYPER, CLUB, AURORA, EXP, DS)", position = 4)
    private String storeType;
    @ApiModelProperty(value = "파트너ID", position = 5)
    private String partnerId;
    @ApiModelProperty(value = "아아템리스트", position = 5)
    private List<InquiryItemDto> itemList;
}
