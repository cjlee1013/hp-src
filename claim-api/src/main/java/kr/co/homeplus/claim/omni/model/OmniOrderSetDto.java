package kr.co.homeplus.claim.omni.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "옴니 대체/결품 요청")
public class OmniOrderSetDto {

    @ApiModelProperty(value = "주문번호")
    private Long ordNo;

    @ApiModelProperty(value = "배송일")
    private String deliveryDt;

    @ApiModelProperty(value = "시프트아이디")
    private Long shiftId;

    @ApiModelProperty(value = "슬롯아이디")
    private Long slotId;

    @ApiModelProperty(value = "주문한상품아이디")
    private String goodId;

    @ApiModelProperty(value = "주문수량")
    private Integer itemQty;

    @ApiModelProperty(value = "결품여부")
    private String oosYn;

    @ApiModelProperty(value = "결품코드")
    private String oosCd;

    @ApiModelProperty(value = "피킹수량")
    private Integer pickingQty;

    @ApiModelProperty(value = "대체여부")
    private String changeYn;

    @ApiModelProperty(value = "대체상품아이디")
    private String changeGoodId;

    @ApiModelProperty(value = "대체수량")
    private Integer changeQty;

    @ApiModelProperty(value = "결품수량")
    private Integer waitingQty;

    @ApiModelProperty(value = "단축번호")
    private String sordNo;

}
