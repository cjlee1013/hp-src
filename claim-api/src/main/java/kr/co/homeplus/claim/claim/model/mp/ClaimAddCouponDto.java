package kr.co.homeplus.claim.claim.model.mp;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClaimAddCouponDto {
    @ApiModelProperty(value = "주문번호")
    private long purchaseOrderNo;
    @ApiModelProperty(value = "상품주문번호")
    private long orderItemNo;
    @ApiModelProperty(value = "상품번호")
    private String itemNo;
    @ApiModelProperty(value = "주문수량")
    private long itemQty;
    @ApiModelProperty(value = "TD쿠폰사용여부")
    private String isUseTdCoupon;
    @ApiModelProperty(value = "DS쿠폰사용여부")
    private String isUseDsCoupon;
}