package kr.co.homeplus.claim.event.service;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.event.model.PayCompleteByAmtCheckSetDto;
import kr.co.homeplus.claim.event.model.PurchaseByShipCompleteGetDto;
import kr.co.homeplus.claim.event.model.PurchaseByShipCompleteSetDto;
import kr.co.homeplus.claim.event.model.PurchaseEventCheckSetDto;
import kr.co.homeplus.claim.event.model.PurchaseMileagePaybackGetDto.PurchasePaymentList;
import kr.co.homeplus.claim.event.model.PurchaseMileagePaybackSetDto;
import kr.co.homeplus.claim.event.model.ShipCompleteByAmtCheckSetDto;

public interface EventMapperService {
    LinkedHashMap<String, Object> getEventPurchaseOrderCheck(PurchaseEventCheckSetDto setDto);

    List<PurchasePaymentList> getEventPurchasePaymentList(PurchaseMileagePaybackSetDto setDto);

    List<LinkedHashMap<String, Object>> getEventShipCompleteList(ShipCompleteByAmtCheckSetDto setDto);

    List<LinkedHashMap<String, Object>> getEventShipCompleteDayLimitList(ShipCompleteByAmtCheckSetDto setDto);

    List<PurchaseByShipCompleteGetDto> getNotStoreKindPurchaseByShipComplete(PurchaseByShipCompleteSetDto setDto);

    List<LinkedHashMap<String, Object>> getEventPayCompleteList(PayCompleteByAmtCheckSetDto setDto);

    List<LinkedHashMap<String, Object>> getEventPayCompleteDayLimitList(PayCompleteByAmtCheckSetDto setDto);
}
