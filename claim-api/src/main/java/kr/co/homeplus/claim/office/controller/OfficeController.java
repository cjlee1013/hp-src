package kr.co.homeplus.claim.office.controller;

import io.swagger.annotations.ApiOperation;
import java.util.LinkedHashMap;
import kr.co.homeplus.claim.office.service.OfficeService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/office")
public class OfficeController {

    private final OfficeService officeService;

    @GetMapping("/claimData")
    @ApiOperation(value = "클레임 테스트", response = String.class)
    public ResponseObject<LinkedHashMap<String, Object>> getClaimData(@RequestParam("bundleNo") long bundleNo){
        return officeService.getClaimData(bundleNo);
    }
}
