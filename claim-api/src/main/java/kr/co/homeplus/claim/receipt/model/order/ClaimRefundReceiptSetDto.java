package kr.co.homeplus.claim.receipt.model.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문/배송 - 환불내역 조회")
public class ClaimRefundReceiptSetDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;
    @JsonIgnore
    @ApiModelProperty(value = "고객번호", position = 2)
    private long userNo;
}
