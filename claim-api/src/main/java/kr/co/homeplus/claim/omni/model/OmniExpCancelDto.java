package kr.co.homeplus.claim.omni.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "옴니 - Exp 전체취소 요청 DTO")
public class OmniExpCancelDto {
    @ApiModelProperty(notes = "옴니거래번호", position = 1)
    private long ordNo;
    @ApiModelProperty(notes = "취소사유", position = 2)
    private String claimReasonType;
    @ApiModelProperty(notes = "취소상세사유", position = 3)
    private String claimReasonDetail;
}
