package kr.co.homeplus.claim.office.service;

import java.util.LinkedHashMap;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OfficeService {

    private final OfficeMapperService mapperService;

    public ResponseObject<LinkedHashMap<String, Object>> getClaimData(long bundleNo){
        return ResourceConverter.toResponseObject(mapperService.getClaimData(bundleNo));
    }

}
