package kr.co.homeplus.claim.enums.impl;

public interface EnumImpl {
    String getResponseCode();
    String getResponseMessage();
}