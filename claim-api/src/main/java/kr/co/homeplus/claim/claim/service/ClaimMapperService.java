package kr.co.homeplus.claim.claim.service;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.claim.model.external.ClaimCouponReIssueSetDto.CouponReIssueDto;
import kr.co.homeplus.claim.claim.model.external.safety.ClaimSafetySetDto;
import kr.co.homeplus.claim.claim.model.market.MarketClaimBasicDataDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimAddCouponDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimChgPaymentRefundSetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimDetailGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimItemListDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimListItemListDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPaymentType;
import kr.co.homeplus.claim.claim.model.mp.ClaimPhonePaymentInfoDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claim.claim.model.mp.ClaimPurchaseGift;
import kr.co.homeplus.claim.claim.model.mp.ClaimReasonSearchGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimRefundType;
import kr.co.homeplus.claim.claim.model.mp.ClaimRegisterPreRefundInfoDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimSearchListGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimSearchListSetDto;
import kr.co.homeplus.claim.claim.model.register.ClaimExchShippingRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimBundleRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimInfoRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimItemRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimMultiBundleRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimMultiItemRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimPaymentRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimProcessHistoryRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimRefundRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimReqRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimShippingRegDto;
import kr.co.homeplus.claim.claim.model.register.ticket.ClaimTicketModifyDto;
import kr.co.homeplus.claim.claim.model.register.ticket.ClaimTicketRegDto;
import kr.co.homeplus.claim.enums.ClaimCode;
import kr.co.homeplus.claim.order.model.mypage.MpOrderGiftItemListDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPaymentInfoDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipEditDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipInfoGetDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipInfoSetDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipSetGto;

public interface ClaimMapperService {

    List<ClaimSearchListGetDto> getClaimSearchList(ClaimSearchListSetDto claimSearchListSetDto);

    List<ClaimListItemListDto> getClaimListItemList(long claimBundleNo);

    List<ClaimItemListDto> getClaimItemList(long claimNo);

    int getClaimSearchListCnt(ClaimSearchListSetDto claimSearchListSetDto);

    ClaimDetailGetDto getClaimDetail(long userNo, long claimNo);

    List<ClaimReasonSearchGetDto> getClaimReason(String claimType);

    Boolean addNoRcvShipInfo(NoRcvShipSetGto noRcvShipSetGto) throws Exception;

    Boolean modifyNoRcvShipInfo(NoRcvShipEditDto noRcvShipEditDto);

    NoRcvShipInfoGetDto getNoRcvShipInfo(NoRcvShipInfoSetDto noRcvShipInfoSetDto);

    ClaimRegisterPreRefundInfoDto getRegisterPreRefundAmt (long claimNo);

    List<ClaimRefundType> getRefundTypeList (long claimNo);

    List<ClaimPurchaseGift> selectClaimPurchaseGift(long claimNo);

    List<ClaimPaymentType> getPaymentTypeList (long purchaseOrderNo);

    MpOrderPaymentInfoDto getPurchaseOrderInfo(long purchaseOrderNo, long userNo);

    /**
     * 환불 예정 금액 조회
     *
     * @param calcSetDto 환불 예정 금액 조회 파라 미터
     * @return ClaimPreRefundCalcGetDto 환불 예정 금액 정보
     * @throws Exception 오류 처리
     */
    ClaimPreRefundCalcDto getClaimPreRefundCalculation(ClaimPreRefundCalcSetDto calcSetDto) throws Exception;

    /**
     * 환불 예정 금액 조회(결품/대체)
     *
     * @param calcSetDto 환불 예정 금액 조회 파라 미터
     * @return ClaimPreRefundCalcGetDto 환불 예정 금액 정보
     * @throws Exception 오류 처리
     */
    ClaimPreRefundCalcDto getClaimPreRefundOosCalculation(ClaimPreRefundCalcSetDto calcSetDto) throws Exception;

    /**
     * 환불 예정 금액 조회
     *
     * @param calcSetDto 환불 예정 금액 조회 파라 미터
     * @return ClaimPreRefundCalcGetDto 환불 예정 금액 정보
     * @throws Exception 오류 처리
     */
    ClaimPreRefundCalcDto getClaimMultiPreRefundCalculation(ClaimPreRefundCalcSetDto calcSetDto) throws Exception;

    /**
     * 클레임 번들 생성을 위한 데이터 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return 클레임번들 기본정보
     */
    LinkedHashMap<String, Object> getCreateClaimBundleInfo(long purchaseOrderNo, long bundleNo, long userNo);

    /**
     * 클레임 아이템 생성을 위한 데이터 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param orderItemNo 상품주문번호
     * @return HashMap<String, Object> 클레임 아이템 기본정보
     */
    LinkedHashMap<String, Object> getClaimItemInfo(long purchaseOrderNo, long orderItemNo);

    /**
     * 클레임 마스터 등록
     *
     * @param claimMstRegDto 등록 데이터
     * @return 성공 유무
     */
    Boolean addClaimMstInfo(ClaimMstRegDto claimMstRegDto);

    /**
     * 클레임 번들 등록
     *
     * @param claimBundleRegDto 클레임 번들 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    Boolean addClaimBundleInfo(ClaimBundleRegDto claimBundleRegDto);

    /**
     * 클레임 다중배송 번들 등록
     *
     * @param claimMultiBundleRegDto 클레임 번들 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    Boolean addClaimMultiBundleInfo(ClaimMultiBundleRegDto claimMultiBundleRegDto);

    /**
     * 클레임 요청 등록
     *
     * @param claimReqRegDto 클레임 요청 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    Boolean addClaimReqInfo(ClaimReqRegDto claimReqRegDto);

    /**
     * 클레임 아이템 정보 등록
     *
     * @param claimItemRegDto 클레임 아이템 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    Boolean addClaimItemInfo(ClaimItemRegDto claimItemRegDto);

    /**
     * 클레임 다중배송 아이템 등록
     *
     * @param claimMultiItemRegDto 클레임 번들 아이템 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    Boolean addClaimMultiItemInfo(ClaimMultiItemRegDto claimMultiItemRegDto);

    /**
     * 클레임 결제 취소를 위한 원주문 결제 정보 조회
     *
     * @param paymentNo 결제 번호
     * @param paymentType 결제 타입
     * @return 클레임 결제 취소를 위한 원주문 결제 정보 맵
     */
    LinkedHashMap<String, Object> getOriPaymentInfoForClaim(long paymentNo, String paymentType);

    /**
     * 클레임 결제 취소 정보 등록
     *
     * @param claimPaymentRegDto 클레임 결제 취소 정보
     * @return Boolean 등록 성공 유무
     */
    Boolean addClaimPaymentInfo(ClaimPaymentRegDto claimPaymentRegDto);

    /**
     * 클레임 아이템 옵션별 등록을 위한 데이터 조회
     *
     * @param orderItemNo 아이템주문번호
     * @param orderOptNo 옵션번호
     * @return 클레임 옵션 등록 정보
     */
    ClaimOptRegDto getOriginOrderOptInfoForClaim(long orderItemNo, String orderOptNo);

    /**
     * 클레임 옵션별 등록
     *
     * @param claimOptRegDto 클레임 옵션별 데이터 DTO
     * @return Boolean 등록 성공 유무
     */
    Boolean addClaimOptInfo(ClaimOptRegDto claimOptRegDto);

    /**
     * 클레임 - 결제취소를 위한 데이터 조회
     *
     * @param claimNo 클레임 번호````
     * @return LinkedHashMap<String, Object> 결제취소용 데이터
     */
    List<LinkedHashMap<String, Object>> getPaymentCancelInfo(long claimNo);

    /**
     * 클레임 - 클레임상태 변경 업데이트
     *
     *
     * @param parameter@return Boolean 클레임상태변경 업데이트 결과
     */
    Boolean setClaimStatus(LinkedHashMap<String, Object> parameter);

    /**
     * 클레임 - 클레임 결제 취소 상태 업데이트
     *
     * @param claimPaymentNo 클레임 페이먼트 번호
     * @param claimCode 클레임 코드
     * @return Boolean 클레임 결제취소상태 업데이트 결과
     */
    Boolean setClaimPaymentStatus(long claimPaymentNo, String claimApprovalCd, ClaimCode claimCode);

    /**
     * 클레임 - 클레임 마지막 작업
     *
     * @param parameterMap
     * @return
     */
    LinkedHashMap<String, Object> callByEndClaimFlag(ClaimInfoRegDto claimInfoRegDto);

    /**
     * 클레임 - 전체주문취소에 대한 원 배송번호 취득
     * @param purchaseOrderNo 주문번호
     * @return 배송번호 정보
     */
    List<LinkedHashMap<String, Object>> getOrderBundleByClaim(long purchaseOrderNo);

    /**
     * 클레임 - 환불예정금액 조회 가능 데이터 취득
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return 환불예정금액 조회 가능 데이터
     */
    List<ClaimPreRefundItemList> getClaimPreRefundItemInfo(long purchaseOrderNo, long bundleNo);

    /**
     * 클레임 취소신청시 부분취소 여부 확인
     *
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return 주문건수, 클레임 건수
     */
    LinkedHashMap<String, Object> getOrderByClaimPartInfo(long purchaseOrderNo, long bundleNo, long userNo);


    /**
     * 클레임 처리내역/히스토리 등록
     *
     * @param  claimProcessHistoryRegDto 클레임 처리내역/히스토리 등록 dto
     * @return Boolean 클레임 히스토리 등록 결과
     */
    Boolean setClaimProcessHistory(ClaimProcessHistoryRegDto claimProcessHistoryRegDto);

    /**
     * 클레임 반품(수거) 정보 입력
     *
     * @param parameter 입력데이터
     * @return 입력결과
     */
    Boolean addClaimPickShippingInfo(ClaimShippingRegDto shippingRegDto);

    /**
     * 클레임 교환 정보 입력
     *
     * @param exchShippingRegDto 입력데이터
     * @return 입력결과
     */
    Boolean addClaimExchShipping(ClaimExchShippingRegDto exchShippingRegDto);

    /**
     * 클레임 환불수단 변경
     *
     * @param claimRefundRegDtoo 입력데이터
     * @return 입력결과
     */
    Boolean setClaimRefund(ClaimRefundRegDto claimRefundRegDtoo);

    /**
     * 등록된 환불수단변경건이 있는지 확인
     * **/
    int getAlreadyRegClaimRefundCnt(ClaimChgPaymentRefundSetDto claimChgPaymentRefundSetDto);

    /**
     * 환불수단변경 등록용 클레임 결제정보 조회
     * **/
    ClaimPhonePaymentInfoDto getClaimPhonePaymentInfo(ClaimChgPaymentRefundSetDto claimChgPaymentRefundSetDto);

    /**
     * 주문 사은품 정보 리스트
     * **/
    List<MpOrderGiftItemListDto>  getOrderGiftItemList(long purchaseOrderNo, long bundleNo);

    void callByClaimAdditionReg(long claimNo);

    void callByClaimItemEmpDiscount(long claimNo, long orderItemNo);

    /**
     * 클레임 번들 생성을 위한 데이터 조회
     *
     * @param purchaseOrderNo 주문번호
     * @return 클레임번들 기본정보
     */
    LinkedHashMap<String, Object> getCreateClaimMstInfo(long purchaseOrderNo, long userNo);

    /**
     * 클레임 가능 여부 판단을 위한 데이터 조회
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return 주문데이터
     */
    LinkedHashMap<String, Object> getClaimPossibleCheck(long purchaseOrderNo, long bundleNo);

    /**
     * 클레임 성공시 기존 쿠폰 재발행
     * @param claimNo 클레임번호
     * @return
     */
    List<CouponReIssueDto> getClaimCouponReIssueInfo(long claimNo, long userNo);

    /**
     * 클레임 마무리 전 클레임데이터 조회
     * @param claimNo 클레임번호
     * @return
     */
    List<LinkedHashMap<String, Object>> getClaimInfo(long claimNo);

    /**
     * 교환/반품시 자동수거대상여부 확인.
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return
     */
    LinkedHashMap<String, Object> getClaimPickIsAutoInfo(long purchaseOrderNo, long bundleNo);

    /**
     * 쿠폰재발행 결과 업데이트
     * @param claimAdditionNo additionNo
     * @return
     */
    Boolean modifyClaimCouponReIssueResult(List<Long> claimAdditionNo);

    /**
     * 슬롯마감결과 업데이트
     * @param claimBundleNo 배송번호
     * @return
     */
    Boolean modifyClaimBundleSlotResult(long claimBundleNo);

    /**
     * 클레임 알람톡/SMS 발송 데이터
     * @param claimNo 클레임번호
     * @return
     */
    LinkedHashMap<String, Object> getClaimSendDataInfo(long claimNo);

    /**
     * 클레임 안심번호 발행 정보
     * @param claimBundleNo 클레임번들번호
     * @param issueType 발행타입
     * @return
     */
    ClaimSafetySetDto getClaimSafetyData(long claimBundleNo, String issueType);

    /**
     * claim_info insert 를 위한 데이터 조회
     * @param claimNo 클레임번호
     * @return
     */
    ClaimInfoRegDto getClaimRegData(long claimNo);

    /**
     * OCB 취소 거래번호 업데이트
     * @param claimPaymentNo
     * @param pgMid
     * @return
     */
    Boolean modifyClaimPaymentOcbTransactionId(long claimPaymentNo, String pgMid);

    /**
     * PG 취소 거래번호 업데이트
     *
     * @param claimPaymentNo 취소거래번호
     * @param cancelTradeId PG취소거래번호
     * @return 결과
     */
    Boolean modifyClaimPaymentPgCancelTradeId(long claimPaymentNo, String cancelTradeId);

    /**
     * 티켓 취소를 위한 티켓정보 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param orderItemNo 상품주문번호
     * @return 티켓정보
     */
    List<LinkedHashMap<String, Object>> getOrderTicketInfo(long purchaseOrderNo, long orderItemNo, int cancelCnt);

    /**
     * 티켓 클레임 insert
     *
     * @param claimTicketRegDto 티켓클레임 저장 정보 DTO
     * @return 티켓클레임저장결과
     */
    Boolean addClaimTicketInfo(ClaimTicketRegDto claimTicketRegDto);

    /**
     * 티켓 취소 처리정보 조회
     *
     * @param claimNo 클레임번호
     * @return 티켓정보
     */
    List<LinkedHashMap<String, Object>> getClaimTicketInfo(long claimNo);

    /**
     * 클레임 주문 타입 체크
     *
     * @param claimNo 클레임 번호
     * @return
     */
    LinkedHashMap<String, String> getClaimOrderTypeCheck(long claimNo);

    /**
     * 티켓 정보 업데이트
     *
     * @param modifyDto 업데이트 정보
     * @return
     */
    Boolean modifyOrderTicketInfo(ClaimTicketModifyDto modifyDto);

    /**
     * 클레임 - 클레임 결제 취소 상태 업데이트
     *
     * @param claimNo 클레임 번호
     * @param claimCode 클레임 코드
     * @return Boolean 클레임 결제취소상태 업데이트 결과
     */
    Boolean setTotalClaimPaymentStatus(long claimNo, ClaimCode claimCode);

    /**
     * MHC 적립취소 생성데이터
     *
     * @param claimNo 클레임번호
     * @return
     */
    List<LinkedHashMap<String, Object>> getCreateClaimAccumulateInfo(long claimNo);

    /**
     * MHC 적립취소 금액계산을 위한 데이터 조회
     *
     * @param purchaseOrderNo 주문번호
     * @return
     */
    LinkedHashMap<String, Object> getAccumulateCancelInfo(long purchaseOrderNo, String pointKind);

    /**
     * 주문적립취소 등록
     *
     * @param parameterMap 파라미터정보
     * @return
     */
    Boolean addClaimAccumulate(LinkedHashMap<String, Object> parameterMap);

    /**
     * 주문적립취소 전송상태 업데이트
     *
     * @param claimNo 클레임번호
     * @param reqSendYn 전송상태
     * @return
     */
    Boolean modifyClaimAccumulateReqSend(long claimNo, String reqSendYn, String pointKind);

    /**
     * 주문적립취소 등록 결과 업데이트
     *
     * @param parameterMap 파라미터정보
     * @return
     */
    Boolean modifyClaimAccumulateReqResult(LinkedHashMap<String, Object> parameterMap);

    /**
     * 합배송이 있는 주문의 클레임 건수를 가지고 온다.
     *
     * @param purchaseOrderNo 주문번호
     * @return
     */
    List<LinkedHashMap<String, Object>> getOrderCombineClaimCheck(String purchaseOrderNo, String type);

    /**
     * 클레임 등록된 TD번들 정보
     *
     * @param claimNo
     * @return
     */
    LinkedHashMap<String, String> getClaimTdBundleInfo(long claimNo);

    /**
     * 마일리지 페이백 여부 체크
     *
     * @param bundleNo
     * @return
     */
    LinkedHashMap<String, Object> getMileagePaybackCheck(long bundleNo);

    /**
     * 페이백 대상 금액
     *
     * @param purchaseOrderNo
     * @return
     */
    LinkedHashMap<String, Object> getOrderCombinePaybackCheck(long purchaseOrderNo);

    /**
     * 페이백 클레임 금액
     *
     * @param bundleNo
     * @param orgBundleNo
     * @return
     */
    LinkedHashMap<String, Object> getClaimCombinePaybackCheck(long bundleNo, long orgBundleNo);

    /**
     * 미수취 철회시 배송정보 확인
     *
     * @param purchaseOrderNo
     * @param bundleNo
     * @return
     */
    LinkedHashMap<String, Object> getNonShipCompleteInfo(long purchaseOrderNo, long bundleNo);

    /**
     * 배송비 페이백 결과 업데이트
     *
     * @param paybackNo
     * @return
     */
    boolean setShipPaybackResult(long paybackNo);

    List<ClaimAddCouponDto> isAddCouponPossible(long purchaseOrderNo);

    List<Long> getMultiBundleInfo(long purchaseOrderNo, long bundleNo);

    List<ClaimMultiItemRegDto> getClaimMultiItemCreateInfo(long claimNo, long multiBundleNo);

    boolean isOrderMarketCheck(long purchaseOrderNo, long claimNo);

    MarketClaimBasicDataDto getClaimBasicDataForMarketOrder(long purchaseOrderNo, long orderOptNo);

    List<LinkedHashMap<String, Object>> getClaimStatusChangeDataForMarket(long claimNo);
}
