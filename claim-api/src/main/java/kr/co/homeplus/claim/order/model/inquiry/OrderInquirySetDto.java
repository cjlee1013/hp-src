package kr.co.homeplus.claim.order.model.inquiry;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrderInquirySetDto {
    @ApiModelProperty(value = "조회시작일", reference = "YYYY-MM-DD", required = true, position = 1)
    private String schStartDt;
    @ApiModelProperty(value = "조회종료일", reference = "YYYY-MM-DD", required = true, position = 2)
    private String schEndDt;
    @ApiModelProperty(value = "유저번호", position = 3)
    private Long userNo;
    @ApiModelProperty(value = "페이지", position = 4)
    private Integer page;
    @ApiModelProperty(value = "페이지조회수", position = 5)
    private Integer perPage;
    @ApiModelProperty(value = "사이트타입", position = 6)
    private String siteType;
}
