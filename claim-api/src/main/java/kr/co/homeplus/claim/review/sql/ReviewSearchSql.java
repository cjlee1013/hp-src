package kr.co.homeplus.claim.review.sql;

import kr.co.homeplus.claim.constants.CustomConstants;
import kr.co.homeplus.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claim.entity.ifdms.RefitCommentProductRealEntity;
import kr.co.homeplus.claim.entity.order.GhsItemReviewEntity;
import kr.co.homeplus.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claim.entity.order.OrderOptEntity;
import kr.co.homeplus.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claim.enums.MysqlFunction;
import kr.co.homeplus.claim.factory.EntityFactory;
import kr.co.homeplus.claim.review.model.ReviewSearchSetDto;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ReviewSearchSql {

    public static String selectReviewPossibleList(ReviewSearchSetDto setDto) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        RefitCommentProductRealEntity refitCommentProductReal = EntityFactory.createEntityIntoValue(RefitCommentProductRealEntity.class, Boolean.TRUE);
        GhsItemReviewEntity ghsItemReview = EntityFactory.createEntityIntoValue(GhsItemReviewEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                orderItem.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.paymentNo, purchaseOrder.paymentNo),
                orderItem.itemNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemNm1, orderItem.itemNm1),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderDt), CustomConstants.YYYYMMDD_COMMA),
                    "order_dt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATEDIFF,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.completeDt), "1")), "60")),
                        "NOW()"
                    ),
                    "dead_line_dt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.storeType, orderItem.storeType),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimItem.claimItemQty, claimItem.claimItemQty),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemQty, orderItem.itemQty),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.completeDt, "delivery_dt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    orderItem,
                    ObjectUtils.toArray(purchaseOrder.purchaseOrderNo),
                    ObjectUtils.toArray(SqlUtils.equalColumnByInput(orderItem.reviewYn, "N"))
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    shippingItem,
                    ObjectUtils.toArray(orderItem.orderItemNo, orderItem.bundleNo),
                    ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("'D4'", "'D5'")))
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, orderItem.orderItemNo)))
            .WHERE(
                SqlUtils.sqlFunction(
                    MysqlFunction.BETWEEN,
                    ObjectUtils.toArray(
                        shippingItem.completeDt,
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray("NOW()", "-61"))),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, "NOW()")
                    )
                ),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, setDto.getUserNo()),
                SqlUtils.equalColumnByInput(purchaseOrder.siteType, setDto.getSiteType())
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo, orderItem.bundleNo, orderItem.itemNo);

        SQL orderOfQuery = new SQL()
            .SELECT(
                SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo),
                SqlUtils.nonAlias(orderItem.bundleNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(purchaseOrder.paymentNo), "payment_no"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.sqlFunction(MysqlFunction.COUNT, SqlUtils.nonAlias(orderItem.itemNo)), "1"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(orderItem.itemNm1)),
                                "' 외 '",
                                SqlUtils.customOperator(CustomConstants.MINUS, SqlUtils.sqlFunction(MysqlFunction.COUNT, SqlUtils.nonAlias(orderItem.itemNo)), "1"),
                                "'건'"
                            )
                        ),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(orderItem.itemNm1))
                    ),
                    "item_nm"
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "order_dt", "order_dt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "dead_line_dt", "dead_line_dt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "delivery_dt", "delivery_dt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(orderItem.storeType), orderItem.storeType)
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "Z"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.customOperator(CustomConstants.MINUS, SqlUtils.nonAlias(orderItem.itemQty), SqlUtils.nonAlias(claimItem.claimItemQty)), "0")
            )
            .GROUP_BY(SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo), SqlUtils.nonAlias(orderItem.bundleNo))
            ;

        SQL ghsOfQuery = new SQL()
            .SELECT(
                refitCommentProductReal.ordNo,
                refitCommentProductReal.ordNo,
                SqlUtils.aliasToRow("0", "payment_no"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.sqlFunction(MysqlFunction.COUNT, refitCommentProductReal.goodId), "1"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.MAX, refitCommentProductReal.goodNm),
                                "' 외 '",
                                SqlUtils.customOperator(CustomConstants.MINUS, SqlUtils.sqlFunction(MysqlFunction.COUNT, refitCommentProductReal.goodId), "1"),
                                "'건'"
                            )
                        ),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, refitCommentProductReal.goodNm)
                    ),
                    "item_nm"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, refitCommentProductReal.tradeDate), CustomConstants.YYYYMMDD_COMMA),
                    "order_dt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATEDIFF,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, refitCommentProductReal.deliveryDt), "1")), "60")),
                        "NOW()"
                    ),
                    "dead_line_dt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, refitCommentProductReal.deliveryDt, refitCommentProductReal.deliveryDt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, refitCommentProductReal.storeType, refitCommentProductReal.storeType)
            )
            .FROM(EntityFactory.getTableNameWithAlias("ifdms", refitCommentProductReal))
            .WHERE(
                SqlUtils.equalColumnByInput(refitCommentProductReal.userId, String.valueOf(setDto.getUserNo())),
                SqlUtils.equalColumnByInput(refitCommentProductReal.writeYn, "N"),
                SqlUtils.sqlFunction(
                    MysqlFunction.BETWEEN,
                    ObjectUtils.toArray(
                        refitCommentProductReal.deliveryDt,
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray("NOW()", "-61"))),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, "NOW()")
                    )
                ),
                SqlUtils.notExists(
                    new SQL().SELECT(ghsItemReview.orderItemNo)
                        .FROM(EntityFactory.getTableNameWithAlias(ghsItemReview))
                        .WHERE(
                            SqlUtils.equalColumn(ghsItemReview.orderItemNo, refitCommentProductReal.pkSeq),
                            SqlUtils.equalColumn(SqlUtils.sqlFunction(MysqlFunction.CAST_NCHAR, ghsItemReview.userNo), refitCommentProductReal.userId)
                        )
                )
            )
            .GROUP_BY(refitCommentProductReal.ordNo)
            ;

        SQL query = new SQL()
            .SELECT("*")
            .FROM(
                "(".concat(orderOfQuery.toString())
                    .concat("\nUNION ALL\n")
                    .concat(ghsOfQuery.toString())
                    .concat(") t")
            )
            .ORDER_BY(SqlUtils.orderByesc("delivery_dt"))
            .ORDER_BY("order_dt");

        if(setDto.getPage() != 0){
            query.LIMIT(setDto.getPerPage())
                .OFFSET((setDto.getPage() - 1) * setDto.getPerPage());
        }

        log.debug("selectReviewPossibleList \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectReviewPossibleItemList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
                orderItem.orderItemNo,
                orderItem.itemNo,
                orderItem.storeType,
                orderItem.storeId,
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemNm1, orderItem.itemNm1),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderOpt.selOpt1Title, orderOpt.selOpt1Title),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimItem.claimItemQty, claimItem.claimItemQty),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemQty, orderItem.itemQty)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    orderItem,
                    ObjectUtils.toArray(purchaseOrder.purchaseOrderNo),
                    ObjectUtils.toArray(SqlUtils.equalColumnByInput(orderItem.reviewYn, "N"))
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, orderItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo)
            )
            .GROUP_BY(orderItem.orderItemNo, orderItem.itemNo, orderItem.storeType, orderItem.storeId)
            .ORDER_BY(orderItem.orderItemNo);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(orderItem.orderItemNo),
                SqlUtils.nonAlias(orderItem.itemNo),
                SqlUtils.nonAlias(orderItem.storeType),
                SqlUtils.nonAlias(orderItem.storeId),
                SqlUtils.aliasToRow(SqlUtils.nonAlias(orderItem.itemNm1), "item_nm"),
                SqlUtils.aliasToRow(SqlUtils.nonAlias(orderOpt.selOpt1Title), "opt_nm")
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "Z"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.customOperator(CustomConstants.MINUS, SqlUtils.nonAlias(orderItem.itemQty), SqlUtils.nonAlias(claimItem.claimItemQty)), "0")
            );
        log.debug("selectReviewPossibleItemList \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectGhsReviewPossibleItemList(@Param("ordNo") long ordNo) {
        RefitCommentProductRealEntity refitCommentProductReal = EntityFactory.createEntityIntoValue(RefitCommentProductRealEntity.class, Boolean.TRUE);
        GhsItemReviewEntity ghsItemReview = EntityFactory.createEntityIntoValue(GhsItemReviewEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.aliasToRow(refitCommentProductReal.pkSeq, "order_item_no"),
                SqlUtils.aliasToRow(refitCommentProductReal.goodId, "item_no"),
                SqlUtils.aliasToRow("'HYPER'", refitCommentProductReal.storeType),
                refitCommentProductReal.storeId,
                SqlUtils.aliasToRow(refitCommentProductReal.goodNm, "item_nm"),
                SqlUtils.aliasToRow(null, "opt_nm")
            )
            .FROM(EntityFactory.getTableNameWithAlias("ifdms", refitCommentProductReal))
            .WHERE(
                SqlUtils.equalColumnByInput(refitCommentProductReal.ordNo, String.valueOf(ordNo)),
                SqlUtils.notExists(
                    new SQL().SELECT(ghsItemReview.orderItemNo)
                        .FROM(EntityFactory.getTableNameWithAlias(ghsItemReview))
                        .WHERE(
                            SqlUtils.equalColumn(ghsItemReview.orderItemNo, refitCommentProductReal.pkSeq),
                            SqlUtils.equalColumn(SqlUtils.sqlFunction(MysqlFunction.CAST_NCHAR, ghsItemReview.userNo), refitCommentProductReal.userId)
                        )
                )
            );
        log.debug("selectGhsReviewPossibleItemList \n[{}]", query.toString());
        return query.toString();
    }

}
