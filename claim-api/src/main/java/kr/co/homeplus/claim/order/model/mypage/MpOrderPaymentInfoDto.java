package kr.co.homeplus.claim.order.model.mypage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.claim.enums.ClaimCode;
import kr.co.homeplus.claim.utils.StringUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "마이페이지 > 조회/배송 상세 > 결제정보")
public class MpOrderPaymentInfoDto {

    @ApiModelProperty(value = "총주문금액", position = 1)
    private int totItemPrice;

    @ApiModelProperty(value = "총할인금액", position = 2)
    private int totDiscountPrice;

    @ApiModelProperty(value = "총배송금액", position = 3)
    private int totShipPrice;

    @ApiModelProperty(value = "총도서산간배송금액", position = 4)
    private int totIslandShipPrice;

    @ApiModelProperty(value = "총결제금액", position = 5)
    private int totPaymentAmt;

    @ApiModelProperty(value = "총임직원할인", position = 6)
    private int totEmpDiscountAmt;

    @ApiModelProperty(value = "쿠폰할인리스트", position = 7)
    private List<OrderCouponDiscountDto> couponDiscountList;

    @ApiModelProperty(value = "프로모션할인리스트", position = 8)
    private List<OrderPromoDiscountDto> promoDiscountList;

    @ApiModelProperty(value = "결제수단리스트", position = 9)
    private List<OrderPaymentListDto> orderPaymentListList;

    @ApiModelProperty(value = "혜택/적립리스트", position = 10)
    private List<OrderBenefitDto> orderBenefitList;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @ApiModel(description = "마이페이지 > 조회/배송 상세 > 결제정보 > 쿠폰할인")
    public static class OrderCouponDiscountDto{
        @ApiModelProperty(value = "쿠폰할인타입", position = 1)
        private String discountType;
        @ApiModelProperty(value = "쿠폰할인금액", position = 2)
        private int discountPrice;

        private void setDiscountType(String discountType){
            this.discountType = !StringUtil.isEmpty(discountType) ? getDiscountKind(discountType) : null;
        }

        private String getDiscountKind(String type){
            String returnStr =  Stream.of(ClaimCode.values())
                .filter(e -> e.name().startsWith("DISCOUNT_KIND_"))
                .collect(Collectors.toMap(ClaimCode::getCode, ClaimCode::getExternalNm))
                .get(type);
            return StringUtil.isNotEmpty(returnStr) ? returnStr : type;
        }
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @ApiModel(description = "마이페이지 > 조회/배송 상세 > 결제정보 > 프로모션할인")
    public static class OrderPromoDiscountDto{
        @ApiModelProperty(value = "프로모션할인타입", position = 1)
        private String promoType;
        @ApiModelProperty(value = "프로모션할인금액", position = 2)
        private int promoDiscountPrice;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @ApiModel(description = "마이페이지 > 조회/배송 상세 > 결제정보 > 결제수단별")
    public static class OrderPaymentListDto {
        @ApiModelProperty(value = "결제수단타입", position = 1)
        private String paymentType;
        @ApiModelProperty(value = "결제수단정보", position = 2)
        private String paymentInfo;
        @ApiModelProperty(value = "결제수단별금액", position = 3)
        private int paymentAmt;

        private void setPaymentType(String paymentType){
            this.paymentType = getPaymentType(paymentType);
        }

        private String getPaymentType(String type){
            return Stream.of(ClaimCode.values())
                .filter(e -> e.name().startsWith("PAYMENT_TYPE_"))
                .collect(Collectors.toMap(ClaimCode::getCode, ClaimCode::getExternalNm))
                .get(type.toUpperCase(Locale.KOREA));
        }
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @ApiModel(description = "마이페이지 > 조회/배송 상세 > 결제정보 > 해택/적립")
    public static class OrderBenefitDto {
        @ApiModelProperty(value = "혜택/적립타입", position = 1)
        private String benefitType;
        @ApiModelProperty(value = "혜택/적립금액", position = 2)
        private int benefitAmt;
    }

}
