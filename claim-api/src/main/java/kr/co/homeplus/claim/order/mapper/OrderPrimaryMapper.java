package kr.co.homeplus.claim.order.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.core.db.annotation.PrimaryConnection;
import kr.co.homeplus.claim.order.model.mypage.MultiOrderShipAddrEditDto;
import kr.co.homeplus.claim.order.model.mypage.MultiOrderShipBasicInfoEditDto;
import kr.co.homeplus.claim.order.model.mypage.OrderShipAddrEditDto;
import kr.co.homeplus.claim.order.sql.MypageOrderSearchSql;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

@PrimaryConnection
public interface OrderPrimaryMapper {

    @UpdateProvider(type = MypageOrderSearchSql.class, method = "updateShippingAddrEditCheck")
    int updateShippingAddrEditCheck(OrderShipAddrEditDto shipAddrEditDto);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectShippingPresentStatus")
    List<LinkedHashMap<String, Object>> selectShippingPresentStatus(@Param("purchaseOrderNo") long purchaseOrderNo);

    /**
     * 선물세트(다중배송) 공통정보 수정(배송얘약일, 선물메시지)
     *
     * @param shipBasicInfoEditDto
     * @return
     */
    @UpdateProvider(type = MypageOrderSearchSql.class, method = "updateMultiOrderShippingAddrBasicInfo")
    int updateMultiOrderShippingAddrBasicInfo(MultiOrderShipBasicInfoEditDto shipBasicInfoEditDto);

    /**
     * 선물세트(다중배송) 배송지별 주소지 변경
     *
     * @param shipAddrEditDto
     * @return
     */
    @UpdateProvider(type = MypageOrderSearchSql.class, method = "updateMultiOrderShippingAddrInfo")
    int updateMultiOrderShippingAddrInfo(MultiOrderShipAddrEditDto shipAddrEditDto);

    @UpdateProvider(type = MypageOrderSearchSql.class, method = "updateOrderUserRecentlyInfo")
    int updateOrderUserRecentlyInfo(@Param("auroraShipMsgCd") String auroraShipMsgCd, @Param("auroraShipMsg") String auroraShipMsg, @Param("userNo") long userNo);

}
