package kr.co.homeplus.claim.omni.model.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "대체주문아이템DTO")
public class OmniSubstitutionItemDto {
    @ApiModelProperty(value = "원주문상품번호", position = 1)
    private String originItemNo;
    @ApiModelProperty(value = "원주문상품수량", position = 2)
    private int originItemQty;
    @ApiModelProperty(value = "대체상품번호", position = 3)
    private String substitutionItemNo;
    @ApiModelProperty(value = "대체상품수량", position = 4)
    private int substitutionItemQty;
}
