package kr.co.homeplus.claim.claim.model.external.ticket;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "티켓상태 조회(COOP용-클레임) 응답 DTO")
public class ClaimCoopTicketCheckGetDto {
    /** 응답 항목 */
    @ApiModelProperty(value = "응답코드(응답)", position = 1)
    private String resultCode;
    @ApiModelProperty(value = "응답메세지(응답)", position = 2)
    private String resultMsg;
    @ApiModelProperty(value = "사용여부 (Y :사용, N :미사용, C :사용취소)(응답)", position = 3)
    private String useYn;
    @ApiModelProperty(value = "쿠폰 사용 일자(응답)", position = 4)
    private String useDate;
    @ApiModelProperty(value = "쿠폰 사용 장소(응답)", position = 5)
    private String usePlace;
    @ApiModelProperty(value = "최종 수신자 번호(응답)", position = 6)
    private String sendHp;
    @ApiModelProperty(value = "유효기간 시작일(응답)", position = 7)
    private String useStart;
    @ApiModelProperty(value = "유효기간 종료일(응답)", position = 8)
    private String useEnd;
    @ApiModelProperty(value = "발급일자(응답)", position = 9)
    private String insDate;
    @ApiModelProperty(value = "임시공간(미 사용시 공백 회신)(응답)", position = 10)
    private String filler01;
    @ApiModelProperty(value = "임시공간(미 사용시 공백 회신)(응답)", position = 11)
    private String filler02;
    @ApiModelProperty(value = "임시공간(미 사용시 공백 회신)(응답)", position = 12)
    private String filler03;
}
