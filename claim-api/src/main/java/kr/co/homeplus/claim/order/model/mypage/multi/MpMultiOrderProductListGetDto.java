package kr.co.homeplus.claim.order.model.mypage.multi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마이페이지 > 주문/배송 - 주문조회 응답")
public class MpMultiOrderProductListGetDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;
    @ApiModelProperty(value = "배송번호", position = 2)
    private long bundleNo;
    @ApiModelProperty(value = "멀티배송번호", position = 3)
    private long multiBundleNo;
    @ApiModelProperty(value = "주문일시", position = 4)
    private String orderDt;
    @ApiModelProperty(value = "점포유형(HYPER, CLUB, AURORA, EXP, DS)", position = 5)
    private String storeType;
    @ApiModelProperty(value = "몰유형(DS:업체상품,TD:매장상품)", position = 6)
    private String mallType;
    @ApiModelProperty(value = "파트너ID", position = 7)
    private String partnerId;
    @ApiModelProperty(value = "파트너명", position = 8)
    private String partnerNm;
    @ApiModelProperty(value = "주문상품번호", position = 9)
    private long orderItemNo;
    @ApiModelProperty(value = "다중주문상품번호", position = 9)
    private long multiOrderItemNo;
    @ApiModelProperty(value = "원상품번호", position = 10)
    private String orgItemNo;
    @ApiModelProperty(value = "상품번호", position = 11)
    private String itemNo;
    @ApiModelProperty(value = "상품명", position = 12)
    private String itemNm1;
    @ApiModelProperty(value = "상품수량", position = 13)
    private long orderItemQty;
    @ApiModelProperty(value = "상품금액(상품 판매가)", position = 14)
    private int itemPrice;
    @ApiModelProperty(value = "주문옵션번호", position = 15)
    private long orderOptNo;
    @ApiModelProperty(value = "선택옵션1타이틀", position = 16)
    private String selOpt1Title;
    @ApiModelProperty(value = "선택옵션2내용", position = 17)
    private String selOpt1Val;
    @ApiModelProperty(value = "선택옵션2타이틀", position = 18)
    private String selOpt2Title;
    @ApiModelProperty(value = "선택옵션2내용", position = 19)
    private String selOpt2Val;
    @ApiModelProperty(value = "텍스트옵션1타이틀", position = 20)
    private String txtOpt1Title;
    @ApiModelProperty(value = "텍스트옵션1내용", position = 21)
    private String txtOpt1Val;
    @ApiModelProperty(value = "텍스트옵션2타이틀", position = 22)
    private String txtOpt2Title;
    @ApiModelProperty(value = "텍스트옵션2내용", position = 23)
    private String txtOpt2Val;
    @ApiModelProperty(value = "옵션번호", position = 24)
    private long optNo;
    @ApiModelProperty(value = "배송방법(DS_DLV:택배배송,DS_DRCT:직접배송,DS_PICK:방문수령/픽업,DS_POST:우편배송,DS_QUICK:퀵배송,TD_DLV:택배배송,TD_DRCT:직접배송,TD_PICK:방문수령,TD_POST:우편배송,TD_QUICK:퀵배송)", position = 25)
    private String shipMethod;
    @ApiModelProperty(value = "운송번호", position = 26)
    private long shipNo;
    @ApiModelProperty(value = "결제상태(P1:결제요청,P3:결제완료(입금확인),P4:결제실패,P8:결제철회(무통장,카드,계좌이체,휴대폰 주문취소요청))", position = 27)
    private String paymentStatus;
    @ApiModelProperty(value = "결제상태_DESC", position = 28)
    private String paymentStatusNm;
    @ApiModelProperty(value = "배송상태(NN:대기,D1:주문확인전(결제완료),D2:주문확인(상품준비중),D3:배송중,D4:배송완료,D5:구매확정)", position = 29)
    private String shipStatus;
    @ApiModelProperty(value = "배송상태_DESC", position = 30)
    private String shipStatusNm;
    @ApiModelProperty(value = "클레임가능여부", position = 31)
    private String claimYn;
    @ApiModelProperty(value = "클레임진행건수", position = 32)
    private Integer claimItemQty;
    @ApiModelProperty(value = "배송일시", position = 33)
    private String shipDt;
    @ApiModelProperty(value = "배송시작시간", position = 34)
    private String shipStartTime;
    @ApiModelProperty(value = "배송종료시간", position = 35)
    private String shipEndTime;
    @ApiModelProperty(value = "배송슬롯마감시간", position = 36)
    private String shipCloseTime;
    @ApiModelProperty(value = "배송시작일(송장입력,원배송)", position = 37)
    private String shippingDt;
    @ApiModelProperty(value = "배송완료일", position = 38)
    private String completeDt;
    @ApiModelProperty(value = "미수취신고여부", position = 39)
    private String noRcvYn;
    @ApiModelProperty(value = "합배송여부", position = 40)
    private String cmbnYn;
    @ApiModelProperty(value = "대체주문여부", position = 41)
    private String substitutionOrderYn;
    @ApiModelProperty(value = "프로모션타입", position = 42)
    private String promoType;
    @ApiModelProperty(value = "배송타입", position = 43)
    private String shipType;
    @ApiModelProperty(value = "프로모션번호", position = 44)
    private String promoNo;
    @ApiModelProperty(value = "프로모션_상세번호", position = 45)
    private String promoDetailNo;
    @ApiModelProperty(value = "프로모션명", position = 46)
    private String promoNm;
    @ApiModelProperty(value = "클레임번들번호", position = 47)
    private Long claimBundleNo;
    @ApiModelProperty(value = "클레임진행건수", position = 48)
    private Long processItemQty;
    @ApiModelProperty(value = "클레임타입", position = 49)
    private String claimType;
    @ApiModelProperty(value = "클레임상태", position = 50)
    private String claimStatus;
    @ApiModelProperty(value = "지연배송코드", position = 51)
    private String delayShipCd;
    @ApiModelProperty(value = "지연배송메시지", position = 52)
    private String delayShipMsg;
    @ApiModelProperty(value = "결제번호", position = 53)
    private String paymentNo;
    @ApiModelProperty(value = "리뷰작성여부", position = 54)
    private String reviewYn;
    @ApiModelProperty(value = "최소구매수량", position = 55)
    private long purchaseMinQty;
    @ApiModelProperty(value = "클레임번호", position = 56)
    private Long claimNo;
    @ApiModelProperty(value = "원상품금액(행사상품)", position = 57)
    private Long simpOrgItemPrice;
    @ApiModelProperty(value = "배송비", position = 58)
    private Long shipPrice;
    @ApiModelProperty(value = "도서산간배송비", position = 59)
    private Long islandShipPrice;
    @ApiModelProperty(value = "착불여부", position = 60)
    private String prepaymentYn;
    @ApiModelProperty(value = "판매단위", position = 61)
    private String saleUnit;
    @ApiModelProperty(value = "판매단위", position = 62)
    private String pickShipStatus;
    @ApiModelProperty(value = "판매단위", position = 63)
    private String exchShipStatus;
    @ApiModelProperty(value = "쿠폰시 쿠프인지여부", position = 64)
    private String isTicketFromCoop;
    @ApiModelProperty(value = "고객수취확인여부", position = 65)
    private String completeUserRegYn;
    @ApiModelProperty(value = "중복쿠폰사용여부", position = 66)
    private String addCouponUseYn;
    @Deprecated
    private int itemQty;
    @Deprecated
    private int optQty;
}
