package kr.co.homeplus.claim.omni.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "옴니 환불/교환 요청")
public class OmniClaimSetDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private Long ordNo;
    @ApiModelProperty(value = "시프트아이디", position = 2)
    private Long shiftId;
    @ApiModelProperty(value = "슬롯아이디", position = 3)
    private Long slotId;
    @ApiModelProperty(value = "주문한상품아이디", position = 4)
    private String goodId;
    @ApiModelProperty(value = "주문수량", position = 5)
    private Integer itemQty;
    @ApiModelProperty(value = "반품여부", position = 6)
    private String refundYn;
    @ApiModelProperty(value = "클레임사유코드", position = 8)
    private String claimCd;
    @ApiModelProperty(value = "반품/교환수량", position = 9)
    private Integer claimQty;
}
