package kr.co.homeplus.claim.claim.model.register;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "클레임 다중배송 아이템 저장 DTO")
public class ClaimMultiItemRegDto {
    @ApiModelProperty(value= "클레임다중배송아이템번호")
    private long claimMultiItemNo;
    @ApiModelProperty(value= "클레임다중배송번들번호")
    private long claimMultiBundleNo;
    @ApiModelProperty(value= "클레임아이템번호")
    private long claimItemNo;
    @ApiModelProperty(value= "클레임그룹번호")
    private long claimNo;
    @ApiModelProperty(value= "클레임번들번호")
    private long claimBundleNo;
    @ApiModelProperty(value= "다중번들번호")
    private long multiBundleNo;
    @ApiModelProperty(value= "상품주문번호")
    private long orderItemNo;
    @ApiModelProperty(value= "다중배송아이템주문번호")
    private long multiOrderItemNo;
    @ApiModelProperty(value= "아이템금액")
    private long itemPrice;
    @ApiModelProperty(value= "클레임취소수량")
    private long claimItemQty;
    @ApiModelProperty(value= "등록일")
    private String regDt;
}
