package kr.co.homeplus.claim.order.model.mypage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마이페이지 > 주문/배송 - 주문조회 응답(아이템정보)")
public class MpOrderItemListDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "몰유형(DS:업체상품,TD:매장상품)", position = 2)
    private String mallType;

    @ApiModelProperty(value = "주문상품번호", position = 3)
    private long orderItemNo;

    @ApiModelProperty(value = "원상품번호", position = 4)
    private String orgItemNo;

    @ApiModelProperty(value = "상품번호", position = 5)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 6)
    private String itemNm1;

    @ApiModelProperty(value = "상품수량", position = 7)
    private long itemQty;

    @ApiModelProperty(value = "상품금액(상품 판매가)", position = 8)
    private int itemPrice;

    @ApiModelProperty(value = "배송시작일(송장입력,원배송)", position = 9)
    private String shippingDt;

    @ApiModelProperty(value = "배송완료일", position = 10)
    private String completeDt;

    @ApiModelProperty(value = "배송상태", position = 11)
    private String shipStatus;

    @ApiModelProperty(value = "미수취신고여부", position = 12)
    private String noRcvYn;

    @ApiModelProperty(value = "운송번호", position = 13)
    private long shipNo;

    @ApiModelProperty(value = "지연상태코드", position = 14)
    private String delayShipCd;

    @ApiModelProperty(value = "지연상태메시지", position = 15)
    private String delayShipMsg;

    @ApiModelProperty(value = "리뷰작성여부", position = 16)
    private String reviewYn;

    @ApiModelProperty(value = "번들번호", position = 17)
    private String bundleNo;

    @ApiModelProperty(value = "착불여부", position = 18)
    private String prepaymentYn;

    @ApiModelProperty(value = "판매단위", position = 19)
    private String saleUnit;

    @ApiModelProperty(value = "결제번호", position = 20)
    private long paymentNo;
}
