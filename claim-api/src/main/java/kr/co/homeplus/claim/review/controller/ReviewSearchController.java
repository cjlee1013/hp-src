package kr.co.homeplus.claim.review.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.claim.review.model.ReviewSearchGetDto;
import kr.co.homeplus.claim.review.model.ReviewSearchSetDto;
import kr.co.homeplus.claim.review.service.ReviewService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/mp/review")
@Api(tags = "마이페이지 > 리뷰가능수 ")
public class ReviewSearchController {

    private final ReviewService reviewService;

    @PostMapping("/getReviewPossible")
    @ApiOperation(value = "주문내역조회", response = ReviewSearchGetDto.class)
    public ResponseObject<List<ReviewSearchGetDto>> getReviewPossible(@RequestBody ReviewSearchSetDto setDto) throws Exception {
        return reviewService.getReviewInfo(setDto);
    }

}
