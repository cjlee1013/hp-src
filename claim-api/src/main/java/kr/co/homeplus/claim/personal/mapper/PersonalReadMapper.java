package kr.co.homeplus.claim.personal.mapper;

import java.util.List;
import kr.co.homeplus.claim.core.db.annotation.SecondaryConnection;
import kr.co.homeplus.claim.personal.model.PersonalGetDto;
import kr.co.homeplus.claim.personal.model.PersonalSetDto;
import kr.co.homeplus.claim.personal.sql.PersonalSql;
import org.apache.ibatis.annotations.SelectProvider;

@SecondaryConnection
public interface PersonalReadMapper {

    @SelectProvider(type = PersonalSql.class, method = "selectPersonalInfoUsingList")
    List<PersonalGetDto> selectPersonalInfoUsingList(PersonalSetDto personalSetDto);

}
