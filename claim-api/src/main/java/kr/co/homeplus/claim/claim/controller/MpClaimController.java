package kr.co.homeplus.claim.claim.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.claim.claim.model.mp.ClaimChgPaymentRefundSetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimDetailGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimReasonSearchGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimSearchListGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimSearchListSetDto;
import kr.co.homeplus.claim.claim.service.MpClaimService;
import kr.co.homeplus.claim.claim.service.ClaimShipService;
import kr.co.homeplus.claim.order.model.mypage.MpOrderGiftItemListDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderGiftListDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipEditDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipInfoGetDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipInfoSetDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipSetGto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/claim")
@Api(tags = "마이페이지 > 취소/반품/교환")
public class MpClaimController {

    private final MpClaimService mpClaimService;
    private final ClaimShipService claimShipService;

    @PostMapping("/claimSearchList")
    @ApiOperation(value = "취소/반품/교환 조회", response = ClaimSearchListGetDto.class)
    public ResponseObject<List<ClaimSearchListGetDto>> getClaimSearchList(@RequestBody @Valid ClaimSearchListSetDto claimSearchListSetDto) throws Exception {
        return mpClaimService.getClaimSearchList(claimSearchListSetDto);
    }

    @GetMapping("/claimDetailList")
    @ApiOperation(value = "취소/반품/교환 상세내역 조회", response = ClaimDetailGetDto.class)
    public ResponseObject<ClaimDetailGetDto> getClaimDetailList(@RequestParam("userNo") long userNo, @RequestParam("claimNo")long claimNo) throws Exception {
        return mpClaimService.getClaimDetail(userNo, claimNo);
    }

    @GetMapping("/claimReasonCodeSearch/{claimType}")
    @ApiOperation(value = "클레임 사유 코드 조회", response = ClaimSearchListGetDto.class)
    public ResponseObject<List<ClaimReasonSearchGetDto>> getClaimReason(@PathVariable("claimType")String claimType) throws Exception {
        return mpClaimService.getClaimReason(claimType);
    }

    @PostMapping("/preRefundPrice")
    @ApiOperation(value = "환불예정금액 조회", response = ClaimPreRefundCalcGetDto.class)
    public ResponseObject<ClaimPreRefundCalcGetDto> getPreRefundPrice(@RequestBody @Valid ClaimPreRefundCalcSetDto claimPreRefundCalcSetDto) throws Exception {
        return mpClaimService.getPreRefundPrice(claimPreRefundCalcSetDto);
    }

    @PostMapping("/preMultiRefundPrice")
    @ApiOperation(value = "환불예정금액 조회(다중배송주문용)", response = ClaimPreRefundCalcGetDto.class)
    public ResponseObject<ClaimPreRefundCalcGetDto> getPreMultiRefundPrice(@RequestBody @Valid ClaimPreRefundCalcSetDto claimPreRefundCalcSetDto) throws Exception {
        return mpClaimService.getPreMultiRefundPrice(claimPreRefundCalcSetDto);
    }

    @GetMapping("/getOrderGiftItemList")
    @ApiOperation(value = "사은품 상품 리스트 조회", response = MpOrderGiftListDto.class)
    public ResponseObject<List<MpOrderGiftItemListDto>> getOrderGiftItemList(@RequestParam(name = "purchaseOrderNo")long purchaseOrderNo, @RequestParam(name="bundleNo")long bundleNo){
        return mpClaimService.getOrderGiftItemList(purchaseOrderNo, bundleNo);
    }

    @PostMapping("/getNoRcvList")
    @ApiOperation(value = "미수취내역 조회", response = NoRcvShipInfoGetDto.class)
    public ResponseObject<NoRcvShipInfoGetDto> getNoRcvShipInfo(@RequestBody @Valid NoRcvShipInfoSetDto infoSetDto) throws Exception {
        return claimShipService.getNoRcvShipInfo(infoSetDto);
    }

    @PostMapping("/addNoRcvInfo")
    @ApiOperation(value = "미수취정보 등록", response = ResponseObject.class)
    public ResponseObject<String> addNoRcvShipInfo(@RequestBody @Valid NoRcvShipSetGto noRcvShipSetGto) throws Exception {
        return claimShipService.addNoRcvShipInfo(noRcvShipSetGto);
    }

    @PostMapping("/modifyNoRcvInfo")
    @ApiOperation(value = "미수취정보 수정", response = ResponseObject.class)
    public ResponseObject<String> modifyNoRcvShipInfo(@RequestBody @Valid NoRcvShipEditDto editDto) throws Exception {
        return claimShipService.modifyNoRcvShipInfo(editDto);
    }

    @PostMapping("/chgPaymentRefund")
    @ApiOperation(value = "환불수단 변경", response = ResponseObject.class)
    public ResponseObject<String> chgPaymentRefund(@RequestBody @Valid ClaimChgPaymentRefundSetDto dto) throws Exception {
        return mpClaimService.chgPaymentRefund(dto);
    }
}
