package kr.co.homeplus.claim.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"kr.co.homeplus.claim", "kr.co.homeplus.plus"})
public class ClaimApiApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(ClaimApiApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ClaimApiApplication.class);
    }
}
