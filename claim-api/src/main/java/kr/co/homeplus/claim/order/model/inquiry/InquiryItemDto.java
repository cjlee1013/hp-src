package kr.co.homeplus.claim.order.model.inquiry;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class InquiryItemDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;
    @ApiModelProperty(value = "상품주문번호", position = 2)
    private long orderItemNo;
    @ApiModelProperty(value = "아이템번호", position = 3)
    private String itemNo;
    @ApiModelProperty(value = "아이템명", position = 4)
    private String itemNm1;
    @ApiModelProperty(value = "아이템옵션명", position = 5)
    private String itemOptNm;
    @ApiModelProperty(value = "주문일시", position = 6)
    private String orderDt;
    @JsonIgnore
    private int qty;
}
