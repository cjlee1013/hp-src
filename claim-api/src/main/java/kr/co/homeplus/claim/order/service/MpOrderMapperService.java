package kr.co.homeplus.claim.order.service;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.order.model.inquiry.InquiryItemDto;
import kr.co.homeplus.claim.order.model.inquiry.OrderInquiryGetDto;
import kr.co.homeplus.claim.order.model.inquiry.OrderInquirySetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderDetailInfoSetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderDetailListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderClaimPaymentInfoDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderDetailListGetDto.MpOrderDetailClaimDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPresentDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderTicketDetailGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderTicketDetailGetDto.MpOrderTicketItemDto;
import kr.co.homeplus.claim.order.model.mypage.MultiOrderShipAddrEditDto;
import kr.co.homeplus.claim.order.model.mypage.MultiOrderShipBasicInfoEditDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderClaimInfoDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderDetailListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderDetailListSetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderGiftListDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderItemListDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListSetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPickupStoreInfoDto;
import kr.co.homeplus.claim.order.model.mypage.OrderShipAddrEditDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderProductListGetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderShipInfoDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderShipListDto;
import kr.co.homeplus.claim.order.model.ship.MultiOrderShipAddrInfoDto;
import kr.co.homeplus.claim.order.model.subMain.MpOrderMainDto.MyPageMainOrderCount;
import kr.co.homeplus.claim.order.model.subMain.MpOrderMainDto.MyPageMainOrderList;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPaymentInfoDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderProductListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderShipInfoDto;
import kr.co.homeplus.claim.order.model.user.CustomerGradeInfoGetDto.CustomerGradeOrderListDto;

public interface MpOrderMapperService {

    List<MpOrderListGetDto> getMpOrderList(MpOrderListSetDto mpOrderListSetDto);

    List<MpOrderItemListDto> getMpOrderListInfo(long purchaseOrderNo, long bundleNo, long userNo, Boolean isCmbn, Boolean isSubYn);

    List<MpOrderShipInfoDto> getMpOrderShipInfo(MpOrderDetailListSetDto mpOrderDetailListSetDto);

    List<MpOrderProductListGetDto> getMpOrderProductList(long purchaseOrderNo, long userNo, Boolean isCmbn);

    MpOrderDetailListGetDto getOrderDetailInfo(long purchaseOrderNo, long userNo, String siteType, Boolean isCmbn);

    MpMultiOrderDetailListGetDto getMultiOrderDetailList(long purchaseOrderNo, long userNo, String siteType);

    MpOrderPaymentInfoDto getPaymentInfoList(long purchaseOrderNo, long userNo);

    MpOrderClaimPaymentInfoDto getClaimPaymentInfoList(long purchaseOrderNo, long userNo);

    List<MyPageMainOrderCount> getMyPageMainOrderCount(long userNo, String siteType);

    List<MyPageMainOrderList> getMyPageMainOrderList(long userNo, String siteType);

    Boolean modifyShipAddrChange(OrderShipAddrEditDto shipAddrEditDto);

    List<LinkedHashMap<String, Object>> getShippingPresentStatus(long purchaseOrderNo);

    List<MpOrderClaimInfoDto> getOrderClaimBundleInfo(long purchaseOrderNo, long bundleNo, Boolean isOrigin);

    List<MpOrderClaimInfoDto> getOrderClaimBundleInfo(long purchaseOrderNo);

    List<MpOrderGiftListDto> getOrderGiftInfo(long purchaseOrderNo);
    /**
     * 픽업시 점포 정보
     * @param purchaseOrderNo 주문번호
     * @return 픽업장소정보
     */
    MpOrderPickupStoreInfoDto getOrderPickupStoreInfo(long purchaseOrderNo);

    List<MpMultiOrderProductListGetDto> getMpOrderMultiProductList(long purchaseOrderNo, long multiBundleNo, long userNo);

    List<MpMultiOrderShipListDto> getMpMultiOrderShipList(MpOrderDetailListSetDto mpOrderDetailListSetDto);

    MpMultiOrderShipInfoDto getMpOrderMultiShipInfo(MpMultiOrderDetailInfoSetDto mpMultiOrderDetailInfoSetDto);

    MpOrderTicketDetailGetDto getOrderTicketDetailInfo(String purchaseOrderNo, long userNo);

    List<MpOrderTicketItemDto> getOrderTicketDetailItemInfo(String purchaseOrderNo, long userNo);

    List<MpOrderDetailClaimDto> getOrderDetailClaimInfo(long purchaseOrderNo);

    List<MpOrderDetailClaimDto> getMultiOrderDetailClaimInfo(long purchaseOrderNo);

    LinkedHashMap<String, Object> getSafetyIssueInfo(long bundleNo);

    MpOrderPresentDto getMpOrderPresentInfo(long purchaseOrderNo);

    LinkedHashMap<String, String> getClaimShippingStatus(long claimNo);

    List<CustomerGradeOrderListDto> getUserGradeOrderInfo(long userNo, String searchDt, String siteType);

    String getTicketFromCoopCheck(long purchaseOrderNo);

    int getClaimSubstitutionAmt(long purchaseOrderNo);

    List<OrderInquiryGetDto> getInquiryOrderItemInfo(OrderInquirySetDto setDto);

    List<InquiryItemDto> getInquiryOrderItemList(long bundleNo);

    boolean modifyMultiOrderShipAddrInfo(MultiOrderShipAddrEditDto multiOrderShipAddrEditDto);

    boolean modifyMultiOrderShipBasicInfo(MultiOrderShipBasicInfoEditDto multiOrderShipBasicInfoEditDto);

    List<MultiOrderShipAddrInfoDto> getMultiOrderShipAddrInfo(long bundleNo);

    List<CustomerGradeOrderListDto> getUserGradeOrderInfoByClaim(long userNo, String searchDt, String siteType);

    Boolean modifyOrderUserRecentlyInfo(String auroraShipMsgCd, String auroraShipMsg, long userNo);
}
