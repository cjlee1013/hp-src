package kr.co.homeplus.claim.order.sql;

import kr.co.homeplus.claim.constants.CustomConstants;
import kr.co.homeplus.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claim.entity.claim.ClaimExchShippingEntity;
import kr.co.homeplus.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claim.entity.claim.ClaimMstEntity;
import kr.co.homeplus.claim.entity.claim.ClaimPaymentEntity;
import kr.co.homeplus.claim.entity.claim.ClaimPickShippingEntity;
import kr.co.homeplus.claim.entity.dms.ItmPartnerSellerEntity;
import kr.co.homeplus.claim.entity.dms.ItmPartnerSellerShopEntity;
import kr.co.homeplus.claim.entity.order.BundleEntity;
import kr.co.homeplus.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claim.entity.order.OrderTicketEntity;
import kr.co.homeplus.claim.entity.order.PaymentEntity;
import kr.co.homeplus.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claim.entity.order.PurchaseStoreInfoEntity;
import kr.co.homeplus.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claim.entity.ship.ShippingNonRcvEntity;
import kr.co.homeplus.claim.entity.ship.StoreShiftMngEntity;
import kr.co.homeplus.claim.entity.ship.StoreSlotMngEntity;
import kr.co.homeplus.claim.enums.MysqlFunction;
import kr.co.homeplus.claim.factory.EntityFactory;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListSetDto;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MypageOrderShipSql {

    public static String selectMypageOrderList(MpOrderListSetDto setDto){
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderDt), CustomConstants.YYYYMMDD), purchaseOrder.orderDt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderType, purchaseOrder.orderType),
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, payment.paymentStatus), "P5"), "'P3'", payment.paymentStatus), payment.paymentStatus),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.cmbnYn, purchaseOrder.cmbnYn),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.substitutionOrderYn, purchaseOrder.substitutionOrderYn),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseOrder.cmbnYn, "N"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE_Y_N,
                            SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimBundle.purchaseOrderNo), 0)
                        ),
                        "'N'"
                    ),
                    "order_all_cancel_possible_yn"
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.nomemOrderYn, "no_member_order_yn")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .WHERE(
                SqlUtils.betweenDay(purchaseOrder.orderDt, setDto.getSchStartDt(), setDto.getSchEndDt()),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, setDto.getUserNo()),
                SqlUtils.equalColumnByInput(purchaseOrder.siteType, setDto.getSiteType()),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_SUB'", "'ORD_COMBINE'"))
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo)
            .ORDER_BY(SqlUtils.orderByesc(purchaseOrder.orderDt, purchaseOrder.purchaseOrderNo))
            .LIMIT(setDto.getPerPage())
            .OFFSET((setDto.getPage() - 1) * setDto.getPerPage());
        log.debug("selectMypageOrderList \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMypageOrderBundleList(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ItmPartnerSellerEntity itmPartnerSeller = EntityFactory.createEntityIntoValue(ItmPartnerSellerEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
                orderItem.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.storeType, orderItem.storeType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.mallType, orderItem.mallType),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.mallType), "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.storeId),
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray("NOW()", "%s%i")), itmPartnerSeller.sellerSeq, SqlUtils.sqlFunction(MysqlFunction.RIGHT, ObjectUtils.toArray("RAND()", 2)))))
                                .FROM(EntityFactory.getTableNameWithAlias("dms", itmPartnerSeller))
                                .WHERE(
                                    SqlUtils.equalColumn(itmPartnerSeller.partnerId, SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.partnerId))
                                )
                        )
                    ),
                    orderItem.partnerId
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.mallType), "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.originStoreId), "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.partnerId), "P"))
                    ),
                    "partner_nm"
                ),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, "'M'"), "ship_type"),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, "'C'"), "sort_type")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            )
            .GROUP_BY(orderItem.purchaseOrderNo, orderItem.bundleNo)
            .ORDER_BY(SqlUtils.orderByesc(orderItem.purchaseOrderNo, orderItem.bundleNo));

        SQL query = new SQL()
            .SELECT("bundle_no", "store_type", "mall_type", "partner_id", "partner_nm", "ship_type")
            .FROM(SqlUtils.subTableQuery(innerQuery, "bundleList"))
            .ORDER_BY(SqlUtils.orderByesc("bundleList.sort_type"));

        log.debug("selectMypageOrderBundleList \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMypageOrderBundleShipInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        PurchaseStoreInfoEntity purchaseStoreInfo = EntityFactory.createEntityIntoValue(PurchaseStoreInfoEntity.class, Boolean.TRUE);
        StoreSlotMngEntity storeSlotMng = EntityFactory.createEntityIntoValue(StoreSlotMngEntity.class, Boolean.TRUE);
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shipStatus, shippingItem.shipStatus),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shipMethod, shippingItem.shipMethod),
                SqlUtils.sqlFunction(MysqlFunction.FN_ESTIMATED_DELIVERY, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.orderItemNo)), "ship_dt"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.reserveShipDt)),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.reserveShipDt), CustomConstants.YYYYMMDD_COMMA)),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.scheduleShipDt)),
                                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.scheduleShipDt), CustomConstants.YYYYMMDD_COMMA)),
                                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shippingDt), CustomConstants.YYYYMMDD_COMMA))
                            )
                        )
                    ),
                    shippingItem.shippingDt
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.completeRegId, shippingItem.completeRegId),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.completeDt), CustomConstants.YYYYMMDD_COMMA), shippingItem.completeDt),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.PLUS,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.MAX, storeSlotMng.shipDt),
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, storeSlotMng.slotShipStartTime), "'00'"))
                            )
                        ),
                        CustomConstants.HHMI
                    ),
                    "ship_start_time"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.PLUS,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.MAX, storeSlotMng.shipDt),
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, storeSlotMng.slotShipEndTime), "'00'"))
                            )
                        ),
                        CustomConstants.HHMI
                    ),
                    "ship_end_time"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.PLUS,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.MAX, storeSlotMng.shipDt),
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CONCAT,
                                    ObjectUtils.toArray(
                                        SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseStoreInfo.slotCloseTime),
                                        "'00'"
                                    )
                                )
                            )
                        )
                        , CustomConstants.HHMI
                    ),
                    "ship_close_time"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, SqlUtils.sqlFunction(MysqlFunction.MAX, shippingNonRcv.noRcvProcessYn)),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, shippingNonRcv.noRcvProcessYn), "Y"),
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CASE,
                                    ObjectUtils.toArray(
                                        SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, shippingNonRcv.noRcvWthdrwYn), "Y"),
                                        "'W'", "'C'"
                                    )
                                ),
                                "'Y'"
                            )
                        ),
                        "'N'"
                    ),
                    "no_rcv_yn"
                ),
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, shippingItem.delayShipCd, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.delayShipCd)), shippingItem.delayShipCd),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.delayShipMsg, shippingItem.delayShipMsg),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.prepaymentYn, bundle.prepaymentYn),
                SqlUtils.aliasToRow("'N'", "cmcb_possible_yn"),
                SqlUtils.aliasToRow("'N'", "change_date_possible_yn"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderDt, purchaseOrder.orderDt),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseStoreInfo.anytimeSlotYn), "'N'" ), purchaseStoreInfo.anytimeSlotYn)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.orderItemNo, orderItem.bundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(purchaseStoreInfo, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.originStoreId, bundle.purchaseStoreInfoNo), ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.NOT_EQUAL, purchaseStoreInfo.storeKind, "'DLV'"))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(storeSlotMng, ObjectUtils.toArray( purchaseStoreInfo.shipDt, purchaseStoreInfo.storeId, purchaseStoreInfo.slotId, purchaseStoreInfo.shiftId )))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(shippingNonRcv, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(bundle.bundleNo, bundleNo)
            )
            .GROUP_BY(orderItem.purchaseOrderNo, orderItem.bundleNo)
            .ORDER_BY(SqlUtils.orderByesc(orderItem.purchaseOrderNo, orderItem.bundleNo));

        log.debug("selectMypageOrderBundleShipInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMypageOrderItemList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo, @Param("searchType") String searchType) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.paymentNo,
                orderItem.orderItemNo,
                orderItem.bundleNo,
                orderItem.itemNo,
                SqlUtils.aliasToRow(orderItem.itemNm1, "item_nm"),
                orderItem.orgItemNo,
                orderItem.itemQty,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.AND(
                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                        ),
                        claimItem.claimItemQty,
                        "0"
                    ),
                    claimItem.claimItemQty
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C3'", "'C4'", "'C9'")),
                        claimItem.claimItemQty,
                        "0"
                    ),
                    "process_item_qty"
                ),
                orderItem.itemPrice,
                shippingItem.shipNo,
                orderItem.saleUnit,
                SqlUtils.sqlFunction(MysqlFunction.CASE_Y_N, SqlUtils.equalColumnByInput(orderItem.reviewYn, "N"), "review_possible_yn"),
                SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.sqlFunction(MysqlFunction.IS_NULL,orderItem.orgItemNo), "substitution_item_yn"),
                orderItem.reserveShipMethod
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.orderItemNo, orderItem.bundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, orderItem.orderItemNo)))
            .ORDER_BY(SqlUtils.orderByesc(orderItem.orderItemNo));

        switch (searchType) {
            case "SUB" :
                innerQuery.WHERE(
                    SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo),
                    SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB"),
                    SqlUtils.equalColumnByInput(bundle.orgBundleNo, bundleNo)
                );
                break;
            case "COMBINE" :
                innerQuery.WHERE(
                    SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo),
                    SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE"),
                    SqlUtils.equalColumnByInput(bundle.orgBundleNo, bundleNo)
                );
                break;
            default:
                innerQuery.WHERE(
                    SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                    SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo)
                );
                break;
        }

        SQL query = new SQL()
            .SELECT(
                "purchase_order_no",
                "order_item_no",
                SqlUtils.sqlFunction(MysqlFunction.MAX, "payment_no", "payment_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "bundle_no", "bundle_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_no", "item_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_nm", "item_nm"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "org_item_no", "org_item_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_qty", "order_item_qty"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "claim_item_qty", "claim_item_qty"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "process_item_qty", "process_item_qty"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_price", "item_price"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "ship_no", "ship_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "sale_unit", "sale_unit"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "review_possible_yn", "review_possible_yn"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "substitution_item_yn", "substitution_item_yn"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "reserve_ship_method", "reserve_ship_method")
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "itemList"))
            .GROUP_BY("itemList.purchase_order_no", "itemList.order_item_no")
            .ORDER_BY("itemList.substitution_item_yn");

        log.debug("selectMypageOrderItemList \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMypageCombineOrderSubstitutionCheck(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.orgPurchaseOrderNo,
                orderItem.bundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE"),
                SqlUtils.equalColumnByInput(purchaseOrder.substitutionOrderYn, "Y")
            );
        log.debug("selectMypageCombineOrderSubstitutionCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMypageOrderClaimInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo){
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class, Boolean.TRUE);
        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                claimBundle.claimNo,
                claimBundle.claimType,
                claimBundle.claimStatus,
                SqlUtils.aliasToRow(claimPickShipping.pickStatus, "pick_ship_status"),
                SqlUtils.aliasToRow(claimExchShipping.exchStatus, "exchange_ship_status"),
                SqlUtils.aliasToRow("'N'", "cancel_possible_yn"),
                SqlUtils.aliasToRow("'N'", "return_possible_yn"),
                SqlUtils.aliasToRow("'N'", "exchange_possible_yn"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_Y_N,
                    SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C3'", "'C4'", "'C9'")),
                    "claim_process_yn"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimPickShipping, ObjectUtils.toArray(claimBundle.claimBundleNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimExchShipping, ObjectUtils.toArray(claimBundle.claimBundleNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(claimBundle.bundleNo, bundleNo),
                SqlUtils.equalColumnByInput(claimMst.oosYn, "N")
            )
            .ORDER_BY(SqlUtils.orderByesc(claimMst.claimNo))
            ;
        log.debug("selectMypageOrderClaimInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectAutoCancelTicketInfo(@Param("bundleNo") long bundleNo) {
        OrderTicketEntity orderTicket = EntityFactory.createEntityIntoValue(OrderTicketEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                orderTicket.orderItemNo,
                orderTicket.orderTicketNo,
                orderTicket.purchaseOrderNo,
                orderTicket.issueChannel,
                orderTicket.ticketStatus,
                orderTicket.issueStatus
            )
            .FROM(EntityFactory.getTableNameWithAlias(orderTicket))
            .WHERE(
                SqlUtils.equalColumnByInput(orderTicket.bundleNo, bundleNo),
                SqlUtils.sqlFunction(MysqlFunction.IN, orderTicket.ticketStatus, ObjectUtils.toArray("'T0'", "'T1'")),
                SqlUtils.OR(
                    Boolean.TRUE,
                    SqlUtils.sqlFunction(MysqlFunction.IS_NULL, orderTicket.ticketValidStartDt),
                    SqlUtils.sqlFunction(MysqlFunction.BETWEEN, ObjectUtils.toArray("NOW()", orderTicket.ticketValidStartDt, orderTicket.ticketValidEndDt))
                )
            );
        log.debug("selectAutoCancelTicketInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimRefundFailCheck(@Param("claimNo") long claimNo) {
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.CASE_Y_N, SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.paymentStatus), "P4"), "is_refund_fail")
            )
            .FROM(EntityFactory.getTableName(claimPayment))
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.claimNo, claimNo)
            );
        log.debug("selectClaimRefundFailCheck \n[{}]", query.toString());
        return query.toString();
    }

}
