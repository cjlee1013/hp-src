package kr.co.homeplus.claim.ship.service.impl;

import java.util.List;
import kr.co.homeplus.claim.ship.mapper.ShipReadMapper;
import kr.co.homeplus.claim.ship.model.DlvCdGetDto;
import kr.co.homeplus.claim.ship.model.ShipMsgCdGetDto;
import kr.co.homeplus.claim.ship.service.ShipMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ShipMapperServiceImpl implements ShipMapperService {

    private final ShipReadMapper shipReadMapper;

    @Override
    public List<DlvCdGetDto> getDlvCd() {
        return shipReadMapper.selectDlvCd();
    }

    @Override
    public List<ShipMsgCdGetDto> getShipMsg() {
        return shipReadMapper.selectShipMsg();
    }
}
