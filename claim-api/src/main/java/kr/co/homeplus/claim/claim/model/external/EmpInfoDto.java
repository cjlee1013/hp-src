package kr.co.homeplus.claim.claim.model.external;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "임직원할인한도 조회")
public class EmpInfoDto {
    @ApiModelProperty(value = "임직원사번")
    private String employeeNo;
    @ApiModelProperty(value = "임직원성명")
    private String employeeNm;
    @ApiModelProperty(value = "임직원상태(A:재직,D:퇴직)")
    private String workstatus;
    @ApiModelProperty(value = "카드번호 RKM암호화(HMAC)")
    private String cardNoH;
    @ApiModelProperty(value = "카드번호 복호화(Prefix만 보임)")
    private String cardNo;
    @ApiModelProperty(value = "사용구분(Y:사용),그이외 카드사용불가")
    private String useYn;
    @ApiModelProperty(value = "잔액")
    private int remainPoint;
    @ApiModelProperty(value = "6개월미만입사구분(현재 사용처 없음)")
    private String joinNosaleYn;
    @ApiModelProperty(value = "임직원유형(00:임직원)")
    private String employeeKind;
}
