package kr.co.homeplus.claim.event.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@ApiModel(description = "이벤트 주문번호 체크 요청 DTO")
public class PurchaseEventCheckSetDto {
    @ApiModelProperty(value = "주문번호")
    private long purchaseOrderNo;
    @ApiModelProperty(value = "조회시작일")
    private String validStartDt;
    @ApiModelProperty(value = "조회종료일")
    private String validEndDt;
    @ApiModelProperty(value = "고객번호")
    private long userNo;
    @ApiModelProperty(value = "상점타입")
    private String storeType;
}
