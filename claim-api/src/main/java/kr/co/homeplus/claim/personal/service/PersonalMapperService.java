package kr.co.homeplus.claim.personal.service;

import java.util.List;
import kr.co.homeplus.claim.personal.model.PersonalGetDto;
import kr.co.homeplus.claim.personal.model.PersonalSetDto;

public interface PersonalMapperService {
    List<PersonalGetDto> getPersonalInfoUsingList(PersonalSetDto personalSetDto);
}
