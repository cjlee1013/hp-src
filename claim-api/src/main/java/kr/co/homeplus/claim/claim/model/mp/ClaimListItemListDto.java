package kr.co.homeplus.claim.claim.model.mp;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimListItemListDto {

    @ApiModelProperty(value = "클레임 상품 번호", position = 1)
    private String claimItemNo;

    @ApiModelProperty(value = "상품 번호", position = 2)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 3)
    private String itemName;

    @ApiModelProperty(value="상품가격", position = 4)
    private String itemPrice;

    @ApiModelProperty(value="클레임 수량", position = 5)
    private String claimItemQty;

    @ApiModelProperty(value="행사상품여부", position = 6)
    private String promoYn;

    @ApiModelProperty(value="대체상품명", position = 7)
    private String subItemNm;

    @ApiModelProperty(value = "배송방법(DS_DLV:택배배송,DS_DRCT:직접배송,DS_PICK:방문수령/픽업,DS_POST:우편배송,DS_QUICK:퀵배송,TD_DLV:택배배송,TD_DRCT:직접배송,TD_PICK:방문수령,TD_POST:우편배송,TD_QUICK:퀵배송)", position = 8)
    private String shipMethod;

    @ApiModelProperty(value = "운송번호", position = 9)
    private long shipNo;
}
