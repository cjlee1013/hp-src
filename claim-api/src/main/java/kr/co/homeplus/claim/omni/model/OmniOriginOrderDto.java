package kr.co.homeplus.claim.omni.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "옴니 결품/대체에 대한 원 주문 DTO")
public class OmniOriginOrderDto {
    @ApiModelProperty(value = "상품번호", position = 1)
    private String itemNo;
    @ApiModelProperty(value = "상품수량", position = 2)
    private String itemQty;
    @ApiModelProperty(value = "상품주문번호", position = 3)
    private String orderItemNo;
    @ApiModelProperty(value = "상품옵션번호", position = 4)
    private String orderOptNo;
    @ApiModelProperty(value = "번들번호", position = 5)
    private String bundleNo;
    @ApiModelProperty(value = "고객번호", position = 6)
    private String userNo;
    @ApiModelProperty(value = "클레임아이템수량", position = 7)
    private String claimItemQty;
    @ApiModelProperty(value = "대체가능여부", position = 8)
    private String substitutionYn;
    @ApiModelProperty(value = "주문타입", position = 9)
    private String orderType;
}
