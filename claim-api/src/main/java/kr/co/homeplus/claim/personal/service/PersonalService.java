package kr.co.homeplus.claim.personal.service;

import java.util.List;
import kr.co.homeplus.claim.personal.model.PersonalGetDto;
import kr.co.homeplus.claim.personal.model.PersonalSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.client.model.ResponsePagination;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class PersonalService {

    private final PersonalMapperService mapperService;

    public ResponseObject<List<PersonalGetDto>> getPersonalUsingInfo(PersonalSetDto personalSetDto) {
        List<PersonalGetDto> personalList = mapperService.getPersonalInfoUsingList(personalSetDto);
        int page = personalSetDto.getPage();
        personalSetDto.setPage(0);
        int totalCount = mapperService.getPersonalInfoUsingList(personalSetDto).size();
        return ResourceConverter.toResponseObject(
            personalList,
            ResponsePagination.Builder.builder().setAsPage(page, personalSetDto.getPerPage(), getTotalPage(totalCount, personalSetDto.getPerPage())).totalCount(totalCount).build()
        );
    }

    private Integer getTotalPage(int totalCount, int perPage) {
        int totalPage = totalCount / perPage;
        if(totalCount % perPage > 0){
            totalPage ++;
        }
        return totalPage;
    }
}
