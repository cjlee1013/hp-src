package kr.co.homeplus.claim.claim.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import kr.co.homeplus.claim.claim.model.external.ClaimDgvCancelGetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimDgvCancelSetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimMhcCancelGetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimMhcCancelSetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimMileageCancelGetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimMileageCancelSetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimOcbCancelGetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimOcbCancelSetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimPgCancelGetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimPgCancelSetDto;
import kr.co.homeplus.claim.claim.model.external.RestoreSlotStockDto;
import kr.co.homeplus.claim.claim.model.external.forward.ClaimForwardSetDto;
import kr.co.homeplus.claim.claim.model.external.mhc.SaveCancelMhcGetDto;
import kr.co.homeplus.claim.claim.model.external.mhc.SaveCancelMhcPartSetDto;
import kr.co.homeplus.claim.claim.model.external.mhc.SaveCancelMhcSetDto;
import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.enums.ExceptionCode;
import kr.co.homeplus.claim.enums.ExternalInfo;
import kr.co.homeplus.claim.enums.ResponseCode;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.RequestUtils;
import kr.co.homeplus.claim.utils.SetParameter;
import kr.co.homeplus.claim.utils.SqlUtils;
import kr.co.homeplus.claim.utils.StringUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.exception.ResourceClientException;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.ResolvableType;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ExternalService {

    private final ResourceClient resourceClient;

    public ClaimPgCancelGetDto externalPgPayment(ClaimPgCancelSetDto claimPgCancelSetDto) throws Exception {
        return this.postTransfer(ExternalInfo.PAYMENT_CANCEL_MILEAGE, claimPgCancelSetDto, ClaimPgCancelGetDto.class);
    }

    /**
     * forward를 활용한 옴니 통신.
     *
     * @param forwardSetDto
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> externalToOmni(ClaimForwardSetDto forwardSetDto) throws Exception {
        return this.postTransfer(ExternalInfo.FORWARD_OMNI_TRANS, forwardSetDto, null, createResponseType(Object.class));
    }

    /**
     * 마일리지 취소 처리
     *
     * @param claimMileageCancelSetDto 마일리지 취소 DTO
     * @return ClaimMileageCancelGetDto 마일리지 취소 응답 DTO
     * @throws Exception 오류시 오류처리 넘김
     */
    public LinkedHashMap<String, Object> externalCancelMileage(
        ClaimMileageCancelSetDto claimMileageCancelSetDto) throws Exception {
        return ObjectUtils.getConvertMap(this.postTransfer(ExternalInfo.PAYMENT_CANCEL_MILEAGE,
            claimMileageCancelSetDto, ClaimMileageCancelGetDto.class));
    }

    /**
     * 외부 통신용
     *
     * @param dtoObject 보내고자하는 데이터
     * @return 통시결과
     * @throws Exception 오류발생시 오류처리
     */
    public LinkedHashMap<String, Object> externalProcess(Object dtoObject) throws Exception {
        if(dtoObject instanceof ClaimMileageCancelSetDto) {
            log.info("마일리지 결제취소 요청정보 :: {}", dtoObject);
            return ObjectUtils.getConvertMap(this.postTransfer(ExternalInfo.PAYMENT_CANCEL_MILEAGE, dtoObject, ClaimMileageCancelGetDto.class));
        } else if (dtoObject instanceof ClaimPgCancelSetDto) {
            log.info("PG 결제취소 요청정보 :: {}", dtoObject);
            return ObjectUtils.getConvertMap(this.postTransfer(ExternalInfo.PAYMENT_CANCEL_PG, dtoObject, ClaimPgCancelGetDto.class));
        } else if(dtoObject instanceof RestoreSlotStockDto) {
            return ObjectUtils.getConvertMap(this.getNonDataTransfer(ExternalInfo.RESTORE_SLOT_CLAIM, convertDtoToSetParameter(dtoObject), String.class));
        } else if(dtoObject instanceof ClaimMhcCancelSetDto) {
            if(((ClaimMhcCancelSetDto) dtoObject).getPartCancelYn().equals("Y")){
                log.info("MHC 부분결제취소 요청정보 :: {}", dtoObject);
                return ObjectUtils.getConvertMap(this.postTransfer(ExternalInfo.PAYMENT_CANCEL_MHC_PART, dtoObject, ClaimMhcCancelGetDto.class));
            }
            log.info("MHC 결제취소 요청정보 :: {}", dtoObject);
            return ObjectUtils.getConvertMap(this.postTransfer(ExternalInfo.PAYMENT_CANCEL_MHC_ALL, dtoObject, ClaimMhcCancelGetDto.class));
        } else if(dtoObject instanceof ClaimOcbCancelSetDto) {
            log.info("OCB 결제취소 요청정보 :: {}", dtoObject);
            return ObjectUtils.getConvertMap(this.postTransfer(ExternalInfo.PAYMENT_CANCEL_OCB, dtoObject, ClaimOcbCancelGetDto.class));
        } else if(dtoObject instanceof SaveCancelMhcSetDto) {
            log.info("MHC 적립취소 요청정보 :: {}", dtoObject);
            return ObjectUtils.getConvertMap(this.postTransfer(ExternalInfo.PAYMENT_SAVE_CANCEL_MHC_ALL, dtoObject, SaveCancelMhcGetDto.class));
        } else if(dtoObject instanceof SaveCancelMhcPartSetDto) {
            log.info("MHC 부분적립취소 요청정보 :: {}", dtoObject);
            return ObjectUtils.getConvertMap(this.postTransfer(ExternalInfo.PAYMENT_SAVE_CANCEL_MHC_PART, dtoObject, SaveCancelMhcGetDto.class));
        } else if(dtoObject instanceof ClaimDgvCancelSetDto) {
            log.info("DGV 결제취소 요청정보 :: {}", dtoObject);
            return ObjectUtils.getConvertMap(this.postTransfer(ExternalInfo.PAYMENT_CANCEL_DGV, dtoObject, ClaimDgvCancelGetDto.class));
        } else {
            // 처리 필요asa
            return null;
        }
    }

    /**
     * 외부연동 - METHOD.GET
     * GET방식으로 외부연동을 한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public <T> T getTransfer(ExternalInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return getResponseObject(getTransfer(apiInfo, parameter, null, createType(returnClass)));
    }

    public <T> ResponseObject<T> getNonDataTransfer(ExternalInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return getTransfer(apiInfo, parameter, null, createType(returnClass));
    }

    public <T> ResponseObject<T> postNonDataTransfer(ExternalInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return postTransfer(apiInfo, parameter, null, createType(returnClass));
    }

    /**
     * 외부연동 - METHOD.GET (feat. List)
     * GET방식으로 외부연동을 한다.
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> getListTransfer(ExternalInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(getTransfer(apiInfo, parameter, null, createListType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header, List / Feat. ResponseObject)
     * GET 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public <T> ResponseObject<List<T>> getResponseTransfer(ExternalInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) getResponseObject(getTransfer(apiInfo, parameter, null, createResponseType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header)
     * GET방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public <T> T getTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return getResponseObject(getTransfer(apiInfo, parameter, httpHeaders, createType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header / feat. List)
     * GET방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> getListTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(getTransfer(apiInfo, parameter, httpHeaders, createListType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header, List / Feat. ResponseObject)
     * GET 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public <T> ResponseObject<List<T>> getResponseTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) getResponseObject(getTransfer(apiInfo, parameter, httpHeaders, createResponseType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST
     * POST 방식으로 외부연동을 한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public <T> T postTransfer(ExternalInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return getResponseObject(postTransfer(apiInfo, parameter, null, createType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST (Feat. List)
     * POST 방식으로 외부연동을 한다.
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> postListTransfer(ExternalInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(postTransfer(apiInfo, parameter, null, createListType(returnClass)));
    }

    /**
     /**
     * 외부연동 - METHOD.POST (include. header, List / Feat. ResponseObject)
     * POST 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public <T> ResponseObject<List<T>> postResponseTransfer(ExternalInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) getResponseObject(postTransfer(apiInfo, parameter, null, createResponseType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST (include. header)
     * POST 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public <T> T postTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return getResponseObject(postTransfer(apiInfo, parameter, httpHeaders, createType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST (include. header / feat. List)
     * POST 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> postListTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(postTransfer(apiInfo, parameter, httpHeaders, createListType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST (include. header, List / Feat. ResponseObject)
     * POST 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public <T> ResponseObject<List<T>> postResponseTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) getResponseObject(postTransfer(apiInfo, parameter, httpHeaders, createResponseType(returnClass)));
    }

    /**
     * Get 방식 통신 모듈
     * 헤더 정보를 입력받아 해당 헤더 정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더정보
     * @param typeReference 응답받고자 하는 클레스의 ParameterizedTypeReference
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    private <T> ResponseObject<T> getTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, ParameterizedTypeReference<ResponseObject<T>> typeReference) throws Exception {
        if(httpHeaders == null){
            httpHeaders = RequestUtils.createHttpHeaders();
        }
        ResponseObject<T> responseObject = new ResponseObject<>();
        try{
            if(parameter == null) {
                responseObject = resourceClient.getForResponseObject( apiInfo.getApiId(), apiInfo.getUri(), httpHeaders, typeReference );
            } else if(parameter instanceof List){
                responseObject = resourceClient.getForResponseObject( apiInfo.getApiId(), apiInfo.getUri() + StringUtil.getParameter((List<SetParameter>) parameter), httpHeaders, typeReference );
            } else {
                responseObject = resourceClient.getForResponseObject( apiInfo.getApiId(), apiInfo.getUri().concat("/").concat(ObjectUtils.toString(parameter)), httpHeaders, typeReference );
            }

        } catch (ResourceClientException rce) {
            log.error("getTransfer Module Error ::: {}", rce.getMessage());
            if (responseObject.getReturnStatus() == 0) {
                responseObject.setReturnStatus(rce.getHttpStatus());
            }
            return (ResponseObject<T>) getResponseObject(responseObject);
        }
        return responseObject;
//        throw new LogicException(ExceptionCode.SYS_ERROR_CODE_9001);
    }

    /**
     * POST 방식 통신 모듈
     * 헤더 정보를 입력받아 해당 헤더 정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더정보
     * @param typeReference 응답받고자 하는 클레스의 ParameterizedTypeReference
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    private <T> ResponseObject<T> postTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, ParameterizedTypeReference<ResponseObject<T>> typeReference) throws Exception {
        try{
            if(httpHeaders == null){
                httpHeaders = RequestUtils.createHttpHeaders();
            }
            if(parameter instanceof List){
                return resourceClient.postForResponseObject( apiInfo.getApiId(), null, apiInfo.getUri() + StringUtil.getParameter((List<SetParameter>) parameter), httpHeaders, typeReference );
            }
            return resourceClient.postForResponseObject( apiInfo.getApiId(), parameter, apiInfo.getUri(), httpHeaders, typeReference );
        } catch (ResourceClientException rce) {
            log.error("postTransfer Error ::: {}", rce.getMessage());
            rce.printStackTrace();
        }
        throw new LogicException(ExceptionCode.SYS_ERROR_CODE_9001);
    }

    private <T> ParameterizedTypeReference<ResponseObject<T>> createType(Class<T> returnClass){
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, returnClass);
        return ParameterizedTypeReference.forType(resolvableType.getType());
    }

    private <T> ParameterizedTypeReference<ResponseObject<T>> createListType(Class<T> returnClass){
        ResolvableType resolvableListType = ResolvableType.forClassWithGenerics(List.class, returnClass);
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, resolvableListType);
        return ParameterizedTypeReference.forType(resolvableType.getType());
    }

    private <T> ParameterizedTypeReference<ResponseObject<T>> createResponseType(Class<T> returnClass){
        ResolvableType resolvableListType = ResolvableType.forClassWithGenerics(List.class, returnClass);
        ResolvableType resolvableResponseType = ResolvableType.forClassWithGenerics(ResponseObject.class, resolvableListType);
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, resolvableResponseType);
        return ParameterizedTypeReference.forType(resolvableType.getType());
    }

    private <T> T getResponseObject(ResponseObject<T> responseObject) throws Exception {
        // responseObject Null Check
        assert responseObject != null;
        log.debug("{} :: {} :: {} :: {}", responseObject.getReturnCode(), responseObject.getReturnMessage(), responseObject.getData(), responseObject.getReturnStatus());
        // httpStatus 가 200이 아닌 경우
        if(responseObject.getReturnStatus() != 200){
            throw new LogicException(ExceptionCode.SYS_ERROR_CODE_9001);
        }
        // Return Code Check
        if(responseObject.getReturnCode().equalsIgnoreCase("0000") || responseObject.getReturnCode().equalsIgnoreCase("SUCCESS")) {
            return responseObject.getData();
        }
        // 정상이 아닐 경우 오류 처리.
        throw new LogicException(responseObject.getReturnCode(), responseObject.getReturnMessage());
    }

    public List<SetParameter> convertDtoToSetParameter(Object dto) throws Exception {
        List<SetParameter> resultList = new ArrayList<>();
        SqlUtils.getConvertMap(dto)
            .forEach((key, value) -> resultList.add(SetParameter.create(key, value)));
        return resultList;
    }
}
