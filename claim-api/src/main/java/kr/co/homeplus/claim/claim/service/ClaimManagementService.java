package kr.co.homeplus.claim.claim.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import java.util.List;
import kr.co.homeplus.claim.claim.model.ClaimOrderCancelDto;
import kr.co.homeplus.claim.claim.model.ClaimRequestSetDto;
import kr.co.homeplus.claim.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.enums.ClaimCode;
import kr.co.homeplus.claim.enums.ClaimResponseCode;
import kr.co.homeplus.claim.utils.ObjectUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClaimManagementService {

    private final ClaimProcessService processService;

    private final ClaimCommonService commonService;

    @SuppressWarnings("unchecked")
    public LinkedHashMap<String, Object> orderTotalCancel(ClaimRegSetDto regSetDto) throws Exception {
        // purchaseOrderNo 으로 주문건수 가지고 옴.
        // List 로 취소 데이터 생성하여 처리.
        LinkedHashMap<String, Object> claimResultMap = processService.addTotalClaimRegister(regSetDto);
        // 상태 변경 C1 -> C2
        for(long claimBundleNo : (List<Long>)claimResultMap.get("claim_bundle_no")){
            if (!processService.setClaimStatus(ObjectUtils.toLong(claimResultMap.get("claim_no")), claimBundleNo, regSetDto.getRegId(), ClaimCode.CLAIM_STATUS_C2)) {
                throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
            }
        }
        /*
         * SETTLE-148 chongmoon.cho
         * 취소신청시 승인 상태까지만 처리하고 클레임배치로 결제취소하도록 로직 변경.
         */
//        if(!paymentCancel( ObjectUtils.toLong(claimResultMap.get("claim_no")), (List<Long>)claimResultMap.get("claim_bundle_no"), regSetDto.getRegId() )) {
//            throw new LogicException(ClaimResponseCode.CLAIM_CANCEL_ERR01);
//        }
//
//        processService.callingClaimReg(
//            ObjectUtils.toLong(regSetDto.getPurchaseOrderNo()),
//            ObjectUtils.toLong(claimResultMap.get("payment_no")),
//            ObjectUtils.toLong(claimResultMap.get("claim_no"))
//        );
        return claimResultMap;
        // 클레임 정보 등록 후 처리 필요.
        // 등록 된 정보 만큼 처리.
    }

    /**
     * 주문 취소
     *
     * @param regSetDto
     * @throws Exception
     */
    public LinkedHashMap<String, Object> orderPartCancel(ClaimRegSetDto regSetDto) throws Exception {
        // 클레임 데이터 생성
        LinkedHashMap<String, Object> claimDataMap = cancelRegister(regSetDto);

        // 상태 변경 C1 -> C2
        if (!processService.setClaimStatus(ObjectUtils.toLong(claimDataMap.get("claim_no")), ObjectUtils.toLong(claimDataMap.get("claim_bundle_no")), regSetDto.getRegId(), ClaimCode.CLAIM_STATUS_C2)) {
            throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
        }

        /*
         * SETTLE-148 chongmoon.cho
         * 취소신청시 승인 상태까지만 처리하고 클레임배치로 결제취소하도록 로직 변경.
         */
//        if(!paymentCancel( ObjectUtils.toLong(claimDataMap.get("claim_no")), ObjectUtils.toLong(claimDataMap.get("claim_bundle_no")), regSetDto.getRegId() )) {
//            throw new LogicException(ClaimResponseCode.CLAIM_CANCEL_ERR01);
//        }
//
//        processService.callingClaimReg(
//            ObjectUtils.toLong(claimDataMap.get("purchase_order_no")),
//            ObjectUtils.toLong(claimDataMap.get("payment_no")),
//            ObjectUtils.toLong(claimDataMap.get("claim_no"))
//        );
        return claimDataMap;
    }

    public LinkedHashMap<String, Object> cancelRegister(ClaimRegSetDto regSetDto) throws Exception {
        // 환불 예정금액 을 따로 조회 후 등록 ?
        return processService.addClaimRegister(regSetDto);
    }

    public Boolean paymentCancel(long claimNo, long claimBundleNo, String regId) throws Exception {
        // 티켓정보 취득
        // 주문에 대한 확인 필요.
        if(!processService.getTicketCancelProcess(claimNo)){
            if (!processService.setClaimStatus(this.createTicketRejectDto(claimNo, claimBundleNo, regId))) {
                throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
            }
            throw new LogicException(ClaimResponseCode.CLAIM_TICKET_ERR04);
        }
        // P1 상태로 업데이트
        processService.setClaimPaymentStatus(claimNo, ClaimCode.CLAIM_PAYMENT_STATUS_P1);
        // 취소 처리.
        if (!processService.paymentCancelProcess(claimNo)) {
            log.error("클레임 결제 취소 중 오류가 발생됨. {}", claimNo);
            throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR02);
        }
        // 상태 변경 C2-> C3
        if (!processService.setClaimStatus(claimNo, claimBundleNo, regId, ClaimCode.CLAIM_STATUS_C3)) {
            throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
        }
        try {
            // claim_info 생성.
            processService.callingClaimReg(claimNo);
            // MHC적립취소처리.
            processService.saveCancelPointProcess(claimNo);
            // 배송비 페이백 확인 및 회수처리.
            processService.shipPaybackProcess(claimNo);
            // 쿠폰 깨진거
            Boolean isCouponAndSlot = processService.isClaimCouponAndSlotProcess(claimNo);
        } catch (Exception e){
            log.error("결제 취소 후 작업 오류 :::: {}", e.getMessage());
        }

        return Boolean.TRUE;
    }

    public Boolean paymentCancel(long claimNo, List<Long> claimBundleNoList, String regId) throws Exception {
        // 상태 변경 C1 -> C2
        for(long claimBundleNo : claimBundleNoList){
            if (!processService.setClaimStatus(claimNo, claimBundleNo, regId, ClaimCode.CLAIM_STATUS_C2)) {
                throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
            }
        }
        // 티켓 취소여부
        if(!processService.getTicketCancelProcess(claimNo)){
            // 상태 변경 C2-> C3
            for(long claimBundleNo : claimBundleNoList){
                if (!processService.setClaimStatus(this.createTicketRejectDto(claimNo, claimBundleNo, regId))){
                    throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
                }
            }
            throw new LogicException(ClaimResponseCode.CLAIM_TICKET_ERR04);
        }
        // P1 상태로 업데이트
        processService.setClaimPaymentStatus(claimNo, ClaimCode.CLAIM_PAYMENT_STATUS_P1);

        // 취소 처리.
        if (!processService.paymentCancelProcess(claimNo)) {
            log.error("클레임 결제 취소 중 오류가 발생됨. {}", claimNo);
            throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR02);
        }
        // 상태 변경 C2-> C3
        for(long claimBundleNo : claimBundleNoList){
            if (!processService.setClaimStatus(claimNo, claimBundleNo, regId, ClaimCode.CLAIM_STATUS_C3)) {
                throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
            }
        }
        try {
            // claim_info 생성.
            processService.callingClaimReg(claimNo);
            // MHC적립취소처리.
            processService.saveCancelPointProcess(claimNo);
            // 배송비 페이백 확인 및 회수처리.
            processService.shipPaybackProcess(claimNo);
            // 쿠폰 깨진거
            Boolean isCouponAndSlot = processService.isClaimCouponAndSlotProcess(claimNo);
        } catch (Exception e){
            log.error("결제 취소 후 작업 오류 :::: {}", e.getMessage());
        }

        return Boolean.TRUE;
    }

    private ClaimRequestSetDto createTicketRejectDto(long claimNo, long claimBundleNo, String regId){
        ClaimRequestSetDto setDto = new ClaimRequestSetDto();
        setDto.setClaimNo(claimNo);
        setDto.setClaimBundleNo(claimBundleNo);
        setDto.setRegId("SYSTEM");
        setDto.setClaimReqType("CD");
        setDto.setReasonTypeDetail("티켓취소 실패로 인한 거절");
        return setDto;
    }
}
