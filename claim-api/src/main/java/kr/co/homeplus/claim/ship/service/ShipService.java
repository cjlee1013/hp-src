package kr.co.homeplus.claim.ship.service;

import java.util.List;
import kr.co.homeplus.claim.claim.service.ClaimMapperService;
import kr.co.homeplus.claim.enums.ResponseCode;
import kr.co.homeplus.claim.factory.ResponseFactory;
import kr.co.homeplus.claim.ship.model.DlvCdGetDto;
import kr.co.homeplus.claim.ship.model.ShipMsgCdGetDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipService {

    private final ShipMapperService shipManagementMapper;

    /**
     * 배송 메시지 코드 리스트 조회
     * @return ShipCodeGetDto
     * **/
    public ResponseObject<List<ShipMsgCdGetDto>> shipMsgSearch(){
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, shipManagementMapper.getShipMsg());
    }

    /**
     * 택배사코드 리스트 조회
     * @return ShipCodeGetDto
     * **/
    public ResponseObject<List<DlvCdGetDto>> dlvCdSearch(){
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, shipManagementMapper.getDlvCd());
    }

}
