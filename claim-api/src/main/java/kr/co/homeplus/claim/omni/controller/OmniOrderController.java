package kr.co.homeplus.claim.omni.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.claim.omni.model.OmniClaimSetDto;
import kr.co.homeplus.claim.omni.model.OmniExpCancelDto;
import kr.co.homeplus.claim.omni.model.OmniOrderSetDto;
import kr.co.homeplus.claim.omni.service.OmniClaimService;
import kr.co.homeplus.claim.omni.service.OmniExpService;
import kr.co.homeplus.claim.omni.service.OmniOrderService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/omni")
@Api(tags = " 옴니 API ")
public class OmniOrderController {

    private final OmniOrderService orderService;

    private final OmniClaimService claimService;

    private final OmniExpService expService;

    @PostMapping("/requestOrder")
    @ApiOperation(value = "옴니 대체/결품 요청", response = String.class)
    public ResponseObject<String> setOmniOrderProcess(@RequestBody List<OmniOrderSetDto> setDto) throws Exception {
        return orderService.setOmniOrderProcess(setDto);
    }

    @PostMapping("/requestClaim")
    @ApiOperation(value = "옴니 반품/교환 요청", response = String.class)
    public ResponseObject<String> setOmniClaimProcess(@RequestBody List<OmniClaimSetDto> setDto) throws Exception {
        return claimService.setOmniClaimProcess(setDto);
    }

    @PostMapping("/requestAllCancel")
    @ApiOperation(value = "옴니(EXP)전체취소요청", response = String.class)
    public ResponseObject<String> setOmniAllCancelProcess(@RequestBody OmniExpCancelDto setDto) throws Exception {
        return expService.setOmniAllCancelProcess(setDto);
    }

    @PostMapping("/requestClaimApprove")
    @ApiOperation(value = "옴니(EXP)클레임승인", response = String.class)
    public ResponseObject<String> setOmniClaimStatusModify(@RequestBody long ordNo) throws Exception {
        return expService.setOmniExpClaimStatusModify(ordNo);
    }
}
