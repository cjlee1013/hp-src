package kr.co.homeplus.claim.order.model.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "회원등급 조회 응답 DTO")
public class UserGradeGetDto {

    @ApiModelProperty(value = "현재등급", position = 1)
    private GradeInfoDto current;

    @ApiModelProperty(value = "다음달예상등급", position = 2)
    private GradeInfoDto next;

    @ApiModelProperty(value = "다음등급정보", position = 3)
    private UserGradeDto expect;
}
