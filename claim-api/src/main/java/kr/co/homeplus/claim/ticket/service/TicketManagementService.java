package kr.co.homeplus.claim.ticket.service;

import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.enums.ResponseCode;
import kr.co.homeplus.claim.factory.ResponseFactory;
import kr.co.homeplus.claim.ticket.mapper.TicketPrimaryMapper;
import kr.co.homeplus.claim.ticket.mapper.TicketSecondaryMapper;
import kr.co.homeplus.claim.ticket.model.OrderTicketGetDto;
import kr.co.homeplus.claim.ticket.model.CoopTicketUpdateSetDto;
import kr.co.homeplus.claim.ticket.model.OrderTicketHistorySetDto;
import kr.co.homeplus.claim.utils.SqlUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class TicketManagementService {

    private final TicketSecondaryMapper ticketSecondaryMapper;

    private final TicketPrimaryMapper ticketPrimaryMapper;


    /**
     * 쿠프 티켓상태변경
     *
     * @param coopTicketUpdateSetDto 쿠프 티켓상태변경 dto
     * @return 티켓상태 변경 결과
    */
    public ResponseObject<String> updateCoopTicketStatus(CoopTicketUpdateSetDto coopTicketUpdateSetDto) throws LogicException {
        log.info("[updateCoopTicketStatus] 쿠프 티켓상태변경 요청 : {}", coopTicketUpdateSetDto);

        /* 1.티켓 정보 조회 */
        OrderTicketGetDto coopTicketInfoDto = ticketSecondaryMapper.selectTicketInfo(coopTicketUpdateSetDto.getTicketCd());

        /* 2.티켓 조회 여부 확인 */
        if(coopTicketInfoDto == null){
            throw new LogicException(ResponseCode.TICKET_STATUS_CHG_ERR01);
        }

        /* 3.티켓 상태 유효성 검사 */
        if(ticketSecondaryMapper.selectTicketStatus(coopTicketUpdateSetDto.getTicketStatus()) == 0){
            log.error("[updateCoopTicketStatus] 티켓변경요청 값 오류 티켓번호 : {}, 변경요청상태값 : {}", coopTicketUpdateSetDto.getTicketCd(), coopTicketUpdateSetDto.getTicketStatus());
            throw new LogicException(ResponseCode.TICKET_STATUS_CHG_ERR03);
        }

        /* 3-1.티켓 상태 변경 유무 확인 */
        if(coopTicketUpdateSetDto.getTicketStatus().equalsIgnoreCase(coopTicketInfoDto.getTicketStatus())){
            throw new LogicException(ResponseCode.TICKET_STATUS_CHG_ERR04);
        }

        /* 4.티켓 상태 변경 */
        Boolean result = SqlUtils.isGreaterThanZero(ticketPrimaryMapper.updateOrderTicketStatus(coopTicketUpdateSetDto));

        /* 5.티켓 상태 변경결과 */
        if(!result){
            throw new LogicException(ResponseCode.TICKET_STATUS_CHG_ERR02);
        }

        /* 6.티켓 히스토리 등록 */
        try{
            OrderTicketHistorySetDto orderTicketHistorySetDto = OrderTicketHistorySetDto.builder().orderTicketNo(coopTicketInfoDto.getOrderTicketNo())
                .historyDetail("[티켓상태변경] 쿠프요청티켓상태 변경 [" + coopTicketUpdateSetDto.getTicketStatus() + "]")
                .regId("system")
                .build();

            SqlUtils.isGreaterThanZero(ticketPrimaryMapper.insertTicketHistory(orderTicketHistorySetDto));
        }catch (Exception e){
            log.error("[updateCoopTicketStatus] 티켓 히스토리 등록 실패 : {}", coopTicketInfoDto.getOrderTicketNo());
        }

        /* response */
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS);
    }


}
