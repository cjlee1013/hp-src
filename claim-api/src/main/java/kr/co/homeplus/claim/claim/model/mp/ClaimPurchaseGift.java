package kr.co.homeplus.claim.claim.model.mp;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimPurchaseGift {

    @ApiModelProperty(value = "구매사은품번호", position = 1)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "사은품번호", position = 2)
    private String giftNo;

    @ApiModelProperty(value="사은품메시지(영수증 노출 문구)", position = 3)
    private String giftMsg;

}
