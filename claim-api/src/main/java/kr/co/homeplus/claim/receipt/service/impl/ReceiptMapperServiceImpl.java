package kr.co.homeplus.claim.receipt.service.impl;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.receipt.mapper.ReceiptReadMapper;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseMethodReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.card.ClaimCardReceiptGetDto;
import kr.co.homeplus.claim.receipt.model.cash.ClaimCashReceiptGetDto;
import kr.co.homeplus.claim.receipt.model.order.ClaimRefundReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.purchase.ClaimPaymentReceiptInfo;
import kr.co.homeplus.claim.receipt.model.purchase.ClaimReceiptItemInfo;
import kr.co.homeplus.claim.receipt.model.purchase.ClaimReceiptMainInfo;
import kr.co.homeplus.claim.receipt.service.ReceiptMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ReceiptMapperServiceImpl implements ReceiptMapperService {

    private final ReceiptReadMapper readMapper;

    @Override
    public ClaimReceiptMainInfo getClaimPurchaseReceiptMainInfo(ClaimPurchaseReceiptSetDto setDto) {
        return readMapper.selectClaimPurchaseReceiptMainInfo(setDto);
    }

    @Override
    public List<ClaimReceiptItemInfo> getClaimPurchaseReceiptItemInfo(ClaimPurchaseReceiptSetDto setDto) {
        return readMapper.selectClaimPurchaseReceiptItemInfo(setDto);
    }

    @Override
    public List<ClaimPaymentReceiptInfo> getClaimPurchaseReceiptPaymentInfo(ClaimPurchaseReceiptSetDto setDto) {
        return readMapper.selectClaimPurchaseReceiptPaymentInfo(setDto);
    }

    @Override
    public ClaimCardReceiptGetDto getClaimCardPurchaseReceiptInfo(ClaimPurchaseMethodReceiptSetDto setDto) {
        return readMapper.selectClaimCardPurchaseReceiptInfo(setDto);
    }

    @Override
    public ClaimCashReceiptGetDto getClaimCashPurchaseReceiptInfo(ClaimPurchaseMethodReceiptSetDto setDto) {
        return readMapper.selectClaimCashPurchaseReceiptInfo(setDto);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getOrderRefundReceiptInfo(ClaimRefundReceiptSetDto setDto) {
        return readMapper.selectOrderRefundReceiptInfo(setDto);
    }
}
