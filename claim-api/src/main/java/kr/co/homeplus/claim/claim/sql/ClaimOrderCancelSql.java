package kr.co.homeplus.claim.claim.sql;

import java.util.Arrays;
import java.util.HashMap;

import kr.co.homeplus.claim.constants.Constants;
import kr.co.homeplus.claim.constants.CustomConstants;
import kr.co.homeplus.claim.constants.SqlPatternConstants;
import kr.co.homeplus.claim.entity.claim.ClaimAdditionEntity;
import kr.co.homeplus.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claim.entity.claim.ClaimExchShippingEntity;
import kr.co.homeplus.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claim.entity.claim.ClaimMstEntity;
import kr.co.homeplus.claim.entity.claim.ClaimMultiBundleEntity;
import kr.co.homeplus.claim.entity.claim.ClaimMultiItemEntity;
import kr.co.homeplus.claim.entity.claim.ClaimOptEntity;
import kr.co.homeplus.claim.entity.claim.ClaimPaymentEntity;
import kr.co.homeplus.claim.entity.claim.ClaimPickShippingEntity;
import kr.co.homeplus.claim.entity.dms.ItmPartnerSellerDeliveryEntity;
import kr.co.homeplus.claim.entity.dms.ItmPartnerSellerEntity;
import kr.co.homeplus.claim.entity.market.MarketOrderEntity;
import kr.co.homeplus.claim.entity.market.MarketOrderItemEntity;
import kr.co.homeplus.claim.entity.multi.MultiBundleEntity;
import kr.co.homeplus.claim.entity.multi.MultiOrderItemEntity;
import kr.co.homeplus.claim.entity.order.BundleEntity;
import kr.co.homeplus.claim.entity.order.OrderDiscountEntity;
import kr.co.homeplus.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claim.entity.order.OrderOptEntity;
import kr.co.homeplus.claim.entity.order.OrderPaymentDivideEntity;
import kr.co.homeplus.claim.entity.order.OrderPromoEntity;
import kr.co.homeplus.claim.entity.order.PaymentEntity;
import kr.co.homeplus.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claim.entity.order.PurchaseStoreInfoEntity;
import kr.co.homeplus.claim.entity.order.ShippingMileagePaybackEntity;
import kr.co.homeplus.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claim.entity.ship.StoreShiftMngEntity;
import kr.co.homeplus.claim.entity.ship.StoreSlotMngEntity;
import kr.co.homeplus.claim.enums.MysqlFunction;
import kr.co.homeplus.claim.factory.EntityFactory;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ClaimOrderCancelSql {

    /**
     * 주문취소 관련 주문내역 조회
     * 주문취소시 상품번호 및 옵션, 추가상품 여부를 선 확인.
     *
     * @param purchaseOrderNo 주문번호
     * @return String 주문취소 상품번호 체크 Query
     * @throws Exception 오류처리.
     */
    public static String selectPrevClaimOrderCancelCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) throws Exception {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        OrderPromoEntity orderPromo = EntityFactory.createEntityIntoValue(OrderPromoEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);

        SQL queryOfOrder = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                orderItem.orderItemNo,
                orderItem.itemNo,
                orderItem.itemNm1,
                SqlUtils.appendSingleQuoteWithAlias("0", "order_opt_no"),
                SqlUtils.appendSingleQuoteWithAlias("0", "order_qty"),
                SqlUtils.appendSingleQuoteWithAlias("0", "order_price"),
                orderItem.purchaseMinQty,
                SqlUtils.appendSingleQuoteWithAlias("1", "order_type"),
                purchaseOrder.orderDt,
                SqlUtils.subQuery(
                    SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, ObjectUtils.toArray(claimItem.claimItemQty)),
                    EntityFactory.getTableNameWithAlias(claimItem),
                    ObjectUtils.toArray(SqlUtils.equalColumn(orderItem.orderItemNo, claimItem.orderItemNo)),
                    "claim_qty"
                ),
                orderItem.promoNo,
                SqlUtils.caseWhenElse(
                    orderPromo.promoKind,
                    ObjectUtils.toArray(
                        "'BASIC'", "'행사상품'",
                        "'PICK'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.MINUS, orderPromo.mixMatchStdVal, "1"), " '+' ", "'1'")),
                        "'TOGETHER'", "'함께할인'",
                        "'INTERVAL'", "'함께할인'"
                    ),
                    SqlUtils.sqlFunction(
                        MysqlFunction.CASE,
                        ObjectUtils.toArray(
                            SqlUtils.AND(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, orderItem.promoNo),
                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.simpOrgItemPrice, "0")
                            ),
                            "'행사상품'",
                            "''"
                        )
                    ),
                    "promo_nm1"
                ),
                bundle.diffQty
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderItem),
                    ObjectUtils.toList( SqlUtils.onClause(new Object[]{ purchaseOrder.purchaseOrderNo }, orderItem ))
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    orderPromo,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderPromo.promoType, "'SIMP'"))
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(bundle),
                    ObjectUtils.toList( SqlUtils.onClause(ObjectUtils.toArray(orderPromo.bundleNo), bundle) )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );

        SQL queryOfOpt = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                orderItem.orderItemNo,
                orderItem.itemNo,
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "item_nm1",
                    SqlUtils.equalColumnByInput(
                        SqlPatternConstants.getConditionByPattern("IFNULL", orderOpt.selOpt1Title, "''"), ""
                    ),
                    orderOpt.txtOpt1Title,
                    orderOpt.selOpt1Title
                ),
                orderOpt.orderOptNo,
                orderOpt.optQty,
                SqlUtils.customOperator(CustomConstants.MULTIPLY, ObjectUtils.toArray(orderItem.itemPrice, orderOpt.optQty), "order_price"),
                SqlUtils.appendSingleQuoteWithAlias("0", "purchase_min_qty"),
                SqlUtils.appendSingleQuoteWithAlias("2", "order_type"),
                SqlUtils.appendSingleQuoteWithAlias("-", "order_dt"),
                SqlUtils.subQuery(
                    SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, ObjectUtils.toArray(claimOpt.claimOptQty)),
                    EntityFactory.getTableNameWithAlias(claimOpt),
                    ObjectUtils.toArray(SqlUtils.equalColumn(orderOpt.orderOptNo, claimOpt.orderOptNo)),
                    "claim_qty"
                ),
                SqlUtils.appendSingleQuoteWithAlias("'-'", "promo_no"),
                SqlUtils.appendSingleQuoteWithAlias("'-'", "promo_nm1"),
                SqlUtils.appendSingleQuoteWithAlias("'-'", "diff_qty")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ purchaseOrder.purchaseOrderNo}, orderItem )),
                    null
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderOpt),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ orderItem.orderItemNo }, orderOpt )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                orderOpt.orderOptNo.concat(" IS NOT NULL")
            )
            .ORDER_BY(
                "item_no", "order_type"
            );

        StringBuilder query = new StringBuilder();

        if(bundleNo != 0){
            queryOfOrder.WHERE(
                SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo)
            );
            queryOfOpt.WHERE(
                SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo)
            );
        }

        query.append(queryOfOrder.toString());
        query.append("\nUNION ALL\n");
        query.append(queryOfOpt.toString());

        log.debug("selectPrevClaimOrderCancelInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderItemInfoByClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                orderItem.itemNo,
                orderOpt.orderOptNo,
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderOpt.optQty),
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.MAX, claimOpt.claimOptQty))
                    ),
                    "claim_qty"
                ),
                orderItem.orderItemNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo, orderItem.purchaseOrderNo, orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimItemNo, orderOpt.orderOptNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo)
            )
            .GROUP_BY(orderItem.itemNo, orderItem.orderItemNo, orderOpt.orderOptNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(orderItem.itemNo),
                SqlUtils.nonAlias(orderOpt.orderOptNo),
                "claim_qty",
                SqlUtils.nonAlias(orderItem.orderItemNo)
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "data"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, "claim_qty", "0")
            );
        log.debug("selectOrderItemInfoByClaim Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderBundleByClaim(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                purchaseOrder.purchaseOrderNo,
                bundle.bundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(bundle),
                    ObjectUtils.toList(SqlUtils.onClause(ObjectUtils.toArray(purchaseOrder.purchaseOrderNo), bundle))
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectOrderBundleByClaim Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
     * 환불예정금액 조회 - call procedure
     *
     * @param parameterMap 조회 파라미터
     * @return 환불예정금액
     */
    public static String callByClaimPreRefundPriceCalculation(HashMap<String, Object> parameterMap){
        log.info("환불예정금 조회(단건) :: \n[{}]", SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION, parameterMap.values().toArray()));
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION, parameterMap.values().toArray());
    }

    /**
     * 환불예정금액 다건 조회 - call procedure
     *
     * @param parameterMap 조회 파라미터
     * @return 환불예정금액
     */
    public static String callByClaimPreRefundPriceMultiCalculation(HashMap<String, Object> parameterMap){
        log.info("환불예정금 조회(다건) :: \n[{}]", SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_MULTI, parameterMap.values().toArray()));
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_MULTI, parameterMap.values().toArray());
    }

    /**
     * 클레임 신청시 부분취소 여부 확인을 위한 쿼리
     *
     * @param purchaseOrderNo 주문거래번호
     * @param bundleNo 배송번호
     * @return 부분취소여부 확인 쿼리
     * @throws Exception
     */
    public static String selectOrderByClaimPartInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo, @Param("userNo") long userNo) throws Exception {
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);

        SQL orderItemQtyQuery = new SQL()
        .SELECT(
            SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, ObjectUtils.toArray(orderItem.itemQty), "orderItemQty")
        )
        .FROM(EntityFactory.getTableNameWithAlias(orderItem))
        .INNER_JOIN(
            SqlUtils.createJoinCondition(
                EntityFactory.getTableNameWithAlias(purchaseOrder),
                ObjectUtils.toList(SqlUtils.onClause(ObjectUtils.toArray(orderItem.purchaseOrderNo), purchaseOrder))
            )
        )
        .WHERE(
            SqlUtils.equalColumnByInput(orderItem.purchaseOrderNo, purchaseOrderNo),
            SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo)
        );

        if(bundleNo != 0){
            orderItemQtyQuery.WHERE(SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo)).GROUP_BY(orderItem.bundleNo);
        }

        SQL claimItemQtyQuery = new SQL()
        .SELECT(
            SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, ObjectUtils.toArray(claimItem.claimItemQty))
        )
        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
        .INNER_JOIN(
            SqlUtils.createJoinCondition(
                claimItem,
                ObjectUtils.toArray(claimBundle.claimBundleNo, claimBundle.purchaseOrderNo, claimBundle.bundleNo),
                ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")))
            )
        )
        .WHERE(
            SqlUtils.equalColumnByInput(claimItem.purchaseOrderNo, purchaseOrderNo),
            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
            SqlUtils.equalColumnByInput(claimBundle.userNo, userNo)
        );

        if(bundleNo != 0){
            claimItemQtyQuery.WHERE(SqlUtils.equalColumnByInput(claimBundle.bundleNo, bundleNo)).GROUP_BY(claimBundle.bundleNo);
        }

        SQL query = new SQL()
        .SELECT(
            SqlUtils.subTableQuery(
                orderItemQtyQuery,
                "orderItemQty"
            ),
            SqlUtils.subTableQuery(
                claimItemQtyQuery,
                "claimItemQty"
            )
        );

        log.debug("getOrderByClaimPartInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimCouponReIssue(@Param("claimNo") long claimNo, @Param("userNo") long userNo) {
        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        OrderDiscountEntity orderDiscount = EntityFactory.createEntityIntoValue(OrderDiscountEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                claimAddition.claimAdditionNo,
                orderDiscount.couponNo,
                orderDiscount.issueNo,
                SqlUtils.aliasToRow(claimAddition.additionSumAmt, "discount_amt"),
                orderDiscount.storeId
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimAddition))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderDiscount, ObjectUtils.toArray(claimAddition.orderDiscountNo, claimAddition.purchaseOrderNo, claimAddition.orderItemNo))
            )
            .WHERE(
                SqlUtils.middleLike(claimAddition.additionTypeDetail, "coupon"),
                SqlUtils.equalColumnByInput(claimAddition.isIssue, "N"),
                SqlUtils.equalColumnByInput(claimAddition.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimAddition.userNo, userNo),
                SqlUtils.equalColumnByInput(claimAddition.totalCancelYn, "Y")
            )
            ;
        log.debug("selectClaimCouponReIssue \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimInfo(@Param("claimNo") long claimNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        PurchaseStoreInfoEntity purchaseStoreInfo = EntityFactory.createEntityIntoValue(PurchaseStoreInfoEntity.class, Boolean.TRUE);
//        StoreSlotMngEntity storeSlotMng = EntityFactory.createEntityIntoValue(StoreSlotMngEntity.class, Boolean.TRUE);
        StoreShiftMngEntity storeShiftMng = EntityFactory.createEntityIntoValue(StoreShiftMngEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimMst.claimNo,
                claimMst.userNo,
                claimMst.claimPartYn,
                claimBundle.claimType,
                claimMst.purchaseOrderNo,
                claimBundle.bundleNo,
                claimBundle.claimBundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.orderCategory, claimBundle.orderCategory),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, orderItem.itemQty, orderItem.itemQty),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimItem.claimItemQty, claimItem.claimItemQty),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_N_Y,
                    SqlUtils.customOperator(
                        Constants.EQUAL,
                        SqlUtils.customOperator(
                            CustomConstants.ANGLE_BRACKET_RIGHT,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(
                                    MysqlFunction.DATE_FULL_24,
                                    SqlUtils.customOperator(
                                        CustomConstants.PLUS,
                                        ObjectUtils.toArray(
                                            SqlUtils.sqlFunction(MysqlFunction.MAX, storeShiftMng.shipDt),
                                            SqlUtils.sqlFunction(
                                                MysqlFunction.CONCAT,
                                                ObjectUtils.toArray(
                                                    SqlUtils.sqlFunction(
                                                        MysqlFunction.CASE,
                                                        ObjectUtils.toArray(
                                                            SqlUtils.sqlFunction(MysqlFunction.IS_NULL, SqlUtils.sqlFunction(MysqlFunction.MAX, storeShiftMng.shiftBeforeCloseTime)),
                                                            SqlUtils.sqlFunction(MysqlFunction.MAX, storeShiftMng.shiftCloseTime),
                                                            SqlUtils.sqlFunction(MysqlFunction.MAX, storeShiftMng.shiftBeforeCloseTime)
                                                        )
                                                    ),
                                                    "'00'"
                                                )
                                            )
                                        )
                                    )
                                ),
                                "NOW()"
                            )
                        ),
                        "1"
                    ),
                    "is_close"
                ),
                purchaseOrder.orderType
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(claimItem.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimMst.purchaseOrderNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(purchaseStoreInfo, ObjectUtils.toArray(bundle.purchaseStoreInfoNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(storeShiftMng, ObjectUtils.toArray( purchaseStoreInfo.storeId, orderItem.storeType, purchaseStoreInfo.shiftId, purchaseStoreInfo.shipDt)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            )
            .GROUP_BY(claimMst.claimNo, claimMst.userNo, claimMst.claimPartYn, claimBundle.claimType, claimMst.purchaseOrderNo, claimBundle.bundleNo, claimBundle.claimBundleNo);

        log.debug("selectClaimInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimPickIsAutoInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        ItmPartnerSellerEntity itmPartnerSeller = EntityFactory.createEntityIntoValue(ItmPartnerSellerEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ItmPartnerSellerDeliveryEntity itmPartnerSellerDelivery = EntityFactory.createEntityIntoValue(ItmPartnerSellerDeliveryEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(shippingItem.purchaseOrderNo, shippingItem.bundleNo, "'M'"), "ship_type"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(itmPartnerSeller.returnDeliveryYn, "'N'")), "Y"),
                        SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.sqlFunction(MysqlFunction.IS_NULL, itmPartnerSellerDelivery.dlvCd)),
                        "'N'"
                    ),
                    itmPartnerSeller.returnDeliveryYn
                ),
                shippingItem.shipMethod,
                shippingItem.dlvCd
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingItem))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmPartnerSeller),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(shippingItem.partnerId), itmPartnerSeller)
                    )
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmPartnerSellerDelivery),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(shippingItem.partnerId, shippingItem.dlvCd), itmPartnerSellerDelivery)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo),
                SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("'D3'", "'D4'", "'D5'"))
            )
            .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(shippingItem.shipNo)))
            .LIMIT(1)
            .OFFSET(0)
            ;
        log.debug("selectClaimPickIsAutoInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimSendDataInfo(@Param("claimNo") long claimNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CONCAT,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(claimItem.itemName)
                                .FROM(EntityFactory.getTableNameWithAlias(claimItem))
                                .WHERE(SqlUtils.equalColumn(claimItem.claimNo, claimBundle.claimNo))
                                .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimItem.claimItemNo)))
                                .LIMIT(1).OFFSET(0)
                        ),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.sqlFunction(MysqlFunction.COUNT, claimItem.claimItemNo), "1"),
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'외 '", SqlUtils.customOperator(CustomConstants.MINUS, SqlUtils.sqlFunction(MysqlFunction.COUNT, claimItem.claimItemNo), "1"), "' 건'")),
                                "''"
                            )
                        )
                    ),
                    "item_name"
                ),
                SqlUtils.subTableQuery(
                    new SQL()
                        .SELECT(
                            SqlUtils.customOperator(
                                CustomConstants.MINUS,
                                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO,claimPayment.paymentAmt),
                                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO,claimPayment.substitutionAmt)
                            )
                        ).FROM(EntityFactory.getTableNameWithAlias(claimPayment))
                        .WHERE(SqlUtils.equalColumn(claimPayment.claimNo, claimBundle.claimNo)),
                    claimPayment.paymentAmt
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimType, claimBundle.claimType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.shipMobileNo, claimBundle.shipMobileNo)
//                SqlUtils.aliasToRow("'010-6556-7978'", claimBundle.shipMobileNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
            )
            .GROUP_BY(claimBundle.claimNo);
        log.debug("selectClaimSendDataInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimSafetyIssueData(@Param("claimBundleNo") long claimBundleNo, @Param("issueType") String issueType) {
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class);
        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class);
        SQL query = new SQL();
        //수거일 경우
        if(issueType.equals("R")){
            query
                .SELECT(
                    SqlUtils.aliasToRow(claimPickShipping.pickMobileNo, "req_phone_no"),
                    SqlUtils.appendSingleQuoteWithAlias(issueType, "claim_type"),
                    claimPickShipping.claimBundleNo
                )
                .FROM(EntityFactory.getTableName(claimPickShipping))
                .WHERE(SqlUtils.equalColumnByInput(claimPickShipping.claimBundleNo, claimBundleNo));
        } else {
            query
                .SELECT(
                    SqlUtils.aliasToRow(claimExchShipping.exchMobileNo, "req_phone_no"),
                    SqlUtils.appendSingleQuoteWithAlias(issueType, "claim_type"),
                    claimExchShipping.claimBundleNo
                )
                .FROM(EntityFactory.getTableName(claimExchShipping))
                .WHERE(SqlUtils.equalColumnByInput(claimExchShipping.claimBundleNo, claimBundleNo));
        }
        log.debug("selectClaimSafetyIssueData \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimRegDataInfo(@Param("claimNo") long claimNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimBundle.paymentNo,
                claimBundle.purchaseOrderNo,
                claimBundle.claimNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .WHERE(SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo))
            .GROUP_BY(claimBundle.paymentNo, claimBundle.purchaseOrderNo, claimBundle.claimNo);
        log.debug("selectClaimRegDataInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimOrderTypeCheck(@Param("claimNo") long claimNo){
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                purchaseOrder.orderType
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo)
            );
        log.debug("selectClaimOrderTypeCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOriginAndCombineClaimCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("searchType") String searchType) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.cmbnYn,
                orderItem.orderItemNo,
                orderItem.bundleNo,
                orderItem.itemNo,
                SqlUtils.aliasToRow(orderItem.itemNm1, "item_nm"),
                orderItem.orgItemNo,
                orderItem.itemQty,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.AND(
                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                        ),
                        claimItem.claimItemQty,
                        "0"
                    ),
                    claimItem.claimItemQty
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C3'", "'C4'", "'C9'")),
                        claimItem.claimItemQty,
                        "0"
                    ),
                    "process_item_qty"
                ),
                orderItem.itemPrice,
                shippingItem.shipNo,
                orderItem.saleUnit
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.orderItemNo, orderItem.bundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, orderItem.orderItemNo)))
            .ORDER_BY(SqlUtils.orderByesc(orderItem.orderItemNo));

        switch (searchType) {
            case "COMBINE" :
                innerQuery
                    .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, bundle.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")))))
                    .WHERE(
                        SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo),
                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE")
                    );
                break;
            case "MULTI" :
                innerQuery
                    .WHERE(
                        SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
                    );
                break;
            default:
                innerQuery
                    .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, bundle.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")))))
                    .WHERE(
                        SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
                    );
                break;
        }

        SQL query = new SQL()
            .SELECT(
                "purchase_order_no",
                "order_item_no",
                SqlUtils.sqlFunction(MysqlFunction.MAX, "cmbn_yn", "cmbn_yn"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "bundle_no", "bundle_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_no", "item_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_nm", "item_nm"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "org_item_no", "org_item_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_qty", "order_item_qty"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "claim_item_qty", "claim_item_qty"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "process_item_qty", "process_item_qty"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_price", "item_price"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "ship_no", "ship_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "sale_unit", "sale_unit")
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "itemList"))
            .GROUP_BY("itemList.purchase_order_no", "itemList.order_item_no");

        log.debug("selectOriginAndCombineClaimCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimBundleTdInfo(@Param("claimNo") long claimNo){
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimBundle.bundleNo,
                claimBundle.purchaseOrderNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimBundle.orderCategory, "TD")
            );
        log.debug("selectClaimBundleTdInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectMileagePaybackCheck(@Param("bundleNo") long bundleNo) {
        ShippingMileagePaybackEntity shippingMileagePayback = EntityFactory.createEntityIntoValue(ShippingMileagePaybackEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.orgPurchaseOrderNo,
                shippingMileagePayback.orgBundleNo,
                shippingMileagePayback.bundleNo,
                shippingMileagePayback.paybackNo,
                shippingMileagePayback.storeType,
                shippingMileagePayback.userNo,
                shippingMileagePayback.cancelYn,
                shippingMileagePayback.resultYn,
                SqlUtils.aliasToRow(shippingMileagePayback.shipPricePayback, "return_amt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingMileagePayback))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(shippingMileagePayback.orgPurchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(payment.paymentStatus, "P3"))))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingMileagePayback.cancelYn, "N"),
                SqlUtils.OR(
                    Boolean.TRUE,
                    SqlUtils.equalColumnByInput(shippingMileagePayback.bundleNo, bundleNo),
                    SqlUtils.equalColumnByInput(shippingMileagePayback.orgBundleNo, bundleNo)
                )
            );
        log.debug("selectMileagePaybackCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileagePaybackInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.SUM, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(orderPaymentDivide.pgDivideAmt, orderPaymentDivide.mileageDivideAmt, orderPaymentDivide.mhcDivideAmt, orderPaymentDivide.ocbDivideAmt, orderPaymentDivide.dgvDivideAmt)), "payment_amt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.freeCondition, bundle.freeCondition)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderPaymentDivide, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderPaymentDivide.bundleNo, orderPaymentDivide.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(payment.paymentStatus, "P3"))))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo)))
            .WHERE(
                SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")),
                SqlUtils.OR(
                    Boolean.TRUE,
                    SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                    SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo)
                )
            );
        log.debug("selectMileagePaybackInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileagePaybackCancelInfo(@Param("bundleNo") long bundleNo, @Param("orgBundleNo") long orgBundleNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.SUM, SqlUtils.customOperator(CustomConstants.MINUS, claimPayment.paymentAmt, claimPayment.substitutionAmt)),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totShipPrice),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totIslandShipPrice)
                    ),
                    "cancel_amt"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimBundle.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimPayment.claimNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                SqlUtils.OR(
                    Boolean.TRUE,
                    SqlUtils.equalColumnByInput(claimBundle.bundleNo, bundleNo),
                    SqlUtils.equalColumnByInput(claimBundle.bundleNo, orgBundleNo)
                )
            );
        log.debug("selectMileagePaybackCancelInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateMileagePaybackResult(@Param("paybackNo") long paybackNo) {
        ShippingMileagePaybackEntity shippingMileagePayback = EntityFactory.createEntityIntoValue(ShippingMileagePaybackEntity.class);
        SQL query  = new SQL()
            .UPDATE(EntityFactory.getTableName(shippingMileagePayback))
            .SET(
                SqlUtils.equalColumnByInput(shippingMileagePayback.cancelYn, "Y"),
                SqlUtils.equalColumnByInput(shippingMileagePayback.chgId, "SYSTEM"),
                SqlUtils.equalColumn(shippingMileagePayback.chgDt, "NOW()")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingMileagePayback.paybackNo, paybackNo)
            );
        log.debug("updateMileagePaybackResult \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectAddCouponCheck(@Param("purchaseOrderNo") long purchaseOrderNo) {
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class);
        SQL query = new SQL()
            .SELECT(
                orderPaymentDivide.purchaseOrderNo,
                orderPaymentDivide.orderItemNo,
                orderPaymentDivide.itemNo,
                orderPaymentDivide.itemQty,
                SqlUtils.sqlFunction(MysqlFunction.CASE_Y_N, SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, ObjectUtils.toArray(orderPaymentDivide.addTdCouponAmt, 0)), "isUseTdCoupon"),
                SqlUtils.sqlFunction(MysqlFunction.CASE_Y_N, SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, ObjectUtils.toArray(orderPaymentDivide.addDsCouponAmt, 0)), "isUseDsCoupon")
            )
            .FROM(EntityFactory.getTableName(orderPaymentDivide))
            .WHERE(
                SqlUtils.equalColumnByInput(orderPaymentDivide.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderPaymentDivide.orderItemNo, "0")
            ).AND()
            .WHERE(
                SqlUtils.OR(
                    SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, orderPaymentDivide.addTdCouponAmt, "0"),
                    SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, orderPaymentDivide.addDsCouponAmt, "0")
                )
            );
        log.debug("selectAddCouponCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMultiBundleInfoForClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        MultiBundleEntity multiBundle = EntityFactory.createEntityIntoValue(MultiBundleEntity.class, Boolean.TRUE);
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                multiBundle.multiBundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(multiBundle))
            .INNER_JOIN(SqlUtils.createJoinCondition(multiOrderItem, ObjectUtils.toArray(multiBundle.multiBundleNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(multiBundle.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(multiBundle.bundleNo, bundleNo)
            )
            .GROUP_BY(multiBundle.multiBundleNo)
            .ORDER_BY(multiBundle.multiBundleNo)
            ;
        log.debug("selectMultiBundleInfoForClaim \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimMultiItemCreateInfo(@Param("claimNo") long claimNo, @Param("multiBundleNo") long multiBundleNo) {
        MultiBundleEntity multiBundle = EntityFactory.createEntityIntoValue(MultiBundleEntity.class, Boolean.TRUE);
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimItem.claimItemNo,
                claimItem.claimNo,
                claimItem.claimBundleNo,
                multiBundle.multiBundleNo,
                claimItem.orderItemNo,
                multiOrderItem.multiOrderItemNo,
                multiOrderItem.itemPrice,
                SqlUtils.aliasToRow(multiOrderItem.itemQty, "claim_item_qty")
            )
            .FROM(EntityFactory.getTableNameWithAlias(multiBundle))
            .INNER_JOIN(SqlUtils.createJoinCondition(multiOrderItem, ObjectUtils.toArray(multiBundle.multiBundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(multiOrderItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimItem.claimNo, claimNo),
                SqlUtils.equalColumnByInput(multiBundle.multiBundleNo, multiBundleNo)
            );
        log.debug("selectClaimMultiItemCreateInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectIsMarketOrderCheckForClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("claimNo") long claimNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.CASE_Y_N, SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_MARKET"), "isMarketOrder")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))));

        if(purchaseOrderNo != 0) {
            query.WHERE(SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo));
        }

        if(claimNo != 0) {
            query
                .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
                .WHERE(SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo));
        }

        log.debug("selectIsMarketOrderCheckForClaim \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimBasicDataForMarketOrder(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderOptNo") long orderOptNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        MarketOrderEntity marketOrder = EntityFactory.createEntityIntoValue(MarketOrderEntity.class, Boolean.TRUE);
        MarketOrderItemEntity marketOrderItem = EntityFactory.createEntityIntoValue(MarketOrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.marketType,
                marketOrder.marketOrderNo,
                orderOpt.marketOrderItemNo,
                orderOpt.optQty
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(marketOrder, ObjectUtils.toArray(purchaseOrder.marketOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(marketOrderItem, ObjectUtils.toArray(marketOrderItem.marketOrderNo, marketOrder.marketOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo, marketOrderItem.marketOrderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderOpt.orderOptNo, orderOptNo)
            );
        log.debug("selectClaimBasicDataForMarketOrder \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimStatusChangeDataForMarketOrder(@Param("claimNo") long claimNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimBundle.purchaseOrderNo,
                claimBundle.claimType,
                claimBundle.claimStatus,
                claimOpt.orderOptNo,
                claimMst.marketType
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimBundle.claimBundleNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo)
            );
        log.debug("selectClaimStatusChangeDataForMarketOrder \n[{}]", query.toString());
        return query.toString();
    }
}
