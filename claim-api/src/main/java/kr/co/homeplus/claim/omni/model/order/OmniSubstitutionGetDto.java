package kr.co.homeplus.claim.omni.model.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "대체주문 응답 DTO")
public class OmniSubstitutionGetDto {
    @ApiModelProperty(value = "고객번호", position = 1)
    private long userNo;
    @ApiModelProperty(value = "대체주문번호", position = 2)
    private long substitutionPurchaseOrderNo;
    @ApiModelProperty(value = "대체배송번호", position = 3)
    private long substitutionShipAddrNo;
    @ApiModelProperty(value = "원번들번호", position = 4)
    private long orgBundleNo;
    @ApiModelProperty(value = "PG금액", position = 5)
    private long pgAmt;
    @ApiModelProperty(value = "마일리지금액", position = 6)
    private long mileageAmt;
    @ApiModelProperty(value = "MHC금액", position = 7)
    private long mhcAmt;
    @ApiModelProperty(value = "DGV금액", position = 8)
    private long dgvAmt;
    @ApiModelProperty(value = "OCB금액", position = 9)
    private long ocbAmt;
}
