package kr.co.homeplus.claim.entity.order;

public class OrderPromoItemGetEntity {

    // 주문프로모션아이템순번
    public String orderPromoItemGetSeq;

    // 구매주문번호
    public String purchaseOrderNo;

    // 주문프로모션번호
    public String orderPromoNo;

    // 프로모션번호
    public String promoNo;

    // 프로모션상세번호
    public String promoDetailNo;

    // 상품번호
    public String itemNo;

    // 상품가격
    public String itemPrice;

    // 구매수량
    public String purchaseQty;

    // 적용수량
    public String applyQty;
}
