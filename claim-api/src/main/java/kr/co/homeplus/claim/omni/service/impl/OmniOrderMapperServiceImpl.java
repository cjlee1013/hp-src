package kr.co.homeplus.claim.omni.service.impl;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claim.claim.model.register.ClaimRegDetailDto;
import kr.co.homeplus.claim.enums.impl.EnumImpl;
import kr.co.homeplus.claim.omni.mapper.OmniReadMapper;
import kr.co.homeplus.claim.omni.mapper.OmniWriteMapper;
import kr.co.homeplus.claim.omni.model.OmniOriginOrderDto;
import kr.co.homeplus.claim.omni.service.OmniOrderMapperService;
import kr.co.homeplus.claim.utils.SqlUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OmniOrderMapperServiceImpl implements OmniOrderMapperService {

    private final OmniReadMapper omniReadMapper;
    private final OmniWriteMapper omniWriteMapper;

    @Override
    public List<OmniOriginOrderDto> getOmniOriginOrderList(long purchaseOrderNo) {
        return omniReadMapper.selectOriginOrderListByOmni(purchaseOrderNo);
    }

    @Override
    public Boolean claimSubstitutionResultModify(LinkedHashMap<String, Object> parameterMap) {
        return SqlUtils.isGreaterThanZero(omniWriteMapper.updateClaimSubstitutionResult(parameterMap));
    }

    @Override
    public Boolean omniClaimStatusModify(long claimNo, String claimStatus) {
        return SqlUtils.isGreaterThanZero(omniWriteMapper.updateOmniClaimComplete(claimNo, claimStatus));
    }

    @Override
    public Long convertOrdNoToPurchaseOrderNo(long ordNo) {
        return omniReadMapper.selectOriginPurchaseOrderNoFromOrdNo(ordNo);
    }

    @Override
    public Long convertOrdNoToClaimNo(long ordNo) {
        return omniReadMapper.selectOriginClaimNoFromOrdNo(ordNo);
    }

    @Override
    public ClaimRegDetailDto getClaimShipInfoByOmni(long purchaseOrderNo, long bundleNo){
        return omniReadMapper.selectClaimShipInfoByOmni(purchaseOrderNo, bundleNo);
    }

    @Override
    public LinkedHashMap<String, String> getClaimReasonType(String claimCd){
        return omniReadMapper.selectClaimReasonType(claimCd);
    }

    @Override
    public List<LinkedHashMap<String, Long>> getExpTotalCancelBundleNo(long purchaseOrderNo) {
        return omniReadMapper.selectExpTotalCancelBundleNo(purchaseOrderNo);
    }

    @Override
    public List<ClaimPreRefundItemList> getOrderItemInfoExp(long purchaseOrderNo, long bundleNo) {
        return omniReadMapper.selectOrderItemInfoExp(purchaseOrderNo, bundleNo);
    }

    @Override
    public boolean addClaimOosInfo(LinkedHashMap<String, Object> parameterMap) {
        return SqlUtils.isGreaterThanZero(omniWriteMapper.insertClaimOosManage(parameterMap));
    }

    @Override
    public boolean addClaimOosItem(LinkedHashMap<String, Object> parameterMap) {
        return SqlUtils.isGreaterThanZero(omniWriteMapper.insertClaimOosItem(parameterMap));
    }

    @Override
    public boolean setClaimOosMangeModify(LinkedHashMap<String, Object> parameterMap) {
        return SqlUtils.isGreaterThanZero(omniWriteMapper.updateClaimOosManage(parameterMap));
    }

    @Override
    public LinkedHashMap<String, Object> getClaimStatusInfo(long claimNo) {
        return omniReadMapper.selectClaimStatusInfo(claimNo);
    }

    @Override
    public LinkedHashMap<String, Object> getOmniShipPresentCheck(long purchaseOrderNo, long bundleNo) {
        return omniReadMapper.selectOmniShipPresentCheck(purchaseOrderNo, bundleNo);
    }
}
