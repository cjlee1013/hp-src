package kr.co.homeplus.claim.entity.order;

public class GhsItemReviewEntity {
    // GHS상품리뷰순번
    public String ghsReviewSeq;
    // 사용자번호
    public String userNo;
    // 주문상품번호(ORD_NO+GOOD_ID)
    public String orderItemNo;
    // 등록일
    public String regDt;
}
