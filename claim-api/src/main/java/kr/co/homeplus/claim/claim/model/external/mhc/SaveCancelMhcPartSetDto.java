package kr.co.homeplus.claim.claim.model.external.mhc;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@ApiModel(description = "적립취소 부분취소 요청 DTO")
public class SaveCancelMhcPartSetDto {
    @ApiModelProperty(value = "고객번호", position = 1)
    private String userNo;
    @ApiModelProperty(value = "매출일자(YYYYMMDD, 해당 취소거래의 매출일자)", position = 2)
    private String saleDt;
    @ApiModelProperty(value = "MHC대체카드번호_C", position = 3)
    private String mhcCardNoEnc;
    @ApiModelProperty(value = "MHC대체카드번호_H", position = 4)
    private String mhcCardNoHmac;
    @ApiModelProperty(value = "클레임번호", position = 5)
    private String claimNo;
    @ApiModelProperty(value = "원거래 구매주문번호", position = 6)
    private String orgPurchaseOrderNo;
    @ApiModelProperty(value = "MHC 기본적립금액", position = 7)
    private int mhcBaseAccAmt;
    @ApiModelProperty(value = "MHC 추가적립금액", position = 8)
    private int mhcAddAccAmt;
}
