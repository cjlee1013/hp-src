package kr.co.homeplus.claim.event.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "이벤트 구매내역 응 DTO")
public class PurchaseByShipCompleteGetDto {
    @ApiModelProperty(value = "주문번호")
    private long purchaseOrderNo;
    @ApiModelProperty(value = "주문일시")
    private String orderDt;
    @ApiModelProperty(value = "배송완료일")
    private String completeDt;
    @ApiModelProperty(value = "스토어타입")
    private String storeType;
    @ApiModelProperty(value = "발급가능여부")
    private String issuancePossibleYn;
    @ApiModelProperty(value = "고객번호")
    private long userNo;
}
