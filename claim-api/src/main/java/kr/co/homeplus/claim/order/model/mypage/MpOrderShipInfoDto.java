package kr.co.homeplus.claim.order.model.mypage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.Data;

@Data
@ApiModel(description = "마이페이지 > 주문/배송 > 상세 > 배송정보")
public class MpOrderShipInfoDto {

    @ApiModelProperty(value = "받는분", position = 1)
    private String receiverNm;

    @ApiModelProperty(value = "멀티번들번호", position = 2)
    private long multiBundleNo;

    @ApiModelProperty(value = "받는사람전화번호", position = 3)
    private String shipMobileNo;

    @ApiModelProperty(value = "우편번호", position = 4)
    private String zipCode;

    @ApiModelProperty(value = "도로명주소", position = 5)
    private String roadBaseAddr;

    @ApiModelProperty(value = "도로명주소상세", position = 6)
    private String roadDetailAddr;

    @ApiModelProperty(value = "안심번호구분", position = 7)
    private String safetyNm;

    @ApiModelProperty(value = "안심번호신청여부", position = 8)
    private String safetyUseYn;

    @ApiModelProperty(value = "매장배송메시지코드", position = 9)
    private String storeShipMsgCd;

    @ApiModelProperty(value = "매장배송메시지", position = 10)
    private String storeShipMsg;

    @ApiModelProperty(value = "택배배송메시지코드", position = 11)
    private String dlvShipMsgCd;

    @ApiModelProperty(value = "택배배송메시지", position = 12)
    private String dlvShipMsg;

    @ApiModelProperty(value = "새벽배송메시지코드", position = 13)
    private String auroraShipMsgCd;

    @ApiModelProperty(value = "새벽배송메시지", position = 14)
    private String auroraShipMsg;

    @ApiModelProperty(value = "개인통관고유번호", position = 15)
    private String personalOverseaNo;

    @ApiModelProperty(value = "고객 주소지 배송가능지역 유형", position = 16)
    private String shipAreaType;

    @ApiModelProperty(value = "선물하기메시지", position = 17)
    private String giftMsg;

    @ApiModelProperty(value = "예약배송일", position = 18)
    private String reserveShipDt;

    @ApiModelProperty(value = "선물수락기한", position = 19)
    private String presentExpireDt;
    @ApiModelProperty(value = "선물수락여부", position = 20)
    private String presentReceiveYn;
    @ApiModelProperty(value = "선물메시지", position = 21)
    private String presentMsg;
    @ApiModelProperty(value = "선물수락일", position = 22)
    private String presentAcpDt;
    @ApiModelProperty(value = "선물자동취소여부", position = 23)
    private String presentAutoCancelYn;

    @ApiModelProperty(value = "배송기사명", position = 24)
    private String driverNm;
    @ApiModelProperty(value = "배송기사연락처", position = 25)
    private String driverTelNo;

    @ApiModelProperty(value = "애니타임슬롯여부", position = 26)
    private String anytimeSlotYn;

    private void setPersonalOverseaNo(String personalOverseaNo){
        this.personalOverseaNo = PrivacyMaskingUtils.maskingCustomsIdNumber(personalOverseaNo);
    }

    private void setSafetyNm(String safetyNm){
        boolean isNumeric =  safetyNm.matches("[+-]?\\d*(\\.\\d+)?");
        if(isNumeric){
            this.safetyNm = safetyNm.replaceFirst("(^[0-9]{4})([0-9]{4})([0-9]{4})$", "$1-$2-$3");
        } else {
            this.safetyNm = safetyNm;
        }
    }


}
