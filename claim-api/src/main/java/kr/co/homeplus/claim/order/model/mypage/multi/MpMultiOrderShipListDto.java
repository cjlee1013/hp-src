package kr.co.homeplus.claim.order.model.mypage.multi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.Data;

@Data
@ApiModel(description = "마이페이지 > 주문/배송 > 다중배송주문 상세 > 배송정보")
public class MpMultiOrderShipListDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;
    @ApiModelProperty(value = "번들번호", position = 2)
    private long bundleNo;
    @ApiModelProperty(value = "멀티번들번호", position = 3)
    private long multiBundleNo;
    @ApiModelProperty(value = "도로명주소", position = 4)
    private String roadBaseAddr;
    @ApiModelProperty(value = "도로명주소상세", position = 5)
    private String roadDetailAddr;
    @ApiModelProperty(value = "선물하기메시지", position = 6)
    private String giftMsg;
    @ApiModelProperty(value = "예약배송일", position = 7)
    private String reserveShipDt;
    @ApiModelProperty(value = "배송상태", position = 8)
    private String shipStatus;
}
