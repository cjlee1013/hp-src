package kr.co.homeplus.claim.review.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "작성가능리뷰 리스트 DTO")
public class ReviewSearchGetDto {
    @ApiModelProperty(value = "주문일자", position = 1)
    private String orderDt;
    @ApiModelProperty(value = "주문명", position = 2)
    private String itemNm;
    @ApiModelProperty(value = "주문번호", position = 3)
    private long purchaseOrderNo;
    @ApiModelProperty(value = "배송번호", position = 4)
    private String bundleNo;
    @ApiModelProperty(value = "결제번호", position = 5)
    private long paymentNo;
    @ApiModelProperty(value = "작성기한일", position = 6)
    private String deadLineDt;
    @ApiModelProperty(value = "상품리스트", position = 7)
    private List<ReviewItemDto> reviewItem;
    @JsonIgnore
    @ApiModelProperty(value = "상점타입", position = 8, hidden = true)
    private String storeType;

    public void setDeadLineDt(String deadLineDt){
        if(Integer.parseInt(deadLineDt) == 61){
            this.deadLineDt = "60";
        } else {
            this.deadLineDt = deadLineDt;
        }
    }
}