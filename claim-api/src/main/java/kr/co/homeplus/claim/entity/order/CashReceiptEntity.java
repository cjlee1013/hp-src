package kr.co.homeplus.claim.entity.order;

public class CashReceiptEntity {
    // 결제번호
    public String paymentNo;
    // 고객번호
    public String userNo;
    // 현금영수증 용도(PERS:개인소득공제용,EVID:지출증빙용,NONE:미발행)
    public String cashReceiptUsage;
    // 현금영수증 유형(PHONE:휴대폰번호,CARD:현금영수증카드번호,BUSINESSNO:사업자번호)
    public String cashReceiptType;
    // 현금영수증 신청번호
    public String cashReceiptReqNo;
    // 등록일자
    public String regDt;

}
