package kr.co.homeplus.claim.claim.model.register.ticket;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@ApiModel(description = "클레임 티켓 정보 Insert Dto")
public class ClaimTicketRegDto {
    @ApiModelProperty(value = "클레임티켓번호", position = 1)
    private long claimTicketNo;
    @ApiModelProperty(value = "클레임번호", position = 2)
    private long claimNo;
    @ApiModelProperty(value = "클레임번들번호", position = 3)
    private long claimBundleNo;
    @ApiModelProperty(value = "클레임아이템번호", position = 4)
    private long claimItemNo;
    @ApiModelProperty(value = "상품주문번호", position = 5)
    private long orderItemNo;
    @ApiModelProperty(value = "티켓주문번호", position = 6)
    private long orderTicketNo;
    @ApiModelProperty(value = "등록일", position = 7)
    private String regDt = "NOW()";
    @ApiModelProperty(value = "수정일", position = 8)
    private String chgDt = "NOW()";
}
