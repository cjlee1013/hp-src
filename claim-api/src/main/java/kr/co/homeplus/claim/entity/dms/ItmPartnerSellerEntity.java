package kr.co.homeplus.claim.entity.dms;

public class ItmPartnerSellerEntity {
    // 파트너아이디
    public String partnerId;
    // 업체명
    public String businessNm;
    // 고객문의연락처
    public String infoCenter;
    // 대표번호1
    public String phone1;
    // 대표번호2
    public String phone2;
    // 소개
    public String companyInfo;
    // 대표 이메일
    public String email;
    // 홈페이지
    public String homepage;
    // 대표 카테고리
    public String categoryCd;
    // SMS 수신동의 여부
    public String smsYn;
    // SMS수신여부수정일시
    public String smsYnDt;
    // 이메일 수신동의 여부
    public String emailYn;
    // 이메일수신여부수정일시
    public String emailYnDt;
    // 매장위탁 물류여부(Y:사용,N:사용안함)
    public String storeUseYn;
    // 문화비소득공제(N:제공안함,Y:제공함)
    public String cultureDeductionYn;
    // 문화비소득공제사업자식별번호
    public String cultureDeductionNo;
    // 반품택배사 여부 (Y: 있음, N: 없음)
    public String returnDeliveryYn;
    // 등록일시
    public String regDt;
    // 등록자
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정자
    public String chgId;
    // 판매유형(N:일반, D:직매입)
    public String saleType;
    // 직매입시 사용하는 RMS 상품번호
    public String itemNo;
    // OF 업체 코드
    public String ofVendorCd;
    // OF 승인 상태 : 대기(B) / 완료(A)
    public String ofVendorStatus;
    // OF 승인 메일 발송일시
    public String ofSendMailDt;
    // OF연동 업체명(OF중복 이슈로 _숫자 붙이기)
    public String ofVendorNm;
    // 일련번호
    public String sellerSeq;
}
