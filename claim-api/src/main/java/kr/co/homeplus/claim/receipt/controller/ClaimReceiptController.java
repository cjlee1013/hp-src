package kr.co.homeplus.claim.receipt.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseMethodReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseReceiptGetDto;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.card.ClaimCardReceiptGetDto;
import kr.co.homeplus.claim.receipt.model.cash.ClaimCashReceiptGetDto;
import kr.co.homeplus.claim.receipt.model.order.ClaimRefundReceiptGetDto;
import kr.co.homeplus.claim.receipt.model.order.ClaimRefundReceiptSetDto;
import kr.co.homeplus.claim.receipt.service.ClaimReceiptService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/receipt")
@Api(tags = "마이페이지 > 영수증 > 클레임영수증 ")
public class ClaimReceiptController {

    private final ClaimReceiptService claimReceiptService;

    @GetMapping("/getPurchaseClaimReceipt")
    @ApiOperation(value = "클레임영수증-구매영수증", response = ClaimPurchaseReceiptGetDto.class)
    public ResponseObject<ClaimPurchaseReceiptGetDto> getPurchaseClaimReceipt(@ModelAttribute ClaimPurchaseReceiptSetDto setDto) throws Exception {
        log.debug("ClaimPurchaseReceiptSetDto ::: {}", setDto);
        return claimReceiptService.getClaimPurchaseReceiptInfo(setDto);
    }

    @GetMapping("/getPurchaseClaimCardReceipt")
    @ApiOperation(value = "클레임영수증-카드영수증", response = ClaimCardReceiptGetDto.class)
    public ResponseObject<ClaimCardReceiptGetDto> getPurchaseClaimCardReceipt(@ModelAttribute ClaimPurchaseMethodReceiptSetDto setDto) throws Exception {
        return claimReceiptService.getClaimCardPurchaseReceiptInfo(setDto);
    }

    @GetMapping("/getPurchaseClaimCashReceipt")
    @ApiOperation(value = "클레임영수증-현금영수증", response = ClaimCashReceiptGetDto.class)
    public ResponseObject<ClaimCashReceiptGetDto> getPurchaseClaimCashReceipt(@ModelAttribute ClaimPurchaseMethodReceiptSetDto setDto) throws Exception {
        return claimReceiptService.getClaimCashPurchaseReceiptInfo(setDto);
    }

    @GetMapping("/getOrderRefundReceipt")
    @ApiOperation(value = "주문상세-클레임취소금액영수증", response = ClaimRefundReceiptGetDto.class)
    public ResponseObject<List<ClaimRefundReceiptGetDto>> getOrderRefundReceipt(@ModelAttribute ClaimRefundReceiptSetDto setDto) throws Exception {
        return claimReceiptService.getOrderRefundReceipt(setDto);
    }

}
