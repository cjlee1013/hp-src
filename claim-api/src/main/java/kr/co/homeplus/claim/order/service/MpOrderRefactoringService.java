package kr.co.homeplus.claim.order.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.claim.enums.DateType;
import kr.co.homeplus.claim.enums.ResponseCode;
import kr.co.homeplus.claim.factory.ResponseFactory;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListSetDto;
import kr.co.homeplus.claim.order.model.order.MpOrderBundleShipInfoDto;
import kr.co.homeplus.claim.order.model.order.MpOrderItemDto;
import kr.co.homeplus.claim.order.model.order.MypageOrderBundleListDto;
import kr.co.homeplus.claim.order.model.order.MypageOrderListGetDto;
import kr.co.homeplus.claim.order.model.order.OrderButtonDto;
import kr.co.homeplus.claim.order.model.order.OrderButtonDto.OrderClaimButtonDto;
import kr.co.homeplus.claim.order.model.order.OrderButtonDto.OrderShipButtonDto;
import kr.co.homeplus.claim.order.model.order.OrderClaimInfoDto;
import kr.co.homeplus.claim.utils.DateUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MpOrderRefactoringService {

    private final MpOrderRefactoringMapperService mapperService;

    public ResponseObject<List<MypageOrderListGetDto>> getMyPageOrderList(MpOrderListSetDto setDto) throws Exception {
        List<MypageOrderListGetDto> orderList = mapperService.getMypageOrderList(setDto);

        for (MypageOrderListGetDto dto : orderList) {
            // 대체주문 여부
            boolean isSubstitutionOrder = dto.getSubstitutionOrderYn().equals("Y");
            // 합주문 여부
            boolean isCombine = dto.getCmbnYn().equals("Y");
            // 비회원 주문 여부
            boolean isNoMemberOrder = dto.getNoMemberOrderYn().equals("Y");
            // 번들리스트 취득
            List<MypageOrderBundleListDto> bundleList = mapperService.getMypageOrderBundleList(dto.getPurchaseOrderNo());

            if (bundleList == null) {
                log.info("주문번호 :: {} 의 배송정보가 없습니다.", dto.getPurchaseOrderNo());
                break;
            }
            for (MypageOrderBundleListDto bundleDto : bundleList) {
                //버든정보 디폴트생성
                bundleDto.setButtonInfo(this.defaultOrderButtonInfo());
                // 배송정보 Set(전체취소여부도 체크)
                this.setShipInfo(dto, bundleDto);
                // 상품리스트
                this.setOrderItemList(dto.getPurchaseOrderNo(), bundleDto, isSubstitutionOrder);
                // 합배송 아이템정보
                if("TD,PICK,AURORA".contains(bundleDto.getShipType())){
                    if(isCombine){
                        bundleDto.setCombineButtonInfo(this.defaultOrderButtonInfo());
                        bundleDto.setCombineList(this.setReviewInfo(mapperService.getMypageOrderItemListByCombine(dto.getPurchaseOrderNo(), bundleDto.getBundleNo()), bundleDto));
                    }
                }
                if (bundleDto.getShipType().equals("PRESENT")) {
                    bundleDto.setPresentInfo(mapperService.getMpOrderPresentInfo(dto.getPurchaseOrderNo()));
                }
                // 클레임 정보
                this.setOrderClaimInfo(bundleDto);
                // 버튼 정보 설정
                this.setOrderButtonInfo(bundleDto, isNoMemberOrder);
            }
            // bundle list set
            dto.setBundleList(bundleList);
            if(dto.getOrderAllCancelPossibleYn().equals("Y")){
                String shipType = dto.getBundleList().stream().map(MypageOrderBundleListDto::getShipType).distinct().collect(Collectors.toList()).get(0);
                if(!(dto.getBundleList().size() > 1) && !"MULTI".contains(shipType)){
                    dto.setOrderAllCancelPossibleYn("N");
                }
                dto.getBundleList().stream().map(MypageOrderBundleListDto::getButtonInfo).forEach(
                    buttonDto -> {
                        if(buttonDto.getClaimButtonInfo().getIsPossibleOrderCancel().equals("N") && !"MULTI".contains(shipType)){
                            dto.setOrderAllCancelPossibleYn("N");
                        }
                    }
                );
            }
        }
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, orderList);
    }

    private void setShipInfo(MypageOrderListGetDto orderDto, MypageOrderBundleListDto bundleDto){
        MpOrderBundleShipInfoDto shipInfo = mapperService.getShipInfoByBundle(orderDto.getPurchaseOrderNo(), bundleDto.getBundleNo());
        if(orderDto.getOrderAllCancelPossibleYn().equals("Y")){
            if(!"D0,D1".contains(shipInfo.getShipStatus())){
                orderDto.setOrderAllCancelPossibleYn("N");
            }
        }
        bundleDto.setShipInfo(shipInfo);
    }

    /**
     * 아이템 정보 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param bundleDto 배송번호
     * @param isSubstitutionOrder 대체주문 조회여부.
     */
    private void setOrderItemList(long purchaseOrderNo, MypageOrderBundleListDto bundleDto, boolean isSubstitutionOrder){
        List<MpOrderItemDto> orderItemList = mapperService.getMypageOrderItemList(purchaseOrderNo, bundleDto.getBundleNo());
        if("TD_DRCT,TD_PICK,AURORA".contains(bundleDto.getShipInfo().getShipMethod())){
            if(isSubstitutionOrder){
                List<MpOrderItemDto> subOrderItemList = mapperService.getMypageOrderItemListBySubstitution(purchaseOrderNo, bundleDto.getBundleNo());
                if(subOrderItemList != null){
                    orderItemList.addAll(subOrderItemList);
                }
            }
        }
        // 번들리스트에 상품정보 셋팅
        bundleDto.setItemList(this.setReviewInfo(orderItemList, bundleDto));
    }

    /**
     * 리뷰버튼 생성 정보
     *
     * @param orderItemList 아이템리스트
     * @return 아이템리스트
     */
    private List<MpOrderItemDto> setReviewInfo(List<MpOrderItemDto> orderItemList, MypageOrderBundleListDto bundleDto) {
        String shipStatus = bundleDto.getShipInfo().getShipStatus();
        for(MpOrderItemDto itemDto : orderItemList){
            if(itemDto.getProcessItemQty() > 0 || (itemDto.getOrderItemQty() - itemDto.getClaimItemQty()) <= 0 || !("D4,D5".contains(shipStatus))) {
                itemDto.setReviewPossibleYn("N");
            }
            if("D4,D5".contains(shipStatus)){
                if(DateUtils.isAfter(bundleDto.getShipInfo().getCompleteDt(), DateType.DAY, 61)){
                    itemDto.setReviewPossibleYn("N");
                }
                if("DS".contains(bundleDto.getShipType())){
                    if(!"TD_DLV".contains(bundleDto.getShipInfo().getShipMethod())){
                        if(!("W,N").contains(bundleDto.getShipInfo().getNoRcvYn())){
                            itemDto.setReviewPossibleYn("N");
                        }
                    }
                }
            }
        }
        return orderItemList;
    }

    /**
     * 합배송정보
     *
     * @param bundleDto 배송정보
     * @param orderRemainQty 남은주문건수
     * @param isOrderClose 주문마감여부
     */
    private void setCmbnInfo(MypageOrderBundleListDto bundleDto, long orderRemainQty, boolean isOrderClose, boolean isNoMemberOrder) {
        // 회원주문 && 마감전 && 클레임미진행 && 주문상품이 있는 경우
        if(!isOrderClose && (orderRemainQty > 0) && !isNoMemberOrder){
            if(bundleDto.getClaimInfo().getIsRefundFail().equalsIgnoreCase("N")){
                bundleDto.getButtonInfo().getShipButtonInfo().setIsPossibleCombine("Y");
                bundleDto.getShipInfo().setCmcnPossibleTime(this.getCovertOrderCloseDescription(bundleDto.getShipInfo()));
            }
        } else {
            bundleDto.getButtonInfo().getShipButtonInfo().setIsPossibleCombine("N");
        }
    }

    /**
     * 일시변경 버튼 생성
     *
     * @param shipButtonDto 버튼정보
     * @param orderRemainQty 남은주문건수
     * @param isOrderClose 주문마감여부
     */
    private void setChangeShipBtn(OrderShipButtonDto shipButtonDto, long orderRemainQty, boolean isOrderClose){
        // 회원주문 && 마감전 && 클레임미진행 && 주문상품이 있는 경우
        if((!isOrderClose && (orderRemainQty > 0))){
            shipButtonDto.setIsPossibleChangeShipDt("Y");
        } else {
            shipButtonDto.setIsPossibleChangeShipDt("N");
        }
    }

    /**
     * 클레임 정보 생성
     *
     * @param bundleDto 배송정보
     */
    private void setOrderClaimInfo(MypageOrderBundleListDto bundleDto){
        // 주문에 따른 클레임 조회
        bundleDto.setClaimInfo(this.setClaimInfo(bundleDto.getItemList()));
        // 합배송이 존재하면, 합배송에 대한 클레임 조회
        if(bundleDto.getCombineList() != null) {
            bundleDto.setCombineClaimInfo(this.setClaimInfo(bundleDto.getCombineList()));
        }
    }

    /**
     * 클레임 정보 조회 및 셋팅
     *
     * @param itemDto 아이템정보
     * @return 클레임정보 DTO
     */
    private OrderClaimInfoDto setClaimInfo(List<MpOrderItemDto> itemDto){
        // 주문번호, 배송번호 중복값 제거 처리.
        Map<Long, Long> map = itemDto.stream().filter(dto -> dto.getSubstitutionItemYn().equals("N")).collect(Collectors.toMap(MpOrderItemDto::getPurchaseOrderNo, MpOrderItemDto::getBundleNo, (p1, p2) -> p1));
        // 클레임 정보 리스트 생성.
        // 번들번호가 다중일 경우를 대비하여 처리.
        // 합배송, 대체에 대한 클레임 정보를 넘겨주기 위함. (현재 원주문에 대한 정보만 내려감.)
        OrderClaimInfoDto claimInfoDto = new OrderClaimInfoDto();
        for(long purchaseOrderNo : map.keySet()){
            log.info("purchaseOrderNo :: {} || bundleNo :: {}", purchaseOrderNo, map.get(purchaseOrderNo));
            claimInfoDto = mapperService.getOrderClaimInfoByBundle(purchaseOrderNo, map.get(purchaseOrderNo)).get(0);
            claimInfoDto.setIsBundleAllCancel(this.setBundleAllCancelInfo(itemDto));
            if(claimInfoDto.getClaimNo() != 0){
                claimInfoDto.setIsRefundFail(mapperService.getClaimRefundFailCheck(claimInfoDto.getClaimNo()));
                if(itemDto.stream().mapToLong(MpOrderItemDto::getProcessItemQty).sum() > 0){
                    log.info("setClaimInfo : {}, {}", itemDto, itemDto.stream().mapToLong(MpOrderItemDto::getProcessItemQty).sum());
                    claimInfoDto.setClaimProcessYn("Y");
                }
            }
            log.info("주문번호({}) - 배송번호({}) 클레임정보 (OrderClaimInfoDto) : {}", purchaseOrderNo, map.get(purchaseOrderNo), claimInfoDto);
        }
        return claimInfoDto;
    }

    /**
     * 배송별 전체취소 여부
     *
     * @param itemDto 아이템리스트
     * @return 배송별 전체취소 여부
     */
    private String setBundleAllCancelInfo(List<MpOrderItemDto> itemDto) {
        long originOrderQty = itemDto.stream().mapToLong(MpOrderItemDto::getOrderItemQty).sum();
        long originClaimQty = itemDto.stream().mapToLong(MpOrderItemDto::getClaimItemQty).sum();
        return (originOrderQty - originClaimQty) == 0 ? "Y" : "N";
    }

    /**
     * 주문리스트 버튼 제어 초기값 셋팅
     * @return 버튼정보 DTO
     */
    private OrderButtonDto defaultOrderButtonInfo(){
        return
            OrderButtonDto.builder().shipButtonInfo(
                OrderShipButtonDto.builder()
                    .isPossibleChangeShipDt("N")
                    .isPossibleCombine("N")
                    .isPossibleCompleteShip("N")
                    .isPossibleDelayShip("N")
                    .isPossibleExpectedDate("N")
                    .isPossibleNotReceiveShip("N")
                    .isPossibleReview("N")
                    .isPossibleSearchShip("N")
                    .isPossibleShipInfo("N")
                    .isPossibleNotReceiverReject("N")
                    .isPossibleNotReceiverShipList("N")
                    .build()
            ).claimButtonInfo(
                OrderClaimButtonDto.builder()
                    .isPossibleOrderCancel("N")
                    .isPossibleCancel("N")
                    .isPossibleReturn("N")
                    .isPossibleExchange("N")
                .build()
            ).build();
    }

    private void setOrderButtonInfo(MypageOrderBundleListDto bundleDto, boolean isNoMemberOrder) {
        // 원주문부터 설정.
        OrderButtonDto buttonInfo = bundleDto.getButtonInfo();
        buttonInfo.setShipButtonInfo(this.setOrderButton(bundleDto, Boolean.FALSE, isNoMemberOrder));
        buttonInfo.setClaimButtonInfo(this.setClaimButton(bundleDto, Boolean.FALSE));
        bundleDto.setButtonInfo(buttonInfo);
        // 합배송정보가 있을 경우에 설정.
        if(bundleDto.getCombineList() != null) {
            OrderButtonDto combineButtonInfo = bundleDto.getCombineButtonInfo();
            combineButtonInfo.setShipButtonInfo(this.setOrderButton(bundleDto, Boolean.TRUE, isNoMemberOrder));
            combineButtonInfo.setClaimButtonInfo(this.setClaimButton(bundleDto, Boolean.TRUE));
            bundleDto.setCombineButtonInfo(combineButtonInfo);
        }
    }

    /**
     * 클레임 버튼 생성.
     *
     */
    private OrderClaimButtonDto setClaimButton(MypageOrderBundleListDto bundleDto, boolean isCombine){
        // 상품리스트정보
        List<MpOrderItemDto> itemList = bundleDto.getItemList();
        // 버튼정보
        OrderClaimButtonDto claimButtonDto = bundleDto.getButtonInfo().getClaimButtonInfo();
        // 합배송데이터 조회시
        if(isCombine){
            itemList = bundleDto.getCombineList();
            claimButtonDto = bundleDto.getCombineButtonInfo().getClaimButtonInfo();
        }
        // 주문전체상품수량
        long totalOrderItemQty = itemList.stream().filter(dto -> dto.getSubstitutionItemYn().equals("N")).mapToLong(MpOrderItemDto::getOrderItemQty).sum();
        // 클레임완료상품수량
        long totalClaimItemQty = itemList.stream().filter(dto -> dto.getSubstitutionItemYn().equals("N")).mapToLong(MpOrderItemDto::getClaimItemQty).sum();
        // 클레임진행중인수량
        long totalProcessItemQty = itemList.stream().filter(dto -> dto.getSubstitutionItemYn().equals("N")).mapToLong(MpOrderItemDto::getProcessItemQty).sum();

        boolean isSubOrder = itemList.stream().map(MpOrderItemDto::getSubstitutionItemYn).anyMatch(s -> s.equals("Y"));
        // 클레임 진행건이 0보다 크면 진행중
        if(totalProcessItemQty == 0){
            // 주문건수 - 클레임완료건수가 0보다 크면 배송상태에 따라 클레임 가능.
            if((totalOrderItemQty - totalClaimItemQty) > 0){
                switch (bundleDto.getShipInfo().getShipStatus()) {
                    case "D0":
                    case "D1" :
                        // 주문취소
                        if(!"MULTI".contains(bundleDto.getShipType())) {
                            claimButtonDto.setIsPossibleOrderCancel("Y");
                        }
                        break;
                    case "D2" :
                        // 취소신청 (티켓주문은 제외)
                        if(!"TICKET,MULTI".contains(bundleDto.getShipType())) {
                            claimButtonDto.setIsPossibleCancel("Y");
                        }
                        break;
                    case "D3" :
                        // 반품/교환신청 (티켓, 렌탈주문 제외)
                        if(!"TICKET,CALL,MULTI".contains(bundleDto.getShipType())) {
                            // TD,PICK일 땐 교환버튼 미노출
                            if(!"TD,PICK".contains(bundleDto.getShipType()) && !"TD_DRCT,TD_PICK,TD_QUICK".contains(bundleDto.getShipInfo().getShipMethod())){
                                if(bundleDto.getCombineList() == null && !isSubOrder){
                                    claimButtonDto.setIsPossibleReturn("Y");
                                }
                                claimButtonDto.setIsPossibleExchange("Y");
                                if("RESERVE".equals(bundleDto.getShipType())) {
                                    if(bundleDto.getItemList().stream().anyMatch(itemDto -> "RESERVE_DRCT_DLV".equals(itemDto.getReserveShipMethod()))){
                                        claimButtonDto.setIsPossibleExchange("N");
                                    }
                                }
                            }
                        }
                        break;
                    case "D4" :
                    case "D5" :
                        // 반품/교환신청 (티켓, 렌탈주문 제외), 취소신청(티켓주문만)
                        if(!"TICKET,CALL,MULTI".contains(bundleDto.getShipType())) {
                            if(DateUtils.isBefore(bundleDto.getShipInfo().getCompleteDt(), DateType.DAY, 8)){
                                claimButtonDto.setIsPossibleReturn("Y");
                                // TD,PICK일 땐 교환버튼 미노출
                                if(!"TD,PICK".contains(bundleDto.getShipType())){
                                    claimButtonDto.setIsPossibleExchange("Y");
                                    if("RESERVE".equals(bundleDto.getShipType())) {
                                        if(bundleDto.getItemList().stream().anyMatch(itemDto -> "RESERVE_DRCT_DLV".equals(itemDto.getReserveShipMethod()))){
                                            claimButtonDto.setIsPossibleExchange("N");
                                        }
                                    }
                                }
                            }
                        }
                        if("D5".contains(bundleDto.getShipInfo().getShipStatus())){
                            if("TICKET".equalsIgnoreCase(bundleDto.getShipType())) {
                                String isAutoCancel = isAutoCancelTicket(bundleDto.getBundleNo());
                                if(isAutoCancel != null){
                                    log.info("배송번호({})에 대한 티켓 취소여부 판단 ::: {}", bundleDto.getBundleNo(), isAutoCancel);
                                    if(Boolean.parseBoolean(isAutoCancel)){
                                        claimButtonDto.setIsPossibleOrderCancel("Y");
                                    } else {
                                        claimButtonDto.setIsPossibleCancel("Y");
                                    }
                                } else {
                                    claimButtonDto.setIsPossibleOrderCancel("N");
                                    claimButtonDto.setIsPossibleCancel("N");
                                }
                            }
                        }

                        break;
                }
            }
        }
        return claimButtonDto;
    }

    private OrderShipButtonDto setOrderButton(MypageOrderBundleListDto bundleDto, boolean isCombine, boolean isNoMemberOrder) {
        // 상품리스트정보
        List<MpOrderItemDto> itemList = bundleDto.getItemList();
        // 버튼정보
        OrderShipButtonDto shipButtonDto = bundleDto.getButtonInfo().getShipButtonInfo();
        // 합배송데이터 조회시
        if(isCombine){
            itemList = bundleDto.getCombineList();
            shipButtonDto = bundleDto.getCombineButtonInfo().getShipButtonInfo();
        }
        // 주문전체상품수량
        long totalOrderItemQty = itemList.stream().filter(dto -> dto.getSubstitutionItemYn().equals("N")).mapToLong(MpOrderItemDto::getOrderItemQty).sum();
        // 클레임완료상품수량
        long totalClaimItemQty = itemList.stream().filter(dto -> dto.getSubstitutionItemYn().equals("N")).mapToLong(MpOrderItemDto::getClaimItemQty).sum();
        // 클레임진행중인수량
        long totalProcessItemQty = itemList.stream().filter(dto -> dto.getSubstitutionItemYn().equals("N")).mapToLong(MpOrderItemDto::getProcessItemQty).sum();

        switch (bundleDto.getShipInfo().getShipStatus()) {
//            case "D0" :
            case "D1" :
                if("TD,PICK".contains(bundleDto.getShipType())) {
                    boolean isOrderClose = this.isOrderClose(bundleDto.getShipInfo().getShipDt(), bundleDto.getShipInfo().getShipCloseTime());
                    // 합배송버튼 (합배송아이템이 있으면 합배송불가)
                    if(!isCombine){
                        // 합배송은 원주문에만 표현해야함.
                        if(bundleDto.getCombineList() == null){
                            if(totalProcessItemQty == 0) {
                                this.setCmbnInfo(bundleDto, (totalOrderItemQty - totalClaimItemQty), isOrderClose, isNoMemberOrder);
                            }
                        } else {
                            bundleDto.getButtonInfo().getShipButtonInfo().setIsPossibleCombine("N");
                        }
                    }
//                    if(DateUtils.isAfter(bundleDto.getShipInfo().getOrderDt(), DateType.MIN, 15)){
                        // 일시변경
                    if("N".equals(bundleDto.getShipInfo().getAnytimeSlotYn())){
                        this.setChangeShipBtn(shipButtonDto, (totalOrderItemQty - totalClaimItemQty), isOrderClose);
                    }
//                    }
                } else if ("DS,PRESENT".contains(bundleDto.getShipType())){
                    // 배송지연 (배송지연정보가 있으면 노출)
                    if(bundleDto.getShipInfo().getDelayShipCd() != null){
                        shipButtonDto.setIsPossibleDelayShip("Y");
                    }
                }
                break;
            case "D2" :
                if ("DS,PRESENT".contains(bundleDto.getShipType())){
                    // 배송지연 (배송지연정보가 있으면 노출)
                    if(bundleDto.getShipInfo().getDelayShipCd() != null){
                        shipButtonDto.setIsPossibleDelayShip("Y");
                    }
                }
                break;
            case "D3" :
                if("TD".contains(bundleDto.getShipType())) {
                    // 배송조회
                    shipButtonDto.setIsPossibleSearchShip("Y");
                } else if("DS,PRESENT".contains(bundleDto.getShipType())) {
                    if(bundleDto.getShipInfo().getShipMethod().contains("DLV")){
                        shipButtonDto.setIsPossibleSearchShip("Y");
                    } else if(bundleDto.getShipInfo().getShipMethod().equals("DS_DRCT")) {
                        // 배송예정일(직접배송)
                        shipButtonDto.setIsPossibleExpectedDate("Y");
                    } else if(bundleDto.getShipInfo().getShipMethod().contains("QUICK") || bundleDto.getShipInfo().getShipMethod().contains("POST")) {
                        // 배송안내(퀵,우편)
                        shipButtonDto.setIsPossibleShipInfo("Y");
                    }
                    if(!"TD_DLV".contains(bundleDto.getShipInfo().getShipMethod())){
                        switch (bundleDto.getShipInfo().getNoRcvYn()){
                            case "N" :
                                if(totalProcessItemQty == 0){
                                    // 미수취신고 배송중일 땐 shipping_dt 기준으로 +3 ~ +8일 까지
                                    if(DateUtils.isBetween(bundleDto.getShipInfo().getShippingDt(), DateType.DAY, 3, 8)){
                                        shipButtonDto.setIsPossibleNotReceiveShip("Y");
                                    }
                                    if(DateUtils.isBefore(bundleDto.getShipInfo().getShippingDt(), DateType.DAY, 8)){
                                        // 배송상태 = 배송중 & 배송 송장 등록일 +8D까지 & 클레임 진행 데이터 = NO & 미수취신고 = NO
                                        shipButtonDto.setIsPossibleCompleteShip("Y");
                                    }
                                }
                                break;
                            case "C" :
                                shipButtonDto.setIsPossibleNotReceiverReject("Y");
                                break;
                            case "Y" :
                            case "W" :
                                shipButtonDto.setIsPossibleNotReceiverShipList("Y");
                                break;
                        }
                    }
                }
                break;
            case "D4" :
                if("TD".contains(bundleDto.getShipType())) {
                    // 배송조회
                    if(DateUtils.isBefore(bundleDto.getShipInfo().getCompleteDt(), DateType.MONTH, 3)){
                        shipButtonDto.setIsPossibleSearchShip("Y");
                    }
                    if(totalProcessItemQty == 0){
                        if(DateUtils.isBefore(bundleDto.getShipInfo().getCompleteDt(), DateType.DAY, 61)){
                            shipButtonDto.setIsPossibleReview("Y");
                        }
                    }
                } else {
                    if("DS,PRESENT".contains(bundleDto.getShipType())) {
                        if(DateUtils.isBefore(bundleDto.getShipInfo().getCompleteDt(), DateType.MONTH, 3)){
                            if(bundleDto.getShipInfo().getShipMethod().contains("DLV")){
                                // 배송조회 (택배) - 배송중 | 배송완료 & 배송완료일 +3M까지
                                shipButtonDto.setIsPossibleSearchShip("Y");
                            } else if(bundleDto.getShipInfo().getShipMethod().equals("DS_DRCT")) {
                                // 배송예정일(직접배송)
                                shipButtonDto.setIsPossibleExpectedDate("Y");
                            } else if(bundleDto.getShipInfo().getShipMethod().contains("QUICK") || bundleDto.getShipInfo().getShipMethod().contains("POST")) {
                                // 배송안내(퀵,우편)
                                shipButtonDto.setIsPossibleShipInfo("Y");
                            }
                        }
                        if(!"TD_DLV".contains(bundleDto.getShipInfo().getShipMethod())){
                            // 미수취신고
                            switch (bundleDto.getShipInfo().getNoRcvYn()){
                                case "N" :
                                    if(totalProcessItemQty == 0){
                                        // 고객이 수취확인 한 거면 미수취신고 미노출
                                        if(!bundleDto.getShipInfo().getCompleteRegId().toUpperCase().equals("MYPAGE")){
                                            // 배송완료 & 배송완료일 +8D까지 & 클레임 진행 데이터 = NO & 미수취신고 = NO
                                            if(DateUtils.isBefore(bundleDto.getShipInfo().getCompleteDt(), DateType.DAY, 8)){
                                                shipButtonDto.setIsPossibleNotReceiveShip("Y");
                                            }
                                        }
                                    }
                                    break;
                                case "C" :
                                    shipButtonDto.setIsPossibleNotReceiverReject("Y");
                                    break;
                                case "Y" :
                                case "W" :
                                    shipButtonDto.setIsPossibleNotReceiverShipList("Y");
                                    break;
                            }
                        }
                    }
                    if(bundleDto.getShipInfo().getNoRcvYn().equals("N")){
                        if(totalProcessItemQty == 0){
                            if(DateUtils.isBefore(bundleDto.getShipInfo().getCompleteDt(), DateType.DAY, 61)){
                                shipButtonDto.setIsPossibleReview("Y");
                            }
                        }
                    }
                }
                break;
            case "D5":
                if("TD".contains(bundleDto.getShipType())) {
                    // 배송조회
                    if(DateUtils.isBefore(bundleDto.getShipInfo().getCompleteDt(), DateType.MONTH, 3)){
                        shipButtonDto.setIsPossibleSearchShip("Y");
                    }
                    if (totalProcessItemQty == 0) {
                        if(DateUtils.isBefore(bundleDto.getShipInfo().getCompleteDt(), DateType.DAY, 61)){
                            shipButtonDto.setIsPossibleReview("N");
                        }
                    }
                } else {
                    if(DateUtils.isBefore(bundleDto.getShipInfo().getCompleteDt(), DateType.MONTH, 3)){
                        if(bundleDto.getShipInfo().getShipMethod().contains("DLV")){
                            // 배송조회 (택배) - 배송중 | 배송완료 & 배송완료일 +3M까지
                            shipButtonDto.setIsPossibleSearchShip("Y");
                        } else if(bundleDto.getShipInfo().getShipMethod().equals("DS_DRCT")) {
                            // 배송예정일(직접배송)
                            shipButtonDto.setIsPossibleExpectedDate("Y");
                        } else if(bundleDto.getShipInfo().getShipMethod().contains("QUICK") || bundleDto.getShipInfo().getShipMethod().contains("POST")) {
                            // 배송안내(퀵,우편)
                            shipButtonDto.setIsPossibleShipInfo("Y");
                        }
                    }
                    if(bundleDto.getShipInfo().getNoRcvYn().equals("N")) {
                        if (totalProcessItemQty == 0) {
                            if(DateUtils.isBefore(bundleDto.getShipInfo().getCompleteDt(), DateType.DAY, 61)){
                                shipButtonDto.setIsPossibleReview("Y");
                            }
                        }
                    }
                }
                break;
        }

        return shipButtonDto;
   }

    /**
     * 주문마감 체크
     *
     * @param shipDt 주문일
     * @param closeTime 마감시간
     * @return 주문 마감 여부
     */
    private Boolean isOrderClose(String shipDt, String closeTime) {
        try{
            return DateUtils.isAfter(DateUtils.createDateLocalTime(shipDt, closeTime), DateType.MIN, -20);
        } catch (Exception e) {
            log.error("주문마감체크 오류 :: {}", e.getMessage());
            return Boolean.FALSE;
        }
    }

    private String isAutoCancelTicket(long bundleNo) {
        List<LinkedHashMap<String, Object>> ticketInfo = mapperService.getAutoCancelTicketCheck(bundleNo);
        log.info("배송번호({}) 티켓 정보 확인 ::: {}", bundleNo, ticketInfo);
        if(ticketInfo == null || ticketInfo.size() == 0){
            return null;
        }
        return String.valueOf(ticketInfo.stream().map(map -> map.get("issue_channel")).map(String::valueOf).allMatch(param -> param.equals("COOP")));
    }

    private String getCovertOrderCloseDescription(MpOrderBundleShipInfoDto shipInfoDto) {
        try{
            return DateUtils.customDateStr(
                DateUtils.setDateTime(DateUtils.createDateLocalTime(shipInfoDto.getShipDt(), shipInfoDto.getShipCloseTime()), DateType.MIN, -20),
                "MM월 dd일 (E) HH시 mm분"
            );
        } catch (Exception e) {
            log.error("합배송 시간 표현 오류 :: {}", e.getMessage());
            return null;
        }
    }
}
