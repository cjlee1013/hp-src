package kr.co.homeplus.claim.omni.model.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "대체주문결과 DTO")
public class OmniSubstitutionCompleteDto {
    @ApiModelProperty(value = "대체주문번호", position = 1)
    private long substitutionPurchaseOrderNo;
    @ApiModelProperty(value = "원주문번호", position = 2)
    private long purchaseOrderNo;
    @ApiModelProperty(value = "고객번호", position = 3)
    private long userNo;
}
