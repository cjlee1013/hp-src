package kr.co.homeplus.claim.order.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.core.db.annotation.SecondaryConnection;
import kr.co.homeplus.claim.order.model.inquiry.InquiryItemDto;
import kr.co.homeplus.claim.order.model.inquiry.OrderInquiryGetDto;
import kr.co.homeplus.claim.order.model.inquiry.OrderInquirySetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderDetailInfoSetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderDetailListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderClaimInfoDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderClaimPaymentInfoDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderDetailListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderDetailListGetDto.MpOrderDetailClaimDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderDetailListSetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderGiftItemListDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderGiftListDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderItemListDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListSetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPaymentInfoDto.OrderBenefitDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPickupStoreInfoDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPresentDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderTicketDetailGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderTicketDetailGetDto.MpOrderTicketItemDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderProductListGetDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderShipInfoDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderShipListDto;
import kr.co.homeplus.claim.order.model.ship.MultiOrderShipAddrInfoDto;
import kr.co.homeplus.claim.order.model.subMain.MpOrderMainDto.MyPageMainOrderCount;
import kr.co.homeplus.claim.order.model.subMain.MpOrderMainDto.MyPageMainOrderList;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPaymentInfoDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPaymentInfoDto.OrderCouponDiscountDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPaymentInfoDto.OrderPaymentListDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPaymentInfoDto.OrderPromoDiscountDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderProductListGetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderShipInfoDto;
import kr.co.homeplus.claim.order.model.user.CustomerGradeInfoGetDto.CustomerGradeOrderListDto;
import kr.co.homeplus.claim.order.sql.MypageOrderSearchSql;
import kr.co.homeplus.claim.order.sql.OrderDataSql;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;

@SecondaryConnection
public interface OrderSecondaryMapper {

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMpOrderList")
    List<MpOrderListGetDto> selectMpOrderList(MpOrderListSetDto mpOrderListSetDto);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMpOrderListInfo")
    List<MpOrderItemListDto> selectMpOrderListInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo, @Param("userNo") long userNo, @Param("isCmbn") Boolean isCmbn, @Param("isSubYn") Boolean isSubYn);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMpOrderShipInfo")
    List<MpOrderShipInfoDto> selectMpOrderShipInfo(MpOrderDetailListSetDto mpOrderDetailListSetDto);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMpOrderMultiShipInfo")
    MpMultiOrderShipInfoDto selectMpOrderMultiShipInfo(MpMultiOrderDetailInfoSetDto mpMultiOrderDetailInfoSetDto);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMpOrderProductList")
    List<MpOrderProductListGetDto> selectMpOrderProductList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo, @Param("isCmbn") Boolean isCmbn);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMpOrderMultiProductList")
    List<MpMultiOrderProductListGetDto> selectMpOrderMultiProductList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("multiBundleNo") long multiBundleNo, @Param("userNo") long userNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectOrderDetailInfo")
    MpOrderDetailListGetDto selectOrderDetailInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo, @Param("siteType") String siteType, @Param("isCmbn") Boolean isCmbn);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMultiOrderDetailList")
    MpMultiOrderDetailListGetDto selectMultiOrderDetailList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo, @Param("siteType") String siteType);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMpMultiOrderShipList")
    List<MpMultiOrderShipListDto> selectMpMultiOrderShipList(MpOrderDetailListSetDto mpOrderDetailListSetDto);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectPurchaseInfoList")
    MpOrderPaymentInfoDto selectPurchaseInfoList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectCouponDiscountInfoList")
    List<OrderCouponDiscountDto> selectCouponDiscountInfoList(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectPromoDiscountInfoList")
    List<OrderPromoDiscountDto> selectPromoDiscountInfoList(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectPaymentInfoList")
    List<OrderPaymentListDto> selectPaymentInfoList(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMpMainOrderCount")
    List<MyPageMainOrderCount> selectMpMainOrderCount(@Param("userNo") long userNo, @Param("siteType") String siteType);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMpMainOrderList")
    List<MyPageMainOrderList> selectMpMainOrderList(@Param("userNo") long userNo, @Param("siteType") String siteType);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMpOrderClaimInfo")
    List<MpOrderClaimInfoDto> selectMpOrderClaimInfo(@Param("purchaseOrderNo") long purchaseOrderNo,  @Param("bundleNo") long bundleNo, @Param("isOrigin") Boolean isOrigin);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMpOrderClaimOrgInfo")
    List<MpOrderClaimInfoDto> selectMpOrderClaimOrgInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMpOrderClaimPaymentInfo")
    MpOrderClaimPaymentInfoDto selectMpOrderClaimPaymentInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectClaimPromoDiscountInfoList")
    List<OrderPromoDiscountDto> selectClaimPromoDiscountInfoList(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectClaimCouponDiscountInfoList")
    List<OrderCouponDiscountDto> selectClaimCouponDiscountInfoList(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectClaimPaymentInfoList")
    List<OrderPaymentListDto> selectClaimPaymentInfoList(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectOrderBenefitInfoList")
    List<OrderBenefitDto> selectOrderBenefitInfoList(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectClaimBenefitInfoList")
    List<OrderBenefitDto> selectClaimBenefitInfoList(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectOrderGiftList")
    List<MpOrderGiftListDto> selectOrderGiftList(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectOrderGiftItemList")
    List<MpOrderGiftItemListDto> selectOrderGiftItemList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectPickStoreInfo")
    MpOrderPickupStoreInfoDto selectPickStoreInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectOrderTicketDetailInfo")
    MpOrderTicketDetailGetDto selectOrderTicketDetailInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectOrderTicketDetailItemInfo")
    List<MpOrderTicketItemDto> selectOrderTicketDetailItemInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectOrderDetailClaimInfo")
    List<MpOrderDetailClaimDto> selectOrderDetailClaimInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMultiOrderDetailClaimInfo")
    List<MpOrderDetailClaimDto> selectMultiOrderDetailClaimInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectSafetyIssueInfo")
    LinkedHashMap<String, Object> selectSafetyIssueInfo(@Param("bundleNo") long bundleNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectOrderPresentInfo")
    MpOrderPresentDto selectOrderPresentInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectClaimShippingStatus")
    LinkedHashMap<String, String> selectClaimShippingStatus(@Param("claimNo") long claimNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectUserGradeUserInfo")
    List<CustomerGradeOrderListDto> selectUserGradeUserInfo(@Param("userNo") long userNo, @Param("searchDt") String searchDt, @Param("siteType") String siteType);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectTicketFromCoopCheck")
    int selectTicketFromCoopCheck(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectSubstitutionAmt")
    int selectSubstitutionAmt(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OrderDataSql.class, method = "selectInquiryOrderInfo")
    List<OrderInquiryGetDto> selectInquiryOrderInfo(OrderInquirySetDto setDto);

    @SelectProvider(type = OrderDataSql.class, method = "selectInquiryOrderItemList")
    List<InquiryItemDto> selectInquiryOrderItemList(@Param("bundleNo") long bundleNo);

    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectUserGradeUserInfoByClaim")
    List<CustomerGradeOrderListDto> selectUserGradeUserInfoByClaim(@Param("userNo") long userNo, @Param("searchDt") String searchDt, @Param("siteType") String siteType);

    @ResultType(value = MultiOrderShipAddrInfoDto.class)
    @SelectProvider(type = MypageOrderSearchSql.class, method = "selectMultiOrderShipAddrInfo")
    List<MultiOrderShipAddrInfoDto> selectMultiOrderShipAddrInfo(@Param("multiBundleNo") long multiBundleNo);
}
