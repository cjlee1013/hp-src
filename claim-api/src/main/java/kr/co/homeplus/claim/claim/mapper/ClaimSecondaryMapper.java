package kr.co.homeplus.claim.claim.mapper;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.claim.model.mp.ClaimItemListDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimDetailGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimListItemListDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPaymentType;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPurchaseGift;
import kr.co.homeplus.claim.claim.model.mp.ClaimReasonSearchGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimRefundType;
import kr.co.homeplus.claim.claim.model.mp.ClaimRegisterPreRefundInfoDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimSearchListGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimSearchListSetDto;
import kr.co.homeplus.claim.claim.sql.ClaimSearchSql;
import kr.co.homeplus.claim.claim.sql.MyPageShipSql;
import kr.co.homeplus.claim.core.db.annotation.SecondaryConnection;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipInfoGetDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipInfoSetDto;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.mapping.StatementType;

@SecondaryConnection
public interface ClaimSecondaryMapper {

    @ResultType(value = ClaimSearchListGetDto.class)
    @SelectProvider(type = ClaimSearchSql.class, method="selectClaimSearchList")
    List<ClaimSearchListGetDto> selectClaimSearchList(ClaimSearchListSetDto claimSearchListSetDto);

    @ResultType(value = ClaimSearchListGetDto.class)
    @SelectProvider(type = ClaimSearchSql.class, method="selectClaimListPartnerInfo")
    LinkedHashMap<String, Object> selectClaimListPartnerInfo(@Param("claimBundleNo")long claimBundleNo, @Param("partnerId")String partnerId);

    @ResultType(value = ClaimSearchListGetDto.class)
    @SelectProvider(type = ClaimSearchSql.class, method="selectClaimSearchListCnt")
    int selectClaimSearchListCnt(ClaimSearchListSetDto claimSearchListSetDto);

    @ResultType(value = ClaimItemListDto.class)
    @SelectProvider(type = ClaimSearchSql.class, method="selectClaimListSearchItemList")
    List<ClaimListItemListDto> selectClaimListSearchItemList(@Param("claimBundleNo")long claimBundleNo);

    @ResultType(value = ClaimItemListDto.class)
    @SelectProvider(type = ClaimSearchSql.class, method="selectClaimSearchItemList")
    List<ClaimItemListDto> selectClaimSearchItemList(@Param("claimNo")long claimNo);

    @ResultType(value = ClaimDetailGetDto.class)
    @SelectProvider(type = ClaimSearchSql.class, method="selectClaimDetail")
    ClaimDetailGetDto selectClaimDetail(@Param("userNo") long userNo, @Param("claimNo")long claimNo);

    @ResultType(value = ClaimReasonSearchGetDto.class)
    @SelectProvider(type = ClaimSearchSql.class, method="selectClaimReason")
    List<ClaimReasonSearchGetDto> selectClaimReason(@Param("claimType")String claimType);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimSearchSql.class, method = "callByClaimPreRefundPriceCalculation")
    ClaimPreRefundCalcDto callByClaimPreRefundPriceCalculation(HashMap<String, Object> parameterMap);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimSearchSql.class, method = "callByClaimPreRefundPriceMultiCalculation")
    ClaimPreRefundCalcDto callByClaimPreRefundPriceMultiCalculation(HashMap<String, Object> parameterMap);

    @ResultType(value = NoRcvShipInfoGetDto.class)
    @SelectProvider(type = MyPageShipSql.class, method = "selectNoRcvShipInfo")
    NoRcvShipInfoGetDto selectNoRcvShipInfo(NoRcvShipInfoSetDto infoSetDto);

    @SelectProvider(type = MyPageShipSql.class, method = "selectNonShipCompleteInfo")
    LinkedHashMap<String, Object> selectNonShipCompleteInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @ResultType(value = ClaimRegisterPreRefundInfoDto.class)
    @SelectProvider(type = ClaimSearchSql.class, method = "selectRegisterPreRefundAmt")
    ClaimRegisterPreRefundInfoDto selectRegisterPreRefundAmt (@Param("claimNo") long claimNo);

    @ResultType(value = ClaimRefundType.class)
    @SelectProvider(type = ClaimSearchSql.class, method = "selectRefundTypeList")
    List<ClaimRefundType> selectRefundTypeList (@Param("claimNo") long claimNo);

    @ResultType(value = ClaimPurchaseGift.class)
    @SelectProvider(type = ClaimSearchSql.class, method = "selectClaimPurchaseGift")
    List<ClaimPurchaseGift> selectClaimPurchaseGift (@Param("claimNo")long claimNo);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimSearchSql.class, method = "callByClaimPreRefundPriceOosCalculation")
    ClaimPreRefundCalcDto callByClaimPreRefundPriceOosCalculation(HashMap<String, Object> parameterMap);

    @ResultType(value = ClaimPaymentType.class)
    @SelectProvider(type = ClaimSearchSql.class, method = "selectPaymentTypeList")
    List<ClaimPaymentType> selectPaymentTypeList (@Param("purchaseOrderNo") long purchaseOrderNo);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimSearchSql.class, method = "callByClaimMultiPreRefundPriceCalculation")
    ClaimPreRefundCalcDto callByClaimMultiPreRefundPriceCalculation(HashMap<String, Object> parameterMap);

}
