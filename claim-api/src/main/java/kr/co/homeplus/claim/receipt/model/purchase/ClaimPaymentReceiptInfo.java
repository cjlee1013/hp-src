package kr.co.homeplus.claim.receipt.model.purchase;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "클레임_구매영수증(결제)")
public class ClaimPaymentReceiptInfo {

    @ApiModelProperty(value = "클레임승인일자", position = 1)
    private String completeDt;

    @ApiModelProperty(value = "파트너아이디", position = 2)
    private String partnerId;

    @ApiModelProperty(value = "파트너명", position = 3)
    private String partnerName;

    @ApiModelProperty(value = "PG결제수단", position = 4)
    private String methodNm;

    @ApiModelProperty(value = "취소금액", position = 5)
    private long claimAmt;

    @ApiModelProperty(value = "영수증구분(NO_TARGET:비대상,CARD:신용카드전표,CASH:현금영수증전표", position = 6)
    private String receiptType;

    @ApiModelProperty(value = "점포타입", position = 7)
    private String storeType;

    @ApiModelProperty(value = "영수증타입", position = 8)
    private String cashReceiptUsage;

    @ApiModelProperty(value = "배송번호", position = 9)
    private long bundleNo;
}