package kr.co.homeplus.claim.ship.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.claim.ship.model.DlvCdGetDto;
import kr.co.homeplus.claim.ship.model.ShipMsgCdGetDto;
import kr.co.homeplus.claim.ship.service.ShipService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ship")
@RequiredArgsConstructor
@Slf4j
@Api(tags = "마이페이지 > 배송관련 정보")
public class ShipController {

    private final ShipService shipService;

    @GetMapping("/dlvCdSearch")
    @ApiOperation(value = "택배사 코드 조회", response = DlvCdGetDto.class)
    public ResponseObject<List<DlvCdGetDto>> dlvCdSearch(){
        return shipService.dlvCdSearch();
    }

    @GetMapping("/shipMsgSearch")
    @ApiOperation(value = "배송시 요청내역 코드 조회", response = ShipMsgCdGetDto.class)
    public ResponseObject<List<ShipMsgCdGetDto>> shipMsgSearch(){
        return shipService.shipMsgSearch();
    }
}
