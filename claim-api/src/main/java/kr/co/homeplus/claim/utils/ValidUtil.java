package kr.co.homeplus.claim.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kr.co.homeplus.claim.constants.PatternConstants;

/**
 * 유효성검사 Util
 */
public class ValidUtil {

    public static boolean isPattern(String format, String s, String... types) {
        if(org.apache.commons.lang3.StringUtils.isBlank(s)) return true;

        StringBuffer regx = new StringBuffer();
        if (org.apache.commons.lang3.StringUtils.isNotBlank(s) && org.apache.commons.lang3.StringUtils.isNoneEmpty(types)) {
            for (String p : types) {
                regx.append(PatternConstants.getConstantsFormat(p));

                if(PatternConstants.NOT_SPACE.equals(p)){
                    format = format.replace("\\s", PatternConstants.getConstantsFormat(p));
                }
            }
        }
        return Pattern.matches(String.format(format, regx.toString()), s);
    }
    public static boolean isAllowInput(String s, String... types) {
        return kr.co.homeplus.claim.utils.ValidUtil.isPattern("^[\\s%s]*$", s, types);
    }

    // 허용하지 않는 문자 여부 (존재: false)
    public static boolean isNotAllowInput(String s, String... types) {
        return kr.co.homeplus.claim.utils.ValidUtil.isPattern("[^%s]+", s, types);
    }

    // 줄바꿈 존재여부 (존재 : true)
    public static boolean isEnterStr(final String str) {
        boolean isBool = false;

        if (!org.apache.commons.lang3.StringUtils.isBlank(str)) {
            final Pattern p = Pattern.compile(PatternConstants.ENTER_STR);
            final Matcher m = p.matcher(str);

            isBool = m.find();
        }

        return isBool;
    }

    // 줄바꿈 허용 - 빈거 있는지  (있음 : true)
    public static boolean isEnterNotEmptyStr(final String str) {
        boolean isBool = false;

        if (org.apache.commons.lang3.StringUtils.isNotBlank(str)) {
            // 엑셀 개행문자(줄바꿈) split
            final String[] strs = StringUtils.getExcelEnterSplit(str);

            for (final String s : strs) {
                if (org.apache.commons.lang3.StringUtils.isBlank(s)) {
                    isBool = true;
                }
            }
        }

        return isBool;
    }
}
