package kr.co.homeplus.claim.core.exception;

import kr.co.homeplus.claim.enums.impl.EnumImpl;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

@Data
@EqualsAndHashCode(callSuper = false)
public class LogicException extends Exception {

    private String responseCode;
    private String responseMsg;
    private Object data;

    public LogicException(EnumImpl enumCode){
        this.responseCode = enumCode.getResponseCode();
        this.responseMsg = enumCode.getResponseMessage();
    }

    public LogicException(EnumImpl enumCode, Object data){
        this.responseCode = enumCode.getResponseCode();
        this.responseMsg = enumCode.getResponseMessage();
        this.data = data;
    }

    public LogicException(EnumImpl enumCode, String replaceCode, String replaceStr){
        this.responseCode = enumCode.getResponseCode();
        this.responseMsg = StringUtils.replace(enumCode.getResponseMessage(), replaceCode, replaceStr);
    }

    public LogicException(EnumImpl enumCode, Object data, String returnMessage, boolean isConvert){
        this.responseCode = enumCode.getResponseCode();
        this.responseMsg = (isConvert ? returnMessage : enumCode.getResponseMessage());
        this.data = data;
    }

    public LogicException(String responseCode, String responseMsg) {
        this.responseCode = responseCode;
        this.responseMsg = responseMsg;
    }
}
