package kr.co.homeplus.claim.claim.sql;

import kr.co.homeplus.claim.constants.CustomConstants;
import kr.co.homeplus.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claim.entity.ship.ShippingNonRcvEntity;
import kr.co.homeplus.claim.enums.MysqlFunction;
import kr.co.homeplus.claim.factory.EntityFactory;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipEditDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipInfoSetDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipSetGto;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MyPageShipSql {

    // 미수취 등록
    public static String insertNoRcvShip(NoRcvShipSetGto noRcvShipSetGto){
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class);

        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(shippingNonRcv))
            .INTO_COLUMNS(shippingNonRcv.purchaseOrderNo, shippingNonRcv.bundleNo,
                shippingNonRcv.userNo, shippingNonRcv.partnerId, shippingNonRcv.noRcvDeclrType, shippingNonRcv.noRcvDetailReason, shippingNonRcv.noRcvDeclrDt,
                shippingNonRcv.regId, shippingNonRcv.regDt, shippingNonRcv.chgId, shippingNonRcv.chgDt)
            .INTO_VALUES(
                String.valueOf(noRcvShipSetGto.getPurchaseOrderNo()),
                String.valueOf(noRcvShipSetGto.getBundleNo()),
                String.valueOf(noRcvShipSetGto.getUserNo()),
                SqlUtils.appendSingleQuote(noRcvShipSetGto.getPartnerId()),
                SqlUtils.appendSingleQuote(noRcvShipSetGto.getNoRcvDeclrType()),
                SqlUtils.appendSingleQuote(noRcvShipSetGto.getNoRcvDetailReason()),
                "NOW()",
                SqlUtils.appendSingleQuote(noRcvShipSetGto.getRegId()),
                "NOW()",
                SqlUtils.appendSingleQuote(noRcvShipSetGto.getRegId()),
                "NOW()"
            );

        log.debug("insertNoRcvShip ::: [{}]", query.toString());
        return query.toString();
    }

    // 미수취 처리 업데이트
    public static String updateNoRcvShip(NoRcvShipEditDto editDto) {
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class);

        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(shippingNonRcv))
            .SET(
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessType, editDto.getNoRcvProcessType()),
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessCntnt, editDto.getNoRcvProcessCntnt()),
                SqlUtils.nonSingleQuote(shippingNonRcv.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(shippingNonRcv.chgId, editDto.getChgId())
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingNonRcv.purchaseOrderNo, editDto.getPurchaseOrderNo()),
                SqlUtils.equalColumnByInput(shippingNonRcv.bundleNo, editDto.getBundleNo())
            );

        if(editDto.getNoRcvProcessType().equalsIgnoreCase("W")){
            query.SET(
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "Y"),
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "Y"),
                SqlUtils.nonSingleQuote(shippingNonRcv.noRcvProcessDt, "NOW()")
            );
        } else if(editDto.getNoRcvProcessType().equalsIgnoreCase("R")){
            query.SET(
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "Y"),
                SqlUtils.nonSingleQuote(shippingNonRcv.noRcvProcessDt, "NOW()")
            );
        }

        log.debug("updateNoRcvShip ::: [{}]", query.toString());
        return query.toString();
    }

    public static String selectNoRcvShipInfo(NoRcvShipInfoSetDto infoSetDto) {
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class);
        SQL query = new SQL()
            .SELECT(
                shippingNonRcv.noRcvDeclrType,
                shippingNonRcv.noRcvDetailReason,
                shippingNonRcv.noRcvDeclrDt,
                shippingNonRcv.noRcvProcessDt,
                shippingNonRcv.noRcvProcessType,
                shippingNonRcv.noRcvProcessCntnt,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "N"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "Y"), "'미수취 신고 철회'", "'미수취 신고 처리완료'")
                        ),
                        "'미수취 신고 처리완료'"
                    ),
                    "no_rcv_process_result"
                ),
                SqlUtils.caseWhenElse(
                    SqlUtils.sqlFunction(MysqlFunction.UPPER, shippingNonRcv.regId),
                    ObjectUtils.toArray(
                        "'PARTNER'", shippingNonRcv.partnerId,
                        "'MYPAGE'", shippingNonRcv.userNo
                    ),
                    shippingNonRcv.regId,
                    "no_rcv_reg_id"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.OR,
                            ObjectUtils.toArray(
                                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "Y"),
                                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "Y")
                            )
                        ),
                        SqlUtils.caseWhenElse(
                            SqlUtils.sqlFunction(MysqlFunction.UPPER, shippingNonRcv.regId),
                            ObjectUtils.toArray(
                                "'PARTNER'", shippingNonRcv.partnerId,
                                "'MYPAGE'", shippingNonRcv.userNo
                            ),
                            shippingNonRcv.regId
                        ),
                        "''"
                    ),
                    "no_rcv_chg_id"
                )
            )
            .FROM(EntityFactory.getTableName(shippingNonRcv))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingNonRcv.purchaseOrderNo, infoSetDto.getPurchaseOrderNo()),
                SqlUtils.equalColumnByInput(shippingNonRcv.bundleNo, infoSetDto.getBundleNo()),
                SqlUtils.equalColumnByInput(shippingNonRcv.userNo, infoSetDto.getUserNo())
            );

        log.debug("selectNoRcvShip ::: [{}]", query.toString());
        return query.toString();
    }

    public static String selectNonShipCompleteInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class);
        SQL query = new SQL()
            .SELECT(
                shippingItem.purchaseOrderNo,
                shippingItem.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shipStatus, shippingItem.shipStatus),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_24, SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.completeDt), shippingItem.completeDt)
            )
            .FROM(EntityFactory.getTableName(shippingItem))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo)
            )
            .GROUP_BY(shippingItem.purchaseOrderNo, shippingItem.bundleNo);
        log.debug("selectNonShipCompleteInfo ::: [{}]", query.toString());
        return query.toString();
    }

}
