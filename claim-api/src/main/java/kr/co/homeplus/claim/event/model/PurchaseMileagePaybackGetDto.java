package kr.co.homeplus.claim.event.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "결제금액 누계액에 따른 마일리지 지급")
public class PurchaseMileagePaybackGetDto {
    @ApiModelProperty(value = "유저번호")
    private long userNo;
    @ApiModelProperty(value = "결제금액_리스트")
    private List<PurchasePaymentList> paymentList;
    @ApiModelProperty(value = "결제금액_합계")
    private int purchaseSumAmt;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @ApiModel(description = "결제금액_리스트")
    public static class PurchasePaymentList {
        @ApiModelProperty(value = "주문번호")
        private long purchaseOrderNo;
        @ApiModelProperty(value = "주문일시")
        private String orderDt;
        @ApiModelProperty(value = "주문결제금액")
        private int paymentAmt;
        @ApiModelProperty(value = "클레임금액")
        private int claimAmt;
    }
}
