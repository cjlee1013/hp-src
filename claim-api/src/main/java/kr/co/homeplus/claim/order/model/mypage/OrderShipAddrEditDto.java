package kr.co.homeplus.claim.order.model.mypage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrderShipAddrEditDto {

    @ApiModelProperty(value = "배송번호", position = 1, hidden = true)
    private long bundleNo;

    @ApiModelProperty(value = "주문번호", position = 2)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "멀티번들번호", position = 3)
    private long multiBundleNo;

    @ApiModelProperty(value = "이름", position = 4)
    private String receiverNm;

    @ApiModelProperty(value = "개인통관부호", position = 5)
    private String personalOverseaNo;

    @ApiModelProperty(value = "연락처", position = 6)
    private String shipMobileNo;

    @ApiModelProperty(value = "우편번호", position = 7)
    private String zipCode;

    @ApiModelProperty(value = "도로명", position = 8)
    private String roadBaseAddr;

    @ApiModelProperty(value = "도로명상세", position = 9)
    private String roadDetailAddr;

    @ApiModelProperty(value = "지번", position = 10)
    private String baseAddr;

    @ApiModelProperty(value = "지번상세", position = 11)
    private String detailAddr;

    @ApiModelProperty(value = "배송요청사항코드", position = 12)
    private String shipMsgCd;

    @ApiModelProperty(value = "배송요청사항", position = 13)
    private String shipMsg;

    @ApiModelProperty(value = "배송구분", position = 14)
    private String shipType;

    @ApiModelProperty(value = "새벽배송메시지코드", position = 15)
    private String auroraShipMsgCd;

    @ApiModelProperty(value = "새벽배송메시지", position = 16)
    private String auroraShipMsg;

    @ApiModelProperty(value = "예약배송일", position = 17)
    private String reserveShipDt;

    @ApiModelProperty(value = "선물하기메시지", position = 18)
    private String giftMsg;

    @ApiModelProperty(value = "출입정보저장여부", position = 19)
    private String auroraSaveYn;
}
