package kr.co.homeplus.claim.entity.claim;

public class ClaimStoreInfoEntity {
    // 클레임 점포정보번호 (1,500,000,000 부터 시작)
    public String claimStoreInfoNo;
    // 클레임 번호
    public String claimNo;
    // 클레임 묶음 배송번호
    public String claimBundleNo;
    // 사이트 유형(HOME:흠플러스, CLUB:더클럽)
    public String siteType;
    // 원천점포ID(매출용 실제점포)
    public String originStoreId;
    // 점포ID(주문서에서 넘어온 점포ID)
    public String storeId;
    // 상점유형(HYPER, CLUB, EXP, DS, AURORA)
    public String storeType;
    // 예약상품 여부( Y : 예약상품, N : 일반상품)
    public String reserveYn;
    // 클레임 거래일자(claim_info.payment_complete_dt)
    public String claimTradeDt;
    // 클레임 포스번호
    public String claimPosNo;
    // 클레임 영수증 번호
    public String claimTradeNo;
    // 구매점포정보번호
    public String purchaseStoreInfoNo;
    // 구매 주문번호
    public String purchaseOrderNo;
    // 주문 배송번호
    public String bundleNo;
    // 클레임 타입(C:취소, R:반품, X:교환)
    public String claimType;
    // 클레임 상태(C1: 신청/ C2: 승인/ C3: 완료/ C4: 철회/ C8: 보류/ C9: 거부)
    public String claimStatus;
    // 수거상태(NN: 요청대기 - 자동접수 미지원 택배사인 경우, N0: 수거예약 - 택배사 수동 예약, P0:자동접수요청  (반품자동접수 요청성공), P1: 요청완료-송장등록 (반품자동접수 후 송장번호 받은경우 / 수거요청 처리로 송장등록), P2: 수거중, P3: 수거완료, P8 :수거실패 (반품자동접수실패))
    public String pickStatus;
    // 교환 배송_상태(D0: 상품출고대기 D1: 상품출고(송장등록) D2: 배송중 D3: 배송완료)
    public String exchStatus;
    // 결제 상태(P1: 요청, P2: 진행중, P3: 완료, P4:실패)
    public String paymentStatus;
    // 몰유형(TD:매장상품, DS:업체상품)
    public String mallType;
    // 클레임 발생 취소 배송비, 추가 배송비, 반품배송비, 배송비 할인금액
    public String claimCancelAddShipAmt;
    // 클레임 발생 취소 배송비, 추가 배송비, 반품 배송비,  배송비 할인금액 의 건수
    public String claimCancelAddShipCnt;
    // 클레임 취소 배송비, 추가 배송비, 반품 배송비, 배송비 할인금액  존재여부(Y:유, N:무)
    public String claimPineSendType;
    // 등록일시
    public String regDt;
    // 수정일시
    public String chgDt;
    // 옴니 인터페이스 여부(N:처리대기  I:처리중  Y:처리완료  X:비대상)
    public String omniIfYn;
    // 옴니인터페이스 일시
    public String omniIfDt;
    // PINE 인터페이스 여부(N:처리대기  I:처리중  Y:처리완료  E:에러)
    public String pineIfYn;
    // PINE 인터페이스 일시
    public String pineIfDt;
}
