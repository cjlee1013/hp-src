package kr.co.homeplus.claim.omni.service;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claim.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.claim.claim.service.ClaimProcessService;
import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.enums.ClaimCode;
import kr.co.homeplus.claim.enums.ClaimMessageInfo;
import kr.co.homeplus.claim.enums.ClaimResponseCode;
import kr.co.homeplus.claim.factory.ResponseFactory;
import kr.co.homeplus.claim.omni.model.OmniExpCancelDto;
import kr.co.homeplus.claim.utils.MessageSendUtil;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OmniExpService {

    private final OmniOrderMapperService omniMapperService;
    private final ClaimProcessService processService;

    public ResponseObject<String> setOmniAllCancelProcess(OmniExpCancelDto reqDto) throws Exception {
        log.info("옴니 주문취소 요청 파라미터 :: {}", reqDto);
        // 주문번호 -> refit 주문번호로 변경
        long purchaseOrderNo = this.getConvertOrdNo(reqDto.getOrdNo());
        // 해당 주문번호에 대한 아이템 취득
        LinkedHashMap<String, Long> expOrderInfo = this.getExpOrderInfo(purchaseOrderNo);
        // 취소처리.
        LinkedHashMap<String, Object> claimDataMap = processService.addClaimRegister(this.createClaimRegSetDto(purchaseOrderNo, ObjectUtils.toLong(expOrderInfo.get("bundle_no")), ObjectUtils.toLong(expOrderInfo.get("user_no")), reqDto.getClaimReasonType(), reqDto.getClaimReasonDetail()));

        // 상태 변경 C1 -> C2
        if (!processService.setClaimStatus(ObjectUtils.toLong(claimDataMap.get("claim_no")), ObjectUtils.toLong(claimDataMap.get("claim_bundle_no")), "OMNI", ClaimCode.CLAIM_STATUS_C2)) {
            throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
        }

        /*
         * SETTLE-148 chongmoon.cho
         * 취소신청시 승인 상태까지만 처리하고 클레임배치로 결제취소하도록 로직 변경.
         */
//        if(!paymentCancel(claimDataMap)) {
//            throw new LogicException(ClaimResponseCode.CLAIM_CANCEL_ERR01);
//        }
//
//        processService.callingClaimReg(
//            ObjectUtils.toLong(claimDataMap.get("purchase_order_no")),
//            ObjectUtils.toLong(claimDataMap.get("payment_no")),
//            ObjectUtils.toLong(claimDataMap.get("claim_no"))
//        );
        return ResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_OMNI_CANCEL_SUCCESS);
    }

    public ResponseObject<String> setOmniExpClaimStatusModify(long ordNo) throws Exception {
        log.info("옴니 클레임 승인 요청 ::: 클레임그룹번호 : {}", ordNo);
        long claimNo = omniMapperService.convertOrdNoToClaimNo(ordNo);
        LinkedHashMap<String, Object> claimStatusMap = omniMapperService.getClaimStatusInfo(claimNo);
        if(claimStatusMap != null){
            if(ObjectUtils.isEquals("C1", claimStatusMap.get("claim_status"))){
                if (!setOmniClaimStatus(claimNo, "C2")) {
                    throw new LogicException(ClaimResponseCode.CLAIM_OMNI_ERR32);
                }
            } else {
                throw new LogicException(ClaimResponseCode.CLAIM_OMNI_ERR33);
            }
        }
        return ResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_OMNI_STATUS_CHANGE_SUCCESS);
    }

    private LinkedHashMap<String, Long> getExpOrderInfo(long purchaseOrderNo) throws Exception {
        List<LinkedHashMap<String, Long>> expOrderInfoList = omniMapperService.getExpTotalCancelBundleNo(purchaseOrderNo);
        if(expOrderInfoList.size() == 0) {
            throw new LogicException(ClaimResponseCode.CLAIM_OMNI_ERR31);
        }
        return expOrderInfoList.get(0);
    }

    /**
     * 취소요청 파라미터 생성.
     *
     * @param map 취소기본정보.
     * @return 취소요청파라미터
     */
    public ClaimRegSetDto createClaimRegSetDto(long purchaseOrderNo, long bundleNo, long userNo, String claimReasonType, String claimReasonDetail) throws Exception{
        List<ClaimPreRefundItemList> claimList = omniMapperService.getOrderItemInfoExp(purchaseOrderNo, bundleNo);
        if(claimList == null || claimList.size() == 0){
            throw new LogicException(ClaimResponseCode.CLAIM_OMNI_ERR31);
        }
        return ClaimRegSetDto.builder()
            .purchaseOrderNo(String.valueOf(purchaseOrderNo))
            .bundleNo(String.valueOf(bundleNo))
            .cancelType("O")
            .claimItemList(claimList)
            .orderCancelYn("Y")
            .claimPartYn("N")
            .whoReason("S")
            .claimType("C")
            .claimReasonType(claimReasonType)
            .claimReasonDetail(claimReasonDetail)
            .regId("OMNI")
            .userNo(userNo)
            .isOmni(Boolean.FALSE)
            .isMultiOrder(Boolean.FALSE)
            .build();
    }

    private boolean setOmniClaimStatus(long claimNo, String claimStatus){
        return omniMapperService.omniClaimStatusModify(claimNo, claimStatus);
    }

    private Boolean paymentCancel(LinkedHashMap<String, Object> paymentMap) throws Exception {
        long claimNo = ObjectUtils.toLong(paymentMap.get("claim_no"));
        // 상태 변경 C1 -> C2
        // 상태 변경 C2-> C3
        if (!setOmniClaimStatus(claimNo, "C2")) {
            throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
        }
        // P1 상태로 업데이트
        processService.setClaimPaymentStatus(claimNo, ClaimCode.CLAIM_PAYMENT_STATUS_P1);
        // 취소 처리.
        try{
            if (!processService.paymentCancelProcess(claimNo)) {
                log.error("클레임 결제 취소 중 오류가 발생됨. [{}]", claimNo);
                throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR02);
            }
        } catch (Exception e) {
            log.error("클레임 결제 취소 중 오류가 발생됨. [{} :: {}]", claimNo, e.getMessage());
            return Boolean.FALSE;
        }

        // 상태 변경 C2-> C3
        if (!setOmniClaimStatus(claimNo, "C3")) {
            throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
        }

        processService.callingClaimReg(claimNo);
        // 쿠폰 깨진거
        Boolean isCouponAndSlot = processService.isClaimCouponAndSlotProcess(claimNo);

        MessageSendUtil.sendOmniOutOfStock(claimNo, ClaimMessageInfo.CLAIM_MESSAGE_EXP_C_ORDER_COMPLETE);

        return Boolean.TRUE;
    }

    private Long getConvertOrdNo(long ordNo) throws Exception {
        try{
            return omniMapperService.convertOrdNoToPurchaseOrderNo(ordNo);
        } catch (Exception e) {
            log.error("옴니 Exp 주문번호({}) refit 주문번호 변경시 오류 :: {}", ordNo, e.getMessage());
            throw new LogicException(ClaimResponseCode.CLAIM_OMNI_ERR10);
        }
    }
}
