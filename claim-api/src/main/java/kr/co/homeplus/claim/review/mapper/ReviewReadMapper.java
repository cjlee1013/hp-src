package kr.co.homeplus.claim.review.mapper;

import java.util.List;
import kr.co.homeplus.claim.core.db.annotation.SecondaryConnection;
import kr.co.homeplus.claim.review.model.ReviewItemDto;
import kr.co.homeplus.claim.review.model.ReviewSearchGetDto;
import kr.co.homeplus.claim.review.model.ReviewSearchSetDto;
import kr.co.homeplus.claim.review.sql.ReviewSearchSql;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

@SecondaryConnection
public interface ReviewReadMapper {

    @SelectProvider(value = ReviewSearchSql.class, method = "selectReviewPossibleList")
    List<ReviewSearchGetDto> selectReviewPossibleList(ReviewSearchSetDto setDto);
    @SelectProvider(value = ReviewSearchSql.class, method = "selectReviewPossibleItemList")
    List<ReviewItemDto> selectReviewPossibleItemList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);
    @SelectProvider(value = ReviewSearchSql.class, method = "selectGhsReviewPossibleItemList")
    List<ReviewItemDto> selectGhsReviewPossibleItemList(@Param("ordNo") long ordNo);
}
