package kr.co.homeplus.claim.claim.model.register;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "클레임등록 DTO")
public class ClaimMstRegDto {
    @ApiModelProperty(value= "클레임번호(환불:클레임번호)")
    private long claimNo;

    @ApiModelProperty(value= "결제번호")
    private long paymentNo;

    @ApiModelProperty(value= "주문번호")
    private long purchaseOrderNo;

    @ApiModelProperty(value= "사이트유형(HOME:홈플러스,CLUB:더클럽)")
    private String siteType;

    @ApiModelProperty(value= "완료 금액(전체환불금액)")
    private long completeAmt;

    @ApiModelProperty(value= "총상품금액")
    private long totItemPrice;

    @ApiModelProperty(value= "총배송가격")
    private long totShipPrice;

    @ApiModelProperty(value= "총도서산간배송가격")
    private long totIslandShipPrice;

    @ApiModelProperty(value= "추가배송비")
    private long totAddShipPrice;

    @ApiModelProperty(value= "추가도서산간배송비")
    private long totAddIslandShipPrice;

    @ApiModelProperty(value= "총할인금액")
    private long totDiscountAmt;

    @ApiModelProperty(value= "총상품할인금액")
    private long totItemDiscountAmt;

    @ApiModelProperty(value= "총배송비할인금액")
    private long totShipDiscountAmt;

    @ApiModelProperty(value= "총 임직원할인금액")
    private long totEmpDiscountAmt;

    @ApiModelProperty(value= "총장바구니할인금액")
    private long totCartDiscountAmt;

    @ApiModelProperty(value= "총행사 할인 금액")
    private long totPromoDiscountAmt;

    @ApiModelProperty(value= "총카드할인 금액")
    private long totCardDiscountAmt;

    @ApiModelProperty(value= "총TD중복쿠폰 금액")
    private long totAddTdDiscountAmt;

    @ApiModelProperty(value= "총DS중복쿠폰 금액")
    private long totAddDsDiscountAmt;

    @ApiModelProperty(value= "PG결제금액")
    private long pgAmt;

    @ApiModelProperty(value= "마일리지결제금액")
    private long mileageAmt;

    @ApiModelProperty(value= "MHC결제금액")
    private long mhcAmt;

    @ApiModelProperty(value= "DGV결제금액")
    private long dgvAmt;

    @ApiModelProperty(value= "OCB결제금액")
    private long ocbAmt;

    @ApiModelProperty(value= "클레임 귀책자(B:Buyer, S:Seller, H:Homeplus)")
    private String whoReason;

    @ApiModelProperty(value= "고객번호")
    private String userNo;

    @ApiModelProperty(value= "부분취소여부")
    private String claimPartYn;

    @ApiModelProperty(value= "임직원번호")
    private String empNo;
    @ApiModelProperty(value= "임직원할인한도잔액")
    private Integer empRemainAmt;
    @ApiModelProperty(value= "대체여부")
    private String oosYn;
    @ApiModelProperty(value= "회원등급번호")
    private String gradeSeq;
    @ApiModelProperty(value= "마켓타입")
    private String marketType;
    @ApiModelProperty(value= "마켓주문번호")
    private String marketOrderNo;

    public void putMap(HashMap<String, Object> map){
        this.completeAmt = Long.parseLong(map.getOrDefault("completeAmt", 0).toString());
        this.totItemPrice = Long.parseLong(map.getOrDefault("totItemPrice", 0).toString());
        this.totShipPrice = Long.parseLong(map.getOrDefault("totShipPrice", 0).toString());
        this.totIslandShipPrice = Long.parseLong(map.getOrDefault("totIslandShipPrice", 0).toString());
        this.totAddShipPrice = Long.parseLong(map.getOrDefault("totAddShipPrice", 0).toString());
        this.totAddIslandShipPrice = Long.parseLong(map.getOrDefault("totAddIslandShipPrice", 0).toString());
        this.totDiscountAmt = Long.parseLong(map.getOrDefault("totDiscountAmt", 0).toString());
        this.totItemDiscountAmt = Long.parseLong(map.getOrDefault("totItemDiscountAmt", 0).toString());
        this.totShipDiscountAmt = Long.parseLong(map.getOrDefault("totShipDiscountAmt", 0).toString());
        this.totCartDiscountAmt = Long.parseLong(map.getOrDefault("totCartDiscountAmt", 0).toString());
        this.totEmpDiscountAmt = Long.parseLong(map.getOrDefault("totEmpDiscountAmt", 0).toString());
        this.totPromoDiscountAmt = Long.parseLong(map.getOrDefault("totPromoDiscountAmt", 0).toString());
        this.totCardDiscountAmt = Long.parseLong(map.getOrDefault("totCardDiscountAmt", 0).toString());
        this.totAddDsDiscountAmt = Long.parseLong(map.getOrDefault("totAddDsDiscountAmt", 0).toString());
        this.totAddTdDiscountAmt = Long.parseLong(map.getOrDefault("totAddTdDiscountAmt", 0).toString());
        this.pgAmt = Long.parseLong(map.getOrDefault("pgAmt", 0).toString());
        this.mileageAmt = Long.parseLong(map.getOrDefault("mileageAmt", 0).toString());
        this.mhcAmt = Long.parseLong(map.getOrDefault("mhcAmt", 0).toString());
        this.dgvAmt = Long.parseLong(map.getOrDefault("dgvAmt", 0).toString());
        this.ocbAmt = Long.parseLong(map.getOrDefault("ocbAmt", 0).toString());
    }
}
