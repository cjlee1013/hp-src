package kr.co.homeplus.claim.enums;

import kr.co.homeplus.claim.enums.impl.EnumImpl;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimCode {

    CLAIM_STATUS_C1("C1", "신청", "신청"),
    CLAIM_STATUS_C2("C2", "승인", "승인"),
    CLAIM_STATUS_C3("C3", "완료", "완료"),
    CLAIM_STATUS_C4("C4", "철회", "철회"),
    CLAIM_STATUS_C8("C8", "보류", "보류"),
    CLAIM_STATUS_C9("C9", "거부", "거부"),

    CLAIM_PAYMENT_STATUS_P0("P0", "대기", "대기"),
    CLAIM_PAYMENT_STATUS_P1("P1", "요청", "요청"),
    CLAIM_PAYMENT_STATUS_P2("P2", "진행중", "진행중"),
    CLAIM_PAYMENT_STATUS_P3("P3", "완료", "완료"),
    CLAIM_PAYMENT_STATUS_P4("P4", "실패", "실패"),

    CLAIM_CONVERT_PAYMENT_TYPE_PG("PG", "PG", "PG"),
    CLAIM_CONVERT_PAYMENT_TYPE_MHC("MP", "MHC", "MHC"),
    CLAIM_CONVERT_PAYMENT_TYPE_DGV("DG", "DGV", "DGV"),
    CLAIM_CONVERT_PAYMENT_TYPE_OCB("OC", "OCB", "OCB"),
    CLAIM_CONVERT_PAYMENT_TYPE_MILEAGE("PP", "MILEAGE", "MILEAGE"),
    CLAIM_CONVERT_PAYMENT_TYPE_FREE("FR", "FREE", "FREE"),

    PAYMENT_TYPE_CARD("CARD", "신용카드", "신용카드"),
    PAYMENT_TYPE_VBANK("VBANK", "무통장", "무통장"),
    PAYMENT_TYPE_RBANK("RBANK", "실시간계좌이체", "실시간계좌이체"),
    PAYMENT_TYPE_PHONE("PHONE", "휴대폰", "휴대폰"),
    PAYMENT_TYPE_KAKAOPAY("KAKAOPAY", "카카오카드", "카카오카드"),
    PAYMENT_TYPE_KAKAOMONEY("KAKAOMONEY", "카카오머니", "카카오머니"),
    PAYMENT_TYPE_NAVERPAY("NAVERPAY", "네이버페이", "네이버페이"),
    PAYMENT_TYPE_SAMSUNGPAY("SAMSUNGPAY", "삼성페이", "삼성페이"),
    PAYMENT_TYPE_PAYCO("PAYCO", "페이코", "페이코"),
    PAYMENT_TYPE_MILEAGE("MILEAGE", "마일리지", "마일리지"),
    PAYMENT_TYPE_MHC("MHC", "MHC포인트", "마이홈플러스포인트"),
    PAYMENT_TYPE_DGV("DGV", "홈플러스상품권", "디지털/모바일상품권"),
    PAYMENT_TYPE_OCB("OCB", "OkCashBack", "OK캐쉬백포인트"),
    PAYMENT_TYPE_FREE("FREE", "0원결제", "0원결제"),
    PAYMENT_TYPE_NONE("NONE", "결제수단없음", "결제수단없음"),

    DISCOUNT_KIND_ITEM("item_discount", "상품할인", "상품할인"),
    DISCOUNT_KIND_AFFI("affi_discount", "제휴상품할인", "제휴상품할인"),
    DISCOUNT_KIND_CARD("card_discount", "카드상품할인", "카드상품할인"),
    DISCOUNT_KIND_ITEM_COUPON("item_coupon", "상품쿠폰", "상품쿠폰"),
    DISCOUNT_KIND_SHIP_COUPON("ship_coupon", "배송비쿠폰", "배송비쿠폰"),
    DISCOUNT_KIND_CART_COUPON("cart_coupon", "장바구니쿠폰", "장바구니쿠폰"),

    CLAIM_ACCUMULATE_REQ_SEND_N("N", "미전송", "미전송"),
    CLAIM_ACCUMULATE_REQ_SEND_I("I", "요청중", "요청중"),
    CLAIM_ACCUMULATE_REQ_SEND_Y("Y", "전송성공", "전송성공"),
    CLAIM_ACCUMULATE_REQ_SEND_E("E", "전송실패", "전송실패"),
    ;

    @Getter
    private final String code;

    @Getter
    private final String description;

    @Getter
    private final String externalNm;

}
