package kr.co.homeplus.claim.ship.sql;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShipSearchSql {

    public String selectShipMsg() {
        StringBuilder query = new StringBuilder();
        query.append("SELECT mc_cd as 'ship_msg_cd', mc_nm as 'ship_msg', ref_1 as 'store_kind' FROM dms.itm_mng_code WHERE gmc_cd = 'ship_msg' and use_yn = 'Y' order by store_kind, priority");
        log.debug("selectShipMsg Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public String selectDlvCd() {
        StringBuilder query = new StringBuilder();
        query.append("select mc_cd as 'dlv_cd', mc_nm as 'dlv_cd_nm' from dms.itm_mng_code where gmc_cd = 'dlv_cd'");
        log.debug("selectDlvCd Query :: \n [{}]", query.toString());
        return query.toString();
    }
}
