package kr.co.homeplus.claim.ticket.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "티켓 히스토리 등록 파라미터")
public class OrderTicketHistorySetDto {

    @ApiModelProperty(value = "주문티켓번호", position = 1)
    public String orderTicketNo;

    @ApiModelProperty(value = "이력상세", position = 2)
    public String historyDetail;

    @ApiModelProperty(value = "등록자", position = 3)
    public String regId;

}
