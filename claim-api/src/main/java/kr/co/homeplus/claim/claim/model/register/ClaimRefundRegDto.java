package kr.co.homeplus.claim.claim.model.register;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "클레임 환불수단변경 등록 DTO")
public class ClaimRefundRegDto {

    @ApiModelProperty(value= "클레임번호(환불:클레임번호)")
    public long claimNo;

    @ApiModelProperty(value= "결제번호")
    public long paymentNo;

    @ApiModelProperty(value= "주문번호")
    public long purchaseOrderNo;

    @ApiModelProperty(value= "환불계좌 예금주명")
    public String bankAccountName;

    @ApiModelProperty(value= "환불계좌번호")
    public String bankAccountNo;

    @ApiModelProperty(value= "은행코드")
    public String bankCode;

    @ApiModelProperty(value= "결제 상태(환불상태 F1:요청, F2:진행중(주결제 환불완료), F3:완료, F8: 실패 (주결제 환불실패), F9:실패 (포인트 환불실패)")
    public String refundStatus;

    @ApiModelProperty(value= "거래유형(1:미설정, 2:위탁, 3:직매입(점포)")
    public String transType;

    @ApiModelProperty(value= "환불 신청 금액")
    public long refundAmt;

    @ApiModelProperty(value= "클레임 차감금액 - 배송비/쿠폰비/즉시할인")
    public String additionAmt;

    @ApiModelProperty(value= "클레임 차감금액 - 공제금")
    public long deductAmt;

    @ApiModelProperty(value= "주결제수단 환불금액")
    public long pgAmt;

    @ApiModelProperty(value= "마일리지 환불금액")
    public long mileageAmt;

    @ApiModelProperty(value= "MHC 포인트 환불금액")
    public long mhcAmt;

    @ApiModelProperty(value= "DGV 상품권 환불금액")
    public long dgvAmt;

    @ApiModelProperty(value= "OCB 포인트 환불금액")
    public long ocbAmt;

    @ApiModelProperty(value= "기타 결제 환불금액(추가 결제수단 대비용)")
    public long etcAmt;

    @ApiModelProperty(value= "등록일")
    public String regDt;

    @ApiModelProperty(value= "등록아이디")
    public String regId;
}
