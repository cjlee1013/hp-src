package kr.co.homeplus.claim.order.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListSetDto;
import kr.co.homeplus.claim.order.model.order.MypageOrderListGetDto;
import kr.co.homeplus.claim.order.service.MpOrderRefactoringService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/mp/order/")
@Api(tags = "마이페이지 > 주문/배송(Re-Factoring Version) ")
public class MpOrderRefactoringController {

    private final MpOrderRefactoringService orderService;

    @PostMapping("/getNewOrderList")
    @ApiOperation(value = "주문내역조회", response = MypageOrderListGetDto.class)
    public ResponseObject<List<MypageOrderListGetDto>> getMyPageOrderList(@RequestBody MpOrderListSetDto setDto) throws Exception {
        return orderService.getMyPageOrderList(setDto);
    }
}
