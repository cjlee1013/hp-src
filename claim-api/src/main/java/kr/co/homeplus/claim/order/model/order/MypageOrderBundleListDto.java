package kr.co.homeplus.claim.order.model.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPresentDto;
import lombok.Data;

@Data
@ApiModel(description = "마이페이지 > 주문/배송 - 주문리스트(bundleList)")
public class MypageOrderBundleListDto {
    @ApiModelProperty(value = "배송번호", position = 1)
    private long bundleNo;
    @ApiModelProperty(value = "상점타입", position = 2)
    private String storeType;
    @ApiModelProperty(value = "몰유형", position = 3)
    private String mallType;
    @ApiModelProperty(value = "판매업체ID", position = 4)
    private String partnerId;
    @ApiModelProperty(value = "판매업명", position = 5)
    private String partnerNm;
    @ApiModelProperty(value = "배송타입 (TD,DS,AURORA,PICK,RESERVE,MULTI,SUM)", position = 6)
    private String shipType;
    @ApiModelProperty(value = "배송정보", position = 7)
    private MpOrderBundleShipInfoDto shipInfo;
    @ApiModelProperty(value = "주문상품리스트(대체포함)", position = 8)
    private List<MpOrderItemDto> itemList;
    @ApiModelProperty(value = "합배송주문상품리스트(대체포함)", position = 9)
    private List<MpOrderItemDto> combineList;
    @ApiModelProperty(value = "선물하기정보", position = 10)
    private MpOrderPresentDto presentInfo;
    @ApiModelProperty(value = "클레임정보", position = 11)
    private OrderClaimInfoDto claimInfo;
    @ApiModelProperty(value = "합배송클레임정보", position = 12)
    private OrderClaimInfoDto combineClaimInfo;
    @ApiModelProperty(value = "버튼정보", position = 13)
    private OrderButtonDto buttonInfo;
    @ApiModelProperty(value = "합배송버튼정보", position = 14)
    private OrderButtonDto combineButtonInfo;
}
