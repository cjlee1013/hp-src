package kr.co.homeplus.claim.omni.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claim.claim.model.register.ClaimRegDetailDto;
import kr.co.homeplus.claim.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.claim.claim.service.ClaimCommonService;
import kr.co.homeplus.claim.claim.service.ClaimMapperService;
import kr.co.homeplus.claim.claim.service.ClaimProcessService;
import kr.co.homeplus.claim.claim.service.ExternalService;
import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.enums.ClaimResponseCode;
import kr.co.homeplus.claim.enums.ResponseCode;
import kr.co.homeplus.claim.factory.ResponseFactory;
import kr.co.homeplus.claim.omni.model.OmniClaimSetDto;
import kr.co.homeplus.claim.omni.model.OmniOrderSetDto;
import kr.co.homeplus.claim.omni.model.OmniOriginOrderDto;
import kr.co.homeplus.claim.omni.model.order.OmniSubstitutionCompleteDto;
import kr.co.homeplus.claim.omni.model.order.OmniSubstitutionGetDto;
import kr.co.homeplus.claim.omni.model.order.OmniSubstitutionSetDto;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class OmniClaimService {
    private final OmniOrderMapperService omniMapperService;
    private final ClaimProcessService processService;

    public ResponseObject<String> setOmniClaimProcess(List<OmniClaimSetDto> omniClaimList) throws Exception {
        log.info("옴니 환불/교환 요청 파라미터 :::: \n[{}]", omniClaimList);
        // ordNo를 purchaseOrderNo 로 변경한다.
        this.convertOrdNoToClaim(omniClaimList);
        // 그룹 재처리
        LinkedHashMap<Long, List<OmniClaimSetDto>> claimListMap = createOmniClaimData(omniClaimList);

        if(claimListMap.size() == 0){
            return ResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_OMNI_ERR01);
        }

        for(Long ordNo : claimListMap.keySet()){
            List<OmniClaimSetDto> ordNoOrderList = new ArrayList<>(claimListMap.get(ordNo));
            boolean isRefund = ordNoOrderList.stream().allMatch(dto -> dto.getRefundYn().equals("Y"));
            boolean isExchange = ordNoOrderList.stream().allMatch(dto -> dto.getRefundYn().equals("N"));

            if(!isExchange && !isRefund){
                return ResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_OMNI_ERR11);
            }

            if(ordNoOrderList.size() > 0){
                if(ordNoOrderList.stream().anyMatch(dto -> StringUtils.isEmpty(dto.getGoodId()))){
                    throw new LogicException(ClaimResponseCode.CLAIM_OMNI_ERR12);
                }
                if(ordNoOrderList.stream().anyMatch(dto -> dto.getClaimQty() == 0)){
                    throw new LogicException(ClaimResponseCode.CLAIM_OMNI_ERR13);
                }
                if(ordNoOrderList.stream().anyMatch(dto -> StringUtils.isEmpty(dto.getClaimCd()))){
                    throw new LogicException(ClaimResponseCode.CLAIM_OMNI_ERR14);
                }
            }
            log.info("주문번호 :: {} 에 대한 {} 처리.", ordNo, (isRefund ? "반품" : "교환"));
            // 반품/교환 정보
            ClaimRegSetDto regSetDto = getClaimPreRefundItemList(ordNoOrderList, isExchange);
            log.info("주문번호 :: {} | {} 에 대한 클레임등록요청 정보 : {}", ordNo, (isRefund ? "반품" : "교환"), regSetDto);
            if(regSetDto != null){
                LinkedHashMap<String, Object> omniOrderMap = processService.addClaimRegister(regSetDto);
                log.info("{} 에 대한 등록 결과 :  {}", (isRefund ? "반품" : "교환"), omniOrderMap);
            } else {
                throw new LogicException("9998", "알수 없는 오류가 발생되었습니다.");
            }
        }
        // 환불예정금액 조회
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS);
    }

    private ClaimRegSetDto getClaimPreRefundItemList (List<OmniClaimSetDto> omniClaimSetDto, Boolean isExchange) throws Exception {
        // 주문번호
        Long ordNo = omniClaimSetDto.stream().map(OmniClaimSetDto::getOrdNo).distinct().collect(Collectors.toList()).get(0);
        log.info("주문번호 :: {} | {} 처리 파라미터 : {}", ordNo, (isExchange ? "교환" : "반품"), omniClaimSetDto);
        // 주문에 따른 원주문 상품정보 취득
        List<OmniOriginOrderDto> originOrderList = omniMapperService.getOmniOriginOrderList(ordNo);
        log.info("주문번호 :: {} | {} 처리 원거래정보 : {}", ordNo, (isExchange ? "교환" : "반품"), originOrderList);
        if(originOrderList == null){
            throw new LogicException(ResponseCode.CLAIM_SUBSTITUTION_ERR03);
        }

        String bundleNo = originOrderList.stream().map(OmniOriginOrderDto::getBundleNo).distinct().collect(Collectors.toList()).get(0);
        String userNo = originOrderList.stream().map(OmniOriginOrderDto::getUserNo).distinct().collect(Collectors.toList()).get(0);
        String claimCd = omniClaimSetDto.stream().map(OmniClaimSetDto::getClaimCd).distinct().collect(Collectors.toList()).get(0);

        LinkedHashMap<String, Object> shipPresentMap = omniMapperService.getOmniShipPresentCheck(ordNo, ObjectUtils.toLong(bundleNo));

        //배송상태 확인.
        if("D1,D2".contains(ObjectUtils.toString(shipPresentMap.get("ship_status")))){
            String purchaseStoreInfoNo = ObjectUtils.toString(shipPresentMap.get("purchase_store_info_no"));
            if(StringUtils.isNotEmpty(purchaseStoreInfoNo)){
                processService.setClaimShipStart(purchaseStoreInfoNo);
            } else {
                throw new LogicException(ClaimResponseCode.CLAIM_OMNI_ERR16);
            }
        }

        LinkedHashMap<String, String> claimTypeMap = omniMapperService.getClaimReasonType(claimCd);
        if(claimTypeMap == null || claimTypeMap.size() == 0){
            throw new LogicException(ClaimResponseCode.CLAIM_OMNI_ERR15);
        }
        String whoReason = claimTypeMap.get("ref_2");

            // 환불예정금액 상품 리스트 생성.
        List<ClaimPreRefundItemList> refundItemList = new ArrayList<>();
        // 환불예정금 반품/교환 데이터 조회시
        // 반품 :
        for(OmniClaimSetDto dto : omniClaimSetDto) {
            int checkPoint = 0;
            if(originOrderList.stream().noneMatch(originList -> originList.getItemNo().equals(dto.getGoodId()))){
                throw new LogicException(ResponseCode.CLAIM_SUBSTITUTION_ERR08, "itemNo", dto.getGoodId());
            }
            if(omniClaimSetDto.stream().filter(crl -> crl.getGoodId().equals(dto.getGoodId())).count() > 1){
                throw new LogicException(ResponseCode.CLAIM_SUBSTITUTION_ERR09, "itemNo", dto.getGoodId());
            }
            for(OmniOriginOrderDto originDto : originOrderList.stream().filter(originDto -> originDto.getItemNo().equals(dto.getGoodId())).collect(Collectors.toList())){
                int itemQty = Integer.parseInt(originDto.getItemQty());
                int claimItemQty = Integer.parseInt(originDto.getClaimItemQty());
                int optTotalQty = originOrderList.stream().filter(optDto -> optDto.getItemNo().equals(dto.getGoodId())).map(OmniOriginOrderDto::getItemQty).mapToInt(Integer::parseInt).sum();
                log.info("[ordNo :: {} 에 대한 {} 환불예정금액 조회 originDto :: {} || checkPoint :: {} - itemQty :: {} ||  claimItemQty :: {}]", ordNo, (isExchange ? "교환" : "반품"), originDto, checkPoint, itemQty, claimItemQty);
                if((optTotalQty - (dto.getClaimQty() + claimItemQty)) < 0){
                    throw new LogicException(ResponseCode.CLAIM_SUBSTITUTION_ERR07, "itemNo", originDto.getItemNo());
                }
                if(dto.getClaimQty() > checkPoint){
                    if((checkPoint + itemQty) > dto.getClaimQty()){
                        itemQty = dto.getClaimQty() - checkPoint;
                    }
                    refundItemList.add(ClaimPreRefundItemList.builder().itemNo(dto.getGoodId()).orderOptNo(originDto.getOrderOptNo()).claimQty(String.valueOf(itemQty)).orderItemNo(originDto.getOrderItemNo()).build());
                    checkPoint += itemQty;
                } else {
                    break;
                }
            }
        }

        return ClaimRegSetDto.builder()
            .purchaseOrderNo(String.valueOf(ordNo))
            .bundleNo(bundleNo)
            .cancelType(isExchange ? "X" : "R")
            .claimItemList(refundItemList)
            .claimDetail(createClaimReqDetailInfo(ordNo, Long.parseLong(bundleNo)))
            .orderCancelYn("N")
            .claimPartYn("N")
            .claimReasonType(claimCd)
            .claimReasonDetail("배송기사접수")
            .whoReason(whoReason)
            .claimType(isExchange ? "X" : "R")
            .regId("OMNI")
            .userNo(Long.parseLong(userNo))
            .isOmni(Boolean.FALSE)
            .isMultiOrder(Boolean.FALSE)
            .build();
    }


    private LinkedHashMap<Long, List<OmniClaimSetDto>> createOmniClaimData(List<OmniClaimSetDto> setDto){
        List<Long> ordNoList = setDto.stream().map(OmniClaimSetDto::getOrdNo).distinct().collect(Collectors.toList());
        LinkedHashMap<Long, List<OmniClaimSetDto>> orderList = new LinkedHashMap<>();
        for (Long ordNo : ordNoList) {
            orderList.put(ordNo, setDto.stream().filter(dto -> dto.getOrdNo().equals(ordNo)).collect(Collectors.toList()));
        }
        return orderList;
    }

    private List<OmniClaimSetDto> convertOrdNoToClaim(List<OmniClaimSetDto> omniClaimList) throws Exception {
        try{
            for(OmniClaimSetDto dto : omniClaimList){
                dto.setOrdNo(omniMapperService.convertOrdNoToPurchaseOrderNo(dto.getOrdNo()));
            }
            return omniClaimList;
        } catch (Exception e) {
            log.error("옴니 환불/교환 클레임번호({}) refit 클레임그룹번호 변경시 오류 :: {}", omniClaimList.get(0).getOrdNo(), e.getMessage());
            throw new LogicException(ClaimResponseCode.CLAIM_OMNI_ERR10);
        }
    }

    private ClaimRegDetailDto createClaimReqDetailInfo(long purchaseOrderNo, long bundleNo) {
        // 주문의 배송정보 조회
        // shipDt,  slotId, shiftId,
        ClaimRegDetailDto claimShipInfo =  omniMapperService.getClaimShipInfoByOmni(purchaseOrderNo, bundleNo);
        claimShipInfo.setIsEnclose("Y");
        claimShipInfo.setIsPick("Y");
        claimShipInfo.setIsPickReq("Y");
        claimShipInfo.setPickRegId("OMNI");
        return claimShipInfo;
    }

}
