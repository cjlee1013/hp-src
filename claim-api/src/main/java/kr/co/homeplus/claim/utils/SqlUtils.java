package kr.co.homeplus.claim.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import kr.co.homeplus.claim.constants.Constants;
import kr.co.homeplus.claim.constants.CustomConstants;
import kr.co.homeplus.claim.constants.SqlPatternConstants;
import kr.co.homeplus.claim.enums.MysqlFunction;
import kr.co.homeplus.claim.factory.EntityFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.util.ReflectionUtils;

@Slf4j
public class SqlUtils {

    /**
     * SQL 생성용.
     * String[]의 underscore 문자열을 camelCase 로 변경
     *
     * @param parameters 컬럼배열 (String[])
     * @return String[]
     */
    public static String[] covertParamToCamelCase(String... parameters){
        List<String> resultList = new ArrayList<>();
        for(String param : parameters){
            resultList.add(convertUnderscoreToCamelCase(param));
        }
        return resultList.toArray(String[]::new);
    }

    /**
     * SQL 생성용!
     * String[]의 underscore 문자열을 camelCase 변환 후
     * insert 에 맞게 수정.(dateType -> now());
     *
     * @param parameters 컬럼배열 (String[])
     * @return String[]
     */
    public static String[] convertInsertParamToCamelCase(String... parameters){
        List<String> insertParamList = new ArrayList<>();
        for(String param : covertParamToCamelCase(parameters)){
            if(param.contains("regDt") || param.contains("chgDt")){
                insertParamList.add("now()");
            } else {
                insertParamList.add(param);
            }
        }
        return insertParamList.toArray(String[]::new);
    }

    public static String[] convertInsertValue(String... parameters){
        List<String> insertParamList = new ArrayList<>();
        for(String param : parameters){
            if(param.contains("reg_dt") || param.contains("chg_dt")){
                insertParamList.add("now()");
            } else {
                insertParamList.add(convertColumnToMapper(param));
            }
        }
        return insertParamList.toArray(String[]::new);
    }

    public static String[] convertInsertIntoValue(Object... values){
        List<String> insertParamList = new ArrayList<>();
        for(Object object : values){
            if(object instanceof String){
                if(!String.valueOf(object).startsWith("(") &&
                    !String.valueOf(object).startsWith("#")){
                    insertParamList.add(appendSingleQuote(object));
                } else {
                    insertParamList.add(String.valueOf(object));
                }
            } else {
                insertParamList.add(String.valueOf(object));
            }
        }
        return insertParamList.toArray(String[]::new);
    }

    /**
     * SQL 생성용!
     * 단일컬럼에 대한 WHERE or And or OR 에 필요한 쿼리 생성.
     *
     * @param column 변환하고자하는 컬럼명
     * @return String 쿼리문(예 : user_id -> user_id = ${userId}
     */
    public static String equalColumnMapping(String column){
        StringBuilder condition = new StringBuilder();
        condition.append(column);
        condition.append(wrapWithSpace(Constants.EQUAL));
        if(isSnakeCase(column)){
            condition.append(convertUnderscoreToCamelCase(column));
        }else{
            condition.append(convertColumnToMapper(column));
        }
        return condition.toString();
    }

    public static String equalColumn(String leftColumn, String rightColumn) {
        return leftColumn + wrapWithSpace(Constants.EQUAL) + rightColumn;
    }

    /**
     * SQL 생성용
     * 입력받은 Value 의 타입에 따라 Quote 여부를 결정하여 리턴.
     * @param column 조건 컬럼
     * @param value 입력값
     * @return String 입력값에 따른 quote 조건식( column = value or column = 'value' )
     */
    public static String equalColumnByInput(String column, Object value){
        if(value instanceof String){
            return singleQuote(column, value);
        } else {
            return nonSingleQuote(column, value);
        }
    }

    /**
     * SQL 생성용!
     * 직접입력용 쿼리 생성.
     *
     * @param column 사용하고자하는 DB 컬럼
     * @param value 사용하고자하는 데이터 값
     * @return String
     */
    public static String nonSingleQuote(String column, Object value){
        return column
            + wrapWithSpace(Constants.EQUAL)
            + value;
    }

    public static String equalConditionByInputArrayWithAlias(String aliasColumn, String[] aliasArray){
        return aliasColumn
            + wrapWithSpace(Constants.EQUAL)
            + Arrays.stream(aliasArray)
            .filter(str -> str.contains(nonAlias(aliasColumn)))
            .collect(Collectors.joining());
    }

    /**
     * SQL 생성용!
     * 직접입력용 쿼리 생성.(Single Quote)
     *
     * @param column 사용하고자하는 DB 컬럼
     * @param value 사용하고자하는 데이터 값
     * @return String
     */
    public static String singleQuote(String column, Object value){
        return column
            + wrapWithSpace(Constants.EQUAL)
            + appendSingleQuote(value);
    }

    /**
     * SQL 생성용.
     * 특정 컬럼과 instance 컬럼의 동일 값을 찾아
     * Equal Condition 을 생성함.
     *
     * @param baseColumn 찾고자하는 컬럼.
     * @param instance 매치하고자하는 컬럼의 instance
     * @return String baseColumn = instance.baseColumn
     */
    public static String equalConditionClassCompare(String baseColumn, Object instance){
        return baseColumn
            + wrapWithSpace(Constants.EQUAL)
            + (String) EntityFactory.getFieldValue(baseColumn, instance);
    }

    /**
     * SQL 생성용.
     * Value 에 SingleQuote 를 Append.
     * @param value 입력값
     * @return String SingleQuote 가 추가된 Value.
     */
    public static String appendSingleQuote(Object value){
        if(value == null){
            return String.valueOf((Object) null);
        }
        if(value instanceof String) {
            if(String.valueOf(value).contains("'")){
                value = String.valueOf(value).replaceAll("'", "\\\\".concat("'"));
            }
        }
        return CustomConstants.SINGLE_QUOTE
            + value
            + CustomConstants.SINGLE_QUOTE;
    }

    /**
     * SQL 생성용!
     * 특정 데이터를 지정해서 검색해야하는 경우 사용한다.
     *
     * @param value 입력 데이터
     * @param alias Alias 명.
     * @return String '입력 데이터' AS alias
     */
    public static String appendSingleQuoteWithAlias(Object value, String alias){
        return aliasToRow(appendSingleQuote(value), nonAlias(alias));
    }

    /**
     * SQL 생성용
     * underscore 로 이뤄진 Str 를
     * CamelCase 로 변환한다.
     *
     * @param column 사용하고자하는 컬럼명
     * @return String
     */
    public static String convertUnderscoreToCamelCase(String column){
        //컬럼명에 Alias 가 있는지 확인.
        //있으면 Alias 제거 후 컬럼명 리턴.
        //없으면 기존 컬럼명 리턴
        return convertColumnToMapper(snakeCaseToCamelCase(nonAlias(column)));
    }

    public static String convertColumnToMapper(String column){
        StringBuilder whereCondition = new StringBuilder();
        whereCondition.append(nonAlias(column));
        whereCondition.append(CustomConstants.CURLY_BRACKET_END);
        whereCondition.insert(0, "#".concat(CustomConstants.CURLY_BRACKET_START));
        return whereCondition.toString();
    }

    /**
     * SQL 생성용!
     * Snake Case 를 Camel Case 표기법으로 변경.
     *
     * @param snakeCase CamelCase 으로 변경할
     * @return String CamelCase.
     */
    public static String snakeCaseToCamelCase(String snakeCase){
        StringBuilder camelCase = new StringBuilder();
        for(String param : snakeCase.split("_")){
            if(camelCase.length() == 0){
                camelCase.append(param);
            }else{
                camelCase.append(StringUtils.capitalize(param));
            }
        }
        return camelCase.toString();
    }

    /**
     * SQL 생성용!
     * camelCase 를 snakeCase 로 변경한다.
     * @param camelCase camelCase 문자열
     * @return String snakeCase 문자열
     */
    public static String convertCamelCaseToSnakeCase(String camelCase){
        return convertCamelCaseToSnakeCase(camelCase, null);
    }

    /**
     * SQL 생성용!
     * camelCase 를 snakeCase 로 변경하면서
     * 입력받은 tableName 의 alias 를 추가한다.
     *
     * @param camelCase camelCase 문자열
     * @param tableName alias 생성용 tableName
     * @return String alias.snakeCase 문자열
     */
    public static String convertCamelCaseToSnakeCase(String camelCase, String tableName){
        StringBuilder convertCondition = new StringBuilder();
        // alias check
        camelCase = nonAlias(camelCase);

        for(String snakeCase : StringUtils.splitByCharacterTypeCamelCase(camelCase)){
            if(convertCondition.length() != 0){
                convertCondition.append(Constants.UNDERLINE);
            }
            if(NumberUtils.isParsable(snakeCase)){
                convertCondition.delete(convertCondition.length() - 1, convertCondition.length());
            }
            convertCondition.append(snakeCase.toLowerCase(Locale.KOREA));
        }
        if(!StringUtils.isEmpty(tableName)){
            convertCondition.insert(0, makeAliasFromTableName(tableName).concat(Constants.DOT));
        }
        return convertCondition.toString();
    }

    /**
     * SQL 생성용!
     * 업데이트 SET 조건문에 연산이 필요한경우.
     *
     * @param column 사용하고자하는 DB 컬럼
     * @param operator 사용하고자하는 연산자
     * @return String 업데이트 연산자 쿼리문.
     */
    public static String conditionForUpdateCalculation(String column, String operator){
        return column
            + wrapWithSpace(Constants.EQUAL)
            + column
            + wrapWithSpace(operator)
            + convertUnderscoreToCamelCase(column);
    }

    /**
     * SQL 생성용!
     * update 쿼리문 생성시 연산자를 추가하여 생성.
     *
     * @param column 기준 컬럼
     * @param value 연산할 Value
     * @param operator 연산자.
     * @return String 업데이트 쿼리문
     */
    public static String conditionValueForUpdateCalculation(String column, Object value, String operator){
        StringBuilder condition = new StringBuilder();
        condition.append(column);
        condition.append(Constants.EQUAL);
        condition.append(column);
        condition.append(wrapWithSpace(operator));
        if(value instanceof String){
            condition.append(appendSingleQuote(value));
        } else {
            condition.append(value);
        }
        return condition.toString();
    }

    /**
     * SQL 생성용!
     * tableName 으로부터 Alias 를 생성한 뒤
     * 입력받은 TableName 에 Alias 를 추가하여 리턴.
     * @param tableName 테이블명.
     * @return String 입력받은 테이블명 + Alias
     */
    public static String getAppendAliasToTableName(String tableName){
        return tableName + Constants.SPACE + makeAliasFromTableName(tableName);
    }

    /**
     * SQL 생성용!
     * 입력받은 Alias 를 입력받은 TableName 에 추가하여 리턴.
     * @param tableName 테이블명.
     * @param alias 별칭.
     * @return String 입력받은 Alias + 입력받은 테이블 명 (예: tableName alias)
     */
    public static String getAppendAliasToTableName(String tableName, String alias){
        if(alias != null){
            return tableName + Constants.SPACE + alias;
        } else {
            return tableName;
        }
    }

    /**
     * SQL 생성용!
     * tableName 으로부터 Alias 를 생성한다.
     * @param tableName 테이블명
     * @return String 생성한 Alias
     */
    public static String makeAliasFromTableName(String tableName){
        StringBuilder tableNameAliasing = new StringBuilder();

        if(tableName.split("_").length > 1){
            for(String param : tableName.split("_")){
                tableNameAliasing.append(param.toCharArray()[0]);
            }
        } else {
            tableNameAliasing.append(tableName);
        }

        return tableNameAliasing.toString();
    }

    /**
     * SQL 생성용!
     * 컬럼명 앞에 alias 를 추가한다.
     *
     * @param columnArray alias 를 추가할 컬럼 배열
     * @param alias 추가할 alias
     * @return String[] alias 가 추가된 컬럼의 배열
     */
    public static String[] appendingAliasToColumnArray(String[] columnArray, String alias){
        List<String> returnColumns = new ArrayList<>();
        for(String column : columnArray){
            returnColumns.add(appendAliasToColumn(column, alias));
        }
        return returnColumns.toArray(String[]::new);
    }

    /**
     * SQL 생성용!
     * 컬럼배열 내 컬럼 앞에
     * tableName 으로 생성한 alias 를 추가한다.
     *
     * @param columnArray Alias 를 추가할 컬럼 배열
     * @param tableName Alias 를 생성할 tableName
     * @return String[] Alias 가 추가된 컬럼 배열
     */
    public static String[] appendingAliasToColumnArrayWithTableName(
        String[] columnArray, String tableName){
        List<String> returnColumns = new ArrayList<>();
        String alias = makeAliasFromTableName(tableName);
        for(String column : columnArray){
            returnColumns.add(appendAliasToColumn(column, alias));
        }
        return returnColumns.toArray(String[]::new);
    }

    /**
     * SQL 생성용!
     * 입력받은 alias 를 입력받은 Column 앞에 추가한다.
     * @param column 입력받은 컬럼
     * @param alias 입력받은 별칭
     * @return String 입력받은 별칭 + 입력받은 컬럼 (예: alias.column)
     */
    public static String appendAliasToColumn(String column, String alias){
        return alias + Constants.DOT + column;
    }

    /**
     * SQL 생성용!
     * 입력받은 TableName 으로부터 Alias 를 생성하여 컬럼앞에 추가한다.
     * @param column alias 를 추가할 컬럼명
     * @param tableName alias 을 생성할 테이블명.
     * @return String 테이블명으로 생성된 Alias + column
     */
    public static String appendAliasToColumnWithTableName(String column, String tableName){
        return makeAliasFromTableName(tableName) + Constants.DOT + column;
    }

    /**
     * SQL 생성용!
     * BETWEEN 문을 생성한다.(날짜)
     * @param standard 기준값
     * @param condition1 조건1
     * @param condition2 조건2
     * @return String 기준값 BETWEEN 조건1 AND 조건2
     */
    public static String between(String standard, Object condition1, Object condition2){
        return sqlFunction(MysqlFunction.BETWEEN_DAY, ObjectUtils.toArray(standard, condition1, condition2));
    }

    /**
     * SQL 생성용!
     * BETWEEN 문을 생성한다.(날짜)
     * @param standard 기준값
     * @param condition1 조건1
     * @param condition2 조건2
     * @return String 기준값 BETWEEN 조건1 AND 조건2
     */
    public static String betweenDay(String standard, Object condition1, Object condition2){
        return sqlFunction(MysqlFunction.BETWEEN_DAY, ObjectUtils.toArray(standard, inputSingleQuoteCheck(condition1), inputSingleQuoteCheck(condition2)));
    }

    /**
     * SQL 생성용!
     * BETWEEN 문을 생성한다.(분)
     * @param standard 기준값
     * @param condition1 조건1
     * @param condition2 조건2
     * @return String 기준값 BETWEEN 조건1 AND 조건2
     */
    public static String betweenMinute(String standard, Object condition1, Object condition2){
        return sqlFunction(MysqlFunction.BETWEEN_MIN, ObjectUtils.toArray(standard, inputSingleQuoteCheck(condition1), inputSingleQuoteCheck(condition2)));
    }

    /**
     * SQL 생성용!
     * 특정 operator 연산용.
     *
     * @param operator 사용하고자하는 연산자
     * @param standard 기준값
     * @param condition 조건
     * @return 기준값 Operator 조건
     */
    public static String customOperator(String operator, String standard, String condition){
        return standard
            + wrapWithSpace(operator)
            + condition;
    }

    //연산자 포멧(SPACE)
    public static String wrapWithSpace(String operator) {
        return Constants.SPACE + operator + Constants.SPACE;
    }

    /**
     * 컬럼에 Alias 가 제거.
     * 컬럼에 alias 가 있는 경우 alias 제거 후 컬럼명만 리턴.
     *
     * @param column 확인할 컬럼
     * @return String alias 가 제거된 컬럼.
     */
    public static String nonAlias(String column){
        return column.contains(Constants.DOT) ? column.split(CustomConstants.SPLIT_DOT)[1] : column;
    }

    /**
     * SQL 생성용!
     * Row 단위 쿼리에 대한 Alias 부여
     * @param column alias 를 부여할 컬럼
     * @return String alias 가 부여된 컬럼정보.
     */
    public static String aliasToRow(String column){
        return column + wrapWithSpace("AS") + nonAlias(column);
    }

    /**
     * SQL 생성용!
     * Row 단위 쿼리에 대한 Alias 부여
     * @param column alias 를 부여할 컬럼
     * @param alias 컬럼에 부여한 alias
     * @return String alias 가 부여된 컬럼정보.
     */
    public static String aliasToRow(String column, String alias){
        if(alias == null || alias.isEmpty()){
            return column;
        }
        return column + wrapWithSpace("AS") + nonAlias(alias);
    }

    public static String createJoinCondition(Object targetInstance, Object[] conditionForEqual){
        return createJoinCondition(EntityFactory.getTableNameWithAlias(targetInstance), ObjectUtils.toList(onClause(conditionForEqual, targetInstance)));
    }

    public static String createJoinCondition(Object targetInstance, Object[] conditionForEqual, Object[] conditionForAnd){
        return createJoinCondition(EntityFactory.getTableNameWithAlias(targetInstance), ObjectUtils.toList(onClause(conditionForEqual, targetInstance)), conditionForAnd);
    }

    public static String createJoinCondition(String tableName, List<Object> conditionForEqual){
        return createJoinCondition(tableName, conditionForEqual, null);
    }

    public static String createJoinCondition(String tableName, Object[] conditionForAnd){
        return createJoinCondition(tableName, (List<Object>)null, conditionForAnd);
    }

    /**
     * SQL 생성용!
     * INNER JOIN 을 생성한다.
     *
     * @param tableName INNER JOIN 할 테이블.
     * @param conditionForEqual 동일조건으로 비교할 컬럼정보
     * @param conditionForAnd 특정입력값 조건식.
     * @return String INNER JOIN 쿼리를 생성하다.
     */
    public static String createJoinCondition(String tableName, List<Object> conditionForEqual,  Object[] conditionForAnd){
        StringBuilder innerJoinCondition = new StringBuilder();
        // tableName + on + 기본조건.
        if(conditionForEqual != null){
            for(Object equalObject : conditionForEqual){
                if(innerJoinCondition.length() == 0){
                    innerJoinCondition.append(equalObject.toString());
                } else {
                    innerJoinCondition.append(
                        MessageFormat.format(SqlPatternConstants.AND, equalObject));
                }
            }
        }

        if(conditionForAnd != null){
            for(Object andObject : conditionForAnd){
                if(innerJoinCondition.length() == 0){
                    innerJoinCondition.append(andObject.toString());
                } else {
                    innerJoinCondition.append(
                        MessageFormat.format(SqlPatternConstants.AND, andObject));
                }
            }
        }

        innerJoinCondition.insert(0, tableName + wrapWithSpace("ON"));

        return innerJoinCondition.toString();
    }

    /**
     * Class instance 를 활용하여
     * columns 의 column 명과 instance 내의 column 을 비교하여
     * 같은 값이 있으면 리턴.
     *
     * @param columns 찾고자 하는 컬럼 집합
     * @param instance 비교 하고자 하는 Class instance
     * @return Object[] 비교하여 생성한 Condition 의 집함.
     */
    public static Object[] onClause(Object[] columns, Object instance){
        List<Object> returnObject = new ArrayList<>();
        for(Object column : columns){
            returnObject.add(equalConditionClassCompare(String.valueOf(column), instance));
        }
        return returnObject.toArray();
    }

    /**
     * snakeCase 인지 확인.
     * 문자열에 UnderScore 가 존재하는 경우
     * SnakeCase 로 판단하여 리턴.
     *
     * @param value 확인하고자하는 값
     * @return Boolean snakeCase 여부.
     */
    private static Boolean isSnakeCase(String value){
        return value.contains("_");
    }

    /**
     * camelCase 인지 확인.
     * StringUtils 에서 camelCase 로 split 하게하여
     * length 가 0보다 크면 CamelCase 로 판단하여 리턴.
     *
     * @param value 확인하고자 하는 값
     * @return Boolean camelCase 여부
     */
    private static Boolean isCamelCase(String value){
        return StringUtils.splitByCharacterTypeCamelCase(value).length > 0 ? Boolean.TRUE : Boolean.FALSE;
    }


    /**
     * Object to Map[bean]
     *
     * @param object DTO 클래스
     * @return HashMap
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public static HashMap<String, Object> getConvertMap(Object object) throws Exception{
        HashMap<String, Object> returnMap = new HashMap<>();
        Class<?> clz = object.getClass();

        for(Field field : clz.getDeclaredFields()){
            if(Modifier.isPrivate(field.getModifiers())){
                ReflectionUtils.makeAccessible(field);
            }
            returnMap.put(field.getName(), field.get(object));
        }
        return returnMap;
    }

    /**
     * 0보다 큰지 확인.
     * 입력받은 값이 0보다 큰지 확인.
     *
     * @param checkBit 입력값
     * @return Boolean 여부
     */
    public static Boolean isGreaterThanZero(int checkBit){
        return checkBit > 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    public static String getConditionBySqlPattern(String patternType, String column, Object[] params) throws Exception {
        String param = Arrays.stream(params).map(o -> inputSingleQuoteCheck(o)).collect(Collectors.joining(","));
        return SqlPatternConstants.getConditionByPattern(patternType, column, param);
    }

    public static String customOperator(String operator, Object[] objects) {
        return customOperator(operator, objects, null);
    }

    public static String customOperator(String operator, Object[] objects, String alias){
        if(objects.length < 2){
            return aliasToRow(String.valueOf(objects[0]), alias);
        }

        StringBuilder operatorMessageQueue = new StringBuilder();
        operatorMessageQueue.append(wrapWithSpace("("));
        int idx = 0;
        while(idx < objects.length){
            operatorMessageQueue.append(objects[idx++]);
            if(idx != objects.length){
                operatorMessageQueue.append(wrapWithSpace(operator));
            }
        }
        operatorMessageQueue.append(wrapWithSpace(")"));
        return aliasToRow(operatorMessageQueue.toString(), alias);
    }

    private static String inputSingleQuoteCheck(Object value){
        if(value instanceof String){
            return appendSingleQuote(value);
        }else{
            return String.valueOf(value);
        }
    }

    public static String leftLike(String standard, String likeClause){
        return sqlFunction(MysqlFunction.LIKE, null, ObjectUtils.toArray(standard, "%".concat(likeClause)));
    }

    public static String rightLike(String standard, String likeClause){
        return sqlFunction(MysqlFunction.LIKE, null, ObjectUtils.toArray(standard, likeClause.concat("%'")));
    }

    public static String middleLike(String standard, String likeClause){
        return sqlFunction(MysqlFunction.LIKE, null, ObjectUtils.toArray(standard, "%".concat(likeClause).concat("%")));
    }

    public static String caseWhenElse(String standard, Object[] values, String elseStr){
        return caseWhenElse(standard, values, elseStr, null);
    }

    public static String caseWhenElse(String standard, Object[] values, String elseStr, String alias){
        return appendAlias(sqlFunction(MysqlFunction.CASE_WHEN, standard, values).replace("END", sqlFunction(MysqlFunction.ELSE, elseStr)), alias);
    }

    public static String sqlFunction(MysqlFunction function, String columnName) {
        return sqlFunction(function, columnName, null, null);
    }

    public static String sqlFunction(MysqlFunction function, String columnName, String alias) {
        return sqlFunction(function, columnName, null, alias);
    }

    public static String sqlFunction(MysqlFunction function, Object[] values) {
        return sqlFunction(function, null, values, null);
    }

    public static String sqlFunction(MysqlFunction function, Object[] values, String alias) {
        return sqlFunction(function, null, values, alias);
    }

    public static String sqlFunction(MysqlFunction function, String columnName, Object[] values, String alias) {
        return aliasToRow(sqlFunction(function, columnName, values), alias);
    }

    public static String sqlFunction(MysqlFunction function, String columnName, Object[] values) {
        if(function == MysqlFunction.IN || function == MysqlFunction.NOT_IN){
            return IN(function, columnName, values);
        } else if (function == MysqlFunction.COMMON_TABLE) {
            return COMMON_QUERY(nonAlias(columnName), values);
        } else if (function.name().startsWith("SP")){
            return CALL_PROCEDURE(function, values);
        }
        return sqlFunction(function.getPattern(), columnName, values);
    }

    private static String IN(MysqlFunction function, String columnName, Object[] values) {
        return MessageFormat.format(function.getPattern(), ObjectUtils.toArray(columnName, Arrays.stream(values).map(Object::toString).collect(Collectors.joining(","))));
    }

    private static String COMMON_QUERY(String columnName, Object[] values) {
        return MessageFormat.format(MysqlFunction.COMMON_TABLE.getPattern(), ObjectUtils.toArray(columnName, Arrays.stream(values).map(Object::toString).collect(Collectors.joining(","))));
    }

    private static String CALL_PROCEDURE(MysqlFunction function, Object[] values) {
        return CustomConstants.CURLY_BRACKET_START
            + wrapWithSpace("CALL")
            + MessageFormat.format(function.getPattern(), values)
            + wrapWithSpace(CustomConstants.CURLY_BRACKET_END);
    }

    private static String sqlFunction(String conditionPattern, String columnName, Object[] values) {

        // 반복 처리 여부 확인.
        if(!conditionPattern.contains("[...]")){
            if(values != null){
                return MessageFormat.format(conditionPattern, values);
            }
            return MessageFormat.format(conditionPattern, columnName);
        }

        int subIndex = StringUtils.countMatches(conditionPattern, "[...]");

        if(subIndex > 0){
            StringBuilder sb = new StringBuilder();
            String[] condition = conditionPattern.split("\\[columnName]");
            String[] repeat;
            if((condition.length - 1) > 0){
                // 메인
                sb.append(condition[0].concat(columnName));
                repeat = condition[1].split("\\[\\.\\.\\.]");
            } else {
                repeat = conditionPattern.split("\\[\\.\\.\\.]");
            }

            if(values.length % subIndex == 0){
                int _idx = 0;

                if(subIndex == 1){
                    sb.append(repeat[0]);
                    sb.append(Arrays.stream(values).map(String::valueOf).collect(Collectors.joining(",")));
                } else {
                    while(_idx < values.length){
                        sb.append(repeat[_idx % subIndex].concat(String.valueOf(values[_idx++])));
                    }
                }
                sb.append(repeat[repeat.length - 1]);
            }
            return sb.toString();
        }

        return null;
    }

    private static String appendAlias(String condition, String alias) {
        return !StringUtils.isEmpty(alias) ? condition + wrapWithSpace("AS") + alias : condition;
    }

    public static String subTableQuery(SQL query) {
        return subTableQuery(query, null);
    }

    public static String subTableQuery(SQL query, String alias) {
        return aliasToRow(
            CustomConstants.ROUND_BRACKET_START
                .concat(query.toString())
                .concat(CustomConstants.ROUND_BRACKET_END),
            alias
        );
    }

    public static String subQuery(Object[] column, String tableName, Object[] conditions){
        return subQuery(Arrays.stream(column).map(String::valueOf).collect(Collectors.joining(",")), tableName, Arrays.asList(conditions));
    }

    public static String subQuery(Object[] column, String tableName, Object[] conditions, String alias){
        return getAppendAliasToTableName(subQuery(Arrays.stream(column).map(String::valueOf).collect(Collectors.joining(", ")), tableName, conditions), alias);
    }

    public static String subQuery(String column, String tableName, Object[] conditions){
        return subQuery(column, tableName, Arrays.asList(conditions));
    }

    public static String subQuery(String column, String tableName, Object[] conditions, String alias){
        return aliasToRow(subQuery(column, tableName, Arrays.asList(conditions)), alias);
    }

    private static String subQuery(String column, String tableName, List<Object> conditions){
        StringBuilder query = new StringBuilder();
        int _index = 0;
        while(_index < conditions.size()){
            query.append(conditions.get( _index++ ));
            if(_index != conditions.size()){
                query.append(wrapWithSpace("AND"));
            }
        }
        return CustomConstants.ROUND_BRACKET_START
            .concat(MessageFormat.format(MysqlFunction.SUB_QUERY.getPattern(), Arrays.asList(column, tableName, query.toString()).toArray()))
            .concat(CustomConstants.ROUND_BRACKET_END);
    }

    public static String OR(String... arrays){
        return OR(Boolean.FALSE, arrays);
    }

    public static String OR(boolean isCurly, String... arrays){
        if(isCurly){
            return "(".concat(makeLinearQuery(arrays, CustomConstants.OR)).concat(")");
        }
        return makeLinearQuery(arrays, CustomConstants.OR);
    }

    public static String AND(boolean isCurly, String... arrays){
        if(isCurly){
            return "(".concat(makeLinearQuery(arrays, CustomConstants.AND)).concat(")");
        }
        return makeLinearQuery(arrays, CustomConstants.AND);
    }

    public static String AND(String... arrays){
        return AND(Boolean.FALSE, arrays);
    }

    public static String exists(SQL query){
        return sqlFunction(MysqlFunction.EXISTS, query.toString());
    }

    public static String notExists(SQL query){
        return sqlFunction(MysqlFunction.NOT_EXISTS, query.toString());
    }

    private static String makeLinearQuery(String[] arrays, String type) {
        StringBuilder condition = new StringBuilder();
        int idx = 0;
        while(idx < arrays.length){
            if(idx != 0){
                condition.append(wrapWithSpace(type));
            }
            condition.append(arrays[idx++]);
        }
        return condition.toString();
    }

    public static String orQuery(String query){
        return query.replaceFirst("\\) \nOR \\(", " OR ");
    }

    public static String orQuery(Object[] querys){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(CustomConstants.ROUND_BRACKET_START);
        int count = 0;
        for(Object obj : querys){
            if(count++ != 0){
                stringBuilder.append(SqlUtils.wrapWithSpace("OR"));
            }
            stringBuilder.append(String.valueOf(obj));
        }
        stringBuilder.append(CustomConstants.ROUND_BRACKET_END);
        return stringBuilder.toString();
    }

    public static String[] orderByesc(Object[] objects) {
        return Arrays.stream(objects)
            .map(o -> String.valueOf(o).concat(" DESC"))
            .collect(Collectors.toList())
            .toArray(String[]::new);
    }

    public static String[] orderByesc(String... columns) {
        return Arrays.stream(columns)
            .map(o -> String.valueOf(o).concat(" DESC"))
            .collect(Collectors.toList())
            .toArray(String[]::new);
    }

    public static String monthOfFirstDay(String dateColumn) {
        return sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(dateColumn, "%Y-%m-01"));
    }

    public static String monthOfFirstDay(String dateColumn, int interval){
        return sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(sqlFunction(MysqlFunction.DATEADD_MON, ObjectUtils.toArray(dateColumn, interval)), "%Y-%m-01"));
    }

    public static String monthOfLastDay(String dateColumn) {
        return sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(monthOfFirstDay(dateColumn, 1), "-1"));
    }
}
