package kr.co.homeplus.claim.omni.service;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claim.claim.model.register.ClaimRegDetailDto;
import kr.co.homeplus.claim.enums.impl.EnumImpl;
import kr.co.homeplus.claim.omni.model.OmniOriginOrderDto;

public interface OmniOrderMapperService {

    List<OmniOriginOrderDto> getOmniOriginOrderList(long purchaseOrderNo);

    Boolean claimSubstitutionResultModify(LinkedHashMap<String, Object> parameterMap);

    Boolean omniClaimStatusModify(long claimNo, String claimStatus);

    Long convertOrdNoToPurchaseOrderNo(long ordNo);

    Long convertOrdNoToClaimNo(long ordNo);

    ClaimRegDetailDto getClaimShipInfoByOmni(long purchaseOrderNo, long bundleNo);

    LinkedHashMap<String, String> getClaimReasonType(String claimCd);

    List<LinkedHashMap<String, Long>> getExpTotalCancelBundleNo(long purchaseOrderNo);

    List<ClaimPreRefundItemList> getOrderItemInfoExp(long purchaseOrderNo, long bundleNo);

    boolean addClaimOosInfo(LinkedHashMap<String, Object> parameterMap);

    boolean addClaimOosItem(LinkedHashMap<String, Object> parameterMap);

    boolean setClaimOosMangeModify(LinkedHashMap<String, Object> parameterMap);

    LinkedHashMap<String, Object> getClaimStatusInfo(long claimNo);

    LinkedHashMap<String, Object> getOmniShipPresentCheck(long purchaseOrderNo, long bundleNo);
}
