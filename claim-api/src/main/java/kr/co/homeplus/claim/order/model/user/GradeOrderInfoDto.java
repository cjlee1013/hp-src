package kr.co.homeplus.claim.order.model.user;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@ApiModel(description = "다음달 예정 등급 참고용 주문데이터")
public class GradeOrderInfoDto {
    private int orderPrice;
    private int orderCnt;
    private long userNo;
}
