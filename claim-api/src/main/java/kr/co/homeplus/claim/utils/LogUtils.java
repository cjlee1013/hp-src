package kr.co.homeplus.claim.utils;

import java.util.LinkedHashMap;
import javax.annotation.PostConstruct;
import kr.co.homeplus.claim.claim.model.register.ClaimProcessHistoryRegDto;
import kr.co.homeplus.claim.claim.service.ClaimMapperService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class LogUtils {
    private static ClaimMapperService mapper;
    private final ClaimMapperService mapperService;

    @PostConstruct
    private void initialize() {
        mapper = mapperService;
    }
    public static void error(String logMsg){
        log.info("error ::: {}", logMsg);
    }
    public static void error(String logPath, Object... objects) {
        log.info(logPath, objects);
    }

    public static void addLogHistory(ClaimProcessHistoryRegDto claimProcessHistoryRegDto) {
        log.info("히스토리 저장 :: {}", mapper.setClaimProcessHistory(claimProcessHistoryRegDto));
    }

    public static void addLogHistory(Object object, String historyReason, String historyDetailDesc, String historyDetailBefore) throws Exception {
        addLogHistory(object, historyReason, historyDetailDesc, historyDetailBefore, "MYPAGE");
    }

    public static void addLogHistory(Object object, String historyReason, String historyDetailDesc, String historyDetailBefore, String regId) throws Exception {
        LinkedHashMap<String, Object> map = ObjectUtils.getConvertMap(object);
        ClaimProcessHistoryRegDto historyRegDto = ClaimProcessHistoryRegDto.builder()
            .historyReason(String.valueOf(historyReason))
            .historyDetailDesc(String.valueOf(historyDetailDesc))
            .regId(regId)
            .regChannel("MYPAGE")
            .build();

        if(map.get("purchaseOrderNo") != null){
            historyRegDto.setPurchaseOrderNo(ObjectUtils.toLong(map.get("purchaseOrderNo")));
        }
        if(map.get("bundleNo") != null){
            historyRegDto.setBundleNo(ObjectUtils.toLong(map.get("bundleNo")));
        }
        if(map.get("claimBundleNo") != null){
            historyRegDto.setClaimBundleNo(ObjectUtils.toLong(map.get("claimBundleNo")));
        }
        if(map.get("claimNo") != null){
            historyRegDto.setClaimNo(ObjectUtils.toLong(map.get("claimNo")));
        }
        if(map.get("shipNo") != null){
            historyRegDto.setShipNo(ObjectUtils.toLong(map.get("shipNo")));
        }
        if(map.get("claimPickShippingNo") != null){
            historyRegDto.setClaimPickShippingNo(ObjectUtils.toLong(map.get("claimPickShippingNo")));
        }
        if(map.get("claimExchShippingNo") != null){
            historyRegDto.setClaimExchShippingNo(ObjectUtils.toLong(map.get("claimExchShippingNo")));
        }
        if(map.get("multiBundleNo") != null){
            historyRegDto.setMultiBundleNo(ObjectUtils.toLong(map.get("multiBundleNo")));
        }
        if(historyDetailBefore != null){
            historyRegDto.setHistoryDetailBefore(historyDetailBefore);
        }
        log.info("히스토리 저장 :: {}", mapper.setClaimProcessHistory(historyRegDto));
    }

    private static ClaimProcessHistoryRegDto createClaimProcessHistoryRegDto(long purchaseOrderNo, long bundleNo, long claimNo, long claimBundleNo, String historyReason, String historyDetailDesc, String historyDetailBefore, String chgId, String historyChannel) {
        return ClaimProcessHistoryRegDto.builder()
            .claimBundleNo(claimBundleNo)
            .claimNo(claimNo)
            .purchaseOrderNo(purchaseOrderNo)
            .bundleNo(bundleNo)
            .historyReason(historyReason)
            .historyDetailDesc(historyDetailDesc)
            .historyDetailBefore(historyDetailBefore)
            .regId(chgId)
            .regChannel(historyChannel)
            .build();
    }
}
