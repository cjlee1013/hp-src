package kr.co.homeplus.claim.claim.mapper;

import kr.co.homeplus.claim.core.db.annotation.PrimaryConnection;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipEditDto;
import kr.co.homeplus.claim.order.model.ship.NoRcvShipSetGto;
import kr.co.homeplus.claim.claim.sql.MyPageShipSql;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.UpdateProvider;

@PrimaryConnection
public interface ClaimPrimaryMapper {

    @InsertProvider(type = MyPageShipSql.class, method = "insertNoRcvShip")
    int insertNoRcvShip(NoRcvShipSetGto setGto);

    @UpdateProvider(type = MyPageShipSql.class, method = "updateNoRcvShip")
    int updateNoRcvShip(NoRcvShipEditDto editDto);

}
