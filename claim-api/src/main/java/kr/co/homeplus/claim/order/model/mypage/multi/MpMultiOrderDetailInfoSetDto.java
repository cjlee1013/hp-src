package kr.co.homeplus.claim.order.model.mypage.multi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마일페이지 > 주문/배송조회 - 상세주문 리스트")
public class MpMultiOrderDetailInfoSetDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private String purchaseOrderNo;
    @ApiModelProperty(value = "유저번호", required = true, position = 2)
    private long userNo;
    @ApiModelProperty(value = "다중배송번호", position = 4)
    private String multiBundleNo;
    @ApiModelProperty(value = "사이트타입", position = 5)
    private String siteType;
}
