package kr.co.homeplus.claim.enums;

import kr.co.homeplus.claim.enums.impl.EnumImpl;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimResponseCode implements EnumImpl {
    CLAIM_REG_SUCCESS("0000", "클레임 등록에 성공하였습니다."),
    CLAIM_REG_ERR01("1001", "클레임 등록에 실패하였습니다."),
    CLAIM_REG_ERR02("1002", "클레임 등록을 위한 데이터 조회에 실패하였습니다."),
    CLAIM_REG_ERR03("1003", "이미 클레임 진행 중인 상품이 있습니다."),
    CLAIM_REG_ERR04("1004", "취소 가능한 결제 건이 없습니다."),
    CLAIM_REG_ERR05("1005", "주문 내역을 확인할 수 없습니다."),
    CLAIM_REG_ERR06("1006", "이미 모든 주문이 취소 되었습니다."),
    CLAIM_REG_ERR07("1007", "취소 건수가 없습니다."),
    CLAIM_REG_ERR08("1008", "클레임 등록가능 건이 없습니다."),
    CLAIM_REG_ERR09("1009", "신청 가능 시간이 초과 되었습니다."),
    CLAIM_REG_ERR11("1011", "반품(수거) 정보를 등록할 수 없습니다."),
    CLAIM_REG_ERR12("1012", "교환 정보를 등록할 수 없습니다."),
    CLAIM_REG_ERR13("1013", "수거 정보를 입력해주세요."),
    CLAIM_REG_ERR14("1014", "합배송이 있는 주문은 전체클레임(%s)이 불가합니다. 합배송 주문 먼저 %s해주시기 바랍니다."),
    CLAIM_REG_ERR15("1015", "클레임 불가능한 상품을 신청하셨습니다."),
    CLAIM_REG_ERR16("1016", "중복쿠폰건은 클레임 처리할 수 없습니다."),
    CLAIM_REG_ERR17("1017", "상품을 준비중입니다.\n주문을 취소 할 수 없습니다.\n불편을 드려 죄송합니다."),

    CLAIM_MULTI_REG_ERR01("1401", "%s는 전체취소만 가능합니다."),

    CLAIM_MARKET_REG_ERR01("1501", "교환신청은 마켓사이트에서만 가능합니다."),
    CLAIM_MARKET_REG_ERR02("1502", "마켓연동주문으로 할 수 없는 기능입니다."),
    CLAIM_MARKET_REG_ERR03("1503", "마켓연동시 오류가 발생되었습니다."),

    CLAIM_TICKET_ERR01("1901", "취소수량과 티켓수량이 맞지 않습니다."),
    CLAIM_TICKET_ERR02("1902", "취소불가능한 티켓이 포함되어 있습니다."),
    CLAIM_TICKET_ERR03("1903", "티켓취소정보 저장중 오류가 발생하였습니다."),
    CLAIM_TICKET_ERR04("1904", "티켓 취소중 오류가 발생하였습니다."),

    CLAIM_CANCEL_ERR01("1301", "클레임 취소에 실패하였습니다."),
    CLAIM_CANCEL_ERR02("1302", "환불예정금액이 0원보다 작습니다."),

    CLAIM_STATUS_CHANGE_ERR01("1201", "클레임 상태 변경중 오류가 발생되었습니다."),
    CLAIM_STATUS_CHANGE_ERR02("1202", "클레임 상태 변경중 오류가 발생되었습니다."),
    CLAIM_STATUS_CHANGE_ERR03("1203", "클레임 상태 변경중 오류가 발생되었습니다."),

    CLAIM_OMNI_ERR01("2001", "대체가능한 주문이 없습니다."),
    CLAIM_OMNI_ERR02("2002", "대체주문 요청에 실패하였습니다."),
    CLAIM_OMNI_ERR03("2003", "대체주문 클레임 생성에 실패하였습니다."),
    CLAIM_OMNI_ERR10("2010", "존재하지 않는 주문번호입니다."),
    CLAIM_OMNI_ERR11("2011", "반품/교환 중 하나만 선택해서 시도해주세요."),
    CLAIM_OMNI_ERR12("2012", "반품/교환 요청중 상품번호가 없는 요청이 존재합니다."),
    CLAIM_OMNI_ERR13("2013", "반품/교환 요청중 대상수량이 없는 요청이 존재합니다."),
    CLAIM_OMNI_ERR14("2014", "반품/교환 요청중 클레임사유가 없는 요청이 존재합니다."),
    CLAIM_OMNI_ERR15("2015", "반품/교환 클레임사유가 잘못되었습니다."),
    CLAIM_OMNI_ERR16("2016", "반품/교환 불가 주문입니다."),

    CLAIM_OMNI_CANCEL_SUCCESS("0000", "전체주문취소에 성공하였습니다."),
    CLAIM_OMNI_STATUS_CHANGE_SUCCESS("0000", "상태변경에 성공하였습니다."),
    CLAIM_OMNI_ERR31("2031", "전체취소가 불가능한 주문입니다."),
    CLAIM_OMNI_ERR32("2032", "클레임 상태 변경에 실패하였습니다."),
    CLAIM_OMNI_ERR33("2033", "이미 승인처리가 완료된 건 입니다."),

    CLAIM_PAYMENT_CANCEL_MHC_ERR01("3101", "MHC 카드 정보 취득에 실패하였습니다."),
    CLAIM_PAYMENT_CANCEL_EMP_ERR01("3111", "임직원할인잔액 취득에 실패하였습니다."),
    CLAIM_PAYMENT_CANCEL_ZERO_ERR01("3121", "취소금액이 0원 보다 작습니다."),
    CLAIM_PAYMENT_CANCEL_MARKET_ERR01("3131", "마켓주문 대체 건이 원주문금액보다 적습니다."),
    ;

    private final String code;
    private final String description;

    @Override
    public String getResponseCode() {
        return this.code;
    }

    @Override
    public String getResponseMessage() {
        return this.description;
    }
}
