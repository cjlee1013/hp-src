package kr.co.homeplus.claim.office.mapper;

import java.util.LinkedHashMap;
import kr.co.homeplus.claim.core.db.annotation.PrimaryConnection;
import kr.co.homeplus.claim.office.sql.ClaimDataSql;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

@PrimaryConnection
public interface OfficeWriteMapper {

    @SelectProvider(value = ClaimDataSql.class, method = "selectClaimData")
    LinkedHashMap<String, Object> selectClaimData(@Param("bundleNo") long bundleNo);

}
