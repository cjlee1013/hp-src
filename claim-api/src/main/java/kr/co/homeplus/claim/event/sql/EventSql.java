package kr.co.homeplus.claim.event.sql;

import kr.co.homeplus.claim.constants.CustomConstants;
import kr.co.homeplus.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claim.entity.claim.ClaimInfoEntity;
import kr.co.homeplus.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claim.entity.claim.ClaimMstEntity;
import kr.co.homeplus.claim.entity.claim.ClaimPaymentEntity;
import kr.co.homeplus.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claim.entity.order.OrderPaymentDivideEntity;
import kr.co.homeplus.claim.entity.order.PaymentEntity;
import kr.co.homeplus.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claim.enums.MysqlFunction;
import kr.co.homeplus.claim.event.model.PayCompleteByAmtCheckSetDto;
import kr.co.homeplus.claim.event.model.PurchaseByShipCompleteSetDto;
import kr.co.homeplus.claim.event.model.PurchaseEventCheckSetDto;
import kr.co.homeplus.claim.event.model.PurchaseMileagePaybackSetDto;
import kr.co.homeplus.claim.event.model.ShipCompleteByAmtCheckSetDto;
import kr.co.homeplus.claim.factory.EntityFactory;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class EventSql {

    public static String selectEventPurchaseOrderCheck(PurchaseEventCheckSetDto setDto){
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                orderItem.orderItemNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemQty, orderItem.itemQty),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimItem.claimItemQty, claimItem.claimItemQty)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    orderItem,
                    ObjectUtils.toArray(purchaseOrder.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.storeType, setDto.getStoreType())
                    )
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    shippingItem,
                    ObjectUtils.toArray(orderItem.orderItemNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")),
                        SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("'D4'", "'D5'")),
                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, shippingItem.completeDt, SqlUtils.appendSingleQuote(setDto.getValidEndDt()))
                    )
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, orderItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, setDto.getUserNo()),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT_EQUAL, purchaseOrder.orderDt, SqlUtils.appendSingleQuote(setDto.getValidStartDt())),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, purchaseOrder.orderDt, SqlUtils.appendSingleQuote(setDto.getValidEndDt())),
                SqlUtils.OR(
                    Boolean.TRUE,
                    SqlUtils.AND(
                        Boolean.TRUE,
                        SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, setDto.getPurchaseOrderNo()),
                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_NOR")
                    ),
                    SqlUtils.AND(
                        Boolean.TRUE,
                        SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, setDto.getPurchaseOrderNo()),
                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB")
                    )
                ),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, "NOW()"), shippingItem.completeDt)
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo, orderItem.orderItemNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.nonAlias(orderItem.itemQty)),
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.nonAlias(claimItem.claimItemQty))
                    ),
                    "order_cnt"
                )
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "A"));

        log.debug("selectEventPurchaseOrderCheck ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectEventPurchasePaymentInfo(PurchaseMileagePaybackSetDto setDto){
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimInfoEntity claimInfo = EntityFactory.createEntityIntoValue(ClaimInfoEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
//                purchaseOrder.purchaseOrderNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB"),
                        purchaseOrder.orgPurchaseOrderNo,
                        purchaseOrder.purchaseOrderNo
                    ),
                    purchaseOrder.purchaseOrderNo
                ),
                orderItem.orderItemNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderDt, purchaseOrder.orderDt),
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.pgDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.mhcDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.ocbDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.dgvDivideAmt)
                    ),
                    "payment_amt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL_SUM_ZERO,
                    SqlUtils.customOperator(
                        CustomConstants.PLUS,
                        ObjectUtils.toArray(
                            claimInfo.pgAmt,
                            claimInfo.mhcAmt,
                            claimInfo.ocbAmt,
                            claimInfo.dgvAmt
                        )
                    ),
                    "claim_amt"
                )
//                ,
//                SqlUtils.subTableQuery(
//                    new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimPayment.substitutionAmt))
//                    .FROM(EntityFactory.getTableNameWithAlias(claimPayment))
//                    .WHERE(SqlUtils.equalColumn(claimPayment.claimNo, SqlUtils.sqlFunction(MysqlFunction.MAX, claimInfo.claimNo))),
//                    claimPayment.substitutionAmt
//                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    shippingItem,
                    ObjectUtils.toArray(orderItem.orderItemNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")),
                        SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("'D4'", "'D5'")),
                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, shippingItem.completeDt, SqlUtils.appendSingleQuote(setDto.getValidEndDt()))
                    )
                )
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(orderPaymentDivide, ObjectUtils.toArray(shippingItem.purchaseOrderNo, shippingItem.orderItemNo, shippingItem.bundleNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(shippingItem.purchaseOrderNo, shippingItem.bundleNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimInfo, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo, shippingItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, setDto.getUserNo()),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT_EQUAL, purchaseOrder.orderDt, SqlUtils.appendSingleQuote(setDto.getValidStartDt())),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, purchaseOrder.orderDt, SqlUtils.appendSingleQuote(setDto.getValidEndDt()))
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo, orderItem.orderItemNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(purchaseOrder.orderDt), purchaseOrder.orderDt),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.nonAlias("payment_amt"), "payment_amt"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.nonAlias("claim_amt"), "claim_amt")
//                ,
//                SqlUtils.customOperator(
//                    CustomConstants.MINUS,
//                    ObjectUtils.toArray(
//                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.nonAlias("claim_amt")),
//                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.nonAlias(claimPayment.substitutionAmt))
//                    ),
//                    "claim_amt"
//                )
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "A"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, "payment_amt", "0")
            )
            .GROUP_BY(SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo));

        log.debug("selectEventPurchasePaymentInfo ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectShipCompleteByBasicAmt(ShipCompleteByAmtCheckSetDto setDto) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimInfoEntity claimInfo = EntityFactory.createEntityIntoValue(ClaimInfoEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderType, purchaseOrder.orderType),
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.pgDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.mileageDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.mhcDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.ocbDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.dgvDivideAmt)
                    ),
                    "payment_amt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimInfo.pgAmt, claimInfo.mileageAmt, claimInfo.mhcAmt, claimInfo.ocbAmt, claimInfo.dgvAmt)), "claim_amt"),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.completeDt), shippingItem.completeDt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.itemQty, orderPaymentDivide.itemQty),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimInfo.completeQty, "claim_qty")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderPaymentDivide, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    shippingItem,
                    ObjectUtils.toArray(orderPaymentDivide.bundleNo, orderPaymentDivide.orderItemNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")),
                        SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("'D4'", "'D5'"))
                    )
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(shippingItem.purchaseOrderNo, shippingItem.bundleNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimBundle.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimMst.oosYn, "N"))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimInfo, ObjectUtils.toArray(claimMst.claimNo, claimBundle.claimBundleNo, shippingItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_NOR"),
                SqlUtils.equalColumnByInput(purchaseOrder.siteTypeExtend, setDto.getStoreKind()),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, setDto.getUserNo()),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT_EQUAL, shippingItem.completeDt, SqlUtils.appendSingleQuote(setDto.getValidStartDt())),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, shippingItem.completeDt, SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(SqlUtils.appendSingleQuote(setDto.getValidEndDt()), "1"))))
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo, orderPaymentDivide.orderItemNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.SUM, "payment_amt"),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, "claim_amt")
                    ),
                    "payment_amt"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.SUM, "item_qty"),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, "claim_qty")
                    ),
                    "payment_qty"
                )
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "Z"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(SqlUtils.nonAlias(shippingItem.completeDt), "1")), "NOW()")
            )
            .GROUP_BY(SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo));
        log.debug("selectShipCompleteByBasicAmt ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectShipCompleteByDayLimit(ShipCompleteByAmtCheckSetDto setDto) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimInfoEntity claimInfo = EntityFactory.createEntityIntoValue(ClaimInfoEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderType, purchaseOrder.orderType),
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.pgDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.mileageDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.mhcDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.ocbDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.dgvDivideAmt)
                    ),
                    "payment_amt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimInfo.pgAmt, claimInfo.mileageAmt, claimInfo.mhcAmt, claimInfo.ocbAmt, claimInfo.dgvAmt)), "claim_amt"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimType), claimBundle.claimType),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.completeDt), shippingItem.completeDt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.itemQty, orderPaymentDivide.itemQty),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimInfo.completeQty, "claim_qty")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderPaymentDivide, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    shippingItem,
                    ObjectUtils.toArray(orderPaymentDivide.bundleNo, orderPaymentDivide.orderItemNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")),
                        SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("'D4'", "'D5'"))
                    )
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(shippingItem.purchaseOrderNo, shippingItem.bundleNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimBundle.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimMst.oosYn, "N"))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimInfo, ObjectUtils.toArray(claimMst.claimNo, claimBundle.claimBundleNo, shippingItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_NOR"),
                SqlUtils.equalColumnByInput(purchaseOrder.siteTypeExtend, setDto.getStoreKind()),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, setDto.getUserNo()),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT_EQUAL, shippingItem.completeDt, SqlUtils.appendSingleQuote(setDto.getValidStartDt())),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, shippingItem.completeDt, SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(SqlUtils.appendSingleQuote(setDto.getValidEndDt()), "1"))))
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo, orderPaymentDivide.orderItemNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(shippingItem.completeDt)), CustomConstants.YYYYMMDD), shippingItem.completeDt),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.SUM, "payment_amt"),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, "claim_amt")
                    ),
                    "payment_amt"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.SUM, "item_qty"),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, "claim_qty")
                    ),
                    "payment_qty"
                )
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "Z"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(SqlUtils.nonAlias(shippingItem.completeDt), "1")), "NOW()")
            )
            .GROUP_BY(SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo));
        log.debug("selectShipCompleteByDayLimit ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectNotStoreKindShipCompleteList(PurchaseByShipCompleteSetDto setDto) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.FN_ORIGIN_PURCHASE_ORDER_NO, purchaseOrder.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(shippingItem.completeDt, "'-'"), shippingItem.completeDt),
                orderItem.storeType,
                purchaseOrder.orderDt,
                purchaseOrder.userNo,
                SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.CAST_SIGNED, orderItem.itemQty), SqlUtils.sqlFunction(MysqlFunction.FN_GET_CLAIM_QTY, ObjectUtils.toArray(shippingItem.bundleNo, orderItem.orderItemNo))), orderItem.itemQty)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    shippingItem,
                    ObjectUtils.toArray(orderItem.orderItemNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")),
                        SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("'D4'", "'D5'"))
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, setDto.getUserNo()),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT_EQUAL, shippingItem.completeDt, SqlUtils.appendSingleQuote(setDto.getValidStartDt())),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, shippingItem.completeDt, SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(SqlUtils.appendSingleQuote(setDto.getValidEndDt()), "1"))))
            );

        SQL query = new SQL()
            .SELECT(
                "purchase_order_no",
                SqlUtils.sqlFunction(MysqlFunction.MAX, "store_type", "store_type"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "complete_dt", "complete_dt"),
                SqlUtils.sqlFunction(MysqlFunction.MIN, "order_dt", "order_dt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "user_no", "user_no"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_Y_N,
                    SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, "NOW()", SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, "complete_dt"), "1")))),
                    "issuance_possible_yn"
                )
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "Z"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, "item_qty", "0")
            )
            .GROUP_BY(SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo));
        log.debug("selectNotStoreKindShipCompleteList ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPayCompleteByBasicAmt(PayCompleteByAmtCheckSetDto setDto) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimInfoEntity claimInfo = EntityFactory.createEntityIntoValue(ClaimInfoEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderType, purchaseOrder.orderType),
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.pgDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.mileageDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.mhcDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.ocbDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.dgvDivideAmt)
                    ),
                    "payment_amt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimInfo.pgAmt, claimInfo.mileageAmt, claimInfo.mhcAmt, claimInfo.ocbAmt, claimInfo.dgvAmt)), "claim_amt"),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.MAX, payment.paymentFshDt), payment.paymentFshDt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.itemQty, orderPaymentDivide.itemQty),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimInfo.completeQty, "claim_qty")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderPaymentDivide, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(orderPaymentDivide.purchaseOrderNo, orderPaymentDivide.bundleNo, orderPaymentDivide.orderItemNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimBundle.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimMst.oosYn, "N"))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimInfo, ObjectUtils.toArray(claimMst.claimNo, claimBundle.claimBundleNo, orderItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_NOR"),
                SqlUtils.equalColumnByInput(orderItem.storeType, setDto.getStoreType()),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, setDto.getUserNo()),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT_EQUAL, payment.paymentFshDt, SqlUtils.appendSingleQuote(setDto.getValidStartDt())),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, payment.paymentFshDt, SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(SqlUtils.appendSingleQuote(setDto.getValidEndDt()), "1")))),
                SqlUtils.sqlFunction(MysqlFunction.IS_NULL, orderItem.reserveShipMethod)
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo, orderPaymentDivide.orderItemNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.SUM, "payment_amt"),
                        SqlUtils.sqlFunction(MysqlFunction.SUM, "claim_amt")
                    ),
                    "payment_amt"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.SUM, "item_qty"),
                        SqlUtils.sqlFunction(MysqlFunction.SUM, "claim_qty")
                    ),
                    "payment_qty"
                )
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "Z"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT_EQUAL, SqlUtils.nonAlias(payment.paymentFshDt), "NOW()")
            )
            .GROUP_BY(SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo));
        log.debug("selectPayCompleteByBasicAmt ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPayCompleteByDayLimit(PayCompleteByAmtCheckSetDto setDto) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimInfoEntity claimInfo = EntityFactory.createEntityIntoValue(ClaimInfoEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderType, purchaseOrder.orderType),
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.pgDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.mileageDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.mhcDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.ocbDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.dgvDivideAmt)
                    ),
                    "payment_amt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimInfo.pgAmt, claimInfo.mileageAmt, claimInfo.mhcAmt, claimInfo.ocbAmt, claimInfo.dgvAmt)), "claim_amt"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimType), claimBundle.claimType),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.MAX, payment.paymentFshDt), payment.paymentFshDt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.itemQty, orderPaymentDivide.itemQty),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimInfo.completeQty, "claim_qty")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderPaymentDivide, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(orderPaymentDivide.purchaseOrderNo, orderPaymentDivide.bundleNo, orderPaymentDivide.orderItemNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimBundle.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimMst.oosYn, "N"))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimInfo, ObjectUtils.toArray(claimMst.claimNo, claimBundle.claimBundleNo, orderItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_NOR"),
                SqlUtils.equalColumnByInput(orderItem.storeType, setDto.getStoreType()),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, setDto.getUserNo()),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT_EQUAL, payment.paymentFshDt, SqlUtils.appendSingleQuote(setDto.getValidStartDt())),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, payment.paymentFshDt, SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(SqlUtils.appendSingleQuote(setDto.getValidEndDt()), "1")))),
                SqlUtils.sqlFunction(MysqlFunction.IS_NULL, orderItem.reserveShipMethod)
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo, orderPaymentDivide.orderItemNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(payment.paymentFshDt)), CustomConstants.YYYYMMDD), payment.paymentFshDt),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.SUM, "payment_amt"),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, "claim_amt")
                    ),
                    "payment_amt"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.SUM, "item_qty"),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, "claim_qty")
                    ),
                    "payment_qty"
                )
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "Z"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT_EQUAL, SqlUtils.nonAlias(payment.paymentFshDt), "NOW()")
            )
            .GROUP_BY(SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo));
        log.debug("selectPayCompleteByDayLimit ::: \n[{}]", query.toString());
        return query.toString();
    }
}
