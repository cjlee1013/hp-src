package kr.co.homeplus.claim.entity.claim;

public class ClaimMultiBundleEntity {
    // 다중배송 클레임 번들 번호
    public String claimMultiBundleNo;
    // 클레임 그룹 번호
    public String claimNo;
    // 클레임 번들 번호
    public String claimBundleNo;
    // 주문번호
    public String purchaseOrderNo;
    // 다중배송 번호
    public String multiBundleNo;
    // 등록일
    public String regDt;
}
