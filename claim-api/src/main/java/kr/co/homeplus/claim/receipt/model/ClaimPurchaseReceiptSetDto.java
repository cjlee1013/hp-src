package kr.co.homeplus.claim.receipt.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "클레임_구매영수증 조회 DTO")
public class ClaimPurchaseReceiptSetDto {
    @ApiModelProperty(value = "클레임번호", position = 1)
    private long claimNo;
    @ApiModelProperty(value = "고객번호", position = 2)
    private long userNo;
}
