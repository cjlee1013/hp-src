package kr.co.homeplus.claim.entity.claim;

public class ProcessHistoryEntity {
  
    // 처리 히스토리 번호
    public String processHistoryNo;

    // 클레임번호(환불:클레임번호)
    public String claimNo;

    // 클레임 묶음 배송번호
    public String claimBundleNo;

    // 클레임 반품수거 번호
    public String claimPickShippingNo;

    // 클레임 교환배송 번호
    public String claimExchShippingNo;

    // 구매주문번호
    public String purchaseOrderNo;

    // 번들번호
    public String bundleNo;

    // 배송번호
    public String shipNo;

    // 히스토리 등록 사유
    public String historyReason;

    // 히스토리 상세내역
    public String historyDetailDesc;

    // 히스토리 이전 상세내역
    public String historyDetailBefore;

    // 처리 채널
    public String regChannel;

    // 등록자 ID
    public String regId;

    // 등록일시
    public String regDt;
    // 박스번호
    public String multiBundleNo;
}
