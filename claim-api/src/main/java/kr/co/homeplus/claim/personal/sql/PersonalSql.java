package kr.co.homeplus.claim.personal.sql;

import java.util.Locale;
import kr.co.homeplus.claim.constants.Constants;
import kr.co.homeplus.claim.constants.CustomConstants;
import kr.co.homeplus.claim.entity.dms.ItmPartnerEntity;
import kr.co.homeplus.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claim.entity.order.PaymentEntity;
import kr.co.homeplus.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claim.enums.MysqlFunction;
import kr.co.homeplus.claim.factory.EntityFactory;
import kr.co.homeplus.claim.personal.model.PersonalSetDto;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class PersonalSql {

    public static String selectPersonalInfoUsingList(PersonalSetDto personalSetDto) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        ItmPartnerEntity itmPartner = EntityFactory.createEntityIntoValue(ItmPartnerEntity.class, Boolean.TRUE);

        String useContents;
        String purpose;
        String customConstants = Constants.EQUAL;
        switch (personalSetDto.getPersonalType().toUpperCase(Locale.KOREA)){
            case "PROD" :
                useContents = "'성명, 휴대폰번호, 수령인정보(성명,휴대폰번호,주소),주문정보'";
                purpose = "'상품배송 및 CS업무'";
                customConstants = CustomConstants.NOT_EQUAL;
                break;
            case "LIQUOR" :
                useContents = "'성명,주소,휴대폰번호,구매일자,상품명,구매수량,구매금액'";
                purpose = "'전통주 판매 및 배송'";
                break;
            default:
                useContents = "";
                purpose = "";
                break;
        }

        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(purchaseOrder.orderDt, CustomConstants.YYYYMMDD_COMMA), "use_dt"),
                SqlUtils.aliasToRow(useContents, "useContents"),
                SqlUtils.aliasToRow(purpose, "purpose"),
                SqlUtils.subQuery(itmPartner.partnerNm, EntityFactory.getTableNameWithAlias("dms", itmPartner), ObjectUtils.toArray(SqlUtils.equalColumn(itmPartner.partnerId, orderItem.partnerId)), "consignment")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.orderItemNo, orderItem.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, personalSetDto.getUserNo()),
                SqlUtils.betweenDay(purchaseOrder.orderDt, personalSetDto.getSchStartDt(), personalSetDto.getSchEndDt()),
                SqlUtils.equalColumnByInput(purchaseOrder.siteType, personalSetDto.getSiteType()),
                SqlUtils.customOperator(customConstants, orderItem.adultType, "'LOCAL_LIQUOR'")
            )
            .ORDER_BY(SqlUtils.orderByesc(purchaseOrder.orderDt));

        if(personalSetDto.getPersonalType().equalsIgnoreCase("PROD")){
            query.WHERE(SqlUtils.sqlFunction(MysqlFunction.LIKE, ObjectUtils.toArray(shippingItem.shipMethod, "DS%")));
        }

        if(personalSetDto.getPage() != 0){
            query.LIMIT(personalSetDto.getPerPage())
                .OFFSET((personalSetDto.getPage() - 1) * personalSetDto.getPerPage());
        }

        log.debug("selectPersonalInfoUsingList ::: \n[{}]", query.toString());
        return query.toString();
    }

}
