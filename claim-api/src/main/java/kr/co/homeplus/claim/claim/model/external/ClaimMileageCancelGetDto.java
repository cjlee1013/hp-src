package kr.co.homeplus.claim.claim.model.external;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "마일리지 취소 응답")
public class ClaimMileageCancelGetDto {

    @ApiModelProperty(value = "유저번호", position = 1)
    private String userNo;

    @ApiModelProperty(value = "마일리지 잔액", position = 2)
    private long mileageAmt;

    @ApiModelProperty(value = "승인번호", position = 3)
    private String approvalNo;

}
