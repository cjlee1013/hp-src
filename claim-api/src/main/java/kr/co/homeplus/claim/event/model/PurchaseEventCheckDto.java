package kr.co.homeplus.claim.event.model;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@ApiModel(description = "이벤트 주문번호 체크 DTO")
public class PurchaseEventCheckDto {
    private boolean validPurchaseOrderNo;
}
