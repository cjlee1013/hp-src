package kr.co.homeplus.claim.claim.model.register;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "클레임 교환 등록 DTO")
public class ClaimExchShippingRegDto {

    @ApiModelProperty(value= "클레임 교환배송 번호")
    private long claimExchShippingNo;

    @ApiModelProperty(value= "클레임 반품수거 번호")
    private long claimPickShippingNo;

    @ApiModelProperty(value= "클레임 묶음번호(환불:클레임 번호)")
    private long claimBundleNo;

    @ApiModelProperty(value= "교환배송수단(C:택배배송 P:우편배송  D:업체직배송 N:배송없음)")
    private String exchShipType;

    @ApiModelProperty(value= "교환 배송_상태(D0: 상품출고대기 D1: 상품출고(송장등록) D2: 배송중 D3: 배송완료)")
    private String exchStatus;

    @ApiModelProperty(value= "교환배송_택배사코드")
    private String exchDlvCd;

    @ApiModelProperty(value= "교환배송_송장번호")
    private String exchInvoiceNo;

    @ApiModelProperty(value= "교환배송_송장번호 등록자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)")
    private String exchInvoiceRegId;

    @ApiModelProperty(value= "배송추적요청여부")
    private String exchTrackingYn;

    @ApiModelProperty(value= "교환배송_이름")
    private String exchReceiverNm;

    @ApiModelProperty(value= "교환배송_우편번호")
    private String exchZipcode;

    @ApiModelProperty(value= "교환배송_주소1")
    private String exchBaseAddr;

    @ApiModelProperty(value= "교환배송_주소2")
    private String exchDetailAddr;

    @ApiModelProperty(value= "교환배송_도로명 기본주소")
    private String exchRoadBaseAddr;

    @ApiModelProperty(value= "교환배송_도로명 상세주소")
    private String exchRoadDetailAddr;

    @ApiModelProperty(value= "교환배송_휴대폰")
    private String exchMobileNo;

    @ApiModelProperty(value= "교환배송_연락처")
    private String exchPhone;

    @ApiModelProperty(value= "교환배송_메모")
    private String exchMemo;

    @ApiModelProperty(value= "업체직배송의 배송예정일 최초등록일")
    private String exchD0Dt;

    @ApiModelProperty(value= "교환배송_송장등록일(업체직배송의 경우 배송예정일)")
    private String exchD1Dt;

    @ApiModelProperty(value= "교환배송_시작일")
    private String exchD2Dt;

    @ApiModelProperty(value= "교환배송_완료일")
    private String exchD3Dt;

    @ApiModelProperty(value= "고객번호")
    private long userNo;

    @ApiModelProperty(value= "파트너ID")
    private String partnerId;

    @ApiModelProperty(value= "등록자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)")
    private String regId;

    @ApiModelProperty(value= "등록일")
    private String regDt;

    @ApiModelProperty(value= "수정자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)")
    private String chgId;

    @ApiModelProperty(value= "수정일")
    private String chgDt;

    @ApiModelProperty(value= "등록ID", hidden = true)
    private String safetyUseYn;
}
