package kr.co.homeplus.claim.event.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "이벤트 주문완료(기준금액) 체크 응답 DTO")
public class PayCompleteByAmtCheckGetDto {
    @ApiModelProperty(value = "유저번호")
    private long userNo;
    @ApiModelProperty(value = "주문완료건")
    private int completeQty;
}
