package kr.co.homeplus.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ExternalInfo {
    PAYMENT_CANCEL_PG("payment", "/pg/pgCancel"),
    PAYMENT_CANCEL_MILEAGE("mileage", "/cancel/cancelMileageRestore"),
    PAYMENT_SAVE_CANCEL_MILEAGE("mileage", "/external/external/mileageCancel"),
    PAYMENT_CANCEL_OCB("payment", "/ocb/cancelOcbPoint"),
    PAYMENT_CANCEL_MHC_ALL("escrow", "/mhc/external/cancelMhcPoint"),
    PAYMENT_CANCEL_MHC_PART("escrow", "/mhc/external/partCancelMhcPoint"),
    PAYMENT_SAVE_CANCEL_MHC_ALL("escrow", "/mhc/external/saveCancelMhcPoint"),
    PAYMENT_SAVE_CANCEL_MHC_PART("escrow", "/mhc/external/partSaveCancelMhcPoint"),
    PAYMENT_CANCEL_DGV("payment", "/dgv/cancelDgvGiftCard"),
    ESCROW_ORDER_SAFETY_ISSUE_UPDATE("escrow", "/safetyissue/updateSafetyNo"),
    ESCROW_ORDER_MULTI_SAFETY_ISSUE_UPDATE("escrow", "/safetyissue/updateMultiSafetyNo"),
    PAYMENT_CANCEL_DGV_RECEIPT("escrow", "/dgv/external/dgvPosReceiptIssue"),
    RESTORE_SLOT_CLAIM("escrow", "/slot/external/restoreSlotStockForClaim"),
    USER_MHC_CARD_INFO("user", "/search/mypage/getMhcInfo"),
    USER_API_USER_GRADE_INFO("user", "/search/mypage/getGradeInfo"),
    SHIPPING_CLAIM_FROM_SHIP_START("shipping", "/external/store/setShipStart"),
    SHIPPING_CLAIM_FROM_SHIP_COMPLETE("shipping", "/admin/shipManage/setShipCompleteByBundle"),
    SHIPPING_CLAIM_FROM_SHIP_DECISION("shipping", "/admin/shipManage/setPurchaseCompleteByBundle"),
    EMP_DISCOUNT_LIMIT_AMT_INFO("escrow", "/ordinis/external/getEmpInfo"),
    CLAIM_BY_COUPON_RE_ISSUE("front","/coupon/escrow/setCouponReIssued"),
    MESSAGE_SEND_ALIM_TALK("message", "/send/alimtalk"),
    CLAIM_SAFETY_ISSUE_ADD("escrow", "/safetyissue/addClaimSafetyNo"),
    CLAIM_SAFETY_ISSUE_MODIFY("escrow", "/safetyissue/updateClaimSafetyNo"),
    ESCROW_SUBSTITUTION_PROCESS("escrow", "/ordinis/substitutionProcess"),
    ESCROW_SUBSTITUTION_COMPLETE("escrow", "/ordinis/substitutionPaymentComplete"),
    ESCROW_SUBSTITUTION_FAIL("escrow", "/ordinis/substitutionPaymentFail"),
    OUTBOUND_COOP_TICKET_CHECK_FOR_CLAIM("outbound", "/coop/cancelTicketInfo"),
    OUTBOUND_COOP_TICKET_CANCEL_FOR_CLAIM("outbound", "/coop/cancelTicket"),
    FORWARD_OMNI_TRANS("forward", "/claim/toOmni"),
    SLOT_STOCK_CHANGE_AHEAD("escrow", "/ordinis/external/reserveOrderSlotStock"),
    SLOT_STOCK_CHANGE_CONFIRM("escrow", "/ordinis/external/confirmOrderSlotStock"),
    OUTBOUND_MARKET_NAVER_CANCEL_REG("outbound", "/market/naver/setCancelSale"),
    OUTBOUND_MARKET_NAVER_RETURN_REG("outbound", "/market/naver/setReturnSale"),
    OUTBOUND_MARKET_NAVER_CLAIM_APPROVE("outbound", "/market/naver/setClaimApprove"),
    OUTBOUND_MARKET_NAVER_CLAIM_REJECT("outbound", "/market/naver/setClaimReject"),
    OUTBOUND_MARKET_NAVER_CLAIM_HOLD("outbound", "/market/naver/setClaimHold"),
    OUTBOUND_MARKET_NAVER_CLAIM_WITHDRAW("outbound", "/market/naver/setClaimWithdraw"),
    OUTBOUND_MARKET_ELEVEN_CANCEL_REG("outbound", "/market/eleven/setCancelSale"),
    ;

    @Getter
    private final String apiId;

    @Getter
    private final String uri;

}
