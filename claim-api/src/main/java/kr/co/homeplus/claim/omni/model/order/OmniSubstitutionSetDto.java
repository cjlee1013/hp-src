package kr.co.homeplus.claim.omni.model.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "대체주문 요청 DTO")
public class OmniSubstitutionSetDto {
    @ApiModelProperty(value = "고객번호", position = 1)
    private long userNo;
    @ApiModelProperty(value = "원주문번호", position = 2)
    private long originPurchaseOrderNo;
    @ApiModelProperty(value = "대체주문상품리스트", position = 3)
    private List<OmniSubstitutionItemDto> substitutionOrderItemParamList;
    @ApiModelProperty(value = "PG금액", position = 4)
    private long pgAmt;
    @ApiModelProperty(value = "마일리지금액", position = 5)
    private long mileageAmt;
    @ApiModelProperty(value = "MHC금액", position = 6)
    private long mhcAmt;
    @ApiModelProperty(value = "DGV금액", position = 7)
    private long dgvAmt;
    @ApiModelProperty(value = "OCB금액", position = 8)
    private long ocbAmt;
}
