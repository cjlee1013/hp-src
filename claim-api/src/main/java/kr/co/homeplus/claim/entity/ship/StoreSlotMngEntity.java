package kr.co.homeplus.claim.entity.ship;

public class StoreSlotMngEntity {

    // 점포ID
    public String storeId;

    // 배송일자(년-월-일 00:00:00)
    public String shipDt;

    // SHIFTID
    public String shiftId;

    // SLOTID
    public String slotId;

    // 사용여부(Y:사용,N:사용안함)
    public String useYn;

    // SLOT배송시작시간
    public String slotShipStartTime;

    // SLOT배송종료시간
    public String slotShipEndTime;

    // SLOT배송마감시간(SLOT주문종료시간)
    public String slotShipCloseTime;

    // 주문고정건수(점포주문 : 주문기준건수)
    public String orderFixCnt;

    // 주문예약건수(점포주문 : 주문실제건수)
    public String orderCnt;

    // 주문변경건수(점포주문 : 주문최대건수) Slot 주문초과 금지
    public String orderChgCnt;

    // 픽업주문고정건수(픽업주문:주문기준건수)
    public String pickupOrderFixCnt;

    // 픽업주문예약건수(픽업주문:주문실제건수)
    public String pickupOrderCnt;

    // 픽업주문변경건수(픽업주문:주문최대건수)
    public String pickupOrderChgCnt;

    // 사물함주문고정건수(사물함주문:주문기준건수)
    public String lockerOrderFixCnt;

    // 사물함주문예약건수(사물함주문:주문실제건수)
    public String lockerOrderCnt;

    // 사물함주문변경건수(사물함주문:주문최대건수)
    public String lockerOrderChgCnt;

    // 예약주문고정건수(사전예약주문:주문기준건수)
    public String reserveOrderFixCnt;

    // 예약주문변경건수(사전예약주문:주문최대건수)
    public String reserveOrderChgCnt;

    // SLOT마감일(SLOTCLOSE_D_DAY 슬랏종료D-DAY 배송일과의차) (사용처모름)
    public String slotCloseDay;

    // 등록자ID
    public String regId;

    // 등록일자
    public String regDt;

    // 수정자ID
    public String chgId;

    //
    public String chgDt;

}
