package kr.co.homeplus.claim.claim.model.mp;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimRefundType {

    @ApiModelProperty(value = "클레임 결제번호", position = 1)
     private String claimPaymentNo;

     @ApiModelProperty(value = "환불수단", position = 2)
     private String refundType;

     @ApiModelProperty(value = "환불수단", position = 3)
     private String refundTypeCode;

     @ApiModelProperty(value = "환불수단명", position = 4)
     private String methodNm;

     @ApiModelProperty(value="카드번호", position = 5)
     private String cardNo;

     @ApiModelProperty(value="카드 할부개월스", position = 6)
     private String cardInstallmentMonth;

     @ApiModelProperty(value = "환불상태", position = 7)
     private String paymentStatus;

     @ApiModelProperty(value = "환불금액", position = 8)
     private String paymentAmt;

     @ApiModelProperty(value = "환불등록일", position = 9)
     private String regDt;
}
