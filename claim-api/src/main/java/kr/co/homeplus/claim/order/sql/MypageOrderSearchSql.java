package kr.co.homeplus.claim.order.sql;

import kr.co.homeplus.claim.constants.CustomConstants;
import kr.co.homeplus.claim.entity.claim.ClaimAccumulateEntity;
import kr.co.homeplus.claim.entity.claim.ClaimAdditionEntity;
import kr.co.homeplus.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claim.entity.claim.ClaimExchShippingEntity;
import kr.co.homeplus.claim.entity.claim.ClaimInfoEntity;
import kr.co.homeplus.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claim.entity.claim.ClaimMstEntity;
import kr.co.homeplus.claim.entity.claim.ClaimMultiBundleEntity;
import kr.co.homeplus.claim.entity.claim.ClaimMultiItemEntity;
import kr.co.homeplus.claim.entity.claim.ClaimOptEntity;
import kr.co.homeplus.claim.entity.claim.ClaimPaymentEntity;
import kr.co.homeplus.claim.entity.claim.ClaimPickShippingEntity;
import kr.co.homeplus.claim.entity.claim.ClaimReqEntity;
import kr.co.homeplus.claim.entity.claim.ClaimTicketEntity;
import kr.co.homeplus.claim.entity.dms.ItmPartnerSellerEntity;
import kr.co.homeplus.claim.entity.dms.ItmPartnerSellerShopEntity;
import kr.co.homeplus.claim.entity.dms.ItmPartnerStoreEntity;
import kr.co.homeplus.claim.entity.multi.MultiBundleEntity;
import kr.co.homeplus.claim.entity.multi.MultiOrderItemEntity;
import kr.co.homeplus.claim.entity.multi.MultiSafetyIssueEntity;
import kr.co.homeplus.claim.entity.multi.MultiShippingAddrEntity;
import kr.co.homeplus.claim.entity.order.BundleEntity;
import kr.co.homeplus.claim.entity.order.OrderDiscountEntity;
import kr.co.homeplus.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claim.entity.order.OrderOptEntity;
import kr.co.homeplus.claim.entity.order.OrderPaymentDivideEntity;
import kr.co.homeplus.claim.entity.order.OrderPickupInfoEntity;
import kr.co.homeplus.claim.entity.order.OrderPromoEntity;
import kr.co.homeplus.claim.entity.order.OrderTicketEntity;
import kr.co.homeplus.claim.entity.order.OrderUserRecentlyInfoEntity;
import kr.co.homeplus.claim.entity.order.PaymentEntity;
import kr.co.homeplus.claim.entity.order.PaymentInfoEntity;
import kr.co.homeplus.claim.entity.order.PurchaseAccumulateEntity;
import kr.co.homeplus.claim.entity.order.PurchaseGiftEntity;
import kr.co.homeplus.claim.entity.order.PurchaseGiftItemEntity;
import kr.co.homeplus.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claim.entity.order.PurchaseStoreInfoEntity;
import kr.co.homeplus.claim.entity.order.ShippingMileagePaybackEntity;
import kr.co.homeplus.claim.entity.ship.SafetyIssueEntity;
import kr.co.homeplus.claim.entity.ship.ShippingAddrEntity;
import kr.co.homeplus.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claim.entity.ship.ShippingNonRcvEntity;
import kr.co.homeplus.claim.entity.ship.StoreShiftMngEntity;
import kr.co.homeplus.claim.entity.ship.StoreSlotMngEntity;
import kr.co.homeplus.claim.enums.MysqlFunction;
import kr.co.homeplus.claim.factory.EntityFactory;
import kr.co.homeplus.claim.order.model.mypage.MpOrderDetailListSetDto;
import kr.co.homeplus.claim.order.model.mypage.MpOrderListSetDto;
import kr.co.homeplus.claim.order.model.mypage.MultiOrderShipAddrEditDto;
import kr.co.homeplus.claim.order.model.mypage.MultiOrderShipBasicInfoEditDto;
import kr.co.homeplus.claim.order.model.mypage.OrderShipAddrEditDto;
import kr.co.homeplus.claim.order.model.mypage.multi.MpMultiOrderDetailInfoSetDto;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MypageOrderSearchSql {

    public static String selectMpOrderList(MpOrderListSetDto dto){
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        OrderPromoEntity orderPromo = EntityFactory.createEntityIntoValue(OrderPromoEntity.class, Boolean.TRUE);
        PurchaseStoreInfoEntity purchaseStoreInfo = EntityFactory.createEntityIntoValue(PurchaseStoreInfoEntity.class, Boolean.TRUE);
        //        StoreSlotMngEntity storeSlotMng = EntityFactory.createEntityIntoValue(StoreSlotMngEntity.class, Boolean.TRUE);
        StoreShiftMngEntity storeShiftMng = EntityFactory.createEntityIntoValue(StoreShiftMngEntity.class, Boolean.TRUE);
        ItmPartnerSellerEntity itmPartnerSeller = EntityFactory.createEntityIntoValue(ItmPartnerSellerEntity.class, Boolean.TRUE);

        SQL basicQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IS_NULL, purchaseOrder.orgPurchaseOrderNo), purchaseOrder.purchaseOrderNo, purchaseOrder.orgPurchaseOrderNo), purchaseOrder.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IS_NULL, bundle.orgBundleNo), bundle.bundleNo, bundle.orgBundleNo), bundle.bundleNo),
                SqlUtils.aliasToRow(orderItem.purchaseOrderNo, "order_purchase_order_no"),
                SqlUtils.aliasToRow(orderItem.bundleNo, "order_bundle_no"),
                orderItem.storeType,
                orderItem.orderItemNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        orderItem.storeId,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray("NOW()", "%s%i")), itmPartnerSeller.sellerSeq, SqlUtils.sqlFunction(MysqlFunction.RIGHT, ObjectUtils.toArray("RAND()", 2)))))
                                .FROM(EntityFactory.getTableNameWithAlias("dms", itmPartnerSeller))
                                .WHERE(
                                    SqlUtils.equalColumn(itmPartnerSeller.partnerId, orderItem.partnerId)
                                )
                        )
                    ), orderItem.partnerId
                ),
                shippingItem.shipMethod,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.originStoreId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "partner_nm"
                ),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(purchaseOrder.orderDt, CustomConstants.YYYYMMDD), purchaseOrder.orderDt),
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(payment.paymentStatus, "P5"), "'P3'", payment.paymentStatus), payment.paymentStatus),
                SqlUtils.sqlFunction(MysqlFunction.FN_ESTIMATED_DELIVERY, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, orderItem.orderItemNo), "ship_dt"),
                shippingItem.shipStatus,
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.PLUS,
                            ObjectUtils.toArray(storeShiftMng.shipDt, SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(storeShiftMng.shiftShipStartTime, "'00'"))))
                        , CustomConstants.HHMI), "ship_start_time"),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.PLUS,
                            ObjectUtils.toArray(storeShiftMng.shipDt, SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(storeShiftMng.shiftShipEndTime, "'00'"))))
                        , CustomConstants.HHMI), "ship_end_time"),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.PLUS,
                            ObjectUtils.toArray(
                                storeShiftMng.shipDt,
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CONCAT,
                                    ObjectUtils.toArray(
                                        SqlUtils.sqlFunction(
                                            MysqlFunction.CASE,
                                            ObjectUtils.toArray(
                                                SqlUtils.sqlFunction(MysqlFunction.IS_NULL, storeShiftMng.shiftBeforeCloseTime),
                                                storeShiftMng.shiftCloseTime,
                                                storeShiftMng.shiftBeforeCloseTime
                                            )
                                        ),
                                        "'00'"
                                    )
                                )
                            )
                        )
                        , CustomConstants.HHMI
                    ),
                    "ship_close_time"
                ),
                SqlUtils.aliasToRow(purchaseOrder.orderType),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")),
                        purchaseOrder.cmbnYn,
                        "'N'"
                    ),
                    purchaseOrder.cmbnYn
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")),
                        purchaseOrder.substitutionOrderYn,
                        "'N'"
                    ),
                    purchaseOrder.substitutionOrderYn
                ),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, "'M'"), "ship_type"),
                orderItem.reviewYn,
                bundle.prepaymentYn
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.orderItemNo, orderItem.bundleNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(orderItem.bundleNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(orderPromo, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, orderItem.promoNo, orderItem.promoDetailNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(purchaseStoreInfo, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.originStoreId, bundle.purchaseStoreInfoNo), ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.NOT_EQUAL, purchaseStoreInfo.storeKind, "'DLV'")))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(storeShiftMng, ObjectUtils.toArray( purchaseStoreInfo.storeId, orderItem.storeType, purchaseStoreInfo.shiftId, purchaseStoreInfo.shipDt))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    SqlUtils.sqlFunction(MysqlFunction.CLAIM_SUB_TABLE, ObjectUtils.toArray(String.valueOf(dto.getUserNo()))),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(orderItem.purchaseOrderNo, "claimSubData.purchase_order_no"),
                        SqlUtils.equalColumn(orderItem.bundleNo, "claimSubData.bundle_no")
                    )
                )
            )
            .WHERE(
                SqlUtils.betweenDay(purchaseOrder.orderDt, dto.getSchStartDt(), dto.getSchEndDt()),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, dto.getUserNo()),
                SqlUtils.equalColumnByInput(purchaseOrder.siteType, dto.getSiteType())
            )
            .AND()
            .WHERE(SqlUtils.sqlFunction(MysqlFunction.IS_NULL, "claimSubData.claim_part_yn"))
            .OR()
            .WHERE(SqlUtils.customOperator(CustomConstants.NOT_EQUAL, "claimSubData.claim_part_yn", "'N'"));

        SQL query = new SQL()
            .SELECT(
                "orderData.purchase_order_no",
                "orderData.bundle_no",
                SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.ship_method", "ship_method"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.store_type", "store_type"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.partner_id", "partner_id"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.partner_nm", "partner_nm"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.order_dt", "order_dt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.payment_status", "payment_status"),
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, payment.paymentStatus, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.payment_status")), "payment_status_nm"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.ship_status", "ship_status"),
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, shippingItem.shipStatus, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.ship_status")), "ship_status_nm"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.ship_dt", "ship_dt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.ship_start_time", "ship_start_time"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.ship_end_time", "ship_end_time"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.ship_close_time", "ship_close_time"),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray("orderData.purchase_order_no", "orderData.bundle_no", "'M'"), "ship_type"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.order_type", "order_type"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.order_type"), "'ORD_COMBINE'")
                        , SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.cmbn_yn")
                        , "'Y'"
                    ),
                    purchaseOrder.cmbnYn
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.order_type"), "'ORD_SUB'")
                        , SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.substitution_order_yn")
                        , "'Y'"
                    ),
                    purchaseOrder.substitutionOrderYn
                ),
                SqlUtils.sqlFunction(MysqlFunction.MIN, "orderData.review_yn", "review_yn"),
                SqlUtils.sqlFunction(MysqlFunction.MIN, "orderData.prepayment_yn", "prepayment_yn")
            )
            .FROM(SqlUtils.subTableQuery(basicQuery, "orderData"))
            .GROUP_BY("orderData.purchase_order_no", "orderData.bundle_no")
            .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.order_dt"), "orderData.purchase_order_no", SqlUtils.sqlFunction(MysqlFunction.MAX, "orderData.ship_type"), "orderData.bundle_no")))
            .LIMIT(dto.getPerPage())
            .OFFSET((dto.getPage() - 1) * dto.getPerPage());

        log.debug("selectMpOrderList \n[{}]", SqlUtils.orQuery(query.toString()));
        return SqlUtils.orQuery(query.toString());
    }

    public static String selectMpOrderListInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo, @Param("userNo") long userNo,  @Param("isCmbn") Boolean isCmbn, @Param("isSubYn") Boolean isSubYn) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        OrderPromoEntity orderPromo = EntityFactory.createEntityIntoValue(OrderPromoEntity.class, Boolean.TRUE);
        PurchaseStoreInfoEntity purchaseStoreInfo = EntityFactory.createEntityIntoValue(PurchaseStoreInfoEntity.class, Boolean.TRUE);
        StoreSlotMngEntity storeSlotMng = EntityFactory.createEntityIntoValue(StoreSlotMngEntity.class, Boolean.TRUE);
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class, Boolean.TRUE);


        SQL query = new SQL()
            .SELECT(
                orderItem.purchaseOrderNo,
                orderItem.mallType,
                orderItem.orderItemNo,
                orderItem.itemNo,
                orderItem.itemNm1,
                orderItem.orgItemNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(orderItem.itemQty, SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray("claimItemData.claim_item_qty", 0)))), "0"),
                        SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(orderItem.itemQty, SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray("claimItemData.claim_item_qty", 0)))),
                        "0"
                    ),
                    "item_qty"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.simpOrgItemPrice, "'0'"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NULL, ObjectUtils.toArray(orderItem.promoNo)),
                                orderItem.simpOrgItemPrice,
                                orderItem.itemPrice
                            )
                        ),
                        orderItem.itemPrice
                    ),
                    "item_price"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, shippingItem.reserveShipDt),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.reserveShipDt, CustomConstants.YYYYMMDD_COMMA)),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, shippingItem.scheduleShipDt),
                                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.scheduleShipDt, CustomConstants.YYYYMMDD_COMMA)),
                                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.shippingDt, CustomConstants.YYYYMMDD_COMMA))
                            )
                        )
                    ),
                    shippingItem.shippingDt
                ),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.completeDt, CustomConstants.YYYYMMDD_COMMA), shippingItem.completeDt),
                shippingItem.shipStatus,
//                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, shippingNonRcv.purchaseOrderNo), "'Y'", "'N'"), "no_rcv_yn"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, shippingNonRcv.noRcvProcessYn),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "Y"),
                                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "Y"), "'W'", "'C'")),
                                "'Y'"
                            )
                        )
                        , "'N'"
                    ),
                    "no_rcv_yn"
                ),
                shippingItem.shipNo,
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, shippingItem.delayShipCd, ObjectUtils.toArray(shippingItem.delayShipCd), shippingItem.delayShipCd),
                shippingItem.delayShipMsg,
                orderItem.reviewYn,
                orderItem.bundleNo,
                bundle.prepaymentYn,
                orderItem.saleUnit,
                purchaseOrder.paymentNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.orderItemNo, orderItem.bundleNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(shippingNonRcv, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(orderItem.bundleNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(orderPromo, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, orderItem.promoNo, orderItem.promoDetailNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(purchaseStoreInfo, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.originStoreId, bundle.purchaseStoreInfoNo), ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.NOT_EQUAL, purchaseStoreInfo.storeKind, "'DLV'")))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(storeSlotMng, ObjectUtils.toArray( purchaseStoreInfo.storeId, purchaseStoreInfo.slotId, purchaseStoreInfo.shiftId, purchaseStoreInfo.shipDt))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    SqlUtils.sqlFunction(MysqlFunction.CLAIM_ITEM_SUB_TABLE, ObjectUtils.toArray(String.valueOf(userNo))),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(orderItem.purchaseOrderNo, "claimItemData.purchase_order_no"),
                        SqlUtils.equalColumn(orderItem.bundleNo, "claimItemData.bundle_no"),
                        SqlUtils.equalColumn(orderItem.itemNo, "claimItemData.item_no")
                    )
                )
            );

        if(isCmbn){
            query.WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE")
            );
        } else {
            if(isSubYn){
                query.WHERE(
                    SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                    SqlUtils.OR(
                        SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo),
                        SqlUtils.AND(
                            SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo),
                            SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB"),
                            SqlUtils.equalColumnByInput(bundle.orgBundleNo, bundleNo)
                        )
                    )
                );
            } else {
                query.WHERE(
                    SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                    SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo)
                );
            }
        }

        query.ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.mallType, orderItem.bundleNo, orderItem.orderItemNo, orderPromo.promoKind)));
        log.debug("selectMpOrderListInfo ::: \n[{}]", query.toString());
        return query.toString();
    }

    // 배송
    public static String selectMpOrderShipInfo(MpOrderDetailListSetDto detailListSetDto){
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        PurchaseStoreInfoEntity purchaseStoreInfo = EntityFactory.createEntityIntoValue(PurchaseStoreInfoEntity.class, Boolean.TRUE);
        StoreSlotMngEntity storeSlotMng = EntityFactory.createEntityIntoValue(StoreSlotMngEntity.class, Boolean.TRUE);
        SafetyIssueEntity safetyIssue = EntityFactory.createEntityIntoValue(SafetyIssueEntity.class, "safety");
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL shippingTdQuery = new SQL()
            .SELECT(
                SqlUtils.aliasToRow(shippingItem.purchaseOrderNo, "td_purchase_order_no"),
                shippingAddr.storeShipMsgCd,
                shippingAddr.storeShipMsg,
                shippingAddr.auroraShipMsgCd,
                shippingAddr.auroraShipMsg
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingItem))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.purchaseOrderNo, detailListSetDto.getPurchaseOrderNo()),
                SqlUtils.equalColumnByInput(shippingItem.userNo, detailListSetDto.getUserNo()),
                SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'"))
            )
            .ORDER_BY(SqlUtils.orderByesc(shippingAddr.chgDt))
            .LIMIT(1)
            .OFFSET(0);

        SQL shippingDlvQuery = new SQL()
            .SELECT(
                SqlUtils.aliasToRow(shippingItem.purchaseOrderNo, "dlv_purchase_order_no"),
                shippingAddr.dlvShipMsgCd,
                shippingAddr.dlvShipMsg
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingItem))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.purchaseOrderNo, detailListSetDto.getPurchaseOrderNo()),
                SqlUtils.equalColumnByInput(shippingItem.userNo, detailListSetDto.getUserNo()),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'"))
            )
            .ORDER_BY(SqlUtils.orderByesc(shippingAddr.chgDt))
            .LIMIT(1)
            .OFFSET(0);

        SQL shippingBasicQuery = new SQL()
            .SELECT(
                shippingItem.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.purchaseOrderNo, shippingItem.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.receiverNm, shippingAddr.receiverNm),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.shipMobileNo, shippingAddr.shipMobileNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.safetyUseYn, shippingAddr.safetyUseYn),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.zipcode, shippingAddr.zipcode),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.roadBaseAddr, shippingAddr.roadBaseAddr),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.roadDetailAddr, shippingAddr.roadDetailAddr),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.personalOverseaNo, shippingAddr.personalOverseaNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.shipAreaType, bundle.shipAreaType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.giftMsg, shippingAddr.giftMsg),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.reserveShipDt, shippingItem.reserveShipDt),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.safetyUseYn), "Y"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE_WHEN,
                            SqlUtils.sqlFunction(MysqlFunction.MAX, safetyIssue.issueStatus),
                            ObjectUtils.toArray(
                                "'R'", "'발급 요청중'",
                                "'C'", SqlUtils.sqlFunction(MysqlFunction.MAX, safetyIssue.issuePhoneNo),
                                "'F'", SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, safetyIssue.issueType), "N"), "'발급실패'", "'변경실패'"))
                            )
                        ),
                        "'신청안함'"
                    ),
                    "safety_nm"
                ),
                SqlUtils.aliasToRow(SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.dsGiftMsg), "present_msg"),
                SqlUtils.aliasToRow(SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.giftAcpDt), "present_acp_dt"),
                SqlUtils.aliasToRow(SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.dsGiftAutoCancelYn), "present_auto_cancel_yn"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CONCAT,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.dsGiftAutoCancelDt), "%m월 %d일")),
                        "' ('",
                        SqlUtils.sqlFunction(MysqlFunction.SUBSTR, ObjectUtils.toArray("'일월화수목금토'", SqlUtils.sqlFunction(MysqlFunction.DAYOFWEEK, SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.dsGiftAutoCancelDt)), "1")),
                        "'요일)'"
                    ),
                    "present_expire_dt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.sqlFunction(MysqlFunction.IS_NULL, SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.giftAcpDt)), "present_receive_yn"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.driverNm, bundle.driverNm),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.driverTelNo, bundle.driverTelNo),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseStoreInfo.anytimeSlotYn), "'N'" ), purchaseStoreInfo.anytimeSlotYn)
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingItem))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(shippingItem.purchaseOrderNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(purchaseStoreInfo, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(safetyIssue, "safety"),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(shippingItem.shipAddrNo, shippingItem.purchaseOrderNo, shippingItem.bundleNo ), safetyIssue)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.purchaseOrderNo, detailListSetDto.getPurchaseOrderNo()),
                SqlUtils.equalColumnByInput(shippingItem.userNo, detailListSetDto.getUserNo())
            )
            .GROUP_BY(shippingItem.bundleNo)
            .ORDER_BY(SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.chgDt))
            .LIMIT(1)
            .OFFSET(0);

        SQL query = new SQL()
            .SELECT("*")
            .FROM(SqlUtils.subTableQuery(shippingBasicQuery, "shippingBasic"))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    SqlUtils.subTableQuery(shippingTdQuery, "shippingTd"),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn("shippingBasic.purchase_order_no", "shippingTd.td_purchase_order_no")
                    )
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    SqlUtils.subTableQuery(shippingDlvQuery, "shippingDlv"),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn("shippingBasic.purchase_order_no", "shippingDlv.dlv_purchase_order_no")
                    )
                )
            );

        log.debug("selectMpOrderShipInfo ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMpMultiOrderShipList(MpOrderDetailListSetDto mpOrderDetailListSetDto) {
        MultiBundleEntity multiBundle = EntityFactory.createEntityIntoValue(MultiBundleEntity.class, Boolean.TRUE);
        MultiShippingAddrEntity multiShippingAddr = EntityFactory.createEntityIntoValue(MultiShippingAddrEntity.class, Boolean.TRUE);
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.MAX, multiBundle.purchaseOrderNo, multiBundle.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, multiBundle.bundleNo, multiBundle.bundleNo),
                multiShippingAddr.multiBundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, multiShippingAddr.roadBaseAddr, multiShippingAddr.roadBaseAddr),
                SqlUtils.sqlFunction(MysqlFunction.MAX, multiShippingAddr.roadDetailAddr, multiShippingAddr.roadDetailAddr),
                SqlUtils.sqlFunction(MysqlFunction.MAX, multiOrderItem.shipStatus, multiOrderItem.shipStatus),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.reserveShipDt, shippingItem.reserveShipDt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, multiShippingAddr.giftMsg, shippingAddr.giftMsg)
            )
            .FROM(EntityFactory.getTableNameWithAlias(multiBundle))
            .INNER_JOIN(SqlUtils.createJoinCondition(multiShippingAddr, ObjectUtils.toArray(multiBundle.multiBundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(multiOrderItem, ObjectUtils.toArray(multiShippingAddr.multiBundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(multiBundle.purchaseOrderNo, multiBundle.bundleNo, multiOrderItem.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(multiBundle.purchaseOrderNo, mpOrderDetailListSetDto.getPurchaseOrderNo())
            )
            .GROUP_BY(multiShippingAddr.multiBundleNo);
        log.debug("selectMpMultiOrderShipList ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMpOrderMultiShipInfo(MpMultiOrderDetailInfoSetDto mpMultiOrderDetailInfoSetDto){
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        MultiSafetyIssueEntity multiSafetyIssue = EntityFactory.createEntityIntoValue(MultiSafetyIssueEntity.class, Boolean.TRUE);
        MultiBundleEntity multiBundle = EntityFactory.createEntityIntoValue(MultiBundleEntity.class, Boolean.TRUE);
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        MultiShippingAddrEntity multiShippingAddr = EntityFactory.createEntityIntoValue(MultiShippingAddrEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT_DISTINCT(
                multiBundle.bundleNo,
                multiBundle.multiBundleNo,
                multiShippingAddr.receiverNm,
                multiShippingAddr.shipMobileNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(multiShippingAddr.safetyUseYn, "Y"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE_WHEN,
                            multiSafetyIssue.issueStatus,
                            ObjectUtils.toArray(
                                "'R'", "'발급 요청중'",
                                "'C'", multiSafetyIssue.issuePhoneNo,
                                "'F'", SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(multiSafetyIssue.issueType, "N"), "'발급실패'", "'변경실패'"))
                            )
                        ),
                        "'신청안함'"
                    ),
                    "safety_nm"
                ),
                multiShippingAddr.safetyUseYn,
                multiShippingAddr.zipcode,
                multiShippingAddr.roadBaseAddr,
                multiShippingAddr.roadDetailAddr,
                shippingAddr.personalOverseaNo,
                SqlUtils.aliasToRow(multiBundle.shipAreaType, multiBundle.shipAreaType),
                multiShippingAddr.giftMsg,
                multiBundle.shipPrice,
                multiBundle.islandShipPrice,
                orderItem.reserveShipMethod
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(multiOrderItem, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, orderItem.orderItemNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(multiBundle, ObjectUtils.toArray(multiOrderItem.multiBundleNo, multiOrderItem.purchaseOrderNo, multiOrderItem.bundleNo, orderItem.storeId))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(multiShippingAddr, ObjectUtils.toArray(multiBundle.multiBundleNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(multiShippingAddr.bundleNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(multiSafetyIssue, ObjectUtils.toArray(multiShippingAddr.multiShipAddrNo, multiShippingAddr.purchaseOrderNo, multiShippingAddr.bundleNo, multiShippingAddr.multiBundleNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, mpMultiOrderDetailInfoSetDto.getPurchaseOrderNo()),
                SqlUtils.equalColumnByInput(multiBundle.multiBundleNo, mpMultiOrderDetailInfoSetDto.getMultiBundleNo()),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, mpMultiOrderDetailInfoSetDto.getUserNo())
            );
        SQL queryOfSort = new SQL()
            .SELECT("*")
            .FROM("(".concat(query.toString()).concat(" ) sort"))
            .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray("sort.bundle_no", "sort.multi_bundle_no")));

        log.debug("selectMpOrderMultiShipInfo ::: [{}]", query.toString());
        return query.toString();
    }

    // 주문상세
    public static String selectMpOrderProductList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo, @Param("isCmbn") Boolean isCmbn){
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        OrderPromoEntity orderPromo = EntityFactory.createEntityIntoValue(OrderPromoEntity.class, Boolean.TRUE);
        PurchaseStoreInfoEntity purchaseStoreInfo = EntityFactory.createEntityIntoValue(PurchaseStoreInfoEntity.class, Boolean.TRUE);
        StoreSlotMngEntity storeSlotMng = EntityFactory.createEntityIntoValue(StoreSlotMngEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ItmPartnerSellerShopEntity itmPartnerSellerShop = EntityFactory.createEntityIntoValue(ItmPartnerSellerShopEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        ItmPartnerSellerEntity itmPartnerSeller = EntityFactory.createEntityIntoValue(ItmPartnerSellerEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                orderItem.purchaseOrderNo,
                orderItem.storeType,
                orderItem.mallType,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        orderItem.storeId,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray("NOW()", "%s%i")), itmPartnerSeller.sellerSeq, SqlUtils.sqlFunction(MysqlFunction.RIGHT, ObjectUtils.toArray("RAND()", 2)))))
                                .FROM(EntityFactory.getTableNameWithAlias("dms", itmPartnerSeller))
                                .WHERE(
                                    SqlUtils.equalColumn(itmPartnerSeller.partnerId, orderItem.partnerId)
                                )
                        )
                    ), orderItem.partnerId
                ),
                orderItem.orderItemNo,
                orderItem.itemNo,
                orderItem.itemNm1,
                shippingItem.shipMethod,
                shippingItem.shipNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.originStoreId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "partner_nm"
                ),
                orderItem.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(purchaseOrder.orderDt, CustomConstants.YYYYMMDD), "order_dt"),
                orderItem.orgItemNo,
                SqlUtils.aliasToRow(orderOpt.optQty, "order_item_qty"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.simpOrgItemPrice, "'0'"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NULL, ObjectUtils.toArray(orderItem.promoNo)),
                                orderItem.simpOrgItemPrice,
                                orderItem.itemPrice
                            )
                        ),
                        orderItem.itemPrice
                    ),
                    "item_price"
                ),
                orderOpt.orderOptNo,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.selOpt1Title, orderOpt.selOpt1Title),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.selOpt1Val, orderOpt.selOpt1Val),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.selOpt2Title, orderOpt.selOpt2Title),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.selOpt2Val, orderOpt.selOpt2Val),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.txtOpt1Title, orderOpt.txtOpt1Title),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.txtOpt1Val, orderOpt.txtOpt1Val),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.txtOpt2Title, orderOpt.txtOpt2Title),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.txtOpt2Val, orderOpt.txtOpt2Val),
                orderOpt.optNo,
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(payment.paymentStatus, "P5"), "'P3'", payment.paymentStatus), payment.paymentStatus),
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, payment.paymentStatus, ObjectUtils.toArray(payment.paymentStatus), "payment_status_nm"),
                shippingItem.shipStatus,
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, shippingItem.shipStatus, ObjectUtils.toArray(shippingItem.shipStatus), "ship_status_nm"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CASE,
                                    ObjectUtils.toArray(
                                        SqlUtils.sqlFunction(MysqlFunction.IS_NULL, claimBundle.claimStatus),
                                        "'A'",
                                        SqlUtils.sqlFunction(
                                            MysqlFunction.CASE,
                                            ObjectUtils.toArray(
                                                SqlUtils.sqlFunction(MysqlFunction.IN, claimBundle.claimStatus, ObjectUtils.toArray("'C1'", "'C2'", "'C8'")),
                                                "'N'",
                                                SqlUtils.sqlFunction(
                                                    MysqlFunction.CASE_Y_N,
                                                    SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(orderItem.itemQty, SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray("claimItemData.claim_item_qty", 0)))), "0")
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimItemNo)))
                                .WHERE(
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                    SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                                    SqlUtils.equalColumn(claimItem.itemNo, orderItem.itemNo),
                                    SqlUtils.equalColumn(claimOpt.orderOptNo, orderOpt.orderOptNo)
                                )
                                .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimBundle.claimBundleNo)))
                                .LIMIT(1)
                                .OFFSET(0)
                        ),
                        "'A'"
                    ),
                    "claim_yn"
                ),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(claimBundle.claimBundleNo)
                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
                        .WHERE(
                            SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                            SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                            SqlUtils.equalColumn(claimItem.itemNo, orderItem.itemNo)
                        )
                        .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .LIMIT(1)
                        .OFFSET(0),
                    claimBundle.claimBundleNo
                ),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(claimBundle.claimNo)
                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .WHERE(
                            SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                            SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                            SqlUtils.equalColumn(claimItem.itemNo, orderItem.itemNo)
                        )
                        .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimBundle.claimNo)))
                        .LIMIT(1)
                        .OFFSET(0),
                    claimBundle.claimNo
                ),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(claimBundle.claimType)
                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimItemNo)))
                        .WHERE(
                            SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                            SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                            SqlUtils.equalColumn(claimItem.itemNo, orderItem.itemNo),
                            SqlUtils.equalColumn(claimOpt.orderOptNo, orderOpt.orderOptNo)
                        )
                        .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .LIMIT(1)
                        .OFFSET(0),
                    claimBundle.claimType
                ),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(claimBundle.claimStatus)
                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimItemNo)))
                        .WHERE(
                            SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                            SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                            SqlUtils.equalColumn(claimItem.itemNo, orderItem.itemNo),
                            SqlUtils.equalColumn(claimOpt.orderOptNo, orderOpt.orderOptNo)
                        )
                        .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .LIMIT(1)
                        .OFFSET(0),
                    claimBundle.claimStatus
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.SUM, claimOpt.claimOptQty, claimItem.claimItemQty))
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimItemNo)))
                                .WHERE(
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                    SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                                    SqlUtils.equalColumn(claimItem.itemNo, orderItem.itemNo),
                                    SqlUtils.equalColumn(claimItem.orderItemNo, orderItem.orderItemNo),
                                    SqlUtils.equalColumn(claimOpt.orderOptNo, orderOpt.orderOptNo),
                                    SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C3'", "'C4'", "'C9'"))
                                )
                        ),
                        "0"
                    ),
                    "process_item_qty"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.SUM, claimOpt.claimOptQty, claimItem.claimItemQty))
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimItemNo)))
                                .WHERE(
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                    SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                                    SqlUtils.equalColumn(claimItem.itemNo, orderItem.itemNo),
                                    SqlUtils.equalColumn(claimItem.orderItemNo, orderItem.orderItemNo),
                                    SqlUtils.equalColumn(claimOpt.orderOptNo, orderOpt.orderOptNo),
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                                )
                        ),
                        "0"
                    ),
                    "claim_item_qty"
                ),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, "'M'"), "ship_type"),
                SqlUtils.sqlFunction(MysqlFunction.FN_ESTIMATED_DELIVERY, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo, shippingItem.bundleNo,shippingItem.orderItemNo), "ship_dt"),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.PLUS,
                            ObjectUtils.toArray(
                                storeSlotMng.shipDt,
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(storeSlotMng.slotShipStartTime, "'00'"))
                            )
                        ),
                        CustomConstants.HHMI
                    ),
                    "ship_start_time"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.PLUS,
                            ObjectUtils.toArray(
                                storeSlotMng.shipDt,
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(storeSlotMng.slotShipEndTime, "'00'"))
                            )
                        ),
                        CustomConstants.HHMI
                    ),
                    "ship_end_time"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.PLUS,
                            ObjectUtils.toArray(
                                storeSlotMng.shipDt,
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CONCAT,
                                    ObjectUtils.toArray(
                                        purchaseStoreInfo.slotCloseTime,
                                        "'00'"
                                    )
                                )
                            )
                        )
                        , CustomConstants.HHMI
                    ),
                    "ship_close_time"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, shippingItem.reserveShipDt),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.reserveShipDt, CustomConstants.YYYYMMDD_COMMA)),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, shippingItem.scheduleShipDt),
                                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.scheduleShipDt, CustomConstants.YYYYMMDD_COMMA)),
                                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.shippingDt, CustomConstants.YYYYMMDD_COMMA))
                            )
                        )
                    ),
                    shippingItem.shippingDt
                ),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.completeDt, CustomConstants.YYYYMMDD_COMMA), shippingItem.completeDt),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, shippingNonRcv.noRcvProcessYn),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "Y"),
                                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "Y"), "'W'", "'C'")),
                                "'Y'"
                            )
                        )
                        , "'N'"
                    ),
                    "no_rcv_yn"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, purchaseOrder.orderType, "'ORD_COMBINE'"),
                        purchaseOrder.cmbnYn,
                        "'Y'"
                    ),
                    purchaseOrder.cmbnYn
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, purchaseOrder.orderType, "'ORD_SUB'"),
                        purchaseOrder.substitutionOrderYn,
                        "'Y'"
                    ),
                    purchaseOrder.substitutionOrderYn
                ),
                SqlUtils.caseWhenElse(
                    orderPromo.promoKind,
                    ObjectUtils.toArray(
                        "'BASIC'", "'행사상품'",
                        "'PICK'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.MINUS, orderPromo.mixMatchStdVal, "1"), " '+' ", "'1'")),
                        "'TOGETHER'", "'함께할인'",
                        "'INTERVAL'", "'함께할인'"
                    ),
                    SqlUtils.sqlFunction(
                        MysqlFunction.CASE,
                        ObjectUtils.toArray(
                            SqlUtils.AND(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, orderItem.promoNo),
                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.simpOrgItemPrice, "0")
                            ),
                            "'행사상품'",
                            "''"
                        )
                    ),
                    "promoType"
                ),
                orderItem.promoNo,
                orderItem.promoDetailNo,
                orderPromo.promoNm,
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, shippingItem.delayShipCd, ObjectUtils.toArray(shippingItem.delayShipCd), shippingItem.delayShipCd),
                shippingItem.delayShipMsg,
                purchaseOrder.paymentNo,
                orderItem.reviewYn,
                orderItem.purchaseMinQty,
                orderItem.simpOrgItemPrice,
                bundle.shipPrice,
                bundle.islandShipPrice,
                bundle.prepaymentYn,
                orderItem.saleUnit,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(orderItem.itemQty, SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray("claimItemData.claim_item_qty", 0)))), "0"),
                        SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(orderItem.itemQty, SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray("claimItemData.claim_item_qty", 0)))),
                        "0"
                    ),
                    "item_qty"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        orderOpt.optQty,
                        SqlUtils.sqlFunction(
                            MysqlFunction.IFNULL,
                            ObjectUtils.toArray(
                                SqlUtils.subTableQuery(
                                    new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.SUM, claimOpt.claimOptQty))
                                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                        .INNER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                                        .WHERE(
                                            SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                            SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                                            SqlUtils.equalColumn(claimOpt.orderOptNo, orderOpt.orderOptNo),
                                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                                        )
                                ),
                                "0"
                            )
                        )
                    ),
                    orderOpt.optQty
                ),
                SqlUtils.sqlFunction(MysqlFunction.CASE_Y_N, SqlUtils.equalColumnByInput(shippingItem.completeRegId, "MYPAGE"), "complete_user_reg_yn"),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, "'C'"), "ship_sort"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_Y_N,
                    SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.customOperator(CustomConstants.PLUS, orderItem.addTdItemDiscountAmt, orderItem.addDsItemDiscountAmt), "0"),
                    "add_coupon_use_yn"
                ),
                SqlUtils.aliasToRow(orderItem.reserveShipMethod, "order_reserve_type")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.orderItemNo, orderItem.bundleNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(shippingNonRcv, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(orderItem.bundleNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'"))))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    orderPromo,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, orderItem.promoNo, orderItem.promoDetailNo),
                    ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderPromo.promoType, "'SIMP'"))
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(purchaseStoreInfo, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.originStoreId, bundle.purchaseStoreInfoNo), ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.NOT_EQUAL, purchaseStoreInfo.storeKind, "'DLV'")))
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(storeSlotMng, ObjectUtils.toArray( purchaseStoreInfo.shipDt, purchaseStoreInfo.storeId, purchaseStoreInfo.slotId, purchaseStoreInfo.shiftId )))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    SqlUtils.sqlFunction(MysqlFunction.CLAIM_ITEM_SUB_TABLE, ObjectUtils.toArray(String.valueOf(userNo))),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(orderItem.purchaseOrderNo, "claimItemData.purchase_order_no"),
                        SqlUtils.equalColumn(orderItem.bundleNo, "claimItemData.bundle_no"),
                        SqlUtils.equalColumn(orderItem.itemNo, "claimItemData.item_no")
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo)
            );

        if(isCmbn){
            query.AND()
                .WHERE(SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo), SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE"))
                .OR()
                .WHERE(SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo), SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB"));
        } else {
            query.AND()
                .WHERE(SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo))
                .OR()
                .WHERE(SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo), SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB"));
        }
        query.ORDER_BY(purchaseOrder.orderDt, purchaseOrder.purchaseOrderNo, orderItem.mallType, orderItem.bundleNo, orderItem.orderItemNo, orderOpt.orderOptNo, orderPromo.promoKind);

        SQL queryOfSort = new SQL()
            .SELECT("*")
            .FROM(
                "(".concat(query.toString()).concat(" ) sort")
            )
            .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray("sort.order_dt", "sort.purchase_order_no","sort.ship_type", "sort.ship_sort", "sort.order_item_no", "sort.bundle_no")));

        log.debug("selectMpOrderProductList \n[{}]", SqlUtils.orQuery(queryOfSort.toString()));
        return SqlUtils.orQuery(queryOfSort.toString());
    }

    // 주문상세
    public static String selectMpOrderMultiProductList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("multiBundleNo") long multiBundleNo, @Param("userNo") long userNo){
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        OrderPromoEntity orderPromo = EntityFactory.createEntityIntoValue(OrderPromoEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        MultiBundleEntity multiBundle = EntityFactory.createEntityIntoValue(MultiBundleEntity.class, Boolean.TRUE);
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        MultiShippingAddrEntity multiShippingAddr = EntityFactory.createEntityIntoValue(MultiShippingAddrEntity.class, Boolean.TRUE);
        ItmPartnerSellerEntity itmPartnerSeller = EntityFactory.createEntityIntoValue(ItmPartnerSellerEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ClaimMultiBundleEntity claimMultiBundle = EntityFactory.createEntityIntoValue(ClaimMultiBundleEntity.class, Boolean.TRUE);
        ClaimMultiItemEntity claimMultiItem = EntityFactory.createEntityIntoValue(ClaimMultiItemEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                multiOrderItem.purchaseOrderNo,
                orderItem.storeType,
                orderItem.mallType,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        orderItem.storeId,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray("NOW()", "%s%i")), itmPartnerSeller.sellerSeq, SqlUtils.sqlFunction(MysqlFunction.RIGHT, ObjectUtils.toArray("RAND()", 2)))))
                                .FROM(EntityFactory.getTableNameWithAlias("dms", itmPartnerSeller))
                                .WHERE(
                                    SqlUtils.equalColumn(itmPartnerSeller.partnerId, orderItem.partnerId)
                                )
                        )
                    ), orderItem.partnerId
                ),
                orderItem.orderItemNo,
                multiOrderItem.multiOrderItemNo,
                multiOrderItem.itemNo,
                orderItem.itemNm1,
                multiBundle.shipMethod,
                shippingItem.shipNo,
                multiBundle.shipPrice,
                multiBundle.islandShipPrice,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.originStoreId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "partner_nm"
                ),
                multiOrderItem.bundleNo,
                multiOrderItem.multiBundleNo,
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(purchaseOrder.orderDt, CustomConstants.YYYYMMDD), "order_dt"),
                orderItem.orgItemNo,
                SqlUtils.aliasToRow(multiOrderItem.itemQty, "order_item_qty"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(multiOrderItem.itemQty, SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray("claimItemData.claim_item_qty", 0)))), "0"),
                        SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(multiOrderItem.itemQty, SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray("claimItemData.claim_item_qty", 0)))),
                        "0"
                    ),
                    "item_qty"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.simpOrgItemPrice, "'0'"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NULL, ObjectUtils.toArray(orderItem.promoNo)),
                                orderItem.simpOrgItemPrice,
                                orderItem.itemPrice
                            )
                        ),
                        orderItem.itemPrice
                    ),
                    "item_price"
                ),
                orderOpt.orderOptNo,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.selOpt1Title, orderOpt.selOpt1Title),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.selOpt1Val, orderOpt.selOpt1Val),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.selOpt2Title, orderOpt.selOpt2Title),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.selOpt2Val, orderOpt.selOpt2Val),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.txtOpt1Title, orderOpt.txtOpt1Title),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.txtOpt1Val, orderOpt.txtOpt1Val),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.txtOpt2Title, orderOpt.txtOpt2Title),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, orderOpt.txtOpt2Val, orderOpt.txtOpt2Val),
                orderOpt.optNo,
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(payment.paymentStatus, "P5"), "'P3'", payment.paymentStatus), payment.paymentStatus),
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, payment.paymentStatus, ObjectUtils.toArray(payment.paymentStatus), "payment_status_nm"),
                multiOrderItem.shipStatus,
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, multiOrderItem.shipStatus, ObjectUtils.toArray(multiOrderItem.shipStatus), "ship_status_nm"),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NULL, SqlUtils.sqlFunction(MysqlFunction.MIN, claimBundle.claimStatus)),
                                "'A'",
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CASE,
                                    ObjectUtils.toArray(
                                        SqlUtils.sqlFunction(MysqlFunction.IN, SqlUtils.sqlFunction(MysqlFunction.MIN, claimBundle.claimStatus), ObjectUtils.toArray("'C1'", "'C2'", "'C8'")),
                                        "'N'",
                                        SqlUtils.sqlFunction(MysqlFunction.CASE_Y_N, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(multiOrderItem.itemQty, SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray("claimItemData.claim_item_qty", 0)))))
                                    )
                                )
                            )
                        )
                    )
                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiBundle, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiItem, ObjectUtils.toArray(claimMultiBundle.claimMultiBundleNo, claimItem.claimItemNo)))
                        .WHERE(
                            SqlUtils.equalColumn(claimBundle.purchaseOrderNo, multiOrderItem.purchaseOrderNo),
                            SqlUtils.equalColumn(claimBundle.bundleNo, multiOrderItem.bundleNo),
                            SqlUtils.equalColumn(claimMultiBundle.multiBundleNo, multiOrderItem.multiBundleNo)
                        )
                        .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimBundle.claimBundleNo))),
                    "claim_yn"
                ),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(claimBundle.claimBundleNo)
                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiBundle, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiItem, ObjectUtils.toArray(claimMultiBundle.claimMultiBundleNo, claimItem.claimItemNo)))
                        .WHERE(
                            SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                            SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                            SqlUtils.equalColumn(claimItem.itemNo, orderItem.itemNo),
                            SqlUtils.equalColumn(claimMultiItem.multiBundleNo, multiBundle.multiBundleNo)
                        )
                        .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .LIMIT(1)
                        .OFFSET(0),
                    claimBundle.claimBundleNo
                ),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(claimBundle.claimNo)
                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiBundle, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiItem, ObjectUtils.toArray(claimMultiBundle.claimMultiBundleNo, claimItem.claimItemNo)))
                        .WHERE(
                            SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                            SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                            SqlUtils.equalColumn(claimItem.itemNo, orderItem.itemNo),
                            SqlUtils.equalColumn(claimMultiItem.multiBundleNo, multiBundle.multiBundleNo)
                        )
                        .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimBundle.claimNo)))
                        .LIMIT(1)
                        .OFFSET(0),
                    claimBundle.claimNo
                ),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(claimBundle.claimType)
                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiBundle, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiItem, ObjectUtils.toArray(claimMultiBundle.claimMultiBundleNo, claimItem.claimItemNo)))
                        .WHERE(
                            SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                            SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                            SqlUtils.equalColumn(claimItem.itemNo, orderItem.itemNo),
                            SqlUtils.equalColumn(claimMultiItem.multiBundleNo, multiBundle.multiBundleNo)
                        )
                        .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .LIMIT(1)
                        .OFFSET(0),
                    claimBundle.claimType
                ),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(claimBundle.claimStatus)
                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiBundle, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiItem, ObjectUtils.toArray(claimMultiBundle.claimMultiBundleNo, claimItem.claimItemNo)))
                        .WHERE(
                            SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                            SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                            SqlUtils.equalColumn(claimItem.itemNo, orderItem.itemNo),
                            SqlUtils.equalColumn(claimMultiItem.multiBundleNo, multiBundle.multiBundleNo)
                        )
                        .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .LIMIT(1)
                        .OFFSET(0),
                    claimBundle.claimStatus
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.SUM, claimMultiItem.claimItemQty, claimMultiItem.claimItemQty))
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiBundle, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiItem, ObjectUtils.toArray(claimMultiBundle.claimMultiBundleNo, claimItem.claimItemNo)))
                                .WHERE(
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                    SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                                    SqlUtils.equalColumn(claimItem.itemNo, orderItem.itemNo),
                                    SqlUtils.equalColumn(claimItem.orderItemNo, orderItem.orderItemNo),
                                    SqlUtils.equalColumn(claimMultiItem.multiBundleNo, multiBundle.multiBundleNo),
                                    SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C3'", "'C4'", "'C9'"))
                                )
                        ),
                        "0"
                    ),
                    "process_item_qty"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.SUM, claimMultiItem.claimItemQty))
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiBundle, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiItem, ObjectUtils.toArray(claimMultiBundle.claimMultiBundleNo, claimItem.claimItemNo)))
                                .WHERE(
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, multiOrderItem.purchaseOrderNo),
                                    SqlUtils.equalColumn(claimBundle.bundleNo, multiOrderItem.bundleNo),
                                    SqlUtils.equalColumn(claimMultiItem.multiBundleNo, multiOrderItem.multiBundleNo),
                                    SqlUtils.equalColumn(claimItem.itemNo, multiOrderItem.itemNo),
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                                )
                        ),
                        "0"
                    ),
                    "claim_item_qty"
                ),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(multiOrderItem.purchaseOrderNo, multiOrderItem.bundleNo, "'M'"), "ship_type"),
                SqlUtils.aliasToRow(multiOrderItem.reserveShipDt, "ship_dt"),
                SqlUtils.appendSingleQuoteWithAlias("", "ship_start_time"),
                SqlUtils.appendSingleQuoteWithAlias("", "ship_end_time"),
                SqlUtils.appendSingleQuoteWithAlias("", "ship_close_time"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, multiOrderItem.reserveShipDt),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(multiOrderItem.reserveShipDt, CustomConstants.YYYYMMDD_COMMA)),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(multiOrderItem.shippingDt, CustomConstants.YYYYMMDD_COMMA))
                    ),
                    shippingItem.shippingDt
                ),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(multiOrderItem.completeDt, CustomConstants.YYYYMMDD_COMMA), multiOrderItem.completeDt),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, shippingNonRcv.noRcvProcessYn),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "Y"),
                                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "Y"), "'W'", "'C'")),
                                "'Y'"
                            )
                        )
                        , "'N'"
                    ),
                    "no_rcv_yn"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, purchaseOrder.orderType, "'ORD_COMBINE'"),
                        purchaseOrder.cmbnYn,
                        "'Y'"
                    ),
                    purchaseOrder.cmbnYn
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, purchaseOrder.orderType, "'ORD_SUB'"),
                        purchaseOrder.substitutionOrderYn,
                        "'Y'"
                    ),
                    purchaseOrder.substitutionOrderYn
                ),
                SqlUtils.caseWhenElse(
                    orderPromo.promoKind,
                    ObjectUtils.toArray(
                        "'BASIC'", "'행사상품'",
                        "'PICK'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.MINUS, orderPromo.mixMatchStdVal, "1"), " '+' ", "'1'")),
                        "'TOGETHER'", "'함께할인'",
                        "'INTERVAL'", "'함께할인'"
                    ),
                    SqlUtils.sqlFunction(
                        MysqlFunction.CASE,
                        ObjectUtils.toArray(
                            SqlUtils.AND(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, orderItem.promoNo),
                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.simpOrgItemPrice, "0")
                            ),
                            "'행사상품'",
                            "''"
                        )
                    ),
                    "promoType"
                ),
                orderItem.promoNo,
                orderItem.promoDetailNo,
                orderPromo.promoNm,
                purchaseOrder.paymentNo,
                orderItem.reviewYn,
                orderItem.purchaseMinQty,
                orderItem.saleUnit,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(multiOrderItem.itemQty, SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray("claimItemData.claim_item_qty", 0)))), "0"),
                        SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(multiOrderItem.itemQty, SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray("claimItemData.claim_item_qty", 0)))),
                        "0"
                    ),
                    "opt_qty"
                ),
                SqlUtils.sqlFunction(MysqlFunction.CASE_Y_N, SqlUtils.equalColumnByInput(shippingItem.completeRegId, "MYPAGE"), "complete_user_reg_yn"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_Y_N,
                    SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.customOperator(CustomConstants.PLUS, orderItem.addTdItemDiscountAmt, orderItem.addDsItemDiscountAmt), "0"),
                    "add_coupon_use_yn"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(multiOrderItem, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, orderItem.orderItemNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(multiBundle, ObjectUtils.toArray(multiOrderItem.multiBundleNo, multiOrderItem.purchaseOrderNo, multiOrderItem.bundleNo, orderItem.storeId))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(multiShippingAddr, ObjectUtils.toArray(multiBundle.multiBundleNo))
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.orderItemNo, orderItem.bundleNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(shippingNonRcv, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'"))))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    orderPromo,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, orderItem.promoNo, orderItem.promoDetailNo),
                    ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderPromo.promoType, "'SIMP'"))
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    SqlUtils.sqlFunction(MysqlFunction.CLAIM_ITEM_MULTI_SUB_TABLE, ObjectUtils.toArray(String.valueOf(userNo))),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(multiOrderItem.purchaseOrderNo, "claimItemData.purchase_order_no"),
                        SqlUtils.equalColumn(multiOrderItem.bundleNo, "claimItemData.bundle_no"),
                        SqlUtils.equalColumn(multiOrderItem.multiBundleNo, "claimItemData.multi_bundle_no"),
                        SqlUtils.equalColumn(multiOrderItem.itemNo, "claimItemData.item_no")
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                SqlUtils.equalColumnByInput(multiBundle.multiBundleNo, multiBundleNo)
            );
        query.ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(purchaseOrder.orderDt, purchaseOrder.purchaseOrderNo, orderItem.mallType, orderItem.orderItemNo, orderItem.bundleNo, multiOrderItem.multiBundleNo)));

        SQL queryOfSort = new SQL()
            .SELECT("*")
            .FROM(
                "(".concat(query.toString()).concat(" ) sort")
            )
            .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray("sort.order_dt", "sort.purchase_order_no","sort.ship_type", "sort.order_item_no", "sort.bundle_no")));

        log.debug("selectMpOrderMultiProductList \n[{}]", SqlUtils.orQuery(queryOfSort.toString()));
        return SqlUtils.orQuery(queryOfSort.toString());
    }

    /**
     * 마이페이지 - 주문상세
     *
     * @param purchaseOrderNo 주문거래번호
     * @return 주문상세기본정보 조회 쿼리
     */
    public static String selectOrderDetailInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo, @Param("siteType") String siteType, @Param("isCmbn") Boolean isCmbn) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.orderDt,
                purchaseOrder.cmbnYn,
                purchaseOrder.substitutionOrderYn,
                purchaseOrder.orderType
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                SqlUtils.equalColumnByInput(purchaseOrder.siteType, siteType)
            );

        if(isCmbn){
            query.WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE")
            ).ORDER_BY(purchaseOrder.purchaseOrderNo)
                .LIMIT(1)
                .OFFSET(0);
        } else {
            query.WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        }
        log.debug("selectOrderDetailInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMultiOrderDetailList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo, @Param("siteType") String siteType) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.orderDt,
                purchaseOrder.orderType
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                SqlUtils.equalColumnByInput(purchaseOrder.siteType, siteType),
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectMultiOrderDetailList \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마이페이지 - 주문상세(결제정보)
     *
     * @param purchaseOrderNo 주문거래번호
     * @return 결제정보 조회 쿼리
     */
    public static String selectPurchaseInfoList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.totItemPrice,
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray( purchaseOrder.totItemDiscountAmt, purchaseOrder.totCardDiscountAmt, purchaseOrder.totCartDiscountAmt, purchaseOrder.totEmpDiscountAmt,
                        purchaseOrder.totPromoDiscountAmt, purchaseOrder.totShipDiscountAmt, purchaseOrder.totAddTdDiscountAmt, purchaseOrder.totAddDsDiscountAmt ),
                    "tot_discount_price"
                ),
                purchaseOrder.totShipPrice,
                purchaseOrder.totIslandShipPrice,
                SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(purchaseOrder.pgAmt, purchaseOrder.mileageAmt, purchaseOrder.mhcAmt, purchaseOrder.dgvAmt, purchaseOrder.ocbAmt), "tot_payment_amt"),
                purchaseOrder.totEmpDiscountAmt
            )
            .FROM(EntityFactory.getTableName(purchaseOrder))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo)
            );

        log.debug("selectPurchaseInfoList \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마이페이지 - 주문상세(쿠폰할인)
     *
     * @param purchaseOrderNo 주문거래번호
     * @return 쿠폰할인정보 조회 쿼리
     */
    public static String selectCouponDiscountInfoList(@Param("purchaseOrderNo") long purchaseOrderNo) {
        OrderDiscountEntity orderDiscount = EntityFactory.createEntityIntoValue(OrderDiscountEntity.class);
        SQL innerQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    orderDiscount.discountKind,
                    ObjectUtils.toArray(
                        "'item_discount'",  "'상품할인'",
                        "'item_coupon'", "'상품할인'",
                        "'card_discount'", "'카드상품할인'",
                        "'ship_coupon'", "'배송비할인'",
                        "'cart_coupon'", "'장바구니할인'",
                        "'add_td_coupon'", "'중복할인'",
                        "'add_ds_coupon'", "'중복할인'"
                    ),
                    orderDiscount.discountKind
                ),
                orderDiscount.discountAmt
            )
            .FROM(EntityFactory.getTableName(orderDiscount))
            .WHERE(
                SqlUtils.equalColumnByInput(orderDiscount.purchaseOrderNo, purchaseOrderNo)
            );

        SQL query = new SQL()
            .SELECT(
                SqlUtils.aliasToRow(orderDiscount.discountKind, "discount_type"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, orderDiscount.discountAmt, "discount_price")
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "od"))
            .GROUP_BY(orderDiscount.discountKind);

        log.debug("selectCouponDiscountInfoList \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마이페이지 - 주문상세(프로모션정보)
     *
     * @param purchaseOrderNo 주문거래번호
     * @return 프로모션정보 조회 쿼리
     */
    public static String selectPromoDiscountInfoList(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderPromoEntity orderPromo = EntityFactory.createEntityIntoValue(OrderPromoEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN, orderPromo.promoKind,
                    ObjectUtils.toArray(
                        "'BASIC'", "'행사할인'",
                        "'PICK'", "'행사할인'",
                        "'TOGETHER'", "'행사할인'",
                        "'INTERVAL'", "'행사할인'"
                    ),
                    orderPromo.promoKind
                ),
                orderPromo.discountAmt
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    orderPromo,
                    ObjectUtils.toArray(purchaseOrder.purchaseOrderNo),
                    ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderPromo.promoType, "'SIMP'"))
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        SQL query = new SQL()
            .SELECT(
                SqlUtils.aliasToRow(orderPromo.promoKind, "promoType"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, orderPromo.discountAmt, "promoDiscountPrice")
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "op"))
            .GROUP_BY(orderPromo.promoKind);

        log.debug("selectPromoDiscountInfoList \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마이페이지 - 주문상세(결제수단)
     *
     * @param purchaseOrderNo 주문결제번호
     * @return 결제수단 조회 쿼리
     */
    public static String selectPaymentInfoList(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PaymentInfoEntity paymentInfo = EntityFactory.createEntityIntoValue(PaymentInfoEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.aliasToRow(paymentInfo.parentMethodCd, "payment_type"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(paymentInfo.paymentType, "DGV"),
                        SqlUtils.sqlFunction(MysqlFunction.AES_DECRYPT, ObjectUtils.toArray(paymentInfo.dgvCardNo, "!@homeplus#$")),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.PAYMENT_METHOD_INFO, ObjectUtils.toArray(purchaseOrder.siteType, paymentInfo.methodCd, paymentInfo.parentMethodCd)),
                                SqlUtils.wrapWithSpace(CustomConstants.SQL_BLANK),
                                SqlUtils.wrapWithSpace(paymentInfo.cardNo),
                                SqlUtils.wrapWithSpace(CustomConstants.SQL_BLANK),
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CASE,
                                    ObjectUtils.toArray(
                                        SqlUtils.equalColumnByInput(paymentInfo.paymentType, "PG"),
                                        SqlUtils.sqlFunction(
                                            MysqlFunction.CASE,
                                            ObjectUtils.toArray(
                                                SqlUtils.equalColumnByInput(paymentInfo.cardInstallmentMonth, "1"),
                                                "'일시불'",
                                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.CAST_NCHAR, paymentInfo.cardInstallmentMonth), "'개월'"))
                                            )
                                        ),
                                        "''"
                                    )
                                )
                            )
                        )
                    ),
                    "payment_info"
                ),
                SqlUtils.aliasToRow(paymentInfo.amt, "payment_amt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(paymentInfo, ObjectUtils.toArray(purchaseOrder.paymentNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            )
            .GROUP_BY()
            ;

        log.debug("selectPaymentInfoList \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMpMainOrderCount(@Param("userNo") long userNo, @Param("siteType") String siteType) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);

        SQL orderQuery = new SQL()
            .SELECT(
//                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, shippingItem.shipStatus, ObjectUtils.toArray(shippingItem.shipStatus), "order_status"),
                SqlUtils.caseWhenElse(
                    shippingItem.shipStatus,
                    ObjectUtils.toArray(
                        "'D0'", "'D1'",
                        "'D5'", "'D4'"
                    ),
                    shippingItem.shipStatus,
                    "order_status"
                ),
//                SqlUtils.aliasToRow(shippingItem.shipStatus, "order_status"),
                SqlUtils.sqlFunction(MysqlFunction.COUNT, SqlUtils.sqlFunction(MysqlFunction.DISTINCT, shippingItem.bundleNo), "order_cnt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN( SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)) )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    shippingItem,
                    ObjectUtils.toArray(purchaseOrder.purchaseOrderNo, orderItem.orderItemNo)
                )
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .WHERE(
                SqlUtils.sqlFunction(
                    MysqlFunction.BETWEEN_ORI,
                    ObjectUtils.toArray(
                        purchaseOrder.orderDt,
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray("NOW()", "-15"))),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, "NOW()")
                    )
                ),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                SqlUtils.equalColumnByInput(purchaseOrder.siteType, siteType),
                SqlUtils.notExists(
                    new SQL()
                        .SELECT(
                            "claimBundleData.purchase_order_no",
                            "claimBundleData.bundle_no",
                            "claimBundleData.order_item_no"
                        )
                        .FROM(SqlUtils.subTableQuery(
                            new SQL()
                                .SELECT(
                                    SqlUtils.customOperator(
                                        CustomConstants.MINUS,
                                        ObjectUtils.toArray(
                                            orderItem.itemQty,
                                            SqlUtils.sqlFunction(MysqlFunction.SUM, claimItem.claimItemQty)
                                        ),
                                        "cnt"
                                    ),
                                    purchaseOrder.purchaseOrderNo,
                                    claimBundle.bundleNo,
                                    orderItem.orderItemNo
                                )
                                .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
                                .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(orderItem.orderItemNo)))
                                .INNER_JOIN( SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimItem.claimBundleNo, orderItem.bundleNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"))) )
                                .WHERE(
                                    SqlUtils.sqlFunction(
                                        MysqlFunction.BETWEEN_ORI,
                                        ObjectUtils.toArray(
                                            purchaseOrder.orderDt,
                                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray("NOW()", "-15"))),
                                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, "NOW()")
                                        )
                                    ),
                                    SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo)
                                )
                                .GROUP_BY(purchaseOrder.purchaseOrderNo, claimBundle.bundleNo, orderItem.orderItemNo),
                            "claimBundleData"
                        ))
                        .WHERE(
                            SqlUtils.equalColumn("claimBundleData.purchase_order_no", purchaseOrder.purchaseOrderNo),
                            SqlUtils.equalColumn("claimBundleData.bundle_no", shippingItem.bundleNo),
                            SqlUtils.equalColumn("claimBundleData.order_item_no", orderItem.orderItemNo),
                            SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT_EQUAL, "claimBundleData.cnt", "0")
                        )
                )
            )
            .GROUP_BY(shippingItem.shipStatus);

        SQL secondaryQuery = new SQL()
            .SELECT(
//                "'취소/교환/반품'",
                "'C'",
                SqlUtils.sqlFunction(MysqlFunction.COUNT, SqlUtils.sqlFunction(MysqlFunction.DISTINCT, claimBundle.claimBundleNo), "order_cnt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN( SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimMst.purchaseOrderNo)) )
            .INNER_JOIN( SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")))) )
            .WHERE(
                SqlUtils.sqlFunction(
                    MysqlFunction.BETWEEN_ORI,
                    ObjectUtils.toArray(
                        purchaseOrder.orderDt,
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray("NOW()", "-15"))),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, "NOW()")
                    )
                ),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                SqlUtils.equalColumnByInput(purchaseOrder.siteType, siteType)
            );

        SQL query = new SQL()
            .SELECT(
                "order_status",
                SqlUtils.sqlFunction(MysqlFunction.SUM, "order_cnt", "order_cnt")
            )
            .FROM(
                SqlUtils.getAppendAliasToTableName("(".concat(orderQuery.toString()).concat("\nUNION ALL\n").concat(secondaryQuery.toString()).concat(")"), "data")
            )
            .GROUP_BY("data.order_status");

        log.debug("selectMpMainOrderCount :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMpMainOrderList(@Param("userNo") long userNo, @Param("siteType") String siteType) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PurchaseOrderEntity orgPurchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, "opo");
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class, Boolean.TRUE);
        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class, Boolean.TRUE);

        SQL orderQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderDt, "order_dt"),
                purchaseOrder.purchaseOrderNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemNm1, "item_nm1"),
                SqlUtils.sqlFunction(MysqlFunction.COUNT, SqlUtils.sqlFunction(MysqlFunction.DISTINCT, orderItem.orderItemNo), "order_cnt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shipStatus, "order_status"),
                SqlUtils.aliasToRow("NULL", "claim_type"),
                SqlUtils.aliasToRow("NULL", "claim_no"),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.purchaseOrderNo), SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.bundleNo), "'M'"), "ship_type"),
                SqlUtils.aliasToRow("NULL", claimPickShipping.pickStatus),
                SqlUtils.aliasToRow("NULL", claimExchShipping.exchStatus)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo, orderItem.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .WHERE(
                SqlUtils.sqlFunction(
                    MysqlFunction.BETWEEN_ORI,
                    ObjectUtils.toArray(
                        purchaseOrder.orderDt,
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray("NOW()", "-15"))),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, "NOW()")
                    )
                ),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                SqlUtils.equalColumnByInput(purchaseOrder.siteType, siteType),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_COMBINE'", "'ORD_SUB'")),
                SqlUtils.notExists(
                    new SQL()
                        .SELECT(
                            "claimBundleData.purchase_order_no",
                            "claimBundleData.bundle_no",
                            "claimBundleData.order_item_no"
                        )
                        .FROM(SqlUtils.subTableQuery(
                            new SQL()
                                .SELECT(
                                    SqlUtils.customOperator(
                                        CustomConstants.MINUS,
                                        ObjectUtils.toArray(
                                            orderItem.itemQty,
                                            SqlUtils.sqlFunction(MysqlFunction.SUM, claimItem.claimItemQty)
                                        ),
                                        "cnt"
                                    ),
                                    purchaseOrder.purchaseOrderNo,
                                    claimBundle.bundleNo,
                                    orderItem.orderItemNo
                                )
                                .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
                                .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(orderItem.orderItemNo)))
                                .INNER_JOIN( SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimItem.claimBundleNo, orderItem.bundleNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"))) )
                                .WHERE(
                                    SqlUtils.sqlFunction(
                                        MysqlFunction.BETWEEN_ORI,
                                        ObjectUtils.toArray(
                                            purchaseOrder.orderDt,
                                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray("NOW()", "-15"))),
                                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, "NOW()")
                                        )
                                    ),
                                    SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                                    SqlUtils.sqlFunction(MysqlFunction.NOT_IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_COMBINE'", "'ORD_SUB'"))
                                )
                                .GROUP_BY(purchaseOrder.purchaseOrderNo, claimBundle.bundleNo, orderItem.orderItemNo)
                            ,
                            "claimBundleData"
                        ))
                        .WHERE(
                            SqlUtils.equalColumn("claimBundleData.purchase_order_no", purchaseOrder.purchaseOrderNo),
                            SqlUtils.equalColumn("claimBundleData.bundle_no", shippingItem.bundleNo),
                            SqlUtils.equalColumn("claimBundleData.order_item_no", orderItem.orderItemNo),
                            SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT_EQUAL, "claimBundleData.cnt", "0")
                        )
                )
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo, shippingItem.bundleNo);

        SQL orderCombineQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderDt, "order_dt"),
                purchaseOrder.orgPurchaseOrderNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemNm1, "item_nm1"),
                SqlUtils.sqlFunction(MysqlFunction.COUNT, SqlUtils.sqlFunction(MysqlFunction.DISTINCT, orderItem.orderItemNo), "order_cnt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shipStatus, "order_status"),
                SqlUtils.aliasToRow("NULL", "claim_type"),
                SqlUtils.aliasToRow("NULL", "claim_no"),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.purchaseOrderNo), SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.bundleNo), "'M'"), "ship_type"),
                SqlUtils.aliasToRow("NULL", claimPickShipping.pickStatus),
                SqlUtils.aliasToRow("NULL", claimExchShipping.exchStatus)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo, orderItem.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .WHERE(
                SqlUtils.sqlFunction(
                    MysqlFunction.BETWEEN_ORI,
                    ObjectUtils.toArray(
                        purchaseOrder.orderDt,
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray("NOW()", "-15"))),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, "NOW()")
                    )
                ),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                SqlUtils.equalColumnByInput(purchaseOrder.siteType, siteType),
                SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE"),
                SqlUtils.notExists(
                    new SQL()
                        .SELECT(
                            "claimBundleData.purchase_order_no",
                            "claimBundleData.bundle_no",
                            "claimBundleData.order_item_no"
                        )
                        .FROM(SqlUtils.subTableQuery(
                            new SQL()
                                .SELECT(
                                    SqlUtils.customOperator(
                                        CustomConstants.MINUS,
                                        ObjectUtils.toArray(
                                            orderItem.itemQty,
                                            SqlUtils.sqlFunction(MysqlFunction.SUM, claimItem.claimItemQty)
                                        ),
                                        "cnt"
                                    ),
                                    purchaseOrder.purchaseOrderNo,
                                    claimBundle.bundleNo,
                                    orderItem.orderItemNo
                                )
                                .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
                                .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(orderItem.orderItemNo)))
                                .INNER_JOIN( SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimItem.claimBundleNo, orderItem.bundleNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"))) )
                                .WHERE(
                                    SqlUtils.sqlFunction(
                                        MysqlFunction.BETWEEN_ORI,
                                        ObjectUtils.toArray(
                                            purchaseOrder.orderDt,
                                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray("NOW()", "-15"))),
                                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, "NOW()")
                                        )
                                    ),
                                    SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                                    SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE")
                                )
                                .GROUP_BY(purchaseOrder.purchaseOrderNo, claimBundle.bundleNo, orderItem.orderItemNo),
                            "claimBundleData"
                        ))
                        .WHERE(
                            SqlUtils.equalColumn("claimBundleData.purchase_order_no", purchaseOrder.purchaseOrderNo),
                            SqlUtils.equalColumn("claimBundleData.bundle_no", shippingItem.bundleNo),
                            SqlUtils.equalColumn("claimBundleData.order_item_no", orderItem.orderItemNo),
                            SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT_EQUAL, "claimBundleData.cnt", "0")
                        )
                )
            )
            .GROUP_BY(purchaseOrder.orgPurchaseOrderNo, shippingItem.bundleNo);

        SQL claimQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.regDt, "order_dt"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(purchaseOrder.orgPurchaseOrderNo, purchaseOrder.purchaseOrderNo), purchaseOrder.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemNm1, "item_nm1"),
                SqlUtils.sqlFunction(MysqlFunction.COUNT, SqlUtils.sqlFunction(MysqlFunction.DISTINCT, orderItem.orderItemNo), "order_cnt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimStatus, "order_status"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimType, claimBundle.claimType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimNo, claimBundle.claimNo),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.purchaseOrderNo), SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.bundleNo), "'M'"), "ship_type"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPickShipping.pickStatus, claimPickShipping.pickStatus),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimExchShipping.exchStatus, claimExchShipping.exchStatus)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN( SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimMst.purchaseOrderNo)) )
            .INNER_JOIN( SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")))) )
            .INNER_JOIN( SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)) )
            .INNER_JOIN( SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(claimMst.purchaseOrderNo, claimBundle.bundleNo, claimItem.itemNo)) )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimPickShipping, ObjectUtils.toArray(claimBundle.claimBundleNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimExchShipping, ObjectUtils.toArray(claimBundle.claimBundleNo)))
            .WHERE(
                SqlUtils.sqlFunction(
                    MysqlFunction.BETWEEN_ORI,
                    ObjectUtils.toArray(
                        purchaseOrder.orderDt,
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray("NOW()", "-15"))),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, "NOW()")
                    )
                ),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                SqlUtils.equalColumnByInput(purchaseOrder.siteType, siteType)
            )
            .GROUP_BY(claimBundle.claimBundleNo);

        SQL query = new SQL().SELECT(
            "order_dt",
            SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo),
            SqlUtils.nonAlias(orderItem.itemNm1),
            "order_cnt",
            "order_status",
            SqlUtils.nonAlias(claimBundle.claimType),
            SqlUtils.nonAlias(claimBundle.claimNo),
            "ship_type",
            SqlUtils.nonAlias(claimPickShipping.pickStatus),
            SqlUtils.nonAlias(claimExchShipping.exchStatus)
        )
            .FROM(SqlUtils.getAppendAliasToTableName("(".concat(orderQuery.toString()
                .concat("\nUNION ALL\n")
                .concat(orderCombineQuery.toString())
                .concat("\nUNION ALL\n")
                .concat(claimQuery.toString()))
                .concat(")"), "A"))
            .ORDER_BY("order_dt DESC", "purchase_order_no DESC")
            .LIMIT(5)
            .OFFSET(0);

        log.debug("selectMpMainOrderList :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectShippingPresentStatus(@Param("purchaseOrderNo") long purchaseOrderNo){
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                shippingItem.shipStatus,
                shippingItem.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(shippingItem.purchaseOrderNo, shippingItem.bundleNo, "'C'"), "ship_type"),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(SqlUtils.nonAlias(purchaseOrder.orderType))
                        .FROM(EntityFactory.getTableName(purchaseOrder))
                        .WHERE(SqlUtils.equalColumn(SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo), shippingItem.purchaseOrderNo)),
                    purchaseOrder.orderType
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.originStoreId, bundle.originStoreId),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.purchaseStoreInfoNo, bundle.purchaseStoreInfoNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.userNo, shippingItem.userNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingItem),
                    ObjectUtils.toArray(
                        SqlUtils.OR(
                            SqlUtils.equalColumn(purchaseOrder.purchaseOrderNo, shippingItem.purchaseOrderNo),
                            SqlUtils.equalColumn(purchaseOrder.orgPurchaseOrderNo, shippingItem.purchaseOrderNo)
                        )
                    )
                )
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo)))
            .WHERE(
                SqlUtils.OR(
                    SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                    SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo)
                )
            )
            .GROUP_BY(shippingItem.purchaseOrderNo, shippingItem.shipStatus, shippingItem.bundleNo)
            ;
        log.debug("selectShippingPresentStatus :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateShippingAddrEditCheck(OrderShipAddrEditDto setShipAddrDto) {
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableNameWithAlias(shippingItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingAddr),
                    ObjectUtils.toList( SqlUtils.onClause(ObjectUtils.toArray(shippingItem.shipAddrNo), shippingAddr) )
                )
            )
            .SET(
                SqlUtils.equalColumnByInput(shippingAddr.receiverNm, setShipAddrDto.getReceiverNm()),
                SqlUtils.equalColumnByInput(shippingAddr.personalOverseaNo, setShipAddrDto.getPersonalOverseaNo()),
                SqlUtils.equalColumnByInput(shippingAddr.shipMobileNo, setShipAddrDto.getShipMobileNo()),
                SqlUtils.equalColumnByInput(shippingAddr.zipcode, setShipAddrDto.getZipCode()),
                SqlUtils.equalColumnByInput(shippingAddr.roadBaseAddr, setShipAddrDto.getRoadBaseAddr()),
                SqlUtils.equalColumnByInput(shippingAddr.roadDetailAddr, setShipAddrDto.getRoadDetailAddr()),
                SqlUtils.equalColumnByInput(shippingAddr.baseAddr, setShipAddrDto.getBaseAddr() == null ? "" : setShipAddrDto.getBaseAddr()),
                SqlUtils.equalColumnByInput(shippingAddr.detailAddr, setShipAddrDto.getDetailAddr() == null ? "" : setShipAddrDto.getDetailAddr()),
                SqlUtils.equalColumnByInput(shippingItem.reserveShipDt, setShipAddrDto.getReserveShipDt()),
                SqlUtils.equalColumnByInput(shippingAddr.giftMsg, setShipAddrDto.getGiftMsg())
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, setShipAddrDto.getBundleNo()),
                // 프론트에서 수정시 D1만 가능.
                SqlUtils.equalColumnByInput(shippingItem.shipStatus, "D1")
//                SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("'D1'", "'D2'"))
            );

        if(setShipAddrDto.getShipType().equalsIgnoreCase("TD")){
            query.SET(
                SqlUtils.equalColumnByInput(shippingAddr.storeShipMsgCd, setShipAddrDto.getShipMsgCd()),
                SqlUtils.equalColumnByInput(shippingAddr.storeShipMsg, setShipAddrDto.getShipMsg())
            );
        } else if(setShipAddrDto.getShipType().equalsIgnoreCase("DS")){
            query.SET(
                SqlUtils.equalColumnByInput(shippingAddr.dlvShipMsgCd, setShipAddrDto.getShipMsgCd()),
                SqlUtils.equalColumnByInput(shippingAddr.dlvShipMsg, setShipAddrDto.getShipMsg())
            );
        }

        if(setShipAddrDto.getAuroraShipMsg() != null && !setShipAddrDto.getAuroraShipMsg().isEmpty()) {
            query.SET(
                SqlUtils.equalColumnByInput(shippingAddr.auroraShipMsgCd, setShipAddrDto.getAuroraShipMsgCd()),
                SqlUtils.equalColumnByInput(shippingAddr.auroraShipMsg, setShipAddrDto.getAuroraShipMsg())
            );
        }

        log.debug("updateShippingAddrEditCheck ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateMultiOrderShippingAddrBasicInfo(MultiOrderShipBasicInfoEditDto shipBasicInfoEditDto) {
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        MultiShippingAddrEntity multiShippingAddr = EntityFactory.createEntityIntoValue(MultiShippingAddrEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableNameWithAlias(multiShippingAddr))
            .INNER_JOIN( SqlUtils.createJoinCondition(multiOrderItem, ObjectUtils.toArray(multiShippingAddr.multiBundleNo)) )
            .INNER_JOIN( SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(multiOrderItem.purchaseOrderNo, multiOrderItem.bundleNo, multiOrderItem.orderItemNo)) )
            .INNER_JOIN( SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo)) )
            .SET(
                SqlUtils.equalColumnByInput(multiOrderItem.reserveShipDt, shipBasicInfoEditDto.getReserveShipDt()),
                SqlUtils.equalColumnByInput(multiShippingAddr.giftMsg, shipBasicInfoEditDto.getGiftMsg()),
                SqlUtils.equalColumnByInput(shippingItem.reserveShipDt, shipBasicInfoEditDto.getReserveShipDt()),
                SqlUtils.equalColumnByInput(shippingAddr.giftMsg, shipBasicInfoEditDto.getGiftMsg())
            )
            .WHERE(
                SqlUtils.equalColumnByInput(multiShippingAddr.bundleNo, shipBasicInfoEditDto.getBundleNo()),
                SqlUtils.equalColumnByInput(multiShippingAddr.multiBundleNo, shipBasicInfoEditDto.getMultiBundleNo()),
                SqlUtils.equalColumnByInput(multiOrderItem.shipStatus, "D1")
            );

        log.debug("updateMultiOrderShippingAddrBasicInfo ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateMultiOrderShippingAddrInfo(MultiOrderShipAddrEditDto shipAddrEditDto) {
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        MultiShippingAddrEntity multiShippingAddr = EntityFactory.createEntityIntoValue(MultiShippingAddrEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableNameWithAlias(multiShippingAddr))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(multiOrderItem, ObjectUtils.toArray(multiShippingAddr.multiBundleNo))
            )
            .SET(
                SqlUtils.equalColumnByInput(multiShippingAddr.receiverNm, shipAddrEditDto.getReceiverNm()),
                SqlUtils.equalColumnByInput(multiShippingAddr.shipMobileNo, shipAddrEditDto.getShipMobileNo()),
                SqlUtils.equalColumnByInput(multiShippingAddr.zipcode, shipAddrEditDto.getZipCode()),
                SqlUtils.equalColumnByInput(multiShippingAddr.roadBaseAddr, shipAddrEditDto.getRoadBaseAddr()),
                SqlUtils.equalColumnByInput(multiShippingAddr.roadDetailAddr, shipAddrEditDto.getRoadDetailAddr()),
                SqlUtils.equalColumnByInput(multiShippingAddr.baseAddr, shipAddrEditDto.getBaseAddr() == null ? "" : shipAddrEditDto.getBaseAddr()),
                SqlUtils.equalColumnByInput(multiShippingAddr.detailAddr, shipAddrEditDto.getDetailAddr() == null ? "" : shipAddrEditDto.getDetailAddr())
            )
            .WHERE(
                SqlUtils.equalColumnByInput(multiShippingAddr.bundleNo, shipAddrEditDto.getBundleNo()),
                SqlUtils.equalColumnByInput(multiShippingAddr.multiBundleNo, shipAddrEditDto.getMultiBundleNo()),
                // 프론트에서 수정시 D1만 가능.
                SqlUtils.equalColumnByInput(multiOrderItem.shipStatus, "D1")
            );
        log.debug("updateMultiOrderShippingAddrInfo ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMpOrderClaimInfo(@Param("purchaseOrderNo") long purchaseOrderNo,  @Param("bundleNo") long bundleNo, @Param("isOrigin") Boolean isOrigin) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT_DISTINCT(
                claimBundle.claimNo,
                orderItem.purchaseOrderNo,
                claimBundle.claimBundleNo,
                claimBundle.claimBundleQty,
                claimBundle.claimType,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, claimBundle.claimStatus),
                        SqlUtils.sqlFunction(MysqlFunction.CASE_Y_N, SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")),
                        "'A'"
                    ),
                    "claim_yn"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, claimBundle.claimStatus),
                        SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")),
                        "'N'"
                    ),
                    "claim_process_yn"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")))))
            .WHERE(
                SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo)
            )
            .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimBundle.claimBundleNo)));

        if(isOrigin){
            query.WHERE(
                SqlUtils.OR(
                    SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                    SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo)
                )
            );
        } else {
            query.WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        }
        log.debug("selectMpOrderClaimInfo ::: \n[{}]", SqlUtils.orQuery(query.toString()));
        return SqlUtils.orQuery(query.toString());
    }

    public static String selectMpOrderClaimOrgInfo(@Param("purchaseOrderNo") long purchaseOrderNo){
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT_DISTINCT(
                claimBundle.claimNo,
                orderItem.purchaseOrderNo,
                claimBundle.claimBundleNo,
                claimBundle.claimBundleQty,
                claimBundle.claimType,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, claimBundle.claimStatus),
                        SqlUtils.sqlFunction(MysqlFunction.CASE_Y_N, SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")),
                        "'A'"
                    ),
                    "claim_yn"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, claimBundle.claimStatus),
                        SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")),
                        "'N'"
                    ),
                    "claim_process_yn"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")))))
            .WHERE(SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo))
            .OR()
            .WHERE(SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo))
            .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimBundle.claimBundleNo)))
            ;
        log.debug("selectMpOrderClaimOrgInfo ::: \n[{}]", SqlUtils.orQuery(query.toString()));
        return SqlUtils.orQuery(query.toString());
    }

    public static String selectMpOrderClaimPaymentInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.claimNo, claimMst.claimNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.purchaseOrderNo, claimMst.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totItemPrice, claimMst.totItemPrice),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totItemDiscountAmt, claimMst.totItemDiscountAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totCardDiscountAmt, claimMst.totCardDiscountAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totCartDiscountAmt, claimMst.totCartDiscountAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totShipDiscountAmt, claimMst.totShipDiscountAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totPromoDiscountAmt, claimMst.totPromoDiscountAmt),
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimShipFeeEnclose), "Y"), 0, SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totAddShipPrice)), claimMst.totAddShipPrice),
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimShipFeeEnclose), "Y"), 0, SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totAddIslandShipPrice)), claimMst.totAddIslandShipPrice),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totShipPrice, claimMst.totShipPrice),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totIslandShipPrice, claimMst.totIslandShipPrice),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.pgAmt, claimMst.pgAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.mileageAmt, claimMst.mileageAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.mhcAmt, claimMst.mhcAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.ocbAmt, claimMst.ocbAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.dgvAmt, claimMst.dgvAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totEmpDiscountAmt, claimMst.totEmpDiscountAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totAddTdDiscountAmt, claimMst.totAddTdDiscountAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totAddDsDiscountAmt, claimMst.totAddDsDiscountAmt)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(claimBundle.userNo, userNo),
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
            )
            .GROUP_BY(claimMst.claimNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimMst.totItemPrice, claimMst.totItemPrice),
                SqlUtils.sqlFunction(
                    MysqlFunction.SUM,
                    SqlUtils.customOperator(
                        CustomConstants.PLUS,
                        ObjectUtils.toArray( claimMst.totItemDiscountAmt, claimMst.totCardDiscountAmt, claimMst.totCartDiscountAmt, claimMst.totEmpDiscountAmt, claimMst.totShipDiscountAmt, claimMst.totPromoDiscountAmt, claimMst.totAddShipPrice, claimMst.totAddIslandShipPrice, claimMst.totAddTdDiscountAmt, claimMst.totAddDsDiscountAmt)
                    ),
                    "tot_discount_price"
                ),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimMst.totShipPrice, claimMst.totShipPrice),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimMst.totIslandShipPrice, claimMst.totIslandShipPrice),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimMst.totAddShipPrice, claimMst.totAddShipPrice),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimMst.totAddIslandShipPrice, claimMst.totAddIslandShipPrice),
                SqlUtils.sqlFunction(MysqlFunction.SUM, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimMst.pgAmt, claimMst.mileageAmt, claimMst.mhcAmt, claimMst.ocbAmt, claimMst.dgvAmt)), "tot_payment_amt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimMst.totEmpDiscountAmt, claimMst.totEmpDiscountAmt)
            )
            .FROM(
                SqlUtils.subTableQuery(innerQuery, "cm")
            )
            .GROUP_BY(claimMst.purchaseOrderNo)
            ;

        log.debug("selectMpOrderClaimPaymentInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimPromoDiscountInfoList(@Param("purchaseOrderNo") long purchaseOrderNo) {
        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderPromoEntity orderPromo = EntityFactory.createEntityIntoValue(OrderPromoEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimAddition.additionTypeDetail, "promo_discount"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE_WHEN, orderPromo.promoKind,
                            ObjectUtils.toArray(
                                "'BASIC'", "'행사할인'",
                                "'PICK'", "'행사할인'",
                                "'TOGETHER'", "'행사할인'",
                                "'INTERVAL'", "'행사할인'"
                            )
                        ),
                        claimAddition.additionTypeDetail
                    ),
                    claimAddition.additionTypeDetail
                ),
                claimAddition.additionSumAmt
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimAddition))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimAddition.claimNo, claimAddition.claimBundleNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"))))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(claimAddition.orderItemNo), ObjectUtils.toArray(SqlUtils.equalColumn(claimAddition.orderDiscountNo, orderItem.promoNo))))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderPromo, ObjectUtils.toArray(orderItem.promoNo, orderItem.promoDetailNo, orderItem.purchaseOrderNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimAddition.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimAddition.additionType, ObjectUtils.toArray("'G'", "'S'"))
            );
        SQL query = new SQL()
            .SELECT(
                SqlUtils.aliasToRow(claimAddition.additionTypeDetail, "promo_type"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimAddition.additionSumAmt, "promo_discount_price")
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "ca"))
            .GROUP_BY(claimAddition.additionTypeDetail);

        log.debug("selectClaimPromoDiscountInfoList \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마이페이지 - 주문상세(쿠폰할인)
     *
     * @param purchaseOrderNo 주문거래번호
     * @return 쿠폰할인정보 조회 쿼리
     */
    public static String selectClaimCouponDiscountInfoList(@Param("purchaseOrderNo") long purchaseOrderNo) {
        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderDiscountEntity orderDiscount = EntityFactory.createEntityIntoValue(OrderDiscountEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    claimAddition.additionTypeDetail,
                    ObjectUtils.toArray(
                        "'item_discount'",  "'상품할인'",
                        "'item_coupon'", "'상품할인'",
                        "'card_discount'", "'카드상품할인'",
                        "'ship_coupon'", "'배송비할인'",
                        "'cart_coupon'", "'장바구니할인'",
                        "'add_td_coupon'", "'중복할인'",
                        "'add_ds_coupon'", "'중복할인'"
                    ),
                    claimAddition.additionTypeDetail
                ),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimAddition.additionSumAmt, claimAddition.additionSumAmt)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimAddition))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimAddition.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"))))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderDiscount, ObjectUtils.toArray(claimAddition.orderDiscountNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimAddition.purchaseOrderNo, purchaseOrderNo),
                CustomConstants.ROUND_BRACKET_START.concat(
                    SqlUtils.OR(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimAddition.additionType, ObjectUtils.toArray("'G'", "'S'")),
                        SqlUtils.AND(
                            SqlUtils.equalColumnByInput(claimAddition.additionType, "S"),
                            SqlUtils.equalColumnByInput(claimAddition.additionTypeDetail, "ship_coupon")
                        )
                    )
                ).concat(CustomConstants.ROUND_BRACKET_END)
            )
            .GROUP_BY(claimAddition.claimNo, claimAddition.additionTypeDetail, claimAddition.additionSumAmt, claimAddition.orderDiscountNo);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.aliasToRow(claimAddition.additionTypeDetail, "discount_type"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimAddition.additionSumAmt, "discount_price")
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "ca"))
            .GROUP_BY(claimAddition.additionTypeDetail);

        log.debug("selectClaimCouponDiscountInfoList \n[{}]", query.toString());
        return query.toString();
    }


    public static String selectClaimPaymentInfoList(@Param("purchaseOrderNo") long purchaseOrderNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        PaymentInfoEntity paymentInfo = EntityFactory.createEntityIntoValue(PaymentInfoEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.aliasToRow(paymentInfo.parentMethodCd, "payment_type"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(paymentInfo.paymentType, "DGV"),
                        SqlUtils.sqlFunction(MysqlFunction.AES_DECRYPT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, paymentInfo.dgvCardNo), "!@homeplus#$")),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.PAYMENT_METHOD_INFO, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.siteType), SqlUtils.sqlFunction(MysqlFunction.MAX, paymentInfo.methodCd), paymentInfo.parentMethodCd)),
                                SqlUtils.wrapWithSpace(CustomConstants.SQL_BLANK),
                                SqlUtils.wrapWithSpace(SqlUtils.sqlFunction(MysqlFunction.MAX, paymentInfo.cardNo)),
                                SqlUtils.wrapWithSpace(CustomConstants.SQL_BLANK),
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CASE,
                                    ObjectUtils.toArray(
                                        SqlUtils.equalColumnByInput(paymentInfo.paymentType, "PG"),
                                        SqlUtils.sqlFunction(
                                            MysqlFunction.CASE,
                                            ObjectUtils.toArray(
                                                SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, paymentInfo.cardInstallmentMonth), "1"),
                                                "'일시불'",
                                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.CAST_NCHAR, SqlUtils.sqlFunction(MysqlFunction.MAX, paymentInfo.cardInstallmentMonth)), "'개월'"))
                                            )
                                        ),
                                        "''"
                                    )
                                )
                            )
                        )
                    ),
                    "payment_info"
                ),
                SqlUtils.sqlFunction(MysqlFunction.SUM, SqlUtils.customOperator(CustomConstants.MINUS, claimPayment.paymentAmt, claimPayment.substitutionAmt), claimPayment.paymentAmt)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimMst.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimMst.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(paymentInfo, ObjectUtils.toArray(claimPayment.paymentNo), ObjectUtils.toArray(SqlUtils.equalColumn(claimPayment.parentPaymentMethod, paymentInfo.parentMethodCd))))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(claimPayment.paymentStatus, "P3"),
                SqlUtils.notExists(
                    new SQL().SELECT(claimBundle.claimNo).FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                    .WHERE(
                        SqlUtils.equalColumn(claimBundle.claimNo, claimMst.claimNo),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimStatus, "'C3'")
                    )
                )
            )
            .GROUP_BY(claimMst.purchaseOrderNo, paymentInfo.parentMethodCd, paymentInfo.paymentType)
            ;

        log.debug("selectClaimPaymentInfoList \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderGiftList(@Param("purchaseOrderNo") long purchaseOrderNo){
        PurchaseGiftEntity purchaseGift = EntityFactory.createEntityIntoValue(PurchaseGiftEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);

        SQL notExists = new SQL().SELECT(claimAddition.orderDiscountNo).FROM(EntityFactory.getTableNameWithAlias(claimAddition))
            .WHERE(SqlUtils.equalColumn(claimAddition.purchaseOrderNo, purchaseGift.purchaseOrderNo), SqlUtils.equalColumnByInput(claimAddition.additionType, "G"));
        SQL query = new SQL()
            .SELECT(
                purchaseGift.purchaseOrderNo,
                purchaseGift.storeId,
                purchaseGift.giftNo,
                purchaseGift.giftMsg
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseGift, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo, bundle.purchaseStoreInfoNo)))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    payment,
                    ObjectUtils.toArray(purchaseOrder.paymentNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'"))
                    )
                )
            )
            .WHERE(SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo))
            .OR()
            .WHERE(SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo))
            .AND()
            .WHERE("NOT EXISTS (".concat(notExists.toString()).concat(")"));
        log.debug("selectOrderGiftList \n[{}]", SqlUtils.orQuery(query.toString()));
        return SqlUtils.orQuery(query.toString());
    }

    public static String selectOrderGiftItemList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo){
        PurchaseGiftEntity purchaseGift = EntityFactory.createEntityIntoValue(PurchaseGiftEntity.class, Boolean.TRUE);
        PurchaseGiftItemEntity purchaseGiftItem = EntityFactory.createEntityIntoValue(PurchaseGiftItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);

        SQL notExists = new SQL().SELECT(claimAddition.orderDiscountNo).FROM(EntityFactory.getTableNameWithAlias(claimAddition))
            .WHERE(SqlUtils.equalColumn(claimAddition.purchaseOrderNo, purchaseGift.purchaseOrderNo), SqlUtils.equalColumnByInput(claimAddition.additionType, "G"));

        SQL query = new SQL()
            .SELECT(
                purchaseGift.purchaseOrderNo,
                purchaseGift.storeId,
                orderItem.bundleNo,
                purchaseGift.giftNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseGift.giftMsg, purchaseGift.giftMsg),
                purchaseGift.giftTargetType,
                purchaseGiftItem.itemNo,
                purchaseGift.stdVal
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseGift, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseGiftItem, ObjectUtils.toArray(purchaseGift.purchaseGiftNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseGift.purchaseOrderNo, purchaseGiftItem.itemNo, purchaseGift.storeId)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo),
                "NOT EXISTS (".concat(notExists.toString()).concat(")")
            )
            .GROUP_BY(purchaseGift.purchaseOrderNo, purchaseGift.storeId, orderItem.bundleNo, purchaseGift.giftNo, purchaseGift.giftTargetType, purchaseGift.stdVal, purchaseGiftItem.itemNo)
            ;
        log.debug("selectOrderGiftItemList \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPickStoreInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        OrderPickupInfoEntity orderPickupInfo = EntityFactory.createEntityIntoValue(OrderPickupInfoEntity.class, Boolean.TRUE);
        ItmPartnerStoreEntity itmPartnerStore = EntityFactory.createEntityIntoValue(ItmPartnerStoreEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                orderPickupInfo.placeAddrExtnd,
                orderPickupInfo.placeExpln,
                orderPickupInfo.imgUrl,
                orderPickupInfo.startTime,
                orderPickupInfo.endTime,
                orderPickupInfo.placeNm,
                orderPickupInfo.telNo,
                itmPartnerStore.storeNm
            )
            .FROM(EntityFactory.getTableNameWithAlias(orderPickupInfo))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmPartnerStore),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(orderPickupInfo.storeId), itmPartnerStore)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(orderPickupInfo.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectPickStoreInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderTicketDetailInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo){
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderTicketEntity orderTicket = EntityFactory.createEntityIntoValue(OrderTicketEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT_DISTINCT(
                purchaseOrder.purchaseOrderNo,
                orderItem.itemNo,
                orderItem.bundleNo,
                orderItem.itemNm1,
                orderItem.itemQty
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderTicket, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, orderItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_TICKET")
            )
            ;

        log.debug("selectOrderTicketDetailInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderTicketDetailItemInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("userNo") long userNo){
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderTicketEntity orderTicket = EntityFactory.createEntityIntoValue(OrderTicketEntity.class, Boolean.TRUE);
        ClaimTicketEntity claimTicket = EntityFactory.createEntityIntoValue(ClaimTicketEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                orderTicket.orderTicketNo,
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(orderTicket.ticketValidEndDt, CustomConstants.YYYYMMDD_COMMA.concat(" ").concat(CustomConstants.HHMI)), orderTicket.ticketValidEndDt),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(orderTicket.ticketUseDt, CustomConstants.YYYYMMDD_COMMA), orderTicket.ticketUseDt),
                orderTicket.ticketStatus,
                orderTicket.ticketSendCnt,
                shippingAddr.shipMobileNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, claimTicket.claimTicketNo),
                        SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.sqlFunction(MysqlFunction.NOT_IN, "ct.claim_status", ObjectUtils.toArray("'C4'", "'C9'"))),
                        SqlUtils.sqlFunction(MysqlFunction.CASE_Y_N, SqlUtils.equalColumnByInput(orderTicket.ticketStatus, "T1"))
                    ),
                    "claim_yn"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, claimTicket.claimNo, claimTicket.claimNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderTicket, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, orderItem.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(orderTicket.bundleNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    SqlUtils.subTableQuery(
                        new SQL().SELECT(claimTicket.claimTicketNo, claimTicket.orderTicketNo, claimTicket.claimNo, claimBundle.claimStatus).FROM(EntityFactory.getTableNameWithAlias(claimTicket))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimTicket.claimBundleNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")))))
                        .WHERE(SqlUtils.equalColumnByInput(claimBundle.purchaseOrderNo, purchaseOrderNo))
                        , "ct"
                    ),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(orderTicket.orderTicketNo, claimTicket.orderTicketNo)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_TICKET")
            );

        log.debug("selectOrderTicketDetailItemInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderDetailClaimInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL orderItemQuery = new SQL()
            .SELECT(
                orderItem.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, orderItem.itemQty, "qty")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .WHERE(
                SqlUtils.OR(
                    SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                    SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo)
                )
            )
            .GROUP_BY(orderItem.bundleNo);

        SQL claimItemQuery = new SQL()
            .SELECT(
                claimBundle.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimItem.claimItemQty, "qty"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimStatus, claimBundle.claimStatus)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    claimItem,
                    ObjectUtils.toArray(claimBundle.claimBundleNo, claimBundle.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    ObjectUtils.toArray(
                        SqlUtils.OR(
                            SqlUtils.equalColumn(claimItem.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                            SqlUtils.equalColumn(claimItem.purchaseOrderNo, purchaseOrder.orgPurchaseOrderNo)
                        )
                    )
                )
            )
            .WHERE(SqlUtils.equalColumnByInput(claimBundle.purchaseOrderNo, purchaseOrderNo))
            .GROUP_BY(claimBundle.bundleNo);

        SQL query = new SQL()
            .SELECT(
                "orderItem.bundle_no",
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.ANGLE_BRACKET_RIGHT,
                            SqlUtils.customOperator(
                                CustomConstants.MINUS,
                                ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, "orderItem.qty"), SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, "claimItem.qty"))
                            ),
                            "0"
                        ),
                        SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.customOperator(CustomConstants.NOT_EQUAL, "claimItem.claim_status", "'C3'")),
                        "'N'"
                    ),
                    "claim_yn"
                )
            )
            .FROM(
                "(".concat(orderItemQuery.toString()).concat(" ) orderItem")
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    "(".concat(claimItemQuery.toString()).concat(" ) claimItem"),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn("orderItem.bundle_no", "claimItem.bundle_no")
                    )
                )
            );
        log.debug("selectOrderDetailClaimInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMultiOrderDetailClaimInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimMultiBundleEntity claimMultiBundle = EntityFactory.createEntityIntoValue(ClaimMultiBundleEntity.class, Boolean.TRUE);
        ClaimMultiItemEntity claimMultiItem = EntityFactory.createEntityIntoValue(ClaimMultiItemEntity.class, Boolean.TRUE);

        SQL orderItemQuery = new SQL()
            .SELECT(
                multiOrderItem.multiBundleNo,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, multiOrderItem.itemQty, "qty")
            )
            .FROM(EntityFactory.getTableNameWithAlias(multiOrderItem))
            .WHERE(
                SqlUtils.equalColumnByInput(multiOrderItem.purchaseOrderNo, purchaseOrderNo)
            )
            .GROUP_BY(multiOrderItem.multiBundleNo);

        SQL claimItemQuery = new SQL()
            .SELECT(
                claimMultiBundle.multiBundleNo,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMultiItem.claimItemQty, "qty"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimStatus, claimBundle.claimStatus)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    claimItem,
                    ObjectUtils.toArray(claimBundle.claimBundleNo, claimBundle.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiBundle, ObjectUtils.toArray(claimBundle.claimBundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiItem, ObjectUtils.toArray(claimMultiBundle.claimMultiBundleNo, claimItem.claimItemNo)))
            .WHERE(SqlUtils.equalColumnByInput(claimBundle.purchaseOrderNo, purchaseOrderNo))
            .GROUP_BY(claimMultiBundle.multiBundleNo);

        SQL query = new SQL()
            .SELECT(
                "orderItem.multi_bundle_no",
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.ANGLE_BRACKET_RIGHT,
                            SqlUtils.customOperator(
                                CustomConstants.MINUS,
                                ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, "orderItem.qty"), SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, "claimItem.qty"))
                            ),
                            "0"
                        ),
                        SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.customOperator(CustomConstants.NOT_EQUAL, "claimItem.claim_status", "'C3'")),
                        "'N'"
                    ),
                    "claim_yn"
                )
            )
            .FROM(
                "(".concat(orderItemQuery.toString()).concat(" ) orderItem")
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    "(".concat(claimItemQuery.toString()).concat(" ) claimItem"),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn("orderItem.multi_bundle_no", "claimItem.multi_bundle_no")
                    )
                )
            );
        log.debug("selectMultiOrderDetailClaimInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectSafetyIssueInfo(@Param("bundleNo") long bundleNo){
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                shippingItem.purchaseOrderNo,
                shippingAddr.safetyUseYn,
                shippingAddr.shipMobileNo,
                shippingAddr.zipcode,
                shippingAddr.roadBaseAddr,
                shippingAddr.roadDetailAddr,
                shippingAddr.receiverNm,
                shippingAddr.auroraShipMsg,
                shippingAddr.dlvShipMsg,
                shippingAddr.storeShipMsg,
                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_24, shippingItem.reserveShipDt, shippingItem.reserveShipDt)
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo)
            )
            .LIMIT(1)
            .OFFSET(0);
        log.debug("selectSafetyIssueInfo ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderBenefitInfoList(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseAccumulateEntity purchaseAccumulate = EntityFactory.createEntityIntoValue(PurchaseAccumulateEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ShippingMileagePaybackEntity shippingMileagePayback = EntityFactory.createEntityIntoValue(ShippingMileagePaybackEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        SQL pointOfQuery = new SQL()
            .SELECT(
                SqlUtils.caseWhenElse(
                    purchaseAccumulate.pointKind,
                    ObjectUtils.toArray(
                        "'MHC'", "'마이홈플러스포인트'",
                        "'MILEAGE'", "'마일리지'"
                    ),
                    purchaseAccumulate.pointKind,
                    "benefit_type"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseAccumulate.pointKind, "MHC"),
                        purchaseAccumulate.resultPoint,
                        purchaseAccumulate.mileageAccAmt
                    ),
                    "benefit_amt"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseAccumulate))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(ObjectUtils.toArray(purchaseAccumulate.purchaseOrderNo))))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );

        SQL paybackOfQuery = new SQL()
            .SELECT(
                SqlUtils.aliasToRow("'배송비 PAYBACK'", "benefit_type"),
                SqlUtils.aliasToRow(shippingMileagePayback.shipPricePayback, "benefit_amt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingMileagePayback))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingMileagePayback.bundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(bundle.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(payment.paymentStatus, "P3"))))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingMileagePayback.orgPurchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(shippingMileagePayback.cancelYn, "N")
            );

        SQL query = new SQL()
            .SELECT("*")
            .FROM(
                "(".concat(pointOfQuery.toString())
                    .concat("\nUNION ALL\n")
                .concat(paybackOfQuery.toString())
                .concat(") t")
            );

        log.debug("selectOrderBenefitInfoList ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimBenefitInfoList(@Param("purchaseOrderNo") long purchaseOrderNo) {
        ClaimAccumulateEntity claimAccumulate = EntityFactory.createEntityIntoValue(ClaimAccumulateEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.caseWhenElse(
                    claimAccumulate.pointKind,
                    ObjectUtils.toArray(
                        "'MHC'", "'마이홈플러스포인트'",
                        "'MILEAGE'", "'마일리지'"
                    ),
                    claimAccumulate.pointKind,
                    "benefit_type"
                ),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimAccumulate.resultPoint, "benefit_amt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimAccumulate))
            .WHERE(
                SqlUtils.equalColumnByInput(claimAccumulate.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(claimAccumulate.resultCd, "0000")
            )
            .GROUP_BY(claimAccumulate.pointKind, claimAccumulate.purchaseOrderNo);
        log.debug("selectClaimBenefitInfoList ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderPresentInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.DATEDIFF, ObjectUtils.toArray(purchaseOrder.dsGiftAutoCancelDt, "NOW()"), "present_expire_day"),
                SqlUtils.aliasToRow(shippingAddr.receiverNm, "present_receiver_nm"),
                SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.sqlFunction(MysqlFunction.IS_NULL, shippingItem.giftAcpDt), "present_receive_yn"),
                SqlUtils.aliasToRow(shippingAddr.dsGiftMsg, "present_msg"),
                SqlUtils.aliasToRow(shippingItem.giftAcpDt, "present_acp_dt"),
                SqlUtils.aliasToRow(purchaseOrder.dsGiftAutoCancelYn, "present_auto_cancel_yn")

            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectOrderPresentInfo ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimShippingStatus(@Param("claimNo") long claimNo) {
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class, Boolean.TRUE);
        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, claimPickShipping.pickStatus, "pick_ship_status"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, claimExchShipping.exchStatus, "exch_ship_status")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimPickShipping, ObjectUtils.toArray(claimBundle.claimBundleNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimExchShipping, ObjectUtils.toArray(claimBundle.claimBundleNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo),
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'C'")
            )
            .ORDER_BY(SqlUtils.orderByesc(claimBundle.chgDt))
            .LIMIT(1)
            .OFFSET(0);
        log.debug("selectClaimShippingStatus ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectUserGradeUserInfo(@Param("userNo") long userNo, @Param("searchDt") String searchDt, @Param("siteType") String siteType) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimInfoEntity claimInfo = EntityFactory.createEntityIntoValue(ClaimInfoEntity.class, Boolean.TRUE);

        SQL dataQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.FN_ORIGIN_PURCHASE_ORDER_NO, purchaseOrder.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                orderItem.orderItemNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemQty, orderItem.itemQty),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimInfo.completeQty, "claim_qty"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderType, purchaseOrder.orderType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.storeType, orderItem.storeType),
                SqlUtils.aliasToRow(SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.shipFshDt), purchaseOrder.orderDt),
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.pgDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.mhcDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.ocbDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.dgvDivideAmt)
                    ),
                    "payment_amt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL_SUM_ZERO,
                    SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimInfo.pgAmt, claimInfo.mhcAmt, claimInfo.ocbAmt, claimInfo.dgvAmt)),
                    "claim_amt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.userNo, purchaseOrder.userNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemNm1, orderItem.itemNm1)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    orderItem,
                    ObjectUtils.toArray(purchaseOrder.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.deptNo, "3106"),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.division, "9")
                    )
                )
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.orderItemNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("'D4'", "'D5'")))))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderPaymentDivide, ObjectUtils.toArray(shippingItem.purchaseOrderNo, shippingItem.orderItemNo, shippingItem.bundleNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(shippingItem.purchaseOrderNo, shippingItem.bundleNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimInfo, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo, shippingItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT_EQUAL, purchaseOrder.shipFshDt, SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.appendSingleQuote(searchDt))),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, purchaseOrder.shipFshDt, SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_MON, ObjectUtils.toArray(SqlUtils.appendSingleQuote(searchDt), 1))))
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo, orderItem.orderItemNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "order_dt", "order_dt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "store_type", "store_type"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "order_type", "order_type"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.sqlFunction(MysqlFunction.COUNT, "order_item_no"), "1"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_nm1"), "' 외 '",
                                SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.COUNT, "order_item_no"), "1")),
                                "'건'"
                            )
                        ),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, "item_nm1")
                    ),
                    "item_nm1"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.ANGLE_BRACKET_RIGHT,
                            SqlUtils.customOperator(
                                CustomConstants.MINUS,
                                SqlUtils.sqlFunction(MysqlFunction.SUM, "item_qty"),
                                SqlUtils.sqlFunction(MysqlFunction.SUM, "claim_qty")
                            ),
                            "0"
                        ), 1, 0
                    ),
                    "order_cnt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "payment_amt", "payment_amt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "claim_amt", "claim_price"),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.SUM, "payment_amt"),
                        SqlUtils.sqlFunction(MysqlFunction.SUM, "claim_amt")
                    ),
                    "order_price"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, "order_type"), "ORD_COMBINE"), "'합배송'", "'배송'"),
                    "ship_type"
                )
            )
            .FROM(SqlUtils.subTableQuery(dataQuery, "Z"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, "payment_amt", "0")
            )
            .GROUP_BY(SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo));
        log.debug("selectUserGradeUserInfo ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectTicketFromCoopCheck(@Param("purchaseOrderNo") long purchaseOrderNo){
        OrderTicketEntity orderTicket = EntityFactory.createEntityIntoValue(OrderTicketEntity.class);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, orderTicket.issueChannel, "cnt")
            )
            .FROM(EntityFactory.getTableName(orderTicket))
            .WHERE(
                SqlUtils.equalColumnByInput(orderTicket.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderTicket.issueChannel, "COOP")
            );
        log.debug("selectTicketFromCoopCheck ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectSubstitutionAmt(@Param("purchaseOrderNo") long purchaseOrderNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimPayment.substitutionAmt, claimPayment.paymentAmt)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimMst.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimPayment.paymentStatus, "P3"))))
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(claimMst.oosYn, "Y")
            );
        log.debug("selectSubstitutionAmt ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMultiOrderShipAddrInfo(@Param("multiBundleNo") long multiBundleNo ) {
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        MultiBundleEntity multiBundle = EntityFactory.createEntityIntoValue(MultiBundleEntity.class, Boolean.TRUE);
        MultiSafetyIssueEntity multiSafetyIssue = EntityFactory.createEntityIntoValue(MultiSafetyIssueEntity.class, "safety");
        MultiShippingAddrEntity multiShippingAddr = EntityFactory.createEntityIntoValue(MultiShippingAddrEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                multiBundle.multiBundleNo,
                multiShippingAddr.receiverNm,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(multiBundle.storeId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "store_nm"
                ),
                multiShippingAddr.shipMobileNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(multiShippingAddr.safetyUseYn, "Y"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE_WHEN,
                            multiSafetyIssue.issueStatus,
                            ObjectUtils.toArray(
                                "'R'", "'발급 요청중'",
                                "'C'", multiSafetyIssue.issuePhoneNo,
                                "'F'", SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(multiSafetyIssue.issueType, "N"), "'발급실패'", "'변경실패'"))
                            )
                        ),
                        "'신청안함'"
                    ),
                    "safety_nm"
                ),
                multiShippingAddr.safetyUseYn,
                multiShippingAddr.zipcode,
                multiShippingAddr.roadBaseAddr,
                multiShippingAddr.roadDetailAddr,
                multiShippingAddr.baseAddr,
                multiShippingAddr.detailAddr,
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, "'M'"), "ship_type"),
                orderItem.purchaseOrderNo,
                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_24, multiOrderItem.reserveShipDt, multiOrderItem.reserveShipDt)
            )
            .FROM(EntityFactory.getTableNameWithAlias(multiBundle))
            .INNER_JOIN(SqlUtils.createJoinCondition( multiOrderItem, ObjectUtils.toArray(multiBundle.multiBundleNo) ))
            .INNER_JOIN(SqlUtils.createJoinCondition( orderItem, ObjectUtils.toArray(multiOrderItem.orderItemNo) ))
            .INNER_JOIN(SqlUtils.createJoinCondition( multiShippingAddr, ObjectUtils.toArray(multiBundle.multiBundleNo) ))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(multiSafetyIssue, "safety"),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(multiShippingAddr.multiShipAddrNo, multiShippingAddr.purchaseOrderNo, multiShippingAddr.multiBundleNo ), multiSafetyIssue)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(multiBundle.multiBundleNo, multiBundleNo)
            );

        StringBuilder sb = new StringBuilder();
        sb.append(query.toString());
        sb.append("\n LIMIT 0, 1");

        log.debug("selectOrderShipAddrInfo ::: \n[{}]", sb.toString());
        return sb.toString();
    }

    public static String selectUserGradeUserInfoByClaim(@Param("userNo") long userNo, @Param("searchDt") String searchDt, @Param("siteType") String siteType) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        ClaimInfoEntity claimInfo = EntityFactory.createEntityIntoValue(ClaimInfoEntity.class, Boolean.TRUE);

        SQL claimQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB"),
                        purchaseOrder.orgPurchaseOrderNo,
                        purchaseOrder.purchaseOrderNo
                    ),
                    purchaseOrder.purchaseOrderNo
                ),
                orderItem.orderItemNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemQty, orderItem.itemQty),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimInfo.completeQty, "claim_qty"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderType, purchaseOrder.orderType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.storeType, orderItem.storeType),
                SqlUtils.aliasToRow(SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.chgDt), purchaseOrder.orderDt),
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.pgDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.mhcDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.ocbDivideAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderPaymentDivide.dgvDivideAmt)
                    ),
                    "payment_amt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL_SUM_ZERO,
                    SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimInfo.pgAmt, claimInfo.mhcAmt, claimInfo.ocbAmt, claimInfo.dgvAmt)),
                    "claim_amt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.userNo, purchaseOrder.userNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemNm1, orderItem.itemNm1)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(purchaseOrder.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    orderItem,
                    ObjectUtils.toArray(claimBundle.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.deptNo, "3106"),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.division, "9")
                    )
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    claimReq,
                    ObjectUtils.toArray(claimBundle.claimBundleNo),
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.ANGLE_BRACKET_RIGHT_EQUAL,
                            claimReq.completeDt,
                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.appendSingleQuote(searchDt))
                        )
                    )
                )
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.orderItemNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("'D4'", "'D5'")))))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderPaymentDivide, ObjectUtils.toArray(shippingItem.purchaseOrderNo, shippingItem.orderItemNo, shippingItem.bundleNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimInfo, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo, shippingItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.userNo, userNo),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, purchaseOrder.shipFshDt, SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.appendSingleQuote(searchDt)))
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo, orderItem.orderItemNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "order_dt", "order_dt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "store_type", "store_type"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "order_type", "order_type"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.sqlFunction(MysqlFunction.COUNT, "order_item_no"), "1"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_nm1"), "' 외 '",
                                SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.COUNT, "order_item_no"), "1")),
                                "'건'"
                            )
                        ),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, "item_nm1")
                    ),
                    "item_nm1"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.ANGLE_BRACKET_LEFT,
                            SqlUtils.customOperator(
                                CustomConstants.MINUS,
                                SqlUtils.sqlFunction(MysqlFunction.SUM, "item_qty"),
                                SqlUtils.sqlFunction(MysqlFunction.SUM, "claim_qty")
                            ),
                            "1"
                        ),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, SqlUtils.sqlFunction(MysqlFunction.MAX, "order_type"), "'ORD_COMBINE'"),
                                -1, 0
                            )
                        ),
                        0
                    ),
                    "order_cnt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "payment_amt", "payment_amt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "claim_amt", "claim_price"),
                SqlUtils.customOperator(CustomConstants.MULTIPLY, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.SUM, "claim_amt"), "-1"), "order_price"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, "order_type"), "ORD_COMBINE"), "'합배송'", "'배송'"),
                    "ship_type"
                )
            )
            .FROM(SqlUtils.subTableQuery(claimQuery, "Z"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, "payment_amt", "0")
            )
            .GROUP_BY(SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo));
        log.debug("selectUserGradeUserInfoByClaim ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateOrderUserRecentlyInfo(@Param("auroraShipMsgCd") String auroraShipMsgCd, @Param("auroraShipMsg") String auroraShipMsg, @Param("userNo") long userNo) {
        OrderUserRecentlyInfoEntity orderUserRecentlyInfo = EntityFactory.createEntityIntoValue(OrderUserRecentlyInfoEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(orderUserRecentlyInfo))
            .SET(
                SqlUtils.equalColumnByInput(orderUserRecentlyInfo.auroraShipMsgCd, auroraShipMsgCd),
                SqlUtils.equalColumnByInput(orderUserRecentlyInfo.auroraShipMsg, auroraShipMsg)
            )
            .WHERE(
                SqlUtils.equalColumnByInput(orderUserRecentlyInfo.userNo, userNo)
            );
        log.debug("updateOrderUserRecentlyInfo ::: \n[{}]", query.toString());
        return query.toString();
    }
}
