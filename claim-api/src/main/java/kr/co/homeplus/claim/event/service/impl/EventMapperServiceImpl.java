package kr.co.homeplus.claim.event.service.impl;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.event.mapper.EventReadMapper;
import kr.co.homeplus.claim.event.model.PayCompleteByAmtCheckSetDto;
import kr.co.homeplus.claim.event.model.PurchaseByShipCompleteGetDto;
import kr.co.homeplus.claim.event.model.PurchaseByShipCompleteSetDto;
import kr.co.homeplus.claim.event.model.PurchaseEventCheckSetDto;
import kr.co.homeplus.claim.event.model.PurchaseMileagePaybackGetDto.PurchasePaymentList;
import kr.co.homeplus.claim.event.model.PurchaseMileagePaybackSetDto;
import kr.co.homeplus.claim.event.model.ShipCompleteByAmtCheckSetDto;
import kr.co.homeplus.claim.event.service.EventMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EventMapperServiceImpl implements EventMapperService {
    private final EventReadMapper readMapper;

    @Override
    public LinkedHashMap<String, Object> getEventPurchaseOrderCheck(PurchaseEventCheckSetDto setDto) {
        return readMapper.selectEventPurchaseOrderCheck(setDto);
    }

    @Override
    public List<PurchasePaymentList> getEventPurchasePaymentList(PurchaseMileagePaybackSetDto setDto) {
        return readMapper.selectEventPurchasePaymentInfo(setDto);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getEventShipCompleteList(ShipCompleteByAmtCheckSetDto setDto) {
        return readMapper.selectShipCompleteByBasicAmt(setDto);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getEventShipCompleteDayLimitList(ShipCompleteByAmtCheckSetDto setDto) {
        return readMapper.selectShipCompleteByDayLimit(setDto);
    }

    @Override
    public List<PurchaseByShipCompleteGetDto> getNotStoreKindPurchaseByShipComplete(PurchaseByShipCompleteSetDto setDto) {
        return readMapper.selectNotStoreKindShipCompleteList(setDto);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getEventPayCompleteList(PayCompleteByAmtCheckSetDto setDto) {
        return readMapper.selectPayCompleteByBasicAmt(setDto);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getEventPayCompleteDayLimitList(PayCompleteByAmtCheckSetDto setDto) {
        return readMapper.selectPayCompleteByDayLimit(setDto);
    }
}
