package kr.co.homeplus.claim.enums;

public enum DateType {
    YEAR, MONTH, DAY, HOUR, MIN, SEC
}
