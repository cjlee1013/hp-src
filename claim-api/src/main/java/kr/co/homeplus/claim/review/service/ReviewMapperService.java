package kr.co.homeplus.claim.review.service;

import java.util.List;
import kr.co.homeplus.claim.review.model.ReviewItemDto;
import kr.co.homeplus.claim.review.model.ReviewSearchGetDto;
import kr.co.homeplus.claim.review.model.ReviewSearchSetDto;

public interface ReviewMapperService {

    List<ReviewSearchGetDto> getReviewInfoList(ReviewSearchSetDto setDto);

    List<ReviewItemDto> getReviewItemList(long purchaseOrderNo, long bundleNo);

    List<ReviewItemDto> getGhsReviewItemList(long ordNo);
}
