package kr.co.homeplus.claim.office.sql;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class ClaimDataSql {

    public static String selectClaimData(@Param("bundleNo") long bundleNo) {
        SQL query = new SQL()
            .SELECT(
                "claim_no",
                "claim_bundle_no"
            )
            .FROM(
                "claim_bundle"
            )
            .WHERE(
                "bundle_no = ".concat(String.valueOf(bundleNo))
            );
        return query.toString();
    }

}
