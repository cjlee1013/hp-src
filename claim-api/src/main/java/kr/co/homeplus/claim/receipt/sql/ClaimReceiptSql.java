package kr.co.homeplus.claim.receipt.sql;

import kr.co.homeplus.claim.constants.CustomConstants;
import kr.co.homeplus.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claim.entity.claim.ClaimInfoEntity;
import kr.co.homeplus.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claim.entity.claim.ClaimMstEntity;
import kr.co.homeplus.claim.entity.claim.ClaimPaymentEntity;
import kr.co.homeplus.claim.entity.dms.ItmMngCodeEntity;
import kr.co.homeplus.claim.entity.dms.ItmPartnerEntity;
import kr.co.homeplus.claim.entity.dms.ItmPartnerSellerEntity;
import kr.co.homeplus.claim.entity.order.CashReceiptCompleteEntity;
import kr.co.homeplus.claim.entity.order.CashReceiptEntity;
import kr.co.homeplus.claim.entity.order.CashReceiptV1Entity;
import kr.co.homeplus.claim.entity.order.PaymentEntity;
import kr.co.homeplus.claim.entity.order.PaymentInfoEntity;
import kr.co.homeplus.claim.entity.order.PaymentMethodEntity;
import kr.co.homeplus.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claim.enums.MysqlFunction;
import kr.co.homeplus.claim.factory.EntityFactory;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseMethodReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.order.ClaimRefundReceiptSetDto;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ClaimReceiptSql {

    public static String selectClaimPurchaseReceiptMainInfo(ClaimPurchaseReceiptSetDto setDto) {
        ClaimInfoEntity claimInfo = EntityFactory.createEntityIntoValue(ClaimInfoEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, "claimItem");
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        SQL claimUpperQuery = new SQL()
            .SELECT(
                claimInfo.claimNo,
                claimInfo.purchaseOrderNo,
                SqlUtils.aliasToRow(claimInfo.paymentCompleteDt, "complete_dt"),
                claimItem.itemName,
                SqlUtils.aliasToRow("1", "cnt"),
                claimInfo.claimPrice,
                purchaseOrder.buyerNm,
                claimInfo.orderItemNo,
                payment.paymentFshDt
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimInfo))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimItem, "claimItem"),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(claimInfo.claimNo, claimItem.claimBundleNo, claimInfo.orderItemNo), claimItem)
                    )
                )
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimInfo.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimInfo.claimNo, setDto.getClaimNo())
            );
        SQL claimLowerQuery = new SQL()
            .SELECT(
                claimInfo.claimNo,
                claimInfo.purchaseOrderNo,
                SqlUtils.aliasToRow(claimInfo.paymentCompleteDt, "complete_dt"),
                SqlUtils.aliasToRow("'0'", claimItem.itemName),
                SqlUtils.aliasToRow("0", "cnt"),
                claimInfo.claimPrice,
                purchaseOrder.buyerNm,
                claimInfo.orderItemNo,
                payment.paymentFshDt
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimInfo))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimInfo.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimInfo.claimNo, setDto.getClaimNo()),
                SqlUtils.equalColumnByInput(claimInfo.orderItemNo, 0)
            );

        SQL subQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(claimInfo.claimNo), claimInfo.claimNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(claimInfo.purchaseOrderNo), claimInfo.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "complete_dt", "complete_dt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(claimItem.itemName), claimItem.itemName),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "cnt", "cnt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, SqlUtils.nonAlias(claimInfo.claimPrice), "tot_payment_amt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(purchaseOrder.buyerNm), purchaseOrder.buyerNm),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(payment.paymentFshDt), "payment_fsh_dt")
            )
            .FROM(
                SqlUtils.getAppendAliasToTableName("(".concat(claimUpperQuery.toString()).concat("\nUNION ALL\n").concat(claimLowerQuery.toString()).concat(")"), "t")
            );

        SQL query = new SQL()
            .SELECT(
                "claim_no",
                "purchase_order_no",
                SqlUtils.sqlFunction(
                    MysqlFunction.CONCAT,
                    ObjectUtils.toArray(
                        "complete_dt",
                        "'('",
                        SqlUtils.sqlFunction(MysqlFunction.SUBSTR, ObjectUtils.toArray("'일월화수목금토'", SqlUtils.sqlFunction(MysqlFunction.DAYOFWEEK, SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray("complete_dt", CustomConstants.YYYYMMDD))), "1")),
                        "')'"
                    ),
                    "claim_dt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CONCAT,
                    ObjectUtils.toArray(
                        "item_name",
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, "cnt", "1"),
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'외 '", SqlUtils.customOperator(CustomConstants.MINUS, "cnt", "1"), "'건'")),
                                "''"
                            )
                        )
                    ),
                    "display_item_nm"
                ),
                "tot_payment_amt",
                "buyer_nm",
                "payment_fsh_dt"
            )
            .FROM(
                "(".concat(subQuery.toString()).concat(") s")
            );
        log.debug("selectClaimPurchaseReceiptMainInfo :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimPurchaseReceiptItemInfo(ClaimPurchaseReceiptSetDto setDto) {
        ClaimInfoEntity claimInfo = EntityFactory.createEntityIntoValue(ClaimInfoEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, "claimItem");
        ItmPartnerEntity itmPartner = EntityFactory.createEntityIntoValue(ItmPartnerEntity.class, Boolean.TRUE);

        SQL primaryQuery = new SQL()
            .SELECT(
                claimItem.partnerId,
                claimItem.itemName,
                claimInfo.completeQty,
                SqlUtils.aliasToRow(claimInfo.claimPrice, claimInfo.completeAmt),
                claimInfo.claimNo,
                claimInfo.itemNo,
                claimInfo.orderItemNo,
                claimInfo.storeType,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimInfo.mallType, "TD"),
                        SqlUtils.caseWhenElse(
                            claimInfo.storeType,
                            ObjectUtils.toArray(
                                "'HYPER'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'홈플러스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S")))),
                                "'CLUB'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'더클럽 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S")))),
                                "'EXP'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'익스프레스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S"))))
                            ),
                            SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'홈플러스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S"))))
                        ),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.partnerId, "P"))
                    ),
                    "partner_name"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimInfo))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimInfo.purchaseOrderNo, claimInfo.claimBundleNo, claimInfo.claimNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimItem, "claimItem"),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(claimInfo.claimNo, claimItem.claimBundleNo, claimInfo.orderItemNo), claimItem)
                    )
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmPartner),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(claimItem.partnerId), itmPartner)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimInfo.userNo, setDto.getUserNo()),
                SqlUtils.equalColumnByInput(claimInfo.claimNo, setDto.getClaimNo()),
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimInfo.claimPrice, "0")
            );
        // 배송비
        SQL bundleFeeQuery = new SQL()
            .SELECT(
                claimInfo.partnerId,
                SqlUtils.appendSingleQuoteWithAlias("배송비", claimItem.itemName),
                SqlUtils.appendSingleQuoteWithAlias("1", claimInfo.completeQty),
                SqlUtils.aliasToRow(claimInfo.shipAmt, claimInfo.completeAmt),
                claimInfo.claimNo,
                claimInfo.itemNo,
                claimInfo.orderItemNo,
                claimInfo.storeType,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimInfo.mallType, "TD"),
                        SqlUtils.caseWhenElse(
                            claimInfo.storeType,
                            ObjectUtils.toArray(
                                "'HYPER'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'홈플러스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S")))),
                                "'CLUB'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'더클럽 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S")))),
                                "'EXP'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'익스프레스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S"))))
                            ),
                            SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'홈플러스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S"))))
                        ),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.partnerId, "P"))
                    ),
                    "partner_name"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimInfo))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmPartner),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(claimInfo.partnerId), itmPartner)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimInfo.userNo, setDto.getUserNo()),
                SqlUtils.equalColumnByInput(claimInfo.claimNo, setDto.getClaimNo()),
                SqlUtils.equalColumn(claimInfo.orderItemNo, "0")
            ).AND()
            .WHERE(
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimInfo.shipAmt, "0")
            );
        SQL bundleDiscountFeeQuery = new SQL()
            .SELECT(
                claimInfo.partnerId,
                SqlUtils.appendSingleQuoteWithAlias("배송비 할인", claimItem.itemName),
                SqlUtils.appendSingleQuoteWithAlias("1", claimInfo.completeQty),
                SqlUtils.aliasToRow(SqlUtils.customOperator(CustomConstants.MULTIPLY, claimInfo.shipDiscountAmt, "-1"), claimInfo.completeAmt),
                claimInfo.claimNo,
                claimInfo.itemNo,
                claimInfo.orderItemNo,
                claimInfo.storeType,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimInfo.mallType, "TD"),
                        SqlUtils.caseWhenElse(
                            claimInfo.storeType,
                            ObjectUtils.toArray(
                                "'HYPER'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'홈플러스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S")))),
                                "'CLUB'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'더클럽 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S")))),
                                "'EXP'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'익스프레스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S"))))
                            ),
                            SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'홈플러스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S"))))
                        ),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.partnerId, "P"))
                    ),
                    "partner_name"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimInfo))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmPartner),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(claimInfo.partnerId), itmPartner)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimInfo.userNo, setDto.getUserNo()),
                SqlUtils.equalColumnByInput(claimInfo.claimNo, setDto.getClaimNo()),
                SqlUtils.equalColumn(claimInfo.orderItemNo, "0")
            ).AND()
            .WHERE(
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimInfo.shipDiscountAmt, "0")
            );
        // 취소/반품배송비
        SQL bundleClaimFeeQuery = new SQL()
            .SELECT(
                claimInfo.partnerId,
                SqlUtils.appendSingleQuoteWithAlias("취소/반품 배송비", claimItem.itemName),
                SqlUtils.appendSingleQuoteWithAlias("1", claimInfo.completeQty),
                SqlUtils.aliasToRow(
                    SqlUtils.customOperator(CustomConstants.MULTIPLY, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimInfo.claimCancelShipAmt, claimInfo.claimAddShipAmt, claimInfo.claimReturnShipAmt)), "-1"),
                    claimInfo.completeAmt
                ),
                claimInfo.claimNo,
                claimInfo.itemNo,
                claimInfo.orderItemNo,
                claimInfo.storeType,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimInfo.mallType, "TD"),
                        SqlUtils.caseWhenElse(
                            claimInfo.storeType,
                            ObjectUtils.toArray(
                                "'HYPER'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'홈플러스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S")))),
                                "'CLUB'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'더클럽 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S")))),
                                "'EXP'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'익스프레스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S"))))
                            ),
                            SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'홈플러스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S"))))
                        ),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.partnerId, "P"))
                    ),
                    "partner_name"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimInfo))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmPartner),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(claimInfo.partnerId), itmPartner)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimInfo.userNo, setDto.getUserNo()),
                SqlUtils.equalColumnByInput(claimInfo.claimNo, setDto.getClaimNo()),
                SqlUtils.equalColumn(claimInfo.orderItemNo, "0")
            ).AND()
            .WHERE(
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimInfo.claimCancelShipAmt, claimInfo.claimAddShipAmt, claimInfo.claimReturnShipAmt)), "0")
            );

        SQL query = new SQL()
            .SELECT("*")
            .FROM(
                "("
                    .concat(primaryQuery.toString())
                    .concat("\nUNION ALL\n")
                    .concat(bundleFeeQuery.toString())
                    .concat("\nUNION ALL\n")
                    .concat(bundleDiscountFeeQuery.toString())
                    .concat("\nUNION ALL\n")
                    .concat(bundleClaimFeeQuery.toString())
                    .concat(") t")
            ).ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray("order_item_no")))
            .ORDER_BY("item_name");

        log.debug("selectClaimPurchaseReceiptItemInfo :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimPurchaseReceiptPaymentInfo(ClaimPurchaseReceiptSetDto setDto) {
        ClaimInfoEntity claimInfo = EntityFactory.createEntityIntoValue(ClaimInfoEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ItmPartnerEntity itmPartner = EntityFactory.createEntityIntoValue(ItmPartnerEntity.class, Boolean.TRUE);
        CashReceiptV1Entity cashReceiptV1 = EntityFactory.createEntityIntoValue(CashReceiptV1Entity.class, Boolean.TRUE);
        CashReceiptEntity cashReceipt = EntityFactory.createEntityIntoValue(CashReceiptEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimInfo.claimNo,
                SqlUtils.aliasToRow(claimPayment.payCompleteDt, "complete_dt"),
                SqlUtils.aliasToRow(
                    " CASE WHEN cp.PARENT_PAYMENT_METHOD ='CARD' THEN '신용카드'\n"
                        + "WHEN cp.METHOD_CD ='NAVERPAY' THEN '네이버페이'\n"
                        + "WHEN cp.METHOD_CD ='SAMSUNGPAY' THEN '삼성페이'\n"
                        + "WHEN cp.METHOD_CD ='TOSS' THEN 'TOSS'\n"
                        + "WHEN cp.METHOD_CD ='KAKAOPAY' AND cp.EASY_METHOD_CD ='CARD' THEN '카카오카드'\n"
                        + "WHEN cp.METHOD_CD ='KAKAOPAY' AND cp.EASY_METHOD_CD ='MONEY' THEN '카카오머니'\n"
                        + "WHEN cp.METHOD_CD ='KAKAOMONEY' AND cp.EASY_METHOD_CD ='MONEY' THEN '카카오머니'\n"
                        + "WHEN cp.PARENT_PAYMENT_METHOD ='RBANK' THEN '실시간 계좌이체'\n"
                        + "WHEN cp.PARENT_PAYMENT_METHOD ='DGV' THEN '디지털/모바일상품권'\n"
                        + "WHEN cp.PARENT_PAYMENT_METHOD ='MHC' THEN '마이홈플러스포인트'\n"
                        + "WHEN cp.PARENT_PAYMENT_METHOD ='MILEAGE' THEN '마일리지'\n"
                        + "WHEN cp.PARENT_PAYMENT_METHOD ='OCB' THEN 'OK캐시백포인트'\n"
                        + "WHEN cp.PARENT_PAYMENT_METHOD ='PHONE' THEN '휴대폰결제'\n"
                        + "ELSE '기타'"
                        + "  END",
                    "method_nm"
                ),
                claimInfo.partnerId,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimInfo.mallType, "TD"),
                        SqlUtils.caseWhenElse(
                            claimInfo.storeType,
                            ObjectUtils.toArray(
                                "'HYPER'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'홈플러스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S")))),
                                "'CLUB'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'더클럽 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S")))),
                                "'EXP'",
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'익스프레스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S"))))
                            ),
                            SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'홈플러스 '", SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.originStoreId, "S"))))
                        ),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimInfo.partnerId, "P"))
                    ),
                    "partner_name"
                ),
                SqlUtils.aliasToRow(
                    "CASE WHEN cp.PARENT_PAYMENT_METHOD ='DGV' THEN if( ci.mall_type = 'TD', (SUM(ci.dgv_amt) - MAX(cp.substitution_amt) ), SUM(ci.dgv_amt) )\n"
                        + " WHEN cp.PARENT_PAYMENT_METHOD ='MHC' THEN if( ci.mall_type = 'TD', (SUM(ci.mhc_amt) - MAX(cp.substitution_amt) ), SUM(ci.mhc_amt) )\n"
                        + " WHEN cp.PARENT_PAYMENT_METHOD ='MILEAGE' THEN if( ci.mall_type = 'TD', (SUM(ci.mileage_amt) - MAX(cp.substitution_amt) ), SUM(ci.mileage_amt) )\n"
                        + " WHEN cp.PARENT_PAYMENT_METHOD ='OCB' THEN if( ci.mall_type = 'TD', (SUM(ci.ocb_amt) - MAX(cp.substitution_amt) ), SUM(ci.ocb_amt) )\n"
                        + " ELSE if( ci.mall_type = 'TD', (SUM(ci.pg_amt) - MAX(cp.substitution_amt) ), SUM(ci.pg_amt) )\n"
                        + " END",
                    "claim_amt"
                ),
//                SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(claimPayment.paymentAmt, claimPayment.substitutionAmt), "claim_amt"),
                SqlUtils.aliasToRow(
                    "CASE \n"
                        + "WHEN cp.PARENT_PAYMENT_METHOD ='CARD' THEN 'CARD'## -- 카드전표 \n"
                        + " WHEN cp.EASY_METHOD_CD ='CARD' THEN 'CARD'## -- 카드전표 \n"
                        + " WHEN cp.EASY_METHOD_CD ='MONEY' and cr.cash_receipt_usage !='NONE' THEN 'CASH'## -- 현금영수증 \n"
                        + " WHEN cp.PARENT_PAYMENT_METHOD ='RBANK' and cr.cash_receipt_usage !='NONE' THEN 'CASH'## -- 현금영수증 \n"
                        + " WHEN cp.PARENT_PAYMENT_METHOD ='DGV' and cr.cash_receipt_usage !='NONE' THEN 'CASH'## -- 현금영수증 \n"
                        + " ELSE 'NO_TARGET'"
                        + " END",
                    "receipt_type"
                ),
                claimInfo.storeType,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(cashReceiptV1.cashReceiptType, "'NONE'"), "cash_receipt_usage"),
                claimInfo.bundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimInfo))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimInfo.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimPayment.paymentStatus, "P3")))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmPartner),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(claimInfo.partnerId), itmPartner)
                    )
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    cashReceiptV1,
                    ObjectUtils.toArray(claimInfo.claimNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(cashReceiptV1.gubun, "2"),
                        SqlUtils.equalColumnByInput(cashReceiptV1.cashReceiptYn, "Y")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(cashReceipt, ObjectUtils.toArray(claimPayment.paymentNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimInfo.claimNo, setDto.getClaimNo()),
                SqlUtils.equalColumnByInput(claimInfo.userNo, setDto.getUserNo()),
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimPayment.paymentAmt, "0")
            )
            .GROUP_BY(
                claimInfo.claimNo, claimPayment.payCompleteDt, claimPayment.parentPaymentMethod,
                claimPayment.methodCd, claimPayment.easyMethodCd, claimInfo.partnerId,
                claimInfo.originStoreId, claimInfo.storeType, claimInfo.mallType,
                cashReceiptV1.cashReceiptType, cashReceipt.cashReceiptUsage,
                claimInfo.bundleNo
            )
            .ORDER_BY(claimInfo.partnerId)
            .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimInfo.mallType)));
        log.debug("selectClaimPurchaseReceiptPaymentInfo :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimCardPurchaseReceiptInfo(ClaimPurchaseMethodReceiptSetDto setDto) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        PaymentInfoEntity paymentInfo = EntityFactory.createEntityIntoValue(PaymentInfoEntity.class, Boolean.TRUE);
        PaymentMethodEntity paymentMethod = EntityFactory.createEntityIntoValue(PaymentMethodEntity.class, Boolean.TRUE);
        ItmPartnerEntity itmPartner = EntityFactory.createEntityIntoValue(ItmPartnerEntity.class, Boolean.TRUE);
        ItmPartnerSellerEntity itmPartnerSeller = EntityFactory.createEntityIntoValue(ItmPartnerSellerEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, "claimItem");
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimInfoEntity claimInfo = EntityFactory.createEntityIntoValue(ClaimInfoEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.aliasToRow(claimPayment.originApprovalCd, "claim_approval_no"),
                claimMst.claimNo,
                claimMst.purchaseOrderNo,
                SqlUtils.aliasToRow(paymentMethod.methodNm, "card_nm"),
                paymentInfo.cardNo,
                SqlUtils.appendSingleQuoteWithAlias("매출취소", "claim_type"),
                SqlUtils.aliasToRow(
                    "CASE \n"
                        + "WHEN pi.CARD_INSTALLMENT_MONTH = 1 THEN '일시불'\n"
                        + "WHEN pi.CARD_INTEREST_YN = 'N' THEN concat( pi.CARD_INSTALLMENT_MONTH ,'개월 (무이자)')\n"
                        + "ELSE concat( pi.CARD_INSTALLMENT_MONTH ,'개월')\n"
                        + " END",
                    "card_installment_month"
                ),
                SqlUtils.aliasToRow(claimPayment.payCompleteDt, "complete_dt"),
                paymentInfo.paymentFshDt,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, "t.item_cnt","1"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                "t.item_name",
                                "'외 '",
                                SqlUtils.customOperator(CustomConstants.MINUS, "t.item_cnt", "1"),
                                "'건'"
                            )
                        ),
                        "t.item_name"
                    ),
                    "item_nm"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        "claim.tot_amt",
                        SqlUtils.sqlFunction(
                            MysqlFunction.ROUND,
                            SqlUtils.customOperator(
                                CustomConstants.MULTIPLY,
                                "claim.tot_amt",
                                "0.1"
                            )
                        )
                    ),
                    "pay_amt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.ROUND,
                    SqlUtils.customOperator(
                        CustomConstants.MULTIPLY,
                        "claim.tot_amt",
                        "0.1"
                    ),
                    "tot_vat_amt"
                ),
                SqlUtils.aliasToRow("0", "service_amt"),
                SqlUtils.aliasToRow("claim.tot_amt", "tot_pay_amt"),
                itmPartner.partnerId,
                itmPartner.partnerNm,
                itmPartner.partnerOwner,
                itmPartner.partnerNo,
                SqlUtils.aliasToRow(itmPartnerSeller.phone1, "partner_info_center"),
                "claim.bundle_no"
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    SqlUtils.subTableQuery(
                        new SQL().SELECT(
                            SqlUtils.sqlFunction(MysqlFunction.MAX, claimItem.itemName, claimItem.itemName),
                            SqlUtils.sqlFunction(MysqlFunction.MAX, claimItem.claimNo, claimItem.claimNo),
                            SqlUtils.sqlFunction(MysqlFunction.COUNT, SqlUtils.sqlFunction(MysqlFunction.DISTINCT, claimItem.itemNo), "item_cnt")
                        ).FROM(EntityFactory.getTableNameWithAlias(claimItem, "claimItem"))
                            .WHERE(
                                SqlUtils.equalColumnByInput(claimItem.claimNo, setDto.getClaimNo()),
                                SqlUtils.equalColumnByInput(claimItem.bundleNo, setDto.getBundleNo())
                            ),
                        "t"
                    ),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn("t.claim_no", claimMst.claimNo)
                    )
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    claimPayment,
                    ObjectUtils.toArray(claimMst.paymentNo, claimMst.claimNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimPayment.paymentStatus, "P3"),
                        SqlUtils.orQuery(
                            ObjectUtils.toArray(
                                SqlUtils.equalColumnByInput(claimPayment.parentPaymentMethod, "CARD"),
                                SqlUtils.equalColumnByInput(claimPayment.easyMethodCd, "CARD")
                            )
                        )
                    )
                )
            )
            .INNER_JOIN( SqlUtils.createJoinCondition(paymentInfo, ObjectUtils.toArray(claimPayment.paymentNo)) )
            .INNER_JOIN( SqlUtils.createJoinCondition(paymentMethod, ObjectUtils.toArray(claimMst.siteType, paymentInfo.methodCd)) )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmPartner),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(itmPartner.partnerId, setDto.getPartnerId())
                    )
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmPartnerSeller),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(itmPartner.partnerId), itmPartnerSeller)
                    )
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    SqlUtils.subTableQuery(
                        new SQL().SELECT(
                            SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimInfo.pgAmt, "tot_amt"),
                            SqlUtils.sqlFunction(MysqlFunction.MAX, claimInfo.bundleNo, claimInfo.bundleNo),
                            SqlUtils.sqlFunction(MysqlFunction.MAX, claimInfo.claimNo, claimInfo.claimNo)
                        ).FROM(EntityFactory.getTableNameWithAlias(claimInfo))
                            .WHERE(
                                SqlUtils.equalColumnByInput(claimInfo.claimNo, setDto.getClaimNo()),
                                SqlUtils.equalColumnByInput(claimInfo.bundleNo, setDto.getBundleNo()),
                                SqlUtils.equalColumnByInput(claimInfo.userNo, setDto.getUserNo())
                            ),
                        "claim"
                    ),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn("claim.claim_no", claimMst.claimNo)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, setDto.getClaimNo()),
                SqlUtils.equalColumnByInput(claimMst.userNo, setDto.getUserNo()),
                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, claimPayment.claimApprovalCd)
            );
        log.debug("selectClaimCardPurchaseReceiptInfo :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimCashPurchaseReceiptInfo(ClaimPurchaseMethodReceiptSetDto setDto) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, "claimItem");
        CashReceiptV1Entity cashReceiptV1 = EntityFactory.createEntityIntoValue(CashReceiptV1Entity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        CashReceiptEntity cashReceipt = EntityFactory.createEntityIntoValue(CashReceiptEntity.class, Boolean.TRUE);
        CashReceiptCompleteEntity cashReceiptComplete = EntityFactory.createEntityIntoValue(CashReceiptCompleteEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.aliasToRow(cashReceiptComplete.cashReceiptApprovalNo, "claim_approval_no"),
                cashReceiptV1.claimNo,
                cashReceiptV1.purchaseOrderNo,
                SqlUtils.aliasToRow(cashReceiptV1.cashReceiptUsage, "cash_receipt_usage"),
                SqlUtils.caseWhenElse(
                    cashReceipt.cashReceiptType,
                    ObjectUtils.toArray(
                        "'PHONE'", "'휴대폰번호'",
                        "'CARD'", "'현금영수증카드번호'",
                        "'BUSINESSNO'", "'사업자번호'"
                    ),
                    "'미발급'",
                    "cash_receipt_type"
                ),
                SqlUtils.caseWhenElse(
                    SqlUtils.sqlFunction(MysqlFunction.LENGTH, cashReceiptV1.cashReceiptReqNo),
                    ObjectUtils.toArray(
                        "11", SqlUtils.sqlFunction(MysqlFunction.REPLACE, ObjectUtils.toArray(cashReceiptV1.cashReceiptReqNo, SqlUtils.sqlFunction(MysqlFunction.SUBSTR, ObjectUtils.toArray(cashReceiptV1.cashReceiptReqNo, 4, 4)), "'****'")),
                        "16", SqlUtils.sqlFunction(MysqlFunction.REPLACE, ObjectUtils.toArray(cashReceiptV1.cashReceiptReqNo, SqlUtils.sqlFunction(MysqlFunction.SUBSTR, ObjectUtils.toArray(cashReceiptV1.cashReceiptReqNo, 7, 6)), "'******'"))
                    ),
                    cashReceiptV1.cashReceiptReqNo,
                    "cash_receipt_req_no"
                ),
                SqlUtils.aliasToRow(purchaseOrder.chgDt, "payment_fsh_dt"),
                SqlUtils.aliasToRow(claimMst.regDt, "complete_dt"),
                SqlUtils.aliasToRow(
                    "CASE \n"
                        + " WHEN t.itemCnt > 1 THEN concat( t.item_name, '외 ', (t.itemCnt -1 ) , '건')\n"
                        + " ELSE t.item_name \n"
                        + " END",
                    "item_nm"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, cashReceiptV1.supplyAmt, "pay_amt"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, cashReceiptV1.vatAmt, "tot_vat_amt"),
                SqlUtils.aliasToRow("0", "service_amt"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, cashReceiptV1.taxSumAmt, "tot_pay_amt"),
                cashReceiptV1.partnerId,
                cashReceiptV1.partnerNm,
                cashReceiptV1.partnerOwner,
                cashReceiptV1.partnerNo,
                SqlUtils.aliasToRow(cashReceiptV1.phone1, "partner_info_center"),
                SqlUtils.aliasToRow(cashReceiptV1.chgDt, "cash_receipt_issue_dt"),
                cashReceiptV1.bundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(cashReceiptV1))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(cashReceiptV1.purchaseOrderNo))
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(cashReceipt, ObjectUtils.toArray(purchaseOrder.paymentNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(cashReceiptV1.claimNo)))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(cashReceiptComplete),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(cashReceiptV1.seq, cashReceiptComplete.cashReceiptSeq),
                        SqlUtils.equalColumnByInput(cashReceiptComplete.cashReceiptIssueResultCd, "0000")
                    )
                )
            )
            .INNER_JOIN(
                SqlUtils.subTableQuery(
                    new SQL().SELECT(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimItem.itemName, claimItem.itemName),
                        SqlUtils.sqlFunction(MysqlFunction.COUNT, claimItem.claimNo, "itemCnt")
                    )
                        .FROM(EntityFactory.getTableNameWithAlias(claimItem, "claimItem"))
                        .WHERE(
                            SqlUtils.equalColumnByInput(claimItem.claimNo, setDto.getClaimNo()),
                            SqlUtils.equalColumnByInput(claimItem.bundleNo, setDto.getBundleNo())
                        )
                        .GROUP_BY(claimItem.claimNo),
                    "t"
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(cashReceiptV1.claimNo, setDto.getClaimNo()),
                SqlUtils.equalColumnByInput(claimMst.userNo, setDto.getUserNo()),
                SqlUtils.equalColumnByInput(cashReceiptV1.bundleNo, setDto.getBundleNo()),
                SqlUtils.equalColumnByInput(cashReceiptV1.gubun, "2"),
                SqlUtils.equalColumnByInput(cashReceiptV1.cashReceiptYn, "Y")
            );
        log.debug("selectClaimCashPurchaseReceiptInfo :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderRefundReceiptInfo(ClaimRefundReceiptSetDto setDto) {
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimPaymentEntity calcClaimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ItmMngCodeEntity itmMngCode = EntityFactory.createEntityIntoValue(ItmMngCodeEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimPayment.claimNo,
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(claimPayment.payCompleteDt, CustomConstants.YYYYMMDD_COMMA), claimPayment.payCompleteDt),
                SqlUtils.aliasToRow(itmMngCode.mcNm, claimPayment.paymentType),
                SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(claimPayment.paymentAmt, claimPayment.substitutionAmt), claimPayment.paymentAmt),
                SqlUtils.subTableQuery(
                    new SQL()
                        .SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.MINUS, calcClaimPayment.paymentAmt, calcClaimPayment.substitutionAmt)))
                        .FROM(EntityFactory.getTableName(calcClaimPayment))
                        .WHERE(
                            SqlUtils.equalColumn(calcClaimPayment.claimNo, claimPayment.claimNo),
                            SqlUtils.equalColumnByInput(calcClaimPayment.paymentStatus, "P3")
                        ),
                    "total_payment_amt"
                ),
                claimPayment.purchaseOrderNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"))))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimBundle.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimPayment.paymentStatus, "P3"))))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmMngCode),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(itmMngCode.gmcCd, SqlUtils.nonAlias(claimPayment.paymentType)),
                        SqlUtils.equalColumn(itmMngCode.mcCd, claimPayment.parentPaymentMethod)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.purchaseOrderNo, setDto.getPurchaseOrderNo()),
                SqlUtils.equalColumnByInput(claimMst.userNo, setDto.getUserNo())
            )
            .ORDER_BY(claimPayment.claimNo.concat(" DESC"), claimPayment.claimPaymentNo);
        log.debug("selectOrderRefundReceiptInfo :: \n[{}]", query.toString());
        return query.toString();
    }

}
