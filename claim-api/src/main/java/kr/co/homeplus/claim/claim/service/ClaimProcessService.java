package kr.co.homeplus.claim.claim.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import java.util.Locale;
import java.util.stream.Collectors;
import kr.co.homeplus.claim.claim.model.ClaimRequestSetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimCouponReIssueGetDto;
import kr.co.homeplus.claim.claim.model.external.ClaimCouponReIssueSetDto.CouponReIssueDto;
import kr.co.homeplus.claim.claim.model.external.ClaimShipCompleteDto;
import kr.co.homeplus.claim.claim.model.external.ClaimShipStartDto;
import kr.co.homeplus.claim.claim.model.external.EmpInfoDto;
import kr.co.homeplus.claim.claim.model.external.RestoreSlotStockDto;
import kr.co.homeplus.claim.claim.model.external.message.MessageSendTalkGetDto;
import kr.co.homeplus.claim.claim.model.external.mileage.MileageSaveCancelDto;
import kr.co.homeplus.claim.claim.model.external.ticket.ClaimCoopTicketCheckGetDto;
import kr.co.homeplus.claim.claim.model.external.ticket.ClaimCoopTicketCheckSetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claim.claim.model.register.ClaimBundleRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimInfoRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimItemRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimMultiBundleRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimMultiItemRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimProcessHistoryRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.claim.claim.model.register.ClaimShippingRegDto;
import kr.co.homeplus.claim.claim.model.register.ticket.ClaimTicketModifyDto;
import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.entity.claim.ClaimOptEntity;
import kr.co.homeplus.claim.enums.ClaimCode;
import kr.co.homeplus.claim.enums.ClaimResponseCode;
import kr.co.homeplus.claim.enums.ExternalInfo;
import kr.co.homeplus.claim.factory.EntityFactory;
import kr.co.homeplus.claim.utils.DateUtils;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.SetParameter;
import kr.co.homeplus.claim.utils.StringUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClaimProcessService {

    private final ClaimMapperService mapperService;

    private final ExternalService externalService;

    private final ClaimCommonService commonService;

    private final ResourceClient resourceClient;

    @Transactional(rollbackFor = {Exception.class, LogicException.class}, propagation = Propagation.NESTED, isolation = Isolation.READ_COMMITTED)
    public LinkedHashMap<String, Object> addClaimRegister(ClaimRegSetDto regSetDto) throws Exception {
        // Claim_mst 등록
        ClaimMstRegDto claimMstRegDto = insertClaimMstProcess(regSetDto);
        // 클레임 등록을 위한 클레임 데이터 가지고 오기
        LinkedHashMap<String, Object> createClaimData = commonService.createClaimDataMap(regSetDto);

        // claim_mst 에 등록된 claim_no 를 map 넣음.
        createClaimData.put("claim_no", claimMstRegDto.getClaimNo());

        createClaimData = insertClaimPartBasicFromBundleProcess(createClaimData, regSetDto);

        // claim addition reg
        mapperService.callByClaimAdditionReg(claimMstRegDto.getClaimNo());

        insertClaimPaymentProcess(createClaimData, claimMstRegDto);

        //안신번호 발행.(반품/수거만)
        if(!regSetDto.getClaimType().equals("C") && ObjectUtils.toString(createClaimData.get("safety_use_yn")).equals("Y")){
            // 수거 등록
            createClaimSafetyIssue(ObjectUtils.toLong(createClaimData.get("claim_bundle_no")), "R");
            if(regSetDto.getClaimType().equals("X")){
                createClaimSafetyIssue(ObjectUtils.toLong(createClaimData.get("claim_bundle_no")), "X");
            }
        }
        if(!regSetDto.getClaimType().equals("C")){
            // 기조 배송상태 완료로 변경.
            if("D3".contains(ObjectUtils.toString(createClaimData.get("ship_status")))){
                this.setClaimShipComplete(regSetDto.getBundleNo(), regSetDto.getRegId());
            }

        }

        return createClaimData;
    }

    @Transactional(rollbackFor = {Exception.class, LogicException.class}, propagation = Propagation.NESTED, isolation = Isolation.READ_COMMITTED)
    public LinkedHashMap<String, Object> addTotalClaimRegister(ClaimRegSetDto regSetDto) throws Exception {
        // Claim_mst 등록
        ClaimMstRegDto claimMstRegDto = insertClaimMstProcess(regSetDto);
        // 클레임 번들리스트
        List<Long> claimBundleNoList = new ArrayList<>();
        // 클레임 정보 조회
        LinkedHashMap<String, Object> createClaimData = new LinkedHashMap<>();
        for (LinkedHashMap<String, Object> bundleData : getOrderBundleByClaim(regSetDto.getPurchaseOrderNo())) {
            regSetDto.setClaimItemList(getClaimPreRefundItemInfo( ObjectUtils.toLong(bundleData.get("purchase_order_no")), ObjectUtils.toLong(bundleData.get("bundle_no"))));
            regSetDto.setBundleNo(ObjectUtils.toString(bundleData.get("bundle_no")));
            // 클레임 등록을 위한 클레임 데이터 가지고 오기
            createClaimData = commonService.createClaimDataMap(regSetDto);
            // claim_mst 에 등록된 claim_no 를 map 넣음.
            createClaimData.put("claim_no", claimMstRegDto.getClaimNo());

            createClaimData = insertClaimPartBasicFromBundleProcess(createClaimData, regSetDto);
            // claim addition reg
            mapperService.callByClaimAdditionReg(claimMstRegDto.getClaimNo());
            // 클레임번들번
            claimBundleNoList.add(ObjectUtils.toLong(createClaimData.get("claim_bundle_no")));
        }
        // 결제취소데이터
        insertClaimPaymentProcess(createClaimData, claimMstRegDto);

        LinkedHashMap<String, Object> returnMap = new LinkedHashMap<>();
        returnMap.put("claim_no", claimMstRegDto.getClaimNo());
        returnMap.put("payment_no", createClaimData.get("payment_no"));
        returnMap.put("claim_bundle_no", claimBundleNoList);

        return returnMap;
    }

    public ClaimMstRegDto insertClaimMstProcess(ClaimRegSetDto regSetDto) throws Exception {
        // 클레임 마스터 등록을 위한 DTO 설정.
        // 환불 예정 금액 조회를 다시 한다. (정합성 체크)
        ClaimMstRegDto claimMstRegDto = commonService.getFusionForClaimMst(regSetDto);

        if(claimMstRegDto.getCompleteAmt() < 0){
            throw new LogicException(ClaimResponseCode.CLAIM_CANCEL_ERR02);
        }

        if(!StringUtil.isEmpty(claimMstRegDto.getEmpNo())){
            claimMstRegDto.setEmpRemainAmt(getEmpDiscountLimitAmt(claimMstRegDto.getEmpNo()));
        }
        // 클레임
        if(!mapperService.addClaimMstInfo(claimMstRegDto)){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
        }
        return claimMstRegDto;
    }

    public LinkedHashMap<String, Object> insertClaimPartBasicFromBundleProcess(LinkedHashMap<String, Object> createClaimData, ClaimRegSetDto regSetDto) throws Exception {
        if("R,X".contains(regSetDto.getClaimType())){
            createClaimData.put("isEnclose", regSetDto.getClaimDetail().getIsEnclose());
        }
        // 클레임 번들
        ClaimBundleRegDto claimBundleRegDto = commonService.createClaimBundleRegDto(createClaimData);
        // 클레임 번들 등록
        if(!mapperService.addClaimBundleInfo(claimBundleRegDto)){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
        }

        // claim_bundle_no 를 map 에 넣음.
        createClaimData.put("claim_bundle_no", claimBundleRegDto.getClaimBundleNo());

        if("R,X".contains(regSetDto.getClaimType())){
            if(!StringUtils.isEmpty(regSetDto.getClaimDetail().getUploadFileName())){
                createClaimData.put("upload_file_name", regSetDto.getClaimDetail().getUploadFileName());
            }
            if(!StringUtils.isEmpty(regSetDto.getClaimDetail().getUploadFileName2())){
                createClaimData.put("upload_file_name2", regSetDto.getClaimDetail().getUploadFileName2());
            }
            if(!StringUtils.isEmpty(regSetDto.getClaimDetail().getUploadFileName3())){
                createClaimData.put("upload_file_name3", regSetDto.getClaimDetail().getUploadFileName3());
            }
        }
        // 환불예정금액 재조회를 위한 파라미터 저장 추가.
        // 할인미차감여부
        createClaimData.put("deduct_discount_yn", "N");
        // 행사미차감여부
        createClaimData.put("deduct_promo_yn", "N");
        // 배송비미차감여부
        createClaimData.put("deduct_ship_yn", "N");

        // 클레임 요청
        if(!mapperService.addClaimReqInfo(commonService.createClaimReqRegDto(createClaimData))){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
        }
        // 클레임 아이템 및 옵션별 저장.
        this.claimItemPartRegProcess(createClaimData, regSetDto);

        // 다중배송일 경우 claim_multi_bundle 생성.
        if(regSetDto.getIsMultiOrder() && regSetDto.getClaimPartYn().equals("N")) {
            ClaimMultiBundleRegDto claimMultiBundleRegDto = new ClaimMultiBundleRegDto();
            claimMultiBundleRegDto.setClaimNo(claimBundleRegDto.getClaimNo());
            claimMultiBundleRegDto.setClaimBundleNo(claimBundleRegDto.getClaimBundleNo());
            claimMultiBundleRegDto.setPurchaseOrderNo(claimBundleRegDto.getPurchaseOrderNo());

            // 번들정보를 조회하여 처리.
            for(Long multiBundleNo : mapperService.getMultiBundleInfo(claimBundleRegDto.getPurchaseOrderNo(), claimBundleRegDto.getBundleNo())) {
                claimMultiBundleRegDto.setMultiBundleNo(multiBundleNo);
                if(!mapperService.addClaimMultiBundleInfo(claimMultiBundleRegDto)){
                    throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
                }
                //클레임아이템 조회
                for(ClaimMultiItemRegDto multiOrderItemInfoDto : mapperService.getClaimMultiItemCreateInfo(claimMultiBundleRegDto.getClaimNo(), claimMultiBundleRegDto.getMultiBundleNo())) {
                    multiOrderItemInfoDto.setClaimMultiBundleNo(claimMultiBundleRegDto.getClaimMultiBundleNo());
                    if(!mapperService.addClaimMultiItemInfo(multiOrderItemInfoDto)){
                        throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
                    }
                }
            }
        }

        if(!regSetDto.getClaimType().equalsIgnoreCase("C")){
            ClaimShippingRegDto shippingRegDto = commonService.createClaimPickShippingDto(regSetDto.getClaimDetail(),  createClaimData);
            if(regSetDto.getIsOmni()){
                shippingRegDto.setPickStatus("P3");
            }
            // pick insert
            if(!mapperService.addClaimPickShippingInfo(shippingRegDto)){
                throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR11);
            }

            if(regSetDto.getClaimType().equalsIgnoreCase("X")){
                if(!mapperService.addClaimExchShipping(commonService.createClaimExchShippingRegDto(shippingRegDto, regSetDto.getClaimDetail()))){
                    throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR12);
                }
            }
            createClaimData.put("pickStatus", shippingRegDto.getPickStatus());
        }

        return createClaimData;
    }

    public void insertClaimPaymentProcess(LinkedHashMap<String, Object> createClaimData, ClaimMstRegDto claimMstRegDto) throws Exception {
        LinkedHashMap<String, Long> claimPaymentType = commonService.getClaimPaymentTypeList(ObjectUtils.getConvertMap(claimMstRegDto));

        if(claimPaymentType == null){
            claimPaymentType = new LinkedHashMap<>(){{
                put("FREE", 0L);
            }};
        }

        for(String key : claimPaymentType.keySet()){
            if(!mapperService.addClaimPaymentInfo(commonService.createClaimPaymentRegDto(createClaimData, key, claimPaymentType.get(key)))){
                throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
            }
        }
    }

    // 취소 처리.
    // 성공 실패시 업데이트가 되어야 함.취소 회수 중요함.
    public Boolean paymentCancelProcess(long claimNo) throws Exception {
        // 클레임 결제 정보 리스트
        List<LinkedHashMap<String, Object>> claimPaymentInfo = mapperService.getPaymentCancelInfo(claimNo);

        if(claimPaymentInfo.size() == 0){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR04);
        }
        // return Type
        boolean externalProcessStep = Boolean.TRUE;
        // 취소데이터 조회
        // 조건 별 취소 시작.
        // MAP? LIST? PG 타입 선택 필요.
        for(LinkedHashMap<String, Object> paymentMap : claimPaymentInfo){
            // 취소로직 SKIP여부
            Boolean isSkip = Boolean.FALSE;
            // 클레임 취소 승인코드
            String claimApprovalCd = null;
            // 취소금액 0보다 적은 경우 오류
            // 취소금액이 0인 경우 취소로직 SKIP
            if(ObjectUtils.toLong(paymentMap.get("payment_amt")) < 0){
                log.error("클레임 금액이 0보다 작으므로 오류처리 ::: claim_no :: {}", claimNo);
                throw new LogicException(ClaimResponseCode.CLAIM_PAYMENT_CANCEL_ZERO_ERR01);
            } else if(ObjectUtils.toLong(paymentMap.get("payment_amt")) == 0){
                claimApprovalCd = "0000";
                isSkip = Boolean.TRUE;
            }

            if(!isSkip) {
                paymentMap.put("reg_id", "MYPAGE");
                paymentMap.put("cancel_message", "클레임 발생으로 결제 취소");
                // 클레임 결제 상태 변경(P1 -> P2)
                setClaimPaymentStatus(paymentMap.get("claim_payment_no"), claimApprovalCd, ClaimCode.CLAIM_PAYMENT_STATUS_P2);
                LinkedHashMap<String, Object> returnMap = new LinkedHashMap<>();
                try {
                    log.info("클레임 결제 취소 요청 ::: [{}]", paymentMap);
                    switch (ObjectUtils.toString(paymentMap.get("payment_type"))) {
                        case "PP" :
                            returnMap = externalService.externalProcess(commonService.createPaymentCancelForMileageDto(paymentMap));
                            returnMap.put("claimApprovalNo", returnMap.get("approvalNo"));
                            returnMap.put("returnCode", "0000");
                            break;
                        case "PG" :
                            returnMap = externalService.externalProcess(commonService.createPaymentPgCancelSetDto(paymentMap));
                            returnMap.put("claimApprovalNo", returnMap.get("resultCode"));
                            returnMap.put("returnCode", returnMap.get("resultCode"));
                            setClaimPaymentCancelTradeId(paymentMap.get("claim_payment_no"), returnMap.get("pgPartTid") == null ? returnMap.get("pgTid") : returnMap.get("pgPartTid"));
                            break;
                        case "MP" :
                            returnMap = externalService.externalProcess(commonService.createPaymentCancelForMhc(paymentMap));
                            returnMap.put("claimApprovalNo", returnMap.get("mhcAprNumber"));
                            break;
                        case "OC" :
                            returnMap = externalService.externalProcess(commonService.createPaymentCancelForOcb(paymentMap));
                            returnMap.put("claimApprovalNo", returnMap.get("ocbAprNumber"));
                            break;
                        case "DG" :
                            returnMap = externalService.externalProcess(commonService.createPaymentCancelForDgv(paymentMap));
                            returnMap.put("claimApprovalNo", returnMap.get("aprNumber"));
                            break;
                        default:
                            throw new LogicException("9999", "결제취소할 수 없는 프로세스입니다.");
                    }
                    claimApprovalCd = ObjectUtils.toString(returnMap.get("claimApprovalNo"));
                    String returnCode = ObjectUtils.toString(returnMap.get("returnCode"));
                    if(!"0000,SUCCESS".contains(returnCode)){
                        claimApprovalCd = returnCode;
                        throw new LogicException(returnCode, ObjectUtils.toString(returnMap.get("returnMessage")));
                    }
                    log.info("클레임 결제 취소 결과 ::: [{}]", claimApprovalCd);
                } catch (LogicException cle) {
                    // 오류시 오류 카운트!
                    log.error("클레임 결제 취소 프로세스 오류 ::: [{} / {}]", cle.getResponseCode(), cle.getResponseMsg());
                    // 결제 취소 실패 업데이트 (P2 -> P4)
                    claimApprovalCd = cle.getResponseCode();
                    externalProcessStep = isProcessStep(ObjectUtils.toString(paymentMap.get("payment_type")), cle.getResponseCode());
                } catch (Exception e){
                    log.error("클레임 결제 취소 Exception ::: [{}]", e.getMessage());
                    externalProcessStep = Boolean.FALSE;
                }
            }

            if(!externalProcessStep){
                setClaimPaymentStatus(paymentMap.get("claim_payment_no"), claimApprovalCd, ClaimCode.CLAIM_PAYMENT_STATUS_P4);
                return Boolean.FALSE;
            }
            // 결제 취소 성공 업데이트(P2 -> P3)
            setClaimPaymentStatus(paymentMap.get("claim_payment_no"), claimApprovalCd, ClaimCode.CLAIM_PAYMENT_STATUS_P3);
        }

        return Boolean.TRUE;
    }

    // 마켓 취소 처리.
    // 성공 실패시 업데이트가 되어야 함.취소 회수 중요함.
    public Boolean marketPaymentCancelProcess(long claimNo) throws Exception {
        // 클레임 결제 정보 리스트
        List<LinkedHashMap<String, Object>> claimPaymentInfo = mapperService.getPaymentCancelInfo(claimNo);

        if(claimPaymentInfo.size() == 0){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR04);
        }
        // return Type
        boolean externalProcessStep = Boolean.TRUE;
        // 취소데이터 조회
        // 조건 별 취소 시작.
        // MAP? LIST? PG 타입 선택 필요.
        for(LinkedHashMap<String, Object> paymentMap : claimPaymentInfo){
            // 취소로직 SKIP여부
            Boolean isSkip = Boolean.FALSE;
            // 클레임 취소 승인코드
            String claimApprovalCd = null;
            // 취소금액 0보다 적은 경우 오류
            // 취소금액이 0인 경우 취소로직 SKIP
            if(ObjectUtils.toLong(paymentMap.get("payment_amt")) < 0){
                log.error("클레임 금액이 0보다 작으므로 오류처리 ::: claim_no :: {}", claimNo);
                throw new LogicException(ClaimResponseCode.CLAIM_PAYMENT_CANCEL_ZERO_ERR01);
            } else if(ObjectUtils.toLong(paymentMap.get("payment_amt")) == 0){
                claimApprovalCd = "0000";
                isSkip = Boolean.TRUE;
            } else if(ObjectUtils.toLong(paymentMap.get("payment_amt")) > 0) {
                log.error("클레임 금액이 0보다 크므로 오류처리 ::: claim_no :: {}", claimNo);
                throw new LogicException(ClaimResponseCode.CLAIM_PAYMENT_CANCEL_MARKET_ERR01);
            }
            // 결제 취소 성공 업데이트(P2 -> P3)
            setClaimPaymentStatus(paymentMap.get("claim_payment_no"), claimApprovalCd, ClaimCode.CLAIM_PAYMENT_STATUS_P3);
        }

        return Boolean.TRUE;
    }

    /**
     * 클레임 아이템 등록 Process
     * 클레임 아이템 등록 후 클레임 옵션 등록
     *
     * @param claimData 클레임 생성 데이터 맵
     * @param claimRegSetDto 클레임 등록 데이터
     * @throws Exception 오류시 오류처리
     */
    public void claimItemPartRegProcess(LinkedHashMap<String, Object> claimData, ClaimRegSetDto claimRegSetDto) throws Exception {
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class);
        // 이전상품주문번호
        long prevOrderItemNo = 0L;
        // 클레임 상품번호
        long claimItemNo = 0L;
        // 아이템 번호별 loop
        for(ClaimPreRefundItemList itemList : claimRegSetDto.getClaimItemList()){
            LinkedHashMap<String, Object> claimItemInfo = mapperService.getClaimItemInfo(Long.parseLong(claimRegSetDto.getPurchaseOrderNo()), Long.parseLong(itemList.getOrderItemNo()));
            // 클레임 데이터 넣기
            claimItemInfo.putAll(claimData);
            // 클레임 등록 DTO 생성
            ClaimItemRegDto claimItemRegDto = commonService.createClaimItemRegDto(claimItemInfo);
            // 이전 주문번호와 현재 처리 주문번호가 같으면 insert skip
            if(prevOrderItemNo != claimItemRegDto.getOrderItemNo()){
                log.info("주문번호[{}] - 상품주문번호[{}]에 대한 Claim_item 생성 데이터 조회 :: {}", claimRegSetDto.getPurchaseOrderNo(), itemList.getOrderItemNo(), claimItemInfo);
                // 클레임 신청 수량
                int claimItemQty = claimRegSetDto.getClaimItemList().stream().filter(itemList1 -> itemList1.getOrderItemNo().equals(String.valueOf(claimItemRegDto.getOrderItemNo()))).map(ClaimPreRefundItemList::getClaimQty).mapToInt(Integer::parseInt).sum();
                // validation
                if(ObjectUtils.isEquals(claimItemInfo.get("item_qty"), claimItemInfo.get("claim_item_qty")) || claimItemQty <= 0){
                    throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR15);
                }
                // 클래임 건수 입력
                claimItemRegDto.setClaimItemQty(claimItemQty);
                // 클레임 아이템생성.
                if(!mapperService.addClaimItemInfo(claimItemRegDto)){
                    throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
                }
                claimItemNo = claimItemRegDto.getClaimItemNo();
                prevOrderItemNo = claimItemRegDto.getOrderItemNo();
                // 티켓 취소시 여기서 처리.
                if(ObjectUtils.toString(claimItemInfo.get("order_type")).equals("ORD_TICKET")){
                    List<LinkedHashMap<String, Object>> orderTicketInfo = mapperService.getOrderTicketInfo(claimItemRegDto.getPurchaseOrderNo(), claimItemRegDto.getOrderItemNo(), claimItemQty);
                    if(orderTicketInfo.stream().map(map -> ObjectUtils.toString(map.get("issue_channel"))).map(String::valueOf).anyMatch(str -> str.equals("COOP"))){
                        List<LinkedHashMap<String, Object>> coopTicketInfoList = orderTicketInfo.stream().filter(map -> ObjectUtils.toString(map.get("issue_channel")).equals("COOP")).collect(Collectors.toList());
                        if(coopTicketInfoList.size() != claimItemQty){
                            throw new LogicException(ClaimResponseCode.CLAIM_TICKET_ERR01);
                        }
                        for(LinkedHashMap<String, Object> coopTicketInfoMap : coopTicketInfoList){
                            ClaimCoopTicketCheckSetDto checkDto = new ClaimCoopTicketCheckSetDto();
                            checkDto.setSellerItemCd(ObjectUtils.toString(coopTicketInfoMap.get("seller_item_cd")));
                            checkDto.setTicketCd(ObjectUtils.toString(coopTicketInfoMap.get("ticket_cd")));
                            if(!this.getCoopTicketCheck(checkDto)){
                                throw new LogicException(ClaimResponseCode.CLAIM_TICKET_ERR02);
                            }
                        }
                    }
                    for(LinkedHashMap<String, Object> insertMap : orderTicketInfo){
                        insertMap.putAll(claimItemInfo);
                        insertMap.put("claim_item_no", claimItemNo);
                        if(!mapperService.addClaimTicketInfo(commonService.createClaimTicketDto(insertMap))){
                            throw new LogicException(ClaimResponseCode.CLAIM_TICKET_ERR03);
                        }
                    }
                }
            }
            // opt 데이터 조회
            // 주문에서 옵션별 데이터를 가지고 온다.
            ClaimOptRegDto claimOptRegDto = mapperService.getOriginOrderOptInfoForClaim(Long.parseLong(itemList.getOrderItemNo()), itemList.getOrderOptNo());
            // 클레임 번호
            claimOptRegDto.setClaimNo((Long) claimItemInfo.get(claimOpt.claimNo));
            // 클레임 번들 번호
            claimOptRegDto.setClaimBundleNo((Long) claimItemInfo.get(claimOpt.claimBundleNo));
            // 클래암 아이템 번호
            claimOptRegDto.setClaimItemNo(claimItemNo);
            // 클레임 수량
            claimOptRegDto.setClaimOptQty(Long.parseLong(itemList.getClaimQty()));
            // 마켓주문번호
            claimOptRegDto.setMarketOrderNo(ObjectUtils.toString(claimItemInfo.get(claimOpt.marketOrderNo)));
            // 생성한 데이터를 클레임 옵션별에 저장.
            if(!mapperService.addClaimOptInfo(claimOptRegDto)){
                throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
            }
            // 아이템 임직원 할인
            mapperService.callByClaimItemEmpDiscount(claimItemRegDto.getClaimNo(), claimItemRegDto.getOrderItemNo());
        }
    }

    public Boolean getTicketCancelProcess(long claimNo) throws Exception {
        LinkedHashMap<String, String> getOrderType = mapperService.getClaimOrderTypeCheck(claimNo);
        if(getOrderType != null){
            if(getOrderType.get("order_type").equals("ORD_TICKET")){
                List<LinkedHashMap<String, Object>> claimTicketInfo = mapperService.getClaimTicketInfo(claimNo);
                // 데이터 취소요청으로 업데이트
                if(claimTicketInfo.stream().map(map -> map.get("ticket_status")).map(String::valueOf).allMatch("T2,T4"::contains)){
                    return false;
                }
                //update ticket_status T1 -> T3
                for(LinkedHashMap<String, Object> map : claimTicketInfo){
                    if(!mapperService.modifyOrderTicketInfo(ClaimTicketModifyDto.builder().claimTicketNo(ObjectUtils.toLong(map.get("claim_ticket_no"))).ticketStatus("T3").issueStatus(null).build())){
                        throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR04);
                    }
                }

                if(claimTicketInfo.stream().map(map -> map.get("issue_channel")).map(String::valueOf).anyMatch(str -> str.equals("COOP"))){
                    // 취소 요청 AND 취소처리 업데이트
                    for(LinkedHashMap<String, Object> coopMap : claimTicketInfo.stream().filter(map -> ObjectUtils.toString(map.get("issue_channel")).equals("COOP")).collect(Collectors.toList())){
                        ClaimCoopTicketCheckSetDto checkDto = new ClaimCoopTicketCheckSetDto();
                        checkDto.setSellerItemCd(ObjectUtils.toString(coopMap.get("seller_item_cd")));
                        checkDto.setTicketCd(ObjectUtils.toString(coopMap.get("ticket_cd")));
                        if(!this.getCoopTicketCancel(checkDto)){
                            throw new LogicException(ClaimResponseCode.CLAIM_TICKET_ERR02);
                        }
                    }
                }
                //update ticket_status T1 -> T3
                for(LinkedHashMap<String, Object> map : claimTicketInfo){
                    if(!mapperService.modifyOrderTicketInfo(ClaimTicketModifyDto.builder().claimTicketNo(ObjectUtils.toLong(map.get("claim_ticket_no"))).ticketStatus("T4").issueStatus("I3").build())){
                        throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR04);
                    }
                }
            }
        }
        return Boolean.TRUE;
    }


    public Boolean saveCancelPointProcess(long claimNo) throws Exception {
        List<LinkedHashMap<String, Object>> createAccumulateMap = mapperService.getCreateClaimAccumulateInfo(claimNo);
        if(createAccumulateMap == null || createAccumulateMap.size() <= 0){
            log.info("적립취소할 금액이 존재하지 않음.");
            return Boolean.FALSE;
        }
        for(LinkedHashMap<String, Object> map : createAccumulateMap){
            Boolean isPartCancel = Boolean.FALSE;
            long purchaseOrderNo = ObjectUtils.toLong(map.get("purchase_order_no"));
            String pointKind = ObjectUtils.toString(map.get("point_kind"));
            log.info("주문번호[{}]의 [{}] 적립취소 데이터 ::: [{}] ", purchaseOrderNo, pointKind, map);
            // 이전 적립취소 금액
            LinkedHashMap<String, Object> preClaimAccumulateInfo = mapperService.getAccumulateCancelInfo(purchaseOrderNo, pointKind);
            log.info("주문번호[{}]의 [{}] 이전취소 데이터 ::: [{}] ", purchaseOrderNo, pointKind, preClaimAccumulateInfo);

            if(preClaimAccumulateInfo == null){
                preClaimAccumulateInfo = new LinkedHashMap<>();
                preClaimAccumulateInfo.put("claim_base_acc_amt", 0);
                preClaimAccumulateInfo.put("claim_add_acc_amt", 0);
                preClaimAccumulateInfo.put("claim_acc_target_amt", 0);
            }
            // 누적 취소금액 = 적립취소대상금액의 합
            map.put("acc_cancel_amt", preClaimAccumulateInfo.get("claim_acc_target_amt"));
            // claim_base_acc_amt / base_acc_amt - (TRUNCATE(acc_target_amt - (claim_acc_target_amt + claim_acc_target_amt), -3) *  =
            int accTargetAmt = ObjectUtils.toInt(map.get("acc_target_amt"));
            int claimAccTargetAmt = (ObjectUtils.toInt(preClaimAccumulateInfo.get("claim_acc_target_amt")) + ObjectUtils.toInt(map.get("claim_acc_target_amt")));
            if(accTargetAmt != ObjectUtils.toInt(map.get("claim_acc_target_amt"))){
                isPartCancel = Boolean.TRUE;
            }
            if(accTargetAmt == ObjectUtils.toInt(map.get("claim_acc_target_amt"))){
                // 남은적립대상금액이 0원인 경우
                // 최초적립금액 - 취소한적립금액 이 취소적립금액
                map.put("claim_base_acc_amt", ObjectUtils.toInt(map.get("base_acc_amt")) - ObjectUtils.toInt(preClaimAccumulateInfo.get("claim_base_acc_amt")));
                map.put("claim_add_acc_amt", ObjectUtils.toInt(map.get("add_acc_amt")) - ObjectUtils.toInt(preClaimAccumulateInfo.get("claim_add_acc_amt")));
            } else {
                map.put("claim_base_acc_amt", this.getCalculationSavePoint(pointKind, ObjectUtils.toInt(map.get("base_acc_amt")), ObjectUtils.toInt(preClaimAccumulateInfo.get("claim_base_acc_amt")), (accTargetAmt - claimAccTargetAmt), ObjectUtils.toDouble(map.get("base_acc_rate"))));
                map.put("claim_add_acc_amt", this.getCalculationSavePoint(pointKind, ObjectUtils.toInt(map.get("add_acc_amt")), ObjectUtils.toInt(preClaimAccumulateInfo.get("claim_add_acc_amt")), (accTargetAmt - claimAccTargetAmt), ObjectUtils.toDouble(map.get("add_acc_rate"))));
            }
            map.put("result_point", ObjectUtils.toInt(map.get("claim_base_acc_amt")) + ObjectUtils.toInt(map.get("claim_add_acc_amt")));
            log.info("주문번호({}) 취소포인트 계산 결과 ::: [{}]", purchaseOrderNo, map);
            // insert
            if(mapperService.addClaimAccumulate(map)) {
                LinkedHashMap<String, Object> resultMap = null;
                if(pointKind.equals("MHC")){
                    if(isPartCancel){
                        mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_I.getCode(), pointKind);
                        try{
                            resultMap = externalService.externalProcess(commonService.createSaveCancelMhcPartDto(map));
                        } catch (Exception e){
                            mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_E.getCode(), pointKind);
                        }
                    } else {
                        try{
                            resultMap = externalService.externalProcess(commonService.createSaveCancelMhcDto(map));
                        } catch (Exception e){
                            mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_E.getCode(), pointKind);
                        }
                    }
                    log.info("주문번호({}) MHC주문적립 취소 결과 :: {}", purchaseOrderNo, resultMap);
                    if(resultMap != null){
                        resultMap.put("claim_no", claimNo);
                        mapperService.modifyClaimAccumulateReqResult(resultMap);
                        if("0000".equals(ObjectUtils.toString(resultMap.get("returnCode")))){
                            mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_Y.getCode(), pointKind);
                        } else {
                            mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_N.getCode(), pointKind);
                        }
                    }
                } else {
                    try {
                        LinkedHashMap<String, Object> mileageSaveReturnMap = new LinkedHashMap<>();
                        mileageSaveReturnMap.put("org_purchase_order_no", map.get("purchase_order_no"));
                        mileageSaveReturnMap.put("store_type", map.get("site_type"));
                        mileageSaveReturnMap.put("user_no", map.get("user_no"));
                        mileageSaveReturnMap.put("return_amt", map.get("result_point"));
                        log.info("주문번호({}) 마일리지 적립취소요청 파라미터 ::: [{}]", purchaseOrderNo, mileageSaveReturnMap);
                        ResponseObject<String> mileageSaveReturnResponse =
                            externalService.postNonDataTransfer(
                                ExternalInfo.PAYMENT_SAVE_CANCEL_MILEAGE,
                                this.createMileageSaveCancelDto(mileageSaveReturnMap, "01"),
                                String.class
                            );
                        log.info("주문번호({}) 마일리지 주문적립 취소 결과 :: {}", purchaseOrderNo, ObjectUtils.getConvertMap(mileageSaveReturnResponse));
                        mapperService.modifyClaimAccumulateReqResult(
                            new LinkedHashMap<>(){{
                                put("returnCode", mileageSaveReturnResponse.getReturnCode());
                                put("mhcSaveCancelPoint", map.get("result_point"));
                                put("claim_no", claimNo);
                                put("mhcAprDate", DateUtils.getCurrentYmd(DateUtils.DIGIT_YMD));
                            }}
                        );
                        if("0000,SUCCESS".contains(mileageSaveReturnResponse.getReturnCode())){
                            mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_Y.getCode(), pointKind);
                        } else {
                            mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_N.getCode(), pointKind);
                        }
                    } catch (Exception e) {
                        log.error("주문번호({}) 마일리지 적립취소 요청 실패 :: {}", purchaseOrderNo, e.getMessage());
                        mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_E.getCode(), pointKind);
                    }
                }
            }
        }
        return Boolean.TRUE;
    }

    public void shipPaybackProcess(long claimNo) throws Exception {
        LinkedHashMap<String, String> claimBundleInfo = mapperService.getClaimTdBundleInfo(claimNo);
        log.info("클레임({}) 배송번호 확인 :: {}", claimNo, claimBundleInfo);
        if(claimBundleInfo != null){
            long bundleNo = ObjectUtils.toLong(claimBundleInfo.get("bundle_no"));
            LinkedHashMap<String, Object> paybackInfoMap = mapperService.getMileagePaybackCheck(bundleNo);
            log.info("클레임({}) 합배송 페이백 정보 :: {}", claimNo, paybackInfoMap);
            if(paybackInfoMap != null) {
                LinkedHashMap<String, Object> orderPriceMap = mapperService.getOrderCombinePaybackCheck(ObjectUtils.toLong(paybackInfoMap.get("org_purchase_order_no")));
                LinkedHashMap<String, Object> claimPriceMap = mapperService.getClaimCombinePaybackCheck(ObjectUtils.toLong(paybackInfoMap.get("bundle_no")), ObjectUtils.toLong(paybackInfoMap.get("org_bundle_no")));
                log.info("클레임({}) 합배송 페이백 정보 :: 주문금액[{}] / 클레임금액[{}]", claimNo, orderPriceMap, claimPriceMap);
                long orderPrice = ObjectUtils.toLong(orderPriceMap.get("payment_amt"));
                long claimPrice = ObjectUtils.toLong(claimPriceMap.get("cancel_amt"));
                long bundleFreeCondition = ObjectUtils.toLong(orderPriceMap.get("free_condition"));
                log.info("클레임({}) 페이백 회수 허들 체크 :: 클레임연산금액[{}] / 배송비허들[{}]", claimNo, (orderPrice - claimPrice), bundleFreeCondition);
                if((orderPrice - claimPrice) < bundleFreeCondition) {
                    boolean isUpdateStep = Boolean.TRUE;
                    // 상태에 따른 처리
                    if(ObjectUtils.isEquals(paybackInfoMap.get("result_yn"), "Y")){
                        try {
                            ResponseObject<String> paybackResult =
                                externalService.postNonDataTransfer(
                                    ExternalInfo.PAYMENT_SAVE_CANCEL_MILEAGE,
                                    this.createMileageSaveCancelDto(paybackInfoMap, "02"),
                                    String.class
                                );
                            if(!"0000,SUCCESS".contains(paybackResult.getReturnCode())){
                                isUpdateStep = Boolean.FALSE;
                            }
                        } catch (Exception e) {
                            log.error("배송비 PAYBACK 요청 실패 :: {}", e.getMessage());
                            isUpdateStep = Boolean.FALSE;
                        }
                    }
                    // 업데이트
                    if(isUpdateStep){
                        mapperService.setShipPaybackResult(ObjectUtils.toLong(paybackInfoMap.get("payback_no")));
                    }
                }
            }
        }
    }

    private MileageSaveCancelDto createMileageSaveCancelDto(LinkedHashMap<String, Object> map, String type) {
        return MileageSaveCancelDto.builder()
            .tradeNo(ObjectUtils.toString(map.get("org_purchase_order_no")))
            .cancelType(type)
            .storeType(ObjectUtils.toString(map.get("store_type")))
            .requestReason(type.equals("02") ? "클레임에 따른 Payback 회수" : "클레이 따른 주문적립 회수")
            .userNo(ObjectUtils.toString(map.get("user_no")))
            .mileageAmt(ObjectUtils.toLong(map.get("return_amt")))
            .regId("CLAIM")
            .build();
    }


    public void setClaimPaymentStatus(Object claimPaymentNo, String claimApprovalCd, ClaimCode claimCode) throws Exception {
        if(!mapperService.setClaimPaymentStatus(Long.parseLong(String.valueOf(claimPaymentNo)), claimApprovalCd, claimCode)){
            throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
        }
    }

    public void setClaimPaymentCancelTradeId(Object claimPaymentNo, Object cancelTradeId ) {
        if(!mapperService.modifyClaimPaymentPgCancelTradeId(ObjectUtils.toLong(claimPaymentNo), ObjectUtils.toString(cancelTradeId))){
            log.error("ClaimPaymentNo({}) - PG 거래취소 업데이트 실패 ", claimPaymentNo);
        }
    }

    public void setClaimPaymentStatus(long claimNo, ClaimCode claimCode) throws Exception {
        if(!mapperService.setTotalClaimPaymentStatus(claimNo, claimCode)){
            throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
        }
    }

    public void callingClaimReg(long purchaseOrderNo, long paymentNo, long claimNo){
        mapperService.callByEndClaimFlag(ClaimInfoRegDto.builder().paymentNo(paymentNo).purchaseOrderNo(purchaseOrderNo).claimNo(claimNo).build());
    }

    public void callingClaimReg(long claimNo){
        ClaimInfoRegDto regDto = mapperService.getClaimRegData(claimNo);
        log.info("클레임그룹번호({}) claim_info Register :: {}", claimNo, regDto);
        if(regDto != null){
            mapperService.callByEndClaimFlag(regDto);
        }
    }

    public List<LinkedHashMap<String, Object>> getOrderBundleByClaim(String purchaseOrderNo) {
        return mapperService.getOrderBundleByClaim(Long.parseLong(purchaseOrderNo));
    }

    public List<ClaimPreRefundItemList> getClaimPreRefundItemInfo(long purchaseOrderNo, long bundleNo) {
        return mapperService.getClaimPreRefundItemInfo(purchaseOrderNo, bundleNo);
    }

    public Boolean setClaimProcessHistory(ClaimProcessHistoryRegDto claimProcessHistoryRegDto){
        return mapperService.setClaimProcessHistory(claimProcessHistoryRegDto);
    }

    public Boolean restoreStockSlot(RestoreSlotStockDto restoreDto) throws Exception {
        try {
            LinkedHashMap<String, Object> returnMap = externalService.externalProcess(restoreDto);
            if("0000,2002,SUCCESS".contains(ObjectUtils.toString(returnMap.get("returnCode")))){
                return Boolean.TRUE;
            }
        } catch (Exception e){
            log.error("슬롯 마감 처리 오류 ::: {}", e.getMessage());
        }
        return Boolean.FALSE;
    }

    /**
     * 클레임 상태 변경
     *
     * @param claimNo 클레임번호
     * @param claimBundleNo 클레임주문번호
     * @param claimStatus 클레임상태
     * @return Boolean 클레임상태변경 결과.
     */
    public Boolean setClaimStatus(long claimNo, long claimBundleNo, String regId, String claimStatus) {
        return mapperService.setClaimStatus(createClaimStatusMap(claimNo, claimBundleNo, regId, claimStatus));
    }

    /**
     * 클레임 상태 변경
     *
     * @param claimNo 클레임번호
     * @param claimBundleNo 클레임주문번호
     * @param claimCode 클레임상태 Enum
     * @return Boolean 클레임상태변경 결과.
     */
    public Boolean setClaimStatus(long claimNo, long claimBundleNo, String regId, ClaimCode claimCode) {
        return mapperService.setClaimStatus(createClaimStatusMap(claimNo, claimBundleNo, regId, claimCode.getCode()));
    }

    /**
     * 클레임 상태 변경
     *
     * @param setDto 클레임 상태변경 DTO
     * @return Boolean 클레임상태변경 결과.
     */
    public Boolean setClaimStatus(ClaimRequestSetDto setDto) throws Exception {
        return mapperService.setClaimStatus(
            new LinkedHashMap<>(){{
                // 클레임 번호
                put("claimNo", setDto.getClaimNo());
                // 클레임 번들번호
                put("claimBundleNo", setDto.getClaimBundleNo());
                // 등록자(*수정자)
                put("regId", setDto.getRegId());
                // 클레임 상태
                put("claimStatus", getClaimStatus(setDto.getClaimReqType()));
                // 클레임 사유코드
                put("reasonType", setDto.getReasonType());
                // 클레임 상세사유
                put("reasonDetail", setDto.getReasonTypeDetail());
            }}
        );
    }

    private LinkedHashMap<String, Object> createClaimStatusMap(long claimNo, long claimBundleNo, String regId, String claimStatus) {
        return new LinkedHashMap<>(){{
            // 클레임 번호
            put("claimNo", claimNo);
            // 클레임 번들번호
            put("claimBundleNo", claimBundleNo);
            // 등록자(*수정자)
            put("regId", (claimStatus.equals("C3") || (claimStatus.equals("C2") && regId.equalsIgnoreCase("MYPAGE"))) ? "SYSTEM" : regId);
            // 클레임 상태
            put("claimStatus", claimStatus);
        }};
    }

    /**
     * 결제취소 응답 코드 중
     * 오류가 아닌 건 체크
     * @param paymentType 결제취소타입
     * @param responseCode 응답코드
     * @return Boolean 진행여부
     */
    private Boolean isProcessStep(String paymentType, String responseCode) {
        switch (paymentType) {
            case "PG" :
                return "AV11".contains(responseCode);
            case "MP" :
                return "3013".contains(responseCode);
            case "PP" :
                return "3218".contains(responseCode);
            case "DG" :
                return "8507".contains(responseCode);
            case "OC" :
                return "3042".contains(responseCode);
            default:
                return Boolean.FALSE;
        }
    }

    public Integer getEmpDiscountLimitAmt(String empNo) throws Exception {
        EmpInfoDto empInfo = externalService.getTransfer(
            ExternalInfo.EMP_DISCOUNT_LIMIT_AMT_INFO,
            new ArrayList<SetParameter>(){{ add(SetParameter.create("empNo", empNo)); }},
            EmpInfoDto.class
        );
        if(empInfo == null){
            throw new LogicException(ClaimResponseCode.CLAIM_PAYMENT_CANCEL_EMP_ERR01);
        }
        return empInfo.getRemainPoint();
    }

    public Boolean isClaimCouponAndSlotProcess(long claimNo) throws Exception {
        // 클레임 정보
        List<LinkedHashMap<String, Object>> claimInfoMap = mapperService.getClaimInfo(claimNo);
        log.info("클레임 쿠폰재발행 & 슬롯마감 처리 데이터 ::: {}", claimInfoMap);
        // 클레임 정보가 없으면 리턴.
        if(claimInfoMap == null){
            return Boolean.FALSE;
        }
        long bundleNo = claimInfoMap.stream().map(map -> map.get("bundle_no")).map(ObjectUtils::toLong).distinct().collect(Collectors.toList()).get(0);
        String claimType = claimInfoMap.stream().map(map -> map.get("claim_type")).map(ObjectUtils::toString).distinct().collect(Collectors.toList()).get(0);
        //해당 상품이 반품/교환일 경우에만 배송확정처리
        if("R|X".contains(claimType)){
            this.setClaimShipDecision(String.valueOf(bundleNo), "CLAIM");
        }
        // userNo
        long userNo = claimInfoMap.stream().map(map -> map.get("user_no")).map(ObjectUtils::toLong).distinct().collect(Collectors.toList()).get(0);
        long purchaseOrderNo = claimInfoMap.stream().map(map -> map.get("purchase_order_no")).map(ObjectUtils::toLong).distinct().collect(Collectors.toList()).get(0);
        // 쿠폰 재발행 건수 확인
        List<CouponReIssueDto> reIssueList = mapperService.getClaimCouponReIssueInfo(claimNo, userNo);
        // 쿠폰 재발행 건수가 있으면, 재발행.
        if(reIssueList != null && reIssueList.size() > 0){
            if(getCouponReIssueInfo(reIssueList, userNo, purchaseOrderNo)){
                mapperService.modifyClaimCouponReIssueResult(reIssueList.stream().map(CouponReIssueDto::getClaimAdditionNo).collect(Collectors.toList()));
            }
        }
        List<LinkedHashMap<String, Object>> slotCloseData = claimInfoMap.stream().filter(map -> ObjectUtils.toString(map.get("order_category")).equals("TD")).filter(map -> ObjectUtils.toString(map.get("claim_part_yn")).equals("N")).filter(map -> ObjectUtils.toString(map.get("is_close")).equals("N")).collect(Collectors.toList());
        // 슬롯마감
        if(slotCloseData.size() > 0) {
            if(slotCloseData.stream().map(map -> map.get("order_type")).map(String::valueOf).noneMatch(str -> str.equals("ORD_COMBINE"))){
                if(restoreStockSlot(commonService.createRestoreStockSlotDto(claimInfoMap.get(0)))){
                    mapperService.modifyClaimBundleSlotResult(ObjectUtils.toLong(claimInfoMap.get(0).get("claim_bundle_no")));
                }
            }
        }
        // 배송 구매확정 처리.
        return Boolean.TRUE;
    }

    public Boolean getCouponReIssueInfo(List<CouponReIssueDto> reIssueList, long userNo, long purchaseOrderNo) throws Exception {
        try {
            ClaimCouponReIssueGetDto result = externalService.postTransfer(
                ExternalInfo.CLAIM_BY_COUPON_RE_ISSUE,
                commonService.createClaimCouponReIssueDto(reIssueList, userNo, purchaseOrderNo),
                ClaimCouponReIssueGetDto.class
            );
            if(!result.getResultCode().equals("0000")){
                return Boolean.FALSE;
            }
            return Boolean.TRUE;
        } catch (Exception e){
            log.error("Coupon Re Issue Error ::: {}", e.getMessage());
            return Boolean.FALSE;
        }
    }

    public void sendAlimTalk(long claimNo) throws Exception {
        LinkedHashMap<String, Object> claimSendDataMap = mapperService.getClaimSendDataInfo(claimNo);
        if("R".equals(ObjectUtils.toString(claimSendDataMap.get("claim_type")))){
            try{
                MessageSendTalkGetDto sendResponse = externalService.postTransfer(
                    ExternalInfo.MESSAGE_SEND_ALIM_TALK,
                    commonService.createMessageSendAlimTalkDto(claimSendDataMap),
                    MessageSendTalkGetDto.class
                );
            } catch (Exception e) {
                log.error("Claim Completed Send Message Error ::: {}", e.getMessage());
            }
        }
    }

    public void setClaimShipStart(String ordNo) throws Exception {
        List<ClaimShipStartDto> shipStartDtoList = new ArrayList<>();
        shipStartDtoList.add(ClaimShipStartDto.builder().purchaseStoreInfoNo(ordNo).build());
        resourceClient.postForResponseObject(
            ExternalInfo.SHIPPING_CLAIM_FROM_SHIP_START.getApiId(),
            shipStartDtoList,
            ExternalInfo.SHIPPING_CLAIM_FROM_SHIP_START.getUri()
        );
    }

    public void setClaimShipComplete(String bundleNo, String regId) throws Exception{
        externalService.postTransfer(
            ExternalInfo.SHIPPING_CLAIM_FROM_SHIP_COMPLETE,
            ClaimShipCompleteDto.builder().bundleNo(bundleNo).chgId(regId).build(),
            String.class
        );
    }

    public void setClaimShipDecision(String bundleNo, String regId) throws Exception{
        externalService.postTransfer(
            ExternalInfo.SHIPPING_CLAIM_FROM_SHIP_DECISION,
            ClaimShipCompleteDto.builder().bundleNo(bundleNo).chgId(regId).build(),
            String.class
        );
    }

    public void createClaimSafetyIssue(long claimBundleNo, String issueType) {
        try {
            Integer resultData = externalService.postTransfer(
                ExternalInfo.CLAIM_SAFETY_ISSUE_ADD,
                mapperService.getClaimSafetyData(claimBundleNo, issueType),
                Integer.class
            );
            log.debug("createClaimSafetyIssue result ::: {}", resultData);
        } catch (Exception e) {
            log.error("createClaimSafetyIssue ERROR ::: {}", e.getMessage());
        }
    }

    private Boolean getCoopTicketCheck(ClaimCoopTicketCheckSetDto checkDto) throws Exception {
        try{
            ClaimCoopTicketCheckGetDto returnDto =
                externalService.postTransfer(
                    ExternalInfo.OUTBOUND_COOP_TICKET_CHECK_FOR_CLAIM,
                    checkDto,
                    ClaimCoopTicketCheckGetDto.class
                );
            return "N,C".contains(returnDto.getUseYn());
        } catch (Exception e){
            log.error("getCoopTicketCheck ::: checkDto :: {} ::: ErrorMsg :: {}", checkDto, e.getMessage());
            return false;
        }
    }

    private Boolean getCoopTicketCancel(ClaimCoopTicketCheckSetDto checkDto) throws Exception {
        try{
            log.debug("getCoopTicketCancel ::: {}", checkDto);
            ClaimCoopTicketCheckGetDto returnDto =
                externalService.postTransfer(
                    ExternalInfo.OUTBOUND_COOP_TICKET_CANCEL_FOR_CLAIM,
//                    new ArrayList<SetParameter>(){{
//                        add(SetParameter.create("sellerItemCd", checkDto.getSellerItemCd()));
//                        add(SetParameter.create("ticketCd", checkDto.getTicketCd()));
//                    }},
                    checkDto,
                    ClaimCoopTicketCheckGetDto.class
                );
            return "00,12".contains(returnDto.getResultCode());
        } catch (Exception e){
            log.error("getCoopTicketCancel ::: checkDto :: {} ::: ErrorMsg :: {}", checkDto, e.getMessage());
            return false;
        }
    }

    private Integer getCalculationSavePoint(String pointKind, int orderSavePoint, int cancelSaveAmt, int claimAccTargetAmt, double rate){
        BigDecimal claimTargetAmt = new BigDecimal(claimAccTargetAmt);
        // 기본적립.
        if(pointKind.equals("MHC")){
            int claimMhcSavePoint = claimTargetAmt.divide(new BigDecimal(100 * 10), 0, RoundingMode.FLOOR).multiply(new BigDecimal(rate * 10)).intValue();
            return (orderSavePoint - cancelSaveAmt) - claimMhcSavePoint;
        }
        return (orderSavePoint - cancelSaveAmt) - claimTargetAmt.divide(new BigDecimal(100), 0, RoundingMode.FLOOR).multiply(new BigDecimal(rate)).setScale(0, RoundingMode.FLOOR).intValue();
    }


    public String getClaimStatus(String claimReqType) {
        String claimChangeStatus = null;
        // 상태에 대한 확인
        switch (claimReqType.toUpperCase(Locale.KOREA)){
            // 승인
            case "CA" :
                claimChangeStatus = ClaimCode.CLAIM_STATUS_C2.getCode();
                break;
            // 철회
            case "CW" :
                claimChangeStatus = ClaimCode.CLAIM_STATUS_C4.getCode();
                break;
            // 보류
            case "CH" :
                claimChangeStatus = ClaimCode.CLAIM_STATUS_C8.getCode();
                break;
            // 거부
            case "CD" :
                claimChangeStatus = ClaimCode.CLAIM_STATUS_C9.getCode();
                break;
            // 완료
            case "CC" :
                claimChangeStatus = ClaimCode.CLAIM_STATUS_C3.getCode();
                break;
            default:
                claimChangeStatus = "";
                break;
        }
        return claimChangeStatus;
    }
}
