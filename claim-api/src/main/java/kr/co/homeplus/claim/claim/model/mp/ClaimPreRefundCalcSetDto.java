package kr.co.homeplus.claim.claim.model.mp;


import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClaimPreRefundCalcSetDto {

    @NotNull(message = "주문번호가 없습니다.")
    @ApiModelProperty(value = "주문번호", position = 1)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "상품번호", position = 2)
    private List<ClaimPreRefundItemList> claimItemList;

    @NotNull(message = "귀책사유가 없습니다.")
    @ApiModelProperty(value = "귀책사유(B:고객/S:판매자/H:홈플러스)", position = 3)
    @Pattern(regexp = "B|S|H", message = "귀책사유")
    private String whoReason;

    @NotNull(message = "취소타입이 없습니다.")
    @ApiModelProperty(value = "취소타입(O:주문취소/R:반품/X:교환)", position = 4)
    @Pattern(regexp = "O|R|X", message = "취소타입")
    private String cancelType;

    @NotNull(message = "클레임사유코드가 없습니다.")
    @ApiModelProperty(value = "클레임사유코드", position = 5)
    private String claimReasonType;

    @ApiModelProperty(value = "부분취소여부(Y:부분취소,N:전체취소)", position = 6)
    private String claimPartYn;

    @JsonIgnore
    @ApiModelProperty(value = "고객번호", position = 7, hidden = true)
    private long userNo;

    @JsonIgnore
    @ApiModelProperty(value = "omni여부", position = 8, hidden = true)
    private Boolean isOmni = Boolean.FALSE;

    @ApiModelProperty(value = "동봉 여부(Y:동봉,N:동봉안함)", position = 9)
    private String encloseYn = "N";

    @ApiModelProperty(value = "주문취소여부", position = 10)
    private String orderCancelYn;

    @NotNull(message = "클레임타입이 없습니다.")
    @ApiModelProperty(value = "클레임타입(C:취소/R:반품/X:교환)", position = 11)
    private String claimType;

    @NotNull(message = "번들번호가 없습니다.")
    @ApiModelProperty(value = "번들번호", position = 12)
    private String bundleNo;

    @ApiModelProperty(value = "다중배송주문여부", position = 13, hidden = true)
    private Boolean isMultiOrder;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ClaimPreRefundItemList {
        @ApiModelProperty(value = "상품번호", position = 1)
        private String itemNo;

        @ApiModelProperty(value = "옵션번호", position = 2)
        private String orderOptNo;

        @ApiModelProperty(value = "클레임수량", position = 3)
        private String claimQty;

        @ApiModelProperty(value = "상품주문번호", position = 4)
        private String orderItemNo;

    }
}
