package kr.co.homeplus.claim.claim.model.external;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "마일리지 취소")
public class ClaimMileageCancelSetDto {

    @ApiModelProperty(value = "유저번호", position = 1)
    private String userNo;

    @ApiModelProperty(value = "취소요청금액", position = 2)
    private String requestCancelAmt;

    @ApiModelProperty(value = "거래번호", position = 3)
    private long tradeNo;

    @ApiModelProperty(value = "취소메시지", position = 4)
    private String cancelMessage;

    @ApiModelProperty(value = "등록자", position = 5)
    private String regId;

}
