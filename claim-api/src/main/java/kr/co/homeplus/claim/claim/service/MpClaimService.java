package kr.co.homeplus.claim.claim.service;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claim.claim.model.mp.ClaimAddCouponDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimChgPaymentRefundSetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimDetailGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimItemListDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimListItemListDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPaymentType;
import kr.co.homeplus.claim.claim.model.mp.ClaimPhonePaymentInfoDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claim.claim.model.mp.ClaimReasonSearchGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimRefundType;
import kr.co.homeplus.claim.claim.model.mp.ClaimRegisterPreRefundInfoDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimSearchListGetDto;
import kr.co.homeplus.claim.claim.model.mp.ClaimSearchListSetDto;
import kr.co.homeplus.claim.core.exception.LogicException;
import kr.co.homeplus.claim.enums.ClaimResponseCode;
import kr.co.homeplus.claim.order.model.mypage.MpOrderPaymentInfoDto;
import kr.co.homeplus.claim.claim.model.register.ClaimProcessHistoryRegDto;
import kr.co.homeplus.claim.claim.model.register.ClaimRefundRegDto;
import kr.co.homeplus.claim.enums.ResponseCode;
import kr.co.homeplus.claim.factory.ResponseFactory;
import kr.co.homeplus.claim.order.model.mypage.MpOrderGiftItemListDto;
import kr.co.homeplus.claim.order.service.MpOrderMapperService;
import kr.co.homeplus.claim.utils.DateUtils;
import kr.co.homeplus.claim.utils.ObjectUtils;
import kr.co.homeplus.claim.utils.StringUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.client.model.ResponsePagination;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MpClaimService {

    private final ClaimMapperService claimManagementMapper;
    private final ClaimProcessService claimProcessService;
    private final MpOrderMapperService mpOrderMapperService;

    /**
     * 클레임 리스트 조회
     * @param claimSearchListSetDto 조회요청 DTO
     * @return List<ClaimSearchListGetDto> 조회응답.
     */
    public ResponseObject<List<ClaimSearchListGetDto>> getClaimSearchList(ClaimSearchListSetDto claimSearchListSetDto) throws Exception {
        log.info("[getClaimSearchList] 클레임 리스트 조회 : {}", claimSearchListSetDto);

        /* 0.파라미터 유효성 검사 */
        if(StringUtils.isEmpty(claimSearchListSetDto.getUserNo())){
            log.info("[getClaimSearchList] 유효성 검사 실패, 회원 아이디 정보가 없습니다.");
            return ResponseFactory.getResponseObject(ResponseCode.CLAIM_VALID_ERROR_USERNO, "회원 아이디 정보가 없습니다.");
        }

        /* 1.클레임 리스트 조회 */
        List<ClaimSearchListGetDto> claimSearchList = claimManagementMapper.getClaimSearchList(claimSearchListSetDto);


        /* 상품/환불 리스트 조회 */
        for(ClaimSearchListGetDto claimSearch : claimSearchList){
            /* 2.상품 리스트 조회 */
            List<ClaimListItemListDto> claimItemList = claimManagementMapper.getClaimListItemList(claimSearch.getClaimBundleNo());

            //상품 정보 세팅
            if(claimItemList != null){
                claimSearch.setClaimItemList(claimItemList);
            }

            /* 3.환불 정보조회 */
            List<ClaimRefundType> claimRefundTypeList = claimManagementMapper.getRefundTypeList(claimSearch.getClaimNo());

            //환불 정보 세팅
            if(claimRefundTypeList != null){
                claimSearch.setClaimRefundTypeList(claimRefundTypeList);
            }
        }

        /* 4.리스트 count 조회 */
        int totalCnt = claimManagementMapper.getClaimSearchListCnt(claimSearchListSetDto);


        /* return */
        return ResponseFactory.getResponseObjectPaging(ResponseCode.SUCCESS, claimSearchList,
            ResponsePagination.Builder.builder().totalCount(totalCnt).setAsPage(claimSearchListSetDto.getPage(), claimSearchListSetDto.getPerPage(), this.getTotalPage(totalCnt, claimSearchListSetDto.getPerPage())).build());
    }

    /**
     * 클레임 상세 조회
     * @param claimNo
     * @return ClaimDetailGetDto 조회응답.
     */
    public ResponseObject<ClaimDetailGetDto> getClaimDetail(long userNo, long claimNo) throws Exception {
        /* 0.클레임 번들 번호 유효성 검사*/
        if(claimNo == 0){
            log.info("[getClaimDetail] 유효성 검사 실패, claimNo 값이 올바르지 않습니다. {}", claimNo);
            return ResponseFactory.getResponseObject(ResponseCode.CLAIM_VALID_ERROR, "claimBundleNo 값이 올바르지 않습니다.");
        }

        /* 1.클레임 상세정보 조회 */
        ClaimDetailGetDto claimDetailGetDto = claimManagementMapper.getClaimDetail(userNo, claimNo);


        /* 1-1. 클레임 상세정보 조회 결과 null인 경우 return*/
        if(claimDetailGetDto == null){
            return ResponseFactory.getResponseObject(ResponseCode.CLAIM_DETAIL_SEARCH_ERR01);
        }

        /* 2.클레임 상품 리스트 조회 */
        List<ClaimItemListDto> claimItemList = claimManagementMapper.getClaimItemList(claimNo);
        claimDetailGetDto.setClaimItemList(claimItemList);

        /* 3.클레임 환불정보 금액 조회 */
        ClaimRegisterPreRefundInfoDto claimRegisterPreRefundInfoDto = claimManagementMapper.getRegisterPreRefundAmt(claimDetailGetDto.getClaimNo());

        /* 3-1. 결품여부 확인 */
        if(claimRegisterPreRefundInfoDto.getOosYn().equalsIgnoreCase("Y")){
            claimRegisterPreRefundInfoDto = new ClaimRegisterPreRefundInfoDto(claimRegisterPreRefundInfoDto.getOutOfStockRefundAmt());
        }

        /* 3-2. 배송비 동봉여부에 따른 차감금액 재계산 */
        if(claimDetailGetDto.getClaimShipFeeEnclose().equalsIgnoreCase("Y")){
            long totShipPrice = claimRegisterPreRefundInfoDto.getAddShipPrice() + claimRegisterPreRefundInfoDto.getAddIslandShipPrice();
            claimRegisterPreRefundInfoDto.setDiscountAmt(claimRegisterPreRefundInfoDto.getDiscountAmt()-totShipPrice);
        }

        claimDetailGetDto.setClaimPreRefundInfo(claimRegisterPreRefundInfoDto);

        /* 4.환불 수단 조회 */
        claimDetailGetDto.setClaimRefundTypeList(claimManagementMapper.getRefundTypeList(claimDetailGetDto.getClaimNo()));

        /* 5.사은품 정보 조회 */
        claimDetailGetDto.setOrderGiftList(claimManagementMapper.selectClaimPurchaseGift(claimDetailGetDto.getClaimNo()));

        /* return */
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, claimDetailGetDto);
    }

    /**
     * 클레임 사유 조회
     * @param claimType C:취소, R:반품, X:교환
     * @return ClaimReasonSearchGetDto
     * **/
    public ResponseObject<List<ClaimReasonSearchGetDto>> getClaimReason(String claimType) throws Exception {

        if("C|R|X".contains(claimType)){
            return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, claimManagementMapper.getClaimReason(claimType));
        }
        return ResponseFactory.getResponseObject(ResponseCode.CLAIM_VALID_ERROR, "클레임 타입이 올바르지 않습니다.");
    }

    /**
     * 환불예정금액조회
     * @param claimPreRefundCalcSetDto
     * @return ClaimPreRefundCalcGetDto
     * **/
    public ResponseObject<ClaimPreRefundCalcGetDto> getPreRefundPrice(ClaimPreRefundCalcSetDto claimPreRefundCalcSetDto) throws Exception {
        //* 0.클레임 가능여부 체크 *//*
        if(!this.getClaimPossible(claimPreRefundCalcSetDto)){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR09);
        }
        // 환불예정금전 합배송주문있는 원주문에 대한 취소 확인
        if("O,R".contains(claimPreRefundCalcSetDto.getCancelType())){
            List<LinkedHashMap<String, Object>> originOrderCheck = claimManagementMapper.getOrderCombineClaimCheck(claimPreRefundCalcSetDto.getPurchaseOrderNo(), "ORIGIN");
            boolean isCombine = originOrderCheck.stream().map(map -> map.get("cmbn_yn")).map(ObjectUtils::toString).allMatch(cmbnYn -> cmbnYn.equals("Y"));
            int originOrderQty = originOrderCheck.stream().map(map -> map.get("order_item_qty")).mapToInt(ObjectUtils::toInt).sum();
            int originClaimQty = originOrderCheck.stream().map(map -> map.get("claim_item_qty")).mapToInt(ObjectUtils::toInt).sum();
            int requestQty = claimPreRefundCalcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getClaimQty).mapToInt(Integer::parseInt).sum();

            if(isCombine){
                List<LinkedHashMap<String, Object>> combineOrderCheck = claimManagementMapper.getOrderCombineClaimCheck(claimPreRefundCalcSetDto.getPurchaseOrderNo(), "COMBINE");
                int combineOrderQty = combineOrderCheck.stream().map(map -> map.get("order_item_qty")).mapToInt(ObjectUtils::toInt).sum();
                int combineClaimQty = combineOrderCheck.stream().map(map -> map.get("claim_item_qty")).mapToInt(ObjectUtils::toInt).sum();
                if(combineOrderQty - combineClaimQty > 0){
                    if(originOrderQty - (originClaimQty + requestQty) <= 0){
                        String cancelTypeStr = claimPreRefundCalcSetDto.getCancelType().equals("O") ? "취소" : "반품";
                        throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR14, "%s", cancelTypeStr);
                    }
                }
            }
            if("O".equals(claimPreRefundCalcSetDto.getCancelType())){
                // 중복쿠폰 여부 판단.
                List<ClaimAddCouponDto> addCouponInfoList = claimManagementMapper.isAddCouponPossible(ObjectUtils.toLong(claimPreRefundCalcSetDto.getPurchaseOrderNo()));
                if(addCouponInfoList != null){
                    log.info("주문번호({})에 대한 중복쿠폰 데이터 조회 : {}", claimPreRefundCalcSetDto.getPurchaseOrderNo(), addCouponInfoList);
                    int originListSize = addCouponInfoList.size();

                    claimPreRefundCalcSetDto.getClaimItemList().forEach(
                        dto -> {
                            if(addCouponInfoList.stream().filter(addCouponDto -> dto.getOrderItemNo().equals(ObjectUtils.toString(addCouponDto.getOrderItemNo()))).allMatch(addCouponDto -> dto.getClaimQty().equals(ObjectUtils.toString(addCouponDto.getItemQty())))) {
                                addCouponInfoList.removeIf(addCouponDto -> dto.getOrderItemNo().equals(ObjectUtils.toString(addCouponDto.getOrderItemNo())));
                            }
                        }
                    );
                    if(originListSize != addCouponInfoList.size() && addCouponInfoList.size() != 0) {
                        if(addCouponInfoList.stream().anyMatch(dto -> dto.getIsUseTdCoupon().equals("Y")) && addCouponInfoList.stream().anyMatch(dto -> dto.getIsUseDsCoupon().equals("Y"))) {
                            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR16);
                        }
                    }
                }
            }
        }
        /* 1.환불예정금액 조회 */
        ClaimPreRefundCalcDto claimPreRefundCalcGetDto = claimManagementMapper.getClaimPreRefundCalculation(claimPreRefundCalcSetDto);
        ClaimPreRefundCalcGetDto responseRefundDto = new ClaimPreRefundCalcGetDto();
        // beanCopy
        BeanUtils.copyProperties(claimPreRefundCalcGetDto, responseRefundDto);

        /* 2.환불수단 리스트 조회 */
        List<ClaimPaymentType> claimPaymentList = claimManagementMapper.getPaymentTypeList(Long.parseLong(claimPreRefundCalcSetDto.getPurchaseOrderNo()));
        //결제 수단 별 환불예정금액 적용
        for(ClaimPaymentType claimPaymentType : claimPaymentList){
            claimPaymentType.setPgAmt(claimPreRefundCalcGetDto.getPgAmt());
            claimPaymentType.setMhcAmt(claimPreRefundCalcGetDto.getMhcAmt());
            claimPaymentType.setDgvAmt(claimPreRefundCalcGetDto.getDgvAmt());
            claimPaymentType.setOcbAmt(claimPreRefundCalcGetDto.getOcbAmt());
            claimPaymentType.setMileageAmt(claimPreRefundCalcGetDto.getMileageAmt());
        }
        responseRefundDto.setClaimPaymentTypeList(claimPaymentList);
        /* 3.주문정보 조회 조회 */
        MpOrderPaymentInfoDto paymentInfoDto = claimManagementMapper.getPurchaseOrderInfo(Long.parseLong(claimPreRefundCalcSetDto.getPurchaseOrderNo()), Long.parseLong(responseRefundDto.getClaimPaymentTypeList().get(0).getUserNo()));
        /* 4.상품금액/결제 금액 세팅 */
        responseRefundDto.setTotItemPrice(paymentInfoDto.getTotItemPrice());
        responseRefundDto.setTotPaymentAmt(paymentInfoDto.getTotPaymentAmt());
        responseRefundDto.setAddCouponDiscountAmt(claimPreRefundCalcGetDto.getAddTdDiscountAmt() + claimPreRefundCalcGetDto.getAddDsDiscountAmt());
        if(claimPreRefundCalcGetDto.getReturnValue() != 0){
            return ResponseFactory.getResponseObject(String.valueOf(claimPreRefundCalcGetDto.getReturnValue()), claimPreRefundCalcGetDto.getReturnMsg());
        }
        /* response */
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, responseRefundDto);
    }

    public ResponseObject<ClaimPreRefundCalcGetDto> getPreMultiRefundPrice(ClaimPreRefundCalcSetDto claimPreRefundCalcSetDto) throws Exception {
        // 프론트는 전체취소만 가능
        if("O".equalsIgnoreCase(claimPreRefundCalcSetDto.getCancelType())){
            List<LinkedHashMap<String, Object>> originOrderCheck = claimManagementMapper.getOrderCombineClaimCheck(claimPreRefundCalcSetDto.getPurchaseOrderNo(), "MULTI");
            int originOrderQty = originOrderCheck.stream().map(map -> map.get("order_item_qty")).mapToInt(ObjectUtils::toInt).sum();
            int claimQty = claimPreRefundCalcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getClaimQty).mapToInt(ObjectUtils::toInt).sum();
            if(originOrderQty != claimQty) {
                throw new LogicException(ClaimResponseCode.CLAIM_MULTI_REG_ERR01, "%s", "선물세트");
            }
        }
        /* 1.환불예정금액 조회 */
        ClaimPreRefundCalcDto claimPreRefundCalcGetDto = claimManagementMapper.getClaimMultiPreRefundCalculation(claimPreRefundCalcSetDto);
        ClaimPreRefundCalcGetDto responseRefundDto = new ClaimPreRefundCalcGetDto();
        // beanCopy
        BeanUtils.copyProperties(claimPreRefundCalcGetDto, responseRefundDto);

        /* 2.환불수단 리스트 조회 */
        List<ClaimPaymentType> claimPaymentList = claimManagementMapper.getPaymentTypeList(Long.parseLong(claimPreRefundCalcSetDto.getPurchaseOrderNo()));
        //결제 수단 별 환불예정금액 적용
        for(ClaimPaymentType claimPaymentType : claimPaymentList){
            claimPaymentType.setPgAmt(claimPreRefundCalcGetDto.getPgAmt());
            claimPaymentType.setMhcAmt(claimPreRefundCalcGetDto.getMhcAmt());
            claimPaymentType.setDgvAmt(claimPreRefundCalcGetDto.getDgvAmt());
            claimPaymentType.setOcbAmt(claimPreRefundCalcGetDto.getOcbAmt());
            claimPaymentType.setMileageAmt(claimPreRefundCalcGetDto.getMileageAmt());
        }
        responseRefundDto.setClaimPaymentTypeList(claimPaymentList);
        /* 3.주문정보 조회 조회 */
        MpOrderPaymentInfoDto paymentInfoDto = claimManagementMapper.getPurchaseOrderInfo(Long.parseLong(claimPreRefundCalcSetDto.getPurchaseOrderNo()), Long.parseLong(responseRefundDto.getClaimPaymentTypeList().get(0).getUserNo()));
        /* 4.상품금액/결제 금액 세팅 */
        responseRefundDto.setTotItemPrice(paymentInfoDto.getTotItemPrice());
        responseRefundDto.setTotPaymentAmt(paymentInfoDto.getTotPaymentAmt());
        responseRefundDto.setAddCouponDiscountAmt(claimPreRefundCalcGetDto.getAddTdDiscountAmt() + claimPreRefundCalcGetDto.getAddDsDiscountAmt());
        if(claimPreRefundCalcGetDto.getReturnValue() != 0){
            return ResponseFactory.getResponseObject(String.valueOf(claimPreRefundCalcGetDto.getReturnValue()), claimPreRefundCalcGetDto.getReturnMsg());
        }
        /* response */
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, responseRefundDto);
    }

    /**
     * 클레임 환불수단 변경
     * @param claimChgPaymentRefundSetDto
     * @return ClaimPreRefundCalcGetDto
     * **/
    public ResponseObject<String> chgPaymentRefund(ClaimChgPaymentRefundSetDto claimChgPaymentRefundSetDto) throws Exception {

        /* 1.환불수단 등록여부 조회 */
        int regResult = claimManagementMapper.getAlreadyRegClaimRefundCnt(claimChgPaymentRefundSetDto);

        /* 1-1.환불수단 등록여부 확인 */
        if(regResult > 0){
            return ResponseFactory.getResponseObject(ResponseCode.CLAIM_REFUND_CHG_ERR01);
        }

        /* 2.환불수단 변경될 휴대폰 결제정보 조회 */
        ClaimPhonePaymentInfoDto claimPhonePaymentInfo = claimManagementMapper.getClaimPhonePaymentInfo(claimChgPaymentRefundSetDto);

        /* 2-1. 휴대폰 결제유무 확인 */
        if(claimPhonePaymentInfo == null){
            return ResponseFactory.getResponseObject(ResponseCode.CLAIM_REFUND_CHG_ERR02);
        }

        /* 4.환불수단 등록 */
        try{
            claimManagementMapper.setClaimRefund(
            ClaimRefundRegDto.builder()
            .claimNo(claimChgPaymentRefundSetDto.getClaimNo())
            .paymentNo(claimPhonePaymentInfo.getPaymentNo())
            .purchaseOrderNo(claimChgPaymentRefundSetDto.getPurchaseOrderNo())
            .bankAccountName(claimChgPaymentRefundSetDto.getBankAccountName())
            .bankAccountNo(claimChgPaymentRefundSetDto.getBankAccountNo())
            .bankCode(claimChgPaymentRefundSetDto.getBankCode())
            .refundAmt(claimPhonePaymentInfo.getPaymentAmt())
            .refundStatus("F1")
            .regId("MYPAGE")
            .build());
        }catch (Exception e){
            log.debug("[chgPaymentRefund] 클레임 환불정보 등록 실패 : {}", e);
            return ResponseFactory.getResponseObject(ResponseCode.CLAIM_REFUND_CHG_ERR03);
        }


        /* 5.ProcessHistory 등록 */
        try{
            claimProcessService.setClaimProcessHistory(
                ClaimProcessHistoryRegDto.builder()
                    .claimBundleNo(claimChgPaymentRefundSetDto.getClaimBundleNo())
                    .claimNo(claimChgPaymentRefundSetDto.getClaimNo())
                    .purchaseOrderNo(claimChgPaymentRefundSetDto.getPurchaseOrderNo())
                    .historyReason("환불수단 변경")
                    .historyDetailDesc("무통장 입금 " + claimPhonePaymentInfo.getBankName()   + " | " + claimChgPaymentRefundSetDto.getBankAccountNo() + " | " + claimChgPaymentRefundSetDto.getBankAccountName())
                    .historyDetailBefore("휴대폰 결제")
                    .regId("MYPAGE")
                    .regChannel("MYPAGE")
                    .build()
            );
        }catch (Exception e){
            log.debug("ProcessHistory 등록 실패 : {}", e);
        }

        /* return */
        return ResponseFactory.getResponseObject(ResponseCode.CLAIM_REFUND_CHG_SUCCESS);
    }

    public ResponseObject<List<MpOrderGiftItemListDto>> getOrderGiftItemList(long purchaseOrderNo, long bundleNo){
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, claimManagementMapper.getOrderGiftItemList(purchaseOrderNo, bundleNo));
    }

    // 클레임 가능 여부 체크
    private Boolean getClaimPossible(ClaimPreRefundCalcSetDto claimPreRefundCalcSetDto) throws Exception {
        // R | X 요청 일 경우에는 거래가 D3, D4, D5 만 가능
        // C 일 경우에는 D1, D2일 때만 가능.
        // 원주문의 배송상태를 조회
        LinkedHashMap<String, Object> claimPossibleDataMap = claimManagementMapper.getClaimPossibleCheck(Long.parseLong(claimPreRefundCalcSetDto.getPurchaseOrderNo()), Long.parseLong(claimPreRefundCalcSetDto.getBundleNo()));

        // 조회 건수가 없으면 오류.
        if(claimPossibleDataMap.size() == 0){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR08);
        }

        String shipStatus = ObjectUtils.toString(claimPossibleDataMap.getOrDefault("ship_status", "NN"));
        String orderType = ObjectUtils.toString(claimPossibleDataMap.get("order_type"));
        // 클레임 타입
        String claimType = claimPreRefundCalcSetDto.getClaimType();
        String shipMethod = ObjectUtils.toString(claimPossibleDataMap.get("ship_method"));

        if(claimPossibleDataMap.get("slot_close_time") != null && "TD_QUICK,TD_DRCT,TD_PICK".contains(shipMethod) && claimType.equals("C")){
            if(DateUtils.isAfter(ObjectUtils.toString(claimPossibleDataMap.get("slot_close_time")))){
                throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR17);
            }
        }

        // ORD_TICKET 일 경우 별도 체크!
        if("ORD_TICKET".equals(orderType)){
            if(!claimType.equals("C")){
                claimType = "C";
            }
        }
        String claimShipStatus = "";
        switch (claimType) {
            case "C" :
            case "c" :
                //클레임 타입이 C 가 아니면 튕김.
                claimShipStatus = orderType.contains("TICKET") ? "D1,D4,D5" : claimPreRefundCalcSetDto.getOrderCancelYn().equals("Y") ? "D0,D1" : "D2";
                break;
            case "R" :
            case "r" :
            case "X" :
            case "x" :
                claimShipStatus = "D3,D4,D5";
                break;
        }

        return claimShipStatus.contains(shipStatus);
    }


    private Integer getTotalPage(int totalCount, int perPage) {
        int totalPage = totalCount / perPage;
        if(totalCount % perPage > 0){
            totalPage ++;
        }
        return totalPage;
    }



}
