package kr.co.homeplus.claim.api.sample.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import kr.co.homeplus.claim.api.TestConfig;
import kr.co.homeplus.claim.claim.service.ExternalService;
import kr.co.homeplus.claim.claim.sql.ClaimManagementSql;
import kr.co.homeplus.claim.claim.sql.ClaimMessageSql;
import kr.co.homeplus.claim.claim.sql.ClaimOrderCancelSql;
import kr.co.homeplus.claim.enums.DateType;
import kr.co.homeplus.claim.enums.StoreType;
import kr.co.homeplus.claim.event.model.PurchaseMileagePaybackSetDto;
import kr.co.homeplus.claim.event.sql.EventSql;
import kr.co.homeplus.claim.omni.sql.OmniOrderSql;
import kr.co.homeplus.claim.order.model.inquiry.OrderInquirySetDto;
import kr.co.homeplus.claim.order.model.mypage.OrderShipAddrEditDto;
import kr.co.homeplus.claim.order.sql.MypageOrderSearchSql;
import kr.co.homeplus.claim.order.sql.MypageOrderShipSql;
import kr.co.homeplus.claim.order.sql.OrderDataSql;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseMethodReceiptSetDto;
import kr.co.homeplus.claim.receipt.model.ClaimPurchaseReceiptSetDto;
import kr.co.homeplus.claim.receipt.sql.ClaimReceiptSql;
import kr.co.homeplus.claim.review.model.ReviewSearchSetDto;
import kr.co.homeplus.claim.review.sql.ReviewSearchSql;
import kr.co.homeplus.claim.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * SampleController test https://github.com/json-path/JsonPath 참조.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SampleControllerTests.class)
@AutoConfigureMockMvc
@Suite.SuiteClasses({TestConfig.class})
@Slf4j
public class SampleControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ExternalService externalService;

    @Test
    public void test_sample() throws Exception {
//        System.out.println(ClaimManagementSql.selectPaymentCancelInfo(1500009705L));
//        System.out.println(ClaimOrderCancelSql.selectOrderByClaimPartInfo(3000303812L, 3000317043L, 25932544L));
        System.out.println(MypageOrderSearchSql.selectUserGradeUserInfo(12484967L, "2021-03-01", "HYPER"));
//        System.out.println(MypageOrderShipSql.selectMypageOrderItemList(3000011984L, 3000014687L, "NONE"));
//        System.out.println(new Date().before(setCalculationDate("2020.10.19", "MONTH", 3)));
//        System.out.println(OrderDataSql.selectInquiryOrderItemList(3000017338L));
//        OrderShipAddrEditDto setDto = new OrderShipAddrEditDto();
//        setDto.setPurchaseOrderNo(3000015083L);
//        System.out.println(MypageOrderSearchSql.selectShippingPresentStatus(setDto));
//        System.out.println(OmniOrderSql.selectOrderItemInfoExp(3000014408L, 3000017385L));
//        ReviewSearchSetDto setDto = new ReviewSearchSetDto();
//        setDto.setSiteType("HOME");
//        setDto.setUserNo(15584162L);
//        setDto.setPage(1);
//        setDto.setPerPage(10);
//        System.out.println(ReviewSearchSql.selectReviewPossibleList(setDto));
//        System.out.println(ReviewSearchSql.selectReviewPossibleItemList(3000346421L, 3000362743L));
//        System.out.println(DateUtils.getCurrentYmd(DateUtils.DIGIT_YMD));
//        Long aaa = 3000008055112534496L;
//        System.out.println((Long)aaa);
//        System.out.println(MypageOrderSearchSql.selectClaimCouponDiscountInfoList(3000014809L));
//        ClaimPurchaseReceiptSetDto setDto = new ClaimPurchaseReceiptSetDto();
//        setDto.setClaimNo(1500012224L);
//        setDto.setUserNo(12484958L);
//        System.out.println(ClaimReceiptSql.selectClaimPurchaseReceiptPaymentInfo(setDto));
//        System.out.println(OmniOrderSql.selectOriginOrderListByOmni(3000000547L));
//        System.out.println(ClaimOrderCancelSql.selectClaimInfo(1500001599L));
//        System.out.println(ClaimOrderCancelSql.selectClaimPickIsAutoInfo(3000031230L, 3000031938L));
//        System.out.println(MypageOrderSearchSql.selectMpOrderProductList(3000136882L,24230232L, Boolean.FALSE));
    }
    private Date setCalculationDate(String date, String addType, int addValue) {
        Calendar calendar = Calendar.getInstance();
        try {
        date.replaceAll("\\.", "-");

            Date completeDt = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            System.out.println(completeDt.toString());
            calendar.setTime(completeDt);
            if(addType.equals("DAY")){
                calendar.add(Calendar.DATE, addValue);
            } else {
                calendar.add(Calendar.MONTH, addValue);
            }

        } catch (Exception e){
            System.out.println(e.getMessage());
            log.error("배송완료일 시간 계산 중 오류 발생... 현재일 기준으로 표기");
        }
        return calendar.getTime();
    }

    @Test
    public void test_alimTalk() throws Exception {
//        System.out.println("050412341234".replaceFirst("(^[0-9]{4})([0-9]{4})([0-9]{4})$", "$1-$2-$3"));;
//        OrderInquirySetDto setDto = new OrderInquirySetDto();
//        setDto.setPage(1);
//        setDto.setPerPage(10);
//        setDto.setSchStartDt("2021-01-29");
//        setDto.setSchEndDt("2021-01-29");
//        setDto.setUserNo(12484961L);
//        setDto.setSiteType("HOME");
//        System.out.println(OrderDataSql.selectInquiryOrderInfo(setDto));
//        System.out.println(OrderDataSql.selectInquiryOrderItemList(3000016961L));
//        System.out.println(ClaimMessageSql.selectClaimBasicMessageInfo(1500006359L));
        // 64519130 4843420
//        System.out.println(MypageOrderSearchSql.selectOrderTicketDetailItemInfo(3000012738L, 12485028L));
        LocalDate localDate = LocalDate.parse("2020-11-01");
        LocalTime localTime = LocalTime.parse("12:15");
        System.out.println(DateUtils.customDateStr(
            DateUtils.setDateTime(DateUtils.createDateLocalTime("2020-11-01", "12:15"), DateType.MIN, -20),
            "MM월 dd일 (E) HH시 mm분"
        ));
        System.out.println(LocalDate.parse("2021-01-25"));
    }

    @Test
    public void test_date() throws Exception {
        Date dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-12-28".concat(" ").concat("06:30".concat(":00")));
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dateFormat1));
        Calendar ddd = Calendar.getInstance();
        ddd.setTime(dateFormat1);
        ddd.add(Calendar.MINUTE, -20);
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(ddd.getTime()));
        System.out.println(new Date().after(ddd.getTime()));
        long a = 10;
        long b = 0;
        System.out.println(a-b);
    }

    private Integer[] convertIntegerArray(String value, String regex) {
        return Arrays.stream(value.split(regex)).map(Integer::parseInt).toArray(Integer[]::new);
    }

    @Test
    public void test_sampleXss() throws Exception {
        System.out.println("AAA");
        Properties prop = System.getProperties();
        for (String stringSet : prop.stringPropertyNames()) {
            System.out.println(stringSet + " = " + prop.getProperty(stringSet));
        }
//        mockMvc.perform(get("/sampleXss?text=<script></script>"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//            .andExpect(jsonPath("$.data", is("&lt;script&gt;&lt;/script&gt;")));
    }
}

