package kr.co.homeplus.escrowmng.enums;

import kr.co.homeplus.escrowmng.enums.impl.EnumImpl;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum OrderCode implements EnumImpl {
    ORDER_SUCCESS("0000", "성공하였습니다."),
    ORDER_SHIP_ADDR_CHANGE_ERR_01("3001", "배송지 변경에 실패하였습니다."),
    ORDER_SHIP_ADDR_CHANGE_ERR_02("3002", "배송정보 변경가능 시간이 초과되었습니다."),
    ORDER_SHIP_ADDR_CHANGE_ERR_03("3003", "주소지변경 대상이 아닙니다."),
    ORDER_SHIP_ADDR_CHANGE_ERR_04("2004", "안심번호 변경 실패입니다.\n잠시후 재시도 부탁드립니다."),
    ORDER_SHIP_ADDR_CHANGE_ERR_05("2405", "주소지 변경대상이 없습니다.\r관리자에게 문의 부탁드립니다."),
    ORDER_SHIP_ADDR_CHANGE_ERR_06("2406", "30자 이내로 작성해주세요"),

    ORDER_NO_RCV_SHIP_ADD_ERR01("3111", "미수취 등록에 실패하였습니다."),
    ORDER_NO_RCV_SHIP_ADD_ERR02("3112", "미수취 등록 가능한 건이 없습니다."),
    ORDER_NO_RCV_SHIP_ADD_ERR03("3113", "이미 미수취 등록된 건 입니다."),
    ORDER_NO_RCV_SHIP_CHANGE_ERR01("3121", "미수취 처리 중 실패하였습니다."),
    ORDER_NO_RCV_SHIP_CHANGE_ERR02("3122", "미수취 처리요청이 불가합니다.(기처리됨)"),

    ORDER_STORE_SHIP_SET_ERR01("3301", "점포배송상품설명 등록/수정에 실패하였습니다."),

    ORDER_ITEM_STORE_KIND_NOR("NOR", "일반점"),
    ORDER_ITEM_STORE_KIND_DLV("DLV", "택배점"),

    ORDER_ITEM_MALL_TYPE_TD("TD", "매장상품"),
    ORDER_ITEM_MALL_TYPE_DS("DS", "업체상품")


    ;

    private final String code;
    private final String description;

    @Override
    public String getResponseCode() {
        return this.code;
    }

    @Override
    public String getResponseMessage() {
        return this.description;
    }
}
