package kr.co.homeplus.escrowmng.order.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문정보 조회 응답 DTO")
public class OrderSearchGetDto {

    @ApiModelProperty(value = "결제상태", position = 1)
    private String paymentStatus;

    @ApiModelProperty(value = "주문일시", position = 2)
    private String orderDt;

    @ApiModelProperty(value = "결제일시", position = 3)
    private String paymentFshDt;

    @ApiModelProperty(value = "주문번호", position = 4)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "합배송 주문번호", position = 5)
    private String orgPurchaseOrderNo;

    @ApiModelProperty(value = "대체주문여부", position = 6)
    private String substitutionOrderYn;

    @ApiModelProperty(value = "점포유형", position = 7)
    private String storeType;

    @ApiModelProperty(value = "묶음주문번호", position = 8)
    private String bundleNo;

    @ApiModelProperty(value = "상품주문번호", position = 9)
    private String orderItemNo;

    @ApiModelProperty(value = "상품번호", position = 10)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 11)
    private String itemNm1;

    @ApiModelProperty(value = "총상품금액", position = 12)
    private String orderPrice;

    @ApiModelProperty(value = "배송비", position = 13)
    private String shipPrice;

    @ApiModelProperty(value = "도서산간배송비", position = 14)
    private String islandShipPrice;

    @ApiModelProperty(value = "클레임비용", position = 15)
    private String claimPrice;

    @ApiModelProperty(value = "공제비용", position = 16)
    private String deductPrice;

    @ApiModelProperty(value = "할인금액", position = 17)
    private String discountAmt;

    @ApiModelProperty(value = "결제금액", position = 18)
    private String totAmt;

    @ApiModelProperty(value = "판매자명", position = 19)
    private String partnerNm;

    @ApiModelProperty(value = "마켓연동", position = 20)
    private String marketType;

    @ApiModelProperty(value = "마켓주문번호", position = 21)
    private String marketOrderNo;

    @ApiModelProperty(value = "구매자", position = 22)
    private String buyerNm;

    @ApiModelProperty(value = "구매자연락처", position = 23)
    private String buyerMobileNo;

    @ApiModelProperty(value = "수령인", position = 24)
    private String receiverNm;

    @ApiModelProperty(value = "수령인연락처", position = 25)
    private String receiverMobileNo;

    @ApiModelProperty(value = "PG사", position = 26)
    private String pgKind;

    @ApiModelProperty(value = "주결제수단", position = 27)
    private String parentMethodCd;

    @ApiModelProperty(value = "사이트타입", position = 28)
    private String siteType;

    @ApiModelProperty(value = "결제번호", position = 29)
    private String paymentNo;

    @ApiModelProperty(value = "영수증번호", position = 30)
    private String tradeNo;

    public void setTotAmt(String totAmt){
        if(Long.parseLong(totAmt) < 0L){
            this.totAmt = "0";
        } else {
            this.totAmt = totAmt;
        }
    }
}
