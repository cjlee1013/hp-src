package kr.co.homeplus.escrowmng.statistics.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.Data;

@Data
public class ExhibitionSaleStatisticsItemDto {
    @RealGridColumnInfo(headText = "상품번호", sortable = true)
    String itemNo;

    @RealGridColumnInfo(headText = "상품명", sortable = true)
    String itemNm;

    @RealGridColumnInfo(headText = "판매수량", sortable = true)
    long saleCount;

    @RealGridColumnInfo(headText = "취소수량", sortable = true)
    long claimCount;

    @RealGridColumnInfo(headText = "판매금액", sortable = true)
    long saleAmt;

    @RealGridColumnInfo(headText = "취소금액", sortable = true)
    long claimAmt;

    @RealGridColumnInfo(headText = "순판매수량", sortable = true)
    long netSaleCount;

    @RealGridColumnInfo(headText = "순판매금액", sortable = true)
    long netSaleAmt;
}
