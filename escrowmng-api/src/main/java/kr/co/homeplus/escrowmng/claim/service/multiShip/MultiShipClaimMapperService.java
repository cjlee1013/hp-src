package kr.co.homeplus.escrowmng.claim.service.multiShip;

import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimShipInfoDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipClaimItemListGetDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipClaimItemListSetDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipInfoGetDto;

public interface MultiShipClaimMapperService {

    /**
     * 다중배송 배송정보 리스트 조회
     *
     * @param purchaseOrderNo 주문번호, bundleNo 번들번호
     * @return List<MultiShipInfoGetDto>
     */
    List<MultiShipInfoGetDto> getMultiShipList(long purchaseOrderNo, long bundleNo);

    List<MultiShipClaimItemListGetDto> getMultiShipItemList(MultiShipClaimItemListSetDto multiShipClaimItemListSetDto);

    /**
     * 환불 예정 금액 조회
     *
     * @param calcSetDto 환불 예정 금액 조회 파라 미터
     * @return ClaimPreRefundCalcGetDto 환불 예정 금액 정보
     * @throws Exception 오류 처리
     */
    ClaimPreRefundCalcDto getMultiShipClaimPreRefundCalculation(ClaimPreRefundCalcSetDto calcSetDto) throws Exception;


    ClaimShipInfoDto getMultiShipInfo (long bundleNo, long multiBundleNo);

    String getMultiShipDeliveryYn(long multiBundleNo);
}
