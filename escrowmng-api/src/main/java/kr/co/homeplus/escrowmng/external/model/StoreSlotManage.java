package kr.co.homeplus.escrowmng.external.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import kr.co.homeplus.escrowmng.core.valid.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "[옴니연동] 점포 SLOT 관리")
public class StoreSlotManage {

    @ApiModelProperty(value = "점포유형")
    @Pattern(regexp = PatternConstants.STORE_TYPE_PATTERN, message="점포유형(HYPER,AURORA,EXP,CLUB)")
    @NotEmpty(message = "점포유형(HYPER,EXP,CLUB)")
    private String storeType;

    @ApiModelProperty(value = "점포ID", position = 1)
    @NotEmpty(message = "점포ID")
    @AllowInput(types = {PatternConstants.NUM}, message = "점포ID")
    private String storeId;

    @ApiModelProperty(value = "배송일", position = 2)
    @NotEmpty
    @Pattern(regexp = PatternConstants.DATE_DF_FORMAT_PATTERN, message = "배송일 날짜 형식 > yyyy-MM-dd")
    @Date(message = "배송일 날짜 형식 > yyyy-MM-dd")
    private String shipDt;

    @ApiModelProperty(value = "shift ID", position = 3)
    @NotEmpty(message = "shift ID")
    @AllowInput(types = {PatternConstants.NUM}, message = "shift ID")
    private String shiftId;

    @ApiModelProperty(value = "slot ID", position = 4)
    @NotEmpty(message = "slot ID")
    @AllowInput(types = {PatternConstants.NUM}, message = "slot ID")
    private String slotId;

    @ApiModelProperty(value = "slot배송시작시간", position = 5)
    @NotEmpty(message = "slot배송시작시간")
    @Pattern(regexp = PatternConstants.DATETIME_PATTERN_HOUR_MINUTES, message = "slot배송시작시간 HHmm")
    private String slotShipStartTime;

    @ApiModelProperty(value = "slot배송종료시간", position = 6)
    @NotEmpty(message = "slot배송종료시간")
    @Pattern(regexp = PatternConstants.DATETIME_PATTERN_HOUR_MINUTES, message = "slot배송종료시간 HHmm")
    private String slotShipEndTime;

    @ApiModelProperty(value = "slot배송마감시간", position = 7)
    @NotEmpty(message = "slot배송종료시간")
    @Pattern(regexp = PatternConstants.DATETIME_PATTERN_HOUR_MINUTES, message = "slot배송종료시간 HHmm")
    private String slotShipCloseTime;

    @ApiModelProperty(value = "주문고정건수", position = 8)
    private long orderFixCnt;

    @ApiModelProperty(value = "주문변경건수", position = 9)
    private long orderChgCnt;

    @ApiModelProperty(value = "주문건수", position = 10)
    private long orderCnt;

    @ApiModelProperty(value = "픽업주문고정건수", position = 11)
    private long pickupOrderFixCnt;

    @ApiModelProperty(value = "픽업주문변경건수", position = 12)
    private long pickupOrderChgCnt;

    @ApiModelProperty(value = "픽업주문건수", position = 13)
    private long pickupOrderCnt;

    @ApiModelProperty(value = "사물함주문고정건수", position = 14)
    private long lockerOrderFixCnt;

    @ApiModelProperty(value = "사물함주문변경건수", position = 15)
    private long lockerOrderChgCnt;

    @ApiModelProperty(value = "사물함주문변경건수", position = 16)
    private long lockerOrderCnt;

    @ApiModelProperty(value = "예약주문고정건수", position = 17)
    private long reserveOrderFixCnt;

    @ApiModelProperty(value = "예약주문변경건수", position = 18)
    private long reserveOrderChgCnt;

    @ApiModelProperty(value = "slot 마감일", position = 19)
    private int slotCloseDay;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{")
            .append("storeType:").append(storeType).append(",")
            .append("storeId:").append(storeId).append(",")
            .append("shipDt:").append(shipDt).append(",")
            .append("shiftId:").append(shiftId).append(",")
            .append("slotId:").append(slotId).append(",")
            .append("slotShipStartTime:").append(slotShipStartTime).append(",")
            .append("slotShipEndTime:").append(slotShipEndTime).append(",")
            .append("slotShipCloseTime:").append(slotShipCloseTime).append(",")
            .append("orderFixCnt:").append(orderFixCnt).append(",")
            .append("orderChgCnt:").append(orderChgCnt).append(",")
            .append("orderCnt:").append(orderCnt).append(",")
            .append("pickupOrderFixCnt:").append(pickupOrderFixCnt).append(",")
            .append("pickupOrderChgCnt:").append(pickupOrderChgCnt).append(",")
            .append("pickupOrderCnt:").append(pickupOrderCnt).append(",")
            .append("lockerOrderFixCnt:").append(lockerOrderFixCnt).append(",")
            .append("lockerOrderChgCnt:").append(lockerOrderChgCnt).append(",")
            .append("lockerOrderCnt:").append(lockerOrderCnt).append(",")
            .append("reserveOrderFixCnt:").append(reserveOrderFixCnt).append(",")
            .append("reserveOrderChgCnt:").append(reserveOrderChgCnt).append(",")
            .append("slotCloseDay:").append(slotCloseDay)
            .append("}");

        return sb.toString();
    }

}
