package kr.co.homeplus.escrowmng.pg.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.pg.model.PgDivideManageDto;
import kr.co.homeplus.escrowmng.pg.model.PgDivideManageSelectDto;

@EscrowSlaveConnection
public interface PgDivideSlaveMapper {
    List<PgDivideManageDto> selectPgDivideRateList(PgDivideManageSelectDto pgDivideManageSelectDto);
    List<PgDivideManageDto> selectPaymentPolicyPgDivideRateList(PgDivideManageSelectDto pgDivideManageSelectDto);
}
