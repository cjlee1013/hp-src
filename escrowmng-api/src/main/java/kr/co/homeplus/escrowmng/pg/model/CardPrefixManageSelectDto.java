package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "결제관리 > 결제수단 관리 > 카드 PREFIX 관리 조회 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class CardPrefixManageSelectDto {
    @ApiModelProperty(value = "카드프리픽스", position = 1)
    @AllowInput(types = {PatternConstants.NUM}, message = "카드프리픽스")
    private String schCardPrefixNo;
    @ApiModelProperty(value = "결제수단코드", position = 2)
    private String schMethodCd;
    @ApiModelProperty(value = "카드상세종류코드(0:일반,1:법인,2:체크,3:임직원,4:제휴)", position = 3)
    @AllowInput(types = {PatternConstants.NUM}, message = "카드상세종류코드")
    private String schCardDetailKind;
    @ApiModelProperty(value = "카드식별명", position = 4)
    private String schCardIdenNm;
}
