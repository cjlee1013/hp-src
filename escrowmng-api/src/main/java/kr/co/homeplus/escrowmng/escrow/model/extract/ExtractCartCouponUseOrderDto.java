package kr.co.homeplus.escrowmng.escrow.model.extract;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "장바구니쿠폰 사용주문 추출용 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class ExtractCartCouponUseOrderDto {
    @ApiModelProperty(value = "고객번호", position = 1)
    private String userNo;

    @ApiModelProperty(value = "주문번호", position = 2)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "SALE_DATE", position = 3)
    private String orderDt;

    @ApiModelProperty(value = "STORE_CD", position = 4)
    private String originStoreId;

    @ApiModelProperty(value = "POS_NO", position = 5)
    private String posNo;

    @ApiModelProperty(value = "영수증번호", position = 6)
    private String tradeNo;

    @ApiModelProperty(value = "CPON-CD", position = 7)
    private String couponNo;

    @ApiModelProperty(value = "상품코드", position = 8)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 9)
    private String itemNm;

    @ApiModelProperty(value = "상품매출", position = 10)
    private long orderPrice;

    @ApiModelProperty(value = "SALE_QTY", position = 11)
    private long itemQty;

    @ApiModelProperty(value = "면세/과세", position = 12)
    private String taxYn;

    @ApiModelProperty(value = "SECTION_CD", position = 13)
    private String deptNo;

    @ApiModelProperty(value = "CLASS_CD", position = 14)
    private String classNo;

    @ApiModelProperty(value = "SUB_CLASS_CD", position = 15)
    private String subClassNo;

    @ApiModelProperty(value = "쿠폰 Cost1", position = 16)
    private long totDiscountAmt;

    @ApiModelProperty(value = "쿠폰 Cost2", position = 17)
    private long discountAmt;
}
