package kr.co.homeplus.escrowmng.utils;

import java.util.LinkedHashMap;
import javax.annotation.PostConstruct;
import kr.co.homeplus.escrowmng.common.model.processHistory.ProcessHistoryRegDto;
import kr.co.homeplus.escrowmng.common.service.prcessHistory.ProcessHistoryMapperService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class LogUtils {
    private static ProcessHistoryMapperService mapper;
    private final ProcessHistoryMapperService mapperService;

    @PostConstruct
    private void initialize() {
        mapper = mapperService;
    }
    public static void error(String logMsg){
        log.info("error ::: {}", logMsg);
    }
    public static void error(String logPath, Object... objects) {
        log.info(logPath, objects);
    }

    public static void addLogHistory(ProcessHistoryRegDto claimProcessHistoryRegDto) {
        log.info("히스토리 저장 :: {}", mapper.setProcessHistory(claimProcessHistoryRegDto));
    }

    public static void addLogHistory(Object object, String historyReason, String historyDetailDesc, String historyDetailBefore, String regId) throws Exception {

        LinkedHashMap<String, Object> map = ObjectUtils.getConvertMap(object);

        ProcessHistoryRegDto processHistoryRegDto = ProcessHistoryRegDto.builder()
            .historyReason(String.valueOf(historyReason))
            .historyDetailDesc(String.valueOf(historyDetailDesc))
            .regId(String.valueOf(regId))
            .regChannel(String.valueOf("ADMIN"))
            .build();

        if(map.get("purchaseOrderNo") != null){
            processHistoryRegDto.setPurchaseOrderNo(String.valueOf(map.get("purchaseOrderNo")));
        }
        if(map.get("bundleNo") != null){
            processHistoryRegDto.setBundleNo(String.valueOf(map.get("bundleNo")));
        }
        if(map.get("claimBundleNo") != null){
            processHistoryRegDto.setClaimBundleNo(String.valueOf(map.get("claimBundleNo")));
        }
        if(map.get("claimNo") != null){
            processHistoryRegDto.setClaimNo(String.valueOf(map.get("claimNo")));
        }
        if(map.get("shipNo") != null){
            processHistoryRegDto.setShipNo(String.valueOf(map.get("shipNo")));
        }
        if(map.get("claimPickShippingNo") != null){
            processHistoryRegDto.setClaimPickShippingNo(String.valueOf(map.get("claimPickShippingNo")));
        }
        if(map.get("claimExchShippingNo") != null){
            processHistoryRegDto.setClaimExchShippingNo(String.valueOf(map.get("claimExchShippingNo")));
        }
        if(map.get("multiBundleNo") != null){
            processHistoryRegDto.setMultiBundleNo(ObjectUtils.toLong(map.get("multiBundleNo")));
        }
        if(historyDetailBefore != null){
            processHistoryRegDto.setHistoryDetailBefore(historyDetailBefore);
        }
        mapper.setProcessHistory(processHistoryRegDto);
    }

    private static ProcessHistoryRegDto createClaimProcessHistoryRegDto(long purchaseOrderNo, long bundleNo, long claimNo, long claimBundleNo, String historyReason, String historyDetailDesc, String historyDetailBefore, String chgId, String historyChannel) {
        return ProcessHistoryRegDto.builder()
            .claimBundleNo(String.valueOf(claimBundleNo))
            .claimNo(String.valueOf(claimNo))
            .purchaseOrderNo(String.valueOf(purchaseOrderNo))
            .bundleNo(String.valueOf(bundleNo))
            .historyReason(historyReason)
            .historyDetailDesc(historyDetailDesc)
            .historyDetailBefore(historyDetailBefore)
            .regId(chgId)
            .regChannel(historyChannel)
            .build();
    }
}
