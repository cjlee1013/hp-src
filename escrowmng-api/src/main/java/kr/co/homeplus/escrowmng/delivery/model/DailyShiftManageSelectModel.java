package kr.co.homeplus.escrowmng.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@ApiModel(description = "일자별 SHIFT 관리 Select Model")
@Data
public class DailyShiftManageSelectModel {
    @ApiModelProperty(value = "조회유형(점포명,점포코드)", position = 1)
    private String schType;

    @ApiModelProperty(value = "검색어", position = 2)
    private String schKeyword;

    @ApiModelProperty(value = "조회상품유형(상품번호,상품명)", position = 3)
    private String schItemType;

    @ApiModelProperty(value = "상품검색어", position = 4)
    private String schItemKeyword;

    @ApiModelProperty(value = "조회일자유형(주문기간,배송기간)", position = 5)
    private String schDateType;

    @ApiModelProperty(value = "조회시작일시", position = 6)
    @NotEmpty(message = "조회시작일시")
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일시", position = 7)
    @NotEmpty(message = "조회종료일시")
    private String schEndDt;
}
