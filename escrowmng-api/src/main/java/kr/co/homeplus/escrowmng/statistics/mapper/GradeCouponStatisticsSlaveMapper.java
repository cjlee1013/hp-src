package kr.co.homeplus.escrowmng.statistics.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.statistics.model.GradeCouponStatisticsListGetDto;
import kr.co.homeplus.escrowmng.statistics.model.GradeCouponStatisticsListSetDto;

@EscrowSlaveConnection
public interface GradeCouponStatisticsSlaveMapper {
    List<GradeCouponStatisticsListGetDto> getGradeCouponStatisticsList(
        GradeCouponStatisticsListSetDto gradeCouponStatisticsListSetDto);
}
