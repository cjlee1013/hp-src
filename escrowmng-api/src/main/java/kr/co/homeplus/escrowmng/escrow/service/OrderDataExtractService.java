package kr.co.homeplus.escrowmng.escrow.service;

import java.util.List;
import kr.co.homeplus.escrowmng.escrow.mapper.OrderDataExtractSlaveMapper;
import kr.co.homeplus.escrowmng.escrow.model.extract.ExtractAddCouponUseOrderDto;
import kr.co.homeplus.escrowmng.escrow.model.extract.ExtractCartCouponUseOrderDto;
import kr.co.homeplus.escrowmng.escrow.model.extract.ExtractPaymentInfoDto;
import kr.co.homeplus.escrowmng.escrow.model.extract.ExtractPickupOrderDto;
import kr.co.homeplus.escrowmng.escrow.model.extract.OrderDataExtractSelectDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderDataExtractService {
    private final OrderDataExtractSlaveMapper orderDataExtractSlaveMapper;

    /**
     * 장바구니쿠폰(농활쿠폰) 사용주문 리스트 조회
     * @param orderDataExtractSelectDto
     * @return
     */
    public List<ExtractCartCouponUseOrderDto> getCartCouponUseOrderList(OrderDataExtractSelectDto orderDataExtractSelectDto) {
        return orderDataExtractSlaveMapper.selectCartCouponUseOrderList(orderDataExtractSelectDto);
    }

    /**
     * 픽업주문 리스트 조회
     * @param orderDataExtractSelectDto
     * @return
     */
    public List<ExtractPickupOrderDto> getPickupOrderList(OrderDataExtractSelectDto orderDataExtractSelectDto) {
        return orderDataExtractSlaveMapper.selectPickupOrderList(orderDataExtractSelectDto);
    }

    /**
     * 결제수단별 결제정보 리스트 조회
     * @param orderDataExtractSelectDto
     * @return
     */
    public List<ExtractPaymentInfoDto> getPaymentInfoListByMethod(OrderDataExtractSelectDto orderDataExtractSelectDto) {
        return orderDataExtractSlaveMapper.selectPaymentInfoListByMethod(orderDataExtractSelectDto);
    }

    /**
     * 보조결제수단 결제정보 리스트 조회
     * @param orderDataExtractSelectDto
     * @return
     */
    public List<ExtractPaymentInfoDto> getAssistancePaymentInfoList(OrderDataExtractSelectDto orderDataExtractSelectDto) {
        return orderDataExtractSlaveMapper.selectAssistancePaymentInfoList(orderDataExtractSelectDto);
    }

    /**
     * 중복쿠폰 사용주문 리스트 조회
     * @param orderDataExtractSelectDto
     * @return
     */
    public List<ExtractAddCouponUseOrderDto> getAddCouponUseOrderList(OrderDataExtractSelectDto orderDataExtractSelectDto) {
        return orderDataExtractSlaveMapper.selectAddCouponUseOrderList(orderDataExtractSelectDto);
    }
}
