package kr.co.homeplus.escrowmng.claim.model.partner;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.ClaimRequestSetDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimApproveSetDto {

    @ApiModelProperty(notes = "취소 승인 요청 dto")
    private List<ClaimRequestSetDto> claimRequestSetList;

}
