package kr.co.homeplus.escrowmng.escrow.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.escrow.model.ZipcodeManageModel;
import kr.co.homeplus.escrowmng.escrow.model.ZipcodeManageSelectModel;
import kr.co.homeplus.escrowmng.escrow.service.ZipcodeManageService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/escrow/zipcode")
@Api(tags = "시스템관리 > 기준정보관리 > 우편번호관리")
public class ZipcodeManageController {

    private final ZipcodeManageService zipcodeManageService;

    @GetMapping("/getZipcodeSidoList")
    @ApiOperation(value = "도로명 시도명 리스트 조회", response = String.class)
    public ResponseObject<List<String>> getZipcodeSidoList() {
        return ResourceConverter.toResponseObject(zipcodeManageService.getZipcodeSidoList());
    }

    @GetMapping("/getZipcodeSigunguList")
    @ApiOperation(value = "도로명 시군구명 리스트 조회", response = String.class)
    public ResponseObject<List<String>> getZipcodeSigunguList(@ApiParam(name = "sidoNm", value = "시도명", required = true) @RequestParam String sidoNm) {
        return ResourceConverter.toResponseObject(zipcodeManageService.getZipcodeSigunguList(sidoNm));
    }

    @GetMapping("/getZipcodeManageList")
    @ApiOperation(value = "우편번호/도로명/지번 검색", response = ZipcodeManageModel.class)
    public ResponseObject<List<ZipcodeManageModel>> getZipcodeManageList(@Valid ZipcodeManageSelectModel zipcodeManageSelectModel) {
        return ResourceConverter.toResponseObject(zipcodeManageService.getZipcodeManageList(zipcodeManageSelectModel));
    }

    @PostMapping("/updateZipcodeIslandType")
    @ApiOperation(value = "도서산간여부(제주/도서산간/해당없음) 수정")
    public ResponseObject<Integer> updateZipcodeIslandType(@RequestBody ZipcodeManageModel zipcodeManageModel) {
        return ResourceConverter.toResponseObject(zipcodeManageService.updateZipcodeIslandType(zipcodeManageModel));
    }
}

