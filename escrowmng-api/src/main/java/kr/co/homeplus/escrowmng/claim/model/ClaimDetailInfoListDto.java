package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimDetailInfoListDto {

    @ApiModelProperty(value = "처리상태", position = 1)
    private String claimStatus;

    @ApiModelProperty(value = "수거상태", position = 2)
    private String pickStatus;

    @ApiModelProperty(value = "환불상태", position = 3)
    private String refundStatus;

    @ApiModelProperty(value = "배송번호", position = 4)
    private String bundleNo;

    @ApiModelProperty(value = "주문번호", position = 5)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "상품 주문번호", position = 6)
    private String orderItemNo;

    @ApiModelProperty(value = "상품번호", position = 7)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 8)
    private String itemName;

    @ApiModelProperty(value = "옵션명", position = 9)
    private String optItemNm;

    @ApiModelProperty(value = "신청수량", position = 10)
    private String claimQty;

    @ApiModelProperty(value = "상품금액", position = 11)
    private String claimPrice;

    @ApiModelProperty(value="클레임번들번호", position = 13)
    private String claimBundleNo;

    @ApiModelProperty(value = "배송상태",position = 14)
    private String exchStatus;

    @ApiModelProperty(value="파트너ID", position = 15)
    private String partnerId;

    @ApiModelProperty(value="매장타입", position = 16)
    private String storeType;

    @ApiModelProperty(value = "사이트타입", position = 17)
    private String siteType;

}
