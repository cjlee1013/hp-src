package kr.co.homeplus.escrowmng.order.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.order.model.ship.MultiOrderShipAddrEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipSetGto;
import kr.co.homeplus.escrowmng.order.model.ship.OrderShipAddrEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.StoreShipMemoDto;
import kr.co.homeplus.escrowmng.order.sql.OrderShipSql;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.UpdateProvider;

@EscrowMasterConnection
public interface OrderWriteMapper {

    @UpdateProvider(type = OrderShipSql.class, method = "updateShippingAddrEditCheck")
    int updateShippingAddrEditCheck(OrderShipAddrEditDto shipAddrEditDto);

    @InsertProvider(type = OrderShipSql.class, method = "insertNoRcvShip")
    int insertNoRcvShip(NoRcvShipSetGto setGto);

    @UpdateProvider(type = OrderShipSql.class, method = "updateNoRcvShip")
    int updateNoRcvShip(NoRcvShipEditDto editDto);

    @UpdateProvider(type = OrderShipSql.class, method = "updateStoreShipMemo")
    int updateStoreShipMemo(StoreShipMemoDto memoDto);

    /**
     * 선물세트(다중배송) 배송지별 주소지 변경
     *
     * @param shipAddrEditDto
     * @return
     */
    @UpdateProvider(type = OrderShipSql.class, method = "updateMultiOrderShippingAddrInfo")
    int updateMultiOrderShippingAddrInfo(MultiOrderShipAddrEditDto shipAddrEditDto);
}
