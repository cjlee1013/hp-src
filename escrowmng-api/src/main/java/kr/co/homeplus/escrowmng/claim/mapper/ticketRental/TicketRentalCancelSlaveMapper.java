package kr.co.homeplus.escrowmng.claim.mapper.ticketRental;

import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerBoardCountGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListSetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ticketRental.TicketRentalCancelListGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListSetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteSetDto;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import org.apache.ibatis.annotations.Param;

@EscrowSlaveConnection
public interface TicketRentalCancelSlaveMapper {

    ClaimPartnerBoardCountGetDto selectTicketRentalClaimBoardCount(@Param("partnerId") String partnerId, @Param("claimType") String claimType);


    List<TicketRentalCancelListGetDto> selectRentalCancelList(ClaimPartnerListSetDto claimCancelListSetDto);

}
