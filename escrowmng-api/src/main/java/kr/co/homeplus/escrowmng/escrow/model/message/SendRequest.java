package kr.co.homeplus.escrowmng.escrow.model.message;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SendRequest {
    @ApiModelProperty(value = "발송 할 메시지 템플릿 코드", position = 1)
    private String templateCode;

    @ApiModelProperty(value = "발송 캠페인 명", required = true, position = 2)
    private String workName;

    @ApiModelProperty(value = "발송 캠페인 설명", required = true, position = 3)
    private String description;

    @ApiModelProperty(value = "등록자", required = true, position = 4)
    private String regId;

    @ApiModelProperty(value = "개인별 매핑 데이터", position = 5)
    private List<BasicRecipient> bodyArgument;

    @ApiModelProperty(value = "전환발송 될 SMS 템플릿 코드", position = 6)
    private String switchTemplateCode;
}
