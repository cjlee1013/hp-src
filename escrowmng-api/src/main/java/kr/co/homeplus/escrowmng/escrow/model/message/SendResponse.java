package kr.co.homeplus.escrowmng.escrow.model.message;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SendResponse {

    @ApiModelProperty(value = "요청 결과", position = 1)
    private String returnCode;

    @ApiModelProperty(value = "요청 결과 수", position = 2)
    private Integer sendCount;
}
