package kr.co.homeplus.escrowmng.claim.model.entity;

public class ClaimPaymentEntity {
    // 클레임 결제번호(클레임 결제수단별 생성)
    public String claimPaymentNo;
    // 클레임번호(환불:클레임 번호)
    public String claimNo;
    // 결제번호
    public String paymentNo;
    // 주문번호
    public String purchaseOrderNo;
    // PG종류(INICIS,LGU)
    public String pgKind;
    // 환불수단(PG:PG취소, BK:계좌입금,PP:포인트 지급, PT : 포인트 사용취소,MP:MHC포인트,DG:DGV상품권, OC:OCB포인트)
    public String paymentType;
    // 결제수단 상위코드(CARD:카드, VBANK:무통장입금, PHONE:휴대폰결제, RBANK: 실시간계좌이체, EASY: 간편결제,FREE:0원결제)
    public String parentPaymentMethod;
    // 결제수단코드
    public String methodCd;
    // 간편결제수단코드(간편결제시 사용한 결제수단 CARD:카드,RBANK:계좌이체 등)
    public String easyMethodCd;
    // PG 상점ID
    public String pgMid;
    // PG 인증Key값
    public String pgOid;
    // 결제 거래 key (주문시 승인거래 값, PG:PG승인값,MILEAGE:거래번호,MHC:원거래승인일자,OCB:OK캐쉬백 고유거래ID)
    public String paymentTradeId;
    // 결제토큰(TOSS)
    public String payToken;
    // 원거래 승인번호
    public String originApprovalCd;
    // 클레임 승인번호
    public String claimApprovalCd;
    // 결제 상태(P1: 요청, P2: 진행중, P3: 완료, P4:실패)
    public String paymentStatus;
    // 환불 금액
    public String paymentAmt;
    // 대체주문금액
    public String substitutionAmt;
    // 원 환불요청 결제번호(대체환불수단 등록시 사용됨)
    public String originClaimPaymentNo;
    // PG 취소 요청 시도횟수(3회까지 재시도)
    public String pgCancelRequestCnt;
    // 결제 완료일시
    public String payCompleteDt;
    // 등록일
    public String regDt;
    // 등록아이디
    public String regId;
    // 수정일
    public String chgDt;
    // 수정아이디
    public String chgId;
    // 취소거래번호
    public String cancelTradeId;
    // 마지막결제여부
    public String lastCancelYn;
}
