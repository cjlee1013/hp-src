package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClaimPreRefundCalcSetDto {

    @NotNull(message = "주문번호가 없습니다.")
    @ApiModelProperty(value = "주문번호", position = 1)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "상품번호", position = 2)
    private List<ClaimPreRefundItemList> claimItemList;

    @NotNull(message = "귀책사유가 없습니다.")
    @ApiModelProperty(value = "귀책사유(C:고객/S:판매자/H:홈플러스)", position = 3)
    private String whoReason;

    @NotNull(message = "취소타입이 없습니다.")
    @ApiModelProperty(value = "취소타입(O:주문취소/R:반품)", position = 4)
    private String cancelType;

    @ApiModelProperty(value = "부분취소여부(Y:부분취소,N:전체취소)", position = 5)
    private String claimPartYn;

    @ApiModelProperty(value = "행사미차감 여부(Y:미차감,N:차감)", position = 6)
    private String piDeductPromoYn;

    @ApiModelProperty(value = "할인 미차감 여부(Y:미차감,N:차감)", position = 7)
    private String piDeductDiscountYn;

    @ApiModelProperty(value = "배송비 미차감 여부(Y:미차감,N:차감)", position = 8)
    private String piDeductShipYn;

    @ApiModelProperty(value = "동봉 여부(Y:동봉,N:동봉안함)", position = 9)
    private String encloseYn = "N";

    @ApiModelProperty(value = "다중배송번호", position = 10)
    private String multiBundleNo;

    @ApiModelProperty(value = "다중배송여부", position = 11)
    private Boolean isMultiOrder;

    @Data
    public static class ClaimPreRefundItemList {
        @ApiModelProperty(value = "상품번호", position = 1)
        private String itemNo;

        @ApiModelProperty(value = "옵션번호", position = 2)
        private String orderOptNo;

        @ApiModelProperty(value = "클레임수량", position = 3)
        private String claimQty;

        @NotNull(message = "상품주문번호가 없습니다.")
        @ApiModelProperty(value = "상품주문번호", position = 4)
        private String orderItemNo;

        public ClaimPreRefundItemList(){
            this.itemNo = "";
            this.orderOptNo = "";
            this.claimQty = "0";
            this.orderItemNo = "";
        }
    }
}
