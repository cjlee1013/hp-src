package kr.co.homeplus.escrowmng.escrow.service;

import static kr.co.homeplus.escrowmng.constants.ResourceRouteName.MESSAGE;

import java.util.List;
import kr.co.homeplus.escrowmng.enums.ApiName;
import kr.co.homeplus.escrowmng.enums.MessageTemplate;
import kr.co.homeplus.escrowmng.escrow.model.message.BasicRecipient;
import kr.co.homeplus.escrowmng.escrow.model.message.SendRequest;
import kr.co.homeplus.escrowmng.escrow.model.message.SendResponse;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageService {

    private final ResourceClient resourceClient;

    /**
     * sms 발송
     * @param sendRequest
     * @return
     */
    public SendResponse sendSms(SendRequest sendRequest) {
        String apiUri = ApiName.SEND_SMS.getApiUri();

        log.info("[sms 발송]:" + sendRequest);
        ResourceClientRequest<SendResponse> request = ResourceClientRequest.<SendResponse>postBuilder()
            .apiId(MESSAGE)
            .uri(apiUri)
            .postObject(sendRequest)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        SendResponse smsSendResponse = resourceClient.post(request, new TimeoutConfig()).getBody().getData();

        log.info("[sms 발송결과]:" + smsSendResponse);

        return smsSendResponse;
    }

    /**
     * sms 발송
     * @param messageTemplate
     * @param regId
     * @param bodyArgument 메세지 매핑 데이터
     * @return
     */
    public SendResponse sendSms(MessageTemplate messageTemplate, String regId, List<BasicRecipient> bodyArgument) {
        SendRequest sendRequest = new SendRequest();
        sendRequest.setTemplateCode(messageTemplate.getTemplateCode());
        sendRequest.setWorkName(messageTemplate.getWorkName());
        sendRequest.setDescription(messageTemplate.getDescription());
        sendRequest.setRegId(regId);
        sendRequest.setBodyArgument(bodyArgument);

        return this.sendSms(sendRequest);
    }

}
