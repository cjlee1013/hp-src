package kr.co.homeplus.escrowmng.claim.service.impl;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.mapper.refundFail.RefundFailMasterMapper;
import kr.co.homeplus.escrowmng.claim.mapper.refundFail.RefundFailSlaveMapper;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListSetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteSetDto;
import kr.co.homeplus.escrowmng.claim.service.refundFail.RefundFailMapperService;
import kr.co.homeplus.escrowmng.utils.SqlUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class RefundFailMapperServiceImpl implements RefundFailMapperService {

    private final RefundFailSlaveMapper refundFailSlaveMapper;

    private final RefundFailMasterMapper refundFailMasterMapper;

    @Override
    public List<RefundFailListGetDto> getRefundFailList(RefundFailListSetDto refundFailListSetDto) {
        return refundFailSlaveMapper.selectRefundFailList(refundFailListSetDto);
    }

    @Override
    public RequestRefundCompleteGetDto getRefundCompleteResultCount(RequestRefundCompleteSetDto requestRefundCompleteSetDto) {
        return refundFailSlaveMapper.selectRefundCompleteResultCount(requestRefundCompleteSetDto);
    }

    @Override
    public Boolean setReqRefundComplete(RequestRefundCompleteSetDto requestRefundCompleteSetDto) {
        return SqlUtils.isGreaterThanZero(refundFailMasterMapper.updateRefundComplete(requestRefundCompleteSetDto));
    }
}
