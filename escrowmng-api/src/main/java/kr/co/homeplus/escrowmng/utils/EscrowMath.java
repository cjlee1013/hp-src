package kr.co.homeplus.escrowmng.utils;

import java.math.BigDecimal;

public class EscrowMath {

    /**
     * String 문자를 BigDecimal로 변환해준다.
     * @param str
     * @return
     */
    public static BigDecimal stringToBigDecimal(String str) {
        return new BigDecimal(str);
    }

    public static BigDecimal intToBigDecimal(int num) {
        return new BigDecimal(num);
    }

    public static BigDecimal longToBigDecimal(long num) {
        return new BigDecimal(num);
    }


    /**
     * 나누기 처리
     * one / two
     * @param one
     * @param two
     * @return
     */
    public static BigDecimal divide(BigDecimal one, BigDecimal two) {
        return one.divide(two,2,BigDecimal.ROUND_UP);
    }

    /**
     * 곱합기 처리
     * one * two
     * @param one
     * @param two
     * @return
     */
    public static BigDecimal multiply(BigDecimal one, BigDecimal two) {
        return one.multiply(two);
    }

    /**
     * 더하기 처리
     * @param one
     * @param two
     * @return
     */
    public static BigDecimal add(BigDecimal one, BigDecimal two) {
        return one.add(two);
    }

    /**
     * 뺼샘 처리
     * @param one
     * @param two
     * @return
     */
    public static BigDecimal subtract(BigDecimal one, BigDecimal two) {
        return one.subtract(two);
    }

    /**
     * BigDecimal의 소수점 앞 부분만 추출한다.
     *
     * @param bigDecimal Big Decimal
     * @return 소수점 앞 부분
     */
    public static String toBeforeDecimalPointString(BigDecimal bigDecimal) {
        String value = bigDecimal.toPlainString();
        if (!value.contains(".")) {
            return bigDecimal.toPlainString();
        }
        String[] strings = value.split("\\.");
        return strings[0];
    }

    /**
     * BigDecimal의 소수점 앞 부분만 추출한다.
     *
     * @param bigDecimal Big Decimal
     * @return 소수점 앞 부분
     */
    public static String toAfterDecimalPointString(BigDecimal bigDecimal) {
        String value = bigDecimal.toPlainString();
        if (!value.contains(".")) {
            return bigDecimal.toPlainString();
        }
        String[] strings = value.split("\\.");
        return strings[1];
    }
}