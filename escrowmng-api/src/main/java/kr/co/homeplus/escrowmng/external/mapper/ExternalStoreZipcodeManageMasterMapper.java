package kr.co.homeplus.escrowmng.external.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.external.model.StoreZipcodeManage;
import org.apache.ibatis.annotations.Param;

@EscrowMasterConnection
public interface ExternalStoreZipcodeManageMasterMapper {

    int insertStoreZipcodeMngList(
        @Param("storeZipcodeManageList") List<StoreZipcodeManage> storeZipcodeManageList,
        @Param("regId") String regId,
        @Param("chgId") String chgId
    );

    int insertStoreZipcodeMngTempList(
        @Param("storeZipcodeManageList") List<StoreZipcodeManage> storeZipcodeManageList);

    List<StoreZipcodeManage> selectStoreZipcodeMngTempList(@Param("apiFulltextSeq") long apiFulltextSeq);

    int deleteStoreZipcodeMngTempNoStoreId(@Param("storeIdList") List<Integer> storeIdList, @Param("apiFulltextSeq") long apiFulltextSeq);

    List<Integer> selectNotUseStoreIdList(@Param("apiFulltextSeq") long apiFulltextSeq);

    int deleteStoreZipcodeMngTemp(@Param("apiFulltextSeq") long apiFulltextSeq);

}
