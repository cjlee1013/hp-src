package kr.co.homeplus.escrowmng.order.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문정보 상세 - 결제정보 DTO")
public class OrderSearchPaymentDto {
    @ApiModelProperty(value = "구분", hidden = true)
    private String payType;
    @ApiModelProperty(value = "구분")
    private String paymentCategory;
    @ApiModelProperty(value = "처리수단")
    private String paymentType;
    @ApiModelProperty(value = "상세내역")
    private String paymentDetail;
    @ApiModelProperty(value = "번호")
    private String cardNo;
    @ApiModelProperty(value = "금액")
    private int amt;
    @ApiModelProperty(value = "연동 주문번호")
    private String pgOid;
    @ApiModelProperty(value = "PG사")
    private String pgKind;
    @ApiModelProperty(value = "상점ID")
    private String pgMid;
    @ApiModelProperty(value = "t_id")
    private String pgTid;
    @ApiModelProperty(value = "IP")
    private String reqIp;
    @ApiModelProperty(value = "처리일시")
    private String paymentFshDt;
    @ApiModelProperty(value = "비고")
    private String explanatoryNote;

    private void setAmt(int amt){
        if(this.payType.equals("CLAIM")){
            this.amt = (amt * -1);
        } else {
            this.amt = amt;
        }
    }
}
