package kr.co.homeplus.escrowmng.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "일자별 SHIFT 관리 Model")
@Getter
@Setter
@EqualsAndHashCode
public class DailyShiftManageModel {
    @ApiModelProperty(value = "상품번호", position = 1)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 2)
    private String itemNm;

    @ApiModelProperty(value = "점포유형", position = 3)
    private String storeType;

    @ApiModelProperty(value = "주문기간", position = 4)
    private String orderPeriod;

    @ApiModelProperty(value = "SHIFT배송기간", position = 5)
    private String shiftShipPeriod;

    @ApiModelProperty(value = "수정일시", position = 6)
    private String chgDt;

    @ApiModelProperty(value = "수정자ID", position = 7)
    private String chgId;

    @ApiModelProperty(value = "등록일시", position = 8)
    private String regDt;

    @ApiModelProperty(value = "등록자ID", position = 9)
    private String regId;

    @ApiModelProperty(value = "주문시작일시", position = 10)
    private String orderStartDt;

    @ApiModelProperty(value = "주문종료일시", position = 11)
    private String orderEndDt;

    @ApiModelProperty(value = "SHIFT배송시작일시", position = 12)
    private String shiftShipStartDt;

    @ApiModelProperty(value = "SHIFT배송종료일시", position = 13)
    private String shiftShipEndDt;

    @ApiModelProperty(value = "사용여부", position = 14)
    private String useYn;

    @ApiModelProperty(value = "일자별SHIFT관리순번", position = 15)
    private Long dailyShiftMngSeq;
}