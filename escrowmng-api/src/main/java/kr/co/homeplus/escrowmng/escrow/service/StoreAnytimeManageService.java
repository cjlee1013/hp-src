package kr.co.homeplus.escrowmng.escrow.service;

import java.util.List;
import kr.co.homeplus.escrowmng.escrow.mapper.StoreAnytimeMasterMapper;
import kr.co.homeplus.escrowmng.escrow.mapper.StoreAnytimeSlaveMapper;
import kr.co.homeplus.escrowmng.escrow.model.StoreAnytimeManageDto;
import kr.co.homeplus.escrowmng.escrow.model.StoreAnytimeManageSelectDto;
import kr.co.homeplus.escrowmng.escrow.model.StoreAnytimeShiftManageDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StoreAnytimeManageService {
    private final StoreAnytimeSlaveMapper storeAnytimeSlaveMapper;
    private final StoreAnytimeMasterMapper storeAnytimeMasterMapper;

    /**
     * 점포 애니타임 정보 조회
     * @param storeAnytimeManageSelectDto
     * @return
     */
    public List<StoreAnytimeManageDto> getStoreAnytimeInfo(StoreAnytimeManageSelectDto storeAnytimeManageSelectDto) {
        return storeAnytimeSlaveMapper.selectStoreAnytimeInfo(storeAnytimeManageSelectDto);
    }

    /**
     * 점포 애니타임 Shift 정보 조회
     * @param storeId
     * @return
     */
    public List<StoreAnytimeShiftManageDto> getStoreAnytimeShiftInfo(int storeId) {
        return storeAnytimeSlaveMapper.selectStoreAnytimeShiftInfo(storeId);
    }

    /**
     * 점포 애니타임 사용여부 저장
     * @param storeAnytimeManageDto
     */
    public int saveStoreAnytimeUseYn(StoreAnytimeManageDto storeAnytimeManageDto) {
        return storeAnytimeMasterMapper.insertStoreAnytimeUseYn(storeAnytimeManageDto);
    }

    /**
     * 점포 애니타임 Shift 사용여부 저장
     * @param storeAnytimeShiftManageDtoList
     */
    public int saveStoreAnytimeShiftUseYn(List<StoreAnytimeShiftManageDto> storeAnytimeShiftManageDtoList) {
        return storeAnytimeMasterMapper.insertStoreAnytimeShiftUseYn(storeAnytimeShiftManageDtoList);
    }
}
