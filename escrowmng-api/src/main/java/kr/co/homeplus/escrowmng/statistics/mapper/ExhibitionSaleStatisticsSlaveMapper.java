package kr.co.homeplus.escrowmng.statistics.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowBatchConnection;
import kr.co.homeplus.escrowmng.statistics.model.ExhibitionSaleStatisticsExhDto;
import kr.co.homeplus.escrowmng.statistics.model.ExhibitionSaleStatisticsItemDto;
import kr.co.homeplus.escrowmng.statistics.model.ExhibitionSaleStatisticsSelectDto;

@EscrowBatchConnection
public interface ExhibitionSaleStatisticsSlaveMapper {
    List<ExhibitionSaleStatisticsExhDto> selectExhibitionSaleStatisticsExh(ExhibitionSaleStatisticsSelectDto exhibitionSaleStatisticsSelectDto);
    List<ExhibitionSaleStatisticsItemDto> selectExhibitionSaleStatisticsItem(ExhibitionSaleStatisticsSelectDto exhibitionSaleStatisticsSelectDto);
}
