package kr.co.homeplus.escrowmng.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(description = "대체공제 쿠폰 통계 리스트 조회 DTO")
public class OrderSubstitutionStatisticsGetDto {
    @ApiModelProperty(value = "점포유형", position = 1)
    private String storeType;
    @ApiModelProperty(value = "점포명", position = 2)
    private String storeNm;
    @ApiModelProperty(value = "매출금액", position = 3)
    private long salesPrice;
    @ApiModelProperty(value = "매출상품수", position = 4)
    private long salesCnt;
    @ApiModelProperty(value = "대체수락", position = 5)
    private long totSubstitutionCnt;
    @ApiModelProperty(value = "대체수락중결품", position = 6)
    private long totSubstitutionOosCnt;
    @ApiModelProperty(value = "대체수락완료", position = 7)
    private long totCompleteSubstitutionCnt;
    @ApiModelProperty(value = "대체수락완료된건의원매출", position = 8)
    private long totOriginSalesPrice;
    @ApiModelProperty(value = "쿠폰발생금액", position = 9)
    private long totCouponPrice;
    @ApiModelProperty(value = "상점아이디", position = 10)
    private String originStoreId;
}
