package kr.co.homeplus.escrowmng.claim.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.escrowmng.claim.mapper.ClaimReadMapper;
import kr.co.homeplus.escrowmng.claim.mapper.ClaimWriteMapper;
import kr.co.homeplus.escrowmng.claim.model.ClaimBundleInfoList;
import kr.co.homeplus.escrowmng.claim.model.ClaimDetailInfoGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimDetailInfoListDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimDetailInfoSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimExchShippingDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimExchShippingModifyDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimGiftItemListDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimHistoryListDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimInfoListGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimInfoListSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimInfoRegDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPaymentProcessGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPickShippingDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPickShippingModifyDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.escrowmng.claim.model.ClaimPrevOrderCancelDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimProcessGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimReasonInfoGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimReasonInfoGetDto.RefundTypeList;
import kr.co.homeplus.escrowmng.claim.model.ClaimRegisterPreRefundInfoDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimShipInfoDto;
import kr.co.homeplus.escrowmng.claim.model.DiscountInfoListDto;
import kr.co.homeplus.escrowmng.claim.model.PromoInfoListDto;
import kr.co.homeplus.escrowmng.claim.model.StoreInfoGetDto;
import kr.co.homeplus.escrowmng.claim.model.external.ClaimCouponReIssueSetDto.CouponReIssueDto;
import kr.co.homeplus.escrowmng.claim.model.external.safety.ClaimSafetySetDto;
import kr.co.homeplus.escrowmng.claim.model.market.MarketClaimBasicDataDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimBundleRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimExchShippingRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimItemRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimMultiBundleRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimMultiItemRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimPaymentRegDto;
import kr.co.homeplus.escrowmng.common.model.processHistory.ProcessHistoryRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimReqRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimShippingRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ticket.ClaimTicketModifyDto;
import kr.co.homeplus.escrowmng.claim.model.register.ticket.ClaimTicketRegDto;
import kr.co.homeplus.escrowmng.claim.service.ClaimMapperService;
import kr.co.homeplus.escrowmng.enums.ClaimCode;
import kr.co.homeplus.escrowmng.order.mapper.OrderReadMapper;
import kr.co.homeplus.escrowmng.order.model.management.OrderGiftItemListDto;
import kr.co.homeplus.escrowmng.utils.ObjectUtils;
import kr.co.homeplus.escrowmng.utils.SqlUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClaimMapperServiceImpl implements ClaimMapperService {

    private final ClaimReadMapper readMapper;
    private final ClaimWriteMapper writeMapper;
    private final OrderReadMapper orderReadMapper;


    @Override
    public List<ClaimPrevOrderCancelDto> getClaimPrevOrderCancelInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectPrevClaimOrderCancelCheck(purchaseOrderNo, bundleNo);
    }

    @Override
    public ClaimPreRefundCalcDto getClaimPreRefundCalculation(ClaimPreRefundCalcSetDto calcSetDto) throws Exception {
        // 다중 클레임 건 여부 확인
        boolean isMulti = Boolean.FALSE;
//        int orderItemNoCnt = (int) calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderItemNo).distinct().count();
        // isMulti Check
//        if(orderItemNoCnt > 1){
        if(calcSetDto.getClaimItemList().size() > 1) {
            isMulti = Boolean.TRUE;
        }
        // 파라미터 용 맵 선언.
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        // 주문번호
        parameterMap.put("purchaseOrderNo", calcSetDto.getPurchaseOrderNo());
        // 상품주문번호
        parameterMap.put("orderItemNo", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderItemNo).collect(Collectors.joining(":")));
        // 상품번호
        parameterMap.put("itemNo", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getItemNo).collect(Collectors.joining(":")));
        // 옵션번호
        parameterMap.put("orderOptNo", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderOptNo).collect(Collectors.joining(":")));
        // 클레임 수량
        parameterMap.put("claimQty", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getClaimQty).collect(Collectors.joining(":")));
        // 귀책사유
        parameterMap.put("whoReason", calcSetDto.getWhoReason());
        // 취소타입
        parameterMap.put("cancelType", calcSetDto.getCancelType());

        //단건 클레임만 결품 여부 값 추가
        if(!isMulti && !calcSetDto.getIsMultiOrder()){
            //결품여부
            parameterMap.put("piOosYn", "N");
        }

        //행사미차감 여부
        parameterMap.put("piDeductPromoYn", calcSetDto.getPiDeductPromoYn());
        //할인 미차감 여부
        parameterMap.put("piDeductDiscountYn", calcSetDto.getPiDeductDiscountYn());
        //배송비 미차감 여부
        parameterMap.put("piDeductShipYn", calcSetDto.getPiDeductShipYn());
        // 동봉 여부
        parameterMap.put("encloseYn", calcSetDto.getEncloseYn());

        if(isMulti && !calcSetDto.getIsMultiOrder()){
            log.debug("Claim Pre Refund Calculation is Multi Action....");
            return readMapper.callByClaimPreRefundPriceMultiCalculation(parameterMap);
        } else if(calcSetDto.getIsMultiOrder()) {
            if(calcSetDto.getMultiBundleNo() == null) {
                parameterMap.put("piMultiBundleNo", "0");
            } else {
                parameterMap.put("piMultiBundleNo", calcSetDto.getMultiBundleNo());
            }
            return readMapper.callByClaimMultiPreRefundPriceCalculation(parameterMap);
        }
        return readMapper.callByClaimPreRefundPriceCalculation(parameterMap);
    }

    /**
     * 클레임 마스터 등록을 위한 주문 원장 조회
     *
     * @param purchaseOrderNo 주문 번호
     * @return ClaimMstRegDto 클레임 마스터 등록 DTO
     */
    @Override
    public ClaimMstRegDto getClaimRegPurchaseOrderInfo(long purchaseOrderNo) {
        return readMapper.selectPurchaseOrderInfo(purchaseOrderNo);
    }

    /**
     * 클레임 번들 생성을 위한 데이터 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return HashMap<String, Object> 클레임번들 기본정보
     */
    @Override
    public LinkedHashMap<String, Object> getCreateClaimBundleInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectCreateClaimDataInfo(purchaseOrderNo, bundleNo);
    }

    /**
     * 클레임 아이템 생성을 위한 데이터 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param orderItemNo 상품주문번호
     * @return HashMap<String, Object> 클레임 아이템 기본정보
     */
    @Override
    public LinkedHashMap<String, Object> getClaimItemInfo(long purchaseOrderNo, long orderItemNo) {
        return readMapper.selectClaimItemInfo(purchaseOrderNo, orderItemNo);
    }

    /**
     * 클레임 리스트 조회
     *
     * @param claimInfoListSetDto 클레임 조회 dto
     * @return ClaimInfoListGetDto dto 리스트
     */
    @Override
    public List<ClaimInfoListGetDto> getClaimInfoList(ClaimInfoListSetDto claimInfoListSetDto) {
        return readMapper.selectClaimList(claimInfoListSetDto);
    }

    /**
     * 클레임 상세정보 조회
     *
     * @param claimDetailInfoSetDto 클레임 상세정보 조회 dto
     * @return ClaimDetailInfoGetDto dto
     */
    @Override
    public ClaimDetailInfoGetDto getClaimDetailInfo(ClaimDetailInfoSetDto claimDetailInfoSetDto) {
        return readMapper.selectClaimDetailInfo(claimDetailInfoSetDto);
    }

    /**
     * 클레임 주문정보 리스트 조회
     *
     * @param claimDetailInfoSetDto 클레임 상세정보 조회 dto
     * @return ClaimDetailInfoGetDto dto
     */
    @Override
    public List<ClaimDetailInfoListDto> getClaimDetailInfoList(ClaimDetailInfoSetDto claimDetailInfoSetDto) {
        return readMapper.selectClaimDetailInfoList(claimDetailInfoSetDto);
    }

    @Override
    public List<ClaimBundleInfoList> getClaimBundleInfoList(long claimNo) {
        return readMapper.selectClaimBundleInfo(claimNo);
    }

    @Override
    public String getShipStatusForBundleNo(long bundleNo) {
        return readMapper.selectShipStatus(bundleNo);
    }

    @Override
    public ClaimShipInfoDto getClaimShipInfo(long bundleNo) {
        return readMapper.selectClaimShipInfo(bundleNo);
    }

    /**
     * 클레임 취소 상세사유 조회
     *
     * @param claimNo
     * @return ClaimReasonInfoGetDto dto
     */
    @Override
    public ClaimReasonInfoGetDto getCancelReasonInfo(long claimNo) {
        return readMapper.selectCancelReasonInfo(claimNo);
    }

    /**
     * 클레임 반품 상세사유 조회
     *
     * @param claimNo
     * @return ClaimReasonInfoGetDto dto
     */
    @Override
    public ClaimReasonInfoGetDto getReturnExchangeReasonInfo(long claimNo) {
        return readMapper.selectReturnExchangeReasonInfo(claimNo);
    }

    /**
     * 클레임 반품배송정보 조회
     *
     * @param claimBundleNo
     * @return ClaimPickShippingDto dto
     */
    @Override
    public ClaimPickShippingDto getClaimPickShippingInfo(long claimBundleNo) {
        return readMapper.selectClaimPickShippingInfo(claimBundleNo);
    }

    /**
     * 클레임 교환배송정보 조회
     *
     * @param claimBundleNo
     * @return ClaimExchShippingDto dto
     */
    @Override
    public ClaimExchShippingDto getClaimExchShippingInfo(long claimBundleNo) {
        return readMapper.selectClaimExchShippingInfo(claimBundleNo);
    }

    /**
     * 클레임 교환 상세사유 조회
     *
     * @param claimNo
     * @return ClaimReasonInfoGetDto dto
     */
    @Override
    public ClaimRegisterPreRefundInfoDto getRegisterPreRefund(long claimNo) {
        return readMapper.selectRegisterPreRefundAmt(claimNo);
    }


    /**
     * 환불수단 조회
     *
     * **/
    @Override
    public List<RefundTypeList> getRefundTypeList(long claimNo) {
        return readMapper.selectRefundTypeList(claimNo);
    }

    /**
     * 차감 할인정보 조회
     *
     * **/
    @Override
    public List<DiscountInfoListDto> getDiscountInfoList(long claimNo) {
        return readMapper.selectDiscountInfoList(claimNo);
    }

    /**
     * 클레임 마스터 등록
     *
     * @param claimMstRegDto 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimMstInfo(ClaimMstRegDto claimMstRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimMst(claimMstRegDto));
    }


    /**
     * 클레임 번들 등록
     *
     * @param claimBundleRegDto 클레임 번들 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimBundleInfo(ClaimBundleRegDto claimBundleRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimBundle(claimBundleRegDto));
    }

    /**
     * 클레임 다중배송 번들 등록
     *
     * @param claimMultiBundleRegDto 클레임 번들 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimMultiBundleInfo(ClaimMultiBundleRegDto claimMultiBundleRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimMultiBundle(claimMultiBundleRegDto));
    }

    /**
     * 클레임 요청 등록
     *
     * @param claimReqRegDto 클레임 요청 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimReqInfo(ClaimReqRegDto claimReqRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimReq(claimReqRegDto));
    }

    /**
     * 클레임 아이템 정보 등록
     *
     * @param claimItemRegDto 클레임 아이템 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimItemInfo(ClaimItemRegDto claimItemRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimItemReq(claimItemRegDto));
    }

    @Override
    public Boolean addClaimMultiItemInfo(ClaimMultiItemRegDto claimMultiItemRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimMultiItem(claimMultiItemRegDto));
    }

    /**
     * 클레임 결제 취소를 위한 원주문 결제 정보 조회
     *
     * @param paymentNo 결제 번호
     * @param paymentType 결제 타입
     * @return 클레임 결제 취소를 위한 원주문 결제 정보 맵
     */
    @Override
    public LinkedHashMap<String, Object> getOriPaymentInfoForClaim(long paymentNo, String paymentType){
        if(paymentType.equals("FREE")){
            paymentType = null;
        }
        return readMapper.selectOriPaymentForClaim(paymentNo, paymentType);
    }

    /**
     * 클레임 결제 취소 정보 등록
     *
     * @param claimPaymentRegDto 클레임 결제 취소 정보
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimPaymentInfo(ClaimPaymentRegDto claimPaymentRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimPayment(claimPaymentRegDto));
    }

    /**
     * 클레임 아이템 옵션별 등록을 위한 데이터 조회
     *
     * @param orderItemNo 아이템주문번호
     * @param orderOptNo 옵션번호
     * @return 클레임 옵션 등록 정보
     */
    @Override
    public ClaimOptRegDto getOriginOrderOptInfoForClaim(long orderItemNo, String orderOptNo){
        return readMapper.selectOrderOptInfoForClaim(orderItemNo, orderOptNo);
    }

    /**
     * 클레임 옵션별 등록
     *
     * @param claimOptRegDto 클레임 옵션별 데이터 DTO
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimOptInfo(ClaimOptRegDto claimOptRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimOpt(claimOptRegDto));
    }

    /**
     * 클레임 - 결제취소를 위한 데이터 조회
     *
     * @param claimNo 클레임 번호
     * @return LinkedHashMap<String, Object> 결제취소용 데이터
     */
    @Override
    public List<LinkedHashMap<String, Object>> getPaymentCancelInfo(long claimNo) {
        return writeMapper.selectPaymentCancelInfo(claimNo);
    }

    /**
     * 클레임 - 클레임상태 변경 업데이트
     *
     *
     * @param parameter@return Boolean 클레임상태변경 업데이트 결과
     */
    @Override
    public Boolean setClaimStatus(LinkedHashMap<String, Object> parameter) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimStatus(parameter));
    }

    /**
     * 클레임 - 클레임 결제 취소 상태 업데이트
     *
     * @param claimPaymentNo 클레임 페이먼트 번호
     * @param claimCode 클레임 코드
     * @return Boolean 클레임 결제취소상태 업데이트 결과
     */
    @Override
    public Boolean setClaimPaymentStatus(long claimPaymentNo, String claimApprovalCd, ClaimCode claimCode) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimPaymentStatus(claimPaymentNo, claimApprovalCd, claimCode));
    }

    @Override
    public void callByEndClaimFlag(ClaimInfoRegDto claimInfoRegDto) {
        writeMapper.callByClaimReg(claimInfoRegDto);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getOrderBundleByClaim(long purchaseOrderNo) {
        return readMapper.selectOrderBundleByClaim(purchaseOrderNo);
    }

    @Override
    public List<ClaimPreRefundItemList> getClaimPreRefundItemInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectOrderItemInfoByClaim(purchaseOrderNo, bundleNo);
    }

    @Override
    public List<ClaimPreRefundItemList> getClaimPreRefundTicketItemInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectOrderItemInfoByTicketClaim(purchaseOrderNo, bundleNo);
    }

    @Override
    public LinkedHashMap<String, Object> getOrderByClaimPartInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectOrderByClaimPartInfo(purchaseOrderNo, bundleNo);
    }


    /**
     * 클레임 처리내역/히스토리 등록
     *
     * @param  processHistoryRegDto 클레임 처리내역/히스토리 등록 dto
     * @return Boolean 클레임 히스토리 등록 결과
     */
    @Override
    public Boolean setClaimProcessHistory(ProcessHistoryRegDto processHistoryRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimProcessHistory(
            processHistoryRegDto));
    }

    @Override
    public Boolean addClaimPickShippingInfo(ClaimShippingRegDto shippingRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimPickShipping(shippingRegDto));
    }

    @Override
    public Boolean addClaimExchShipping(ClaimExchShippingRegDto exchShippingRegDto) {
        log.debug("addClaimExchShipping ::: {}", exchShippingRegDto);
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimExchShipping(exchShippingRegDto));
    }

    @Override
    public Boolean setClaimPickShipping(ClaimPickShippingModifyDto claimPickShippingModifyDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimPickShipping(claimPickShippingModifyDto));
    }

    @Override
    public Boolean setClaimExchShipping(ClaimExchShippingModifyDto claimEchShippingModifyDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimExchShipping(claimEchShippingModifyDto));
    }

    @Override
    public void callByClaimAdditionReg(long claimNo) {
        LinkedHashMap<String, Object> returnMap = writeMapper.callByClaimAdditionReg(claimNo);
        log.debug("callByClaimAdditionReg ::: {}", returnMap);
    }

    @Override
    public void callByClaimItemEmpDiscount(long claimNo, long orderItemNo) {
        writeMapper.callByClaimItemEmpDiscount(claimNo, orderItemNo);
    }

    @Override
    public LinkedHashMap<String, Object> getCreateClaimMstInfo(long purchaseOrderNo) {
        return readMapper.selectCreateClaimMstDataInfo(purchaseOrderNo);
    }

    @Override
    public LinkedHashMap<String, Object> getClaimPossibleCheck(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectClaimPossibleCheck(purchaseOrderNo, bundleNo);
    }

    @Override
    public LinkedHashMap<String, Object> getMultiOrderClaimPossibleCheck(long purchaseOrderNo, long multiBundleNo) {
        return readMapper.selectMultiOrderClaimPossibleCheck(purchaseOrderNo, multiBundleNo);
    }

    @Override
    public List<CouponReIssueDto> getClaimCouponReIssueInfo(long claimNo, long userNo) {
        return readMapper.selectClaimCouponReIssue(claimNo, userNo);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getClaimInfo(long claimNo) {
        return readMapper.selectClaimInfo(claimNo);
    }

    @Override
    public LinkedHashMap<String, Object> getClaimPickIsAutoInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectClaimPickIsAutoInfo(purchaseOrderNo, bundleNo);
    }

    @Override
    public Boolean modifyClaimCouponReIssueResult(List<Long> claimAdditionNo) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimCouponReIssueResult(claimAdditionNo));
    }

    @Override
    public Boolean modifyClaimBundleSlotResult(long claimBundleNo){
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimBundleSlotResult(claimBundleNo));
    }

    @Override
    public LinkedHashMap<String, Object> getClaimSendDataInfo(long claimNo){
        return writeMapper.selectClaimSendDataInfo(claimNo);
    }

    @Override
    public ClaimSafetySetDto getClaimSafetyData(long claimBundleNo, String issueType) {
        return writeMapper.selectClaimSafetyIssueData(claimBundleNo, issueType);
    }

    @Override
    public ClaimInfoRegDto getClaimRegData(long claimNo) {
        return readMapper.selectClaimRegDataInfo(claimNo);
    }

    @Override
    public Boolean modifyClaimPaymentOcbTransactionId(long claimPaymentNo, String pgMid) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimPaymentOcbTransactionId(claimPaymentNo, pgMid));
    }

    @Override
    public Boolean modifyClaimPaymentPgCancelTradeId(long claimPaymentNo, String cancelTradeId) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimPaymentPgCancelTradeId(claimPaymentNo, cancelTradeId));
    }

    @Override
    public List<OrderGiftItemListDto> getOrderGiftItemList(long purchaseOrderNo, long bundleNo) {
        return orderReadMapper.selectOrderGiftItemList(purchaseOrderNo, bundleNo);
    }

    @Override
    public List<ClaimGiftItemListDto> getClaimGiftItemList(long claimNo) {
        return readMapper.selectClaimGiftList(claimNo);
    }

    @Override
    public List<PromoInfoListDto> getPromoInfoList(long claimNo) {
        return readMapper.selectPromoInfoList(claimNo);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getOrderTicketInfo(long purchaseOrderNo, long orderItemNo, int cancelCnt) {
        return readMapper.selectOrderTicketInfo(purchaseOrderNo, orderItemNo, cancelCnt);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getOrderTicketInfoForOrderTicketList(
        long purchaseOrderNo, long orderItemNo, ArrayList<Long> orderTicketList) {
        return readMapper.selectOrderTicketInfoForOrderTicketList(purchaseOrderNo, orderItemNo, orderTicketList);
    }

    @Override
    public Boolean addClaimTicketInfo(ClaimTicketRegDto claimTicketRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimTicket(claimTicketRegDto));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getClaimTicketInfo(long claimNo) {
        return readMapper.selectClaimTicketInfo(claimNo);
    }

    @Override
    public LinkedHashMap<String, String> getClaimOrderTypeCheck(long claimNo) {
        return readMapper.selectClaimOrderTypeCheck(claimNo);
    }

    @Override
    public Boolean modifyOrderTicketInfo(ClaimTicketModifyDto modifyDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateOrderTicketStatus(modifyDto));
    }

    @Override
    public LinkedHashMap<String, String> getSlotSiftInfo(String slotId, String shiftId, String storeType, String storeId) {
        return readMapper.selectSlotSiftInfo(slotId, shiftId, storeType, storeId);
    }

    @Override
    public Boolean setTotalClaimPaymentStatus(long claimNo, ClaimCode claimCode){
        return SqlUtils.isGreaterThanZero(writeMapper.updateTotalClaimPaymentStatus(claimNo, claimCode));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getCreateClaimAccumulateInfo(long claimNo){
        return readMapper.selectCreateClaimAccumulateInfo(claimNo);
    }

    @Override
    public LinkedHashMap<String, Object> getAccumulateCancelInfo(long purchaseOrderNo, String pointKind){
        return readMapper.selectAccumulateCancelInfo(purchaseOrderNo, pointKind);
    }

    @Override
    public Boolean addClaimAccumulate(LinkedHashMap<String, Object> parameterMap) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimAccumulate(parameterMap));
    }

    @Override
    public Boolean modifyClaimAccumulateReqSend(long claimNo, String reqSendYn, String pointKind){
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimAccumulateReqSend(claimNo, reqSendYn, pointKind));
    }

    @Override
    public Boolean modifyClaimAccumulateReqResult(LinkedHashMap<String, Object> parameterMap) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimAccumulate(parameterMap));
    }

    @Override
    public ClaimProcessGetDto getClaimProcess(long claimNo) {
        return readMapper.selectClaimProcess(claimNo);
    }

    @Override
    public List<ClaimPaymentProcessGetDto> getClaimPaymentProcess(long claimNo) {
        return readMapper.selectClaimPaymentProcess(claimNo);
    }

    @Override
    public List<ClaimHistoryListDto> getClaimHistoryList(long claimNo) {
        return readMapper.selectClaimHistoryList(claimNo);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getOrderCombineClaimCheck(String purchaseOrderNo, String type) {
        return readMapper.selectOriginAndCombineClaimCheck(Long.parseLong(purchaseOrderNo), type);
    }

    @Override
    public ClaimPreRefundCalcSetDto getPreRefundReCheckData(long claimNo) {
        return readMapper.selectPreRefundRecheckData(claimNo);
    }

    @Override
    public List<ClaimPreRefundItemList> getPreRefundItemData(long claimNo) {
        return readMapper.selectPreRefundItemData(claimNo);
    }

    @Override
    public ClaimMstRegDto getPreRefundCompareData(long claimNo) {
        return readMapper.selectPreRefundCompareData(claimNo);
    }

    @Override
    public ClaimPreRefundCalcDto getClaimPreRefundReCheckCalculation(ClaimPreRefundCalcSetDto calcSetDto) throws Exception {
        // 파라미터 용 맵 선언.
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        // 주문번호
        parameterMap.put("purchaseOrderNo", calcSetDto.getPurchaseOrderNo());
        // 상품주문번호
        parameterMap.put("orderItemNo", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderItemNo).collect(Collectors.joining(":")));
        // 상품번호
        parameterMap.put("itemNo", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getItemNo).collect(Collectors.joining(":")));
        // 옵션번호
        parameterMap.put("orderOptNo", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderOptNo).collect(Collectors.joining(":")));
        // 클레임 수량
        parameterMap.put("claimQty", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getClaimQty).collect(Collectors.joining(":")));
        // 귀책사유
        parameterMap.put("whoReason", calcSetDto.getWhoReason());
        // 취소타입
        parameterMap.put("cancelType", calcSetDto.getCancelType());
        //행사미차감 여부
        parameterMap.put("piDeductPromoYn", calcSetDto.getPiDeductPromoYn());
        //할인 미차감 여부
        parameterMap.put("piDeductDiscountYn", calcSetDto.getPiDeductDiscountYn());
        //배송비 미차감 여부
        parameterMap.put("piDeductShipYn", calcSetDto.getPiDeductShipYn());
        // 동봉 여부
        parameterMap.put("encloseYn", calcSetDto.getEncloseYn());
        return readMapper.callByClaimPreRefundReCheckCalculation(parameterMap);
    }

    @Override
    public boolean modifyClaimMstPaymentAmt(long claimNo, String column, long amt) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimMstPaymentAmt(claimNo, column, amt));
    }

    @Override
    public boolean modifyClaimPaymentAmt(long claimNo, String paymentType, long paymentAmt) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimPaymentAmt(claimNo, paymentType, paymentAmt));
    }

    @Override
    public LinkedHashMap<String, String> getClaimTdBundleInfo(long claimNo) {
        return readMapper.selectClaimBundleTdInfo(claimNo);
    }

    @Override
    public LinkedHashMap<String, Object> getMileagePaybackCheck(long bundleNo) {
        return readMapper.selectMileagePaybackCheck(bundleNo);
    }

    @Override
    public LinkedHashMap<String, Object> getOrderCombinePaybackCheck(long purchaseOrderNo) {
        return readMapper.selectMileagePaybackInfo(purchaseOrderNo);
    }

    @Override
    public LinkedHashMap<String, Object> getClaimCombinePaybackCheck(long bundleNo, long orgBundleNo) {
        return readMapper.selectMileagePaybackCancelInfo(bundleNo, orgBundleNo);
    }

    @Override
    public boolean setShipPaybackResult(long paybackNo) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateMileagePaybackResult(paybackNo));
    }

    @Override
    public StoreInfoGetDto getStoreInfo(String storeId) {
        return readMapper.selectStoreInfo(storeId);
    }

    @Override
    public List<Long> getMultiBundleInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectMultiBundleInfoForClaim(purchaseOrderNo, bundleNo);
    }

    @Override
    public List<ClaimMultiItemRegDto> getClaimMultiItemCreateInfo(long claimNo, long multiBundleNo) {
        return writeMapper.selectClaimMultiItemCreateInfo(claimNo, multiBundleNo);
    }

    @Override
    public boolean isOrderMarketCheck(long purchaseOrderNo, long claimNo) {
        String isOrderMarket = readMapper.selectIsMarketOrderCheckForClaim(purchaseOrderNo, claimNo);
        return StringUtils.isNotEmpty(isOrderMarket) && isOrderMarket.equals("Y");
    }

    @Override
    public MarketClaimBasicDataDto getClaimBasicDataForMarketOrder(long purchaseOrderNo, long orderOptNo) {
        return readMapper.selectClaimBasicDataForMarketOrder(purchaseOrderNo, orderOptNo);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getClaimStatusChangeDataForMarket(long claimNo) {
        return readMapper.selectClaimStatusChangeDataForMarketOrder(claimNo);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getMarketCollectedInfo(long claimBundleNo) {
        return readMapper.selectMarketCollectedExchangeInfo(claimBundleNo);
    }
}
