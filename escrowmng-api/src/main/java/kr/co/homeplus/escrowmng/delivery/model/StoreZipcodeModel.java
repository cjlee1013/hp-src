package kr.co.homeplus.escrowmng.delivery.model;

import static kr.co.homeplus.escrowmng.escrow.constants.ZipcodeConstants.HYPHEN;
import static kr.co.homeplus.escrowmng.escrow.constants.ZipcodeConstants.ZERO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 점포배송정보 > 점포별 우편번호 조회 DTO")
public class StoreZipcodeModel {
    @ApiModelProperty(notes = "우편번호", position = 1)
    private String zipcode;
    @ApiModelProperty(notes = "시도", position = 2)
    private String sidoNm;
    @ApiModelProperty(notes = "시군구", position = 3)
    private String sigunguNm;
    @ApiModelProperty(notes = "도로명", position = 4)
    private String roadAddrNm;
    @ApiModelProperty(notes = "건물본번", position = 5)
    private String buildBonNo;
    @ApiModelProperty(notes = "건물부번", position = 6)
    private String buildBuNo;
    @ApiModelProperty(notes = "건물번호", position = 7)
    private String buildNo;
    @ApiModelProperty(notes = "건물명", position = 8)
    private String sigunguBuildNm;
    @ApiModelProperty(notes = "법정동", position = 9)
    private String courtEubmyndnNm;
    @ApiModelProperty(notes = "점포명", position = 10)
    private String storeNm;
    @ApiModelProperty(notes = "원거리배송여부", position = 11)
    private String remoteShipYn;
    @ApiModelProperty(notes = "사용여부", position = 12)
    private String useYn;
    @ApiModelProperty(notes = "도서산간 가능여부", position = 14)
    private String islandTypeYn;

    /** 화면에서 필요함 */
    public String getBuildNo() {
        if (EscrowString.isNotEmpty(this.buildBuNo)) {
            if (ZERO.equals(this.buildBuNo)) {
                return this.buildBonNo;
            } else {
                return this.buildBonNo + HYPHEN + this.buildBuNo;
            }
        }
        return this.buildBonNo;
    }
}