package kr.co.homeplus.escrowmng.constants;

/**
 * resource api 명을 관리
 * apiId의 url은 application.yml의 plus.resource-routes 에 정의 됨
 */
public class ResourceRouteName {

    public static final String ESCROW = "escrow";
    public static final String ITEM = "item";

    public static final String ESCROWSLACK = "escrowslack";

    public static final String MESSAGE = "message";

}
