package kr.co.homeplus.escrowmng.claim.model.external;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "DGV 취소 응답 DTO")
public class ClaimDgvCancelGetDto {
    @ApiModelProperty(notes = "결과코드", required = true, position = 1)
    private String returnCode;

    @ApiModelProperty(notes = "결과메시지", required = true, position = 2)
    private String returnMessage = "정상취소";

    @ApiModelProperty(notes = "승인번호", required = true, position = 3)
    private String aprNumber;

    @ApiModelProperty(notes = "승인일자", required = true, position = 4)
    private String aprDate;

    @ApiModelProperty(notes = "승인시간", required = true, position = 5)
    private String aprTime;

    @ApiModelProperty(notes = "DGV잔액", required = true, position = 6)
    private Long dgvRemainAmount;

    @ApiModelProperty(notes = "전문관리번호")
    private String mngNumber;
}
