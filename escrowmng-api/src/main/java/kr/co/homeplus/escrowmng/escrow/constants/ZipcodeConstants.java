package kr.co.homeplus.escrowmng.escrow.constants;

public class ZipcodeConstants {
    /** 우편번호 검색 키워트 타입 */
    public static final String ROADADDR = "ROADADDR";           // 도로명
    public static final String GIBUN = "GIBUN";                 // 지번
    public static final String ZIPCODE = "ZIPCODE";             // 우편번호
    public static final String BUILD_MNG_NO = "BUILD_MNG_NO";   // 건물 관리번호

    /** 필드 매칭 */
    public static final int INT_ZERO = 0;
    public static final String ZERO = "0";
    public static final String ONE = "1";
    public static final String TWO = "2";

    // 필드 매칭
    public static final String SPACE = " ";
    public static final String HYPHEN = "-";
    public static final String COMMA = ",";

    public static final String ZERO_STRING = "0";
    public static final String ONE_STRING = "1";
    public static final String TWO_STRING = "2";

    public static final String DOWN = "지하";
    public static final String SKY = "공중";

    public static final String LEFT_BRACE = "(";
    public static final String RIGHT_BRACE = ")";

    /** 서비스 필요 상수 */
    public static final int START_LIMIT = 0;
    public static final int END_LIMIT = 5000;
    public static final int RETURN_FALSE = -1;
}
