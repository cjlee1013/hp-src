package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "교환배송 상태 수정.")
public class ClaimExchShippingModifyDto {

    @ApiModelProperty(value = "클레임 교환배송 번호", position = 2)
    public String claimPickShippingNo;

    @ApiModelProperty(value = "클레임 번들 번호", position = 3)
    public String claimBundleNo;

    @ApiModelProperty(value = "교환배송 수정 타입 [주소:ADDR, 상태:STATUS, 배송타입:TYPE, 전부:ALL]", position = 4)
    public String modifyType;

    @ApiModelProperty(value = "배송지-이름", position = 5)
    public String exchReceiverNm;

    @ApiModelProperty(value = "배송지-휴대폰", position = 6)
    public String exchMobileNo;

    @ApiModelProperty(value = "배송지 우편번호", position = 7)
    public String exchZipcode;

    @ApiModelProperty(value = "배송지 기본주소", position = 8)
    public String exchRoadBaseAddr;

    @ApiModelProperty(value = "배송지 샹세주소", position = 9)
    public String exchRoadAddrDetail;

    @ApiModelProperty(value = "배송지 기본주소", position = 10)
    public String exchBaseAddr;

    @ApiModelProperty(value = "배송지 샹세주소", position = 11)
    public String exchDetailAddr;

    @ApiModelProperty(value = "배송배송수단", position = 12)
    public String exchShipType;

    @ApiModelProperty(value = "배송배송수단코드", position = 13)
    public String exchShipTypeCode;

    @ApiModelProperty(value = "배송택배사코드", position = 14)
    public String exchDlvCd;

    @ApiModelProperty(value = "배송택배사명", position = 15)
    public String exchDlv;

    @ApiModelProperty(value = "배송송장번호", position = 16)
    public String exchInvoiceNo;

    @ApiModelProperty(value = "배송메모", position = 17)
    public String exchMemo;

    @ApiModelProperty(value="배송상태", position = 18)
    public String exchStatus;

    @ApiModelProperty(value="배송일자", position = 19)
    public String shipDt;

    @ApiModelProperty(value="배송shiftID", position = 20)
    public String shiftId;

    @ApiModelProperty(value="배송slotID", position = 21)
    public String slotId;

    @ApiModelProperty(value="배송 예약접수자", position = 22)
    public String exchP0Id;

    @ApiModelProperty(value="배송 예약일(업체직배송인 경우 배송예정일 최초등록일)", position = 23)
    public String exchD0Dt;

    @ApiModelProperty(value="배송 송장등록일(업체직배송의 경우 배송예정일)", position = 24)
    public String exchD1Dt;

    @ApiModelProperty(value="배송 시작일", position = 25)
    public String exchD2Dt;

    @ApiModelProperty(value="배송 완료일", position = 26)
    public String exchD3Dt;

    @ApiModelProperty(value = "수정ID", position = 27)
    public String chgId;

    @ApiModelProperty(value = "수정ID", position = 28)
    public String shipType;

    @ApiModelProperty(value = "클레임 번호", position = 29)
    private long claimNo;

}