package kr.co.homeplus.escrowmng.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ExtractMenuName {
    CMBN_ORDER_SALES("합배송 주문 매출")
    ;

    private String menuNm;
}
