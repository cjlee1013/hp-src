package kr.co.homeplus.escrowmng.order.model.managementPop;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문관리 > 상세 > 배송정보 > 주문확인/송장등록/송장수정/배송완료")
public class OrderShipTotalInfoDto {

    @ApiModelProperty(value = "배송번호", position = 1)
    private long bundleNo;

    @ApiModelProperty(value = "운송번호", position = 2)
    private long shipNo;

    @ApiModelProperty(value = "상품주문번호", position = 3)
    private long orderItemNo;

    @ApiModelProperty(value = "아이템번호", position = 4)
    private long itemNo;

    @ApiModelProperty(value = "상품명", position = 5)
    private String itemNm1;

    @ApiModelProperty(value = "배송유형", position = 6)
    private String shipMethod;

    @ApiModelProperty(value = "배송상태", position = 7)
    private String shipStatus;

    @ApiModelProperty(value = "송장번호", position = 8)
    private String invoiceNo;
}
