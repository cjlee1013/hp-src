package kr.co.homeplus.escrowmng.claim.service.StoreMultiCancel;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.storeMultiCancel.StoreMultiCancelListGetDto;
import kr.co.homeplus.escrowmng.claim.model.storeMultiCancel.StoreMultiCancelListSetDto;


public interface StoreMultiCancelMapperService {

    List<StoreMultiCancelListGetDto> getStoreMultiCancelList (StoreMultiCancelListSetDto storeMultiCancelListSetDto);
}
