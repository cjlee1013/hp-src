package kr.co.homeplus.escrowmng.claim.model.multiShip;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "주문관리 > 주문정보 상세 > 취소/교환/환불 요청 DTO")
public class MultiShipClaimItemListSetDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "다중배송번호", position = 2)
    private long multiBundleNo;
}
