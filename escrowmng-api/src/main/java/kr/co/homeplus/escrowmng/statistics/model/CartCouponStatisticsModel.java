package kr.co.homeplus.escrowmng.statistics.model;

import lombok.Data;

@Data
public class CartCouponStatisticsModel {

    private String orderDt;
    private String storeType;
    private String storeId;
    private String storeNm;
    private long useCnt;
    private long useAmt;
    private long claimCnt;
    private long claimAmt;
    private long sumAmt;
}
