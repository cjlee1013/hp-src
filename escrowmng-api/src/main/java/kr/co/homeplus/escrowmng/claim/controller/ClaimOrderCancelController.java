package kr.co.homeplus.escrowmng.claim.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.ClaimOrderShippingInfoGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPrevOrderCancelDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.escrowmng.claim.service.ClaimOrderCancelService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/claim")
@RequiredArgsConstructor
@Api(tags = "주문관리 > 주문정보 상세 > 주문취소 ")
public class ClaimOrderCancelController {

    // 클레임 주문취소 관련 서비스
    private final ClaimOrderCancelService orderCancelService;

    @PostMapping("/orderCancel")
    @ApiOperation(value = "주문취소팝업 리스트조회", response = ClaimPrevOrderCancelDto.class)
    public ResponseObject<List<ClaimPrevOrderCancelDto>> getClaimPrevOrderCancelInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo, @RequestParam(value = "bundleNo", defaultValue = "0", required = false) long bundleNo, @RequestParam("claimType") String claimType) {
        return orderCancelService.getClaimPrevOrderCancelInfo(purchaseOrderNo, bundleNo);
    }

    @PostMapping("/preRefundPrice")
    @ApiOperation(value = "주문취소 - 환불예정금액", response = ClaimPreRefundCalcGetDto.class)
    public ResponseObject<ClaimPreRefundCalcGetDto> getRefundCalculation(@RequestBody ClaimPreRefundCalcSetDto calcSetDto) throws Exception{
        return orderCancelService.getClaimPreRefundCalculation(calcSetDto);
    }
}
