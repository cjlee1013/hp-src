package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(description = "결제관리 > 결제수단 관리 > 결제수단 관리 search DTO")
public class PaymentMethodSelectDto {
    @ApiModelProperty(value = "사이트유형(HOME:홈플러스,CLUB:더클럽)")
    private String siteType;
    @ApiModelProperty(value = "결제수단코드")
    private String methodCd;
    @ApiModelProperty(value = "상위결제수단코드(신용카드,휴대폰결제등)", required = true)
    private String parentMethodCd;
    @ApiModelProperty(value = "사용여부")
    private String useYn;
    @ApiModelProperty(value = "상위결제수단코드리스트(신용카드,휴대폰결제등)", hidden = true)
    private List<String> parentMethodCdList;
}
