package kr.co.homeplus.escrowmng.claim.model.external;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClaimPgCancelGetDto {

    @ApiModelProperty(value = "결과코드", position = 1)
    private String resultCode;

    @ApiModelProperty(value = "결과메시지", position = 2)
    private String resultMsg;

    @ApiModelProperty(value = "취소일자(YYYYMMDD)", position = 3)
    private String cancelData;

    @ApiModelProperty(value = "취소시간(HHMISS)", position = 4)
    private String cancelTime;

    @ApiModelProperty(value = "PG원거래번호", position = 5)
    private String pgTid;

    @ApiModelProperty(value = "PG부분취소거래번호", position = 6)
    private String pgPartTid;

    @ApiModelProperty(value = "부분취소요청금액", position = 7)
    private String partCancelPrice;

    @ApiModelProperty(value = "부분취소남은금액", position = 8)
    private String partRemainPrice;

    @ApiModelProperty(value = "부분취소요청횟수", position = 9)
    private String partCnt;

}
