package kr.co.homeplus.escrowmng.delivery.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.delivery.model.DailyShiftDuplicateCheckModel;
import kr.co.homeplus.escrowmng.delivery.model.DailyShiftManageModel;
import kr.co.homeplus.escrowmng.delivery.model.DailyShiftManageSelectModel;
import kr.co.homeplus.escrowmng.delivery.model.DailyShiftStoreManageModel;
import org.apache.ibatis.annotations.Param;

@EscrowSlaveConnection
public interface DailyShiftManageSlaveMapper {
    List<DailyShiftManageModel> selectDailyShiftManageList(DailyShiftManageSelectModel dailyShiftManageSelectModel);
    List<DailyShiftStoreManageModel> selectDailyShiftStoreManageList(@Param("dailyShiftMngSeq") long dailyShiftMngSeq);
    int checkDuplicateDailyShiftManage(DailyShiftDuplicateCheckModel dailyShiftDuplicateCheckModel);
}
