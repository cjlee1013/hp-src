package kr.co.homeplus.escrowmng.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "쿠폰통계 결과")
@Getter
@Setter
public class CouponStatistics {

    @ApiModelProperty(value = "점포유형")
    private String storeType;
    @ApiModelProperty(value = "점포명")
    private String storeNm;
    @ApiModelProperty(value = "쿠폰번호")
    private long couponNo;
    @ApiModelProperty(value = "쿠폰명")
    private String couponNm;
    @ApiModelProperty(value = "쿠폰종류")
    private String couponKind;
    @ApiModelProperty(value = "쿠폰종류명")
    private String couponKindNm;
    @ApiModelProperty(value = "주문건수")
    private int orderCnt;
    @ApiModelProperty(value = "상품번호")
    private String itemNo;
    @ApiModelProperty(value = "대카테고리")
    private String lcateNm;
    @ApiModelProperty(value = "중카테고리")
    private String mcateNm;
    @ApiModelProperty(value = "주문금액")
    private long orderAmt;
    @ApiModelProperty(value = "쿠폰사용건수")
    private int couponUseCnt;
    @ApiModelProperty(value = "할인금액")
    private long discountAmt;
    @ApiModelProperty(value = "취소금액")
    private long claimDiscountAmt;
    @ApiModelProperty(value = "취소건수")
    private int claimCnt;


    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{")
            .append("storeType:").append(storeType).append(",")
            .append("storeNm:").append(storeNm).append(",")
            .append("couponNo:").append(couponNo).append(",")
            .append("couponNm:").append(couponNm).append(",")
            .append("couponKind:").append(couponKind).append(",")
            .append("couponKindNm:").append(couponKindNm).append(",")
            .append("orderCnt:").append(orderCnt)
            .append("}");

        return sb.toString();
    }
}
