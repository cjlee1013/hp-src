package kr.co.homeplus.escrowmng.pg.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.pg.model.PgCommissionManageDto;
import kr.co.homeplus.escrowmng.pg.model.PgCommissionTargetDto;

@EscrowMasterConnection
public interface PgCommissionManageMasterMapper {
    int insertPgCommissionMngList(PgCommissionManageDto pgCommissionManageDto);
    int insertPgCommissionTargetList(PgCommissionTargetDto pgCommissionTargetDto);
}
