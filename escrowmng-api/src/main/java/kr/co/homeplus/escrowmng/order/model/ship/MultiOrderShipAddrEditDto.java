package kr.co.homeplus.escrowmng.order.model.ship;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MultiOrderShipAddrEditDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;
    @ApiModelProperty(value = "주문번호", position = 2)
    private long multiBundleNo;
    @ApiModelProperty(value = "이름", position = 3)
    private String receiverNm;
    @ApiModelProperty(value = "연락처", position = 4)
    private String shipMobileNo;
    @ApiModelProperty(value = "우편번호", position = 6)
    private String zipCode;
    @ApiModelProperty(value = "도로명", position = 7)
    private String roadBaseAddr;
    @ApiModelProperty(value = "도로명상세", position = 8)
    private String roadDetailAddr;
    @ApiModelProperty(value = "지번", position = 9)
    private String baseAddr;
    @ApiModelProperty(value = "지번상세", position = 10)
    private String detailAddr;
    @ApiModelProperty(value = "배송구분", position = 13)
    private String shipType;
    @ApiModelProperty(value = "처리자", position = 14)
    private String chgId;
}
