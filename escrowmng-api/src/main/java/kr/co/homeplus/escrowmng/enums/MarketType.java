package kr.co.homeplus.escrowmng.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum MarketType {
    NAVER("NAVER", "네이버", "Y"), //네이버
    GMARKET("GMARKET", "지마켓", "N"), //지마켓
    AUCTION("AUCTION", "옥션", "N"), //옥션
    ELEVEN("ELEVEN", "11번가", "N"), //11번가
    ;

    private String type;
    private String typeNm;
    private String useYn;

    public static List<MarketType> getUsableMarketTypeList() {
        return Arrays.stream(MarketType.values()).filter(mt -> "Y".equals(mt.useYn)).collect(Collectors.toList());
    }

}
