package kr.co.homeplus.escrowmng.claim.model.entity;

public class OrderPromoEntity {

    // 주문프로모션번호
    public String orderPromoNo;

    // 구매주문번호
    public String purchaseOrderNo;

    // 번들번호
    public String bundleNo;

    // 프로모션번호(레거시 프로모션 번호 promoDisplayId)
    public String promoNo;

    // 프로모션상세번호(레거시 프로모션 상세 번호 compDisplayId)
    public String promoDetailNo;

    // 프로모션명
    public String promoNm;

    // 프로모션유형(SIMP:simple, THRES:Threhold, MIX:MixMatch)
    public String promoType;

    // 프로모션종류(BASIC:기본할인, PICK:골라담기, TOGETHER:함께할인, INTERVAL:구간할인)
    public String promoKind;

    // 프로모션상품수량
    public String promoItemQty;

    // 할인유형(0:할인율(%), 1:할인금액, 2:고정금액(3개 구매시 9900원), 4:제외, -1:변경없음)
    public String discountType;

    // 할인율
    public String discountRate;

    // 할인금액
    public String discountAmt;

    // 기준점유형(0:금액(사용불가), 1:수량(QTY)
    public String thresholdType;

    // 기준점ID(구간별 할인 정보 조회 Threshole Key값)
    public String thresholdId;

    // 기준점적용수량(첫구간)
    public String thresholdApplyQty;

    // 기준점적용금액(첫구간)
    public String thresholdApplyAmt;

    // 믹스매치유형(1:구매수량, 2:구매금액)
    public String mixMatchType;

    // 믹스매치기준값(믹스&매치 구매수량 or 구매금액)
    public String mixMatchStdVal;

    // 믹스매치개수(MixMatch 구매 대상 상품 개수)
    public String mixMatchCnt;

    // 점포ID
    public String storeId;

    // 등록일시
    public String regDt;

}
