package kr.co.homeplus.escrowmng.pg.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.pg.model.PgMidManageModel;
import org.apache.ibatis.annotations.Param;

@EscrowSlaveConnection
public interface PgMidSlaveMapper {
    List<PgMidManageModel> selectPgMidManage(@Param("pgKind") String pgKind, @Param("useYn") String useYn);
}
