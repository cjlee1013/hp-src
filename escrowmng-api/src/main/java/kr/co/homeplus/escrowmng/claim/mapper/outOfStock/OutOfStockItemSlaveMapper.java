package kr.co.homeplus.escrowmng.claim.mapper.outOfStock;

import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.OutOfStockItemListGetDto;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.OutOfStockItemListSetDto;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.StoreShiftListSetDto;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;

@EscrowSlaveConnection
public interface OutOfStockItemSlaveMapper {

    List<OutOfStockItemListGetDto> selectOutOfStockItemList(OutOfStockItemListSetDto outOfStockItemListSetDto);

    List<String> selectStoreShiftList(StoreShiftListSetDto storeShiftListSetDto);

}
