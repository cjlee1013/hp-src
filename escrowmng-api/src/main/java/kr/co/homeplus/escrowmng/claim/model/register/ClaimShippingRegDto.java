package kr.co.homeplus.escrowmng.claim.model.register;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "클레임 반품 등록 DTO")
public class ClaimShippingRegDto {

    @ApiModelProperty(value= "클레임반품수거번호")
    private long claimPickShippingNo;

    @ApiModelProperty(value= "클레임 묶음번호(환불:클레임 번호)")
    private long claimBundleNo;

    @ApiModelProperty(value= "고객번호")
    private long userNo;

    @ApiModelProperty(value= "반품수거_자동접수 여부(자동접수: Y)")
    private String isAuto;

    @ApiModelProperty(value= "구매자 반품발송여부(구매자 발송 : Y)")
    private String isPick;

    @ApiModelProperty(value= "수거대행 여부 - 파트너 등록 정보 참조(Y: 대행가능 / N:대행불가)")
    private String isPickProxy;

    @ApiModelProperty(value= "수거배송수단(C:택배배송 D:업체직배송 N:배송없음)")
    private String pickShipType;

    @ApiModelProperty(value= "수거상태(NN: 요청대기 - 자동접수 미지원 택배사인 경우 N0: 수거예약 - 택배사 수동 예약 P0:자동접수요청  (반품자동접수 요청성공) P1: 요청완료-송장등록 (반품자동접수 후 송장번호 받은경우 / 수거요청 처리로 송장등록) P2: 수거중 P3: 수거완료 P8 :수거실패 (반품자동접수실패))")
    private String pickStatus;

    @ApiModelProperty(value= "수거_택배코드")
    private String pickDlvCd;

    @ApiModelProperty(value= "원 송장번호")
    private String orgInvoiceNo;

    @ApiModelProperty(value= "수거_송장번호")
    private String pickInvoiceNo;

    @ApiModelProperty(value= "수거송장 등록자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)")
    private String pickInvoiceRegId;

    @ApiModelProperty(value= "배송추적요청여부")
    private String pickTrackingYn;

    @ApiModelProperty(value= "수거_이름")
    private String pickReceiverNm;

    @ApiModelProperty(value= "수거_우편번호")
    private String pickZipcode;

    @ApiModelProperty(value= "수거_주소1")
    private String pickBaseAddr;

    @ApiModelProperty(value= "수거_주소2")
    private String pickDetailAddr;

    @ApiModelProperty(value= "수거_도로명 기본주소")
    private String pickRoadBaseAddr;

    @ApiModelProperty(value= "수거_도로명 상세주소")
    private String pickRoadDetailAddr;

    @ApiModelProperty(value= "수거_휴대전화")
    private String pickMobileNo;

    @ApiModelProperty(value= "수거_연락처")
    private String pickPhone;

    @ApiModelProperty(value= "수거_메모")
    private String pickMemo;

    @ApiModelProperty(value= "수거일자")
    private String shipDt;

    @ApiModelProperty(value= "수거shiftId")
    private String shiftId;

    @ApiModelProperty(value= "수거slotId")
    private String slotId;

    @ApiModelProperty(value= "파트너ID")
    private String partnerId;

    @ApiModelProperty(value= "등록ID")
    private String regId;

    @ApiModelProperty(value= "수정자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)")
    private String chgId;

    @ApiModelProperty(value= "등록ID", hidden = true)
    private String safetyUseYn;

    @JsonIgnore
    @ApiModelProperty(value= "원배송방법", hidden = true)
    private String shipMethod;
}
