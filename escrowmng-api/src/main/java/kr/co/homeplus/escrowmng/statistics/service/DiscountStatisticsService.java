package kr.co.homeplus.escrowmng.statistics.service;

import java.util.List;
import kr.co.homeplus.escrowmng.statistics.mapper.DiscountStatisticsSlaveMapper;
import kr.co.homeplus.escrowmng.statistics.model.DiscountStatistics;
import kr.co.homeplus.escrowmng.statistics.model.DiscountStatisticsSelect;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DiscountStatisticsService {

    private final DiscountStatisticsSlaveMapper discountStatisticsSlaveMapper;

    public List<DiscountStatistics> getDiscountStatisticsList(DiscountStatisticsSelect discountStatisticsSelect) {
        return discountStatisticsSlaveMapper.selectDiscountStatisticsList(discountStatisticsSelect);
    }

    public List<DiscountStatistics> getDiscountStatisticsDetailList(DiscountStatisticsSelect discountStatisticsSelect) {
        return discountStatisticsSlaveMapper.selectDiscountStatisticsDetailList(discountStatisticsSelect);
    }
}
