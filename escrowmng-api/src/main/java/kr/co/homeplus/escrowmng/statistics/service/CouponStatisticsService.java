package kr.co.homeplus.escrowmng.statistics.service;

import java.util.List;
import kr.co.homeplus.escrowmng.statistics.mapper.CouponStatisticsSlaveMapper;
import kr.co.homeplus.escrowmng.statistics.model.CouponStatistics;
import kr.co.homeplus.escrowmng.statistics.model.CouponStatisticsSelect;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CouponStatisticsService {

    private final CouponStatisticsSlaveMapper couponStatisticsSlaveMapper;

    public List<CouponStatistics> getCouponStatisticsList(CouponStatisticsSelect couponStatisticsSelect) {
        return couponStatisticsSlaveMapper.selectCouponStatisticsList(couponStatisticsSelect);
    }

    /**
     * 상품쿠폰, 상품할인 상세 조회
     * @param couponStatisticsSelect
     * @return
     */
    public List<CouponStatistics> getItemCouponStatisticsDetailList(CouponStatisticsSelect couponStatisticsSelect) {
        return couponStatisticsSlaveMapper.selectItemCouponStatisticsDetailList(couponStatisticsSelect);
    }

    /**
     * 장바구니 쿠폰, 배송비 쿠폰 상세 조회
     * @param couponStatisticsSelect
     * @return
     */
    public List<CouponStatistics> getCartCouponStatisticsDetailList(CouponStatisticsSelect couponStatisticsSelect) {
        return couponStatisticsSlaveMapper.selectCartCouponStatisticsDetailList(couponStatisticsSelect);
    }
}
