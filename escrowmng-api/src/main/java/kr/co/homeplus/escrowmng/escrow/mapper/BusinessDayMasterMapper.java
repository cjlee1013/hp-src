package kr.co.homeplus.escrowmng.escrow.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.escrow.model.BusinessDayManageModel;
import org.apache.ibatis.annotations.Param;

@EscrowMasterConnection
public interface BusinessDayMasterMapper {
    void insertBusinessDay(BusinessDayManageModel businessDayManageModel);
    void updateBusinessDay(BusinessDayManageModel businessDayManageModel);
    void insertBusinessDayHistory(@Param("businessNo") String businessNo);
}
