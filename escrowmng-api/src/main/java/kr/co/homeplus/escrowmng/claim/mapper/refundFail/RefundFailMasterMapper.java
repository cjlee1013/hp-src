package kr.co.homeplus.escrowmng.claim.mapper.refundFail;


import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteSetDto;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;

@EscrowMasterConnection
public interface RefundFailMasterMapper {
    int updateRefundComplete(RequestRefundCompleteSetDto requestRefundCompleteSetDto);
}
