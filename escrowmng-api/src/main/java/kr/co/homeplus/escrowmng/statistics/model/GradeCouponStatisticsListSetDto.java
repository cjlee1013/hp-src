package kr.co.homeplus.escrowmng.statistics.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(description = "등급별 쿠폰 통계 리스트 조회 DTO")
public class GradeCouponStatisticsListSetDto {

    @ApiModelProperty(value = "조회시작일", position = 1)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", position = 2)
    private String schEndDt;
}
