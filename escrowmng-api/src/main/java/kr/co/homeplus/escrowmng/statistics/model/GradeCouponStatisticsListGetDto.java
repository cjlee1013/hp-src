package kr.co.homeplus.escrowmng.statistics.model;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GradeCouponStatisticsListGetDto {

    @ApiModelProperty(value = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "등급")
    private String gradeStr;

    @ApiModelProperty(value = "주문고객수")
    private String userCnt;

    @ApiModelProperty(value = "주문건수")
    private String purchaseNumber;

    @ApiModelProperty(value = "매출")
    private String totAmt;

    @ApiModelProperty(value = "주문횟수")
    private double purchaseCnt;

    @ApiModelProperty(value = "객단가")
    private double customerPrice;

    @ApiModelProperty(value = "사용금액")
    private String discountAmt;

}
