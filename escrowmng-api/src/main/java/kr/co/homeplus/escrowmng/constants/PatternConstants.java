package kr.co.homeplus.escrowmng.constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 * 패턴/포멧 관련 공통 상수
 */
public class PatternConstants {
    public static final String KOR = "KOR";
    public static final String ENG = "ENG";
    public static final String ENG_UPPER = "ENG_UPPER";
    public static final String ENG_LOWER = "ENG_LOWER";
    public static final String NUM = "NUM";
    public static final String SPC_SCH = "SPC_SCH";
    public static final String NOT_SPACE = "NOT_SPACE";
    public static final String SYS_DIVISION = "SYS_DIVISION";
    public static final String SPACE = "SPACE";

    // Spaces, Tab, line breaks
    public static final String SPACE_MSG = "띄어쓰기";
    public static final String SPACE_FORMAT = "\\s";

    // 한글
    public static final String KOR_MSG = "한글";
    public static final String KOR_FORMAT = "ㄱ-ㅎㅏ-ㅣ가-힣";

    // 영문
    public static final String ENG_MSG = "영문";
    public static final String ENG_FORMAT = "a-zA-Z";

    public static final String ENG_UPPER_MSG = "영문 대문자";
    public static final String ENG_UPPER_FORMAT = "A-Z";

    public static final String ENG_LOWER_MSG = "영문 소문자";
    public static final String ENG_LOWER_FORMAT = "a-z";

    // 숫자
    public static final String NUM_MSG = "숫자";
    public static final String NUM_FORMAT = "0-9";

    // % \ 미포함 isNotAllowInput 에 사용
    public static final String SPC_SCH_MSG = "% \\ 특수문자는 입력할 수 없습니다.";
    public static final String SPC_SCH_FORMAT = "%\\\\";

    // 시스템 구분자
    public static final String SYS_DIVISION_MSG =  Constants.SYS_DIVISION;
    public static final String SYS_DIVISION_FORMAT = "\\" + Constants.SYS_DIVISION;

    // Y 또는 N 1문자
    public static final String YN_PATTERN = "(Y|N){1}";
    public static final String YN_PATTERN_SMALL_LETTER = "(y|n){1}";

    // 건물번호
    public static final String BUILD_NO_PATTERN = "^([\\d]*)|([\\d]+[\\-]{1}[\\d]+)$";

    // 우편번호
    public static final String ZIPCODE_PATTERN = "[0-9]{5}";

    // 점포유형
    public static final String STORE_TYPE_PATTERN = "(HYPER|CLUB|EXP|AURORA)";

    // 사이트유형
    public static final String SITE_TYPE_PATTERN = "(home|HOME|club|CLUB)";

    // 날짜(YYYY-MM-DD 또는 YYYYMMDD)
    public static final String DATE_YYYYMMDD_FORMAT_PATTERN = "[0-9]{4}[-]?[0-9]{2}[-]?[0-9]{2}";

    // 날짜(YYYY-MM-DD)
    public static final String DATE_DF_FORMAT = "yyyy-MM-dd";
    public static final String DATE_DF_FORMAT_PATTERN = "(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)";

    // 날짜(YYYY-MM-DD HH:mm:ss)
    public static final String DATE_TIME_DF_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_TIME_DF_PATTERN = "(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])";

    // 날짜(YYYY-MM-DD 00:00)
    public static final String DATE_TIME_MIDNIGHT_FORMAT = "yyyy-MM-dd 00:00";
    public static final String DATE_TIME_MIDNIGHT_PATTERN = "(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31) ([01][0-9]|2[0-3]):([0][0])";

    // 날짜(YYYY-MM-DD HH:00)
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:00";
    public static final String DATE_TIME_PATTERN = "(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31) ([01][0-9]|2[0-3]):([0][0])"; // ([0-5][0-9])

    // 날짜(YYYY-MM-DD HH:59)
    public static final String DATE_END_TIME_FORMAT = "yyyy-MM-dd HH:59";
    public static final String DATE_END_TIME_PATTERN = "(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31) ([01][0-9]|2[0-3]):([5][9])"; // ([0-5][0-9])

    // 날짜(YYYY-MM-DD HH:00:00)
    public static final String DATE_TIME_FORMAT_BASIC = "yyyy-MM-dd HH:00:00";
    public static final String DATE_TIME_PATTERN_BASIC = "(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31) ([01][0-9]|2[0-3]):([0][0]):([0][0])"; // ([0-5][0-9])

    // 날짜(yyyy-MM-dd HH:mm)
    public static final String DATE_TIME_FORMAT_MINUTES = "yyyy-MM-dd HH:mm";
    public static final String DATE_TIME_PATTERN_MINUTES = "(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31) ([01][0-9]|2[0-3]):([0-5][0-9])";

    public static final String EMPTY = "\\p{Z}";

    public static final String ENTER_STR = "(\r\n|\r|\n|\n\r)";

    // 유니코드(U+00A0) No-Break space
    public static final String NO_BREAK_SPACE = "(\r\n|\r|\n|\n\r)";

    // ISBN 13 반복 체크
    public static final String ISBN13_REPEATS_PATTERN = "(\\d)\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1";

    // ISBN 10 반복 체크
    public static final String ISBN10_REPEATS_PATTERN = "(\\d)\\1\\1\\1\\1\\1\\1\\1\\1\\1";

    // 카테고리 수수료율
    public static final String CATEGORY_COMMISSION_RATE_PATTERN = "[0-9]{1,3}([.][0-9]{0,2})?";

    // 이메일
    public static final String EMAIL_PATTERN = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$";

    // 핸드폰
    public static final String MOBILE_PATTERN = "^01(?:0|1|[6-9])-(?:\\d{3}|\\d{4})-\\d{4}$";

    // 전화번호
    public static final String PHONE_PATTERN = "^\\d{2,3}-\\d{3,4}-\\d{4}";

    // 배송요일
    public static final String SHIP_WEEKDYA_PATTERN = "[1-7]{1}";

    public static final String DATETIME_PATTERN_HOUR_MINUTES = "([01][0-9]|2[0-3])([0-5][0-9])";

    // 휴무유형
    public static final String CLOSE_DAY_TYPE = "(F|R|S|C|E|T){1}";


    public static final Map<String, String[]> constantsMap = new HashMap<String, String[]>(){
        {
            put(KOR, new String[]{KOR_MSG, KOR_FORMAT});
            put(ENG, new String[]{ENG_MSG, ENG_FORMAT});
            put(ENG_UPPER, new String[]{ENG_UPPER_MSG, ENG_UPPER_FORMAT});
            put(ENG_LOWER, new String[]{ENG_LOWER_MSG, ENG_LOWER_FORMAT});
            put(NUM, new String[]{NUM_MSG, NUM_FORMAT});
            put(NOT_SPACE, new String[]{"", ""});
            put(SPC_SCH, new String[]{SPC_SCH_MSG, SPC_SCH_FORMAT});
            put(SYS_DIVISION, new String[]{SYS_DIVISION_MSG, SYS_DIVISION_FORMAT});
            put(SPACE, new String[]{SPACE_MSG, SPACE_FORMAT});
        }
    };

    public static String getConstantsFormat(String type){
        return constantsMap.containsKey(type) ? constantsMap.get(type)[1] : "";
    }

    public static String getConstantsMessage(String... types){
        List<String> messageList = new ArrayList<>();

        if(StringUtils.isNoneEmpty(types)){
            for(String type : types){
                if(constantsMap.containsKey(type) && StringUtils.isNoneEmpty(constantsMap.get(type)[0])){
                    messageList.add(constantsMap.get(type)[0]);
                }
            }
        }
        return String.join(", ", messageList);
    }

}

