package kr.co.homeplus.escrowmng.claim.model.register;

import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ApiModel(description = "일괄취소(발주관리,점포취소)")
public class ClaimAllCancelSetDto {

    private String claimReasonType;
    private String claimReasonDetail;
    private List<OrderAllCancelDto> itemList;
    private String regId;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class OrderAllCancelDto {
        private long purchaseOrderNo;
        private long bundleNo;
    }
}
