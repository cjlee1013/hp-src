package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel(description = "결제관리 > PG 대사 > PG 승인대사 조회 > PG 승인대사 합계 DTO")
public class PgCompareApprovalSumDto {

    @ApiModelProperty(notes = "PG종류(TOSSPG,INICIS)", required = true, position = 1)
    @NotEmpty(message = "PG")
    private String pgKind;

    @ApiModelProperty(notes = "승인금액(홈플러스)", position = 2)
    private long approvalTotAmt;

    @ApiModelProperty(notes = "취소금액(홈플러스)", position = 3)
    private long cancelTotAmt;

    @ApiModelProperty(notes = "승인취소금액합계(홈플러스)", position = 4)
    private long sumAmt;

    @ApiModelProperty(notes = "승인금액(PG)", position = 5)
    private long pgApprovalTotAmt;

    @ApiModelProperty(notes = "취소금액(PG)", position = 6)
    private long pgCancelTotAmt;

    @ApiModelProperty(notes = "승인취소금액합계(PG)", position = 7)
    private long pgSumAmt;

    @ApiModelProperty(notes = "승인차액", position = 8)
    private long approvalDiffTotAmt;

    @ApiModelProperty(notes = "취소차액)", position = 9)
    private long cancelDiffTotAmt;

    @ApiModelProperty(notes = "승인취소차액합계", position = 10)
    private long diffSumAmt;

}
