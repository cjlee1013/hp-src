package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimProcessGetDto {

    @ApiModelProperty(value = "등록ID")
    private String requestId;
    @ApiModelProperty(value = "등록ID")
    private String requestChannel;
    @ApiModelProperty(value = "등록일자")
    private String requestDt;
    @ApiModelProperty(value = "승인채널")
    private String approveChannel;
    @ApiModelProperty(value = "승인ID")
    private String approveId;
    @ApiModelProperty(value = "승인날짜")
    private String approveDt;
    @ApiModelProperty(value = "거부ID")
    private String pendingId;
    @ApiModelProperty(value = "거부채널")
    private String pendingChannel;
    @ApiModelProperty(value = "거부날짜")
    private String pendingDt;
    @ApiModelProperty(value = "철회ID")
    private String withdrawId;
    @ApiModelProperty(value = "철회채널")
    private String withdrawChannel;
    @ApiModelProperty(value = "철회날짜")
    private String withdrawDt;
    @ApiModelProperty(value = "거부ID")
    private String rejectId;
    @ApiModelProperty(value = "거부채널")
    private String rejectChannel;
    @ApiModelProperty(value = "거부날짜")
    private String rejectDt;
    @ApiModelProperty(value = "완료ID")
    private String completeId;
    @ApiModelProperty(value = "완료채널")
    private String completeChannel;
    @ApiModelProperty(value = "완료날짜")
    private String completeDt;
    @ApiModelProperty(value = "수거요청일")
    private String pickRegDt;
    @ApiModelProperty(value = "수거요청ID")
    private String pickRegId;
    @ApiModelProperty(value = "수거요청채널")
    private String pickRegChannel;
    @ApiModelProperty(value = "수거완료채널")
    private String pickCompleteChannel;
    @ApiModelProperty(value = "수거완료ID")
    private String pickCompleteId;
    @ApiModelProperty(value = "수거완료일")
    private String pickCompleteDt;
    @ApiModelProperty(value = "교환배송일")
    private String exchRegDt;
    @ApiModelProperty(value = "교환배송요청채널")
    private String exchRegChannel;
    @ApiModelProperty(value = "교환배송요청ID")
    private String exchRegId;
    @ApiModelProperty(value = "교환완료일")
    private String exchCompleteDt;
    @ApiModelProperty(value = "교환배송완료채널")
    private String exchCompleteChannel;
    @ApiModelProperty(value = "교환배송완료ID")
    private String exchCompleteId;
}
