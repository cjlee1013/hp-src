package kr.co.homeplus.escrowmng.statistics.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.escrowmng.extract.service.ExtractHistoryService;
import kr.co.homeplus.escrowmng.statistics.model.ZipcodeOrderStatistics;
import kr.co.homeplus.escrowmng.statistics.model.ZipcodeOrderStatisticsSelect;
import kr.co.homeplus.escrowmng.statistics.service.ZipcodeStatisticsOrderService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/statistics")
@Api(tags = "통계 > 점포 상품 통계 > 우편번호별 주문현황")
public class ZipcodeStatisticsOrderController {

    private final ExtractHistoryService extractHistoryService;
    private final ZipcodeStatisticsOrderService zipcodeStatisticsOrderService;

    @PostMapping("/getZipcodeOrderStatisticsList")
    @ApiOperation("우편번호별 주문현황 리스트 조회")
    public ResponseObject<List<ZipcodeOrderStatistics>> getZipcodeOrderStatisticsList(@RequestBody ZipcodeOrderStatisticsSelect zipcodeOrderStatisticsSelect) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(zipcodeOrderStatisticsSelect.getExtractCommonSetDto());
        // 우편번호별 주문현황 리스트 조회
        return ResourceConverter.toResponseObject(zipcodeStatisticsOrderService.getZipcodeOrderStatisticsList(zipcodeOrderStatisticsSelect));
    }
}
