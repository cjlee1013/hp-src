package kr.co.homeplus.escrowmng.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "픽업점포관리 model")
@Getter
@Setter
@EqualsAndHashCode
public class PickupStoreManageModel {
    @ApiModelProperty(value = "점포유형", position = 1)
    private String storeType;

    @ApiModelProperty(value = "점포ID", position = 2)
    private String storeId;

    @ApiModelProperty(value = "점포명", position = 3)
    private String storeNm;

    @ApiModelProperty(value = "장소번호", position = 4)
    private String placeNo;

    @ApiModelProperty(value = "장소사용여부", position = 5)
    private String placeUseYn;

    @ApiModelProperty(value = "픽업장소", position = 6)
    private String placeNm;

    @ApiModelProperty(value = "장소이미지", position = 7)
    private String imgUrl;

    @ApiModelProperty(value = "전화번호", position = 8)
    private String telNo;

    @ApiModelProperty(value = "장소정보", position = 9)
    private String placeAddr;

    @ApiModelProperty(value = "픽업장소상세", position = 10)
    private String placeExpln;

    @ApiModelProperty(value = "운영시간", position = 11)
    private String pickupTime;

    @ApiModelProperty(value = "락커사용여부", position = 12)
    private String lockerUseYn;

    @ApiModelProperty(value = "사용락커개수", position = 13)
    private int lockerCount;
}





