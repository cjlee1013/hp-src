package kr.co.homeplus.escrowmng.statistics.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.homeplus.escrowmng.statistics.model.CartCouponStatisticsModel;
import kr.co.homeplus.escrowmng.statistics.model.CartCouponStatisticsSelectModel;
import kr.co.homeplus.escrowmng.statistics.service.CartCouponStatisticsService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/statistics/cartcoupon")
@Api(tags = "통계 > 프로모션통계 > 장바구니쿠폰통계")
public class CartCouponStatisticsController {

    private final CartCouponStatisticsService cartCouponStatisticsService;

    @GetMapping("/getCartCouponStatistics")
    @ApiOperation(value = "장바구니쿠폰 점포별 통계 조회", response = CartCouponStatisticsModel.class)
    public ResponseObject<List<CartCouponStatisticsModel>> getCartCouponStatistics(@Valid CartCouponStatisticsSelectModel cartCouponStatisticsSelectModel) {
        log.info("cartCouponStatisticsSelectModel: {}", cartCouponStatisticsSelectModel);
        return ResourceConverter.toResponseObject(cartCouponStatisticsService.getCartCouponStatistics(cartCouponStatisticsSelectModel));
    }

    @GetMapping("/getCartCouponStatisticsByStoreId")
    @ApiOperation(value = "장바구니쿠폰 일자별 통계 조회", response = CartCouponStatisticsModel.class)
    public ResponseObject<List<CartCouponStatisticsModel>> getCartCouponStatisticsByStoreId(@Valid CartCouponStatisticsSelectModel cartCouponStatisticsSelectModel) {
        return ResourceConverter.toResponseObject(cartCouponStatisticsService.getCartCouponStatisticsByStoreId(cartCouponStatisticsSelectModel));
    }
}
