package kr.co.homeplus.escrowmng.statistics.service;

import kr.co.homeplus.escrowmng.statistics.mapper.RealTimeOneItemSaleSlaveMapper;
import kr.co.homeplus.escrowmng.statistics.model.RealTimeOneItemSaleGetDto;
import kr.co.homeplus.escrowmng.statistics.model.RealTimeOneItemSaleSearchSetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RealTimeOneItemSaleService {
    private final RealTimeOneItemSaleSlaveMapper realTimeOneItemSaleSlaveMapper;

    /**
     * 실시간 특정 상품 판매 현황
     * @param realTimeOneItemSaleSearchSetDto 실시간 특정 상품 판매 현황 파라미터
     * @return {@link RealTimeOneItemSaleGetDto}
     */
    public List<RealTimeOneItemSaleGetDto> getRealTimeOneItemSaleList(RealTimeOneItemSaleSearchSetDto realTimeOneItemSaleSearchSetDto) {
        return realTimeOneItemSaleSlaveMapper.selectRealTimeOneItemSaleList(realTimeOneItemSaleSearchSetDto);
    }

}
