package kr.co.homeplus.escrowmng.extract.service;

import kr.co.homeplus.escrowmng.extract.mapper.ExtractHistoryMasterMapper;
import kr.co.homeplus.escrowmng.extract.model.ExtractCommonSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ExtractHistoryService {
    private final ExtractHistoryMasterMapper extractHistoryMasterMapper;
    public void saveExtractHistory(ExtractCommonSetDto extractCommonSetDto) {
        try {
            /* 조회 히스토리 저장 */
            log.info("[extract-history] 추출자사번: {}, 메뉴명: {}, 추출타입: {}, 추출일자: {}"
                , extractCommonSetDto.getEmpId(), extractCommonSetDto.getMenuNm(), extractCommonSetDto.getExtractType(), extractCommonSetDto.getExtractDt());

            extractHistoryMasterMapper.insertExtractHistory(extractCommonSetDto);
        } catch (Exception e) {
            log.info("[extract-history] 로그 적재 실패\n{}", ExceptionUtils.getStackTrace(e));
        }
    }
}
