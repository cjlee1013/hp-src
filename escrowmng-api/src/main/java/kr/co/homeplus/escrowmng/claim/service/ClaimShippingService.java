package kr.co.homeplus.escrowmng.claim.service;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import kr.co.homeplus.escrowmng.claim.model.ClaimExchShippingDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPickShippingDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPickShippingModifyDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimReasonInfoGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimShippingSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimShippingStatusSetDto;
import kr.co.homeplus.escrowmng.claim.model.external.safety.ClaimSafetySetDto;
import kr.co.homeplus.escrowmng.claim.service.market.MarketClaimService;
import kr.co.homeplus.escrowmng.enums.ExternalInfo;
import kr.co.homeplus.escrowmng.utils.MessageSendUtil;
import kr.co.homeplus.escrowmng.utils.ObjectUtils;
import kr.co.homeplus.escrowmng.utils.ProcessHistoryUtills;
import kr.co.homeplus.escrowmng.core.exception.LogicException;
import kr.co.homeplus.escrowmng.enums.ClaimCode;
import kr.co.homeplus.escrowmng.enums.ClaimResponseCode;
import kr.co.homeplus.escrowmng.enums.OrderCode;
import kr.co.homeplus.escrowmng.order.factory.OrderResponseFactory;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClaimShippingService {

    private final ClaimMapperService claimMapperService;

    private final ProcessHistoryUtills processHistoryUtills;

    private final ExternalService externalService;
    //마켓클레임 서비스
    private final MarketClaimService marketClaimService;

    /**
     * 반품/교환 배송정보 변경
     * **/
    public ResponseObject<String> reqClaimShippingChange(ClaimShippingSetDto reqSetDto) throws Exception {

        /* 1.반품/교환 정보 변경 */
        switch (reqSetDto.getClaimShippingReqType()){
            //반품 배송정보 변경
            case "P" :
                //수거방법을 배송없음, 고객센터 직접 반품 : N,T 으로 선택할 경우 [수거완료] 단계로 변경
                if(reqSetDto.getClaimPickShippingModifyDto().getPickShipType().equalsIgnoreCase("N") ||
                reqSetDto.getClaimPickShippingModifyDto().getPickShipType().equalsIgnoreCase("T")){
                    reqSetDto.getClaimPickShippingModifyDto().setPickStatus("P3");
                }
                reqSetDto.getClaimPickShippingModifyDto().setChgId(reqSetDto.getChgId());

                /* 2.히스토리 등록용 변경전 수거정보 조회 */
                ClaimPickShippingDto beforeClaimPickShippingDto = claimMapperService.getClaimPickShippingInfo(reqSetDto.getClaimBundleNo());

                /* 3.수거정보 변경 */
                claimMapperService.setClaimPickShipping(reqSetDto.getClaimPickShippingModifyDto());

                /* 3-1. 안심번호 수정 요청 */
                if(reqSetDto.getClaimPickShippingModifyDto().getPickMobileNo() != null && beforeClaimPickShippingDto.getSafetyUseYn().equalsIgnoreCase("Y")){
                    if(!beforeClaimPickShippingDto.getPickMobileNo().equalsIgnoreCase(reqSetDto.getClaimPickShippingModifyDto().getPickMobileNo())){
                        modifyClaimSafetyNo(reqSetDto.getClaimBundleNo(), "R", reqSetDto.getClaimPickShippingModifyDto().getPickMobileNo());
                    }
                }

                /* 4.ProcessHistory insert */
                try{
                    this.setPickShippingHistory(beforeClaimPickShippingDto, reqSetDto);
                }catch (Exception e){
                    log.debug("반품 배송정보 변경 히스토리 등록 실패 :::: {}", e);
                }
                break;
            //교환 발송정보 변경
            case "E" :
                /* 2.히스토리 등록용 변경전 배송정보 조회 */
                ClaimExchShippingDto beforeExchShippingDto = claimMapperService.getClaimExchShippingInfo(reqSetDto.getClaimBundleNo());
                reqSetDto.getClaimExchShippingModifyDto().setChgId(reqSetDto.getChgId());

                // 교환 배송시작일 경우 메시지 발송
                if(reqSetDto.getClaimExchShippingModifyDto().getExchStatus().equals("D1") && beforeExchShippingDto.getExchStatusCode().equals("D0")){
                    MessageSendUtil.sendExchangeShipInfo(reqSetDto.getClaimNo());
                }

                /* 3.배송정보 변경 */
                claimMapperService.setClaimExchShipping(reqSetDto.getClaimExchShippingModifyDto());

                /* 3-1.교환배송완료 요청시 수거완료 처리 */
                if(reqSetDto.getClaimExchShippingModifyDto().getExchStatus().equalsIgnoreCase("D3")){
                    this.reqPickShippingComplete(reqSetDto);
                }

                /* 3-2. 안심번호 수정 요청 */
                if(reqSetDto.getClaimExchShippingModifyDto().getExchMobileNo() != null && beforeExchShippingDto.getSafetyUseYn().equalsIgnoreCase("Y")){
                    if(!beforeExchShippingDto.getExchMobileNo().equalsIgnoreCase(reqSetDto.getClaimExchShippingModifyDto().getExchMobileNo())){
                        modifyClaimSafetyNo(reqSetDto.getClaimBundleNo(), "X", reqSetDto.getClaimExchShippingModifyDto().getExchMobileNo());
                    }
                }
                /* 4.ProcessHistory insert */
                try{
                    this.setExchShippingHistory(beforeExchShippingDto, reqSetDto);
                }catch (Exception e){
                    log.debug("교환 발송정보 변경 히스토리 등록 실패 :::: {}", e);
                }

                break;
            default:
                break;
        }
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS);
    }

    /**
     * 수거상태 변경
     * **/
    public ResponseObject<Boolean> reqClaimShippingStatus(ClaimShippingStatusSetDto reqSetDto) throws Exception {
        Boolean result = true;

        /* 1.수거 상태 조회 */
        ClaimPickShippingDto claimPickShippingDto = claimMapperService.getClaimPickShippingInfo(Long.parseLong(reqSetDto.getClaimBundleNo()));

        // 마켓이면서 교환인 경우에는 N마트로 수거완료를 요청해야함.
        if(marketClaimService.isOrderMarketCheck(ObjectUtils.toLong(reqSetDto.getClaimNo())) && !"P3".equals(claimPickShippingDto.getPickStatusCode()) && "P3".equals(reqSetDto.getPickStatus())) {
            log.info("마켓연동상품에 대한 교환처리를 합니다.");
            marketClaimService.setMarketCollectedExchange(ObjectUtils.toLong(reqSetDto.getClaimNo()));
        }

        /* 2.수거상태 변경 */
        if(!claimPickShippingDto.getPickStatusCode().equalsIgnoreCase(reqSetDto.getPickStatus())){
            result = claimMapperService.setClaimPickShipping(
                ClaimPickShippingModifyDto.builder()
                    .modifyType("STATUS")
                    .claimBundleNo(reqSetDto.getClaimBundleNo())
                    .pickStatus(reqSetDto.getPickStatus())
                    .chgId(reqSetDto.getChgId())
                    .build()
            );
            /* 3.ProcessHistory insert */
            processHistoryUtills.setProcessHistory(reqSetDto, this.getPickStatusText(reqSetDto.getPickStatus()), this.getPickShipTypeText(claimPickShippingDto, claimPickShippingDto.getShipType(), claimPickShippingDto.getShipMethod(), Long.parseLong(reqSetDto.getClaimNo())),null, reqSetDto.getChgId());
        }

        return OrderResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_STATUS_CHANGE_SUCCESS, result);
    }


    private void reqPickShippingComplete(ClaimShippingSetDto reqSetDto) throws Exception {

        ClaimShippingStatusSetDto claimShippingStatusSetDto = new ClaimShippingStatusSetDto();
        claimShippingStatusSetDto.setClaimBundleNo(String.valueOf(reqSetDto.getClaimBundleNo()));
        claimShippingStatusSetDto.setClaimNo(String.valueOf(reqSetDto.getClaimNo()));
        claimShippingStatusSetDto.setChgId(reqSetDto.getChgId());
        claimShippingStatusSetDto.setPickStatus("P3");

        /* 3.수거 완료 처리 */
        if(!this.reqClaimShippingStatus(claimShippingStatusSetDto).getData()){
            throw new LogicException(ClaimResponseCode.CLAIM_SHIPPING_STATUS_CHANGE_ERR01);
        }

    }

    public void modifyClaimSafetyNo(long claimBundleNo, String issueType, String reqPhoneNo) {
        ClaimSafetySetDto claimSafetySetDto = new ClaimSafetySetDto();
        claimSafetySetDto.setClaimBundleNo(claimBundleNo);
        claimSafetySetDto.setClaimType(issueType);
        claimSafetySetDto.setReqPhoneNo(reqPhoneNo);

        try {
            Integer resultData = externalService.postTransfer(
                ExternalInfo.CLAIM_SAFETY_ISSUE_MODIFY,
                claimSafetySetDto,
                Integer.class
           );
            log.debug("안심번호 변경 요청 result ::: {}", resultData);
        } catch (Exception e) {
            log.error("안심번호 변경 요청 ERROR ::: {}", e.getMessage());
        }
    }

    public void setPickShippingHistory(ClaimPickShippingDto beforePickShippingDto, ClaimShippingSetDto reqSetDto) throws Exception {

        String historyReason;
        String historyDetailDesc;
        String historyDetailBefore;

        /* 1.수거 상태 조회 */
        ClaimPickShippingDto afterClaimPickShippingDto = claimMapperService.getClaimPickShippingInfo(reqSetDto.getClaimBundleNo());
        reqSetDto.getClaimPickShippingModifyDto().setClaimNo(reqSetDto.getClaimNo());

        //[수거지 이름] 다른 경우
        if(!beforePickShippingDto.getPickReceiverNm().equalsIgnoreCase(afterClaimPickShippingDto.getPickReceiverNm())){
            processHistoryUtills
                .setProcessHistory(reqSetDto.getClaimPickShippingModifyDto(), "수거지-이름 변경", reqSetDto.getClaimPickShippingModifyDto().getPickReceiverNm(), beforePickShippingDto.getPickReceiverNm(), reqSetDto.getChgId());
        }

        //[수거지 주소] 다른 경우
        if(!beforePickShippingDto.getPickAddr().equalsIgnoreCase(afterClaimPickShippingDto.getPickAddr())){
            processHistoryUtills
                .setProcessHistory(reqSetDto.getClaimPickShippingModifyDto(), "수거지-주소 변경", reqSetDto.getClaimPickShippingModifyDto().getPickRoadBaseAddr(), beforePickShippingDto.getPickAddr(), reqSetDto.getChgId());
        }

        //[수거지 상세 주소] 다른 경우
        if(!beforePickShippingDto.getPickAddrDetail().equalsIgnoreCase(afterClaimPickShippingDto.getPickAddrDetail())){
            processHistoryUtills
                .setProcessHistory(reqSetDto.getClaimPickShippingModifyDto(), "수거지-상세 주소 변경", reqSetDto.getClaimPickShippingModifyDto().getPickDetailAddr(), beforePickShippingDto.getPickAddrDetail(), reqSetDto.getChgId());
        }

        //[수거지 연락처] 다른 경우
        if(!beforePickShippingDto.getPickMobileNo().equalsIgnoreCase(afterClaimPickShippingDto.getPickMobileNo())){
            processHistoryUtills
                .setProcessHistory(reqSetDto.getClaimPickShippingModifyDto(), "수거지-연락처 변경", reqSetDto.getClaimPickShippingModifyDto().getPickMobileNo(), beforePickShippingDto.getPickMobileNo(), reqSetDto.getChgId());
        }

        //[수거 방법] 다른 경우
        if((!beforePickShippingDto.getPickShipTypeCode().equalsIgnoreCase(afterClaimPickShippingDto.getPickShipTypeCode()))){
            historyReason = "수거 방법 변경";
            historyDetailDesc = this.getPickShipTypeText(afterClaimPickShippingDto, reqSetDto.getClaimPickShippingModifyDto().getShipType(), beforePickShippingDto.getShipMethod(), reqSetDto.getClaimNo());
            historyDetailBefore = this.getPickShipTypeText(beforePickShippingDto, reqSetDto.getClaimPickShippingModifyDto().getShipType(), beforePickShippingDto.getShipMethod(), reqSetDto.getClaimNo());
            processHistoryUtills
                .setProcessHistory(reqSetDto.getClaimPickShippingModifyDto(), historyReason, historyDetailDesc, historyDetailBefore, reqSetDto.getChgId());
        }else{
            switch (afterClaimPickShippingDto.getPickShipTypeCode()){
                case "C":
                    if(!String.valueOf(beforePickShippingDto.getPickDlvCd()).equalsIgnoreCase("null") && !String.valueOf(beforePickShippingDto.getPickInvoiceNo()).equalsIgnoreCase("null")){
                        if((!beforePickShippingDto.getPickDlvCd().equalsIgnoreCase(afterClaimPickShippingDto.getPickDlvCd()))
                        || (!beforePickShippingDto.getPickInvoiceNo().equalsIgnoreCase(afterClaimPickShippingDto.getPickInvoiceNo()))){
                            historyReason = "수거 방법 변경";
                            historyDetailDesc = this.getPickShipTypeText(afterClaimPickShippingDto, reqSetDto.getClaimPickShippingModifyDto().getShipType(), beforePickShippingDto.getShipMethod(), reqSetDto.getClaimNo());
                            historyDetailBefore = this.getPickShipTypeText(beforePickShippingDto, reqSetDto.getClaimPickShippingModifyDto().getShipType(), beforePickShippingDto.getShipMethod(), reqSetDto.getClaimNo());
                            processHistoryUtills.setProcessHistory(reqSetDto.getClaimPickShippingModifyDto(), historyReason, historyDetailDesc, historyDetailBefore, reqSetDto.getChgId());
                        }
                    }
                    break;
                case "D":
                    if(!String.valueOf(beforePickShippingDto.getShipDt()).equalsIgnoreCase("null")){
                        if(!beforePickShippingDto.getShipDt().equalsIgnoreCase(afterClaimPickShippingDto.getShipDt())){
                            historyReason = "수거 방법 변경";
                            historyDetailDesc = this.getPickShipTypeText(afterClaimPickShippingDto, reqSetDto.getClaimPickShippingModifyDto().getShipType(), beforePickShippingDto.getShipMethod(), reqSetDto.getClaimNo());
                            historyDetailBefore = this.getPickShipTypeText(beforePickShippingDto, reqSetDto.getClaimPickShippingModifyDto().getShipType(), beforePickShippingDto.getShipMethod(), reqSetDto.getClaimNo());
                            processHistoryUtills.setProcessHistory(reqSetDto.getClaimPickShippingModifyDto(), historyReason, historyDetailDesc, historyDetailBefore, reqSetDto.getChgId());
                        }
                    }
                    break;
                case "P":
                    if(!String.valueOf(beforePickShippingDto.getPickMemo()).equalsIgnoreCase("null")){
                        if (!beforePickShippingDto.getPickMemo().equalsIgnoreCase(afterClaimPickShippingDto.getPickMemo())){
                            historyReason = "수거 방법 변경";
                            historyDetailDesc = this.getPickShipTypeText(afterClaimPickShippingDto, reqSetDto.getClaimPickShippingModifyDto().getShipType(), beforePickShippingDto.getShipMethod(), reqSetDto.getClaimNo());
                            historyDetailBefore = this.getPickShipTypeText(beforePickShippingDto, reqSetDto.getClaimPickShippingModifyDto().getShipType(), beforePickShippingDto.getShipMethod(), reqSetDto.getClaimNo());
                            processHistoryUtills.setProcessHistory(reqSetDto.getClaimPickShippingModifyDto(), historyReason, historyDetailDesc, historyDetailBefore, reqSetDto.getChgId());
                        }
                    }
                    break;
            }
        }

        //[수거 상태 변경]
        if((!beforePickShippingDto.getPickStatusCode().equalsIgnoreCase(afterClaimPickShippingDto.getPickStatusCode()))){
            historyReason = this.getPickStatusText(afterClaimPickShippingDto.getPickStatusCode());
            historyDetailDesc = this.getPickShipTypeText(afterClaimPickShippingDto, reqSetDto.getClaimPickShippingModifyDto().getShipType(), beforePickShippingDto.getShipMethod(), reqSetDto.getClaimNo());
            processHistoryUtills
                .setProcessHistory(reqSetDto.getClaimPickShippingModifyDto(), historyReason, historyDetailDesc, null, reqSetDto.getChgId());
        }
    }

    public void setExchShippingHistory(ClaimExchShippingDto beforeExchShippingDto, ClaimShippingSetDto reqSetDto) throws Exception {

        String historyReason;
        String historyDetailDesc;
        String historyDetailBefore;

        /* 1.수거 상태 조회 */
        ClaimExchShippingDto afterExchShippingDto = claimMapperService.getClaimExchShippingInfo(reqSetDto.getClaimBundleNo());
        reqSetDto.getClaimExchShippingModifyDto().setClaimNo(reqSetDto.getClaimNo());

        //[배송지 이름] 다른 경우
        if(beforeExchShippingDto.getExchReceiverNm() != null &&  afterExchShippingDto.getExchReceiverNm() != null){
            if(!beforeExchShippingDto.getExchReceiverNm().equalsIgnoreCase(afterExchShippingDto.getExchReceiverNm())){
                processHistoryUtills
                    .setProcessHistory(reqSetDto.getClaimExchShippingModifyDto(), "배송지-이름 변경", afterExchShippingDto.getExchReceiverNm(), beforeExchShippingDto.getExchReceiverNm(), reqSetDto.getChgId());
            }
        }

        //[배송지 주소] 다른 경우
        if(!beforeExchShippingDto.getExchAddr().equalsIgnoreCase(afterExchShippingDto.getExchAddr())){
            processHistoryUtills
                .setProcessHistory(reqSetDto.getClaimExchShippingModifyDto(), "배송지-주소 변경", afterExchShippingDto.getExchAddr(), beforeExchShippingDto.getExchAddr(), reqSetDto.getChgId());
        }

        //[배송지 상세 주소] 다른 경우
        if(!beforeExchShippingDto.getExchAddrDetail().equalsIgnoreCase(afterExchShippingDto.getExchAddrDetail())){
            processHistoryUtills
                .setProcessHistory(reqSetDto.getClaimExchShippingModifyDto(), "배송지-상세 주소 변경", afterExchShippingDto.getExchAddrDetail(), beforeExchShippingDto.getExchAddrDetail(), reqSetDto.getChgId());

        }

        //[배송지 연락처] 다른 경우
        if(!beforeExchShippingDto.getExchMobileNo().equalsIgnoreCase(afterExchShippingDto.getExchMobileNo())){
            processHistoryUtills
                .setProcessHistory(reqSetDto.getClaimExchShippingModifyDto(), "배송지-연락처 변경", afterExchShippingDto.getExchMobileNo(), beforeExchShippingDto.getExchMobileNo(), reqSetDto.getChgId());
        }


        //[배송 방법] 다른 경우
        if((!beforeExchShippingDto.getExchShipTypeCode().equalsIgnoreCase(afterExchShippingDto.getExchShipTypeCode()))){
            historyReason = "배송 방법 변경";
            historyDetailDesc = this.getExchShipTypeText(afterExchShippingDto, reqSetDto.getClaimBundleNo());
            historyDetailBefore = this.getExchShipTypeText(beforeExchShippingDto, reqSetDto.getClaimBundleNo());
            processHistoryUtills
                .setProcessHistory(reqSetDto.getClaimExchShippingModifyDto(), historyReason, historyDetailDesc, historyDetailBefore, reqSetDto.getChgId());
        }else{

            switch (afterExchShippingDto.getExchShipTypeCode()){
                case "C":
                    if(!String.valueOf(beforeExchShippingDto.getExchDlvCd()).equalsIgnoreCase("null") && !String.valueOf(beforeExchShippingDto.getExchInvoiceNo()).equalsIgnoreCase("null")){
                        if(!String.valueOf(beforeExchShippingDto.getExchDlvCd()).equalsIgnoreCase(afterExchShippingDto.getExchDlvCd()) || !String.valueOf(beforeExchShippingDto.getExchInvoiceNo()).equalsIgnoreCase(afterExchShippingDto.getExchInvoiceNo())){
                            historyReason = "배송 방법 변경";
                            historyDetailDesc = this.getExchShipTypeText(afterExchShippingDto, reqSetDto.getClaimBundleNo());
                            historyDetailBefore = this.getExchShipTypeText(beforeExchShippingDto, reqSetDto.getClaimBundleNo());
                            processHistoryUtills.setProcessHistory(reqSetDto.getClaimExchShippingModifyDto(), historyReason, historyDetailDesc, historyDetailBefore, reqSetDto.getChgId());
                        }
                    }
                    break;
                case "D":
                    if(!String.valueOf(beforeExchShippingDto.getExchD0Dt()).equalsIgnoreCase("null")){
                        if(!String.valueOf(beforeExchShippingDto.getExchD0Dt()).equalsIgnoreCase(afterExchShippingDto.getExchD0Dt())){
                            historyReason = "배송 방법 변경";
                            historyDetailDesc = this.getExchShipTypeText(afterExchShippingDto, reqSetDto.getClaimBundleNo());
                            historyDetailBefore = this.getExchShipTypeText(beforeExchShippingDto, reqSetDto.getClaimBundleNo());
                            processHistoryUtills.setProcessHistory(reqSetDto.getClaimExchShippingModifyDto(), historyReason, historyDetailDesc, historyDetailBefore, reqSetDto.getChgId());
                        }
                    }
                    break;
                case "P":
                    if(!String.valueOf(beforeExchShippingDto.getExchMemo()).equalsIgnoreCase("null")){
                        if (!String.valueOf(beforeExchShippingDto.getExchMemo()).equalsIgnoreCase(afterExchShippingDto.getExchMemo())){
                            historyReason = "배송 방법 변경";
                            historyDetailDesc = this.getExchShipTypeText(afterExchShippingDto, reqSetDto.getClaimBundleNo());
                            historyDetailBefore = this.getExchShipTypeText(beforeExchShippingDto, reqSetDto.getClaimBundleNo());
                            processHistoryUtills.setProcessHistory(reqSetDto.getClaimExchShippingModifyDto(), historyReason, historyDetailDesc, historyDetailBefore, reqSetDto.getChgId());
                        }
                    }
                    break;
            }
        }

        //[배송 상태 변경]
        if((!beforeExchShippingDto.getExchStatusCode().equalsIgnoreCase(afterExchShippingDto.getExchStatusCode()))){
            historyReason = this.getExchStatusText(afterExchShippingDto.getExchStatusCode());
            historyDetailDesc = this.getExchShipTypeText(afterExchShippingDto, reqSetDto.getClaimBundleNo());
            processHistoryUtills
                .setProcessHistory(reqSetDto.getClaimExchShippingModifyDto(), historyReason, historyDetailDesc, null, reqSetDto.getChgId());
        }
    }

    public String getPickShipTypeText(ClaimPickShippingDto claimPickShippingDto, String shipType, String shipMethod, long claimNo) {
        String claimShipType = null;

        if(shipType.equalsIgnoreCase("PICK")){
            if(claimPickShippingDto.getPickShipTypeCode().equalsIgnoreCase("N")){
                claimShipType = "수거안함";
            }else{
                claimShipType = "점포방문";
            }
        }else{
            if(shipMethod.equalsIgnoreCase("TD_DRCT")) {

                if (claimPickShippingDto.getPickShipTypeCode().equalsIgnoreCase("N")) {
                    claimShipType = "수거안함";
                } else if (claimPickShippingDto.getPickShipTypeCode().equalsIgnoreCase("T")) {
                    claimShipType = "고객센터 직접 반품";
                } else {
                    String tdShipDt = null;
                    ClaimReasonInfoGetDto claimReasonInfoGetDto = claimMapperService
                        .getReturnExchangeReasonInfo(claimNo);
                    LinkedHashMap<String, String> slotResult = claimMapperService
                        .getSlotSiftInfo(claimPickShippingDto.getSlotId(),
                            claimPickShippingDto.getShiftId(), claimReasonInfoGetDto.getStoreType(),
                            claimReasonInfoGetDto.getStoreId());
                    if (slotResult != null) {
                        tdShipDt = "(" + slotResult.get("ship_weekday") + ")" + " " + slotResult
                            .get("slot_ship_start_time").substring(0, 2) + ":" + slotResult
                            .get("slot_ship_start_time").substring(2, 4)
                            + " ~ " + slotResult.get("slot_ship_end_time").substring(0, 2) + ":"
                            + slotResult.get("slot_ship_end_time").substring(2, 4);
                    }
                    claimShipType = "자차수거 : " + tdShipDt;
                }

            }else if(shipMethod.equalsIgnoreCase("TD_QUICK")){
                if (claimPickShippingDto.getPickShipTypeCode().equalsIgnoreCase("N")) {
                    claimShipType = "수거안함";
                } else {
                    claimShipType = "퀵수거";
                }
            }else{

                switch (claimPickShippingDto.getPickShipTypeCode()) {
                    case "C":
                        claimShipType = claimPickShippingDto.getPickDlv() == null ? "택배 : -" : "택배 : " + claimPickShippingDto.getPickDlv() + " | " + claimPickShippingDto.getPickInvoiceNo();
                        break;
                    case "D":
                        claimShipType = claimPickShippingDto.getShipDt() ==null ? "직접수거 : -" : "직접수거 : " + claimPickShippingDto.getShipDt();
                        break;
                    case "P":
                        claimShipType = claimPickShippingDto.getPickMemo() == null ? "우편 : -" : "우편 : " + claimPickShippingDto.getPickMemo();
                        break;
                    case "N":
                        claimShipType = "수거안함";
                        break;
                    default:
                        claimShipType = "-";
                        break;
                }
            }
        }
        return claimShipType;
    }


    public String getExchShipTypeText(ClaimExchShippingDto claimExchShippingDto, long claimBundleNo) {
        String exchShipTypeText = null;
        ClaimPickShippingDto claimPickShippingDto = claimMapperService.getClaimPickShippingInfo(claimBundleNo);

        if(claimPickShippingDto.getShipType().equalsIgnoreCase("PICK")){
            if(claimExchShippingDto.getExchShipTypeCode().equalsIgnoreCase("N")){
                exchShipTypeText = "배송없음";
            }else{
                exchShipTypeText = "점포방문";
            }
        }else{
            if(claimPickShippingDto.getShipMethod().equalsIgnoreCase("TD_DRCT")){
                if(claimExchShippingDto.getExchShipTypeCode().equalsIgnoreCase("N")){
                    exchShipTypeText = "배송없음";
                }else{
                    exchShipTypeText = "자차배송";
                }
            }else if(claimPickShippingDto.getShipMethod().equalsIgnoreCase("TD_QUICK")){
                if (claimExchShippingDto.getExchShipTypeCode().equalsIgnoreCase("N")) {
                    exchShipTypeText = "배송없음";
                } else {
                    exchShipTypeText = "퀵";
                }
            }else{

                switch (claimExchShippingDto.getExchShipTypeCode()) {
                    case "C":
                        exchShipTypeText = claimExchShippingDto.getExchDlv() == null ? "택배 : -" : "택배 : " +  claimExchShippingDto.getExchDlv() + " | " + claimExchShippingDto.getExchInvoiceNo();
                        break;
                    case "D":
                        exchShipTypeText = claimExchShippingDto.getExchD0Dt() == null ? "직접배송 : -" : "직접배송 : " + claimExchShippingDto.getExchD0Dt().substring(0,10);
                        break;
                    case "P":
                        exchShipTypeText = claimExchShippingDto.getExchMemo() == null ? "우편배송 : -" : "우편배송 : " + claimExchShippingDto.getExchMemo();
                        break;
                    case "N":
                        exchShipTypeText = "배송없음";
                        break;
                    default:
                        exchShipTypeText = "-";
                        break;
                }
            }
        }
        return exchShipTypeText;
    }

    public String getPickStatusText(String pickStatus) {
        String pickStatusText = null;
        // 상태에 대한 확인
        switch (pickStatus.toUpperCase(Locale.KOREA)){
            // 수거대기
            case "NN" : case "N0" :
                pickStatusText = ClaimCode.CLAIM_PICKUP_STATUS_N0.getDescription();
                break;
            // 수거요청
            case "P0" : case "P1" :
                pickStatusText = ClaimCode.CLAIM_PICKUP_STATUS_P0.getDescription();
                break;
            // 수거중
            case "P2" :
                pickStatusText = ClaimCode.CLAIM_PICKUP_STATUS_P2.getDescription();
                break;
            // 수거완료
            case "P3" :
                pickStatusText = ClaimCode.CLAIM_PICKUP_STATUS_P3.getDescription();
                break;
            // 수거실패
            case "P8" :
                pickStatusText = ClaimCode.CLAIM_PICKUP_STATUS_P8.getDescription();
                break;
            default:
                pickStatusText = "";
                break;
        }
        return pickStatusText;
    }

    public String getExchStatusText(String exchStatus) {
        String exchStatusText = null;
        // 상태에 대한 확인
        switch (exchStatus.toUpperCase(Locale.KOREA)){
            // 수거대기
            case "D0" :
                exchStatusText = ClaimCode.CLAIM_EXCH_STATUS_D0.getDescription();
                break;
            // 수거요청
            case "D1" :
                exchStatusText = ClaimCode.CLAIM_EXCH_STATUS_D1.getDescription();
                break;
            // 수거중
            case "D2" :
                exchStatusText = ClaimCode.CLAIM_EXCH_STATUS_D2.getDescription();
                break;
            // 수거완료
            case "D3" :
                exchStatusText = ClaimCode.CLAIM_EXCH_STATUS_D3.getDescription();
                break;
            default:
                exchStatusText = "";
                break;
        }
        return exchStatusText;
    }


}
