package kr.co.homeplus.escrowmng.delivery.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.delivery.model.StoreSlotManageModel;

@EscrowMasterConnection
public interface StoreShiftSlotMasterMapper {
    int updateStoreSlot(StoreSlotManageModel storeSlotManageModel);
}
