package kr.co.homeplus.escrowmng.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import kr.co.homeplus.escrowmng.enums.DateType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DateUtils {
    // LocalDate
    public static final String DIGIT_YMD = "yyyyMMdd";

    public static final String DEFAULT_YMD = "yyyy-MM-dd";
    // LocalDateTime
    public static final String DIGIT_YMD_HIS = "yyyyMMddHHmmss";

    public static final String DEFAULT_YMD_HIS = "yyyy-MM-dd HH:mm:ss";

    public static final String MILL_YMD_HIS = "yyyy-MM-dd HH:mm:ss.SSS";

    public static String getCurrentYmd() {
        return LocalDate.now().format(getCreateDateTimeFormatter(DEFAULT_YMD));
    }

    public static String getCurrentYmd(String format) {
        return LocalDate.now().format(getCreateDateTimeFormatter(format));
    }

    public static boolean isBefore(String dateTime){
        return setNow().isBefore(setDateTime(dateTime));
    }

    public static boolean isBefore(String dateTime, DateType dateType, int value) {
        return setNow().isBefore(setCalculationTime(dateTime, dateType, value));
    }

    public static boolean isAfter(String dateTime){
        return setNow().isAfter(setDateTime(dateTime));
    }

    public static boolean isAfter(String dateTime, DateType dateType, int value) {
        return setNow().isAfter(setCalculationTime(dateTime, dateType, value));
    }

    public static boolean isBetween(String dateTime, DateType dateType, int start, int end) {
        return isAfter(dateTime, dateType, start) && isBefore(dateTime, dateType, end);
    }

    public static String createDateLocalTime(String date, String time) {
        LocalDate localDate = LocalDate.parse(date);
        LocalTime localTime = LocalTime.parse(time);
        return LocalDateTime.of(localDate, localTime).format(getDefaultDate(null));
    }

    public static String setDateTime(String dateTime, DateType dateType, int value) {
        return setCalculationTime(dateTime, dateType, value).format(getDefaultDate(dateTime));
    }

    public static String customDateStr(String dateTime, String customPattern) {
        return setDateTime(dateTime).format(getCreateDateTimeFormatter(customPattern));
    }

    private static LocalDateTime setCalculationTime(String dateTime, DateType dateType, int value) {
        LocalDateTime localDateTime = setLocalDateTime(dateTime);
        switch (dateType){
            case YEAR :
                return setYear(localDateTime, value);
            case MONTH :
                return setMonth(localDateTime, value);
            case DAY :
                return setDay(localDateTime, value);
            case HOUR :
                return setHourTime(localDateTime, value);
            case MIN :
                return setMinuteTime(localDateTime, value);
            case SEC :
                return setSecondTime(localDateTime, value);
            default :
                return localDateTime;
        }
    }

    private static LocalDateTime setLocalDateTime(String dateTime){
        if(dateTime == null){
            return setNow();
        }
        return setDateTime(dateTime);
    }

    private static LocalDateTime setDateTime(String dateTime) {
        dateTime = dateTime.replaceAll("\\.", "-");
        if(dateTime.length() > 10){
            return LocalDateTime.parse(dateTime, getDefaultDate(dateTime));
        }
        return LocalDateTime.of(LocalDate.parse(dateTime), LocalTime.of(0, 0, 0));
    }

    private static DateTimeFormatter getDefaultDate(String dateTime) {
        if(dateTime == null || dateTime.length() > 10){
            return getCreateDateTimeFormatter(DEFAULT_YMD_HIS);
        }
        return getCreateDateTimeFormatter(DEFAULT_YMD);
    }

    private static DateTimeFormatter getCreateDateTimeFormatter(String pattern) {
        return DateTimeFormatter.ofPattern(pattern, Locale.KOREA);
    }

    private static LocalDateTime setYear(LocalDateTime localDateTime, int value) {
        return localDateTime.plusYears(value);
    }

    private static LocalDateTime setMonth(LocalDateTime localDateTime, int value) {
        return localDateTime.plusMonths(value);
    }

    private static LocalDateTime setDay(LocalDateTime localDateTime, int value) {
        return localDateTime.plusDays(value);
    }

    private static LocalDateTime setHourTime(LocalDateTime localDateTime, int value) {
        return localDateTime.plusHours(value);
    }

    private static LocalDateTime setMinuteTime(LocalDateTime localDateTime, int value) {
        return localDateTime.plusMinutes(value);
    }

    private static LocalDateTime setSecondTime(LocalDateTime localDateTime, int value) {
        return localDateTime.plusSeconds(value);
    }

    private static LocalDateTime setNow() {
        return LocalDateTime.now();
    }

}
