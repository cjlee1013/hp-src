package kr.co.homeplus.escrowmng.escrow.service;

import kr.co.homeplus.escrowmng.constants.ResourceRouteName;
import kr.co.homeplus.escrowmng.enums.ApiName;
import kr.co.homeplus.escrowmng.escrow.model.ClaimSafetyIssueRequest;
import kr.co.homeplus.escrowmng.escrow.model.MultiSafetyIssueRequest;
import kr.co.homeplus.escrowmng.escrow.model.SafetyIssueRequest;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class SafetyIssueService {

    private final ResourceClient resourceClient;

    /**
     * 안심번호 변경 등록 By 배송지주소번호(shipAddrNo) 또는 번들번호(bundleNo)
     * @param safetyIssueRequest
     * @return
     */
    public ResponseObject<Integer> updateSafetyIssueForChange(SafetyIssueRequest safetyIssueRequest) throws Exception {
        ResourceClientRequest<Integer> request = ResourceClientRequest.<Integer>postBuilder()
            .apiId(ResourceRouteName.ESCROW)
            .uri(ApiName.UPDATE_SAFETY_NO.getApiUri())
            .postObject(safetyIssueRequest) // RestTemplate 단에서 자동으로 json으로 변환 되어 서버로 날아갑니다.
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        ResponseObject responseObject = resourceClient.post(request, new TimeoutConfig()).getBody();
        return responseObject;
    }

    /**
     * 클레임 안심번호 변경 등록
     * @param claimSafetyIssueRequest
     * @return
     */
    public ResponseObject<Integer> updateClaimSafetyIssueForChange(ClaimSafetyIssueRequest claimSafetyIssueRequest) throws Exception {
        ResourceClientRequest<Integer> request = ResourceClientRequest.<Integer>postBuilder()
            .apiId(ResourceRouteName.ESCROW)
            .uri(ApiName.UPDATE_CLAIM_SAFETY_NO.getApiUri())
            .postObject(claimSafetyIssueRequest) // RestTemplate 단에서 자동으로 json으로 변환 되어 서버로 날아갑니다.
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        ResponseObject responseObject = resourceClient.post(request, new TimeoutConfig()).getBody();
        return responseObject;
    }

    /**
     * 안심번호 변경 등록 By 디증배송번호(multiBundleNo)
     * @param multiSafetyIssueRequest
     * @return
     */
    public ResponseObject<Integer> updateMultiSafetyIssueForChange(MultiSafetyIssueRequest multiSafetyIssueRequest) throws Exception {
        ResourceClientRequest<Integer> request = ResourceClientRequest.<Integer>postBuilder()
            .apiId(ResourceRouteName.ESCROW)
            .uri(ApiName.UPDATE_MULTI_SAFETY_NO.getApiUri())
            .postObject(multiSafetyIssueRequest) // RestTemplate 단에서 자동으로 json으로 변환 되어 서버로 날아갑니다.
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        ResponseObject responseObject = resourceClient.post(request, new TimeoutConfig()).getBody();
        return responseObject;
    }

}
