package kr.co.homeplus.escrowmng.escrow.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.homeplus.escrowmng.escrow.model.ClaimSafetyIssueRequest;
import kr.co.homeplus.escrowmng.escrow.model.SafetyIssueRequest;
import kr.co.homeplus.escrowmng.escrow.service.SafetyIssueService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "안심번호 변경 등록", value = "안심번호 컨트롤러")
@RestController
@RequiredArgsConstructor
@RequestMapping("/safetyissue")
public class SafetyIssueController {

    private final SafetyIssueService safetyIssueService;

    @ApiOperation(value = "안심번호 변경 등록")
    @PostMapping(value = "/updateSafetyNo")
    public ResponseObject<Integer> updateSafetyNo(@RequestBody SafetyIssueRequest safetyIssueRequest) throws Exception {
        return safetyIssueService.updateSafetyIssueForChange(safetyIssueRequest);
    }

    @ApiOperation(value = "클레임 안심번호 변경 등록")
    @PostMapping(value = "/updateClaimSafetyNo")
    public ResponseObject<Integer> updateClaimSafetyNo(@RequestBody ClaimSafetyIssueRequest claimSafetyIssueRequest) throws Exception {
        return safetyIssueService.updateClaimSafetyIssueForChange(claimSafetyIssueRequest);
    }

}
