package kr.co.homeplus.escrowmng.order.model.managementPop;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "사은 행사정보 팝업 DTO")
public class OrderGiftInfoDto {
    @ApiModelProperty(value = "점포", position = 1)
    private String storeNm;
    @ApiModelProperty(value = "상품번호", position = 2)
    private String itemNo;
    @ApiModelProperty(value = "상품명", position = 3)
    private String itemNm1;
    @ApiModelProperty(value = "행사구분", position = 4)
    private String giftType;
    @ApiModelProperty(value = "행사번호", position = 5)
    private String giftNo;
    @ApiModelProperty(value = "행사명", position = 6)
    private String giftMsg;
}
