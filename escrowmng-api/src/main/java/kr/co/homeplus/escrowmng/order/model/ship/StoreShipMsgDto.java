package kr.co.homeplus.escrowmng.order.model.ship;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StoreShipMsgDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;
    @ApiModelProperty(value = "배송번", position = 2)
    private long bundleNo;
    @ApiModelProperty(value = "배송메시지", position = 3)
    private String storeShipMsg;
}
