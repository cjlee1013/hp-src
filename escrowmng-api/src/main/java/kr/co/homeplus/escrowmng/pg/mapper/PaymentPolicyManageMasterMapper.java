package kr.co.homeplus.escrowmng.pg.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyApplyDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyManageDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyMethodDto;

@EscrowMasterConnection
public interface PaymentPolicyManageMasterMapper {
    Long insertPaymentPolicyMng(PaymentPolicyManageDto paymentPolicyManageDto);
    int insertPaymentPolicyHistMethod(Long policyNo);
    int deletePaymentPolicyMethod(Long policyNo);
    int insertPaymentPolicyMethod(PaymentPolicyMethodDto paymentPolicyMethodDto);
    int insertPaymentPolicyHistApply(Long policyNo);
    int deletePaymentPolicyApply(Long policyNo);
    int insertPaymentPolicyApply(PaymentPolicyApplyDto paymentPolicyApplyDto);
}
