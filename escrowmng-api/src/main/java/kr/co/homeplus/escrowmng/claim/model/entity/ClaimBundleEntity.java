package kr.co.homeplus.escrowmng.claim.model.entity;

public class ClaimBundleEntity {
    // 클레임 묶음 배송번호
    public String claimBundleNo;
    // 클레임번호(환불:클레임번호)
    public String claimNo;
    // 결제번호
    public String paymentNo;
    // 주문번호
    public String purchaseOrderNo;
    // 배송번호
    public String bundleNo;
    // 클레임 타입(C: 취소 R: 반품 X: 교환)
    public String claimType;
    // 클레임 상태(C1: 신청/ C2: 승인/ C3: 완료/ C4: 철회/ C8: 보류/ C9: 거부)
    public String claimStatus;
    // 파트너 아이디
    public String partnerId;
    // 고객번호
    public String userNo;
    // 클레임수량
    public String claimBundleQty;
    // 원 클레임 묶음번호(교환/반품 전환시)
    public String originClaimBundleNo;
    // 클레임배송비 처리방법 Y:상품에 동봉인 경우 claim_addition 항목 중 pickup, delivery 는 is_apply-> N 설정 Y:상품에 동봉, N:환불금액 차감
    public String claimShipFeeEnclose;
    // Admin에서만 설정이 가능  클레임배송비 계좌입금여부  Y:계좌입금, N:계좌입금 아님
    public String claimShipFeeBank;
    // 물류센터
    public String warehouse;
    // 할인쿠폰 미차감 여부 (Y:미차감, N:차감)
    public String isNoDiscountDeduct;
    // 배송비 미차감 여부(Y:미차감, N:차감 (delivery 만 적용))
    public String isNoShipDeduct;
    // 배송모바일번호(휴대폰번호)
    public String shipMobileNo;
    // 예약상품여부
    public String reserveYn;
    // 주문구분
    public String orderCategory;
    // 슬롯차감전송여부(order_category 가 TD/PICK 인 경우에만 대상. 그외의 건은 모두 Y
    public String closeSendYn;
    // 등록일
    public String regDt;
    // 수정일
    public String chgDt;
    // 마켓연동주문번호
    public String marketOrderNo;
}
