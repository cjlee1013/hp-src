package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 대사 > PG 승인대사 조회 > PG 승인대사 차이내역 DTO")
public class PgCompareApprovalDiffDto {

    @ApiModelProperty(notes = "PG종류(TOSSPG,INICIS)", required = true, position = 1)
    private String pgKind;

    @ApiModelProperty(notes = "PG상점ID", position = 2)
    private String pgMid;

    @ApiModelProperty(notes = "결제일", position = 3)
    private String paymentDt;

    @ApiModelProperty(notes = "주문번호", position = 4)
    private long purchaseOrderNo;

    @ApiModelProperty(notes = "거래구분코드", position = 5)
    private String tradeTypeCd;

    @ApiModelProperty(notes = "거래구분명", position = 5)
    private String tradeTypeNm;

    @ApiModelProperty(notes = "결제금액(홈플러스)", position = 6)
    private long paymentAmt;

    @ApiModelProperty(notes = "결제금액(PG)", position = 7)
    private long pgPaymentAmt;

    @ApiModelProperty(notes = "차이사유코드", position = 8)
    private String diffReasonCd;

    @ApiModelProperty(notes = "차이사유명", position = 8)
    private String diffReasonNm;

    @ApiModelProperty(notes = "PG거래ID", position = 9)
    private String pgTid;

    @ApiModelProperty(notes = "PG결과코드", position = 10)
    private String pgResultCd;

    @ApiModelProperty(notes = "결제수단(홈플러스)", position = 11)
    private String paymentMethod;

    @ApiModelProperty(notes = "결제수단(PG)", position = 12)
    private String pgPaymentMethod;

    @ApiModelProperty(notes = "클레임번호", position = 13)
    private long claimNo;

}
