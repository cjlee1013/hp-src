package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "PG 수수료 관리")
@Getter
@Setter
@EqualsAndHashCode
public class PgCommissionManageDto {
    @ApiModelProperty(value = "PG수수료관리순번", position = 1)
    private Long pgCommissionMngSeq;

    @ApiModelProperty(value = "PG종류(INICIS,TOSSPG)", position = 2)
    private String pgKind;

    @ApiModelProperty(value = "상위결제수단코드", position = 3)
    private String parentMethodCd;

    @ApiModelProperty(value = "수수료관리명", position = 4)
    @AllowInput(types = {PatternConstants.NUM, PatternConstants.ENG, PatternConstants.KOR}, message = "수수료관리명")
    private String commissionMngNm;

    @ApiModelProperty(value = "시작일시(YYYY-MM-DD 00:00:00)", position = 5)
    private String startDt;

    @ApiModelProperty(value = "종료일시(YYYY-MM-DD 23:59:59)", position = 6)
    private String endDt;

    @ApiModelProperty(value = "정산주기(DAY:일정산,WEEK:주정산,EARLY_MONTH:익월초,END_MONTH:익월말,END_NEXT_MONTH:익익월말)", position = 7)
    private String costPeriod;

    @ApiModelProperty(value = "정산일(정산주기 DAY 경우만 입력됨)", position = 8)
    private Integer costDay;

    @ApiModelProperty(value = "소수점처리(UP:올림,DOWN,버림,ROUND:반올림)", position = 9)
    private String decpntProcess;

    @ApiModelProperty(value = "부가세여부(Y:포함,N:포함안함)", position = 10)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "부가세여부(Y|N)")
    private String vatYn;

    @ApiModelProperty(value = "사용여부", position = 11)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "사용여부(Y|N)")
    private String useYn;

    @ApiModelProperty(value = "등록자ID", position = 12)
    private String regId;

    @ApiModelProperty(value = "등록일자", position = 13)
    private String regDt;

    @ApiModelProperty(value = "수정자ID", position = 14)
    private String chgId;

    @ApiModelProperty(value = "수정일자", position = 15)
    private String chgDt;

    @ApiModelProperty(value = "PG수수료대상목록", position = 16)
    private List<PgCommissionTargetDto> pgCommissionTargetDtoList;
}
