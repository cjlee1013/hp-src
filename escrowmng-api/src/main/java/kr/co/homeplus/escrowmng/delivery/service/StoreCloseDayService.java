package kr.co.homeplus.escrowmng.delivery.service;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.construct.TransactionManagerName;
import kr.co.homeplus.escrowmng.delivery.mapper.StoreCloseDayMasterMapper;
import kr.co.homeplus.escrowmng.delivery.mapper.StoreCloseDaySlaveMapper;
import kr.co.homeplus.escrowmng.delivery.model.StoreCloseDayImageModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreCloseDayModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreCloseDaySelectModel;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class StoreCloseDayService {
    private final StoreCloseDaySlaveMapper storeCloseDaySlaveMapper;
    private final StoreCloseDayMasterMapper storeCloseDayMasterMapper;

    /**
     * 점포별 휴일 리스트 조회
     * @param storeCloseDaySelectModel
     * @return
     */
    public List<StoreCloseDayModel> getStoreCloseDayList(StoreCloseDaySelectModel storeCloseDaySelectModel) {
        return storeCloseDaySlaveMapper.selectStoreCloseDayList(storeCloseDaySelectModel);
    }

    /**
     * 휴일 이미지 리스트 조회
     * @return
     */
    public List<StoreCloseDayImageModel> getCloseDayImageList() {
        return storeCloseDaySlaveMapper.selectCloseDayImageList();
    }

    /**
     * 휴일 이미지 저장
     * @param storeCloseDayImageModel
     * @return
     * @throws Exception
     */
    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int saveCloseDayImageList(StoreCloseDayImageModel storeCloseDayImageModel) throws Exception {
        int rtnVal = 0;

        try {
            if (EscrowString.isNotEmpty(storeCloseDayImageModel.getImageList())) {
                // loop 수행하면서 1건씩 업데이트
                for (StoreCloseDayImageModel storeCloseDayImage : storeCloseDayImageModel.getImageList()) {
                    storeCloseDayImage.setChgId(storeCloseDayImageModel.getChgId());
                    rtnVal += storeCloseDayMasterMapper.updateCloseDayImageList(storeCloseDayImage);
                }
            }
            return rtnVal;
        } catch (Exception e) {
            log.error("error:", e);
            rtnVal = -1;
            return rtnVal;
        } finally {
            log.info("saveCloseDayImageList-resultCount:" + rtnVal);
        }
    }
}
