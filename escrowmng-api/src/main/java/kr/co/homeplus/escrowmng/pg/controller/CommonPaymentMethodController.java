package kr.co.homeplus.escrowmng.pg.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodDto;
import kr.co.homeplus.escrowmng.pg.service.CommonPaymentMethodService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pg/paymentMethod")
@Api(tags = "결제수단별 SiteType 에 따라 반환 함")
public class CommonPaymentMethodController {

    private final CommonPaymentMethodService commonPaymentMethodService;

    /**
     * 상위결제수단 조회 후 중복되는 methodCd를 제거 후 결제수단 종류를 반환
     * @param parentMethodCd
     * @return
     * @throws Exception
     */
    @GetMapping("/getDistinctMethodCd")
    @ApiOperation(value = "상위결제수단코드기준으로 SiteType중복 제거", response = PaymentMethodDto.class)
    public ResponseObject getDistinctMethodCd(
        @ApiParam(name = "parentMethodCd", value = "상위결제수단코드(CARD:신용카드,RBANK:실시간계좌이체)", required = true) @RequestParam String parentMethodCd
    ) throws Exception{
        List<PaymentMethodDto> paymentMethodDtoList = commonPaymentMethodService.getDistinctMethodCd(parentMethodCd);
        return ResponseObject.Builder.<List<PaymentMethodDto>>builder().data(paymentMethodDtoList).build();
    }

    /**
     * 상위결제수단 조회 후 siteType 기준으로 리스트 분리
     * @param parentMethodCd
     * @return
     * @throws Exception
     */
    @GetMapping("/getMethodCdBySiteType")
    @ApiOperation(value = "상위결제수단 조회 후 siteType 기준으로 리스트 분리", response = Map.class)
    public ResponseObject getMethodCdBySiteType(
        @ApiParam(name = "parentMethodCd", value = "상위결제수단코드(CARD:신용카드,RBANK:실시간계좌이체)", required = true) @RequestParam String parentMethodCd
    ) throws Exception{
        Map<String, List<PaymentMethodDto>> paymentMethodListMap = commonPaymentMethodService.getMethodCdBySiteType(parentMethodCd);
        return ResponseObject.Builder.<Map<String, List<PaymentMethodDto>>>builder().data(paymentMethodListMap).build();
    }

    /**
     * 중복 제거한 모든 결제수단코드 조회
     * @return
     * @throws Exception
     */
    @GetMapping("/getAllDistinctMethodCd")
    @ApiOperation(value = "중복 제거한 모든 결제수단코드 조회")
    public ResponseObject getAllDistinctMethodCd() throws Exception {
        List<PaymentMethodDto> paymentMethodDtoList = commonPaymentMethodService.getDistinctMethodCd(null);
        return ResponseObject.Builder.<List<PaymentMethodDto>>builder().data(paymentMethodDtoList).build();
    }
}