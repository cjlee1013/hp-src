package kr.co.homeplus.escrowmng.statistics.service;

import java.util.List;
import kr.co.homeplus.escrowmng.enums.OrderCode;
import kr.co.homeplus.escrowmng.order.factory.OrderResponseFactory;
import kr.co.homeplus.escrowmng.statistics.mapper.CartCouponSlaveMapper;
import kr.co.homeplus.escrowmng.statistics.mapper.GradeCouponStatisticsSlaveMapper;
import kr.co.homeplus.escrowmng.statistics.model.CartCouponStatisticsModel;
import kr.co.homeplus.escrowmng.statistics.model.CartCouponStatisticsSelectModel;
import kr.co.homeplus.escrowmng.statistics.model.GradeCouponStatisticsListGetDto;
import kr.co.homeplus.escrowmng.statistics.model.GradeCouponStatisticsListSetDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class GradeCouponStatisticsService {

    private final GradeCouponStatisticsSlaveMapper gradeCouponStatisticsSlaveMapper;

    /**
     * 등급별 쿠폰 통계 리스트 조회
     * @param gradeCouponStatisticsListSetDto
     * @return
     */
    public ResponseObject<List<GradeCouponStatisticsListGetDto>> getGradeCouponStatisticsList(
        GradeCouponStatisticsListSetDto gradeCouponStatisticsListSetDto) {

        log.debug("[gradeCouponStatisticsListSetDto] : {} ", gradeCouponStatisticsListSetDto);
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, gradeCouponStatisticsSlaveMapper.getGradeCouponStatisticsList(gradeCouponStatisticsListSetDto));
    }

}
