package kr.co.homeplus.escrowmng.external.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "API연동전문로그")
public class ApiFulltext {

    @ApiModelProperty(value = "API유형")
    @NotEmpty(message = "API유형")
    private String apiType;

    @ApiModelProperty(value = "API구분", position = 1)
    @Pattern(regexp = "(IN|OUT)", message="API구분(IN,OUT)")
    private String apiKind;

    @ApiModelProperty(value = "결과상태", position = 2)
    private String resultStatus;

    @ApiModelProperty(value = "요청데이터전문", position = 3)
    private String reqDataFulltext;

    @ApiModelProperty(value = "결과데이터전문", position = 4)
    private String resultDataFulltext;

    @ApiModelProperty(value = "구매주문번호", position = 5)
    private Long purchaseOrderNo;

    @ApiModelProperty(value = "고객번호", position = 6)
    private Long userNo;

    @ApiModelProperty(value = "요청데이터사이즈", position = 7)
    private Integer reqDataSize;

    @ApiModelProperty(value = "요청데이터사이즈", position = 8)
    private Integer resultDataSize;

    private long apiFulltextSeq;

}