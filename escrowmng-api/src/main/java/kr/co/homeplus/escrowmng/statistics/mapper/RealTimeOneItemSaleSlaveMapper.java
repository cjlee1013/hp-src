package kr.co.homeplus.escrowmng.statistics.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.statistics.model.RealTimeOneItemSaleGetDto;
import kr.co.homeplus.escrowmng.statistics.model.RealTimeOneItemSaleSearchSetDto;

import java.util.List;

@EscrowSlaveConnection
public interface RealTimeOneItemSaleSlaveMapper {
    List<RealTimeOneItemSaleGetDto> selectRealTimeOneItemSaleList(RealTimeOneItemSaleSearchSetDto realTimeOneItemSaleSearchSetDto);
}
