package kr.co.homeplus.escrowmng.escrow.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import org.apache.ibatis.annotations.Param;

@EscrowSlaveConnection
public interface OrderMonitoringSlaveMapper {
    long selectOrderCount();
    long countMarketOrderCollect(@Param("marketType") String marketType);
    long countMarketOrderCreatePurchaseOrder(@Param("marketType") String marketType);
}
