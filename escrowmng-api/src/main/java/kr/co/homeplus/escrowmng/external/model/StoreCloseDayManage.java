package kr.co.homeplus.escrowmng.external.model;

import static kr.co.homeplus.escrowmng.constants.Constants.OMNI_IF_CHG_ID;
import static kr.co.homeplus.escrowmng.constants.Constants.OMNI_IF_REG_ID;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import lombok.Data;

@Data
@ApiModel(description = "[옴니연동] 점포별 휴일 관리")
public class StoreCloseDayManage {

    @JsonIgnore
    @ApiModelProperty(notes = "휴일 등록번호(PK)")
    private String closeDaySeq;

    @ApiModelProperty(notes = "등록번호(GHS Key)", position = 1, required = true)
    @NotEmpty(message = "등록번호(GHS Key)")
    @AllowInput(types = {PatternConstants.NUM}, message = "등록번호(GHS Key)")
    private String closeDayGhsKey;

    @ApiModelProperty(notes = "점포유형(HYPER,EXP,CLUB)", position = 2, required = true)
    @NotEmpty(message = "점포유형(HYPER,EXP,CLUB)")
    @Pattern(regexp = PatternConstants.STORE_TYPE_PATTERN, message = "점포유형(HYPER,EXP,CLUB)")
    private String storeType;

    @ApiModelProperty(notes = "휴무유형(F:자율휴무,R:정기휴무,S:설,C:추석,E:기타,T:임시)", position = 3, required = true)
    @NotEmpty(message = "휴무유형(F:자율휴무,R:정기휴무,S:설,C:추석,E:기타,T:임시)")
    @Pattern(regexp = PatternConstants.CLOSE_DAY_TYPE, message = "휴무유형(F:자율휴무,R:정기휴무,S:설,C:추석,E:기타,T:임시)")
    private String closeDayType;

    @ApiModelProperty(notes = "등록제목", position = 4, required = true)
    @NotEmpty(message = "등록제목")
    private String titleNm;

    @ApiModelProperty(notes = "진행기간", position = 5, required = true)
    @NotEmpty(message = "진행기간")
    @Pattern(regexp = PatternConstants.DATE_DF_FORMAT_PATTERN, message = "진행기간")
    private String closeDt;

    @ApiModelProperty(notes = "진행상태(Y:정상,N:취소)", position = 6, required = true)
    @NotEmpty(message = "진행상태(Y:정상,N:취소)")
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "진행상태(Y:정상,N:취소)")
    private String useYn;

    @ApiModelProperty(notes = "점포ID 리스트", position = 7, required = true)
    @NotEmpty(message = "점포ID 리스트")
    private List<String> storeIdList;

    @JsonIgnore
    @ApiModelProperty(notes = "등록자")
    private String regId = OMNI_IF_REG_ID;
    @JsonIgnore
    @ApiModelProperty(notes = "수정자")
    private String chgId = OMNI_IF_CHG_ID;
}
