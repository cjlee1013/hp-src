package kr.co.homeplus.escrowmng.claim.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListSetDto;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;

@EscrowSlaveConnection
public interface ClaimPartnerSlaveMapper {
    List<ClaimPartnerListGetDto> selectPOClaimList(ClaimPartnerListSetDto claimPartnerListSetDto);
}
