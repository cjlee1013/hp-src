package kr.co.homeplus.escrowmng.order.model.entity.market;

public class MarketOrderEntity {
    // 마켓제휴주문순번
    public String marketOrderSeq;
    // 마켓유형
    public String marketType;
    // 마켓주문번호
    public String marketOrderNo;
    // 점포ID
    public String storeId;
    // 주문자ID
    public String buyerId;
    // 주문자명
    public String buyerNm;
    // 주문자전화번호
    public String buyerTelNo;
    // 주문자휴대폰번호
    public String buyerMobileNo;
    // 주문자이메일
    public String buyerEmail;
    // 주문자우편번호
    public String buyerZipcode;
    // 주문자기본주소
    public String buyerBaseAddr;
    // 주문자상세주소
    public String buyerDetailAddr;
    // 받는사람명
    public String receiverNm;
    // 받는사람전화번호
    public String receiverTelNo;
    // 받는사람휴대폰번호
    public String receiverMobileNo;
    // 받는사람우편번호
    public String receiverZipcode;
    // 받는사람기본주소
    public String receiverBaseAddr;
    // 받는사람상세주소
    public String receiverDetailAddr;
    // 배송메모
    public String shipMemo;
    // 총상품금액
    public String itemAmt;
    // 총배송비금액
    public String shipAmt;
    // 총상품할인금액
    public String itemDiscountAmt;
    // 총배송비할인금액
    public String shipDiscountAmt;
    // 총프로모션할인금액
    public String promoDiscountAmt;
    // 총쿠폰할인금액
    public String couponDiscountAmt;
    // 할인금액
    public String discountAmt;
    // 거래합계금액
    public String tradeTotAmt;
    // 거래금액
    public String tradeAmt;
    // 받은금액
    public String tradeRcvAmt;
    // 거래건수
    public String tradeCnt;
    // 결제일시
    public String paymentDt;
    // 결제수단
    public String paymentMethod;
    // 마켓주문상태(R: 대기, I: 진행중, E: 오류, C: 완료)
    public String marketOrderStatus;
    // 거래일시
    public String tradeDt;
    // 수신여부
    public String rcvYn;
    // 패키지번호
    public String pkgNo;
    // 배송요청일시
    public String shipReqDt;
    // 배송요청슬롯시작시간
    public String shipReqSlotStartTime;
    // 배송요청슬롯종료시간
    public String shipReqSlotEndTime;
    // 슬롯ID
    public String slotId;
    // SHIFTID
    public String shiftId;
    // 슬롯원복여부
    public String slotRestrYn;
    // 구매주문번호
    public String purchaseOrderNo;
    // 등록일시
    public String regDt;
    // 등록자ID
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정자ID
    public String chgId;
}
