package kr.co.homeplus.escrowmng.claim.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.ClaimBundleInfoList;
import kr.co.homeplus.escrowmng.claim.model.ClaimExchShippingDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimGiftItemListDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimHistoryListDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimInfoRegDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPaymentProcessGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPickShippingDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimProcessGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimReasonInfoGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimReasonInfoGetDto.RefundTypeList;
import kr.co.homeplus.escrowmng.claim.model.ClaimDetailInfoGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimDetailInfoListDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimDetailInfoSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimInfoListGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimInfoListSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.escrowmng.claim.model.ClaimPrevOrderCancelDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimRegisterPreRefundInfoDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimShipInfoDto;
import kr.co.homeplus.escrowmng.claim.model.DiscountInfoListDto;
import kr.co.homeplus.escrowmng.claim.model.PromoInfoListDto;
import kr.co.homeplus.escrowmng.claim.model.StoreInfoGetDto;
import kr.co.homeplus.escrowmng.claim.model.external.ClaimCouponReIssueSetDto.CouponReIssueDto;
import kr.co.homeplus.escrowmng.claim.model.market.MarketClaimBasicDataDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.escrowmng.claim.sql.ClaimInfoListSql;
import kr.co.homeplus.escrowmng.claim.sql.ClaimManagementSql;
import kr.co.homeplus.escrowmng.claim.sql.ClaimOrderCancelSql;
import kr.co.homeplus.escrowmng.claim.sql.ClaimOrderTicketSql;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.order.sql.OrderDataSql;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.mapping.StatementType;

@EscrowSlaveConnection
public interface ClaimReadMapper {

    @ResultType(value = ClaimPrevOrderCancelDto.class)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectPrevClaimOrderCancelCheck")
    List<ClaimPrevOrderCancelDto> selectPrevClaimOrderCancelCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "callByClaimPreRefundPriceCalculation")
    ClaimPreRefundCalcDto callByClaimPreRefundPriceCalculation(LinkedHashMap<String, Object> parameterMap);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "callByClaimPreRefundPriceMultiCalculation")
    ClaimPreRefundCalcDto callByClaimPreRefundPriceMultiCalculation(LinkedHashMap<String, Object> parameterMap);

    @SelectProvider(type = OrderDataSql.class, method = "selectPurchaseOrderInfo")
    ClaimMstRegDto selectPurchaseOrderInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OrderDataSql.class, method = "selectCreateClaimDataInfo")
    LinkedHashMap<String, Object> selectCreateClaimDataInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = OrderDataSql.class, method = "selectClaimItemInfo")
    LinkedHashMap<String, Object> selectClaimItemInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderItemNo") long orderItemNo);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectOriPaymentForClaim")
    LinkedHashMap<String, Object> selectOriPaymentForClaim(@Param("paymentNo") long paymentNo, @Param("paymentType") String paymentType);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectOrderOptInfoForClaim")
    ClaimOptRegDto selectOrderOptInfoForClaim(@Param("orderItemNo") long orderItemNo, @Param("orderOptNo") String orderOptNo);

    @ResultType(value = ClaimInfoListGetDto.class)
    @SelectProvider(type = ClaimInfoListSql.class, method = "selectClaimList")
    List<ClaimInfoListGetDto> selectClaimList(ClaimInfoListSetDto claimInfoListSetDto);

    @ResultType(value = ClaimDetailInfoGetDto.class)
    @SelectProvider(type = ClaimInfoListSql.class, method = "selectClaimDetailInfo")
    ClaimDetailInfoGetDto selectClaimDetailInfo(ClaimDetailInfoSetDto claimDetailInfoSetDto);

    @ResultType(value = ClaimDetailInfoListDto.class)
    @SelectProvider(type = ClaimInfoListSql.class, method = "selectClaimDetailInfoList")
    List<ClaimDetailInfoListDto> selectClaimDetailInfoList(ClaimDetailInfoSetDto claimDetailInfoSetDto);

    @SelectProvider(type = ClaimInfoListSql.class, method = "selectClaimBundleInfo")
    List<ClaimBundleInfoList> selectClaimBundleInfo(@Param("claimNo") long claimNo);

    @ResultType(value = ClaimRegisterPreRefundInfoDto.class)
    @SelectProvider(type = ClaimInfoListSql.class, method = "selectRegisterPreRefundAmt")
    ClaimRegisterPreRefundInfoDto selectRegisterPreRefundAmt(@Param("claimNo") long claimNo);

    @ResultType(value = ClaimReasonInfoGetDto.class)
    @SelectProvider(type = ClaimInfoListSql.class, method = "selectCancelReasonInfo")
    ClaimReasonInfoGetDto selectCancelReasonInfo(@Param("claimNo") long claimNo);

    @ResultType(value = ClaimReasonInfoGetDto.class)
    @SelectProvider(type = ClaimInfoListSql.class, method = "selectReturnExchangeReasonInfo")
    ClaimReasonInfoGetDto selectReturnExchangeReasonInfo(@Param("claimNo") long claimNo);

    @ResultType(value = RefundTypeList.class)
    @SelectProvider(type = ClaimInfoListSql.class, method = "selectRefundTypeList")
    List<RefundTypeList> selectRefundTypeList(@Param("claimNo") long claimNo);

    @ResultType(value = DiscountInfoListDto.class)
    @SelectProvider(type = ClaimInfoListSql.class, method = "selectDiscountInfoList")
    List<DiscountInfoListDto> selectDiscountInfoList(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectOrderBundleByClaim")
    List<LinkedHashMap<String, Object>> selectOrderBundleByClaim(@Param("purchaseOrderNo") long purchaseOrderNo);

    @ResultType(value = ClaimPreRefundItemList.class)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectOrderItemInfoByClaim")
    List<ClaimPreRefundItemList> selectOrderItemInfoByClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectOrderByClaimPartInfo")
    LinkedHashMap<String, Object> selectOrderByClaimPartInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @ResultType(value = ClaimPickShippingDto.class)
    @SelectProvider(type = ClaimInfoListSql.class, method = "selectClaimPickShippingInfo")
    ClaimPickShippingDto selectClaimPickShippingInfo(@Param("claimBundleNo") long claimBundleNo);

    @ResultType(value = ClaimExchShippingDto.class)
    @SelectProvider(type = ClaimInfoListSql.class, method = "selectClaimExchShippingInfo")
    ClaimExchShippingDto selectClaimExchShippingInfo(@Param("claimBundleNo") long claimBundleNo);

    @SelectProvider(type = OrderDataSql.class, method = "selectCreateClaimMstDataInfo")
    LinkedHashMap<String, Object> selectCreateClaimMstDataInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectClaimPossibleCheck")
    LinkedHashMap<String, Object> selectClaimPossibleCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectMultiOrderClaimPossibleCheck")
    LinkedHashMap<String, Object> selectMultiOrderClaimPossibleCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("multiBundleNo") long multiBundleNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimCouponReIssue")
    List<CouponReIssueDto> selectClaimCouponReIssue(@Param("claimNo") long claimNo, @Param("userNo") long userNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimInfo")
    List<LinkedHashMap<String, Object>> selectClaimInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimPickIsAutoInfo")
    LinkedHashMap<String, Object> selectClaimPickIsAutoInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimRegDataInfo")
    ClaimInfoRegDto selectClaimRegDataInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimInfoListSql.class, method = "selectShipStatus")
    String selectShipStatus(@Param("bundleNo") long bundleNo);

    @SelectProvider(type = ClaimInfoListSql.class, method = "selectClaimShipInfo")
    ClaimShipInfoDto selectClaimShipInfo(@Param("bundleNo") long bundleNo);

    @SelectProvider(type = ClaimInfoListSql.class, method = "selectClaimGiftList")
    List<ClaimGiftItemListDto> selectClaimGiftList(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimInfoListSql.class, method = "selectPromoInfoList")
    List<PromoInfoListDto> selectPromoInfoList(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderTicketSql.class, method = "selectOrderTicketInfo")
    List<LinkedHashMap<String, Object>> selectOrderTicketInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderItemNo") long orderItemNo, @Param("cancelCnt") int cancelCnt);

    @SelectProvider(type = ClaimOrderTicketSql.class, method = "selectOrderTicketInfoForOrderTicketList")
    List<LinkedHashMap<String, Object>> selectOrderTicketInfoForOrderTicketList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderItemNo") long orderItemNo, @Param("orderTicketList") ArrayList<Long> orderTicketList);

    @SelectProvider(type = ClaimOrderTicketSql.class, method = "selectClaimTicketInfo")
    List<LinkedHashMap<String, Object>> selectClaimTicketInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimOrderTypeCheck")
    LinkedHashMap<String, String> selectClaimOrderTypeCheck(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimInfoListSql.class, method = "selectSlotSiftInfo")
    LinkedHashMap<String, String> selectSlotSiftInfo (@Param("slotId") String slotId, @Param("shiftId") String shiftId, @Param("storeType") String storeType, @Param("storeId") String storeId);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectCreateClaimAccumulateInfo")
    List<LinkedHashMap<String, Object>> selectCreateClaimAccumulateInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectAccumulateCancelInfo")
    LinkedHashMap<String, Object> selectAccumulateCancelInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("pointKind") String pointKind);

    @SelectProvider(type = ClaimInfoListSql.class, method = "selectClaimProcess")
    ClaimProcessGetDto selectClaimProcess (@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimInfoListSql.class, method = "selectClaimPaymentProcess")
    List<ClaimPaymentProcessGetDto> selectClaimPaymentProcess (@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimInfoListSql.class, method = "selectClaimHistoryList")
    List<ClaimHistoryListDto> selectClaimHistoryList (@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectOriginAndCombineClaimCheck")
    List<LinkedHashMap<String, Object>> selectOriginAndCombineClaimCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("searchType") String searchType);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectPreRefundRecheckData")
    ClaimPreRefundCalcSetDto selectPreRefundRecheckData(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectPreRefundItemData")
    List<ClaimPreRefundItemList> selectPreRefundItemData(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectPreRefundCompareData")
    ClaimMstRegDto selectPreRefundCompareData(@Param("claimNo") long claimNo);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "callByClaimPreRefundReCheckCalculation")
    ClaimPreRefundCalcDto callByClaimPreRefundReCheckCalculation(LinkedHashMap<String, Object> parameterMap);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimBundleTdInfo")
    LinkedHashMap<String, String> selectClaimBundleTdInfo(@Param("claimNo") long claimNo);
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectMileagePaybackCheck")
    LinkedHashMap<String, Object> selectMileagePaybackCheck(@Param("bundleNo") long bundleNo);
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectMileagePaybackInfo")
    LinkedHashMap<String, Object> selectMileagePaybackInfo(@Param("purchaseOrderNo") long purchaseOrderNo);
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectMileagePaybackCancelInfo")
    LinkedHashMap<String, Object> selectMileagePaybackCancelInfo(@Param("bundleNo") long bundleNo, @Param("orgBundleNo") long orgBundleNo);

    @ResultType(value = ClaimPreRefundItemList.class)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectOrderItemInfoByTicketClaim")
    List<ClaimPreRefundItemList> selectOrderItemInfoByTicketClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @ResultType(value = StoreInfoGetDto.class)
    @SelectProvider(type = ClaimInfoListSql.class, method = "selectStoreInfo")
    StoreInfoGetDto selectStoreInfo(@Param("storeId") String storeId);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectMultiBundleInfoForClaim")
    List<Long> selectMultiBundleInfoForClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "callByClaimMultiPreRefundPriceCalculation")
    ClaimPreRefundCalcDto callByClaimMultiPreRefundPriceCalculation(HashMap<String, Object> parameterMap);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectIsMarketOrderCheckForClaim")
    String selectIsMarketOrderCheckForClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimBasicDataForMarketOrder")
    MarketClaimBasicDataDto selectClaimBasicDataForMarketOrder(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderOptNo") long orderOptNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimStatusChangeDataForMarketOrder")
    List<LinkedHashMap<String, Object>> selectClaimStatusChangeDataForMarketOrder(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectMarketCollectedExchangeInfo")
    List<LinkedHashMap<String, Object>> selectMarketCollectedExchangeInfo(@Param("claimBundleNo") long claimBundleNo);

}