package kr.co.homeplus.escrowmng.claim.mapper.multiShip;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimShipInfoDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipClaimItemListGetDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipClaimItemListSetDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipInfoGetDto;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import org.apache.ibatis.annotations.Param;


@EscrowSlaveConnection
public interface MultiShipClaimReadMapper {
    List<MultiShipInfoGetDto> selectMultiShipList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    List<MultiShipClaimItemListGetDto> selectMultiShipItemList(MultiShipClaimItemListSetDto multiShipClaimItemListSetDto);

    ClaimPreRefundCalcDto callByMultiShipClaimPreRefundPriceCalculation(LinkedHashMap<String, Object> parameterMap);

    ClaimPreRefundCalcDto callByMultiShipClaimPreRefundPriceMultiCalculation(LinkedHashMap<String, Object> parameterMap);

    ClaimShipInfoDto selectMultiShipInfo (@Param("bundleNo") long bundleNo, @Param("multiBundleNo") long multiBundleNo);

    String selectMultiShipDeliveryYn(@Param("multiBundleNo")long multiBundleNo);
}