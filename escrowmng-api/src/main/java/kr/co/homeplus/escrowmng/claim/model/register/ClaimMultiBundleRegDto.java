package kr.co.homeplus.escrowmng.claim.model.register;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "클레임 다중 번들 DTO")
public class ClaimMultiBundleRegDto {
    @ApiModelProperty(value= "클레임다중배송번들번호")
    private long claimMultiBundleNo;
    @ApiModelProperty(value= "클레임그룹번호")
    private long claimNo;
    @ApiModelProperty(value= "클레임번들번호")
    private long claimBundleNo;
    @ApiModelProperty(value= "주문번호")
    private long purchaseOrderNo;
    @ApiModelProperty(value= "다중번들번호")
    private long multiBundleNo;
    @ApiModelProperty(value= "등록일")
    private String regDt;
}
