package kr.co.homeplus.escrowmng.delivery.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.delivery.model.StoreCloseDayImageModel;

@EscrowMasterConnection
public interface StoreCloseDayMasterMapper {
    int updateCloseDayImageList(StoreCloseDayImageModel storeCloseDayImageModel);
}
