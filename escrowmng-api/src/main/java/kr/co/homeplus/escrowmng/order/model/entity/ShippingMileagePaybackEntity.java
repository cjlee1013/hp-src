package kr.co.homeplus.escrowmng.order.model.entity;

public class ShippingMileagePaybackEntity {
    //
    public String paybackNo;
    // 원구매주문번호
    public String orgPurchaseOrderNo;
    // 원배송배송번호
    public String orgBundleNo;
    // 합배송배송번호
    public String bundleNo;
    // 점포유형(HYPER, CLUB, AURORA, EXP, DS)
    public String storeType;
    // 적립 금액
    public String shipPricePayback;
    // 고객번호(비회원주문시 신규생성)
    public String userNo;
    // 적립결과 (Y:적립성공,N:적립예정,E:미적립)
    public String resultYn;
    // 취소여부 (Y:취소,N:정상주문)
    public String cancelYn;
    // 등록ID
    public String regId;
    // 등록일자
    public String regDt;
    //
    public String chgId;
    //
    public String chgDt;
}
