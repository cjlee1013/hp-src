package kr.co.homeplus.escrowmng.claim.service.impl;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.mapper.outOfStock.OutOfStockItemSlaveMapper;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.OutOfStockItemListGetDto;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.OutOfStockItemListSetDto;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.StoreShiftListSetDto;
import kr.co.homeplus.escrowmng.claim.service.outOfStock.OutOfStockItemMapperService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class OutOfStockItemMapperServiceImpl implements OutOfStockItemMapperService {

    private final OutOfStockItemSlaveMapper outOfStockItemSlaveMapper;

    @Override
    public List<OutOfStockItemListGetDto> getOutOfStockItemList(
        OutOfStockItemListSetDto outOfStockItemListSetDto) {
        return outOfStockItemSlaveMapper.selectOutOfStockItemList(outOfStockItemListSetDto);
    }

    @Override
    public List<String> getStoreShiftList(StoreShiftListSetDto storeShiftListSetDto) {
        return outOfStockItemSlaveMapper.selectStoreShiftList(storeShiftListSetDto);
    }

}
