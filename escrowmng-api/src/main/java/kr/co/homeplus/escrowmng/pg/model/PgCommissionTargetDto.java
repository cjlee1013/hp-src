package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "PG 수수료 대상")
@Getter
@Setter
@EqualsAndHashCode
public class PgCommissionTargetDto {
    @ApiModelProperty(value = "PG수수료대상순번", position = 1)
    private Long pgCommissionTargetSeq;

    @ApiModelProperty(value = "PG수수료관리순번", position = 2)
    private Long pgCommissionMngSeq;

    @ApiModelProperty(value = "결제수단코드", position = 3)
    @AllowInput(types = {PatternConstants.NUM, PatternConstants.ENG}, message = "결제수단코드")
    private String methodCd;

    @ApiModelProperty(value = "신용카드수수료율", position = 4)
    @AllowInput(types = {PatternConstants.NUM}, message = "신용카드수수료율")
    private BigDecimal creditCommissionRate;

    @ApiModelProperty(value = "체크카드수수료율(실시간결제,휴대폰결제는 미입력)", position = 5)
    @AllowInput(types = {PatternConstants.NUM}, message = "체크카드수수료율")
    private BigDecimal debitCommissionRate;

    @ApiModelProperty(value = "수수료금액", position = 6)
    private int commissionAmt;

    @ApiModelProperty(value = "모바일환불수수료금액(휴대폰결제 환불수수료 금액)", position = 7)
    private int mobileRefundCommissionAmt;

    @ApiModelProperty(value = "실시간계좌이체최저수수료금액(토스,이니시스 실시간계좌이체 최저수수료금액)", position = 8)
    private int rbankLowCommissionAmt;

    @ApiModelProperty(value = "TOSS실시간계좌이체최초수수료금액(토스 10만원 초과 수수료금액)", position = 9)
    private int tossRbankFstCommissionAmt;

    @ApiModelProperty(value = "TOSS실시간계좌이체다음수수료금액(토스 100만원 초과 수수료금액)", position = 10)
    private int tossRbankNextCommissionAmt;

    @ApiModelProperty(value = "등록자ID", position = 11)
    private String regId;

    @ApiModelProperty(value = "등록일자", position = 12)
    private String regDt;

    @ApiModelProperty(value = "수정자ID", position = 13)
    private String chgId;

    @ApiModelProperty(value = "수정일자", position = 14)
    private String chgDt;
}
