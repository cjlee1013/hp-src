package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@ApiModel(description = "결제관리 > 결제수단관리 > 결제수단별 혜택관리 DTO")
@Data
public class PaymentMethodBenefitManageDto {
    @ApiModelProperty(notes = "혜택관리 순번", required = true)
    private Long paymentMethodBenefitManageSeq;

    @ApiModelProperty(notes = "사이트유형", required = true)
    @Pattern(regexp = PatternConstants.SITE_TYPE_PATTERN, message="사용여부(Y/N)")
    private String siteType;

    @ApiModelProperty(notes = "결제수단")
    @NotEmpty(message = "결제수단")
    private String parentMethodCd;

    @ApiModelProperty(notes = "제목", required = true)
    @NotEmpty(message = "제목")
    @Length(max = 20, message = "제목")
    private String benefitTitle;

    @ApiModelProperty(notes = "노출 시작기간")
    @NotEmpty(message = "노출 시작기간")
    private String applyStartDt;

    @ApiModelProperty(notes = "노출 종료기간")
    @NotEmpty(message = "노출 종료기간")
    private String applyEndDt;

    @ApiModelProperty(notes = "혜택안내문구1")
    @NotEmpty(message = "혜택안내문구")
    @Length(max = 20, message = "혜택안내문구")
    private String benefitMessage1;
    @ApiModelProperty(notes = "혜택안내문구2")
    private String benefitMessage2;
    @ApiModelProperty(notes = "혜택안내문구3")
    private String benefitMessage3;

    @ApiModelProperty(notes = "사용여부(Y:사용,N:미사용)", required = true)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message="사용여부(Y/N)")
    private String useYn;

    @ApiModelProperty(notes = "등록자ID", required = true)
    private String regId;
    @ApiModelProperty(notes = "등록일자", required = true, hidden = true)
    private String regDt;
    @ApiModelProperty(notes = "수정자ID", required = true)
    private String chgId;
    @ApiModelProperty(notes = "수정일자", required = true, hidden = true)
    private String chgDt;
}
