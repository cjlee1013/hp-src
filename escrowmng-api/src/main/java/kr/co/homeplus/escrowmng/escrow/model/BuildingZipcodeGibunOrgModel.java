package kr.co.homeplus.escrowmng.escrow.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 우편번호관련 지번DB Model 
 */
@Getter
@Setter
public class BuildingZipcodeGibunOrgModel {

    /* 원본 필드 정보 (building_zipcode_gibun_org) */
    /** 도로명코드(시군구코드(5)+도로명번호(7)) */
    private String roadAddrCd;
    /** 지하숫자종류(0:지상,1:지하,2:공중) */
    private String undgrdNumKind;
    /** 건물본번 */
    private int buildBonNo;
    /** 건물부번 */
    private int buildBuNo;
    /** 지번주소 일련번호 */
    private int gibunSeq;

    /** 법정동코드 */
    private String courtDongCd;
    /** 시/도명 */
    private String sidoNm;
    /** 시/군/구명 */
    private String sigunguNm;
    /** 법정읍면동명 */
    private String courtEubmyndnNm;
    /** 법정리명 */
    private String courtRiNm;
    /** 산숫자종류(0:대지,1:산) */
    private String mountainNumKind;
    /** 지번본번(번지) */
    private int gibunBonNo;
    /** 지번부번(호) */
    private int gibunBuNo;
    /** 이동사유코드(31:신규,34:변동,63:폐지,72:건물군내일부건물폐지,73:건물군내일부건물생성) */
    private String moveReasonCd;


    /**
     * 파싱 데이터 DTO 에 매핑
     * @param buildingFieldArray
     */
    public void setBuildingZipcodeGibunOrgByTxt(String[] buildingFieldArray) {
        this.courtDongCd = buildingFieldArray[0];
        this.sidoNm = buildingFieldArray[1];
        this.sigunguNm = buildingFieldArray[2];
        this.courtEubmyndnNm = buildingFieldArray[3];
        this.courtRiNm = buildingFieldArray[4];
        this.mountainNumKind = buildingFieldArray[5];
        this.gibunBonNo = Integer.parseInt(buildingFieldArray[6]);
        this.gibunBuNo = Integer.parseInt(buildingFieldArray[7]);
        this.roadAddrCd = buildingFieldArray[8];
        this.undgrdNumKind = buildingFieldArray[9];
        this.buildBonNo = Integer.parseInt(buildingFieldArray[10]);
        this.buildBuNo = Integer.parseInt(buildingFieldArray[11]);
        this.gibunSeq = Integer.parseInt(buildingFieldArray[12]);
        this.moveReasonCd = buildingFieldArray[13];
    }
}
