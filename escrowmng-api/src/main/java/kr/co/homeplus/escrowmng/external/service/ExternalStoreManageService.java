package kr.co.homeplus.escrowmng.external.service;

import static kr.co.homeplus.escrowmng.constants.Constants.OMNI_IF_CHG_ID;
import static kr.co.homeplus.escrowmng.constants.Constants.OMNI_IF_REG_ID;
import static kr.co.homeplus.escrowmng.constants.Constants.PROFILES_DEV;
import static kr.co.homeplus.escrowmng.constants.Constants.PROFILES_PRD;
import static kr.co.homeplus.escrowmng.constants.Constants.PROFILES_QA;
import static kr.co.homeplus.escrowmng.constants.Constants.RESULT_MSG_FAIL;
import static kr.co.homeplus.escrowmng.constants.Constants.RESULT_MSG_SUCCESS;
import static kr.co.homeplus.escrowmng.constants.Constants.RETURN_SUCCESS;
import static kr.co.homeplus.escrowmng.constants.Constants.USE_N;
import static kr.co.homeplus.escrowmng.constants.Constants.USE_Y;
import static kr.co.homeplus.escrowmng.constants.Constants.ZERO;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.escrowmng.core.db.construct.TransactionManagerName;
import kr.co.homeplus.escrowmng.core.exception.ExternalException;
import kr.co.homeplus.escrowmng.enums.ExceptionCode;
import kr.co.homeplus.escrowmng.external.mapper.ExternalStoreCloseDayManageMasterMapper;
import kr.co.homeplus.escrowmng.external.mapper.ExternalStorePickupMasterMapper;
import kr.co.homeplus.escrowmng.external.mapper.ExternalStoreSlotMasterMapper;
import kr.co.homeplus.escrowmng.external.mapper.ExternalStoreZipcodeManageMasterMapper;
import kr.co.homeplus.escrowmng.external.model.ApiFulltext;
import kr.co.homeplus.escrowmng.external.model.RemoteShipManage;
import kr.co.homeplus.escrowmng.external.model.StoreCloseDayManage;
import kr.co.homeplus.escrowmng.external.model.StorePickupPlaceManage;
import kr.co.homeplus.escrowmng.external.model.StoreShiftManage;
import kr.co.homeplus.escrowmng.external.model.StoreSlotManage;
import kr.co.homeplus.escrowmng.external.model.StoreZipcodeManage;
import kr.co.homeplus.escrowmng.utils.EscrowDate;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import kr.co.homeplus.plus.util.LimitToStringStyle;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExternalStoreManageService {

    @Value("${spring.profiles.active}")
    private String PROFILES_ACTIVE;

    private final ExternalStoreSlotMasterMapper externalStoreSlotMasterMapper;
    private final ExternalStoreZipcodeManageMasterMapper externalStoreZipcodeManageMasterMapper;
    private final ExternalStoreCloseDayManageMasterMapper externalStoreCloseDayManageMasterMapper;
    private final ExternalStorePickupMasterMapper externalStorePickupMasterMapper;
    private final ApiFulltextService apiFulltextService;

    /**
     * 점포별 shift 저장 (옴니 I/F 연동)
     * - merge insert 데이터 있는 경우 업데이트 함.
     *
     * @param shiftManageList
     * @return
     */
    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int saveStoreShiftList(List<StoreShiftManage> shiftManageList, ApiFulltext apiFulltext) {
        int saveCnt;
        try {
            saveCnt = externalStoreSlotMasterMapper.insertStoreShift(shiftManageList, OMNI_IF_REG_ID);
            log.info("[saveStoreShiftList] result cnt: " + saveCnt);

            // 연동로그 저장
            apiFulltext.setResultDataSize(saveCnt);
            apiFulltext.setResultStatus(RESULT_MSG_SUCCESS);
            apiFulltextService.updateApiFulltext(apiFulltext);

        } catch (Exception e) {
            log.error("[saveStoreShiftList] " + ExceptionUtils.getStackTrace(e));

            // 연동 실패 로그 저장
            apiFulltext.setResultStatus(RESULT_MSG_FAIL);
            apiFulltextService.updateApiFulltext(apiFulltext);
            throw new ExternalException(ExceptionCode.SYS_ERROR_CODE_9006, "점포별 shift 저장", shiftManageList);
        }

        return saveCnt;
    }

    /**
     * 점포별 slot 저장 (옴니 I/F 연동)
     * - merge insert 데이터 있는 경우 업데이트 함.
     * @param slotManageList
     * @return
     */
    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int saveStoreSlotList(List<StoreSlotManage> slotManageList, ApiFulltext apiFulltext) {
        int saveCnt;
        try {
            // 주문가능수량 임의 변경
            this.changeSlotCapa(slotManageList);
            saveCnt = externalStoreSlotMasterMapper.insertStoreSlot(slotManageList, OMNI_IF_REG_ID);
            // 연동로그 저장
            apiFulltext.setResultDataSize(saveCnt);
            apiFulltext.setResultStatus(RESULT_MSG_SUCCESS);
            apiFulltextService.updateApiFulltext(apiFulltext);

        } catch (Exception e) {
            log.error("[saveStoreSlotList] " + ExceptionUtils.getStackTrace(e));

            // 연동 실패 로그 저장
            apiFulltext.setResultStatus(RESULT_MSG_FAIL);
            apiFulltextService.updateApiFulltext(apiFulltext);
            throw new ExternalException(ExceptionCode.SYS_ERROR_CODE_9006, "점포별 slot 저장", slotManageList);
        }

        return RETURN_SUCCESS;
    }

    /**
     * 운영 trial 기간 동안 주문 가능 수량 0 으로 설정
     * @param slotManageList
     */
    private void changeSlotCapa(List<StoreSlotManage> slotManageList) {
        String trialEndDt = "2021-02-13 00:00:00";

        if (EscrowString.equals(PROFILES_PRD, PROFILES_ACTIVE)) {
            slotManageList.stream().forEach(
                sl -> {
                    if (EscrowDate.compareDateTimeStr(trialEndDt, sl.getShipDt() + " 00:00:00") >= ZERO) {
                        sl.setOrderChgCnt(ZERO);
                        sl.setPickupOrderChgCnt(ZERO);
                        sl.setLockerOrderChgCnt(ZERO);
                        sl.setReserveOrderChgCnt(ZERO);
                    }
                }
            );
        }
    }

    /**
     * 원거리배송 slot 저장 (옴니 I/F 연동)
     * - delete insert 변경한 부분만 내려오기 때문에 store_type, zipcode_cd 기준으로 as-is 데이터 삭제 후 insert 함.
     * @param remoteShipManageList
     * @return
     */
    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int saveRemoteShipList(List<RemoteShipManage> remoteShipManageList, ApiFulltext apiFulltext) {
        try {
            Map<String, List<RemoteShipManage>> remoteShipMap = remoteShipManageList.stream().collect(Collectors.groupingBy(RemoteShipManage::getStoreType));

            remoteShipMap.entrySet()
                .forEach(entry -> {
                    List<String> zipcodeList = remoteShipMap.get(entry.getKey())
                        .stream()
                        .map(RemoteShipManage::getZipcodeCd)
                        .distinct()
                        .collect(Collectors.toList());
                    // as-is 데이터 삭제
                    log.info("[deleteOldRemoteShip] : storeType " + entry.getKey() + ", zipcode " + zipcodeList);
                    externalStoreSlotMasterMapper.deleteRemoteShip(zipcodeList, entry.getKey());
                });

            externalStoreSlotMasterMapper.insertRemoteShip(remoteShipManageList, OMNI_IF_REG_ID);
            // 연동로그 저장
            apiFulltext.setResultStatus(RESULT_MSG_SUCCESS);
            apiFulltextService.updateApiFulltext(apiFulltext);

        } catch (Exception e) {
            log.error("[saveRemoteShipList] " + ExceptionUtils.getStackTrace(e));

            // 연동 실패 로그 저장
            apiFulltext.setResultStatus(RESULT_MSG_FAIL);
            apiFulltextService.updateApiFulltext(apiFulltext);
            throw new ExternalException(ExceptionCode.SYS_ERROR_CODE_9006, "원거리배송 slot 저장");
        }

        return RETURN_SUCCESS;
    }

    /**
     * 원거리배송 slot 저장 (옴니 I/F 연동)
     * 전체 데이터 연동. 초기 등록 시 사용
     * @param remoteShipManageList
     * @return
     */
    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int saveAllRemoteShipList(List<RemoteShipManage> remoteShipManageList) {
        try {
            externalStoreSlotMasterMapper.insertRemoteShip(remoteShipManageList, OMNI_IF_REG_ID);

        } catch (Exception e) {
            log.error("[saveAllRemoteShipList] " + ExceptionUtils.getStackTrace(e));
            throw new ExternalException(ExceptionCode.SYS_ERROR_CODE_9006, "원거리배송 all slot 저장");
        }

        return RETURN_SUCCESS;
    }

    /**
     * 점포별 우편번호 정보 저장 (옴니 I/F 연동)
     * - merge insert 데이터 있는 경우 업데이트 함.
     *
     * @param storeZipcodeManageList
     * @return
     */
    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int saveStoreZipcodeList(List<StoreZipcodeManage> storeZipcodeManageList, ApiFulltext apiFulltext) {
        try {
            long apiFulltextSeq = apiFulltext.getApiFulltextSeq();
            log.info("[saveStoreZipcodeList] apiFulltextSeq:" + apiFulltextSeq);
            storeZipcodeManageList.stream().forEach(zl -> zl.setApiFulltextSeq(apiFulltextSeq));
            // insert temp table > store_zipcode_mng_temp
            int tempCnt = externalStoreZipcodeManageMasterMapper.insertStoreZipcodeMngTempList(storeZipcodeManageList);
            log.info("[saveStoreZipcodeList] apiFulltextSeq:" + apiFulltextSeq + ", tempInsertCnt:" + tempCnt);
            // 점포관리 테이블에 없는 점포ID는 삭제
            List<Integer> notUseStoreIdList = externalStoreZipcodeManageMasterMapper.selectNotUseStoreIdList(apiFulltextSeq);
            if (EscrowString.isNotEmpty(notUseStoreIdList)) {
                int deleteCnt = externalStoreZipcodeManageMasterMapper.deleteStoreZipcodeMngTempNoStoreId(notUseStoreIdList, apiFulltextSeq);
                log.info("[saveStoreZipcodeList] apiFulltextSeq:" + apiFulltextSeq + ", deleteTempNoStoreIdCnt:" + deleteCnt);
            }
            // selectStoreZipcodeMngTempList
            List<StoreZipcodeManage> storeZipcodeManageTempList = externalStoreZipcodeManageMasterMapper.selectStoreZipcodeMngTempList(apiFulltextSeq);
            log.info("[saveStoreZipcodeList] apiFulltextSeq:" + apiFulltextSeq + ", insertCnt:" + storeZipcodeManageTempList.size());
            // merge into > store_zipcode_mng
            if (EscrowString.isNotEmpty(storeZipcodeManageTempList)) {
                externalStoreZipcodeManageMasterMapper.insertStoreZipcodeMngList(storeZipcodeManageTempList, OMNI_IF_REG_ID, OMNI_IF_CHG_ID);
                // delete > remote_ship_mng
                Map<String, List<StoreZipcodeManage>> mapList = storeZipcodeManageList.stream()
                    .collect(Collectors.groupingBy(StoreZipcodeManage::getStoreType));
                mapList.forEach((key, value) -> {
                    List<String> zipcodeList = mapList.get(key)
                        .stream()
                        .filter(szm -> EscrowString.equals(szm.getRemoteShipYn(), USE_N))
                        .map(StoreZipcodeManage::getZipcodeCd)
                        .distinct()
                        .collect(Collectors.toList());
                    // as-is 데이터 삭제
                    if (!zipcodeList.isEmpty()) {
                        externalStoreSlotMasterMapper.deleteRemoteShip(zipcodeList, key);
                    }
                });
            }
            // 임시테이블 삭제
            int deleteTempCnt = externalStoreZipcodeManageMasterMapper.deleteStoreZipcodeMngTemp(apiFulltextSeq);
            log.info("[saveStoreZipcodeList] apiFulltextSeq:" + apiFulltextSeq + ", deleteTempCnt:" + deleteTempCnt);

            // 연동로그 저장
            apiFulltext.setResultDataSize(storeZipcodeManageTempList.size());
            apiFulltext.setResultStatus(RESULT_MSG_SUCCESS);
            apiFulltextService.updateApiFulltext(apiFulltext);

        } catch (Exception e) {
            log.error("[saveStoreZipcodeList] " + ExceptionUtils.getStackTrace(e));

            // 연동 실패 로그 저장
            apiFulltext.setResultStatus(RESULT_MSG_FAIL);
            apiFulltextService.updateApiFulltext(apiFulltext);
            throw new ExternalException(ExceptionCode.SYS_ERROR_CODE_9006, "점포별 우편번호 정보 저장", storeZipcodeManageList);
        }

        return RETURN_SUCCESS;
    }

    /**
     * 점포별 휴일 정보 저장 (옴니 I/F 연동)
     * - merge insert 데이터 있는 경우 업데이트 함.
     *
     * @param storeCloseDayManageList
     * @return
     */
    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int saveStoreCloseDayList(List<StoreCloseDayManage> storeCloseDayManageList, ApiFulltext apiFulltext) {
        StoreCloseDayManage errorObj = new StoreCloseDayManage();

        try {
            for (StoreCloseDayManage storeCloseDayManage : storeCloseDayManageList) {
                errorObj = storeCloseDayManage;
                // close_day_mng (master) insert-update
                externalStoreCloseDayManageMasterMapper.insertStoreCloseDayMng(storeCloseDayManage);
                // close_day (detail) delete-insert
                externalStoreCloseDayManageMasterMapper.deleteStoreCloseDay(storeCloseDayManage.getCloseDaySeq());
                externalStoreCloseDayManageMasterMapper.insertStoreCloseDay(storeCloseDayManage.getCloseDaySeq(), storeCloseDayManage.getStoreIdList(), storeCloseDayManage.getStoreType());
            }
            // 연동로그 저장
            apiFulltext.setResultDataSize(storeCloseDayManageList.size());
            apiFulltext.setResultStatus(RESULT_MSG_SUCCESS);
            apiFulltextService.updateApiFulltext(apiFulltext);
        } catch (Exception e) {
            log.error("[saveStoreCloseDayList] " + ExceptionUtils.getStackTrace(e));

            // 연동 실패 로그 저장
            apiFulltext.setResultStatus(RESULT_MSG_FAIL);
            apiFulltextService.updateApiFulltext(apiFulltext);
            throw new ExternalException(ExceptionCode.SYS_ERROR_CODE_9006, "점포별 휴일 정보 저장", errorObj);
        }

        return RETURN_SUCCESS;
    }

    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int storePickupPlaceList(List<StorePickupPlaceManage> storePickupPlaceManageList, ApiFulltext apiFulltext) {
        StorePickupPlaceManage errorObj = new StorePickupPlaceManage();
        try {
            for (StorePickupPlaceManage storePickupPlaceManage : storePickupPlaceManageList) {
                errorObj = storePickupPlaceManage;
                externalStorePickupMasterMapper.insertStorePickupPlace(storePickupPlaceManage, OMNI_IF_REG_ID);
                if (EscrowString.equals(USE_Y, storePickupPlaceManage.getLockerUseYn())) {
                    if (EscrowString.isNotEmpty(storePickupPlaceManage.getLockerList())) {
                        externalStorePickupMasterMapper.insertStorePickupLocker(storePickupPlaceManage.getStoreId(), storePickupPlaceManage.getPlaceNo(), storePickupPlaceManage.getLockerList(), OMNI_IF_REG_ID);
                    } else {
                        throw new ExternalException(ExceptionCode.ERROR_CODE_1003, "lockerList", errorObj);
                    }
                }
            }

            // 연동로그 저장
            apiFulltext.setResultDataSize(storePickupPlaceManageList.size());
            apiFulltext.setResultStatus(RESULT_MSG_SUCCESS);
            apiFulltextService.updateApiFulltext(apiFulltext);

        } catch (ExternalException ee) {
            log.info("ExternalException Code:{}, Message:{}, data:{}", ee.getErrorCode(), ee.getErrorMsg() + ee.getErrorDescription(), ToStringBuilder.reflectionToString(ee.getData(), new LimitToStringStyle(1, ee.getData()
                .getClass(), 0)));
            // 연동 실패 로그 저장
            apiFulltext.setResultStatus(RESULT_MSG_FAIL);
            apiFulltextService.updateApiFulltext(apiFulltext);
            throw ee;
        } catch (Exception e) {
            log.error("[storePickupPlaceList] " + ExceptionUtils.getStackTrace(e));
            // 연동 실패 로그 저장
            apiFulltext.setResultStatus(RESULT_MSG_FAIL);
            apiFulltextService.updateApiFulltext(apiFulltext);
            throw new ExternalException(ExceptionCode.SYS_ERROR_CODE_9006, "점포픽업장소 정보 저장", errorObj);
        }

        return RETURN_SUCCESS;
    }

    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int saveStoreSlotOrderCntList(List<StoreSlotManage> slotManageList, ApiFulltext apiFulltext) {
        if (EscrowString.equals(PROFILES_PRD, PROFILES_ACTIVE)) {
            int saveCnt = ZERO;
            try {
                for (StoreSlotManage storeSlotManage : slotManageList) {
                    // shipDt 02-14 ~ 02-17
                    if (EscrowDate.compareDateTimeStr("2021-02-17" + " 00:00:00", storeSlotManage.getShipDt() + " 00:00:00") >= ZERO
                        && EscrowDate.compareDateTimeStr(storeSlotManage.getShipDt() + " 00:00:00", "2021-02-14" + " 00:00:00") >= ZERO) {
                        saveCnt += externalStoreSlotMasterMapper.updateStoreSlotOrderCnt(storeSlotManage);
                    }
                }
                // 연동로그 저장
                apiFulltext.setResultDataSize(saveCnt);
                apiFulltext.setResultStatus(RESULT_MSG_SUCCESS);
                apiFulltextService.updateApiFulltext(apiFulltext);

            } catch (Exception e) {
                log.error("[saveStoreSlotOrderCntList] " + ExceptionUtils.getStackTrace(e));

                // 연동 실패 로그 저장
                apiFulltext.setResultStatus(RESULT_MSG_FAIL);
                apiFulltextService.updateApiFulltext(apiFulltext);
                throw new ExternalException(ExceptionCode.SYS_ERROR_CODE_9006, "점포별 slot order cnt 저장", slotManageList);
            }
            return saveCnt;
        } else {
            return ZERO;
        }
    }
}
