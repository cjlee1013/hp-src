package kr.co.homeplus.escrowmng.delivery.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.delivery.model.StoreCloseDayImageModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreCloseDayModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreCloseDaySelectModel;

@EscrowSlaveConnection
public interface StoreCloseDaySlaveMapper {

    List<StoreCloseDayModel> selectStoreCloseDayList(StoreCloseDaySelectModel storeCloseDaySelectModel);

    List<StoreCloseDayImageModel> selectCloseDayImageList();
}
