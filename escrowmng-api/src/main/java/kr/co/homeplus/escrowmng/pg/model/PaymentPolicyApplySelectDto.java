package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 배분율 관리 > 결제정책 관리-적용대상 search DTO")
public class PaymentPolicyApplySelectDto {
    @ApiModelProperty(value = "적용순번", position = 1)
    private Long applySeq;

    @ApiModelProperty(value = "정책번호", required = true, position = 2)
    private Long policyNo;

    @ApiModelProperty(value = "적용코드(상품코드,카테고리번호,제휴코드)", position = 3)
    private String applyCd;

    @ApiModelProperty(value = "적용우선순위(1:채널,2:상품,3:카테고리(subString...),4:그이외...)", position = 4)
    private Integer applyPriority;

    @ApiModelProperty(value = "적용대상명(상품명,카테고리명,채널명)", position = 5)
    private String applyTargetNm;
}
