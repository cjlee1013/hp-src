package kr.co.homeplus.escrowmng.pg.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.homeplus.escrowmng.pg.model.PgCompareApprovalDiffDto;
import kr.co.homeplus.escrowmng.pg.model.PgCompareApprovalSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PgCompareApprovalSumDto;
import kr.co.homeplus.escrowmng.pg.service.PgCompareApprovalService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pg/pgCompareApproval")
@Api(tags = "결제관리 > PG 대사 > PG 승인대사 조회")
public class PgCompareApprovalController {

    private final PgCompareApprovalService pgCompareApprovalService;

    @GetMapping("/getSum")
    @ApiOperation(value = "PG 승인대사 합계 조회", response = PgCompareApprovalSumDto.class)
    public ResponseObject getPgCompareApprovalSum(PgCompareApprovalSelectDto pgCompareApprovalSelectDto) {
        return ResourceConverter.toResponseObject(pgCompareApprovalService.getPgCompareApprovalSum(pgCompareApprovalSelectDto));
    }

    @GetMapping("/getDiff")
    @ApiOperation(value = "PG 승인대사 차이내역 조회", response = PgCompareApprovalDiffDto.class)
    public ResponseObject getPgCompareApprovalDiff(PgCompareApprovalSelectDto pgCompareApprovalSelectDto) {
        return ResourceConverter.toResponseObject(pgCompareApprovalService.getPgCompareApprovalDiff(pgCompareApprovalSelectDto));
    }
}