package kr.co.homeplus.escrowmng.order.model.ship;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "주소지변경에 따른 옴니정보 전송")
public class ShipChangeOmniSetDto {
    @ApiModelProperty(value = "배송메시지")
    private String dlvMsg;
    @ApiModelProperty(value = "주문번호")
    private String ordNo;
    @ApiModelProperty(value = "상세주소")
    private String rcptAddr;
    @ApiModelProperty(value = "이름")
    private String rcptName;
    @ApiModelProperty(value = "전화번호")
    private String rcptPhone;
    @ApiModelProperty(value = "상점번호")
    private String storeId;
    @ApiModelProperty(value = "합배송번호")
    private String combineOrdNo;
    @ApiModelProperty(value = "현관비밀번호메세지코드")
    private String doorPasswordCode;
    @ApiModelProperty(value = "현관비밀번호메세지")
    private String doorPassword;
}