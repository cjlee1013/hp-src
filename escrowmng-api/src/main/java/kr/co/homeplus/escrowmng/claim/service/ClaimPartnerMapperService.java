package kr.co.homeplus.escrowmng.claim.service;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerBoardCountGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerDetailGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerDetailItem;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerExchangeListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListSetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerDetailListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerReturnListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.PartnerMainBoardClaimRequestCountGetDto;


public interface ClaimPartnerMapperService {
    /**
     * PO 클레임 건수 조회
     *
     * @param partnerId
     * @param claimType
     * @return 클레임 신청/대기/완료 건수
     * **/
    ClaimPartnerBoardCountGetDto getPOClaimBoardCount(String partnerId, String claimType);

    /**
     * PO 메인보드 클레임 요청 건수 조회
     *
     * @param partnerId
     * @return 클레임 요청 건수
     * **/
    PartnerMainBoardClaimRequestCountGetDto getPOMainClaimRequestCount(String partnerId);

    /**
     * PO 클레임 리스트
     *
     * @param claimCancelListSetDto
     * @return PO 취소 클레임 리스트
     * **/
    List<ClaimPartnerListGetDto> getPOClaimList(ClaimPartnerListSetDto claimCancelListSetDto);


    /**
     * PO 취소 클레임 리스트 건수
     *
     * @param claimCancelListSetDto
     * @return PO 취소 클레임 리스트
     * **/
    Integer getPOClaimListCnt(ClaimPartnerListSetDto claimCancelListSetDto);

    /**
     * PO 클레임 상세 리스트 정보
     *
     * @param purchaseOrderNo
     * @return 클레임 상세 정보
     * **/
    List<ClaimPartnerDetailListGetDto> getPOClaimDetailList(long purchaseOrderNo, long bundleNo);

    /**
     * PO 클레임 상세 정보 - 상품 리스트
     *
     * @param claimBundleNo
     * @return 클레임 상세 정보 - 상품 리스트
     * **/
    List<ClaimPartnerDetailItem> getPOClaimDetailItemList(long claimBundleNo);

    /**
     * PO 클레임 상세정보
     *
     * @param claimBundleNo
     * @return 클레임 상세 정보 - 상품 리스트
     * **/
    ClaimPartnerDetailGetDto getPOClaimDetail(long claimBundleNo);

}
