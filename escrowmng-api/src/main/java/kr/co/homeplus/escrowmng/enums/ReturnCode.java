package kr.co.homeplus.escrowmng.enums;

public enum ReturnCode {

    /**
     * 요청 단계
     */
    READY,

    /**
     * 발송 에러
     */
    ERROR,

    /**
     * 발송 진행 (pinpoint 및 tran 요청)
     */
    SENDING,

    /**
     * 발송 요청 취소 (수동 및 예약발송)
     */
    CANCELED,

    /**
     * 발송 결과 업데이트 완료
     */
    COMPLETED,

    /**
     * 기타 상태
     */
    ETC
}
