package kr.co.homeplus.escrowmng.order.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문정보 상세 - 금액정보 DTO")
public class OrderSearchPriceDto {

    @ApiModelProperty(value = "결제금액")
    private String totAmt;

    @ApiModelProperty(value = "총상품금액")
    private String totItemPrice;

    @ApiModelProperty(value = "총할인금액")
    private String totDiscountAmt;

    @ApiModelProperty(value = "배송")
    private String totShippingAmt;

    @ApiModelProperty(value = "클레임비용")
    private String totClaimAmt;

    @ApiModelProperty(value = "공제비용")
    private String totDeductAmt;

    @ApiModelProperty(value = "상품할인")
    private String totItemDiscountAmt;

    @ApiModelProperty(value = "배송비할인")
    private String totShipDiscountAmt;

    @ApiModelProperty(value = "장바구니할인")
    private String totCartDiscountAmt;

    @ApiModelProperty(value = "카드상품즉시할인")
    private String totCardDiscountAmt;

    @ApiModelProperty(value = "프로모션할인")
    private String totPromoDiscountAmt;

    @ApiModelProperty(value = "임직원할인")
    private String totEmpDiscountAmt;

    @ApiModelProperty(value = "중복쿠폰")
    private String totAddCouponAmt;

}
