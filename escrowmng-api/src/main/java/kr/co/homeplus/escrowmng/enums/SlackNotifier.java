package kr.co.homeplus.escrowmng.enums;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import java.util.List;
import kr.co.homeplus.escrowmng.constants.ResourceRouteName;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class SlackNotifier {

    private final ResourceClient resourceClient;

    // Local(DEV)용 slack URL
    private static String DEV_WEBHOOK_URL = "/services/T01FW2QQYBD/B02VBA1AU69/HGYt6RPvqPaBSlxe1rpS7sLp";
    private static String DEV_CHANNEL = "alarm-dev";
    // QA용
    private static String QA_WEBHOOK_URL = "/services/T01FW2QQYBD/B02V7HXUREJ/crsqLyhVIZ4I1id5vG4Xe3pJ";
    private static String QA_CHANNEL = "alarm-qa";
    // PRD용
    private static String WEBHOOK_URL = "/services/T01FW2QQYBD/B02VBCU6TFF/iUCbU4lTA08PXpt6I8Ae44s5";
    private static String CHANNEL = "alarm-prd";



    public enum SlackTarget {
        CH_PROD_BATCH(SlackNotifier.WEBHOOK_URL, SlackNotifier.CHANNEL),
        ;

        String webHookUrl;
        String channel;

        SlackTarget(String webHookUrl, String channel) {
            this.webHookUrl = webHookUrl;
            this.channel = channel;
        }

        public void setTest() {
            this.webHookUrl = DEV_WEBHOOK_URL;
            this.channel = DEV_CHANNEL;
        }

        public void setQaTest() {
            this.webHookUrl = QA_WEBHOOK_URL;
            this.channel = QA_CHANNEL;
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class SlackMessageAttachment {
        private String color;
        private String pretext;
        private String title;
        private String title_link;
        private String text;

        private void setColor(String level) {
            if("error".equals(level)) {
                this.color = "#e32072";
            } else if ("warn".equals(level)) {
                this.color = "#ff9000";
            }
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class SlackMessage {
        private String text;
        private String channel;
        private List<SlackMessageAttachment> attachments;
    }

    public boolean slackNotify(SlackTarget target, String title, String message, String level) {
        try {
            String serverProfile = System.getProperty("spring.profiles.active");
            if( "local".equals(serverProfile) || "dev".equals(serverProfile)) {
                // 로컬일 경우 테스트용 슬렉으로 발송
                target.setTest();
            } else if ("qa".equals(serverProfile)) {
                target.setQaTest();
            }

            SlackMessageAttachment slackMessageAttachment = new SlackMessageAttachment();
            slackMessageAttachment.setTitle(title);
            slackMessageAttachment.setText(message);
            slackMessageAttachment.setColor(level);

            SlackMessage slackMessage = SlackMessage.builder().channel(target.channel)
                    .attachments(Lists.newArrayList(slackMessageAttachment)).build();

            ObjectMapper mapper = new ObjectMapper();
            log.info("slack json : {}",mapper.writeValueAsString(slackMessageAttachment));
            String apiId = ResourceRouteName.ESCROWSLACK;
            ResourceClientRequest<String> request = ResourceClientRequest.<String>postBuilder()
                .apiId(apiId)
                .uri(target.webHookUrl)
                .postObject(slackMessage) // RestTemplate 단에서 자동으로 json으로 변환 되어 서버로 날아갑니다.
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
            resourceClient.post(request, new TimeoutConfig());

            return true;
        } catch (Exception e) {
            log.info("slackNotify Exception: {}", e);
            return false;
        }
    }

}
