package kr.co.homeplus.escrowmng.order.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문정보 상세 - 클레임정보 DTO")
public class OrderSearchClaimDto {

    @ApiModelProperty(value = "클레임구분코드")
    private String claimType;

    @ApiModelProperty(value = "클레임구분")
    private String claimTypeNm;

    @ApiModelProperty(value = "그룹 클레임번호")
    private long claimNo;

    @ApiModelProperty(value = "클레임번호")
    private long claimBundleNo;

    @ApiModelProperty(value = "주문번호")
    private long purchaseOrderNo;

    @ApiModelProperty(value = "클레임상태")
    private String claimStatus;

    @ApiModelProperty(value = "배송번호")
    private long bundleNo;

    @ApiModelProperty(value = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    private String itemName;

    @ApiModelProperty(value = "옵션번호")
    private long orderOptNo;

    @ApiModelProperty(value = "옵션명")
    private String optNm;

    @ApiModelProperty(value = "신청수량")
    private int claimItemQty;

    @ApiModelProperty(value = "신청일시")
    private String regDt;

    @ApiModelProperty(value = "처리일시")
    private String chgDt;

}
