package kr.co.homeplus.escrowmng.order.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.order.model.management.OrderHistoryListDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchClaimDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchDetailDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchPaymentDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchPriceDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchProdDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchShipDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchGetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchSetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchSubstitutionDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchTckGetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchTckSetDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.MultiOrderShipAddrInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderCashReceiptDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderCouponDiscountDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderDelayShipInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderGiftInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderPartnerInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderPromoInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderSaveInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderShipAddrInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderShipTotalInfoDto;
import kr.co.homeplus.escrowmng.order.model.ship.MultiOrderShipAddrEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipInfoGetDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipInfoSetDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipSetGto;
import kr.co.homeplus.escrowmng.order.model.ship.OrderShipAddrEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.OrderShipOriginInfo;
import kr.co.homeplus.escrowmng.order.model.ship.StoreShipMemoDto;
import kr.co.homeplus.escrowmng.order.model.ship.StoreShipMsgDto;
import kr.co.homeplus.escrowmng.order.service.OrderManagementService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/order")
@Api(tags = "주문관리 > 주문관리 ")
public class OrderManagementController {

    // 주문관리 서비스
    private final OrderManagementService orderManagementService;

    /**
     * 주문관리 조회
     *
     * @param searchSetDto 주문관리 조회 DTO
     * @return List<OrderSearchGetDto> 주문 관련 리스트 리턴.
     */
    @PostMapping("/orderSearch")
    @ApiOperation(value = "주문관리 조회", response = OrderSearchGetDto.class)
    public ResponseObject<List<OrderSearchGetDto>> getOrderSearch(@RequestBody OrderSearchSetDto searchSetDto) {
        return orderManagementService.getOrderMainSearch(searchSetDto);
    }

    @GetMapping("/orderSearch/detail/prod/{purchaseOrderNo}")
    @ApiOperation(value = "주문정보 상세 - 구매자정보", response = OrderSearchProdDto.class)
    public ResponseObject<OrderSearchProdDto> getOrderDetailSearchByUser(@PathVariable("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOrderDetailUserInfo(purchaseOrderNo);
    }

    @GetMapping("/orderSearch/detail/price/{purchaseOrderNo}")
    @ApiOperation(value = "주문정보 상세 - 금액정보", response = OrderSearchPriceDto.class)
    public ResponseObject<OrderSearchPriceDto> getOrderDetailSearchByPrice(@PathVariable("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOrderDetailPriceInfo(purchaseOrderNo);
    }

    @GetMapping("/orderSearch/detail/purchase/{purchaseOrderNo}")
    @ApiOperation(value = "주문정보 상세 - 주문정보", response = OrderSearchDetailDto.class)
    public ResponseObject<List<OrderSearchDetailDto>> getOrderDetailSearchByPurchase(@PathVariable("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOrderDetailPurchaseInfo(purchaseOrderNo);
    }

    @GetMapping("/orderSearch/detail/payment/{purchaseOrderNo}")
    @ApiOperation(value = "주문정보 상세 - 결제정보", response = OrderSearchPaymentDto.class)
    public ResponseObject<List<OrderSearchPaymentDto>> getOrderDetailSearchByPayment(@PathVariable("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOrderDetailPaymentInfo(purchaseOrderNo);
    }

    @GetMapping("/orderSearch/detail/shipInfo")
    @ApiOperation(value = "주문정보 상세 - 배송", response = OrderSearchShipDto.class)
    public ResponseObject<List<OrderSearchShipDto>> getOrderDetailShipInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo, @RequestParam("mallType") String mallType){
        return orderManagementService.getOrderDetailShipInfo(purchaseOrderNo, mallType);
    }

    @GetMapping("/orderSearch/detail/claim/{purchaseOrderNo}")
    @ApiOperation(value = "주문정보 상세 - 클레임상태", response = OrderSearchClaimDto.class)
    public ResponseObject<List<OrderSearchClaimDto>> getOrderDetailClaimInfo(@PathVariable("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOrderDetailClaimInfo(purchaseOrderNo);
    }

    @GetMapping("/orderSearch/detail/pop/shipAddr/{bundleNo}")
    @ApiOperation(value = "주문정보 상세 - 배송 > 배송지변경팝업 - 배송지조회", response = OrderShipAddrInfoDto.class)
    public ResponseObject<List<OrderShipAddrInfoDto>> getOrderShipAddrInfo(@PathVariable("bundleNo") long bundleNo){
        return orderManagementService.getOrderShipAddrInfo(bundleNo);
    }

    @GetMapping("/orderSearch/detail/pop/multiShipAddr/{multiBundleNo}")
    @ApiOperation(value = "주문정보 상세 - 배송 > 배송지변경팝업 - 배송지조회", response = MultiOrderShipAddrInfoDto.class)
    public ResponseObject<List<MultiOrderShipAddrInfoDto>> getMultiOrderShipAddrInfo(@PathVariable("multiBundleNo") long multiBundleNo){
        return orderManagementService.getMultiOrderShipAddrInfo(multiBundleNo);
    }

    @GetMapping("/orderSearch/detail/pop/shipTotal")
    @ApiOperation(value = "주문정보 상세 - 배송 > 송장등록/송장수정/배송완료", response = OrderShipAddrInfoDto.class)
    public ResponseObject<List<OrderShipTotalInfoDto>> getOrderShipTotalInfo(@RequestParam("bundleNo") long bundleNo, @RequestParam("shipStatus") String shipStatus){
        return orderManagementService.getOrderShipTotalInfo(bundleNo, shipStatus);
    }

    @PostMapping("/orderSearch/ship/orderShipChange")
    @ApiOperation(value = "주문정보 상세 - 배송 > 배송지 변경", response = String.class)
    public ResponseObject<String> modifyShipAddrChange(@RequestBody OrderShipAddrEditDto shipAddrEditDto) throws Exception {
        return orderManagementService.modifyShipAddrChange(shipAddrEditDto);
    }

    @PostMapping("/orderSearch/ship/orderMultiShipChange")
    @ApiOperation(value = "주문정보 상세 - 배송 > 배송지 변경", response = String.class)
    public ResponseObject<String> modifyMultiShipAddrChange(@RequestBody MultiOrderShipAddrEditDto multiOrderShipAddrEditDto) throws Exception {
        return orderManagementService.modifyMultiShipAddrChange(multiOrderShipAddrEditDto);
    }

    @PostMapping("/orderSearch/noRcvList")
    @ApiOperation(value = "미수취내역 조회", response = NoRcvShipInfoGetDto.class)
    public ResponseObject<NoRcvShipInfoGetDto> getNoRcvShipInfo(@RequestBody @Valid NoRcvShipInfoSetDto infoSetDto) throws Exception {
        return orderManagementService.getNoRcvShipInfo(infoSetDto);
    }

    @PostMapping("/orderSearch/addNoRcvInfo")
    @ApiOperation(value = "미수취정보 등록", response = String.class)
    public ResponseObject<String> addNoRcvShipInfo(@RequestBody @Valid NoRcvShipSetGto noRcvShipSetGto) throws Exception {
        return orderManagementService.addNoRcvShipInfo(noRcvShipSetGto);
    }

    @PostMapping("/orderSearch/modifyNoRcvInfo")
    @ApiOperation(value = "미수취정보 수정", response = OrderSearchGetDto.class)
    public ResponseObject<String> modifyNoRcvShipInfo(@RequestBody @Valid NoRcvShipEditDto editDto) throws Exception {
        return orderManagementService.modifyNoRcvShipInfo(editDto);
    }

    @PostMapping("/orderSearch/modifyNoRcvInfoByShip")
    @ApiOperation(value = "미수취정보철회요청(Shipping전용)", response = OrderSearchGetDto.class)
    public ResponseObject<String> modifyNoRcvInfoByShip(@RequestBody @Valid NoRcvShipEditDto editDto) throws Exception {
        return orderManagementService.modifyNoRcvShipInfoForShipping(editDto);
    }

    @GetMapping("/orderSearch/delayShipping/{shipNo}")
    @ApiOperation(value = "주문정보 상세 - 발송지연", response = OrderDelayShipInfoDto.class)
    public ResponseObject<OrderDelayShipInfoDto> getDelayShipInfo(@PathVariable("shipNo") long shipNo){
        return orderManagementService.getDelayShipInfo(shipNo);
    }

    @GetMapping("/orderSearch/orderPartnerInfo")
    @ApiOperation(value = "주문정보 상세 - 배송 > 판매업체정보", response = OrderPartnerInfoDto.class)
    public ResponseObject<OrderPartnerInfoDto> getOrderPartnerInfo(@RequestParam("partnerId") String partnerId, @RequestParam("bundleNo") long bundleNo){
        return orderManagementService.getOrderPartnerInfo(partnerId, bundleNo);
    }

    @GetMapping("/orderSearch/orderPromoPopInfo/{purchaseOrderNo}")
    @ApiOperation(value = "주문정보 상세 - 행사할인정보", response = OrderPromoInfoDto.class)
    public ResponseObject<List<OrderPromoInfoDto>> getOrderPromoPopInfo(@PathVariable("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOrderPromoPopInfo(purchaseOrderNo);
    }

    @GetMapping("/orderSearch/getOrderCouponDiscountPopInfo/{purchaseOrderNo}")
    @ApiOperation(value = "주문정보 상세 - 쿠폰할인정보", response = OrderCouponDiscountDto.class)
    public ResponseObject<List<OrderCouponDiscountDto>> getOrderCouponDiscountPopInfo(@PathVariable("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOrderCouponDiscountPopInfo(purchaseOrderNo);
    }

    @GetMapping("/orderSearch/getOderGiftPopInfo/{purchaseOrderNo}")
    @ApiOperation(value = "주문정보 상세 - 사은행사정보", response = OrderGiftInfoDto.class)
    public ResponseObject<List<OrderGiftInfoDto>> getOderGiftPopInfo(@PathVariable("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOderGiftPopInfo(purchaseOrderNo);
    }

    @GetMapping("/orderSearch/getOrderSubsInfo/{purchaseOrderNo}")
    @ApiOperation(value = "주문정보 상세 - 대체주문정보", response = OrderGiftInfoDto.class)
    public ResponseObject<List<OrderSearchSubstitutionDto>> getOrderSubstitutionInfo(@PathVariable("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOrderSubstitutionInfo(purchaseOrderNo);
    }

    @GetMapping("/orderSearch/getOrderSavePopInfo/{purchaseOrderNo}")
    @ApiOperation(value = "주문정보 상세 - 주문적립", response = OrderSaveInfoDto.class)
    public ResponseObject<List<OrderSaveInfoDto>> getOrderSavePopInfo(@PathVariable("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOrderSavePopInfo(purchaseOrderNo);
    }

    @PostMapping("/orderSearchTck")
    @ApiOperation(value = "주문관리 조회", response = OrderSearchTckGetDto.class)
    public ResponseObject<List<OrderSearchTckGetDto>> getOrderSearchTck(@RequestBody OrderSearchTckSetDto searchSetDto) {
        return orderManagementService.getOrderMainSearchTck(searchSetDto);
    }

    @GetMapping("/orderSearch/getStoreShipMemo")
    @ApiOperation(value = "주문정보 상세 - 점포배송상품설명 조회", response = StoreShipMemoDto.class)
    public ResponseObject<StoreShipMemoDto> getStoreShipMemoInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo, @RequestParam("bundleNo") long bundleNo){
        return orderManagementService.getStoreShipMemoInfo(purchaseOrderNo, bundleNo);
    }

    @PostMapping("/orderSearch/setStoreShipMemo")
    @ApiOperation(value = "주문정보 상세 - 점포배송상품설명 등록/수정", response = ResponseObject.class)
    public ResponseObject<String> setStoreShipMemoInfo(@RequestBody StoreShipMemoDto memoDto) {
        return orderManagementService.setStoreShipMemoInfo(memoDto);
    }

    @GetMapping("/orderSearch/getStoreShipMsg")
    @ApiOperation(value = "주문정보 상세 - 배송메시지 조회", response = StoreShipMsgDto.class)
    public ResponseObject<StoreShipMsgDto> getShipMsgInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getShipMsgInfo(purchaseOrderNo);
    }

    @GetMapping("/orderSearch/getOrderHistoryList")
    @ApiOperation(value = "주문정보 상세 - 히스토리 내역 조회", response = ResponseObject.class)
    public ResponseObject<List<OrderHistoryListDto>> getOrderHistoryList(@RequestParam("purchaseOrderNo") long purchaseOrderNo) {
        return orderManagementService.getOrderHistoryList(purchaseOrderNo);
    }

    @GetMapping("/orderSearch/getOrderCashReceiptPopInfo")
    @ApiOperation(value = "주문정보 상세 - 영수증팝업", response = ResponseObject.class)
    public ResponseObject<OrderCashReceiptDto> getOrderCashReceiptInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo) {
        return orderManagementService.getOrderCashReceiptInfo(purchaseOrderNo);
    }

    @GetMapping("/orderSearch/getOrderShipOriginInfo")
    @ApiOperation(value = "주문에 따른 원배송정보 확인", response = ResponseObject.class)
    public ResponseObject<OrderShipOriginInfo> getOrderShipOriginInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo) {
        return orderManagementService.getOrderShipOriginInfo(purchaseOrderNo);
    }
}
