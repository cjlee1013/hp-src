package kr.co.homeplus.escrowmng.claim.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimOptEntity;
import kr.co.homeplus.escrowmng.claim.model.external.ticket.ClaimCoopTicketCheckGetDto;
import kr.co.homeplus.escrowmng.claim.model.external.ticket.ClaimCoopTicketCheckSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimBundleRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimItemRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ticket.ClaimTicketModifyDto;
import kr.co.homeplus.escrowmng.core.exception.LogicException;
import kr.co.homeplus.escrowmng.enums.ClaimResponseCode;
import kr.co.homeplus.escrowmng.enums.ExternalInfo;
import kr.co.homeplus.escrowmng.order.factory.EntityFactory;
import kr.co.homeplus.escrowmng.utils.ObjectUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClaimMultiCancelProcessService {

    private final ClaimMapperService mapperService;

    private final ClaimProcessService processService;

    private final ExternalService externalService;

    private final ClaimCommonService commonService;

    @Transactional(rollbackFor = {Exception.class, LogicException.class}, propagation = Propagation.NESTED)
    public LinkedHashMap<String, Object> addMultiTicketClaimRegister(ClaimRegSetDto regSetDto, ArrayList<Long> orderTicketList) throws Exception {

        /* 1. claim_mst 등록 */
        ClaimMstRegDto claimMstRegDto = processService.insertClaimMstProcess(regSetDto);

        /* 2.클레임 등록을 위한 클레임 데이터 생성 */
        LinkedHashMap<String, Object> createClaimData = commonService.createClaimDataMap(regSetDto);

        /* 2-1.claim_mst 에 등록된 claim_no 를 map 넣음. */
        createClaimData.put("claim_no", claimMstRegDto.getClaimNo());

        /* 3.클레임 번들 dto 생성 */
        ClaimBundleRegDto claimBundleRegDto = commonService.createClaimBundleRegDto(createClaimData);

        /* 3-1. claim_bundle 등록 */
        if(!mapperService.addClaimBundleInfo(claimBundleRegDto)){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
        }
        /* 3-1. claim_bundle_no 넣기 */
        createClaimData.put("claim_bundle_no", claimBundleRegDto.getClaimBundleNo());

        /* 4. claim_req 등록 */
        if(!mapperService.addClaimReqInfo(commonService.createClaimReqRegDto(createClaimData))){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
        }

        /* 5. claim_item, claim_ticket, claim_opt 등록 */
        this.multiTicketClaimItemPartRegProcess(createClaimData, regSetDto, orderTicketList);

        /* 6. claim_addition 등록 */
        mapperService.callByClaimAdditionReg(claimMstRegDto.getClaimNo());

        /* 7. claim_payment 등록 */
        processService.insertClaimPaymentProcess(createClaimData, claimMstRegDto);

        return createClaimData;
    }

    /**
     * 클레임 아이템 등록 Process
     * 클레임 아이템 등록 후 클레임 옵션 등록
     *
     * @param claimData 클레임 생성 데이터 맵
     * @param claimRegSetDto 클레임 등록 데이터
     * @throws Exception 오류시 오류처리
     */
    public void multiTicketClaimItemPartRegProcess(LinkedHashMap<String, Object> claimData, ClaimRegSetDto claimRegSetDto, ArrayList<Long> orderTicketList) throws Exception {
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class);

        // 클레임 상품번호
        long claimItemNo = 0L;
        // 아이템 번호별 loop
        for(ClaimPreRefundItemList itemList : claimRegSetDto.getClaimItemList()){
            //claim_item 등록용 order_item 데이터 조회
            LinkedHashMap<String, Object> claimItemInfo = mapperService.getClaimItemInfo(Long.parseLong(claimRegSetDto.getPurchaseOrderNo()), Long.parseLong(itemList.getOrderItemNo()));
            claimItemInfo.putAll(claimData);// 클레임 데이터 넣기
            ClaimItemRegDto claimItemRegDto = commonService.createClaimItemRegDto(claimItemInfo);// 클레임 등록 DTO 생성
            int claimItemQty = orderTicketList.size();// 클레임 신청 수량

            if( claimItemQty <= 0){
                throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR15);
            }
            claimItemRegDto.setClaimItemQty(claimItemQty);// 클래임 건수 입력

            /* claim_item 등록 */
            if(!mapperService.addClaimItemInfo(claimItemRegDto)){
                throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
            }
            claimItemNo = claimItemRegDto.getClaimItemNo();

            // 티켓 취소시 여기서 처리.
            if(ObjectUtils.toString(claimItemInfo.get("order_type")).equals("ORD_TICKET")){
                List<LinkedHashMap<String, Object>> orderTicketInfo = mapperService.getOrderTicketInfoForOrderTicketList(claimItemRegDto.getPurchaseOrderNo(), claimItemRegDto.getOrderItemNo(), orderTicketList);
                if(orderTicketInfo.stream().map(map -> ObjectUtils.toString(map.get("issue_channel"))).map(String::valueOf).anyMatch(str -> str.equals("COOP"))){
                    List<LinkedHashMap<String, Object>> coopTicketInfoList = orderTicketInfo.stream().filter(map -> ObjectUtils.toString(map.get("issue_channel")).equals("COOP")).collect(Collectors.toList());
                    if(coopTicketInfoList.size() != claimItemQty){
                        throw new LogicException(ClaimResponseCode.CLAIM_TICKET_ERR01);
                    }
                    for(LinkedHashMap<String, Object> coopTicketInfoMap : coopTicketInfoList){
                        ClaimCoopTicketCheckSetDto checkDto = new ClaimCoopTicketCheckSetDto();
                        checkDto.setSellerItemCd(ObjectUtils.toString(coopTicketInfoMap.get("seller_item_cd")));
                        checkDto.setTicketCd(ObjectUtils.toString(coopTicketInfoMap.get("ticket_cd")));
                        if(!this.getCoopTicketCheck(checkDto)){
                            throw new LogicException(ClaimResponseCode.CLAIM_TICKET_ERR02);
                        }
                    }
                }
                for(LinkedHashMap<String, Object> insertMap : orderTicketInfo){
                    insertMap.putAll(claimItemInfo);
                    insertMap.put("claim_item_no", claimItemNo);

                    /* claim_ticket 등록 */
                    if(!mapperService.addClaimTicketInfo(commonService.createClaimTicketDto(insertMap))){
                        throw new LogicException(ClaimResponseCode.CLAIM_TICKET_ERR03);
                    }
                }
            }

            // opt 데이터 조회
            // 주문에서 옵션별 데이터를 가지고 온다.
            ClaimOptRegDto claimOptRegDto = mapperService.getOriginOrderOptInfoForClaim(Long.parseLong(itemList.getOrderItemNo()), itemList.getOrderOptNo());
            claimOptRegDto.setClaimNo((Long) claimItemInfo.get(claimOpt.claimNo));
            claimOptRegDto.setClaimBundleNo((Long) claimItemInfo.get(claimOpt.claimBundleNo));
            claimOptRegDto.setClaimItemNo(claimItemNo);
            claimOptRegDto.setClaimOptQty(Long.parseLong(itemList.getClaimQty()));

            /* claim_opt 등록 */
            if(!mapperService.addClaimOptInfo(claimOptRegDto)){
                throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
            }
            // 아이템 임직원 할인
            mapperService.callByClaimItemEmpDiscount(claimItemRegDto.getClaimNo(), claimItemRegDto.getOrderItemNo());
        }
    }

    private Boolean getCoopTicketCheck(ClaimCoopTicketCheckSetDto checkDto) throws Exception {
        try{
            log.debug("getCoopTicketCheck ::: {}", checkDto);
            ClaimCoopTicketCheckGetDto returnDto =
                externalService.postTransfer(
                    ExternalInfo.OUTBOUND_COOP_TICKET_CHECK_FOR_CLAIM,
                    checkDto,
                    ClaimCoopTicketCheckGetDto.class
                );
            return returnDto.getUseYn().equals("N");
        } catch (Exception e){
            log.error("getCoopTicketCheck ::: checkDto :: {} ::: ErrorMsg :: {}", checkDto, e.getMessage());
            return false;
        }
    }

}
