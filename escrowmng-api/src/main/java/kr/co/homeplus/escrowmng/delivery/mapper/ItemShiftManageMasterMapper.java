package kr.co.homeplus.escrowmng.delivery.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.delivery.model.ItemShiftManageInsertModel;

@EscrowMasterConnection
public interface ItemShiftManageMasterMapper {

    void insertItemShiftManageHistory(ItemShiftManageInsertModel itemShiftManageInsertModel);

    int insertItemShift(ItemShiftManageInsertModel itemShiftManageInsertModel);

    int deleteItemShift(ItemShiftManageInsertModel itemShiftManageInsertModel);
}
