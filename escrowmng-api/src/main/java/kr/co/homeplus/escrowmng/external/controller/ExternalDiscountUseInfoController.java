package kr.co.homeplus.escrowmng.external.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.external.model.DiscountUseInfo;
import kr.co.homeplus.escrowmng.external.model.DiscountUseInfoSelect;
import kr.co.homeplus.escrowmng.external.service.ExternalDiscountUseInfoService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "[할인] 할인관련조회")
@RestController
@RequiredArgsConstructor
@RequestMapping(value="/external/discount", produces = MediaType.APPLICATION_JSON_VALUE )
public class ExternalDiscountUseInfoController {

    private final ExternalDiscountUseInfoService externalDiscountUseInfoService;

    /**
     * IDE팀 제공 (최수영K 요청)
     * @param discountUseInfoSelect
     * @return
     * @throws Exception
     */
    @PostMapping("/getDiscountUseInfoList")
    @ApiOperation(value = "할인사용정보조회", response = DiscountUseInfo.class)
    public ResponseObject<List<DiscountUseInfo>> getDiscountUseInfoList(@Valid @RequestBody DiscountUseInfoSelect discountUseInfoSelect) throws Exception {
        return ResourceConverter.toResponseObject(externalDiscountUseInfoService.getDiscountUseInfoList(discountUseInfoSelect));
    }

}
