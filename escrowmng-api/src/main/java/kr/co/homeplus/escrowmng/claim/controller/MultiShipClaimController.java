package kr.co.homeplus.escrowmng.claim.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPrevOrderCancelDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimShipInfoDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipClaimItemListGetDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipClaimItemListSetDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipInfoGetDto;
import kr.co.homeplus.escrowmng.claim.service.multiShip.MultiShipClaimService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/claim")
@RequiredArgsConstructor
@Api(tags = "주문관리 > 주문정보 상세 > 다중배송 주문취소")
public class MultiShipClaimController {

    private final MultiShipClaimService multiShipClaimService;

    @GetMapping("/getMultiShipList")
    @ApiOperation(value = "다중배송 배송지 리스트조회", response = MultiShipInfoGetDto.class)
    public ResponseObject<List<MultiShipInfoGetDto>> getMultiShipList(@RequestParam("purchaseOrderNo") long purchaseOrderNo, @RequestParam(value = "bundleNo", defaultValue = "0", required = false) long bundleNo) {
        return multiShipClaimService.getMultiShipList(purchaseOrderNo, bundleNo);
    }

    @PostMapping("/getMultiShipItemList")
    @ApiOperation(value = "다중배송 상품 리스트조회", response = ClaimPrevOrderCancelDto.class)
    public ResponseObject<List<MultiShipClaimItemListGetDto>> getMultiShipItemList(@RequestBody MultiShipClaimItemListSetDto multiShipClaimItemListSetDto) {
        return multiShipClaimService.getMultiShipItemList(multiShipClaimItemListSetDto);
    }

    @PostMapping("/multiShipPreRefundPrice")
    @ApiOperation(value = "다중배송 - 환불예정금액", response = ClaimPreRefundCalcGetDto.class)
    public ResponseObject<ClaimPreRefundCalcGetDto> getRefundCalculation(@RequestBody ClaimPreRefundCalcSetDto calcSetDto) throws Exception{
        return multiShipClaimService.getMultiShipRefundCalculation(calcSetDto);
    }

    @GetMapping("/getMultiShipInfo")
    @ApiOperation(value = "다중배송 - 배송정보조회", response = ClaimShipInfoDto.class)
    public ResponseObject<ClaimShipInfoDto> multiShipClaimShipInfo(@RequestParam("bundleNo") long bundleNo, @RequestParam(value = "multiBundleNo") long multiBundleNo) throws Exception{
        return multiShipClaimService.getMultiShipInfo(bundleNo, multiBundleNo);
    }

}
