package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.LinkedHashMap;
import kr.co.homeplus.escrowmng.utils.ObjectUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임 주문 취소 진행을 위한 DTO")
public class ClaimOrderCancelDto {

    @ApiModelProperty(value = "클레임번호", position = 1)
    private long claimNo;

    @ApiModelProperty(value = "클레임배송번호", position = 2)
    private long claimBundleNo;

    @ApiModelProperty(value = "원거래결제번호", position = 3)
    private long paymentNo;

    @ApiModelProperty(value = "주문번호", position = 4)
    private long purchaseOrderNo;

    public void setData(LinkedHashMap<String, Object> claimDataMap) {
        if(claimDataMap.get("claim_no") != null){
            this.claimNo = ObjectUtils.toLong(claimDataMap.get("claim_no"));
        }
        if(claimDataMap.get("claim_bundle_no") != null){
            this.claimBundleNo = ObjectUtils.toLong(claimDataMap.get("claim_bundle_no"));
        }
        if(claimDataMap.get("payment_no") != null){
            this.paymentNo = ObjectUtils.toLong(claimDataMap.get("payment_no"));
        }
        if(claimDataMap.get("purchase_order_no") != null){
            this.purchaseOrderNo = ObjectUtils.toLong(claimDataMap.get("purchase_order_no"));
        }
    }
}
