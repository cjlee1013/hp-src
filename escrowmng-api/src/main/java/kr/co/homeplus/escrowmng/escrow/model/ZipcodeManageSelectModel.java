package kr.co.homeplus.escrowmng.escrow.model;

import static kr.co.homeplus.escrowmng.escrow.constants.ZipcodeConstants.HYPHEN;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import lombok.Data;

@Data
@ApiModel(description = "시스템관리 > 기준정보관리 > 우편번호관리 Search DTO")
public class ZipcodeManageSelectModel {
    @ApiModelProperty(notes = "도서산간유형 (ALL:전국,JEJU:제주,ISLAND:도서산간)")
    private String schIslandType;

    @ApiModelProperty(notes = "검색키워드유형 (ROADADDR:도로명,GIBUN:지번,ZIPCODE:우편번호:BUILD_MNG_NO:건물관리번호)", required = true)
    @NotEmpty(message = "검색키워드 유형")
    private String schKeywordType;

    @ApiModelProperty(notes = "시도명 (검색키워드가 ROADADDR 일 때 사용)")
    private String schSidoType;

    @ApiModelProperty(notes = "시군구명 (검색키워드가 ROADADDR 일 때 사용)")
    private String schSigunguType;

    @ApiModelProperty(notes = "검색키워드")
    @NotEmpty(message = "검색키워드")
    @Size(min = 2, message = "검색키워드")
    private String schKeyword;

    @ApiModelProperty(notes = "건물번호")
    @Pattern(regexp = PatternConstants.BUILD_NO_PATTERN, message = "건물번호")
    private String schBuildNo;

    /** 쿼리에서 사용함 */
    @ApiModelProperty(notes = "건물본번", hidden = true)
    private Integer schBuildBonNo;
    @ApiModelProperty(notes = "건물부번", hidden = true)
    private Integer schBuildBuNo;

    public Integer getSchBuildBonNo() {
        if (EscrowString.isEmpty(this.schBuildNo)) {
            return null;
        }
        if (this.schBuildNo.contains(HYPHEN)) {
            return Integer.valueOf(this.schBuildNo.split(HYPHEN)[0]);
        } else {
            return Integer.valueOf(this.schBuildNo);
        }
    }
    public Integer getSchBuildBuNo() {
        if (EscrowString.isEmpty(this.schBuildNo)) {
            return null;
        }
        if (this.schBuildNo.contains(HYPHEN)) {
            return Integer.valueOf(this.schBuildNo.split(HYPHEN)[1]);
        }
        return null;
    }
}