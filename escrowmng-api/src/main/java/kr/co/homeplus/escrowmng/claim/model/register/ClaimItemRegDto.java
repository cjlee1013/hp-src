package kr.co.homeplus.escrowmng.claim.model.register;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClaimItemRegDto {

    @ApiModelProperty(value= "클레임 상품번호")
    private long claimItemNo;

    @ApiModelProperty(value= "클레임 번호(환불:클레임번호)")
    private long claimNo;

    @ApiModelProperty(value= "클레임 묶음번호")
    private long claimBundleNo;

    @ApiModelProperty(value= "결제번호")
    private long paymentNo;

    @ApiModelProperty(value= "주문번호")
    private long purchaseOrderNo;

    @ApiModelProperty(value= "배송번호")
    private long bundleNo;

    @ApiModelProperty(value= "원천점포ID(매출용실제점포)")
    private String originStoreId;

    @ApiModelProperty(value= "점포ID(주문서에서 넘어온 점포ID)")
    private String storeId;

    @ApiModelProperty(value= "FC점포ID")
    private String fcStoreId;

    @ApiModelProperty(value= "상품주문번호(결제완료:주문번호)")
    private long orderItemNo;

    @ApiModelProperty(value= "상품번호")
    private String itemNo;

    @ApiModelProperty(value= "상품명")
    private String itemName;

    @ApiModelProperty(value= "상품가격")
    private long itemPrice;

    @ApiModelProperty(value= "클레임 상품수량(옵션합계)")
    private long claimItemQty;

    @ApiModelProperty(value= "점포유형(Hyper, Club, Exp, DS)")
    private String storeType;

    @ApiModelProperty(value= "몰유형(DS:업체상품,TD:매장상품)")
    private String mallType;

    @ApiModelProperty(value= "세금여부(Y:과세,N:면세)")
    private String taxYn;

    @ApiModelProperty(value= "파트너 아이디")
    private String partnerId;

    @ApiModelProperty(value= "임직원할인율")
    private double empDiscountRate;

    @ApiModelProperty(value= "임직원할인금액")
    private long empDiscountAmt;

    @ApiModelProperty(value= "등록일")
    private String regDt;

    @ApiModelProperty(value= "등록아이디")
    private String regId;

    @ApiModelProperty(value= "마켓주문번호")
    private String marketOrderNo;
}
