package kr.co.homeplus.escrowmng.claim.service.StoreMultiCancel;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.storeMultiCancel.StoreMultiCancelListGetDto;
import kr.co.homeplus.escrowmng.claim.model.storeMultiCancel.StoreMultiCancelListSetDto;
import kr.co.homeplus.escrowmng.enums.OrderCode;
import kr.co.homeplus.escrowmng.order.factory.OrderResponseFactory;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class StoreMultiCancelService {

    private final StoreMultiCancelMapperService storeMultiCancelMapperService;

    /**
     * 점포 일괄주문취소 리스트 조회
     *
     * @param storeMultiCancelListSetDto
     * @return 점포 일괄주문취소 리스트 조회
     */
    public ResponseObject<List<StoreMultiCancelListGetDto>> getStoreMultiCancelList (StoreMultiCancelListSetDto storeMultiCancelListSetDto) {
        List<StoreMultiCancelListGetDto> resultList = storeMultiCancelMapperService.getStoreMultiCancelList(storeMultiCancelListSetDto);

        if (ObjectUtils.isNotEmpty(resultList)) {
            for (StoreMultiCancelListGetDto next : resultList) {
                next.setBuyerNm(PrivacyMaskingUtils.maskingUserName(next.getBuyerNm()));
                next.setBuyerMobileNo(PrivacyMaskingUtils.maskingPhone(next.getBuyerMobileNo()));
                next.setReceiverNm(PrivacyMaskingUtils.maskingUserName(next.getReceiverNm()));
                next.setShipMobileNo(PrivacyMaskingUtils.maskingPhone(next.getShipMobileNo()));
                next.setShipAddr(PrivacyMaskingUtils.maskingPhone(next.getShipAddr()));
            }
        }
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, resultList);
    }
}

