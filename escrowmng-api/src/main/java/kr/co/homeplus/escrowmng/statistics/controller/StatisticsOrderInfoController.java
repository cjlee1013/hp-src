package kr.co.homeplus.escrowmng.statistics.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.escrowmng.extract.service.ExtractHistoryService;
import kr.co.homeplus.escrowmng.statistics.model.StatisticsOrderInfoDto;
import kr.co.homeplus.escrowmng.statistics.model.StatisticsOrderInfoSelectDto;
import kr.co.homeplus.escrowmng.statistics.model.StatisticsPartnerOrderInfoDto;
import kr.co.homeplus.escrowmng.statistics.model.StatisticsStoreOrderInfoDto;
import kr.co.homeplus.escrowmng.statistics.model.StoreBasicInfoDto;
import kr.co.homeplus.escrowmng.statistics.service.StatisticsOrderInfoService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/escrow/statistics")
@Api(tags = "통계 > 상품 주문통계 / 주문결제 통계")
public class StatisticsOrderInfoController {
    private final ExtractHistoryService extractHistoryService;
    private final StatisticsOrderInfoService statisticsOrderInfoService;

    /**
     * 통계주문정보 랭킹 리스트 조회
     */
    @PostMapping("/getStatisticsOrderInfoRankingList")
    @ApiOperation(value = "통계주문정보 랭킹 리스트 조회")
    public ResponseObject<List<StatisticsOrderInfoDto>> getStatisticsOrderInfoRankingList(
            @RequestBody StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(statisticsOrderInfoSelectDto.getExtractCommonSetDto());
        // 통계주문정보 조회
        return ResourceConverter.toResponseObject(statisticsOrderInfoService.getStatisticsOrderInfoRankingList(statisticsOrderInfoSelectDto));
    }

    /**
     * 통계주문정보 점포상품 랭킹 리스트 조회
     * -. 실시간 점포 상품 판매 현황 조회, 마켓연동주문 포함
     */
    @PostMapping("/getStatisticsOrderInfoStoreItemRankingList")
    @ApiOperation(value = "통계주문정보 점포상품 랭킹 리스트 조회")
    public ResponseObject<List<StatisticsOrderInfoDto>> getStatisticsOrderInfoStoreItemRankingList(
            @RequestBody StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(statisticsOrderInfoSelectDto.getExtractCommonSetDto());
        // 통계주문정보 조회
        return ResourceConverter.toResponseObject(statisticsOrderInfoService.getStatisticsOrderInfoStoreItemRankingList(statisticsOrderInfoSelectDto));
    }

    /**
     * 통계주문정보 점포별 리스트 조회
     */
    @PostMapping("/getStatisticsOrderInfoListByStore")
    @ApiOperation(value = "통계주문정보 점포별 리스트 조회")
    public ResponseObject<List<StatisticsStoreOrderInfoDto>> getStatisticsOrderInfoListByStore(
            @RequestBody StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(statisticsOrderInfoSelectDto.getExtractCommonSetDto());
        // 통계주문정보 조회
        return ResourceConverter.toResponseObject(statisticsOrderInfoService.getStatisticsOrderInfoListByStore(statisticsOrderInfoSelectDto));
    }

    /**
     * 통계주문정보 판매업체별 리스트 조회
     */
    @PostMapping("/getStatisticsOrderInfoListByPartner")
    @ApiOperation(value = "통계주문정보 판매업체별 리스트 조회")
    public ResponseObject<List<StatisticsPartnerOrderInfoDto>> getStatisticsOrderInfoListByPartner(
            @RequestBody StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(statisticsOrderInfoSelectDto.getExtractCommonSetDto());
        // 통계주문정보 조회
        return ResourceConverter.toResponseObject(statisticsOrderInfoService.getStatisticsOrderInfoListByPartner(statisticsOrderInfoSelectDto));
    }

    /**
     * 점포 기본정보 조회
     */
    @GetMapping("/getStoreBasicInfo")
    @ApiOperation(value = "점포 기본정보 조회")
    public ResponseObject<StoreBasicInfoDto> getStoreBasicInfo(@RequestParam(value = "storeId") int storeId) {
        return ResourceConverter.toResponseObject(statisticsOrderInfoService.getStoreBasicInfo(storeId));
    }
}
