package kr.co.homeplus.escrowmng.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "통계주문정보 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class StatisticsOrderInfoDto {
    @ApiModelProperty(value = "날짜", position = 1)
    private String basicDt;

    @ApiModelProperty(value = "대상시간", position = 2)
    private String basicTime;

    @ApiModelProperty(value = "점포유형(HYPER, CLUB, EXP, AURORA, DS)", position = 3)
    private String storeType;

    @ApiModelProperty(value = "상품번호(상품 일때만 입력)", position = 4)
    private String itemNo;

    @ApiModelProperty(value = "상품명1", position = 5)
    private String itemName;

    @ApiModelProperty(value = "파트너ID", position = 6)
    private String partnerId;

    @ApiModelProperty(value = "세금여부(Y:과세,N:면세)", position = 7)
    private String taxYn;

    @ApiModelProperty(value = "상품수량 (SUM(상품옵션수량))", position = 8)
    private long itemQty;

    @ApiModelProperty(value = "클레임 상품수량 (SUM(상품옵션수량))", position = 9)
    private long claimQty;

    @ApiModelProperty(value = "주문금액 ((상품가격+옵션가격N)*옵션수량N) + (추가상품가격*추가상품수량)", position = 10)
    private long orderPrice;

    @ApiModelProperty(value = "클레임 주문금액(상품금액*클레임건수)", position = 11)
    private long claimPrice;

    @ApiModelProperty(value = "순판매수량(주문-클레임)", position = 12)
    private long totQty;

    @ApiModelProperty(value = "순판매금액(주문-클레임)", position = 13)
    private long totPrice;

    @ApiModelProperty(value = "대카테고리명", position = 14)
    private String lcateNm;

    @ApiModelProperty(value = "중카테고리명", position = 15)
    private String mcateNm;

    @ApiModelProperty(value = "소카테고리명", position = 16)
    private String scateNm;

    @ApiModelProperty(value = "세카테고리명", position = 17)
    private String dcateNm;

    @ApiModelProperty(value = "플랫폼(PC,APP,MWEB)", position = 18)
    private String platform;

    @ApiModelProperty(value = "몰유형(DS:업체상품,TD:매장상품)", position = 19)
    private String mallType;

    @ApiModelProperty(value = "수수료율", position = 20)
    private BigDecimal commissionRate;

    @ApiModelProperty(value = "원금액(할인 미적용금액)", position = 21)
    private long orgPrice;

    @ApiModelProperty(value = "상품금액(상품 판매가)", position = 22)
    private long itemPrice;

    @ApiModelProperty(value = "매출금액(in vat)", position = 23)
    private long paymentInvat;

    @ApiModelProperty(value = "매출금액(Ex vat)", position = 24)
    private long paymentExvat;

    @ApiModelProperty(value = "정산(In vat)", position = 25)
    private long settleInvat;

    @ApiModelProperty(value = "정산(Ex vat)", position = 26)
    private long settleExvat;

    @ApiModelProperty(value = "판매수수료", position = 27)
    private int commissionAmt;

    @ApiModelProperty(value = "총할인금액", position = 28)
    private long discountAmt;

    @ApiModelProperty(value = "결제금액 합계", position = 29)
    private long paymentAmt;

    @ApiModelProperty(value = "등록일시", position = 30)
    private String regDt;

    @ApiModelProperty(value = "랭킹번호", position = 31)
    private int rankingNo;

    @ApiModelProperty(value = "매출금액", position = 32)
    private long saleAmt;

    @ApiModelProperty(value = "순매출금액", position = 33)
    private long netSaleAmt;

    @ApiModelProperty(value = "점포코드", position = 34)
    private String storeId;
}