package kr.co.homeplus.escrowmng.pg.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodDto;

@EscrowMasterConnection
public interface PaymentMethodMasterMapper {
    int insertPaymentMethod(PaymentMethodDto paymentMethod);
}
