package kr.co.homeplus.escrowmng.claim.model.partner;


import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RealGridOptionInfo(checkBar = true)
public class ClaimPartnerListSetDto {

    @ApiModelProperty(value = "처리상태", position = 1)
    private String schClaimStatus;

    @ApiModelProperty(value = "클레임 검색기간", position = 2)
    private String claimDateType;

    @ApiModelProperty(value = "조회시작일", position = 3)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", position = 4)
    private String schEndDt;

    @ApiModelProperty(value = "검색어", position = 5)
    private String claimSearchType;

    @ApiModelProperty(value = "검색어", position = 6)
    private String schClaimSearch;

    @ApiModelProperty(value = "신청자", position = 7)
    private String schClaimChannel;

    @ApiModelProperty(value = "파트너Id", position = 8)
    private String partnerId;

    @ApiModelProperty(value = "보드 건 수 클레임 상태", position = 9)
    private String schStatus;

    @ApiModelProperty(value = "수거 상태", position = 10)
    private String schPickupStatus;

    @ApiModelProperty(value = "배송 상태", position = 11)
    private String schExchStatus;

    @ApiModelProperty(value = "클레임타입", position = 12)
    private String claimType;

}
