package kr.co.homeplus.escrowmng.claim.model.partner;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimPartnerListGetDto {

    @ApiModelProperty(value = "클레임번들번호")
    private String claimBundleNo;

    @ApiModelProperty(value = "클레임번호")
    private String claimNo;

    @ApiModelProperty(value = "클레임요청번호")
    private String claimReqNo;

    @ApiModelProperty(value = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(value = "배송번호")
    private String bundleNo;

    @ApiModelProperty(value = "상품 주문번호")
    private String orderItemNo;

    @ApiModelProperty(value = "처리상태")
    private String claimStatus;

    @ApiModelProperty(value = "처리상태코드")
    private String claimStatusCode;

    @ApiModelProperty(value = "클레임타입")
    private String claimType;

    @ApiModelProperty(value = "신청일")
    private String requestDt;

    @ApiModelProperty(value = "신청사유")
    private String claimReasonType;

    @ApiModelProperty(value = "접수채널")
    private String requestId;

    @ApiModelProperty(value = "처리일")
    private String processDt;

    @ApiModelProperty(value = "승인채널")
    private String approveId;

    @ApiModelProperty(value = "승인채널")
    private String approveDt;

    @ApiModelProperty(value = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    private String itemName;

    @ApiModelProperty(value = "옵션명")
    private String optItemNm;

    @ApiModelProperty(value = "신청수량")
    private String claimItemQty;

    @ApiModelProperty(value = "상품금액", position = 10)
    private String itemPrice;

    @ApiModelProperty(value = "총 상품금액", position = 10)
    private String totItemPrice;

    @ApiModelProperty(value = "거부사유")
    private String rejectReasonType;

    @ApiModelProperty(value = "구매자이름")
    private String buyerNm;

    @ApiModelProperty(value = "구매자연락처")
    private String buyerMobileNo;

    @ApiModelProperty(value = "수령인이름")
    private String receiverNm;

    @ApiModelProperty(value = "수령인연락처")
    private String shipMobileNo;

    @ApiModelProperty(value = "우편번호")
    private String zipcode;

    @ApiModelProperty(value = "주소")
    private String addr;

    @ApiModelProperty(value = "배송메시지")
    private String dlvShipMsg;

    @ApiModelProperty(value = "배송비지급")
    private String shipPriceType;

    @ApiModelProperty(value = "수거상태")
    private String pickStatus;

    @ApiModelProperty(value = "수거상태코드")
    private String pickStatusCode;

    @ApiModelProperty(value = "수거완료일")
    private String pickP3Dt;

    @ApiModelProperty(value = "반품배송비")
    private String returnShipAmt;

    @ApiModelProperty(value = "반품책임")
    private String whoReason;

    @ApiModelProperty(value = "클레임비용결제")
    private String claimShipFeeEnclose;

    @ApiModelProperty(value = "배송방법")
    private String shipMethod;

    @ApiModelProperty(value = "택배사코드")
    private String dlvCd;

    @ApiModelProperty(value = "택배사")
    private String dlvCdNm;

    @ApiModelProperty(value = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(value = "수거방법")
    private String pickShipType;

    @ApiModelProperty(value = "수거택배사코드")
    private String pickDlvCd;

    @ApiModelProperty(value = "수거택배사")
    private String pickDlvCdNm;

    @ApiModelProperty(value = "수거송장번호")
    private String pickInvoiceNo;

    @ApiModelProperty(value = "보류설정일")
    private String pendingDt;

    @ApiModelProperty(value = "수거지우편번호")
    private String pickZipCode;

    @ApiModelProperty(value = "수거지주소")
    private String pickAddr;

    @ApiModelProperty(value = "교환배송상태코드")
    private String exchStatusCode;

    @ApiModelProperty(value = "교환배송상태")
    private String exchStatus;

    @ApiModelProperty(value = "교환배송 방법")
    private String exchShipNm;

    @ApiModelProperty(value = "교환배송 택배사")
    private String exchDlvCdNm;

    @ApiModelProperty(value = "교환배송 택배사 코드")
    private String exchDlvCd;

    @ApiModelProperty(value = "교환배송 송장번호")
    private String exchInvoiceNo;

    @ApiModelProperty(value = "교환배송 처리일")
    private String exchD0Dt;

    @ApiModelProperty(value = "교환배송지 우편번호")
    private String exchZipcode;

    @ApiModelProperty(value = "교환배송지주소")
    private String exchAddr;

    @ApiModelProperty(value = "업체상품코드")
    private String sellerItemCd;

    @ApiModelProperty(value = "업체옵션코드")
    private String sellerOptCd;

    @ApiModelProperty(value = "관리코드")
    private String sellerManageCd;

    @ApiModelProperty(value = "클레임 완료일자")
    private String completeDt;

    @ApiModelProperty(value = "거부일자")
    private String rejectDt;
}
