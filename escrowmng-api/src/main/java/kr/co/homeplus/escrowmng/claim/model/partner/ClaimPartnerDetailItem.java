package kr.co.homeplus.escrowmng.claim.model.partner;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimPartnerDetailItem {

    @ApiModelProperty(value = "상품주문번호", position = 1)
    private String orderItemNo;

    @ApiModelProperty(value = "상품번호", position = 2)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 3)
    private String itemName;

    @ApiModelProperty(value = "옵션명", position = 4)
    private String optItemName;

    @ApiModelProperty(value = "클레임수량", position = 5)
    private String claimItemQty;

    @ApiModelProperty(value = "상품금액", position = 6)
    private String itemPrice;

    @ApiModelProperty(value = "클레임 상품번호", position = 7)
    private String claimItemNo;


}
