package kr.co.homeplus.escrowmng.delivery.service;

import java.util.List;
import kr.co.homeplus.escrowmng.delivery.mapper.PickupStoreManageSlaveMapper;
import kr.co.homeplus.escrowmng.delivery.model.PickupStoreLockerModel;
import kr.co.homeplus.escrowmng.delivery.model.PickupStoreManageModel;
import kr.co.homeplus.escrowmng.delivery.model.PickupStoreManageSelectModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PickupStoreManageService {
    private final PickupStoreManageSlaveMapper pickupStoreManageSlaveMapper;

    /**
     * 픽업점포관리 리스트 조회
     * @param pickupStoreManageSelectModel
     * @return
     */
    public List<PickupStoreManageModel> getPickupStoreManageList(PickupStoreManageSelectModel pickupStoreManageSelectModel) {
        return pickupStoreManageSlaveMapper.selectPickupStoreManageList(pickupStoreManageSelectModel);
    }

    /**
     * 픽업점포 락커 리스트 조회
     * @param storeId
     * @return
     */
    public List<PickupStoreLockerModel> getPickupStoreLockerList(int storeId) {
        return pickupStoreManageSlaveMapper.selectPickupStoreLockerList(storeId);
    }
}