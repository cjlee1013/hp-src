package kr.co.homeplus.escrowmng.claim.model.register;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimOptRegDto {

    @ApiModelProperty(value= "클레임 옵션번호")
    private long claimOptNo;

    @ApiModelProperty(value= "클레임 번호")
    private long claimNo;

    @ApiModelProperty(value= "클레임 묶음번호")
    private long claimBundleNo;

    @ApiModelProperty(value= "클레임 상품번호")
    private long claimItemNo;

    @ApiModelProperty(value= "주문 옵션번호")
    private long orderOptNo;

    @ApiModelProperty(value= "옵션번호")
    private long optNo;

    @ApiModelProperty(value= "옵션금액")
    private long optPrice;

    @ApiModelProperty(value= "클레임 옵션 수량")
    private long claimOptQty;

    @ApiModelProperty(value= "등록일")
    private String regDt;

    @ApiModelProperty(value= "마켓주문번호")
    private String marketOrderNo;

    @ApiModelProperty(value= "마켓상품주문번호")
    private String marketOrderItemNo;

}
