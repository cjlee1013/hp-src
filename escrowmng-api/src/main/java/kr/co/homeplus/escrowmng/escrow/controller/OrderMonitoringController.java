package kr.co.homeplus.escrowmng.escrow.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.homeplus.escrowmng.escrow.service.OrderMonitoringService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "주문 모니터링")
@RequestMapping("/escrow/monitoring")
@RestController
@RequiredArgsConstructor
public class OrderMonitoringController {

    private final OrderMonitoringService orderMonitoringService;
    /**
     * 2분내 주문 데이터 없는 경우 slack notify
     */
    @GetMapping("/getOrderCountCheck")
    @ApiOperation(value = "주문데이터 없는 경우 notify")
    public ResponseObject<String> getOrderCountCheck()
    {
        return ResourceConverter.toResponseObject(orderMonitoringService.getOrderCountCheck());
    }

}
