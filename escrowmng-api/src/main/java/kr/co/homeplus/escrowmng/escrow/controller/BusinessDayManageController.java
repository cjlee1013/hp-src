package kr.co.homeplus.escrowmng.escrow.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.escrow.model.BusinessDayManageModel;
import kr.co.homeplus.escrowmng.escrow.model.BusinessDayManageSelectModel;
import kr.co.homeplus.escrowmng.escrow.service.BusinessDayManageService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/escrow/businessDay")
@Api(tags = "시스템관리 > 영업일관리 > 영업일관리")
public class BusinessDayManageController {

    private final BusinessDayManageService businessDayManageService;

    @GetMapping("/getBusinessDayList")
    @ApiOperation(value = "영업일 리스트 조회 (월단위)", response = BusinessDayManageModel.class)
    public ResponseObject<List<BusinessDayManageModel>> getBusinessDayList(BusinessDayManageSelectModel businessDayManageSelectModel) {
        return ResourceConverter.toResponseObject(businessDayManageService.getBusinessDayList(businessDayManageSelectModel));
    }

    @PostMapping("/saveBusinessDay")
    @ApiOperation(value = "영업일 등록")
    public ResponseObject<BusinessDayManageModel> saveBusinessDay(@Valid @RequestBody BusinessDayManageModel businessDayManageModel) {
        return ResourceConverter.toResponseObject(businessDayManageService.saveBusinessDay(businessDayManageModel));
    }

    @PostMapping("/updateBusinessDay")
    @ApiOperation(value = "영업일 수정")
    public ResponseObject<BusinessDayManageModel> updateBusinessDay(@Valid @RequestBody BusinessDayManageModel businessDayManageModel) {
        return ResourceConverter.toResponseObject(businessDayManageService.updateBusinessDay(businessDayManageModel));
    }
}
