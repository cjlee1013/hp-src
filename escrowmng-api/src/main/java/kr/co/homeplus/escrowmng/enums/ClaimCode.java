package kr.co.homeplus.escrowmng.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimCode {

    CLAIM_STATUS_C1("C1", "신청"),
    CLAIM_STATUS_C2("C2", "승인"),
    CLAIM_STATUS_C3("C3", "완료"),
    CLAIM_STATUS_C4("C4", "철회"),
    CLAIM_STATUS_C8("C8", "보류"),
    CLAIM_STATUS_C9("C9", "거부"),

    CLAIM_PAYMENT_STATUS_P0("P0", "대기"),
    CLAIM_PAYMENT_STATUS_P1("P1", "요청"),
    CLAIM_PAYMENT_STATUS_P2("P2", "진행중"),
    CLAIM_PAYMENT_STATUS_P3("P3", "완료"),
    CLAIM_PAYMENT_STATUS_P4("P4", "실패"),

    CLAIM_CONVERT_PAYMENT_TYPE_PG("PG", "PG"),
    CLAIM_CONVERT_PAYMENT_TYPE_MHC("MP", "MHC"),
    CLAIM_CONVERT_PAYMENT_TYPE_DGV("DG", "DGV"),
    CLAIM_CONVERT_PAYMENT_TYPE_OCB("OC", "OCB"),
    CLAIM_CONVERT_PAYMENT_TYPE_MILEAGE("PP", "MILEAGE"),
    CLAIM_CONVERT_PAYMENT_TYPE_FREE("FR", "FREE"),

    CLAIM_ACCUMULATE_REQ_SEND_N("N", "미전송"),
    CLAIM_ACCUMULATE_REQ_SEND_I("I", "요청중"),
    CLAIM_ACCUMULATE_REQ_SEND_Y("Y", "전송성공"),
    CLAIM_ACCUMULATE_REQ_SEND_E("E", "전송실패"),

    CLAIM_PICKUP_STATUS_NN("NN", "수거대기"),
    CLAIM_PICKUP_STATUS_N0("N0", "수거대기"),
    CLAIM_PICKUP_STATUS_P0("P0", "수거요청"),
    CLAIM_PICKUP_STATUS_P1("P1", "수거예정"),
    CLAIM_PICKUP_STATUS_P2("P2", "수거중"),
    CLAIM_PICKUP_STATUS_P3("P3", "수거완료"),
    CLAIM_PICKUP_STATUS_P8("P8", "수거실패"),

    CLAIM_EXCH_STATUS_D0("D0", "출고대기"),
    CLAIM_EXCH_STATUS_D1("D1", "교환배송"),
    CLAIM_EXCH_STATUS_D2("D2", "배송중"),
    CLAIM_EXCH_STATUS_D3("D3", "배송완료")
    ;

    @Getter
    private final String code;

    @Getter
    private final String description;

}
