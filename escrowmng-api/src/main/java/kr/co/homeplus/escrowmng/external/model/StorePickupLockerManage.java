package kr.co.homeplus.escrowmng.external.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "[옴니연동] 픽업점포락정보 관리")
public class StorePickupLockerManage {

    @ApiModelProperty(value = "점포ID", position = 1)
    @NotEmpty(message = "점포ID")
    @AllowInput(types = {PatternConstants.NUM}, message = "점포ID")
    private String storeId;

    @ApiModelProperty(value = "장소번호", position = 2)
    @NotEmpty(message = "장소번호")
    @AllowInput(types = {PatternConstants.NUM}, message = "장소번호")
    private String placeNo;

    @ApiModelProperty(value = "락커번호", position = 3)
    @NotEmpty(message = "락커번호")
    @AllowInput(types = {PatternConstants.NUM}, message = "락커번호")
    private String lockerNo;

    @ApiModelProperty(value = "사용여부", position = 4)
    @NotEmpty(message = "사용여부")
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "사용여부(Y|N)")
    private String useYn;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{storeId:").append(storeId)
            .append(", placeNo:").append(placeNo)
            .append(", lockerNo:").append(lockerNo)
            .append(", useYn:").append(useYn)
            .append("}");

        return sb.toString();
    }
}
