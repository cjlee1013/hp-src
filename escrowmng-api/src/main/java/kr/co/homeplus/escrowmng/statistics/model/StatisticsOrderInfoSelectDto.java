package kr.co.homeplus.escrowmng.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.escrowmng.extract.model.ExtractCommonSetDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "통계주문정보 조회 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class StatisticsOrderInfoSelectDto {
    @ApiModelProperty(value = "날짜", position = 1)
    private String schBasicDt;

    @ApiModelProperty(value = "당일여부", position = 2)
    private String schTodayYn;

    @ApiModelProperty(value = "점포유형(HYPER, CLUB, EXP, AURORA, DS)", position = 3)
    private String schStoreType;

    @ApiModelProperty(value = "점포ID", position = 4)
    private String schStoreId;

    @ApiModelProperty(value = "상품번호리스트", position = 5)
    private List<String> schItemNoList;

    @ApiModelProperty(value = "파트너ID", position = 6)
    private String schPartnerId;

    @ApiModelProperty(value = "대카테고리코드", position = 7)
    private String schLcateCd;

    @ApiModelProperty(value = "중카테고리코드", position = 8)
    private String schMcateCd;

    @ApiModelProperty(value = "소카테고리코드", position = 9)
    private String schScateCd;

    @ApiModelProperty(value = "세카테고리코드", position = 10)
    private String schDcateCd;

    @ApiModelProperty(value = "조회시작일자", position = 11)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일자", position = 12)
    private String schEndDt;

    @ApiModelProperty(value = "조회마켓타입(ALL:전체,EXCEPT:제휴제외,NAVER:네이버,11ST:11번가)", position = 13)
    private String schMarketType;

    @ApiModelProperty(value = "조회그룹기준(ITEM:상품기준,STORE:점포기준)", position = 14)
    private String schGroupStd;

    @ApiModelProperty(value = "추출이력 저장용 DTO", position = 15)
    ExtractCommonSetDto extractCommonSetDto;
}