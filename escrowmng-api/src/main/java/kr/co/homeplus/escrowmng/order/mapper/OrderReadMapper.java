package kr.co.homeplus.escrowmng.order.mapper;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.order.model.management.OrderGiftItemListDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderHistoryListDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchClaimDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchDetailDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchPaymentDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchPriceDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchProdDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchShipDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchGetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchSetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchSubstitutionDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchTckGetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchTckSetDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.MultiOrderShipAddrInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderCashReceiptDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderCouponDiscountDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderDelayShipInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderGiftInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderPartnerInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderPromoInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderSaveInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderShipAddrInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderShipTotalInfoDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipInfoGetDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipInfoSetDto;
import kr.co.homeplus.escrowmng.order.model.ship.OrderShipAddrEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.OrderShipOriginInfo;
import kr.co.homeplus.escrowmng.order.model.ship.StoreShipMemoDto;
import kr.co.homeplus.escrowmng.order.model.ship.StoreShipMsgDto;
import kr.co.homeplus.escrowmng.order.sql.OrderSearchSql;
import kr.co.homeplus.escrowmng.order.sql.OrderShipSql;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;

@EscrowSlaveConnection
public interface OrderReadMapper {

    @ResultType(value = OrderSearchGetDto.class)
    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderSearchInfo")
    List<OrderSearchGetDto> selectOrderSearchInfo(OrderSearchSetDto orderSearchSetDto);

    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderDetailUserInfo")
    OrderSearchProdDto selectOrderDetailUserInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderDetailPriceInfo")
    OrderSearchPriceDto selectOrderDetailPriceInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @ResultType(value = OrderSearchDetailDto.class)
    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderDetailPurchaseInfo")
    List<OrderSearchDetailDto> selectOrderDetailPurchaseInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @ResultType(value = OrderSearchDetailDto.class)
    @SelectProvider(type = OrderSearchSql.class, method = "selectMultiOrderDetailPurchaseInfo")
    List<OrderSearchDetailDto> selectMultiOrderDetailPurchaseInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @ResultType(value = OrderSearchPaymentDto.class)
    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderDetailPaymentInfo")
    List<OrderSearchPaymentDto> selectOrderDetailPaymentInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @ResultType(value = OrderSearchShipDto.class)
    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderDetailShippingInfo")
    List<OrderSearchShipDto> selectOrderDetailShippingInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("mallType") String mallType);

    @ResultType(value = OrderSearchShipDto.class)
    @SelectProvider(type = OrderSearchSql.class, method = "selectMultiOrderDetailShippingInfo")
    List<OrderSearchShipDto> selectMultiOrderDetailShippingInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @ResultType(value = OrderSearchClaimDto.class)
    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderDetailClaimInfo")
    List<OrderSearchClaimDto> selectOrderDetailClaimInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @ResultType(value = OrderShipAddrInfoDto.class)
    @SelectProvider(type = OrderShipSql.class, method = "selectOrderShipAddrInfo")
    List<OrderShipAddrInfoDto> selectOrderShipAddrInfo(@Param("bundleNo") long bundleNo);

    @ResultType(value = MultiOrderShipAddrInfoDto.class)
    @SelectProvider(type = OrderShipSql.class, method = "selectMultiOrderShipAddrInfo")
    List<MultiOrderShipAddrInfoDto> selectMultiOrderShipAddrInfo(@Param("multiBundleNo") long multiBundleNo);

    @ResultType(value = OrderShipTotalInfoDto.class)
    @SelectProvider(type = OrderShipSql.class, method = "selectOrderShipInfo")
    List<OrderShipTotalInfoDto> selectOrderShipInfo(@Param("bundleNo") long bundleNo, @Param("shipStatus") String shipStatus);

    @ResultType(value = NoRcvShipInfoGetDto.class)
    @SelectProvider(type = OrderShipSql.class, method = "selectNoRcvShipInfo")
    NoRcvShipInfoGetDto selectNoRcvShipInfo(NoRcvShipInfoSetDto infoSetDto);

    @SelectProvider(type = OrderShipSql.class, method = "selectNoRcvMakeData")
    LinkedHashMap<String, Object> selectNoRcvMakeData(@Param("bundleNo") long bundleNo);

    @SelectProvider(type = OrderShipSql.class, method = "selectNoRcvDuplCheck")
    HashMap<String, Object> selectNoRcvDuplCheck(@Param("bundleNo") long bundleNo);

    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderGiftItemList")
    List<OrderGiftItemListDto> selectOrderGiftItemList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = OrderSearchSql.class, method = "selectShippingDelayInfo")
    OrderDelayShipInfoDto selectShippingDelayInfo(@Param("shipNo") long shipNo);

    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderPartnerInfo")
    OrderPartnerInfoDto selectOrderPartnerInfo(@Param("partnerId") String partnerId, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderPromoPopInfo")
    List<OrderPromoInfoDto> selectOrderPromoPopInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OrderSearchSql.class, method = "selectCouponDiscountPopInfo")
    List<OrderCouponDiscountDto> selectCouponDiscountPopInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderGiftPopInfo")
    List<OrderGiftInfoDto> selectOrderGiftPopInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderSubstitutionData")
    List<OrderSearchSubstitutionDto>selectOrderSubstitutionData(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OrderShipSql.class, method = "selectSafetyIssueInfo")
    LinkedHashMap<String, Object> selectSafetyIssueInfo(@Param("bundleNo") long bundleNo);

    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderSavePopInfo")
    List<OrderSaveInfoDto> selectOrderSavePopInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OrderShipSql.class, method = "selectShippingPresentStatus")
    List<LinkedHashMap<String, Object>> selectShippingPresentStatus(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OrderSearchSql.class, method = "selectCmbnClaimInfo")
    String selectCmbnClaimInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @ResultType(value = OrderSearchTckGetDto.class)
    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderSearchInfoTck")
    List<OrderSearchTckGetDto> selectOrderSearchInfoTck(OrderSearchTckSetDto orderSearchSetDto);

    @SelectProvider(type = OrderShipSql.class, method = "selectStoreShipMemoInfo")
    StoreShipMemoDto selectStoreShipMemoInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = OrderShipSql.class, method = "selectStoreShipMsgInfo")
    StoreShipMsgDto selectStoreShipMsgInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderHistoryList")
    List<OrderHistoryListDto> selectOrderHistoryList(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OrderShipSql.class, method = "selectOriPurchaseOrderInfoFromCombine")
    OrderShipOriginInfo selectOriPurchaseOrderInfoFromCombine(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderCashReceiptInfo")
    OrderCashReceiptDto selectOrderCashReceiptInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = OrderShipSql.class, method = "selectNonShipCompleteInfo")
    LinkedHashMap<String, Object> selectNonShipCompleteInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = OrderSearchSql.class, method = "selectOrderTypeCheck")
    String selectOrderTypeCheck(@Param("purchaseOrderNo") long purchaseOrderNo);
}
