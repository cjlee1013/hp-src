package kr.co.homeplus.escrowmng.escrow.model;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import lombok.Data;

/**
 * 영업일 정보 Dto
 */
@Data
public class BusinessDayManageModel {
    @ApiModelProperty(notes = "영업일관리 고유번호")
    private String businessNo;

    @ApiModelProperty(notes = "영업일(YYYY-MM-DD)", required = true)
    @NotEmpty(message = "영업일")
    private String businessDay;

    @ApiModelProperty(notes = "비영업일 여부(y:비영업일,n:영업일)", required = true)
    @Pattern(regexp = PatternConstants.YN_PATTERN_SMALL_LETTER, message="비영업일 여부")
    private String holidayYn;

    @ApiModelProperty(notes = "배송지연 여부(Y:배송지연,N:배송정상)", required = true)
    @Pattern(regexp = PatternConstants.YN_PATTERN_SMALL_LETTER, message="배송지연 여부")
    private String nonDeliveryYn;

    @ApiModelProperty(notes = "정산불가 여부(Y:정산불가,N:정산가능)", required = true)
    @Pattern(regexp = PatternConstants.YN_PATTERN_SMALL_LETTER, message="정산불가 여부")
    private String nonSettleYn;

    @ApiModelProperty(notes = "등록자id")
    private String regId;
    @ApiModelProperty(notes = "수정자id")
    private String chgId;
}
