package kr.co.homeplus.escrowmng.claim.sql;

import org.apache.ibatis.jdbc.SQL;

public class ClaimCommonSql {

    public static String rowNumber(String tableName, String standardColumn, String sortColumn, String alias) {
        return "(".concat(rowNumber(tableName, standardColumn, sortColumn)).concat(") ").concat(alias);
    }

    public static String rowNumber(String tableName, String standardColumn, String sortColumn) {
        return new SQL()
            .SELECT(
                "*",
                "CASE @GROUPING WHEN ".concat(standardColumn).concat(" THEN @RANKT := @RANK + 1 ELSE @RANK := 1 END AS RANKING"),
                "@GROUPING := ".concat(standardColumn)
            )
            .FROM(
                tableName,
                "(SELECT @GROUPING := '', @RANK := 0) XX"
            )
            .ORDER_BY(
                sortColumn
            ).toString();
    }

}
