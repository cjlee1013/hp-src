package kr.co.homeplus.escrowmng.delivery.model;

import static kr.co.homeplus.escrowmng.escrow.constants.ZipcodeConstants.HYPHEN;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import lombok.Data;

@Data
@ApiModel(description = "원거리배송 관리 slot 조회")
public class RemoteShipManageSelectModel {

    @ApiModelProperty(value = "우편번호코드", position = 96)
    private String schZipcode;

    @NotEmpty
    @Pattern(regexp = PatternConstants.STORE_TYPE_PATTERN, message="점포유형(HYPER, CLUB, EXP)")
    @ApiModelProperty(value = "점포유형", position = 97)
    private String schStoreType;

    @NotNull
    @ApiModelProperty(value = "점포Id", position = 98)
    private int schStoreId;

    @NotEmpty(message = "요일은 하나 이상 선택 해야 합니다.")
    @ApiModelProperty(value = "요일", position = 99)
    private List<String> shipWeekdayList;

    @ApiModelProperty(value = "slot 리스트", position = 100)
    private List<RemoteShipSlotModel> remoteShipSlotModelList;

    @ApiModelProperty(notes = "시/도", position = 5)
    private String schSidoType;

    @ApiModelProperty(notes = "시군구", position = 6)
    private String schSigunguType;

    @ApiModelProperty(notes = "검색어(도로명)", position = 7)
    private String schAddrKeyword;

    @ApiModelProperty(notes = "검색어(건물번호)", position = 8)
    @Pattern(regexp = PatternConstants.BUILD_NO_PATTERN, message = "검색어(건물번호)")
    private String schBuildNo;

    /** 쿼리에서 사용함 */
    @ApiModelProperty(notes = "건물본번", hidden = true)
    private String schBuildBonNo;
    @ApiModelProperty(notes = "건물부번", hidden = true)
    private String schBuildBuNo;

    public String getSchBuildBonNo() {
        if (EscrowString.isEmpty(this.schBuildNo)) {
            return null;
        }
        if (this.schBuildNo.contains(HYPHEN)) {
            return this.schBuildNo.split(HYPHEN)[0];
        } else {
            return this.schBuildNo;
        }
    }
    public String getSchBuildBuNo() {
        if (EscrowString.isEmpty(this.schBuildNo)) {
            return null;
        }
        if (this.schBuildNo.contains(HYPHEN)) {
            return this.schBuildNo.split(HYPHEN)[1];
        }
        return null;
    }

}
