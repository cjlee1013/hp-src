package kr.co.homeplus.escrowmng.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 점포배송정보 > 상품별 Shift 관리 INSERT 용 DTO")
public class ItemShiftManageInsertModel {
    @ApiModelProperty(notes = "점포유형(HYPER,EXP,CLUB)", position = 1, required = true)
    @Pattern(regexp = PatternConstants.STORE_TYPE_PATTERN, message="점포유형(HYPER,EXP,CLUB)")
    @NotEmpty(message = "점포유형(HYPER,EXP,CLUB)")
    private String storeType;

    @ApiModelProperty(notes = "점포ID", position = 2, required = true)
    @NotEmpty(message = "점포ID")
    private String storeId;

    @ApiModelProperty(notes = "상품번호", position = 3, required = true)
    @NotEmpty(message = "상품번호")
    private String itemNo;

    @ApiModelProperty(notes = "Shift1", position = 4, required = true)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "Shift1")
    private String shift1;

    @ApiModelProperty(notes = "Shift2", position = 5, required = true)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "Shift2")
    private String shift2;

    @ApiModelProperty(notes = "Shift3", position = 6, required = true)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "Shift3")
    private String shift3;

    @ApiModelProperty(notes = "Shift4", position = 7, required = true)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "Shift4")
    private String shift4;

    @ApiModelProperty(notes = "Shift5", position = 8, required = true)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "Shift5")
    private String shift5;

    @ApiModelProperty(notes = "Shift6", position = 9, required = true)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "Shift6")
    private String shift6;

    @ApiModelProperty(notes = "Shift7", position = 9, required = true)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "Shift7")
    private String shift7;

    @ApiModelProperty(notes = "Shift8", position = 9, required = true)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "Shift8")
    private String shift8;

    @ApiModelProperty(notes = "Shift9", position = 9, required = true)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "Shift9")
    private String shift9;

    @ApiModelProperty(notes = "등록자", position = 10, required = true)
    private String regId;

    @ApiModelProperty(notes = "수정자", position = 11, required = true)
    private String chgId;

    /** 실제 데이터 insert/update 할 때 필요함 */
    @ApiModelProperty(notes = "SHIFT 대상번호 / 사용여부", hidden = true)
    private Map<String, String> shiftInfoMap;
}
