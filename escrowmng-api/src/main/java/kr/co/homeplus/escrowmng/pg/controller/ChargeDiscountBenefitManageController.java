package kr.co.homeplus.escrowmng.pg.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.pg.model.ChargeDiscountBenefitManageDto;
import kr.co.homeplus.escrowmng.pg.model.ChargeDiscountBenefitManageSelectDto;
import kr.co.homeplus.escrowmng.pg.service.ChargeDiscountBenefitManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/pg/paymentMethod")
@Api(tags = "결제관리 > 결제수단관리 > 청구할인 혜택 관리")
public class ChargeDiscountBenefitManageController {

    private final ChargeDiscountBenefitManageService chargeDiscountBenefitManageService;

    @GetMapping("/getChargeDiscountBenefitManage")
    @ApiOperation(value = "청구할인 혜택 조회", response = ChargeDiscountBenefitManageDto.class)
    public ResponseObject getChargeDiscountBenefitManage(@Valid ChargeDiscountBenefitManageSelectDto chargeDiscountBenefitManageSelectDto) throws Exception {
        log.info("[getChargeDiscountBenefitManage]");
        List<ChargeDiscountBenefitManageDto> chargeDiscountBenefitManageDtoList = chargeDiscountBenefitManageService.getChargeDiscountBenefitManageList(chargeDiscountBenefitManageSelectDto);
        return ResponseObject.Builder.<List<ChargeDiscountBenefitManageDto>>builder().data(chargeDiscountBenefitManageDtoList).build();
    }

    @PostMapping("/saveChargeDiscountBenefitManage")
    @ApiOperation(value = "청구할인 혜택 등록", response = Integer.class)
    public ResponseObject saveChargeDiscountBenefitManage(@Valid @RequestBody ChargeDiscountBenefitManageDto chargeDiscountBenefitManageDto) throws Exception {
        return ResponseObject.Builder.<Integer>builder().data(chargeDiscountBenefitManageService.saveChargeDiscountBenefitManage(chargeDiscountBenefitManageDto)).build();
    }
}