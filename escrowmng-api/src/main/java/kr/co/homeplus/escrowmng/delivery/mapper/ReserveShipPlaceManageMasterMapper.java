package kr.co.homeplus.escrowmng.delivery.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.delivery.model.ReserveShipPlaceManageModel;

@EscrowMasterConnection
public interface ReserveShipPlaceManageMasterMapper {
    int insertReserveShipPlace(ReserveShipPlaceManageModel reserveShipPlaceManageModel);
    int deleteReserveShipPlace(ReserveShipPlaceManageModel reserveShipPlaceManageModel);
}
