package kr.co.homeplus.escrowmng.pg.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import kr.co.homeplus.escrowmng.enums.ParentMethodCd;
import kr.co.homeplus.escrowmng.pg.mapper.PaymentMethodMasterMapper;
import kr.co.homeplus.escrowmng.pg.mapper.PaymentMethodSlaveMapper;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodTreeDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PaymentMethodService {
    private final PaymentMethodMasterMapper paymentMethodMasterMapper;
    private final PaymentMethodSlaveMapper paymentMethodSlaveMapper;

    /**
     * 결제수단 조회
     * @param paymentMethodSelectDto
     * @return
     * @throws Exception
     */
    public List<PaymentMethodDto> getPaymentMethod(PaymentMethodSelectDto paymentMethodSelectDto) throws Exception {
        return paymentMethodSlaveMapper.selectPaymentMethod(paymentMethodSelectDto);
    }

    /**
     * 결제수단 저장
     * @param paymentMethod
     * @return
     * @throws Exception
     */
    public int savePaymentMethod(PaymentMethodDto paymentMethod) throws Exception {
        return paymentMethodMasterMapper.insertPaymentMethod(paymentMethod);
    }

    /**
     * 결제수단코드 중복체크
     * @param paymentMethodSelectDto
     * @return
     * @throws Exception
     */
    public List<PaymentMethodDto> getMethodCd(PaymentMethodSelectDto paymentMethodSelectDto) throws Exception {
        return paymentMethodSlaveMapper.selectMethodCd(paymentMethodSelectDto);
    }

    /**
     * 결제수단 Tree 구조 조회
     * @param paymentMethodSelectDto
     * @return
     * @throws Exception
     */
    public List<PaymentMethodTreeDto> getPaymentMethodTree(PaymentMethodSelectDto paymentMethodSelectDto) throws Exception {
        List<PaymentMethodTreeDto> result = paymentMethodSlaveMapper.selectPaymentMethodTreeParent(paymentMethodSelectDto);
        result.addAll(paymentMethodSlaveMapper.selectPaymentMethodTreeChild(paymentMethodSelectDto));
        return result;
    }

    /**
     * 결제수단 조회 - 파라미터 사용
     * @param paymentMethodSelectDto
     * @return
     * @throws Exception
     */
    public List<PaymentMethodDto> getPaymentMethodByParam(PaymentMethodSelectDto paymentMethodSelectDto) throws Exception {
        return paymentMethodSlaveMapper.selectPaymentMethodByParam(paymentMethodSelectDto);
    }

    /**
     * 결제수단 조회 - 유형별
     * @param siteType
     * @param paymentMethodType
     * @return
     * @throws Exception
     */
    public List<PaymentMethodDto> getPaymentMethodByType(String siteType, String paymentMethodType) throws Exception {

        List<String> parentMethodCdList = new ArrayList<String>();
        for(String element : paymentMethodType.split(",")) {
            parentMethodCdList.addAll(Arrays.asList(ParentMethodCd.valueOf(element).getMethodCd().split(",")));
        }

        PaymentMethodSelectDto paymentMethodSelectDto = new PaymentMethodSelectDto();
        paymentMethodSelectDto.setSiteType(siteType);
        paymentMethodSelectDto.setParentMethodCdList(parentMethodCdList);

        return paymentMethodSlaveMapper.selectPaymentMethodByParam(paymentMethodSelectDto);
    }
}
