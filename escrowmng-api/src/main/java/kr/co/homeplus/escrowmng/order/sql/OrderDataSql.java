package kr.co.homeplus.escrowmng.order.sql;

import kr.co.homeplus.escrowmng.claim.model.entity.ClaimBundleEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimItemEntity;
import kr.co.homeplus.escrowmng.enums.MysqlFunction;
import kr.co.homeplus.escrowmng.order.constants.CustomConstants;
import kr.co.homeplus.escrowmng.order.factory.EntityFactory;
import kr.co.homeplus.escrowmng.order.model.entity.BundleEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderItemEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PurchaseOrderEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingAddrEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingItemEntity;
import kr.co.homeplus.escrowmng.utils.ObjectUtils;
import kr.co.homeplus.escrowmng.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class OrderDataSql {

    public static String selectPurchaseOrderInfo( @Param("purchaseOrderNo") long purchaseOrderNo ) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class);
        return new SQL()
            .SELECT(
                purchaseOrder.paymentNo,
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.siteType,
                purchaseOrder.userNo
            )
            .FROM(EntityFactory.getTableName(PurchaseOrderEntity.class))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            ).toString();
    }

    public static String selectOrderItemInfo( @Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderItemNo") long orderItemNo ) {
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class);
        return new SQL()
            .SELECT(
                orderItem.purchaseOrderNo,
                orderItem.bundleNo,
                orderItem.orderItemNo,
                orderItem.itemNm1,
                orderItem.itemPrice,
                orderItem.storeType,
                orderItem.mallType,
                orderItem.taxYn,
                orderItem.partnerId
            )
            .FROM(EntityFactory.getTableName(OrderItemEntity.class))
            .WHERE(
                SqlUtils.equalColumnByInput(orderItem.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.orderItemNo, orderItemNo)
            ).toString();
    }

    /**
     * 클레임에 필요한 데이터 생성
     *  -> 클레임 아이템 데이터 생성을 위한 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param orderItemNo 상품주문번호
     * @return 클레임 아이템 데이터 생성을 위한 데이터 쿼리
     */
    public static String selectClaimItemInfo( @Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderItemNo") long orderItemNo ) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);

        SQL query =  new SQL()
            .SELECT(
                orderItem.orderItemNo,
                orderItem.itemNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemNm1, "item_name"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemQty, orderItem.itemQty),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemPrice, orderItem.itemPrice),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.storeType, orderItem.storeType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.mallType, orderItem.mallType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.originStoreId, orderItem.originStoreId),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.storeId, orderItem.storeId),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.taxYn, orderItem.taxYn),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.empDiscountRate, orderItem.empDiscountRate),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.empDiscountAmt, orderItem.empDiscountAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderType, purchaseOrder.orderType),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimItem.claimItemQty, claimItem.claimItemQty),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.marketOrderNo, purchaseOrder.marketOrderNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo, orderItem.purchaseOrderNo, orderItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.orderItemNo, orderItemNo)
            )
            .GROUP_BY(orderItem.orderItemNo, orderItem.itemNo);
        log.debug("selectClaimItemInfo ::: \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 클레임에 필요한 데이터 생성
     *  -> 클레임 번들 생성을 위한 데이터.
     *
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return 클레임 번들 생성을 위한 데이터 쿼리
     */
    public static String selectCreateClaimDataInfo( @Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.paymentNo,
                purchaseOrder.siteType,
                purchaseOrder.userNo,
                bundle.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shipStatus, shippingItem.shipStatus),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.partnerId, shippingItem.partnerId),
                SqlUtils.sqlFunction(MysqlFunction.FN_PAYMENT_TYPE, purchaseOrder.paymentNo, "payment_type"),
                SqlUtils.sqlFunction(MysqlFunction.FN_PAYMENT_TYPE, purchaseOrder.paymentNo, "refund_method"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.invoiceNo, shippingItem.invoiceNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.empNo, purchaseOrder.empNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.reserveYn, purchaseOrder.reserveYn),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo,  bundle.bundleNo, "'C'"), "order_category"),
                SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo,  bundle.bundleNo, "'M'")), "TD"), "close_send_yn"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.shipMobileNo, shippingAddr.shipMobileNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.safetyUseYn, shippingAddr.safetyUseYn),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.marketOrderNo, purchaseOrder.marketOrderNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(bundle.bundleNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(bundle.bundleNo, bundleNo)
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo, purchaseOrder.paymentNo, purchaseOrder.siteType, purchaseOrder.userNo, bundle.bundleNo);
        log.debug("selectCreateClaimDataInfo ::: {}", query.toString());
        return query.toString();
    }

    public static String selectCreateClaimMstDataInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.paymentNo,
                purchaseOrder.siteType,
                purchaseOrder.userNo,
                purchaseOrder.empNo,
                purchaseOrder.gradeSeq,
                purchaseOrder.marketType,
                purchaseOrder.marketOrderNo
            )
            .FROM(EntityFactory.getTableName(purchaseOrder))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectClaimMstDtaInfo ::: {}", query.toString());
        return query.toString();
    }

}
