package kr.co.homeplus.escrowmng.delivery.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.delivery.model.StoreZipcodeModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreZipcodeSelectModel;

@EscrowSlaveConnection
public interface StoreZipcodeSlaveMapper {

    List<StoreZipcodeModel> selectStoreZipcodeList(StoreZipcodeSelectModel storeZipcodeSelectModel);
}
