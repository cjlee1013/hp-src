package kr.co.homeplus.escrowmng.order.model.entity;

public class PaymentMethodEntity {
    // 사이트유형(HOME:홈플러스,CLUB:더클럽)
    public String siteType;
    // 결제수단코드
    public String methodCd;
    // GHS결제수단코드
    public String codeCd;
    // 결제수단명
    public String methodNm;
    // 상위결제수단코드(신용카드,휴대폰결제등)
    public String parentMethodCd;
    // PG인증종류(안심:ASIAM,ISP:ISP,V3D:V3D 등)
    public String pgCertifyKind;
    // inicis코드(PG명 향후 변경가능)
    public String inicisCd;
    // tosspg코드
    public String tosspgCd;
    // 사용여부
    public String useYn;
    // 플랫폼(전체,PC,MOBILE)
    public String platform;
    // 정렬순서
    public String sortSeq;
    // 이미지url
    public String imgUrl;
    // 이미지url모바일
    public String imgUrlMobile;
    // 이미지넓이
    public String imgWidth;
    // 이미지높이
    public String imgHight;
    // 등록자id
    public String regId;
    // 등록일자
    public String regDt;
    // 수정자id
    public String chgId;
    // 수정일자
    public String chgDt;
}
