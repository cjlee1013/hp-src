package kr.co.homeplus.escrowmng.external.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@ApiModel(description = "할인(카드상품할인)사용현황 정보")
public class DiscountUseInfoSelect {

    @ApiModelProperty(value = "할인번호", position = 1)
    @NotNull(message = "할인번호")
    private Long discountNo;

    @ApiModelProperty(value = "주문시작일(YYYY-MM-DD)", position = 2)
    @Pattern(regexp = PatternConstants.DATE_DF_FORMAT_PATTERN, message = "주문시작일(YYYY-MM-DD)")
    @NotEmpty(message = "주문시작일")
    private String startDt;

    @ApiModelProperty(value = "주문종료일(YYYY-MM-DD)", position = 3)
    @Pattern(regexp = PatternConstants.DATE_DF_FORMAT_PATTERN, message = "주문종료일(YYYY-MM-DD)")
    @NotEmpty(message = "주문종료일")
    private String endDt;

    @ApiModelProperty(value = "고객번호", position = 4)
    @AllowInput(types = {PatternConstants.NUM}, message = "고객번호")
    private String userNo;
}
