package kr.co.homeplus.escrowmng.pg.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.pg.model.FreeInterestApplyDto;
import kr.co.homeplus.escrowmng.pg.model.FreeInterestCopyDto;
import kr.co.homeplus.escrowmng.pg.model.FreeInterestDto;
import kr.co.homeplus.escrowmng.pg.model.FreeInterestSelectDto;
import kr.co.homeplus.escrowmng.pg.service.FreeInterestService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/pg/paymentMethod")
@Api(tags = "결제관리 > 결제수단관리 > 무이자혜택관리")
public class FreeInterestController {

    private final FreeInterestService freeInterestService;

    /**
     * 카드무이자혜택 마스터 조회
     */
    @GetMapping(value = "/getFreeInterestMngList")
    @ApiOperation(value = "카드무이자혜택 조회")
    @ApiResponse(code = 200, message = "Success", response = FreeInterestDto.class)
    public ResponseObject getFreeInterestMngList(@Valid FreeInterestSelectDto freeInterestSelectDto) throws Exception {
        log.info("[getFreeInterestMngList]");
        List<FreeInterestDto> freeInterestDtoList = freeInterestService.getFreeInterestMngList(freeInterestSelectDto);
        return ResponseObject.Builder.<List<FreeInterestDto>>builder().data(freeInterestDtoList).build();
    }

    /**
     * 카드무이자혜택 저장
     */
    @PostMapping("/saveFreeInterest")
    @ApiOperation(value = "카드무이자혜택 저장" , response = void.class)
    @ApiResponse(code = 200, message = "Success", response = void.class)
    public ResponseObject<Integer> saveFreeInterest(@RequestBody @Valid FreeInterestDto freeInterestDtoList) throws Exception {
        return ResponseObject.Builder.<Integer>builder().data(freeInterestService.saveFreeInterest(freeInterestDtoList)).build();
    }

    /**
     * 카드무이자혜택 적용대상 조회
     */
    @GetMapping(value = "/getFreeInterestApplyList")
    @ApiOperation(value = "카드무이자혜택 적용대상 조회")
    @ApiResponse(code = 200, message = "Success", response = FreeInterestDto.class)
    public ResponseObject getFreeInterestApplyList(
        @ApiParam(value = "카드무이자관리번호", required = true) @RequestParam long freeInterestMngSeq) throws Exception {
        List<FreeInterestApplyDto> freeInterestDtoList = freeInterestService.getFreeInterestApplyList(freeInterestMngSeq);
        return ResponseObject.Builder.<List<FreeInterestApplyDto>>builder().data(freeInterestDtoList).build();
    }

    /**
     * 무이자혜택 카드정보 저장
     */
    @PostMapping("/saveFreeInterestApply")
    @ApiOperation(value = "무이자혜택카드정보저장" , response = void.class)
    @ApiResponse(code = 200, message = "Success", response = void.class)
    public ResponseObject<Integer> saveFreeInterestApply(@RequestBody @Valid FreeInterestApplyDto freeInterestApplyDto) throws Exception {
        return ResponseObject.Builder.<Integer>builder().data(freeInterestService.saveFreeInterestApply(freeInterestApplyDto)).build();
    }

    /**
     * 무이자혜택 복사 정보 저장
     */
    @PostMapping("/saveCopyFreeInterest")
    @ApiOperation(value = "무이자혜택복사정보저장" , response = void.class, hidden = true)
    @ApiResponse(code = 200, message = "Success", response = void.class)
    public ResponseObject<Integer> saveCopyFreeInterest(@RequestBody @Valid FreeInterestCopyDto freeInterestCopyDto) throws Exception {
        return ResponseObject.Builder.<Integer>builder().data(freeInterestService.saveCopyFreeInterest(freeInterestCopyDto)).build();
    }

    /**
     * 무이자혜택 카드정보 삭제
     */
    @PostMapping("/deleteFreeInterestApply")
    @ApiOperation(value = "무이자혜택카드정보삭제" , response = void.class, hidden = true)
    @ApiResponse(code = 200, message = "Success", response = void.class)
    public ResponseObject<Integer> deleteFreeInterestApply(@RequestBody FreeInterestApplyDto freeInterestApplyDto) throws Exception {
        return ResponseObject.Builder.<Integer>builder().data(freeInterestService.deleteFreeInterestApply(freeInterestApplyDto)).build();
    }

}
