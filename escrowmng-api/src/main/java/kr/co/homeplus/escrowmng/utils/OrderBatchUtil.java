package kr.co.homeplus.escrowmng.utils;

import static kr.co.homeplus.escrowmng.constants.Constants.EMPTY_STRING;
import static kr.co.homeplus.escrowmng.constants.Constants.USE_Y;

import kr.co.homeplus.escrowmng.escrow.model.OrderBatchHistoryModel;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchManageModel;
import kr.co.homeplus.escrowmng.escrow.service.OrderBatchManageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderBatchUtil {
    private final OrderBatchManageService orderBatchManageService;

    /**
     * [배치용] 배치 실행시 실행이력 생성
     * @param batchType
     * @param stopWatch
     * @return
     */
    public OrderBatchHistoryModel createOrderBatchHistory(String batchType, StopWatch stopWatch) {
        // 객체가 null 이면 insert 하지 않음.
        if (EscrowString.isEmpty(batchType)) {
            return null;
        }

        // 스톱워치가 비어있으면 실행
        if (EscrowString.isNotEmpty(stopWatch)) {
            stopWatch.start(batchType);
        }

        OrderBatchHistoryModel orderBatchHistoryModel = new OrderBatchHistoryModel();
        try {
            orderBatchHistoryModel.setBatchNo(this.getBatchNo(batchType));
            orderBatchManageService.createOrderBatchHistory(orderBatchHistoryModel);
        } catch(Exception e) {
            log.info("[External] createOrderBatchHistory is error", e);
        }

        return orderBatchHistoryModel;
    }

    /**
     * [배치용] 배치 종료시 실행이력 수정
     * @param orderBatchHistoryModel
     * @param totalCount
     * @param successCount
     * @param failCount
     * @param errorMessage
     * @param stopWatch
     */
    public void updateOrderBatchHistory(OrderBatchHistoryModel orderBatchHistoryModel, long totalCount, long successCount, long failCount, String errorMessage, StopWatch stopWatch) {
        // 객체가 null 이면 update 하지 않음.
        if (EscrowString.isEmpty(orderBatchHistoryModel)) {
            return;
        }

        try {
            // 스톱워치가 실행 중이면 종료 & 배치 수행시간 세팅
            if (EscrowString.isNotEmpty(stopWatch) && stopWatch.isRunning()) {
                stopWatch.stop();
                orderBatchHistoryModel.setBatchTime(this.getConverterDateByMillis(stopWatch.getTotalTimeMillis()));
            }

            orderBatchHistoryModel.setTotalCount(totalCount);
            orderBatchHistoryModel.setSuccessCount(successCount);
            orderBatchHistoryModel.setFailCount(failCount);
            orderBatchHistoryModel.setErrorMessage(EscrowString.isEmpty(errorMessage) ? EMPTY_STRING : errorMessage);

            orderBatchManageService.updateOrderBatchHistory(orderBatchHistoryModel);
        } catch(Exception e) {
            log.info("[External] updateOrderBatchHistory is error", e);
        }
    }

    /**
     * [배치용] 배치 실행이력용 배치번호 조회
     * @param batchType
     * @return
     */
    public String getBatchNo(String batchType) {
        OrderBatchManageModel orderBatchManageModel = new OrderBatchManageModel();
        try {
            orderBatchManageModel = orderBatchManageService.selectOrderBatchInfo(batchType);
        } catch(Exception e) {
            log.info("[External] getBatchNo is error", e);
        }

        return orderBatchManageModel.getBatchNo();
    }

    /**
     * ms -> 00:00:00 포맷으로 변환
     *
     * @param millis
     * @return
     */
    private String getConverterDateByMillis(long millis) {
        long hour = (millis / (1000 * 60 * 60)) % 24;
        long minute = (millis / (1000 * 60))  % 60;
        long second = (millis / 1000)  % 60;

        return String.format("%02d:%02d:%02d", hour, minute, second);
    }

    /**
     * [배치용] 배치 사용여부 조회
     * @param batchType
     * @return
     */
    public boolean isBatchUse(String batchType) {
        try {
            OrderBatchManageModel orderBatchManageModel = orderBatchManageService.selectOrderBatchInfo(batchType);
            if (EscrowString.isNotEmpty(orderBatchManageModel) && EscrowString.equals(USE_Y, orderBatchManageModel.getUseYn().toUpperCase())) {
                return true;
            }
        } catch(Exception e) {
            log.info("isBatchUse is error", e);
        }

        return false;
    }
}
