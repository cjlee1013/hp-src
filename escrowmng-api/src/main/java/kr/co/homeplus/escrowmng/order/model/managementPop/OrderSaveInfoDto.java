package kr.co.homeplus.escrowmng.order.model.managementPop;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문관리 > 상세 > 적립내역")
public class OrderSaveInfoDto {
    @ApiModelProperty(value = "포인트타입", position = 1)
    private String pointKind;
    @ApiModelProperty(value = "마일리지종류", position = 2)
    private String mileageKind;
    @ApiModelProperty(value = "마일리지번호", position = 3)
    private String approvalNo;
    @ApiModelProperty(value = "금액", position = 4)
    private int resultPoint;
    @ApiModelProperty(value = "적립/회수상태", position = 5)
    private String pointStatus;
}