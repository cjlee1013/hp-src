package kr.co.homeplus.escrowmng.pg.service;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.construct.TransactionManagerName;
import kr.co.homeplus.escrowmng.pg.mapper.PgDivideMasterMapper;
import kr.co.homeplus.escrowmng.pg.mapper.PgDivideSlaveMapper;
import kr.co.homeplus.escrowmng.pg.model.PgDivideManageDto;
import kr.co.homeplus.escrowmng.pg.model.PgDivideManageSelectDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class PgDivideManageService {
    private final PgDivideMasterMapper pgDivideMasterMapper;
    private final PgDivideSlaveMapper pgDivideSlaveMapper;

    /**
     * 기본배분율 조회
     * @param pgDivideManageSelectDto
     * @return
     * @throws Exception
     */
    public List<PgDivideManageDto> getPgDivideRateList(PgDivideManageSelectDto pgDivideManageSelectDto) throws Exception {
        return pgDivideSlaveMapper.selectPgDivideRateList(pgDivideManageSelectDto);
    }

    /**
     * 기본배분율 저장
     * @param pgDivideManageDto
     * @return
     * @throws Exception
     */
    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int savePgDivideRate(PgDivideManageDto pgDivideManageDto) throws Exception {
        int rtnVal = 0;

        try {
            pgDivideMasterMapper.updatePgDivideRateHistory(pgDivideManageDto.getDivideSeq());
            rtnVal = pgDivideMasterMapper.insertPgDivideRate(pgDivideManageDto);
            return rtnVal;

        } catch (Exception e) {
            log.error("error:", e);
            rtnVal = -1;
            return rtnVal;

        } finally {
            log.info("savePgDivideRate-resultCount:" + rtnVal);
        }
    }

    /**
     * 결제정책 PG배분율 조회
     * @param pgDivideManageSelectDto
     * @return
     * @throws Exception
     */
    public List<PgDivideManageDto> getPaymentPolicyPgDivideRateList(PgDivideManageSelectDto pgDivideManageSelectDto) throws Exception {
        return pgDivideSlaveMapper.selectPaymentPolicyPgDivideRateList(pgDivideManageSelectDto);
    }
}
