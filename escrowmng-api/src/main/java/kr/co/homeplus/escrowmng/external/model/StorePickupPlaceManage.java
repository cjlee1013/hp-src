package kr.co.homeplus.escrowmng.external.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "[옴니연동] 픽업점포정보 관리")
public class StorePickupPlaceManage {

    @ApiModelProperty(value = "점포유형")
    @Pattern(regexp = PatternConstants.STORE_TYPE_PATTERN, message="점포유형(HYPER,EXP,CLUB)")
    @NotEmpty(message = "점포유형(HYPER,EXP,CLUB)")
    private String storeType;

    @ApiModelProperty(value = "점포ID", position = 1)
    @NotEmpty(message = "점포ID")
    @AllowInput(types = {PatternConstants.NUM}, message = "점포ID")
    private String storeId;

    @ApiModelProperty(value = "장소번호", position = 2)
    @NotEmpty(message = "장소번호")
    @AllowInput(types = {PatternConstants.NUM}, message = "장소번호")
    private String placeNo;

    @ApiModelProperty(value = "사용여부", position = 3)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "사용여부(Y|N)")
    private String useYn;

    @ApiModelProperty(value = "장소명", position = 4)
    @NotEmpty(message = "장소명")
    private String placeNm;

    @ApiModelProperty(value = "장소설명", position = 5)
    private String placeExpln;

    @ApiModelProperty(value = "장소이미지", position = 6)
    private String imgUrl;

    @ApiModelProperty(value = "전화번호", position = 8)
    private String telNo;

    @ApiModelProperty(value = "운영시작시간", position = 9)
    @NotEmpty(message = "운영시작시간")
    @Pattern(regexp = PatternConstants.DATETIME_PATTERN_HOUR_MINUTES, message = "운영시작시간 HHmm")
    private String startTime;

    @ApiModelProperty(value = "운영종료시간", position = 10)
    @NotEmpty(message = "운영종료시간")
    @Pattern(regexp = PatternConstants.DATETIME_PATTERN_HOUR_MINUTES, message = "운영종료시간 HHmm")
    private String endTime;

    @ApiModelProperty(value = "장소주소", position = 11)
    private String placeAddr;

    @ApiModelProperty(value = "락커사용여부", position = 12)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "락커사용여부(Y|N)")
    private String lockerUseYn;

    @ApiModelProperty(value = "락커정보리스트", position = 13)
    private List<StorePickupLockerManage> lockerList;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{storeType:").append(storeType)
            .append(", storeId:").append(storeId)
            .append(", placeNo:").append(placeNo)
            .append(", useYn:").append(useYn)
            .append(", placeNm:").append(placeNm)
            .append(", imgUrl:").append(imgUrl)
            .append(", telNo:").append(telNo)
            .append(", startTime:").append(startTime)
            .append(", endTime:").append(endTime)
            .append(", placeAddr:").append(placeAddr)
            .append(", lockerUseYn:").append(lockerUseYn)
            .append(", lockerList:").append(lockerList)
            .append("}");

        return sb.toString();
    }

}
