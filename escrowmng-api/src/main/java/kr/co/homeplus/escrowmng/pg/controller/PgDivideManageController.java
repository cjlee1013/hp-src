package kr.co.homeplus.escrowmng.pg.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.escrowmng.pg.model.PgDivideManageDto;
import kr.co.homeplus.escrowmng.pg.model.PgDivideManageSelectDto;
import kr.co.homeplus.escrowmng.pg.service.PgDivideManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pg/pgDivide")
@Api(tags = "결제관리 > 결제수단 관리 > 기본배분율 관리")
public class PgDivideManageController {
    private final PgDivideManageService pgDivideManageService;

    /**
     * 기본배분율 조회
     */
    @GetMapping(value = "/getPgDivideRateList")
    @ApiOperation(value = "기본배분율 조회", response = PgDivideManageDto.class)
    public ResponseObject getPgDivideRateList(PgDivideManageSelectDto pgDivideManageSelectDto) throws Exception {
        List<PgDivideManageDto> pgDivideManageDtoList = pgDivideManageService.getPgDivideRateList(pgDivideManageSelectDto);
        return ResponseObject.Builder.<List<PgDivideManageDto>>builder().data(pgDivideManageDtoList).build();
    }

    /**
     * 기본배분율 저장
     */
    @PostMapping(value = "/savePgDivideRate")
    @ApiOperation(value = "기본배분율 저장", response = Integer.class, hidden = true)
    public ResponseObject savePgDivideRate(@RequestBody PgDivideManageDto pgDivideManageDto) throws Exception {
        return ResponseObject.Builder.<Integer>builder().data(pgDivideManageService.savePgDivideRate(pgDivideManageDto)).build();
    }

    /**
     * 결제정책 PG배분율 조회
     */
    @GetMapping(value = "/getPaymentPolicyPgDivideRateList")
    @ApiOperation(value = "결제정책 PG배분율 조회", response = PgDivideManageDto.class)
    public ResponseObject getPaymentPolicyPgDivideRateList(PgDivideManageSelectDto pgDivideManageSelectDto) throws Exception {
        List<PgDivideManageDto> pgDivideManageDtoList = pgDivideManageService.getPaymentPolicyPgDivideRateList(pgDivideManageSelectDto);
        return ResponseObject.Builder.<List<PgDivideManageDto>>builder().data(pgDivideManageDtoList).build();
    }
}
