package kr.co.homeplus.escrowmng.claim.model.refundFail;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RefundFailListGetDto {

    @ApiModelProperty(value = "환불요청일", position = 1)
    private String payRegDt;

    @ApiModelProperty(value = "클레임 타입", position = 2)
    private String claimType;

    @ApiModelProperty(value = "그룹클레임번호", position = 3)
    private String claimNo;

    @ApiModelProperty(value = "주문번호", position = 4)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "처리상태", position = 5)
    private String paymentStatus;

    @ApiModelProperty(value = "환불금액", position = 6)
    private String paymentAmt;

    @ApiModelProperty(value = "결제수단", position = 7)
    private String paymentType;

    @ApiModelProperty(value = "환불수단", position = 8)
    private String refundType;

    @ApiModelProperty(value = "주문완료일", position = 9)
    private String orderDt;

    @ApiModelProperty(value = "회원번호", position = 10)
    private String userNo;

    @ApiModelProperty(value = "구매자명", position = 11)
    private String buyerNm;

    @ApiModelProperty(value = "구매자 연락처", position = 12)
    private String buyerMobileNo;

    @ApiModelProperty(value = "클레임환불번호", position = 13)
    private String claimPaymentNo;

    @ApiModelProperty(value = "클레임번호", position = 14)
    private String claimBundleNo;

    @ApiModelProperty(value = "클레임번호", position = 14)
    private String pgOid;

    @ApiModelProperty(value = "", position = 14)
    private String pgKind;

    @ApiModelProperty(value = "환불실패사유", position = 15)
    private String refundDesc;
}
