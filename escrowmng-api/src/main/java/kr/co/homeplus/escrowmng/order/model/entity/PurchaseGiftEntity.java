package kr.co.homeplus.escrowmng.order.model.entity;

public class PurchaseGiftEntity {
    // 구매사은품번호
    public String purchaseGiftNo;
    // 구매주문번호
    public String purchaseOrderNo;
    // 구매점포정보번호
    public String purchaseStoreInfoNo;
    // 점포ID
    public String storeId;
    //
    public String ersType;
    //
    public String promoType;
    // 사은품번호
    public String giftNo;
    // 사은품메시지(영수증 노출 문구)
    public String giftMsg;
    // 사은품대상유형(13:개별아이템, 21:벤더-금액,22:카테고리-금액, 23:아이템-금액, 24:카드-금액, 25:영수증-금액, 26:벤더-카드금액, 27:카테고리-카드금액, 28:아이템-카드금액, 31:벤더-수량, 32:카테고리-수량, 33:아이템-수량)
    public String giftTargetType;
    // 사은품부여유형(01:사은품, 02:상품권정액, 03:상품권정액+사은품, 04:상품권정률, 05:상품권정률+사은품, 11:온라인, 12:오프라인, 13:기타)
    public String giftGiveType;
    // 기준값
    public String stdVal;
    // 등록일시
    public String regDt;
    // 등록ID
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정ID
    public String chgId;
}
