package kr.co.homeplus.escrowmng.delivery.mapper;

import java.util.HashMap;
import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.delivery.model.RemoteShipManageSelectModel;
import kr.co.homeplus.escrowmng.delivery.model.RemoteShipSlotModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreShiftManageModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreShiftManageSelectModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreSlotManageModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreSlotManageSelectModel;

@EscrowSlaveConnection
public interface StoreShiftSlotSlaveMapper {
    List<StoreShiftManageModel> selectStoreShift(StoreShiftManageSelectModel storeShiftManageSelectModel);
    List<StoreSlotManageModel> selectStoreSlot(StoreSlotManageSelectModel storeSlotManageSelectModel);
    List<RemoteShipSlotModel> selectRemoteShip(RemoteShipManageSelectModel remoteShipManageSelectModel);
    List<HashMap<String, Object>> selectRemoteShipSlot(RemoteShipManageSelectModel remoteShipManageSelectModel);
}
