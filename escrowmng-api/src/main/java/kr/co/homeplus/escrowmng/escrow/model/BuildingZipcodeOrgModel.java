package kr.co.homeplus.escrowmng.escrow.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 우편번호관련 건물DB Model
 */
@Getter
@Setter
public class BuildingZipcodeOrgModel {

    /* 원본 필드 정보 (building_zipcode_org) */
    /** 건물관리번호 */
    private String buildMngNo;

    /** 법정동코드 */
    private String courtDongCd;
    /** 시/도명 */
    private String sidoNm;
    /** 시/군/구명 */
    private String sigunguNm;
    /** 법정읍면동명 */
    private String courtEubmyndnNm;
    /** 법정리명 */
    private String courtRiNm;
    /** 산숫자종류(0:대지,1:산) */
    private String mountainNumKind;
    /** 지번본번(번지) */
    private int gibunBonNo;
    /** 지번부번(호) */
    private int gibunBuNo;
    /** 도로명코드(시군구코드(5)+도로명번호(7)) */
    private String roadAddrCd;
    /** 도로명 */
    private String roadAddrNm;
    /** 지하숫자종류(0:지상,1:지하,2:공중) */
    private String undgrdNumKind;
    /** 건물본번 */
    private int buildBonNo;
    /** 건물부번 */
    private int buildBuNo;
    /** 건축물대장건물명 */
    private String registerBuildNm;
    /** 상세건물명 */
    private String detailBuildNm;
    /** 읍면동일련번호 */
    private String eubmyndnSeq;
    /** 행정동코드(참고용) */
    private String adminDongCd;
    /** 행정동명(참고용) */
    private String adminDongNm;
    /** 우편번호(2015.8.1이후 기초구역번호(신우편번호)제공) */
    private String zipcode;
    /** 우편일련번호(2015.8.1이후 미제공(NULL)) */
    private String postSeq;
    /** 대량배송지명(2015.8.1이후 미제공(NULL)) */
    private String bulkShipNm;
    /** 이동사유코드(31:신규,34:변동,63:폐지,72:건물군내일부건물폐지,73:건물군내일부건물생성) */
    private String moveReasonCd;
    /** 고시일자(YYYYMMDD) */
    private String publishDt;
    /** 변동전도로명주소(NULL) */
    private String varBefRoadAddr;
    /** 시/군/구용건물명 */
    private String sigunguBuildNm;
    /** 공동주택숫자종류(0:비공동주택,1:공동주택) */
    private String cmmHouseNumKind;
    /** 기본구역번호 */
    private String baseAreaNo;
    /** 상세주소숫자종류(0:미부여,1:부여) */
    private String detailAddrNumKind;
    /** 비고1 */
    private String note1;
    /** 비고2 */
    private String note2;
    /* 필요해서 추가된 컬럼 (building_zipcode_org) */
    /** 수정일시(업데이트일시) */
    private String chgDt;
    /* ESCROW 에서 필요하여 추가된 컬럼 (building_zipcode) */
    /** 도서산간유형(ALL:전국,JEJU:제주,ISLAND:산간지역) */
    private String islandType;
    /** 조합된 도로명주소(노출용) */
    private String roadAddrFullTxt;
    /** 조합된 지번주소(노출용) */
    private String gibunAddrFullTxt;
    /** 대표지번정보 */
    private String repGibunTxt;
    /** 수정자 */
    private String chgId;


    /**
     * 파싱 데이터 DTO 에 매핑
     * @param buildingFieldArray
     */
    public void setBuildingZipcodeOrgByTxt(String[] buildingFieldArray) {
        this.courtDongCd = buildingFieldArray[0];
        this.sidoNm = buildingFieldArray[1];
        this.sigunguNm = buildingFieldArray[2];
        this.courtEubmyndnNm = buildingFieldArray[3];
        this.courtRiNm = buildingFieldArray[4];
        this.mountainNumKind = buildingFieldArray[5];
        this.gibunBonNo = Integer.parseInt(buildingFieldArray[6]);
        this.gibunBuNo = Integer.parseInt(buildingFieldArray[7]);
        this.roadAddrCd = buildingFieldArray[8];
        this.roadAddrNm = buildingFieldArray[9];
        this.undgrdNumKind = buildingFieldArray[10];
        this.buildBonNo = Integer.parseInt(buildingFieldArray[11]);
        this.buildBuNo = Integer.parseInt(buildingFieldArray[12]);
        this.registerBuildNm = buildingFieldArray[13];
        this.detailBuildNm = buildingFieldArray[14];
        this.buildMngNo = buildingFieldArray[15];
        this.eubmyndnSeq = buildingFieldArray[16];
        this.adminDongCd = buildingFieldArray[17];
        this.adminDongNm = buildingFieldArray[18];
        this.zipcode = buildingFieldArray[19];
        this.postSeq = buildingFieldArray[20];
        this.bulkShipNm = buildingFieldArray[21];
        this.moveReasonCd = buildingFieldArray[22];
        this.publishDt = buildingFieldArray[23];
        this.varBefRoadAddr = buildingFieldArray[24];
        this.sigunguBuildNm = buildingFieldArray[25];
        this.cmmHouseNumKind = buildingFieldArray[26];
        this.baseAreaNo = buildingFieldArray[27];
        this.detailAddrNumKind = buildingFieldArray[28];
    }
}
