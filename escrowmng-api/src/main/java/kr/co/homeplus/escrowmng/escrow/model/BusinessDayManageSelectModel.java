package kr.co.homeplus.escrowmng.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "시스템관리 > 영업일관리 > 영업일관리 Search DTO")
public class BusinessDayManageSelectModel {
    @ApiModelProperty(notes = "조회 년도", required = true)
    private Integer schYear;
    @ApiModelProperty(notes = "조회 월", required = true)
    private Integer schMonth;
}
