package kr.co.homeplus.escrowmng.order.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문정보 상세 - 주문정보 DTO")
public class OrderSearchDetailDto {

    @ApiModelProperty(value = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "판매자아이디")
    private String partnerId;

    @ApiModelProperty(value = "판매자")
    private String partnerNm;

    @ApiModelProperty(value = "배송번호")
    private String bundleNo;

    @ApiModelProperty(value = "상품주문번호")
    private String orderItemNo;

    @ApiModelProperty(value = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "박스번호")
    private String multiBundleNo;

    @ApiModelProperty(value = "상품명")
    private String itemNm1;

    @ApiModelProperty(value = "옵션번호")
    private String orderOptNo;

    @ApiModelProperty(value = "옵션명")
    private String optNm;

    @ApiModelProperty(value = "상품금액")
    private String itemPrice;

    @ApiModelProperty(value = "최초주문수량")
    private String itemQty;

    @ApiModelProperty(value = "클레임수량")
    private String claimQty;

    @ApiModelProperty(value = "대체여부")
    private String substitutionYn;

    @ApiModelProperty(value = "결품수량")
    private String oosQty;

    @ApiModelProperty(value = "대체상품")
    private String changeItemNm;

    @ApiModelProperty(value = "장바구니할인여부")
    private String discountCartYn;

    @ApiModelProperty(value = "총 상품금액")
    private String orderPrice;

    @ApiModelProperty(value = "상품할인금액")
    private String discountAmt;

    @ApiModelProperty(value = "카드할인금액")
    private String cardDiscountAmt;

    @ApiModelProperty(value = "행사할인금액")
    private String promoDiscountAmt;

    @ApiModelProperty(value = "중복쿠폰")
    private String addCouponAmt;

    @ApiModelProperty(value = "새벽배송 회수백비용")
    private String auroraReturnAmt;

    @ApiModelProperty(value = "배송비")
    private String shipPrice;

    @ApiModelProperty(value = "추가배송비(도서산간)")
    private String islandShipPrice;

    @ApiModelProperty(value = "클레임배송비")
    private String claimShipPrice;

    @ApiModelProperty(value = "클레임추가배송비")
    private String claimAddShipPrice;

    @ApiModelProperty(value = "총배송비할인")
    private String shipDiscountPrice;

    @ApiModelProperty(value = "클레임비용")
    private String claimPrice;

    @ApiModelProperty(value = "장바구니할인금액")
    private String totCartDiscountAmt;

    @ApiModelProperty(value = "상품별 장바구니할인금액")
    private String cartItemDiscountAmt;

    @ApiModelProperty(value = "임직원할인")
    private String totEmpDiscountAmt;

    @ApiModelProperty(value = "결제금액")
    private String totAmt;

    @ApiModelProperty(value = "택배비지급")
    private String prepaymentYn;

    @ApiModelProperty(value = "결제상태", hidden = true)
    private String paymentStatus;

    @ApiModelProperty(value = "배송상태", hidden = true)
    private String shipStatus;

    @ApiModelProperty(value = "클레임가능여부 ", hidden = true)
    private String claimYn;

    @ApiModelProperty(value = "배송타입", hidden = true)
    private String shipType;

    @ApiModelProperty(value = "합배송클레임완료여부", hidden = true)
    private String cmbnClaimYn;

    @ApiModelProperty(value = "사이트타입", hidden = true)
    private String siteType;

    @ApiModelProperty(value = "최초상품병장바구니할인금액")
    private String basicCartItemDiscountAmt;

}
