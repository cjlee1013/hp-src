package kr.co.homeplus.escrowmng.message.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.message.model.OrderSendListInfo;
import kr.co.homeplus.escrowmng.message.model.OrderSendListRequest;

@EscrowSlaveConnection
public interface OrderSendListSlaveMapper {

    List<OrderSendListInfo> selectOrderSendList(final OrderSendListRequest request);
}
