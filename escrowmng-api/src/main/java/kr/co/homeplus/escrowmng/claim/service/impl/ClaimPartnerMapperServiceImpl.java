package kr.co.homeplus.escrowmng.claim.service.impl;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.mapper.ClaimPartnerReadMapper;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerBoardCountGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerDetailGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerDetailItem;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerExchangeListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListSetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerDetailListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerReturnListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.PartnerMainBoardClaimRequestCountGetDto;
import kr.co.homeplus.escrowmng.claim.service.ClaimPartnerMapperService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClaimPartnerMapperServiceImpl implements ClaimPartnerMapperService {

    private final ClaimPartnerReadMapper partnerMapper;


    @Override
    public ClaimPartnerBoardCountGetDto getPOClaimBoardCount(String partnerId, String claimType) {
        return partnerMapper.selectPOClaimBoardCount(partnerId, claimType);
    }

    @Override
    public PartnerMainBoardClaimRequestCountGetDto getPOMainClaimRequestCount(String partnerId) {
        return partnerMapper.selectPOMainClaimRequestCount(partnerId);
    }

    @Override
    public List<ClaimPartnerListGetDto> getPOClaimList(ClaimPartnerListSetDto claimCancelListSetDto) {
        return partnerMapper.getPOClaimList(claimCancelListSetDto);
    }

    @Override
    public Integer getPOClaimListCnt(ClaimPartnerListSetDto claimCancelListSetDto) {
        return partnerMapper.getPOClaimListCnt(claimCancelListSetDto);
    }

    @Override
    public List<ClaimPartnerDetailListGetDto> getPOClaimDetailList(long purchaseOrderNo, long bundleNo) {
        return partnerMapper.selectPOClaimDetailList(purchaseOrderNo, bundleNo);
    }

    @Override
    public ClaimPartnerDetailGetDto getPOClaimDetail(long claimBundleNo) {
        return partnerMapper.selectPOClaimDetail(claimBundleNo);
    }

    @Override
    public List<ClaimPartnerDetailItem> getPOClaimDetailItemList(long claimBundleNo) {
        return partnerMapper.selectPOClaimDetailItemList(claimBundleNo);
    }

}
