package kr.co.homeplus.escrowmng.claim.model.register;

import io.swagger.annotations.ApiModel;
import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimTicketMultiCancelSetDto.TicketAllCancelDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ApiModel(description = "일괄취소(발주관리,점포취소)")
public class ClaimOrderShipMultiCancelSetDto {

    private String claimReasonType;
    private String claimReasonDetail;
    private List<OrderShipMultiCancelSetDto> itemList;
    private String regId;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class OrderShipMultiCancelSetDto {
        private long purchaseOrderNo;
        private long bundleNo;
        private long orderItemNo;
    }
}
