package kr.co.homeplus.escrowmng.claim.service.refundFail;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListSetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteSetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteSetDto.claimPaymentNoDto;
import kr.co.homeplus.escrowmng.claim.service.ClaimProcessService;
import kr.co.homeplus.escrowmng.common.model.processHistory.ProcessHistoryRegDto;
import kr.co.homeplus.escrowmng.core.exception.LogicException;
import kr.co.homeplus.escrowmng.enums.OrderCode;
import kr.co.homeplus.escrowmng.enums.RefundFailCode;
import kr.co.homeplus.escrowmng.order.factory.OrderResponseFactory;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class RefundFailService {

    private final RefundFailMapperService refundFailService;

    private final ClaimProcessService claimProcessService;


    /**
     * 환불실패대상 조회
     *
     * @param refundFailListSetDto
     * @return 환불실패대상 리스트 조회
     */
    public ResponseObject<List<RefundFailListGetDto>> getRefundFailList(RefundFailListSetDto refundFailListSetDto) {
        List<RefundFailListGetDto> refundFailList = refundFailService.getRefundFailList(refundFailListSetDto);

        if (ObjectUtils.isNotEmpty(refundFailList)) {
            for (RefundFailListGetDto next : refundFailList) {
                next.setBuyerNm(PrivacyMaskingUtils.maskingUserName(next.getBuyerNm()));
                next.setBuyerMobileNo(PrivacyMaskingUtils.maskingPhone(next.getBuyerMobileNo()));
                next.setUserNo(PrivacyMaskingUtils.maskingUserId(next.getUserNo()));
            }
        }
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, refundFailList);
    }

    /**
     * 환불완료 요청
     *
     * @param requestRefundCompleteSetDto
     * @return 환불완료 건 수
     */
    public ResponseObject<RequestRefundCompleteGetDto> reqRefundComplete(RequestRefundCompleteSetDto requestRefundCompleteSetDto) throws LogicException {

        /* 1.환불완료 요청 */
        try{
            refundFailService.setReqRefundComplete(requestRefundCompleteSetDto);
        }catch (Exception e){
            throw new LogicException(RefundFailCode.REFUND_FAIL_ERR01);
        }

        /* 2.ProcessHistory 등록 */
        for(claimPaymentNoDto claimPaymentDto : requestRefundCompleteSetDto.getClaimPaymentNoList()){
            try{
            claimProcessService.setClaimProcessHistory(
                ProcessHistoryRegDto.builder()
                    .claimNo(String.valueOf(claimPaymentDto.getClaimNo()))
                    .historyReason("환불완료")
                    .historyDetailDesc("환불실패대상 환불완료 처리")
                    .regId(requestRefundCompleteSetDto.getChgId())
                    .regChannel("ADMIN")
                    .build()
            );
            }catch (Exception e){
                log.debug("ProcessHistory 등록 실패 : {}", e);
            }
        }

        /* 2.환불완료 등록/성공/실패건 조회 후 return */
        return OrderResponseFactory.getResponseObject(RefundFailCode.REFUND_COMPLETE_SUCCESS, refundFailService.getRefundCompleteResultCount(requestRefundCompleteSetDto));
    }
}
