package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "결제관리 > 결제수단 관리 > PG 수수료 관리 search DTO")
@Getter
@Setter
@EqualsAndHashCode
public class PgCommissionManageSelectDto {
    private String schFromDt;
    private String schEndDt;
    private String schPgKind;
    private String schParentMethodCd;
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "사용여부(Y|N)")
    private String schUseYn;
    @AllowInput(types = {PatternConstants.NUM, PatternConstants.ENG, PatternConstants.KOR}, message = "제목검색어")
    private String schKeyword;
    private String schPgCommissionMngSeq;
}
