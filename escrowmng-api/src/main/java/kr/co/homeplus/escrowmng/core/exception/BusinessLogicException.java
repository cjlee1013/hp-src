package kr.co.homeplus.escrowmng.core.exception;

import kr.co.homeplus.escrowmng.enums.BusinessExceptionCode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.poi.ss.formula.functions.T;

/**
 * 비즈니스 로직 Exception
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class BusinessLogicException extends RuntimeException {

	private String errorCode;
	private String description  = "";
	private String errorMsg     = "";
	private String errorMsg2    = "";
	private T data;

	public BusinessLogicException(BusinessExceptionCode exceptionCode) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDescription();
	}

	public BusinessLogicException(BusinessExceptionCode exceptionCode, String errorMsg) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDescription();
		this.errorMsg       = errorMsg;
	}

	public BusinessLogicException(BusinessExceptionCode exceptionCode, T data) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDescription();
		this.data       = data;
	}

	public BusinessLogicException(BusinessExceptionCode exceptionCode, String errorMsg, String errorMsg2) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDescription();
		this.errorMsg       = errorMsg;
		this.errorMsg2		= errorMsg2;
	}

	public <T extends Number> BusinessLogicException(BusinessExceptionCode exceptionCode, T errorMsg) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDescription();
		this.errorMsg       = String.valueOf(errorMsg);
	}

	public <T extends Number> BusinessLogicException(BusinessExceptionCode exceptionCode, String errorMsg, T errorMsg2) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDescription();
		this.errorMsg       = errorMsg;
		this.errorMsg2		= String.valueOf(errorMsg2);
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}
}
