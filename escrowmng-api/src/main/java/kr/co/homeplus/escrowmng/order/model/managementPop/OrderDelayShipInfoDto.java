package kr.co.homeplus.escrowmng.order.model.managementPop;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "발송지연내역 정보 팝업")
public class OrderDelayShipInfoDto {
    @ApiModelProperty(value = "발송지연등록일", position = 1)
    private String delayShipRegDt;
    @ApiModelProperty(value = "발송지연사유", position = 1)
    private String delayShipCd;
    @ApiModelProperty(value = "발송지연일", position = 1)
    private String delayShipDt;
    @ApiModelProperty(value = "발송지연상세사유", position = 1)
    private String delayShipMsg;
}
