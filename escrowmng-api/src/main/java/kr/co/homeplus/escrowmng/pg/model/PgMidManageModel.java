package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단관리 > PG상점ID관리 DTO")
public class PgMidManageModel {
    @ApiModelProperty(notes = "PG상점ID", required = true, position = 1)
    @NotEmpty(message = "상점ID")
    private String pgMid;

    @ApiModelProperty(notes = "PG종류(TOSSPG,INICIS)", required = true, position = 2)
    @NotEmpty(message = "PG")
    private String pgKind;

    @ApiModelProperty(notes = "사이트키(PG사에서 발급한 key)", position = 3)
    private String siteKey;

    @ApiModelProperty(notes = "사용목적(NORMAL:일반,GIFT:상품권,GOLD:순금,CULTURE:문화비소득공제,PROMOTION:프로모션)", position = 4)
    private String usePurpose;

    @ApiModelProperty(notes = "무이자여부(무이자:Y,이자:N)", position = 5)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message="무이자여부(Y/N)")
    private String freeInterestYn;

    @ApiModelProperty(notes = "노출순서", required = true, position = 6)
    @NotNull(message = "노출순서")
    private Integer dispSeq;

    @ApiModelProperty(notes = "간편결제 적용대상(SAMSUNGPAY:삼성페이,KAKAOPAY:카카오카드,KAKAOMONEY:카카오머니,NAVERPAY:네이버페이,TOSS:토스,PAYCO:페이코)", position = 7)
    private String easyPayTarget;

    @ApiModelProperty(notes = "사용여부(사용:Y,미사용:N)", required = true, position = 8)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message="사용여부(Y/N)")
    private String useYn;

    @ApiModelProperty(notes = "비고", position = 9)
    private String note;

    @ApiModelProperty(notes = "등록자ID", required = true, position = 10)
    private String regId;
    @ApiModelProperty(notes = "등록일자", required = true, hidden = true, position = 11)
    private String regDt;
    @ApiModelProperty(notes = "수정자ID", required = true, position = 12)
    private String chgId;
    @ApiModelProperty(notes = "수정일자", required = true, hidden = true, position = 13)
    private String chgDt;

}
