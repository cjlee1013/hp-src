package kr.co.homeplus.escrowmng.delivery.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.delivery.model.ReserveShipPlaceManageModel;
import kr.co.homeplus.escrowmng.delivery.model.ReserveShipPlaceManageSelectModel;

@EscrowSlaveConnection
public interface ReserveShipPlaceManageSlaveMapper {
    List<ReserveShipPlaceManageModel> selectReserveShipPlaceMngList(ReserveShipPlaceManageSelectModel reserveShipPlaceManageSelectModel);
}
