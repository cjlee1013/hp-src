package kr.co.homeplus.escrowmng.claim.sql.partner;

import java.util.Arrays;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimBundleEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimExchShippingEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimItemEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimMstEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimOptEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimPaymentEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimPickShippingEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimReqEntity;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListSetDto;
import kr.co.homeplus.escrowmng.enums.MysqlFunction;
import kr.co.homeplus.escrowmng.order.constants.CustomConstants;
import kr.co.homeplus.escrowmng.order.constants.SqlPatternConstants;
import kr.co.homeplus.escrowmng.order.factory.EntityFactory;
import kr.co.homeplus.escrowmng.order.model.entity.BundleEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderItemEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderOptEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PurchaseOrderEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingAddrEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingItemEntity;
import kr.co.homeplus.escrowmng.utils.ObjectUtils;
import kr.co.homeplus.escrowmng.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ClaimPartnerSql {


    // 공통코드 조회.
    private static final String escrowMngCode = "(SELECT IFNULL(imc.mc_nm, '''') AS mc_cd FROM dms.itm_mng_code imc where imc.gmc_cd = ''{0}'' AND imc.mc_cd = {1})";
    private static final String escrowDfCode = "(SELECT IFNULL(imc.ref_2, '''') AS ref_2 FROM dms.itm_mng_code imc where imc.gmc_cd = ''{0}'' AND imc.mc_cd = {1})";


    /**
     * PO 클레임 관리 - 클레임 관리 - 취소관리 -취소 클레임 리스트
     * 클레임 건수 조회
     *
     * @param claimCancelListSetDto 취소 클레임 조회 dto
     * @return String 취소 클레임 리스트 Query
     * @throws Exception 오류처리.
     */
    public static String getPOClaimList(ClaimPartnerListSetDto claimCancelListSetDto) throws Exception {

        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class, Boolean.TRUE);
        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);

        // 조회기간 타입에 따른 조회기준설정.
        String claimDateType;
        switch (claimCancelListSetDto.getClaimDateType()){
            case "REQUEST":
                claimDateType = claimReq.requestDt;
                break;
            case "COMPLETE":
                claimDateType = claimReq.completeDt;
                break;
            case "ORDER":
                claimDateType = purchaseOrder.orderDt;
                break;
            default:
                claimDateType = null;
        }

        //검색어에 따른 조회기준설정
        String claimSearchType;
        switch (claimCancelListSetDto.getClaimSearchType()){
            case  "CLAIMBUNDLENO" :
                claimSearchType = claimBundle.claimBundleNo;
                break;
            case "MOBILENO" :
                claimSearchType = purchaseOrder.buyerMobileNo;
                break;
            case  "PURCHASEORDERNO" :
                claimSearchType = claimMst.purchaseOrderNo;
                break;
            case "ORDERITEMNO" :
                claimSearchType = claimItem.orderItemNo;
                break;
            case "ITEMNO" :
                claimSearchType = claimItem.itemNo;
                break;
            case "CLAIMNO" :
                claimSearchType = claimMst.claimNo;
                break;
            case "SHIPMOBILENO" :
                claimSearchType = claimPickShipping.pickMobileNo;
                break;
            default:
                claimSearchType = null;
        }


        SQL queryOfOrder = new SQL()
            .SELECT(
                claimMst.claimNo,
                claimReq.claimReqNo,
                claimBundle.claimBundleNo,
                claimMst.purchaseOrderNo,
                claimBundle.bundleNo,
                claimItem.orderItemNo,
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimBundle.claimStatus), nonAliasing(claimBundle.claimStatus), claimBundle.claimStatus),
                claimBundle.claimStatus.concat(" as claimStatusCode"),
                claimBundle.claimType,
                claimReq.requestDt,
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimReq.claimReasonType), nonAliasing(claimReq.claimReasonType), claimReq.claimReasonType),
	            "case cr.request_id "
	             + "when 'MYPAGE' then cr.request_id "
	             + "when 'PARTNER' then cr.request_id "
	             + "when 'STORE' then cr.request_id "
	             + "when 'SYSTEM' then cr.request_id "
	            + "else 'ADMIN'"
	            + "end request_id",
                claimPickShipping.pickStatus.concat(" as pick_status_code"),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimPickShipping.pickStatus), "pickup_status", claimPickShipping.pickStatus),
                claimPickShipping.pickP3Dt,
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(claimMst.totAddShipPrice, claimMst.totAddIslandShipPrice),
                    "return_ship_amt"
                ),
                "case cb.claim_status "
                    + "when 'C1' then '-' "
                    + "when 'C2' then cr.approve_dt "
                    + "when 'C3' then cr.complete_dt "
                    + "when 'C4' then cr.withdraw_dt "
                    + "when 'C8' then cr.pending_dt "
                    + "when 'C9' then cr.reject_dt "
                    + "end process_dt",
                "case "
                    + "when cr.approve_dt is null then '-' "
                    + "else "
                    + "case "
                    + "cr.approve_id "
                    + "when 'MYPAGE' then cr.approve_id "
                    + "when 'PARTNER' then cr.approve_id "
                    + "when 'STORE' then cr.approve_id "
                    + "when 'SYSTEM' then cr.approve_id "
                    + "when 'BATCH' then cr.approve_id "
                    + "when 'WMS' then cr.request_id "
                    + "else 'ADMIN' end "
                    + "end as approve_id",
                claimReq.approveDt,
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "claim_ship_fee_enclose",
                    SqlUtils.equalColumnByInput(
                        claimBundle.claimShipFeeEnclose, "Y"
                    ),
                    "'동봉'",
                    "'차감'"
                ),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(shippingItem.shipStatus), nonAliasing(shippingItem.shipStatus), shippingItem.shipStatus),
                "case cm.who_reason "
                 + "when 'B' then '구매자'"
                 + "when 'S' then '판매자'"
                 + "when 'H' then '홈플러스'"
                + "end who_reason",
                claimItem.itemNo,
                claimItem.itemName,
                SqlUtils.sqlFunction(MysqlFunction.FN_ORDER_OPTION_NM_MANAGE, claimOpt.orderOptNo, "opt_item_nm"),
                claimItem.claimItemQty,
                claimItem.itemPrice,
                SqlUtils.customOperator(
                    CustomConstants.MULTIPLY,
                    ObjectUtils.toArray(claimItem.itemPrice, claimItem.claimItemQty),
                    "tot_item_price"
                ),
                purchaseOrder.buyerNm,
                purchaseOrder.buyerMobileNo,
                shippingAddr.zipcode,
                shippingAddr.receiverNm,
                shippingAddr.shipMobileNo,
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "addr",
                    SqlUtils.equalColumnByInput(
                        SqlPatternConstants.getConditionByPattern("IFNULL", shippingAddr.roadBaseAddr, "''"), ""
                    ),
                    shippingAddr.baseAddr,
                    shippingAddr.roadBaseAddr
                ),
                shippingAddr.dlvShipMsg,
                orderItem.sellerItemCd,
                orderOpt.sellerOptCd,
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "ship_price_type",
                    SqlUtils.equalColumnByInput(
                        bundle.prepaymentYn, "Y"
                    ),
                    "'선불'",
                    "'후불'"
                ),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(shippingItem.shipMethod), nonAliasing(shippingItem.shipMethod), shippingItem.shipMethod),
                SqlPatternConstants.getConvertPattern(escrowMngCode, "dlv_cd_nm", nonAliasing(shippingItem.dlvCd), shippingItem.dlvCd),
                shippingItem.dlvCd,

                shippingItem.invoiceNo,
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimPickShipping.pickShipType), "pick_delivery_type", claimPickShipping.pickShipType),
                SqlPatternConstants.getConvertPattern(escrowMngCode, "pick_dlv_cd_nm", "dlv_cd", claimPickShipping.pickDlvCd),
                claimPickShipping.pickDlvCd,

                claimPickShipping.pickInvoiceNo,
                claimReq.pendingDt,
                claimPickShipping.pickZipcode,
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "pick_addr",
                    SqlUtils.equalColumnByInput(
                        SqlPatternConstants.getConditionByPattern("IFNULL", claimPickShipping.pickRoadBaseAddr, "''"), ""
                    ),
                    claimPickShipping.pickBaseAddr,
                    claimPickShipping.pickRoadBaseAddr
                ),
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "pick_addr_detail",
                    SqlUtils.equalColumnByInput(
                        SqlPatternConstants.getConditionByPattern("IFNULL", claimPickShipping.pickRoadDetailAddr, "''"), ""
                    ),
                    claimPickShipping.pickRoadDetailAddr,
                    claimPickShipping.pickRoadDetailAddr
                ),
                claimExchShipping.exchStatus.concat(" as exch_status_code"),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimExchShipping.exchStatus), "exch_shipping_status", claimExchShipping.exchStatus),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimExchShipping.exchShipType), "delivery_type", claimExchShipping.exchShipType),
                SqlPatternConstants.getConvertPattern(escrowMngCode, "exch_dlv_cd_nm", "dlv_cd", claimExchShipping.exchDlvCd),
                claimExchShipping.exchDlvCd,
                claimExchShipping.exchInvoiceNo,
                claimExchShipping.exchD0Dt,
                claimExchShipping.exchZipcode,
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "exch_addr",
                    SqlUtils.equalColumnByInput(
                        SqlPatternConstants.getConditionByPattern("IFNULL", claimExchShipping.exchRoadBaseAddr, "''"), ""
                    ),
                    claimExchShipping.exchBaseAddr,
                    claimExchShipping.exchRoadBaseAddr
                ),
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "exch_addr_detail",
                    SqlUtils.equalColumnByInput(
                        SqlPatternConstants.getConditionByPattern("IFNULL", claimExchShipping.exchRoadDetailAddr, "''"), ""
                    ),
                    claimExchShipping.exchRoadDetailAddr,
                    claimExchShipping.exchRoadDetailAddr
                ),
                claimReq.completeDt,
                claimReq.rejectDt
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.claimNo, claimItem.claimBundleNo}, claimBundle )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimMst),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.claimNo}, claimMst )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimReq),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.claimNo,claimItem.claimBundleNo}, claimReq )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(bundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.bundleNo}, bundle )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.purchaseOrderNo}, purchaseOrder )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingAddr),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.bundleNo}, shippingAddr )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ purchaseOrder.purchaseOrderNo, claimItem.itemNo }, shippingItem )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.orderItemNo, purchaseOrder.purchaseOrderNo}, orderItem )),
                    null
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimOpt),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.claimNo, claimItem.claimBundleNo, claimItem.claimItemNo }, claimOpt )),
                    null
                )
            )

            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderOpt),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.orderItemNo, claimOpt.orderOptNo }, orderOpt )),
                    null
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimPickShipping),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimPickShipping )),
                    null
                )
            ).LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimExchShipping),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimExchShipping )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.partnerId, claimCancelListSetDto.getPartnerId()),

                SqlUtils.equalColumnByInput(claimBundle.claimType, claimCancelListSetDto.getClaimType()),
                SqlUtils.equalColumnByInput(claimItem.mallType, "DS"),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_CALL','ORD_TICKET'"))
            );

        //메뉴 상단 보드 상태 건수 클릭 시 요청 된 클레임 상태
        if(claimCancelListSetDto.getSchStatus() != null && !claimCancelListSetDto.getSchStatus().equalsIgnoreCase("")){
            //request : 요청, delay : 지연, complete : 완료, pickComplete : 수거완료, 보류 : pending
            switch (claimCancelListSetDto.getSchStatus()){
                case  "request" :
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C1"),
                        SqlUtils.betweenNowMonth(claimBundle.regDt)
                    );
                    break;
                case  "delay" :
                    if(claimCancelListSetDto.getClaimType().equalsIgnoreCase("C")){
                        queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C2"),
                            "cb.claim_no in (select claim_no from claim_payment cp where cp.claim_no = cb.claim_no and cp.payment_status in ('P0', 'P1', 'P2') group by claim_no)",
                             SqlUtils.betweenNowMonth(claimBundle.regDt),
                            "cb.reg_dt >= UF_S_BUSINESS_DAY(DATE_FORMAT(now(), '%Y-%m-%d'),-3,'y','y','','')"
                        );
                    }else if(claimCancelListSetDto.getClaimType().equalsIgnoreCase("R")){
                        queryOfOrder.WHERE(
                            SqlUtils.betweenNowMonth(claimBundle.regDt),
                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C1"),
                            SqlUtils.equalColumnByInput(claimPickShipping.pickStatus, "P3"),
                            "cps.pick_p3_dt >= UF_S_BUSINESS_DAY(DATE_FORMAT(now(), '%Y-%m-%d'),-3,'y','y','','')"
                        );
                    }
                    break;

                case  "complete" :
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                        SqlUtils.betweenNowMonth(claimBundle.regDt)
                    );
                    break;
                case "pickComplete" :
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C1"),
                        SqlUtils.equalColumnByInput(claimPickShipping.pickStatus, "P3"),
                        SqlUtils.betweenNowMonth(claimBundle.regDt)
                    );
                    break;
                case "pending":
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C8"),
                        SqlUtils.betweenNowMonth(claimBundle.regDt)
                    );
                    break;
            }
        }else{
            //검색어 조건
            if(!claimCancelListSetDto.getClaimSearchType().equalsIgnoreCase("ALL") && !claimCancelListSetDto.getSchClaimSearch().equalsIgnoreCase("")){
                //주문번호, 배송번호, 클레임번호로 조회 시 날짜 조건 없이 전체 조회
                switch (claimCancelListSetDto.getClaimSearchType()){
                    case  "PURCHASEORDERNO" : case  "BUNDLENO" : case  "CLAIMBUNDLENO" :
                        queryOfOrder.WHERE(
                            SqlUtils.equalColumnByInput(claimSearchType, claimCancelListSetDto.getSchClaimSearch())
                        );
                        break;
                    default:
                        queryOfOrder.WHERE(
                            SqlUtils.betweenDay(claimDateType, claimCancelListSetDto.getSchStartDt(), claimCancelListSetDto.getSchEndDt()),
                            SqlUtils.equalColumnByInput(claimSearchType, claimCancelListSetDto.getSchClaimSearch())
                        );
                        break;
                }
            }else{
                queryOfOrder.WHERE(
                    SqlUtils.betweenDay(claimDateType, claimCancelListSetDto.getSchStartDt(), claimCancelListSetDto.getSchEndDt())
                );
            }


            //클레임 상태 (C1: 신청/ C2: 승인/ C3: 완료/ C4: 철회/ C8: 보류/ C9: 거부)
            if(claimCancelListSetDto.getSchClaimStatus() != null && !claimCancelListSetDto.getSchClaimStatus().equalsIgnoreCase("ALL")){
                queryOfOrder.WHERE(
                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, claimCancelListSetDto.getSchClaimStatus())
                );
            }

            //수거 상태 (NN: 요청대기, N0: 수거예약, P0:자동접수요청, P1: 요청완료, P2: 수거중, P3: 수거완료, P8 :수거실패 (반품자동접수실패))
            if(claimCancelListSetDto.getSchPickupStatus() != null && !claimCancelListSetDto.getSchPickupStatus().equalsIgnoreCase("ALL")){
                switch (claimCancelListSetDto.getSchPickupStatus()){
                    case "P0" :
                        queryOfOrder.WHERE(
                            "cps.pick_status IN('P0', 'P1')"
                        );
                        break;
                    default:
                        queryOfOrder.WHERE(
                            SqlUtils.equalColumnByInput(claimPickShipping.pickStatus, claimCancelListSetDto.getSchPickupStatus())
                        );
                        break;
                }
            }

            //배송 상태 (NN: 요청대기, N0: 수거예약, P0:자동접수요청, P1: 요청완료, P2: 수거중, P3: 수거완료, P8 :수거실패 (반품자동접수실패))
            if(claimCancelListSetDto.getSchExchStatus() != null && !claimCancelListSetDto.getSchExchStatus().equalsIgnoreCase("ALL")){
                queryOfOrder.WHERE(
                    SqlUtils.equalColumnByInput(claimExchShipping.exchStatus, claimCancelListSetDto.getSchExchStatus())
                );
            }

            //승인채널 (MYPAGE:구매자 / STORE: 스토어/ ADMIN: 어드민웹 / PARTNER: 파트너웹 / SYSTEM: 시스템배치)
            if(claimCancelListSetDto.getSchClaimChannel() != null && !claimCancelListSetDto.getSchClaimChannel().equalsIgnoreCase("ALL")){
                switch (claimCancelListSetDto.getSchClaimChannel()) {
                    case "ADMIN":
                        queryOfOrder.WHERE(
                            "cr.approve_id NOT IN('MYPAGE', 'STORE', 'PARTNER', 'SYSTEM')"
                        );
                        break;
                    default:
                        queryOfOrder.WHERE(
                            SqlUtils.equalColumnByInput(claimReq.approveId, claimCancelListSetDto.getSchClaimChannel())
                        );
                        break;
                }
            }
        }

        StringBuilder query = new StringBuilder();

        query.append(queryOfOrder.toString());
        query.append(" order by claim_bundle_no desc");

        log.debug("selectPOCancelClaimList Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
     * PO 클레임 관리 - 클레임 관리 - 취소관리 -취소 클레임 리스트
     * 클레임 건수 조회
     *
     * @param claimCancelListSetDto 취소 클레임 조회 dto
     * @return String 취소 클레임 리스트 Query
     * @throws Exception 오류처리.
     */
    public static String getPOClaimListCnt(ClaimPartnerListSetDto claimCancelListSetDto) throws Exception {

        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class, Boolean.TRUE);
        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);

        // 조회기간 타입에 따른 조회기준설정.
        String claimDateType;
        switch (claimCancelListSetDto.getClaimDateType()){
            case "REQUEST":
                claimDateType = claimReq.requestDt;
                break;
            case "COMPLETE":
                claimDateType = claimReq.completeDt;
                break;
            case "ORDER":
                claimDateType = purchaseOrder.orderDt;
                break;
            default:
                claimDateType = null;
        }

        //검색어에 따른 조회기준설정
        String claimSearchType;
        switch (claimCancelListSetDto.getClaimSearchType()){
            case  "CLAIMBUNDLENO" :
                claimSearchType = claimBundle.claimBundleNo;
                break;
            case "MOBILENO" :
                claimSearchType = purchaseOrder.buyerMobileNo;
                break;
            case  "PURCHASEORDERNO" :
                claimSearchType = claimMst.purchaseOrderNo;
                break;
            case "ORDERITEMNO" :
                claimSearchType = claimItem.orderItemNo;
                break;
            case "ITEMNO" :
                claimSearchType = claimItem.itemNo;
                break;
            case "CLAIMNO" :
                claimSearchType = claimMst.claimNo;
                break;
            case "SHIPMOBILENO" :
                claimSearchType = claimPickShipping.pickMobileNo;
                break;
            default:
                claimSearchType = null;
        }


        SQL queryOfOrder = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, "*")

            )
            .FROM(EntityFactory.getTableNameWithAlias(claimItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.claimNo, claimItem.claimBundleNo}, claimBundle )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimMst),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.claimNo}, claimMst )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimReq),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.claimNo,claimItem.claimBundleNo}, claimReq )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(bundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.bundleNo}, bundle )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.purchaseOrderNo}, purchaseOrder )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingAddr),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.bundleNo}, shippingAddr )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ purchaseOrder.purchaseOrderNo, claimItem.itemNo }, shippingItem )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.orderItemNo, purchaseOrder.purchaseOrderNo}, orderItem )),
                    null
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimOpt),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.claimNo, claimItem.claimBundleNo, claimItem.claimItemNo }, claimOpt )),
                    null
                )
            )

            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderOpt),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.orderItemNo, claimOpt.orderOptNo }, orderOpt )),
                    null
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimPickShipping),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimPickShipping )),
                    null
                )
            ).LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimExchShipping),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimExchShipping )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.partnerId, claimCancelListSetDto.getPartnerId()),

                SqlUtils.equalColumnByInput(claimBundle.claimType, claimCancelListSetDto.getClaimType()),
                SqlUtils.equalColumnByInput(claimItem.mallType, "DS"),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_CALL','ORD_TICKET'"))
            );

        //메뉴 상단 보드 상태 건수 클릭 시 요청 된 클레임 상태
        if(claimCancelListSetDto.getSchStatus() != null && !claimCancelListSetDto.getSchStatus().equalsIgnoreCase("")){
            //request : 요청, delay : 지연, complete : 완료, pickComplete : 수거완료, 보류 : pending
            switch (claimCancelListSetDto.getSchStatus()){
                case  "request" :
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C1"),
                        SqlUtils.betweenNowMonth(claimBundle.regDt)
                    );
                    break;
                case  "delay" :
                    if(claimCancelListSetDto.getClaimType().equalsIgnoreCase("C")){
                        queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C2"),
                            "cb.claim_no in (select claim_no from claim_payment cp where cp.claim_no = cb.claim_no and cp.payment_status in ('P0', 'P1', 'P2') group by claim_no)",
                             SqlUtils.betweenNowMonth(claimBundle.regDt),
                            "cb.reg_dt >= UF_S_BUSINESS_DAY(DATE_FORMAT(now(), '%Y-%m-%d'),-3,'y','y','','')"
                        );
                    }else if(claimCancelListSetDto.getClaimType().equalsIgnoreCase("R")){
                        queryOfOrder.WHERE(
                            SqlUtils.betweenNowMonth(claimBundle.regDt),
                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C1"),
                            SqlUtils.equalColumnByInput(claimPickShipping.pickStatus, "P3"),
                            "cps.pick_p3_dt >= UF_S_BUSINESS_DAY(DATE_FORMAT(now(), '%Y-%m-%d'),-3,'y','y','','')"
                        );
                    }
                    break;

                case  "complete" :
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                        SqlUtils.betweenNowMonth(claimBundle.regDt)
                    );
                    break;
                case "pickComplete" :
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C1"),
                        SqlUtils.equalColumnByInput(claimPickShipping.pickStatus, "P3"),
                        SqlUtils.betweenNowMonth(claimBundle.regDt)
                    );
                    break;
                case "pending":
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C8"),
                        SqlUtils.betweenNowMonth(claimBundle.regDt)
                    );
                    break;
            }
        }else{
            //검색어 조건
            if(!claimCancelListSetDto.getClaimSearchType().equalsIgnoreCase("ALL") && !claimCancelListSetDto.getSchClaimSearch().equalsIgnoreCase("")){
                //주문번호, 배송번호, 클레임번호로 조회 시 날짜 조건 없이 전체 조회
                switch (claimCancelListSetDto.getClaimSearchType()){
                    case  "PURCHASEORDERNO" : case  "BUNDLENO" : case  "CLAIMBUNDLENO" :
                        queryOfOrder.WHERE(
                            SqlUtils.equalColumnByInput(claimSearchType, claimCancelListSetDto.getSchClaimSearch())
                        );
                        break;
                    default:
                        queryOfOrder.WHERE(
                            SqlUtils.betweenDay(claimDateType, claimCancelListSetDto.getSchStartDt(), claimCancelListSetDto.getSchEndDt()),
                            SqlUtils.equalColumnByInput(claimSearchType, claimCancelListSetDto.getSchClaimSearch())
                        );
                        break;
                }
            }else{
                queryOfOrder.WHERE(
                    SqlUtils.betweenDay(claimDateType, claimCancelListSetDto.getSchStartDt(), claimCancelListSetDto.getSchEndDt())
                );
            }


            //클레임 상태 (C1: 신청/ C2: 승인/ C3: 완료/ C4: 철회/ C8: 보류/ C9: 거부)
            if(claimCancelListSetDto.getSchClaimStatus() != null && !claimCancelListSetDto.getSchClaimStatus().equalsIgnoreCase("ALL")){
                queryOfOrder.WHERE(
                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, claimCancelListSetDto.getSchClaimStatus())
                );
            }

            //수거 상태 (NN: 요청대기, N0: 수거예약, P0:자동접수요청, P1: 요청완료, P2: 수거중, P3: 수거완료, P8 :수거실패 (반품자동접수실패))
            if(claimCancelListSetDto.getSchPickupStatus() != null && !claimCancelListSetDto.getSchPickupStatus().equalsIgnoreCase("ALL")){
                switch (claimCancelListSetDto.getSchPickupStatus()){
                    case "P0" :
                        queryOfOrder.WHERE(
                            "cps.pick_status IN('P0', 'P1')"
                        );
                        break;
                    default:
                        queryOfOrder.WHERE(
                            SqlUtils.equalColumnByInput(claimPickShipping.pickStatus, claimCancelListSetDto.getSchPickupStatus())
                        );
                        break;
                }
            }

            //배송 상태 (NN: 요청대기, N0: 수거예약, P0:자동접수요청, P1: 요청완료, P2: 수거중, P3: 수거완료, P8 :수거실패 (반품자동접수실패))
            if(claimCancelListSetDto.getSchExchStatus() != null && !claimCancelListSetDto.getSchExchStatus().equalsIgnoreCase("ALL")){
                queryOfOrder.WHERE(
                    SqlUtils.equalColumnByInput(claimExchShipping.exchStatus, claimCancelListSetDto.getSchExchStatus())
                );
            }

            //승인채널 (MYPAGE:구매자 / STORE: 스토어/ ADMIN: 어드민웹 / PARTNER: 파트너웹 / SYSTEM: 시스템배치)
            if(claimCancelListSetDto.getSchClaimChannel() != null && !claimCancelListSetDto.getSchClaimChannel().equalsIgnoreCase("ALL")){
                switch (claimCancelListSetDto.getSchClaimChannel()) {
                    case "ADMIN":
                        queryOfOrder.WHERE(
                            "cr.approve_id NOT IN('MYPAGE', 'STORE', 'PARTNER', 'SYSTEM')"
                        );
                        break;
                    default:
                        queryOfOrder.WHERE(
                            SqlUtils.equalColumnByInput(claimReq.approveId, claimCancelListSetDto.getSchClaimChannel())
                        );
                        break;
                }
            }
        }
        queryOfOrder.GROUP_BY(claimBundle.claimBundleNo);

        SQL claimListCnt = new SQL()
        .SELECT(
            SqlUtils.sqlFunction(MysqlFunction.COUNT, "*")
        ).FROM("(" + queryOfOrder.toString() + ") AS claimListCnt"
        );

        StringBuilder query = new StringBuilder();

        query.append(claimListCnt.toString());
        log.debug("getPOClaimListCnt Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
     * PO 클레임 관리 - 클레임 관리 - 클레임 상세정보
     * 클레임 상세정보 조회
     *
     * @param purchaseOrderNo
     * @param bundleNo
     * @return String 클레임 상세정보 Query
     * @throws Exception 오류처리.
     */
    public static String selectPOClaimDetailList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) throws Exception {

        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimBundle.claimType.concat(" as claimTypeCode"),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimBundle.claimType), nonAliasing(claimBundle.claimType), claimBundle.claimType),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimBundle.claimStatus), nonAliasing(claimBundle.claimStatus), claimBundle.claimStatus),
                claimBundle.bundleNo,
                claimBundle.claimBundleNo,
                claimBundle.claimBundleQty,
                claimReq.requestDt,
                "case cr.request_id "
	             + "when 'MYPAGE' then cr.request_id "
	             + "when 'PARTNER' then cr.request_id "
	             + "when 'STORE' then cr.request_id "
	             + "when 'SYSTEM' then cr.request_id "
  	             + "when 'BATCH' then cr.request_id "
  	             + "when 'WMS' then cr.request_id "
	            + "else 'ADMIN'"
	            + "end request_id",
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimReq.claimReasonType), nonAliasing(claimReq.claimReasonType), claimReq.claimReasonType),
                SqlPatternConstants.getConditionByPattern("IFNULL", claimReq.claimReasonDetail, "''").concat(" as claim_reason_detail"),
                "case "
	             + "when cr.pending_id is null then ''"
                 + "when cr.pending_id = 'MYPAGE' then cr.pending_id "
	             + "when cr.pending_id = 'PARTNER' then cr.pending_id "
	             + "when cr.pending_id = 'STORE' then cr.pending_id "
	             + "when cr.pending_id = 'SYSTEM' then cr.pending_id "
   	             + "when cr.pending_id = 'BATCH' then cr.pending_id "
   	             + "when cr.pending_id = 'WMS' then cr.pending_id "
	            + "else 'ADMIN'"
	            + "end pending_id",
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimReq.pendingReasonType), nonAliasing(claimReq.claimReasonType), claimReq.pendingReasonType),
                SqlPatternConstants.getConditionByPattern("IFNULL", claimReq.pendingReasonDetail, "''").concat(" as pending_reason_detail"),
                SqlPatternConstants.getConditionByPattern("IFNULL", claimReq.pendingDt, "''").concat(" as pending_dt"),
                "case "
   	             + "when cr.reject_id is null then ''"
	             + "when cr.reject_id = 'MYPAGE' then cr.reject_id "
	             + "when cr.reject_id = 'PARTNER' then cr.reject_id "
	             + "when cr.reject_id = 'STORE' then cr.reject_id "
	             + "when cr.reject_id = 'SYSTEM' then cr.reject_id "
   	             + "when cr.reject_id = 'BATCH' then cr.reject_id "
   	             + "when cr.reject_id = 'WMS' then cr.reject_id "
	            + "else 'ADMIN'"
	            + "end reject_id",
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimReq.rejectReasonType), nonAliasing(claimReq.claimReasonType), claimReq.rejectReasonType),
                SqlPatternConstants.getConditionByPattern("IFNULL", claimReq.rejectReasonDetail, "''").concat(" as reject_reason_detail"),
                SqlPatternConstants.getConditionByPattern("IFNULL", claimReq.rejectDt, "''").concat(" as reject_dt"),
                "cm.tot_add_ship_price + cm.tot_add_island_ship_price as add_ship_price",
                "case cm.who_reason "
                 + "when 'B' then '구매자'"
                 + "when 'S' then '판매자'"
                 + "when 'H' then '홈플러스'"
                + "end who_reason"
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo }, claimBundle )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimReq),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo, claimBundle.claimBundleNo }, claimReq )),
                    null
                )
            )
            .WHERE(
              SqlUtils.equalColumnByInput(claimBundle.purchaseOrderNo, purchaseOrderNo),
              SqlUtils.equalColumnByInput(claimBundle.bundleNo, bundleNo)
            );
        log.debug("selectPOClaimDetailList Query :: \n [{}]", query.toString());
        return query.toString();
    }


    /**
     * PO 클레임 관리 - 클레임 관리 - 팝업창 - 클레임 정보
     * 클레임 상세정보 조회
     *
     * @param claimBundleNo
     * @return String 클레임 상세정보 Query
     * @throws Exception 오류처리.
     */
    public static String selectPOClaimDetail(@Param("claimBundleNo") long claimBundleNo) throws Exception {

        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimBundle.claimType.concat(" as claimTypeCode"),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimBundle.claimType), nonAliasing(claimBundle.claimType), claimBundle.claimType),
                claimBundle.claimStatus.concat(" as claimStatusCode"),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimBundle.claimStatus), nonAliasing(claimBundle.claimStatus), claimBundle.claimStatus),
                claimBundle.bundleNo,
                claimBundle.claimBundleNo,
                claimReq.uploadFileName,
                claimReq.uploadFileName2,
                claimReq.uploadFileName3,
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimReq.claimReasonType), nonAliasing(claimReq.claimReasonType), claimReq.claimReasonType),
                claimReq.claimReasonDetail,
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "claim_ship_fee_enclose",
                    SqlUtils.equalColumnByInput(
                        claimBundle.claimShipFeeEnclose, "Y"
                    ),
                    "'택배 동봉'",
                    "'환불금액 차감'"
                ),
                claimMst.whoReason,
                claimMst.totAddShipPrice,
                claimMst.totAddIslandShipPrice
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimReq),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimReq )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimMst),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimNo }, claimMst )),
                    null
                )
            )
            .WHERE(
              SqlUtils.equalColumnByInput(claimBundle.claimBundleNo, claimBundleNo)
            );
        log.debug("selectPOClaimDetail Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
     * PO 클레임 관리 - 클레임 관리 - 클레임 상세정보 - 상품 리스트
     * 클레임 상세정보 - 상품 리스트
     *
     * @param claimBundleNo
     * @return String 클레임 상세정보 - 상품 리스트 Query
     * @throws Exception 오류처리.
     */
    public static String selectPOClaimDetailItemList(@Param("claimBundleNo")long claimBundleNo) throws Exception {

        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class,Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);


        SQL queryOfOpt = new SQL()
            .SELECT(
                claimItem.claimItemNo,
                claimItem.orderItemNo,
                claimItem.itemNo,
                claimItem.itemName,
                SqlUtils.sqlFunction(MysqlFunction.FN_ORDER_OPTION_NM_MANAGE, claimOpt.orderOptNo, "opt_item_name"),
                claimItem.claimItemQty,
                claimItem.itemPrice
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimItem))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimOpt),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.claimNo, claimItem.claimBundleNo, claimItem.claimItemNo }, claimOpt )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimItem.claimBundleNo, claimBundleNo),
                SqlUtils.equalColumnByInput(claimItem.mallType, "DS")
            )
            .ORDER_BY(
                "claim_item_no"
            );

        log.debug("selectPOClaimDetailItemList Query :: \n [{}]", queryOfOpt.toString());
        return queryOfOpt.toString();
    }

    public static String selectPOMainClaimRequestCount(@Param("partnerId") String partnerId) throws Exception {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL();

        //클레임 신청건수
        SQL cancelCntQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, "*")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimType, "C"),
                SqlUtils.equalColumnByInput(claimBundle.partnerId, partnerId),
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C1"),
                SqlUtils.betweenNowMonth(claimBundle.regDt)
            );

        //클레임 신청건수
        SQL returnCntQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, "*")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimType, "R"),
                SqlUtils.equalColumnByInput(claimBundle.partnerId, partnerId),
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C1"),
                SqlUtils.betweenNowMonth(claimBundle.regDt)
            );

        //클레임 신청건수
        SQL exchangeCntQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, "*")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimType, "X"),
                SqlUtils.equalColumnByInput(claimBundle.partnerId, partnerId),
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C1"),
                SqlUtils.betweenNowMonth(claimBundle.regDt)
            );

        query.SELECT(
                SqlUtils.subTableQuery(cancelCntQuery,"cancelRequestCnt"),
                SqlUtils.subTableQuery(returnCntQuery,"returnRequestCnt"),
                SqlUtils.subTableQuery(exchangeCntQuery,"exchangeRequestCnt")
        );
        log.debug("selectPOMainClaimRequestCount Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
     * PO 클레임 관리 - 클레임 관리 - 신청/대기/완료 건수 조회
     * 클레임 건수 조회
     *
     * @param partnerId 파트너Id
     * @param claimType 클레임 타입(C,R,X)
     * @return String 클레임 건수 Query
     * @throws Exception 오류처리.
     */
    public static String selectPOClaimBoardCount(@Param("partnerId") String partnerId, @Param("claimType") String claimType) throws Exception {

        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        SQL query = new SQL();

        //클레임 신청건수
        SQL requestCnt = new SQL()
            .SELECT(
                claimBundle.claimBundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo}, claimItem )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.purchaseOrderNo}, purchaseOrder )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimType, claimType),
                SqlUtils.equalColumnByInput(claimBundle.partnerId, partnerId),
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C1"),
                SqlUtils.equalColumnByInput(claimItem.mallType, "DS"),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_CALL','ORD_TICKET'")),
                SqlUtils.betweenNowMonth(claimBundle.regDt)
            )
            .GROUP_BY(claimBundle.claimBundleNo);

        SQL requestCntQuery = new SQL()
        .SELECT(
            SqlUtils.sqlFunction(MysqlFunction.COUNT, "*")
        ).FROM("(" + requestCnt.toString() + ") AS claimRequestCnt"
        );


        SQL delayCnt = new SQL();
        if(claimType.equalsIgnoreCase("C")){
            //취소 클레임 지연건수
            delayCnt
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, "*")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo}, claimItem )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.purchaseOrderNo}, purchaseOrder )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimType, claimType),
                SqlUtils.equalColumnByInput(claimBundle.partnerId, partnerId),
                SqlUtils.betweenNowMonth(claimBundle.regDt),
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C2"),
                SqlUtils.equalColumnByInput(claimItem.mallType, "DS"),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_CALL','ORD_TICKET'")),
                "cb.claim_no in (select claim_no from claim_payment cp where cp.claim_no = cb.claim_no and cp.payment_status in ('P0', 'P1', 'P2') group by claim_no)",
                "cb.reg_dt >= UF_S_BUSINESS_DAY(DATE_FORMAT(now(), '%Y-%m-%d'),-3,'y','y','','')"
            )
            .GROUP_BY(claimBundle.claimBundleNo);

        }else{
            //반품 클레임 지연건수
            delayCnt
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, "*")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo}, claimItem )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.purchaseOrderNo}, purchaseOrder )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimPickShipping),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimPickShipping ))
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimType, claimType),
                SqlUtils.equalColumnByInput(claimBundle.partnerId, partnerId),
                SqlUtils.betweenNowMonth(claimBundle.regDt),
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C1"),
                SqlUtils.equalColumnByInput(claimPickShipping.pickStatus, "P3"),
                SqlUtils.equalColumnByInput(claimItem.mallType, "DS"),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_CALL','ORD_TICKET'")),
                "cps.pick_p3_dt >= UF_S_BUSINESS_DAY(DATE_FORMAT(now(), '%Y-%m-%d'),-3,'y','y','','')"
            )
            .GROUP_BY(claimBundle.claimBundleNo);

        }

        SQL delayCntQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, "*")
            ).FROM("(" + delayCnt.toString() + ") AS claimDelayCnt"
            );

        //클레임 완료건수
        SQL completeCnt = new SQL()
            .SELECT(
                claimBundle.claimBundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo}, claimItem )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.purchaseOrderNo}, purchaseOrder )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimType, claimType),
                SqlUtils.equalColumnByInput(claimBundle.partnerId, partnerId),
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                SqlUtils.equalColumnByInput(claimItem.mallType, "DS"),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_CALL','ORD_TICKET'")),
                SqlUtils.betweenNowMonth(claimBundle.regDt)
            )
            .GROUP_BY(claimBundle.claimBundleNo);

        SQL completeCntQuery = new SQL()
        .SELECT(
            SqlUtils.sqlFunction(MysqlFunction.COUNT, "*")
        ).FROM("(" + completeCnt.toString() + ") AS claimCompleteCnt"
        );


        //클레임 수거완료건수
        SQL pickupCnt = new SQL()
            .SELECT(
                claimBundle.claimBundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo}, claimItem )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.purchaseOrderNo}, purchaseOrder )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimPickShipping),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimPickShipping ))
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimType, claimType),
                SqlUtils.equalColumnByInput(claimBundle.partnerId, partnerId),
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C1"),
                SqlUtils.equalColumnByInput(claimPickShipping.pickStatus, "P3"),
                SqlUtils.equalColumnByInput(claimItem.mallType, "DS"),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_CALL','ORD_TICKET'")),
                SqlUtils.betweenNowMonth(claimBundle.regDt)
            )
            .GROUP_BY(claimBundle.claimBundleNo);

        SQL pickupCntQuery = new SQL()
        .SELECT(
            SqlUtils.sqlFunction(MysqlFunction.COUNT, "*")
        ).FROM("(" + pickupCnt.toString() + ") AS claimPickupCnt"
        );


        //클레임 보류건수
        SQL pendingCnt = new SQL()
            .SELECT(
                claimBundle.claimBundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo}, claimItem )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.purchaseOrderNo}, purchaseOrder )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimType, claimType),
                SqlUtils.equalColumnByInput(claimBundle.partnerId, partnerId),
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C8"),
                SqlUtils.equalColumnByInput(claimItem.mallType, "DS"),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_CALL','ORD_TICKET'")),
                SqlUtils.betweenNowMonth(claimBundle.regDt)
            )
            .GROUP_BY(claimBundle.claimBundleNo);

        SQL pendingCntQuery = new SQL()
        .SELECT(
            SqlUtils.sqlFunction(MysqlFunction.COUNT, "*")
        ).FROM("(" + pendingCnt.toString() + ") AS claimPendingCnt"
        );



        switch (claimType){
            case "C" : //취소 : 신청,지연,완료 건수 조회
                query.SELECT(
                    SqlUtils.subTableQuery(requestCntQuery,"claimRequestCnt"),
                    SqlUtils.subTableQuery(delayCntQuery,"claimDelayCnt"),
                    SqlUtils.subTableQuery(completeCntQuery,"claimCompleteCnt")
                );
                break;
            case "R" : //반품 : 신청/지연/완료/수거/보류 건수 조회
                query.SELECT(
                    SqlUtils.subTableQuery(requestCntQuery,"claimRequestCnt"),
                    SqlUtils.subTableQuery(delayCntQuery,"claimDelayCnt"),
                    SqlUtils.subTableQuery(completeCntQuery,"claimCompleteCnt"),
                    SqlUtils.subTableQuery(pickupCntQuery,"claimPickupCnt"),
                    SqlUtils.subTableQuery(pendingCntQuery,"claimPendingCnt")
                );
                break;
            case "X" : //교환 : 신청/보류/완료/수거 건수 조회
                query.SELECT(
                    SqlUtils.subTableQuery(requestCntQuery,"claimRequestCnt"),
                    SqlUtils.subTableQuery(completeCntQuery,"claimCompleteCnt"),
                    SqlUtils.subTableQuery(pickupCntQuery,"claimPickupCnt"),
                    SqlUtils.subTableQuery(pendingCntQuery,"claimPendingCnt")
                );

        }


        log.debug("selectClaimBoardCount Query :: \n [{}]", query.toString());
        return query.toString();
    }

    private static String nonAliasing(String value){
        return SqlUtils.nonAlias(value);
    }

}
