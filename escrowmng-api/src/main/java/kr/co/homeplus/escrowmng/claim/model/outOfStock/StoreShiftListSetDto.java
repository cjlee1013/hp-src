package kr.co.homeplus.escrowmng.claim.model.outOfStock;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StoreShiftListSetDto {

    @ApiModelProperty(value = "점포유형", position = 1)
    private String schStoreType;

    @ApiModelProperty(value = "점포ID", position = 2)
    private String schStoreId;

    @ApiModelProperty(value = "요일숫자", position = 3)
    private String weekDay;
}
