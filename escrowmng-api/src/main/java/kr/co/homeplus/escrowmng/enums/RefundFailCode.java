package kr.co.homeplus.escrowmng.enums;

import kr.co.homeplus.escrowmng.enums.impl.EnumImpl;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum RefundFailCode implements EnumImpl {
    REFUND_COMPLETE_SUCCESS("0000", "환불완료 요청 성공하였습니다."),
    REFUND_FAIL_ERR01("1001", "환불완료 실패하였습니다.");

    private final String code;
    private final String description;

    @Override
    public String getResponseCode() {
        return this.code;
    }

    @Override
    public String getResponseMessage() {
        return this.description;
    }
}
