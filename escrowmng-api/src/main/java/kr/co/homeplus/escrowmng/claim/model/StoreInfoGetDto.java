package kr.co.homeplus.escrowmng.claim.model;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreInfoGetDto {
       
    @ApiModelProperty(value = "점포ID", position = 1)
    public String storeId;

    @ApiModelProperty(value = "파트너ID", position = 2)
    public String partnerId;

    @ApiModelProperty(value = "점포명", position = 3)
    public String storeNm;

    @ApiModelProperty(value = "점포명2", position = 4)
    public String storeNm2;

    @ApiModelProperty(value = "점포타입", position = 5)
    public String storeType;

    @ApiModelProperty(value = "점포유형", position = 6)
    public String storeKind;

    @ApiModelProperty(value = "매니저명", position = 7)
    public String mgrNm;

    @ApiModelProperty(value = "점포ID", position = 8)
    public String originStoreId;

    @ApiModelProperty(value = "우편번호", position = 9)
    public String zipcode;

    @ApiModelProperty(value = "주소1", position = 10)
    public String addr1;

    @ApiModelProperty(value = "주소2", position = 11)
    public String addr2;

    @ApiModelProperty(value = "휴대폰번호", position = 12)
    public String phone;

    @ApiModelProperty(value = "매장마감정보", position = 13)
    public String closedInfo;

    @ApiModelProperty(value = "사용유무", position = 14)
    public String useYn;

    @ApiModelProperty(value = "온라인유무", position = 15)
    public String onlineYn;

    @ApiModelProperty(value = "픽업유무", position = 16)
    public String pickupYn;

    @ApiModelProperty(value = "온라인고객센터", position = 17)
    public String onlineCostCenter;

    @ApiModelProperty(value = "점포고객센터", position = 18)
    public String storeCostCenter;
}
