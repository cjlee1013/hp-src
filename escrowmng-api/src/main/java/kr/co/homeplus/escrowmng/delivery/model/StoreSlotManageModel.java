package kr.co.homeplus.escrowmng.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "점포 SLOT 관리(옴니연동)")
@Getter
@Setter
@EqualsAndHashCode(callSuper=false)
public class StoreSlotManageModel extends StoreShiftManageModel {

    @ApiModelProperty(value = "슬롯 ID", position = 1)
    private String slotId;

    @ApiModelProperty(value = "사용여부", position = 2)
    private String useYn;

    @ApiModelProperty(value = "SLOT 배송 시작 시간", position = 3)
    private String slotShipStartTime;

    @ApiModelProperty(value = "SLOT 배송 종료 시간", position = 4)
    private String slotShipEndTime;

    @ApiModelProperty(value = "SLOT 배송 마감 시간(SLOT주문종료시간)", position = 5)
    private String slotShipCloseTime;

    @ApiModelProperty(value = "주문고정건수(점포주문 : 주문기준건수)", position = 6)
    private Long orderFixCnt;

    @ApiModelProperty(value = "주문건수(점포주문 : 주문실제건수)", position = 7)
    private Long orderCnt;

    @ApiModelProperty(value = "주문변경건수(점포주문 : 주문최대건수) Slot 주문초과 금지", position = 8)
    private Long orderChgCnt;

    @ApiModelProperty(value = "픽업주문고정건수(픽업주문:주문기준건수)", position = 9)
    private Long pickupOrderFixCnt;

    @ApiModelProperty(value = "픽업주문건수(픽업주문:주문실제건수)", position = 10)
    private Long pickupOrderCnt;

    @ApiModelProperty(value = "픽업주문변경건수(픽업주문:주문최대건수)", position = 11)
    private Long pickupOrderChgCnt;

    @ApiModelProperty(value = "사물함주문고정건수(사물함주문:주문기준건수)", position = 12)
    private Long lockerOrderFixCnt;

    @ApiModelProperty(value = "사물함주문예약건수(사물함주문:주문실제건수)", position = 13)
    private Long lockerOrderCnt;

    @ApiModelProperty(value = "사물함주문변경건수(사물함주문:주문최대건수)", position = 14)
    private Long lockerOrderChgCnt;

    @ApiModelProperty(value = "예약주문건수(예약주문:주문실제건수)", position = 13)
    private Long reserveOrderFixCnt;

    @ApiModelProperty(value = "예약주문변경건수(예약주문:주문최대건수)", position = 14)
    private Long reserveOrderChgCnt;

    @ApiModelProperty(value = "수정자", position = 98)
    private String chgId;

    @ApiModelProperty(value = "수정일시", position = 99)
    private String chgDt;

}
