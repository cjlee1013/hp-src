package kr.co.homeplus.escrowmng.claim.model.partner;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PartnerMainBoardClaimRequestCountGetDto {

    @ApiModelProperty(value="취소요청건수")
    private long cancelRequestCnt;

    @ApiModelProperty(value="반품요청건수")
    private long returnRequestCnt;

    @ApiModelProperty(value="교환요청건수")
    private long exchangeRequestCnt;

}
