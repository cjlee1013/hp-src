package kr.co.homeplus.escrowmng.order.service;

import com.google.re2j.Matcher;
import com.google.re2j.Pattern;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.escrowmng.claim.model.external.message.MessageSendTalkSetDto;
import kr.co.homeplus.escrowmng.claim.service.ClaimProcessService;
import kr.co.homeplus.escrowmng.claim.service.ExternalService;
import kr.co.homeplus.escrowmng.claim.service.forward.ClaimForwardSetDto;
import kr.co.homeplus.escrowmng.core.exception.LogicException;
import kr.co.homeplus.escrowmng.enums.DateType;
import kr.co.homeplus.escrowmng.enums.OrderCode;
import kr.co.homeplus.escrowmng.escrow.model.MultiSafetyIssueRequest;
import kr.co.homeplus.escrowmng.escrow.model.SafetyIssueRequest;
import kr.co.homeplus.escrowmng.escrow.service.SafetyIssueService;
import kr.co.homeplus.escrowmng.order.factory.OrderResponseFactory;
import kr.co.homeplus.escrowmng.order.model.management.OrderHistoryListDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchClaimDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchDetailDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchGetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchPaymentDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchPriceDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchProdDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchSetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchShipDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchSubstitutionDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchTckGetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchTckSetDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.MultiOrderShipAddrInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderCashReceiptDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderCouponDiscountDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderDelayShipInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderGiftInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderPartnerInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderPromoInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderSaveInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderShipAddrInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderShipTotalInfoDto;
import kr.co.homeplus.escrowmng.order.model.ship.MultiOrderShipAddrEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipInfoGetDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipInfoSetDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipSetGto;
import kr.co.homeplus.escrowmng.order.model.ship.OrderShipAddrEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.OrderShipOriginInfo;
import kr.co.homeplus.escrowmng.order.model.ship.ShipChangeOmniGetDto;
import kr.co.homeplus.escrowmng.order.model.ship.ShipChangeOmniSetDto;
import kr.co.homeplus.escrowmng.order.model.ship.StoreShipMemoDto;
import kr.co.homeplus.escrowmng.order.model.ship.StoreShipMsgDto;
import kr.co.homeplus.escrowmng.utils.DateUtils;
import kr.co.homeplus.escrowmng.utils.LogUtils;
import kr.co.homeplus.escrowmng.utils.MessageSendUtil;
import kr.co.homeplus.escrowmng.utils.ObjectUtils;
import kr.co.homeplus.escrowmng.utils.ProcessHistoryUtills;
import kr.co.homeplus.escrowmng.utils.SetParameter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderManagementService {

    private final OrderManagementMapperService mapperService;

    private final SafetyIssueService safetyIssueService;

    private final ExternalService externalService;

    private final ProcessHistoryUtills processHistoryUtills;

    private final ClaimProcessService processService;

    /**
     * 주문관리 조회
     * @param orderSearchSetDto 조회요청 DTO
     * @return List<OrderSearchGetDto> 조회응답.
     */
    public ResponseObject<List<OrderSearchGetDto>> getOrderMainSearch(OrderSearchSetDto orderSearchSetDto) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOrderManagementInfo(orderSearchSetDto));
    }

    /**
     * 주문정보 상세 - 구매자정보
     *
     * @param purchaseOrderNo 주문번호
     * @return OrderSearchProdDto 구매자정보
     */
    public ResponseObject<OrderSearchProdDto> getOrderDetailUserInfo(long purchaseOrderNo){
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOrderDetailByUser(purchaseOrderNo));
    }

    /**
     * 주문정보 상세 - 금액정보
     *
     * @param purchaseOrderNo 주문번호
     * @return OrderSearchPriceDto 금액정보
     */
    public ResponseObject<OrderSearchPriceDto> getOrderDetailPriceInfo(long purchaseOrderNo){
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOrderDetailByPrice(purchaseOrderNo));
    }

    /**
     * 주문정보 상세 - 주문정보
     *
     * @param purchaseOrderNo 주문번호
     * @return OrderSearchDetailDto 주문정보
     */
    public ResponseObject<List<OrderSearchDetailDto>> getOrderDetailPurchaseInfo(long purchaseOrderNo){
        //주문 종류 파악
        List<OrderSearchDetailDto> resultDto = new ArrayList<>();
        if(!"ORD_MULTI".contains(mapperService.getOrderTypeCheck(purchaseOrderNo))){
            resultDto = mapperService.getOrderDetailByPurchase(purchaseOrderNo);
        } else {
            resultDto = mapperService.getMultiOrderDetailByPurchase(purchaseOrderNo);
        }
        if(resultDto != null){
            for(OrderSearchDetailDto dto : resultDto){
                if(ObjectUtils.toInt(dto.getClaimQty()) != 0){
                    int orderPrice = ObjectUtils.toInt(dto.getOrderPrice()) - (ObjectUtils.toInt(dto.getItemPrice()) * ObjectUtils.toInt(dto.getClaimQty()));
                    dto.setOrderPrice(String.valueOf(orderPrice));
                }
            }
        }
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, resultDto);
    }

    /**
     * 주문정보 상세 - 결제정보
     * @param purchaseOrderNo 주문번호
     * @return OrderSearchPaymentDto 결제내역
     */
    public ResponseObject<List<OrderSearchPaymentDto>> getOrderDetailPaymentInfo(long purchaseOrderNo){
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOrderDetailByPayment(purchaseOrderNo));
    }

    /**
     * 주문정보 상세 - 배송정보
     *
     * @param purchaseOrderNo 주문번호
     * @return OrderSearchShipDto 배송정보
     */
    public ResponseObject<List<OrderSearchShipDto>> getOrderDetailShipInfo(long purchaseOrderNo, String mallType) {
        List<OrderSearchShipDto> returnList = new ArrayList<>();
        String orderType = mapperService.getOrderTypeCheck(purchaseOrderNo);
        if("ORD_MULTI".contains(orderType) && mallType.equals("DS")){
            returnList = mapperService.getMultiOrderDetailByShipping(purchaseOrderNo);
        } else if(!"ORD_MULTI".contains(orderType)){
            returnList = mapperService.getOrderDetailByShipping(purchaseOrderNo, mallType);
            if(mallType.equals("TD")){
                List<OrderSearchShipDto> tdList = new ArrayList<>();
                for(long bundleNo : returnList.stream().map(OrderSearchShipDto::getBundleNo).distinct().collect(Collectors.toList())){
                    OrderSearchShipDto shipDto = returnList.stream().filter(dto -> dto.getBundleNo() == bundleNo).sorted(Comparator.comparing(OrderSearchShipDto::getShipStatus).reversed()).collect(Collectors.toList()).get(0);
                    shipDto.setItemQty(String.valueOf(returnList.stream().filter(dto -> dto.getBundleNo() == bundleNo).map(OrderSearchShipDto::getItemQty).mapToInt(Integer::parseInt).sum()));
                    tdList.add(shipDto);
                }
                return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, tdList);
            }
        }
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, returnList);
    }

    /**
     * 주문정보 상세 - 클레임정보
     *
     * @param purchaseOrderNo 주문번호
     * @return OrderSearchClaimDto 클레임정보
     */
    public ResponseObject<List<OrderSearchClaimDto>> getOrderDetailClaimInfo(long purchaseOrderNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOrderDetailByClaim(purchaseOrderNo));
    }

    /**
     * 주문정보 상세 - 주문 > 배송정보 수정
     *
     * @param bundleNo 배송번호
     * @return OrderShipAddrInfoDto 배송정보
     */
    public ResponseObject<List<OrderShipAddrInfoDto>> getOrderShipAddrInfo(long bundleNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOrderShipAddrInfo(bundleNo));
    }

    /**
     * 주문정보 상세 - 주문 > 배송정보 수정
     *
     * @param bundleNo 배송번호
     * @return OrderShipAddrInfoDto 배송정보
     */
    public ResponseObject<List<MultiOrderShipAddrInfoDto>> getMultiOrderShipAddrInfo(long bundleNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getMultiOrderShipAddrInfo(bundleNo));
    }

    /**
     * 주문정보 상세 - 주문 > 송장 등록/수정, 배송완료
     *
     * @param bundleNo 배송번호
     * @param shipStatus 배송상태
     * @return OrderShipTotalInfoDto 배송관련정보
     */
    public ResponseObject<List<OrderShipTotalInfoDto>> getOrderShipTotalInfo(long bundleNo, String shipStatus) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOrderShipTotalInfo(bundleNo, shipStatus));
    }

    /**
     * 주문정보 상세 - 주문 > 주소지변경
     *
     * @param shipAddrEditDto 주소지변경 데이터
     * @return 주소지 변경 결과
     */
    @Transactional(rollbackFor = {Exception.class, LogicException.class}, propagation = Propagation.NESTED, isolation = Isolation.READ_COMMITTED)
    public ResponseObject<String> modifyShipAddrChange(OrderShipAddrEditDto shipAddrEditDto) throws Exception {
        // 배송번호에 포함된 주문번호로 배송타입 조회시 TD외에 다른 건이 존재하면 주문번호로 업데이트 아닐 경우 해당 번들번호로 수정하도록 함.
        List<LinkedHashMap<String, Object>> presentStatusList = mapperService.getShippingPresentStatus(shipAddrEditDto.getPurchaseOrderNo());

        List<String> shipStatusList = presentStatusList.stream().map(map -> map.get("ship_status")).map(ObjectUtils::toString).distinct().collect(Collectors.toList());
        if(shipStatusList.contains("D3") || shipStatusList.contains("D4")|| shipStatusList.contains("D5") || shipStatusList.contains("NN")){
            if(presentStatusList.stream().map(map -> map.get("ship_status")).map(ObjectUtils::toString).allMatch(status -> status.equalsIgnoreCase("NN"))){
                throw new LogicException(OrderCode.ORDER_SHIP_ADDR_CHANGE_ERR_03);
            } else {
                throw new LogicException(OrderCode.ORDER_SHIP_ADDR_CHANGE_ERR_02);
            }
        }
        boolean isTdOnly = presentStatusList.stream().map(map -> map.get("ship_type")).map(ObjectUtils::toString).allMatch(status -> status.equalsIgnoreCase("TD"));
        boolean isDsOnly = presentStatusList.stream().map(map -> map.get("ship_type")).map(ObjectUtils::toString).allMatch(status -> status.equalsIgnoreCase("DS"));
        boolean isCombine = presentStatusList.stream().map(map -> map.get("order_type")).map(ObjectUtils::toString).anyMatch(status -> status.equalsIgnoreCase("ORD_COMBINE"));
        boolean isTd = presentStatusList.stream().map(map -> map.get("ship_type")).map(ObjectUtils::toString).anyMatch(status -> status.equalsIgnoreCase("TD"));
        OrderShipAddrInfoDto beforeOrderShipAddrInfo = new OrderShipAddrInfoDto();

        // 리스트로 처리
        List<Long> bundleList = new ArrayList<>();
        // 전부 TD가 아니거나 DS가 아닌 경우 복합으로처리.
        if((!isDsOnly && !isTdOnly) || (isTdOnly && isCombine)){
            for(LinkedHashMap<String, Object> map : presentStatusList){
                bundleList.add(ObjectUtils.toLong(map.get("bundle_no")));
            }
        } else {
            bundleList.add(shipAddrEditDto.getBundleNo());
        }

        for(long bundleNo: bundleList){
            // 배송지변경 저장
            StringBuilder beforeDesc = new StringBuilder();
            StringBuilder afterDesc = new StringBuilder();
            shipAddrEditDto.setBundleNo(bundleNo);
            List<OrderShipAddrInfoDto> shipAddrInfo = mapperService.getOrderShipAddrInfo(shipAddrEditDto.getBundleNo());
            if(!shipAddrInfo.stream().map(OrderShipAddrInfoDto::getShipMobileNo).allMatch(str -> str.equals(shipAddrEditDto.getShipMobileNo()))){
                // 전화번호 안신번호 변경 요청.
                beforeDesc.append(shipAddrInfo.stream().map(OrderShipAddrInfoDto::getShipMobileNo).collect(Collectors.toList()).get(0));
                afterDesc.append(shipAddrEditDto.getShipMobileNo());
                // 안심번호 신청여부 판단
                LinkedHashMap<String, Object> safetyIssueInfo = mapperService.getSafetyIssueInfo(shipAddrEditDto.getBundleNo());
                if(ObjectUtils.toString(safetyIssueInfo.get("safety_use_yn")).equalsIgnoreCase("Y")){
                    try {
                        safetyIssueService.updateSafetyIssueForChange(
                            createSafetyUpdateDto(
                                shipAddrEditDto.getBundleNo(),
                                ObjectUtils.toLong(safetyIssueInfo.get("purchase_order_no")),
                                shipAddrEditDto.getShipMobileNo()
                            )
                        );
                    } catch (Exception e){
                        log.error("safetyIssue Update Error ::: {}", e.getMessage());
                        throw new LogicException(OrderCode.ORDER_SHIP_ADDR_CHANGE_ERR_04);
                    }
                }
            };

            if(!shipAddrInfo.stream().map(OrderShipAddrInfoDto::getReceiverNm).allMatch(str -> str.equals(shipAddrEditDto.getReceiverNm()))){
                beforeDesc.append("\n".concat(shipAddrInfo.stream().map(OrderShipAddrInfoDto::getReceiverNm).collect(Collectors.toList()).get(0)));
                afterDesc.append("\n".concat(shipAddrEditDto.getReceiverNm()));
            }

            if(!shipAddrInfo.stream().map(OrderShipAddrInfoDto::getRoadBaseAddr).allMatch(str -> str.equals(shipAddrEditDto.getRoadBaseAddr()))
                || !shipAddrInfo.stream().map(OrderShipAddrInfoDto::getRoadDetailAddr).allMatch(str -> str.equals(shipAddrEditDto.getRoadDetailAddr()))
            ){
                beforeDesc.append("\n".concat(shipAddrInfo.stream().map(OrderShipAddrInfoDto::getZipCode).collect(Collectors.toList()).get(0)).concat("\n").concat(shipAddrInfo.stream().map(OrderShipAddrInfoDto::getRoadBaseAddr).collect(Collectors.toList()).get(0)).concat("\n".concat(shipAddrInfo.stream().map(OrderShipAddrInfoDto::getRoadDetailAddr).collect(Collectors.toList()).get(0))));
                afterDesc.append("\n".concat(shipAddrEditDto.getZipCode()).concat("\n").concat(shipAddrEditDto.getRoadBaseAddr()).concat(" ").concat(shipAddrEditDto.getRoadDetailAddr()));
            }

            if(!shipAddrInfo.stream().map(OrderShipAddrInfoDto::getShipMsg).allMatch(str -> str.equals(shipAddrEditDto.getShipMsg()))){
                beforeDesc.append("\n".concat(shipAddrInfo.stream().map(OrderShipAddrInfoDto::getShipMsg).collect(Collectors.toList()).get(0)));
                afterDesc.append("\n".concat(shipAddrEditDto.getShipMsg()));
            }

            if(beforeDesc.toString().length() > 0) {
                LogUtils.addLogHistory(shipAddrEditDto, "배송정보 수정", afterDesc.toString(), beforeDesc.toString(), shipAddrEditDto.getChgId());
            }

            if(!mapperService.modifyShipAddrChange(shipAddrEditDto)){
                throw new LogicException(OrderCode.ORDER_SHIP_ADDR_CHANGE_ERR_01);
            }
            beforeOrderShipAddrInfo = shipAddrInfo.get(0);
        }

        // 주문 정보 변경 히스토리 등록
        try{
            this.setOrderModifyAddrHistory(beforeOrderShipAddrInfo, shipAddrEditDto);
        }catch (Exception e){
            log.error("[modifyShipAddrChange] 주문 주소지 변경 히스토리 등록 오류 : {}", e);
        }

        // 옴니전송.
        if(isTd){
            LinkedHashMap<String, Object> paramMap = presentStatusList.stream().filter(map -> ObjectUtils.isEquals("ORD_NOR", map.get("order_type")) && ObjectUtils.isEquals("TD", map.get("ship_type"))).collect(Collectors.toList()).get(0);
            List<Object> combineList = presentStatusList.stream().filter(map -> ObjectUtils.isEquals("ORD_COMBINE", map.get("order_type")) && ObjectUtils.isEquals("TD", map.get("ship_type"))).map(map -> map.get("purchase_store_info_no")).collect(Collectors.toList());
            if(combineList.size() > 0) {
                paramMap.put("combine_purchase_store_info_no", combineList.get(0));
            }
            //ShipChangeOmniSetDto 을 만들어야 함.
            try {
                paramMap.putAll(ObjectUtils.getConvertMap(shipAddrEditDto));
                ResponseObject<Object> result = externalService.externalToOmni(createShipChangeOmniDto(paramMap));
                log.info("주소지 변경에 따른 옴니 전송결과 :: [purchaseOrderNo : {}, returnCode : {}, returnMsg : {}]", shipAddrEditDto.getPurchaseOrderNo(), result.getReturnCode(), result.getReturnMessage());
                if(!"0000,SUCCESS".contains(result.getReturnCode())){
                    throw new LogicException(result.getReturnCode(), result.getReturnMessage());
                }
            } catch (Exception e) {
                log.error("주문번호({}) 주소지 변경 옴니 요청중 오류가 발생했습니다. ::: {}", shipAddrEditDto.getPurchaseOrderNo(), e.getMessage());
                throw new LogicException(OrderCode.ORDER_SHIP_ADDR_CHANGE_ERR_01);
            }
        }
        // 옴니연동 필수.
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS);
    }

    @Transactional(rollbackFor = {Exception.class, LogicException.class}, propagation = Propagation.NESTED, isolation = Isolation.READ_COMMITTED)
    public ResponseObject<String> modifyMultiShipAddrChange(MultiOrderShipAddrEditDto multiOrderShipAddrEditDto) throws Exception{
        List<LinkedHashMap<String, Object>> shipPresentInfo = mapperService.getShippingPresentStatus(multiOrderShipAddrEditDto.getPurchaseOrderNo());
        if(shipPresentInfo.size() <= 0){
            throw new LogicException(OrderCode.ORDER_SHIP_ADDR_CHANGE_ERR_05);
        }
        if(!shipPresentInfo.stream().map(map -> map.get("ship_status")).map(ObjectUtils::toString).allMatch(status -> status.equalsIgnoreCase("D1"))){
            if(shipPresentInfo.stream().map(map -> map.get("ship_status")).map(ObjectUtils::toString).allMatch(status -> status.equalsIgnoreCase("NN"))){
                throw new LogicException(OrderCode.ORDER_SHIP_ADDR_CHANGE_ERR_03);
            } else {
                throw new LogicException(OrderCode.ORDER_SHIP_ADDR_CHANGE_ERR_02);
            }
        }

        // 배송지변경 저장
        StringBuilder beforeDesc = new StringBuilder();
        StringBuilder afterDesc = new StringBuilder();

        List<MultiOrderShipAddrInfoDto> shipAddrInfo = mapperService.getMultiOrderShipAddrInfo(multiOrderShipAddrEditDto.getMultiBundleNo());
        // 멀티번들용 전화번호 안심번호 설정은 추루.
        if(!shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getShipMobileNo).allMatch(str -> str.equals(multiOrderShipAddrEditDto.getShipMobileNo()))){
            // 전화번호 변경여부
            beforeDesc.append(shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getShipMobileNo).collect(Collectors.toList()).get(0));
            afterDesc.append(multiOrderShipAddrEditDto.getShipMobileNo());
            // 안심번호 신청여부 판단
            if(shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getSafetyUseYn).anyMatch(str -> str.equals("Y"))){
                try {
                    safetyIssueService.updateMultiSafetyIssueForChange(
                        createMultiSafetyUpdateDto(
                            multiOrderShipAddrEditDto.getMultiBundleNo(),
                            multiOrderShipAddrEditDto.getShipMobileNo()
                        )
                    );
                } catch (Exception e){
                    log.error("multiSafetyIssue Update Error ::: {}", e.getMessage());
                    throw new LogicException(OrderCode.ORDER_SHIP_ADDR_CHANGE_ERR_04);
                }
            }
        }

        if(!shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getReceiverNm).allMatch(str -> str.equals(multiOrderShipAddrEditDto.getReceiverNm()))){
            beforeDesc.append("\n".concat(shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getReceiverNm).collect(Collectors.toList()).get(0)));
            afterDesc.append("\n".concat(multiOrderShipAddrEditDto.getReceiverNm()));
        }

        if(!shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getRoadBaseAddr).allMatch(str -> str.equals(multiOrderShipAddrEditDto.getRoadBaseAddr()))
            || !shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getRoadDetailAddr).allMatch(str -> str.equals(multiOrderShipAddrEditDto.getRoadDetailAddr()))
        ){
            beforeDesc.append("\n".concat(shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getZipCode).collect(Collectors.toList()).get(0)).concat("\n").concat(shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getRoadBaseAddr).collect(Collectors.toList()).get(0)).concat("\n".concat(shipAddrInfo.stream().map(MultiOrderShipAddrInfoDto::getRoadDetailAddr).collect(Collectors.toList()).get(0))));
            afterDesc.append("\n".concat(multiOrderShipAddrEditDto.getZipCode()).concat("\n").concat(multiOrderShipAddrEditDto.getRoadBaseAddr()).concat(" ").concat(multiOrderShipAddrEditDto.getRoadDetailAddr()));
        }

        if(beforeDesc.toString().length() > 0) {
            LogUtils.addLogHistory(multiOrderShipAddrEditDto, "배송정보 수정", afterDesc.toString(), beforeDesc.toString(), multiOrderShipAddrEditDto.getChgId());
        }

        // 멀티번들 주소 변경
        if(!mapperService.modifyMultiOrderShipAddrInfo(multiOrderShipAddrEditDto)){
            throw new LogicException(OrderCode.ORDER_SHIP_ADDR_CHANGE_ERR_01);
        }

        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS);
    }

    /**
     * 주문정보 상세 - 미수취 처리
     *
     * @param noRcvShipEditDto 미수취 처리 데이터
     * @return 미수취 처리 결과
     */
    public ResponseObject<String> modifyNoRcvShipInfo(NoRcvShipEditDto noRcvShipEditDto) throws Exception {
        HashMap<String, Object> checkData = mapperService.getNoReceiveCheck(noRcvShipEditDto.getBundleNo());
        if(!mapperService.modifyNoRcvShipInfo(noRcvShipEditDto)){
            return OrderResponseFactory.getResponseObject(OrderCode.ORDER_NO_RCV_SHIP_CHANGE_ERR01);
        }
        try {
            if(noRcvShipEditDto.getNoRcvProcessType().equalsIgnoreCase("W")){
                processHistoryUtills.setProcessHistory(noRcvShipEditDto, "미수취신고 철회", "", "", noRcvShipEditDto.getChgId());
                LinkedHashMap<String, Object> shipCompleteInfo = mapperService.getNonShipCompleteInfo(noRcvShipEditDto.getPurchaseOrderNo(), noRcvShipEditDto.getBundleNo());
                String shipStatus = ObjectUtils.toString(shipCompleteInfo.get("ship_status"));
                if("D3,D4".contains(shipStatus)){
                    if("D3".equals(shipStatus)){
                        processService.setClaimShipComplete(String.valueOf(noRcvShipEditDto.getBundleNo()), "ADMIN");
                    } else {
                        if(DateUtils.isAfter(ObjectUtils.toString(shipCompleteInfo.get("complete_dt")), DateType.DAY, 8)){
                            processService.setClaimShipDecision(String.valueOf(noRcvShipEditDto.getBundleNo()), "ADMIN");
                        }
                    }
                }
            } else if(noRcvShipEditDto.getNoRcvProcessType().equalsIgnoreCase("C")){
                this.sendNoRcvMessage(noRcvShipEditDto.getBundleNo(), checkData.get("buyer_mobile_no"), noRcvShipEditDto.getNoRcvProcessCntnt());
                processHistoryUtills.setProcessHistory(noRcvShipEditDto, "미수취신고 철회요청", "", "", noRcvShipEditDto.getChgId());
            }
        } catch (Exception e) {
            log.error("미수취 철회에 따른 배송상태 업데이트 중 오류발생 ::: {}", e.getMessage());
        }
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS);
    }

    /**
     * 주문정보 상세 - 미수취 처리
     *
     * @param noRcvShipEditDto 미수취 처리 데이터
     * @return 미수취 처리 결과
     */
    public ResponseObject<String> modifyNoRcvShipInfoForShipping(NoRcvShipEditDto noRcvShipEditDto) throws Exception {
        HashMap<String, Object> checkData = mapperService.getNoReceiveCheck(noRcvShipEditDto.getBundleNo());
        if(checkData.size() > 0 &&
            (ObjectUtils.toString(checkData.get("no_rcv_process_yn")).equalsIgnoreCase("N")
                && ObjectUtils.toString(checkData.get("no_rcv_wthdrw_yn")).equalsIgnoreCase("N"))
        ){
            if(!mapperService.modifyNoRcvShipInfo(noRcvShipEditDto)){
                return OrderResponseFactory.getResponseObject(OrderCode.ORDER_NO_RCV_SHIP_CHANGE_ERR01);
            }
            try {
                if(noRcvShipEditDto.getNoRcvProcessType().equalsIgnoreCase("W")){
                    processHistoryUtills.setProcessHistory(noRcvShipEditDto, "미수취신고 철회", "", "", noRcvShipEditDto.getChgId());
                    LinkedHashMap<String, Object> shipCompleteInfo = mapperService.getNonShipCompleteInfo(noRcvShipEditDto.getPurchaseOrderNo(), noRcvShipEditDto.getBundleNo());
                    String shipStatus = ObjectUtils.toString(shipCompleteInfo.get("ship_status"));
                    if("D3,D4".contains(shipStatus)){
                        if("D3".equals(shipStatus)){
                            processService.setClaimShipComplete(String.valueOf(noRcvShipEditDto.getBundleNo()), "ADMIN");
                        } else {
                            if(DateUtils.isAfter(ObjectUtils.toString(shipCompleteInfo.get("complete_dt")), DateType.DAY, 8)){
                                processService.setClaimShipDecision(String.valueOf(noRcvShipEditDto.getBundleNo()), "ADMIN");
                            }
                        }
                    }
                } else if(noRcvShipEditDto.getNoRcvProcessType().equalsIgnoreCase("C")){
                    this.sendNoRcvMessage(noRcvShipEditDto.getBundleNo(), checkData.get("buyer_mobile_no"), noRcvShipEditDto.getNoRcvProcessCntnt());
                    processHistoryUtills.setProcessHistory(noRcvShipEditDto, "미수취신고 철회요청", "", "", noRcvShipEditDto.getChgId());
                }
            } catch (Exception e) {
                log.error("(Shipping) 미수취 철회에 따른 배송상태 업데이트 중 오류발생 ::: {}", e.getMessage());
            }
            return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS);
        }
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_NO_RCV_SHIP_CHANGE_ERR02);
    }

    /**
     * 주문정보 상세 - 미수취 등록
     *
     * @param noRcvShipSetGto 미수취 등록 데이터
     * @return 미수취 등록 결과
     */
    public ResponseObject<String> addNoRcvShipInfo(NoRcvShipSetGto noRcvShipSetGto) throws Exception {
        try {
            if(ObjectUtils.toLong(mapperService.getNoReceiveCheck(noRcvShipSetGto.getBundleNo()).get("cnt")) > 0){
                throw new LogicException(OrderCode.ORDER_NO_RCV_SHIP_ADD_ERR03);
            }

            LinkedHashMap<String, Object> noRcvMakeData = mapperService.getNoRcvShipRegMakeData(noRcvShipSetGto.getBundleNo());

            if(noRcvMakeData.isEmpty()){
                throw new LogicException(OrderCode.ORDER_NO_RCV_SHIP_ADD_ERR02);
            }
            noRcvShipSetGto.setUserNo(ObjectUtils.toLong(noRcvMakeData.get("user_no")));
             // 주문번호
            noRcvShipSetGto.setPurchaseOrderNo(ObjectUtils.toLong(noRcvMakeData.get("purchase_order_no")));
            // 파트너ID
            noRcvShipSetGto.setPartnerId(ObjectUtils.toString(noRcvMakeData.get("partner_id")));

            if(!mapperService.addNoRcvShipInfo(noRcvShipSetGto)){
                return OrderResponseFactory.getResponseObject(OrderCode.ORDER_NO_RCV_SHIP_ADD_ERR01);
            }
            processHistoryUtills.setProcessHistory(noRcvShipSetGto, "미수취신고", "", "", noRcvShipSetGto.getRegId());
        } catch (SQLException se) {
            log.debug("{} ::: {}", se.getErrorCode(), se.getMessage());
        }

        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS);
    }

    /**
     * 주문정보 상세 - 미수취 조회
     *
     * @param infoSetDto 미수취 조회 데이터
     * @return 미수취 조회 결과
     */
    public ResponseObject<NoRcvShipInfoGetDto> getNoRcvShipInfo(NoRcvShipInfoSetDto infoSetDto) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getNoRcvShipInfo(infoSetDto));
    }

    /**
     * 주문정보 상세 - 발송지연내역
     * @param shipNo 운송번호
     * @return OrderDelayShipInfoDto 발송지연 데이터
     */
    public ResponseObject<OrderDelayShipInfoDto> getDelayShipInfo(long shipNo){
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getDelayShipInfo(shipNo));
    }

    /**
     * 주문정보 상세 - 판매업체 정보
     * @param partnerId 판매업체ID
     * @param bundleNo 배송번호
     * @return
     */
    public ResponseObject<OrderPartnerInfoDto> getOrderPartnerInfo(String partnerId, long bundleNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOrderPartnerInfo(partnerId, bundleNo));
    }

    public ResponseObject<List<OrderPromoInfoDto>> getOrderPromoPopInfo(long purchaseOrderNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOrderPromoPopInfo(purchaseOrderNo));
    }

    public ResponseObject<List<OrderCouponDiscountDto>> getOrderCouponDiscountPopInfo(long purchaseOrderNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOrderCouponDiscountPopInfo(purchaseOrderNo));
    }

    public ResponseObject<List<OrderGiftInfoDto>> getOderGiftPopInfo(long purchaseOrderNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOderGiftPopInfo(purchaseOrderNo));
    }

    public ResponseObject<List<OrderSaveInfoDto>> getOrderSavePopInfo(long purchaseOrderNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOrderSavePopInfo(purchaseOrderNo));
    }


    public ResponseObject<List<OrderSearchSubstitutionDto>> getOrderSubstitutionInfo(long purchaseOrderNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOrderSubstitutionInfo(purchaseOrderNo));
    }

    private SafetyIssueRequest createSafetyUpdateDto(long bundleNo, long purchaseOrderNo, String changeMobileNo){
        SafetyIssueRequest safetyIssueRequest = new SafetyIssueRequest();
        safetyIssueRequest.setBundleNo(bundleNo);
        safetyIssueRequest.setPurchaseOrderNo(purchaseOrderNo);
        safetyIssueRequest.setReqPhoneNo(changeMobileNo);
        return safetyIssueRequest;
    }

    private MultiSafetyIssueRequest createMultiSafetyUpdateDto(long multiBundleNo, String changeMobileNo){
        MultiSafetyIssueRequest safetyIssueRequest = new MultiSafetyIssueRequest();
        safetyIssueRequest.setMultiBundleNo(multiBundleNo);
        safetyIssueRequest.setReqPhoneNo(changeMobileNo);
        return safetyIssueRequest;
    }

    /**
     * TCK전용 주문관리 조회
     * @param orderSearchSetDto 조회요청 DTO
     * @return List<OrderSearchTckGetDto> 조회응답.
     */
    public ResponseObject<List<OrderSearchTckGetDto>> getOrderMainSearchTck(OrderSearchTckSetDto orderSearchSetDto) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOrderManagementInfoTck(orderSearchSetDto));
    }

    public ResponseObject<StoreShipMemoDto> getStoreShipMemoInfo(long purchaseOrderNo, long bundleNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getStoreShipMemoInfo(purchaseOrderNo, bundleNo));
    }

    public ResponseObject<StoreShipMsgDto> getShipMsgInfo(long purchaseOrderNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getStoreShipMsgInfo(purchaseOrderNo));
    }

    public ResponseObject<String> setStoreShipMemoInfo(StoreShipMemoDto memoDto){
        if(mapperService.setStoreShipMemoInfo(memoDto)){
            return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS);
        }
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_STORE_SHIP_SET_ERR01);
    }

    private ClaimForwardSetDto createShipChangeOmniDto(LinkedHashMap<String, Object> paramMap) {
        return ClaimForwardSetDto.builder()
            .transactionId("omni")
            .transactionUrl("/modifyRcptInfoFull")
            .httpMethod(HttpMethod.POST)
            .data(
                ShipChangeOmniSetDto.builder()
                    .dlvMsg(ObjectUtils.toString(paramMap.get("shipMsg")))
                    .rcptName(ObjectUtils.toString(paramMap.get("receiverNm")))
                    .rcptAddr(ObjectUtils.toString(paramMap.get("roadDetailAddr")))
                    .rcptPhone(ObjectUtils.toString(paramMap.get("shipMobileNo")))
                    .storeId(StringUtils.leftPad(ObjectUtils.toString(paramMap.get("origin_store_id")), 4, "0"))
                    .ordNo(ObjectUtils.toString(paramMap.get("purchase_store_info_no")))
                    .combineOrdNo(ObjectUtils.toString(paramMap.get("combine_purchase_store_info_no")))
                    .doorPasswordCode(ObjectUtils.toString(paramMap.get("auroraShipMsgCd")))
                    .doorPassword(ObjectUtils.toString(paramMap.get("auroraShipMsg")))
                    .build()
            )
            .build();
    }

    private boolean sendNoRcvMessage(Object tradeNo, Object shipMobileNo, Object displayMessage){
        LinkedHashMap<String, Object> messageMap = new LinkedHashMap<>();
        messageMap.put("claim_no", tradeNo);
        messageMap.put("buyer_mobile_no", shipMobileNo);
        messageMap.put("displayMessage", displayMessage);
        log.info("미수취 철회요청 발송 정보 :: {}", messageMap);
        return MessageSendUtil.sendNoRcvCancelRequest(messageMap);
    }

    /**
     * 주문 히스토리 내역 조회
     *
     * @param purchaseOrderNo
     * @return OrderHistoryListDto 히스토리 내
     */
    public ResponseObject<List<OrderHistoryListDto>> getOrderHistoryList(long purchaseOrderNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOrderHistoryList(purchaseOrderNo));
    }

    public ResponseObject<OrderCashReceiptDto> getOrderCashReceiptInfo(long purchaseOrderNo) {
        OrderCashReceiptDto responseDto = mapperService.getOrderCashReceiptInfo(purchaseOrderNo);
        if(responseDto != null){
            if(responseDto.getCashReceiptType().equals("PHONE")) {
                responseDto.setCashReceiptReqNo(PrivacyMaskingUtils.maskingPhone(responseDto.getCashReceiptReqNo()));
            } else if (responseDto.getCashReceiptType().equals("CARD")) {
                responseDto.setCashReceiptReqNo(PrivacyMaskingUtils.maskingCard(responseDto.getCashReceiptReqNo()));
            } else {
                Matcher matcher = Pattern.compile("(\\d{3})(\\d{2})(\\d{5,6})").matcher(responseDto.getCashReceiptReqNo());
                if(matcher.find()){
                    responseDto.setCashReceiptReqNo(matcher.group(1) + '-' + matcher.group(2) + '-' + matcher.group(3));
                }
            }
        }
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, responseDto);
    }

    public void setOrderModifyAddrHistory(OrderShipAddrInfoDto beforeOrderShipAddrInfo, OrderShipAddrEditDto shipAddrEditDto) throws Exception {

        //[배송지 이름] 다른 경우
        if(!StringUtils.isEmpty(beforeOrderShipAddrInfo.getReceiverNm()) && !StringUtils.isEmpty(shipAddrEditDto.getReceiverNm())){
            if(!beforeOrderShipAddrInfo.getReceiverNm().equalsIgnoreCase(shipAddrEditDto.getReceiverNm())){
                processHistoryUtills.setProcessHistory(shipAddrEditDto, "배송지-이름 변경", shipAddrEditDto.getReceiverNm(), beforeOrderShipAddrInfo.getReceiverNm(), shipAddrEditDto.getChgId());
            }
        }
        //[배송지 상세 주소] 다른 경우
        if(!beforeOrderShipAddrInfo.getRoadDetailAddr().equalsIgnoreCase(shipAddrEditDto.getRoadDetailAddr())){
            processHistoryUtills
                .setProcessHistory(shipAddrEditDto, "배송지-상세 주소 변경", shipAddrEditDto.getRoadDetailAddr(), beforeOrderShipAddrInfo.getRoadDetailAddr(), shipAddrEditDto.getChgId());
        }
        //[배송지 연락처] 다른 경우
        if(!beforeOrderShipAddrInfo.getShipMobileNo().equalsIgnoreCase(shipAddrEditDto.getShipMobileNo())){
            processHistoryUtills
                .setProcessHistory(shipAddrEditDto, "배송지-연락처 변경", shipAddrEditDto.getShipMobileNo(), beforeOrderShipAddrInfo.getShipMobileNo(), shipAddrEditDto.getChgId());
        }
        if(!beforeOrderShipAddrInfo.getAuroraShipMsg().equalsIgnoreCase(shipAddrEditDto.getAuroraShipMsg())){
            processHistoryUtills
                .setProcessHistory(shipAddrEditDto, "배송지-공통현 출입번호 변경", shipAddrEditDto.getAuroraShipMsg(), beforeOrderShipAddrInfo.getAuroraShipMsg(), shipAddrEditDto.getChgId());
        }
    }

    public ResponseObject<OrderShipOriginInfo> getOrderShipOriginInfo(long purchaseOrderNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getOriginPurchaseBundleInfo(purchaseOrderNo));
    }
}
