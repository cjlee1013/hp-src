package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DiscountInfoListDto {
    @ApiModelProperty(value = "할인번호", position = 1)
    private String orderDiscountNo;

    @ApiModelProperty(value = "종류", position = 2)
    private String additionTypeDetail;

    @ApiModelProperty(value = "할인적용대상", position = 3)
    private String discountTarget;

    @ApiModelProperty(value = "할인금액", position = 4)
    private String discountAmt;

}
