package kr.co.homeplus.escrowmng.claim.model.external;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "MHC 카드 정보")
public class MhcCardInfoDto {

    @ApiModelProperty(value = "카드발급유형 0021:마이홈플러스(신용) 0022:마이홈플러스(체크) 0023:마이홈플러스임직원(신용) 0024:마이홈플러스임직원(체크) 0030:마이홈플러스(일반) 1000:신한제휴카드 2000:삼성제휴카드 3000:KB제휴카드 9000:기타제휴카드")
    private String mhcCardType;

    @ApiModelProperty(value = "MHC ucid")
    private String mhcUcid;

    @ApiModelProperty(value = "MHC 대체카드번호 암호화")
    private String mhcCardnoEnc;

    @ApiModelProperty(value = "MHC 대체카드번호 HMAC")
    private String mhcCardnoHmac;

    @ApiModelProperty(value = "MHC 카드발급일자")
    private String mhcCardIssueDt;

}
