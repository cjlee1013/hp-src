package kr.co.homeplus.escrowmng.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "통계 판매업체 주문 정보 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class StatisticsPartnerOrderInfoDto {
    @ApiModelProperty(value = "판매업체명", position = 1)
    private String businessNm;

    @ApiModelProperty(value = "판매업체 ID", position = 2)
    private String partnerId;

    @ApiModelProperty(value = "판매업체코드(OF 밴더코드)", position = 3)
    private String ofVendorCd;

    @ApiModelProperty(value = "주문 건수", position = 4)
    private long orderQty;

    @ApiModelProperty(value = "주문 금액", position = 5)
    private long orderPrice;

    @ApiModelProperty(value = "취소 건수", position = 6)
    private long cancelQty;

    @ApiModelProperty(value = "취소 금액", position = 7)
    private long cancelPrice;

    @ApiModelProperty(value = "반품 건수", position = 8)
    private long returnQty;

    @ApiModelProperty(value = "반품 금액", position = 9)
    private long returnPrice;
}