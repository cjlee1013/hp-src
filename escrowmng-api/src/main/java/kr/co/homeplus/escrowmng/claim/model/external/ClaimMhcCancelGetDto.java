package kr.co.homeplus.escrowmng.claim.model.external;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "MHC 사용취소(부분) 응답 DTO")
public class ClaimMhcCancelGetDto {
    @ApiModelProperty(notes = "결과코드", required = true, position = 1)
    private String returnCode;
    @ApiModelProperty(notes = "결과메시지", required = true, position = 2)
    private String returnMessage;
    @ApiModelProperty(notes = "승인일자(취소)", required = true, position = 3)
    private String mhcAprDate;
    @ApiModelProperty(notes = "승인번호(취소)", required = true, position = 4)
    private String mhcAprNumber;
    @ApiModelProperty(notes = "MHC취소포인트", position = 5)
    private Long mhcCancelPoint;
}
