package kr.co.homeplus.escrowmng.escrow.service;

import java.util.List;
import kr.co.homeplus.escrowmng.escrow.mapper.OrderBatchMasterMapper;
import kr.co.homeplus.escrowmng.escrow.mapper.OrderBatchSlaveMapper;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchHistoryModel;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchHistorySelectModel;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchManageModel;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchManageSelectModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderBatchManageService {
    private final OrderBatchMasterMapper orderBatchMasterMapper;
    private final OrderBatchSlaveMapper orderBatchSlaveMapper;

    /**
     * 주문배치관리 목록조회
     * @param orderBatchManageSelectModel
     * @return
     */
    public List<OrderBatchManageModel> getOrderBatchManageList(OrderBatchManageSelectModel orderBatchManageSelectModel) {
        return orderBatchSlaveMapper.selectOrderBatchManageList(orderBatchManageSelectModel);
    }

    /**
     * 주문배치관리 저장
     * @param orderBatchManageModel
     * @return
     */
    public int saveOrderBatchManage(OrderBatchManageModel orderBatchManageModel) {
        return orderBatchMasterMapper.insertOrderBatchManage(orderBatchManageModel);
    }

    /**
     * 주문배치관리 삭제
     * @param orderBatchManageModel
     * @return
     */
    public int deleteOrderBatchManage(OrderBatchManageModel orderBatchManageModel) {
        return orderBatchMasterMapper.deleteOrderBatchManage(orderBatchManageModel);
    }

    /**
     * 주문배치이력 목록조회
     * @param orderBatchHistorySelectModel
     * @return
     */
    public List<OrderBatchHistoryModel> getOrderBatchHistoryList(OrderBatchHistorySelectModel orderBatchHistorySelectModel) {
        return orderBatchSlaveMapper.selectOrderBatchHistoryList(orderBatchHistorySelectModel);
    }

    /**
     * LGW 배치이력 목록조회 (프로시저 이력조회)
     * @param orderBatchHistorySelectModel
     * @return
     */
    public List<OrderBatchHistoryModel> getLgwBatchHistoryList(OrderBatchHistorySelectModel orderBatchHistorySelectModel) {
        return orderBatchSlaveMapper.selectLgwBatchHistoryList(orderBatchHistorySelectModel);
    }

    /**
     * [배치용] 주문배치이력 생성
     * @param orderBatchHistoryModel
     */
    public void createOrderBatchHistory(OrderBatchHistoryModel orderBatchHistoryModel) {
        orderBatchMasterMapper.createOrderBatchHistory(orderBatchHistoryModel);
    }

    /**
     * [배치용] 주문배치이력 수정
     * @param orderBatchHistoryModel
     */
    public void updateOrderBatchHistory(OrderBatchHistoryModel orderBatchHistoryModel) {
        orderBatchMasterMapper.updateOrderBatchHistory(orderBatchHistoryModel);
    }

    /**
     * [배치용] 주문배치정보 조회
     * @param batchType
     * @return
     */
    public OrderBatchManageModel selectOrderBatchInfo(String batchType) {
        return orderBatchSlaveMapper.selectOrderBatchInfo(batchType);
    }
}
