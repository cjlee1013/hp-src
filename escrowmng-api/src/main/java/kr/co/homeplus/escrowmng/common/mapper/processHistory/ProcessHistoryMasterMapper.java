package kr.co.homeplus.escrowmng.common.mapper.processHistory;


import kr.co.homeplus.escrowmng.common.model.processHistory.ProcessHistoryRegDto;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;

@EscrowMasterConnection
public interface ProcessHistoryMasterMapper {
    int insertProcessHistory(ProcessHistoryRegDto processHistoryRegDto);
}
