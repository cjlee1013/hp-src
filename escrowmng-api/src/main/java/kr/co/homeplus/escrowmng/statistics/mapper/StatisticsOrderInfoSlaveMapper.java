package kr.co.homeplus.escrowmng.statistics.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.statistics.model.StatisticsOrderInfoDto;
import kr.co.homeplus.escrowmng.statistics.model.StatisticsOrderInfoSelectDto;
import kr.co.homeplus.escrowmng.statistics.model.StatisticsPartnerOrderInfoDto;
import kr.co.homeplus.escrowmng.statistics.model.StatisticsStoreOrderInfoDto;
import kr.co.homeplus.escrowmng.statistics.model.StoreBasicInfoDto;
import org.apache.ibatis.annotations.Param;

@EscrowSlaveConnection
public interface StatisticsOrderInfoSlaveMapper {
    // 통계주문정보 랭킹 리스트 조회
    List<StatisticsOrderInfoDto> selectStatisticsOrderInfoRankingList(StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto);
    // 통계주문정보 점포상품 랭킹 리스트 조회
    List<StatisticsOrderInfoDto> selectStatisticsOrderInfoStoreItemRankingList(StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto);
    // 통계주문정보 점포별 리스트 조회
    List<StatisticsStoreOrderInfoDto> selectStatisticsOrderInfoListByStore(StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto);
    // 통계주문정보 판매업체별 리스트 조회
    List<StatisticsPartnerOrderInfoDto> selectStatisticsOrderInfoListByPartner(StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto);
    // 점포 기본정보 조회
    StoreBasicInfoDto selectStoreBasicInfo(@Param("storeId") int storeId);
}
