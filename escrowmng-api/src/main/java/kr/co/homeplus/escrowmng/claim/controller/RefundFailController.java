package kr.co.homeplus.escrowmng.claim.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListSetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteSetDto;
import kr.co.homeplus.escrowmng.claim.service.refundFail.RefundFailService;
import kr.co.homeplus.escrowmng.core.exception.LogicException;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/claim/refundFail")
@RequiredArgsConstructor
@Slf4j
@Api(tags = "주문관리 > 클레임 관리 > 환불실패대상 조회")
public class RefundFailController {

    private final RefundFailService refundFailService;

    @PostMapping("/getRefundFailList")
    @ApiOperation(value = "환불실패대상 리스트 조회")
    public ResponseObject<List<RefundFailListGetDto>> getRefundFailList(@RequestBody RefundFailListSetDto refundFailListSetDto) {
        return refundFailService.getRefundFailList(refundFailListSetDto);
    }

    @PostMapping("/reqRefundComplete")
    @ApiOperation(value = "환불완료 요청")
    public ResponseObject<RequestRefundCompleteGetDto> reqRefundComplete(@RequestBody RequestRefundCompleteSetDto requestRefundCompleteSetDto) throws LogicException {
        return refundFailService.reqRefundComplete(requestRefundCompleteSetDto);
    }

}
