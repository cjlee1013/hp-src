package kr.co.homeplus.escrowmng.external.service;

import java.util.List;
import kr.co.homeplus.escrowmng.core.exception.ExternalException;
import kr.co.homeplus.escrowmng.enums.ExceptionCode;
import kr.co.homeplus.escrowmng.external.mapper.ExternalDiscountUseInfoSlaveMapper;
import kr.co.homeplus.escrowmng.external.model.DiscountUseInfo;
import kr.co.homeplus.escrowmng.external.model.DiscountUseInfoSelect;
import kr.co.homeplus.escrowmng.utils.EscrowDate;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExternalDiscountUseInfoService {

    private final ExternalDiscountUseInfoSlaveMapper externalDiscountUseInfoSlaveMapper;

    /**
     * 카드상품할인 사용정보 조회
     * @param discountUseInfoSelect
     * @return
     */
    public List<DiscountUseInfo> getDiscountUseInfoList(DiscountUseInfoSelect discountUseInfoSelect) {
        int dateDiff = EscrowDate.compareDateTimeStr(discountUseInfoSelect.getEndDt() + " 00:00:00", discountUseInfoSelect.getStartDt() + " 00:00:00");
        if (dateDiff > 7) {
            throw new ExternalException(ExceptionCode.ERROR_CODE_1079, "최대 7일 이내만 조회 가능합니다.");
        }
        if (dateDiff < 0) {
            throw new ExternalException(ExceptionCode.ERROR_CODE_1079, "시작일이 종료일보다 클 수 없습니다.");
        }

        List<DiscountUseInfo> discountUseInfoList = externalDiscountUseInfoSlaveMapper.selectDiscountUseInfoList(discountUseInfoSelect);

        for (DiscountUseInfo discountUseInfo:discountUseInfoList) {
            discountUseInfo.setUserNm(PrivacyMaskingUtils.maskingUserName(discountUseInfo.getUserNm()));
        }

        return discountUseInfoList;
    }

}
