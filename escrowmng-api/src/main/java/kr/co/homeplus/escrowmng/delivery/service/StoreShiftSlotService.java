package kr.co.homeplus.escrowmng.delivery.service;

import static kr.co.homeplus.escrowmng.constants.Constants.RESULT_MSG_FAIL;
import static kr.co.homeplus.escrowmng.constants.Constants.RESULT_MSG_SUCCESS;

import java.util.HashMap;
import java.util.List;
import kr.co.homeplus.escrowmng.constants.Constants;
import kr.co.homeplus.escrowmng.delivery.mapper.StoreShiftSlotMasterMapper;
import kr.co.homeplus.escrowmng.delivery.mapper.StoreShiftSlotSlaveMapper;
import kr.co.homeplus.escrowmng.delivery.model.RemoteShipManageSelectModel;
import kr.co.homeplus.escrowmng.delivery.model.RemoteShipSlotModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreShiftManageModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreShiftManageSelectModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreSlotManageModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreSlotManageSelectModel;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StoreShiftSlotService {

    private final StoreShiftSlotSlaveMapper storeShiftSlotSlaveMapper;
    private final StoreShiftSlotMasterMapper storeShiftSlotMasterMapper;

    /**
     * 점포 shift 조회
     * @param storeShiftManageSelectModel
     * @return
     */
    public List<StoreShiftManageModel> getStoreShiftManageList(StoreShiftManageSelectModel storeShiftManageSelectModel) {
        return storeShiftSlotSlaveMapper.selectStoreShift(storeShiftManageSelectModel);
    }

    /**
     * 점포 slot 조회
     * @param storeSlotManageSelectModel
     * @return
     */
    public List<StoreSlotManageModel> getStoreSlotManageList(StoreSlotManageSelectModel storeSlotManageSelectModel) {
        return storeShiftSlotSlaveMapper.selectStoreSlot(storeSlotManageSelectModel);
    }

    /**
     * 점포 slot 사용여부 변경
     * @param storeSlotManageModelList
     * @return
     */
    public int saveStoreSlot(List<StoreSlotManageModel> storeSlotManageModelList) {

        for(StoreSlotManageModel storeSlotManageModel : storeSlotManageModelList) {
            storeShiftSlotMasterMapper.updateStoreSlot(storeSlotManageModel);
        }

        return Constants.ZERO;
    }

    /**
     * 원거리 배송 조회
     * @param remoteShipManageSelectModel
     * @return
     */
    public HashMap<String, Object> getRemoteShipList(RemoteShipManageSelectModel remoteShipManageSelectModel) {

        HashMap resultMap = new HashMap<String, Object>();

        // 1. 점포 slot 조회 ( 그리드 헤더 노출 )
        List<RemoteShipSlotModel> remoteShipManageSlotModelList =  storeShiftSlotSlaveMapper.selectRemoteShip(remoteShipManageSelectModel);

        // 2. 1.점포 slot 조회 결과 있으면 slot list 를 검색 param 설정 후 주소별 원거리 배송 여부 조회 함
        if (EscrowString.isNotEmpty(remoteShipManageSlotModelList)) {
            remoteShipManageSelectModel.setRemoteShipSlotModelList(remoteShipManageSlotModelList);
            // 우편번호 원거리 배송 가능 여부 리스트 조회
            List<HashMap<String, Object>> remoteShipSlotList = storeShiftSlotSlaveMapper.selectRemoteShipSlot(remoteShipManageSelectModel);

            if (EscrowString.isNotEmpty(remoteShipSlotList)) {
                resultMap.put("returnStatus", RESULT_MSG_SUCCESS);
                resultMap.put("storeSlotList", remoteShipManageSlotModelList);
                resultMap.put("storeRemoteShipSlotList", remoteShipSlotList);
            } else {
                resultMap.put("returnStatus", RESULT_MSG_FAIL);
            }
        } else {
            resultMap.put("returnStatus", RESULT_MSG_FAIL);
        }

        return resultMap;
    }
}
