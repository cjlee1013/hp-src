package kr.co.homeplus.escrowmng.external.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.external.model.RemoteShipManage;
import kr.co.homeplus.escrowmng.external.model.StoreShiftManage;
import kr.co.homeplus.escrowmng.external.model.StoreSlotManage;
import org.apache.ibatis.annotations.Param;

@EscrowMasterConnection
public interface ExternalStoreSlotMasterMapper {

    int insertStoreShift(@Param("shiftManageList") List<StoreShiftManage> shiftManageList, @Param("chgId") String chgId);
    int insertStoreSlot(@Param("slotManageList") List<StoreSlotManage> slotManageList, @Param("chgId") String chgId);
    int insertRemoteShip(@Param("remoteShipManageList") List<RemoteShipManage> remoteShipManageList, @Param("chgId") String chgId);
    int deleteRemoteShip(@Param("zipcodeList") List<String>zipcodeList, @Param("storeType") String storeType);
    int updateStoreSlotOrderCnt(StoreSlotManage slotManageList);

}