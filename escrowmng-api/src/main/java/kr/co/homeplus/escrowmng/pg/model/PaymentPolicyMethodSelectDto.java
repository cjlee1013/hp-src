package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 배분율 관리 > 결제정책 관리-결제수단 search DTO")
public class PaymentPolicyMethodSelectDto {
    @ApiModelProperty(notes = "정책번호", position = 1)
    private Long policyNo;

    @ApiModelProperty(notes = "사이트유형", required = true, position = 2)
    @NotEmpty(message = "사이트유형")
    private String siteType;
}
