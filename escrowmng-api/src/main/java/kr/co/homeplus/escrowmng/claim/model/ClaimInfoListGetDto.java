package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ClaimInfoListGetDto {

    @ApiModelProperty(value = "클레임타입", position = 1)
    public String claimType;

    @ApiModelProperty(value = "클레임상태", position = 2)
    public String claimStatus;

    @ApiModelProperty(value = "신청일자", position = 3)
    public String requestDt;

    @ApiModelProperty(value = "처리일시", position = 4)
    public String processDt;

    @ApiModelProperty(value = "승인일자", position = 5)
    public String approveDt;

    @ApiModelProperty(value = "철회일자", position = 6)
    public String withdrawDt;

    @ApiModelProperty(value = "거부일자", position = 7)
    public String rejectDt;

    @ApiModelProperty(value = "완료일자", position = 8)
    public String completeDt;

    @ApiModelProperty(value = "클레임번호", position = 9)
    public String claimNo;

    @ApiModelProperty(value = "주문번호", position = 10)
    public String purchaseOrderNo;

    @ApiModelProperty(value = "배송번호", position = 11)
    public String bundleNo;

    @ApiModelProperty(value = "상품주문번호", position = 12)
    public String orderItemNo;

    @ApiModelProperty(value = "상품번호", position = 13)
    public String itemNo;

    @ApiModelProperty(value = "상품명", position = 14)
    public String itemName;

    @ApiModelProperty(value = "신청수량", position = 15)
    public String claimItemQty;

    @ApiModelProperty(value = "신청채널")
    private String requestId;

    @ApiModelProperty(value = "클레임신청타입", position = 16)
    public String claimReasonType;

    @ApiModelProperty(value = "클레임신청사유", position = 17)
    public String claimReasonDetail;

    @ApiModelProperty(value = "보류일자", position = 18)
    public String pendingDt;

    @ApiModelProperty(value = "보류사유타입", position = 19)
    public String pendingReasonType;

    @ApiModelProperty(value = "보류사유", position = 20)
    public String pendingReasonDetail;

    @ApiModelProperty(value = "거부사유타입", position = 21)
    public String rejectReasonType;

    @ApiModelProperty(value = "거부사유", position = 22)
    public String rejectReasonDetail;

    @ApiModelProperty(value = "점포유형", position = 23)
    public String storeType;

    @ApiModelProperty(value = "판매업체ID",position = 24)
    private String partnerId;

    @ApiModelProperty(value = "판매자명", position = 25)
    public String partnerNm;

    @ApiModelProperty(value = "원천점포ID", position = 26)
    public String originStoreId;

    @ApiModelProperty(value = "점포ID", position = 27)
    public String storeId;

    @ApiModelProperty(value = "FC점포ID", position = 28)
    public String fcStoreId;

    @ApiModelProperty(value = "마켓연동", position = 29)
    public String marketType;

    @ApiModelProperty(value = "구매자회원번호", position = 30)
    public String userNo;

    @ApiModelProperty(value = "구매자이름", position = 31)
    public String buyerNm;

    @ApiModelProperty(value = "구매자연락처", position =32)
    private String buyerMobileNo;

    @ApiModelProperty(value = "그룹클레임번호", position = 33)
    private String claimBundleNo;

    @ApiModelProperty(value = "클레임타입코드", position = 34)
    private String claimTypeCode;

    @ApiModelProperty(value = "마켓주문번호", position = 35)
    private String marketOrderNo;

}
