package kr.co.homeplus.escrowmng.statistics.model;

import kr.co.homeplus.escrowmng.extract.model.ExtractCommonSetDto;
import lombok.Data;

@Data
public class CombineOrderStatusSelectDto {
    String schOrderStartDt;
    String schOrderEndDt;
    String schShipReqStartDt;
    String schShipReqEndDt;
    String schStoreType;
    Integer schStoreId;

    /* 추출 히스토리 저장용 DTO */
    ExtractCommonSetDto extractCommonSetDto;
}
