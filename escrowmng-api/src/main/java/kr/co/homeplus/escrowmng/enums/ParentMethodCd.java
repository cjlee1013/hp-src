package kr.co.homeplus.escrowmng.enums;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum ParentMethodCd {
    CARD("CARD","신용카드"),
    VBANK("VBANK","무통장"),
    RBANK("RBANK","실시간계좌이체"),
    PHONE("PHONE","휴대폰"),
    KAKAOPAY("KAKAOPAY","카카오카드"),
    KAKAOMONEY("KAKAOMONEY","카카오머니"),
    NAVERPAY("NAVERPAY", "네이버페이"),
    SAMSUNGPAY("SAMSUNGPAY", "삼성페이"),
    TOSS("TOSS", "토스"),
    PAYCO("PAYCO", "페이코"),
    EASYPAY("KAKAOPAY,KAKAOMONEY,NAVERPAY,SAMSUNGPAY,TOSS,PAYCO", "간편결제"),
    MKTNAVER("MKTNAVER", "마켓연동네이버")
    ;
    private String methodCd;
    private String methodNm;

    public static ParentMethodCd getPaymentMethod(String methodCd){
        for(ParentMethodCd method : ParentMethodCd.values()){
            if(method.methodCd.equals(methodCd)){
                return method;
            }
        }
        return null;
    }

    // 간편결제 결제수단 추출
    public static List<ParentMethodCd> getEasyPayMethodList() {
        return Arrays.stream(ParentMethodCd.values())
            .filter(parentMethodCd -> parentMethodCd.isEasyPay(parentMethodCd.methodCd))
            .collect(Collectors.toList());
    }
    private boolean isEasyPay(String methodCd) {
        if (ParentMethodCd.KAKAOPAY.methodCd.equals(methodCd)
            || ParentMethodCd.KAKAOMONEY.methodCd.equals(methodCd)
            || ParentMethodCd.NAVERPAY.methodCd.equals(methodCd)
            || ParentMethodCd.TOSS.methodCd.equals(methodCd)
            || ParentMethodCd.PAYCO.methodCd.equals(methodCd)
            || ParentMethodCd.SAMSUNGPAY.methodCd.equals(methodCd)) {
            return true;
        }
        return false;
    }
}