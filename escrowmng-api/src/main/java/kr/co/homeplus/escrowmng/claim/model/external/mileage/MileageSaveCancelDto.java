package kr.co.homeplus.escrowmng.claim.model.external.mileage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@ApiModel(description = "마일리지 적립 취소 요청")
public class MileageSaveCancelDto {
    @ApiModelProperty(value = "거래번호", position = 1)
    private String tradeNo;
    @ApiModelProperty(value = "취소타입(01:주문적립,02:페이백)", position = 2)
    private String cancelType;
    @ApiModelProperty(value = "상점타입", position = 3)
    private String storeType;
    @ApiModelProperty(value = "요청사유", position = 4)
    private String requestReason;
    @ApiModelProperty(value = "고객번호", position = 5)
    private String userNo;
    @ApiModelProperty(value = "취소마일리지금액", position = 6)
    private long mileageAmt;
    @ApiModelProperty(value = "요청자", position = 7)
    private String regId;
}
