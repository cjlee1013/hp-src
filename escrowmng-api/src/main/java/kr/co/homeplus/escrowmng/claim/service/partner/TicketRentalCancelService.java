package kr.co.homeplus.escrowmng.claim.service.partner;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerBoardCountGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListSetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ticketRental.TicketRentalCancelListGetDto;
import kr.co.homeplus.escrowmng.claim.service.ClaimProcessService;
import kr.co.homeplus.escrowmng.enums.OrderCode;
import kr.co.homeplus.escrowmng.order.factory.OrderResponseFactory;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class TicketRentalCancelService {

    private final TicketRentalCancelMapperService ticketRentalCancelService;

    private final ClaimProcessService claimProcessService;



    /**
     * PO 이티켓/렌탈 취소 클레임 건수
     *
     * @param partnerId
     * @param claimType
     * @return PO 클레임 신청/대기/완료 건수
     */
    public ResponseObject<ClaimPartnerBoardCountGetDto> getTicketRentalClaimBoardCount(String partnerId, String claimType) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, ticketRentalCancelService.getTicketRentalClaimBoardCount(partnerId, claimType));
    }

    /**
     * 이티켓/렌탈 취소 리스트 조회
     *
     * @param claimCancelListSetDto
     * @return 이티켓/렌탈 취소 리스트 조회
     */
    public ResponseObject<List<TicketRentalCancelListGetDto>> getTicketRentalCancelList(ClaimPartnerListSetDto claimCancelListSetDto) {

        List<TicketRentalCancelListGetDto> rentalCancelList = ticketRentalCancelService.getRentalCancelList(claimCancelListSetDto);

        if (ObjectUtils.isNotEmpty(rentalCancelList)) {
            for (TicketRentalCancelListGetDto next : rentalCancelList) {
                next.setBuyerNm(PrivacyMaskingUtils.maskingUserName(next.getBuyerNm()));
                next.setBuyerMobileNo(PrivacyMaskingUtils.maskingPhone(next.getBuyerMobileNo()));
                next.setReceiverNm(PrivacyMaskingUtils.maskingUserName(next.getReceiverNm()));
                next.setShipMobileNo(PrivacyMaskingUtils.maskingPhone(next.getShipMobileNo()));
            }
        }
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, rentalCancelList);
    }

}
