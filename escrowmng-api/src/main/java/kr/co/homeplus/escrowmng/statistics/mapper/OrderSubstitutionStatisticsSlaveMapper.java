package kr.co.homeplus.escrowmng.statistics.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.statistics.model.OrderSubstitutionStatisticsGetDto;
import kr.co.homeplus.escrowmng.statistics.model.OrderSubstitutionStatisticsModel;

@EscrowSlaveConnection
public interface OrderSubstitutionStatisticsSlaveMapper {
    List<OrderSubstitutionStatisticsGetDto> getOrderSubstitutionStatistics(OrderSubstitutionStatisticsModel orderSubstitutionStatisticsModel);
}
