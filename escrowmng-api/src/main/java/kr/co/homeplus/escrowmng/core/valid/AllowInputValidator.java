package kr.co.homeplus.escrowmng.core.valid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import kr.co.homeplus.escrowmng.utils.ValidUtil;

public class AllowInputValidator implements ConstraintValidator<AllowInput, String>{

    private String[] types;

    @Override
    public void initialize(AllowInput constraintAnnotation) {
        this.types = constraintAnnotation.types();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return ValidUtil.isAllowInput(s, types);
    }
}
