package kr.co.homeplus.escrowmng.escrow.service;

import java.util.List;
import kr.co.homeplus.escrowmng.escrow.model.BusinessDayManageModel;
import kr.co.homeplus.escrowmng.escrow.model.BusinessDayManageSelectModel;
import kr.co.homeplus.escrowmng.escrow.mapper.BusinessDayMasterMapper;
import kr.co.homeplus.escrowmng.escrow.mapper.BusinessDaySlaveMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BusinessDayManageService {

    private final BusinessDayMasterMapper businessDayMasterMapper;
    private final BusinessDaySlaveMapper businessDaySlaveMapper;


    /**
     * 영업일 리스트 조회 (월단위)
     * @param businessDayManageSelectModel
     * @return
     */
    public List<BusinessDayManageModel> getBusinessDayList(BusinessDayManageSelectModel businessDayManageSelectModel) {
        return businessDaySlaveMapper.getBusinessDayList(businessDayManageSelectModel);
    }

    /**
     * 영업일 등록
     * @param businessDayManageModel
     * @return
     */
    public BusinessDayManageModel saveBusinessDay(BusinessDayManageModel businessDayManageModel) {
        businessDayMasterMapper.insertBusinessDay(businessDayManageModel);
        return businessDayManageModel;
    }

    /**
     * 영업일 수정
     * @param businessDayManageModel
     * @return
     */
    public BusinessDayManageModel updateBusinessDay(BusinessDayManageModel businessDayManageModel) {
        // 영업일 타입 히스토리 저장
        businessDayMasterMapper.insertBusinessDayHistory(businessDayManageModel.getBusinessNo());
        // 영업일 타입 수정
        businessDayMasterMapper.updateBusinessDay(businessDayManageModel);

        return businessDayManageModel;
    }
}
