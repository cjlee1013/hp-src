package kr.co.homeplus.escrowmng.order.model.managementPop;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "쿠폰할인정보 DTO")
public class OrderCouponDiscountDto {
    @ApiModelProperty(value = "점포명", position = 1)
    private String storeNm;
    @ApiModelProperty(value = "할인번호", position = 2)
    private String couponNo;
    @ApiModelProperty(value = "종류", position = 3, hidden = true)
    private String discountKind;
    @ApiModelProperty(value = "종류명", position = 4)
    private String discountKindNm;
    @ApiModelProperty(value = "쿠폰명", position = 5)
    private String couponNm;
    @ApiModelProperty(value = "쿠폰정보", position = 6)
    private String couponInfo;
    @ApiModelProperty(value = "할인적용대상", position = 7)
    private String discountApplyTarget;
    @ApiModelProperty(value = "할인금액", position = 8)
    private long discountAmt;
    @ApiModelProperty(value = "사용가능기간", position = 9)
    private String discountCouponPeriod;
}
