package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단 관리 > 무이자 혜택 관리 DTO")
public class FreeInterestDto {
    @ApiModelProperty(value = "카드무이자관리순번")
    private Long freeInterestMngSeq;
    @ApiModelProperty(value = "사이트유형(HOME:홈플러스,CLUB:더클럽)")
    private String siteType;
    @ApiModelProperty(value = "무이자관리명")
    private String freeInterestNm;
    @ApiModelProperty(value = "적용시작일자")
    private String applyStartDt;
    @ApiModelProperty(value = "적용종료일자")
    private String applyEndDt;
    @ApiModelProperty(value = "사용여부")
    private String useYn;
    @ApiModelProperty(value = "적용순번")
    private long applySeq;
    @ApiModelProperty(value = "수단코드(카드사 별 카드종류)")
    private String methodCd;
    @ApiModelProperty(value = "결제금액(최대값:999만원)")
    private int paymentAmt;
    @ApiModelProperty(value = "무이자유형(무이자:N,부분무이자:P)")
    private String freeInterestType;
    @ApiModelProperty(value = "할부기간")
    private String installmentTerm;
    @ApiModelProperty(value = "등록자id")
    private String regId;
    @ApiModelProperty(value = "등록일자")
    private String regDt;
    @ApiModelProperty(value = "수정자id")
    private String chgId;
    @ApiModelProperty(value = "수정일자")
    private String chgDt;
}
