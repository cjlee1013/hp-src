package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "클레임상세정보 - 차감 행사정보 팝업")
public class PromoInfoListDto {
    @ApiModelProperty(value = "상품번호", position = 1)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 2)
    private String itemName;

    @ApiModelProperty(value = "행사종류", position = 3)
    private String promoType;

    @ApiModelProperty(value = "행사번호", position = 4)
    private String promoNo;

    @ApiModelProperty(value = "행사할인금액", position = 5)
    private String promoDiscountAmt;
}
