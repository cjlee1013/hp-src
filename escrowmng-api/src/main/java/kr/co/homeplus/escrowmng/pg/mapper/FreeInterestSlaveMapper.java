package kr.co.homeplus.escrowmng.pg.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.pg.model.FreeInterestApplyDto;
import kr.co.homeplus.escrowmng.pg.model.FreeInterestDto;
import kr.co.homeplus.escrowmng.pg.model.FreeInterestSelectDto;
import org.apache.ibatis.annotations.Param;

@EscrowSlaveConnection
public interface FreeInterestSlaveMapper {
    List<FreeInterestDto> selectFreeInterestMngList(FreeInterestSelectDto freeInterestSelectDto);
    List<FreeInterestApplyDto> selectFreeInterestApplyList(@Param("freeInterestMngSeq") long freeInterestMngSeq);
    List<FreeInterestDto> selectFreeInterestList(@Param("applyStartDt") String applyStartDt, @Param("applyEndDt") String applyEndDt,
        @Param("siteType") String siteType, @Param("methodCd") String methodCd,
        @Param("freeInterestNm") String freeInterestNm, @Param("useYn") String useYn);
}
