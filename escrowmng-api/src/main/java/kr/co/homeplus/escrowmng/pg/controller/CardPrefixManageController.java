package kr.co.homeplus.escrowmng.pg.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.pg.model.CardPrefixManageDto;
import kr.co.homeplus.escrowmng.pg.model.CardPrefixManageSelectDto;
import kr.co.homeplus.escrowmng.pg.service.CardPrefixManageService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pg/paymentMethod")
@Api(tags = "결제관리 > 결제수단 관리 > 카드 PREFIX 관리")
public class CardPrefixManageController {

    private final CardPrefixManageService cardPrefixManageService;

    /**
     * 카드 PREFIX 리스트 조회
     * @param cardPrefixManageSelectDto
     * @return
     * @throws Exception
     */
    @GetMapping("/getCardPrefixManageList")
    @ApiOperation(value = "카드 PREFIX 조회", response = CardPrefixManageDto.class)
    public ResponseObject getCardPrefixManageList(@Valid CardPrefixManageSelectDto cardPrefixManageSelectDto) throws Exception{
        List<CardPrefixManageDto> cardPrefixManageDtoList = cardPrefixManageService.getCardPrefixManageList(cardPrefixManageSelectDto);
        return ResourceConverter.toResponseObject(cardPrefixManageDtoList);
    }

    /**
     * 카드 PREFIX 저장/수정
     * @param cardPrefixManageDto
     * @return
     * @throws Exception
     */
    @PostMapping("/saveCardPrefixManage")
    @ApiOperation(value = "카드 PREFIX 저장/수정", response = Integer.class)
    public ResponseObject<Integer> saveCardPrefixManage(@Valid @RequestBody CardPrefixManageDto cardPrefixManageDto) throws Exception {
        return ResourceConverter.toResponseObject(cardPrefixManageService.saveCardPrefixManage(cardPrefixManageDto));
    }

    /**
     * 카드 PREFIX 결제수단 일괄변경
     * @param cardPrefixManageDto
     * @return
     * @throws Exception
     */
    @PostMapping("/saveBundleCardPrefixMethod")
    @ApiOperation(value = "카드 PREFIX 결제수단 일괄변경", response = Integer.class)
    public ResponseObject<Integer> saveBundleCardPrefixMethod(@Valid @RequestBody CardPrefixManageDto cardPrefixManageDto) throws Exception {
        return ResourceConverter.toResponseObject(cardPrefixManageService.saveBundleCardPrefixMethod(cardPrefixManageDto));
    }
}