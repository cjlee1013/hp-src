package kr.co.homeplus.escrowmng.claim.service;

import java.util.LinkedHashMap;

public interface ClaimMessageService {
    LinkedHashMap<String, Object> getClaimBasicMessageInfo(long claimNo);

    LinkedHashMap<String, Object> getClaimPaymentMessageInfo(long claimNo);

    LinkedHashMap<String, Object> getOrderSubstitutionMessageInfo(long claimNo);

    LinkedHashMap<String, Object> getPresentAutoCancelInfo(long claimNo);
}
