package kr.co.homeplus.escrowmng.statistics.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowBatchConnection;
import kr.co.homeplus.escrowmng.statistics.model.CombineOrderStatusDto;
import kr.co.homeplus.escrowmng.statistics.model.CombineOrderStatusSelectDto;

@EscrowBatchConnection
public interface CombineOrderStatusSlaveMapper {
    List<CombineOrderStatusDto> selectCombOrderStatistics(CombineOrderStatusSelectDto combineOrderStatusSelectDto);
}
