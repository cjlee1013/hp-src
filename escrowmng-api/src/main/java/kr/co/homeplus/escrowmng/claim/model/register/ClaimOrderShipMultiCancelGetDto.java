package kr.co.homeplus.escrowmng.claim.model.register;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "일괄취소 결과.")
public class ClaimOrderShipMultiCancelGetDto {
    private long successCnt;
    private long failCnt;

    public ClaimOrderShipMultiCancelGetDto(long successCnt, long failCnt){
        this.successCnt = successCnt;
        this.failCnt = failCnt;
    }
}
