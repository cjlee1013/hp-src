package kr.co.homeplus.escrowmng.message.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.Getter;

public enum ShipStatus {

    NN("대기"),
    D1("결제완료"),
    D2("상품준비중"),
    D3("배송중"),
    D4("배송완료"),
    D5("구매확정");

    @Getter
    private final String status;

    ShipStatus(final String status) {
        this.status = status;
    }

    private static final Map<String, String> LOOK_UP_MAP = Stream
        .of(values())
        .collect(Collectors.toMap(ShipStatus::getStatus, ShipStatus::name));

    private static ShipStatus of(String status) {
        return ShipStatus.valueOf(LOOK_UP_MAP.get(status));
    }
}
