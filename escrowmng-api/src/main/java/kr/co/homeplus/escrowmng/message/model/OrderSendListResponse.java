package kr.co.homeplus.escrowmng.message.model;

import java.util.List;
import lombok.Data;

@Data
public class OrderSendListResponse {

    private List<OrderSendListInfo> list;
}
