package kr.co.homeplus.escrowmng.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimMessageInfo {

    CLAIM_MESSAGE_HOME_C_ORDER_COMPLETE("T11101", "S11101", "클레임_접수_전체주문취소,주문취소", "대표상품명 외 n건|환불금액", "item_name|refund_amt"),
    CLAIM_MESSAGE_HOME_C_ORDER_APPLICATION("T11102", "S11102", "클레임_접수_취소신청", "대표상품명 외 n건", "item_name"),
    CLAIM_MESSAGE_HOME_RX_ORDER_APPLICATION_TD("T11103", "S11103", "클레임_접수_반품/교환_TD_자차", "클레임구분|대표 상품명 외 n건|수거날짜|수거시간", "claim_type|item_name|ship_dt|slot_ship_time"),
    CLAIM_MESSAGE_HOME_RX_ORDER_APPLICATION_TD_ENCLOSE("T11104", "S11104", "클레임_접수_반품/교환_TD_자차_배송비동봉", "클레임구분|대표 상품명 외 n건|수거날짜|수거시간|반품/교환 배송비", "claim_type|item_name|ship_dt|slot_ship_time|claim_ship_amt"),
    CLAIM_MESSAGE_HOME_RX_ORDER_APPLICATION_PICK("T11105", "S11105", "클레임_접수_반품/교환_TD_픽업", "대표 상품명 외 n건|픽업점포명|클레임구분", "item_name|partner_nm|claim_type"),
    CLAIM_MESSAGE_HOME_RX_ORDER_APPLICATION_DS("T11106", "S11106", "클레임_접수_반품_DC/DS", "대표 상품명 외 n건|클레임구분", "item_name|claim_type"),
    CLAIM_MESSAGE_HOME_RX_ORDER_APPLICATION_DS_ENCLOSE("T11107", "S11107", "클레임_접수_반품/교환_DC/DS_배송비동봉", "대표 상품명 외 n건|클레임구분|반품/교환 배송비", "item_name|claim_type|claim_ship_amt"),
    CLAIM_MESSAGE_HOME_T11108("T11108", "S11108", "클레임_장기미배송_취소_DS", "주문날짜|대표상품명 외 n건|환불금액", "order_dt|item_name|refund_amt"),
    CLAIM_MESSAGE_HOME_T11109("T11109", "S11109", "클레임_장기미회수_반품철회_DS", "클레임구분|요청일|대표상품명 외 n건", "claim_type|reg_dt|item_name"),
    CLAIM_MESSAGE_HOME_T11042("T11042", "S11042", "클레임_수거_수거시간_알림", "클레임구분|수거시간", "claim_type|slot_ship_time"),
    CLAIM_MESSAGE_HOME_X_SHIP_START_DS("T11110", "S11110", "클레임_배송_교환배송안내", "대표상품명 외 n건", "item_name"),
    CLAIM_MESSAGE_HOME_T11111("T11111", "S11111", "클레임_환불_환불실패_부결제수단", "대표상품명 외 n건", "item_name"),
    CLAIM_MESSAGE_HOME_CR_COMPLETE("T11112", "S11112", "클레임_완료_취소/반품완료", "클레임구분|대표상품명 외 n건|환불금액", "claim_type|item_name|refund_amt"),
    CLAIM_MESSAGE_HOME_X_COMPLETE("T11113", "S11113", "클레임_완료_교환완료", "대표상품명 외 n건", "item_name"),
    CLAIM_MESSAGE_HOME_OOS_NOT_SUB("T11114", "S11114", "클레임_접수_주문취소_대체미선택_품절(결품)", "주문번호|상품명|수량|주문금액", "purchase_order_no|item_name|item_qty|refund_amt"),
    CLAIM_MESSAGE_HOME_OOS_SUB("T11115", "S11115", "클레임_접수_주문취소_대체선택_품절(결품)", "주문번호|상품명|수량|주문금액", "purchase_order_no|item_name|item_qty|refund_amt"),
    CLAIM_MESSAGE_HOME_SUBSTITUTION_NOT_PRICE("T11138", "S11138", "대체주문_품절(결품)", "주문번호|상품명|수량|대체상품명|대체수량", "purchase_order_no|item_name|item_qty|sub_item_name|sub_item_qty"),
    CLAIM_MESSAGE_HOME_SUBSTITUTION_PRICE("T11139", "S11139", "대체주문_품절(결품)_환불금액발생", "주문번호|상품명|수량|대체상품명|대체수량|환불금액", "purchase_order_no|item_name|item_qty|sub_item_name|sub_item_qty|refund_amt"),
    CLAIM_MESSAGE_HOME_PRESENT_CANCEL_RECEIVER("T11124", "S11124", "주문_선물하기_결제취소_받는사람", "받는 고객명|보낸 고객명|대표상품명 외 n건", "receiver_nm|sender_nm|item_name"),
    CLAIM_MESSAGE_HOME_AUTO_CANCEL_SENDER("T11125", "S11125", "주문_선물하기_미수락_보낸사람", "받는 고객명|대표상품명 외 n건|배송기한 입력날짜|결제금액", "receiver_nm|item_name|receive_dt|refund_amt"),
    CLAIM_MESSAGE_HOME_AUTO_CANCEL_RECEIVER("T11126", "S11126", "주문_선물하기_입력기한초과_받는사람", "보낸 고객명|대표상품명 외 n건", "sender_nm|item_name"),
    CLAIM_MESSAGE_HOME_NO_RCV_CANCEL_REQUEST("T11099", "S11099", "배송_미수취신고 철회요청", "bo에서 작성한 메시지 노출", "displayMessage"),

    CLAIM_MESSAGE_CLUB_C_ORDER_COMPLETE("T20045", "S20045", "더클럽_클레임_접수_전체주문취소,주문취소", "대표상품명 외 n건|환불금액", "item_name|refund_amt"),
    CLAIM_MESSAGE_CLUB_C_ORDER_APPLICATION("T20046", "S20046", "더클럽_클레임_접수_취소신청", "대표상품명 외 n건", "item_name"),
    CLAIM_MESSAGE_CLUB_RX_ORDER_APPLICATION_TD("T20047", "S20047", "더클럽_클레임_접수_반품/교환_TD_자차", "클레임구분|대표 상품명 외 n건|수거날짜, 시간", "claim_type|item_name|club_ship_info"),
    CLAIM_MESSAGE_CLUB_RX_ORDER_APPLICATION_TD_ENCLOSE("T20048", "S20048", "더클럽_클레임_접수_반품/교환_TD_자차_배송비동봉", "클레임구분|대표 상품명 외 n건|수거날짜, 시간|반품/교환 배송비", "claim_type|item_name|club_ship_info|claim_ship_amt"),
    CLAIM_MESSAGE_CLUB_RX_ORDER_APPLICATION_PICK("T20067", "S20067", "클레임_접수_반품/교환_TD_픽업", "대표 상품명 외 n건|픽업점포명|클레임구분", "item_name|partner_nm|claim_type"),
    CLAIM_MESSAGE_CLUB_X_SHIP_START_DS("T20049", "S20049", "더클럽_클레임_배송_교환배송안내", "대표상품명 외 n건", "item_name"),
    CLAIM_MESSAGE_CLUB_REFUND_FAIL("T20050", "S20050", "더클럽_클레임_환불_환불실패", "대표상품명 외 n건", "item_name"),
    CLAIM_MESSAGE_CLUB_CR_COMPLETE("T20051", "S20051", "더클럽_클레임_완료_취소/반품완료", "클레임구분|대표상품명 외 n건|주문금액", "claim_type|item_name|refund_amt"),
    CLAIM_MESSAGE_CLUB_X_COMPLETE("T20052", "S20052", "더클럽_클레임_완료_교환완료", "대표상품명 외 n건", "item_name"),
    CLAIM_MESSAGE_CLUB_OOS_NOT_SUB("T20053", "S20053", "더클럽_클레임_접수_주문취소_품절(결품)", "주문번호|상품명|수량|주문금액", "purchase_order_no|item_name|item_qty|refund_amt"),
    CLAIM_MESSAGE_CLUB_OOS_SUB("T20063", "S20063", "더클럽_클레임_접수_주문취소_대체선택_품절(결품)", "주문번호|상품명|수량|주문금액", "purchase_order_no|item_name|item_qty|refund_amt"),
    CLAIM_MESSAGE_CLUB_SUBSTITUTION_NOT_PRICE("T20064", "S20064", "더클럽_대체주문_품절(결품)", "주문번호|상품명|수량|대체상품명|대체수량", "purchase_order_no|item_name|item_qty|sub_item_name|sub_item_qty"),
    CLAIM_MESSAGE_CLUB_SUBSTITUTION_PRICE("T20065", "S20065", "더클럽_대체주문_품절(결품)_환불금액발생", "주문번호|상품명|수량|대체상품명|대체수량|환불금액", "purchase_order_no|item_name|item_qty|sub_item_name|sub_item_qty|refund_amt"),
    CLAIM_MESSAGE_CLUB_PRESENT_CANCEL_RECEIVER("T20059", "S20059", "더클럽_주문_선물하기_결제취소_받는사람", "받는 고객명|보낸 고객명|대표상품명 외 n건", "receiver_nm|sender_nm|item_name"),
    CLAIM_MESSAGE_CLUB_AUTO_CANCEL_SENDER("T20060", "S20060", "더클럽_주문_선물하기_미수락_보낸사람", "받는 고객명|대표상품명 외 n건|배송기한 입력날짜|결제금액", "receiver_nm|item_name|receive_dt|refund_amt"),
    CLAIM_MESSAGE_CLUB_AUTO_CANCEL_RECEIVER("T20061", "S20061", "더클럽_주문_선물하기_입력기한초과_받는사람", "보낸 고객명|대표상품명 외 n건", "sender_nm|item_name"),

    CLAIM_MESSAGE_EXP_C_ORDER_COMPLETE("T30003", "S30003", "익스_클레임_취소완료", "대표상품명 외 n건|주문금액", "item_name|refund_amt"),
    CLAIM_MESSAGE_EXP_OOS_NOT_SUB("T30004", "S30004", "익스_클레임_접수_주문취소_품절(결품)", "주문번호|상품명|수량|주문금액", "purchase_order_no|item_name|item_qty|refund_amt"),
    CLAIM_MESSAGE_EXP_RX_ORDER_APPLICATION_TD("T30005", "S30005", "익스_클레임_접수_반품/교환_TD_자차", "클레임구분|대표 상품명 외 n건", "claim_type|item_name"),
    CLAIM_MESSAGE_EXP_RX_ORDER_APPLICATION_DS_ENCLOSE("T30009", "S30009", "클레임_접수_반품/교환_DC/DS_배송비동봉", "대표 상품명 외 n건|클레임구분|반품/교환 배송비", "item_name|claim_type|claim_ship_amt"),
    CLAIM_MESSAGE_EXP_REFUND_FAIL("T30006", "S30006", "익스_클레임_환불_환불실패", "대표상품명 외 n건", "item_name"),
    CLAIM_MESSAGE_EXP_CR_COMPLETE("T30007", "S30007", "익스_클레임_완료_반품완료", "클레임구분|대표상품명 외 n건|환불금액", "claim_type|item_name|refund_amt"),
    CLAIM_MESSAGE_EXP_X_COMPLETE("T30008", "S30008", "익스_클레임_완료_교환완료", "대표상품명 외 n건", "item_name"),

    CLAIM_MESSAGE_MARKET_OOS_NOT_SUB("T11241", "S11241", "[알림톡]마켓_클레임_접수_주문취소_대체미선택_품절(결품)", "상품명|수량|주문금액", "item_name|item_qty|refund_amt"),
    CLAIM_MESSAGE_MARKET_OOS_SUB("T11242", "S11242", "[알림톡]마켓_클레임_접수_주문취소_대체선택_품절(결품)", "상품명|수량|주문금액", "item_name|item_qty|refund_amt"),
    CLAIM_MESSAGE_MARKET_SUBSTITUTION_NOT_PRICE("T11243", "S11243", "[알림톡]대체주문_품절(결품)", "상품명|수량|대체상품명|대체수량", "item_name|item_qty|sub_item_name|sub_item_qty"),
    ;

    @Getter
    private final String templateCode;

    @Getter
    private final String switchingTemplateCode;

    @Getter
    private final String sendTitle;

    @Getter
    private final String sendParams;

    @Getter
    private final String mappingParams;
}
