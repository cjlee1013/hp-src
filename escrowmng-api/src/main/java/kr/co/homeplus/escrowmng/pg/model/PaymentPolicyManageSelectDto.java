package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단관리 > 결제정책관리 search DTO")
public class PaymentPolicyManageSelectDto {
    @ApiModelProperty(notes = "조회기간유형", required = true, position = 1)
    private String schPeriod;

    @ApiModelProperty(notes = "조회시작일자", required = true, position = 2)
    @NotEmpty(message = "조회시작일자")
    private String schFromDt;

    @ApiModelProperty(notes = "조회종료일자", required = true, position = 3)
    @NotEmpty(message = "조회종료일자")
    private String schEndDt;

    @ApiModelProperty(notes = "사이트유형", position = 4)
    private String schSiteType;

    @ApiModelProperty(notes = "사용여부", position = 5)
    private String schUseYn;

    @ApiModelProperty(notes = "대분류코드", position = 6)
    private String schLcateCd;

    @ApiModelProperty(notes = "중분류코드", position = 7)
    private String schMcateCd;

    @ApiModelProperty(notes = "소분류코드", position = 8)
    private String schScateCd;

    @ApiModelProperty(notes = "세분류코드", position = 9)
    private String schDcateCd;

    @ApiModelProperty(notes = "카테고리코드", position = 10)
    private String schCateCd;

    @ApiModelProperty(notes = "검색유형", position = 11)
    private String schType;

    @ApiModelProperty(notes = "검색어", position = 12)
    private String schKeyword;

    @ApiModelProperty(notes = "카테고리리스트", position = 13)
    private List<String> categoryList;
}
