package kr.co.homeplus.escrowmng.pg.model;

import lombok.Data;

@Data
public class ExcelItem {
    private String itemNo;
    private String itemNm;
}
