package kr.co.homeplus.escrowmng.order.sql;

import kr.co.homeplus.escrowmng.enums.MysqlFunction;
import kr.co.homeplus.escrowmng.order.constants.CustomConstants;
import kr.co.homeplus.escrowmng.order.factory.EntityFactory;
import kr.co.homeplus.escrowmng.order.model.entity.BundleEntity;
import kr.co.homeplus.escrowmng.order.model.entity.EmployeeInfoEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderItemEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderPickupInfoEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PurchaseOrderEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PurchaseStoreInfoEntity;
import kr.co.homeplus.escrowmng.order.model.entity.SafetyIssueEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingAddrEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingItemEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingNonRcvEntity;
import kr.co.homeplus.escrowmng.order.model.entity.StoreSlotMngEntity;
import kr.co.homeplus.escrowmng.order.model.entity.multi.MultiBundleEntity;
import kr.co.homeplus.escrowmng.order.model.entity.multi.MultiOrderItemEntity;
import kr.co.homeplus.escrowmng.order.model.entity.multi.MultiSafetyIssueEntity;
import kr.co.homeplus.escrowmng.order.model.entity.multi.MultiShippingAddrEntity;
import kr.co.homeplus.escrowmng.order.model.ship.MultiOrderShipAddrEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipInfoSetDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipSetGto;
import kr.co.homeplus.escrowmng.order.model.ship.OrderShipAddrEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.StoreShipMemoDto;
import kr.co.homeplus.escrowmng.utils.ObjectUtils;
import kr.co.homeplus.escrowmng.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class OrderShipSql {

    public static String selectOrderShipAddrInfo(@Param("bundleNo") long bundleNo ) {
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        PurchaseStoreInfoEntity purchaseStoreInfo = EntityFactory.createEntityIntoValue(PurchaseStoreInfoEntity.class, Boolean.TRUE);
        StoreSlotMngEntity storeSlotMng = EntityFactory.createEntityIntoValue(StoreSlotMngEntity.class, Boolean.TRUE);
        SafetyIssueEntity safetyIssue = EntityFactory.createEntityIntoValue(SafetyIssueEntity.class, "safety");
        OrderPickupInfoEntity orderPickupInfo = EntityFactory.createEntityIntoValue(OrderPickupInfoEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                shippingItem.shipNo,
                shippingItem.bundleNo,
                shippingAddr.receiverNm,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.storeId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "store_nm"
                ),
                shippingAddr.shipMobileNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(shippingAddr.safetyUseYn, "Y"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE_WHEN,
                            safetyIssue.issueStatus,
                            ObjectUtils.toArray(
                                "'R'", "'발급 요청중'",
                                "'C'", safetyIssue.issuePhoneNo,
                                "'F'", SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(safetyIssue.issueType, "N"), "'발급실패'", "'변경실패'"))
                            )
                        ),
                        "'신청안함'"
                    ),
                    "safety_nm"
                ),
                shippingAddr.safetyUseYn,
                shippingAddr.zipcode,
                shippingAddr.roadBaseAddr,
                shippingAddr.roadDetailAddr,
                shippingAddr.baseAddr,
                shippingAddr.detailAddr,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray( SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")), shippingAddr.storeShipMsgCd, shippingAddr.dlvShipMsgCd ),
                    "ship_msg_cd"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray( SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")), shippingAddr.storeShipMsg, shippingAddr.dlvShipMsg ),
                    "ship_msg"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, shippingAddr.auroraShipMsgCd, SqlUtils.nonAlias(shippingAddr.auroraShipMsgCd)),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, shippingAddr.auroraShipMsg, SqlUtils.nonAlias(shippingAddr.auroraShipMsg)),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, storeSlotMng.shipDt),
                                storeSlotMng.shipDt,
                                orderPickupInfo.shipDt
                            )
                        ),
                        CustomConstants.YYYYMMDD
                    ),
                    "slotShipDt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CONCAT,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.LEFT, ObjectUtils.toArray(storeSlotMng.slotShipStartTime, 2)),
                        SqlUtils.appendSingleQuote(CustomConstants.COLON),
                        SqlUtils.sqlFunction(MysqlFunction.RIGHT, ObjectUtils.toArray(storeSlotMng.slotShipStartTime, 2))
                    ),
                    SqlUtils.nonAlias(storeSlotMng.slotShipStartTime)
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CONCAT,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.LEFT, ObjectUtils.toArray(storeSlotMng.slotShipEndTime, 2)),
                        SqlUtils.appendSingleQuote(CustomConstants.COLON),
                        SqlUtils.sqlFunction(MysqlFunction.RIGHT, ObjectUtils.toArray(storeSlotMng.slotShipEndTime, 2))
                    ),
                    SqlUtils.nonAlias(storeSlotMng.slotShipEndTime)
                ),
                purchaseStoreInfo.slotId,
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, "'M'"), "ship_type"),
                orderPickupInfo.placeNm,
                orderPickupInfo.lockerNo,
                orderPickupInfo.lockerPasswd,
                orderPickupInfo.startTime,
                orderPickupInfo.endTime,
                shippingAddr.personalOverseaNo,
                orderItem.purchaseOrderNo,
                purchaseOrder.orderType,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(purchaseStoreInfo.anytimeSlotYn, "'N'"), purchaseStoreInfo.anytimeSlotYn)
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition( orderItem, ObjectUtils.toArray(shippingItem.orderItemNo) )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition( purchaseOrder, ObjectUtils.toArray(orderItem.purchaseOrderNo) )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition( shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo) )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    purchaseStoreInfo,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(SqlUtils.equalColumnByInput(purchaseStoreInfo.shipMethod, "TD_DRCT"))
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(storeSlotMng, ObjectUtils.toArray(purchaseStoreInfo.shipDt, purchaseStoreInfo.storeId, purchaseStoreInfo.slotId))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(safetyIssue, "safety"),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(shippingItem.shipAddrNo, orderItem.purchaseOrderNo, shippingItem.bundleNo ), safetyIssue)
                    )
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(orderPickupInfo, ObjectUtils.toArray(orderItem.purchaseOrderNo,shippingItem.bundleNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo)
            );

        StringBuilder sb = new StringBuilder();
        sb.append(query.toString());
        sb.append("\n LIMIT 0, 1");

        log.debug("selectOrderShipAddrInfo ::: \n[{}]", sb.toString());
        return sb.toString();
    }

    public static String selectMultiOrderShipAddrInfo(@Param("multiBundleNo") long multiBundleNo ) {
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        MultiBundleEntity multiBundle = EntityFactory.createEntityIntoValue(MultiBundleEntity.class, Boolean.TRUE);
        MultiSafetyIssueEntity multiSafetyIssue = EntityFactory.createEntityIntoValue(MultiSafetyIssueEntity.class, "safety");
        MultiShippingAddrEntity multiShippingAddr = EntityFactory.createEntityIntoValue(MultiShippingAddrEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                multiBundle.multiBundleNo,
                multiShippingAddr.receiverNm,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(multiBundle.storeId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "store_nm"
                ),
                multiShippingAddr.shipMobileNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(multiShippingAddr.safetyUseYn, "Y"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE_WHEN,
                            multiSafetyIssue.issueStatus,
                            ObjectUtils.toArray(
                                "'R'", "'발급 요청중'",
                                "'C'", multiSafetyIssue.issuePhoneNo,
                                "'F'", SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(multiSafetyIssue.issueType, "N"), "'발급실패'", "'변경실패'"))
                            )
                        ),
                        "'신청안함'"
                    ),
                    "safety_nm"
                ),
                multiShippingAddr.safetyUseYn,
                multiShippingAddr.zipcode,
                multiShippingAddr.roadBaseAddr,
                multiShippingAddr.roadDetailAddr,
                multiShippingAddr.baseAddr,
                multiShippingAddr.detailAddr,
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, "'M'"), "ship_type"),
                orderItem.purchaseOrderNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(multiBundle))
            .INNER_JOIN(SqlUtils.createJoinCondition( multiOrderItem, ObjectUtils.toArray(multiBundle.multiBundleNo) ))
            .INNER_JOIN(SqlUtils.createJoinCondition( orderItem, ObjectUtils.toArray(multiOrderItem.orderItemNo) ))
            .INNER_JOIN(SqlUtils.createJoinCondition( multiShippingAddr, ObjectUtils.toArray(multiBundle.multiBundleNo) ))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(multiSafetyIssue, "safety"),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(multiShippingAddr.multiShipAddrNo, multiShippingAddr.purchaseOrderNo, multiShippingAddr.multiBundleNo ), multiSafetyIssue)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(multiBundle.multiBundleNo, multiBundleNo)
            );

        StringBuilder sb = new StringBuilder();
        sb.append(query.toString());
        sb.append("\n LIMIT 0, 1");

        log.debug("selectOrderShipAddrInfo ::: \n[{}]", sb.toString());
        return sb.toString();
    }

    public static String selectOrderShipInfo(@Param("bundleNo") long bundleNo, @Param("shipStatus") String shipStatus) {
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                bundle.bundleNo,
                shippingItem.shipNo,
                orderItem.orderItemNo,
                orderItem.itemNo,
                orderItem.itemNm1,
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, shippingItem.shipMethod, ObjectUtils.toArray(shippingItem.shipMethod), "ship_method"),
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, shippingItem.shipStatus, ObjectUtils.toArray(shippingItem.shipStatus), "ship_status"),
                shippingItem.invoiceNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(bundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingItem),
                    ObjectUtils.toList( SqlUtils.onClause(ObjectUtils.toArray(bundle.bundleNo), shippingItem) )
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderItem),
                    ObjectUtils.toList( SqlUtils.onClause(ObjectUtils.toArray(shippingItem.orderItemNo), orderItem))
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(bundle.bundleNo, bundleNo)
            );
        log.debug("selectOrderShipInfo ::: [{}]", query.toString());
        return query.toString();
    }

    public static String updateShippingAddrEditCheck(OrderShipAddrEditDto setShipAddrDto) {
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);

//        SQL query = new SQL()
//            .SELECT(
//                SqlUtils.sqlFunction(
//                    MysqlFunction.CASE,
//                    ObjectUtils.toArray(
//                        SqlUtils.customOperator(
//                            CustomConstants.NOT_EQUAL,
//                            ObjectUtils.toArray(SqlUtils.convertColumnToMapper(shippingAddr.receiverNm), shippingAddr.receiverNm)
//                        ),
//                        SqlUtils.convertColumnToMapper(shippingAddr.receiverNm),
//                        "NULL"
//                    )
//                )
//            )
//            .FROM(EntityFactory.getTableNameWithAlias(shippingItem))
//            .INNER_JOIN(
//                SqlUtils.createJoinCondition(
//                    EntityFactory.getTableNameWithAlias(shippingAddr),
//                    ObjectUtils.toList( SqlUtils.onClause(ObjectUtils.toArray(shippingItem.shipAddrNo), shippingAddr) )
//                )
//            )
//            .WHERE(
//                SqlUtils.equalColumnMapping(shippingItem.bundleNo),
//                SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("D1", "D2"))
//            );
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableNameWithAlias(shippingItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingAddr),
                    ObjectUtils.toList( SqlUtils.onClause(ObjectUtils.toArray(shippingItem.shipAddrNo), shippingAddr) )
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo))
            )
            .SET(
                SqlUtils.equalColumnByInput(shippingAddr.receiverNm, setShipAddrDto.getReceiverNm()),
                SqlUtils.equalColumnByInput(shippingAddr.personalOverseaNo, setShipAddrDto.getPersonalOverseaNo()),
                SqlUtils.equalColumnByInput(shippingAddr.shipMobileNo, setShipAddrDto.getShipMobileNo()),
                SqlUtils.equalColumnByInput(shippingAddr.zipcode, setShipAddrDto.getZipCode()),
                SqlUtils.equalColumnByInput(shippingAddr.roadBaseAddr, setShipAddrDto.getRoadBaseAddr()),
                SqlUtils.equalColumnByInput(shippingAddr.roadDetailAddr, setShipAddrDto.getRoadDetailAddr()),
                SqlUtils.equalColumnByInput(shippingAddr.baseAddr, setShipAddrDto.getBaseAddr() == null ? "" : setShipAddrDto.getBaseAddr()),
                SqlUtils.equalColumnByInput(shippingAddr.detailAddr, setShipAddrDto.getDetailAddr() == null ? "" : setShipAddrDto.getDetailAddr())
            )
            .WHERE(
                SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("'D1'", "'D2'")),
                SqlUtils.OR(
                    SqlUtils.equalColumnByInput(bundle.bundleNo, setShipAddrDto.getBundleNo()),
                    SqlUtils.equalColumnByInput(bundle.orgBundleNo, setShipAddrDto.getBundleNo())
                )
            );
        if("TD,PICK,SUM".contains(setShipAddrDto.getShipType())){
            query.SET(
                SqlUtils.equalColumnByInput(shippingAddr.storeShipMsgCd, setShipAddrDto.getShipMsgCd()),
                SqlUtils.equalColumnByInput(shippingAddr.storeShipMsg, setShipAddrDto.getShipMsg())
            );
        } else if(setShipAddrDto.getShipType().equalsIgnoreCase("DS")){
            query.SET(
                SqlUtils.equalColumnByInput(shippingAddr.dlvShipMsgCd, setShipAddrDto.getShipMsgCd()),
                SqlUtils.equalColumnByInput(shippingAddr.dlvShipMsg, setShipAddrDto.getShipMsg())
            );
        }

        if(!setShipAddrDto.getAuroraShipMsgCd().isEmpty()) {
            query.SET(
                SqlUtils.equalColumnByInput(shippingAddr.auroraShipMsgCd, setShipAddrDto.getAuroraShipMsgCd()),
                SqlUtils.equalColumnByInput(shippingAddr.auroraShipMsg, setShipAddrDto.getAuroraShipMsg())
            );
        }

        log.debug("updateShippingAddrEditCheck ::: \n[{}]", query.toString());
        return query.toString();
    }

    // 미수취 등록
    public static String insertNoRcvShip(NoRcvShipSetGto noRcvShipSetGto){
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class);

        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(shippingNonRcv))
            .INTO_COLUMNS(shippingNonRcv.purchaseOrderNo, shippingNonRcv.bundleNo,
                shippingNonRcv.userNo, shippingNonRcv.partnerId, shippingNonRcv.noRcvDeclrType, shippingNonRcv.noRcvDetailReason, shippingNonRcv.noRcvDeclrDt,
                shippingNonRcv.regId, shippingNonRcv.regDt, shippingNonRcv.chgId, shippingNonRcv.chgDt)
            .INTO_VALUES(
                String.valueOf(noRcvShipSetGto.getPurchaseOrderNo()),
                String.valueOf(noRcvShipSetGto.getBundleNo()),
                String.valueOf(noRcvShipSetGto.getUserNo()),
                SqlUtils.appendSingleQuote(noRcvShipSetGto.getPartnerId()),
                SqlUtils.appendSingleQuote(noRcvShipSetGto.getNoRcvDeclrType()),
                SqlUtils.appendSingleQuote(noRcvShipSetGto.getNoRcvDetailReason()),
                "NOW()",
                SqlUtils.appendSingleQuote(noRcvShipSetGto.getRegId()),
                "NOW()",
                SqlUtils.appendSingleQuote(noRcvShipSetGto.getRegId()),
                "NOW()"
            );

        log.debug("insertNoRcvShip ::: \n[{}]", query.toString());
        return query.toString();
    }

    // 미수취 처리 업데이트
    public static String updateNoRcvShip(NoRcvShipEditDto editDto) {
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class);

        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(shippingNonRcv))
            .SET(
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessType, editDto.getNoRcvProcessType()),
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessCntnt, editDto.getNoRcvProcessCntnt()),
                SqlUtils.nonSingleQuote(shippingNonRcv.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(shippingNonRcv.chgId, editDto.getChgId())
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingNonRcv.purchaseOrderNo, editDto.getPurchaseOrderNo()),
                SqlUtils.equalColumnByInput(shippingNonRcv.bundleNo, editDto.getBundleNo())
            );

        if(editDto.getNoRcvProcessType().equalsIgnoreCase("W")){
            query.SET(
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "Y"),
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "Y"),
                SqlUtils.nonSingleQuote(shippingNonRcv.noRcvProcessDt, "NOW()")
            );
        } else if(editDto.getNoRcvProcessType().equalsIgnoreCase("C")){
            query.SET(
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "Y"),
                SqlUtils.nonSingleQuote(shippingNonRcv.noRcvProcessDt, "NOW()")
            );
        }

        log.debug("updateNoRcvShip ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectNonShipCompleteInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class);
        SQL query = new SQL()
            .SELECT(
                shippingItem.purchaseOrderNo,
                shippingItem.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shipStatus, shippingItem.shipStatus),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_24, SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.completeDt), shippingItem.completeDt)
            )
            .FROM(EntityFactory.getTableName(shippingItem))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo)
            )
            .GROUP_BY(shippingItem.purchaseOrderNo, shippingItem.bundleNo);
        log.debug("selectNonShipCompleteInfo ::: [{}]", query.toString());
        return query.toString();
    }

    public static String selectNoRcvShipInfo(NoRcvShipInfoSetDto infoSetDto) {
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class, Boolean.TRUE);
        EmployeeInfoEntity employeeInfo = EntityFactory.createEntityIntoValue(EmployeeInfoEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                shippingNonRcv.noRcvDeclrType,
                shippingNonRcv.noRcvDetailReason,
                shippingNonRcv.noRcvDeclrDt,
                shippingNonRcv.noRcvProcessDt,
                shippingNonRcv.noRcvProcessType,
                shippingNonRcv.noRcvProcessCntnt,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "Y"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "Y"), "'미수취 신고 철회'", "'미수취 신고 철회요청'")
                        ),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "Y"), "'미수취 신고 철회'", "''")
                        )
                    ),
                    "no_rcv_process_result"
                ),
                SqlUtils.caseWhenElse(
                    SqlUtils.sqlFunction(MysqlFunction.UPPER, shippingNonRcv.regId),
                    ObjectUtils.toArray(
                        "'PARTNER'", "'판매자'", // shippingNonRcv.partnerId,
                        "'MYPAGE'", "'구매자'" // shippingNonRcv.userNo
                    ),
                    SqlUtils.subTableQuery(
                        new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(employeeInfo.name, "'('", shippingNonRcv.regId, "')'")))
                            .FROM(EntityFactory.getTableNameWithAlias("dms", employeeInfo))
                            .WHERE(SqlUtils.equalColumn(employeeInfo.empid, shippingNonRcv.regId))
                    ),
                    "no_rcv_reg_id"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.OR,
                            ObjectUtils.toArray(
                                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "Y"),
                                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "Y")
                            )
                        ),
                        SqlUtils.caseWhenElse(
                            SqlUtils.sqlFunction(MysqlFunction.UPPER, shippingNonRcv.regId),
                            ObjectUtils.toArray(
                                "'PARTNER'", "'판매자'", // shippingNonRcv.partnerId,
                                "'MYPAGE'", "'구매자'" // shippingNonRcv.userNo
                            ),
                            SqlUtils.subTableQuery(
                                new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(employeeInfo.name, "'('", shippingNonRcv.regId, "')'")))
                                    .FROM(EntityFactory.getTableNameWithAlias("dms", employeeInfo))
                                    .WHERE(SqlUtils.equalColumn(employeeInfo.empid, shippingNonRcv.chgId))
                            )
                        ),
                        "''"
                    ),
                    "no_rcv_chg_id"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingNonRcv))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingNonRcv.bundleNo, infoSetDto.getBundleNo())
            );

        if(infoSetDto.getUserNo() != 0) {
            query.WHERE(
                SqlUtils.equalColumnByInput(shippingNonRcv.userNo, infoSetDto.getUserNo())
            );
        }

        log.debug("selectNoRcvShipInfo ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectNoRcvMakeData(@Param("bundleNo") long bundleNo) {
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class);
        SQL query = new SQL()
            .SELECT(
                shippingItem.purchaseOrderNo,
                shippingItem.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.userNo, shippingItem.userNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.partnerId, shippingItem.partnerId)
            )
            .FROM(EntityFactory.getTableName(shippingItem))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo)
            )
            .GROUP_BY(shippingItem.purchaseOrderNo, shippingItem.bundleNo)
            ;
        log.debug("selectNoRcvMakeData ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectNoRcvDuplCheck(@Param("bundleNo") long bundleNo) {
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, shippingNonRcv.noRcvDeclrType, "cnt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingNonRcv.noRcvProcessYn, shippingNonRcv.noRcvProcessYn),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingNonRcv.noRcvWthdrwYn, shippingNonRcv.noRcvWthdrwYn),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.buyerMobileNo, purchaseOrder.buyerMobileNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingNonRcv))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(shippingNonRcv.purchaseOrderNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingNonRcv.bundleNo, bundleNo)
            );
        log.debug("selectNoRcvDuplCheck ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectSafetyIssueInfo(@Param("bundleNo") long bundleNo){
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                shippingItem.purchaseOrderNo,
                shippingAddr.safetyUseYn
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo)
            )
            .LIMIT(1)
            .OFFSET(0);
        log.debug("selectSafetyIssueInfo ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectShippingPresentStatus(@Param("purchaseOrderNo") long purchaseOrderNo){
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                shippingItem.shipStatus,
                shippingItem.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(shippingItem.purchaseOrderNo, shippingItem.bundleNo, "'C'"), "ship_type"),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(SqlUtils.nonAlias(purchaseOrder.orderType))
                        .FROM(EntityFactory.getTableName(purchaseOrder))
                        .WHERE(SqlUtils.equalColumn(SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo), shippingItem.purchaseOrderNo)),
                    purchaseOrder.orderType
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.originStoreId, bundle.originStoreId),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.purchaseStoreInfoNo, bundle.purchaseStoreInfoNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingItem),
                    ObjectUtils.toArray(
                        SqlUtils.OR(
                            SqlUtils.equalColumn(purchaseOrder.purchaseOrderNo, shippingItem.purchaseOrderNo),
                            SqlUtils.equalColumn(purchaseOrder.orgPurchaseOrderNo, shippingItem.purchaseOrderNo)
                        )
                    )
                )
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo)))
            .WHERE(
                SqlUtils.OR(
                    SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                    SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo)
                )
            )
            .GROUP_BY(shippingItem.purchaseOrderNo, shippingItem.shipStatus, shippingItem.bundleNo)
            ;
        log.debug("selectShippingPresentStatus :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectStoreShipMemoInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                shippingItem.bundleNo,
                shippingAddr.shipMemo,
                shippingAddr.shipAddrNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo),
                SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'"))
            )
            .LIMIT(1).OFFSET(0);
        log.debug("selectStoreShipMemoInfo :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectStoreShipMsgInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                shippingItem.bundleNo,
                shippingAddr.storeShipMsg
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'"))
            )
            .LIMIT(1).OFFSET(0);
        log.debug("selectStoreShipMsgInfo :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateStoreShipMemo(StoreShipMemoDto memoDto) {
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(shippingAddr))
            .SET(
                SqlUtils.equalColumnByInput(shippingAddr.shipMemo, memoDto.getShipMemo()),
                SqlUtils.equalColumn(shippingAddr.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(shippingAddr.chgId, memoDto.getChgId())
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingAddr.shipAddrNo, memoDto.getShipAddrNo())
            );
        log.debug("updateStoreShipMemo :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOriPurchaseOrderInfoFromCombine(@Param("purchaseOrderNo") long purchaseOrderNo) {
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                shippingItem.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderType, purchaseOrder.orderType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orgPurchaseOrderNo, purchaseOrder.orgPurchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.orgBundleNo, bundle.orgBundleNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    shippingItem,
                    ObjectUtils.toArray(purchaseOrder.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'"))
                    )
                )
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo, shippingItem.bundleNo);
        log.debug("selectOriPurchaseOrderInfoFromCombine :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateMultiOrderShippingAddrInfo(MultiOrderShipAddrEditDto shipAddrEditDto) {
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        MultiShippingAddrEntity multiShippingAddr = EntityFactory.createEntityIntoValue(MultiShippingAddrEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableNameWithAlias(multiShippingAddr))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(multiOrderItem, ObjectUtils.toArray(multiShippingAddr.multiBundleNo))
            )
            .SET(
                SqlUtils.equalColumnByInput(multiShippingAddr.receiverNm, shipAddrEditDto.getReceiverNm()),
                SqlUtils.equalColumnByInput(multiShippingAddr.shipMobileNo, shipAddrEditDto.getShipMobileNo()),
                SqlUtils.equalColumnByInput(multiShippingAddr.zipcode, shipAddrEditDto.getZipCode()),
                SqlUtils.equalColumnByInput(multiShippingAddr.roadBaseAddr, shipAddrEditDto.getRoadBaseAddr()),
                SqlUtils.equalColumnByInput(multiShippingAddr.roadDetailAddr, shipAddrEditDto.getRoadDetailAddr()),
                SqlUtils.equalColumnByInput(multiShippingAddr.baseAddr, shipAddrEditDto.getBaseAddr() == null ? "" : shipAddrEditDto.getBaseAddr()),
                SqlUtils.equalColumnByInput(multiShippingAddr.detailAddr, shipAddrEditDto.getDetailAddr() == null ? "" : shipAddrEditDto.getDetailAddr()),
                SqlUtils.equalColumnByInput(multiShippingAddr.chgId, shipAddrEditDto.getChgId())
            )
            .WHERE(
                SqlUtils.equalColumnByInput(multiShippingAddr.multiBundleNo, shipAddrEditDto.getMultiBundleNo()),
                // 프론트에서 수정시 D1만 가능.
                SqlUtils.equalColumnByInput(multiOrderItem.shipStatus, "D1")
            );
        log.debug("updateMultiOrderShippingAddrInfo ::: \n[{}]", query.toString());
        return query.toString();
    }
}
