package kr.co.homeplus.escrowmng.pg.service;

import static kr.co.homeplus.escrowmng.constants.Constants.COMMNA;
import static kr.co.homeplus.escrowmng.constants.Constants.INSTALLMENT_TYPE_N;
import static kr.co.homeplus.escrowmng.constants.Constants.INSTALLMENT_TYPE_P;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.escrowmng.constants.Constants;
import kr.co.homeplus.escrowmng.core.db.construct.TransactionManagerName;
import kr.co.homeplus.escrowmng.core.exception.BusinessLogicException;
import kr.co.homeplus.escrowmng.enums.BusinessExceptionCode;
import kr.co.homeplus.escrowmng.pg.mapper.FreeInterestMasterMapper;
import kr.co.homeplus.escrowmng.pg.mapper.FreeInterestSlaveMapper;
import kr.co.homeplus.escrowmng.pg.model.FreeInterestApplyDto;
import kr.co.homeplus.escrowmng.pg.model.FreeInterestCopyDto;
import kr.co.homeplus.escrowmng.pg.model.FreeInterestDto;
import kr.co.homeplus.escrowmng.pg.model.FreeInterestSelectDto;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class FreeInterestService {

    public static final String INTEREST_INSERT = "INSERT";
    public static final String INTEREST_UPDATE = "UPDATE";

    private final FreeInterestSlaveMapper freeInterestSlaveMapper;
    private final FreeInterestMasterMapper freeInterestMasterMapper;


    /**
     * 카드무이자혜택 마스터 조회
     * @param freeInterestSelectDto
     * @return
     * @throws Exception
     */
    public List<FreeInterestDto> getFreeInterestMngList(FreeInterestSelectDto freeInterestSelectDto) throws Exception {
        return freeInterestSlaveMapper.selectFreeInterestMngList(freeInterestSelectDto);
    }

    /**
     * 카드무이자혜택 적용대상 조회
     * @param freeInterestMngSeq
     * @return
     * @throws Exception
     */
    public List<FreeInterestApplyDto> getFreeInterestApplyList(long freeInterestMngSeq) throws Exception {
        return freeInterestSlaveMapper.selectFreeInterestApplyList(freeInterestMngSeq);
    }

    /**
     * 복사된 내용 저장
     * @param freeInterestCopyDto
     * @return
     * @throws Exception
     */
    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int saveCopyFreeInterest(FreeInterestCopyDto freeInterestCopyDto) throws Exception {

        int rtnVal = 0;
        //무이자혜택 기본정보 저장
        FreeInterestDto freeInterestDto = freeInterestCopyDto.getFreeInterestDto();
        freeInterestDto.setRegId(freeInterestCopyDto.getRegId());
        freeInterestDto.setChgId(freeInterestCopyDto.getChgId());

        rtnVal = this.saveFreeInterest(freeInterestDto);

        //무이자적용 대상 카드 저장
        for (FreeInterestApplyDto each: freeInterestCopyDto.getFreeInterestApplyDtoList()) {
            each.setFreeInterestMngSeq(freeInterestDto.getFreeInterestMngSeq());
            each.setRegId(freeInterestCopyDto.getRegId());
            rtnVal = this.saveFreeInterestApply(each);
        }

        return rtnVal;
    }

    /**
     * 무이자 기간을 형식에 맞게 재정의함.
     * ex : 2,3,4,,,5,6,7 => 2,3,4,5,6,7 리턴
     * @param installmentTerm
     * @return
     */
    private String changeInstallmentTerm(String installmentTerm) {
        List<String> list = Stream.of(installmentTerm.split(COMMNA)).filter(p->p.length()>0).collect(Collectors.toList());
        String rtnInstallmentTerm = list.stream().collect(Collectors.joining(COMMNA));
        return rtnInstallmentTerm;
    }
    /**
     * 무이자기본정보 저장
     * @param freeInterestDto
     * @return
     * @throws Exception
     */
    public int saveFreeInterest(FreeInterestDto freeInterestDto) throws Exception {
        int rtnVal = 0;
        // 신규입력시 존재 여부 확인
        FreeInterestSelectDto freeInterestSelectDto = new FreeInterestSelectDto();
        freeInterestSelectDto.setSiteType(freeInterestDto.getSiteType());
        freeInterestSelectDto.setApplyStartDt(freeInterestDto.getApplyStartDt());
        freeInterestSelectDto.setApplyEndDt(freeInterestDto.getApplyEndDt());
        freeInterestSelectDto.setUseYn(Constants.USE_Y);
        List<FreeInterestDto> freeInterestDtoList = freeInterestSlaveMapper.selectFreeInterestMngList(freeInterestSelectDto);

        if (EscrowString.isEmpty(freeInterestDto.getFreeInterestMngSeq())) {
            if (EscrowString.isNotEmpty(freeInterestDtoList)) {
                throw new BusinessLogicException(BusinessExceptionCode.ADMIN_VALIDATION_10000);
            }
            freeInterestDto.setApplyStartDt(freeInterestDto.getApplyStartDt() + " 00:00:00");
            freeInterestDto.setApplyEndDt(freeInterestDto.getApplyEndDt() + " 23:59:59");
            rtnVal = freeInterestMasterMapper.insertFreeInterestMng(freeInterestDto);
        } else {
            rtnVal = freeInterestMasterMapper.updateFreeInterestMng(freeInterestDto);
        }
        return rtnVal;

    }

    /**
     * 무이자 대상카드 정보 저장
     * @param freeInterestApplyDto
     * @return
     * @throws Exception
     */
    public int saveFreeInterestApply(FreeInterestApplyDto freeInterestApplyDto) throws Exception {
        int rtnVal = 0;
        //신규입력시 존재여부 확인
        List<FreeInterestApplyDto> freeInterestApplyDtoList = freeInterestSlaveMapper.selectFreeInterestApplyList(freeInterestApplyDto.getFreeInterestMngSeq());
        //무이자기간을 형식에 맞게 재정의
        freeInterestApplyDto.setInstallmentTerm(this.changeInstallmentTerm(freeInterestApplyDto.getInstallmentTerm()));
        if (EscrowString.isEmpty(freeInterestApplyDto.getApplySeq())) {
            this.validDuplicationFreeInstallment(freeInterestApplyDto, freeInterestApplyDtoList,INTEREST_INSERT);
            rtnVal = freeInterestMasterMapper.insertFreeInterestApply(freeInterestApplyDto);
        } else {
            this.validDuplicationFreeInstallment(freeInterestApplyDto, freeInterestApplyDtoList,INTEREST_UPDATE);
            rtnVal = freeInterestMasterMapper.updateFreeInterestApply(freeInterestApplyDto);
        }
        return rtnVal;
    }

    /**
     * 무이자 대상카드 중복 검증
     * @param freeInterestApplyDto
     * @param freeInterestApplyDtoList
     * @param status
     * @throws Exception
     */
    private void validDuplicationFreeInstallment(FreeInterestApplyDto freeInterestApplyDto, List<FreeInterestApplyDto> freeInterestApplyDtoList, String status) throws Exception {

        long methodCdCnt = freeInterestApplyDtoList.stream()
            .filter(p -> p.getMethodCd()
                .equals(freeInterestApplyDto.getMethodCd()))
            .count();
        if (methodCdCnt>=2 && INTEREST_INSERT.equals(status)) {
            throw new BusinessLogicException (BusinessExceptionCode.ADMIN_VALIDATION_10002);
        }

        String installment = "";          //무이자 기간
        String installmentOption = "";    //부분무이자 기간

        for (FreeInterestApplyDto each: freeInterestApplyDtoList) {
            if (freeInterestApplyDto.getMethodCd().equals(each.getMethodCd())) {
                if(freeInterestApplyDto.getFreeInterestType().equals(each.getFreeInterestType()) && INTEREST_INSERT.equals(status)) {
                    throw new BusinessLogicException(BusinessExceptionCode.ADMIN_VALIDATION_10003);
                }
                switch (each.getFreeInterestType()) {
                    case INSTALLMENT_TYPE_N :
                        installment = each.getInstallmentTerm();
                        break;
                    case INSTALLMENT_TYPE_P :
                        installmentOption = each.getInstallmentTerm();
                        break;
                }
            }
        }
        if (EscrowString.isNotEmpty(installment) || EscrowString.isNotEmpty(installmentOption)) {
            this.validationSpiteInstallment(installment, installmentOption, status, freeInterestApplyDto);
        }


    }

    /**
     * 무이자대상카드 중복개월수 검증
     * @param installment
     * @param installmentOption
     * @param status
     * @param freeInterestApplyDto
     * @throws Exception
     */
    private void validationSpiteInstallment(String installment, String installmentOption, String status, FreeInterestApplyDto freeInterestApplyDto) throws Exception {

        String[] installmentArray = installment.split(",");         //무이자 할부기간
        String[] installmentOptionArray = installmentOption.split(",");     //부분무이자 할부기간
        String[] inputInstallmentArray = freeInterestApplyDto.getInstallmentTerm().split(",");        //화면입력할부기간

        if (status.equals(INTEREST_UPDATE)) {
            if (INSTALLMENT_TYPE_N.equals(freeInterestApplyDto.getFreeInterestType())){
                for (int i = 0; i < installmentOptionArray.length; i++) {
                    for (int j = 0; j < inputInstallmentArray.length; j++) {
                        if (installmentOptionArray[i].equals(inputInstallmentArray[j])) {
                            throw new BusinessLogicException(BusinessExceptionCode.ADMIN_VALIDATION_10004);
                        }
                    }
                }
            } else {
                for (int i = 0; i < installmentArray.length; i++) {
                    for (int j = 0; j < inputInstallmentArray.length; j++) {
                        if (installmentArray[i].equals(inputInstallmentArray[j])) {
                            throw new BusinessLogicException(BusinessExceptionCode.ADMIN_VALIDATION_10004);
                        }
                    }
                }
            }
        } else {
            if (freeInterestApplyDto.getFreeInterestType().equals(INSTALLMENT_TYPE_N)) {
                for (int i = 0; i < inputInstallmentArray.length; i++) {
                    for (int j = 0; j < installmentOptionArray.length; j++) {
                        if (inputInstallmentArray[i].equals(installmentOptionArray[j])) {
                            throw new BusinessLogicException(BusinessExceptionCode.ADMIN_VALIDATION_10004);
                        }
                    }
                }
            } else {
                for (int i = 0; i < inputInstallmentArray.length; i++) {
                    for (int j = 0; j < installmentArray.length; j++) {
                        if (inputInstallmentArray[i].equals(installmentArray[j])) {
                            throw new BusinessLogicException(BusinessExceptionCode.ADMIN_VALIDATION_10004);
                        }
                    }
                }
            }
        }
    }

    /**
     * 무이자적용대상 카드를 삭제처리
     * @param freeInterestApplyDto
     * @return
     * @throws Exception
     */
    public int deleteFreeInterestApply(FreeInterestApplyDto freeInterestApplyDto) throws Exception {
        return freeInterestMasterMapper.deleteFreeInterestApply(freeInterestApplyDto);
    }
}
