package kr.co.homeplus.escrowmng.statistics.model;

import lombok.Data;

@Data
public class CombineOrderStatusDto {
    String storeNm;
    Integer storeId;

    // 원주문
    long orgOrderCount;
    long orgOrderAmt;
    long orgCancelCount;
    long orgCancelAmt;
    long orgReturnCount;
    long orgReturnAmt;
    long orgSubCount;
    long orgSubAmt;

    // 합배송
    long combOrderCount;
    long combOrderAmt;
    long combCancelCount;
    long combCancelAmt;
    long combReturnCount;
    long combReturnAmt;
    long combSubCount;
    long combSubAmt;
}
