package kr.co.homeplus.escrowmng.claim.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClaimInfoRegDto {
    private Long paymentNo;
    private Long purchaseOrderNo;
    private Long claimNo;
}
