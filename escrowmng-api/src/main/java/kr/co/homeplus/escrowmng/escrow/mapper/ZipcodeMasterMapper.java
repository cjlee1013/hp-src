package kr.co.homeplus.escrowmng.escrow.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.escrow.model.ZipcodeManageModel;

@EscrowMasterConnection
public interface ZipcodeMasterMapper {
    int updateZipcodeIslandType(ZipcodeManageModel zipcodeManageModel);
}
