package kr.co.homeplus.escrowmng.claim.mapper.refundFail;

import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListSetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteSetDto;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;

@EscrowSlaveConnection
public interface RefundFailSlaveMapper {

    List<RefundFailListGetDto> selectRefundFailList(RefundFailListSetDto refundFailListSetDto);

    RequestRefundCompleteGetDto selectRefundCompleteResultCount(RequestRefundCompleteSetDto requestRefundCompleteSetDto);
}
