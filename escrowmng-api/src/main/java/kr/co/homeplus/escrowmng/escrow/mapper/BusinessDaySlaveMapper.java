package kr.co.homeplus.escrowmng.escrow.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.escrow.model.BusinessDayManageModel;
import kr.co.homeplus.escrowmng.escrow.model.BusinessDayManageSelectModel;

@EscrowSlaveConnection
public interface BusinessDaySlaveMapper {
    List<BusinessDayManageModel> getBusinessDayList(BusinessDayManageSelectModel businessDayManageSelectModel);
}
