package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PgDivideManageSelectDto {
    @ApiModelProperty(value = "배분순번")
    private long divideSeq;
    @ApiModelProperty(value = "pg관리유형(기본배분율:BASIC,기간배분율:PERIOD,결제정책배분율:POLICY)")
    private String pgMngType;
    @ApiModelProperty(value = "사이트유형(HOME:홈플러스,CLUB:더클럽)")
    private String siteType;
    @ApiModelProperty(value = "플랫폼(PC,모바일)")
    private String platform;
    @ApiModelProperty(value = "상위결제수단코드(신용카드,휴대폰결제등)")
    private String parentMethodCd;
    @ApiModelProperty(value = "사용여부")
    private String useYn;
    @ApiModelProperty(value = "팝업여부")
    private String isPopup;
    @ApiModelProperty(value = "정책번호")
    private Long policyNo;
}
