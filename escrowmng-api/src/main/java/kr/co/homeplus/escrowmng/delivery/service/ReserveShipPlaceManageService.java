package kr.co.homeplus.escrowmng.delivery.service;

import java.util.List;
import kr.co.homeplus.escrowmng.delivery.mapper.ReserveShipPlaceManageMasterMapper;
import kr.co.homeplus.escrowmng.delivery.mapper.ReserveShipPlaceManageSlaveMapper;
import kr.co.homeplus.escrowmng.delivery.model.ReserveShipPlaceManageModel;
import kr.co.homeplus.escrowmng.delivery.model.ReserveShipPlaceManageSelectModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ReserveShipPlaceManageService {
    private final ReserveShipPlaceManageSlaveMapper reserveShipPlaceManageSlaveMapper;
    private final ReserveShipPlaceManageMasterMapper reserveShipPlaceManageMasterMapper;

    /**
     * 선물세트 발송지 리스트 조회
     * @param reserveShipPlaceManageSelectModel
     * @return
     */
    public List<ReserveShipPlaceManageModel> getReserveShipPlaceMngList(ReserveShipPlaceManageSelectModel reserveShipPlaceManageSelectModel) {
        return reserveShipPlaceManageSlaveMapper.selectReserveShipPlaceMngList(reserveShipPlaceManageSelectModel);
    }

    /**
     * 선물세트 발송지 저장
     * @param reserveShipPlaceManageModel
     * @return
     */
    public int saveReserveShipPlace(ReserveShipPlaceManageModel reserveShipPlaceManageModel) {
        return reserveShipPlaceManageMasterMapper.insertReserveShipPlace(reserveShipPlaceManageModel);
    }

    /**
     * 선물세트 발송지 리스트 저장 (다중건)
     * @param reserveShipPlaceManageModelList
     * @return
     */
    public int saveReserveShipPlaceList(List<ReserveShipPlaceManageModel> reserveShipPlaceManageModelList) {
        int resultCnt = 0;
        for (ReserveShipPlaceManageModel reserveShipPlaceManageModel : reserveShipPlaceManageModelList) {
            resultCnt += reserveShipPlaceManageMasterMapper.insertReserveShipPlace(reserveShipPlaceManageModel);
        }
        return resultCnt;
    }

    /**
     * 선물세트 발송지 리스트 삭제
     * @param reserveShipPlaceManageModelList
     * @return
     */
    public int deleteReserveShipPlaceList(List<ReserveShipPlaceManageModel> reserveShipPlaceManageModelList) {
        int resultCnt = 0;
        for (ReserveShipPlaceManageModel reserveShipPlaceManageModel : reserveShipPlaceManageModelList) {
            resultCnt += reserveShipPlaceManageMasterMapper.deleteReserveShipPlace(reserveShipPlaceManageModel);
        }
        return resultCnt;
    }
}