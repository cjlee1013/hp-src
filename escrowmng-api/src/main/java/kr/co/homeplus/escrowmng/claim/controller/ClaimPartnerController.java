package kr.co.homeplus.escrowmng.claim.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimApproveSetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerBoardCountGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerDetailGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerExchangeListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListSetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerDetailListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerReturnListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.PartnerMainBoardClaimRequestCountGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ticketRental.TicketRentalCancelListGetDto;
import kr.co.homeplus.escrowmng.claim.service.ClaimPartnerService;
import kr.co.homeplus.escrowmng.claim.service.partner.TicketRentalCancelService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/po/claim")
@RequiredArgsConstructor
@Api(tags = "Partner Web > 클레임 관리")
public class ClaimPartnerController {

    private final ClaimPartnerService claimPartnerService;

    private final TicketRentalCancelService ticketRentalCancelService;


    @ApiOperation(value = "클레임 간판 건수 조회")
    @GetMapping(value = "/getClaimBoardCount")
    public ResponseObject<ClaimPartnerBoardCountGetDto> getPOClaimBoardCount(
        @RequestParam @ApiParam(name = "partnerId", value = "파트너 ID", required = true) String partnerId,
        @RequestParam @ApiParam(name = "claimType", value = "클레임타입(C:취소/R:반품/X:교환)", required = true) String claimType
    ){
        return claimPartnerService.getPOClaimBoardCount(partnerId, claimType);
    }

    @ApiOperation(value = "PO메인 간판 - 클레임 요청 건수 조회")
    @GetMapping(value = "/getTicketRentalClaimBoardCount")
    public ResponseObject<ClaimPartnerBoardCountGetDto> getTicketRentalClaimBoardCount(
        @RequestParam @ApiParam(name = "partnerId", value = "파트너 ID", required = true) String partnerId,
        @RequestParam @ApiParam(name = "claimType", value = "클레임타입(C:취소/R:반품/X:교환)", required = true) String claimType
    ){
        return ticketRentalCancelService.getTicketRentalClaimBoardCount(partnerId,claimType);
    }

    @ApiOperation(value = "PO메인 간판 - 클레임 요청 건수 조회")
    @GetMapping(value = "/getPOMainClaimRequestCount")
    public ResponseObject<PartnerMainBoardClaimRequestCountGetDto> getPOMainClaimRequestCount(
        @RequestParam @ApiParam(name = "partnerId", value = "파트너 ID", required = true) String partnerId
    ){
        return claimPartnerService.getPOMainClaimRequestCount(partnerId);
    }

    @ApiOperation(value = "PO클레임 리스트 조회")
    @PostMapping(value = "/getPOClaimList")
    public ResponseObject<List<ClaimPartnerListGetDto>> getPOClaimList(@RequestBody ClaimPartnerListSetDto claimCancelListSetDto){
        return claimPartnerService.getPOClaimList(claimCancelListSetDto);
    }

    @ApiOperation(value = "PO클레임 리스트 조회")
    @PostMapping(value = "/getPOClaimListCnt")
    public ResponseObject<Integer> getPOClaimListCnt(@RequestBody ClaimPartnerListSetDto claimCancelListSetDto){
        return claimPartnerService.getPOClaimListCnt(claimCancelListSetDto);
    }

    @ApiOperation(value = "이티켓/렌잘 취소 리스트 조회")
    @PostMapping(value = "/getTicketRentalCancelList")
    public ResponseObject<List<TicketRentalCancelListGetDto>> getTicketRentalCancelList(@RequestBody ClaimPartnerListSetDto claimCancelListSetDto){
        return ticketRentalCancelService.getTicketRentalCancelList(claimCancelListSetDto);
    }

    @ApiOperation(value = "클레임 상세정보 조회 리스트")
    @GetMapping(value = "/getClaimDetailList")
    public ResponseObject<List<ClaimPartnerDetailListGetDto>> getPOClaimDetailInfoList(
        @RequestParam @ApiParam(name = "purchaseOrderNo", value = "주문번호", required = true) long purchaseOrderNo,
        @RequestParam @ApiParam(name = "bundleNo", value = "배송번호", required = true) long bundleNo

    ){
        return claimPartnerService.getPOClaimDetailInfoList(purchaseOrderNo, bundleNo);
    }

    @ApiOperation(value = "클레임 단건 상세정보 조회")
    @GetMapping(value = "/getClaimDetail")
    public ResponseObject<ClaimPartnerDetailGetDto> getPOClaimDetailInfoList(
        @RequestParam @ApiParam(name = "claimBundleNo", value = "클레임 번호", required = true) long claimBundleNo,
        @RequestParam @ApiParam(name = "claimType", value = "클레임 타입", required = true) String claimType){
        return claimPartnerService.getPOClaimDetailInfo(claimBundleNo, claimType);
    }

    @ApiOperation(value = "PO 취소 승인 요청")
    @PostMapping(value = "/requestClaimApprove")
    public ResponseObject<Integer> requestClaimApprove(@RequestBody ClaimApproveSetDto claimApproveSetDto) throws Exception {
        return claimPartnerService.requestClaimApprove(claimApproveSetDto);
    }

}
