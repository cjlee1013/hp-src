package kr.co.homeplus.escrowmng.escrow.model.extract;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import kr.co.homeplus.escrowmng.extract.model.ExtractCommonSetDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "주문데이터추출 조회 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class OrderDataExtractSelectDto {
    @ApiModelProperty(value = "조회시작일자", position = 1)
    @NotEmpty(message = "조회시작일자")
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일자", position = 2)
    @NotEmpty(message = "조회종료일자")
    private String schEndDt;

    @ApiModelProperty(value = "추가파라미터리스트", position = 3)
    @NotEmpty(message = "파라미터")
    private List<String> schAddParamList;

    @ApiModelProperty(value = "주문번호", position = 4)
    private String schPurchaseOrderNo;

    @ApiModelProperty(value = "조회시작배송일자", position = 5)
    private String schStartShipDt;

    @ApiModelProperty(value = "조회종료배송일자", position = 6)
    private String schEndShipDt;

    @ApiModelProperty(value = "추출이력 저장용 DTO", position = 7)
    ExtractCommonSetDto extractCommonSetDto;
}