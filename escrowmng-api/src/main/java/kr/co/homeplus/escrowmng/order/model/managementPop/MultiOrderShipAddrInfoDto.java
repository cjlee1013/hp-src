package kr.co.homeplus.escrowmng.order.model.managementPop;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문관리 > 상세 > 배송정보 > 다중배송정보수정 Dto")
public class MultiOrderShipAddrInfoDto {

    @ApiModelProperty(value = "이름", position = 1)
    private String receiverNm;

    @ApiModelProperty(value = "박스번호", position = 2)
    private String multiBundleNo;

    @ApiModelProperty(value = "연락처", position = 3)
    private String shipMobileNo;

    @ApiModelProperty(value = "안심번호신청여부", position = 5)
    private String safetyNm;

    @ApiModelProperty(value = "우편번호", position = 6)
    private String zipCode;

    @ApiModelProperty(value = "도로명", position = 7)
    private String roadBaseAddr;

    @ApiModelProperty(value = "도로명상세", position = 8)
    private String roadDetailAddr;

    @ApiModelProperty(value = "지번", position = 9)
    private String baseAddr;

    @ApiModelProperty(value = "지번상세", position = 10)
    private String detailAddr;

    @ApiModelProperty(value = "배송타입(TD,DS,AURORA)", position = 24, hidden = true)
    private String shipType;

    @ApiModelProperty(value="주문번호", position = 27)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "안심번호사용여부", position = 14)
    private String safetyUseYn;

    @ApiModelProperty(value = "처리자", position = 14)
    private String chgId;
}
