package kr.co.homeplus.escrowmng.order.model.ship;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "주문관리 > 상세 > 미수취 신청내용")
public class NoRcvShipInfoSetDto {

    @ApiModelProperty(value= "구매주문번호", position = 1)
    @NotNull(message = "구매주문번호")
    @Min(value = 1, message = "구매주문번호")
    private long purchaseOrderNo;

    @ApiModelProperty(value= "배송번호", position = 4)
    @NotNull(message = "배송번호")
    @Min(value = 1, message = "배송번호")
    private long bundleNo;

    @ApiModelProperty(value= "고객번호", position = 5)
    @NotNull(message = "고객번호")
    @Min(value = 1, message = "고객번호")
    private long userNo;

}