package kr.co.homeplus.escrowmng.claim.mapper.storeMultiCancel;

import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.storeMultiCancel.StoreMultiCancelListGetDto;
import kr.co.homeplus.escrowmng.claim.model.storeMultiCancel.StoreMultiCancelListSetDto;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;

@EscrowSlaveConnection
public interface StoreMultiCancelSlaveMapper {

    List<StoreMultiCancelListGetDto> selectStoreMultiCancelList(StoreMultiCancelListSetDto storeMultiCancelListSetDto);

}
