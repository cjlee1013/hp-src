package kr.co.homeplus.escrowmng.pg.service;

import static kr.co.homeplus.escrowmng.constants.Constants.USE_Y;
import static kr.co.homeplus.escrowmng.constants.ResourceRouteName.ITEM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.escrowmng.core.db.construct.TransactionManagerName;
import kr.co.homeplus.escrowmng.enums.ApiName;
import kr.co.homeplus.escrowmng.pg.mapper.PaymentPolicyManageMasterMapper;
import kr.co.homeplus.escrowmng.pg.mapper.PaymentPolicyManageSlaveMapper;
import kr.co.homeplus.escrowmng.pg.mapper.PgDivideMasterMapper;
import kr.co.homeplus.escrowmng.pg.model.ExcelItem;
import kr.co.homeplus.escrowmng.pg.model.ItemCategoryGetDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodTreeDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyApplyDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyApplySelectDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyHistDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyHistSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyManageDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyManageSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyMethodDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyMethodSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PgDivideManageDto;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import kr.co.homeplus.escrowmng.utils.SetParameter;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

@Slf4j
@Service
@RequiredArgsConstructor
public class PaymentPolicyManageService {
    private final PaymentPolicyManageSlaveMapper paymentPolicyManageSlaveMapper;
    private final PaymentPolicyManageMasterMapper paymentPolicyManageMasterMapper;
    private final PgDivideMasterMapper pgDivideMasterMapper;
    private final ResourceClient resourceClient;

    /**
     * 결제정책 마스터 조회
     * @param paymentPolicyManageSelectDto
     * @return
     * @throws Exception
     */
    public List<PaymentPolicyManageDto> getPaymentPolicyMng(PaymentPolicyManageSelectDto paymentPolicyManageSelectDto) throws Exception {
        List<String> categoryList = new ArrayList<>();

        // 대중소세 카테고리 하위부터 검색해서 schCateCd 에 세팅
        // 상위 카테고리 세팅 시 하위 카테고리 모두 categoryList 에 세팅
        if (EscrowString.isNotEmpty(paymentPolicyManageSelectDto.getSchDcateCd())) {
            paymentPolicyManageSelectDto.setSchCateCd(paymentPolicyManageSelectDto.getSchDcateCd());
        } else if (EscrowString.isNotEmpty(paymentPolicyManageSelectDto.getSchScateCd())) {
            paymentPolicyManageSelectDto.setSchCateCd(paymentPolicyManageSelectDto.getSchScateCd());
            categoryList = this.getCategoryList("4", paymentPolicyManageSelectDto.getSchScateCd());
        } else if (EscrowString.isNotEmpty(paymentPolicyManageSelectDto.getSchMcateCd())) {
            paymentPolicyManageSelectDto.setSchCateCd(paymentPolicyManageSelectDto.getSchMcateCd());
            categoryList = this.getCategoryList("3", paymentPolicyManageSelectDto.getSchMcateCd());
        } else if (EscrowString.isNotEmpty(paymentPolicyManageSelectDto.getSchLcateCd())) {
            paymentPolicyManageSelectDto.setSchCateCd(paymentPolicyManageSelectDto.getSchLcateCd());
            categoryList = this.getCategoryList("2", paymentPolicyManageSelectDto.getSchLcateCd());
        } else {
            if (EscrowString.isNotEmpty(paymentPolicyManageSelectDto.getSchKeyword())) {
                if (EscrowString.equals(paymentPolicyManageSelectDto.getSchType(), "itemNo")) {
                    paymentPolicyManageSelectDto.setSchCateCd(paymentPolicyManageSelectDto.getSchKeyword());
                    ItemCategoryGetDto itemCategoryGetDto = this.getItemCategory(paymentPolicyManageSelectDto.getSchKeyword());
                    if (EscrowString.isNotEmpty(itemCategoryGetDto)) {
                        categoryList.add(Integer.toString(itemCategoryGetDto.getLcateCd()));
                        categoryList.add(Integer.toString(itemCategoryGetDto.getMcateCd()));
                        categoryList.add(Integer.toString(itemCategoryGetDto.getScateCd()));
                        categoryList.add(Integer.toString(itemCategoryGetDto.getDcateCd()));
                    }
                }
            }
        }

        if (EscrowString.isNotEmpty(categoryList)) {
            paymentPolicyManageSelectDto.setCategoryList(categoryList);
        }

        return paymentPolicyManageSlaveMapper.selectPaymentPolicyMng(paymentPolicyManageSelectDto);
    }

    /**
     * 하위 카테고리 리스트 조회
     * @param depth
     * @param parentCateCd
     * @return
     */
    public List<String> getCategoryList(@RequestParam(name = "depth") String depth,
                                        @RequestParam(name = "parentCateCd") String parentCateCd) {
        List<SetParameter> setParameterList = Arrays.asList(
            SetParameter.create("depth", depth),
            SetParameter.create("parentCateCd", parentCateCd),
            SetParameter.create("dispYnArr", "Y|A"),
            SetParameter.create("useYn", USE_Y)
        );
        String apiUri = ApiName.GET_CATEGORY_FOR_SELECT_BOX.getApiUri() + EscrowString.getParameter(setParameterList);

        ResourceClientRequest<List<Map<String, Object>>> request = ResourceClientRequest.<List<Map<String, Object>>>getBuilder()
            .apiId(ITEM)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        List<Map<String, Object>> data = resourceClient.get(request, new TimeoutConfig()).getBody().getData();

        List<String> categoryList = new ArrayList<>();
        for (Map<String, Object> cate : data) {
            categoryList.add((String)cate.get("cateCd"));
        }

        return categoryList;
    }

    /**
     * 상품 카테고리 정보 조회
     * @param itemNo
     * @return
     */
    public ItemCategoryGetDto getItemCategory(@RequestParam(name = "itemNo") String itemNo) {
        List<SetParameter> setParameterList = Arrays.asList(
            SetParameter.create("itemNo", itemNo),
            SetParameter.create("mallType", "TD"),
            SetParameter.create("storeId", "0"),
            SetParameter.create("menuType", "basic")
        );
        String apiUri = ApiName.GET_DETAIL.getApiUri() + EscrowString.getParameter(setParameterList);

        ResourceClientRequest<ItemCategoryGetDto> request = ResourceClientRequest.<ItemCategoryGetDto>getBuilder()
            .apiId(ITEM)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ItemCategoryGetDto data = resourceClient.get(request, new TimeoutConfig()).getBody().getData();

        return data;
    }

    /**
     * 결제정책 결제수단 Tree 구조 조회
     * @param paymentPolicyMethodSelectDto
     * @return
     * @throws Exception
     */
    public List<PaymentMethodTreeDto> getPaymentPolicyMethodTree(PaymentPolicyMethodSelectDto paymentPolicyMethodSelectDto) throws Exception {
        List<PaymentMethodTreeDto> result = paymentPolicyManageSlaveMapper.selectPaymentPolicyMethodTree(paymentPolicyMethodSelectDto);
        return result;
    }

    /**
     * 결제정책 저장
     * @param paymentPolicyManageDto
     * @return
     * @throws Exception
     */
    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int savePaymentPolicyMng(PaymentPolicyManageDto paymentPolicyManageDto) throws Exception {
        int rtnVal = 0;

        try {
            // 결제정책 마스터 저장
            paymentPolicyManageMasterMapper.insertPaymentPolicyMng(paymentPolicyManageDto);

            // 결제정책 결제수단 저장
            if (EscrowString.isNotEmpty(paymentPolicyManageDto.getPaymentPolicyMethodDtoList())) {
                // 히스토리 저장
                paymentPolicyManageMasterMapper.insertPaymentPolicyHistMethod(paymentPolicyManageDto.getPolicyNo());
                // 기존 결제정책 결제수단 삭제
                paymentPolicyManageMasterMapper.deletePaymentPolicyMethod(paymentPolicyManageDto.getPolicyNo());
                // 신규 결제정책 결제수단 저장
                for (PaymentPolicyMethodDto method : paymentPolicyManageDto.getPaymentPolicyMethodDtoList()) {
                    method.setRegId(paymentPolicyManageDto.getRegId());
                    method.setPolicyNo(paymentPolicyManageDto.getPolicyNo());
                    rtnVal = paymentPolicyManageMasterMapper.insertPaymentPolicyMethod(method);
                }
            }

            // PG배분율 저장
            if (EscrowString.isNotEmpty(paymentPolicyManageDto.getPgDivideManageDtoList())) {
                // 정책번호 기준으로 기존 배분율 사용여부 'N' 처리
                pgDivideMasterMapper.updatePaymentPolicyPgDivideRateHistory(paymentPolicyManageDto.getPolicyNo());
                // 신규 PG배분율 저장
                for (PgDivideManageDto pg : paymentPolicyManageDto.getPgDivideManageDtoList()) {
                    pg.setRegId(paymentPolicyManageDto.getRegId());
                    pg.setChgId(paymentPolicyManageDto.getRegId());
                    pg.setSiteType(paymentPolicyManageDto.getSiteType());
                    pg.setPolicyNo(paymentPolicyManageDto.getPolicyNo());
                    pg.setApplyStartDt(paymentPolicyManageDto.getApplyStartDt());
                    pg.setApplyEndDt(paymentPolicyManageDto.getApplyEndDt());
                    rtnVal = pgDivideMasterMapper.insertPaymentPolicyPgDivideRate(pg);
                }
            }
            return rtnVal;

        } catch (Exception e) {
            log.error("error:", e);
            rtnVal = -1;
            return rtnVal;

        } finally {
            log.info("savePaymentPolicyMng-resultCount:" + rtnVal);
        }
    }

    /**
     * 결제정책 적용대상 조회
     * @param paymentPolicyApplySelectDtoDto
     * @return
     * @throws Exception
     */
    public List<PaymentPolicyApplyDto> getPaymentPolicyApplyList(PaymentPolicyApplySelectDto paymentPolicyApplySelectDtoDto) throws Exception {
        return paymentPolicyManageSlaveMapper.selectPaymentPolicyApplyList(paymentPolicyApplySelectDtoDto);
    }

    /**
     * 결제정책 적용대상 저장
     * @param paymentPolicyApplyDto
     * @return
     * @throws Exception
     */
    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int savePaymentPolicyApply(PaymentPolicyApplyDto paymentPolicyApplyDto) throws Exception {
        int rtnVal = 0;

        try {
            if (EscrowString.isNotEmpty(paymentPolicyApplyDto.getPaymentPolicyApplyDtoList())) {
                // 히스토리 저장
                paymentPolicyManageMasterMapper.insertPaymentPolicyHistApply(paymentPolicyApplyDto.getPolicyNo());
                // 기존 결제정책 적용대상 삭제
                paymentPolicyManageMasterMapper.deletePaymentPolicyApply(paymentPolicyApplyDto.getPolicyNo());
                // 신규 적용대상 저장
                for (PaymentPolicyApplyDto apply : paymentPolicyApplyDto.getPaymentPolicyApplyDtoList()) {
                    apply.setRegId(paymentPolicyApplyDto.getRegId());
                    rtnVal = paymentPolicyManageMasterMapper.insertPaymentPolicyApply(apply);
                }
            }
            return rtnVal;

        } catch (Exception e) {
            log.error("error:", e);
            rtnVal = -1;
            return rtnVal;

        } finally {
            log.info("savePaymentPolicyApply-resultCount:" + rtnVal);
        }
    }

    /**
     * 결제정책 히스토리 조회
     * @param paymentPolicyHistSelectDto
     * @return
     * @throws Exception
     */
    public List<PaymentPolicyHistDto> getPaymentPolicyHistory(PaymentPolicyHistSelectDto paymentPolicyHistSelectDto) throws Exception {
        // 대중소세 분류 하위부터 검색해서 schCateCd 에 세팅
        if (EscrowString.isNotEmpty(paymentPolicyHistSelectDto.getSchDcateCd())) {
            paymentPolicyHistSelectDto.setSchCateCd(paymentPolicyHistSelectDto.getSchDcateCd());
        } else if (EscrowString.isNotEmpty(paymentPolicyHistSelectDto.getSchScateCd())) {
            paymentPolicyHistSelectDto.setSchCateCd(paymentPolicyHistSelectDto.getSchScateCd());
        } else if (EscrowString.isNotEmpty(paymentPolicyHistSelectDto.getSchMcateCd())) {
            paymentPolicyHistSelectDto.setSchCateCd(paymentPolicyHistSelectDto.getSchMcateCd());
        } else if (EscrowString.isNotEmpty(paymentPolicyHistSelectDto.getSchLcateCd())) {
            paymentPolicyHistSelectDto.setSchCateCd(paymentPolicyHistSelectDto.getSchLcateCd());
        }

        return paymentPolicyManageSlaveMapper.selectPaymentPolicyHistory(paymentPolicyHistSelectDto);
    }

    /**
     * 상품유효확인 조회 - DMS I/F 상품목록을 조회
     * @param itemNoList
     * @return
     * @throws Exception
     */
    public List<ExcelItem> getItemValidCheck(List<String> itemNoList) throws Exception{
        return paymentPolicyManageSlaveMapper.selectItemValidCheck(itemNoList);
    }
}
