package kr.co.homeplus.escrowmng.pg.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodTreeDto;

@EscrowSlaveConnection
public interface PaymentMethodSlaveMapper {
    List<PaymentMethodDto> selectPaymentMethod(PaymentMethodSelectDto paymentMethodSelectDto);
    List<PaymentMethodDto> selectMethodCd(PaymentMethodSelectDto paymentMethodSelectDto);
    List<PaymentMethodTreeDto> selectPaymentMethodTreeParent(PaymentMethodSelectDto paymentMethodSelectDto);
    List<PaymentMethodTreeDto> selectPaymentMethodTreeChild(PaymentMethodSelectDto paymentMethodSelectDto);
    List<PaymentMethodDto> selectPaymentMethodByParam(PaymentMethodSelectDto paymentMethodSelectDto);
}
