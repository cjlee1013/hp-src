package kr.co.homeplus.escrowmng.delivery.service;

import java.util.List;
import kr.co.homeplus.escrowmng.delivery.mapper.StoreZipcodeSlaveMapper;
import kr.co.homeplus.escrowmng.delivery.model.StoreZipcodeModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreZipcodeSelectModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StoreZipcodeService {

    private final StoreZipcodeSlaveMapper storeZipcodeSlaveMapper;

    /**
     * 점포별 우편번호 조회
     * @param storeZipcodeSelectModel
     * @return
     */
    public List<StoreZipcodeModel> getStoreZipcodeList(StoreZipcodeSelectModel storeZipcodeSelectModel) {
        return storeZipcodeSlaveMapper.selectStoreZipcodeList(storeZipcodeSelectModel);
    }
}