package kr.co.homeplus.escrowmng.external.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "원거리배송 점포 slot")
public class RemoteShipManage {

    @ApiModelProperty(value = "우편번호", position = 1)
    @NotEmpty(message = "우편번호(5자리)")
    @Pattern(regexp = PatternConstants.ZIPCODE_PATTERN, message = "우편번호(5자리)")
    @AllowInput(types = {PatternConstants.NUM}, message = "우편번호(5자리)")
    private String zipcodeCd;

    @ApiModelProperty(value = "점포유형", position = 2)
    @Pattern(regexp = PatternConstants.STORE_TYPE_PATTERN, message="점포유형(HYPER,EXP,CLUB)")
    @NotEmpty(message = "점포유형(HYPER,EXP,CLUB)")
    private String storeType;

    @ApiModelProperty(value = "shift ID", position = 3)
    @NotEmpty(message = "shift ID")
    @AllowInput(types = {PatternConstants.NUM}, message = "shift ID")
    private String shiftId;

    @ApiModelProperty(value = "slot ID", position = 4)
    @NotEmpty(message = "slot ID")
    @AllowInput(types = {PatternConstants.NUM}, message = "slot ID")
    private String slotId;

    public String toString() {
        return "{zipcodeCd:" + zipcodeCd + ",storeType:" + storeType + ",shiftId:" + shiftId + ",slotId:" + slotId + "}";
    }
}
