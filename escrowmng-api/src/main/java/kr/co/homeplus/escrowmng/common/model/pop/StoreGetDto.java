package kr.co.homeplus.escrowmng.common.model.pop;

import lombok.Data;

/**
 * 점포 정보 Get_entry
 */
@Data
public class StoreGetDto {

	//점포ID
	private String 	storeId;

	//점포명
	private String 	storeNm;

}
