package kr.co.homeplus.escrowmng.claim.model.partner.ticketRental;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(description = "PO 이티켓/렌탈 취소 리스트 조회 DTO")
public class TicketRentalCancelListGetDto {

    @ApiModelProperty(value = "클레임번호")
    private String claimBundleNo;

    @ApiModelProperty(value = "클레임번호")
    private String claimNo;

    @ApiModelProperty(value = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(value = "배송번호")
    private String bundleNo;

    @ApiModelProperty(value = "클레임신청번호")
    private String claimReqNo;

    @ApiModelProperty(value = "상품 주문번호")
    private String orderItemNo;

    @ApiModelProperty(value = "처리상태")
    private String claimStatus;

    @ApiModelProperty(value = "처리상태코드")
    private String claimStatusCode;


    @ApiModelProperty(value = "신청일")
    private String requestDt;

    @ApiModelProperty(value = "신청사유")
    private String claimReasonType;

    @ApiModelProperty(value = "접수채널")
    private String requestId;

    @ApiModelProperty(value = "처리일")
    private String processDt;

    @ApiModelProperty(value = "승인채널")
    private String approveId;

    @ApiModelProperty(value = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    private String itemName;

    @ApiModelProperty(value = "옵션명")
    private String optItemNm;

    @ApiModelProperty(value = "신청수량")
    private String claimItemQty;

    @ApiModelProperty(value = "상품금액")
    private String itemPrice;

    @ApiModelProperty(value = "총 상품금액")
    private String totItemPrice;

    @ApiModelProperty(value = "거부사유")
    private String rejectReasonType;

    @ApiModelProperty(value = "구매자이름")
    private String buyerNm;

    @ApiModelProperty(value = "구매자연락처")
    private String buyerMobileNo;

    @ApiModelProperty(value = "수령인이름")
    private String receiverNm;

    @ApiModelProperty(value = "수령인연락처")
    private String shipMobileNo;

    @ApiModelProperty(value = "업체상품코드")
    private String sellerItemCd;

    @ApiModelProperty(value = "업체옵션코드")
    private String sellerOptCd;

    @ApiModelProperty(value = "관리코드")
    private String sellerManageCd;

}
