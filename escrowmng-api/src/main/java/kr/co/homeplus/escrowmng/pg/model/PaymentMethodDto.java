package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단관리 > 결제수단관리 DTO")
public class PaymentMethodDto {
    @ApiModelProperty(value = "수단코드")
    private String methodCd;
    @ApiModelProperty(value = "수단명")
    private String methodNm;
    @ApiModelProperty(value = "상위수단코드(신용카드,휴대폰결제등)")
    private String parentMethodCd;
    @ApiModelProperty(value = "상위수단명")
    private String parentMethodNm;
    @ApiModelProperty(value = "사이트유형(HOME:홈플러스,CLUB:더클럽)")
    private String siteType;
    @ApiModelProperty(value = "사이트유형명")
    private String siteTypeNm;
    @ApiModelProperty(value = "PG인증종류(안심:ANSIM,ISP:ISP,V3D:V3D 등)")
    private String pgCertifyKind;
    @ApiModelProperty(value = "inicis코드(PG명 향후 변경가능)")
    private String inicisCd;
    @ApiModelProperty(value = "tosspg코드(PG명 향후 변경가능)")
    private String tosspgCd;
    @ApiModelProperty(value = "사용여부")
    private String useYn;
    @ApiModelProperty(value = "플랫폼(전체,PC,MOBILE)")
    private String platform;
    @ApiModelProperty(value = "정렬순서")
    private int sortSeq;
    @ApiModelProperty(value = "이미지url")
    private String imgUrl;
    @ApiModelProperty(value = "이미지url모바일")
    private String imgUrlMobile;
    @ApiModelProperty(value = "이미지넓이")
    private int imgWidth;
    @ApiModelProperty(value = "이미지높이")
    private int imgHight;
    @ApiModelProperty(value = "등록자id")
    private String regId;
    @ApiModelProperty(value = "등록일자")
    private String regDt;
    @ApiModelProperty(value = "수정자id")
    private String chgId;
    @ApiModelProperty(value = "수정일자")
    private String chgDt;
}
