package kr.co.homeplus.escrowmng.claim.model.partner.ticketRental;


import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(description = "PO 이티켓/렌탈 취소 리스트 조회 DTO")
public class TicketRentalCancelListSetDto {

}
