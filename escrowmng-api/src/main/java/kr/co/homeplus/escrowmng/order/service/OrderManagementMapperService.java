package kr.co.homeplus.escrowmng.order.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.escrowmng.order.model.management.OrderHistoryListDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchClaimDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchDetailDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchPaymentDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchPriceDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchProdDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchShipDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchGetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchSetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchSubstitutionDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchTckGetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchTckSetDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.MultiOrderShipAddrInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderCashReceiptDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderCouponDiscountDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderDelayShipInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderGiftInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderPartnerInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderPromoInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderSaveInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderShipAddrInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderShipTotalInfoDto;
import kr.co.homeplus.escrowmng.order.model.ship.MultiOrderShipAddrEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipInfoGetDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipInfoSetDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipSetGto;
import kr.co.homeplus.escrowmng.order.model.ship.OrderShipAddrEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.OrderShipOriginInfo;
import kr.co.homeplus.escrowmng.order.model.ship.StoreShipMemoDto;
import kr.co.homeplus.escrowmng.order.model.ship.StoreShipMsgDto;

public interface OrderManagementMapperService {

    List<OrderSearchGetDto> getOrderManagementInfo(OrderSearchSetDto orderSearchSetDto);

    OrderSearchProdDto getOrderDetailByUser(long purchaseOrderNo);

    OrderSearchPriceDto getOrderDetailByPrice(long purchaseOrderNo);

    List<OrderSearchDetailDto> getOrderDetailByPurchase(long purchaseOrderNo);

    List<OrderSearchDetailDto> getMultiOrderDetailByPurchase(long purchaseOrderNo);

    List<OrderSearchPaymentDto> getOrderDetailByPayment(long purchaseOrderNo);

    List<OrderSearchShipDto> getOrderDetailByShipping(long purchaseOrderNo, String mallType);

    List<OrderSearchShipDto> getMultiOrderDetailByShipping(long purchaseOrderNo);

    List<OrderSearchClaimDto> getOrderDetailByClaim(long purchaseOrderNo);

    List<OrderShipAddrInfoDto> getOrderShipAddrInfo(long bundleNo);

    List<MultiOrderShipAddrInfoDto> getMultiOrderShipAddrInfo(long bundleNo);

    List<OrderShipTotalInfoDto> getOrderShipTotalInfo(long bundleNo, String shipStatus);

    Boolean modifyShipAddrChange(OrderShipAddrEditDto shipAddrEditDto);

    Boolean modifyMultiOrderShipAddrInfo(MultiOrderShipAddrEditDto multiOrderShipAddrEditDto);

    Boolean addNoRcvShipInfo(NoRcvShipSetGto noRcvShipSetGto) throws Exception;

    Boolean modifyNoRcvShipInfo(NoRcvShipEditDto noRcvShipEditDto);

    NoRcvShipInfoGetDto getNoRcvShipInfo(NoRcvShipInfoSetDto noRcvShipInfoSetDto);

    LinkedHashMap<String, Object> getNoRcvShipRegMakeData(long bundleNo);

    HashMap<String, Object> getNoReceiveCheck(long bundleNo);

    OrderDelayShipInfoDto getDelayShipInfo(long shipNo);

    OrderPartnerInfoDto getOrderPartnerInfo(String partnerId, long bundleNo);

    List<OrderPromoInfoDto> getOrderPromoPopInfo(long purchaseOrderNo);

    List<OrderCouponDiscountDto> getOrderCouponDiscountPopInfo(long purchaseOrderNo);

    List<OrderGiftInfoDto> getOderGiftPopInfo(long purchaseOrderNo);

    List<OrderSearchSubstitutionDto> getOrderSubstitutionInfo(long purchaseOrderNo);

    LinkedHashMap<String, Object> getSafetyIssueInfo(long bundleNo);

    List<OrderSaveInfoDto> getOrderSavePopInfo(long purchaseOrderNo);

    List<LinkedHashMap<String, Object>> getShippingPresentStatus(long purchaseOrderNo);

    String getCmbnClaimInfo(long purchaseOrderNo);

    List<OrderSearchTckGetDto> getOrderManagementInfoTck(OrderSearchTckSetDto orderSearchSetDto);

    StoreShipMemoDto getStoreShipMemoInfo(long purchaseOrderNo, long bundleNo);

    StoreShipMsgDto getStoreShipMsgInfo(long purchaseOrderNo);

    Boolean setStoreShipMemoInfo(StoreShipMemoDto memoDto);

    OrderShipOriginInfo getOriginPurchaseBundleInfo(long purchaseOrderNo);

    List<OrderHistoryListDto> getOrderHistoryList(long purchaseOrderNo);

    OrderCashReceiptDto getOrderCashReceiptInfo(long purchaseOrderNo);

    String getOrderTypeCheck(long purchaseOrderNo);

    /**
     * 미수취 철회시 배송정보 확인
     *
     * @param purchaseOrderNo
     * @param bundleNo
     * @return
     */
    LinkedHashMap<String, Object> getNonShipCompleteInfo(long purchaseOrderNo, long bundleNo);
}
