package kr.co.homeplus.escrowmng.claim.model.entity;

public class ClaimExchShippingEntity {
    // 클레임 교환배송 번호
    public String claimExchShippingNo;
    // 클레임 반품수거 번호
    public String claimPickShippingNo;
    // 클레임 묶음번호(환불:클레임 번호)
    public String claimBundleNo;
    // 교환배송수단(C:택배배송 P:우편배송  D:업체직배송 N:배송없음)
    public String exchShipType;
    // 교환 배송_상태(D0: 상품출고대기 D1: 상품출고(송장등록) D2: 배송중 D3: 배송완료)
    public String exchStatus;
    // 교환배송_택배사코드
    public String exchDlvCd;
    // 교환배송_송장번호
    public String exchInvoiceNo;
    // 교환배송_송장번호 등록자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)
    public String exchInvoiceRegId;
    // 배송추적요청여부
    public String exchTrackingYn;
    // 배송추적응답여부(R:추적대기,T:추적여부수신(스윗트래커콜백수신시),F1:실패-필수 파라미터가 빈 값이거나 없음,F2:실패-운송장번호 유효성 검사 실패,F3:이미 등록된 운송장번호,F4:지원하지 않는 택배사코드,F9:시스템오류)
    public String exchTrackingResult;
    // 교환배송_이름
    public String exchReceiverNm;
    // 교환배송_우편번호
    public String exchZipcode;
    // 교환배송_주소1
    public String exchBaseAddr;
    // 교환배송_주소2
    public String exchDetailAddr;
    // 교환배송_도로명 기본주소
    public String exchRoadBaseAddr;
    // 교환배송_도로명 상세주소
    public String exchRoadDetailAddr;
    // 교환배송_휴대폰
    public String exchMobileNo;
    // 교환배송_연락처
    public String exchPhone;
    // 교환배송_메모
    public String exchMemo;
    // 업체직배송의 배송예정일 최초등록일
    public String exchD0Dt;
    // 교환배송_송장등록일(업체직배송의 경우 배송예정일)
    public String exchD1Dt;
    // 교환배송_시작일
    public String exchD2Dt;
    // 교환배송_완료일
    public String exchD3Dt;
    // 안심번호사용유무
    public String safetyUseYn;
    // 고객번호
    public String userNo;
    // 파트너ID
    public String partnerId;
    // 등록자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)
    public String regId;
    // 등록일
    public String regDt;
    // 수정자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)
    public String chgId;
    // 수정일
    public String chgDt;
}
