package kr.co.homeplus.escrowmng.pg.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.pg.model.ExcelItem;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodTreeDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyApplyDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyApplySelectDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyHistDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyHistSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyManageDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyManageSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyMethodSelectDto;
import kr.co.homeplus.escrowmng.pg.service.PaymentPolicyManageService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pg/pgDivide")
@Api(tags = "결제관리 > PG 배분율 관리 > 결제정책 관리")
public class PaymentPolicyManageController {
    private final PaymentPolicyManageService paymentPolicyManageService;

    /**
     * 결제정책 마스터 조회
     */
    @GetMapping(value = "/getPaymentPolicyMngList")
    @ApiOperation(value = "결제정책 마스터 조회", response = PaymentPolicyManageDto.class)
    public ResponseObject getPaymentPolicyMng(@Valid PaymentPolicyManageSelectDto paymentPolicyManageSelectDto) throws Exception {
        List<PaymentPolicyManageDto> PaymentPolicyManageDtoList = paymentPolicyManageService.getPaymentPolicyMng(paymentPolicyManageSelectDto);
        return ResponseObject.Builder.<List<PaymentPolicyManageDto>>builder().data(PaymentPolicyManageDtoList).build();
    }

    /**
     * 결제정책 결제수단 Tree 구조 조회
     */
    @GetMapping(value = "/getPaymentPolicyMethodTree")
    @ApiOperation(value = "결제정책 결제수단 Tree 구조 조회", response = PaymentMethodTreeDto.class)
    public ResponseObject getPaymentPolicyMethodTree(@Valid PaymentPolicyMethodSelectDto paymentPolicyMethodSelectDto) throws Exception {
        return ResourceConverter.toResponseObject(paymentPolicyManageService.getPaymentPolicyMethodTree(paymentPolicyMethodSelectDto));
    }

    /**
     * 결제정책 저장
     */
    @PostMapping(value = "/savePaymentPolicyMng")
    @ApiOperation(value = "결제정책 저장", response = Integer.class, hidden = true)
    public ResponseObject savePaymentPolicyMng(@Valid @RequestBody PaymentPolicyManageDto paymentPolicyManageDto) throws Exception {
        return ResponseObject.Builder.<Integer>builder().data(paymentPolicyManageService.savePaymentPolicyMng(paymentPolicyManageDto)).build();
    }

    /**
     * 결제정책 적용대상 조회
     */
    @GetMapping(value = "/getPaymentPolicyApplyList")
    @ApiOperation(value = "결제정책 적용대상 조회", response = PaymentPolicyApplyDto.class)
    public ResponseObject getPaymentPolicyApplyList(@Valid PaymentPolicyApplySelectDto paymentPolicyApplySelectDtoDto) throws Exception {
        List<PaymentPolicyApplyDto> paymentPolicyApplyDtoList = paymentPolicyManageService.getPaymentPolicyApplyList(paymentPolicyApplySelectDtoDto);
        return ResponseObject.Builder.<List<PaymentPolicyApplyDto>>builder().data(paymentPolicyApplyDtoList).build();
    }

    /**
     * 결제정책 적용대상 저장
     */
    @PostMapping(value = "/savePaymentPolicyApply")
    @ApiOperation(value = "결제정책 적용대상 저장", response = Integer.class, hidden = true)
    public ResponseObject savePaymentPolicyApply(@Valid @RequestBody PaymentPolicyApplyDto paymentPolicyApplyDto) throws Exception {
        return ResponseObject.Builder.<Integer>builder().data(paymentPolicyManageService.savePaymentPolicyApply(paymentPolicyApplyDto)).build();
    }

    /**
     * 결제정책 히스토리 조회
     */
    @GetMapping(value = "/getPaymentPolicyHistory")
    @ApiOperation(value = "결제정책 히스토리 조회", response = PaymentPolicyHistDto.class)
    public ResponseObject getPaymentPolicyHistory(@Valid PaymentPolicyHistSelectDto paymentPolicyHistSelectDto) throws Exception {
        List<PaymentPolicyHistDto> paymentPolicyHistDtoList = paymentPolicyManageService.getPaymentPolicyHistory(paymentPolicyHistSelectDto);
        return ResponseObject.Builder.<List<PaymentPolicyHistDto>>builder().data(paymentPolicyHistDtoList).build();
    }

    /**
     * 상품유효확인 조회
     */
    @PostMapping(value = "/getItemValidCheck")
    @ApiOperation(value = "상품유효확인 조회", response = ExcelItem.class)
    public ResponseObject getItemValidCheck(@RequestBody List<String> itemNoList) throws Exception {
        return ResponseObject.Builder.<List<ExcelItem>>builder().data(paymentPolicyManageService.getItemValidCheck(itemNoList)).build();
    }
}
