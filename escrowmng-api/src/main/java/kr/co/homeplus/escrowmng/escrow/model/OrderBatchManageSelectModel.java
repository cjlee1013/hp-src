package kr.co.homeplus.escrowmng.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "주문 배치 관리 select model")
@Getter
@Setter
public class OrderBatchManageSelectModel {
    @ApiModelProperty(value = "사용여부")
    private String schUseYn;

    @ApiModelProperty(value = "검색어")
    private String schKeyword;
}
