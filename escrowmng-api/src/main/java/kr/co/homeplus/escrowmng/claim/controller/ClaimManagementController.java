package kr.co.homeplus.escrowmng.claim.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.claim.model.ClaimRequestSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimShippingSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimShippingStatusSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimAllCancelGetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimAllCancelSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimOrderShipMultiCancelGetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimOrderShipMultiCancelSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimTicketMultiCancelGetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimTicketMultiCancelSetDto;
import kr.co.homeplus.escrowmng.claim.service.ClaimOrderCancelService;
import kr.co.homeplus.escrowmng.claim.service.ClaimShippingService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/claim")
@RequiredArgsConstructor
@Slf4j
@Api(tags = "클레임 관리(등록/취소 등) ")
public class ClaimManagementController {

    private final ClaimOrderCancelService orderCancelService;

    private final ClaimShippingService claimShippingService;


    @PostMapping("/claimCancelReg")
    @ApiOperation(value = "주문취소 등록", response = String.class)
    public ResponseObject<String> getClaimPrevOrderCancelInfo(@RequestBody @Valid ClaimRegSetDto claimRegSetDto) throws Exception {
        claimRegSetDto.setIsMultiOrder(Boolean.FALSE);
        return orderCancelService.orderCancelAction(claimRegSetDto);
    }

    @PostMapping("/claimMultiCancelReg")
    @ApiOperation(value = "주문취소 등록", response = String.class)
    public ResponseObject<String> getClaimPrevMultiOrderCancelInfo(@RequestBody @Valid ClaimRegSetDto claimRegSetDto) throws Exception {
        claimRegSetDto.setIsMultiOrder(Boolean.TRUE);
        return orderCancelService.orderCancelAction(claimRegSetDto);
    }

    @PostMapping("/reqChange")
    @ApiOperation(value = "클레임 상태 변경 요청", response = Object.class)
    public ResponseObject<Object> reqChange(@RequestBody @Valid ClaimRequestSetDto reqDto) throws Exception {
        return orderCancelService.requestChangeByClaim(reqDto);
    }

    @PostMapping("/reqClaimShippingChange")
    @ApiOperation(value = "클레임 교환/반품 배송지정보 변경 요청", response = Object.class)
    public ResponseObject<String> reqClaimShippingChange(@RequestBody @Valid ClaimShippingSetDto reqDto) throws Exception {
        return claimShippingService.reqClaimShippingChange(reqDto);
    }

    @PostMapping("/reqClaimShippingStatus")
    @ApiOperation(value = "수거 상태 변경 요청", response = Object.class)
    public ResponseObject<Boolean> reqClaimShippingStatus(@RequestBody @Valid ClaimShippingStatusSetDto reqDto) throws Exception {
        return claimShippingService.reqClaimShippingStatus(reqDto);
    }

    @PostMapping("/claimAllCancelReq")
    @ApiOperation(value = "일괄취소요청", response = ClaimAllCancelGetDto.class)
    public ResponseObject<ClaimAllCancelGetDto> reqClaimAllCancel(@RequestBody ClaimAllCancelSetDto setDto) throws Exception {
        return orderCancelService.orderAllCancelAction(setDto);
    }


    @PostMapping("/ticketAllCancelReq")
    @ApiOperation(value = "티켓일괄취소요청", response = ClaimAllCancelGetDto.class)
    public ResponseObject<ClaimTicketMultiCancelGetDto> ticketAllCancelReq(@RequestBody ClaimTicketMultiCancelSetDto setDto) throws Exception {
        return orderCancelService.ticketAllCancelAction(setDto);
    }

    @PostMapping("/orderShipMultiCancelReq")
    @ApiOperation(value = "PO-발주/발송관리 일괄취소요청", response = ClaimAllCancelGetDto.class)
    public ResponseObject<ClaimOrderShipMultiCancelGetDto> orderShipMultiCancelReq(@RequestBody ClaimOrderShipMultiCancelSetDto setDto) throws Exception {
        return orderCancelService.orderShipMultiCancelReq(setDto);
    }

}
