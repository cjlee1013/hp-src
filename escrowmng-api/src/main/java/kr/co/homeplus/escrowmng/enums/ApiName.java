package kr.co.homeplus.escrowmng.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ApiName {
    NONE("")

    /* escrow-api */
    , UPDATE_SAFETY_NO("/safetyissue/updateSafetyNo")
    , UPDATE_CLAIM_SAFETY_NO("/safetyissue/updateClaimSafetyNo")
    , UPDATE_MULTI_SAFETY_NO("/safetyissue/updateMultiSafetyNo")

    /* item-api */
    , GET_CATEGORY_FOR_SELECT_BOX("/common/CategoryMng/getCategoryForSelectBox")
    , GET_DETAIL("/item/getDetail")
    , GET_COMMON_CODE("/common/getCodeList")

    /* message-api */
    , SEND_SMS("/send/sms")
    ;

    private String apiUri;
}
