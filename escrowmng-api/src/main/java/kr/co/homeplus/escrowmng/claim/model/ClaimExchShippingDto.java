package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimExchShippingDto {

    @ApiModelProperty(value = "클레임 교환접수 번호", position = 1)
    public String claimExchShippingNo;

    @ApiModelProperty(value = "교환-이름", position = 2)
    public String exchReceiverNm;

    @ApiModelProperty(value = "교환-휴대폰", position = 3)
    public String exchMobileNo;

    @ApiModelProperty(value = "교환 우편번호", position = 4)
    public String exchZipcode;

    @ApiModelProperty(value = "교환 기본주소", position = 5)
    public String exchAddr;

    @ApiModelProperty(value = "교환 상세주소", position = 6)
    public String exchAddrDetail;

    @ApiModelProperty(value = "교환 송장번호", position = 7)
    public String exchInvoiceNo;

    @ApiModelProperty(value = "교환배송수단(C:택배배송 P:우편배송  D:업체직배송 N:배송없음)", position = 8)
    public String exchShipType;

    @ApiModelProperty(value = "교환배송수단코드(C:택배배송 P:우편배송  D:업체직배송 N:배송없음)", position = 8)
    public String exchShipTypeCode;

    @ApiModelProperty(value = "교환 배송_상태", position = 9)
    public String exchStatus;

    @ApiModelProperty(value = "교환 배송_상태 코드(D1: 상품출고(송장등록) D2: 배송중 D3: 배송완료)", position = 10)
    public String exchStatusCode;

    @ApiModelProperty(value = "교환배송 택배사코드", position = 11)
    public String exchDlvCd;

    @ApiModelProperty(value = "교환배송 택배사명", position = 12)
    public String exchDlv;

    @ApiModelProperty(value = "교환배송 메모", position = 13)
    public String exchMemo;

    @ApiModelProperty(value = "업체직배송의 배송예정일 최초등록일", position = 14)
    public String exchD0Dt;

    @ApiModelProperty(value = "교환배송 송장등록일(업체직배송의 경우 배송예정일)", position = 15)
    public String exchD1Dt;

    @ApiModelProperty(value = "교환배송 시작일", position = 16)
    public String exchD2Dt;

    @ApiModelProperty(value = "교환배송 완료일", position = 17)
    public String exchD3Dt;

    @ApiModelProperty(value="수거지 지번주소", position = 18)
    public String exchBaseAddr;

    @ApiModelProperty(value="안심번호 사용유무", position = 18)
    public String safetyUseYn;


}
