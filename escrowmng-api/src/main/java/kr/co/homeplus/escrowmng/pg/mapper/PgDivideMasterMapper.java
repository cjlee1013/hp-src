package kr.co.homeplus.escrowmng.pg.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.pg.model.PgDivideManageDto;
import org.apache.ibatis.annotations.Param;

@EscrowMasterConnection
public interface PgDivideMasterMapper {
    int insertPgDivideRate(PgDivideManageDto pgDivideManageDto);
    int updatePgDivideRateHistory(@Param("divideSeq") long divideSeq);
    int updatePaymentPolicyPgDivideRateHistory(@Param("policyNo") Long policyNo);
    int insertPaymentPolicyPgDivideRate(PgDivideManageDto pgDivideManageDto);
}
