package kr.co.homeplus.escrowmng.escrow.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.escrow.model.StoreAnytimeManageDto;
import kr.co.homeplus.escrowmng.escrow.model.StoreAnytimeManageSelectDto;
import kr.co.homeplus.escrowmng.escrow.model.StoreAnytimeShiftManageDto;
import org.apache.ibatis.annotations.Param;

@EscrowSlaveConnection
public interface StoreAnytimeSlaveMapper {
    List<StoreAnytimeManageDto> selectStoreAnytimeInfo(StoreAnytimeManageSelectDto storeAnytimeManageSelectDto);
    List<StoreAnytimeShiftManageDto> selectStoreAnytimeShiftInfo(@Param("storeId") int storeId);
}
