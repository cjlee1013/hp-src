package kr.co.homeplus.escrowmng.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "점포 shift 관리(옴니연동)")
public class StoreShiftManageModel {

    @ApiModelProperty(value = "점포ID")
    private String storeId;

    @ApiModelProperty(value = "배송일자", position = 1)
    private String shipDt;

    @ApiModelProperty(value = "배송요일(1:일요일,2:월요일~7:토요일)", position = 2)
    private String shipWeekday;

    @ApiModelProperty(value = "SHIFTID", position = 3)
    private String shiftId;

    @ApiModelProperty(value = "VAN명(VAN A,B,C조)", position = 4)
    private String vanNm;

    @ApiModelProperty(value = "배송시작시간(1000,2330)", position = 5)
    private String shiftShipStartTime;

    @ApiModelProperty(value = "배송종료시간(1000,2330)", position = 6)
    private String shiftShipEndTime;

    @ApiModelProperty(value = "(SHIFT실행 D-DAY) (0:배송일, 1:배송전일)", position = 7)
    private String shiftExecDay;

    @ApiModelProperty(value = "SHIFT마감시간(DMS실행시간)", position = 8)
    private String shiftCloseTime;

    @ApiModelProperty(value = "SHIFT사전마감시간(1차전송시간)", position = 9)
    private String shiftBeforeCloseTime;

    @ApiModelProperty(value = "배정 slot 수", position = 10)
    private int slotCnt;

    @ApiModelProperty(value = "반품/교환 가능여부", position = 11)
    private String returnYn;

    @ApiModelProperty(value = "점포명", position = 12)
    private String storeNm;

}