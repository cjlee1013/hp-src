package kr.co.homeplus.escrowmng.claim.model.external;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "PG 결제 취소 DTO")
public class ClaimPgCancelSetDto {

    @ApiModelProperty(value = "PG종류(PG종류:INICIS,TOSSPG)", position = 1)
    private String pgKind;

    @ApiModelProperty(value = "PG인증key값", position = 2)
    private String pgOid;

    @ApiModelProperty(value = "PG가맹점ID", position = 3)
    private String pgMid;

    @ApiModelProperty(value = "PG거래번호", position = 4)
    private String pgTid;

    @ApiModelProperty(value = "결제수단코드", position = 5)
    private String parentMethodCd;

    @ApiModelProperty(value = "취소유형(ALL:전체,PART:부분)", position = 6)
    private String cancelType;

    @ApiModelProperty(value = "취소사유", position = 7)
    private String cancelReason;

    @ApiModelProperty(value = "고객번호", position = 8)
    private long userNo;

    /** KG이니시스 */
    @ApiModelProperty(value = "요청IP", position = 9)
    private String clientIp;

    /** KG이니시스(부분취소) */
    @ApiModelProperty(value = "부분취소요청금액", position = 10)
    private long partCancelPrice;
    @ApiModelProperty(value = "부분취소남은금액", position = 11)
    private long partRemainPrice;

    @ApiModelProperty(value = "토스 결제 토큰", position = 12)
    private String payToken;

}
