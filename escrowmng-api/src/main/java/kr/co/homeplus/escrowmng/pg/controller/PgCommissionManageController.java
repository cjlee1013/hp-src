package kr.co.homeplus.escrowmng.pg.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.pg.model.PgCommissionManageDto;
import kr.co.homeplus.escrowmng.pg.model.PgCommissionManageSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PgCommissionTargetDto;
import kr.co.homeplus.escrowmng.pg.model.PgCommissionTargetSelectDto;
import kr.co.homeplus.escrowmng.pg.service.PgCommissionManageService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pg/paymentMethod")
@Api(tags = "결제관리 > 결제수단 관리 > PG 수수료 관리")
public class PgCommissionManageController {
    private final PgCommissionManageService pgCommissionManageService;

    /**
     * PG 수수료 마스터 조회
     */
    @GetMapping(value = "/getPgCommissionMngList")
    @ApiOperation(value = "PG 수수료 마스터 조회", response = PgCommissionManageDto.class)
    public ResponseObject getPgCommissionMngList(@Valid PgCommissionManageSelectDto pgCommissionManageSelectDto) throws Exception {
        List<PgCommissionManageDto> pgCommissionManageDtoList = pgCommissionManageService.getPgCommissionMngList(pgCommissionManageSelectDto);
        return ResourceConverter.toResponseObject(pgCommissionManageDtoList);
    }

    /**
     * PG 수수료 대상 조회
     */
    @GetMapping(value = "/getPgCommissionTargetList")
    @ApiOperation(value = "PG 수수료 대상 조회", response = PgCommissionTargetDto.class)
    public ResponseObject getPgCommissionTargetList(@Valid PgCommissionTargetSelectDto pgCommissionTargetSelectDto) throws Exception {
        List<PgCommissionTargetDto> pgCommissionTargetDtoList = pgCommissionManageService.getPgCommissionTargetList(pgCommissionTargetSelectDto);
        return ResourceConverter.toResponseObject(pgCommissionTargetDtoList);
    }

    /**
     * 중복 수수료 정책 조회
     */
    @GetMapping(value = "/checkDuplicateCommission")
    @ApiOperation(value = "중복 수수료 정책 조회", response = Integer.class)
    public ResponseObject<Integer> checkDuplicateCommission(@RequestParam(value = "pgKind") String pgKind,
            @RequestParam(value = "parentMethodCd") String parentMethodCd, @RequestParam(value = "startDt") String startDt,
            @RequestParam(value = "endDt") String endDt, @RequestParam(value = "pgCommissionMngSeq", required = false) String pgCommissionMngSeq
    ) throws Exception {
        PgCommissionManageSelectDto pgCommissionManageSelectDto = new PgCommissionManageSelectDto();
        pgCommissionManageSelectDto.setSchPgKind(pgKind);
        pgCommissionManageSelectDto.setSchParentMethodCd(parentMethodCd);
        pgCommissionManageSelectDto.setSchFromDt(startDt);
        pgCommissionManageSelectDto.setSchEndDt(endDt);
        pgCommissionManageSelectDto.setSchPgCommissionMngSeq(pgCommissionMngSeq);
        return ResourceConverter.toResponseObject(pgCommissionManageService.checkDuplicateCommission(pgCommissionManageSelectDto));
    }

    /**
     * PG 수수료 저장
     */
    @PostMapping(value = "/savePgCommission")
    @ApiOperation(value = "PG 수수료 저장", response = Integer.class)
    public ResponseObject<Integer> savePgCommission(@Valid @RequestBody PgCommissionManageDto pgCommissionManageDto) throws Exception {
        return ResourceConverter.toResponseObject(pgCommissionManageService.savePgCommission(pgCommissionManageDto));
    }
}
