package kr.co.homeplus.escrowmng.order.model.entity;

public class PurchaseGiftItemEntity {
    // 주문사은품상품순번
    public String purchaseGiftItemSeq;
    // 주문사은품번호
    public String purchaseGiftNo;
    // 상품번호
    public String itemNo;
}
