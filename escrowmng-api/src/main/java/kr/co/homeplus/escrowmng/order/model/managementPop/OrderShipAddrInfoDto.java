package kr.co.homeplus.escrowmng.order.model.managementPop;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문관리 > 상세 > 배송정보 > 배송정보수정 Dto")
public class OrderShipAddrInfoDto {

    @ApiModelProperty(value = "이름", position = 1)
    private String receiverNm;

    @ApiModelProperty(value = "점포명/파트너명", position = 2)
    private String storeNm;

    @ApiModelProperty(value = "연락처", position = 3)
    private String shipMobileNo;

    @ApiModelProperty(value = "개인통관부호", position = 4)
    private String personalOverseaNo;

    @ApiModelProperty(value = "안심번호신청여부", position = 5)
    private String safetyNm;

    @ApiModelProperty(value = "우편번호", position = 6)
    private String zipCode;

    @ApiModelProperty(value = "도로명", position = 7)
    private String roadBaseAddr;

    @ApiModelProperty(value = "도로명상세", position = 8)
    private String roadDetailAddr;

    @ApiModelProperty(value = "지번", position = 9)
    private String baseAddr;

    @ApiModelProperty(value = "지번상세", position = 10)
    private String detailAddr;

    @ApiModelProperty(value = "배송요청사항코드(새벽배송)", position = 11)
    private String auroraShipMsgCd;

    @ApiModelProperty(value = "배송요청사항(새벽배송)", position = 12)
    private String auroraShipMsg;

    @ApiModelProperty(value = "배송요청사항코드", position = 13)
    private String shipMsgCd;

    @ApiModelProperty(value = "배송요청사항", position = 14)
    private String shipMsg;

    @ApiModelProperty(value = "배송요청일", position = 15)
    private String slotShipDt;

    @ApiModelProperty(value = "배송시작시간", position = 16)
    private String slotShipStartTime;

    @ApiModelProperty(value = "배송완료시간", position = 17)
    private String slotShipEndTime;

    @ApiModelProperty(value = "슬롯번호", position = 18)
    private String slotId;

    @ApiModelProperty(value = "방문수령(위치)", position = 19)
    private String placeNm;

    @ApiModelProperty(value = "락커번호", position = 20)
    private String lockerNo;

    @ApiModelProperty(value = "비밀번호", position = 21)
    private String lockerPasswd;

    @ApiModelProperty(value = "운송번호", position = 22, hidden = true)
    private long shipNo;

    @ApiModelProperty(value = "배송번호", position = 23, hidden = true)
    private long bundleNo;

    @ApiModelProperty(value = "배송타입(TD,DS,AURORA)", position = 24, hidden = true)
    private String shipType;

    @ApiModelProperty(value="수거 시작시간", position = 25)
    public String startTime;

    @ApiModelProperty(value="수거 종료시간", position = 26)
    public String endTime;

    @ApiModelProperty(value="주문번호", position = 27)
    private long purchaseOrderNo;

    @ApiModelProperty(value="주문타입", position = 28)
    private String orderType;

    @ApiModelProperty(value = "애니타입슬롯여부")
    private String anytimeSlotYn;
}
