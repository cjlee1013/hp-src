package kr.co.homeplus.escrowmng.claim.service;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.ClaimInfoListGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimRequestSetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimApproveSetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerBoardCountGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerDetailGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerDetailItem;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerExchangeListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListSetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerDetailListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerReturnListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.PartnerMainBoardClaimRequestCountGetDto;
import kr.co.homeplus.escrowmng.enums.OrderCode;
import kr.co.homeplus.escrowmng.order.factory.OrderResponseFactory;
import kr.co.homeplus.escrowmng.utils.EscrowDate;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClaimPartnerService {


    private final ClaimPartnerMapperService partnerMapperService;

    private final ClaimOrderCancelService claimOrderCancelService;

    private final ClaimMapperService claimMapperService;



    /**
     * PO 클레임 신청/대기/완료 건수
     *
     * @param partnerId
     * @param claimType
     * @return PO 클레임 신청/대기/완료 건수
     */
    public ResponseObject<ClaimPartnerBoardCountGetDto> getPOClaimBoardCount(String partnerId, String claimType) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, partnerMapperService.getPOClaimBoardCount(partnerId, claimType));
    }


    /**
     * PO 클레임 신청/대기/완료 건수
     *
     * @param partnerId
     * @return PO 클레임 신청/대기/완료 건수
     */
    public ResponseObject<PartnerMainBoardClaimRequestCountGetDto> getPOMainClaimRequestCount(String partnerId) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, partnerMapperService.getPOMainClaimRequestCount(partnerId));
    }


    /**
     * PO 클레임 취소 리스트
     *
     * @param claimCancelListSetDto
     * @return PO 클레임 신청/대기/완료 건수
     */
    public ResponseObject<List<ClaimPartnerListGetDto>> getPOClaimList(
        ClaimPartnerListSetDto claimCancelListSetDto) {
        List<ClaimPartnerListGetDto> claimList = partnerMapperService.getPOClaimList(claimCancelListSetDto);

        if (ObjectUtils.isNotEmpty(claimList)) {
            for (ClaimPartnerListGetDto next : claimList) {

                //클레임 완료 건 중 클레임 완료 7일 후 부터 마스킹처리
                if(next.getClaimStatusCode().equalsIgnoreCase("C3")){
                    if(EscrowDate.compareDateTimeStr(EscrowDate.getCurrentYmdHis(), EscrowDate.plusDays(next.getCompleteDt(),7)) > 0){
                        next.setBuyerNm(PrivacyMaskingUtils.maskingUserName(next.getBuyerNm()));
                        next.setBuyerMobileNo(PrivacyMaskingUtils.maskingPhone(next.getBuyerMobileNo()));
                        next.setReceiverNm(PrivacyMaskingUtils.maskingUserName(next.getReceiverNm()));
                        next.setShipMobileNo(PrivacyMaskingUtils.maskingPhone(next.getShipMobileNo()));
                    }
                }
            }
        }

        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimList);
    }

    public ResponseObject<Integer> getPOClaimListCnt(ClaimPartnerListSetDto claimCancelListSetDto){
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, partnerMapperService.getPOClaimListCnt(claimCancelListSetDto));
    }

    /**
     * PO 클레임 상세정보
     * 주문에 신청된 클레임들을 조회한다.
     *
     * @param purchaseOrderNo
     * @return PO 클레임 상세정보
     */
    public ResponseObject<List<ClaimPartnerDetailListGetDto>> getPOClaimDetailInfoList(long purchaseOrderNo, long bundleNo) {

        /* 1.클레임 상세 정보 조회 */
        List<ClaimPartnerDetailListGetDto> claimDetailList = partnerMapperService.getPOClaimDetailList(purchaseOrderNo, bundleNo);


        /* 2.클레임 상품 리스트 조회 */
        for(ClaimPartnerDetailListGetDto claimDetail : claimDetailList){
            List<ClaimPartnerDetailItem> claimItemList = partnerMapperService.getPOClaimDetailItemList(claimDetail.getClaimBundleNo());

            if(claimItemList != null){
                claimDetail.setClaimDetailItemList(claimItemList);
            }
        }

        /* return */
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimDetailList);
    }

    /**
     * PO 클레임 상세정보
     * 클레임상세정보 단건 조회
     *
     * @param claimBundleNo
     * @return PO 클레임 상세정보
     */
    public ResponseObject<ClaimPartnerDetailGetDto> getPOClaimDetailInfo(long claimBundleNo, String claimType) {

        /* 1.클레임 상세 정보 조회 */
        ClaimPartnerDetailGetDto claimDetail = partnerMapperService.getPOClaimDetail(claimBundleNo);


        /* 2.클레임 상품 리스트 조회 */
        List<ClaimPartnerDetailItem> claimItemList = partnerMapperService.getPOClaimDetailItemList(claimBundleNo);
        if(claimItemList != null){
            claimDetail.setClaimDetailItemList(claimItemList);
        }

        switch (claimType) {
            case "R" :
                claimDetail.setClaimPickShippingDto(claimMapperService.getClaimPickShippingInfo(claimBundleNo));
                break;
            case "X" :
                claimDetail.setClaimPickShippingDto(claimMapperService.getClaimPickShippingInfo(claimBundleNo));
                claimDetail.setClaimExchShippingDto(claimMapperService.getClaimExchShippingInfo(claimBundleNo));
                break;
        }

        /* return */
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimDetail);
    }


    /**
     * PO 취소 승인
     *
     * @param claimApproveSetDto
     * @return 취소승인 성공 건수
     * **/
    public ResponseObject<Integer> requestClaimApprove(ClaimApproveSetDto claimApproveSetDto) throws Exception {
        int result = 0;
        for(ClaimRequestSetDto  claimRequestSetDto : claimApproveSetDto.getClaimRequestSetList()){
            claimRequestSetDto.setClaimReqType("CA");
            claimRequestSetDto.setRegId("PARTNER");
            //취소 승인 요청
            try{
                ResponseObject<Object> resultObject = claimOrderCancelService.requestChangeByClaim(claimRequestSetDto);

                //취소 승인 성공시 result 값 증가
                if(resultObject.getReturnCode().equalsIgnoreCase("0000")){
                    result++;
                }

            }catch (Exception e){
                log.info("PO취소 승인 실패 claimBundleNo : {}", claimRequestSetDto.getClaimBundleNo(), e);
            }
        }
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, result);
    }
}
