package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimPickShippingDto {

    @ApiModelProperty(value = "클레임 반품수거 번호", position = 1)
    public String claimPickShippingNo;

    @ApiModelProperty(value = "수거지-이름", position = 2)
    public String pickReceiverNm;

    @ApiModelProperty(value = "수거지-휴대폰", position = 3)
    public String pickMobileNo;

    @ApiModelProperty(value = "수거지 우편번호", position = 4)
    public String pickZipcode;

    @ApiModelProperty(value = "수거지 기본주소", position = 5)
    public String pickAddr;

    @ApiModelProperty(value = "수거지 샹세주소", position = 6)
    public String pickAddrDetail;

    @ApiModelProperty(value = "수거배송수단", position = 7)
    public String pickShipType;

    @ApiModelProperty(value = "수거배송수단코드", position = 7)
    public String pickShipTypeCode;

    @ApiModelProperty(value = "수거택배사코드", position = 8)
    public String pickDlvCd;

    @ApiModelProperty(value = "수거택배사명", position = 9)
    public String pickDlv;

    @ApiModelProperty(value = "수거송장번호", position = 10)
    public String pickInvoiceNo;

    @ApiModelProperty(value = "수거접수타입", position = 11)
    public String pickRegisterType;

    @ApiModelProperty(value = "수거메모", position = 12)
    public String pickMemo;

    @ApiModelProperty(value="수거상태", position = 13)
    public String pickStatus;

    @ApiModelProperty(value="수거상태코드", position = 14)
    public String pickStatusCode;

    @ApiModelProperty(value="수거일자", position = 15)
    public String shipDt;

    @ApiModelProperty(value="수거shiftID", position = 16)
    public String shiftId;

    @ApiModelProperty(value="수거slotID", position = 17)
    public String slotId;

    @ApiModelProperty(value="수거 예약접수자", position = 18)
    public String pickP0Id;

    @ApiModelProperty(value="수거 예약일(업체직배송인 경우 수거예정일 최초등록일)", position = 19)
    public String pickP0Dt;

    @ApiModelProperty(value="수거 송장등록일(업체직배송의 경우 수거예정일)", position = 20)
    public String pickP1Dt;

    @ApiModelProperty(value="수거 시작일", position = 21)
    public String pickP2Dt;

    @ApiModelProperty(value="수거 완료일", position = 22)
    public String pickP3Dt;

    @ApiModelProperty(value="점포명", position = 23)
    public String storeNm;

    @ApiModelProperty(value="수거지 지번주소", position = 24)
    public String pickBaseAddr;

    @ApiModelProperty(value="배송타입", position = 25)
    public String shipType;

    @ApiModelProperty(value="수거 장소", position = 26)
    public String placeAddrExtnd;

    @ApiModelProperty(value="수거 시작시간", position = 27)
    public String startTime;

    @ApiModelProperty(value="수거 종료시간", position = 28)
    public String endTime;

    @ApiModelProperty(value="수거방법", position = 29)
    public String shipMethod;

    @ApiModelProperty(value="수거 요일시간", position = 30)
    public String slotShipDateTime;

    @ApiModelProperty(value="안심번호 사용유무", position = 30)
    public String safetyUseYn;
}
