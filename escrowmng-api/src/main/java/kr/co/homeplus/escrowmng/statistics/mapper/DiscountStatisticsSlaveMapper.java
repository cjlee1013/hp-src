package kr.co.homeplus.escrowmng.statistics.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowBatchConnection;
import kr.co.homeplus.escrowmng.statistics.model.DiscountStatistics;
import kr.co.homeplus.escrowmng.statistics.model.DiscountStatisticsSelect;

@EscrowBatchConnection
public interface DiscountStatisticsSlaveMapper {

    List<DiscountStatistics> selectDiscountStatisticsList(DiscountStatisticsSelect discountStatisticsSelect);

    List<DiscountStatistics> selectDiscountStatisticsDetailList(DiscountStatisticsSelect discountStatisticsSelect);
}