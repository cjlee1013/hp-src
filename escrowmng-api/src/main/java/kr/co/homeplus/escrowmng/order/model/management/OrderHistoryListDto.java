package kr.co.homeplus.escrowmng.order.model.management;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class OrderHistoryListDto {

    @ApiModelProperty(value = "처리일시", position = 1)
    public String historyDt;

    @ApiModelProperty(value = "처리자", position = 2)
    public long bundleNo;

    @ApiModelProperty(value = "처리구분", position = 3)
    public String historyDetailType;

    @ApiModelProperty(value = "상세내역", position = 4)
    public String historyDetailDesc;

    @ApiModelProperty(value = "처리자", position = 5)
    public String regId;


}
