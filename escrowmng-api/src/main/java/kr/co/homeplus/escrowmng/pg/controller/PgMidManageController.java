package kr.co.homeplus.escrowmng.pg.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.pg.model.PgMidManageModel;
import kr.co.homeplus.escrowmng.pg.service.PgMidManageService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pg/paymentMethod")
@Api(tags = "결제관리 > 결제수단관리 > PG상점ID관리")
public class PgMidManageController {

    private final PgMidManageService pgMidManageService;

    @GetMapping("/getPgMidManage")
    @ApiOperation(value = "PG상점ID 조회", response = PgMidManageModel.class)
    public ResponseObject getPgMidManage(
        @ApiParam(name = "pgKind", value = "PG사", required = true) @RequestParam String pgKind
        , @ApiParam(name = "useYn", value = "사용여부", required = true) @RequestParam String useYn
    ) {
        return ResourceConverter.toResponseObject(pgMidManageService.getPgMidManage(pgKind, useYn));
    }

    @PostMapping("/savePgMidManage")
    @ApiOperation(value = "PG상점ID 등록")
    public ResponseObject savePgMidManage(@Valid @RequestBody PgMidManageModel pgMidManageModel) {
        return ResourceConverter.toResponseObject(pgMidManageService.savePgMidManage(pgMidManageModel));
    }
}