package kr.co.homeplus.escrowmng.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MessageTemplate {

    CHECK_HOMEPLUS_ORDER("S11253", "홈플러스 주문건수 없음", "홈플러스 주문건수 없음", "S11253")
    , CHECK_MARKET_ORDER("S11254", "마켓 주문건수 없음", "마켓 주문건수 없음", "S11254")

    ;

    private String templateCode;
    private String workName;
    private String description;
    private String switchTemplateCode;
}
