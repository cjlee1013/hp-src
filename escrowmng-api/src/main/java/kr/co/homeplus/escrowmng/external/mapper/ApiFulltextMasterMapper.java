package kr.co.homeplus.escrowmng.external.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.external.model.ApiFulltext;

@EscrowMasterConnection
public interface ApiFulltextMasterMapper {

    int insertApiFulltext(ApiFulltext apiFulltextList);
    int updateApiFulltext(ApiFulltext apiFulltextList);
}