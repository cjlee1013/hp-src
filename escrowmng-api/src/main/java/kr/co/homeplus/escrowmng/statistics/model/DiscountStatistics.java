package kr.co.homeplus.escrowmng.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

@ApiModel(value = "할인통계 결과")
@Getter
@Setter
public class DiscountStatistics {

    @ApiModelProperty(value = "점포유형")
    private String storeType;
    @ApiModelProperty(value = "상품유형")
    private String mallType;
    @ApiModelProperty(value = "할인번호")
    private long discountNo;
    @ApiModelProperty(value = "할인명")
    private String discountNm;

    @ApiModelProperty(value = "주문일자")
    private String orderDt;
    @ApiModelProperty(value = "주문건수")
    private int orderCnt;
    @ApiModelProperty(value = "결제금액")
    private long payAmt;
    @ApiModelProperty(value = "할인금액")
    private long discountAmt;
    @ApiModelProperty(value = "취소건수")
    private int claimCnt;
    @ApiModelProperty(value = "취소금액")
    private long claimAmt;
    @ApiModelProperty(value = "할인취소금액")
    private long claimDiscountAmt;


}
