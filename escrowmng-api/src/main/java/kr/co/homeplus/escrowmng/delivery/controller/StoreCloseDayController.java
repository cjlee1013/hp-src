package kr.co.homeplus.escrowmng.delivery.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.delivery.model.StoreCloseDayImageModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreCloseDayModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreCloseDaySelectModel;
import kr.co.homeplus.escrowmng.delivery.service.StoreCloseDayService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/escrow/storeDelivery")
@Api(tags = "배송관리 > 점포배송정보 > 점포별 휴일 조회")
public class StoreCloseDayController {

    private final StoreCloseDayService storeCloseDayService;

    @GetMapping("/getStoreCloseDayList")
    @ApiOperation(value = "점포별 휴일 조회", response = StoreCloseDayModel.class)
    public ResponseObject getStoreCloseDayList(@Valid StoreCloseDaySelectModel storeCloseDaySelectModel) {
        return ResourceConverter.toResponseObject(storeCloseDayService.getStoreCloseDayList(storeCloseDaySelectModel));
    }

    @GetMapping("/getCloseDayImageList")
    @ApiOperation(value = "휴일 이미지 리스트 조회", response = StoreCloseDayImageModel.class)
    public ResponseObject getCloseDayImageList() {
        return ResourceConverter.toResponseObject(storeCloseDayService.getCloseDayImageList());
    }

    @PostMapping("/saveCloseDayImageList")
    @ApiOperation(value = "휴일 이미지 저장", response = Integer.class)
    public ResponseObject<Integer> saveCloseDayImageList(@RequestBody StoreCloseDayImageModel storeCloseDayImageModel) throws Exception {
        return ResourceConverter.toResponseObject(storeCloseDayService.saveCloseDayImageList(storeCloseDayImageModel));
    }
}
