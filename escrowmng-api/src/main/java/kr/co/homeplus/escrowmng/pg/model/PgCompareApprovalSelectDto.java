package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 대사 > PG 승인대사 조회 > PG 승인대사 조회 DTO")
public class PgCompareApprovalSelectDto {

    @ApiModelProperty(notes = "PG종류(TOSSPG,INICIS)", position = 1)
    private String schPgKind;

    @ApiModelProperty(notes = "조회시작일자", position = 2)
    private String schFromDt;

    @ApiModelProperty(notes = "조회종료일자", position = 3)
    private String schEndDt;

    @ApiModelProperty(notes = "거래유형코드", position = 4)
    private String schTradeTypeCd;

    @ApiModelProperty(notes = "결제수단", position = 5)
    private String schPaymentMethod;

    @ApiModelProperty(notes = "차이사유코드", position = 6)
    private String schDiffReasonCd;

}
