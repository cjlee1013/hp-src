package kr.co.homeplus.escrowmng.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@ApiModel(description = "클레임 안심번호 요청 정보")
@Data
@RequiredArgsConstructor
public class ClaimSafetyIssueRequest {

    @ApiModelProperty(value = "클레임번들번호", position = 1)
    private long claimBundleNo;

    @ApiModelProperty(value = "클레임타입(R:수거,X:교환)", position = 2)
    private String claimType;

    @ApiModelProperty(value = "요청연락처번호", position = 3)
    private String reqPhoneNo;

}
