package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 배분율 관리 > 결제정책 히스토리 search DTO")
public class PaymentPolicyHistSelectDto {
    @ApiModelProperty(notes = "조회시작일자", required = true, position = 1)
    @NotEmpty(message = "조회시작일자")
    private String schFromDt;

    @ApiModelProperty(notes = "조회종료일자", required = true, position = 2)
    @NotEmpty(message = "조회종료일자")
    private String schEndDt;

    @ApiModelProperty(notes = "정책번호", required = true, position = 3)
    private Long policyNo;

    @ApiModelProperty(notes = "상품번호", position = 4)
    private String schItemCd;

    @ApiModelProperty(notes = "대분류코드", position = 5)
    private String schLcateCd;

    @ApiModelProperty(notes = "중분류코드", position = 6)
    private String schMcateCd;

    @ApiModelProperty(notes = "소분류코드", position = 7)
    private String schScateCd;

    @ApiModelProperty(notes = "세분류코드", position = 8)
    private String schDcateCd;

    @ApiModelProperty(notes = "카테고리코드", position = 9)
    private String schCateCd;

    @ApiModelProperty(notes = "제휴검색유형", position = 10)
    private String schAffiliateType;

    @ApiModelProperty(notes = "제휴검색값", position = 11)
    private String schAffiliateValue;
}
