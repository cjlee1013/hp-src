package kr.co.homeplus.escrowmng.statistics.service;

import java.util.List;
import kr.co.homeplus.escrowmng.statistics.mapper.ZipcodeOrderStatisticsSlaveMapper;
import kr.co.homeplus.escrowmng.statistics.model.ZipcodeOrderStatistics;
import kr.co.homeplus.escrowmng.statistics.model.ZipcodeOrderStatisticsSelect;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ZipcodeStatisticsOrderService {

    private final ZipcodeOrderStatisticsSlaveMapper zipcodeOrderStatisticsSlaveMapper;

    /**
     * 우편번호별 주문현황 리스트 조회
     * @param zipcodeOrderStatisticsSelect
     * @return
     */
    public List<ZipcodeOrderStatistics> getZipcodeOrderStatisticsList(ZipcodeOrderStatisticsSelect zipcodeOrderStatisticsSelect) {
        return zipcodeOrderStatisticsSlaveMapper.selectZipcodeOrderStatisticsList(zipcodeOrderStatisticsSelect);
    }
}
