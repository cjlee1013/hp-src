package kr.co.homeplus.escrowmng.order.model.managementPop;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "행사할인정보 팝업 DTO")
public class OrderPromoInfoDto {
    @ApiModelProperty(value = "점포명", position = 1)
    private String storeNm;
    @ApiModelProperty(value = "상품번호", position = 2)
    private String itemNo;
    @ApiModelProperty(value = "상품명", position = 3)
    private String itemNm1;
    @ApiModelProperty(value = "행사종류", position = 4)
    private String promoNm1;
    @ApiModelProperty(value = "행사번호", position = 5)
    private String promoNo;
    @ApiModelProperty(value = "행사할인금액", position = 6)
    private long discountAmt;
}
