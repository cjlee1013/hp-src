package kr.co.homeplus.escrowmng.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "배송관리 > 점포배송정보 > 상품별 Shift 관리 DTO")
@Getter @Setter
public class ItemShiftManageModel {
    @ApiModelProperty(notes = "상품번호", position = 1)
    private String itemNo;
    @ApiModelProperty(notes = "상품명", position = 2)
    private String itemNm;
    @ApiModelProperty(notes = "점포유형", position = 3)
    private String storeType;
    @ApiModelProperty(notes = "적용점포", position = 4)
    private String storeId;
    @ApiModelProperty(notes = "적용점포명", position = 5)
    private String storeNm;
    @ApiModelProperty(notes = "Shift1", position = 6)
    private String shift1;
    @ApiModelProperty(notes = "Shift2", position = 7)
    private String shift2;
    @ApiModelProperty(notes = "Shift3", position = 8)
    private String shift3;
    @ApiModelProperty(notes = "Shift4", position = 9)
    private String shift4;
    @ApiModelProperty(notes = "Shift5", position = 10)
    private String shift5;
    @ApiModelProperty(notes = "Shift6", position = 11)
    private String shift6;
    @ApiModelProperty(notes = "Shift7", position = 11)
    private String shift7;
    @ApiModelProperty(notes = "Shift8", position = 11)
    private String shift8;
    @ApiModelProperty(notes = "Shift9", position = 11)
    private String shift9;
    @ApiModelProperty(notes = "수정일", position = 12)
    private String chgDt;
    @ApiModelProperty(notes = "수정자", position = 13)
    private String chgId;
}