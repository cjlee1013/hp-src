package kr.co.homeplus.escrowmng.order.model.entity;

public class ShippingNonRcvEntity {
    // 구매주문번호
    public String purchaseOrderNo;
    // 번들번호
    public String bundleNo;
    // 고객번호
    public String userNo;
    // 파트너ID
    public String partnerId;
    // 미수취신고유형(NA:상품미도착,NR:도난의심,NT:배송조회안됨,NE:ETC)
    public String noRcvDeclrType;
    // 미수취 상세 사유
    public String noRcvDetailReason;
    // 미수취 신고 일자
    public String noRcvDeclrDt;
    // 미수취 처리 일자
    public String noRcvProcessDt;
    // 미수취처리유형(C : 미수취신고철회요청, W : 미수취신고철회)
    public String noRcvProcessType;
    // 처리내용
    public String noRcvProcessCntnt;
    // 처리여부
    public String noRcvProcessYn;
    // 철회여부
    public String noRcvWthdrwYn;
    // 미수취 등록자(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  admin : 사번으로 인식하여 사원정보 노출)
    public String regId;
    // 미수취 등록일자
    public String regDt;
    // 미수취 수정자(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  admin : 사번으로 인식하여 사원정보 노출)
    public String chgId;
    // 미수취 수정일자
    public String chgDt;
}
