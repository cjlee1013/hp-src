package kr.co.homeplus.escrowmng.delivery.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.delivery.model.ReserveShipPlaceManageModel;
import kr.co.homeplus.escrowmng.delivery.model.ReserveShipPlaceManageSelectModel;
import kr.co.homeplus.escrowmng.delivery.service.ReserveShipPlaceManageService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/escrow/shipManage")
@Api(tags = "배송관리 > 배송관리 > 선물세트 발송지 관리")
public class ReserveShipPlaceManageController {
    private final ReserveShipPlaceManageService reserveShipPlaceManageService;

    @GetMapping("/getReserveShipPlaceMngList")
    @ApiOperation(value = "선물세트 발송지 리스트 조회", response = ReserveShipPlaceManageModel.class)
    public ResponseObject<List<ReserveShipPlaceManageModel>> getReserveShipPlaceMngList(@Valid ReserveShipPlaceManageSelectModel reserveShipPlaceManageSelectModel) {
        return ResourceConverter.toResponseObject(reserveShipPlaceManageService.getReserveShipPlaceMngList(reserveShipPlaceManageSelectModel));
    }

    @PostMapping("/saveReserveShipPlace")
    @ApiOperation(value = "선물세트 발송지 저장")
    public ResponseObject<Integer> saveReserveShipPlace(@Valid @RequestBody ReserveShipPlaceManageModel reserveShipPlaceManageModel) {
        return ResourceConverter.toResponseObject(reserveShipPlaceManageService.saveReserveShipPlace(reserveShipPlaceManageModel));
    }

    @PostMapping("/saveReserveShipPlaceList")
    @ApiOperation(value = "선물세트 발송지 리스트 저장 (다중건)")
    public ResponseObject<Integer> saveReserveShipPlaceList(@Valid @RequestBody List<ReserveShipPlaceManageModel> reserveShipPlaceManageModelList) {
        return ResourceConverter.toResponseObject(reserveShipPlaceManageService.saveReserveShipPlaceList(reserveShipPlaceManageModelList));
    }

    @PostMapping("/deleteReserveShipPlaceList")
    @ApiOperation(value = "선물세트 발송지 리스트 삭제")
    public ResponseObject<Integer> deleteReserveShipPlaceList(@Valid @RequestBody List<ReserveShipPlaceManageModel> reserveShipPlaceManageModelList) {
        return ResourceConverter.toResponseObject(reserveShipPlaceManageService.deleteReserveShipPlaceList(reserveShipPlaceManageModelList));
    }
}
