package kr.co.homeplus.escrowmng.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
@ApiModel(description = "통계 > 프로모션통계 > 장바구니쿠폰통계 SELECT DTO")
public class CartCouponStatisticsSelectModel {

    @ApiModelProperty(notes = "조회시작일자", position = 1, required = true)
    @NotEmpty(message = "조회시작일자")
    private String schFromDt;

    @ApiModelProperty(notes = "조회종료일자", position = 2, required = true)
    @NotEmpty(message = "조회종료일자")
    private String schEndDt;

    @ApiModelProperty(notes = "점포유형(HYPER|CLUB|EXP|AURORA)", position = 3, required = true)
    @Pattern(regexp = PatternConstants.STORE_TYPE_PATTERN, message="점포유형(HYPER,EXP,CLUB)")
    private String schStoreType;

    @ApiModelProperty(notes = "조회쿠폰유형", position = 4)
    @NotEmpty(message = "쿠폰유형")
    private String schCouponKind;

    @ApiModelProperty(notes = "조회점포ID", position = 5)
    private String schStoreId;
}
