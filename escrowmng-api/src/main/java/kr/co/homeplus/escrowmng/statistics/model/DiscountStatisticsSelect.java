package kr.co.homeplus.escrowmng.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.extract.model.ExtractCommonSetDto;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "할인통계조회 DTO")
@Getter
@Setter
public class DiscountStatisticsSelect {

    @ApiModelProperty(value = "검색시작일자")
    private String schStartDt;

    @ApiModelProperty(value = "검색종료일자")
    private String schEndDt;

    @Pattern(regexp = PatternConstants.STORE_TYPE_PATTERN, message="사이트유형확장(HYPER, EXP)")
    @ApiModelProperty(value = "사이트유형확장(HOME,EXP)")
    private String schSiteTypeExtend;

    @ApiModelProperty(value = "상품유형(TD,DS)")
    private String schMallType;

    @ApiModelProperty(value = "할인유형")
    private String schDiscountKind;

    @ApiModelProperty(value = "할인번호")
    private Long schDiscountNo;

    @ApiModelProperty(value = "할인명")
    private String schDiscountNm;

    @ApiModelProperty(value = "할인번호(상세조회시 사용)")
    private Long schDetailDiscountNo;

    /* 추출 히스토리 저장용 DTO */
    ExtractCommonSetDto extractCommonSetDto;
}
