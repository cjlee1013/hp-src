package kr.co.homeplus.escrowmng.claim.service.partner;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerBoardCountGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListSetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ticketRental.TicketRentalCancelListGetDto;


public interface TicketRentalCancelMapperService {

    /**
     * 이티켓 렌탈 취소 건수 조회
     *
     * @param partnerId
     * @param claimType
     * @return 클레임 신청/대기/완료 건수
     * **/
    ClaimPartnerBoardCountGetDto getTicketRentalClaimBoardCount(String partnerId, String claimType);

    List<TicketRentalCancelListGetDto> getRentalCancelList(ClaimPartnerListSetDto claimCancelListSetDto);

}
