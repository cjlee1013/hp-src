package kr.co.homeplus.escrowmng.order.model.entity;

public class ItmMngCodeEntity {
    //
    public String gmcCd;
    //
    public String mcCd;
    //
    public String mcNm;
    //
    public String priority;
    //
    public String useYn;
    //
    public String ref1;
    //
    public String ref2;
    //
    public String ref3;
    //
    public String lgcCodeCd;
    //
    public String lgcUpcodeCd;
    //
    public String regId;
    //
    public String regDt;
    //
    public String chgId;
    //
    public String chgDt;
}
