package kr.co.homeplus.escrowmng.statistics.service;

import java.util.List;
import kr.co.homeplus.escrowmng.extract.service.ExtractHistoryService;
import kr.co.homeplus.escrowmng.statistics.mapper.CombineOrderStatusSlaveMapper;
import kr.co.homeplus.escrowmng.statistics.model.CombineOrderStatusDto;
import kr.co.homeplus.escrowmng.statistics.model.CombineOrderStatusSelectDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CombineOrderStatusService {
    private final CombineOrderStatusSlaveMapper combineOrderStatusSlaveMapper;
    private final ExtractHistoryService extractHistoryService;

    public List<CombineOrderStatusDto> getCombineOrderStatus(CombineOrderStatusSelectDto combineOrderStatusSelectDto) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(combineOrderStatusSelectDto.getExtractCommonSetDto());
        return combineOrderStatusSlaveMapper.selectCombOrderStatistics(combineOrderStatusSelectDto);
    }
}
