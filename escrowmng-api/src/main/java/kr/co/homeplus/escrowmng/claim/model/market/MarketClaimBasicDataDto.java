package kr.co.homeplus.escrowmng.claim.model.market;

import lombok.Data;

@Data
public class MarketClaimBasicDataDto {
    private long marketOrderNo;
    private long marketOrderItemNo;
    private String marketType;
    private long optQty;
    private String claimRequestReasonType;
}
