package kr.co.homeplus.escrowmng.core.valid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import kr.co.homeplus.escrowmng.utils.ValidUtil;

public class EnterNotEmptyValidator implements ConstraintValidator<EnterNotEmpty, String> {
    @Override
    public void initialize(EnterNotEmpty constraintAnnotation) {}

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return !ValidUtil.isEnterNotEmptyStr(s);
    }
}
