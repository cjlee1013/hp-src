package kr.co.homeplus.escrowmng.pg.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.pg.model.PgCompareApprovalDiffDto;
import kr.co.homeplus.escrowmng.pg.model.PgCompareApprovalSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PgCompareApprovalSumDto;

import java.util.List;

@EscrowSlaveConnection
public interface PgCompareSlaveMapper {
    List<PgCompareApprovalSumDto> selectPgCompareApprovalSum(PgCompareApprovalSelectDto pgCompareApprovalSelectDto);
    List<PgCompareApprovalDiffDto> selectPgCompareApprovalDiff(PgCompareApprovalSelectDto pgCompareApprovalSelectDto);
}
