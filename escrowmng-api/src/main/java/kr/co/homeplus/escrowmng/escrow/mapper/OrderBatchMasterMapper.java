package kr.co.homeplus.escrowmng.escrow.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchHistoryModel;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchManageModel;

@EscrowMasterConnection
public interface OrderBatchMasterMapper {
    int insertOrderBatchManage(OrderBatchManageModel orderBatchManageModel);
    int deleteOrderBatchManage(OrderBatchManageModel orderBatchManageModel);
    void createOrderBatchHistory(OrderBatchHistoryModel orderBatchHistoryModel);
    void updateOrderBatchHistory(OrderBatchHistoryModel orderBatchHistoryModel);
}
