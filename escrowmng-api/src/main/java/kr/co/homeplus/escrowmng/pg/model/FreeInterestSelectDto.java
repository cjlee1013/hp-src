package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단 관리 > 무이자 혜택 관리 조회 DTO")
public class FreeInterestSelectDto {
    @ApiModelProperty(value = "무이자관리순번")
    private Long freeInterestMngSeq;
    @ApiModelProperty(value = "적용시작일자")
    private String applyStartDt;
    @ApiModelProperty(value = "적용종료일자")
    private String applyEndDt;
    @ApiModelProperty(value = "사이트유형(HOME:홈플러스,CLUB:더클럽)")
    private String siteType;
    @ApiModelProperty(value = "무이자관리명")
    private String freeInterestNm;
    @ApiModelProperty(value = "사용여부")
    private String useYn;
}
