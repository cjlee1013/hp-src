package kr.co.homeplus.escrowmng.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "일자별 SHIFT 점포 관리 Model")
@Getter
@Setter
@EqualsAndHashCode
public class DailyShiftStoreManageModel {
    @ApiModelProperty(value = "일자별SHIFT관리순번", position = 1)
    private Long dailyShiftMngSeq;

    @ApiModelProperty(value = "점포ID", position = 2)
    private Integer storeId;
}
