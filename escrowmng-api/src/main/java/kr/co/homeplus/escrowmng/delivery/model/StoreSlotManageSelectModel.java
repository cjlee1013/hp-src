package kr.co.homeplus.escrowmng.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import lombok.Data;

@Data
@ApiModel("SLOT 관리 조회용 DTO")
public class StoreSlotManageSelectModel {

    @ApiModelProperty(value = "검색시작일자")
    private String schStartDt;

    @ApiModelProperty(value = "검색종료일자")
    private String schEndDt;

    @Pattern(regexp = PatternConstants.STORE_TYPE_PATTERN, message="점포유형(HYPER, CLUB, EXP)")
    @ApiModelProperty(value = "점포유형(Hyper, Club, Exp)", required = true)
    private String schStoreType;

    @NotNull
    @ApiModelProperty(value = "점포ID", required = true)
    private int schStoreId;

    @ApiModelProperty(value = "사용여부")
    private String schUseYn;
}
