package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PgDivideManageDto {
    @ApiModelProperty(value = "배분순번")
    private Long divideSeq;
    @ApiModelProperty(value = "배분명(기본배분율)")
    private String divideNm;
    @ApiModelProperty(value = "사이트유형(HOME:홈플러스,CLUB:더클럽)")
    private String siteType;
    @ApiModelProperty(value = "사이트유형명")
    private String siteTypeNm;
    @ApiModelProperty(value = "플랫폼(PC,모바일)")
    private String platform;
    @ApiModelProperty(value = "상위결제수단코드(신용카드,휴대폰결제등)")
    private String parentMethodCd;
    @ApiModelProperty(value = "상위결제수단명")
    private String parentMethodNm;
    @ApiModelProperty(value = "결제수단코드(삼성카드,현대카드,무통장등)")
    private String methodCd;
    @ApiModelProperty(value = "결제수단명")
    private String methodNm;
    @ApiModelProperty(value = "정책번호(pg_mng_type:POLICY이경우만 입력)")
    private Long policyNo;
    @ApiModelProperty(value = "pg관리유형(기본배분율:BASIC,기간배분율:PERIOD,결제정책배분율:POLICY)")
    private String pgMngType;
    @ApiModelProperty(value = "tosspgmid")
    private String tosspgMid;
    @ApiModelProperty(value = "tosspg비율")
    private Integer tosspgRate;
    @ApiModelProperty(value = "inicismid")
    private String inicisMid;
    @ApiModelProperty(value = "inicis비율")
    private Integer inicisRate;
    @ApiModelProperty(value = "kakaopaymid")
    private String kakaoMid;
    @ApiModelProperty(value = "kakaopay비율")
    private Integer kakaoRate;
    @ApiModelProperty(value = "kakaomoneymid")
    private String kakaoMoneyMid;
    @ApiModelProperty(value = "kakaomoney비율")
    private Integer kakaoMoneyRate;
    @ApiModelProperty(value = "naverpaymid")
    private String naverMid;
    @ApiModelProperty(value = "naverpay비율")
    private Integer naverRate;
    @ApiModelProperty(value = "samsungpaymid")
    private String samsungMid;
    @ApiModelProperty(value = "samsungpay비율")
    private Integer samsungRate;
    @ApiModelProperty(value = "적용시작일자")
    private String applyStartDt;
    @ApiModelProperty(value = "적용종료일자")
    private String applyEndDt;
    @ApiModelProperty(value = "우선순위(기본배분율:8000,기간별배분율:5000)")
    private Integer priority;
    @ApiModelProperty(value = "사용여부")
    private String useYn;
    @ApiModelProperty(value = "등록자id")
    private String regId;
    @ApiModelProperty(value = "등록일자")
    private String regDt;
    @ApiModelProperty(value = "수정자id")
    private String chgId;
    @ApiModelProperty(value = "수정일자")
    private String chgDt;
}
