package kr.co.homeplus.escrowmng.pg.service;

import java.util.List;
import kr.co.homeplus.escrowmng.constants.Constants;
import kr.co.homeplus.escrowmng.enums.BusinessExceptionCode;
import kr.co.homeplus.escrowmng.core.exception.BusinessLogicException;
import kr.co.homeplus.escrowmng.pg.model.ChargeDiscountBenefitManageDto;
import kr.co.homeplus.escrowmng.pg.model.ChargeDiscountBenefitManageSelectDto;
import kr.co.homeplus.escrowmng.pg.mapper.ChargeDiscountBenefitMasterMapper;
import kr.co.homeplus.escrowmng.pg.mapper.ChargeDiscountBenefitSlaveMapper;
import kr.co.homeplus.escrowmng.utils.EscrowDate;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ChargeDiscountBenefitManageService {

    private final ChargeDiscountBenefitMasterMapper chargeDiscountBenefitMasterMapper;
    private final ChargeDiscountBenefitSlaveMapper chargeDiscountBenefitSlaveMapper;

    /**
     * 청구할인 혜택 조회
     *
     * @param chargeDiscountBenefitManageSelectDto
     * @return
     * @throws Exception
     */
    public List<ChargeDiscountBenefitManageDto> getChargeDiscountBenefitManageList(
        ChargeDiscountBenefitManageSelectDto chargeDiscountBenefitManageSelectDto) throws Exception {
        return chargeDiscountBenefitSlaveMapper.selectChargeDiscountBenefitManageList(chargeDiscountBenefitManageSelectDto);
    }

    /**
     * 청구할인 혜택 등록/수정
     *
     * @param chargeDiscountBenefitManageDto
     * @return
     */
    public int saveChargeDiscountBenefitManage(ChargeDiscountBenefitManageDto chargeDiscountBenefitManageDto) throws Exception {

        if (EscrowString.equals(Constants.USE_Y, chargeDiscountBenefitManageDto.getUseYn())
            && this.getChargeDiscountBenefitDuplicate(chargeDiscountBenefitManageDto) > 0) { // 사용여부 Y 일 인경우 중복 체크
            throw new BusinessLogicException(BusinessExceptionCode.ADMIN_VALIDATION_10000);
        }

        //적용 시작일 종료일 포맷 변경 시작일 -> 시작일 00:00:00 / 종료일 23:59:59
        chargeDiscountBenefitManageDto.setApplyStartDt(EscrowDate.changeDateStrToTimeStr(chargeDiscountBenefitManageDto.getApplyStartDt(), EscrowDate.DEFAULT_YMD,
            EscrowDate.DEFAULT_YMD_HIS, 00, 00, 00));
        chargeDiscountBenefitManageDto.setApplyEndDt(
            EscrowDate.changeDateStrToTimeStr(chargeDiscountBenefitManageDto.getApplyEndDt(), EscrowDate.DEFAULT_YMD, EscrowDate.DEFAULT_YMD_HIS,
            23, 59, 59));

        if (chargeDiscountBenefitManageDto.getChargeDiscountBenefitMngNo() > 0) {
            return chargeDiscountBenefitMasterMapper.updateChargeDiscountBenefit(chargeDiscountBenefitManageDto);
        } else {
            return chargeDiscountBenefitMasterMapper.insertChargeDiscountBenefit(chargeDiscountBenefitManageDto);
        }
    }

    /**
     * 기간 내 동일 결제수단 존재 여부 확인
     *
     * @param chargeDiscountBenefitManageDto
     * @return
     */
    private int getChargeDiscountBenefitDuplicate(ChargeDiscountBenefitManageDto chargeDiscountBenefitManageDto) {
        return chargeDiscountBenefitSlaveMapper.getChargeDiscountBenefitDuplicate(chargeDiscountBenefitManageDto);
    }
}
