package kr.co.homeplus.escrowmng.statistics.model;

import lombok.Data;

@Data
public class ExhibitionSaleStatisticsSelectDto {
    String schApplyStartDt;
    String schApplyEndDt;

    String schExhibitionNm;
}
