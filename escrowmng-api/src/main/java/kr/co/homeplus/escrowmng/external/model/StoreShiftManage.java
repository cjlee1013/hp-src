package kr.co.homeplus.escrowmng.external.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import kr.co.homeplus.escrowmng.core.valid.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "[옴니연동] 점포 shift 관리")
public class StoreShiftManage {

    @ApiModelProperty(value = "점포유형")
    @Pattern(regexp = PatternConstants.STORE_TYPE_PATTERN, message="점포유형(HYPER,EXP,CLUB)")
    @NotEmpty(message = "점포유형(HYPER,EXP,CLUB)")
    private String storeType;

    @ApiModelProperty(value = "점포ID", position = 1)
    @NotEmpty(message = "점포ID")
    @AllowInput(types = {PatternConstants.NUM}, message = "점포ID")
    private String storeId;

    @ApiModelProperty(value = "배송일", position = 2)
    @NotEmpty
    @Pattern(regexp = PatternConstants.DATE_DF_FORMAT_PATTERN, message = "배송일 날짜 형식 > yyyy-MM-dd")
    @Date(message = "배송일 날짜 형식 > yyyy-MM-dd")
    private String shipDt;

    @ApiModelProperty(value = "배송요일(1:일요일,2:월요일~7:토요일)", position = 3)
    @NotEmpty(message = "배송요일")
    @Pattern(regexp = PatternConstants.SHIP_WEEKDYA_PATTERN, message = "배송요일(1:일요일,2:월요일~7:토요일)")
    private String shipWeekday;

    @ApiModelProperty(value = "shift ID", position = 4)
    @NotEmpty(message = "shift ID")
    @AllowInput(types = {PatternConstants.NUM}, message = "shift ID")
    private String shiftId;

    @ApiModelProperty(value = "VAN명", position = 5)
    @Pattern(regexp = "[A-Z]{0,1}", message = "VAN명")
    private String vanNm;

    @ApiModelProperty(value = "배송시작시간", position = 6)
    @NotEmpty(message = "배송시작시간")
    @Pattern(regexp = PatternConstants.DATETIME_PATTERN_HOUR_MINUTES, message = "배송시작시간 HHmm")
    private String shiftShipStartTime;

    @ApiModelProperty(value = "배송종료시간", position = 7)
    @NotEmpty(message = "배송종료시간")
    @Pattern(regexp = PatternConstants.DATETIME_PATTERN_HOUR_MINUTES, message = "배송종료시간 HHmm")
    private String shiftShipEndTime;

    @ApiModelProperty(value = "(shift실행 D-DAY) (0:배송일, 1:배송전일)", position = 8)
    private int shiftExecDay;

    @ApiModelProperty(value = "shift마감시간", position = 9)
    @NotEmpty(message = "shift마감시간")
    @Pattern(regexp = PatternConstants.DATETIME_PATTERN_HOUR_MINUTES, message = "shift마감시간 HHmm")
    private String shiftCloseTime;

    @ApiModelProperty(value = "shift사전마감시간(1차전송시간)", position = 10)
    private String shiftBeforeCloseTime;

    @ApiModelProperty(value = "assigndrvYn", position = 97)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "assigndrvYn")
    private String assigndrvYn;

    @ApiModelProperty(value = "반품/교환 가능 여부", position = 98)
    @NotEmpty(message = "반품/교환 가능 여부")
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "반품/교환 가능 여부")
    private String returnYn;

    @JsonIgnore
    @ApiModelProperty(value = "수정자ID", position = 99)
    private String chgId;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{")
            .append("storeType:").append(storeType).append(",")
            .append("storeId:").append(storeId).append(",")
            .append("shipDt:").append(shipDt).append(",")
            .append("shipWeekday:").append(shipWeekday).append(",")
            .append("shiftId:").append(shiftId).append(",")
            .append("vanNm:").append(vanNm).append(",")
            .append("shiftShipStartTime:").append(shiftShipStartTime).append(",")
            .append("shiftShipEndTime:").append(shiftShipEndTime).append(",")
            .append("shiftExecDay:").append(shiftExecDay).append(",")
            .append("shiftCloseTime:").append(shiftCloseTime).append(",")
            .append("shiftBeforeCloseTime:").append(shiftBeforeCloseTime).append(",")
            .append("assigndrvYn:").append(assigndrvYn).append(",")
            .append("returnYn:").append(returnYn)
            .append("}");

        return sb.toString();
    }

}