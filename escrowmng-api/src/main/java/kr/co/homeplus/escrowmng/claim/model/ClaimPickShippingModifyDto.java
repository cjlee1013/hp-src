package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "반품수거 상태 수정.")
public class ClaimPickShippingModifyDto {

    @ApiModelProperty(value = "클레임 반품수거 번호", position = 1)
    public String claimPickShippingNo;

    @ApiModelProperty(value = "클레임 번들 번호", position = 2)
    public String claimBundleNo;

    @ApiModelProperty(value = "반품수거 수정 타입 [주소:ADDR, 상태:STATUS, 배송타입:TYPE, 전부:ALL]", position = 3)
    public String modifyType;

    @ApiModelProperty(value = "수거지-이름", position = 4)
    public String pickReceiverNm;

    @ApiModelProperty(value = "수거지-휴대폰", position = 5)
    public String pickMobileNo;

    @ApiModelProperty(value = "수거지 우편번호", position = 6)
    public String pickZipcode;

    @ApiModelProperty(value = "수거지 기본주소", position = 5)
    public String pickRoadBaseAddr;

    @ApiModelProperty(value = "수거지 기본주소", position = 5)
    public String pickBaseAddr;

    @ApiModelProperty(value = "수거지 샹세주소", position = 6)
    public String pickDetailAddr;

    @ApiModelProperty(value = "수거배송수단", position = 7)
    public String pickShipType;

    @ApiModelProperty(value = "수거택배사코드", position = 8)
    public String pickDlvCd;

    @ApiModelProperty(value = "수거송장번호", position = 10)
    public String pickInvoiceNo;

    @ApiModelProperty(value = "수거메모", position = 12)
    public String pickMemo;

    @ApiModelProperty(value="수거상태", position = 13)
    public String pickStatus;

    @ApiModelProperty(value="수거일자", position = 15)
    public String shipDt;

    @ApiModelProperty(value="수거shiftID", position = 16)
    public String shiftId;

    @ApiModelProperty(value="수거slotID", position = 17)
    public String slotId;

    @ApiModelProperty(value="수거 예약접수자", position = 18)
    public String pickP0Id;

    @ApiModelProperty(value="수거 예약일(업체직배송인 경우 수거예정일 최초등록일)", position = 19)
    public String pickP0Dt;

    @ApiModelProperty(value="수거 송장등록일(업체직배송의 경우 수거예정일)", position = 20)
    public String pickP1Dt;

    @ApiModelProperty(value="수거 시작일", position = 21)
    public String pickP2Dt;

    @ApiModelProperty(value="수거 완료일", position = 22)
    public String pickP3Dt;

    @ApiModelProperty(value = "수정ID", position = 24)
    public String chgId;

    @ApiModelProperty(value = "배송타입", position = 25)
    public String shipType;

    @ApiModelProperty(value = "클레임 번호", position = 26)
    private long claimNo;

}