package kr.co.homeplus.escrowmng.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "픽업점포관리 select model")
@Getter
@Setter
@EqualsAndHashCode
public class PickupStoreManageSelectModel {
    @ApiModelProperty(value = "점포유형", position = 1)
    @NotEmpty(message = "점포유형")
    private String schStoreType;

    @ApiModelProperty(value = "점포ID", position = 2)
    private int schStoreId;

    @ApiModelProperty(value = "장소사용여부", position = 3)
    private String schPlaceUseYn;

    @ApiModelProperty(value = "락커사용여부", position = 4)
    private String schLockerUseYn;
}