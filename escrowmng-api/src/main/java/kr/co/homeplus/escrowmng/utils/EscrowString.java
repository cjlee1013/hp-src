package kr.co.homeplus.escrowmng.utils;

import static kr.co.homeplus.escrowmng.constants.Constants.AMP;
import static kr.co.homeplus.escrowmng.constants.Constants.EMPTY_STRING;
import static kr.co.homeplus.escrowmng.constants.Constants.EQUAL;
import static kr.co.homeplus.escrowmng.constants.Constants.QUESTION;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

public class EscrowString {
    /**
     * str 값이 NULL, str 이 공백으로만 이루어진 경우 ""로 치환해서 리턴
     * @param str 체크할 문자열
     * @return "" 또는 원문자열
     * */
    public static String NVL(String str) {
        if (str == null || str.isBlank()) {
            return "";
        } else {
            return str;
        }
    }

    /**
     * Object 값이 NULL 일경우 T 에 해당 하는 형식으로 리턴
     * Long : 0
     * Integer : 0
     * String : ""
     * @return T
     */
    public static <T> T NVL(Object obj, Class<T> tClass) {

        if (obj == null) {
            if (tClass.getName().equals(Long.class.getName())) {
                return tClass.cast(0L);
            } else if (tClass.getName().equals(Integer.class.getName())) {
                return tClass.cast(0);
            } else if (tClass.getName().equals(String.class.getName())) {
                return tClass.cast("");
            }
        }
        return tClass.cast(obj);
    }

    /** Object 공백 check
     * @param object 체크할 객체
     * @return boolean
     * */
    public static boolean isEmpty(Object object){
        return ObjectUtils.isEmpty(object);
    }
    public static boolean isNotEmpty(Object object){
        return !ObjectUtils.isEmpty(object);
    }

    /** String 공백 check
     * @param str 체크할 문자열
     * @return boolean
     * */
    public static boolean isEmpty(String str) {
        return StringUtils.isEmpty(str);
    }
    public static boolean isNotEmpty(String str) {
        return !StringUtils.isEmpty(str);
    }

    /** long 공백 check
     * @param number 체크할 Long 객체
     * @return boolean
     * */
    public static boolean isEmpty(Long number){
        return number == null;
    }
    public static boolean isNotEmpty(Long number){
        return number != null;
    }

    /** string equals */
    public static boolean equals(String firstStr, String secondStr){
        if(isEmpty(firstStr) || isEmpty(secondStr)) return false;
        return firstStr.equals(secondStr);
    }

    /**
     * url parameter 만들기
     *
     * @param setParameterList param 이 담긴 List
     * @return
     * @see SetParameter
     */
    public static String getParameter(List<SetParameter> setParameterList){
        return setParameterList.stream().map(p -> p.getKey() + EQUAL + URLEncoder.encode(String.valueOf(p.getValue()), StandardCharsets.UTF_8))
            .reduce((p1, p2) -> p1 + AMP + p2)
            .map(s -> QUESTION + s)
            .orElse(EMPTY_STRING);
    }

    // 엑셀 개행문자(줄바꿈) split
    public static String[] getExcelEnterSplit(final String str) {
        return str.split(PatternConstants.ENTER_STR);
    }
}
