package kr.co.homeplus.escrowmng.pg.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodBenefitManageDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodBenefitManageSelectDto;

@EscrowSlaveConnection
public interface PaymentMethodBenefitManageSlaveMapper {
    List<PaymentMethodBenefitManageDto> selectPaymentMethodBenefitManage(PaymentMethodBenefitManageSelectDto paymentMethodBenefitManageSelectDto);

    int getPaymentMethodBenefitDuplicate(PaymentMethodBenefitManageDto paymentMethodBenefitManageDto);
}
