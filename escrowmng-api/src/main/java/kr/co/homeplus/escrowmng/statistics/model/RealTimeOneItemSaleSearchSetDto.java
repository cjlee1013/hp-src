package kr.co.homeplus.escrowmng.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "실시간 특정 상품 판매 현황")
@Getter
@Setter
@EqualsAndHashCode
public class RealTimeOneItemSaleSearchSetDto {
    @ApiModelProperty(value = "주문일시", position = 1)
    private String schStartDt;

    @ApiModelProperty(value = "주문일시", position = 2)
    private String schEndDt;

    @ApiModelProperty(value = "상품번호", position = 3)
    private String schItemNo;

    @ApiModelProperty(value = "점포유형(HYPER, CLUB, EXP, AURORA, DS)", position = 4)
    private String schStoreType;

    @ApiModelProperty(value = "점포코드", position = 5)
    private String schStoreId;

    @ApiModelProperty(value = "마켓유형(EXCEPT(제휴제외), NAVER, ELEVEN)", position = 6)
    private String schMarketType;
}