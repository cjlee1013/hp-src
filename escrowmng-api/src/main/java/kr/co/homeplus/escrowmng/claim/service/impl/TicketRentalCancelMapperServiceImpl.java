package kr.co.homeplus.escrowmng.claim.service.impl;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.mapper.ticketRental.TicketRentalCancelSlaveMapper;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerBoardCountGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListSetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ticketRental.TicketRentalCancelListGetDto;
import kr.co.homeplus.escrowmng.claim.service.partner.TicketRentalCancelMapperService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class TicketRentalCancelMapperServiceImpl implements TicketRentalCancelMapperService {

    private final TicketRentalCancelSlaveMapper ticketRentalCancelSlaveMapper;


    @Override
    public ClaimPartnerBoardCountGetDto getTicketRentalClaimBoardCount(String partnerId,
        String claimType) {
        return ticketRentalCancelSlaveMapper.selectTicketRentalClaimBoardCount(partnerId, claimType);
    }

    @Override
    public List<TicketRentalCancelListGetDto> getRentalCancelList(
        ClaimPartnerListSetDto claimPartnerListSetDto) {
        return ticketRentalCancelSlaveMapper.selectRentalCancelList(claimPartnerListSetDto);
    }
}
