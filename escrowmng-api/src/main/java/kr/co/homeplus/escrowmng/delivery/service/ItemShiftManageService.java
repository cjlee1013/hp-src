package kr.co.homeplus.escrowmng.delivery.service;

import static kr.co.homeplus.escrowmng.constants.Constants.SHIFT_1;
import static kr.co.homeplus.escrowmng.constants.Constants.SHIFT_2;
import static kr.co.homeplus.escrowmng.constants.Constants.SHIFT_3;
import static kr.co.homeplus.escrowmng.constants.Constants.SHIFT_4;
import static kr.co.homeplus.escrowmng.constants.Constants.SHIFT_5;
import static kr.co.homeplus.escrowmng.constants.Constants.SHIFT_6;
import static kr.co.homeplus.escrowmng.constants.Constants.SHIFT_7;
import static kr.co.homeplus.escrowmng.constants.Constants.SHIFT_8;
import static kr.co.homeplus.escrowmng.constants.Constants.SHIFT_9;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.escrowmng.constants.Constants;
import kr.co.homeplus.escrowmng.core.db.construct.TransactionManagerName;
import kr.co.homeplus.escrowmng.delivery.mapper.ItemShiftManageMasterMapper;
import kr.co.homeplus.escrowmng.delivery.mapper.ItemShiftManageSlaveMapper;
import kr.co.homeplus.escrowmng.delivery.model.ItemShiftManageInsertModel;
import kr.co.homeplus.escrowmng.delivery.model.ItemShiftManageModel;
import kr.co.homeplus.escrowmng.delivery.model.ItemShiftManageSelectModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ItemShiftManageService {

    private final ItemShiftManageMasterMapper itemShiftManageMasterMapper;
    private final ItemShiftManageSlaveMapper itemShiftManageSlaveMapper;

    /**
     * 상품별 Shift 리스트 조회
     * @param itemShiftManageSelectModel
     * @return
     */
    public List<ItemShiftManageModel> getItemShiftManageList(ItemShiftManageSelectModel itemShiftManageSelectModel) {
        return itemShiftManageSlaveMapper.selectItemShiftManageList(itemShiftManageSelectModel);
    }

    /**
     * 단일상품 Shift 등록 (다중점포)
     * @param itemShiftManageInsertModelList
     * @return
     */
    public int saveItemShiftList(List<ItemShiftManageInsertModel> itemShiftManageInsertModelList) {
        for (ItemShiftManageInsertModel itemShiftManageInsertModel : itemShiftManageInsertModelList) {
            this.saveItemShift(itemShiftManageInsertModel);
        }
        return Constants.ZERO;
    }

    /**
     * 단일상품 Shift 등록 (단일점포)
     * @param itemShiftManageInsertModel
     * @return
     */
    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int saveItemShift(ItemShiftManageInsertModel itemShiftManageInsertModel) {
        // 실제 데이터 insert/update 할 수 있도록 Model 변환하는 작업 수행
        this.convertItemShiftManageModel(itemShiftManageInsertModel);
        // history 테이블에 insert
        this.saveItemShiftHistory(itemShiftManageInsertModel);
        // update
        return itemShiftManageMasterMapper.insertItemShift(itemShiftManageInsertModel);
    }

    /**
     * 상품별 Shift 관리 리스트 삭제
     * @param itemShiftManageInsertModelList
     */
    public void deleteItemShiftList(List<ItemShiftManageInsertModel> itemShiftManageInsertModelList) {
        for (ItemShiftManageInsertModel itemShiftManageInsertModel : itemShiftManageInsertModelList) {
            // delete
            itemShiftManageMasterMapper.deleteItemShift(itemShiftManageInsertModel);
        }
    }

    /**
     * 상품별 Shift 관리 히스토리 등록
     * @param itemShiftManageInsertModel
     */
    public void saveItemShiftHistory(ItemShiftManageInsertModel itemShiftManageInsertModel) {
        itemShiftManageMasterMapper.insertItemShiftManageHistory(itemShiftManageInsertModel);
    }

    /**
     * 실제 데이터 insert/update 할 수 있도록 Model 변환
     * @param itemShiftManageInsertModel
     */
    private void convertItemShiftManageModel(ItemShiftManageInsertModel itemShiftManageInsertModel) {
        Map<String, String> shiftInfoMap = new HashMap<>();
        shiftInfoMap.put(SHIFT_1, itemShiftManageInsertModel.getShift1());
        shiftInfoMap.put(SHIFT_2, itemShiftManageInsertModel.getShift2());
        shiftInfoMap.put(SHIFT_3, itemShiftManageInsertModel.getShift3());
        shiftInfoMap.put(SHIFT_4, itemShiftManageInsertModel.getShift4());
        shiftInfoMap.put(SHIFT_5, itemShiftManageInsertModel.getShift5());
        shiftInfoMap.put(SHIFT_6, itemShiftManageInsertModel.getShift6());
        shiftInfoMap.put(SHIFT_7, itemShiftManageInsertModel.getShift7());
        shiftInfoMap.put(SHIFT_8, itemShiftManageInsertModel.getShift8());
        shiftInfoMap.put(SHIFT_9, itemShiftManageInsertModel.getShift9());
        itemShiftManageInsertModel.setShiftInfoMap(shiftInfoMap);
    }
}
