package kr.co.homeplus.escrowmng.escrow.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.escrow.model.StoreAnytimeManageDto;
import kr.co.homeplus.escrowmng.escrow.model.StoreAnytimeShiftManageDto;

@EscrowMasterConnection
public interface StoreAnytimeMasterMapper {
    int insertStoreAnytimeUseYn(StoreAnytimeManageDto storeAnytimeManageDto);
    int insertStoreAnytimeShiftUseYn(List<StoreAnytimeShiftManageDto> storeAnytimeShiftManageDtoList);
}
