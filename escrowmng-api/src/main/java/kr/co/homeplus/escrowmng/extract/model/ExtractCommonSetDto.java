package kr.co.homeplus.escrowmng.extract.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExtractCommonSetDto {
    // 추출자 사번
    String empId;
    // 메뉴명
    String menuNm;
    // 구분(조회/엑셀저장)
    String extractType;
    // 추출일시
    String extractDt;
}
