package kr.co.homeplus.escrowmng.message.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.message.model.OrderSendListRequest;
import kr.co.homeplus.escrowmng.message.model.OrderSendListResponse;
import kr.co.homeplus.escrowmng.message.service.OrderSendListService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 주문회원 팝업 검색 컨트롤러
 */
@Api(tags = "메시지관리 > 수동발송관리 > 주문검색")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
@RestController
@RequestMapping(value = "/escrow/message", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class OrderSendListController {

    private final OrderSendListService orderSendListService;

    @ApiOperation(value = "주문 대상 조회")
    @ApiResponse(code = 200, message = "주문 대상 조회 성공", response = OrderSendListResponse.class)
    @PostMapping("/getOrderSendList")
    public ResponseObject<OrderSendListResponse> getSendList(
        @ApiParam(value = "검색 조건", required = true) @RequestBody @Valid final OrderSendListRequest request) {
        return ResourceConverter.toResponseObject(orderSendListService.getSendList(request));
    }
}
