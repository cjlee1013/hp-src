package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "item-api Category 조회 DTO")
public class ItemCategoryGetDto {
    private String itemNo;
    private String itemNm;
    private String mallType;
    private String mallTypeNm;
    private String storeId;
    private String storeNm;
    private String storeType;
    private String storeTypeNm;
    private int dcateCd;
    private int scateCd;
    private int mcateCd;
    private int lcateCd;
    private String dcateNm;
    private String scateNm;
    private String mcateNm;
    private String lcateNm;
}
