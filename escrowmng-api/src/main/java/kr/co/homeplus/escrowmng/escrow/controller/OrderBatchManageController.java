package kr.co.homeplus.escrowmng.escrow.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchHistoryModel;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchHistorySelectModel;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchManageModel;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchManageSelectModel;
import kr.co.homeplus.escrowmng.escrow.service.OrderBatchManageService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "시스템관리 > 시스템관리 > 주문배치관리")
@RequestMapping("/escrow/orderBatch")
@RestController
@RequiredArgsConstructor
public class OrderBatchManageController {
    private final OrderBatchManageService orderBatchManageService;

    /**
     * 주문배치관리 목록조회
     */
    @PostMapping("/getOrderBatchManageList")
    @ApiOperation(value = "주문배치관리 목록조회")
    public ResponseObject<List<OrderBatchManageModel>> getOrderBatchManageList(@RequestBody OrderBatchManageSelectModel orderBatchManageSelectModel) {
        return ResourceConverter.toResponseObject(orderBatchManageService.getOrderBatchManageList(orderBatchManageSelectModel));
    }

    /**
     * 주문배치관리 저장
     */
    @PostMapping("/saveOrderBatchManage")
    @ApiOperation(value = "주문배치관리 저장")
    public ResponseObject<Integer> saveOrderBatchManage(@Valid @RequestBody OrderBatchManageModel orderBatchManageModel) {
        return ResourceConverter.toResponseObject(orderBatchManageService.saveOrderBatchManage(orderBatchManageModel));
    }

    /**
     * 주문배치관리 삭제
     */
    @PostMapping("/deleteOrderBatchManage")
    @ApiOperation(value = "주문배치관리 삭제")
    public ResponseObject<Integer> deleteOrderBatchManage(@RequestBody OrderBatchManageModel orderBatchManageModel) {
        return ResourceConverter.toResponseObject(orderBatchManageService.deleteOrderBatchManage(orderBatchManageModel));
    }

    /**
     * 주문배치이력 목록조회
     */
    @PostMapping("/getOrderBatchHistoryList")
    @ApiOperation(value = "주문배치이력 목록조회")
    public ResponseObject<List<OrderBatchHistoryModel>> getOrderBatchHistoryList(@Valid @RequestBody OrderBatchHistorySelectModel orderBatchHistorySelectModel) {
        return ResourceConverter.toResponseObject(orderBatchManageService.getOrderBatchHistoryList(orderBatchHistorySelectModel));
    }

    /**
     * LGW 배치이력 목록조회 (프로시저 이력조회)
     */
    @PostMapping("/getLgwBatchHistoryList")
    @ApiOperation(value = "LGW 배치이력 목록조회 (프로시저 이력조회)")
    public ResponseObject<List<OrderBatchHistoryModel>> getLgwBatchHistoryList(@Valid @RequestBody OrderBatchHistorySelectModel orderBatchHistorySelectModel) {
        return ResourceConverter.toResponseObject(orderBatchManageService.getLgwBatchHistoryList(orderBatchHistorySelectModel));
    }
}
