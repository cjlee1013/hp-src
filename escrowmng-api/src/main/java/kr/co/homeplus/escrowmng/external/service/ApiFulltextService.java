package kr.co.homeplus.escrowmng.external.service;

import kr.co.homeplus.escrowmng.external.mapper.ApiFulltextMasterMapper;
import kr.co.homeplus.escrowmng.external.model.ApiFulltext;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ApiFulltextService {

    private final ApiFulltextMasterMapper apiFulltextMasterMapper;

    /**
     * 외부 I/F 연동 로그 등록
     * @param apiFulltext
     * @return
     */
    public long insertApiFulltext(ApiFulltext apiFulltext) {
        try {
            apiFulltextMasterMapper.insertApiFulltext(apiFulltext);
        } catch (Exception e) {
            log.info("[insertApiFulltext] " + ExceptionUtils.getStackTrace(e));
        }

        return apiFulltext.getApiFulltextSeq();
    }

    /**
     * 외부 I/F 연동 로그 수정
     * @param apiFulltext
     * @return
     */
    public long updateApiFulltext(ApiFulltext apiFulltext) {
        try {
            apiFulltextMasterMapper.updateApiFulltext(apiFulltext);
        } catch (Exception e) {
            log.info("[updateApiFulltext] " + ExceptionUtils.getStackTrace(e));
        }

        return apiFulltext.getApiFulltextSeq();
    }
}
