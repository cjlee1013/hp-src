package kr.co.homeplus.escrowmng.claim.model.multiShip;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MultiShipInfoGetDto {

    @ApiModelProperty(value = "다중번들번호", position = 1)
    private long multiBundleNo;

    @ApiModelProperty(value = "배송정보", position = 2)
    private String shipDetail;
}
