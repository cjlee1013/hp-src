package kr.co.homeplus.escrowmng.claim.model.external.mhc;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "적립취소 결과 DTO")
public class SaveCancelMhcGetDto {
    @ApiModelProperty(notes = "결과코드", required = true, position = 1)
    private String returnCode;
    @ApiModelProperty(notes = "결과메시지", required = true, position = 2)
    private String returnMessage;
    @ApiModelProperty(notes = "승인일자(취소)", required = true, position = 3)
    private String mhcAprDate;
    @ApiModelProperty(notes = "승인번호(취소)", required = true, position = 4)
    private String mhcAprNumber;
    @ApiModelProperty(notes = "적립취소포인트", required = true, position = 5)
    private String mhcSaveCancelPoint;
}
