package kr.co.homeplus.escrowmng.claim.model.external;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "OCB 사용취소 응답 DTO")
public class ClaimOcbCancelGetDto {
    @ApiModelProperty(notes = "결과코드", required = true, position = 1)
    private String returnCode = "0000";
    @ApiModelProperty(notes = "결과메시지", required = true, position = 2)
    private String returnMessage;
    @ApiModelProperty(notes = "(기요청된)가맹점고유거래ID", required = true, position = 3)
    private String mctTransactionId;
    @ApiModelProperty(notes = "OK캐쉬백고유거래ID(취소)", required = true, position = 4)
    private String ocbTransactionId;
    // OCB 정산용 승인일시(YYYYMMDD)
    @ApiModelProperty(notes = "승인일자(취소)", required = true, position = 5)
    private String ocbAprDate;
    // OCB 정산용 승인번호
    @ApiModelProperty(notes = "승인번호(취소)", required = true, position = 6)
    private String ocbAprNumber;
    // OCB 취소포인트(취소승인완료금액)
    @ApiModelProperty(notes = "취소된OK캐쉬백포인트", required = true, position = 7)
    private long canceledOcbPoint;
}
