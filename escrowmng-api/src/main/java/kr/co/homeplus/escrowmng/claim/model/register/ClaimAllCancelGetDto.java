package kr.co.homeplus.escrowmng.claim.model.register;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "일괄취소 결과.")
public class ClaimAllCancelGetDto {
    private long successCnt;
    private long failCnt;

    public ClaimAllCancelGetDto(){
        this.successCnt = 0;
        this.failCnt = 0;
    }

    public void setSuccess(){
        this.successCnt += 1;
    }

    public void setFail(){
        this.failCnt += 1;
    }
}
