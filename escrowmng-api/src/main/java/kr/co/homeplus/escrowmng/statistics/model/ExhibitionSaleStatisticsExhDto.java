package kr.co.homeplus.escrowmng.statistics.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.Data;

@Data
public class ExhibitionSaleStatisticsExhDto {
    @RealGridColumnInfo(headText = "전시 기획전명", sortable = true)
    String exhibitionNm;

    @RealGridColumnInfo(headText = "판매수량", sortable = true)
    long saleCount;

    @RealGridColumnInfo(headText = "취소수량", sortable = true)
    long claimCount;

    @RealGridColumnInfo(headText = "판매금액", sortable = true)
    long saleAmt;

    @RealGridColumnInfo(headText = "취소금액", sortable = true)
    long claimAmt;

    @RealGridColumnInfo(headText = "순판매수량", sortable = true)
    long netSaleCount;

    @RealGridColumnInfo(headText = "순판매금액", sortable = true)
    long netSaleAmt;
}
