package kr.co.homeplus.escrowmng.claim.service;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.ClaimRequestSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimMultiBundleRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimMultiItemRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.escrowmng.core.exception.LogicException;
import kr.co.homeplus.escrowmng.enums.ClaimCode;
import kr.co.homeplus.escrowmng.enums.ClaimResponseCode;
import kr.co.homeplus.escrowmng.utils.ObjectUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClaimManagementService {

    private final ClaimProcessService processService;

    private final ClaimCommonService commonService;

    @Value("${spring.profiles.active}")
    private String profilesActive;

    @SuppressWarnings("unchecked")
    public LinkedHashMap<String, Object> orderTotalCancel(ClaimRegSetDto regSetDto) throws Exception {
        // purchaseOrderNo 으로 주문건수 가지고 옴.
        // List 로 취소 데이터 생성하여 처리.
        LinkedHashMap<String, Object> claimResultMap = processService.addTotalClaimRegister(regSetDto);
        // 상태 변경 C1 -> C2
        for(long claimBundleNo : (List<Long>)claimResultMap.get("claim_bundle_no")){
            if (!processService.setClaimStatus(ObjectUtils.toLong(claimResultMap.get("claim_no")), claimBundleNo, regSetDto.getRegId(), ClaimCode.CLAIM_STATUS_C2)) {
                throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
            }
        }
        return claimResultMap;
    }

    /**
     * 주문 취소
     *
     * @param regSetDto
     * @throws Exception
     */
    public LinkedHashMap<String, Object> orderPartCancel(ClaimRegSetDto regSetDto) throws Exception {
        // 클레임 데이터 생성
        LinkedHashMap<String, Object> claimDataMap = cancelRegister(regSetDto);

        // 상태 변경 C1 -> C2
        if (!processService.setClaimStatus(ObjectUtils.toLong(claimDataMap.get("claim_no")), ObjectUtils.toLong(claimDataMap.get("claim_bundle_no")), regSetDto.getRegId(), ClaimCode.CLAIM_STATUS_C2)) {
            throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
        }
        return claimDataMap;
    }

    public LinkedHashMap<String, Object> cancelRegister(ClaimRegSetDto regSetDto) throws Exception {
        // 환불 예정금액 을 따로 조회 후 등록 ?
        return processService.addClaimRegister(regSetDto);
    }
}