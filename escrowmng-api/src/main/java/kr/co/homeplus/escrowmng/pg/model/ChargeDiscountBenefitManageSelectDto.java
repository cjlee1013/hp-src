package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단관리 > 청구할인 혜택 관리 조회 DTO")
public class ChargeDiscountBenefitManageSelectDto {

    @ApiModelProperty(notes = "사이트유형(HOME:홈플러스,CLUB:더클럽)")
    private String schSiteType;

    @ApiModelProperty(notes = "청구할인명")
    private String schChargeDiscountNm;

    @ApiModelProperty(notes = "적용시작일자")
    private String schApplyStartDt;

    @ApiModelProperty(notes = "적용종료일자")
    private String schApplyEndDt;

    @ApiModelProperty(notes = "사용여부")
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "사용여부")
    private String schUseYn;

}
