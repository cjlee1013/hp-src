package kr.co.homeplus.escrowmng.order.model.entity;

public class ItmPartnerSellerShipEntity {
    //
    public String shipPolicyNo;
    //
    public String partnerId;
    //
    public String storeId;
    //
    public String defaultYn;
    //
    public String shipPolicyNm;
    //
    public String shipMethod;
    //
    public String releaseDay;
    //
    public String releaseTime;
    //
    public String holidayExceptYn;
    //
    public String shipType;
    //
    public String shipKind;
    //
    public String shipFee;
    //
    public String freeCondition;
    //
    public String dispYn;
    //
    public String diffYn;
    //
    public String diffQty;
    //
    public String prepaymentYn;
    //
    public String claimShipFee;
    //
    public String extraJejuFee;
    //
    public String extraIslandFee;
    //
    public String shipArea;
    //
    public String releaseZipcode;
    //
    public String releaseAddr1;
    //
    public String releaseAddr2;
    //
    public String returnZipcode;
    //
    public String returnAddr1;
    //
    public String returnAddr2;
    //
    public String safeNumberUseYn;
    //
    public String useYn;
    //
    public String regDt;
    //
    public String regId;
    //
    public String chgDt;
    //
    public String chgId;
}
