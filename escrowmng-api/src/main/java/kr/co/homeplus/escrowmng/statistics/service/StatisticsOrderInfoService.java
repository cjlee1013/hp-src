package kr.co.homeplus.escrowmng.statistics.service;

import java.util.List;
import kr.co.homeplus.escrowmng.statistics.mapper.StatisticsOrderInfoSlaveMapper;
import kr.co.homeplus.escrowmng.statistics.model.StatisticsOrderInfoDto;
import kr.co.homeplus.escrowmng.statistics.model.StatisticsOrderInfoSelectDto;
import kr.co.homeplus.escrowmng.statistics.model.StatisticsPartnerOrderInfoDto;
import kr.co.homeplus.escrowmng.statistics.model.StatisticsStoreOrderInfoDto;
import kr.co.homeplus.escrowmng.statistics.model.StoreBasicInfoDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StatisticsOrderInfoService {
    private final StatisticsOrderInfoSlaveMapper statisticsOrderInfoSlaveMapper;

    /**
     * 통계주문정보 랭킹 리스트 조회
     * @param statisticsOrderInfoSelectDto
     * @return
     */
    public List<StatisticsOrderInfoDto> getStatisticsOrderInfoRankingList(StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) {
        return statisticsOrderInfoSlaveMapper.selectStatisticsOrderInfoRankingList(statisticsOrderInfoSelectDto);
    }

    /**
     * 통계주문정보 점포상품 랭킹 리스트 조회
     * @param statisticsOrderInfoSelectDto
     * @return
     */
    public List<StatisticsOrderInfoDto> getStatisticsOrderInfoStoreItemRankingList(StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) {
        return statisticsOrderInfoSlaveMapper.selectStatisticsOrderInfoStoreItemRankingList(statisticsOrderInfoSelectDto);
    }

    /**
     * 통계주문정보 점포별 리스트 조회
     * @param statisticsOrderInfoSelectDto
     * @return
     */
    public List<StatisticsStoreOrderInfoDto> getStatisticsOrderInfoListByStore(StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) {
        return statisticsOrderInfoSlaveMapper.selectStatisticsOrderInfoListByStore(statisticsOrderInfoSelectDto);
    }

    /**
     * 통계주문정보 판매업체별 리스트 조회
     * @param statisticsOrderInfoSelectDto
     * @return
     */
    public List<StatisticsPartnerOrderInfoDto> getStatisticsOrderInfoListByPartner(StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) {
        return statisticsOrderInfoSlaveMapper.selectStatisticsOrderInfoListByPartner(statisticsOrderInfoSelectDto);
    }

    /**
     * 점포 기본정보 조회
     * @param storeId
     * @return
     */
    public StoreBasicInfoDto getStoreBasicInfo(int storeId) {
        return statisticsOrderInfoSlaveMapper.selectStoreBasicInfo(storeId);
    }
}
