package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "카드 PREFIX 관리")
@Getter
@Setter
@EqualsAndHashCode
public class CardPrefixManageDto {
    @ApiModelProperty(value = "카드프리픽스", position = 1)
    @AllowInput(types = {PatternConstants.NUM}, message = "카드프리픽스")
    private String cardPrefixNo;

    @ApiModelProperty(value = "카드종류(0:자사카드,1:MyHomeplus카드,4:신한카드,6:타사카드,8:상품권(모바일,디지털),9:직불카드)", position = 2)
    private String cardKind;

    @ApiModelProperty(value = "결제수단코드", position = 3)
    @AllowInput(types = {PatternConstants.NUM, PatternConstants.ENG}, message = "결제수단코드")
    private String methodCd;

    @ApiModelProperty(value = "결제수단코드명", position = 4)
    private String methodNm;

    @ApiModelProperty(value = "카드상세종류코드(0:일반,1:법인,2:체크,3:임직원,4:제휴)", position = 5)
    @AllowInput(types = {PatternConstants.NUM}, message = "카드상세종류코드")
    private String cardDetailKind;

    @ApiModelProperty(value = "카드상세종류코드명", position = 6)
    private String cardDetailKindNm;

    @ApiModelProperty(value = "제휴카드종류", position = 7)
    private String affiliateCardKind;

    @ApiModelProperty(value = "카드회사코드", position = 8)
    private String cardCompanyCd;

    @ApiModelProperty(value = "카드회사명", position = 9)
    private String cardCompanyNm;

    @ApiModelProperty(value = "카드식별코드", position = 10)
    private String cardIdenCd;

    @ApiModelProperty(value = "카드식별명", position = 11)
    private String cardIdenNm;

    private List<CardPrefixManageDto> cardPrefixManageDtoList;
}
