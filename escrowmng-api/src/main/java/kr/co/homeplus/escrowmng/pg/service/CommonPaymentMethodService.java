package kr.co.homeplus.escrowmng.pg.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import kr.co.homeplus.escrowmng.constants.Constants;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodSelectDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CommonPaymentMethodService {

    private final PaymentMethodService paymentMethodService;

    /**
     * parentMethod로 조회 후 중복되는 methodCd를 제거 후 결제수단 종류를 반환
     * @param parentMethodCd
     * @return
     * @throws Exception
     */
    public List<PaymentMethodDto> getDistinctMethodCd(String parentMethodCd) throws Exception{
        PaymentMethodSelectDto paymentMethodSelectDto = new PaymentMethodSelectDto();
        paymentMethodSelectDto.setUseYn(Constants.USE_ALL);
        paymentMethodSelectDto.setParentMethodCd(parentMethodCd);

        List<PaymentMethodDto> paymentMethodDtoList = paymentMethodService.getPaymentMethodByParam(paymentMethodSelectDto);
        paymentMethodDtoList = paymentMethodDtoList.stream()
            .filter(distinctByKey(PaymentMethodDto::getMethodCd))
            .collect(Collectors.toList());

        return paymentMethodDtoList;
    }

    /**
     * stream 객체에서 중복제거용 Key 설정
     * @param keyExtractor Function<? super T, ?>
     * @param <T> <T>
     * @return Predicate<T>
     */
    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object,Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    /**
     * 상위결제수단 조회 후 siteType 기준으로 리스트 분리
     * @param parentMethodCd
     * @return
     * @throws Exception
     */
    public Map<String, List<PaymentMethodDto>> getMethodCdBySiteType(String parentMethodCd) throws Exception {
        PaymentMethodSelectDto paymentMethodSelectDto = new PaymentMethodSelectDto();
        paymentMethodSelectDto.setUseYn(Constants.USE_Y);
        paymentMethodSelectDto.setParentMethodCd(parentMethodCd);

        List<PaymentMethodDto> paymentMethodDtoList = paymentMethodService.getPaymentMethodByParam(paymentMethodSelectDto);
        Map<String, List<PaymentMethodDto>> paymentMethodDtoListMap = paymentMethodDtoList.stream()
            .collect(Collectors.groupingBy(PaymentMethodDto::getSiteType));

        return paymentMethodDtoListMap;
    }
}
