package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimPaymentProcessGetDto {

    @ApiModelProperty(value = "결제수단")
    private String paymentType;
    @ApiModelProperty(value = "등록일자")
    private String regDt;
    @ApiModelProperty(value = "등록채널")
    private String regChannel;
    @ApiModelProperty(value = "등록ID")
    private String regId;
    @ApiModelProperty(value = "환불일자")
    private String payCompleteDt;
    @ApiModelProperty(value = "수정채널")
    private String chgChannel;
    @ApiModelProperty(value = "수정ID")
    private String chgId;
}
