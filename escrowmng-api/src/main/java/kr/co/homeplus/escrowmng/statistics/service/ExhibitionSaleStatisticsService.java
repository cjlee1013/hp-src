package kr.co.homeplus.escrowmng.statistics.service;

import java.util.List;
import kr.co.homeplus.escrowmng.statistics.mapper.ExhibitionSaleStatisticsSlaveMapper;
import kr.co.homeplus.escrowmng.statistics.model.ExhibitionSaleStatisticsExhDto;
import kr.co.homeplus.escrowmng.statistics.model.ExhibitionSaleStatisticsItemDto;
import kr.co.homeplus.escrowmng.statistics.model.ExhibitionSaleStatisticsSelectDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ExhibitionSaleStatisticsService {
    private final ExhibitionSaleStatisticsSlaveMapper exhibitionSaleStatisticsSlaveMapper;
    public List<ExhibitionSaleStatisticsExhDto> getExhibitionSaleStatisticsExh(ExhibitionSaleStatisticsSelectDto exhibitionSaleStatisticsSelectDto) {
        return exhibitionSaleStatisticsSlaveMapper.selectExhibitionSaleStatisticsExh(exhibitionSaleStatisticsSelectDto);
    }

    public List<ExhibitionSaleStatisticsItemDto> getExhibitionSaleStatisticsItem(ExhibitionSaleStatisticsSelectDto exhibitionSaleStatisticsSelectDto) {
        return exhibitionSaleStatisticsSlaveMapper.selectExhibitionSaleStatisticsItem(exhibitionSaleStatisticsSelectDto);
    }
}
