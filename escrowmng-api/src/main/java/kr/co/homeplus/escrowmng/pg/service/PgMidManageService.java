package kr.co.homeplus.escrowmng.pg.service;

import java.util.List;
import kr.co.homeplus.escrowmng.pg.model.PgMidManageModel;
import kr.co.homeplus.escrowmng.pg.mapper.PgMidMasterMapper;
import kr.co.homeplus.escrowmng.pg.mapper.PgMidSlaveMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PgMidManageService {

    private final PgMidMasterMapper pgMidMasterMapper;
    private final PgMidSlaveMapper pgMidSlaveMapper;

    /**
     * PG 정보 조회
     * @param pgKind
     * @param useYn
     * @return
     */
    public List<PgMidManageModel> getPgMidManage(String pgKind, String useYn) {
        return pgMidSlaveMapper.selectPgMidManage(pgKind, useYn);
    }

    /**
     * PG MID 등록
     * @param pgMidManageModel
     * @return
     * @throws Exception
     */
    public int savePgMidManage(PgMidManageModel pgMidManageModel) {
        return pgMidMasterMapper.insertPgMidManage(pgMidManageModel);
    }
}
