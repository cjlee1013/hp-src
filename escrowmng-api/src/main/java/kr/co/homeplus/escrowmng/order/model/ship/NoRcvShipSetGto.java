package kr.co.homeplus.escrowmng.order.model.ship;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "주문관리 > 상세 > 미수취등록")
public class NoRcvShipSetGto {

    @ApiModelProperty(value= "배송번호", position = 2)
    @NotNull(message = "배송번호")
    @Min(value = 1, message = "배송번호")
    private long bundleNo;

    @ApiModelProperty(value= "미수취신고유형(NA:상품미도착,NR:도난의심,NT:배송조회안됨,NE:ETC)", position = 3)
    @Pattern(regexp = "NA|NR|NT|NE", message = "미수취신고유형")
    @NotNull(message = "미수취신고유형")
    private String noRcvDeclrType;

    @ApiModelProperty(value= "미수취 상세 사유", position = 4)
    private String noRcvDetailReason;

    @ApiModelProperty(value= "미수취 등록자(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  admin : 사번으로 인식하여 사원정보 노출)", position = 6)
    @NotNull(message = "미수취 등록자")
    @NotEmpty(message = "미수취 등록자")
    private String regId;

    @ApiModelProperty(value= "구매주문번호", position = 6, hidden = true)
    private long purchaseOrderNo;

    @ApiModelProperty(value= "고객번호", position = 8, hidden = true)
    private long userNo;

    @ApiModelProperty(value= "파트너ID", position = 9, hidden = true)
    private String partnerId;

}
