package kr.co.homeplus.escrowmng.claim.enums;

import java.util.Arrays;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum  MarketClaimAction {

    NAVER_CANCEL_ACTION(new String[]{"C1", "C2"}),
    NAVER_RETURN_ACTION(new String[]{"C1", "C2", "C8", "C9"}),
    NAVER_EXCHANGE_ACTION(new String[]{"C2", "C8", "C9"}),
    ELEVEN_CANCEL_ACTION(new String[]{"C1", "C2", "C9"}),
    ELEVEN_RETURN_ACTION(new String[]{"C1", "C2", "C8", "C9"}),
    ELEVEN_EXCHANGE_ACTION(new String[]{"C8", "C9"}),
    ;

    private final String[] claimStatusCodeArray;

    public static boolean isActionCheck(String marketType, String actionType, String claimStatusCode) {
        return Stream.of(MarketClaimAction.values())
            .filter(e -> e.name().contains(marketType))
            .filter(e -> e.name().contains(actionType))
            .anyMatch(e -> Arrays.asList(e.claimStatusCodeArray).contains(claimStatusCode));
    }

}
