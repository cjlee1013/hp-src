package kr.co.homeplus.escrowmng.pg.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.pg.model.CardPrefixManageDto;
import kr.co.homeplus.escrowmng.pg.model.CardPrefixManageSelectDto;

@EscrowSlaveConnection
public interface CardPrefixManageSlaveMapper {
    List<CardPrefixManageDto> selectCardPrefixManageList(CardPrefixManageSelectDto cardPrefixManageSelectDto);
}
