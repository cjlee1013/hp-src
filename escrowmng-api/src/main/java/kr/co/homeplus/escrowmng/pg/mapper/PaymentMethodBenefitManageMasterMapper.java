package kr.co.homeplus.escrowmng.pg.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodBenefitManageDto;

@EscrowMasterConnection
public interface PaymentMethodBenefitManageMasterMapper {
    int insertPaymentMethodBenefitManage(PaymentMethodBenefitManageDto paymentMethodBenefitManageDto);
}
