package kr.co.homeplus.escrowmng.order.sql;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimAccumulateEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimAdditionEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimBundleEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimInfoEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimItemEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimMstEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimMultiBundleEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimMultiItemEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimOptEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimPaymentEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.OrderPromoEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ProcessHistoryEntity;
import kr.co.homeplus.escrowmng.entity.dms.ItmPartnerEntity;
import kr.co.homeplus.escrowmng.entity.dms.ItmPartnerSellerEntity;
import kr.co.homeplus.escrowmng.enums.MysqlFunction;
import kr.co.homeplus.escrowmng.enums.OrderCode;
import kr.co.homeplus.escrowmng.order.constants.CustomConstants;
import kr.co.homeplus.escrowmng.order.factory.EntityFactory;
import kr.co.homeplus.escrowmng.order.model.entity.BundleEntity;
import kr.co.homeplus.escrowmng.order.model.entity.CashReceiptEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ItmPartnerManagerEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ItmPartnerSellerShipEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ItmPartnerStoreEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderDiscountCartEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderDiscountEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderItemEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderOptEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderPaymentDivideEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PaymentEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PaymentInfoEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PaymentMethodEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PurchaseAccumulateEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PurchaseGiftEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PurchaseGiftItemEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PurchaseOrderEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PurchaseStoreInfoEntity;
import kr.co.homeplus.escrowmng.order.model.entity.SafetyIssueEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingAddrEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingItemEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingMileagePaybackEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingNonRcvEntity;
import kr.co.homeplus.escrowmng.order.model.entity.StoreSlotMngEntity;
import kr.co.homeplus.escrowmng.order.model.entity.multi.MultiBundleEntity;
import kr.co.homeplus.escrowmng.order.model.entity.multi.MultiOrderItemEntity;
import kr.co.homeplus.escrowmng.order.model.entity.multi.MultiSafetyIssueEntity;
import kr.co.homeplus.escrowmng.order.model.entity.multi.MultiShippingAddrEntity;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchSetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchTckSetDto;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import kr.co.homeplus.escrowmng.utils.ObjectUtils;
import kr.co.homeplus.escrowmng.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class OrderSearchSql {

    // 공통코드 조회.
    private static final String escrowMngCode = "(SELECT IFNULL(imc.mc_nm, '''') AS mc_cd FROM dms.itm_mng_code imc where imc.gmc_cd = ''{0}'' AND imc.mc_cd = {1})";

    /**
     * 주문관리 조회 Query.
     *
     * @return query
     */
    public static String selectOrderSearchInfo(OrderSearchSetDto orderSearchSetDto) throws Exception {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        PaymentInfoEntity paymentInfo = EntityFactory.createEntityIntoValue(PaymentInfoEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        OrderDiscountEntity orderDiscount = EntityFactory.createEntityIntoValue(OrderDiscountEntity.class, Boolean.TRUE);
        OrderDiscountCartEntity orderDiscountCart = EntityFactory.createEntityIntoValue(OrderDiscountCartEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class,  Boolean.TRUE);
        PurchaseStoreInfoEntity purchaseStoreInfo = EntityFactory.createEntityIntoValue(PurchaseStoreInfoEntity.class, Boolean.TRUE);
        // 조회기간 타입에 따른 조회기준설정.
        String schPeriodType = orderSearchSetDto.getSchPeriodType().equals("01") ? purchaseOrder.orderDt : payment.paymentFshDt;
        // 상세검색
        String detailSearchColumn;
        switch (orderSearchSetDto.getSchDetailType()){
            case "PURCHASE":
            case "SUB_ORDER" :
                detailSearchColumn = purchaseOrder.purchaseOrderNo;
                break;
            case "ORDER":
                detailSearchColumn = orderItem.orderItemNo;
                break;
            case "SHIP":
                detailSearchColumn = shippingItem.bundleNo;
                break;
            case "ITEM":
                detailSearchColumn = orderItem.itemNo;
                break;
            case "ITEM_NM":
                detailSearchColumn = orderItem.itemNm1;
                break;
            case "BUYER_TEL_NO" :
                detailSearchColumn = purchaseOrder.buyerMobileNo;
                break;
            case "RECEIVER_TEL_NO" :
                detailSearchColumn = shippingAddr.shipMobileNo;
                break;
            case "PG_OID" :
                detailSearchColumn = paymentInfo.pgOid;
                break;
            case "OMNI_PURCHASE" :
                detailSearchColumn = purchaseStoreInfo.purchaseStoreInfoNo;
                break;
            case "MARKETORDERNO" :
                detailSearchColumn = purchaseOrder.marketOrderNo;
                break;
            default:
                detailSearchColumn = null;
        }

        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, payment.paymentStatus, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(payment.paymentStatus, "P5"), "'P3'", payment.paymentStatus))), payment.paymentStatus),
                purchaseOrder.orderDt,
                payment.paymentFshDt,
                purchaseOrder.buyerEmail,
                purchaseOrder.siteType,
                purchaseOrder.paymentNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE"),
                        purchaseOrder.orgPurchaseOrderNo,
                        purchaseOrder.purchaseOrderNo
                    ),
                    purchaseOrder.purchaseOrderNo
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE"),
                        purchaseOrder.purchaseOrderNo,
                        "''"
                    ),
                    purchaseOrder.orgPurchaseOrderNo
                ),
                orderItem.storeType,
                bundle.bundleNo,
                orderItem.orderItemNo,
                orderItem.itemNo,
                orderItem.itemNm1,
                orderItem.orderPrice,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(bundle.prepaymentYn, "Y"),
                        SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(bundle.shipPrice, bundle.shipDiscountPrice)),
//                        "'착불'"
                        "'0'"
                    ),
                    bundle.shipPrice
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(bundle.prepaymentYn, "Y"),
                        bundle.islandShipPrice,
//                        "'착불'"
                        "'0'"
                    ),
                    bundle.islandShipPrice
                ),
//                SqlUtils.aliasToRow(purchaseOrder.totShipPrice, bundle.shipPrice),
//                SqlUtils.aliasToRow(purchaseOrder.totIslandShipPrice, bundle.islandShipPrice),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(SqlUtils.customOperator(CustomConstants.MINUS, SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMst.totAddShipPrice), SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMst.totAddIslandShipPrice)))
                    .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                    .INNER_JOIN(
                        SqlUtils.createJoinCondition(
                            claimMst,
                            ObjectUtils.toArray(claimBundle.claimNo),
                            ObjectUtils.toArray(
                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                            )
                        )
                    )
                    .WHERE(
                        SqlUtils.equalColumn(claimBundle.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                        SqlUtils.equalColumn(claimBundle.bundleNo, bundle.bundleNo)
                    ),
                    "claim_price"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.PLUS, ObjectUtils.toArray(purchaseOrder.totItemDiscountAmt, purchaseOrder.totCardDiscountAmt,  purchaseOrder.totCartDiscountAmt, purchaseOrder.totEmpDiscountAmt, purchaseOrder.totPromoDiscountAmt, purchaseOrder.totShipDiscountAmt, purchaseOrder.totAddTdDiscountAmt, purchaseOrder.totAddDsDiscountAmt)
                        ),
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(
                                SqlUtils.sqlFunction(
                                    MysqlFunction.IFNULL_SUM_ZERO,
                                    SqlUtils.customOperator(
                                        CustomConstants.PLUS,
                                        ObjectUtils.toArray( claimMst.totItemDiscountAmt, claimMst.totCardDiscountAmt, claimMst.totCartDiscountAmt, claimMst.totEmpDiscountAmt, claimMst.totPromoDiscountAmt, claimMst.totShipDiscountAmt, claimMst.totAddTdDiscountAmt, claimMst.totAddDsDiscountAmt )
                                    )
                                )
                            )
                            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                            .INNER_JOIN(
                                SqlUtils.createJoinCondition(
                                    claimMst,
                                    ObjectUtils.toArray(claimBundle.claimNo),
                                    ObjectUtils.toArray(
                                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                                    )
                                )
                            )
                            .WHERE(
                                SqlUtils.equalColumn(claimBundle.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                                SqlUtils.equalColumn(claimBundle.bundleNo, bundle.bundleNo)
                            )
                        )
                    ),
                    "discount_amt"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        payment.totAmt,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(claimPayment.paymentAmt, claimPayment.substitutionAmt))))
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimBundle.claimNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, purchaseOrder.purchaseOrderNo)
                                )
                        )
                    ),
                    payment.totAmt
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.storeId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "partnerNm"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(purchaseOrder.marketType, "'-'"), purchaseOrder.marketType),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(purchaseOrder.marketOrderNo, "'-'"), purchaseOrder.marketOrderNo),
                purchaseOrder.buyerNm,
                shippingAddr.receiverNm,
                SqlUtils.sqlFunction(MysqlFunction.FN_PAYMENT_ROWNUM, ObjectUtils.toArray(payment.paymentNo, "'kind'"), paymentInfo.pgKind),
                SqlUtils.sqlFunction(MysqlFunction.FN_PAYMENT_ROWNUM, ObjectUtils.toArray(payment.paymentNo, "'method'"), paymentInfo.parentMethodCd),
                purchaseOrder.buyerMobileNo,
                SqlUtils.aliasToRow(shippingAddr.shipMobileNo, "receiver_mobile_no"),
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(purchaseOrder.substitutionOrderYn, "Y"), "'Y'", "'-'"), purchaseOrder.substitutionOrderYn),
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IS_NULL, purchaseStoreInfo.purchaseStoreInfoNo), "'-'", purchaseStoreInfo.purchaseStoreInfoNo), purchaseStoreInfo.purchaseStoreInfoNo),
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, bundle.sordNo), bundle.sordNo, "'-'"), bundle.sordNo),
                purchaseStoreInfo.tradeNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray( purchaseOrder.purchaseOrderNo, orderItem.orderItemNo, orderItem.itemNo )))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(purchaseStoreInfo, ObjectUtils.toArray(bundle.purchaseStoreInfoNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(orderDiscount, ObjectUtils.toArray( orderItem.orderItemNo, orderItem.itemNo )))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(orderDiscountCart, ObjectUtils.toArray( orderDiscount.orderDiscountNo )));

        //주문/상품/배송
        if(!"PURCHASE,ORDER,SHIP,PG_OID,SUB_ORDER,MARKETORDERNO".contains(orderSearchSetDto.getSchDetailType()) || EscrowString.isEmpty(orderSearchSetDto.getSchDetailContents())){
            query.WHERE(
                SqlUtils.betweenMinute(schPeriodType, orderSearchSetDto.getSchStartDt(), orderSearchSetDto.getSchEndDt())
            );
        }

        if (detailSearchColumn != null && EscrowString.isNotEmpty(orderSearchSetDto.getSchDetailContents())) {
            if(orderSearchSetDto.getSchDetailType().equalsIgnoreCase("ITEM_NM")){
                query.WHERE(
                    SqlUtils.middleLike(detailSearchColumn, orderSearchSetDto.getSchDetailContents())
                );
            } else if(orderSearchSetDto.getSchDetailType().equalsIgnoreCase("PURCHASE")){
                query.WHERE(
                    SqlUtils.OR(
                        Boolean.TRUE,
                        SqlUtils.equalColumnByInput(detailSearchColumn, orderSearchSetDto.getSchDetailContents()),
                        "(".concat(
                            SqlUtils.AND(
                                SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, orderSearchSetDto.getSchDetailContents()),
                                SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE")
                            )
                        ).concat(")")
                    )
                );
            } else if(orderSearchSetDto.getSchDetailType().equalsIgnoreCase("PG_OID")){
                query.INNER_JOIN(
                    SqlUtils.createJoinCondition(
                        paymentInfo,
                        ObjectUtils.toArray(purchaseOrder.paymentNo),
                        ObjectUtils.toArray(SqlUtils.equalColumnByInput(detailSearchColumn, orderSearchSetDto.getSchDetailContents()))
                    )
                );
            } else if(orderSearchSetDto.getSchDetailType().equalsIgnoreCase("SUB_ORDER")){
                if(orderSearchSetDto.getPaymentStatus().contains("P3")){
                    orderSearchSetDto.setPaymentStatus(orderSearchSetDto.getPaymentStatus().concat(",P5"));
                }
                query.WHERE(
                    SqlUtils.equalColumnByInput(detailSearchColumn, orderSearchSetDto.getSchDetailContents()),
                    SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB")
                );
            } else {
                query.WHERE(
                    SqlUtils.equalColumnByInput(detailSearchColumn, orderSearchSetDto.getSchDetailContents())
                );
            }
        }

        if (!orderSearchSetDto.getPaymentStatus().contains("ALL")) {
            query.WHERE(
                SqlUtils.getConditionBySqlPattern("IN", payment.paymentStatus, orderSearchSetDto.getPaymentStatus().split(","))
            );
        }

        if (!StringUtils.equalsIgnoreCase(orderSearchSetDto.getAffiliateCd(), "ALL")) {
            query.WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.marketType, orderSearchSetDto.getAffiliateCd().toUpperCase(Locale.KOREA))
            );
        }

        if (!orderSearchSetDto.getSchStoreType().equalsIgnoreCase("ALL")) {
            query.WHERE(
                SqlUtils.equalColumnByInput(orderItem.storeType, orderSearchSetDto.getSchStoreType())
            );
            if(!StringUtils.isEmpty(orderSearchSetDto.getSchStoreId())){
                String searchStoreType = orderSearchSetDto.getSchStoreType();
                if(searchStoreType.equals("DS")){
                    searchStoreType = orderItem.partnerId;
                } else {
                    searchStoreType = orderItem.storeId;
                }
                query.WHERE(
                    SqlUtils.equalColumnByInput(searchStoreType, orderSearchSetDto.getSchStoreId())
                );
            }
        }

        if(EscrowString.isNotEmpty(orderSearchSetDto.getSchUserSearch())){
            String nonUserSearchColumn = purchaseOrder.userNo;
            // 비회원
            if(orderSearchSetDto.getSchNonUserYn().equals("Y")){
                switch (orderSearchSetDto.getSchBuyerInfo()) {
                    case "02" :
                        nonUserSearchColumn = purchaseOrder.buyerMobileNo;
                        break;
                    case "03" :
                        nonUserSearchColumn = purchaseOrder.buyerEmail;
                        break;
                    default :
                        nonUserSearchColumn = purchaseOrder.buyerNm;
                        break;
                }
            }
            query.WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.nomemOrderYn, orderSearchSetDto.getSchNonUserYn()),
                SqlUtils.equalColumnByInput(nonUserSearchColumn, orderSearchSetDto.getSchUserSearch())
            );
        }
        if(EscrowString.isNotEmpty(orderSearchSetDto.getSchReserveShipMethod())){
            query.WHERE(SqlUtils.equalColumnByInput(orderItem.reserveShipMethod, orderSearchSetDto.getSchReserveShipMethod()));
        }
        if(EscrowString.isNotEmpty(orderSearchSetDto.getSchItemType())){
            query.WHERE(SqlUtils.equalColumnByInput(orderItem.itemType, orderSearchSetDto.getSchItemType()));
        }
        query.ORDER_BY(SqlUtils.orderByesc(purchaseOrder.purchaseOrderNo, bundle.bundleNo, orderItem.itemNo));

        List<String> parentMethodCdList = orderSearchSetDto.getParentMethodCd();
        if (parentMethodCdList != null && parentMethodCdList.size() > 0 && !parentMethodCdList.contains("ALL")) {
            if(parentMethodCdList.contains("POINT")){
                parentMethodCdList.remove("POINT");
                parentMethodCdList.add("MILEAGE");
                parentMethodCdList.add("OCB");
                parentMethodCdList.add("MHC");
            }
            if (parentMethodCdList.size() == 1) {
                query = new SQL()
                    .SELECT("*")
                    .FROM(SqlUtils.subTableQuery(query, "ZZ"))
                    .WHERE(
                        SqlUtils.equalColumnByInput(SqlUtils.nonAlias(paymentInfo.parentMethodCd), parentMethodCdList.get(0))
                    );
            } else {
                query = new SQL()
                    .SELECT("*")
                    .FROM(SqlUtils.subTableQuery(query, "ZZ"))
                    .WHERE(
                        SqlUtils.getConditionBySqlPattern("IN", SqlUtils.nonAlias(paymentInfo.parentMethodCd), parentMethodCdList.toArray())
                    );
            }
        }

        log.debug("selectOrderSearchInfo query :: \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 주문정보상세 - 구매자정보
     *
     * @param purchaseOrderNo 주문번호
     * @return 주문정보상세 - 구매자정보 조회 Query
     */
    public static String selectOrderDetailUserInfo(@Param("purchaseOrderNo") long purchaseOrderNo){
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PurchaseOrderEntity orgPurchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                payment.paymentStatus,
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, payment.paymentStatus, ObjectUtils.toArray(payment.paymentStatus), "payment_status_nm"),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_24, purchaseOrder.orderDt, purchaseOrder.orderDt),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_24, payment.paymentFshDt, payment.paymentFshDt),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IN, purchaseOrder.platform, ObjectUtils.toArray("'PC'", "'MWEB'")),
                        purchaseOrder.platform,
                        SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(purchaseOrder.platform, "'('", purchaseOrder.osType, "')'"))
                    ),
                    "order_device"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(purchaseOrder.marketType, "'-'"), purchaseOrder.marketType),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(purchaseOrder.marketOrderNo, "'-'"), purchaseOrder.marketOrderNo),
                purchaseOrder.userNo,
                purchaseOrder.nomemOrderYn,
                purchaseOrder.buyerNm,
                purchaseOrder.buyerEmail,
                purchaseOrder.buyerMobileNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE"),
                        purchaseOrder.orgPurchaseOrderNo,
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.equalColumnByInput(purchaseOrder.cmbnYn, "Y"),
                                SqlUtils.subTableQuery(
                                    new SQL().SELECT(orgPurchaseOrder.purchaseOrderNo).FROM(EntityFactory.getTableName(orgPurchaseOrder))
                                        .WHERE(
                                            SqlUtils.equalColumn(orgPurchaseOrder.orgPurchaseOrderNo, purchaseOrder.purchaseOrderNo),
                                            SqlUtils.equalColumnByInput(orgPurchaseOrder.orderType, "ORD_COMBINE")
                                        ).ORDER_BY(SqlUtils.orderByesc(orgPurchaseOrder.purchaseOrderNo)).LIMIT(1).OFFSET(0)
                                ),
                                "''"
                            )
                        )
                    ),
                    purchaseOrder.orgPurchaseOrderNo
                ),
                purchaseOrder.orderType,
                shippingItem.giftAcpDt,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseOrder.orderGiftYn, "Y"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NULL, purchaseOrder.dsGiftAutoCancelDt),
                                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(purchaseOrder.orderDt, "3"))),
                                purchaseOrder.dsGiftAutoCancelDt
                            )
                        ),
                        null
                    ),
                    purchaseOrder.dsGiftAutoCancelDt
                ),
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(purchaseOrder.orderGiftYn, "Y"), shippingAddr.receiverNm, "'-'"), "receive_gift_nm"),
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(purchaseOrder.orderGiftYn, "Y"), shippingAddr.shipMobileNo, "'-'"), "receive_gift_phone"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(shippingAddr.corpOrderYn, "'-'"), shippingAddr.corpOrderYn),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(purchaseOrder.affiliateCd, "'-'"), purchaseOrder.affiliateCd)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(payment),
                    ObjectUtils.toList( SqlUtils.onClause(new Object[]{ purchaseOrder.paymentNo }, payment ) )
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            ).LIMIT(1).OFFSET(0);
        log.debug("selectOrderDetailUserInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderDetailPriceInfo(@Param("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, payment.totAmt),
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMst.completeAmt)
                    ),
                    payment.totAmt
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.totItemPrice),
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMst.totItemPrice)
                    ),
                    purchaseOrder.totItemPrice
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(
                            MysqlFunction.MAX,
                            SqlUtils.customOperator(
                                CustomConstants.PLUS,
                                ObjectUtils.toArray( purchaseOrder.totItemDiscountAmt, purchaseOrder.totCardDiscountAmt, purchaseOrder.totCartDiscountAmt, purchaseOrder.totEmpDiscountAmt, purchaseOrder.totPromoDiscountAmt, purchaseOrder.totShipDiscountAmt, purchaseOrder.totAddTdDiscountAmt, purchaseOrder.totAddDsDiscountAmt )
                            )
                        ),
                        SqlUtils.sqlFunction(
                            MysqlFunction.IFNULL_SUM_ZERO,
                            SqlUtils.customOperator(
                                CustomConstants.PLUS,
                                ObjectUtils.toArray( claimMst.totItemDiscountAmt, claimMst.totCardDiscountAmt, claimMst.totCartDiscountAmt, claimMst.totEmpDiscountAmt, claimMst.totPromoDiscountAmt, claimMst.totShipDiscountAmt, claimMst.totAddTdDiscountAmt, claimMst.totAddDsDiscountAmt )
                            )
                        )
                    ),
                    "tot_discount_amt"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(
                            MysqlFunction.MAX,
                            SqlUtils.customOperator(
                                CustomConstants.PLUS,
                                ObjectUtils.toArray( purchaseOrder.totShipPrice, purchaseOrder.totIslandShipPrice )
                            )
                        ),
                        SqlUtils.sqlFunction(
                            MysqlFunction.IFNULL_SUM_ZERO,
                            SqlUtils.customOperator(
                                CustomConstants.PLUS,
                                ObjectUtils.toArray( claimMst.totShipPrice, claimMst.totIslandShipPrice )
                            )
                        )
                    ),
                    "tot_shipping_amt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.PLUS, claimMst.totAddShipPrice, claimMst.totAddIslandShipPrice), "tot_claim_amt"),
                SqlUtils.appendSingleQuoteWithAlias(0, "tot_deduct_amt"),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(
                            MysqlFunction.MAX,
                            SqlUtils.customOperator(
                                CustomConstants.PLUS,
                                ObjectUtils.toArray(purchaseOrder.totItemDiscountAmt, purchaseOrder.totPromoDiscountAmt)
                            )
                        ),
                        SqlUtils.sqlFunction(
                            MysqlFunction.IFNULL_SUM_ZERO,
                            SqlUtils.customOperator(
                                CustomConstants.PLUS,
                                ObjectUtils.toArray(claimMst.totItemDiscountAmt, claimMst.totPromoDiscountAmt)
                            )
                        )
                    ),
                    purchaseOrder.totItemDiscountAmt
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.totShipDiscountAmt),
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMst.totShipDiscountAmt)
                    ),
                    purchaseOrder.totShipDiscountAmt
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.totCartDiscountAmt),
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMst.totCartDiscountAmt)
                    ),
                    purchaseOrder.totCartDiscountAmt
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.totCardDiscountAmt),
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMst.totCardDiscountAmt)
                    ),
                    purchaseOrder.totCardDiscountAmt
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.totPromoDiscountAmt),
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMst.totPromoDiscountAmt)
                    ),
                    purchaseOrder.totPromoDiscountAmt
                ),
                purchaseOrder.totEmpDiscountAmt,
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(
                            MysqlFunction.MAX,
                            SqlUtils.customOperator(
                                CustomConstants.PLUS,
                                ObjectUtils.toArray(purchaseOrder.totAddTdDiscountAmt, purchaseOrder.totAddDsDiscountAmt)
                            )
                        ),
                        SqlUtils.sqlFunction(
                            MysqlFunction.IFNULL_SUM_ZERO,
                            SqlUtils.customOperator(
                                CustomConstants.PLUS,
                                ObjectUtils.toArray(claimMst.totAddTdDiscountAmt, claimMst.totAddDsDiscountAmt)
                            )
                        )
                    ),
                    "tot_add_coupon_amt"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    SqlUtils.subTableQuery(
                        new SQL().SELECT(claimBundle.claimNo, claimBundle.purchaseOrderNo)
                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                        .WHERE(
                            SqlUtils.equalColumnByInput(claimBundle.purchaseOrderNo, purchaseOrderNo),
                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                        )
                        .GROUP_BY(claimBundle.claimNo, claimBundle.purchaseOrderNo),
                        "cb"
                    ),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(purchaseOrder.purchaseOrderNo, claimBundle.purchaseOrderNo)
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimBundle.claimNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectOrderDetailPriceInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderDetailPurchaseInfo(@Param("purchaseOrderNo") long purchaseOrderNo) throws Exception {

        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        OrderItemEntity changeOrderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, "orderItem");
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimInfoEntity claimInfo = EntityFactory.createEntityIntoValue(ClaimInfoEntity.class, "claimInfo");
        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                orderItem.storeType,
                purchaseOrder.siteType,
                orderItem.partnerId,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.originStoreId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "partnerNm"
                ),
                orderItem.bundleNo,
                shippingItem.shipNo,
                orderItem.orderItemNo,
                orderItem.itemNo,
                orderItem.itemNm1,
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(orderOpt.optNo, "0"), null, orderOpt.orderOptNo), orderOpt.orderOptNo),
                SqlUtils.sqlFunction(MysqlFunction.FN_ORDER_OPTION_NM_MANAGE, orderOpt.orderOptNo, "opt_nm"),
                orderItem.itemPrice,
                SqlUtils.aliasToRow(orderOpt.optQty, orderItem.itemQty),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimOpt.claimOptQty))
                    .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                    .INNER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                    .WHERE(
                        SqlUtils.equalColumn(claimOpt.orderOptNo, orderOpt.orderOptNo),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                    ),
                    "claim_qty"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, "'C'")), "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(orderItem.substitutionYn, "Y"), "'대체안함'", "'대체허용'")),
                        "'-'"
                    ),
                    "substitution_yn"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL_ZERO,
                    SqlUtils.subTableQuery(
                        new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.SUM, SqlUtils.customOperator(CustomConstants.MINUS, claimItem.claimItemQty, SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO,claimItem.changeQty))))
                            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                            .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                            .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimItem.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimMst.oosYn, "Y"))))
                            .WHERE(
                                SqlUtils.equalColumn(claimItem.orderItemNo, orderItem.orderItemNo),
                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                            )
                    ),
                    "oos_qty"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(changeOrderItem.itemNm1)
                                .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
                                .INNER_JOIN(
                                    SqlUtils.createJoinCondition(
                                        EntityFactory.getTableNameWithAlias(changeOrderItem, "orderItem"),
                                        ObjectUtils.toList(SqlUtils.onClause(ObjectUtils.toArray(purchaseOrder.purchaseOrderNo), changeOrderItem))
                                    )
                                )
                                .WHERE(
                                    SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo),
                                    SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB"),
                                    SqlUtils.equalColumn(changeOrderItem.orgItemNo, orderItem.itemNo)
                                ).ORDER_BY(changeOrderItem.orderItemNo).LIMIT(1).OFFSET(0)
                        ),
                        "'-'"
                    ),
                    "change_item_nm"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray( SqlUtils.equalColumnByInput(purchaseOrder.totCartDiscountAmt, "0"), "'N'", "'Y'" ),
                    "discount_cart_yn"
                ),
                orderItem.orderPrice,
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        orderPaymentDivide.itemCouponAmt,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimAddition.additionSumAmt) )
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimAddition, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimAddition.orderItemNo, orderItem.orderItemNo),
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                    SqlUtils.rightLike(claimAddition.additionTypeDetail, "item")
                                )
                        )
                    ),
                    orderItem.discountAmt
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        orderPaymentDivide.cardDiscountAmt,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimAddition.additionSumAmt) )
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimAddition, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimAddition.orderItemNo, orderItem.orderItemNo),
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                    SqlUtils.rightLike(claimAddition.additionTypeDetail, "card")
                                )
                        )
                    ),
                    "card_discount_amt"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        orderPaymentDivide.promoDiscountAmt,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimAddition.additionSumAmt) )
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimAddition, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimAddition.orderItemNo, orderItem.orderItemNo),
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                                    SqlUtils.rightLike(claimAddition.additionTypeDetail, "promo")
                                )
                        )
                    ),
                    orderItem.promoDiscountAmt
                ),

                SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(bundle.shipPrice, bundle.shipDiscountPrice), bundle.shipPrice),
                bundle.islandShipPrice,
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(bundle.shipPrice, bundle.shipDiscountPrice)),
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.MINUS, claimMst.totShipPrice, claimMst.totAddShipPrice)))
                                .FROM(EntityFactory.getTableNameWithAlias(claimMst))
                                .INNER_JOIN(
                                    SqlUtils.createJoinCondition(
                                        claimBundle,
                                        ObjectUtils.toArray(claimMst.claimNo),
                                        ObjectUtils.toArray(
                                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                                        )
                                    )
                                )
                                .WHERE(SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo))
                        )
                    ),
                    "claim_ship_price"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        bundle.islandShipPrice,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.MINUS, claimMst.totIslandShipPrice, claimMst.totIslandShipPrice)))
                                .FROM(EntityFactory.getTableNameWithAlias(claimMst))
                                .INNER_JOIN(
                                    SqlUtils.createJoinCondition(
                                        claimBundle,
                                        ObjectUtils.toArray(claimMst.claimNo),
                                        ObjectUtils.toArray(
                                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                                        )
                                    )
                                )
                                .WHERE(SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo))
                        )
                    ),
                    "claim_add_ship_price"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        bundle.shipDiscountPrice,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMst.totShipDiscountAmt))
                                .FROM(EntityFactory.getTableNameWithAlias(claimMst))
                                .INNER_JOIN(
                                    SqlUtils.createJoinCondition(
                                        claimBundle,
                                        ObjectUtils.toArray(claimMst.claimNo),
                                        ObjectUtils.toArray(
                                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                                        )
                                    )
                                )
                                .WHERE(SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo))
                        )
                    ),
                    bundle.shipDiscountPrice
                ),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.PLUS, claimMst.totAddShipPrice, claimMst.totAddIslandShipPrice)))
                    .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                    .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimBundle.claimNo)))
                    .WHERE(
                        SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                    ),
                    "claim_price"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        purchaseOrder.totCartDiscountAmt,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMst.totCartDiscountAmt) )
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimBundle.claimNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimMst.purchaseOrderNo, purchaseOrder.purchaseOrderNo)
                                )
                        )
                    ),
                    purchaseOrder.totCartDiscountAmt
                ),
                SqlUtils.aliasToRow(orderItem.empDiscountAmt, purchaseOrder.totEmpDiscountAmt),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        payment.totAmt,
                        SqlUtils.subTableQuery(
//                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(claimPayment.paymentAmt, claimPayment.substitutionAmt))))
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimPayment.paymentAmt))
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimBundle.claimNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, purchaseOrder.purchaseOrderNo)
                                )
                        )
                    ),
                    payment.totAmt
                ),
                payment.paymentStatus,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.ANGLE_BRACKET_LEFT,
                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(shippingItem.completeDt, "365"))),
                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_24, "NOW()")
                        ),
                        "'NN'",
                        shippingItem.shipStatus
                    ),
                    shippingItem.shipStatus
                ),
                SqlUtils.subQuery(
                    SqlUtils.sqlFunction(
                        MysqlFunction.CASE,
                        ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.NOT_IN, SqlUtils.sqlFunction(MysqlFunction.MIN, claimBundle.claimStatus), ObjectUtils.toArray("'C3'", "'C4'", "'C9'")), "'N'", "'Y'")
                    ),
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(purchaseOrder.purchaseOrderNo, claimBundle.purchaseOrderNo),
                        SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo)
                    ),
                    "claim_yn"
                ),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, "'M'"), "ship_type"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(bundle.prepaymentYn, "Y"),
                        "'선불'",
                        "'착불'"
                    ),
                    bundle.prepaymentYn
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        orderPaymentDivide.cartCouponAmt,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimInfo.cartCouponCardAmt, claimInfo.cartCouponHomeAmt, claimInfo.cartCouponSellerAmt))))
                                .FROM(EntityFactory.getTableNameWithAlias(claimInfo, "claimInfo"))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimInfo.claimNo, claimInfo.claimBundleNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimInfo.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                                    SqlUtils.equalColumn(claimInfo.orderItemNo, orderPaymentDivide.orderItemNo)
                                )
                        )
                    ),
                    "cart_item_discount_amt"
                ),
                SqlUtils.aliasToRow(orderPaymentDivide.cartCouponAmt, "basic_cart_item_discount_amt"),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(orderPaymentDivide.addTdCouponAmt, orderPaymentDivide.addDsCouponAmt)),
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimAddition.additionSumAmt) )
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimAddition, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimAddition.orderItemNo, orderItem.orderItemNo),
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                    SqlUtils.rightLike(claimAddition.additionTypeDetail, "add")
                                )
                        )
                    ),
                    "add_coupon_amt"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition( orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo, orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(orderItem.bundleNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(orderPaymentDivide, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            )
            .ORDER_BY(
                SqlUtils.orderByesc(orderItem.storeType, bundle.bundleNo, orderItem.orderItemNo, orderOpt.orderOptNo)
            );

        log.debug("selectOrderDetailPurchaseInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectMultiOrderDetailPurchaseInfo(@Param("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        OrderItemEntity changeOrderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, "orderItem");
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimInfoEntity claimInfo = EntityFactory.createEntityIntoValue(ClaimInfoEntity.class, "claimInfo");
        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class, Boolean.TRUE);
        MultiBundleEntity multiBundle = EntityFactory.createEntityIntoValue(MultiBundleEntity.class, Boolean.TRUE);
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        ClaimMultiBundleEntity claimMultiBundle = EntityFactory.createEntityIntoValue(ClaimMultiBundleEntity.class, Boolean.TRUE);
        ClaimMultiItemEntity claimMultiItem = EntityFactory.createEntityIntoValue(ClaimMultiItemEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                orderItem.storeType,
                purchaseOrder.siteType,
                orderItem.partnerId,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.originStoreId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "partnerNm"
                ),
                multiOrderItem.multiBundleNo,
                orderItem.bundleNo,
                shippingItem.shipNo,
                orderItem.orderItemNo,
                orderItem.itemNo,
                orderItem.itemNm1,
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(orderOpt.optNo, "0"), null, orderOpt.orderOptNo), orderOpt.orderOptNo),
                SqlUtils.sqlFunction(MysqlFunction.FN_ORDER_OPTION_NM_MANAGE, orderOpt.orderOptNo, "opt_nm"),
                orderItem.itemPrice,
                SqlUtils.aliasToRow(multiOrderItem.itemQty, multiOrderItem.itemQty),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMultiItem.claimItemQty))
                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .WHERE(
                            SqlUtils.equalColumn(claimMultiItem.multiOrderItemNo, multiOrderItem.multiOrderItemNo),
                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                        ),
                    "claim_qty"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, "'C'")), "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(orderItem.substitutionYn, "Y"), "'대체안함'", "'대체허용'")),
                        "'-'"
                    ),
                    "substitution_yn"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL_ZERO,
                    SqlUtils.subTableQuery(
                        new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.SUM, SqlUtils.customOperator(CustomConstants.MINUS, claimItem.claimItemQty, SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO,claimItem.changeQty))))
                            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                            .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                            .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimItem.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimMst.oosYn, "Y"))))
                            .WHERE(
                                SqlUtils.equalColumn(claimItem.orderItemNo, orderItem.orderItemNo),
                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                            )
                    ),
                    "oos_qty"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(changeOrderItem.itemNm1)
                                .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
                                .INNER_JOIN(
                                    SqlUtils.createJoinCondition(
                                        EntityFactory.getTableNameWithAlias(changeOrderItem, "orderItem"),
                                        ObjectUtils.toList(SqlUtils.onClause(ObjectUtils.toArray(purchaseOrder.purchaseOrderNo), changeOrderItem))
                                    )
                                )
                                .WHERE(
                                    SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo),
                                    SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB"),
                                    SqlUtils.equalColumn(changeOrderItem.orgItemNo, orderItem.itemNo)
                                ).ORDER_BY(changeOrderItem.orderItemNo).LIMIT(1).OFFSET(0)
                        ),
                        "'-'"
                    ),
                    "change_item_nm"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray( SqlUtils.equalColumnByInput(purchaseOrder.totCartDiscountAmt, "0"), "'N'", "'Y'" ),
                    "discount_cart_yn"
                ),
                orderItem.orderPrice,
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        orderPaymentDivide.itemCouponAmt,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimAddition.additionSumAmt) )
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimAddition, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimAddition.orderItemNo, orderItem.orderItemNo),
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                    SqlUtils.rightLike(claimAddition.additionTypeDetail, "item")
                                )
                        )
                    ),
                    orderItem.discountAmt
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        orderPaymentDivide.cardDiscountAmt,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimAddition.additionSumAmt) )
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimAddition, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimAddition.orderItemNo, orderItem.orderItemNo),
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                    SqlUtils.rightLike(claimAddition.additionTypeDetail, "card")
                                )
                        )
                    ),
                    "card_discount_amt"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        orderPaymentDivide.promoDiscountAmt,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimAddition.additionSumAmt) )
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimAddition, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimAddition.orderItemNo, orderItem.orderItemNo),
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                                    SqlUtils.rightLike(claimAddition.additionTypeDetail, "promo")
                                )
                        )
                    ),
                    orderItem.promoDiscountAmt
                ),

                SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(bundle.shipPrice, bundle.shipDiscountPrice), bundle.shipPrice),
                bundle.islandShipPrice,
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(bundle.shipPrice, bundle.shipDiscountPrice)),
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.MINUS, claimMst.totShipPrice, claimMst.totAddShipPrice)))
                                .FROM(EntityFactory.getTableNameWithAlias(claimMst))
                                .INNER_JOIN(
                                    SqlUtils.createJoinCondition(
                                        claimBundle,
                                        ObjectUtils.toArray(claimMst.claimNo),
                                        ObjectUtils.toArray(
                                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                                        )
                                    )
                                )
                                .WHERE(SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo))
                        )
                    ),
                    "claim_ship_price"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        bundle.islandShipPrice,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.MINUS, claimMst.totIslandShipPrice, claimMst.totIslandShipPrice)))
                                .FROM(EntityFactory.getTableNameWithAlias(claimMst))
                                .INNER_JOIN(
                                    SqlUtils.createJoinCondition(
                                        claimBundle,
                                        ObjectUtils.toArray(claimMst.claimNo),
                                        ObjectUtils.toArray(
                                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                                        )
                                    )
                                )
                                .WHERE(SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo))
                        )
                    ),
                    "claim_add_ship_price"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        bundle.shipDiscountPrice,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMst.totShipDiscountAmt))
                                .FROM(EntityFactory.getTableNameWithAlias(claimMst))
                                .INNER_JOIN(
                                    SqlUtils.createJoinCondition(
                                        claimBundle,
                                        ObjectUtils.toArray(claimMst.claimNo),
                                        ObjectUtils.toArray(
                                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                                        )
                                    )
                                )
                                .WHERE(SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo))
                        )
                    ),
                    bundle.shipDiscountPrice
                ),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.PLUS, claimMst.totAddShipPrice, claimMst.totAddIslandShipPrice)))
                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimBundle.claimNo)))
                        .WHERE(
                            SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                        ),
                    "claim_price"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        purchaseOrder.totCartDiscountAmt,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMst.totCartDiscountAmt) )
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimBundle.claimNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimMst.purchaseOrderNo, purchaseOrder.purchaseOrderNo)
                                )
                        )
                    ),
                    purchaseOrder.totCartDiscountAmt
                ),
                SqlUtils.aliasToRow(orderItem.empDiscountAmt, purchaseOrder.totEmpDiscountAmt),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        payment.totAmt,
                        SqlUtils.subTableQuery(
//                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(claimPayment.paymentAmt, claimPayment.substitutionAmt))))
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimPayment.paymentAmt))
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimBundle.claimNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, purchaseOrder.purchaseOrderNo)
                                )
                        )
                    ),
                    payment.totAmt
                ),
                payment.paymentStatus,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.ANGLE_BRACKET_LEFT,
                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(multiOrderItem.completeDt, "365"))),
                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_24, "NOW()")
                        ),
                        "'NN'",
                        multiOrderItem.shipStatus
                    ),
                    multiOrderItem.shipStatus
                ),
                SqlUtils.subQuery(
                    SqlUtils.sqlFunction(
                        MysqlFunction.CASE,
                        ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.NOT_IN, SqlUtils.sqlFunction(MysqlFunction.MIN, claimBundle.claimStatus), ObjectUtils.toArray("'C3'", "'C4'", "'C9'")), "'N'", "'Y'")
                    ),
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(purchaseOrder.purchaseOrderNo, claimBundle.purchaseOrderNo),
                        SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo)
                    ),
                    "claim_yn"
                ),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, "'M'"), "ship_type"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(bundle.prepaymentYn, "Y"),
                        "'선불'",
                        "'착불'"
                    ),
                    bundle.prepaymentYn
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        orderPaymentDivide.cartCouponAmt,
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimInfo.cartCouponCardAmt, claimInfo.cartCouponHomeAmt, claimInfo.cartCouponSellerAmt))))
                                .FROM(EntityFactory.getTableNameWithAlias(claimInfo, "claimInfo"))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimInfo.claimNo, claimInfo.claimBundleNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimInfo.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                                    SqlUtils.equalColumn(claimInfo.orderItemNo, orderPaymentDivide.orderItemNo)
                                )
                        )
                    ),
                    "cart_item_discount_amt"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(orderPaymentDivide.addTdCouponAmt, orderPaymentDivide.addDsCouponAmt)),
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimAddition.additionSumAmt) )
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimAddition, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
                                .WHERE(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                                    SqlUtils.equalColumn(claimAddition.orderItemNo, orderItem.orderItemNo),
                                    SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                    SqlUtils.rightLike(claimAddition.additionTypeDetail, "add")
                                )
                        )
                    ),
                    "add_coupon_amt"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition( orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition( multiBundle, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition( multiOrderItem, ObjectUtils.toArray(multiBundle.multiBundleNo, orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo, orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(orderItem.bundleNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(orderPaymentDivide, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            )
            .ORDER_BY(
                SqlUtils.orderByesc(orderItem.storeType, multiBundle.multiBundleNo, orderItem.orderItemNo, orderOpt.orderOptNo)
            );

        log.debug("selectMultiOrderDetailPurchaseInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderDetailShippingInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("mallType") String mallType) {

        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        SafetyIssueEntity safetyIssue = EntityFactory.createEntityIntoValue(SafetyIssueEntity.class, "safety");
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class, Boolean.TRUE);
        PurchaseStoreInfoEntity purchaseStoreInfo = EntityFactory.createEntityIntoValue(PurchaseStoreInfoEntity.class, Boolean.TRUE);
        StoreSlotMngEntity storeSlotMng = EntityFactory.createEntityIntoValue(StoreSlotMngEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                orderItem.storeType,
                orderItem.partnerId,
                bundle.bundleNo,
                orderItem.orderItemNo,
                shippingItem.shipNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, shippingNonRcv.bundleNo),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "Y"),
//                                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "Y"), "'철회'", "'처리'")),
                                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessType, "C"), "'철회요청'", "'철회완료'")),
                                "'신고'"
                            )
                        ),
                        "''"
                    ),
                    "not_receive_type"
                ),
                shippingItem.userNo,
                shippingAddr.receiverNm,
                shippingAddr.shipMobileNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.storeId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "partner_nm"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(safetyIssue.issuePhoneNo, "'-'"), "issue_phone_no"),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_ADDR, ObjectUtils.toArray(shippingItem.shipNo), "shipping_addr"),
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, shippingItem.shipMethod, ObjectUtils.toArray(shippingItem.shipMethod), "ship_method_nm"),
                shippingItem.dlvCd,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, shippingItem.dlvCd, ObjectUtils.toArray(shippingItem.dlvCd)), "'-'"), "dlv_nm"),
                SqlUtils.caseWhenElse(
                    shippingItem.shipMethod,
                    ObjectUtils.toArray(
                        "'DS_DRCT'", SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(shippingItem.invoiceNo, "'-'")),
                        "'DS_DLV'", SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(shippingItem.invoiceNo, "'-'")),
                        "'TD_DLV'", SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(shippingItem.invoiceNo, "'-'"))
                    ),
                    "''",
                    "invoice_no"
                ),
//                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(shippingItem.invoiceNo, "'-'"), "invoice_no"),
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, shippingItem.shipStatus, ObjectUtils.toArray(shippingItem.shipStatus), "ship_status_nm"),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.orgShipDt, CustomConstants.YYYYMMDD), "org_ship_dt"),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.delayShipDt, CustomConstants.YYYYMMDD), "delay_ship_dt"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, shippingItem.scheduleShipDt),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.scheduleShipDt, CustomConstants.YYYYMMDD)),
                        "'-'"
                    ),
                    "schedule_ship_dt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.FN_ESTIMATED_DELIVERY, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo, shippingItem.bundleNo, shippingItem.orderItemNo), "ship_dt"),
//                SqlUtils.sqlFunction(
//                    MysqlFunction.CASE,
//                    ObjectUtils.toArray(
//                        SqlUtils.AND(SqlUtils.equalColumnByInput(orderItem.mallType, "TD"), SqlUtils.equalColumnByInput(orderItem.storeKind, "NOR")),
//                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(storeSlotMng.shipDt, CustomConstants.YYYYMMDD)),
//                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.reserveShipDt, CustomConstants.YYYYMMDD))
//                    ),
//                    "ship_dt"
//                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CONCAT,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.LEFT, ObjectUtils.toArray(storeSlotMng.slotShipStartTime, 2)),
                                SqlUtils.appendSingleQuote(CustomConstants.COLON),
                                SqlUtils.sqlFunction(MysqlFunction.RIGHT, ObjectUtils.toArray(storeSlotMng.slotShipStartTime, 2))
                            )
                        ),
                        SqlUtils.appendSingleQuote(SqlUtils.wrapWithSpace(CustomConstants.TILDE)),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.LEFT, ObjectUtils.toArray(storeSlotMng.slotShipEndTime, 2)),
                                SqlUtils.appendSingleQuote(CustomConstants.COLON),
                                SqlUtils.sqlFunction(MysqlFunction.RIGHT, ObjectUtils.toArray(storeSlotMng.slotShipEndTime, 2))
                            )
                        )
                    ),
                    "ship_time"
                ),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.confirmDt, CustomConstants.YYYYMMDD), "confirm_dt"),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.shippingDt, CustomConstants.YYYYMMDD), "shipping_dt"),
                shippingItem.completeDt,
                shippingItem.shipMethod,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.ANGLE_BRACKET_LEFT,
                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(shippingItem.completeDt, "30"))),
                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_24, "NOW()")
                        ),
                        "'NN'",
                        shippingItem.shipStatus
                    ),
                    shippingItem.shipStatus
                ),
                SqlUtils.subQuery(
                    SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, SqlUtils.sqlFunction(MysqlFunction.MIN, claimBundle.claimStatus)),
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(claimBundle.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                        SqlUtils.equalColumn(claimBundle.bundleNo, bundle.bundleNo),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'"))
                    ),
                    "claim_status"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        orderItem.itemQty,
                        SqlUtils.sqlFunction(
                            MysqlFunction.IFNULL_ZERO,
                            SqlUtils.subTableQuery(
                                new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.SUM, claimItem.claimItemQty))
                                .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                                .WHERE(
                                    SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                                    SqlUtils.equalColumn(claimItem.itemNo, orderItem.itemNo),
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.sqlFunction(MysqlFunction.IN, claimBundle.claimStatus, ObjectUtils.toArray("'C3'"))
                                )
                            )
                        )
                    ),
                    orderItem.itemQty
                ),
                orderItem.storeKind,
                bundle.shipAreaType,
                bundle.driverNm,
                bundle.driverTelNo,
                purchaseStoreInfo.purchaseStoreInfoNo,
                bundle.sordNo,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, purchaseStoreInfo.slotChgCnt, purchaseStoreInfo.slotChgCnt),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(purchaseStoreInfo.anytimeSlotYn, "'N'"), purchaseStoreInfo.anytimeSlotYn)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ purchaseOrder.purchaseOrderNo }, orderItem ))
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ orderItem.orderItemNo, orderItem.itemNo }, shippingItem ))
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(bundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ purchaseOrder.purchaseOrderNo, shippingItem.bundleNo }, bundle ))
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingAddr),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ shippingItem.shipAddrNo }, shippingAddr ))
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    "safety_issue safety",
                    Arrays.asList( SqlUtils.onClause(new Object[]{ shippingItem.shipAddrNo }, safetyIssue ))
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(shippingNonRcv, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo, shippingItem.bundleNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    purchaseStoreInfo,
                    ObjectUtils.toArray(purchaseOrder.purchaseOrderNo, purchaseOrder.paymentNo, orderItem.storeKind),
                    ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, purchaseStoreInfo.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'"))))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(storeSlotMng, ObjectUtils.toArray(purchaseStoreInfo.shipDt, purchaseStoreInfo.storeId, purchaseStoreInfo.slotId))
            )
            .WHERE(SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo));

        // mall_type 이 TD 일 때
        if(mallType.equalsIgnoreCase(OrderCode.ORDER_ITEM_MALL_TYPE_TD.getResponseCode())) {
            query.WHERE(
                SqlUtils.equalColumnByInput(orderItem.mallType, OrderCode.ORDER_ITEM_MALL_TYPE_TD.getResponseCode()),
                SqlUtils.equalColumnByInput(orderItem.storeKind, OrderCode.ORDER_ITEM_STORE_KIND_NOR.getResponseCode()),
                SqlUtils.sqlFunction(MysqlFunction.NOT_LIKE, ObjectUtils.toArray(shippingItem.shipMethod, "%DLV"))
            );
        } else if (mallType.equalsIgnoreCase(OrderCode.ORDER_ITEM_MALL_TYPE_DS.getResponseCode())) {
            query.AND()
                .WHERE(
                    SqlUtils.OR(
                        SqlUtils.equalColumnByInput(orderItem.mallType, OrderCode.ORDER_ITEM_MALL_TYPE_DS.getResponseCode()),
                        SqlUtils.equalColumnByInput(orderItem.storeKind, OrderCode.ORDER_ITEM_STORE_KIND_DLV.getResponseCode()),
                        SqlUtils.sqlFunction(MysqlFunction.LIKE, ObjectUtils.toArray(shippingItem.shipMethod, "%DLV"))
                    )
                );
        }

        log.debug("selectOrderDetailShippingInfo :: \n[{}]", SqlUtils.orQuery(query.toString()));
        return SqlUtils.orQuery(query.toString());
    }

    public static String selectMultiOrderDetailShippingInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {

        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        MultiSafetyIssueEntity multiSafetyIssue = EntityFactory.createEntityIntoValue(MultiSafetyIssueEntity.class, "safety");
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimMultiItemEntity claimMultiItem = EntityFactory.createEntityIntoValue(ClaimMultiItemEntity.class, Boolean.TRUE);
        MultiBundleEntity multiBundle = EntityFactory.createEntityIntoValue(MultiBundleEntity.class, Boolean.TRUE);
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        MultiShippingAddrEntity multiShippingAddr = EntityFactory.createEntityIntoValue(MultiShippingAddrEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                orderItem.storeType,
                shippingItem.bundleNo,
                orderItem.orderItemNo,
                shippingItem.shipNo,
                multiBundle.multiBundleNo,
                multiShippingAddr.receiverNm,
                multiShippingAddr.shipMobileNo,
                orderItem.partnerId,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(multiBundle.storeId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "partner_nm"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(multiSafetyIssue.issuePhoneNo, "'-'"), "issue_phone_no"),
                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'('", multiShippingAddr.zipcode, "')'", "' '", multiShippingAddr.roadBaseAddr, "' '", multiShippingAddr.roadDetailAddr), "shipping_addr"),
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, multiBundle.shipMethod, ObjectUtils.toArray(multiBundle.shipMethod), "ship_method_nm"),
                multiOrderItem.dlvCd,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, multiOrderItem.dlvCd, ObjectUtils.toArray(multiOrderItem.dlvCd)), "'-'"), "dlv_nm"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(multiOrderItem.invoiceNo, "'-'"), "invoice_no"),
                shippingItem.shipMethod,
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, multiOrderItem.shipStatus, ObjectUtils.toArray(multiOrderItem.shipStatus), "ship_status_nm"),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.orgShipDt, CustomConstants.YYYYMMDD), "org_ship_dt"),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.delayShipDt, CustomConstants.YYYYMMDD), "delay_ship_dt"),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(multiOrderItem.reserveShipDt, CustomConstants.YYYYMMDD), "ship_dt"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, shippingItem.scheduleShipDt),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.scheduleShipDt, CustomConstants.YYYYMMDD)),
                        "'-'"
                    ),
                    "schedule_ship_dt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(multiOrderItem.confirmDt, CustomConstants.YYYYMMDD), "confirm_dt"),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(multiOrderItem.shippingDt, CustomConstants.YYYYMMDD), "shipping_dt"),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(multiOrderItem.completeDt, CustomConstants.YYYYMMDD), "complete_dt"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.ANGLE_BRACKET_LEFT,
                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(multiOrderItem.completeDt, "30"))),
                            SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_24, "NOW()")
                        ),
                        "'NN'",
                        multiOrderItem.shipStatus
                    ),
                    multiOrderItem.shipStatus
                ),
                SqlUtils.subQuery(
                    SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, SqlUtils.sqlFunction(MysqlFunction.MIN, claimBundle.claimStatus)),
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(claimBundle.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                        SqlUtils.equalColumn(claimBundle.bundleNo, shippingItem.bundleNo),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'"))
                    ),
                    "claim_status"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        orderItem.itemQty,
                        SqlUtils.sqlFunction(
                            MysqlFunction.IFNULL_ZERO,
                            SqlUtils.subTableQuery(
                                new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.SUM, claimMultiItem.claimItemQty))
                                    .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                    .INNER_JOIN(SqlUtils.createJoinCondition(claimMultiItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                                    .WHERE(
                                        SqlUtils.equalColumn(claimBundle.bundleNo, shippingItem.bundleNo),
                                        SqlUtils.equalColumn(claimMultiItem.multiBundleNo, multiOrderItem.multiBundleNo),
                                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                        SqlUtils.sqlFunction(MysqlFunction.IN, claimBundle.claimStatus, ObjectUtils.toArray("'C3'"))
                                    )
                            )
                        )
                    ),
                    orderItem.itemQty
                ),
                orderItem.storeKind,
                multiBundle.shipAreaType,
                shippingItem.userNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.orderItemNo, orderItem.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(multiBundle, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(multiOrderItem, ObjectUtils.toArray(multiBundle.multiBundleNo, orderItem.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(multiShippingAddr, ObjectUtils.toArray(multiOrderItem.multiBundleNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(multiSafetyIssue, "safety"),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(multiShippingAddr.multiShipAddrNo), multiSafetyIssue)
                    )
                )
            )
            .WHERE(SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo))
            .ORDER_BY(SqlUtils.orderByesc(multiBundle.multiBundleNo, orderItem.orderItemNo));
        log.debug("selectMultiOrderDetailShippingInfo :: \n[{}]", SqlUtils.orQuery(query.toString()));
        return query.toString();
    }

    public static String selectOrderDetailClaimInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimBundle.claimType,
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, claimBundle.claimType, ObjectUtils.toArray(claimBundle.claimType), "claim_type_nm"),
                claimBundle.claimNo,
                claimBundle.claimBundleNo,
                claimBundle.purchaseOrderNo,
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, claimBundle.claimStatus, ObjectUtils.toArray(claimBundle.claimStatus), "claim_status"),
                claimBundle.bundleNo,
                claimItem.itemNo,
                claimItem.itemName,
                claimItem.claimItemQty,
                claimOpt.orderOptNo,
                claimBundle.regDt,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IN, claimBundle.claimStatus, ObjectUtils.toArray("'C3'", "'C4'", "'C9'")),
                        claimBundle.chgDt,
                        "'-'"
                    ),
                    "chg_dt"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimItem),
                    Arrays.asList( SqlUtils.onClause(ObjectUtils.toArray(claimBundle.claimBundleNo, claimBundle.claimNo), claimItem))
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimOpt),
                    Arrays.asList( SqlUtils.onClause(ObjectUtils.toArray(claimItem.claimNo, claimItem.claimItemNo, claimItem.claimBundleNo), claimOpt))
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.purchaseOrderNo, purchaseOrderNo)
            )
            .ORDER_BY(SqlUtils.orderByesc(claimBundle.claimNo))
            ;
        log.debug("selectOrderDetailClaimInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }


    public static String selectOrderGiftItemList(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo){
        PurchaseGiftEntity purchaseGift = EntityFactory.createEntityIntoValue(PurchaseGiftEntity.class, Boolean.TRUE);
        PurchaseGiftItemEntity purchaseGiftItem = EntityFactory.createEntityIntoValue(PurchaseGiftItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);

        SQL notExists = new SQL().SELECT(claimAddition.orderDiscountNo).FROM(EntityFactory.getTableNameWithAlias(claimAddition))
            .WHERE(SqlUtils.equalColumn(claimAddition.purchaseOrderNo, purchaseGift.purchaseOrderNo), SqlUtils.equalColumnByInput(claimAddition.additionType, "G"));

        SQL query = new SQL()
            .SELECT(
                purchaseGift.purchaseOrderNo,
                purchaseGift.storeId,
                orderItem.bundleNo,
                purchaseGift.giftNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseGift.giftMsg, purchaseGift.giftMsg),
                purchaseGift.giftTargetType,
                purchaseGiftItem.itemNo,
                purchaseGift.stdVal,
                orderItem.itemNm1
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseGift, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseGiftItem, ObjectUtils.toArray(purchaseGift.purchaseGiftNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseGift.purchaseOrderNo, purchaseGiftItem.itemNo, purchaseGift.storeId)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo),
                "NOT EXISTS (".concat(notExists.toString()).concat(")")
            )
            .GROUP_BY(purchaseGift.purchaseOrderNo, purchaseGift.storeId, orderItem.bundleNo, purchaseGift.giftNo, purchaseGift.giftTargetType, purchaseGift.stdVal, purchaseGiftItem.itemNo, orderItem.itemNm1)
            ;
        log.debug("selectOrderGiftItemList \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderDetailPaymentInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentInfoEntity paymentInfo = EntityFactory.createEntityIntoValue(PaymentInfoEntity.class, Boolean.TRUE);
        PaymentMethodEntity paymentMethod = EntityFactory.createEntityIntoValue(PaymentMethodEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);

        SQL orderQuery = new SQL()
            .SELECT(
                SqlUtils.aliasToRow("'PAY'", "pay_type"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    paymentInfo.paymentStatus,
                    ObjectUtils.toArray(
                        "'P3'", "'결제'",
                        "'P4'", "'결제실패'",
                        "'P5'", "'결제'"
                    ),
                    "payment_category"
                ),
                SqlUtils.caseWhenElse(
                    paymentInfo.paymentType,
                    ObjectUtils.toArray(
                        "'PG'",
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.OR(
                                    SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, paymentInfo.easyMethodCd),
                                    SqlUtils.sqlFunction(MysqlFunction.IN, paymentInfo.methodCd, ObjectUtils.toArray("'MKTNAVER'", "'MKTELEVEN'"))
                                ),
                                paymentMethod.methodNm,
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CASE,
                                    ObjectUtils.toArray(SqlUtils.equalColumnByInput(paymentInfo.cardTypeCd, "CREDIT"), "'신용카드'", "'체크카드'")
                                )
                            )
                        ),
                        "'MILEAGE'", "'마일리지'",
                        "'MHC'", "'마이홈플러스포인트'",
                        "'DGV'", "'디지털상품권'",
                        "'OCB'", "'OK캐쉬백'",
                        "'FREE'", "'0원결제'"
                    ),
                    paymentInfo.paymentType,
                    paymentInfo.paymentType
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(paymentInfo.parentMethodCd, "CARD"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                paymentMethod.methodNm,
                                "' | '",
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CASE,
                                    ObjectUtils.toArray(
                                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, paymentInfo.cardInstallmentMonth, "2"),
                                        "'일시불'",
                                        SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(paymentInfo.cardInstallmentMonth, "'개월'"))
                                    )
                                ),
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CASE,
                                    ObjectUtils.toArray(
                                        SqlUtils.equalColumnByInput(paymentInfo.cardInterestYn, "N"),
                                        "'(무)'",
                                        "'(유)'"
                                    )
                                )
                            )
                        ),
                        "'-'"
                    ),
                    "payment_detail"
                ),
                SqlUtils.caseWhenElse(
                    paymentInfo.paymentType,
                    ObjectUtils.toArray(
                        "'PG'", paymentInfo.cardNo,
                        "'DGV'", SqlUtils.sqlFunction(MysqlFunction.AES_DECRYPT, ObjectUtils.toArray(paymentInfo.dgvCardNo, "!@homeplus#$"))
                    ),
                    "'-'",
                    paymentInfo.cardNo
                ),
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(paymentInfo.paymentStatus, "P4"), 0, paymentInfo.amt), paymentInfo.amt),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(paymentInfo.pgOid, "'-'"), paymentInfo.pgOid),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(paymentInfo.pgKind, "'-'"), paymentInfo.pgKind),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(paymentInfo.pgMid, "'-'"), paymentInfo.pgMid),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(paymentInfo.pgTid, "'-'"), paymentInfo.pgTid),
                purchaseOrder.reqIp,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(paymentInfo.paymentFshDt, paymentInfo.paymentReqDt), paymentInfo.paymentFshDt),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(paymentInfo.paymentStatus, "P4"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.OR(
                                    SqlUtils.equalColumnByInput(paymentInfo.returnCd, "0000"),
                                    SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.LEFT, ObjectUtils.toArray(paymentInfo.returnMsg, "1")), "\n")
                                ),
                                "'-'",
                                paymentInfo.returnMsg
                            )
                        ),
                        "'-'"
                    ),
                    "explanatory_note"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(paymentInfo, ObjectUtils.toArray(purchaseOrder.paymentNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(paymentMethod, ObjectUtils.toArray(purchaseOrder.siteType, paymentInfo.methodCd, paymentInfo.parentMethodCd)))
            .WHERE(SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo));


        SQL claimQuery = new SQL()
            .SELECT(
                SqlUtils.aliasToRow("'CLAIM'", "pay_type"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    claimPayment.paymentStatus,
                    ObjectUtils.toArray(
                        "'P3'", "'환불'",
                        "'P4'", "'환불실패'"
                    ),
                    "payment_category"
                ),
                SqlUtils.caseWhenElse(
                    claimPayment.paymentType,
                    ObjectUtils.toArray(
                        "'PG'",
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.OR(
                                    SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, claimPayment.easyMethodCd),
                                    SqlUtils.sqlFunction(MysqlFunction.IN, paymentInfo.methodCd, ObjectUtils.toArray("'MKTNAVER'", "'MKTELEVEN'"))
                                ),
                                paymentMethod.methodNm,
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CASE,
                                    ObjectUtils.toArray(SqlUtils.equalColumnByInput(paymentInfo.cardTypeCd, "CREDIT"), "'신용카드'", "'체크카드'")
                                )
                            )
                        ),
                        "'PP'", "'마일리지'",
                        "'MP'", "'마이홈플러스포인트'",
                        "'DG'", "'디지털상품권'",
                        "'OC'", "'OK캐쉬백'",
                        "'FR'", "'0원결제'"
                    ),
                    claimPayment.paymentType,
                    claimPayment.paymentType
                ),
                SqlUtils.aliasToRow("'-'", "payment_detail"),
                SqlUtils.aliasToRow("'-'", paymentInfo.cardNo),
                SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(claimPayment.paymentAmt, claimPayment.substitutionAmt), claimPayment.paymentAmt),
                SqlUtils.aliasToRow("'-'", paymentInfo.pgOid),
                SqlUtils.aliasToRow("'-'", paymentInfo.pgKind),
                SqlUtils.aliasToRow("'-'", paymentInfo.pgMid),
                SqlUtils.aliasToRow("'-'", paymentInfo.pgTid),
                SqlUtils.aliasToRow("'-'", purchaseOrder.reqIp),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(claimPayment.payCompleteDt, claimPayment.chgDt), paymentInfo.paymentFshDt),
                SqlUtils.aliasToRow("'-'", "explanatory_note")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimPayment))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimPayment.purchaseOrderNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(paymentInfo, ObjectUtils.toArray(claimPayment.paymentNo, claimPayment.methodCd)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(paymentMethod, ObjectUtils.toArray(purchaseOrder.siteType, paymentInfo.methodCd, paymentInfo.parentMethodCd)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.IN, claimPayment.paymentStatus, ObjectUtils.toArray("'P3'", "'P4'"))
            );

        StringBuilder query = new StringBuilder();
        query.append(orderQuery.toString());
        query.append("\nUNION ALL\n");
        query.append(claimQuery.toString());

        log.debug("selectOrderDetailPaymentInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectShippingDelayInfo(@Param("shipNo") long shipNo) {
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                shippingItem.delayShipRegDt,
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, shippingItem.delayShipCd, ObjectUtils.toArray(shippingItem.delayShipCd), shippingItem.delayShipCd),
                shippingItem.delayShipDt,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(shippingItem.delayShipMsg, "'-'"), shippingItem.delayShipMsg)
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingItem))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.shipNo, shipNo),
                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, shippingItem.delayShipDt)
            );
        log.debug("selectShippingDelayInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderPartnerInfo(@Param("partnerId") String partnerId, @Param("bundleNo") long bundleNo) {
        ItmPartnerEntity itmPartner = EntityFactory.createEntityIntoValue(ItmPartnerEntity.class, Boolean.TRUE);
        ItmPartnerSellerEntity itmPartnerSeller = EntityFactory.createEntityIntoValue(ItmPartnerSellerEntity.class, Boolean.TRUE);
        ItmPartnerSellerShipEntity itmPartnerSellerShip = EntityFactory.createEntityIntoValue(ItmPartnerSellerShipEntity.class, Boolean.TRUE);
        ItmPartnerManagerEntity itmPartnerManager = EntityFactory.createEntityIntoValue(ItmPartnerManagerEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                itmPartner.partnerNm,
                itmPartner.partnerId,
                SqlUtils.aliasToRow(itmPartnerManager.mngNm, "mng_nm"),
//                SqlUtils.aliasToRow(SqlUtils.sqlFunction(MysqlFunction.AES_DECRYPT, ObjectUtils.toArray(itmPartnerManager.mngPhone, "!@homeplus#$")), "mng_phone"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, SqlUtils.sqlFunction(MysqlFunction.IFNULL_BLANK, itmPartnerManager.mngPhone), "''"),
                        itmPartnerManager.mngPhone,
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(itmPartnerManager.mngMobile, "'-'"))
                    ),
                    itmPartnerManager.mngPhone
                ),
                itmPartnerSeller.phone1,
                SqlUtils.sqlFunction(
                    MysqlFunction.CONCAT,
                    ObjectUtils.toArray(
                        "'('", itmPartnerSellerShip.releaseZipcode, "')'",
                        CustomConstants.SQL_BLANK,
                        "ipss.release_addr_1"
                    ),
                    "release_addr"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CONCAT,
                    ObjectUtils.toArray(
                        "'('", itmPartnerSellerShip.returnZipcode, "')'",
                        CustomConstants.SQL_BLANK,
                        "ipss.return_addr_1"
                    ),
                    "return_addr"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias("dms", itmPartner))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(EntityFactory.getTableNameWithAlias("dms", itmPartnerSeller), ObjectUtils.toList(SqlUtils.onClause(ObjectUtils.toArray(itmPartner.partnerId), itmPartnerSeller))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(EntityFactory.getTableNameWithAlias("dms", itmPartnerSellerShip), ObjectUtils.toList(SqlUtils.onClause(ObjectUtils.toArray(itmPartnerSeller.partnerId), itmPartnerSellerShip))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(itmPartnerSellerShip.shipPolicyNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(bundle.bundleNo, bundleNo))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(EntityFactory.getTableNameWithAlias("dms", itmPartnerManager), ObjectUtils.toList(SqlUtils.onClause(ObjectUtils.toArray(itmPartner.partnerId), itmPartnerManager)), ObjectUtils.toArray(SqlUtils.equalColumnByInput(itmPartnerManager.mngCd, "CS"))))
            .WHERE(
                SqlUtils.equalColumnByInput(itmPartner.partnerId, partnerId)
            )
            .LIMIT(1)
            .OFFSET(0);
        log.debug("selectOrderPartnerInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderPromoPopInfo(@Param("purchaseOrderNo") long purchaseOrderNo){
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderPromoEntity orderPromo = EntityFactory.createEntityIntoValue(OrderPromoEntity.class, Boolean.TRUE);
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.originStoreId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "store_nm"
                ),
                orderItem.itemNo,
                orderItem.itemNm1,
                SqlUtils.caseWhenElse(
                    orderPromo.promoKind,
                    ObjectUtils.toArray(
                        "'BASIC'", "'행사상품'",
                        "'PICK'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.MINUS, orderPromo.mixMatchStdVal, "1"), " '+' ", "'1'")),
                        "'TOGETHER'", "'함께할인'",
                        "'INTERVAL'", "'함께할인'"
                    ),
                    SqlUtils.sqlFunction(
                        MysqlFunction.CASE,
                        ObjectUtils.toArray(
                            SqlUtils.AND(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, orderItem.promoNo),
                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.simpOrgItemPrice, "0")
                            ),
                            "'행사상품'",
                            "''"
                        )
                    ),
                    "promo_nm1"
                ),
                orderItem.promoNo,
                SqlUtils.aliasToRow(orderPaymentDivide.promoDiscountAmt, "discount_amt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(orderItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    orderPromo,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, orderItem.promoNo, orderItem.promoDetailNo),
                    ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderPromo.promoType, "'SIMP'"))
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    orderPaymentDivide,
                    ObjectUtils.toArray(orderPromo.purchaseOrderNo, orderPromo.bundleNo, orderItem.orderItemNo),
                    ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderPaymentDivide.promoDiscountAmt, "0"))
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(orderItem.purchaseOrderNo, purchaseOrderNo)
            )
            .ORDER_BY(SqlUtils.orderByesc(orderItem.orderItemNo));
        log.debug("selectOrderPromoPopInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectCouponDiscountPopInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        OrderDiscountEntity orderDiscount = EntityFactory.createEntityIntoValue(OrderDiscountEntity.class, Boolean.TRUE);
        OrderDiscountCartEntity orderDiscountCart = EntityFactory.createEntityIntoValue(OrderDiscountCartEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderDiscount.storeId, "S"), "store_nm"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderDiscount.discountKind, "card_discount"),
                        orderDiscount.discountNo,
                        orderDiscount.couponNo
                    ),
                    orderDiscount.couponNo
                ),
                orderDiscount.couponNm,
                orderDiscount.discountKind,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    orderDiscount.discountKind,
                    ObjectUtils.toArray(
                        "'item_discount'",  "'상품할인'",
                        "'item_coupon'", "'상품쿠폰'",
                        "'card_discount'", "'카드상품할인'",
                        "'ship_coupon'", "'배송비쿠폰'",
                        "'cart_coupon'", "'장바구니쿠폰'",
                        "'add_td_coupon'", "'중복쿠폰'",
                        "'add_ds_coupon'", "'중복쿠폰'"
                    ),
                    "discount_kind_nm"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    orderDiscount.discountKind,
                    ObjectUtils.toArray(
                        "'item_discount'",
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE_WHEN,
                            orderDiscount.discountType,
                            ObjectUtils.toArray(
                                "'1'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'(할인방법 할인율) 개당 '", orderDiscount.discountRate, "'% 할인(최대 '", orderDiscount.discountMax, "'원)'")),
                                "'2'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'(최대할인금액) 개당 '", orderDiscount.discountAmt, "'원 할인'"))
                            )
                        ),
                        "'item_coupon'",
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE_WHEN,
                            orderDiscount.discountType,
                            ObjectUtils.toArray(
                                "'1'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'(할인방법 할인율) 개당 '", orderDiscount.discountRate, "'% 할인(최대 '", orderDiscount.discountMax, "'원)'")),
                                "'2'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'(최대할인금액) 개당 '", orderDiscount.discountAmt, "'원 할인'"))
                            )
                        ),
                        "'card_discount'",
                        SqlUtils.caseWhenElse(
                            orderDiscount.discountType,
                            ObjectUtils.toArray(
                                "'2'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'(할인방법 할인율) 개당 '", orderDiscount.discountRate, "'% 할인(최대 '", orderDiscount.discountMax, "'원)'"))
                            ),
                            SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'(최대할인금액) 개당 '", orderDiscountCart.discountAmt, "'원 할인'"))
                        ),
                        "'ship_coupon'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'(최소구매금액) '", orderDiscount.purchaseMin, "'원 이상 구매시 | 배송비 '", orderDiscount.discountRate, "'% 할인'")),
                        "'add_td_coupon'",
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                "'(최소구매금액) '", orderDiscount.purchaseMin, "'원 이상 구매시 | '",
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CASE_WHEN,
                                    orderDiscount.discountType,
                                    ObjectUtils.toArray(
                                        "'1'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'(할인방법 할인율) '", orderDiscount.discountRate, "'% 할인(최대 '", orderDiscount.discountMax, "'원)'")),
                                        "'2'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'(할인방법 할인액) '", orderDiscount.discountAmt, "'원 할인'"))
                                    )
                                )
                            )
                        ),
                        "'add_ds_coupon'",
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                "'(최소구매금액) '", orderDiscount.purchaseMin, "'원 이상 구매시 | '",
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CASE_WHEN,
                                    orderDiscount.discountType,
                                    ObjectUtils.toArray(
                                        "'1'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'(할인방법 할인율) '", orderDiscount.discountRate, "'% 할인(최대 '", orderDiscount.discountMax, "'원)'")),
                                        "'2'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'(할인방법 할인액) '", orderDiscountCart.discountAmt, "'원 할인'"))
                                    )
                                )
                            )
                        ),
                        "'cart_coupon'",
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                "'(최소구매금액) '", orderDiscount.purchaseMin, "'원 이상 구매시 | '",
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CASE_WHEN,
                                    orderDiscount.discountType,
                                    ObjectUtils.toArray(
                                        "'1'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'(할인방법 할인율) '", orderDiscount.discountRate, "'% 할인(최대 '", orderDiscount.discountMax, "'원)'")),
                                        "'2'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'(할인방법 할인액) '", orderDiscount.discountAmt, "'원 할인'"))
                                    )
                                )
                            )
                        )
                    ),
                    "coupon_info"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    orderDiscount.discountKind,
                    ObjectUtils.toArray(
                        "'item_discount'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'상품번호'", SqlUtils.appendSingleQuote(SqlUtils.wrapWithSpace(CustomConstants.COLON)), orderDiscount.itemNo)),
                        "'item_coupon'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'상품번호'", SqlUtils.appendSingleQuote(SqlUtils.wrapWithSpace(CustomConstants.COLON)), orderDiscount.itemNo)),
                        "'card_discount'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'상품번호'", SqlUtils.appendSingleQuote(SqlUtils.wrapWithSpace(CustomConstants.COLON)), orderDiscountCart.itemNo)),
                        "'ship_coupon'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'배송번호'", SqlUtils.appendSingleQuote(SqlUtils.wrapWithSpace(CustomConstants.COLON)), orderDiscount.bundleNo)),
                        "'cart_coupon'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'주문번호'", SqlUtils.appendSingleQuote(SqlUtils.wrapWithSpace(CustomConstants.COLON)), orderDiscount.purchaseOrderNo)),
                        "'add_td_coupon'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'주문번호'", SqlUtils.appendSingleQuote(SqlUtils.wrapWithSpace(CustomConstants.COLON)), orderDiscount.purchaseOrderNo)),
                        "'add_ds_coupon'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'주문번호'", SqlUtils.appendSingleQuote(SqlUtils.wrapWithSpace(CustomConstants.COLON)), orderDiscount.purchaseOrderNo))
                    ),
                    "discount_apply_target"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IN, orderDiscount.discountKind, ObjectUtils.toArray("'card_discount'", "'cart_coupon'", "'add_td_coupon'", "'add_ds_coupon'")),
                        orderDiscountCart.discountAmt,
                        orderDiscount.discountAmt
                    ),
                    orderDiscount.discountAmt
                ),
                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(orderDiscount.couponValidStartDt, SqlUtils.appendSingleQuote(SqlUtils.wrapWithSpace(CustomConstants.TILDE)), orderDiscount.couponValidEndDt), "discount_coupon_period")
            )
            .FROM(EntityFactory.getTableNameWithAlias(orderDiscount))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    orderDiscountCart,
                    ObjectUtils.toArray(orderDiscount.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(orderDiscount.orderDiscountNo,orderDiscountCart.orderDiscountNo)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(orderDiscount.purchaseOrderNo, purchaseOrderNo)
            );

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(orderDiscount.couponNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "store_nm", "store_nm"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(orderDiscount.couponNm), orderDiscount.couponNm),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(orderDiscount.discountKind), orderDiscount.discountKind),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "discount_kind_nm", "discount_kind_nm"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "coupon_info", "coupon_info"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "discount_apply_target", "discount_apply_target"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.nonAlias(orderDiscount.discountAmt), orderDiscount.discountAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "discount_coupon_period", "discount_coupon_period")
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "Z"))
            .GROUP_BY(SqlUtils.nonAlias(orderDiscount.couponNo));
        log.debug("selectCouponDiscountPopInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderGiftPopInfo(@Param("purchaseOrderNo") long purchaseOrderNo){
        PurchaseGiftEntity purchaseGift = EntityFactory.createEntityIntoValue(PurchaseGiftEntity.class, Boolean.TRUE);
        PurchaseGiftItemEntity purchaseGiftItem = EntityFactory.createEntityIntoValue(PurchaseGiftItemEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.originStoreId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "store_nm"
                ),
                purchaseGiftItem.itemNo,
                orderItem.itemNm1,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseGift.promoType, "ADD"),
                        "'덤'",
                        "'사은품'"
                    ),
                    "gift_type"
                ),
                purchaseGift.giftNo,
                purchaseGift.giftMsg
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseGift))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseGiftItem, ObjectUtils.toArray(purchaseGift.purchaseGiftNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseGift.purchaseOrderNo, purchaseGiftItem.itemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseGift.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectOrderGiftPopInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderSubstitutionData(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                orderItem.purchaseOrderNo,
                orderItem.itemNo,
                orderItem.itemNm1,
                orderItem.itemQty,
                orderItem.orderPrice,
                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_24, purchaseOrder.orderDt, purchaseOrder.orderDt)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_SUB'", "'ORD_SUB_MARKET'"))
            );
        log.debug("selectOrderSubstitutionData Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderSavePopInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseAccumulateEntity purchaseAccumulate = EntityFactory.createEntityIntoValue(PurchaseAccumulateEntity.class, Boolean.TRUE);
        ShippingMileagePaybackEntity shippingMileagePayback = EntityFactory.createEntityIntoValue(ShippingMileagePaybackEntity.class, Boolean.TRUE);
        ClaimAccumulateEntity claimAccumulate = EntityFactory.createEntityIntoValue(ClaimAccumulateEntity.class, Boolean.TRUE);
        SQL orderQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    purchaseAccumulate.pointKind,
                    ObjectUtils.toArray(
                        "'MHC'", "'마이홈플러스포인트'",
                        "'MILEAGE'", "'마일리지'"
                    ),
                    purchaseAccumulate.pointKind
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseAccumulate.pointKind, "MILEAGE"),
                        "'주문적립'",
                        "'-'"
                    ),
                    "mileage_kind"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseAccumulate.pointKind, "MHC"),
                        purchaseAccumulate.approvalNo,
                        "'-'"
                    ),
                    purchaseAccumulate.approvalNo
                ),
                SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(purchaseAccumulate.mhcBaseAccAmt, purchaseAccumulate.mhcAddAccAmt, purchaseAccumulate.mileageAccAmt), purchaseAccumulate.resultPoint),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseAccumulate.pointKind, "MILEAGE"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, purchaseAccumulate.resultCd),
                                "'적립완료'",
                                "'적립대기'"
                            )
                        ),
                        "'-'"
                    ),
                    "point_status"
                ),
                purchaseAccumulate.regDt
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseAccumulate))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseAccumulate.purchaseOrderNo, purchaseOrderNo)
            );

        SQL claimQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    claimAccumulate.pointKind,
                    ObjectUtils.toArray(
                        "'MHC'", "'마이홈플러스포인트'",
                        "'MILEAGE'", "'마일리지'"
                    ),
                    claimAccumulate.pointKind
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimAccumulate.pointKind, "MILEAGE"),
                        "'주문적립회수'",
                        "'-'"
                    ),
                    "mileage_kind"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimAccumulate.pointKind, "MHC"),
                        claimAccumulate.approvalNo,
                        "'-'"
                    ),
                    claimAccumulate.approvalNo
                ),
                SqlUtils.customOperator(CustomConstants.MULTIPLY, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, claimAccumulate.resultPoint), "-1"), claimAccumulate.resultPoint),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimAccumulate.pointKind, "MILEAGE"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, claimAccumulate.resultCd),
                                "'회수완료'",
                                "'회수대기'"
                            )
                        ),
                        "'-'"
                    ),
                    "point_status"
                ),
                claimAccumulate.regDt
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimAccumulate))
            .WHERE(
                SqlUtils.equalColumnByInput(claimAccumulate.purchaseOrderNo, purchaseOrderNo)
            );

        SQL paybackPaymentQuery = new SQL()
            .SELECT(
                SqlUtils.aliasToRow("'마일리지'", "point_kind"),
                SqlUtils.aliasToRow("'합배송 Payback'","mileage_kind"),
                SqlUtils.aliasToRow("'-'", claimAccumulate.approvalNo),
                SqlUtils.aliasToRow(shippingMileagePayback.shipPricePayback, "result_point"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(shippingMileagePayback.resultYn, "Y"),
                        "'적립대기'",
                        "'적립완료'"
                    ),
                    "point_status"
                ),
                shippingMileagePayback.regDt
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingMileagePayback))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingMileagePayback.orgPurchaseOrderNo, purchaseOrderNo),
                SqlUtils.OR(
                    Boolean.TRUE,
                    SqlUtils.equalColumnByInput(shippingMileagePayback.cancelYn, "N"),
                    SqlUtils.AND(
                        SqlUtils.equalColumnByInput(shippingMileagePayback.resultYn, "Y"),
                        SqlUtils.equalColumnByInput(shippingMileagePayback.cancelYn, "Y")
                    )
                )
            );

        SQL paybackClaimQuery = new SQL()
            .SELECT(
                SqlUtils.aliasToRow("'마일리지'", "point_kind"),
                SqlUtils.aliasToRow("'합배송 Payback 취소회수'","mileage_kind"),
                SqlUtils.aliasToRow("'-'", claimAccumulate.approvalNo),
                SqlUtils.aliasToRow(shippingMileagePayback.shipPricePayback, "result_point"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(shippingMileagePayback.cancelYn, "Y"),
                        "'회수대기'",
                        "'회수완료'"
                    ),
                    "point_status"
                ),
                shippingMileagePayback.regDt
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingMileagePayback))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingMileagePayback.orgPurchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(shippingMileagePayback.cancelYn, "Y"),
                SqlUtils.equalColumnByInput(shippingMileagePayback.resultYn, "Y")
            );

        SQL query = new SQL()
            .SELECT("*").FROM("(".concat(orderQuery.toString()).concat("\nUNION ALL\n").concat(claimQuery.toString()).concat("\nUNION ALL\n").concat(paybackPaymentQuery.toString()).concat("\nUNION ALL\n").concat(paybackClaimQuery.toString()).concat(") ZZ"))
            .ORDER_BY("reg_dt");

        log.debug("selectOrderSavePopInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectCmbnClaimInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_Y_N,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(
                            SqlUtils.customOperator(
                                CustomConstants.MINUS,
                                orderItem.itemQty,
                                SqlUtils.sqlFunction(
                                    MysqlFunction.IFNULL_ZERO,
                                    SqlUtils.subTableQuery(
                                        new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.SUM, claimItem.claimItemQty))
                                            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                            .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                                            .WHERE(
                                                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")),
                                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                                SqlUtils.equalColumn(claimBundle.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                                                SqlUtils.equalColumn(claimItem.orderItemNo, orderItem.orderItemNo)
                                            )
                                    )
                                )
                            ),
                            "0"
                        )
                    ),
                    "claimCmbnYn"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE")
            );
        SQL query = new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.MIN, "claimCmbnYn")).FROM(SqlUtils.subTableQuery(innerQuery, "A"));
        log.debug("selectCmbnClaimInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
     * TCK용 주문관리 조회 Query.
     *
     * @return query
     */
    public static String selectOrderSearchInfoTck(OrderSearchTckSetDto orderSearchSetDto) throws Exception {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        PaymentInfoEntity paymentInfo = EntityFactory.createEntityIntoValue(PaymentInfoEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        OrderDiscountEntity orderDiscount = EntityFactory.createEntityIntoValue(OrderDiscountEntity.class, Boolean.TRUE);
        OrderDiscountCartEntity orderDiscountCart = EntityFactory.createEntityIntoValue(OrderDiscountCartEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class,  Boolean.TRUE);
        PurchaseStoreInfoEntity purchaseStoreInfo = EntityFactory.createEntityIntoValue(PurchaseStoreInfoEntity.class, Boolean.TRUE);
        StoreSlotMngEntity storeSlotMng = EntityFactory.createEntityIntoValue(StoreSlotMngEntity.class, Boolean.TRUE);
        ItmPartnerStoreEntity itmPartnerStoreEntity = EntityFactory.createEntityIntoValue(ItmPartnerStoreEntity.class, Boolean.TRUE);

        // 조회기간 타입에 따른 조회기준설정.
        String schPeriodType = orderSearchSetDto.getSchPeriodType().equals("01") ? purchaseOrder.orderDt : payment.paymentFshDt;
        // 상세검색
        String detailSearchColumn;
        switch (orderSearchSetDto.getSchDetailType()){
            case "PURCHASE":
            case "SUB_ORDER" :
                detailSearchColumn = purchaseOrder.purchaseOrderNo;
                break;
            case "ORDER":
                detailSearchColumn = orderItem.orderItemNo;
                break;
            case "SHIP":
                detailSearchColumn = shippingItem.bundleNo;
                break;
            case "ITEM":
                detailSearchColumn = orderItem.itemNo;
                break;
            case "ITEM_NM":
                detailSearchColumn = orderItem.itemNm1;
                break;
            case "BUYER_TEL_NO" :
                detailSearchColumn = purchaseOrder.buyerMobileNo;
                break;
            case "RECEIVER_TEL_NO" :
                detailSearchColumn = shippingAddr.shipMobileNo;
                break;
            case "PG_OID" :
                detailSearchColumn = paymentInfo.pgOid;
                break;
            case "OMNI_PURCHASE" :
                detailSearchColumn = purchaseStoreInfo.purchaseStoreInfoNo;
                break;
            case "MARKETORDERNO" :
                detailSearchColumn = purchaseOrder.marketOrderNo;
                break;
            default:
                detailSearchColumn = null;
        }

        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COMMON_TABLE, payment.paymentStatus, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(payment.paymentStatus, "P5"), "'P3'", payment.paymentStatus))), payment.paymentStatus),
                purchaseOrder.orderDt,
                payment.paymentFshDt,
                purchaseOrder.buyerEmail,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE"),
                        purchaseOrder.orgPurchaseOrderNo,
                        purchaseOrder.purchaseOrderNo
                    ),
                    purchaseOrder.purchaseOrderNo
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE"),
                        purchaseOrder.purchaseOrderNo,
                        "''"
                    ),
                    purchaseOrder.orgPurchaseOrderNo
                ),
                orderItem.storeType,
                bundle.bundleNo,
                orderItem.orderItemNo,
                orderItem.itemNo,
                orderItem.itemNm1,
                orderItem.orderPrice,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(bundle.prepaymentYn, "Y"),
                        SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(bundle.shipPrice, bundle.shipDiscountPrice)),
//                        "'착불'"
                        "'0'"
                    ),
                    bundle.shipPrice
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(bundle.prepaymentYn, "Y"),
                        bundle.islandShipPrice,
//                        "'착불'"
                        "'0'"
                    ),
                    bundle.islandShipPrice
                ),
//                SqlUtils.aliasToRow(purchaseOrder.totShipPrice, bundle.shipPrice),
//                SqlUtils.aliasToRow(purchaseOrder.totIslandShipPrice, bundle.islandShipPrice),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(SqlUtils.customOperator(CustomConstants.MINUS, SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMst.totAddShipPrice), SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimMst.totAddIslandShipPrice)))
                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                        .INNER_JOIN(
                            SqlUtils.createJoinCondition(
                                claimMst,
                                ObjectUtils.toArray(claimBundle.claimNo),
                                ObjectUtils.toArray(
                                    SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                    SqlUtils.sqlFunction(MysqlFunction.NOT_IN,claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'"))
                                )
                            )
                        )
                        .WHERE(
                            SqlUtils.equalColumn(claimBundle.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                            SqlUtils.equalColumn(claimBundle.bundleNo, bundle.bundleNo)
                        ),
                    "claim_price"
                ),
//                SqlUtils.aliasToRow("(SELECT IFNULL(SUM(complete_amt), 0) - IFNULL(SUM(complete_amt), 0) FROM claim_mst cm INNER JOIN claim_bundle cb ON cm.claim_no = cb.claim_no WHERE cm.purchase_order_no = po.purchase_order_no)", "claim_price"),
                SqlUtils.customOperator(
                    CustomConstants.PLUS, ObjectUtils.toArray(purchaseOrder.totItemDiscountAmt, purchaseOrder.totShipDiscountAmt, purchaseOrder.totCartDiscountAmt, purchaseOrder.totEmpDiscountAmt), "discount_amt"
                ),
                payment.totAmt,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.storeId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "partnerNm"

                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(purchaseOrder.marketType, "'-'"), purchaseOrder.marketType),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(purchaseOrder.marketOrderNo, "'-'"), purchaseOrder.marketOrderNo),
                purchaseOrder.buyerNm,
                shippingAddr.receiverNm,
                SqlUtils.sqlFunction(MysqlFunction.FN_PAYMENT_ROWNUM, ObjectUtils.toArray(payment.paymentNo, "'kind'"), paymentInfo.pgKind),
                SqlUtils.sqlFunction(MysqlFunction.FN_PAYMENT_ROWNUM, ObjectUtils.toArray(payment.paymentNo, "'method'"), paymentInfo.parentMethodCd),
                purchaseOrder.buyerMobileNo,
                SqlUtils.aliasToRow(shippingAddr.shipMobileNo, "receiver_mobile_no"),
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(purchaseOrder.substitutionOrderYn, "Y"), "'Y'", "'-'"), purchaseOrder.substitutionOrderYn),
                itmPartnerStoreEntity.storeNm,
                bundle.driverNm,
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, purchaseStoreInfo.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")), storeSlotMng.shipDt, SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(shippingItem.reserveShipDt, CustomConstants.YYYYMMDD))), "reserve_ship_dt"),
                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.SUBSTR, ObjectUtils.toArray(storeSlotMng.slotShipStartTime,1,2)),"':'",SqlUtils.sqlFunction(MysqlFunction.SUBSTR, ObjectUtils.toArray(storeSlotMng.slotShipStartTime,3,2)))),"'~'",SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.SUBSTR, ObjectUtils.toArray(storeSlotMng.slotShipEndTime,1,2)),"':'",SqlUtils.sqlFunction(MysqlFunction.SUBSTR, ObjectUtils.toArray(storeSlotMng.slotShipEndTime,3,2))))), "reserve_ship_time"),
                itmPartnerStoreEntity.storeId,
                bundle.sordNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(EntityFactory.getTableNameWithAlias(orderItem),Arrays.asList(
                    // 동일컬럼으로 맵핑하는 쿼리 생성.
                    // onClause(new Object[], Object)
                    // onClause(컬럼정보, 맵핑되는 컬럼의 instance)
                    SqlUtils.onClause(ObjectUtils.toArray( purchaseOrder.purchaseOrderNo ), orderItem))
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(payment),
                    Arrays.asList( SqlUtils.onClause(ObjectUtils.toArray( purchaseOrder.paymentNo ), payment ))
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingItem),
                    ObjectUtils.toList( SqlUtils.onClause(ObjectUtils.toArray( purchaseOrder.purchaseOrderNo, orderItem.orderItemNo, orderItem.itemNo ), shippingItem ))
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingAddr),
                    ObjectUtils.toList( SqlUtils.onClause(ObjectUtils.toArray( shippingItem.shipAddrNo ), shippingAddr ))
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(bundle),
                    ObjectUtils.toList( SqlUtils.onClause(ObjectUtils.toArray( shippingItem.bundleNo ), bundle ))
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderDiscount),
                    ObjectUtils.toList(SqlUtils.onClause(ObjectUtils.toArray( orderItem.orderItemNo, orderItem.itemNo ), orderDiscount))
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderDiscountCart),
                    ObjectUtils.toList(SqlUtils.onClause(ObjectUtils.toArray( orderDiscount.orderDiscountNo ), orderDiscountCart))
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms",itmPartnerStoreEntity),
                    ObjectUtils.toList(SqlUtils.equalColumn(orderItem.originStoreId, itmPartnerStoreEntity.storeId))
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseStoreInfo),
                    ObjectUtils.toList(SqlUtils.onClause(ObjectUtils.toArray( bundle.purchaseStoreInfoNo ), purchaseStoreInfo)),
                    ObjectUtils.toArray( SqlUtils.sqlFunction(MysqlFunction.IN, purchaseStoreInfo.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")) )
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(storeSlotMng),
                    ObjectUtils.toList(SqlUtils.onClause(ObjectUtils.toArray( purchaseStoreInfo.slotId, purchaseStoreInfo.shipDt, purchaseStoreInfo.storeId, purchaseStoreInfo.shiftId ), storeSlotMng)),
                    ObjectUtils.toArray( SqlUtils.equalColumnByInput("ssm.use_yn", "Y") )
                )
            );

        //주문/상품/배송
        if(!"PURCHASE,ORDER,SHIP,PG_OID,SUB_ORDER,MARKETORDERNO".contains(orderSearchSetDto.getSchDetailType()) || EscrowString.isEmpty(orderSearchSetDto.getSchDetailContents())){
            query.WHERE(
                SqlUtils.betweenMinute(schPeriodType, orderSearchSetDto.getSchStartDt(), orderSearchSetDto.getSchEndDt())
            );
        }

        if (detailSearchColumn != null && EscrowString.isNotEmpty(orderSearchSetDto.getSchDetailContents())) {
            if(orderSearchSetDto.getSchDetailType().equalsIgnoreCase("ITEM_NM")){
                query.WHERE(
                    SqlUtils.middleLike(detailSearchColumn, orderSearchSetDto.getSchDetailContents())
                );
            } else if(orderSearchSetDto.getSchDetailType().equalsIgnoreCase("PURCHASE")){
                query.WHERE(
                    SqlUtils.OR(
                        SqlUtils.equalColumnByInput(detailSearchColumn, orderSearchSetDto.getSchDetailContents()),
                        "(".concat(
                            SqlUtils.AND(
                                SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, orderSearchSetDto.getSchDetailContents()),
                                SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE")
                            )
                        ).concat(")")
                    )
                );
            } else {
                query.WHERE(
                    SqlUtils.equalColumnByInput(detailSearchColumn, orderSearchSetDto.getSchDetailContents())
                );
            }
        }

        List<String> parentMethodCdList = orderSearchSetDto.getParentMethodCd();
        if (parentMethodCdList != null && parentMethodCdList.size() > 0 && !parentMethodCdList.contains("ALL")) {

            if(parentMethodCdList.contains("POINT")){
                parentMethodCdList.remove("POINT");
                parentMethodCdList.add("MILEAGE");
                parentMethodCdList.add("OCB");
                parentMethodCdList.add("MHC");
            }

            if (parentMethodCdList.size() == 1) {
                query.WHERE(
                    SqlUtils.equalColumnByInput(paymentInfo.parentMethodCd, parentMethodCdList.get(0))
                );
            } else {
                query.WHERE(
                    SqlUtils.getConditionBySqlPattern("IN", paymentInfo.parentMethodCd, parentMethodCdList.toArray())
                );
            }
        }

        if (!orderSearchSetDto.getPaymentStatus().contains("ALL")) {
            query.WHERE(
                SqlUtils.getConditionBySqlPattern("IN", payment.paymentStatus, orderSearchSetDto.getPaymentStatus().split(","))
            );
        }

        if (!StringUtils.equalsIgnoreCase(orderSearchSetDto.getAffiliateCd(), "ALL")) {
            query.WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.marketType, orderSearchSetDto.getAffiliateCd().toUpperCase(Locale.KOREA))
            );
        }

        if (!orderSearchSetDto.getSchStoreType().equalsIgnoreCase("ALL")) {
            query.WHERE(
                SqlUtils.equalColumnByInput(orderItem.storeType, orderSearchSetDto.getSchStoreType())
            );
            if(!StringUtils.isEmpty(orderSearchSetDto.getSchStoreId())){
                String searchStoreType = orderSearchSetDto.getSchStoreType();
                if(searchStoreType.equals("DS")){
                    searchStoreType = orderItem.partnerId;
                } else {
                    searchStoreType = orderItem.storeId;
                }
                query.WHERE(
                    SqlUtils.equalColumnByInput(searchStoreType, orderSearchSetDto.getSchStoreId())
                );
            }
        }

        if(EscrowString.isNotEmpty(orderSearchSetDto.getSchUserSearch())){
            String nonUserSearchColumn = purchaseOrder.userNo;
            // 비회원
            if(orderSearchSetDto.getSchNonUserYn().equals("Y")){
                switch (orderSearchSetDto.getSchBuyerInfo()) {
                    case "02" :
                        nonUserSearchColumn = purchaseOrder.buyerMobileNo;
                        break;
                    case "03" :
                        nonUserSearchColumn = purchaseOrder.buyerEmail;
                        break;
                    default :
                        nonUserSearchColumn = purchaseOrder.buyerNm;
                        break;
                }
            }
            query.WHERE(
                SqlUtils.equalColumnByInput(nonUserSearchColumn, orderSearchSetDto.getSchUserSearch())
            );
        }
        List<String> reserveList = new ArrayList<>();
        if(EscrowString.isNotEmpty(orderSearchSetDto.getSchReserveDlvYn())){
            reserveList.add(SqlUtils.appendSingleQuote(orderSearchSetDto.getSchReserveDlvYn()));
        }
        if(EscrowString.isNotEmpty(orderSearchSetDto.getSchReserveDrctYn())){
            reserveList.add(SqlUtils.appendSingleQuote(orderSearchSetDto.getSchReserveDrctYn()));
        }
        if(reserveList.size() > 0){
            query.WHERE(SqlUtils.sqlFunction(MysqlFunction.IN, orderItem.reserveShipMethod, reserveList.toArray()));
        }
        query.WHERE(SqlUtils.equalColumnByInput(purchaseOrder.nomemOrderYn, orderSearchSetDto.getSchNonUserYn()));
        query.ORDER_BY(SqlUtils.orderByesc(purchaseOrder.purchaseOrderNo, bundle.bundleNo, orderItem.itemNo));

        log.debug("selectOrderSearchInfoTck query :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderCashReceiptInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        CashReceiptEntity cashReceipt = EntityFactory.createEntityIntoValue(CashReceiptEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                cashReceipt.paymentNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, cashReceipt.cashReceiptUsage, cashReceipt.cashReceiptUsage),
                SqlUtils.sqlFunction(MysqlFunction.MAX, cashReceipt.cashReceiptType, cashReceipt.cashReceiptType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, cashReceipt.cashReceiptReqNo, cashReceipt.cashReceiptReqNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(cashReceipt, ObjectUtils.toArray(purchaseOrder.paymentNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            )
            .GROUP_BY(cashReceipt.paymentNo)
            ;
        log.debug("selectOrderCashReceiptInfo query :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderHistoryList (@Param("purchaseOrderNo") long purchaseOrderNo){
        ProcessHistoryEntity processHistory = EntityFactory.createEntityIntoValue(ProcessHistoryEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                processHistory.historyReason.concat(" as historyDetailType"),
                "case "
                + "when ph.history_detail_before != '' "
                + "then concat(ph.history_detail_before,' -> ',ph.history_detail_desc) "
                + "else ph.history_detail_desc "
                + "end as historyDetailDesc",
                processHistory.regDt.concat(" as historyDt"),
                "CASE "
                + "WHEN ph.reg_channel IN ('PARTNER') "
                + "    THEN concat(uf_get_store_partner_name(ph.reg_id, 'P'), ' | ',ph.reg_id) "
                + "WHEN ph.reg_channel IN ('MYPAGE','STORE') "
                + "    THEN CONCAT(po.buyer_nm, '(',  CASE WHEN ph.reg_id = 'MYPAGE' THEN po.user_no ELSE ph.reg_id END, ')')"
                + "WHEN ph.reg_channel IN ('STORE', 'SYSTEM', 'BATCH', 'WMS', 'OMNI') "
                + "    THEN ph.reg_id "
                + "ELSE (select concat(ei.name, ' | ', ei.empid) from dms.employee_info ei where ei.empid = ph.reg_id) "
                + "END reg_id",
                processHistory.bundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(processHistory))
            .INNER_JOIN(
                SqlUtils.createJoinCondition( purchaseOrder, ObjectUtils.toArray(processHistory.purchaseOrderNo) )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(processHistory.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectOrderHistoryList Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
     * 주문 타입 확인
     *
     * @param purchaseOrderNo
     * @return
     */
    public static String selectOrderTypeCheck(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.orderType
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectOrderTypeCheck Query :: \n [{}]", query.toString());
        return query.toString();
    }
}
