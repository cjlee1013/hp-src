package kr.co.homeplus.escrowmng.order.model.managementPop;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "판매업체정보 팝업")
public class OrderPartnerInfoDto {
    @ApiModelProperty(value = "판매자", position = 1)
    private String partnerNm;
    @ApiModelProperty(value = "판매자ID", position = 1)
    private String partnerId;
    @ApiModelProperty(value = "담당자", position = 1)
    private String mngNm;
    @ApiModelProperty(value = "담당자 연락처", position = 1)
    private String mngPhone;
    @ApiModelProperty(value = "대표번호", position = 1)
    private String phone1;
    @ApiModelProperty(value = "업체출고지", position = 1)
    private String releaseAddr;
    @ApiModelProperty(value = "업체회수지", position = 1)
    private String returnAddr;
}
