package kr.co.homeplus.escrowmng.message.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.escrowmng.message.enums.ShipStatus;
import lombok.Data;

@ApiModel(description = "수동발송 > 주문검색 팝업의 search result dto")
@Data
public class OrderSendListInfo {

    @ApiModelProperty("주문번호")
    private long purchaseOrderNo;

    @ApiModelProperty("배송번호")
    private String bundleNo;

    @ApiModelProperty("점포 id")
    private String storeId;

    @ApiModelProperty("점포명")
    private String storeNm;

    @ApiModelProperty("고객번호 (비회원은 신규생성)")
    private String userNo;

    @ApiModelProperty("구매자명")
    private String buyerNm;

    @ApiModelProperty("구매자 핸드폰")
    private String buyerMobileNo;

    @ApiModelProperty("수취인명")
    private String receiverNm;

    @ApiModelProperty("수취인 휴대폰")
    private String shipMobileNo;

    @ApiModelProperty("주문일시")
    private String orderDt;

    @ApiModelProperty("배송일시")
    private String shipDt;

    @ApiModelProperty("shift")
    private String shiftId;

    @ApiModelProperty("slot")
    private String slotId;

    @ApiModelProperty("주문상태")
    private ShipStatus shipStatus;
}
