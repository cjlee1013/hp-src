package kr.co.homeplus.escrowmng.claim.sql;

import java.util.Arrays;
import java.util.Locale;
import kr.co.homeplus.escrowmng.claim.model.ClaimDetailInfoSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimInfoListSetDto;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimAdditionEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimBundleEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimItemEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimMstEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimOptEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimPaymentEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimPickShippingEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimExchShippingEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimReqEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.OrderPromoEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ProcessHistoryEntity;
import kr.co.homeplus.escrowmng.enums.MysqlFunction;
import kr.co.homeplus.escrowmng.order.constants.CustomConstants;
import kr.co.homeplus.escrowmng.order.constants.SqlPatternConstants;
import kr.co.homeplus.escrowmng.order.factory.EntityFactory;
import kr.co.homeplus.escrowmng.order.model.entity.ItmPartnerStoreEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderItemEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderPickupInfoEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PaymentEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PaymentMethodEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PurchaseGiftEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PurchaseOrderEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingAddrEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingItemEntity;
import kr.co.homeplus.escrowmng.order.model.entity.StoreShiftMngEntity;
import kr.co.homeplus.escrowmng.order.model.entity.StoreSlotMngEntity;
import kr.co.homeplus.escrowmng.utils.ObjectUtils;
import kr.co.homeplus.escrowmng.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ClaimInfoListSql {

    // 공통코드 조회.
    private static final String escrowMngCode = "(SELECT IFNULL(imc.mc_nm, '''') AS mc_cd FROM dms.itm_mng_code imc where imc.gmc_cd = ''{0}'' AND imc.mc_cd = {1})";
    private static final String escrowDfCode = "(SELECT IFNULL(imc.ref_2, '''') AS ref_2 FROM dms.itm_mng_code imc where imc.gmc_cd = ''{0}'' AND imc.mc_cd = {1})";

    /**
     * Admin 클레임 관리 - 클레임 관리 - 조회
     * 클레임 리스트 조회
     *
     * @param claimInfoListSetDto 주문번호
     * @return String 클레임 리스트 Query
     * @throws Exception 오류처리.
     */
    public static String selectClaimList(ClaimInfoListSetDto claimInfoListSetDto) throws Exception {

        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class, Boolean.TRUE);
        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class, Boolean.TRUE);

        // 조회기간 타입에 따른 조회기준설정.
        String claimDateType;
        switch (claimInfoListSetDto.getClaimDateType()){
            case "REQUEST":
                claimDateType = claimReq.requestDt;
                break;
            case "COMPLETE":
                claimDateType = claimReq.completeDt;
                break;
            case "PENDING":
                claimDateType = claimReq.pendingDt;
                break;
            default:
                claimDateType = null;
        }

        //검색어에 따른 조회기준설정
        String claimSearchType;
        switch (claimInfoListSetDto.getClaimSearchType()){

            case  "CLAIMBUNDLENO" :
                claimSearchType = claimBundle.claimBundleNo;
                break;
            case "MOBILENO" :
                claimSearchType = purchaseOrder.buyerMobileNo;
                break;
            case  "PURCHASEORDERNO" :
                claimSearchType = claimMst.purchaseOrderNo;
                break;
            case  "BUNDLENO" :
                claimSearchType = claimBundle.bundleNo;
                break;
            case "ORDERITEMNO" :
                claimSearchType = claimItem.orderItemNo;
                break;
            case "ITEMNO" :
                claimSearchType = claimItem.itemNo;
                break;
            case "CLAIMNO" :
                claimSearchType = claimMst.claimNo;
                break;
            case "MARKETORDERNO" :
                claimSearchType = purchaseOrder.marketOrderNo;
                break;
            default:
                claimSearchType = "ALL";
        }

        //검색조건이 빈값으로 올경우 검색조건 타입은 'ALL'로 지정
        if(claimInfoListSetDto.getSchClaimSearch().equalsIgnoreCase("")){
            claimSearchType = "ALL";
        }

        SQL queryOfOrder = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    claimBundle.claimType,
                    ObjectUtils.toArray(
                        "'C'", "'취소'",
                        "'R'", "'반품'",
                        "'X'", "'교환'"
                    ),
                    claimBundle.claimType
                ),
                "cs.mc_nm as claim_status",
                claimReq.requestDt,
                "case cb.claim_status when 'C1' then cr.request_dt when 'C2' then cr.approve_dt when 'C3' then cr.complete_dt when 'C4' then cr.withdraw_dt when 'C8' then cr.pending_dt else cr.reject_dt end processDt",
                claimReq.approveDt,
                claimReq.rejectDt,
                claimReq.completeDt,
                claimMst.claimNo,
                claimMst.purchaseOrderNo,
                claimBundle.bundleNo,
                claimItem.orderItemNo,
                claimItem.itemNo,
                claimItem.itemName,
                claimItem.claimItemQty,
                "crt.mc_nm AS claim_reason_type",
                claimReq.claimReasonDetail,
                claimReq.pendingDt,
                "case when cr.pending_reason_type is null then null "
                    + "else (SELECT IFNULL(imc.mc_nm, '') AS mc_cd FROM dms.itm_mng_code imc where imc.gmc_cd = 'claim_reason_type' AND imc.mc_cd = cr.pending_reason_type) "
                    + "end AS pending_reason_type",
                claimReq.pendingReasonDetail,
                "case when cr.reject_reason_type is null then null "
                    + "else (SELECT IFNULL(imc.mc_nm, '') AS mc_cd FROM dms.itm_mng_code imc where imc.gmc_cd = 'claim_reason_type' AND imc.mc_cd = cr.reject_reason_type) "
                    + "end AS reject_reason_type",
                claimReq.rejectReasonDetail,
                claimBundle.partnerId,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimItem.originStoreId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimItem.partnerId, "P"))
                    ),
                    "partner_nm"
                ),
                claimItem.storeType,
                claimItem.originStoreId,
                claimItem.storeId,
                claimItem.fcStoreId,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(purchaseOrder.marketType, "'-'"), purchaseOrder.marketType),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(purchaseOrder.marketOrderNo, "'-'"), purchaseOrder.marketOrderNo),
                purchaseOrder.userNo,
                purchaseOrder.buyerNm,
                purchaseOrder.buyerMobileNo,
                claimItem.claimBundleNo,
                claimBundle.claimType.concat(" as claimTypeCode"),
                "case cr.request_id "
	             + "when 'MYPAGE' then cr.request_id "
	             + "when 'PARTNER' then cr.request_id "
	             + "when 'STORE' then cr.request_id "
	             + "when 'SYSTEM' then cr.request_id "
                 + "when 'OMNI' then cr.request_id "
                 + "when 'BATCH' then cr.request_id "
                 + "when 'WMS' then cr.request_id "
                 + "when 'M-ADMIN' then cr.request_id "
                 + "when 'M-PARTNER' then cr.request_id "
                 + "when 'M-FRONT' then cr.request_id "
	             + "else 'ADMIN'"
	             + "end request_id"
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo }, claimBundle )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimReq),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo, claimBundle.claimBundleNo }, claimReq )),
                    null
                )
            )
            .INNER_JOIN("(SELECT * FROM dms.itm_mng_code imc WHERE imc.gmc_cd = 'claim_status') cs ON cs.mc_cd = cb.claim_status")
            .INNER_JOIN("(SELECT * FROM dms.itm_mng_code imc WHERE imc.gmc_cd = 'claim_reason_type') crt ON crt.mc_cd = cr.claim_reason_type")
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo, claimBundle.claimBundleNo }, claimItem )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.purchaseOrderNo }, purchaseOrder ))
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.purchaseOrderNo,claimItem.itemNo }, orderItem ))
                )
            )

            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimPickShipping),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimPickShipping ))
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimExchShipping),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimExchShipping ))
                )
            );

        //검색어 조건
        if(!claimInfoListSetDto.getClaimSearchType().equalsIgnoreCase("ALL") && !claimInfoListSetDto.getSchClaimSearch().equalsIgnoreCase("")){
            //클레임 번호, 주문번호, 배송번호, 주문상품번호, 그룹 클레임 번호로 조회 시 날짜 조건 없이 전체 조회
            switch (claimInfoListSetDto.getClaimSearchType()){
                case  "CLAIMBUNDLENO" : case  "PURCHASEORDERNO" : case  "BUNDLENO" : case "ORDERITEMNO": case "CLAIMNO" : case "MARKETORDERNO" :
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(claimSearchType, claimInfoListSetDto.getSchClaimSearch())
                    );
                    break;
                default:
                    queryOfOrder.WHERE(
                        SqlUtils.betweenDay(claimDateType, claimInfoListSetDto.getSchStartDt(), claimInfoListSetDto.getSchEndDt()),
                        SqlUtils.equalColumnByInput(claimSearchType, claimInfoListSetDto.getSchClaimSearch())
                    );
                    break;
            }
        }else{
            queryOfOrder.WHERE(
                SqlUtils.betweenDay(claimDateType, claimInfoListSetDto.getSchStartDt(), claimInfoListSetDto.getSchEndDt())
            );
        }
        //클레임 타입 C:취소, R:반품, X:교환
        if(!claimInfoListSetDto.getSchClaimType().equalsIgnoreCase("ALL")){
            queryOfOrder.WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimType, claimInfoListSetDto.getSchClaimType())
            );
        }

        //점포사용자인 경우 점포코드를 적용
        if(claimInfoListSetDto.getStoreId() != null && !claimInfoListSetDto.getStoreId().equalsIgnoreCase("")){
            queryOfOrder.WHERE(
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimItem.storeType, ObjectUtils.toArray("'DS'")),
                SqlUtils.equalColumnByInput(claimItem.storeId, claimInfoListSetDto.getStoreId())
            );
        }

        //클레임 상태 (C1: 신청/ C2: 승인/ C3: 완료/ C4: 철회/ C8: 보류/ C9: 거부)
        if(claimInfoListSetDto.getSchClaimStatus() != null && !claimInfoListSetDto.getSchClaimStatus().equalsIgnoreCase("ALL")){
            switch (claimInfoListSetDto.getSchClaimStatus()){
                case "P4" :
                    queryOfOrder.WHERE(
                        "(select count(*) from claim_payment cp where cp.claim_no = cm.claim_no and payment_status in ('P4') > 0)"
                    );
                    break;
                default:
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, claimInfoListSetDto.getSchClaimStatus())
                    );
                    break;
            }
        }

        //신청채널 (MYPAGE:구매자 / STORE: 스토어/ ADMIN: 어드민웹 / PARTNER: 파트너웹 / SYSTEM: 시스템배치)
        if(claimInfoListSetDto.getSchClaimChannel() != null && !claimInfoListSetDto.getSchClaimChannel().equalsIgnoreCase("ALL")){
            //ADMIN으로 조회시 NOT IN 으로 조건을 건다. 어드민에서 등록된 클레임은 userId가 requestId로 등록된다.
            switch (claimInfoListSetDto.getSchClaimChannel()) {
                case "ADMIN":
                    queryOfOrder.WHERE(
                        "cr.request_id NOT IN('MYPAGE', 'STORE', 'PARTNER', 'SYSTEM', 'WMS', 'OMNI', 'M-ADMIN', 'M-PARTNER', 'M-FRONT')"
                    );
                    break;
                default:
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(claimReq.requestId, claimInfoListSetDto.getSchClaimChannel())
                    );
                    break;
            }
        }

        //회원/비회원 여부 (Y:회원 / N:비회원)
        if(claimInfoListSetDto.getSchUserYn().equalsIgnoreCase("Y") && !claimInfoListSetDto.getSchUserSearch().equalsIgnoreCase("")){//회원
            queryOfOrder.WHERE(
                SqlUtils.equalColumnByInput(claimBundle.userNo, claimInfoListSetDto.getSchUserSearch())
            );
        }else if(claimInfoListSetDto.getSchUserYn().equalsIgnoreCase("N")){//비회원
            //비회원 조회 조건(BUYERNM:구매자 이름 / BUYERMOBILE:구매자 연락처)
            switch (claimInfoListSetDto.getSchBuyerInfo()){
                //구매자 이름으롲 조회
                case "BUYERNM" :
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(purchaseOrder.buyerNm, claimInfoListSetDto.getSchUserSearch())
                    );
                    break;
                //구매자 연락처로 조회
                case "BUYERMOBILE" :
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(purchaseOrder.buyerMobileNo, claimInfoListSetDto.getSchUserSearch())
                    );
                    break;
                case "BUYERMAIL" :
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(purchaseOrder.buyerEmail, claimInfoListSetDto.getSchUserSearch())
                    );
                    break;

                default:
                    break;
            }
        }

        //파트너 ID 조회
        if (claimInfoListSetDto.getStoreType() != null && !claimInfoListSetDto.getStoreType().equalsIgnoreCase("ALL")) {
            queryOfOrder.WHERE(
                SqlUtils.equalColumnByInput(claimItem.storeType, claimInfoListSetDto.getStoreType())
            );
            if(!StringUtils.isEmpty(claimInfoListSetDto.getSchPartnerId())){
                String searchStoreType = claimInfoListSetDto.getStoreType();
                if(searchStoreType.equals("DS")){
                    searchStoreType = claimItem.partnerId;
                } else {
                    searchStoreType = claimItem.storeId;
                }
                queryOfOrder.WHERE(
                    SqlUtils.equalColumnByInput(searchStoreType, claimInfoListSetDto.getSchPartnerId())
                );
            }
        }

        //수거 상태 (NN: 요청대기, N0: 수거예약, P0:자동접수요청, P1: 요청완료, P2: 수거중, P3: 수거완료, P8 :수거실패 (반품자동접수실패))
        if(claimInfoListSetDto.getSchPickupStatus() != null && !claimInfoListSetDto.getSchPickupStatus().equalsIgnoreCase("ALL")){
            switch (claimInfoListSetDto.getSchPickupStatus()){
                case "P0" :
                    queryOfOrder.WHERE(
                        "cps.pick_status IN('P0', 'P1')"
                    );
                    break;
                default:
                    queryOfOrder.WHERE(
                        SqlUtils.equalColumnByInput(claimPickShipping.pickStatus, claimInfoListSetDto.getSchPickupStatus())
                    );
                    break;
            }
        }

        //배송 상태 (NN: 요청대기, N0: 수거예약, P0:자동접수요청, P1: 요청완료, P2: 수거중, P3: 수거완료, P8 :수거실패 (반품자동접수실패))
        if(claimInfoListSetDto.getSchExchStatus() != null && !claimInfoListSetDto.getSchExchStatus().equalsIgnoreCase("ALL")){
            queryOfOrder.WHERE(
                SqlUtils.equalColumnByInput(claimExchShipping.exchStatus, claimInfoListSetDto.getSchExchStatus())
            );
        }

        //배송유형 (RESERVE_DRCT_DLV : 명절선물세트, RESERVE_TD_DRCT : 절임배추, RESERVE_DRCT_NOR : 일반상품
        if(claimInfoListSetDto.getSchReserveShipMethod() != null && !claimInfoListSetDto.getSchReserveShipMethod().equalsIgnoreCase("")){
            queryOfOrder.WHERE(
                SqlUtils.equalColumnByInput(orderItem.reserveShipMethod, claimInfoListSetDto.getSchReserveShipMethod())
            );
        }

        if(!"ALL".equals(claimInfoListSetDto.getPartnerCd())){
            queryOfOrder.WHERE(
                SqlUtils.sqlFunction(MysqlFunction.IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_MARKET'", "'ORD_SUB_MARKET'")),
                SqlUtils.equalColumnByInput(purchaseOrder.marketType, claimInfoListSetDto.getPartnerCd().toUpperCase(Locale.KOREA))
            );
        }
        log.debug("selectClaimList Query :: \n [{}]", queryOfOrder.toString());
        return queryOfOrder.toString();
    }


     /**
     * Admin 클레임 관리 - 클레임 관리 - 조회 - 클레임정보
     * 클레임 상세정보 조회
     *
     * @param claimDetailInfoSetDto 클레임 정보 조회 dto
     * @return String 클레임 상세정보 Query
     * @throws Exception 오류처리.
     */
    public static String selectClaimDetailInfo(ClaimDetailInfoSetDto claimDetailInfoSetDto) throws Exception {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);

        SQL queryOfOrder = new SQL()
            .SELECT(
                claimMst.claimNo,
                claimReq.claimReqNo,
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimBundle.claimType), nonAliasing(claimBundle.claimType), claimBundle.claimType),
                claimBundle.claimType.concat(" as claim_type_code"),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimBundle.claimStatus), nonAliasing(claimBundle.claimStatus), claimBundle.claimStatus),
                claimBundle.claimStatus.concat(" as claim_status_code"),
                claimBundle.claimBundleNo,
                purchaseOrder.purchaseOrderNo,
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(payment.paymentStatus), nonAliasing(payment.paymentStatus), payment.paymentStatus),
                purchaseOrder.orderDt,
                payment.paymentFshDt,
                purchaseOrder.platform,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(purchaseOrder.marketType, "'-'"), purchaseOrder.marketType),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(purchaseOrder.marketOrderNo, "'-'"), purchaseOrder.marketOrderNo),
                purchaseOrder.userNo,
                purchaseOrder.buyerNm,
                purchaseOrder.buyerEmail,
                purchaseOrder.nomemOrderYn,
                purchaseOrder.buyerMobileNo,
                claimBundle.bundleNo,
                claimMst.claimPartYn,
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "org_purchase_order_no",
                    SqlUtils.equalColumnByInput(
                        purchaseOrder.cmbnYn, "Y"
                    ),
                    "(select purchase_order_no from purchase_order where org_purchase_order_no = cm.purchase_order_no limit 1)",
                    "'-'"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo }, claimBundle )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimReq),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo, claimBundle.claimBundleNo }, claimReq )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(payment),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.paymentNo }, payment )),
                    null
                )
            )
             .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.purchaseOrderNo }, purchaseOrder )),
                    null
                )
            )
            .WHERE(
              SqlUtils.equalColumnByInput(claimMst.claimNo, claimDetailInfoSetDto.claimNo),
              SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, claimDetailInfoSetDto.purchaseOrderNo)
            )
            .LIMIT(1);
        StringBuilder query = new StringBuilder();

        query.append(queryOfOrder.toString());

        log.debug("selectClaimDetailInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

     /**
     * Admin 클레임 관리 - 클레임 관리 - 조회 - 클레임정보 주문상세 리스트
     * 클레임 상세정보 리스트 조회
     *
     * @param claimDetailInfoSetDto 클레임 상세정보 조회 dto
     * @return String 클레임 상세정보 Query
     * @throws Exception   오류처리.
     */
    public static String selectClaimDetailInfoList(ClaimDetailInfoSetDto claimDetailInfoSetDto) throws Exception {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class,Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class, Boolean.TRUE);
        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);


        SQL query = new SQL()
            .SELECT(
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimBundle.claimStatus), nonAliasing(claimBundle.claimStatus), claimBundle.claimStatus),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimPickShipping.pickStatus), "pickup_status", claimPickShipping.pickStatus),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimExchShipping.exchStatus), "exch_shipping_status", claimExchShipping.exchStatus),
                "case when "
                    + "(select count(*) "
                    + "from claim_payment cp "
                    + "where "
                    + "cp.claim_no = ci.claim_no "
                    + "and cp.purchase_order_no = ci.purchase_order_no "
                    + "and cp.payment_status in('P4')) > 0 then '실패' "
                    + "when "
                    + "(select count(*) "
                    + "from claim_payment cp "
                    + "where "
                    + "cp.claim_no = ci.claim_no "
                    + "and cp.purchase_order_no = ci.purchase_order_no "
                    + "and cp.payment_status not in('P4') and cp.payment_status in('P1')) > 0 then '요청' "
                    + "when "
                    + "(select count(*) "
                    + "from claim_payment cp "
                    + "where "
                    + "cp.claim_no = ci.claim_no "
                    + "and cp.purchase_order_no = ci.purchase_order_no "
                    + "and cp.payment_status not in('P4') and cp.payment_status in('P0')) > 0 then '대기' "
                    + "else '완료' end as 'refund_status'",
                claimBundle.claimBundleNo,
                claimItem.claimItemNo,
                claimItem.bundleNo,
                claimItem.purchaseOrderNo,
                claimItem.orderItemNo,
                claimItem.itemNo,
                claimItem.itemName,
                claimOpt.claimOptNo,
                SqlUtils.sqlFunction(MysqlFunction.FN_ORDER_OPTION_NM_MANAGE, claimOpt.orderOptNo, "opt_item_nm"),
                SqlUtils.aliasToRow(claimOpt.claimOptQty, "claim_qty"),
                SqlUtils.aliasToRow(claimItem.itemPrice, "claim_price"),
                claimItem.partnerId,
                claimItem.storeType,
                claimMst.siteType
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimItem.claimNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.claimNo, claimItem.claimBundleNo }, claimBundle )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimItemNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimPickShipping),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimPickShipping )),
                    null
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimExchShipping),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.claimBundleNo }, claimExchShipping )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimItem.purchaseOrderNo, claimDetailInfoSetDto.purchaseOrderNo),
                SqlUtils.equalColumnByInput(claimItem.claimNo, claimDetailInfoSetDto.claimNo)
            )
            .ORDER_BY(
                "claim_item_no"
            );

        log.debug("selectClaimDetailInfoList Query :: \n [{}]", query.toString());
        return query.toString();
    }


    /**
     * Admin 클레임 관리 - 클레임 관리 - 조회 - 클레임정보 주문상세 - 환불예정금액 조회
     * 등록된 클레임 환불예정금액조회
     *
     * @param claimNo
     * @return String 등록된 클레임 환불예정금액조회
     */
    public static String selectRegisterPreRefundAmt(@Param("claimNo") long claimNo){
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimMst.claimNo,
                SqlUtils.aliasToRow(claimMst.completeAmt, "refund_amt"),
                SqlUtils.aliasToRow(claimMst.totItemPrice, "item_price"),
                SqlUtils.aliasToRow(claimMst.totShipPrice, "ship_price"),
                SqlUtils.aliasToRow(claimMst.totIslandShipPrice, "island_ship_price"),
                SqlUtils.aliasToRow(claimMst.totAddShipPrice, "add_ship_price"),
                SqlUtils.aliasToRow(claimMst.totAddIslandShipPrice, "add_island_ship_price"),
                SqlUtils.aliasToRow(claimMst.totDiscountAmt, "discount_amt"),
                SqlUtils.aliasToRow(claimMst.totItemDiscountAmt, "item_discount_amt"),
                SqlUtils.aliasToRow(claimMst.totShipDiscountAmt, "ship_discount_amt"),
                SqlUtils.aliasToRow(claimMst.totEmpDiscountAmt, "emp_discount_amt"),
                SqlUtils.aliasToRow(claimMst.totCartDiscountAmt, "cart_discount_amt"),
                SqlUtils.aliasToRow(claimMst.totPromoDiscountAmt, "promo_discount_amt"),
                SqlUtils.aliasToRow(claimMst.totCardDiscountAmt, "card_discount_amt"),
                claimMst.pgAmt,
                claimMst.mileageAmt,
                claimMst.mhcAmt,
                claimMst.dgvAmt,
                claimMst.ocbAmt,
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(claimMst.totItemPrice, claimMst.totShipPrice, claimMst.totIslandShipPrice),
                    "purchase_amt"
                ),
                "case "
                + "when cm.oos_yn = 'Y' "
                + "then (select sum(cp.substitution_amt) from claim_payment cp where cp.claim_no = cm.claim_no) "
                + "else 0 "
                + "end as substitution_amt",
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(claimMst.totAddTdDiscountAmt, claimMst.totAddDsDiscountAmt),
                    "add_coupon_discount_amt"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            );
        log.debug("selectRegisterPreRefundAmt Query :: \n [{}]", query.toString());
        return query.toString();
    }


    /**
    * Admin 클레임 관리 - 클레임 관리 - 조회 - 취소신청 사유
    * 클레임 상세정보 조회
    *
    * @param claimNo
    * @return String 취소신청 사유
    * @throws Exception 오류처리.
    */
    public static String selectCancelReasonInfo(@Param("claimNo") long claimNo){

        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimMst.claimNo,
                SqlUtils.aliasToRow(claimReq.claimReasonType, "claim_reason_type_code"),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimReq.claimReasonType), nonAliasing(claimReq.claimReasonType), claimReq.claimReasonType),
                claimReq.claimReasonDetail,
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimReq.pendingReasonType), nonAliasing(claimReq.claimReasonType), claimReq.pendingReasonType),
                claimReq.pendingReasonDetail,
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimReq.rejectReasonType), nonAliasing(claimReq.claimReasonType), claimReq.rejectReasonType),
                claimReq.rejectReasonDetail,
                "case "
                 + "when cb.claim_ship_fee_enclose = 'Y' then '택배 동봉'"
                 + "else '환불금에서 차감'"
                 + "end claim_ship_fee_enclose",
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimBundle.claimStatus), nonAliasing(claimBundle.claimStatus), claimBundle.claimStatus),
                claimBundle.claimStatus.concat(" as claim_status_code"),
                claimReq.deductShipYn,
                claimReq.deductPromoYn,
                claimReq.deductDiscountYn,
                claimMst.marketType
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimReq),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo }, claimReq ))
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo }, claimBundle )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            )
            .LIMIT(1);
        log.debug("selectCancelReasonInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
    * Admin 클레임 관리 - 클레임 관리 - 조회 - 반품/교환 신청 사유
    * 클레임 상세정보 조회
    *
    * @param claimNo
    * @return String 반품신청 사유
    * @throws Exception 오류처리.
    */
    public static String selectReturnExchangeReasonInfo(@Param("claimNo") long claimNo)throws Exception {

        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class,Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimMst.claimNo,
                SqlUtils.aliasToRow(claimReq.claimReasonType, "claim_reason_type_code"),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimReq.claimReasonType), nonAliasing(claimReq.claimReasonType), claimReq.claimReasonType),
                claimReq.claimReasonDetail,
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimReq.pendingReasonType), nonAliasing(claimReq.claimReasonType), claimReq.pendingReasonType),
                claimReq.pendingReasonDetail,
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimReq.rejectReasonType), nonAliasing(claimReq.claimReasonType), claimReq.rejectReasonType),
                claimReq.rejectReasonDetail,
                claimReq.uploadFileName,
                claimReq.uploadFileName2,
                claimReq.uploadFileName3,
                claimMst.totAddIslandShipPrice,
                claimMst.totAddShipPrice,
                "case "
                 + "when cm.who_reason = 'B' then '구매자 책임'"
                 + "when cm.who_reason = 'S' then '판매자 책임'"
                 + "when cm.who_reason = 'H' then '홈플러스 책임'"
                 + "else '귀책사유 등록 오류'"
                 + "end who_reason ",
                "case "
                 + "when cb.claim_ship_fee_enclose = 'Y' then '택배 동봉'"
                 + "else '환불금에서 차감'"
                 + "end claim_ship_fee_enclose",
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimBundle.claimStatus), nonAliasing(claimBundle.claimStatus), claimBundle.claimStatus),
                claimBundle.claimStatus.concat(" as claim_status_code"),
                claimBundle.bundleNo,
                claimBundle.purchaseOrderNo,
                claimItem.storeId,
                claimItem.storeType,
                "case "
                 + "when cr.request_id = 'MYPAGE' then 'Y' "
                 + "else 'N' "
                 + "end as userRequestYn",
                claimReq.deductDiscountYn,
                claimReq.deductPromoYn,
                claimReq.deductShipYn,
                claimMst.marketType
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimReq),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo}, claimReq ))
                )
            ).INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo }, claimBundle ))
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimMst.claimNo, claimBundle.claimBundleNo }, claimItem )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            )
            .LIMIT(1);
        log.debug("selectReturnExchangeReasonInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
    * Admin 클레임 관리 - 클레임 관리 - 클레임 상세조회 - 반품배송정보
    * 클레임 반품배송정보 조회
    *
    * @param claimBundleNo
    * @return String 반품배송정보
    * @throws Exception 오류처리.
    */
    public static String selectClaimPickShippingInfo(@Param("claimBundleNo") long claimBundleNo) throws Exception {

        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        OrderPickupInfoEntity orderPickupInfo = EntityFactory.createEntityIntoValue(OrderPickupInfoEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);



        SQL query = new SQL()
            .SELECT(
                claimPickShipping.claimPickShippingNo,
                claimPickShipping.pickReceiverNm,
                claimPickShipping.pickMobileNo,
                claimPickShipping.pickZipcode,
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimPickShipping.pickShipType), "delivery_type", claimPickShipping.pickShipType),
                claimPickShipping.pickShipType.concat(" as pick_ship_type_code"),
                SqlPatternConstants.getConvertPattern(escrowMngCode, "pick_dlv", "dlv_cd", claimPickShipping.pickDlvCd),
                claimPickShipping.pickDlvCd,
                claimPickShipping.pickInvoiceNo,
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "pick_register_type",
                    SqlUtils.equalColumnByInput(
                        claimPickShipping.isAuto, "Y"
                    ),
                    "'자동접수'",
                    "'일반접수'"
                ),
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "pick_addr",
                    SqlUtils.equalColumnByInput(
                        SqlPatternConstants.getConditionByPattern("IFNULL", claimPickShipping.pickRoadBaseAddr, "''"), ""
                    ),
                    claimPickShipping.pickBaseAddr,
                    claimPickShipping.pickRoadBaseAddr
                ),
                claimPickShipping.pickBaseAddr,
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "pick_addr_detail",
                    SqlUtils.equalColumnByInput(
                        SqlPatternConstants.getConditionByPattern("IFNULL", claimPickShipping.pickRoadDetailAddr, "''"), ""
                    ),
                    claimPickShipping.pickRoadDetailAddr,
                    claimPickShipping.pickRoadDetailAddr
                ),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimPickShipping.pickStatus), "pickup_status", claimPickShipping.pickStatus),
                claimPickShipping.pickStatus.concat(" as pickStatusCode"),
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "ship_dt",
                    SqlUtils.equalColumnByInput(
                        SqlPatternConstants.getConditionByPattern("IFNULL", claimPickShipping.shipDt, "''"), ""
                    ),
                    claimPickShipping.pickP0Dt,
                    SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(claimPickShipping.shipDt, CustomConstants.YYYYMMDD))
                ),
                claimPickShipping.shiftId,
                claimPickShipping.slotId,
                claimPickShipping.pickP0Id,
                claimPickShipping.pickP0Dt,
                claimPickShipping.pickP1Dt,
                claimPickShipping.pickP2Dt,
                claimPickShipping.pickP3Dt,
                claimPickShipping.pickMemo,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimItem.originStoreId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(claimItem.partnerId, "P"))
                    ),
                    "store_nm"
                ),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(claimItem.purchaseOrderNo, claimItem.bundleNo, "'M'"), "ship_type"),
                orderPickupInfo.placeAddrExtnd,
                orderPickupInfo.startTime,
                orderPickupInfo.endTime,
                shippingItem.shipMethod,
                claimPickShipping.safetyUseYn
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimPickShipping))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimBundle),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimPickShipping.claimBundleNo }, claimBundle )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimBundle.partnerId, claimBundle.claimBundleNo }, claimItem )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(shippingItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.orderItemNo, claimBundle.purchaseOrderNo, claimBundle.bundleNo }, shippingItem )),
                    null
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(orderPickupInfo, ObjectUtils.toArray(claimItem.purchaseOrderNo,claimItem.bundleNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimPickShipping.claimBundleNo, claimBundleNo)
            )
            .LIMIT(1);;
        log.debug("selectClaimPickShippingInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
    * Admin 클레임 관리 - 클레임 관리 - 클레임 상세조회 - 교환배송정보
    * 클레임 교환배송정보 조회
    *
    * @param claimBundleNo
    * @return String 교환배송정보
    * @throws Exception 오류처리.
    */
    public static String selectClaimExchShippingInfo(@Param("claimBundleNo") long claimBundleNo) throws Exception {

        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimExchShipping.claimExchShippingNo,
                claimExchShipping.exchReceiverNm,
                claimExchShipping.exchMobileNo,
                claimExchShipping.exchZipcode,
                claimExchShipping.exchShipType.concat(" as exch_ship_type_code"),
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimExchShipping.exchShipType), "delivery_type", claimExchShipping.exchShipType),
                SqlPatternConstants.getConvertPattern(escrowMngCode, "exch_dlv", "dlv_cd", claimExchShipping.exchDlvCd),
                claimExchShipping.exchDlvCd,
                claimExchShipping.exchInvoiceNo,
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "exch_addr",
                    SqlUtils.equalColumnByInput(
                        SqlPatternConstants.getConditionByPattern("IFNULL", claimExchShipping.exchRoadBaseAddr, "''"), ""
                    ),
                    claimExchShipping.exchBaseAddr,
                    claimExchShipping.exchRoadBaseAddr
                ),
                claimExchShipping.exchBaseAddr,
                SqlPatternConstants.getConditionByPatternWithAlias(
                    "CASE_WHEN",
                    "exch_addr_detail",
                    SqlUtils.equalColumnByInput(
                        SqlPatternConstants.getConditionByPattern("IFNULL", claimExchShipping.exchRoadDetailAddr, "''"), ""
                    ),
                    claimExchShipping.exchRoadDetailAddr,
                    claimExchShipping.exchRoadDetailAddr
                ),
                claimExchShipping.exchMemo,
                SqlPatternConstants.getConvertPattern(escrowMngCode, nonAliasing(claimExchShipping.exchStatus), "exch_shipping_status", claimExchShipping.exchStatus),
                claimExchShipping.exchStatus.concat(" as exchStatusCode"),
                claimExchShipping.exchD0Dt,
                claimExchShipping.exchD1Dt,
                claimExchShipping.exchD2Dt,
                claimExchShipping.exchD3Dt,
                claimExchShipping.safetyUseYn
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimExchShipping))
            .WHERE(
                SqlUtils.equalColumnByInput(claimExchShipping.claimBundleNo, claimBundleNo)
            );
        log.debug("selectClaimExchShippingInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
    * 클레임 환불수단 조회
    *
    * @param claimNo
    * @return String 클레임 상세정보 Query
    * @throws Exception 오류처리.
    */
    public static String selectRefundTypeList(@Param("claimNo") long claimNo){

        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        PaymentMethodEntity paymentMethod = EntityFactory.createEntityIntoValue(PaymentMethodEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimPayment.claimPaymentNo,
                SqlPatternConstants.getConvertPattern(escrowMngCode, "payment_type", "payment_type", claimPayment.parentPaymentMethod),
                SqlUtils.aliasToRow(claimPayment.parentPaymentMethod, "payment_type_code"),
                SqlPatternConstants.getConvertPattern(escrowMngCode, "refund_type", "refund_type", claimPayment.paymentType),
                SqlUtils.aliasToRow(claimPayment.paymentType, "refund_type_code"),
                claimPayment.paymentStatus.concat(" as refund_status"),
                claimPayment.originClaimPaymentNo,
                SqlPatternConstants.getConvertPattern(escrowMngCode, "refund_status_name", "claim_payment_status", claimPayment.paymentStatus),
                SqlUtils.subQuery(
                    paymentMethod.methodNm,
                    EntityFactory.getTableNameWithAlias(paymentMethod),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(paymentMethod.methodCd, claimPayment.methodCd),
                        SqlUtils.equalColumn(paymentMethod.siteType, claimMst.siteType)
                    ),
                    "method_nm"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimPayment))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimMst),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimPayment.claimNo }, claimMst )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.claimNo, claimNo)
            );
        log.debug("selectRefundTypeList Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectDiscountInfoList(@Param("claimNo")long claimNo){
        ClaimAdditionEntity claimAddtion = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);

        SQL discountQuery = new SQL()
            .SELECT(
                "CASE "
                    + "when ca.addition_type_detail like '%discount' then ca.addition_discount_no "
                    + "else ca.addition_coupon_no "
                    + "end order_discount_no",
                "case"
                + " when addition_type_detail = 'item_coupon' "
                    + "then '상품쿠폰'"
                + "when addition_type_detail = 'item_discount' "
                    + "then '상품즉시할인'"
                + "when addition_type_detail = 'affi_discount' "
                    + "then '제휴즉시할인'"
                + "when addition_type_detail = 'ship_coupon' "
                    + "then '배송비쿠폰'"
                + "when addition_type_detail = 'cart_coupon' "
                    + "then '장바구니쿠폰'"
                + "when addition_type_detail = 'card_discount' "
                    + "then '카드사즉시할인'"
                + "when addition_type_detail = 'add_td_coupon' "
                    + "then '중복쿠폰'"
                + "when addition_type_detail = 'add_ds_coupon' "
                    + "then '중복쿠폰'"
                + "else '-' "
                    + "end addition_type_detail",
                "case"
                + " when addition_type_detail = 'item_coupon' "
                    + "then concat('상품번호 : ', "+ claimAddtion.itemNo +") "
                + "when addition_type_detail = 'item_discount' "
                    + "then concat('상품번호 : ',"+ claimAddtion.itemNo +") "
                + "when addition_type_detail = 'affi_discount' "
                    + "then concat('상품번호 : ',"+ claimAddtion.itemNo +") "
                + "when addition_type_detail = 'ship_coupon' "
                    + "then concat('배송번호 : ',"+ claimAddtion.bundleNo +") "
                + "when addition_type_detail = 'cart_coupon' "
                    + "then concat('주문번호 : ',"+ claimAddtion.purchaseOrderNo +") "
                + "when addition_type_detail = 'card_discount' "
                    + "then concat('상품번호 : ',"+ claimAddtion.itemNo +") "
                + "when addition_type_detail = 'add_td_coupon' "
                    + "then concat('상품번호 : ',"+ claimAddtion.itemNo +") "
                + "when addition_type_detail = 'add_ds_coupon' "
                    + "then concat('상품번호 : ',"+ claimAddtion.itemNo +") "
                + "else '-'"
                    + "end discount_target",
                claimAddtion.additionSumAmt.concat(" as discountAmt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimAddtion))
            .WHERE(
                SqlUtils.sqlFunction(MysqlFunction.IN, claimAddtion.additionType, ObjectUtils.toArray("'D','S'")),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimAddtion.additionTypeDetail, ObjectUtils.toArray("'promo_discount', 'pickup', 'delivery'")),
                "ca.addition_sum_amt > 0",
                SqlUtils.equalColumnByInput(claimAddtion.claimNo, claimNo)
            );


        SQL empDiscountQuery = new SQL()
            .SELECT(
                "''".concat(" as order_disount_no"),
                 "'임직원 할인' as addition_type_detail",
                "concat('상품번호 : ', "+claimItem.itemNo+") as discount_target",
                claimItem.empDiscountAmt.concat(" as discountAmt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimItem))
            .WHERE(
                "emp_discount_amt > 0",
                SqlUtils.equalColumnByInput(claimItem.claimNo, claimNo)
            );

        StringBuilder query = new StringBuilder();
        query.append(discountQuery.toString());
        query.append("\nUNION ALL\n");
        query.append(empDiscountQuery.toString());
        log.debug("selectDiscountInfoList Query ::  [{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimGiftList(@Param("claimNo")long claimNo){

        ClaimAdditionEntity claimAddtion = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        PurchaseGiftEntity purchaseGift = EntityFactory.createEntityIntoValue(PurchaseGiftEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimAddtion.itemNo,
                claimItem.itemName,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseGift.giftTargetType, "23"),
                        "'아이템-금액'",
                        "'개별아이템'"),
                    purchaseGift.giftTargetType
                    ),
                purchaseGift.purchaseGiftNo,
                claimAddtion.giftMsg
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimAddtion))
            .INNER_JOIN(
                   "purchase_gift pg on ca.addition_discount_no = pg.purchase_gift_no "
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimAddtion.itemNo, claimAddtion.claimNo}, claimItem )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimAddtion.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimAddtion.additionType, "G")
            );
        log.debug("selectClaimGiftList Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectPromoInfoList(@Param("claimNo")long claimNo){

        ClaimAdditionEntity claimAddtion = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        OrderPromoEntity orderPromo = EntityFactory.createEntityIntoValue(OrderPromoEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimAddtion.itemNo,
                claimItem.itemName,
                SqlUtils.caseWhenElse(
                    orderPromo.promoKind,
                    ObjectUtils.toArray(
                        "'BASIC'", "'행사상품'",
                        "'PICK'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.MINUS, orderPromo.mixMatchStdVal, "1"), " '+' ", "'1'")),
                        "'TOGETHER'", "'함께할인'",
                        "'INTERVAL'", "'함께할인'"
                    ),
                    SqlUtils.sqlFunction(
                        MysqlFunction.CASE,
                        ObjectUtils.toArray(
                            SqlUtils.AND(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, orderItem.promoNo),
                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.simpOrgItemPrice, "0")
                            ),
                            "'행사상품'",
                            "''"
                        )
                    ),
                    "promo_type"
                ),
                orderPromo.promoNo,
                claimAddtion.additionSumAmt.concat(" as promoDiscountAmt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimAddtion))
            .INNER_JOIN(
                   "order_promo op on ca.order_discount_no = op.promo_no "
                       + "and ca.purchase_order_no = op.purchase_order_no "
                       + "and ca.bundle_no = op.bundle_no "
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimAddtion.claimItemNo}, claimItem )),
                    null
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(orderItem),
                    Arrays.asList( SqlUtils.onClause(new Object[]{ claimItem.orderItemNo}, orderItem )),
                    null
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimAddtion.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimAddtion.additionTypeDetail, "promo_discount")
            );
        log.debug("selectPromoInfoList Query :: \n [{}]", query.toString());
        return query.toString();
    }


    public static String selectClaimBundleInfo(@Param("claimNo") long claimNo){
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimBundle.bundleNo,
                claimBundle.purchaseOrderNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo)
            );
        log.debug("selectClaimBundleInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimShipInfo (@Param("bundleNo") long bundleNo){
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        OrderPickupInfoEntity orderPickupInfo = EntityFactory.createEntityIntoValue(OrderPickupInfoEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                shippingItem.shipNo,
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, "'M'"), "ship_type"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderItem.mallType, "TD"),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.storeId, "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(orderItem.partnerId, "P"))
                    ),
                    "store_nm"
                ),
                shippingItem.shipMethod,
                shippingAddr.receiverNm,
                shippingAddr.shipMobileNo,
                shippingAddr.zipcode,
                shippingAddr.roadBaseAddr,
                shippingAddr.roadDetailAddr,
                shippingAddr.baseAddr,
                shippingAddr.detailAddr,
                orderPickupInfo.placeNm,
                orderPickupInfo.placeAddrExtnd,
                orderPickupInfo.startTime,
                orderPickupInfo.endTime
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition( orderItem, ObjectUtils.toArray(shippingItem.orderItemNo) )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition( shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo) )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(orderPickupInfo, ObjectUtils.toArray(orderItem.purchaseOrderNo,shippingItem.bundleNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo)
            );

        StringBuilder sb = new StringBuilder();
        sb.append(query.toString());
        sb.append("\n LIMIT 0, 1");

        log.debug("selectClaimShipInfo Query :: \n [{}]", sb.toString());
        return sb.toString();
    }


    public static String selectShipStatus (@Param("bundleNo") long bundleNo){
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                "case when count( si.ship_status ) > 0 then 'Y' else 'N' end deliveryYn"
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingItem))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo),
                SqlUtils.equalColumnByInput(shippingItem.shipStatus, "D3")
            );
        log.debug("selectShipStatus Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectSlotSiftInfo (@Param("slotId") String slotId, @Param("shiftId") String shiftId, @Param("storeType") String storeType, @Param("storeId") String storeId){
        StoreSlotMngEntity storeSlotMng = EntityFactory.createEntityIntoValue(StoreSlotMngEntity.class);
        StoreShiftMngEntity storeShiftMng = EntityFactory.createEntityIntoValue(StoreShiftMngEntity.class);

        SQL query = new SQL()
            .SELECT(
                "slot.slot_ship_start_time",
                    "slot.slot_ship_end_time",
                    "(select "
                    + "IFNULL(imc.mc_nm, '') as mc_cd "
                    + "from dms.itm_mng_code imc "
                    + "where imc.gmc_cd = 'weekday'"
                    + "and imc.mc_cd = shift.ship_weekday ) as ship_weekday"
            )
            .FROM("store_slot_mng slot inner join store_shift_mng shift "
                + "on slot.store_id = shift.store_id and slot.shift_id = shift.shift_id ")
            .WHERE(
                SqlUtils.equalColumnByInput("slot.slot_id", slotId),
                SqlUtils.equalColumnByInput("shift.shift_id", shiftId),
                SqlUtils.equalColumnByInput("shift.store_type", storeType),
                SqlUtils.equalColumnByInput("slot.store_id", storeId)

            )
            .LIMIT(1);;
        log.debug("selectSlotSiftInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimProcess (@Param("claimNo") long claimNo){
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class, Boolean.TRUE);
        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(

                "CASE "
                + "WHEN cr.request_id not in ('MYPAGE','PARTNER', 'STORE', 'SYSTEM', 'BATCH', 'WMS', 'OMNI', 'M-ADMIN', 'M-PARTNER', 'M-FRONT') then 'ADMIN' "
                + "ELSE cr.request_id "
                + "END request_channel",
                "CASE "
                + "WHEN cr.request_id = 'PARTNER' "
                + "    THEN concat(uf_get_store_partner_name(cb.partner_id, 'P'), ' | ',cb.partner_id) "
                + "WHEN cr.request_id IN ('MYPAGE','STORE','M-FRONT') "
                + "    THEN po.buyer_nm "
                + "WHEN cr.request_id IN ('SYSTEM', 'BATCH', 'WMS', 'OMNI','M-ADMIN','M-PARTNER') "
                + "    THEN cr.request_id "
                + "ELSE (select concat(ei.name, ' | ', ei.empid) from dms.employee_info ei where ei.empid = cr.request_id) "
                + "END request_id",
                claimReq.requestDt,
                "CASE "
                + "WHEN cr.approve_id not in ('MYPAGE','PARTNER', 'STORE', 'SYSTEM', 'BATCH', 'WMS', 'OMNI', 'M-ADMIN', 'M-PARTNER', 'M-FRONT') then 'ADMIN' "
                + "ELSE cr.approve_id "
                + "END approve_channel",
                "CASE "
                + "WHEN cr.approve_id = 'PARTNER' "
                + "    THEN concat(uf_get_store_partner_name(cb.partner_id, 'P'), ' | ',cb.partner_id) "
                + "WHEN cr.approve_id IN ('MYPAGE','STORE','M-FRONT') "
                + "    THEN po.buyer_nm "
                + "WHEN cr.approve_id IN ('SYSTEM', 'BATCH', 'WMS', 'OMNI') "
                + "    THEN cr.approve_id "
                + "ELSE (select concat(ei.name, ' | ', ei.empid) from dms.employee_info ei where ei.empid = cr.approve_id) "
                + "END approve_id",
                claimReq.approveDt,

                "CASE "
                + "WHEN cr.pending_id not in ('MYPAGE','PARTNER', 'STORE', 'SYSTEM', 'BATCH', 'WMS', 'OMNI','M-ADMIN','M-PARTNER','M-FRONT') then 'ADMIN' "
                + "ELSE cr.pending_id "
                + "END pending_channel",
                "CASE "
                + "WHEN cr.pending_id = 'PARTNER' "
                + "    THEN concat(uf_get_store_partner_name(cb.partner_id, 'P'), ' | ',cb.partner_id) "
                + "WHEN cr.pending_id IN ('MYPAGE','STORE','M-FRONT') "
                + "    THEN po.buyer_nm "
                + "WHEN cr.pending_id IN ('SYSTEM', 'BATCH', 'WMS', 'OMNI') "
                + "    THEN cr.pending_id "
                + "ELSE (select concat(ei.name, ' | ', ei.empid) from dms.employee_info ei where ei.empid = cr.pending_id) "
                + "END pending_id",
                claimReq.pendingDt,

                "CASE "
                + "WHEN cr.withdraw_id not in ('MYPAGE','PARTNER', 'STORE', 'SYSTEM', 'BATCH', 'WMS', 'OMNI', 'M-ADMIN','M-PARTNER', 'M-FRONT') then 'ADMIN' "
                + "ELSE cr.withdraw_id "
                + "END withdraw_channel",
                "CASE "
                + "WHEN cr.withdraw_id = 'PARTNER' "
                + "    THEN concat(uf_get_store_partner_name(cb.partner_id, 'P'), ' | ',cb.partner_id) "
                + "WHEN cr.withdraw_id IN ('MYPAGE','STORE','M-FRONT') "
                + "    THEN po.buyer_nm "
                + "WHEN cr.withdraw_id IN ('SYSTEM', 'BATCH', 'WMS', 'OMNI') "
                + "    THEN cr.withdraw_id "
                + "ELSE (select concat(ei.name, ' | ', ei.empid) from dms.employee_info ei where ei.empid = cr.withdraw_id) "
                + "END withdraw_id",
                claimReq.withdrawDt,

                "case  "
                + "when cr.reject_id not in ('MYPAGE','PARTNER', 'STORE', 'SYSTEM', 'BATCH', 'WMS', 'OMNI','M-ADMIN','M-PARTNER','M-FRONT') then 'ADMIN' "
                + "else cr.reject_id "
                + "end reject_channel",
                "CASE "
                + "WHEN cr.reject_id = 'PARTNER' "
                + "    THEN concat(uf_get_store_partner_name(cb.partner_id, 'P'), ' | ',cb.partner_id) "
                + "WHEN cr.reject_id IN ('MYPAGE','STORE','M-FRONT') "
                + "    THEN po.buyer_nm "
                + "WHEN cr.reject_id IN ('SYSTEM', 'BATCH', 'WMS', 'OMNI') "
                + "    THEN cr.reject_id "
                + "ELSE (select concat(ei.name, ' | ', ei.empid) from dms.employee_info ei where ei.empid = cr.reject_id) "
                + "END reject_id",
                claimReq.rejectDt,

                "case  "
                + "when cr.complete_id not in ('MYPAGE','PARTNER', 'STORE', 'SYSTEM', 'BATCH', 'WMS', 'OMNI','M-ADMIN','M-PARTNER','M-FRONT') then 'ADMIN' "
                + "else cr.complete_id "
                + "end complete_channel",
                "CASE "
                + "WHEN cr.complete_id = 'PARTNER' "
                + "    THEN concat(uf_get_store_partner_name(cb.partner_id, 'P'), ' | ',cb.partner_id) "
                + "WHEN cr.complete_id IN ('MYPAGE','STORE','M-FRONT') "
                + "    THEN po.buyer_nm "
                + "WHEN cr.complete_id IN ('SYSTEM', 'BATCH', 'WMS', 'OMNI') "
                + "    THEN cr.complete_id "
                + "ELSE (select concat(ei.name, ' | ', ei.empid) from dms.employee_info ei where ei.empid = cr.complete_id) "
                + "END complete_id",
                claimReq.completeDt,

                "CASE "
                + "WHEN pr.reg_id not in ('MYPAGE','PARTNER', 'STORE', 'SYSTEM', 'BATCH', 'WMS', 'OMNI','M-ADMIN','M-PARTNER','M-FRONT') then 'ADMIN' "
                + "ELSE pr.reg_id "
                + "END pick_reg_channel",
                "CASE "
                + "WHEN pr.reg_id = 'PARTNER' "
                + "    THEN concat(uf_get_store_partner_name(cb.partner_id, 'P'), ' | ',cb.partner_id) "
                + "WHEN pr.reg_id IN ('MYPAGE','STORE','M-FRONT') "
                + "    THEN po.buyer_nm "
                + "WHEN pr.reg_id IN ('SYSTEM', 'BATCH', 'WMS', 'OMNI') "
                + "    THEN pr.reg_id "
                + "ELSE (select concat(ei.name, ' | ', ei.empid) from dms.employee_info ei where ei.empid = pr.reg_id) "
                + "END pick_reg_id",
                "pr.reg_dt as pick_reg_dt",

                "case  "
                + "when cps.chg_id not in ('MYPAGE','PARTNER', 'STORE', 'SYSTEM', 'BATCH', 'WMS', 'OMNI','M-ADMIN','M-PARTNER','M-FRONT') then 'ADMIN' "
                + "else cps.chg_id "
                + "end pick_complete_channel",
                "CASE "
                + "WHEN cps.chg_id = 'PARTNER' "
                + "    THEN concat(uf_get_store_partner_name(cb.partner_id, 'P'), ' | ',cb.partner_id) "
                + "WHEN cps.chg_id IN ('MYPAGE','STORE','M-FRONT') "
                + "    THEN po.buyer_nm "
                + "WHEN cps.chg_id IN ('SYSTEM', 'BATCH', 'WMS', 'OMNI') "
                + "    THEN cps.chg_id "
                + "ELSE (select concat(ei.name, ' | ', ei.empid) from dms.employee_info ei where ei.empid = cps.chg_id) "
                + "END pick_complete_id",
                claimPickShipping.pickP3Dt.concat(" as pick_complete_dt"),

                "CASE "
                + "WHEN er.reg_id not in ('MYPAGE','PARTNER', 'STORE', 'SYSTEM', 'BATCH', 'WMS', 'OMNI','M-ADMIN','M-PARTNER','M-FRONT') then 'ADMIN' "
                + "ELSE er.reg_id "
                + "END exch_reg_channel",
                "CASE "
                + "WHEN er.reg_id = 'PARTNER' "
                + "    THEN concat(uf_get_store_partner_name(cb.partner_id, 'P'), ' | ',cb.partner_id) "
                + "WHEN er.reg_id IN ('MYPAGE','STORE','M-FRONT') "
                + "    THEN po.buyer_nm "
                + "WHEN er.reg_id IN ('SYSTEM', 'BATCH', 'WMS', 'OMNI') "
                + "    THEN er.reg_id "
                + "ELSE (select concat(ei.name, ' | ', ei.empid) from dms.employee_info ei where ei.empid = er.reg_id) "
                + "END exch_reg_id",
                "er.reg_dt as exch_reg_dt",

                "case  "
                + "when ces.chg_id not in ('MYPAGE','PARTNER', 'STORE', 'SYSTEM', 'BATCH', 'WMS', 'OMNI','M-ADMIN','M-PARTNER','M-FRONT') then 'ADMIN' "
                + "else ces.chg_id "
                + "end exch_complete_channel",
                "CASE "
                + "WHEN ces.chg_id = 'PARTNER' "
                + "    THEN concat(uf_get_store_partner_name(cb.partner_id, 'P'), ' | ',cb.partner_id) "
                + "WHEN ces.chg_id IN ('MYPAGE','STORE','M-FRONT') "
                + "    THEN po.buyer_nm "
                + "WHEN ces.chg_id IN ('SYSTEM', 'BATCH', 'WMS', 'OMNI') "
                + "    THEN cps.chg_id "
                + "ELSE (select concat(ei.name, ' | ', ei.empid) from dms.employee_info ei where ei.empid = ces.chg_id) "
                + "END exch_complete_id",
                claimExchShipping.exchD3Dt.concat(" as exch_complete_dt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimReq))
            .INNER_JOIN(
                SqlUtils.createJoinCondition( claimBundle, ObjectUtils.toArray(claimReq.claimNo) )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition( purchaseOrder, ObjectUtils.toArray(claimBundle.purchaseOrderNo) )
            )
            .LEFT_OUTER_JOIN(
                "(select * from process_history ph where ph.history_reason = '수거요청') pr on cr.claim_no = pr.claim_no"
            )
            .LEFT_OUTER_JOIN(
                "(select * from process_history ph where ph.history_reason = '교환배송') er on cr.claim_no = er.claim_no"
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(claimPickShipping, ObjectUtils.toArray(claimReq.claimBundleNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(claimExchShipping, ObjectUtils.toArray(claimReq.claimBundleNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimReq.claimNo, claimNo)
            )
            .LIMIT(1);
        log.debug("selectClaimProcess Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimPaymentProcess (@Param("claimNo") long claimNo){
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                SqlPatternConstants.getConvertPattern(escrowMngCode, "payment_type", "payment_type", claimPayment.parentPaymentMethod),
                claimPayment.regDt,
                "case  "
                + "when cp.reg_id not in ('MYPAGE','PARTNER', 'STORE', 'SYSTEM', 'BATCH', 'WMS', 'OMNI','M-ADMIN','M-PARTNER','M-FRONT') then 'ADMIN' "
                + "else cp.reg_id "
                + "end reg_channel",
                "CASE "
                + "WHEN cp.reg_id = 'PARTNER' "
                + "    THEN concat(uf_get_store_partner_name(cb.partner_id, 'P'), ' | ',cb.partner_id) "
                + "WHEN cp.reg_id IN ('MYPAGE','STORE', 'M-FRONT') "
                + "    THEN po.buyer_nm "
                + "WHEN cp.reg_id IN ('SYSTEM', 'BATCH', 'WMS', 'OMNI') "
                + "    THEN cp.reg_id "
                + "ELSE (select concat(ei.name, ' | ', ei.empid) from dms.employee_info ei where ei.empid = cp.reg_id) "
                + "END reg_id",
                claimPayment.payCompleteDt,
                "case  "
                + "when cp.chg_id not in ('MYPAGE','PARTNER', 'STORE', 'SYSTEM', 'BATCH', 'WMS', 'OMNI','M-ADMIN','M-PARTNER','M-FRONT') then 'ADMIN' "
                + "else cp.chg_id "
                + "end chg_channel",
                "CASE "
                + "WHEN cp.chg_id = 'PARTNER' "
                + "    THEN concat(uf_get_store_partner_name(cb.partner_id, 'P'), ' | ',cb.partner_id) "
                + "WHEN cp.chg_id IN ('MYPAGE','STORE','M-FRONT') "
                + "    THEN po.buyer_nm "
                + "WHEN cp.chg_id IN ('SYSTEM', 'BATCH', 'WMS', 'OMNI') "
                + "    THEN cp.chg_id "
                + "ELSE (select concat(ei.name, ' | ', ei.empid) from dms.employee_info ei where ei.empid = cp.chg_id) "
                + "END chg_id"
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimPayment))
            .INNER_JOIN("(select * from claim_bundle cb where cb.claim_no = "+ claimNo +" limit 1) cb ON cp.claim_no = cb.claim_no")
            .INNER_JOIN(
                SqlUtils.createJoinCondition( purchaseOrder, ObjectUtils.toArray(claimPayment.purchaseOrderNo) )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.claimNo, claimNo)
            );
        log.debug("selectClaimPaymentProcess Query :: \n [{}]", query.toString());
        return query.toString();
    }


    public static String selectClaimHistoryList (@Param("claimNo") long claimNo){
        ProcessHistoryEntity processHistory = EntityFactory.createEntityIntoValue(ProcessHistoryEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                processHistory.historyReason,
                "case "
                + "when ph.history_detail_before is not null "
                + "then concat(ph.history_detail_before,' -> ',ph.history_detail_desc) "
                + "else ph.history_detail_desc "
                + "end as history_detail",
                processHistory.regDt,
                "CASE "
                + "WHEN ph.reg_id = 'PARTNER' "
                + "    THEN concat(uf_get_store_partner_name(cb.partner_id, 'P'), ' | ',cb.partner_id) "
                + "WHEN ph.reg_id IN ('MYPAGE','STORE','M-FRONT') "
                + "    THEN po.buyer_nm "
                + "WHEN ph.reg_id IN ('STORE', 'SYSTEM', 'BATCH', 'WMS', 'OMNI') "
                + "    THEN ph.reg_id "
                + "ELSE (select concat(ei.name, ' | ', ei.empid) from dms.employee_info ei where ei.empid = ph.reg_id) "
                + "END reg_id"
            )
            .FROM(EntityFactory.getTableNameWithAlias(processHistory))
            .INNER_JOIN(
                SqlUtils.createJoinCondition( claimBundle, ObjectUtils.toArray(processHistory.claimNo) )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition( purchaseOrder, ObjectUtils.toArray(claimBundle.purchaseOrderNo) )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(processHistory.claimNo, claimNo)
            );
        log.debug("selectClaimHistoryList Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectStoreInfo(@Param("storeId")String storeId){
        ItmPartnerStoreEntity itemPartnerStore = EntityFactory.createEntityIntoValue(ItmPartnerStoreEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                itemPartnerStore.storeId,
                itemPartnerStore.partnerId,
                itemPartnerStore.storeNm,
                itemPartnerStore.storeNm2,
                itemPartnerStore.storeType,
                itemPartnerStore.storeKind,
                itemPartnerStore.mgrNm,
                itemPartnerStore.originStoreId,
                itemPartnerStore.zipcode,
                itemPartnerStore.addr1,
                itemPartnerStore.addr2,
                itemPartnerStore.phone,
                itemPartnerStore.closedInfo,
                itemPartnerStore.useYn,
                itemPartnerStore.onlineYn,
                itemPartnerStore.pickupYn,
                itemPartnerStore.onlineCostCenter,
                itemPartnerStore.storeCostCenter
            )
            .FROM(EntityFactory.getTableNameWithAlias("dms", itemPartnerStore))
            .WHERE(
                SqlUtils.equalColumnByInput(itemPartnerStore.storeId, storeId)
            );
        log.debug("selectStoreInfo \n[{}]", query.toString());
        return query.toString();
    }

   private static String nonAliasing(String value){
        return SqlUtils.nonAlias(value);
    }
}
