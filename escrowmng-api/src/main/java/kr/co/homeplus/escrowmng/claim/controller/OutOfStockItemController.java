package kr.co.homeplus.escrowmng.claim.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.OutOfStockItemListGetDto;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.OutOfStockItemListSetDto;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.StoreShiftListSetDto;
import kr.co.homeplus.escrowmng.claim.service.outOfStock.OutOfStockItemService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/claim/outOfStock")
@RequiredArgsConstructor
@Slf4j
@Api(tags = "주문관리 > 결품/대체상품 조회")
public class OutOfStockItemController {

    private final OutOfStockItemService outOfStockItemService;

    @PostMapping("/getOutOfStockItemList")
    @ApiOperation(value = "결품/대체상품 리스트 조회")
    public ResponseObject<List<OutOfStockItemListGetDto>> getOutOfStockItemList(@RequestBody OutOfStockItemListSetDto outOfStockItemListSetDto) {
        return outOfStockItemService.getOutOfStockItemList(outOfStockItemListSetDto);
    }

    @PostMapping("/getStoreShiftList")
    @ApiOperation(value = "점포 shift 리스트 조회")
    public ResponseObject<List<String>> getStoreShiftList(@RequestBody StoreShiftListSetDto storeShiftListSetDto) {
        return outOfStockItemService.getStoreShiftList(storeShiftListSetDto);
    }
}
