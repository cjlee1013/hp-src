package kr.co.homeplus.escrowmng.order.model.entity;

public class OrderDiscountCartEntity {

    // 주문할인장바구니순번
    public String orderDiscountCartSeq;

    // 주문할인번호
    public String orderDiscountNo;

    // 구매주문번호
    public String purchaseOrderNo;

    // 상품번호
    public String itemNo;

    // 옵션번호
    public String optNo;

    // 추가구성번호
    public String addOptNo;

    // 할인율
    public String discountRate;

    // 할인금액
    public String discountAmt;

    // 등록일시
    public String regDt;

}
