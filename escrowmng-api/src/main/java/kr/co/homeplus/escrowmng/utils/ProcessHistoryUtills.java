package kr.co.homeplus.escrowmng.utils;


import java.util.LinkedHashMap;
import kr.co.homeplus.escrowmng.common.model.processHistory.ProcessHistoryRegDto;
import kr.co.homeplus.escrowmng.common.service.prcessHistory.impl.ProcessHistoryMapperServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProcessHistoryUtills {

    private final ProcessHistoryMapperServiceImpl processHistoryMapperService;


    public void setProcessHistory(Object object, String historyReason, String historyDetailDesc, String historyDetailBefore, String regId) throws Exception {

        LinkedHashMap<String, Object> map = ObjectUtils.getConvertMap(object);

        ProcessHistoryRegDto processHistoryRegDto = ProcessHistoryRegDto.builder()
            .historyReason(String.valueOf(historyReason))
            .historyDetailDesc(String.valueOf(historyDetailDesc))
            .regId(String.valueOf(regId))
            .regChannel(String.valueOf("ADMIN"))
            .build();

        if(map.get("purchaseOrderNo") != null){
            processHistoryRegDto.setPurchaseOrderNo(String.valueOf(map.get("purchaseOrderNo")));
        }
        if(map.get("bundleNo") != null){
            processHistoryRegDto.setBundleNo(String.valueOf(map.get("purchaseOrderNo")));
        }
        if(map.get("claimBundleNo") != null){
            processHistoryRegDto.setClaimBundleNo(String.valueOf(map.get("claimBundleNo")));
        }
        if(map.get("claimNo") != null){
            processHistoryRegDto.setClaimNo(String.valueOf(map.get("claimNo")));
        }
        if(map.get("shipNo") != null){
            processHistoryRegDto.setShipNo(String.valueOf(map.get("shipNo")));
        }
        if(map.get("claimPickShippingNo") != null){
            processHistoryRegDto.setClaimPickShippingNo(String.valueOf(map.get("claimPickShippingNo")));
        }
        if(map.get("claimExchShippingNo") != null){
            processHistoryRegDto.setClaimExchShippingNo(String.valueOf(map.get("claimExchShippingNo")));
        }
        if(map.get("multiBundleNo") != null){
            processHistoryRegDto.setMultiBundleNo(ObjectUtils.toLong(map.get("multiBundleNo")));
        }
        if(historyDetailBefore != null){
            processHistoryRegDto.setHistoryDetailBefore(historyDetailBefore);
        }
        processHistoryMapperService.setProcessHistory(processHistoryRegDto);
    }
}
