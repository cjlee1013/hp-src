package kr.co.homeplus.escrowmng.message.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import lombok.Data;

@ApiModel("주문검색 search dto")
@Data
public class OrderSendListRequest {

    @ApiModelProperty(value = "날짜타입 (배송일/주문일)", allowableValues = "ship,order")
    private String orderType;

    @ApiModelProperty("배송/주문일")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Asia/Seoul")
    private LocalDate orderDt;

    @ApiModelProperty("점포 id")
    private String storeId;

    @ApiModelProperty("점포명")
    private String storeNm;

    @ApiModelProperty("회원여부")
    private boolean isMember;

    @ApiModelProperty("회원번호")
    private long userNo;

    @ApiModelProperty("구매자명")
    private String buyerNm;

    @ApiModelProperty("shift")
    private String shiftId;

    @ApiModelProperty("slot")
    private String slotId;

    @ApiModelProperty("배송기사명")
    private String driverNm;
}
