package kr.co.homeplus.escrowmng.order.model.entity;

public class OrderPickupInfoEntity {

    // 주문번호
    public String purchaseOrderNo;

    //
    public String bundleNo;

    // 점포ID
    public String storeId;

    // 장소번호
    public String placeNo;

    // 락커사용여부
    public String lockerYn;

    // 락커번호
    public String lockerNo;

    // 락커비밀번호
    public String lockerPasswd;

    // 배송일(픽업일)
    public String shipDt;

    // 장소명
    public String placeNm;

    // 장소주소
    public String placeAddr;

    // 장소주소
    public String placeAddrExtnd;

    // 장소전화번호
    public String telNo;

    // 등록자
    public String regId;

    // 등록일
    public String regDt;

    // 수정자
    public String chgId;

    // 수정일
    public String chgDt;

    //시작시간
    public String startTime;

    //종료시간
    public String endTime;

}
