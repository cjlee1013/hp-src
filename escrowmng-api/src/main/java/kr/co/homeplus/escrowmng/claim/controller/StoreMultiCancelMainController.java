package kr.co.homeplus.escrowmng.claim.controller;



import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.storeMultiCancel.StoreMultiCancelListGetDto;
import kr.co.homeplus.escrowmng.claim.model.storeMultiCancel.StoreMultiCancelListSetDto;
import kr.co.homeplus.escrowmng.claim.service.StoreMultiCancel.StoreMultiCancelService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/claim/storeMultiCancel")
@RequiredArgsConstructor
@Api(tags = "주문관리 > 점포 일괄주문취소")
public class StoreMultiCancelMainController {

    private final StoreMultiCancelService storeMultiCancelService;

    /**
     * 점포일괄취소 조회
     */
    @PostMapping("/getStoreMultiCancelList")
    @ApiOperation(value = "점포일괄취소 리스트 조회")
    public ResponseObject<List<StoreMultiCancelListGetDto>> getStoreMultiCancelList(@RequestBody StoreMultiCancelListSetDto storeMultiCancelListSetDto) {
        return storeMultiCancelService.getStoreMultiCancelList(storeMultiCancelListSetDto);
    }
}
