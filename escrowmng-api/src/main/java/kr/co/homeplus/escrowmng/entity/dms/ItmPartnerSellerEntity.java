package kr.co.homeplus.escrowmng.entity.dms;

public class ItmPartnerSellerEntity {
    //
    public String partnerId;
    //
    public String businessNm;
    //
    public String infoCenter;
    //
    public String phone1;
    //
    public String phone2;
    //
    public String companyInfo;
    //
    public String email;
    //
    public String homepage;
    //
    public String categoryCd;
    //
    public String smsYn;
    //
    public String smsYnDt;
    //
    public String emailYn;
    //
    public String emailYnDt;
    //
    public String storeUseYn;
    //
    public String cultureDeductionYn;
    //
    public String cultureDeductionNo;
    //
    public String returnDeliveryYn;
    //
    public String regDt;
    //
    public String regId;
    //
    public String chgDt;
    //
    public String chgId;
    //
    public String saleType;
    //
    public String itemNo;
    //
    public String ofVendorCd;
    //
    public String ofVendorStatus;
}
