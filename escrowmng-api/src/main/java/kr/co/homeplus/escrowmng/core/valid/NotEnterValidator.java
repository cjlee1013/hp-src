package kr.co.homeplus.escrowmng.core.valid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import kr.co.homeplus.escrowmng.utils.ValidUtil;

public class NotEnterValidator implements ConstraintValidator<NotEnter, String> {
    @Override
    public void initialize(NotEnter constraintAnnotation) {}

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return !ValidUtil.isEnterStr(s);
    }
}
