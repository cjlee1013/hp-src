package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimShipInfoDto {

    @ApiModelProperty(value = "운송번호", position = 1)
    private long shipNo;

    @ApiModelProperty(value = "이름", position = 2)
    private String receiverNm;

    @ApiModelProperty(value = "점포명/파트너명", position = 3)
    private String storeNm;

    @ApiModelProperty(value = "연락처", position = 4)
    private String shipMobileNo;

    @ApiModelProperty(value = "우편번호", position = 5)
    private String zipCode;

    @ApiModelProperty(value = "도로명", position = 6)
    private String roadBaseAddr;

    @ApiModelProperty(value = "도로명상세", position = 7)
    private String roadDetailAddr;

    @ApiModelProperty(value = "지번", position = 8)
    private String baseAddr;

    @ApiModelProperty(value = "지번상세", position = 9)
    private String detailAddr;

    @ApiModelProperty(value = "방문수령(위치)", position = 10)
    private String placeNm;

    @ApiModelProperty(value = "배송타입(TD,DS,AURORA)", position = 11)
    private String shipType;

    @ApiModelProperty(value = "배송타입(TD,DS,AURORA)", position = 12)
    private String shipMethod;

    @ApiModelProperty(value="수거 시작시간", position = 13)
    public String startTime;

    @ApiModelProperty(value="수거 종료시간", position = 14)
    public String endTime;

    @ApiModelProperty(value = "배송중 상태 여부", position = 15)
    public String deliveryYn;

}
