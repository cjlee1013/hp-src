package kr.co.homeplus.escrowmng.delivery.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.delivery.model.ItemShiftManageModel;
import kr.co.homeplus.escrowmng.delivery.model.ItemShiftManageSelectModel;

@EscrowSlaveConnection
public interface ItemShiftManageSlaveMapper {

    List<ItemShiftManageModel> selectItemShiftManageList(ItemShiftManageSelectModel itemShiftManageSelectModel);
}
