package kr.co.homeplus.escrowmng.escrow.model;

import lombok.Data;

@Data
public class getParameter {
    private String key;
    private Object value;

    public static getParameter create(String key, Object value) {
        getParameter param = new getParameter();
        param.setKey(key);
        param.setValue(value);
        return param;
    }
}
