package kr.co.homeplus.escrowmng.core.valid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import kr.co.homeplus.escrowmng.utils.ValidUtil;

public class NotAllowInputValidator implements ConstraintValidator<NotAllowInput, String>{

    private String[] types;

    @Override
    public void initialize(NotAllowInput constraintAnnotation) {
        this.types = constraintAnnotation.types();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return ValidUtil.isNotAllowInput(s, types);
    }
}
