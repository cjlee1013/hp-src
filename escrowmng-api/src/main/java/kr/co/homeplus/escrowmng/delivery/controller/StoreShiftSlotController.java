package kr.co.homeplus.escrowmng.delivery.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.delivery.model.RemoteShipManageSelectModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreShiftManageModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreShiftManageSelectModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreSlotManageModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreSlotManageSelectModel;
import kr.co.homeplus.escrowmng.delivery.service.StoreShiftSlotService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Admin > 배송관리 > 점포배송정보 > Shift 관리
 */
@RestController
@RequestMapping(value=("/escrow/storeDelivery"),produces = MediaType.APPLICATION_JSON_VALUE )
@RequiredArgsConstructor
@Api(tags = "배송관리 > 점포배송정보 > Shift 관리")
public class StoreShiftSlotController {

    private final StoreShiftSlotService storeShiftSlotService;

    @GetMapping("/getShiftManageList")
    @ApiOperation(value = "점포 shift 조회", response = StoreShiftManageModel.class)
    public ResponseObject getStoreShift(@Valid StoreShiftManageSelectModel storeShiftManageSelectModel) throws Exception {
        List<StoreShiftManageModel> storeShiftManageModelList = storeShiftSlotService.getStoreShiftManageList(storeShiftManageSelectModel);
        return ResourceConverter.toResponseObject(storeShiftManageModelList);
    }

    @GetMapping("/getSlotManageList")
    @ApiOperation(value = "점포 slot 조회", response = StoreSlotManageModel.class)
    public ResponseObject getStoreSlot(StoreSlotManageSelectModel storeSlotManageSelectModel) throws Exception {
        List<StoreSlotManageModel> storeSlotMngList = storeShiftSlotService.getStoreSlotManageList(storeSlotManageSelectModel);
        return ResourceConverter.toResponseObject(storeSlotMngList);
    }

    @PostMapping("/saveSlotManage")
    @ApiOperation(value = "점포 slot 사용여부 저장", response = Integer.class)
    public ResponseObject<Integer> saveStoreSlot(@RequestBody List<StoreSlotManageModel> storeSlotManageModelList) throws Exception {
        return ResourceConverter.toResponseObject(storeShiftSlotService.saveStoreSlot(storeSlotManageModelList));
    }

    @GetMapping("/getRemoteShipList")
    @ApiOperation(value = "원거리 배송 조회", response = StoreSlotManageModel.class)
    public ResponseObject getRemoteShip(@Valid RemoteShipManageSelectModel remoteShipManageSelectModel) throws Exception {
        HashMap<String, Object> storeRemoteShipList = storeShiftSlotService.getRemoteShipList(remoteShipManageSelectModel);
        return ResourceConverter.toResponseObject(storeRemoteShipList);
    }

}
