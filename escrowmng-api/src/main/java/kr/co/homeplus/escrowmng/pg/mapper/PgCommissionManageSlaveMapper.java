package kr.co.homeplus.escrowmng.pg.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.pg.model.PgCommissionManageDto;
import kr.co.homeplus.escrowmng.pg.model.PgCommissionManageSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PgCommissionTargetDto;
import kr.co.homeplus.escrowmng.pg.model.PgCommissionTargetSelectDto;

@EscrowSlaveConnection
public interface PgCommissionManageSlaveMapper {
    List<PgCommissionManageDto> selectPgCommissionMngList(PgCommissionManageSelectDto pgCommissionManageSelectDto);
    List<PgCommissionTargetDto> selectPgCommissionTargetList(PgCommissionTargetSelectDto pgCommissionTargetSelectDto);
    int checkDuplicateCommission(PgCommissionManageSelectDto pgCommissionManageSelectDto);
}
