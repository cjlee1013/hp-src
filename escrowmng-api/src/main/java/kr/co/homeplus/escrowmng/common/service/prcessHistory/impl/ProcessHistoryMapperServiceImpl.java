package kr.co.homeplus.escrowmng.common.service.prcessHistory.impl;

import kr.co.homeplus.escrowmng.common.mapper.processHistory.ProcessHistoryMasterMapper;
import kr.co.homeplus.escrowmng.common.model.processHistory.ProcessHistoryRegDto;
import kr.co.homeplus.escrowmng.common.service.prcessHistory.ProcessHistoryMapperService;
import kr.co.homeplus.escrowmng.utils.SqlUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProcessHistoryMapperServiceImpl implements ProcessHistoryMapperService {

    private final ProcessHistoryMasterMapper writeMapper;

    public Boolean setProcessHistory(ProcessHistoryRegDto processHistoryRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertProcessHistory(processHistoryRegDto));
    }
}
