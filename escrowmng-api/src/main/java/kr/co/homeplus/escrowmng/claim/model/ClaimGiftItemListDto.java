package kr.co.homeplus.escrowmng.claim.model;


import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClaimGiftItemListDto {

    @ApiModelProperty(value = "상품번호", position = 1)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 2)
    private String itemNm;

    @ApiModelProperty(value = "행사구분", position = 3)
    private String giftTargetType;

    @ApiModelProperty(value = "행사번호", position = 4)
    private String giftNo;

    @ApiModelProperty(value = "행사명", position = 5)
    private String giftMsg;

}
