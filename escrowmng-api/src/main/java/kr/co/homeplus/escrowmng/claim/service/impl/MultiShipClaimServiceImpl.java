package kr.co.homeplus.escrowmng.claim.service.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.escrowmng.claim.mapper.multiShip.MultiShipClaimReadMapper;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.escrowmng.claim.model.ClaimShipInfoDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipClaimItemListGetDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipClaimItemListSetDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipInfoGetDto;
import kr.co.homeplus.escrowmng.claim.service.multiShip.MultiShipClaimMapperService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class MultiShipClaimServiceImpl implements MultiShipClaimMapperService {

    private final MultiShipClaimReadMapper multiShipClaimReadMapper;

    @Override
    public List<MultiShipInfoGetDto> getMultiShipList(long purchaseOrderNo, long bundleNo) {
        return multiShipClaimReadMapper.selectMultiShipList(purchaseOrderNo,bundleNo);
    }

    @Override
    public List<MultiShipClaimItemListGetDto> getMultiShipItemList(
        MultiShipClaimItemListSetDto multiShipClaimItemListSetDto) {
        return multiShipClaimReadMapper.selectMultiShipItemList(multiShipClaimItemListSetDto);
    }

    @Override
    public ClaimPreRefundCalcDto getMultiShipClaimPreRefundCalculation(ClaimPreRefundCalcSetDto calcSetDto) throws Exception {

        // 다중 클레임 건 여부 확인
        boolean isMulti = Boolean.FALSE;

        if(calcSetDto.getClaimItemList().size() > 1) {
            isMulti = Boolean.TRUE;
        }
        // 파라미터 용 맵 선언.
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        // 주문번호
        parameterMap.put("purchaseOrderNo", calcSetDto.getPurchaseOrderNo());
        // 상품주문번호
        parameterMap.put("orderItemNo", calcSetDto.getClaimItemList().stream().map(
            ClaimPreRefundItemList::getOrderItemNo).collect(Collectors.joining(":")));
        // 상품번호
        parameterMap.put("itemNo", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getItemNo).collect(Collectors.joining(":")));
        // 옵션번호
        parameterMap.put("orderOptNo", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderOptNo).collect(Collectors.joining(":")));
        // 클레임 수량
        parameterMap.put("claimQty", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getClaimQty).collect(Collectors.joining(":")));
        // 귀책사유
        parameterMap.put("whoReason", calcSetDto.getWhoReason());
        // 취소타입
        parameterMap.put("cancelType", calcSetDto.getCancelType());
        //행사미차감 여부
        parameterMap.put("piDeductPromoYn", calcSetDto.getPiDeductPromoYn());
        //할인 미차감 여부
        parameterMap.put("piDeductDiscountYn", calcSetDto.getPiDeductDiscountYn());
        //배송비 미차감 여부
        parameterMap.put("piDeductShipYn", calcSetDto.getPiDeductShipYn());
        // 동봉 여부
        parameterMap.put("encloseYn", calcSetDto.getEncloseYn());
        // 다중배송변호
        parameterMap.put("multiBundleNo", calcSetDto.getMultiBundleNo());
        log.info("다중배송 환불예정금조회 파라미터 : {}", parameterMap);
        return multiShipClaimReadMapper.callByMultiShipClaimPreRefundPriceMultiCalculation(parameterMap);
    }

    @Override
    public ClaimShipInfoDto getMultiShipInfo(long bundleNo, long multiBundleNo) {
        return multiShipClaimReadMapper.selectMultiShipInfo(bundleNo, multiBundleNo);
    }

    @Override
    public String getMultiShipDeliveryYn(long multiBundleNo) {
        return multiShipClaimReadMapper.selectMultiShipDeliveryYn(multiBundleNo);
    }
}
