package kr.co.homeplus.escrowmng.pg.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.pg.model.PgMidManageModel;

@EscrowMasterConnection
public interface PgMidMasterMapper {
    int insertPgMidManage(PgMidManageModel pgMidManageModel);
}
