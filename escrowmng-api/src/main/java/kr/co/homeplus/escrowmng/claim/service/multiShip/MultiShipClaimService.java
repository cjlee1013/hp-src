package kr.co.homeplus.escrowmng.claim.service.multiShip;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimShipInfoDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipClaimItemListGetDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipClaimItemListSetDto;
import kr.co.homeplus.escrowmng.claim.model.multiShip.MultiShipInfoGetDto;
import kr.co.homeplus.escrowmng.enums.OrderCode;
import kr.co.homeplus.escrowmng.order.factory.OrderResponseFactory;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MultiShipClaimService {

    private final MultiShipClaimMapperService mapperService;

    /**
     * 다중배송 배송지 정보 리스트 조회
     *
     * @param purchaseOrderNo 주문번호, bundleNo 번들번호
     * @return List<MultiShipInfoGetDto>
     */
    public ResponseObject<List<MultiShipInfoGetDto>> getMultiShipList(long purchaseOrderNo, long bundleNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getMultiShipList(purchaseOrderNo, bundleNo));
    }

    /**
     * 다중배송 상품 리스트 조회
     *
     * @param multiShipClaimItemListSetDto 주문번호, bundleNo 번들번호
     * @return List<MultiShipClaimItemListGetDto>
     */
    public ResponseObject<List<MultiShipClaimItemListGetDto>> getMultiShipItemList(MultiShipClaimItemListSetDto multiShipClaimItemListSetDto) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, mapperService.getMultiShipItemList(multiShipClaimItemListSetDto));
    }

    /**
     * 다중배송 환불예정금액 조회
     *
     * @param calcSetDto
     * @return ClaimPreRefundCalcGetDto
     */
    public ResponseObject<ClaimPreRefundCalcGetDto> getMultiShipRefundCalculation(ClaimPreRefundCalcSetDto calcSetDto) throws Exception {
        ClaimPreRefundCalcDto claimPreRefundCalcGetDto = mapperService.getMultiShipClaimPreRefundCalculation(calcSetDto);
        ClaimPreRefundCalcGetDto responseDto = new ClaimPreRefundCalcGetDto();
        BeanUtils.copyProperties(claimPreRefundCalcGetDto, responseDto);
        responseDto.setAddCouponDiscountAmt(claimPreRefundCalcGetDto.getAddTdDiscountAmt() + claimPreRefundCalcGetDto.getAddDsDiscountAmt());
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, responseDto);
    }

    /**
     * 다중배송 환불예정금액 조회
     *
     * @param bundleNo, multiBundleNo
     * @return ClaimShipInfoDto
     */
    public ResponseObject<ClaimShipInfoDto> getMultiShipInfo(long bundleNo, long multiBundleNo) throws Exception {
        ClaimShipInfoDto claimShipInfoDto = mapperService.getMultiShipInfo(bundleNo, multiBundleNo);
        claimShipInfoDto.setDeliveryYn(mapperService.getMultiShipDeliveryYn(multiBundleNo));
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimShipInfoDto);
    }



}
