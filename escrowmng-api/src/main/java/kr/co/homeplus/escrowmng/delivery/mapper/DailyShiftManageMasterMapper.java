package kr.co.homeplus.escrowmng.delivery.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.delivery.model.DailyShiftManageModel;
import kr.co.homeplus.escrowmng.delivery.model.DailyShiftStoreManageModel;

@EscrowMasterConnection
public interface DailyShiftManageMasterMapper {
    void insertDailyShiftManage(DailyShiftManageModel dailyShiftManageModel);
    int insertDailyShiftStoreManageList(List<DailyShiftStoreManageModel> dailyShiftStoreManageModelList);
    int updateDailyShiftManageUseYn(List<DailyShiftManageModel> dailyShiftManageModelList);
}
