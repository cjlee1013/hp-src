package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제수단 Tree DTO")
public class PaymentMethodTreeDto {
    @ApiModelProperty(notes = "결제수단코드", position = 1)
    private String methodCode;

    @ApiModelProperty(notes = "결제수단명", position = 2)
    private String methodName;

    @ApiModelProperty(notes = "상위결제수단코드", position = 3)
    private String parentCode;

    @ApiModelProperty(notes = "TreeDepth", position = 4)
    private String depth;
}