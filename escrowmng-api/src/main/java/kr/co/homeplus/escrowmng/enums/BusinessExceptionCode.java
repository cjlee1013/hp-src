package kr.co.homeplus.escrowmng.enums;


import kr.co.homeplus.escrowmng.enums.impl.EnumImpl;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * [공통] 에러코드 정의
 * 1001 ~ 8999 사용자 에러코드
 */
@RequiredArgsConstructor
public enum BusinessExceptionCode implements EnumImpl {

	ADMIN_VALIDATION_10000("10000", "동일한 기간에 중복된 데이터가 존재합니다.")
	, ADMIN_VALIDATION_10002("10002", "해당 카드는 이미 무이자/부분무이자 정보가 등록되어 있습니다.")
	, ADMIN_VALIDATION_10003("10003", "해당 카드는 이미 무이자유형이 이미 등록되어 있습니다.")
	, ADMIN_VALIDATION_10004("10004", "무이자 기간과 부분무이자 기간이 중복 됩니다. 할부기간 확인해 주시기 바랍니다.")
	;

	@Getter
	private final String code;
	@Getter
	private final String description;

	@Override
	public String toString() {
		return code + ": " + description;
	}

	@Override
	public String getResponseCode() {
		return this.code;
	}

	@Override
	public String getResponseMessage() {
		return this.description;
	}
}
