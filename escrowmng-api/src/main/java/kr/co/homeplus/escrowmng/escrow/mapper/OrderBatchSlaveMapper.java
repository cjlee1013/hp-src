package kr.co.homeplus.escrowmng.escrow.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchHistoryModel;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchHistorySelectModel;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchManageModel;
import kr.co.homeplus.escrowmng.escrow.model.OrderBatchManageSelectModel;
import org.apache.ibatis.annotations.Param;

@EscrowSlaveConnection
public interface OrderBatchSlaveMapper {
    List<OrderBatchManageModel> selectOrderBatchManageList(OrderBatchManageSelectModel orderBatchManageSelectModel);
    List<OrderBatchHistoryModel> selectOrderBatchHistoryList(OrderBatchHistorySelectModel orderBatchHistorySelectModel);
    List<OrderBatchHistoryModel> selectLgwBatchHistoryList(OrderBatchHistorySelectModel orderBatchHistorySelectModel);
    OrderBatchManageModel selectOrderBatchInfo(@Param("batchType") String batchType);
}
