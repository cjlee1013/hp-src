package kr.co.homeplus.escrowmng.statistics.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;

import kr.co.homeplus.escrowmng.statistics.model.GradeCouponStatisticsListGetDto;
import kr.co.homeplus.escrowmng.statistics.model.GradeCouponStatisticsListSetDto;
import kr.co.homeplus.escrowmng.statistics.service.GradeCouponStatisticsService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/statistics/promotion")
@RequiredArgsConstructor
@Api(tags = "통계 > 프로모션통계 > 등급별 쿠폰 통계")
public class GradeCouponStatisticsController {

    private final GradeCouponStatisticsService gradeCouponStatisticsService;

    /**
     * 통계 > 프로모션통계 > 등급별 쿠폰 통계 리스트 조회
     *
     * @param gradeCouponStatisticsListSetDto
     * @return String
     */
    @ResponseBody
    @PostMapping("/getGradeCouponStatisticsList")
    @ApiOperation(value = "등급별 쿠폰 통계 리스트 조회", response = GradeCouponStatisticsListGetDto.class)
    public ResponseObject<List<GradeCouponStatisticsListGetDto>> getGradeCouponStatisticsList(@RequestBody GradeCouponStatisticsListSetDto gradeCouponStatisticsListSetDto) {
        return gradeCouponStatisticsService.getGradeCouponStatisticsList(gradeCouponStatisticsListSetDto);
    }
}