package kr.co.homeplus.escrowmng.claim.sql;

import java.util.HashMap;
import java.util.LinkedHashMap;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimAdditionEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimBundleEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimExchShippingEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimItemEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimMstEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimOptEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimPaymentEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimPickShippingEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimReqEntity;
import kr.co.homeplus.escrowmng.claim.model.entity.OrderPromoEntity;
import kr.co.homeplus.escrowmng.constants.Constants;
import kr.co.homeplus.escrowmng.entity.dms.ItmPartnerSellerDeliveryEntity;
import kr.co.homeplus.escrowmng.entity.dms.ItmPartnerSellerEntity;
import kr.co.homeplus.escrowmng.enums.MysqlFunction;
import kr.co.homeplus.escrowmng.order.constants.CustomConstants;
import kr.co.homeplus.escrowmng.order.factory.EntityFactory;
import kr.co.homeplus.escrowmng.order.model.entity.BundleEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ItmMngCodeEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderDiscountEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderItemEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderOptEntity;
import kr.co.homeplus.escrowmng.order.model.entity.OrderPaymentDivideEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PaymentEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PurchaseOrderEntity;
import kr.co.homeplus.escrowmng.order.model.entity.PurchaseStoreInfoEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingItemEntity;
import kr.co.homeplus.escrowmng.order.model.entity.ShippingMileagePaybackEntity;
import kr.co.homeplus.escrowmng.order.model.entity.StoreShiftMngEntity;
import kr.co.homeplus.escrowmng.order.model.entity.StoreSlotMngEntity;
import kr.co.homeplus.escrowmng.order.model.entity.market.MarketOrderEntity;
import kr.co.homeplus.escrowmng.order.model.entity.market.MarketOrderItemEntity;
import kr.co.homeplus.escrowmng.order.model.entity.multi.MultiBundleEntity;
import kr.co.homeplus.escrowmng.order.model.entity.multi.MultiOrderItemEntity;
import kr.co.homeplus.escrowmng.utils.SqlUtils;
import kr.co.homeplus.escrowmng.utils.ObjectUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ClaimOrderCancelSql {

    /**
     * 주문취소 관련 주문내역 조회
     * 주문취소시 상품번호 및 옵션, 추가상품 여부를 선 확인.
     *
     * @param purchaseOrderNo 주문번호
     * @return String 주문취소 상품번호 체크 Query
     * @throws Exception 오류처리.
     */
    public static String selectPrevClaimOrderCancelCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) throws Exception {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        OrderPromoEntity orderPromo = EntityFactory.createEntityIntoValue(OrderPromoEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);

        SQL queryOfOrder = new SQL()
            .SELECT_DISTINCT(
                purchaseOrder.purchaseOrderNo,
                orderItem.orderItemNo,
                orderItem.itemNo,
                orderItem.itemNm1,
                SqlUtils.appendSingleQuoteWithAlias("0", "order_opt_no"),
                SqlUtils.appendSingleQuoteWithAlias("0", "order_qty"),
                SqlUtils.appendSingleQuoteWithAlias("0", "order_price"),
                orderItem.purchaseMinQty,
                SqlUtils.appendSingleQuoteWithAlias("1", "order_type"),
                purchaseOrder.orderDt,
                SqlUtils.subTableQuery(
                    new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO,claimItem.claimItemQty))
                        .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                        .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                        .WHERE(
                            SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                            SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                            SqlUtils.equalColumn(claimItem.itemNo, orderItem.itemNo),
                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                            SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'"))
                        ),
                    "claim_qty"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(orderItem.promoNo, "'-'"), orderItem.promoNo),
                SqlUtils.caseWhenElse(
                    orderPromo.promoKind,
                    ObjectUtils.toArray(
                        "'BASIC'", "'행사상품'",
                        "'PICK'", SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.MINUS, orderPromo.mixMatchStdVal, "1"), " '+' ", "'1'")),
                        "'TOGETHER'", "'함께할인'",
                        "'INTERVAL'", "'함께할인'"
                    ),
                    SqlUtils.sqlFunction(
                        MysqlFunction.CASE,
                        ObjectUtils.toArray(
                            SqlUtils.AND(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, orderItem.promoNo),
                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.simpOrgItemPrice, "0")
                            ),
                            "'행사상품'",
                            "''"
                        )
                    ),
                    "promo_nm1"
                ),
                bundle.diffQty,
                purchaseOrder.marketType
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition( orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo) )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition( bundle, ObjectUtils.toArray(orderItem.bundleNo, orderItem.purchaseOrderNo) )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    orderPromo,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, orderItem.promoNo, orderItem.promoDetailNo),
                    ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderPromo.promoType, "'SIMP'"))
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            )
            ;

        SQL queryOfOpt = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                orderItem.orderItemNo,
                orderItem.itemNo,
                SqlUtils.sqlFunction(MysqlFunction.FN_ORDER_OPT_NM, orderOpt.orderOptNo),
                orderOpt.orderOptNo,
                orderOpt.optQty,
                SqlUtils.customOperator(
                    CustomConstants.MULTIPLY,
                    ObjectUtils.toArray(
                        orderItem.itemPrice,
                        "(".concat(
                            SqlUtils.customOperator(
                                CustomConstants.MINUS,
                                orderOpt.optQty,
                                SqlUtils.sqlFunction(
                                    MysqlFunction.IFNULL_ZERO,
                                    SqlUtils.subTableQuery(
                                        new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimOpt.claimOptQty))
                                            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                                            .INNER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimBundle.claimBundleNo)) )
                                            .WHERE(
                                                SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                                SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                                                SqlUtils.equalColumn(orderOpt.orderOptNo, claimOpt.orderOptNo),
                                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'"))
                                            )
                                    )
                                )
                            )
                        ).concat(")")
                    ),
                    "order_price"
                ),
                SqlUtils.appendSingleQuoteWithAlias("0", "purchase_min_qty"),
                SqlUtils.appendSingleQuoteWithAlias("2", "order_type"),
                SqlUtils.appendSingleQuoteWithAlias("-", "order_dt"),
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL_ZERO,
                    SqlUtils.subTableQuery(
                        new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimOpt.claimOptQty))
                            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                            .INNER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimBundle.claimBundleNo)) )
                            .WHERE(
                                SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                                SqlUtils.equalColumn(orderOpt.orderOptNo, claimOpt.orderOptNo),
                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'"))
                            )
                    ),
                    "claim_qty"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(orderItem.promoNo, "'-'"), orderItem.promoNo),
                SqlUtils.appendSingleQuoteWithAlias("-", "promo_nm1"),
                SqlUtils.appendSingleQuoteWithAlias("-", "diff_qty"),
                purchaseOrder.marketType
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, orderOpt.orderOptNo)
            );

        if(bundleNo != 0){
            queryOfOrder.WHERE(
                SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo)
            );
            queryOfOpt.WHERE(
                SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo)
            );
        }
        SQL query = new SQL()
            .SELECT(
                "*"
            )
            .FROM("(".concat(queryOfOrder.toString()).concat("\nUNION ALL\n").concat(queryOfOpt.toString()).concat(") t"))
            .ORDER_BY(SqlUtils.orderByesc("order_item_no", "promo_no"))
            .ORDER_BY("order_type", "order_opt_no");

        log.debug("selectPrevClaimOrderCancelInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderItemInfoByClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                orderItem.itemNo,
                orderOpt.orderOptNo,
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderOpt.optQty),
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.MAX, claimOpt.claimOptQty))
                    ),
                    "claim_qty"
                ),
                orderItem.orderItemNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo)))
//            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.orderItemNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("'D1'", "'D2'")))))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo, orderItem.purchaseOrderNo, orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimItemNo, orderOpt.orderOptNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo)
            )
            .GROUP_BY(orderItem.itemNo, orderItem.orderItemNo, orderOpt.orderOptNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(orderItem.itemNo),
                SqlUtils.nonAlias(orderOpt.orderOptNo),
                "claim_qty",
                SqlUtils.nonAlias(orderItem.orderItemNo)
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "data"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, "claim_qty", "0")
            );
        log.debug("selectOrderItemInfoByClaim Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderItemInfoByTicketClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                orderItem.itemNo,
                orderOpt.orderOptNo,
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderOpt.optQty),
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.MAX, claimOpt.claimOptQty))
                    ),
                    "claim_qty"
                ),
                orderItem.orderItemNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.orderItemNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("'D1'","'D2'", "'D4'", "'D5'")))))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo, orderItem.purchaseOrderNo, orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimItemNo, orderOpt.orderOptNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo)
            )
            .GROUP_BY(orderItem.itemNo, orderItem.orderItemNo, orderOpt.orderOptNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(orderItem.itemNo),
                SqlUtils.nonAlias(orderOpt.orderOptNo),
                "claim_qty",
                SqlUtils.nonAlias(orderItem.orderItemNo)
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "data"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, "claim_qty", "0")
            );
        log.debug("selectOrderItemInfoByTicketClaim Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderBundleByClaim(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                purchaseOrder.purchaseOrderNo,
                bundle.bundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(bundle),
                    ObjectUtils.toList(SqlUtils.onClause(ObjectUtils.toArray(purchaseOrder.purchaseOrderNo), bundle))
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectOrderBundleByClaim Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
     * 환불예정금액 조회 - call procedure
     *
     * @param parameterMap 조회 파라미터
     * @return 환불예정금액
     */
    public static String callByClaimPreRefundPriceCalculation(LinkedHashMap<String, Object> parameterMap){
        log.info("환불예정금 조회(단건) :: \n[{}]", SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION, parameterMap.values().toArray()));
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION, parameterMap.values().toArray());
    }

    /**
     * 환불예정금액 다건 조회 - call procedure
     *
     * @param parameterMap 조회 파라미터
     * @return 환불예정금액
     */
    public static String callByClaimPreRefundPriceMultiCalculation(LinkedHashMap<String, Object> parameterMap){
        log.info("환불예정금 조회(다건) :: \n[{}]", SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_MULTI, parameterMap.values().toArray()));
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_MULTI, parameterMap.values().toArray());
    }

    /**
     * 환불예정금액 선물세트(다중배송) 조회 - call procedure
     *
     * @param parameterMap 조회 파라미터
     * @return 환불예정금액
     */
    public static String callByClaimMultiPreRefundPriceCalculation(HashMap<String, Object> parameterMap){
        log.info("환불예정금 조회(다중배송) :: \n[{}]", SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_MULTI_REFUND_CALCULATION_MULTI, parameterMap.values().toArray()));
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_MULTI_REFUND_CALCULATION_MULTI, parameterMap.values().toArray());
    }

    /**
     * 클레임 신청시 부분취소 여부 확인을 위한 쿼리
     *
     * @param purchaseOrderNo 주문거래번호
     * @param bundleNo 배송번호
     * @return 부분취소여부 확인 쿼리
     * @throws Exception
     */
    public static String selectOrderByClaimPartInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) throws Exception {
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);

        Object[] parameter = new Object[1];
        if(bundleNo != 0){
            parameter = new Object[2];
            parameter[1] = SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo);
        }
        parameter[0] = SqlUtils.equalColumnByInput(orderItem.purchaseOrderNo, purchaseOrderNo);

        SQL claimData = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, ObjectUtils.toArray(claimItem.claimItemQty))
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    claimItem,
                    ObjectUtils.toArray(claimBundle.claimBundleNo, claimBundle.purchaseOrderNo, claimBundle.bundleNo),
                    ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")))
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimItem.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
            )
            .GROUP_BY(claimItem.purchaseOrderNo);

        if(parameter.length > 1){
            claimData.WHERE(SqlUtils.equalColumnByInput(claimItem.bundleNo, bundleNo)).GROUP_BY(claimItem.bundleNo);
        }

        SQL query = new SQL()
            .SELECT(
                SqlUtils.subQuery(
                    SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, ObjectUtils.toArray(orderItem.itemQty), "orderItemQty"),
                    EntityFactory.getTableName(orderItem),
                    parameter,
                    "orderItemQty"
                ),
                SqlUtils.getAppendAliasToTableName("(" + claimData.toString() + ")", "claimItemQty")
            );
        log.debug("getOrderByClaimPartInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimCouponReIssue(@Param("claimNo") long claimNo, @Param("userNo") long userNo) {
        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        OrderDiscountEntity orderDiscount = EntityFactory.createEntityIntoValue(OrderDiscountEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                claimAddition.claimAdditionNo,
                orderDiscount.couponNo,
                orderDiscount.issueNo,
                SqlUtils.aliasToRow(claimAddition.additionSumAmt, "discount_amt"),
                orderDiscount.storeId
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimAddition))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderDiscount, ObjectUtils.toArray(claimAddition.orderDiscountNo, claimAddition.purchaseOrderNo, claimAddition.orderItemNo))
            )
            .WHERE(
                SqlUtils.middleLike(claimAddition.additionTypeDetail, "coupon"),
                SqlUtils.equalColumnByInput(claimAddition.isIssue, "N"),
                SqlUtils.equalColumnByInput(claimAddition.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimAddition.userNo, userNo),
                SqlUtils.equalColumnByInput(claimAddition.totalCancelYn, "Y")
            )
            ;
        log.debug("selectClaimCouponReIssue \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimInfo(@Param("claimNo") long claimNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        PurchaseStoreInfoEntity purchaseStoreInfo = EntityFactory.createEntityIntoValue(PurchaseStoreInfoEntity.class, Boolean.TRUE);
//        StoreSlotMngEntity storeSlotMng = EntityFactory.createEntityIntoValue(StoreSlotMngEntity.class, Boolean.TRUE);
        StoreShiftMngEntity storeShiftMng = EntityFactory.createEntityIntoValue(StoreShiftMngEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimMst.claimNo,
                claimMst.userNo,
                claimMst.claimPartYn,
                claimBundle.claimType,
                claimMst.purchaseOrderNo,
                claimBundle.bundleNo,
                claimBundle.claimBundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.orderCategory, claimBundle.orderCategory),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, orderItem.itemQty, orderItem.itemQty),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimItem.claimItemQty, claimItem.claimItemQty),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_N_Y,
                    SqlUtils.customOperator(
                        Constants.EQUAL,
                        SqlUtils.customOperator(
                            CustomConstants.ANGLE_BRACKET_RIGHT,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(
                                    MysqlFunction.DATE_FULL_24,
                                    SqlUtils.customOperator(
                                        CustomConstants.PLUS,
                                        ObjectUtils.toArray(
                                            SqlUtils.sqlFunction(MysqlFunction.MAX, storeShiftMng.shipDt),
                                            SqlUtils.sqlFunction(
                                                MysqlFunction.CONCAT,
                                                ObjectUtils.toArray(
                                                    SqlUtils.sqlFunction(
                                                        MysqlFunction.CASE,
                                                        ObjectUtils.toArray(
                                                            SqlUtils.sqlFunction(MysqlFunction.IS_NULL, SqlUtils.sqlFunction(MysqlFunction.MAX, storeShiftMng.shiftBeforeCloseTime)),
                                                            SqlUtils.sqlFunction(MysqlFunction.MAX, storeShiftMng.shiftCloseTime),
                                                            SqlUtils.sqlFunction(MysqlFunction.MAX, storeShiftMng.shiftBeforeCloseTime)
                                                        )
                                                    ),
                                                    "'00'"
                                                )
                                            )
                                        )
                                    )
                                ),
                                "NOW()"
                            )
                        ),
                        "1"
                    ),
                    "is_close"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(claimItem.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(purchaseStoreInfo, ObjectUtils.toArray(bundle.purchaseStoreInfoNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(storeShiftMng, ObjectUtils.toArray( purchaseStoreInfo.storeId, orderItem.storeType, purchaseStoreInfo.shiftId, purchaseStoreInfo.shipDt)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            )
            .GROUP_BY(claimMst.claimNo, claimMst.userNo, claimMst.claimPartYn, claimBundle.claimType, claimMst.purchaseOrderNo, claimBundle.bundleNo, claimBundle.claimBundleNo);

        log.debug("selectClaimInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimPickIsAutoInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        ItmPartnerSellerEntity itmPartnerSeller = EntityFactory.createEntityIntoValue(ItmPartnerSellerEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ItmPartnerSellerDeliveryEntity itmPartnerSellerDelivery = EntityFactory.createEntityIntoValue(ItmPartnerSellerDeliveryEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(shippingItem.purchaseOrderNo, shippingItem.bundleNo, "'M'"), "ship_type"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(itmPartnerSeller.returnDeliveryYn, "'N'")), "Y"),
                        SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.sqlFunction(MysqlFunction.IS_NULL, itmPartnerSellerDelivery.dlvCd)),
                        "'N'"
                    ),
                    itmPartnerSeller.returnDeliveryYn
                ),
                shippingItem.shipMethod,
                shippingItem.dlvCd
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingItem))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmPartnerSeller),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(shippingItem.partnerId), itmPartnerSeller)
                    )
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmPartnerSellerDelivery),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(shippingItem.partnerId, shippingItem.dlvCd), itmPartnerSellerDelivery)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo)
            )
            .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(shippingItem.shipNo)))
            .LIMIT(1)
            .OFFSET(0)
            ;
        log.debug("selectClaimPickIsAutoInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimSendDataInfo(@Param("claimNo") long claimNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CONCAT,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(claimItem.itemName)
                                .FROM(EntityFactory.getTableNameWithAlias(claimItem))
                                .WHERE(SqlUtils.equalColumn(claimItem.claimNo, claimBundle.claimNo))
                                .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(claimItem.claimItemNo)))
                                .LIMIT(1).OFFSET(0)
                        ),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.sqlFunction(MysqlFunction.COUNT, claimItem.claimItemNo), "1"),
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray("'외 '", SqlUtils.customOperator(CustomConstants.MINUS, SqlUtils.sqlFunction(MysqlFunction.COUNT, claimItem.claimItemNo), "1"), "' 건'")),
                                "''"
                            )
                        )
                    ),
                    "item_name"
                ),
                SqlUtils.subTableQuery(
                    new SQL()
                        .SELECT(
                            SqlUtils.customOperator(
                                CustomConstants.MINUS,
                                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO,claimPayment.paymentAmt),
                                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO,claimPayment.substitutionAmt)
                            )
                        ).FROM(EntityFactory.getTableNameWithAlias(claimPayment))
                        .WHERE(SqlUtils.equalColumn(claimPayment.claimNo, claimBundle.claimNo)),
                    claimPayment.paymentAmt
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimType, claimBundle.claimType),
//                SqlUtils.aliasToRow("'010-6556-7978'", claimBundle.shipMobileNo)
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.shipMobileNo, claimBundle.shipMobileNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
            )
            .GROUP_BY(claimBundle.claimNo);
        log.debug("selectClaimSendDataInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimSafetyIssueData(@Param("claimBundleNo") long claimBundleNo, @Param("issueType") String issueType) {
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class);
        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class);
        SQL query = new SQL();
        //수거일 경우
        if(issueType.equals("R")){
            query
                .SELECT(
                    SqlUtils.aliasToRow(claimPickShipping.pickMobileNo, "req_phone_no"),
                    SqlUtils.appendSingleQuoteWithAlias(issueType, "claim_type"),
                    claimPickShipping.claimBundleNo
                )
                .FROM(EntityFactory.getTableName(claimPickShipping))
                .WHERE(SqlUtils.equalColumnByInput(claimPickShipping.claimBundleNo, claimBundleNo));
        } else {
            query
                .SELECT(
                    SqlUtils.aliasToRow(claimExchShipping.exchMobileNo, "req_phone_no"),
                    SqlUtils.appendSingleQuoteWithAlias(issueType, "claim_type"),
                    claimExchShipping.claimBundleNo
                )
                .FROM(EntityFactory.getTableName(claimExchShipping))
                .WHERE(SqlUtils.equalColumnByInput(claimExchShipping.claimBundleNo, claimBundleNo));
        }
        log.debug("selectClaimSafetyIssueData \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimRegDataInfo(@Param("claimNo") long claimNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimBundle.paymentNo,
                claimBundle.purchaseOrderNo,
                claimBundle.claimNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .WHERE(SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo))
            .GROUP_BY(claimBundle.paymentNo, claimBundle.purchaseOrderNo, claimBundle.claimNo);
        log.debug("selectClaimRegDataInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimOrderTypeCheck(@Param("claimNo") long claimNo){
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                purchaseOrder.orderType
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo)
            );
        log.debug("selectClaimOrderTypeCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOriginAndCombineClaimCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("searchType") String searchType) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.cmbnYn,
                orderItem.orderItemNo,
                orderItem.bundleNo,
                orderItem.itemNo,
                SqlUtils.aliasToRow(orderItem.itemNm1, "item_nm"),
                orderItem.orgItemNo,
                orderItem.itemQty,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.AND(
                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                        ),
                        claimItem.claimItemQty,
                        "0"
                    ),
                    claimItem.claimItemQty
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C3'", "'C4'", "'C9'")),
                        claimItem.claimItemQty,
                        "0"
                    ),
                    "process_item_qty"
                ),
                orderItem.itemPrice,
                shippingItem.shipNo,
                orderItem.saleUnit
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.orderItemNo, orderItem.bundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, bundle.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, orderItem.orderItemNo)))
            .ORDER_BY(SqlUtils.orderByesc(orderItem.orderItemNo));

        switch (searchType) {
          case "COMBINE" :
                innerQuery.WHERE(
                    SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo),
                    SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE")
                );
                break;
            default:
                innerQuery.WHERE(
                    SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
                );
                break;
        }

        SQL query = new SQL()
            .SELECT(
                "purchase_order_no",
                "order_item_no",
                SqlUtils.sqlFunction(MysqlFunction.MAX, "cmbn_yn", "cmbn_yn"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "bundle_no", "bundle_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_no", "item_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_nm", "item_nm"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "org_item_no", "org_item_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_qty", "order_item_qty"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "claim_item_qty", "claim_item_qty"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "process_item_qty", "process_item_qty"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_price", "item_price"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "ship_no", "ship_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "sale_unit", "sale_unit")
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "itemList"))
            .GROUP_BY("itemList.purchase_order_no", "itemList.order_item_no");

        log.debug("selectOriginAndCombineClaimCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPreRefundRecheckData(@Param("claimNo") long claimNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        ItmMngCodeEntity itmMngCode = EntityFactory.createEntityIntoValue(ItmMngCodeEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                claimMst.purchaseOrderNo,
                claimMst.whoReason,
                SqlUtils.aliasToRow("imc.ref_3", "cancel_type"),
                claimMst.claimPartYn,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(claimReq.deductPromoYn, "'N'"), "pi_deduct_promo_yn"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(claimReq.deductDiscountYn, "'N'"), "pi_deduct_discount_yn"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(claimReq.deductShipYn, "'N'"), "pi_deduct_ship_yn"),
                SqlUtils.aliasToRow(claimBundle.claimShipFeeEnclose, "enclose_yn")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimReq, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmMngCode),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(itmMngCode.gmcCd, "claim_reason_type"),
                        SqlUtils.equalColumn(itmMngCode.mcCd, claimReq.claimReasonType)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            )
            .LIMIT(1)
            .OFFSET(0)
            ;
        log.debug("selectPreRefundRecheckData \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPreRefundItemData(@Param("claimNo") long claimNo) {
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimItem.itemNo,
                claimOpt.orderOptNo,
                SqlUtils.aliasToRow(claimOpt.claimOptQty, "claim_qty"),
                claimItem.orderItemNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimItem))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimNo, claimItem.claimItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimItem.claimNo, claimNo)
            );
        log.debug("selectPreRefundItemData \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPreRefundCompareData(@Param("claimNo") long claimNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class);
        SQL query = new SQL()
            .SELECT(
                claimMst.claimNo,
                claimMst.completeAmt,
                claimMst.totItemPrice,
                claimMst.totShipPrice,
                claimMst.totIslandShipPrice,
                claimMst.totAddIslandShipPrice,
                claimMst.totDiscountAmt,
                claimMst.totItemDiscountAmt,
                claimMst.totShipDiscountAmt,
                claimMst.totEmpDiscountAmt,
                claimMst.totCartDiscountAmt,
                claimMst.totPromoDiscountAmt,
                claimMst.totCardDiscountAmt,
                claimMst.pgAmt,
                claimMst.mileageAmt,
                claimMst.mhcAmt,
                claimMst.dgvAmt,
                claimMst.ocbAmt
            )
            .FROM(EntityFactory.getTableName(claimMst))
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimMst.oosYn, "N")
            );
        log.debug("selectPreRefundCompareData \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 환불예정금액 다건 조회 - call procedure
     *
     * @param parameterMap 조회 파라미터
     * @return 환불예정금액
     */
    public static String callByClaimPreRefundReCheckCalculation(LinkedHashMap<String, Object> parameterMap){
        log.info("환불예정금 재조회 :: \n[{}]", SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_RECHECK, parameterMap.values().toArray()));
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_RECHECK, parameterMap.values().toArray());
    }

    public static String updateClaimMstPaymentAmt(@Param("claimNo") long claimNo, @Param("column") String column, @Param("amt") long amt) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimMst))
            .SET(
                SqlUtils.equalColumnByInput(SqlUtils.convertCamelCaseToSnakeCase(column), amt)
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            );
        log.debug("updateClaimMstPaymentAmt \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateClaimPaymentAmt(@Param("claimNo") long claimNo, @Param("paymentType") String paymentType, @Param("paymentAmt") long paymentAmt) {
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimPayment))
            .SET(
                SqlUtils.equalColumnByInput(claimPayment.paymentAmt, paymentAmt)
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.paymentType, paymentType),
                SqlUtils.equalColumnByInput(claimPayment.claimNo, claimNo)
            );
        log.debug("updateClaimPaymentAmt \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimBundleTdInfo(@Param("claimNo") long claimNo){
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimBundle.bundleNo,
                claimBundle.purchaseOrderNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimBundle.orderCategory, "TD")
            );
        log.debug("selectClaimBundleTdInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectMileagePaybackCheck(@Param("bundleNo") long bundleNo) {
        ShippingMileagePaybackEntity shippingMileagePayback = EntityFactory.createEntityIntoValue(ShippingMileagePaybackEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.orgPurchaseOrderNo,
                shippingMileagePayback.orgBundleNo,
                shippingMileagePayback.bundleNo,
                shippingMileagePayback.paybackNo,
                shippingMileagePayback.storeType,
                shippingMileagePayback.userNo,
                shippingMileagePayback.cancelYn,
                shippingMileagePayback.resultYn,
                SqlUtils.aliasToRow(shippingMileagePayback.shipPricePayback, "return_amt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingMileagePayback))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(shippingMileagePayback.orgPurchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(payment.paymentStatus, "P3"))))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingMileagePayback.cancelYn, "N"),
                SqlUtils.OR(
                    Boolean.TRUE,
                    SqlUtils.equalColumnByInput(shippingMileagePayback.bundleNo, bundleNo),
                    SqlUtils.equalColumnByInput(shippingMileagePayback.orgBundleNo, bundleNo)
                )
            );
        log.debug("selectMileagePaybackCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileagePaybackInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.SUM, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(orderPaymentDivide.pgDivideAmt, orderPaymentDivide.mileageDivideAmt, orderPaymentDivide.mhcDivideAmt, orderPaymentDivide.ocbDivideAmt, orderPaymentDivide.dgvDivideAmt)), "payment_amt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.freeCondition, bundle.freeCondition)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderPaymentDivide, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderPaymentDivide.bundleNo, orderPaymentDivide.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(payment.paymentStatus, "P3"))))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo)))
            .WHERE(
                SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")),
                SqlUtils.OR(
                    Boolean.TRUE,
                    SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                    SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo)
                )
            );
        log.debug("selectMileagePaybackInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileagePaybackCancelInfo(@Param("bundleNo") long bundleNo, @Param("orgBundleNo") long orgBundleNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.SUM, SqlUtils.customOperator(CustomConstants.MINUS, claimPayment.paymentAmt, claimPayment.substitutionAmt)),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totShipPrice),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totIslandShipPrice)
                    ),
                    "cancel_amt"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimBundle.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimPayment.claimNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                SqlUtils.OR(
                    Boolean.TRUE,
                    SqlUtils.equalColumnByInput(claimBundle.bundleNo, bundleNo),
                    SqlUtils.equalColumnByInput(claimBundle.bundleNo, orgBundleNo)
                )
            );
        log.debug("selectMileagePaybackCancelInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateMileagePaybackResult(@Param("paybackNo") long paybackNo) {
        ShippingMileagePaybackEntity shippingMileagePayback = EntityFactory.createEntityIntoValue(ShippingMileagePaybackEntity.class);
        SQL query  = new SQL()
            .UPDATE(EntityFactory.getTableName(shippingMileagePayback))
            .SET(
                SqlUtils.equalColumnByInput(shippingMileagePayback.cancelYn, "Y"),
                SqlUtils.equalColumnByInput(shippingMileagePayback.chgId, "SYSTEM"),
                SqlUtils.equalColumn(shippingMileagePayback.chgDt, "NOW()")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingMileagePayback.paybackNo, paybackNo)
            );
        log.debug("updateMileagePaybackResult \n[{}]", query.toString());
        return query.toString();
    }


    public static String selectMultiBundleInfoForClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        MultiBundleEntity multiBundle = EntityFactory.createEntityIntoValue(MultiBundleEntity.class, Boolean.TRUE);
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                multiBundle.multiBundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(multiBundle))
            .INNER_JOIN(SqlUtils.createJoinCondition(multiOrderItem, ObjectUtils.toArray(multiBundle.multiBundleNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(multiBundle.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(multiBundle.bundleNo, bundleNo)
            )
            .GROUP_BY(multiBundle.multiBundleNo)
            .ORDER_BY(multiBundle.multiBundleNo)
            ;
        log.debug("selectMultiBundleInfoForClaim \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimMultiItemCreateInfo(@Param("claimNo") long claimNo, @Param("multiBundleNo") long multiBundleNo) {
        MultiBundleEntity multiBundle = EntityFactory.createEntityIntoValue(MultiBundleEntity.class, Boolean.TRUE);
        MultiOrderItemEntity multiOrderItem = EntityFactory.createEntityIntoValue(MultiOrderItemEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimItem.claimItemNo,
                claimItem.claimNo,
                claimItem.claimBundleNo,
                multiBundle.multiBundleNo,
                claimItem.orderItemNo,
                multiOrderItem.multiOrderItemNo,
                multiOrderItem.itemPrice,
                SqlUtils.aliasToRow(multiOrderItem.itemQty, "claim_item_qty")
            )
            .FROM(EntityFactory.getTableNameWithAlias(multiBundle))
            .INNER_JOIN(SqlUtils.createJoinCondition(multiOrderItem, ObjectUtils.toArray(multiBundle.multiBundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(multiOrderItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimItem.claimNo, claimNo),
                SqlUtils.equalColumnByInput(multiBundle.multiBundleNo, multiBundleNo)
            );
        log.debug("selectClaimMultiItemCreateInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectIsMarketOrderCheckForClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("claimNo") long claimNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.CASE_Y_N, SqlUtils.sqlFunction(MysqlFunction.IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_MARKET'", "'ORD_SUB_MARKET'")), "isMarketOrder")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))));

        if(purchaseOrderNo != 0) {
            query.WHERE(SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo));
        }

        if(claimNo != 0) {
            query
                .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
                .WHERE(SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo));
        }

        log.debug("selectIsMarketOrderCheckForClaim \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimBasicDataForMarketOrder(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderOptNo") long orderOptNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        MarketOrderEntity marketOrder = EntityFactory.createEntityIntoValue(MarketOrderEntity.class, Boolean.TRUE);
        MarketOrderItemEntity marketOrderItem = EntityFactory.createEntityIntoValue(MarketOrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.marketType,
                marketOrder.marketOrderNo,
                orderOpt.marketOrderItemNo,
                orderOpt.optQty
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(marketOrder, ObjectUtils.toArray(purchaseOrder.marketOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(marketOrderItem, ObjectUtils.toArray(marketOrderItem.marketOrderNo, marketOrder.marketOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo, marketOrderItem.marketOrderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderOpt.orderOptNo, orderOptNo)
            );
        log.debug("selectClaimBasicDataForMarketOrder \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimStatusChangeDataForMarketOrder(@Param("claimNo") long claimNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimBundle.purchaseOrderNo,
                claimBundle.claimType,
                claimBundle.claimStatus,
                claimOpt.orderOptNo,
                claimMst.marketType,
                claimReq.claimReasonType,
                claimReq.claimReasonDetail
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimReq, ObjectUtils.toArray(claimBundle.claimBundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimBundle.claimBundleNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo)
            );
        log.debug("selectClaimStatusChangeDataForMarketOrder \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMarketCollectedExchangeInfo(@Param("claimBundleNo") long claimBundleNo) {
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class);
        SQL query = new SQL()
            .SELECT(
                claimOpt.marketOrderItemNo
            )
            .FROM(EntityFactory.getTableName(claimOpt))
            .WHERE(
                SqlUtils.equalColumnByInput(claimOpt.claimBundleNo, claimBundleNo)
            );
        log.debug("selectMarketCollectedExchangeInfo \n[{}]", query.toString());
        return query.toString();
    }
}
