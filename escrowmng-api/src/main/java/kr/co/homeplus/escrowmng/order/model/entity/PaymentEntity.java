package kr.co.homeplus.escrowmng.order.model.entity;

public class PaymentEntity {

    // 결제번호
    public String paymentNo;

    // 결제상태(P1:결제요청,P2:입금대기중(무통장),P3:결제완료(입금확인),P4:결제실패,P5:결제철회(무통장,카드,계좌이체,휴대폰주문취소),P9:만료(무통장))
    public String paymentStatus;

    // 총결제금액
    public String totAmt;

    // PG결제금액
    public String pgAmt;

    // 마일리지결제금액
    public String mileageAmt;

    // MHC결제금액(홈플러스MHC포인트)
    public String mhcAmt;

    // DGV결제금액(홈플러스상품권)
    public String dgvAmt;

    // OCB결제금액(OkCashBag)
    public String ocbAmt;

    // 문화비소득공제여부(Y or N)
    public String cultureCostYn;

    // 결제완료일자(승인완료일자, 무통장은 입금완료일자)
    public String paymentFshDt;

    // 리턴메시지 (주문생성 사유 저장)
    public String returnMsg;

    // 리턴코드 (승인결과코드, 0000:정상, 그외 :비정상)
    public String returnCd;

    // 등록일시
    public String regDt;

    // 등록자ID
    public String regId;

    // 수정일시
    public String chgDt;

    // 수정자ID
    public String chgId;

}
