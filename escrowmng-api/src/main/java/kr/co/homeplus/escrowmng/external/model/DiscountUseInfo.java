package kr.co.homeplus.escrowmng.external.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "할인(카드상품할인)사용현황 정보")
public class DiscountUseInfo {

    @ApiModelProperty(value = "회원번호", position = 1)
    private long userNo;
    @ApiModelProperty(value = "회원명(마스킹)", position = 2)
    private String userNm;
    @ApiModelProperty(value = "주문번호", position = 3)
    private long purchaseOrderNo;
    @ApiModelProperty(value = "할인종류(card_discount:카드상품할인)", position = 4)
    private String discountKind;
    @ApiModelProperty(value = "사용금액(적용금액)", position = 5)
    private long discountAmt;
    @ApiModelProperty(value = "사용일", position = 6)
    private String orderDt;

}
