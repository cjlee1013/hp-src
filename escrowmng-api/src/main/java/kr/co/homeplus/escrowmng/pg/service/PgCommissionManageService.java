package kr.co.homeplus.escrowmng.pg.service;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.construct.TransactionManagerName;
import kr.co.homeplus.escrowmng.pg.mapper.PgCommissionManageMasterMapper;
import kr.co.homeplus.escrowmng.pg.mapper.PgCommissionManageSlaveMapper;
import kr.co.homeplus.escrowmng.pg.model.PgCommissionManageDto;
import kr.co.homeplus.escrowmng.pg.model.PgCommissionManageSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PgCommissionTargetDto;
import kr.co.homeplus.escrowmng.pg.model.PgCommissionTargetSelectDto;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class PgCommissionManageService {
    private final PgCommissionManageSlaveMapper pgCommissionManageSlaveMapper;
    private final PgCommissionManageMasterMapper pgCommissionManageMasterMapper;

    /**
     * PG 수수료 마스터 조회
     * @param pgCommissionManageSelectDto
     * @return
     * @throws Exception
     */
    public List<PgCommissionManageDto> getPgCommissionMngList(PgCommissionManageSelectDto pgCommissionManageSelectDto) throws Exception {
        return pgCommissionManageSlaveMapper.selectPgCommissionMngList(pgCommissionManageSelectDto);
    }

    /**
     * PG 수수료 대상 조회
     * @param pgCommissionTargetSelectDto
     * @return
     * @throws Exception
     */
    public List<PgCommissionTargetDto> getPgCommissionTargetList(PgCommissionTargetSelectDto pgCommissionTargetSelectDto) throws Exception {
        return pgCommissionManageSlaveMapper.selectPgCommissionTargetList(pgCommissionTargetSelectDto);
    }

    /**
     * 중복 수수료 정책 조회
     * @param pgCommissionManageSelectDto
     * @return
     * @throws Exception
     */
    public int checkDuplicateCommission(PgCommissionManageSelectDto pgCommissionManageSelectDto) throws Exception {
        return pgCommissionManageSlaveMapper.checkDuplicateCommission(pgCommissionManageSelectDto);
    }

    /**
     * PG 수수료 저장
     * @param pgCommissionManageDto
     * @return
     * @throws Exception
     */
    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int savePgCommission(PgCommissionManageDto pgCommissionManageDto) throws Exception {
        int rtnVal = 0;

        try {
            // PG 수수료 마스터 저장
            rtnVal = pgCommissionManageMasterMapper.insertPgCommissionMngList(pgCommissionManageDto);

            // PG 수수료 대상 저장
            if (EscrowString.isNotEmpty(pgCommissionManageDto.getPgCommissionTargetDtoList())) {
                // 신규 저장 시 마스터 키값을 세팅하여 대상 저장, 업데이트 시 화면에서 마스터 키값 같이 넘겨줌.
                for (PgCommissionTargetDto target : pgCommissionManageDto.getPgCommissionTargetDtoList()) {
                    if (EscrowString.isEmpty(target.getPgCommissionMngSeq())) {
                        target.setPgCommissionMngSeq(pgCommissionManageDto.getPgCommissionMngSeq());
                    }
                    target.setRegId(pgCommissionManageDto.getRegId());
                    target.setChgId(pgCommissionManageDto.getChgId());
                    rtnVal = pgCommissionManageMasterMapper.insertPgCommissionTargetList(target);
                }
            }
            return rtnVal;
        } catch (Exception e) {
            log.error("error:", e);
            rtnVal = -1;
            return rtnVal;
        } finally {
            log.info("savePgCommission-resultCount:" + rtnVal);
        }
    }
}
