package kr.co.homeplus.escrowmng.claim.service.outOfStock;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.OutOfStockItemListGetDto;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.OutOfStockItemListSetDto;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.StoreShiftListSetDto;
import kr.co.homeplus.escrowmng.enums.OrderCode;
import kr.co.homeplus.escrowmng.order.factory.OrderResponseFactory;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class OutOfStockItemService {

    private final OutOfStockItemMapperService outOfStockItemMapperService;

    /**
     * 결품/대체상품 조회
     *
     * @param outOfStockItemListSetDto
     * @return 결품/대체상품 조회
     */
    public ResponseObject<List<OutOfStockItemListGetDto>> getOutOfStockItemList(OutOfStockItemListSetDto outOfStockItemListSetDto) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, outOfStockItemMapperService.getOutOfStockItemList(outOfStockItemListSetDto));
    }

    /**
     * 결품/대체상품 조회
     *
     * @param storeShiftListSetDto
     * @return 결품/대체상품 조회
     */
    public ResponseObject<List<String>> getStoreShiftList(StoreShiftListSetDto storeShiftListSetDto) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, outOfStockItemMapperService.getStoreShiftList(storeShiftListSetDto));
    }
}
