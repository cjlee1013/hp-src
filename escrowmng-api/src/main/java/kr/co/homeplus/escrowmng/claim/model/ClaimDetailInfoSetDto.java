package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ClaimDetailInfoSetDto {

  @ApiModelProperty(value = "그룹클레임번호", position = 1)
  public String claimBundleNo;

  @ApiModelProperty(value = "클레임번호", position = 2)
  public long claimNo;

  @ApiModelProperty(value = "주문번호", position = 3)
  public String purchaseOrderNo;

  @ApiModelProperty(value = "클레임 타입", position = 4)
  public String claimType;
}
