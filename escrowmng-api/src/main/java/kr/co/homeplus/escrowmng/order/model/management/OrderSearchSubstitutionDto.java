package kr.co.homeplus.escrowmng.order.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "대체주문 정보 DTO")
public class OrderSearchSubstitutionDto {
    @ApiModelProperty(value = "대체 주문번호")
    private String purchaseOrderNo;
    @ApiModelProperty(value = "대체 상품번호")
    private String itemNo;
    @ApiModelProperty(value = "대체 상품명")
    private String itemNm1;
    @ApiModelProperty(value = "수량")
    private String itemQty;
    @ApiModelProperty(value = "상품금액")
    private String orderPrice;
    @ApiModelProperty(value = "대체주문일시")
    private String orderDt;
}
