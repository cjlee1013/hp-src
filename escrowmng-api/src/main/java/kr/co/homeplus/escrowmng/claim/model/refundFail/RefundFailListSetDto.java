package kr.co.homeplus.escrowmng.claim.model.refundFail;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@ApiModel(description = "환불실패대상 리스트 조회 DTO")
public class RefundFailListSetDto {


    @ApiModelProperty(value = "검색기간", position = 1)
    private String refundDateType;

    @ApiModelProperty(value = "조회시작일", position = 2)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", position = 3)
    private String schEndDt;

    @ApiModelProperty(value = "검색어 타입", position = 4)
    private String schClaimSearchType;

    @ApiModelProperty(value = "검색어", position = 5)
    private String schClaimSearch;

    @ApiModelProperty(value = "회원/비회원 여부", position = 6)
    private String schUserYn;

    @ApiModelProperty(value = "비회원번호 검색조건", position = 7)
    private String schBuyerInfo;

    @ApiModelProperty(value = "회원/비회원 검색 값", position = 8)
    private String schUserSearch;

    @ApiModelProperty(value = "처리상태", position = 9)
    private List<String> paymentStatus;

    @ApiModelProperty(value = "주결제수단", position = 10)
    private List<String> parentMethodCd;

}
