package kr.co.homeplus.escrowmng.order.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문정보 상세 - 구매자정보 DTO")
public class OrderSearchProdDto {
    @ApiModelProperty(value = "주문번호")
    private String purchaseOrderNo;
    @ApiModelProperty(value = "결제상태")
    private String paymentStatus;
    @ApiModelProperty(value = "결제상태명")
    private String paymentStatusNm;
    @ApiModelProperty(value = "주문일시")
    private String orderDt;
    @ApiModelProperty(value = "결제일시")
    private String paymentFshDt;
    @ApiModelProperty(value = "주문디바이스")
    private String orderDevice;
    @ApiModelProperty(value = "마켓연동")
    private String marketType;
    @ApiModelProperty(value = "회원번호")
    private String userNo;
    @ApiModelProperty(value = "구매자")
    private String buyerNm;
    @ApiModelProperty(value = "구매자아이디(이메일)")
    private String buyerEmail;
    @ApiModelProperty(value = "구매자연락처")
    private String buyerMobileNo;
    @ApiModelProperty(value = "합배송주문번호")
    private String orgPurchaseOrderNo;
    @ApiModelProperty(value = "마켓주문번호")
    private String marketOrderNo;
    @ApiModelProperty(value = "주문타입")
    private String orderType;
    @ApiModelProperty(value = "선물수령인")
    private String receiveGiftNm;
    @ApiModelProperty(value = "수령인연락처")
    private String receiveGiftPhone;
    @ApiModelProperty(value = "선물수락기한")
    private String dsGiftAutoCancelDt;
    @ApiModelProperty(value = "선물수락일시")
    private String giftAcpDt;
    @ApiModelProperty(value = "비회원여부")
    private String nomemOrderYn;
    @ApiModelProperty(value = "법인주문여부")
    private String corpOrderYn;
    @ApiModelProperty(value = "제휴채널ID")
    private String affiliateCd;
}
