package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단관리 > 청구할인 혜택 관리 DTO")
public class ChargeDiscountBenefitManageDto {

    @ApiModelProperty(notes = "청구할인혜택관리번호", hidden = true)
    private long chargeDiscountBenefitMngNo;

    @ApiModelProperty(notes = "사이트유형(HOME:홈플러스,CLUB:더클럽)", required = true, position = 1)
    @NotEmpty(message="사이트")
    @Pattern(regexp = "(HOME|CLUB)", message="사이트")
    private String siteType;

    @ApiModelProperty(notes = "수단코드(신용카드 리스트)", required = true, position = 2)
    @NotEmpty(message="카드구분")
    private String methodCd;

    @ApiModelProperty(notes = "수단명(신용카드 리스트)", required = true, position = 2)
    private String methodNm;

    @ApiModelProperty(notes = "청구할인명", required = true, position = 3)
    @NotEmpty(message="청구할인명")
    private String chargeDiscountNm;

    @ApiModelProperty(notes = "적용시작일자", required = true, position = 4)
    private String applyStartDt;

    @ApiModelProperty(notes = "적용종료일자", required = true, position = 5)
    private String applyEndDt;

    @ApiModelProperty(notes = "할인유형(PRICE:정액, RATE:정률)", required = true, position = 6)
    @NotEmpty(message="할인방법")
    private String discountType;

    @ApiModelProperty(notes = "할인가격", position = 7)
    @Max(value = 99999999, message = "할인가격")
    private int discountPrice;

    @ApiModelProperty(notes = "할인율", position = 8)
    @Max(value = 99, message = "할인율")
    private int discountRate;

    @ApiModelProperty(notes = "할인최대가격", position = 9)
    @Max(value = 99999999, message = "최대할인금액")
    private int discountMaxPrice;

    @ApiModelProperty(notes = "할인최소가격(최소구매금액)", required = true, position = 10)
    @Min(value = 1, message = "최소구매금액")
    @Max(value = 99999999, message = "최소구매금액")
    private int purchaseMinPrice;

    @ApiModelProperty(notes = "사용여부", required = true, position = 11)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message="사용여부")
    private String useYn;

    @ApiModelProperty(notes = "등록자ID", hidden = true)
    private String regId;
    @ApiModelProperty(notes = "등록일자", hidden = true)
    private String regDt;
    @ApiModelProperty(notes = "수정자ID", hidden = true)
    private String chgId;
    @ApiModelProperty(notes = "수정일자", hidden = true)
    private String chgDt;

}
