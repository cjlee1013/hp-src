package kr.co.homeplus.escrowmng.utils;

import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;
import javax.annotation.PostConstruct;
import kr.co.homeplus.escrowmng.claim.enums.ClaimMessageInfo;
import kr.co.homeplus.escrowmng.claim.model.external.message.MessageSendTalkGetDto;
import kr.co.homeplus.escrowmng.claim.model.external.message.MessageSendTalkSetDto;
import kr.co.homeplus.escrowmng.claim.model.external.message.MessageSendTalkSetDto.MessageSendBodyDto;
import kr.co.homeplus.escrowmng.claim.service.ClaimMessageService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class MessageSendUtil {
    private final ClaimMessageService messageService;
    private static ClaimMessageService sendService;
    private final ResourceClient resourceClient;
    private static ResourceClient messageClient;

    @PostConstruct
    private void initialize() {
        sendService = messageService;
        messageClient = resourceClient;
    }

    public static boolean sendClaimCancel(long claimNo){
        LinkedHashMap<String, Object> messageMap = getSendMessageInfo(claimNo);
        if(messageMap == null){
            return Boolean.FALSE;
        }
        if(ObjectUtils.isEquals(messageMap.get("order_gift_yn"), "Y")) {
            messageMap.putAll(sendService.getPresentAutoCancelInfo(claimNo));
            messageMap.put("ship_mobile_no", messageMap.get("sender_mobile_no"));
        }
        return sendMessageTalk(ClaimMessageInfo.CLAIM_MESSAGE_HOME_C_ORDER_COMPLETE, messageMap);
    }

    public static boolean sendExchangeShipInfo(long claimNo){
        LinkedHashMap<String, Object> messageMap = getSendMessageInfo(claimNo);
        if(messageMap == null){
            return Boolean.FALSE;
        }
        if(!ObjectUtils.isEquals("X", messageMap.get("claim_type"))){
            return Boolean.FALSE;
        }
        return sendMessageTalk(ClaimMessageInfo.CLAIM_MESSAGE_HOME_X_SHIP_START_DS, getSendMessageInfo(claimNo));
    }

    public static boolean sendClaimCompleteWithoutExchange(long claimNo) {
        LinkedHashMap<String, Object> messageMap = getSendMessageInfo(claimNo);
        if(messageMap == null){
            return Boolean.FALSE;
        }
        if(!ObjectUtils.isEquals("X", messageMap.get("claim_type")) && ObjectUtils.isEquals("C3", messageMap.get("claim_status"))){
            return sendMessageTalk(ClaimMessageInfo.CLAIM_MESSAGE_HOME_CR_COMPLETE, getSendMessageInfo(claimNo));
        }
        return Boolean.FALSE;
    }

    public static boolean sendExchangeClaimComplete(long claimNo) {
        LinkedHashMap<String, Object> messageMap = getSendMessageInfo(claimNo);
        if(messageMap == null){
            return Boolean.FALSE;
        }
        if(!ObjectUtils.isEquals("X", messageMap.get("claim_type")) || !ObjectUtils.isEquals("C3", messageMap.get("claim_status"))){
            return Boolean.FALSE;
        }
        return sendMessageTalk(ClaimMessageInfo.CLAIM_MESSAGE_HOME_X_COMPLETE, getSendMessageInfo(claimNo));
    }

    public static boolean sendClaimApplication(long claimNo) {
        LinkedHashMap<String, Object> messageMap = getSendMessageInfo(claimNo);
        if(messageMap == null){
            return Boolean.FALSE;
        }
        ClaimMessageInfo choiceMessageTemplate;
        switch (messageMap.get("claim_type").toString()){
            case "C" :
                // 취소신청 : T11102
                if("C001".equals(ObjectUtils.toString(messageMap.get("claim_reason_type"))) && "TD_DRCT,TD_PICK,TD_QUICK".contains(ObjectUtils.toString(messageMap.get("ship_method")))){
                    choiceMessageTemplate = ClaimMessageInfo.CLAIM_MESSAGE_HOME_OOS_NOT_SUB;
                } else {
                    choiceMessageTemplate = ClaimMessageInfo.CLAIM_MESSAGE_HOME_C_ORDER_APPLICATION;
                }
                break;
            case "R" :
            case "X" :
                // TD : T11103(비동봉), T11104(동봉), T11105(PICK)
                if(ObjectUtils.isEqualsIgnoreCase("TD", messageMap.get("ship_type"))){
                    choiceMessageTemplate = ClaimMessageInfo.CLAIM_MESSAGE_HOME_RX_ORDER_APPLICATION_TD;
                    if(ObjectUtils.isEquals(messageMap.get("enclose_yn"), "Y") && ObjectUtils.toLong(messageMap.get("claim_ship_amt")) > 0){
                        choiceMessageTemplate = ClaimMessageInfo.CLAIM_MESSAGE_HOME_RX_ORDER_APPLICATION_TD_ENCLOSE;
                    }
                    if(ObjectUtils.isEqualsIgnoreCase("TD_PICK", messageMap.get("ship_method"))){
                        choiceMessageTemplate = ClaimMessageInfo.CLAIM_MESSAGE_HOME_RX_ORDER_APPLICATION_PICK;
                    }
                } else {
                    // DS : T11106(비동봉), T11107(동봉)
                    choiceMessageTemplate = ClaimMessageInfo.CLAIM_MESSAGE_HOME_RX_ORDER_APPLICATION_DS;
                    if(ObjectUtils.isEquals(messageMap.get("enclose_yn"), "Y") && ObjectUtils.toLong(messageMap.get("claim_ship_amt")) > 0){
                        choiceMessageTemplate = ClaimMessageInfo.CLAIM_MESSAGE_HOME_RX_ORDER_APPLICATION_DS_ENCLOSE;
                    }
                }
                break;
            default:
                return Boolean.FALSE;
        }

        return sendMessageTalk(choiceMessageTemplate, messageMap);
    }

    public static boolean sendOmniOutOfStock(long claimNo, ClaimMessageInfo messageInfo) {
        return sendMessageTalk(messageInfo, getSendMessageInfo(claimNo));
    }

    public static boolean sendOmniSubstitution(long claimNo) {
        LinkedHashMap<String, Object> messageMap = getSendMessageInfo(claimNo);
        if(messageMap == null){
            return Boolean.FALSE;
        }
        try {
            messageMap.putAll(sendService.getOrderSubstitutionMessageInfo(claimNo));
            if(ObjectUtils.toLong(messageMap.get("refund_amt")) > 0){
                return sendMessageTalk(ClaimMessageInfo.CLAIM_MESSAGE_HOME_SUBSTITUTION_PRICE, messageMap);
            } else {
                return sendMessageTalk(ClaimMessageInfo.CLAIM_MESSAGE_HOME_SUBSTITUTION_NOT_PRICE, messageMap);
            }
        } catch (Exception e){
            log.error("클레임그룹번호({}) 옴니 대체주문 조회 실패 :: {}", claimNo, e.getMessage());
        }
        return Boolean.FALSE;
    }

    public static boolean sendPresentAutoCancel(long claimNo) {
        LinkedHashMap<String, Object> messageMap = getSendMessageInfo(claimNo);
        if(messageMap == null){
            return Boolean.FALSE;
        }
        try {
            messageMap.putAll(sendService.getPresentAutoCancelInfo(claimNo));
            // 발송 전화번호 교체가 필요함.
            messageMap.put("ship_mobile_no", messageMap.get("sender_mobile_no"));
            return sendMessageTalk(ClaimMessageInfo.CLAIM_MESSAGE_HOME_AUTO_CANCEL_SENDER, messageMap);
        } catch (Exception e) {
            log.error("클레임그룹번호({}) 선물하기 취소 정보 조회 실패 ::: {}", claimNo, e.getMessage());
        }
        return Boolean.FALSE;
    }

    public static boolean sendNoRcvCancelRequest(LinkedHashMap<String, Object> messageMap) {
        return sendMessageTalk(ClaimMessageInfo.CLAIM_MESSAGE_HOME_NO_RCV_CANCEL_REQUEST, messageMap);
    }

    private static boolean sendMessageTalk(ClaimMessageInfo messageInfo, LinkedHashMap<String, Object> messageMap){
        messageInfo = getChoiceClaimMessageInfo(messageInfo, ObjectUtils.toString(messageMap.get("site_type")));
        if(messageInfo == null){
            return Boolean.FALSE;
        }
        MessageSendTalkSetDto setDto = createMessageSendAlimTalkDto(messageInfo, messageMap);
        log.info("알림톡 발송 정보 :: {}", setDto);
        try{
            ResponseObject<MessageSendTalkGetDto> responseObject =
                messageClient.postForResponseObject(
                "message",
                setDto,
                    "/send/alimtalk/switch",
                new ParameterizedTypeReference<ResponseObject<MessageSendTalkGetDto>>() {}
            );
            log.info("알림톡 발송결과 :: {} :: {} :: {}", responseObject.getReturnCode(), responseObject.getReturnMessage(), responseObject.getData());
        } catch (Exception e) {
            log.error("Claim Completed Send Message Error ::: {}", e.getMessage());
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    private static LinkedHashMap<String, String> getConvertParameterMap(ClaimMessageInfo messageInfo, LinkedHashMap<String, Object> dataMap) {
        LinkedHashMap<String, String> returnMap = new LinkedHashMap<>();
        String[] sendParams = messageInfo.getSendParams().split("\\|");
        String[] mappingParams = messageInfo.getMappingParams().split("\\|");
        for(int idx = 0; idx < mappingParams.length; idx++){
            if(dataMap.get(mappingParams[idx]) != null){
                returnMap.put(sendParams[idx], getConvertParam(mappingParams[idx], dataMap.get(mappingParams[idx]).toString()));
            }
        }
        return returnMap;
    }

    private static MessageSendTalkSetDto createMessageSendAlimTalkDto(ClaimMessageInfo messageInfo, LinkedHashMap<String, Object> messageMap) {
        return MessageSendTalkSetDto.builder()
            .workName(messageInfo.getSendTitle())
            .description(messageInfo.getSendTitle())
            .regId("SYSTEM")
            .templateCode(messageInfo.getTemplateCode())
            .switchTemplateCode(messageInfo.getSwitchingTemplateCode())
            .bodyArgument(
                new ArrayList<>(){{
                    add(MessageSendBodyDto.builder()
                        .identifier(ObjectUtils.toString(messageMap.get("claim_no")))
                        .toToken(ObjectUtils.toString(messageMap.get("buyer_mobile_no")))
                        .mappingData(getConvertParameterMap(messageInfo, messageMap))
                        .build());
                }}
            )
            .build();
    }

    private static LinkedHashMap<String, Object> getSendMessageInfo(long claimNo) {
        try{
            LinkedHashMap<String, Object> messageMap = sendService.getClaimBasicMessageInfo(claimNo);
            messageMap.putAll(sendService.getClaimPaymentMessageInfo(claimNo));
            if(ObjectUtils.toString(messageMap.get("ship_type")).equals("TD")){
                messageMap.put("club_ship_info", MessageFormat.format("{0} ({1}) {2} ~ {3}", ObjectUtils.toArray(messageMap, "ship_dt", "day_of_week", "slot_ship_start_time", "slot_ship_end_time")));
                messageMap.put("ship_dt", MessageFormat.format("{0} ({1})", ObjectUtils.toArray(messageMap, "ship_dt", "day_of_week")));
                messageMap.put("slot_ship_time", MessageFormat.format("{0} ~ {1}", ObjectUtils.toArray(messageMap, "slot_ship_start_time", "slot_ship_end_time")));
                if(ObjectUtils.isEquals(messageMap.get("pick_ship_type"), "N")){
                    messageMap.put("club_ship_info", "수거안함");
                    messageMap.put("ship_dt", "수거안함");
                    messageMap.put("slot_ship_time", "");
                }else if(ObjectUtils.isEquals(messageMap.get("pick_ship_type"), "T")){
                    messageMap.put("club_ship_info", "고객센터 직접 반품");
                    messageMap.put("ship_dt", "고객센터 직접 반품");
                    messageMap.put("slot_ship_time", "");
                }
            }
            // 주문타입이 마켓이면 사이트타입을 마켓으로 변경처리.
            if("ORD_MARKET,ORD_SUB_MARKET".contains(ObjectUtils.toString(messageMap.get("order_type")))) {
                messageMap.put("site_type", "MARKET");
            }
            return messageMap;
        } catch (Exception e) {
            log.error("클레임그룹번호({}) 메시지 획득 실패 :: {}", claimNo, e.getMessage());
        }
        return null;
    }

    private static ClaimMessageInfo getChoiceClaimMessageInfo(ClaimMessageInfo before, String siteType) {
        try {
            if(siteType != null){
                String choiceEnum = before.name().replace("HOME", siteType);
                return ClaimMessageInfo.valueOf(choiceEnum);
            }
            return before;
        } catch (Exception e){
            return null;
        }
    }

    private static String getConvertParam(String key, String value) {
        if(key.contains("amt")){
            return NumberFormat.getInstance(Locale.KOREA).format(Long.parseLong(value));
        } else if(key.contains("claim_type")){
            return value.equals("C") ? "취소" : value.equals("R") ? "반품" : "교환";
        } else if(key.contains("qty")) {
            return String.valueOf(value).concat("개");
        } else {
            return value;
        }
    }
}
