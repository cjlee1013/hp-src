package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 배분율 관리 > 결제정책 관리-결제수단 DTO")
public class PaymentPolicyMethodDto {
    @ApiModelProperty(value = "결제수단순번", position = 1)
    private Long methodSeq;

    @ApiModelProperty(value = "정책번호", required = true, position = 2)
    @AllowInput(types = {PatternConstants.NUM}, message = "정책번호")
    @NotEmpty(message = "정책번호")
    private Long policyNo;

    @ApiModelProperty(value = "결제수단코드(신한카드,현대카드,무통장등)", required = true, position = 3)
    @NotEmpty(message = "결제수단코드")
    private String methodCd;

    @ApiModelProperty(value = "등록자id", required = true, position = 4)
    private String regId;

    @ApiModelProperty(value = "등록일자", position = 5)
    private String regDt;
}
