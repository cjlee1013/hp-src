package kr.co.homeplus.escrowmng.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "점포 애니타임 관리 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class StoreAnytimeManageDto {
    @ApiModelProperty(value = "점포유형", position = 1)
    private String storeType;

    @ApiModelProperty(value = "점포ID", position = 2)
    private int storeId;

    @ApiModelProperty(value = "점포명", position = 3)
    private String storeNm;

    @ApiModelProperty(value = "점포애니타임사용여부", position = 4)
    private String storeAnytimeUseYn;

    @ApiModelProperty(value = "등록일시", position = 5)
    private String regDt;

    @ApiModelProperty(value = "등록자ID", position = 6)
    private String regId;

    @ApiModelProperty(value = "수정일시", position = 7)
    private String chgDt;

    @ApiModelProperty(value = "수정자ID", position = 8)
    private String chgId;
}
