package kr.co.homeplus.escrowmng.enums.impl;

public interface EnumImpl {
    String getResponseCode();
    String getResponseMessage();
}