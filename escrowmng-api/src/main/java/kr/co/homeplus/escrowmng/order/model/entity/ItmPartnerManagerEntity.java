package kr.co.homeplus.escrowmng.order.model.entity;

public class ItmPartnerManagerEntity {
    //
    public String seq;
    //
    public String partnerId;
    //
    public String mngCd;
    //
    public String mngNm;
    //
    public String mngMobile;
    //
    public String mngPhone;
    //
    public String mngEmail;
    //
    public String useYn;
    //
    public String regDt;
    //
    public String regId;
    //
    public String chgDt;
    //
    public String chgId;
}
