package kr.co.homeplus.escrowmng.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ExternalInfo {
    PAYMENT_CANCEL_PG("payment", "/pg/pgCancel"),
    PAYMENT_CANCEL_MILEAGE("mileage", "/cancel/cancelMileageRestore"),
    PAYMENT_SAVE_CANCEL_MILEAGE("mileage", "/external/external/mileageCancel"),
    PAYMENT_CANCEL_OCB("payment", "/ocb/cancelOcbPoint"),
    PAYMENT_CANCEL_MHC_ALL("escrow", "/mhc/external/cancelMhcPoint"),
    PAYMENT_CANCEL_MHC_PART("escrow", "/mhc/external/partCancelMhcPoint"),
    PAYMENT_SAVE_CANCEL_MHC_ALL("escrow", "/mhc/external/saveCancelMhcPoint"),
    PAYMENT_SAVE_CANCEL_MHC_PART("escrow", "/mhc/external/partSaveCancelMhcPoint"),
    PAYMENT_CANCEL_DGV("payment", "/dgv/cancelDgvGiftCard"),
    PAYMENT_CANCEL_DGV_RECEIPT("escrow", "/dgv/external/dgvPosReceiptIssue"),
    RESTORE_SLOT_CLAIM("escrow", "/slot/external/restoreSlotStockForClaim"),
    USER_MHC_CARD_INFO("user", "/search/basic/getUserInfo"),
    SHIPPING_CLAIM_FROM_SHIP_COMPLETE("shipping", "/admin/shipManage/setShipCompleteByBundle"),
    SHIPPING_CLAIM_FROM_SHIP_DECISION("shipping", "/admin/shipManage/setPurchaseCompleteByBundle"),
    EMP_DISCOUNT_LIMIT_AMT_INFO("escrow", "/ordinis/external/getEmpInfo"),
    CLAIM_BY_COUPON_RE_ISSUE("front","/coupon/escrow/setCouponReIssued"),
    CLAIM_SAFETY_ISSUE_ADD("escrow", "/safetyissue/addClaimSafetyNo"),
    CLAIM_SAFETY_ISSUE_MODIFY("escrow", "/safetyissue/updateClaimSafetyNo"),
    OUTBOUND_COOP_TICKET_CHECK_FOR_CLAIM("outbound", "/coop/cancelTicketInfo"),
    OUTBOUND_COOP_TICKET_CANCEL_FOR_CLAIM("outbound", "/coop/cancelTicket"),
    FORWARD_OMNI_TRANS("forward", "/claim/toOmni"),
    MESSAGE_SEND_ALIM_TALK("message", "/send/alimtalk"),
    ITEM_STORE_NM_SEARCH("item", "/item/store/getStore"),
    OUTBOUND_MARKET_NAVER_CANCEL_REG("outbound", "/market/naver/setCancelSale"),
    OUTBOUND_MARKET_NAVER_RETURN_REG("outbound", "/market/naver/setReturnSale"),
    OUTBOUND_MARKET_NAVER_EXCHANGE_COLLECT("outbound", "/market/naver/setExchangeCollect"),
    OUTBOUND_MARKET_NAVER_CLAIM_APPROVE("outbound", "/market/naver/setClaimApprove"),
    OUTBOUND_MARKET_NAVER_CLAIM_REJECT("outbound", "/market/naver/setClaimReject"),
    OUTBOUND_MARKET_NAVER_CLAIM_HOLD("outbound", "/market/naver/setClaimHold"),
    OUTBOUND_MARKET_NAVER_CLAIM_WITHDRAW("outbound", "/market/naver/setClaimWithdraw"),
    OUTBOUND_MARKET_ELEVEN_CANCEL_REG("outbound", "/market/eleven/setCancelSale"),

    ;

    @Getter
    private final String apiId;

    @Getter
    private final String uri;

}
