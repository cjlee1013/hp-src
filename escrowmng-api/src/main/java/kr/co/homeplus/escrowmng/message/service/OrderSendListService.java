package kr.co.homeplus.escrowmng.message.service;

import java.util.List;
import kr.co.homeplus.escrowmng.message.mapper.OrderSendListSlaveMapper;
import kr.co.homeplus.escrowmng.message.model.OrderSendListInfo;
import kr.co.homeplus.escrowmng.message.model.OrderSendListRequest;
import kr.co.homeplus.escrowmng.message.model.OrderSendListResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class OrderSendListService {

    private final OrderSendListSlaveMapper orderSendListSlaveMapper;

    public OrderSendListResponse getSendList(final OrderSendListRequest request) {
        log.debug("OrderSendListRequest : {}", request.toString());
        List<OrderSendListInfo> list = orderSendListSlaveMapper.selectOrderSendList(request);
        OrderSendListResponse orderSendListResponse = new OrderSendListResponse();
        orderSendListResponse.setList(list);
        return orderSendListResponse;
    }
}
