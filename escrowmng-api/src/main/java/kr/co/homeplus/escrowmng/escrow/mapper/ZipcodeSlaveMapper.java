package kr.co.homeplus.escrowmng.escrow.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.escrow.model.ZipcodeManageModel;
import kr.co.homeplus.escrowmng.escrow.model.ZipcodeManageSelectModel;
import org.apache.ibatis.annotations.Param;

@EscrowSlaveConnection
public interface ZipcodeSlaveMapper {
    List<String> getZipcodeSidoList();
    List<String> getZipcodeSigunguList(@Param("sidoNm") String sidoNm);
    List<ZipcodeManageModel> getZipcodeManageListForRoadAddr(ZipcodeManageSelectModel zipcodeManageSelectModel);
    List<ZipcodeManageModel> getZipcodeManageListForGibun(ZipcodeManageSelectModel zipcodeManageSelectModel);
    List<ZipcodeManageModel> getZipcodeManageList(ZipcodeManageSelectModel zipcodeManageSelectModel);
}
