package kr.co.homeplus.escrowmng.order.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "주문정보 조회 요청 DTO")
public class OrderSearchTckSetDto {

    @ApiModelProperty(value = "조회기간타입", position = 1, notes = "조회기간타입 note")
    private String schPeriodType;

    @ApiModelProperty(value = "조회시작일", position = 2)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", position = 3)
    private String schEndDt;

    @ApiModelProperty(value = "주문제휴", position = 4)
    private String affiliateCd;

    @ApiModelProperty(value = "결제상태", position = 5)
    private String paymentStatus;

    @ApiModelProperty(value = "주결제수단", position = 6)
    private List<String> parentMethodCd;

    @ApiModelProperty(value = "점포유형", position = 7)
    private String schStoreType;

    @ApiModelProperty(value = "점포아이디", position = 8)
    private String schStoreId;

    @ApiModelProperty(value = "점포명", position = 9)
    private String schStoreNm;

    @ApiModelProperty(value = "비회원주문여부", position = 10)
    private String schNonUserYn;

    @ApiModelProperty(value = "비회원조회타입", position = 11)
    private String schBuyerInfo;

    @ApiModelProperty(value = "비회원조회내용", position = 12)
    private String schUserSearch;

    @ApiModelProperty(value = "상세조회타입", position = 13)
    private String schDetailType;

    @ApiModelProperty(value = "상세조회내용", position = 14)
    private String schDetailContents;

    @ApiModelProperty(value = "명절선물세트여부", position = 15)
    private String schReserveDlvYn;

    @ApiModelProperty(value = "절임배추여부", position = 16)
    private String schReserveDrctYn;
}
