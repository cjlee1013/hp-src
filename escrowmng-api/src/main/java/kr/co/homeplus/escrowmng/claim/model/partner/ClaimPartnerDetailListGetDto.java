package kr.co.homeplus.escrowmng.claim.model.partner;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimPartnerDetailListGetDto {

    @ApiModelProperty(value = "클레임타입코드")
    private String claimTypeCode;

    @ApiModelProperty(value = "클레임타입")
    private String claimType;

    @ApiModelProperty(value = "처리상태")
    private String claimStatus;

    @ApiModelProperty(value = "상품 주문번호")
    private String orderItemNo;

    @ApiModelProperty(value = "배송번호")
    private String bundleNo;

    @ApiModelProperty(value = "클레임번호")
    private long claimBundleNo;

    @ApiModelProperty(value = "클레임 신청수량")
    private String claimBundleQty;

    @ApiModelProperty(value = "신청일")
    private String requestDt;

    @ApiModelProperty(value = "접수채널")
    private String requestId;

    @ApiModelProperty(value = "신청사유")
    private String claimReasonType;

    @ApiModelProperty(value = "신청사유상세")
    private String claimReasonDetail;

    @ApiModelProperty(value = "보류채널")
    private String pendingId;

    @ApiModelProperty(value = "보류사유")
    private String pendingReasonType;

    @ApiModelProperty(value = "보류사유상세")
    private String pendingReasonDetail;

    @ApiModelProperty(value = "보류일시")
    private String pendingDt;

    @ApiModelProperty(value = "거부채널")
    private String rejectId;

    @ApiModelProperty(value = "거부사유")
    private String rejectReasonType;

    @ApiModelProperty(value = "거부사유상세")
    private String rejectReasonDetail;

    @ApiModelProperty(value = "거부일시")
    private String rejectDt;

    @ApiModelProperty(value = "추가배송비")
    private String addShipPrice;

    @ApiModelProperty(value = "책임사유")
    private String whoReason;

    @ApiModelProperty(value = "클레임 상품 리스트")
    private List<ClaimPartnerDetailItem> claimDetailItemList;
}
