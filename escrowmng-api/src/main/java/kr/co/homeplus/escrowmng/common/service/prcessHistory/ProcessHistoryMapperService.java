package kr.co.homeplus.escrowmng.common.service.prcessHistory;

import kr.co.homeplus.escrowmng.common.model.processHistory.ProcessHistoryRegDto;

public interface ProcessHistoryMapperService {

     /**
     * 주문/클레임 히스토리 등록
     *
     * @param  processHistoryRegDto 클레임 처리내역/히스토리 등록 dto
     * @return Boolean 히스토리 등록 결과
     */
    Boolean setProcessHistory(ProcessHistoryRegDto processHistoryRegDto);

}
