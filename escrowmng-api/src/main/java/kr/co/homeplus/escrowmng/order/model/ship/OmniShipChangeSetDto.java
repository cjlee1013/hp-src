package kr.co.homeplus.escrowmng.order.model.ship;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "TD 배송지정보 변경시 옴니에 전달 요청 DTO")
public class OmniShipChangeSetDto {
    @ApiModelProperty(value = "배송메시지", position = 1)
    private String dlvMsg;
    @ApiModelProperty(value = "옴니주문번호", position = 1)
    private String ordNo;
    @ApiModelProperty(value = "주소지", position = 1)
    private String rcptAddr;
    @ApiModelProperty(value = "받는사람이름", position = 1)
    private String rcptName;
    @ApiModelProperty(value = "받는사람전화번호", position = 1)
    private String rcptPhone;
    @ApiModelProperty(value = "상점ID", position = 1)
    private String storeId;
}
