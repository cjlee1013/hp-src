package kr.co.homeplus.escrowmng.delivery.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.delivery.model.PickupStoreLockerModel;
import kr.co.homeplus.escrowmng.delivery.model.PickupStoreManageModel;
import kr.co.homeplus.escrowmng.delivery.model.PickupStoreManageSelectModel;
import org.apache.ibatis.annotations.Param;

@EscrowSlaveConnection
public interface PickupStoreManageSlaveMapper {
    List<PickupStoreManageModel> selectPickupStoreManageList(PickupStoreManageSelectModel pickupStoreManageSelectModel);
    List<PickupStoreLockerModel> selectPickupStoreLockerList(@Param("storeId") int storeId);
}
