package kr.co.homeplus.escrowmng.claim.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.escrowmng.claim.model.ClaimPrevOrderCancelDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimRequestSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimAllCancelGetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimAllCancelSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimAllCancelSetDto.OrderAllCancelDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimOrderShipMultiCancelGetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimOrderShipMultiCancelSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimOrderShipMultiCancelSetDto.OrderShipMultiCancelSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimTicketMultiCancelGetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimTicketMultiCancelSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimTicketMultiCancelSetDto.TicketAllCancelDto;
import kr.co.homeplus.escrowmng.claim.model.register.ticket.ClaimTicketModifyDto;
import kr.co.homeplus.escrowmng.claim.service.market.MarketClaimService;
import kr.co.homeplus.escrowmng.core.exception.LogicException;
import kr.co.homeplus.escrowmng.enums.ClaimCode;
import kr.co.homeplus.escrowmng.enums.ClaimResponseCode;
import kr.co.homeplus.escrowmng.enums.OrderCode;
import kr.co.homeplus.escrowmng.order.factory.OrderResponseFactory;
import kr.co.homeplus.escrowmng.utils.MessageSendUtil;
import kr.co.homeplus.escrowmng.utils.ObjectUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClaimOrderCancelService {

    private final ClaimMapperService claimMapperService;

    private final ClaimManagementService claimManagementService;

    private final ClaimProcessService claimProcessService;

    private final ClaimMultiCancelProcessService claimMultiCancelProcessService;

    private final MarketClaimService marketClaimService;

    /**
     * 주문취소 관련 주문내역 조회
     *
     * @param purchaseOrderNo 주문번호
     * @return List<ClaimPrevOrderCancelDto> 주문내역 리스트
     */
    public ResponseObject<List<ClaimPrevOrderCancelDto>> getClaimPrevOrderCancelInfo(long purchaseOrderNo, long bundleNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimMapperService.getClaimPrevOrderCancelInfo(purchaseOrderNo, bundleNo));
    }

    /**
     * 환불예정금액 (단건)
     *
     * @param calcSetDto 환불예정금액 조회 DTO
     * @return ClaimPreRefundCalcGetDto 환불예정금액 응답
     * @throws Exception 오류처리
     */
    public ResponseObject<ClaimPreRefundCalcGetDto> getClaimPreRefundCalculation(ClaimPreRefundCalcSetDto calcSetDto) throws Exception {
        ClaimPreRefundCalcDto claimPreRefundCalcDto = claimMapperService.getClaimPreRefundCalculation(calcSetDto);
        ClaimPreRefundCalcGetDto responseDto = new ClaimPreRefundCalcGetDto();
        BeanUtils.copyProperties(claimPreRefundCalcDto, responseDto);
        responseDto.setAddCouponDiscountAmt(claimPreRefundCalcDto.getAddTdDiscountAmt() + claimPreRefundCalcDto.getAddDsDiscountAmt());
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, responseDto);
    }

    public ResponseObject<String> orderCancelAction(ClaimRegSetDto claimRegSetDto) throws Exception {
        if(!this.getClaimPossible(claimRegSetDto)){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR09);
        }

        if("R,X".contains(claimRegSetDto.getClaimType())) {
            if(claimRegSetDto.getClaimDetail().getIsPick().equals("Y")){
                if(claimRegSetDto.getClaimDetail().getPickDlvCd().isEmpty() || claimRegSetDto.getClaimDetail().getPickInvoiceNo().isEmpty()){
                    throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR13);
                }
            }
        }

        // 주문건수, 클레임건수 데이터 가지고 옴
        LinkedHashMap<String, Object> getOrderClaimPartInfoMap = this.getOrderByClaimPartInfo(claimRegSetDto.getPurchaseOrderNo(), claimRegSetDto.getBundleNo());
        // 클레임신청된 건수 + 이미 클레임 신청된 건수 비교
        int totalClaimItemQty = ObjectUtils.toInt(getOrderClaimPartInfoMap.get("claimItemQty")) + claimRegSetDto.getClaimItemList().stream().mapToInt(itemList -> Integer.parseInt(itemList.getClaimQty())).sum();
        // 부분취소 로직 여부
        boolean isPartCancel = Boolean.TRUE;
        // 주문건수 = 클레임건수(기클레임건수 + 신청클레임건수)이면 전체취소
        // 단, 기 클레임건수가 있는 경우 마지막 건수만 부분취소가 아닌 전체 취소로 표시
        if(ObjectUtils.toInt(getOrderClaimPartInfoMap.get("orderItemQty")) == totalClaimItemQty){
            // 전체취소 건수 set
            claimRegSetDto.setClaimPartYn("N");
            // 기 등록된 건이 없으면 전체취소로직
            if(ObjectUtils.toInt(getOrderClaimPartInfoMap.get("claimItemQty")) == 0 && Long.parseLong(claimRegSetDto.getBundleNo()) == 0){
                isPartCancel = Boolean.FALSE;
            }
        } else if (ObjectUtils.toInt(getOrderClaimPartInfoMap.get("orderItemQty")) < totalClaimItemQty) {
            log.debug("ERR1006 : {} / {}", ObjectUtils.toInt(getOrderClaimPartInfoMap.get("orderItemQty")), totalClaimItemQty);
            // 취소건(기취소 + 취소요청건) 이 주문 건수보다 클 경우 오류.
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR06);
        } else if(claimRegSetDto.getClaimItemList().stream().mapToInt(itemList -> Integer.parseInt(itemList.getClaimQty())).sum() == 0){
            // 요청 취소건이 0건이면 오류
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR07);
        } else {
            claimRegSetDto.setClaimPartYn("Y");
        }
        long claimNo = 0;
        // 주문취소(전체주문취소, 주문취소)
        if(claimRegSetDto.getOrderCancelYn().equals("Y") && claimRegSetDto.getClaimType().equalsIgnoreCase("C")){
            // 전체주문취소
            if(isPartCancel){
                // 주문취소
                LinkedHashMap<String, Object> claimRegResultMap =  claimManagementService.orderPartCancel(claimRegSetDto);
                claimNo = ObjectUtils.toLong(claimRegResultMap.get("claim_no"));
            } else {
                LinkedHashMap<String, Object> claimRegResultMap = claimManagementService.orderTotalCancel(claimRegSetDto);
                claimNo = ObjectUtils.toLong(claimRegResultMap.get("claim_no"));
            }
        } else {
            // 취소/반품/교환 신청
            LinkedHashMap<String, Object> claimRegResultMap = claimManagementService.cancelRegister(claimRegSetDto);
            claimNo = ObjectUtils.toLong(claimRegResultMap.get("claim_no"));
            // 발송처리.
            MessageSendUtil.sendClaimApplication(claimNo);
        }
        return OrderResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_REG_SUCCESS);
    }

    /**
     * 클레임 상태변경
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> requestChangeByClaim(ClaimRequestSetDto reqDto) throws Exception {
        // 클레임 요청 타입에 따른 클레임 상태값
        String claimChangeStatus = claimProcessService.getClaimStatus(reqDto.getClaimReqType());
        // 마켓연동여부를 판단하여 처리
        if(marketClaimService.isOrderMarketCheck(reqDto.getClaimNo())){
            if("C".equals(reqDto.getClaimType()) && "C2".equals(claimChangeStatus)) {
                List<LinkedHashMap<String, Object>> claimMap = claimMapperService.getClaimStatusChangeDataForMarket(reqDto.getClaimNo());
                if(claimMap == null) {
                    throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR04);
                }
                try {
                    // 취소할 정보 가지고 오기
                    List<String> orderOptList = claimMap.stream().map(map -> map.get("order_opt_no")).map(ObjectUtils::toString).distinct().collect(Collectors.toList());;
                    String purchaseOrderNo = claimMap.stream().map(map -> ObjectUtils.toString(map.get("purchase_order_no"))).collect(Collectors.toList()).get(0);
                    String claimReasonType = claimMap.stream().map(map -> ObjectUtils.toString(map.get("claim_reason_type"))).collect(Collectors.toList()).get(0);
                    marketClaimService.setMarketCancelReg(purchaseOrderNo, orderOptList, claimReasonType);
                } catch (LogicException e) {
                    log.error("마켓연동 등록 처리중 오류가 발생되었습니다. :: {}", e.getMessage());
                    claimProcessService.setClaimStatus(reqDto.getClaimNo(), reqDto.getClaimBundleNo(), reqDto.getRegId(), ClaimCode.CLAIM_STATUS_C9);
                    throw new LogicException(e.getResponseCode(), e.getResponseMsg());
                }
            } else if(!"C3,C4".contains(claimChangeStatus)) {
                marketClaimService.setMarketStatusChange(reqDto.getClaimNo(), claimChangeStatus, reqDto.getReasonType(), reqDto.getReasonTypeDetail());
            }
        }
        // 클레임 상태 변경값이 C2(클레임승인) 이면 결제 취소 로직을 수행한다.
        if(claimChangeStatus.equalsIgnoreCase(ClaimCode.CLAIM_STATUS_C2.getCode())){
            // 상태 변경 C1 -> C2
            if (!claimProcessService.setClaimStatus(reqDto.getClaimNo(), reqDto.getClaimBundleNo(), reqDto.getRegId(), ClaimCode.CLAIM_STATUS_C2)) {
                throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
            }
//            if(!claimManagementService.paymentCancel(reqDto.getClaimNo(), reqDto.getClaimBundleNo(), reqDto.getRegId())){
//                log.error("클레임 결제 취소 중 오류가 발생됨. {}", reqDto.getClaimNo());
//                throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
//            }
//            if("C,R".contains(reqDto.getClaimType())){
//                MessageSendUtil.sendClaimCompleteWithoutExchange(reqDto.getClaimNo());
//            } else {
//                MessageSendUtil.sendExchangeClaimComplete(reqDto.getClaimNo());
//            }
        } else {
            // 클레임 상태 업데이트
            if(!claimProcessService.setClaimStatus(reqDto)){
                throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
            }
        }
        return OrderResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_REG_SUCCESS);
    }

    public ResponseObject<ClaimAllCancelGetDto> orderAllCancelAction(ClaimAllCancelSetDto setDto) throws Exception {
        // 주문번호/번들번호 리스트
        Map<Long, List<Long>> parameterMap = getConvertListToMap(setDto.getItemList());
        log.info("일괄취소 List to Map :::; {}", parameterMap);
        ClaimAllCancelGetDto getDto = new ClaimAllCancelGetDto();
        for(long purchaseOrderNo : parameterMap.keySet()){
            // 주문번호. MAP에서 번들을 추측하여 처리.
            for(long bundleNo : parameterMap.get(purchaseOrderNo)){
                try {
                    LinkedHashMap<String, Object> claimDataMap = claimProcessService.addClaimRegister(this.createClaimRegSetDto(purchaseOrderNo, bundleNo, setDto.getClaimReasonType(), setDto.getClaimReasonDetail(), setDto.getRegId() ));
                    log.info("주문번호 ({}) 번들번호 : ({}) 거래취소 요청 결과 :: {}", purchaseOrderNo, bundleNo, claimDataMap);
                    // 상태 변경 C1 -> C2
                    if (!claimProcessService.setClaimStatus(ObjectUtils.toLong(claimDataMap.get("claim_no")), ObjectUtils.toLong(claimDataMap.get("claim_bundle_no")), setDto.getRegId(), ClaimCode.CLAIM_STATUS_C2)) {
                        throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
                    }
                    getDto.setSuccess();
                } catch (LogicException le) {
                    log.error("주문번호 ({}) 번들번호 : ({}) 거래취소 요청 중 오류가 발생되었습니다. :: {}", purchaseOrderNo, bundleNo, le.getResponseMsg());
                    getDto.setFail();
                } catch (Exception e) {
                    log.error("주문번호 ({}) 번들번호 : ({}) 거래취소 요청 중 오류가 발생되었습니다. :: {}", purchaseOrderNo, bundleNo, e.getMessage());
                    getDto.setFail();
                }
            }
        }
        return OrderResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_REG_SUCCESS, getDto);
    }

    public ResponseObject<ClaimTicketMultiCancelGetDto> ticketAllCancelAction(
        ClaimTicketMultiCancelSetDto setDto) throws Exception {
        long successCnt = 0;
        long failCnt = 0;

        // 주문번호/번들번호 리스트
        Map<Long, List<Long>> parameterMap = getTicketConvertListToMap(setDto.getItemList());
        Map<Long, List<Long>> parameterMap2 = getTicketConvertListToMap2(setDto.getItemList());

        for(long purchaseOrderNo : parameterMap.keySet()){
            for(long bundleNo : parameterMap.get(purchaseOrderNo)){
                ArrayList<Long> orderTicketList = (ArrayList<Long>) parameterMap2.get(bundleNo);
               log.info("[ticketAllCancelAction]  주문번호:: {}, 번들번호 :::: {}, orderTicketList ::: {}", purchaseOrderNo, bundleNo, orderTicketList);
               try {
                   /* 클레임 데이터 등록 */
                   LinkedHashMap<String, Object> claimDataMap = claimMultiCancelProcessService.addMultiTicketClaimRegister(this.createTicketAllClaimRegSetDto(purchaseOrderNo, bundleNo, orderTicketList, setDto.getClaimReasonType(), setDto.getClaimReasonDetail(), setDto.getRegId()), orderTicketList);

                   /*
                   티켓 취소요청 시 쿠프와 티켓상태가 안맞을 수 있어서 티켓상태 변경 로직 주석처리
                   homeplus : 티켓취소요청 T3
                   쿠프 : 티켓 사용
                   *//* 클레임 티켓 데이터 조회 *//*
                   List<LinkedHashMap<String, Object>> claimTicketInfo = claimMapperService.getClaimTicketInfo(ObjectUtils.toLong(claimDataMap.get("claim_no")));

                   *//* 티켓 상태 변경 T1 -> T3 *//*
                   for(LinkedHashMap<String, Object> map : claimTicketInfo){
                       if(!claimMapperService.modifyOrderTicketInfo(ClaimTicketModifyDto.builder().claimTicketNo(ObjectUtils.toLong(map.get("claim_ticket_no"))).ticketStatus("T3").issueStatus(null).build())){
                           throw new LogicException(ClaimResponseCode.CLAIM_TICKET_ERR04);
                       }
                   }
                   */

                   /* 클레임 상태 변경 C1 -> C2 */
                   if (!claimProcessService.setClaimStatus(ObjectUtils.toLong(claimDataMap.get("claim_no")), ObjectUtils.toLong(claimDataMap.get("claim_bundle_no")), setDto.getRegId(), ClaimCode.CLAIM_STATUS_C2)) {
                       throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
                   }

                   successCnt += orderTicketList.size();
                } catch (LogicException le) {
                    log.error("주문번호 ({}) 번들번호 : ({}) 거래취소 요청 중 오류가 발생되었습니다. :: {}", purchaseOrderNo, bundleNo, le.getResponseMsg());
                    failCnt += orderTicketList.size();
                } catch (Exception e) {
                    log.error("주문번호 ({}) 번들번호 : ({}) 거래취소 요청 중 오류가 발생되었습니다. :: {}", purchaseOrderNo, bundleNo, e.getMessage());
                    failCnt += orderTicketList.size();
                }
            }
        }
        return OrderResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_REG_SUCCESS, new ClaimTicketMultiCancelGetDto(successCnt, failCnt));
    }

        public ResponseObject<ClaimOrderShipMultiCancelGetDto> orderShipMultiCancelReq(ClaimOrderShipMultiCancelSetDto setDto) throws Exception {
        long successCnt = 0;
        long failCnt = 0;

        // 주문번호/번들번호 리스트
        Map<Long, List<Long>> parameterMap = getOrderShipConvertListToMap(setDto.getItemList());
        Map<Long, List<Long>> parameterMap2 = getOrderShipConvertListToMap2(setDto.getItemList());

        for(long purchaseOrderNo : parameterMap.keySet()){
            for(long bundleNo : parameterMap.get(purchaseOrderNo)){
                ArrayList<Long> orderItemNoList = (ArrayList<Long>) parameterMap2.get(bundleNo);
               log.info("[orderShipMultiCancelReq]  주문번호:: {}, 번들번호 :::: {}, orderItemNoList ::: {}", purchaseOrderNo, bundleNo, orderItemNoList);
               try {
                   /* 클레임 데이터 등록 */
                   LinkedHashMap<String, Object> claimDataMap = claimProcessService.addClaimRegister(this.createOrderShipMultiClaimRegSetDto(purchaseOrderNo, bundleNo, orderItemNoList, setDto.getClaimReasonType(), setDto.getClaimReasonDetail(), setDto.getRegId()));

                   /* 클레임 상태 변경 C1 -> C2 */
                   if (!claimProcessService.setClaimStatus(ObjectUtils.toLong(claimDataMap.get("claim_no")), ObjectUtils.toLong(claimDataMap.get("claim_bundle_no")), setDto.getRegId(), ClaimCode.CLAIM_STATUS_C2)) {
                       throw new LogicException(ClaimResponseCode.CLAIM_STATUS_CHANGE_ERR01);
                   }

                   successCnt += orderItemNoList.size();
                } catch (LogicException le) {
                    log.error("주문번호 ({}) 번들번호 : ({}) 거래취소 요청 중 오류가 발생되었습니다. :: {}", purchaseOrderNo, bundleNo, le.getResponseMsg());
                    failCnt += orderItemNoList.size();
                } catch (Exception e) {
                    log.error("주문번호 ({}) 번들번호 : ({}) 거래취소 요청 중 오류가 발생되었습니다. :: {}", purchaseOrderNo, bundleNo, e.getMessage());
                    failCnt += orderItemNoList.size();
                }
            }
        }
        return OrderResponseFactory.getResponseObject(ClaimResponseCode.CLAIM_REG_SUCCESS, new ClaimOrderShipMultiCancelGetDto(successCnt, failCnt));
    }



    // 클레임 가능 여부 체크
    private Boolean getClaimPossible(ClaimRegSetDto claimRegSetDto) throws Exception {
        // R | X 요청 일 경우에는 거래가 D3, D4, D5 만 가능
        // C 일 경우에는 D1, D2일 때만 가능.
        // 원주문의 배송상태를 조회
        LinkedHashMap<String, Object> claimPossibleDataMap = claimMapperService.getClaimPossibleCheck(Long.parseLong(claimRegSetDto.getPurchaseOrderNo()), Long.parseLong(claimRegSetDto.getBundleNo()));
        // 조회 건수가 없으면 오류.
        if(claimPossibleDataMap.size() == 0){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR08);
        }

        String shipStatus = ObjectUtils.toString(claimPossibleDataMap.getOrDefault("ship_status", "NN"));
        String orderType = ObjectUtils.toString(claimPossibleDataMap.get("order_type"));
        // 클레임 타입
        String claimType = claimRegSetDto.getClaimType();
        // ORD_TICKET 일 경우 별도 체크!
        if("ORD_TICKET".equals(orderType)){
            if(!claimType.equals("C")){
                return false;
            }
        }
        if("ORD_MULTI".equals(orderType)){
            claimRegSetDto.setIsMultiOrder(Boolean.TRUE);
            shipStatus = ObjectUtils.toString(claimMapperService.getMultiOrderClaimPossibleCheck(Long.parseLong(claimRegSetDto.getPurchaseOrderNo()), Long.parseLong(claimRegSetDto.getMultiBundleNo())).getOrDefault("ship_status", "NN"));
        }

        String claimShipStatus = "";
        switch (claimType.toUpperCase(Locale.KOREA)) {
            case "C" :
                //클레임 타입이 C 가 아니면 튕김.
                claimShipStatus = orderType.toUpperCase(Locale.KOREA).contains("TICKET") ? "D1,D2,D3,D4,D5" : claimRegSetDto.getOrderCancelYn().equals("Y") ? "D0,D1" : "D2";
                break;
            case "R" :
            case "X" :
                claimShipStatus = orderType.contains("MULTI") ? "D4,D5" : "D3,D4,D5";
                break;
        }
        return claimShipStatus.contains(shipStatus);
    }

    private LinkedHashMap<String, Object> getOrderByClaimPartInfo(String purchaseOrderNo, String bundleNo) throws Exception {
        LinkedHashMap<String, Object> returnMap = claimMapperService.getOrderByClaimPartInfo(Long.parseLong(purchaseOrderNo), Long.parseLong(bundleNo));
        if(ObjectUtils.toInt(returnMap.get("orderItemQty")) == 0){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR05);
        }
        return returnMap;
    }

    /**
     * 취소요청 파라미터 생성.
     *
     * @param map 취소기본정보.
     * @return 취소요청파라미터
     */
    public ClaimRegSetDto createClaimRegSetDto(long purchaseOrderNo, long bundleNo, String claimReasonType, String claimReasonDetail, String regId) throws Exception {
        List<ClaimPreRefundItemList> itemInfo = claimMapperService.getClaimPreRefundItemInfo(purchaseOrderNo, bundleNo);
        if(itemInfo == null || itemInfo.size() == 0){
            log.error("거래번호({})는 이미 취소된 거래입니다.", purchaseOrderNo);
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR06);
        }
        return ClaimRegSetDto.builder()
            .purchaseOrderNo(String.valueOf(purchaseOrderNo))
            .bundleNo(String.valueOf(bundleNo))
            .claimItemList(itemInfo)
            .whoReason("S")
            .cancelType("O")
            .claimType("C")
            .claimReasonType(claimReasonType)
            .claimReasonDetail(claimReasonDetail)
            .regId(regId)
            .claimPartYn("N")
            .orderCancelYn("Y")
            .piDeductPromoYn("N")
            .piDeductDiscountYn("N")
            .piDeductShipYn("N")
            .multiBundleNo(null)
            .isMultiOrder(Boolean.FALSE)
            .build();
    }

    private Map<Long, List<Long>> getConvertListToMap(List<OrderAllCancelDto> dataList){
        return dataList.stream()
            .collect(
                Collectors.toMap(
                    OrderAllCancelDto::getPurchaseOrderNo,
                    dto -> new ArrayList<>(Arrays.asList(dto.getBundleNo())),
                    (l1, l2) -> {
                        if(!l1.containsAll(l2)){
                            l1.addAll(l2);
                        }
                        return l1;
                    }
                )
            );
    }



    /**
     * 티켓일괄 취소요청 파라미터 생성.
     *
     * @return 취소요청파라미터
     */
    public ClaimRegSetDto createTicketAllClaimRegSetDto(long purchaseOrderNo, long bundleNo, ArrayList<Long> orderTicketList, String claimReasonType, String claimReasonDetail, String regId) throws Exception {
        List<ClaimPreRefundItemList> itemInfo = claimMapperService.getClaimPreRefundTicketItemInfo(purchaseOrderNo, bundleNo);
        if(itemInfo == null || itemInfo.size() == 0){
            log.error("거래번호({})는 이미 취소된 거래입니다.", purchaseOrderNo);
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR06);
        }
        itemInfo.get(0).setClaimQty(String.valueOf(orderTicketList.size()));

        return ClaimRegSetDto.builder()
            .purchaseOrderNo(String.valueOf(purchaseOrderNo))
            .bundleNo(String.valueOf(bundleNo))
            .claimItemList(itemInfo)
            .whoReason("S")
            .cancelType("O")
            .claimType("C")
            .claimReasonType(claimReasonType)
            .claimReasonDetail(claimReasonDetail)
            .regId(regId)
            .claimPartYn("N")
            .orderCancelYn("Y")
            .piDeductPromoYn("N")
            .piDeductDiscountYn("N")
            .piDeductShipYn("N")
            .multiBundleNo(null)
            .isMultiOrder(Boolean.FALSE)
            .build();
    }

    /**
     * 취소요청 파라미터 생성.
     *
     * @return 취소요청파라미터
     */
    public ClaimRegSetDto createOrderShipMultiClaimRegSetDto(long purchaseOrderNo, long bundleNo, ArrayList<Long> orderItemList, String claimReasonType, String claimReasonDetail, String regId) throws Exception {
        List<ClaimPreRefundItemList> itemInfo = claimMapperService.getClaimPreRefundItemInfo(purchaseOrderNo, bundleNo);
        if(itemInfo == null || itemInfo.size() == 0){
            log.error("거래번호({})는 이미 취소된 거래입니다.", purchaseOrderNo);
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR06);
        }

        List<ClaimPreRefundItemList> claimItemList = new ArrayList<>();

        for(ClaimPreRefundItemList item : itemInfo){
            for(Long orderItemNo : orderItemList){
                if(item.getOrderItemNo().equalsIgnoreCase(String.valueOf(orderItemNo))){
                    log.info("발주/발송 일괄취소 대상 : {}", orderItemNo);
                    claimItemList.add(item);
                }
            }
        }

        return ClaimRegSetDto.builder()
            .purchaseOrderNo(String.valueOf(purchaseOrderNo))
            .bundleNo(String.valueOf(bundleNo))
            .claimItemList(claimItemList)
            .whoReason("S")
            .cancelType("O")
            .claimType("C")
            .claimReasonType(claimReasonType)
            .claimReasonDetail(claimReasonDetail)
            .regId(regId)
            .claimPartYn("N")
            .orderCancelYn("Y")
            .piDeductPromoYn("N")
            .piDeductDiscountYn("N")
            .piDeductShipYn("N")
            .multiBundleNo(null)
            .isMultiOrder(Boolean.FALSE)
            .build();
    }

    private Map<Long, List<Long>> getOrderShipConvertListToMap(List<OrderShipMultiCancelSetDto> dataList){
        Map<Long, List<Long>> result = dataList.stream()
            .collect(
                Collectors.toMap(
                    OrderShipMultiCancelSetDto::getPurchaseOrderNo,
                    dto -> new ArrayList<>(Arrays.asList(dto.getBundleNo())),
                    (oldValue, newValue) -> {
                        if(!oldValue.containsAll(newValue)){
                            oldValue.addAll(newValue);
                        }
                        return oldValue;
                    }
                )
            );
        return result;
    }

    private Map<Long, List<Long>> getOrderShipConvertListToMap2(List<OrderShipMultiCancelSetDto> dataList){
        Map<Long, List<Long>> result = dataList.stream()
            .collect(
                Collectors.toMap(
                    OrderShipMultiCancelSetDto::getBundleNo,
                    dto -> new ArrayList<>(Arrays.asList(dto.getOrderItemNo())),
                    (oldValue, newValue) -> {
                        if(!oldValue.containsAll(newValue)){
                            oldValue.addAll(newValue);
                        }
                        return oldValue;
                    }
                )
            );
        return result;
    }

    private Map<Long, List<Long>> getTicketConvertListToMap(List<TicketAllCancelDto> dataList){
        Map<Long, List<Long>> result = dataList.stream()
            .collect(
                Collectors.toMap(
                    TicketAllCancelDto::getPurchaseOrderNo,
                    dto -> new ArrayList<>(Arrays.asList(dto.getBundleNo())),
                    (oldValue, newValue) -> {
                        if(!oldValue.containsAll(newValue)){
                            oldValue.addAll(newValue);
                        }
                        return oldValue;
                    }
                )
            );
        return result;
    }

    private Map<Long, List<Long>> getTicketConvertListToMap2(List<TicketAllCancelDto> dataList){
        Map<Long, List<Long>> result = dataList.stream()
            .collect(
                Collectors.toMap(
                    TicketAllCancelDto::getBundleNo,
                    dto -> new ArrayList<>(Arrays.asList(dto.getOrderTicketNo())),
                    (oldValue, newValue) -> {
                        if(!oldValue.containsAll(newValue)){
                            oldValue.addAll(newValue);
                        }
                        return oldValue;
                    }
                )
            );
        return result;
    }


    private void isException(boolean isException) throws Exception {
        if(isException) {
            throw new LogicException("9999", "잠시후 이용부탁드립니다.");
        }
    }

    /*
        private Map<Long, Map<Long, List<Long>>> getTicketConvertListToMap(List<TicketAllCancelDto> dataList){


        Map<Long, Map<Long, List<Long>>> result = dataList.stream()
            .collect(
                Collectors.toMap(
                    TicketAllCancelDto::getPurchaseOrderNo,
                    dto -> dto.getBundleNo(),
                    (l1, l2) -> {
                        dataList.stream().collect(Collectors.toMap(
                            TicketAllCancelDto::getBundleNo,
                            dto -> new ArrayList<>(Arrays.asList(dto.getOrderTicketNo())),
                            (ll1, ll2) -> {
                                if(!ll1.containsAll(ll2)){
                                    ll1.addAll(ll2);
                                }
                                return ll1;
                            })
                        );

                        return l1;
                    }
                )
            );

        return result;

    }*/
}
