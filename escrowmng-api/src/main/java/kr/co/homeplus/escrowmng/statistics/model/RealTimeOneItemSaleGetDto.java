package kr.co.homeplus.escrowmng.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.escrowmng.extract.model.ExtractCommonSetDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@ApiModel(description = "실시간 특정 상품 판매 현황")
@Getter
@Setter
@EqualsAndHashCode
public class RealTimeOneItemSaleGetDto {
    @ApiModelProperty(value = "점포유형(HYPER, CLUB, EXP, AURORA, DS)", position = 1)
    private String storeType;

    @ApiModelProperty(value = "상품번호", position = 2)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 3)
    private String itemName;

    @ApiModelProperty(value = "판매수량", position = 4)
    private String itemQty;

    @ApiModelProperty(value = "취소수량", position = 5)
    private String claimQty;

    @ApiModelProperty(value = "량판매금액", position = 6)
    private String orderPrice;

    @ApiModelProperty(value = "취소금액", position = 7)
    private String claimPrice;

    @ApiModelProperty(value = "순판매수량", position = 8)
    private String totQty;

    @ApiModelProperty(value = "순판매금액", position = 9)
    private String totPrice;

    @ApiModelProperty(value = "대카테고리", position = 10)
    private String lcateNm;

    @ApiModelProperty(value = "중카테고리", position = 11)
    private String mcateNm;

    @ApiModelProperty(value = "소카테고리", position = 12)
    private String scateNm;

    @ApiModelProperty(value = "세카테고리", position = 13)
    private String dcateNm;
}