package kr.co.homeplus.escrowmng.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "ESCROW 공통 코드관리 Dto")
public class MngCodeGetModel {
    @ApiModelProperty(notes = "유형코드")
    private String gmcCd;
    @ApiModelProperty(notes = "코드")
    private String mcCd;
    @ApiModelProperty(notes = "코드명")
    private String mcNm;
    @ApiModelProperty(notes = "우선순위")
    private Integer priority;
    @ApiModelProperty(notes = "사용여부")
    private String useYn;
    @ApiModelProperty(notes = "참조1")
    private String ref1;
    @ApiModelProperty(notes = "참조2")
    private String ref2;
    @ApiModelProperty(notes = "참조3")
    private String ref3;
}
