package kr.co.homeplus.escrowmng.delivery.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.delivery.model.DailyShiftDuplicateCheckModel;
import kr.co.homeplus.escrowmng.delivery.model.DailyShiftManageModel;
import kr.co.homeplus.escrowmng.delivery.model.DailyShiftManageSelectModel;
import kr.co.homeplus.escrowmng.delivery.model.DailyShiftStoreManageModel;
import kr.co.homeplus.escrowmng.delivery.service.DailyShiftManageService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/escrow/storeDelivery")
@Api(tags = "배송관리 > 점포배송정보 > 일자별 Shift 관리")
public class DailyShiftManageController {
    private final DailyShiftManageService dailyShiftManageService;

    /**
     * 일자별 Shift 정보 조회
     */
    @PostMapping("/getDailyShiftManageList")
    @ApiOperation(value = "일자별 Shift 정보 조회")
    public ResponseObject<List<DailyShiftManageModel>> getDailyShiftManageList(@Valid @RequestBody DailyShiftManageSelectModel dailyShiftManageSelectModel) {
        return ResourceConverter.toResponseObject(dailyShiftManageService.getDailyShiftManageList(dailyShiftManageSelectModel));
    }

    /**
     * 일자별 Shift 점포 정보 조회
     */
    @GetMapping("/getDailyShiftStoreManageList")
    @ApiOperation(value = "일자별 Shift 점포 정보 조회")
    public ResponseObject<List<DailyShiftStoreManageModel>> getDailyShiftStoreManageList(@RequestParam(value = "dailyShiftMngSeq") long dailyShiftMngSeq) {
        return ResourceConverter.toResponseObject(dailyShiftManageService.getDailyShiftStoreManageList(dailyShiftMngSeq));
    }

    /**
     * 일자별 Shift 관리 중복체크
     */
    @PostMapping("/checkDuplicateDailyShiftManage")
    @ApiOperation(value = "일자별 Shift 관리 중복체크")
    public ResponseObject<Integer> checkDuplicateDailyShiftManage(@RequestBody DailyShiftDuplicateCheckModel dailyShiftDuplicateCheckModel) {
        return ResourceConverter.toResponseObject(dailyShiftManageService.checkDuplicateDailyShiftManage(dailyShiftDuplicateCheckModel));
    }

    /**
     * 일자별 Shift 정보 저장
     */
    @PostMapping("/saveDailyShiftManage")
    @ApiOperation(value = "일자별 Shift 정보 저장")
    public ResponseObject<DailyShiftManageModel> saveDailyShiftManage(@RequestBody DailyShiftManageModel dailyShiftManageModel) {
        return ResourceConverter.toResponseObject(dailyShiftManageService.saveDailyShiftManage(dailyShiftManageModel));
    }

    /**
     * 일자별 Shift 점포 정보 저장
     */
    @PostMapping("/saveDailyShiftStoreManageList")
    @ApiOperation(value = "일자별 Shift 점포 정보 저장")
    public ResponseObject<Integer> saveDailyShiftStoreManageList(@RequestBody List<DailyShiftStoreManageModel> dailyShiftStoreManageModelList) {
        return ResourceConverter.toResponseObject(dailyShiftManageService.saveDailyShiftStoreManageList(dailyShiftStoreManageModelList));
    }

    /**
     * 일자별 Shift 정보 삭제
     */
    @PostMapping("/deleteDailyShiftManage")
    @ApiOperation(value = "일자별 Shift 정보 삭제")
    public ResponseObject<Integer> deleteDailyShiftManage(@RequestBody List<DailyShiftManageModel> dailyShiftManageModelList) {
        return ResourceConverter.toResponseObject(dailyShiftManageService.deleteDailyShiftManage(dailyShiftManageModelList));
    }
}
