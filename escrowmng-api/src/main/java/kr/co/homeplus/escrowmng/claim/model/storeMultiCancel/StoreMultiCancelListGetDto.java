package kr.co.homeplus.escrowmng.claim.model.storeMultiCancel;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreMultiCancelListGetDto {

    @ApiModelProperty(value = "주문번호", position = 1)
    public String purchaseOrderNo;

    @ApiModelProperty(value = "배송번호", position = 2)
    public String shipNo;

    @ApiModelProperty(value = "번들번호", position = 3)
    public String bundleNo;

    @ApiModelProperty(value = "주문일시", position = 4)
    public String orderDt;

    @ApiModelProperty(value = "배송일", position = 5)
    public String shipDt;

    @ApiModelProperty(value = "배송shift", position = 6)
    public String shiftId;

    @ApiModelProperty(value = "배송상태", position = 7)
    public String shipStatus;

    @ApiModelProperty(value = "구매자", position = 8)
    public String buyerNm;

    @ApiModelProperty(value = "구매자", position = 9)
    public String buyerMobileNo;

    @ApiModelProperty(value = "구매자연락처", position = 10)
    public String receiverNm;

    @ApiModelProperty(value = "수령인연락처", position = 11)
    public String shipMobileNo;

    @ApiModelProperty(value = "주문상품", position = 12)
    public String itemNm;

    @ApiModelProperty(value = "결제금액", position = 13)
    public String itemPrice;

    @ApiModelProperty(value = "주소", position = 14)
    private String shipAddr;

    @ApiModelProperty(value = "마켓연동", position = 15)
    private String marketType;

    @ApiModelProperty(value = "마켓주문번호", position = 16)
    private String marketOrderNo;

}
