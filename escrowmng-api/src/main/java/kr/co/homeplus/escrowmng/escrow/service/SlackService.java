package kr.co.homeplus.escrowmng.escrow.service;

import static kr.co.homeplus.escrowmng.constants.Constants.SPACE;

import kr.co.homeplus.escrowmng.enums.MarketType;
import kr.co.homeplus.escrowmng.enums.SlackNotifier;
import kr.co.homeplus.escrowmng.enums.SlackNotifier.SlackTarget;
import kr.co.homeplus.escrowmng.utils.EscrowDate;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SlackService {

    private final SlackNotifier slackNotifier;

    @Value("${spring.profiles.active}")
    private String PROFILES_ACTIVE;

    public void noOrderCountSendCheck() {
        StringBuilder slackMessage = new StringBuilder();
        String today = EscrowDate.getToday();
        String title = "ESCROW 주문건수";
        slackMessage.append("2분간 주문건이 없음(즉시 확인 필요)");
        slackNotifier.slackNotify(SlackTarget.CH_PROD_BATCH, today + SPACE + title, slackMessage.toString(),"error");

    }

    public void noMarketOrderCollectCountSendCheck(MarketType marketType) {
        StringBuilder slackMessage = new StringBuilder();
        String today = EscrowDate.getToday();
        String title = "마켓연동주문 수집건수";
        slackMessage.append(marketType.getTypeNm());
        slackMessage.append(" 10분간 수집된 주문이 없음(즉시 확인 필요)");
        slackNotifier.slackNotify(SlackTarget.CH_PROD_BATCH, today + SPACE + title, slackMessage.toString(),"error");
    }

    public void noMarketOrderCreatePurchasetCountSendCheck(MarketType marketType) {
        StringBuilder slackMessage = new StringBuilder();
        String today = EscrowDate.getToday();
        String title = "마켓연동주문 생성건수";
        slackMessage.append(marketType.getTypeNm());
        slackMessage.append(" 10분간 생성된 주문이 없음(즉시 확인 필요)");
        slackNotifier.slackNotify(SlackTarget.CH_PROD_BATCH, today + SPACE + title, slackMessage.toString(),"error");
    }
}
