package kr.co.homeplus.escrowmng.extract.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.homeplus.escrowmng.extract.model.ExtractCommonSetDto;
import kr.co.homeplus.escrowmng.extract.service.ExtractHistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/extract")
@Api(tags = "추출 이력 저장용")
public class ExtractHistoryController {

    private final ExtractHistoryService extractHistoryService;

    @PostMapping("/saveExtractHistory")
    @ApiOperation("데이터 추출 히스토리 저장")
    public void saveExtractHistory(@RequestBody ExtractCommonSetDto extractCommonSetDto) {
        extractHistoryService.saveExtractHistory(extractCommonSetDto);
    }
}
