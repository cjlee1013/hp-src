package kr.co.homeplus.escrowmng.claim.service;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.escrowmng.claim.mapper.ClaimReadMapper;
import kr.co.homeplus.escrowmng.claim.model.ClaimDetailInfoGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimDetailInfoListDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimDetailInfoSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimGiftItemListDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimHistoryListDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimInfoListGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimInfoListSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPaymentProcessGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPickShippingDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimProcessGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimProcessListDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimReasonInfoGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimReasonInfoSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimRegisterPreRefundInfoDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimShipInfoDto;
import kr.co.homeplus.escrowmng.claim.model.DiscountInfoListDto;
import kr.co.homeplus.escrowmng.claim.model.PromoInfoListDto;
import kr.co.homeplus.escrowmng.claim.model.StoreInfoGetDto;
import kr.co.homeplus.escrowmng.enums.OrderCode;
import kr.co.homeplus.escrowmng.order.factory.OrderResponseFactory;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClaimAdminMainService {

    private final ClaimMapperService claimMapperService;

     /**
     * 클레임 리스트 조회
     *
     * @param claimInfoListSetDto 클레임 조회 dto
     * @return List<ClaimInfoListGetDto> 클레임 내역 리스트
     */
    public ResponseObject<List<ClaimInfoListGetDto>> getClaimInfoList(ClaimInfoListSetDto claimInfoListSetDto) {
        List<ClaimInfoListGetDto> claimList = claimMapperService.getClaimInfoList(claimInfoListSetDto);
        if (ObjectUtils.isNotEmpty(claimList)) {
            for (ClaimInfoListGetDto next : claimList) {
                next.setBuyerNm(PrivacyMaskingUtils.maskingUserName(next.getBuyerNm()));
                next.setBuyerMobileNo(PrivacyMaskingUtils.maskingPhone(next.getBuyerMobileNo()));
                next.setUserNo(PrivacyMaskingUtils.maskingUserId(next.getUserNo()));
            }
        }
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimList);
    }

     /**
     * 클레임 정보 조회
     *
     * @param claimDetailInfoSetDto 클레임 조회 dto
     * @return List<ClaimInfoListGetDto> 클레임 내역 리스트
     */
    public ResponseObject<ClaimDetailInfoGetDto> getClaimDetailInfo(ClaimDetailInfoSetDto claimDetailInfoSetDto) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimMapperService.getClaimDetailInfo(claimDetailInfoSetDto));
    }

     /**
     * 클레임 리스트 조회
     *
     * @param claimDetailInfoSetDto 클레임 조회 dto
     * @return List<ClaimInfoListGetDto> 클레임 내역 리스트
     */
    public ResponseObject<List<ClaimDetailInfoListDto>> getClaimDetailInfoList(
        ClaimDetailInfoSetDto claimDetailInfoSetDto) {

        List<ClaimDetailInfoListDto> list = claimMapperService.getClaimDetailInfoList(claimDetailInfoSetDto);
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, list);
    }
     /**
     * 취소 상세사유 조회
     *
     * @param claimReasonInfoSetDto 클레임 조회 dto
     * @return CancelReason 클레임 내역 리스트
     */
    public ResponseObject<ClaimReasonInfoGetDto> getClaimReasonInfo(ClaimReasonInfoSetDto claimReasonInfoSetDto) {
        ClaimReasonInfoGetDto claimReasonInfoGetDto = new ClaimReasonInfoGetDto();
        ClaimPickShippingDto claimPickShippingDto  = new ClaimPickShippingDto();
        switch (claimReasonInfoSetDto.getClaimType()){
            case "C" :
                claimReasonInfoGetDto = claimMapperService.getCancelReasonInfo(claimReasonInfoSetDto.getClaimNo());
                break;
            case "R" :
                claimPickShippingDto = claimMapperService.getClaimPickShippingInfo(claimReasonInfoSetDto.getClaimBundleNo());
                claimReasonInfoGetDto = claimMapperService.getReturnExchangeReasonInfo(claimReasonInfoSetDto.getClaimNo());

                if(claimPickShippingDto.getShipMethod().indexOf("DRCT") > -1){
                    LinkedHashMap<String, String> slotResult =  claimMapperService.getSlotSiftInfo(claimPickShippingDto.getSlotId(),claimPickShippingDto.getShiftId(), claimReasonInfoGetDto.getStoreType(),claimReasonInfoGetDto.getStoreId());
                    if(slotResult != null){
                        claimPickShippingDto.setSlotShipDateTime("(" + slotResult.get("ship_weekday") + ")" + " " + slotResult.get("slot_ship_start_time").substring(0,2) + ":" + slotResult.get("slot_ship_start_time").substring(2,4)
                            + " ~ " + slotResult.get("slot_ship_end_time").substring(0,2) + ":" + slotResult.get("slot_ship_end_time").substring(2,4));
                    }
                }
                claimReasonInfoGetDto.setClaimPickShipping(claimPickShippingDto);//반품배송정보 조회
                break;
            case "X":
                claimPickShippingDto = claimMapperService.getClaimPickShippingInfo(claimReasonInfoSetDto.getClaimBundleNo());
                claimReasonInfoGetDto = claimMapperService.getReturnExchangeReasonInfo(claimReasonInfoSetDto.getClaimNo());

                if(claimPickShippingDto.getShipMethod().indexOf("DRCT") > -1){
                    LinkedHashMap<String, String> slotResult =  claimMapperService.getSlotSiftInfo(claimPickShippingDto.getSlotId(),claimPickShippingDto.shiftId,claimReasonInfoGetDto.getStoreType(),claimReasonInfoGetDto.getStoreId());
                    if(slotResult != null){
                        claimPickShippingDto.setSlotShipDateTime("(" + slotResult.get("ship_weekday") + ")" + " " + slotResult.get("slot_ship_start_time").substring(0,2) + ":" + slotResult.get("slot_ship_start_time").substring(2,4)
                            + " ~ " + slotResult.get("slot_ship_end_time").substring(0,2) + ":" + slotResult.get("slot_ship_end_time").substring(2,4));
                    }
                }
                claimReasonInfoGetDto.setClaimPickShipping(claimPickShippingDto);//반품배송정보 조회
                claimReasonInfoGetDto.setClaimExchShipping(claimMapperService.getClaimExchShippingInfo(claimReasonInfoSetDto.getClaimBundleNo()));//교환배송정보 조회
                break;
            default:
                break;
        }

        claimReasonInfoGetDto.setRefundTypeList(claimMapperService.getRefundTypeList(claimReasonInfoSetDto.getClaimNo()));
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimReasonInfoGetDto);
    }

    /**
     * 차감 할인정보 조회
     *
     * @param claimNo
     * @return List<DiscountInfoListDto> 할인 내역 리스트
     */
    public ResponseObject<List<DiscountInfoListDto>> getDiscountInfoList(long claimNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimMapperService.getDiscountInfoList(claimNo));
    }
     /**
     * 등록된 클레임 환불예정금액 조회
     *
     * @param claimNo
     * @return claimRegisterPreRefundInfoDto 등록된 클레임 환불예정금액
     */
    public ResponseObject<ClaimRegisterPreRefundInfoDto> getRegisterPreRefund(long claimNo) {

        ClaimRegisterPreRefundInfoDto claimRegisterPreRefundInfoDto = claimMapperService.getRegisterPreRefund(claimNo);

        log.debug("ClaimDetailInfoGetDto {}", claimRegisterPreRefundInfoDto);

        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimRegisterPreRefundInfoDto);
    }

    /**
     * 등록된 클레임 환불예정금액 조회
     *
     * @param claimNo
     * @return claimRegisterPreRefundInfoDto 등록된 클레임 환불예정금액
     */
    public ResponseObject<List<ClaimProcessListDto>> getClaimProcessListObject(long claimNo, String claimType) {

        String claimTypeNm = "";
        switch (claimType){
            case "C" :
                claimTypeNm = "취소";
                break;
            case "R" :
                claimTypeNm = "반품";
                break;
            case "X" :
                claimType = "교환";
                break;
        }
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, this.getClaimProcessList(claimNo, claimTypeNm));
    }

    /**
     * 등록된 클레임 환불예정금액 조회
     *
     * @param claimNo
     * @return claimRegisterPreRefundInfoDto 등록된 클레임 환불예정금액
     */
    public ResponseObject<List<ClaimHistoryListDto>> getClaimHistoryList(long claimNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimMapperService.getClaimHistoryList(claimNo));
    }

    /**
     * 반품/교환 신청 될 배송 상태 조회
     *
     * @param bundleNo
     * @return claimRegisterPreRefundInfoDto 등록된 클레임 환불예정금액
     */
    public ResponseObject<ClaimShipInfoDto> getClaimShipInfo(long bundleNo) throws Exception {

        ClaimShipInfoDto claimShipInfoDto = claimMapperService.getClaimShipInfo(bundleNo);
        claimShipInfoDto.setDeliveryYn(claimMapperService.getShipStatusForBundleNo(bundleNo));

        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimShipInfoDto);
    }


    /**
     * 클레임 사은행사 조회
     * @param claimNo
     * @return ClaimGiftItemListDto
     * **/
    public ResponseObject<List<ClaimGiftItemListDto>> claimGiftInfoList(long claimNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimMapperService.getClaimGiftItemList(claimNo));

    }

    /**
     * 클레임 행사정보 조회
     * @param claimNo
     * @return PromoInfoListDto
     * **/
    public ResponseObject<List<PromoInfoListDto>> getPromoInfoList(long claimNo) {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimMapperService.getPromoInfoList(claimNo));

    }

    /**
     * 클레임 처리내역 조회
     * @param claimNo
     * @return PromoInfoListDto
     * **/
    private List<ClaimProcessListDto> getClaimProcessList(long claimNo, String claimType) {
        ClaimProcessGetDto result = claimMapperService.getClaimProcess(claimNo);
        List<ClaimProcessListDto> claimProcessList = new ArrayList<>();
        ClaimProcessListDto dto;

        /* 1.신청 */
        if(!StringUtils.isEmpty(result.getRequestId())){
            dto = new ClaimProcessListDto(claimType + "신청", result.getRequestDt(), result.getRequestChannel(), result.getRequestId());
            claimProcessList.add(dto);
        }
        /* 2.승인 */
        if(!StringUtils.isEmpty(result.getApproveId())){
            dto = new ClaimProcessListDto(claimType + "승인", result.getApproveDt(), result.getApproveChannel(), result.getApproveId());
            claimProcessList.add(dto);
        }

        if(!claimType.equalsIgnoreCase("취소")){

            /* 7.수거요청 */
            if(!StringUtils.isEmpty(result.getPickRegDt())){
                dto = new ClaimProcessListDto("수거요청",result.getPickRegDt(),result.getPickRegChannel(), result.getPickRegId());
                claimProcessList.add(dto);
            }

            /* 8.수거완료 */
            if(!StringUtils.isEmpty(result.getPickCompleteDt())){
                dto = new ClaimProcessListDto("수거완료",result.getPickCompleteDt(),result.getPickCompleteChannel(), result.getPickCompleteId());
                claimProcessList.add(dto);
            }

            /* 9.배송요청 */
            if(!StringUtils.isEmpty(result.getExchRegDt())){
                dto = new ClaimProcessListDto("교환배송",result.getExchRegDt(),result.getExchRegChannel(), result.getExchRegId());
                claimProcessList.add(dto);
            }

            /* 10.수거완료 */
            if(!StringUtils.isEmpty(result.getExchCompleteDt())){
                dto = new ClaimProcessListDto("배송완료", result.getExchCompleteDt(), result.getExchCompleteChannel(), result.getExchCompleteId());
                claimProcessList.add(dto);
            }
        }

        if(!claimType.equalsIgnoreCase("교환")){
            List<ClaimPaymentProcessGetDto> claimPaymentList = claimMapperService.getClaimPaymentProcess(claimNo);

            for(ClaimPaymentProcessGetDto paymentDto : claimPaymentList){
                if(!StringUtils.isEmpty(paymentDto.getRegDt())){
                    dto = new ClaimProcessListDto(paymentDto.getPaymentType() + " | 환불대기", paymentDto.getRegDt(), "SYSTEM", "SYSTEM");
                    claimProcessList.add(dto);
                }
                 if(!StringUtils.isEmpty(paymentDto.getPayCompleteDt())){
                    dto = new ClaimProcessListDto(paymentDto.getPaymentType() + " | 환불완료", paymentDto.getPayCompleteDt(), paymentDto.getChgChannel(), paymentDto.getChgId());
                    claimProcessList.add(dto);
                }

            }
        }

        /* 3.보류 */
        if(!StringUtils.isEmpty(result.getPendingId())){
            dto = new ClaimProcessListDto(claimType + "보류", result.getPendingDt(),result.getPendingChannel(), result.getPendingId());
            claimProcessList.add(dto);
        }

        /* 4.철회 */
        if(!StringUtils.isEmpty(result.getWithdrawId())){
            dto = new ClaimProcessListDto(claimType + "철회", result.getWithdrawDt(),result.getWithdrawChannel(), result.getWithdrawId());
            claimProcessList.add(dto);
        }
        /* 5.거부 */
        if(!StringUtils.isEmpty(result.getRejectId())){
            dto = new ClaimProcessListDto(claimType + "거부",result.getRejectDt(),result.getRejectChannel(), result.getRejectId());
            claimProcessList.add(dto);
        }

        /* 6.완료 */
        if(!StringUtils.isEmpty(result.getCompleteId())){
            dto = new ClaimProcessListDto(claimType + "완료",result.getCompleteDt(),result.getCompleteChannel(), result.getCompleteId());
            claimProcessList.add(dto);
        }
        return claimProcessList;
    }

    /**
     * 점포정보 조회
     *
     * @param storeId
     * @return 점포정보 조회
     */
    public ResponseObject<StoreInfoGetDto> getStoreInfo(String storeId) throws Exception {
        return OrderResponseFactory.getResponseObject(OrderCode.ORDER_SUCCESS, claimMapperService.getStoreInfo(storeId));
    }

}
