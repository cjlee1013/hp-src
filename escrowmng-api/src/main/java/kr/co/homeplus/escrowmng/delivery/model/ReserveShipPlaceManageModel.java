package kr.co.homeplus.escrowmng.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "선물세트 발송지 관리")
@Getter
@Setter
@EqualsAndHashCode
public class ReserveShipPlaceManageModel {
    @ApiModelProperty(value = "발송일자(YYYY-MM-DD)", position = 1)
    @NotEmpty(message = "발송일자")
    private String shipDt;

    @ApiModelProperty(value = "발송장소점포여부", position = 2)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "발송장소점포여부(Y|N)")
    private String shipPlaceStoreYn;

    @ApiModelProperty(value = "발송장소안성여부", position = 3)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "발송장소안성여부(Y|N)")
    private String shipPlaceAnseongYn;

    @ApiModelProperty(value = "발송장소함안여부", position = 4)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "발송장소함안여부(Y|N)")
    private String shipPlaceHamanYn;

    @ApiModelProperty(value = "등록자ID", position = 5)
    private String regId;

    @ApiModelProperty(value = "등록일자", position = 6)
    private String regDt;

    @ApiModelProperty(value = "수정자ID", position = 7)
    private String chgId;

    @ApiModelProperty(value = "수정일자", position = 8)
    private String chgDt;
}
