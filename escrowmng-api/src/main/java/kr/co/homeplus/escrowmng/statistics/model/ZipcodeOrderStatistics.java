package kr.co.homeplus.escrowmng.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "우편번호별 주문현황 DTO")
@Getter
@Setter
public class ZipcodeOrderStatistics {
    @ApiModelProperty(value = "점포유형", position = 1)
    private String storeType;

    @ApiModelProperty(value = "점포명", position = 2)
    private String storeNm;

    @ApiModelProperty(value = "우편번호", position = 3)
    private String zipcode;

    @ApiModelProperty(value = "시/도", position = 4)
    private String sidoNm;

    @ApiModelProperty(value = "시군구", position = 5)
    private String sigunguNm;

    @ApiModelProperty(value = "주문건수", position = 6)
    private long orderCnt;

    @ApiModelProperty(value = "주문금액", position = 7)
    private long orderPrice;

    @ApiModelProperty(value = "1 Shift 주문건수", position = 8)
    private long shift1OrderCnt;

    @ApiModelProperty(value = "1 Shift 주문금액", position = 9)
    private long shift1OrderPrice;

    @ApiModelProperty(value = "2 Shift 주문건수", position = 10)
    private long shift2OrderCnt;

    @ApiModelProperty(value = "2 Shift 주문금액", position = 11)
    private long shift2OrderPrice;

    @ApiModelProperty(value = "3 Shift 주문건수", position = 12)
    private long shift3OrderCnt;

    @ApiModelProperty(value = "3 Shift 주문금액", position = 13)
    private long shift3OrderPrice;

    @ApiModelProperty(value = "4 Shift 주문건수", position = 14)
    private long shift4OrderCnt;

    @ApiModelProperty(value = "4 Shift 주문금액", position = 15)
    private long shift4OrderPrice;

    @ApiModelProperty(value = "5 Shift 주문건수", position = 16)
    private long shift5OrderCnt;

    @ApiModelProperty(value = "5 Shift 주문금액", position = 17)
    private long shift5OrderPrice;

    @ApiModelProperty(value = "6 Shift 주문건수", position = 18)
    private long shift6OrderCnt;

    @ApiModelProperty(value = "6 Shift 주문금액", position = 19)
    private long shift6OrderPrice;

    @ApiModelProperty(value = "7 Shift 주문건수", position = 20)
    private long shift7OrderCnt;

    @ApiModelProperty(value = "7 Shift 주문금액", position = 21)
    private long shift7OrderPrice;

    @ApiModelProperty(value = "8 Shift 주문건수", position = 22)
    private long shift8OrderCnt;

    @ApiModelProperty(value = "8 Shift 주문금액", position = 23)
    private long shift8OrderPrice;

    @ApiModelProperty(value = "9 Shift 주문건수", position = 24)
    private long shift9OrderCnt;

    @ApiModelProperty(value = "9 Shift 주문금액", position = 25)
    private long shift9OrderPrice;

    /** getter 재정의 */
    public long getOrderCnt() {
        return this.shift1OrderCnt + this.shift2OrderCnt + this.shift3OrderCnt + this.shift4OrderCnt + this.shift5OrderCnt
                + this.shift6OrderCnt + this.shift7OrderCnt + this.shift8OrderCnt + this.shift9OrderCnt;
    }
    public long getOrderPrice() {
        return this.shift1OrderPrice + this.shift2OrderPrice + this.shift3OrderPrice + this.shift4OrderPrice + this.shift5OrderPrice
                + this.shift6OrderPrice + this.shift7OrderPrice + this.shift8OrderPrice + this.shift9OrderPrice;
    }
}
