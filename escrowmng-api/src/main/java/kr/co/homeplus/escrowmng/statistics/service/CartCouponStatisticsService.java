package kr.co.homeplus.escrowmng.statistics.service;

import kr.co.homeplus.escrowmng.statistics.mapper.CartCouponSlaveMapper;
import kr.co.homeplus.escrowmng.statistics.model.CartCouponStatisticsModel;
import kr.co.homeplus.escrowmng.statistics.model.CartCouponStatisticsSelectModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CartCouponStatisticsService {

    private final CartCouponSlaveMapper cartCouponSlaveMapper;

    /**
     * 장바구니쿠폰 통계 조회(점포별)
     * @param cartCouponStatisticsSelectModel
     * @return
     */
    public List<CartCouponStatisticsModel> getCartCouponStatistics(CartCouponStatisticsSelectModel cartCouponStatisticsSelectModel) {
        return cartCouponSlaveMapper.getCartCouponStatistics(cartCouponStatisticsSelectModel);
    }

    /**
     * 장바구니쿠폰 통계 조회(일자별)
     * @param cartCouponStatisticsSelectModel
     * @return
     */
    public List<CartCouponStatisticsModel> getCartCouponStatisticsByStoreId(CartCouponStatisticsSelectModel cartCouponStatisticsSelectModel) {
        return cartCouponSlaveMapper.getCartCouponStatisticsByStoreId(cartCouponStatisticsSelectModel);
    }

}
