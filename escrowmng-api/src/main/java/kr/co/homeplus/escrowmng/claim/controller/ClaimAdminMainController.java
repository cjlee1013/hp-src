package kr.co.homeplus.escrowmng.claim.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.ClaimDetailInfoGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimDetailInfoListDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimDetailInfoSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimGiftItemListDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimHistoryListDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimInfoListGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimInfoListSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimProcessListDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimReasonInfoGetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimReasonInfoSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimRegisterPreRefundInfoDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimShipInfoDto;
import kr.co.homeplus.escrowmng.claim.model.DiscountInfoListDto;
import kr.co.homeplus.escrowmng.claim.model.PromoInfoListDto;
import kr.co.homeplus.escrowmng.claim.model.StoreInfoGetDto;
import kr.co.homeplus.escrowmng.claim.service.ClaimAdminMainService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/claim")
@RequiredArgsConstructor
@Api(tags = "클레임관리 > 클레임 관리 > 조회")
public class ClaimAdminMainController {

  private final ClaimAdminMainService claimAdminMainService;

  @PostMapping("/getClaimList")
  @ApiOperation(value = "클레임 리스트 조회")
  public ResponseObject<List<ClaimInfoListGetDto>> getClaimList(@RequestBody ClaimInfoListSetDto claimInfoListSetDto) {
    return claimAdminMainService.getClaimInfoList(claimInfoListSetDto);
  }

  @PostMapping("/getClaimDetailInfo")
  @ApiOperation(value = "클레임 상세정보 조회")
  public ResponseObject<ClaimDetailInfoGetDto> getClaimDetailInfo(@RequestBody ClaimDetailInfoSetDto claimDetailInfoSetDto) {
    return claimAdminMainService.getClaimDetailInfo(claimDetailInfoSetDto);
  }

  @PostMapping("/getClaimDetailInfoList")
  @ApiOperation(value = "클레임 상세정보 리스트 조회")
  public ResponseObject<List<ClaimDetailInfoListDto>> getClaimDetailInfoList(@RequestBody ClaimDetailInfoSetDto claimDetailInfoSetDto) {
    return claimAdminMainService.getClaimDetailInfoList(claimDetailInfoSetDto);
  }

  @PostMapping("/getClaimReasonInfo")
  @ApiOperation(value = "클레임 상세사유 조회")
  public ResponseObject<ClaimReasonInfoGetDto> getCancelReasonInfo(@RequestBody ClaimReasonInfoSetDto claimReasonInfoSetDto) {
    return claimAdminMainService.getClaimReasonInfo(claimReasonInfoSetDto);
  }

  @PostMapping("/getDiscountInfoList")
  @ApiOperation(value="차감 할인정보 조회")
  public ResponseObject<List<DiscountInfoListDto>> getDiscountInfoList(@RequestParam("claimNo") long claimNo){
    return claimAdminMainService.getDiscountInfoList(claimNo);
  }

  @PostMapping("/getPromoInfoList")
  @ApiOperation(value="차감 행사정보 조회")
  public ResponseObject<List<PromoInfoListDto>> getPromoInfoList(@RequestParam("claimNo") long claimNo){
    return claimAdminMainService.getPromoInfoList(claimNo);
  }

  @PostMapping("/getRegisterPreRefund")
  @ApiOperation(value="등록된 클레임 환불예정금액 조회")
  public ResponseObject<ClaimRegisterPreRefundInfoDto> getRegisterPreRefund(@RequestParam("claimNo") long claimNo){
    return claimAdminMainService.getRegisterPreRefund(claimNo);
  }

  @PostMapping("/getClaimProcessList")
  @ApiOperation(value="등록된 클레임 환불예정금액 조회")
  public ResponseObject<List<ClaimProcessListDto>> getClaimProcessList(@RequestParam("claimNo") long claimNo, @RequestParam("claimType") String claimType){
    return claimAdminMainService.getClaimProcessListObject(claimNo, claimType);
  }

  @PostMapping("/getClaimHistoryList")
  @ApiOperation(value="등록된 클레임 환불예정금액 조회")
  public ResponseObject<List<ClaimHistoryListDto>> getClaimHistoryList(@RequestParam("claimNo") long claimNo){
    return claimAdminMainService.getClaimHistoryList(claimNo);
  }

  @PostMapping("/claimGiftInfoList")
  @ApiOperation(value="클레임 사은행사 조회")
  public ResponseObject<List<ClaimGiftItemListDto>> claimGiftInfoList(@RequestParam(value = "claimNo") long claimNo){
    return claimAdminMainService.claimGiftInfoList(claimNo);
  }

  @GetMapping(value = "/getClaimShipInfo")
  @ApiOperation(value="반품/교환 신청용 배송 정보 조회")
  public ResponseObject<ClaimShipInfoDto> getClaimShipInfo(@RequestParam(name = "bundleNo") long bundleNo) throws Exception {
    return claimAdminMainService.getClaimShipInfo(bundleNo);
  }

  @GetMapping("/getStoreInfo")
  @ApiOperation(value = "점포정보 조회", response = StoreInfoGetDto.class)
  public ResponseObject<StoreInfoGetDto> getStoreInfo(@RequestParam(value = "storeId") String storeId) throws Exception {
      return claimAdminMainService.getStoreInfo(storeId);
  }

}
