package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ClaimBundleInfoList {

    @ApiModelProperty(value = "주문번호", position = 1)
    public long purchaseOrderNo;

    @ApiModelProperty(value = "번들번호", position = 2)
    public long bundleNo;

}
