package kr.co.homeplus.escrowmng.statistics.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowBatchConnection;
import kr.co.homeplus.escrowmng.statistics.model.CouponStatistics;
import kr.co.homeplus.escrowmng.statistics.model.CouponStatisticsSelect;

@EscrowBatchConnection
public interface CouponStatisticsSlaveMapper {

    List<CouponStatistics> selectCouponStatisticsList(CouponStatisticsSelect couponStatisticsSelect);

    List<CouponStatistics> selectItemCouponStatisticsDetailList(CouponStatisticsSelect couponStatisticsSelect);

    List<CouponStatistics> selectCartCouponStatisticsDetailList(CouponStatisticsSelect couponStatisticsSelect);
}