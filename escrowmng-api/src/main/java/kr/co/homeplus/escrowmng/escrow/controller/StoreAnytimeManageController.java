package kr.co.homeplus.escrowmng.escrow.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.escrow.model.StoreAnytimeManageDto;
import kr.co.homeplus.escrowmng.escrow.model.StoreAnytimeManageSelectDto;
import kr.co.homeplus.escrowmng.escrow.model.StoreAnytimeShiftManageDto;
import kr.co.homeplus.escrowmng.escrow.service.StoreAnytimeManageService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/escrow/anytime")
@Api(tags = "상품관리 > 점포관리 > 점포운영관리")
public class StoreAnytimeManageController {
    private final StoreAnytimeManageService storeAnytimeManageService;

    /**
     * 점포 애니타임 정보 조회
     */
    @PostMapping("/getStoreAnytimeInfo")
    @ApiOperation(value = "점포 애니타임 정보 조회")
    public ResponseObject<List<StoreAnytimeManageDto>> getStoreAnytimeInfo(@Valid @RequestBody StoreAnytimeManageSelectDto storeAnytimeManageSelectDto) {
        return ResourceConverter.toResponseObject(storeAnytimeManageService.getStoreAnytimeInfo(storeAnytimeManageSelectDto));
    }

    /**
     * 점포 애니타임 Shift 정보 조회
     */
    @GetMapping("/getStoreAnytimeShiftInfo")
    @ApiOperation(value = "점포 애니타임 Shift 정보 조회")
    public ResponseObject<List<StoreAnytimeShiftManageDto>> getStoreAnytimeShiftInfo(@RequestParam(value = "storeId") int storeId) {
        return ResourceConverter.toResponseObject(storeAnytimeManageService.getStoreAnytimeShiftInfo(storeId));
    }

    /**
     * 점포 애니타임 사용여부 저장
     */
    @PostMapping("/saveStoreAnytimeUseYn")
    @ApiOperation(value = "점포 애니타임 사용여부 저장")
    public ResponseObject<Integer> saveStoreAnytimeUseYn(@RequestBody StoreAnytimeManageDto storeAnytimeManageDto) {
        return ResourceConverter.toResponseObject(storeAnytimeManageService.saveStoreAnytimeUseYn(storeAnytimeManageDto));
    }

    /**
     * 점포 애니타임 Shift 사용여부 저장
     */
    @PostMapping("/saveStoreAnytimeShiftUseYn")
    @ApiOperation(value = "점포 애니타임 Shift 사용여부 저장")
    public ResponseObject<Integer> saveStoreAnytimeShiftUseYn(@RequestBody List<StoreAnytimeShiftManageDto> storeAnytimeShiftManageDtoList) {
        return ResourceConverter.toResponseObject(storeAnytimeManageService.saveStoreAnytimeShiftUseYn(storeAnytimeShiftManageDtoList));
    }
}
