package kr.co.homeplus.escrowmng.claim.model.entity;

public class ClaimAddItemEntity {

    // 클레임추가상품번호
    public String claimAddItemNo;

    // 클레임상품번호
    public String claimItemNo;

    // 주문추가상품번호
    public String orderAddItemNo;

    // 주문상품번호
    public String orderItemNo;

    // 추가구성번호(상품Key값)
    public String addOptNo;

    // 추가구성금액
    public String addOptPrice;

    // 클레임 추가구성수량
    public String claimAddOptQty;

    // 등록일시
    public String regDt;
}
