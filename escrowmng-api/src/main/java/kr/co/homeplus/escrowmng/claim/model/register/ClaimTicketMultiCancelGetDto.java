package kr.co.homeplus.escrowmng.claim.model.register;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ApiModel(description = "일괄취소 결과.")
public class ClaimTicketMultiCancelGetDto {
    private long successCnt;
    private long failCnt;


    public ClaimTicketMultiCancelGetDto(long successCnt, long failCnt){
        this.successCnt = successCnt;
        this.failCnt = failCnt;
    }
}
