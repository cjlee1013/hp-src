package kr.co.homeplus.escrowmng.core.exception.handler;

import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collections;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.exception.BusinessLogicException;
import kr.co.homeplus.escrowmng.core.exception.LogicException;
import kr.co.homeplus.escrowmng.core.exception.ExternalException;
import kr.co.homeplus.escrowmng.enums.ExceptionCode;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.exception.ResourceClientException;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.servlet.NoHandlerFoundException;

@Slf4j
@ControllerAdvice
public class ExceptionControllerAdvice {

    /**
     * 에러 전달시 사용할 HttpHeaders 생성<p>
     * <p>
     * char : UTF-8<br> MediaType : application/json
     *
     * @return HttpHeaders
     */
    private HttpHeaders getDefaultHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        MediaType mediaType = new MediaType(MediaType.APPLICATION_JSON_UTF8, StandardCharsets.UTF_8);
        httpHeaders.setContentType(mediaType);
        return httpHeaders;
    }

    /**
     * {@link Throwable}로 발생되는 예외를 처리하는 메소드로 명시적이지 않는 모든 예외를 처리한다.
     * <p>응답 내용은 에러내용을 담은 {@link ResponseObject} 와 {@link HttpStatus} 를 전달한다.
     *
     * @param req HttpServletRequest
     * @param ex  Exception
     * @return json 타입으로 return
     * @see ResponseObject
     */
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ResponseObject<String>> handleServerError(final HttpServletRequest req, final Exception ex) {
        // 응답할 ResponseObject 설정
        ResponseObject<String> responseObject = setResponseObjectForException(ex,
            HttpStatus.INTERNAL_SERVER_ERROR);
        log.error("Throwable Error!!! uri: {}, trace: {}", req.getRequestURI(),
            ExceptionUtils.getStackTrace(ex));

        return new ResponseEntity<>(responseObject, getDefaultHttpHeaders(),
            HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Resttemplate 사용 시 예외를 처리하는 핸들러<p> <p/> 전달되는 {@link Exception}의 type에 따라 리턴되는 값이 변경된다. 세부내용은 하단의 내용을 참조한다. <p/>
     * <pre>
     * 1. {@link RestClientResponseException}이 발생 할 경우
     *  - 리턴 : status, ResponseBody 그대로 전달
     *
     * 2. 기타 그외의 exception
     *  - 리턴 : status만 변경(500), {@link ResponseObject}를 전달
     *   * unknownHostException, IOException 등의 기타 Exception 처리
     *   * 에러 내용을 {@link ResponseObject}로 담아 전달
     * </pre>
     *
     * @param req HttpServletRequest
     * @param res HttpServletResponse
     * @param ex  ResourceClientException
     * @return responseEntity
     * @see RestClientResponseException
     * @see kr.co.homeplus.plus.api.support.client.ResourceClient;
     * @see ResponseObject
     */
    @ExceptionHandler(ResourceClientException.class)
    public HttpEntity<?> handleResourceClientException(final HttpServletRequest req, final HttpServletResponse res, final ResourceClientException ex) {

        Throwable cause = ex.getCause();
        /*
         * 1. RestClientResponseException이 발생 할 경우
         * 리턴 : status, ResponseBody 그대로 전달
         */
        if (cause instanceof RestClientResponseException) {
            res.setStatus(ex.getHttpStatus());

            log.error(
                "ResourceClientException Error!!! uri:{}, resourceUrl:{}, httpStatus:{}, getResponseBody:{}",
                req.getRequestURI(),
                ex.getResourceUrl(), ex.getHttpStatus(),
                ((RestClientResponseException) cause).getResponseBodyAsString());
            /*
             * 2. 기타 그외의 exception
             * 리턴 : status 지정(500), 에러 내용은 ResponseObject로 담아 전달
             */
        } else {
            res.setStatus(500);
            ResponseObject<String> responseObject = setResponseObjectForException(ex,
                HttpStatus.INTERNAL_SERVER_ERROR);

            log.error(
                "ResourceClientException Error!!! uri:{}, resourceUrl:{}, httpStatus:{}, getResponseObject:{}, trace:{}",
                req.getRequestURI(),
                ex.getResourceUrl(), ex.getHttpStatus(), responseObject,
                ExceptionUtils.getStackTrace(ex));

            return new HttpEntity<>(responseObject, getDefaultHttpHeaders());
        }

        // 추후 해당부분 세부분석 필요
        return new HttpEntity<>(ex.getResponseBody(), getDefaultHttpHeaders());
    }


    /**
     * 404 error not found 예외 처리<p>
     *
     * @param req HttpServletRequest
     * @param ex  예외
     * @return json 타입으로 return
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ResponseObject<String>> NoHandlerFoundException(final HttpServletRequest req, final Exception ex) {
        log.warn("404 Not Found Error!!! <uri:{}>, <RequestMethod:{}>, <HttpStatus:{}>", req.getRequestURI(), req.getMethod(),
            HttpStatus.NOT_FOUND.value());
        ResponseObject<String> responseObject = setResponseObjectForException(ex, HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(responseObject, getDefaultHttpHeaders(), HttpStatus.NOT_FOUND);
    }

    /**
     * 에러 발생 시 리턴 할 {@link ResponseObject}를 spec에 맞춰 생성해주는 메소드
     *
     * @param ex         Exception
     * @param httpStatus ResponseObject 내에 입력할 HttpStatus
     * @return ResponseObject
     */
    private ResponseObject<String> setResponseObjectForException(final Exception ex, final HttpStatus httpStatus) {
        Exception processedException = new Exception(ex.getMessage());
        processedException.setStackTrace(Arrays.copyOf(ex.getStackTrace(), 3));

        return ResponseObject.Builder.<String>builder().status(httpStatus)
            .data(ExceptionUtils.getStackTrace(processedException)).returnCode("01")
            .returnMessage(ExceptionUtils.getMessage(processedException)).build();
    }

    /* CUSTOM EXCEPTION -- S */
    /**
     * 비즈니스 로직 Validation Exception 분리
     *
     * @param ble
     * @see BusinessLogicException
     */
    @ExceptionHandler(BusinessLogicException.class)
    public ResponseEntity<ResponseObject<Object>> handleBusinessLogicException(BusinessLogicException ble) {
        Object data = ObjectUtils.isEmpty(ble.getData()) ? Collections.EMPTY_LIST : ble.getData();
        ResponseObject<Object> responseObject =  ResponseObject.Builder.builder()
            .status(HttpStatus.UNPROCESSABLE_ENTITY)
            .returnCode(ble.getErrorCode())
            .returnMessage(ExceptionControllerAdvice.getErrorMsgReplace(ble.getDescription(), ble.getErrorMsg(), ble.getErrorMsg2()))
            .data(data).build();
        return new ResponseEntity<>(responseObject, getDefaultHttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
    }
    /**
     * 정보제공 API Exception 분리
     *
     * @param ee
     * @see ExternalException
     */
    @ExceptionHandler(ExternalException.class)
    public ResponseEntity<?> handleExternalException(ExternalException ee) {
        String returnMessage = getErrorMsgReplace(ee.getErrorDescription(), ee.getErrorMsg());
        ResponseObject responseObject = ResourceConverter.toResponseObject(
            ee.getData(), HttpStatus.UNPROCESSABLE_ENTITY, ee.getErrorCode(), returnMessage
        );
        log.info("ExternalException 오류! Code:{}, Message:{}, data:{}", ee.getErrorCode(), returnMessage, ee.getData());
        return new ResponseEntity<>(responseObject, getDefaultHttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(LogicException.class)
    public ResponseEntity<ResponseObject<?>> handleClaimLogicException(LogicException cle) {
        log.info("LogicException [\nExceptionInfo :: {}\nReturnCode :: {}\nReturnMessage :: {}]", convertExceptionMessage(cle.getStackTrace()[0]), cle.getResponseCode(), cle.getResponseMsg());
        return new ResponseEntity<>(ResponseObject.Builder.builder()
            .status(HttpStatus.OK)
            .returnCode(cle.getResponseCode())
            .returnMessage(cle.getResponseMsg())
            .data(cle.getData())
            .build(), getDefaultHttpHeaders(), HttpStatus.OK);
    }

    /* CUSTOM EXCEPTION -- E */

    /**
     * DTO Validation Exception 처리
     * @param e
     * @param request
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ResponseObject<String>> handleMethodArgumentNotValidException(MethodArgumentNotValidException e, HttpServletRequest request) {
        return this.getHandleException(e.getBindingResult(), request);
    }

    @ExceptionHandler(SQLSyntaxErrorException.class)
    public ResponseEntity<ResponseObject<?>> handleSQLSyntaxErrorException(SQLSyntaxErrorException e) {
        log.error("SQLSyntax ERROR[\nsourceInfo : {} \nErrorMessage : {}\n]", this.convertExceptionMessage(e.getStackTrace()[0]), e.getMessage());
        return new ResponseEntity<>(ResponseObject.Builder.builder()
            .status(HttpStatus.OK)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9004.getResponseCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9004.getResponseMessage())
            .data(null)
            .build(), getDefaultHttpHeaders(), HttpStatus.OK);
    }

    @ExceptionHandler(BadSqlGrammarException.class)
    public ResponseEntity<ResponseObject<String>> handleMethodBadSqlGrammarException(BadSqlGrammarException e, HttpServletRequest request) {
        log.error("SQL BadSqlGrammar ERROR[\nsourceInfo : {} \nErrorMessage : {}\n]", this.convertExceptionMessage(e.getStackTrace()[0]), e.getMessage());
        return new ResponseEntity<>(ResponseObject.Builder.<String>builder()
            .status(HttpStatus.OK)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9004.getResponseCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9004.getResponseMessage())
            .data(null)
            .build(), getDefaultHttpHeaders(), HttpStatus.OK);
    }

    /**
     * Database SQL Exception handler
     *
     * @param req HttpServletRequest
     * @param ex SQLException
     */
    @ExceptionHandler(value = {SQLException.class, DataAccessException.class})
    public ResponseEntity<ResponseObject<Void>> sqlExceptionHandler(
        final HttpServletRequest req, final SQLException ex) {

        ResponseObject<Void> responseObject = ResponseObject.Builder.<Void>builder()
            .status(HttpStatus.OK)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9004.getResponseCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9004.getResponseMessage())
            .data(null)
            .build();

        log.error("An Error occurred SQL Exception!!! ErrorCode: {}. ({}), uri: {}?{}, trace: {}",
            ex.getErrorCode(), ex.getMessage(), req.getRequestURI(),
            req.getQueryString(), ExceptionUtils.getStackTrace(ex));

        return new ResponseEntity<>(responseObject, getDefaultHttpHeaders(), HttpStatus.OK);
    }

    /**
     * DTO Validation Exception 처리
     * @param e
     * @param request
     * @return
     */
    @ExceptionHandler(BindException.class)
    public ResponseEntity<ResponseObject<String>> handleBindException(BindException e, HttpServletRequest request) {
        return this.getHandleException(e.getBindingResult(), request);
    }

    private ResponseEntity<ResponseObject<String>> getHandleException(BindingResult br, HttpServletRequest request) {
        return this.getHandleResponseEntity(br.getFieldError(), request);
    }

    private ResponseEntity<ResponseObject<String>> getHandleResponseEntity(final FieldError fieldError, HttpServletRequest request) {
        String returnCode;
        String returnMessage;

        if (fieldError != null) {
            StringBuilder errorMessage = new StringBuilder();
            String errorCode;
            String errorField = fieldError.getField();
            String codeType = StringUtils.defaultString(fieldError.getCode());
            String errorDefMsg = fieldError.getDefaultMessage();
            Object[] errorArguments = org.apache.commons.lang3.ObjectUtils.defaultIfNull(fieldError.getArguments(), new Object[]{});
            Object errorArguments1  = errorArguments.length > 1 ? errorArguments[1] : "";
            Object errorArguments2  = errorArguments.length > 2 ? errorArguments[2] : "";

            switch (codeType) {
                case "typeMismatch":
                    errorCode = ExceptionCode.ERROR_CODE_1004.getCode();
                    errorMessage.append("(").append(errorField).append(")").append(ExceptionCode.ERROR_CODE_1004.getDescription());
                    break;
                case "NotNull":
                case "NotEmpty":
                case "NotBlank":
                    errorCode = ExceptionCode.ERROR_CODE_1002.getCode();
                    errorMessage.append("(").append(errorDefMsg).append(")").append(ExceptionCode.ERROR_CODE_1002.getDescription());
                    break;
                case "Range":
                case "Length":
                case "Size":
                    if (!ObjectUtils.isEmpty(errorArguments2) && (int) errorArguments2 > 0) {
                        errorCode = ExceptionCode.ERROR_CODE_1005.getCode();
                        errorMessage.append("(").append(errorDefMsg).append(")");
                        errorMessage.append(errorArguments2).append("~").append(errorArguments1).append(ExceptionCode.ERROR_CODE_1005.getDescription());

                    } else {
                        errorCode = ExceptionCode.ERROR_CODE_1007.getCode();
                        errorMessage.append("(").append(errorDefMsg).append(")");
                        errorMessage.append(errorArguments1).append(ExceptionCode.ERROR_CODE_1007.getDescription());
                    }
                    break;
                case "Date":
                case "Pattern":
                case "AllowInput":
                case "NotAllowInput":
                    errorCode = ExceptionCode.ERROR_CODE_1021.getCode();
                    errorMessage.append("(").append(errorDefMsg);
                    if (errorArguments1 instanceof Boolean && errorArguments2 instanceof String[]) {
                        if (!(boolean) errorArguments1) {
                            switch (codeType) {
                                case "NotAllowInput":
                                    errorMessage.append(" ※불가 문자: ");
                                    break;
                                default:
                                    errorMessage.append(" ※허용 문자: ");
                                    break;
                            }
                            errorMessage.append(PatternConstants.getConstantsMessage((String[])errorArguments2));
                        }
                    }
                    errorMessage.append(")").append(ExceptionCode.ERROR_CODE_1021.getDescription());
                    break;
                case "EnterNotEmpty":
                case "NotEnter":
                    errorCode = ExceptionCode.ERROR_CODE_1090.getCode();
                    errorMessage.append("(").append(errorDefMsg).append(")").append(ExceptionCode.ERROR_CODE_1090.getDescription().replace("(%d)", ""));
                    break;
                case "Min":
                    errorCode = ExceptionCode.ERROR_CODE_1028.getCode();
                    errorMessage.append("(").append(errorDefMsg).append(")").append(errorArguments1).append(ExceptionCode.ERROR_CODE_1028.getDescription());
                    break;
                case "Max":
                    errorCode = ExceptionCode.ERROR_CODE_1029.getCode();
                    errorMessage.append("(").append(errorDefMsg).append(")").append(errorArguments1).append(ExceptionCode.ERROR_CODE_1029.getDescription());
                    break;
                case "DecimalMin":
                    errorCode = ExceptionCode.ERROR_CODE_1022.getCode();
                    errorMessage.append("(").append(errorDefMsg).append(")").append(fieldError.getRejectedValue()).append(ExceptionCode.ERROR_CODE_1022.getDescription());
                    break;
                case "DecimalMax":
                    errorCode = ExceptionCode.ERROR_CODE_1023.getCode();
                    errorMessage.append("(").append(errorDefMsg).append(")").append(fieldError.getRejectedValue()).append(ExceptionCode.ERROR_CODE_1023.getDescription());
                    break;
                case "Digits":
                    errorCode = ExceptionCode.ERROR_CODE_1001.getCode();
                    errorMessage
                        .append("(")
                        .append(errorDefMsg)
                        .append(") 숫자:")
                        .append(errorArguments1)
                        .append("자리, 소수점:").append(errorArguments2)
                        .append("자리 까지")
                        .append(ExceptionCode.ERROR_CODE_1001.getDescription());
                    break;
                default:
                    errorCode = ExceptionCode.ERROR_CODE_1001.getCode();
                    errorMessage.append("(").append(errorDefMsg).append(")").append(ExceptionCode.ERROR_CODE_1001.getDescription());
                    break;
            }

            returnCode = errorCode;
            returnMessage = errorMessage.toString();
        } else {
//            logService.setBasicErrorLog("getHandleResponseEntityNULL", HttpStatus.UNPROCESSABLE_ENTITY.value(), GlobalExceptionHandler.getRequestInfo(request), error.toString());
            returnCode = ExceptionCode.SYS_ERROR_CODE_9001.getCode();
            returnMessage = ExceptionCode.SYS_ERROR_CODE_9001.getDescription();
        }

        return new ResponseEntity<>(ResponseObject.Builder.<String>builder()
            .status(HttpStatus.UNPROCESSABLE_ENTITY)
            .returnCode(returnCode)
            .returnMessage(returnMessage).build(), getDefaultHttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    private static String getErrorMsgReplace(String description, String msg) {
        return ExceptionControllerAdvice.getErrorMsgReplace(description, msg, "");
    }

    /**
     * ExceptionCode 에 있는 메세지 치환
     * @param description
     * @param msg
     * @param msg2
     * @return
     */
    private static String getErrorMsgReplace(String description, String msg, String msg2) {
        try {
            String retMsg;
            if (StringUtils.isBlank(msg)) {
                retMsg = description;
            } else {
                retMsg = description.replace("%d", msg);
            }

            if (!StringUtils.isBlank(msg2)) {
                retMsg = retMsg.replace("%s", msg2);
            }

             return retMsg;
        } catch(Exception e) {
            return StringUtils.isBlank(msg) ? description : "(" + msg + ")" + description;
        }
    }

    private String convertExceptionMessage(StackTraceElement ste){
        return MessageFormat.format("{0}::{1}:{2}", ste.getFileName(), ste.getMethodName(), ste.getLineNumber());
    }
}
