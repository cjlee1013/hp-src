package kr.co.homeplus.escrowmng.statistics.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.escrowmng.extract.service.ExtractHistoryService;
import kr.co.homeplus.escrowmng.statistics.model.CouponStatistics;
import kr.co.homeplus.escrowmng.statistics.model.CouponStatisticsSelect;
import kr.co.homeplus.escrowmng.statistics.model.DiscountStatistics;
import kr.co.homeplus.escrowmng.statistics.model.DiscountStatisticsSelect;
import kr.co.homeplus.escrowmng.statistics.service.CouponStatisticsService;
import kr.co.homeplus.escrowmng.statistics.service.DiscountStatisticsService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/escrow/statistics")
@Api(tags = "통계 > 프로모션통계")
public class PromotionStatisticsController {

    private final DiscountStatisticsService discountStatisticsService;
    private final CouponStatisticsService couponStatisticsService;
    private final ExtractHistoryService extractHistoryService;

    @PostMapping("/getCardDiscountStatisticsList")
    @ApiOperation(value = "카드즉시할인통계조회", response = DiscountStatistics.class)
    public ResponseObject<List<DiscountStatistics>> getDiscountStatisticsList(@RequestBody DiscountStatisticsSelect discountStatisticsSelect) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(discountStatisticsSelect.getExtractCommonSetDto());
        return ResourceConverter.toResponseObject(discountStatisticsService.getDiscountStatisticsList(discountStatisticsSelect));
    }

    @PostMapping("/getCardDiscountStatisticsDetailList")
    @ApiOperation(value = "카드즉시할인통계상세조회", response = DiscountStatistics.class)
    public ResponseObject<List<DiscountStatistics>> getDiscountStatisticsDetailList(@RequestBody DiscountStatisticsSelect discountStatisticsSelect) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(discountStatisticsSelect.getExtractCommonSetDto());
        return ResourceConverter.toResponseObject(discountStatisticsService.getDiscountStatisticsDetailList(discountStatisticsSelect));
    }

    @PostMapping("/getCouponStatisticsList")
    @ApiOperation(value = "쿠폰통계조회", response = CouponStatistics.class)
    public ResponseObject<List<CouponStatistics>> getCouponStatisticsList(@RequestBody CouponStatisticsSelect couponStatisticsSelect) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(couponStatisticsSelect.getExtractCommonSetDto());
        return ResourceConverter.toResponseObject(couponStatisticsService.getCouponStatisticsList(couponStatisticsSelect));
    }

    @PostMapping("/getItemCouponStatisticsDetailList")
    @ApiOperation(value = "상품쿠폰통계조회", response = CouponStatistics.class)
    public ResponseObject<List<CouponStatistics>> getItemCouponDetailStatisticsList(@RequestBody CouponStatisticsSelect couponStatisticsSelect) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(couponStatisticsSelect.getExtractCommonSetDto());
        return ResourceConverter.toResponseObject(couponStatisticsService.getItemCouponStatisticsDetailList(couponStatisticsSelect));
    }

    @PostMapping("/getCartCouponStatisticsDetailList")
    @ApiOperation(value = "장바구니쿠폰통계조회", response = CouponStatistics.class)
    public ResponseObject<List<CouponStatistics>> getCartCouponDetailStatisticsList(@RequestBody CouponStatisticsSelect couponStatisticsSelect) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(couponStatisticsSelect.getExtractCommonSetDto());
        return ResourceConverter.toResponseObject(couponStatisticsService.getCartCouponStatisticsDetailList(couponStatisticsSelect));
    }
}
