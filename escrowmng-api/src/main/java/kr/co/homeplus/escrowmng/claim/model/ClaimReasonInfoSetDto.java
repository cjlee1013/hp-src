package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimReasonInfoSetDto {

  @ApiModelProperty(value = "클레임번호", position = 1)
  private long claimNo;

  @ApiModelProperty(value = "클레임번들번호", position = 2)
  private long claimBundleNo;

  @ApiModelProperty(value = "클레임타입", position = 3)
  public String claimType;
}
