package kr.co.homeplus.escrowmng.escrow.service;

import static kr.co.homeplus.escrowmng.escrow.constants.ZipcodeConstants.BUILD_MNG_NO;
import static kr.co.homeplus.escrowmng.escrow.constants.ZipcodeConstants.GIBUN;
import static kr.co.homeplus.escrowmng.escrow.constants.ZipcodeConstants.ROADADDR;
import static kr.co.homeplus.escrowmng.escrow.constants.ZipcodeConstants.ZIPCODE;

import java.util.List;
import kr.co.homeplus.escrowmng.escrow.mapper.ZipcodeMasterMapper;
import kr.co.homeplus.escrowmng.escrow.mapper.ZipcodeSlaveMapper;
import kr.co.homeplus.escrowmng.escrow.model.ZipcodeManageModel;
import kr.co.homeplus.escrowmng.escrow.model.ZipcodeManageSelectModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ZipcodeManageService {

    private final ZipcodeMasterMapper zipcodeMasterMapper;
    private final ZipcodeSlaveMapper zipcodeSlaveMapper;


    /**
     * 도로명 시도명 리스트 조회
     * @return
     */
    public List<String> getZipcodeSidoList() {
        return zipcodeSlaveMapper.getZipcodeSidoList();
    }

    /**
     * 도로명 시군구명 리스트 조회
     * @param sidoNm    시도명
     * @return
     */
    public List<String> getZipcodeSigunguList(String sidoNm) {
        return zipcodeSlaveMapper.getZipcodeSigunguList(sidoNm);
    }

    /**
     * 우편번호/도로명/지번 검색
     * @param zipcodeManageSelectModel
     * @return
     */
    public List<ZipcodeManageModel> getZipcodeManageList(ZipcodeManageSelectModel zipcodeManageSelectModel) {
        List<ZipcodeManageModel> zipcodeManageModelList;
        switch (zipcodeManageSelectModel.getSchKeywordType()) {
            // 도로명주소로 검색
            case ROADADDR:
                zipcodeManageModelList = zipcodeSlaveMapper.getZipcodeManageListForRoadAddr(zipcodeManageSelectModel);
                break;
            // 지번주소로 검색
            case GIBUN:
                zipcodeManageModelList = zipcodeSlaveMapper.getZipcodeManageListForGibun(zipcodeManageSelectModel);
                break;
            // 우편번호, 건물관리번호로 검색
            case ZIPCODE:
            case BUILD_MNG_NO:
            default:
                zipcodeManageModelList = zipcodeSlaveMapper.getZipcodeManageList(zipcodeManageSelectModel);
        }

        return zipcodeManageModelList;
    }

    /**
     * 도서산간여부(제주/도서산간/해당없음) 수정
     * @param zipcodeManageModel
     * @return
     */
    public int updateZipcodeIslandType(ZipcodeManageModel zipcodeManageModel) {
        return zipcodeMasterMapper.updateZipcodeIslandType(zipcodeManageModel);
    }
}

