package kr.co.homeplus.escrowmng.statistics.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.statistics.model.CartCouponStatisticsModel;
import kr.co.homeplus.escrowmng.statistics.model.CartCouponStatisticsSelectModel;

import java.util.List;

@EscrowSlaveConnection
public interface CartCouponSlaveMapper {
    List<CartCouponStatisticsModel> getCartCouponStatistics(CartCouponStatisticsSelectModel cartCouponStatisticsSelectModel);
    List<CartCouponStatisticsModel> getCartCouponStatisticsByStoreId(CartCouponStatisticsSelectModel cartCouponStatisticsSelectModel);
}
