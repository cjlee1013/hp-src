package kr.co.homeplus.escrowmng.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "주문 배치 관리 model")
@Getter
@Setter
@EqualsAndHashCode
public class OrderBatchManageModel {
    @ApiModelProperty(value = "배치번호(seq)", position = 1)
    private String batchNo;

    @ApiModelProperty(value = "배치타입", position = 2)
    @NotEmpty(message = "배치타입")
    private String batchType;

    @ApiModelProperty(value = "배치명", position = 3)
    @NotEmpty(message = "배치명")
    private String batchNm;

    @ApiModelProperty(value = "배치주기(스케줄러에 등록된 주기설정 정보)", position = 4)
    @NotEmpty(message = "배치주기")
    private String batchTerm;

    @ApiModelProperty(value = "설명", position = 5)
    private String description;

    @ApiModelProperty(value = "사용여부", position = 6)
    @NotEmpty(message = "사용여부")
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "사용여부(Y|N)")
    private String useYn;

    @ApiModelProperty(value = "메타필드1", position = 7)
    private String metaField1;

    @ApiModelProperty(value = "메타필드2", position = 8)
    private String metaField2;

    @ApiModelProperty(value = "메타필드3", position = 9)
    private String metaField3;

    @ApiModelProperty(value = "메타필드4", position = 10)
    private String metaField4;

    @ApiModelProperty(value = "메타필드5", position = 11)
    private String metaField5;

    @ApiModelProperty(value = "등록자ID", position = 12)
    private String regId;

    @ApiModelProperty(value = "등록일시", position = 13)
    private String regDt;

    @ApiModelProperty(value = "수정자ID", position = 14)
    private String chgId;

    @ApiModelProperty(value = "수정일시", position = 15)
    private String chgDt;
}
