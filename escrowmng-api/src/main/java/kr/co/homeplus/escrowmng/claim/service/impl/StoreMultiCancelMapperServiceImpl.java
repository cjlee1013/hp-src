package kr.co.homeplus.escrowmng.claim.service.impl;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.mapper.refundFail.RefundFailMasterMapper;
import kr.co.homeplus.escrowmng.claim.mapper.refundFail.RefundFailSlaveMapper;
import kr.co.homeplus.escrowmng.claim.mapper.storeMultiCancel.StoreMultiCancelSlaveMapper;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListSetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteSetDto;
import kr.co.homeplus.escrowmng.claim.model.storeMultiCancel.StoreMultiCancelListGetDto;
import kr.co.homeplus.escrowmng.claim.model.storeMultiCancel.StoreMultiCancelListSetDto;
import kr.co.homeplus.escrowmng.claim.service.StoreMultiCancel.StoreMultiCancelMapperService;
import kr.co.homeplus.escrowmng.claim.service.refundFail.RefundFailMapperService;
import kr.co.homeplus.escrowmng.utils.SqlUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class StoreMultiCancelMapperServiceImpl implements StoreMultiCancelMapperService {

    private final StoreMultiCancelSlaveMapper storeMultiCancelSlaveMapper;

    @Override
    public List<StoreMultiCancelListGetDto> getStoreMultiCancelList(StoreMultiCancelListSetDto storeMultiCancelListSetDto) {
        return storeMultiCancelSlaveMapper.selectStoreMultiCancelList(storeMultiCancelListSetDto);
    }
}
