package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임 상태 변경 요청 파라미터")
public class ClaimRequestSetDto {

    @NotNull(message = "클레임번호")
    @ApiModelProperty(value = "클레임번호", position = 1)
    private long claimNo;

    @NotNull(message = "클레임번들번호")
    @ApiModelProperty(value = "클레임번들번호", position = 2)
    private long claimBundleNo;

    @ApiModelProperty(value = "클레임요청번호", position = 3)
    private long claimReqNo;

    @ApiModelProperty(value = "요청타입", position = 4)
    @Pattern(regexp = "CA|CW|CH|CD|CC|ca|cw|ch|cd|cc", message = "요청타입")
    private String claimReqType;

    @ApiModelProperty(value = "클레임타입", position = 5)
    private String claimType;

    @ApiModelProperty(value = "클레임사유코드", position = 6)
    private String reasonType;

    @ApiModelProperty(value = "클레임상세사유", position = 7)
    private String reasonTypeDetail;

    @ApiModelProperty(value = "등록자ID", position = 8)
    private String regId;
}
