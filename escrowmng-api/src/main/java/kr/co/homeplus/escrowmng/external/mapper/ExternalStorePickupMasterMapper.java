package kr.co.homeplus.escrowmng.external.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.external.model.StorePickupLockerManage;
import kr.co.homeplus.escrowmng.external.model.StorePickupPlaceManage;
import org.apache.ibatis.annotations.Param;

@EscrowMasterConnection
public interface ExternalStorePickupMasterMapper {

    int insertStorePickupPlace(@Param("place") StorePickupPlaceManage storePickupPlaceManage, @Param("chgId") String chgId);
    int insertStorePickupLocker(@Param("storeId") String storeId, @Param("placeNo") String placeNo, @Param("pickupLockerList") List<StorePickupLockerManage> storePickupLockerManageList, @Param("chgId") String chgId);
}
