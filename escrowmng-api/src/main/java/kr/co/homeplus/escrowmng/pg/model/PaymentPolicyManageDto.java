package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단관리 > 결제정책관리 DTO")
public class PaymentPolicyManageDto {
    @ApiModelProperty(value = "정책번호", required = true, position = 1)
    private Long policyNo;

    @ApiModelProperty(value = "정책명", required = true, position = 2)
    @Size(max = 50, message = "정책명")
    private String policyNm;

    @ApiModelProperty(value = "사이트유형(HOME:홈플러스,CLUB:더클럽)", required = true, position = 3)
    private String siteType;

    @ApiModelProperty(value = "적용대상(상품번호:ITEM,카테고리:CATE,제휴:AFFI)", required = true, position = 4)
    private String applyTarget;

    @ApiModelProperty(value = "적용시작일자", required = true, position = 5)
    private String applyStartDt;

    @ApiModelProperty(value = "적용종료일자", required = true, position = 6)
    private String applyEndDt;

    @ApiModelProperty(value = "결제수단안내여부", required = true, position = 7)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "결제수단안내여부(Y/N)")
    private String paymentMethodGuideYn;

    @ApiModelProperty(value = "포인트사용여부", position = 8)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "포인트사용여부(Y/N)")
    private String pointUseYn;

    @ApiModelProperty(value = "카드할부사용여부", position = 9)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "카드할부사용여부(Y/N)")
    private String cardInstallmentUseYn;

    @ApiModelProperty(value = "무이자사용여부", position = 10)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "무이자사용여부(Y/N)")
    private String freeInterestUseYn;

    @ApiModelProperty(value = "현금영수증사용여부", position = 11)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "현금영수증사용여부(Y/N)")
    private String cashReceiptUseYn;

    @ApiModelProperty(value = "pg상점설정(N:해당없음,C:PG카드할인,P:PG배분)", position = 12)
    private String pgStoreSet;

    @ApiModelProperty(value = "우선순위", position = 13)
    @Max(value = 999, message = "우선순위")
    private Integer priority;

    @ApiModelProperty(value = "사용여부", required = true, position = 14)
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "사용여부(Y/N)")
    private String useYn;

    @ApiModelProperty(value = "등록자id", required = true, position = 15)
    private String regId;

    @ApiModelProperty(value = "등록일자", position = 16)
    private String regDt;

    @ApiModelProperty(value = "수정자id", required = true, position = 17)
    private String chgId;

    @ApiModelProperty(value = "수정일자", position = 18)
    private String chgDt;

    @ApiModelProperty(value = "결제수단리스트", position = 19)
    private List<PaymentPolicyMethodDto> paymentPolicyMethodDtoList;

    @ApiModelProperty(value = "PG배분리스트", position = 20)
    private List<PgDivideManageDto> pgDivideManageDtoList;
}
