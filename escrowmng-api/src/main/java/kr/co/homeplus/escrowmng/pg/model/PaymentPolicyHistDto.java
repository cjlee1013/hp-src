package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 배분율 관리 > 결제정책 히스토리 DTO")
public class PaymentPolicyHistDto {
    @ApiModelProperty(value = "결제정책이력순번", position = 1)
    private Long paymentPolicyHistSeq;

    @ApiModelProperty(value = "적용범위(APPLY:적용대상,METHOD:결제수단)", required = true, position = 2)
    @NotEmpty(message = "적용범위")
    private String applyScope;

    @ApiModelProperty(value = "정책번호", required = true, position = 3)
    private Long policyNo;

    @ApiModelProperty(value = "적용코드(상품코드,카테고리번호,제휴코드)", position = 4)
    private String applyCd;

    @ApiModelProperty(value = "적용우선순위(1:채널,2:상품,3:카테고리(subString...),4:그이외...)", position = 5)
    @AllowInput(types = {PatternConstants.NUM}, message = "적용우선순위")
    private Integer applyPriority;

    @ApiModelProperty(value = "적용대상명(상품명,카테고리명,채널명)", position = 6)
    private String applyTargetNm;

    @ApiModelProperty(value = "결제수단코드", position = 7)
    private String methodCd;

    @ApiModelProperty(value = "등록자id(apply/method)", position = 8)
    private String regId;

    @ApiModelProperty(value = "등록일자(apply/method)", position = 9)
    private String regDt;
}
