package kr.co.homeplus.escrowmng.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"kr.co.homeplus.escrowmng", "kr.co.homeplus.plus"})
public class EscrowmngApiApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(EscrowmngApiApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(EscrowmngApiApplication.class);
    }
}
