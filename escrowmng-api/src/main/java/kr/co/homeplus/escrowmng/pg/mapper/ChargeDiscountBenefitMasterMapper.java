package kr.co.homeplus.escrowmng.pg.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.pg.model.ChargeDiscountBenefitManageDto;

@EscrowMasterConnection
public interface ChargeDiscountBenefitMasterMapper {
    int insertChargeDiscountBenefit(ChargeDiscountBenefitManageDto chargeDiscountBenefitManageDto);
    int updateChargeDiscountBenefit(ChargeDiscountBenefitManageDto chargeDiscountBenefitManageDto);
}
