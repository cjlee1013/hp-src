package kr.co.homeplus.escrowmng.pg.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodTreeDto;
import kr.co.homeplus.escrowmng.pg.service.PaymentMethodService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pg/paymentMethod")
@Api(tags = "결제관리 > 결제수단 관리 > 결제수단 관리")
public class PaymentMethodController {
    private final PaymentMethodService paymentMethodService;

    /**
     * 결제수단 조회
     */
    @GetMapping(value = "/getPaymentMethod")
    @ApiOperation(value = "결제수단 조회", response = PaymentMethodDto.class)
    public ResponseObject getPaymentMethod(PaymentMethodSelectDto paymentMethodSelectDto) throws Exception {
        List<PaymentMethodDto> paymentMethodDtoList = paymentMethodService.getPaymentMethod(paymentMethodSelectDto);
        return ResponseObject.Builder.<List<PaymentMethodDto>>builder().data(paymentMethodDtoList).build();
    }

    /**
     * 결제수단 저장
     */
    @PostMapping("/savePaymentMethod")
    @ApiOperation(value = "결제수단 등록/수정", response = Integer.class, hidden = true)
    public ResponseObject savePaymentMethod(@RequestBody PaymentMethodDto paymentMethodDto) throws Exception {
        return ResponseObject.Builder.<Integer>builder().data(paymentMethodService.savePaymentMethod(paymentMethodDto)).build();
    }

    /**
     * 결제수단코드 중복체크
     */
    @GetMapping(value = "/checkDuplicateCode")
    @ApiOperation(value = "결제수단코드 중복체크", response = PaymentMethodDto.class, hidden = true)
    public ResponseObject checkDuplicateCode(PaymentMethodSelectDto paymentMethodSelectDto) throws Exception {
        List<PaymentMethodDto> paymentMethodDtoList = paymentMethodService.getMethodCd(paymentMethodSelectDto);
        return ResponseObject.Builder.<List<PaymentMethodDto>>builder().data(paymentMethodDtoList).build();
    }

    /**
     * 결제수단 Tree 구조 조회
     */
    @GetMapping(value = "/getPaymentMethodTree")
    @ApiOperation(value = "결제수단 Tree 구조 조회", response = PaymentMethodTreeDto.class)
    public ResponseObject getPaymentMethodTree(PaymentMethodSelectDto paymentMethodSelectDto) throws Exception {
        return ResourceConverter.toResponseObject(paymentMethodService.getPaymentMethodTree(paymentMethodSelectDto));
    }

    /**
     * 결제수단 조회(유형별)
     * @param siteType
     * @param paymentMethodType
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "결제수단 조회(유형별)", response = PaymentMethodDto.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "siteType", value = "사이트유형", required = true, defaultValue = "HOME"),
            @ApiImplicitParam(name = "paymentMethodType", value = "결제수단유형(복수가능/콤마(,)로 구분/EASYPAY(간편결제))", required = true, defaultValue = "CARD")
    })
    @GetMapping(value = "/getPaymentMethodByType")
    public ResponseObject getPaymentMethodByType(
        @RequestParam(value = "siteType") String siteType,
        @RequestParam(value = "paymentMethodType") String paymentMethodType
    ) throws Exception {
        return ResourceConverter.toResponseObject(paymentMethodService.getPaymentMethodByType(siteType, paymentMethodType));
    }
}
