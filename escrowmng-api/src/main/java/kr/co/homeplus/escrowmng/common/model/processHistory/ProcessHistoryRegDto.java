package kr.co.homeplus.escrowmng.common.model.processHistory;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "클레임 처리내역/히스토리 등록")
public class ProcessHistoryRegDto {

    @ApiModelProperty(value = "처리 히스토리 번호")
    public long processHistoryNo;

    @ApiModelProperty(value = "클레임번호(환불:클레임번호)")
    public String claimNo;

    @ApiModelProperty(value = "클레임 묶음 배송번호")
    public String claimBundleNo;

    @ApiModelProperty(value = "클레임 반품수거 번호")
    public String claimPickShippingNo;

    @ApiModelProperty(value = "클레임 교환배송 번호")
    public String claimExchShippingNo;

    @ApiModelProperty(value = "구매주문번호")
    public String purchaseOrderNo;

    @ApiModelProperty(value = "번들번호")
    public String bundleNo;

    @ApiModelProperty(value = "배송번호")
    public String shipNo;

    @ApiModelProperty(value = "히스토리 등록 사유")
    public String historyReason;

    @ApiModelProperty(value = "히스토리 상세내역")
    public String historyDetailDesc;

    @ApiModelProperty(value = "히스토리 이전 상세내역")
    public String historyDetailBefore;

    @ApiModelProperty(value = "처리채널")
    public String regChannel;

    @ApiModelProperty(value = "등록자 ID")
    public String regId;

    @ApiModelProperty(value = "등록일시")
    public String regDt;

    @ApiModelProperty(value = "다중배번들송번호")
    public long multiBundleNo;
}
