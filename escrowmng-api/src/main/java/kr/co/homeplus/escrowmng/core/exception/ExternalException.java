package kr.co.homeplus.escrowmng.core.exception;

import java.util.List;
import kr.co.homeplus.escrowmng.enums.ExceptionCode;
import lombok.Getter;

/**
 * 정보제공 API Exception
 */
@Getter
public class ExternalException extends RuntimeException {

	private String errorCode;
	private String errorDescription;
	private String errorMsg;
	private Object data = List.of();

	/**
	 * 메세지 리턴방식 (exception code)
	 */
	public ExternalException(ExceptionCode exceptionCode) {
		this.errorCode 			= exceptionCode.getCode();
		this.errorDescription 	= exceptionCode.getDescription();
	}

	/**
	 * 메세지 리턴방식 (exception code + custom message)
	 */
	public ExternalException(ExceptionCode exceptionCode, String errorMsg) {
		this(exceptionCode);
		this.errorMsg 			= errorMsg;
	}

	/**
	 * 메세지 리턴방식 (exception code + custom data)
	 */
	public ExternalException(ExceptionCode exceptionCode, Object data) {
		this(exceptionCode);
		this.data 				= data;
	}

	/**
	 * 메세지 리턴방식 (exception code + custom message + custom data)
	 */
	public ExternalException(ExceptionCode exceptionCode, String errorMsg, Object data) {
		this(exceptionCode);
		this.errorMsg 			= errorMsg;
		this.data 				= data;
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}
}
