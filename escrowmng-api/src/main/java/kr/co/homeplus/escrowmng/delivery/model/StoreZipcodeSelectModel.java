package kr.co.homeplus.escrowmng.delivery.model;

import static kr.co.homeplus.escrowmng.escrow.constants.ZipcodeConstants.HYPHEN;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 점포배송정보 > 점포별 우편번호 조회 SELECT DTO")
public class StoreZipcodeSelectModel {
    @ApiModelProperty(notes = "점포유형(HYPER,EXP,CLUB)", position = 1, required = true)
    @Pattern(regexp = PatternConstants.STORE_TYPE_PATTERN, message="점포유형(HYPER,EXP,CLUB)")
    private String schStoreType;

    @ApiModelProperty(notes = "점포ID", position = 2, required = true)
    private Integer schStoreId;

    @ApiModelProperty(notes = "원거리 배송 여부", position = 3)
    private String schRemoteShipYn;

    @ApiModelProperty(notes = "우편번호", position = 4)
    @AllowInput(types = {PatternConstants.NUM}, message = "우편번호")
    private String schZipcode;

    @ApiModelProperty(notes = "시/도", position = 5)
    private String schSidoType;

    @ApiModelProperty(notes = "시군구", position = 6)
    private String schSigunguType;

    @ApiModelProperty(notes = "검색어(도로명/건물명)", position = 7)
    private String schAddrKeyword;

    @ApiModelProperty(notes = "검색어(건물번호)", position = 8)
    @Pattern(regexp = PatternConstants.BUILD_NO_PATTERN, message = "검색어(건물번호) (예:65, 65-1..)")
    private String schBuildNo;

    /** 쿼리에서 사용함 */
    @ApiModelProperty(notes = "건물본번", hidden = true)
    private Integer schBuildBonNo;
    @ApiModelProperty(notes = "건물부번", hidden = true)
    private Integer schBuildBuNo;

    public Integer getSchBuildBonNo() {
        if (EscrowString.isEmpty(this.schBuildNo)) {
            return null;
        }
        if (this.schBuildNo.contains(HYPHEN)) {
            return Integer.valueOf(this.schBuildNo.split(HYPHEN)[0]);
        } else {
            return Integer.valueOf(this.schBuildNo);
        }
    }
    public Integer getSchBuildBuNo() {
        if (EscrowString.isEmpty(this.schBuildNo)) {
            return null;
        }
        if (this.schBuildNo.contains(HYPHEN)) {
            return Integer.valueOf(this.schBuildNo.split(HYPHEN)[1]);
        }
        return null;
    }
}
