package kr.co.homeplus.escrowmng.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@ApiModel(description = "안심번호 요청 정보")
@Data
@RequiredArgsConstructor
public class SafetyIssueRequest {

    @ApiModelProperty(value = "주문구매번호", position = 1)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "번들번호", position = 2)
    private long bundleNo;

    @ApiModelProperty(value = "요청연락처번호", position = 3)
    private String reqPhoneNo;

}
