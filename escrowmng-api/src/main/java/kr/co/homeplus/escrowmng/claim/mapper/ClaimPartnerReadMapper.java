package kr.co.homeplus.escrowmng.claim.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerBoardCountGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerDetailGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerDetailItem;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerExchangeListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerListSetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerDetailListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.ClaimPartnerReturnListGetDto;
import kr.co.homeplus.escrowmng.claim.model.partner.PartnerMainBoardClaimRequestCountGetDto;
import kr.co.homeplus.escrowmng.claim.sql.partner.ClaimPartnerSql;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;

@EscrowSlaveConnection
public interface ClaimPartnerReadMapper {

    @ResultType(value = ClaimPartnerBoardCountGetDto.class)
    @SelectProvider(type = ClaimPartnerSql.class, method = "selectPOClaimBoardCount")
    ClaimPartnerBoardCountGetDto selectPOClaimBoardCount(@Param("partnerId") String partnerId, @Param("claimType") String claimType);

    @ResultType(value = ClaimPartnerListGetDto.class)
    @SelectProvider(type = ClaimPartnerSql.class, method = "getPOClaimList")
    List<ClaimPartnerListGetDto> getPOClaimList(ClaimPartnerListSetDto claimCancelListSetDto);

    @ResultType(value = ClaimPartnerListGetDto.class)
    @SelectProvider(type = ClaimPartnerSql.class, method = "getPOClaimListCnt")
    Integer getPOClaimListCnt(ClaimPartnerListSetDto claimCancelListSetDto);


    @ResultType(value = ClaimPartnerDetailListGetDto.class)
    @SelectProvider(type = ClaimPartnerSql.class, method = "selectPOClaimDetailList")
    List<ClaimPartnerDetailListGetDto> selectPOClaimDetailList(@Param("purchaseOrderNo")long purchaseOrderNo, @Param("bundleNo")long bundleNo);

    @ResultType(value = ClaimPartnerDetailGetDto.class)
    @SelectProvider(type = ClaimPartnerSql.class, method = "selectPOClaimDetail")
    ClaimPartnerDetailGetDto selectPOClaimDetail(@Param("claimBundleNo")long claimBundleNo);

    @ResultType(value = ClaimPartnerDetailItem.class)
    @SelectProvider(type = ClaimPartnerSql.class, method = "selectPOClaimDetailItemList")
    List<ClaimPartnerDetailItem> selectPOClaimDetailItemList(@Param("claimBundleNo")long claimBundleNo);


    @ResultType(value = PartnerMainBoardClaimRequestCountGetDto.class)
    @SelectProvider(type = ClaimPartnerSql.class, method = "selectPOMainClaimRequestCount")
    PartnerMainBoardClaimRequestCountGetDto selectPOMainClaimRequestCount(@Param("partnerId") String partnerId);






}
