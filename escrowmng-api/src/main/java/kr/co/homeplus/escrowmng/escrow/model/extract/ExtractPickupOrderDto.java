package kr.co.homeplus.escrowmng.escrow.model.extract;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "픽업주문 추출용 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class ExtractPickupOrderDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "점포명", position = 2)
    private String storeNm;

    @ApiModelProperty(value = "주문일", position = 3)
    private String orderDt;

    @ApiModelProperty(value = "픽업일", position = 4)
    private String shipDt;

    @ApiModelProperty(value = "픽업시간", position = 5)
    private String pickupTime;

    @ApiModelProperty(value = "주문금액", position = 6)
    private long orderAmt;

    @ApiModelProperty(value = "장바구니할인금액", position = 7)
    private long cartDiscountAmt;

    @ApiModelProperty(value = "중복쿠폰할인금액", position = 8)
    private long addDiscountAmt;

    @ApiModelProperty(value = "결제금액", position = 9)
    private long payAmt;

    @ApiModelProperty(value = "주문상태", position = 10)
    private String paymentStatus;

    @ApiModelProperty(value = "배송상태", position = 11)
    private String shipStatus;
}
