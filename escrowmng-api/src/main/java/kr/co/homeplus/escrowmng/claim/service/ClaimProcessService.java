package kr.co.homeplus.escrowmng.claim.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.escrowmng.claim.model.ClaimRequestSetDto;
import kr.co.homeplus.escrowmng.claim.model.entity.ClaimOptEntity;
import kr.co.homeplus.escrowmng.claim.model.external.ClaimShipCompleteDto;
import kr.co.homeplus.escrowmng.claim.model.external.EmpInfoDto;
import kr.co.homeplus.escrowmng.claim.model.external.ticket.ClaimCoopTicketCheckGetDto;
import kr.co.homeplus.escrowmng.claim.model.external.ticket.ClaimCoopTicketCheckSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimBundleRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimItemRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimMultiBundleRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimMultiItemRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.escrowmng.claim.model.register.ClaimShippingRegDto;
import kr.co.homeplus.escrowmng.claim.service.market.MarketClaimService;
import kr.co.homeplus.escrowmng.common.model.processHistory.ProcessHistoryRegDto;
import kr.co.homeplus.escrowmng.core.exception.LogicException;
import kr.co.homeplus.escrowmng.enums.ClaimCode;
import kr.co.homeplus.escrowmng.enums.ClaimResponseCode;
import kr.co.homeplus.escrowmng.enums.ExternalInfo;
import kr.co.homeplus.escrowmng.order.factory.EntityFactory;
import kr.co.homeplus.escrowmng.utils.ObjectUtils;
import kr.co.homeplus.escrowmng.utils.SetParameter;
import kr.co.homeplus.escrowmng.utils.StringUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClaimProcessService {

    private final ClaimMapperService mapperService;

    private final ExternalService externalService;

    private final ClaimCommonService commonService;

    private final MarketClaimService marketClaimService;

    @Transactional(rollbackFor = {Exception.class, LogicException.class}, propagation = Propagation.NESTED, isolation = Isolation.READ_COMMITTED)
    public LinkedHashMap<String, Object> addClaimRegister(ClaimRegSetDto regSetDto) throws Exception {
        // Claim_mst 등록
        ClaimMstRegDto claimMstRegDto = insertClaimMstProcess(regSetDto);
        // 클레임 등록을 위한 클레임 데이터 가지고 오기
        LinkedHashMap<String, Object> createClaimData = commonService.createClaimDataMap(regSetDto);
        log.info("createClaimDataMap : {}", createClaimData);
        // claim_mst 에 등록된 claim_no 를 map 넣음.
        createClaimData.put("claim_no", claimMstRegDto.getClaimNo());

        createClaimData = insertClaimPartBasicFromBundleProcess(createClaimData, regSetDto);

        if(regSetDto.getIsMultiOrder()) {
            createClaimData.put("multi_bundle_no", regSetDto.getMultiBundleNo());
            this.addMultiOrderPartClaimInfo(createClaimData, regSetDto.getClaimItemList());
        }

        // claim addition reg
        mapperService.callByClaimAdditionReg(claimMstRegDto.getClaimNo());

        insertClaimPaymentProcess(createClaimData, claimMstRegDto);

        //안신번호 발행.(반품/수거만)
        if(!regSetDto.getClaimType().equals("C") && ObjectUtils.toString(createClaimData.get("safety_use_yn")).equals("Y")){
            // 수거 등록
            createClaimSafetyIssue(ObjectUtils.toLong(createClaimData.get("claim_bundle_no")), "R");
            if(regSetDto.getClaimType().equals("X")){
                createClaimSafetyIssue(ObjectUtils.toLong(createClaimData.get("claim_bundle_no")), "X");
            }
        }

        if(!regSetDto.getClaimType().equals("C")){
            // 기조 배송상태 완료로 변경.
            if("D3".contains(ObjectUtils.toString(createClaimData.get("ship_status")))){
                this.setClaimShipComplete(regSetDto.getBundleNo(), regSetDto.getRegId());
            }
        }

        return createClaimData;
    }

    @Transactional(rollbackFor = {Exception.class, LogicException.class}, propagation = Propagation.NESTED, isolation = Isolation.READ_COMMITTED)
    public LinkedHashMap<String, Object> addTotalClaimRegister(ClaimRegSetDto regSetDto) throws Exception {
        // Claim_mst 등록
        ClaimMstRegDto claimMstRegDto = insertClaimMstProcess(regSetDto);
        List<Long> claimBundleNoList = new ArrayList<>();
        LinkedHashMap<String, Object> createClaimData = new LinkedHashMap<>();
        for (LinkedHashMap<String, Object> bundleData : getOrderBundleByClaim(regSetDto.getPurchaseOrderNo())) {
            regSetDto.setClaimItemList(getClaimPreRefundItemInfo( ObjectUtils.toLong(bundleData.get("purchase_order_no")), ObjectUtils.toLong(bundleData.get("bundle_no"))));
            regSetDto.setBundleNo(ObjectUtils.toString(bundleData.get("bundle_no")));
            // 클레임 등록을 위한 클레임 데이터 가지고 오기
            createClaimData = commonService.createClaimDataMap(regSetDto);
            // claim_mst 에 등록된 claim_no 를 map 넣음.
            createClaimData.put("claim_no", claimMstRegDto.getClaimNo());

            createClaimData = insertClaimPartBasicFromBundleProcess(createClaimData, regSetDto);

            if(regSetDto.getIsMultiOrder()) {
                this.addMultiOrderTotalClaimInfo(createClaimData);
            }

            // claim addition reg
            mapperService.callByClaimAdditionReg(claimMstRegDto.getClaimNo());
            // 클레임번들번
            claimBundleNoList.add(ObjectUtils.toLong(createClaimData.get("claim_bundle_no")));
        }
        // 결제취소데이터
        insertClaimPaymentProcess(createClaimData, claimMstRegDto);

        LinkedHashMap<String, Object> returnMap = new LinkedHashMap<>();
        returnMap.put("claim_no", claimMstRegDto.getClaimNo());
        returnMap.put("payment_no", createClaimData.get("payment_no"));
        returnMap.put("claim_bundle_no", claimBundleNoList);

        return returnMap;
    }

    public ClaimMstRegDto insertClaimMstProcess(ClaimRegSetDto regSetDto) throws Exception {
        // 클레임 마스터 등록을 위한 DTO 설정.
        // 환불 예정 금액 조회를 다시 한다. (정합성 체크)
        ClaimMstRegDto claimMstRegDto = commonService.getFusionForClaimMst(commonService.convertCalcSetDto(regSetDto));
        // 취소,반품 신청시 환불금액이 0원미만인 경우 오류처리
        if(claimMstRegDto.getCompleteAmt() < 0 && "C,R".contains(regSetDto.getClaimType())){
            throw new LogicException(ClaimResponseCode.CLAIM_CANCEL_ERR02);
        }
        // 임직원할인번호가 있으면, 현재 임직원할인 잔액 설정.
        if(!StringUtil.isEmpty(claimMstRegDto.getEmpNo())){
            claimMstRegDto.setEmpRemainAmt(getEmpDiscountLimitAmt(claimMstRegDto.getEmpNo()));
        }

        // 마켓연동 주문 확인 후 마켓연동일 경우
        // 해당 마켓으로 취소 신청.
        if(marketClaimService.isOrderMarketCheck(regSetDto.getPurchaseOrderNo())){
            if("R".equals(regSetDto.getClaimType()) || ("C".equals(regSetDto.getClaimType()) && "Y".equals(regSetDto.getOrderCancelYn()))){
                marketClaimService.setMarketCancelReg(regSetDto.getPurchaseOrderNo(), regSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderOptNo).collect(Collectors.toList()), regSetDto.getClaimReasonType());
            } else if ("X".equals(regSetDto.getClaimType())) {
                throw new LogicException(ClaimResponseCode.CLAIM_MARKET_REG_ERR01);
            }
        }

        // 클레임 마스터 등록.
        if(!mapperService.addClaimMstInfo(claimMstRegDto)){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
        }
        return claimMstRegDto;
    }

    public LinkedHashMap<String, Object> insertClaimPartBasicFromBundleProcess(LinkedHashMap<String, Object> createClaimData, ClaimRegSetDto regSetDto) throws Exception {
        if("R,X".contains(regSetDto.getClaimType())){
            createClaimData.put("isEnclose", regSetDto.getClaimDetail().getIsEnclose());
        }
        log.info("insertClaimPartBasicFromBundleProcess : {}", createClaimData);
        // 클레임 번들
        ClaimBundleRegDto claimBundleRegDto = commonService.createClaimBundleRegDto(createClaimData);
        // 클레임 번들 등록
        if(!mapperService.addClaimBundleInfo(claimBundleRegDto)){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
        }

        // claim_bundle_no 를 map 에 넣음.
        createClaimData.put("claim_bundle_no", claimBundleRegDto.getClaimBundleNo());

        if("R,X".contains(regSetDto.getClaimType())){
            log.debug("insert claimReq :: {}", !StringUtils.isEmpty(regSetDto.getClaimDetail().getUploadFileName()));
            if(!StringUtils.isEmpty(regSetDto.getClaimDetail().getUploadFileName())){
                createClaimData.put("upload_file_name", regSetDto.getClaimDetail().getUploadFileName());
            }
            if(!StringUtils.isEmpty(regSetDto.getClaimDetail().getUploadFileName2())){
                createClaimData.put("upload_file_name2", regSetDto.getClaimDetail().getUploadFileName2());
            }
            if(!StringUtils.isEmpty(regSetDto.getClaimDetail().getUploadFileName3())){
                createClaimData.put("upload_file_name3", regSetDto.getClaimDetail().getUploadFileName3());
            }
        }
        // 환불예정금액 재조회를 위한 파라미터 저장 추가.
        // 할인미차감여부
        createClaimData.put("deduct_discount_yn", regSetDto.getPiDeductDiscountYn());
        // 행사미차감여부
        createClaimData.put("deduct_promo_yn", regSetDto.getPiDeductPromoYn());
        // 배송비미차감여부
        createClaimData.put("deduct_ship_yn", regSetDto.getPiDeductShipYn());

        log.debug("insert claimReq :: {}", createClaimData);
        // 클레임 요청
        if(!mapperService.addClaimReqInfo(commonService.createClaimReqRegDto(createClaimData))){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
        }

        // 클레임 아이템 및 옵션별 저장.
        this.claimItemPartRegProcess(createClaimData, regSetDto);

        if(!regSetDto.getClaimType().equalsIgnoreCase("C")){
            ClaimShippingRegDto shippingRegDto = commonService.createClaimPickShippingDto(regSetDto.getClaimDetail(),  createClaimData);
            // pick insert
            if(!mapperService.addClaimPickShippingInfo(shippingRegDto)){
                throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR11);
            }

            if(regSetDto.getClaimType().equalsIgnoreCase("X")){
                if(!mapperService.addClaimExchShipping(commonService.createClaimExchShippingRegDto(shippingRegDto, regSetDto.getClaimDetail()))){
                    throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR12);
                }
            }
            createClaimData.put("pickStatus", shippingRegDto.getPickStatus());
        }

        return createClaimData;
    }

    public void insertClaimPaymentProcess(LinkedHashMap<String, Object> createClaimData, ClaimMstRegDto claimMstRegDto) throws Exception {
        LinkedHashMap<String, Long> claimPaymentType = commonService.getClaimPaymentTypeList(claimMstRegDto);

        if(claimPaymentType == null){
            claimPaymentType = new LinkedHashMap<>(){{
                put("FREE", 0L);
            }};
        }

        for(String key : claimPaymentType.keySet()){
            if(!mapperService.addClaimPaymentInfo(commonService.createClaimPaymentRegDto(createClaimData, key, claimPaymentType.get(key)))){
                throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
            }
        }
    }

    /**
     * 결제취소 전 환불예정금액 비교
     *
     */
    public void getPrePaymentCancel(long claimNo) throws Exception {
        // 기존 환불예정금 취득
        ClaimMstRegDto claimMstInfo = mapperService.getPreRefundCompareData(claimNo);
        if(claimMstInfo != null) {
            // 환불예정금액 재조회
            ClaimPreRefundCalcDto reCheckRefundDto =  mapperService.getClaimPreRefundReCheckCalculation(getClaimPreRefundData(claimNo));
            log.info("클레임번호({}) 환불예정금액 재조회 결과 :: {}", claimNo, reCheckRefundDto);
            if(reCheckRefundDto.getReturnValue() != 0) {
                log.info("결제취소전 환불예정금 조회실패 :: {}", reCheckRefundDto.getReturnMsg());
                throw new LogicException(ClaimResponseCode.CLAIM_PAYMENT_REFUND_CHECK_ERR02, reCheckRefundDto.getReturnMsg());
            }
            LinkedHashMap<String, Object> claimMstInfoMap = ObjectUtils.getConvertMap(claimMstInfo);
            LinkedHashMap<String, Object> reCheckMap = commonService.convertClaimRegInfo(ClaimMstRegDto.builder().build(), reCheckRefundDto);
            if(!getPreRefundAmtCheck(claimMstInfoMap, reCheckMap, "pgAmt", "mileageAmt", "mhcAmt", "dgvAmt", "ocbAmt")){
                // 환불예정금 변경처리.
                throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
            }
        }
    }

    private void addMultiOrderTotalClaimInfo(LinkedHashMap<String, Object> createClaimData) throws Exception {
        ClaimMultiBundleRegDto claimMultiBundleRegDto = new ClaimMultiBundleRegDto();
        claimMultiBundleRegDto.setClaimNo(ObjectUtils.toLong(createClaimData.get("claim_no")));
        claimMultiBundleRegDto.setClaimBundleNo(ObjectUtils.toLong(createClaimData.get("claim_bundle_no")));
        claimMultiBundleRegDto.setPurchaseOrderNo(ObjectUtils.toLong(createClaimData.get("purchase_order_no")));

        // 번들정보를 조회하여 처리.
        for(Long multiBundleNo : mapperService.getMultiBundleInfo(ObjectUtils.toLong(createClaimData.get("purchase_order_no")), ObjectUtils.toLong(createClaimData.get("bundle_no")))) {
            claimMultiBundleRegDto.setMultiBundleNo(multiBundleNo);
            if(!mapperService.addClaimMultiBundleInfo(claimMultiBundleRegDto)){
                throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
            }
            //클레임아이템 조회
            for(ClaimMultiItemRegDto multiOrderItemInfoDto : mapperService.getClaimMultiItemCreateInfo(claimMultiBundleRegDto.getClaimNo(), claimMultiBundleRegDto.getMultiBundleNo())) {
                multiOrderItemInfoDto.setClaimMultiBundleNo(claimMultiBundleRegDto.getClaimMultiBundleNo());
                if(!mapperService.addClaimMultiItemInfo(multiOrderItemInfoDto)){
                    throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
                }
            }
        }
    }

    private void addMultiOrderPartClaimInfo(LinkedHashMap<String, Object> createClaimData, List<ClaimPreRefundItemList> claimItemList) throws Exception {
        ClaimMultiBundleRegDto claimMultiBundleRegDto = new ClaimMultiBundleRegDto();
        claimMultiBundleRegDto.setClaimNo(ObjectUtils.toLong(createClaimData.get("claim_no")));
        claimMultiBundleRegDto.setClaimBundleNo(ObjectUtils.toLong(createClaimData.get("claim_bundle_no")));
        claimMultiBundleRegDto.setPurchaseOrderNo(ObjectUtils.toLong(createClaimData.get("purchase_order_no")));
        claimMultiBundleRegDto.setMultiBundleNo(ObjectUtils.toLong(createClaimData.get("multi_bundle_no")));

        if(!mapperService.addClaimMultiBundleInfo(claimMultiBundleRegDto)){
            throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
        }

        //클레임아이템 조회
        for(ClaimMultiItemRegDto multiOrderItemInfoDto : mapperService.getClaimMultiItemCreateInfo(claimMultiBundleRegDto.getClaimNo(), claimMultiBundleRegDto.getMultiBundleNo())) {
            multiOrderItemInfoDto.setClaimMultiBundleNo(claimMultiBundleRegDto.getClaimMultiBundleNo());
            if(claimItemList.stream().anyMatch(itemListDto -> itemListDto.getOrderItemNo().equals(String.valueOf(multiOrderItemInfoDto.getOrderItemNo())))) {
                multiOrderItemInfoDto.setClaimItemQty(claimItemList.stream().filter(iemListDto -> iemListDto.getOrderItemNo().equals(String.valueOf(multiOrderItemInfoDto.getOrderItemNo()))).map(ClaimPreRefundItemList::getClaimQty).mapToLong(Long::parseLong).sum());
                if(!mapperService.addClaimMultiItemInfo(multiOrderItemInfoDto)){
                    throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
                }
            }
        }
    }

    private boolean getPreRefundAmtCheck(Map mstInfoMap, Map reCheckMap, String... keys){
        for(String key : keys){
            log.info("클레임번호({}) 결제수단({}) 확인 :: [기존 : {} / 재조회 : {}]", mstInfoMap.get("claimNo"), key, mstInfoMap.get(key), reCheckMap.get(key));
            if(!ObjectUtils.isCompareMap(mstInfoMap, reCheckMap, key)){
                log.info("환불예정금이 기존과 다릅니다. 금액 교체를 시도 합니다.");
                if(!mapperService.modifyClaimMstPaymentAmt((Long) mstInfoMap.get("claimNo"), key, (Long) reCheckMap.get(key))){
                    return Boolean.FALSE;
                }
                if(!mapperService.modifyClaimPaymentAmt((Long) mstInfoMap.get("claimNo"), getClaimPaymentType(key), (Long) reCheckMap.get(key))){
                    return Boolean.FALSE;
                }
            }
        }
        return Boolean.TRUE;
    }

    private String getClaimPaymentType(String key){
        return commonService.getClaimPaymentTypeCode(key.replaceAll("Amt", ""));
    }

    /**
     * 결제취소 전 환불예정금재조회
     * 재조회용 데이터
     *
     * @param claimNo 클레임그룹번호
     * @return 환불예정금액 조회 파라미터
     * @throws Exception 오류시 오류처리.
     */
    private ClaimPreRefundCalcSetDto getClaimPreRefundData (long claimNo) throws Exception {
        ClaimPreRefundCalcSetDto calcDto = mapperService.getPreRefundReCheckData(claimNo);
        if(calcDto == null) {
            throw new LogicException(ClaimResponseCode.CLAIM_PAYMENT_REFUND_CHECK_ERR01);
        }
        calcDto.setClaimItemList(mapperService.getPreRefundItemData(claimNo));
        return calcDto;
    }

    /**
     * 클레임 아이템 등록 Process
     * 클레임 아이템 등록 후 클레임 옵션 등록
     *
     * @param claimData 클레임 생성 데이터 맵
     * @param claimRegSetDto 클레임 등록 데이터
     * @throws Exception 오류시 오류처리
     */
    public void claimItemPartRegProcess(LinkedHashMap<String, Object> claimData, ClaimRegSetDto claimRegSetDto) throws Exception {
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class);
        // 이전상품주문번호
        long prevOrderItemNo = 0L;
        // 클레임 상품번호
        long claimItemNo = 0L;
        // 아이템 번호별 loop
        for(ClaimPreRefundItemList itemList : claimRegSetDto.getClaimItemList()){
            LinkedHashMap<String, Object> claimItemInfo = mapperService.getClaimItemInfo(Long.parseLong(claimRegSetDto.getPurchaseOrderNo()), Long.parseLong(itemList.getOrderItemNo()));
            // 클레임 데이터 넣기
            claimItemInfo.putAll(claimData);
            // 클레임 등록 DTO 생성
            ClaimItemRegDto claimItemRegDto = commonService.createClaimItemRegDto(claimItemInfo);
            // 이전 주문번호와 현재 처리 주문번호가 같으면 insert skip
            if(prevOrderItemNo != claimItemRegDto.getOrderItemNo()){
                log.info("주문번호[{}] - 상품주문번호[{}]에 대한 Claim_item 생성 데이터 조회 :: {}", claimRegSetDto.getPurchaseOrderNo(), itemList.getOrderItemNo(), claimItemInfo);
                // 클레임 신청 수량
                int claimItemQty = claimRegSetDto.getClaimItemList().stream().filter(itemList1 -> itemList1.getOrderItemNo().equals(String.valueOf(claimItemRegDto.getOrderItemNo()))).map(ClaimPreRefundItemList::getClaimQty).mapToInt(Integer::parseInt).sum();
                // validation
                if(ObjectUtils.isEquals(claimItemInfo.get("item_qty"), claimItemInfo.get("claim_item_qty")) || claimItemQty <= 0){
                    throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR15);
                }
                // 클래임 건수 입력
                claimItemRegDto.setClaimItemQty(claimItemQty);
                // 클레임 아이템생성.
                if(!mapperService.addClaimItemInfo(claimItemRegDto)){
                    throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
                }
                claimItemNo = claimItemRegDto.getClaimItemNo();
                prevOrderItemNo = claimItemRegDto.getOrderItemNo();
                // 티켓 취소시 여기서 처리.
                if(ObjectUtils.toString(claimItemInfo.get("order_type")).equals("ORD_TICKET")){
                    List<LinkedHashMap<String, Object>> orderTicketInfo = mapperService.getOrderTicketInfo(claimItemRegDto.getPurchaseOrderNo(), claimItemRegDto.getOrderItemNo(), claimItemQty);
                    if(orderTicketInfo.stream().map(map -> ObjectUtils.toString(map.get("issue_channel"))).map(String::valueOf).anyMatch(str -> str.equals("COOP"))){
                        List<LinkedHashMap<String, Object>> coopTicketInfoList = orderTicketInfo.stream().filter(map -> ObjectUtils.toString(map.get("issue_channel")).equals("COOP")).collect(Collectors.toList());
                        if(coopTicketInfoList.size() != claimItemQty){
                            throw new LogicException(ClaimResponseCode.CLAIM_TICKET_ERR01);
                        }
                        for(LinkedHashMap<String, Object> coopTicketInfoMap : coopTicketInfoList){
                            ClaimCoopTicketCheckSetDto checkDto = new ClaimCoopTicketCheckSetDto();
                            checkDto.setSellerItemCd(ObjectUtils.toString(coopTicketInfoMap.get("seller_item_cd")));
                            checkDto.setTicketCd(ObjectUtils.toString(coopTicketInfoMap.get("ticket_cd")));
                            if(!this.getCoopTicketCheck(checkDto)){
                                throw new LogicException(ClaimResponseCode.CLAIM_TICKET_ERR02);
                            }
                        }
                    }
                    for(LinkedHashMap<String, Object> insertMap : orderTicketInfo){
                        insertMap.putAll(claimItemInfo);
                        insertMap.put("claim_item_no", claimItemNo);
                        if(!mapperService.addClaimTicketInfo(commonService.createClaimTicketDto(insertMap))){
                            throw new LogicException(ClaimResponseCode.CLAIM_TICKET_ERR03);
                        }
                    }
                }
            }
            // opt 데이터 조회
            // 주문에서 옵션별 데이터를 가지고 온다.
            ClaimOptRegDto claimOptRegDto = mapperService.getOriginOrderOptInfoForClaim(Long.parseLong(itemList.getOrderItemNo()), itemList.getOrderOptNo());
            // 클레임 번호
            claimOptRegDto.setClaimNo((Long) claimItemInfo.get(claimOpt.claimNo));
            // 클레임 번들 번호
            claimOptRegDto.setClaimBundleNo((Long) claimItemInfo.get(claimOpt.claimBundleNo));
            // 클래암 아이템 번호
            claimOptRegDto.setClaimItemNo(claimItemNo);
            // 클레임 수량
            claimOptRegDto.setClaimOptQty(Long.parseLong(itemList.getClaimQty()));
            // 마켓주문번호
            claimOptRegDto.setMarketOrderNo(ObjectUtils.toString(claimItemInfo.get(claimOpt.marketOrderNo)));
            // 생성한 데이터를 클레임 옵션별에 저장.
            if(!mapperService.addClaimOptInfo(claimOptRegDto)){
                throw new LogicException(ClaimResponseCode.CLAIM_REG_ERR01);
            }
            // 아이템 임직원 할인
            mapperService.callByClaimItemEmpDiscount(claimItemRegDto.getClaimNo(), claimItemRegDto.getOrderItemNo());
        }
    }


    public List<LinkedHashMap<String, Object>> getOrderBundleByClaim(String purchaseOrderNo) {
        return mapperService.getOrderBundleByClaim(Long.parseLong(purchaseOrderNo));
    }

    public List<ClaimPreRefundItemList> getClaimPreRefundItemInfo(long purchaseOrderNo, long bundleNo) {
        return mapperService.getClaimPreRefundItemInfo(purchaseOrderNo, bundleNo);
    }

    public Boolean setClaimProcessHistory(ProcessHistoryRegDto processHistoryRegDto){
        return mapperService.setClaimProcessHistory(processHistoryRegDto);
    }

    /**
     * 클레임 상태 변경
     *
     * @param claimNo 클레임번호
     * @param claimBundleNo 클레임주문번호
     * @param claimCode 클레임상태 Enum
     * @return Boolean 클레임상태변경 결과.
     */
    public Boolean setClaimStatus(long claimNo, long claimBundleNo, String regId, ClaimCode claimCode) {
        return mapperService.setClaimStatus(createClaimStatusMap(claimNo, claimBundleNo, regId, claimCode.getCode()));
    }

    /**
     * 클레임 상태 변경
     *
     * @param setDto 클레임 상태변경 DTO
     * @return Boolean 클레임상태변경 결과.
     */
    public Boolean setClaimStatus(ClaimRequestSetDto setDto) throws Exception {
        return mapperService.setClaimStatus(
            new LinkedHashMap<>(){{
                // 클레임 번호
                put("claimNo", setDto.getClaimNo());
                // 클레임 번들번호
                put("claimBundleNo", setDto.getClaimBundleNo());
                // 등록자(*수정자)
                put("regId", setDto.getRegId());
                // 클레임 상태
                put("claimStatus", getClaimStatus(setDto.getClaimReqType()));
                // 클레임 사유코드
                put("reasonType", setDto.getReasonType());
                // 클레임 상세사유
                put("reasonDetail", setDto.getReasonTypeDetail());
            }}
        );
    }

    private LinkedHashMap<String, Object> createClaimStatusMap(long claimNo, long claimBundleNo, String regId, String claimStatus) {
        return new LinkedHashMap<>(){{
            // 클레임 번호
            put("claimNo", claimNo);
            // 클레임 번들번호
            put("claimBundleNo", claimBundleNo);
            // 등록자(*수정자)
            put("regId", (claimStatus.equals("C3") || (claimStatus.equals("C2") && regId.equalsIgnoreCase("MYPAGE"))) ? "SYSTEM" : regId);
            // 클레임 상태
            put("claimStatus", claimStatus);
        }};
    }

    /**
     * 결제취소 응답 코드 중
     * 오류가 아닌 건 체크
     * @param paymentType 결제취소타입
     * @param responseCode 응답코드
     * @return Boolean 진행여부
     */
    private Boolean isProcessStep(String paymentType, String responseCode) {
        switch (paymentType) {
            case "PG" :
                return "AV11".contains(responseCode);
            case "MP" :
                return "3013".contains(responseCode);
            case "PP" :
                return "3218".contains(responseCode);
            case "DG" :
                return "8507".contains(responseCode);
            case "OC" :
                return "3042".contains(responseCode);
            default:
                return Boolean.FALSE;
        }
    }

    public Integer getEmpDiscountLimitAmt(String empNo) throws Exception {
        EmpInfoDto empInfo = externalService.getTransfer(
            ExternalInfo.EMP_DISCOUNT_LIMIT_AMT_INFO,
            new ArrayList<SetParameter>(){{ add(SetParameter.create("empNo", empNo)); }},
            EmpInfoDto.class
        );
        if(empInfo == null){
            throw new LogicException(ClaimResponseCode.CLAIM_PAYMENT_CANCEL_EMP_ERR01);
        }
        return empInfo.getRemainPoint();
    }

    public void createClaimSafetyIssue(long claimBundleNo, String issueType) {
        try {
            Integer resultData = externalService.postTransfer(
                ExternalInfo.CLAIM_SAFETY_ISSUE_ADD,
                mapperService.getClaimSafetyData(claimBundleNo, issueType),
                Integer.class
            );
            log.debug("createClaimSafetyIssue result ::: {}", resultData);
        } catch (Exception e) {
            log.error("createClaimSafetyIssue ERROR ::: {}", e.getMessage());
        }
    }

    private Boolean getCoopTicketCheck(ClaimCoopTicketCheckSetDto checkDto) throws Exception {
        try{
            log.debug("getCoopTicketCheck ::: {}", checkDto);
            ClaimCoopTicketCheckGetDto returnDto =
                externalService.postTransfer(
                    ExternalInfo.OUTBOUND_COOP_TICKET_CHECK_FOR_CLAIM,
//                    new ArrayList<SetParameter>(){{
//                        add(SetParameter.create("sellerItemCd", checkDto.getSellerItemCd()));
//                        add(SetParameter.create("ticketCd", checkDto.getTicketCd()));
//                    }},
                    checkDto,
                    ClaimCoopTicketCheckGetDto.class
                );
            if(returnDto != null){
                if(StringUtil.isEmpty(returnDto.getUseYn())){
                    returnDto.setUseYn("N");
                }
                return "N,C".contains(returnDto.getUseYn());
            } else {
                return false;
            }
        } catch (Exception e){
            log.error("getCoopTicketCheck ::: checkDto :: {} ::: ErrorMsg :: {}", checkDto, e.getMessage());
            return false;
        }
    }

    public void setClaimShipComplete(String bundleNo, String regId) throws Exception{
        externalService.postTransfer(
            ExternalInfo.SHIPPING_CLAIM_FROM_SHIP_COMPLETE,
            ClaimShipCompleteDto.builder().bundleNo(bundleNo).chgId(regId).build(),
            String.class
        );
    }

    public void setClaimShipDecision(String bundleNo, String regId) throws Exception{
        externalService.postTransfer(
            ExternalInfo.SHIPPING_CLAIM_FROM_SHIP_DECISION,
            ClaimShipCompleteDto.builder().bundleNo(bundleNo).chgId(regId).build(),
            String.class
        );
    }

    public String getClaimStatus(String claimReqType) {
        String claimChangeStatus = null;
        // 상태에 대한 확인
        switch (claimReqType.toUpperCase(Locale.KOREA)){
            // 승인
            case "CA" :
                claimChangeStatus = ClaimCode.CLAIM_STATUS_C2.getCode();
                break;
            // 철회
            case "CW" :
                claimChangeStatus = ClaimCode.CLAIM_STATUS_C4.getCode();
                break;
            // 보류
            case "CH" :
                claimChangeStatus = ClaimCode.CLAIM_STATUS_C8.getCode();
                break;
            // 거부
            case "CD" :
                claimChangeStatus = ClaimCode.CLAIM_STATUS_C9.getCode();
                break;
            // 완료
            case "CC" :
                claimChangeStatus = ClaimCode.CLAIM_STATUS_C3.getCode();
                break;
            default:
                claimChangeStatus = "";
                break;
        }
        return claimChangeStatus;
    }
}
