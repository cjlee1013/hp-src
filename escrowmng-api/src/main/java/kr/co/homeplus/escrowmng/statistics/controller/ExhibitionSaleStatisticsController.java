package kr.co.homeplus.escrowmng.statistics.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.escrowmng.statistics.model.ExhibitionSaleStatisticsExhDto;
import kr.co.homeplus.escrowmng.statistics.model.ExhibitionSaleStatisticsItemDto;
import kr.co.homeplus.escrowmng.statistics.model.ExhibitionSaleStatisticsSelectDto;
import kr.co.homeplus.escrowmng.statistics.service.ExhibitionSaleStatisticsService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/statistics")
@Api(tags = "통계 > 상품 주문통계 > 기획전별 판매현황")
public class ExhibitionSaleStatisticsController {

    private final ExhibitionSaleStatisticsService exhibitionSaleStatisticsService;

    @GetMapping("/getExhibitionSaleStatisticsExh")
    @ApiOperation("기획전별판매현황조회-기획전")
    public ResponseObject<List<ExhibitionSaleStatisticsExhDto>> getExhibitionSaleStatisticsExh(ExhibitionSaleStatisticsSelectDto exhibitionSaleStatisticsSelectDto) {
        return ResourceConverter.toResponseObject(exhibitionSaleStatisticsService.getExhibitionSaleStatisticsExh(exhibitionSaleStatisticsSelectDto));
    }

    @PostMapping("/getExhibitionSaleStatisticsItem")
    @ApiOperation("기획전별판매현황조회-기획전>상품")
    public ResponseObject<List<ExhibitionSaleStatisticsItemDto>> getExhibitionSaleStatisticsItem(@RequestBody ExhibitionSaleStatisticsSelectDto exhibitionSaleStatisticsSelectDto) {
        return ResourceConverter.toResponseObject(exhibitionSaleStatisticsService.getExhibitionSaleStatisticsItem(exhibitionSaleStatisticsSelectDto));
    }
}
