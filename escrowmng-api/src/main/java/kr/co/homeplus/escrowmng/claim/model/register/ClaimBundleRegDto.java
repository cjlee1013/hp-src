package kr.co.homeplus.escrowmng.claim.model.register;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "클레임 번들 DTO")
public class ClaimBundleRegDto {

    @ApiModelProperty(value= "클레임 묶음 배송번호")
    private long claimBundleNo;

    @ApiModelProperty(value= "클레임번호(환불:클레임번호)")
    private long claimNo;

    @ApiModelProperty(value= "결제번호")
    private long paymentNo;

    @ApiModelProperty(value= "주문번호")
    private long purchaseOrderNo;

    @ApiModelProperty(value= "배송번호")
    private long bundleNo;

    @ApiModelProperty(value= "클레임 타입(C: 취소 R: 반품 X: 교환)")
    private String claimType;

    @ApiModelProperty(value= "클레임 상태(C1: 신청/ C2: 승인/ C3: 완료/ C4: 철회/ C8: 보류/ C9: 거부)")
    private String claimStatus;

    @ApiModelProperty(value= "파트너 아이디")
    private String partnerId;

    @ApiModelProperty(value= "고객번호")
    private long userNo;

    @ApiModelProperty(value= "클레임수량")
    private long claimBundleQty;

    @ApiModelProperty(value= "원 클레임 묶음번호(교환/반품 전환시)")
    private long originClaimBundleNo;

    @ApiModelProperty(value= "클레임배송비 처리방법   Y:상품에 동봉인 경우 claim_addition 항목 중 pickup, delivery 는 is_apply-> N 설정  Y:상품에 동봉, N:환불금액 차감")
    private String claimShipFeeEnclose;

    @ApiModelProperty(value= "Admin에서만 설정이 가능  클레임배송비 계좌입금여부  Y:계좌입금, N:계좌입금 아님")
    private String claimShipFeeBank;

    @ApiModelProperty(value= "물류센터")
    private String warehouse;

    @ApiModelProperty(value= "배송비 미차감 여부(Y:미차감, N:차감 (delivery 만 적용))")
    private String isNoDiscountDeduct;

    @ApiModelProperty(value= "할인쿠폰 미차감 여부 (Y:미차감, N:차감)")
    private String isNoShipDeduct;

    @ApiModelProperty(value= "배송모바일번호(휴대폰번호)")
    private String shipMobileNo;

    @ApiModelProperty(value= "예약상품여부")
    private String reserveYn;

    @ApiModelProperty(value= "주문구분")
    private String orderCategory;

    @ApiModelProperty(value= "슬롯차감전송여부")
    private String closeSendYn;

    @ApiModelProperty(value= "등록일")
    private String regDt;

    @ApiModelProperty(value= "수정일")
    private String chgDt;

    @ApiModelProperty(value= "마켓주문번호")
    private String marketOrderNo;

}
