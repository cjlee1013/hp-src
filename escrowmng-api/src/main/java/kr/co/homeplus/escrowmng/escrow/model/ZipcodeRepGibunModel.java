package kr.co.homeplus.escrowmng.escrow.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 우편번호관련 동기화 Model - 대표지번조회
 */
@Getter
@Setter
public class ZipcodeRepGibunModel {
    /** 법정동 */
    private String courtEubmyndnNm;
    /** 지번주소 */
    private String gibunAddr;
}