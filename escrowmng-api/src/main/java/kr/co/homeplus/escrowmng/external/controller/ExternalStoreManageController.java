package kr.co.homeplus.escrowmng.external.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.core.valid.CustomCollectionValidator;
import kr.co.homeplus.escrowmng.external.model.ApiFulltext;
import kr.co.homeplus.escrowmng.external.model.RemoteShipManage;
import kr.co.homeplus.escrowmng.external.model.StoreCloseDayManage;
import kr.co.homeplus.escrowmng.external.model.StorePickupPlaceManage;
import kr.co.homeplus.escrowmng.external.model.StoreShiftManage;
import kr.co.homeplus.escrowmng.external.model.StoreSlotManage;
import kr.co.homeplus.escrowmng.external.model.StoreZipcodeManage;
import kr.co.homeplus.escrowmng.external.service.ApiFulltextService;
import kr.co.homeplus.escrowmng.external.service.ExternalStoreManageService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(tags = "[옴니연동] 점포정보관리")
@RestController
@RequiredArgsConstructor
@RequestMapping(value=("/external/store"), produces = MediaType.APPLICATION_JSON_VALUE )
public class ExternalStoreManageController {

    private final ExternalStoreManageService externalStoreManageService;
    private final CustomCollectionValidator customCollectionValidator;
    private final ApiFulltextService apiFulltextService;

    @PostMapping("/saveStoreShiftList")
    @ApiOperation(value = "점포 shift 정보 저장", response = Integer.class)
    public ResponseObject<Integer> saveStoreShift(@Valid  @RequestBody List<StoreShiftManage> storeShiftManageList
        , BindingResult bindingResult) throws Exception {
        log.info("[controller] saveStoreShift - {}", storeShiftManageList);

        // 연동로그 저장
        ApiFulltext apiFulltext =
            ApiFulltext.builder().apiType("saveStoreShift").apiKind("IN").reqDataFulltext(storeShiftManageList.toString()).reqDataSize(storeShiftManageList.size())
                .build();
        apiFulltextService.insertApiFulltext(apiFulltext);
        // request model list valid
        customCollectionValidator.validate(storeShiftManageList, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        return ResourceConverter.toResponseObject(externalStoreManageService.saveStoreShiftList(storeShiftManageList, apiFulltext));
    }

    @PostMapping("/saveStoreSlotList")
    @ApiOperation(value = "점포 slot 정보 저장", response = Integer.class)
    public ResponseObject<Integer> saveStoreSlot(@Valid @RequestBody List<StoreSlotManage> storeSlotManageList, BindingResult bindingResult) throws Exception {
        log.info("[controller] saveStoreSlotList - {}", storeSlotManageList);

        // 연동로그 저장
        ApiFulltext apiFulltext =
            ApiFulltext.builder().apiType("saveStoreSlot").apiKind("IN").reqDataFulltext(storeSlotManageList.toString()).reqDataSize(storeSlotManageList.size())
                .build();

        apiFulltextService.insertApiFulltext(apiFulltext);

        // request model list valid
        customCollectionValidator.validate(storeSlotManageList, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        return ResourceConverter.toResponseObject(externalStoreManageService.saveStoreSlotList(storeSlotManageList, apiFulltext));
    }

    @PostMapping("/saveStoreZipcodeList")
    @ApiOperation(value = "점포별 우편번호 정보 저장", response = Integer.class)
    public ResponseObject<Integer> saveStoreZipcodeList(@Valid @RequestBody List<StoreZipcodeManage> storeZipcodeManageList, BindingResult bindingResult) throws BindException {
        log.info("[controller] saveStoreZipcodeList - {}", storeZipcodeManageList);

        // 연동로그 저장
        ApiFulltext apiFulltext =
            ApiFulltext.builder().apiType("saveStoreZipcode").apiKind("IN").reqDataFulltext(storeZipcodeManageList.toString()).reqDataSize(storeZipcodeManageList.size())
                .build();

        apiFulltextService.insertApiFulltext(apiFulltext);

        // request model list valid
        customCollectionValidator.validate(storeZipcodeManageList, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        return ResourceConverter.toResponseObject(externalStoreManageService.saveStoreZipcodeList(storeZipcodeManageList, apiFulltext));
    }

    @PostMapping("/saveStoreCloseDayList")
    @ApiOperation(value = "점포별 휴일 정보 저장", response = Integer.class)
    public ResponseObject<Integer> saveStoreCloseDayList(@Valid @RequestBody List<StoreCloseDayManage> storeCloseDayManageList, BindingResult bindingResult) throws BindException {
        log.info("[controller] saveStoreCloseDayList - {}", storeCloseDayManageList);

        // 연동로그 저장
        ApiFulltext apiFulltext =
            ApiFulltext.builder().apiType("saveStoreCloseDay").apiKind("IN").reqDataFulltext(storeCloseDayManageList.toString()).reqDataSize(storeCloseDayManageList.size())
                .build();

        apiFulltextService.insertApiFulltext(apiFulltext);

        // request model list valid
        customCollectionValidator.validate(storeCloseDayManageList, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        return ResourceConverter.toResponseObject(externalStoreManageService.saveStoreCloseDayList(storeCloseDayManageList, apiFulltext));
    }

    @PostMapping("/saveStoreRemoteShipList")
    @ApiOperation(value = "점포 우편번호별 원거리배송 정보 저장", response = Integer.class)
    public ResponseObject<Integer> saveStoreRemoteShip(@Valid @RequestBody List<RemoteShipManage> remoteShipManageList, BindingResult bindingResult) throws Exception {
        log.info("[controller] saveStoreRemoteShipList - {}", remoteShipManageList);

        // 연동로그 저장
        ApiFulltext apiFulltext =
            ApiFulltext.builder().apiType("saveStoreRemoteShip").apiKind("IN").reqDataFulltext(remoteShipManageList.toString()).reqDataSize(remoteShipManageList.size())
                .build();

        apiFulltextService.insertApiFulltext(apiFulltext);

        // request model list valid
        customCollectionValidator.validate(remoteShipManageList, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        return ResourceConverter.toResponseObject(externalStoreManageService.saveRemoteShipList(remoteShipManageList, apiFulltext));
    }

    @PostMapping("/saveAllStoreRemoteShipList")
    @ApiOperation(value = "전체 점포 우편번호별 원거리배송 정보 저장", response = Integer.class)
    public ResponseObject<Integer> saveAllStoreRemoteShip(@Valid @RequestBody List<RemoteShipManage> remoteShipManageList, BindingResult bindingResult) throws Exception {
        log.info("[controller] saveAllStoreRemoteShipList - {}", remoteShipManageList);

        // request model list valid
        customCollectionValidator.validate(remoteShipManageList, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        return ResourceConverter.toResponseObject(externalStoreManageService.saveAllRemoteShipList(remoteShipManageList));
    }

    @PostMapping("/saveStorePickupPlaceList")
    @ApiOperation(value = "점포픽업장소정보 저장", response = Integer.class)
    public ResponseObject<Integer> saveStorePickupPlace(@Valid @RequestBody List<StorePickupPlaceManage> storePickupPlaceManageList, BindingResult bindingResult) throws Exception {
        log.info("[controller] saveStorePickupPlaceList - {}", storePickupPlaceManageList);

        // 연동로그 저장
        ApiFulltext apiFulltext =
            ApiFulltext.builder().apiType("saveStorePickupPlace").apiKind("IN").reqDataFulltext(storePickupPlaceManageList.toString()).reqDataSize(storePickupPlaceManageList.size())
                .build();

        apiFulltextService.insertApiFulltext(apiFulltext);

        // request model list valid
        customCollectionValidator.validate(storePickupPlaceManageList, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        return ResourceConverter.toResponseObject(externalStoreManageService.storePickupPlaceList(storePickupPlaceManageList, apiFulltext));
    }

//    @PostMapping("/saveStoreSlotOrderCntList")
//    @ApiOperation(value = "점포 slot 주문건수 정보 저장", response = Integer.class)
//    public ResponseObject<Integer> saveStoreSlotOrderCnt(@Valid @RequestBody List<StoreSlotManage> storeSlotManageList, BindingResult bindingResult) throws Exception {
//        log.info("[controller] saveStoreSlotOrderCntList - {}", storeSlotManageList);
//
//        // 연동로그 저장
//        ApiFulltext apiFulltext =
//            ApiFulltext.builder().apiType("saveStoreSlotOrderCnt").apiKind("IN").reqDataFulltext(storeSlotManageList.toString()).reqDataSize(storeSlotManageList.size())
//                .build();
//
//        apiFulltextService.insertApiFulltext(apiFulltext);
//
//        // request model list valid
//        customCollectionValidator.validate(storeSlotManageList, bindingResult);
//        if (bindingResult.hasErrors()) {
//            throw new BindException(bindingResult);
//        }
//
//        return ResourceConverter.toResponseObject(externalStoreManageService.saveStoreSlotOrderCntList(storeSlotManageList, apiFulltext));
//    }

}
