package kr.co.homeplus.escrowmng.pg.service;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.construct.TransactionManagerName;
import kr.co.homeplus.escrowmng.pg.mapper.CardPrefixManageMasterMapper;
import kr.co.homeplus.escrowmng.pg.mapper.CardPrefixManageSlaveMapper;
import kr.co.homeplus.escrowmng.pg.model.CardPrefixManageDto;
import kr.co.homeplus.escrowmng.pg.model.CardPrefixManageSelectDto;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class CardPrefixManageService {

    private final CardPrefixManageMasterMapper cardPrefixManageMasterMapper;
    private final CardPrefixManageSlaveMapper cardPrefixManageSlaveMapper;

    /**
     * 카드 PREFIX 리스트 조회
     * @param cardPrefixManageSelectDto
     * @return
     * @throws Exception
     */
    public List<CardPrefixManageDto> getCardPrefixManageList(CardPrefixManageSelectDto cardPrefixManageSelectDto) throws Exception {
        return cardPrefixManageSlaveMapper.selectCardPrefixManageList(cardPrefixManageSelectDto);
    }

    /**
     * 카드 PREFIX 저장/수정
     * @param cardPrefixManageDto
     * @return
     * @throws Exception
     */
    public int saveCardPrefixManage(CardPrefixManageDto cardPrefixManageDto) throws Exception {
        return cardPrefixManageMasterMapper.updateCardPrefixManage(cardPrefixManageDto);
    }

    /**
     * 카드 PREFIX 결제수단 일괄변경
     * @param cardPrefixManageDto
     * @return
     * @throws Exception
     */
    @Transactional(value = TransactionManagerName.ESCROW, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int saveBundleCardPrefixMethod(CardPrefixManageDto cardPrefixManageDto) throws Exception {
        int rtnVal = 0;

        try {
            if (EscrowString.isNotEmpty(cardPrefixManageDto.getCardPrefixManageDtoList())) {
                // loop 수행하면서 1건씩 업데이트
                for (CardPrefixManageDto cardPrefix : cardPrefixManageDto.getCardPrefixManageDtoList()) {
                    rtnVal += cardPrefixManageMasterMapper.updateCardPrefixManage(cardPrefix);
                }
            }
            return rtnVal;

        } catch (Exception e) {
            log.error("error:", e);
            rtnVal = -1;
            return rtnVal;

        } finally {
            log.info("saveBundleCardPrefixMethod-resultCount:" + rtnVal);
        }
    }
}
