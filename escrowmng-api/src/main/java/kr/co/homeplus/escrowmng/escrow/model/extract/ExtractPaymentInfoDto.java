package kr.co.homeplus.escrowmng.escrow.model.extract;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "결제정보 추출용 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class ExtractPaymentInfoDto {
    @ApiModelProperty(value = "상위결제수단명", position = 1)
    private String parentMethodNm;

    @ApiModelProperty(value = "카드명", position = 2)
    private String cardNm;

    @ApiModelProperty(value = "카드유형", position = 3)
    private String cardType;

    @ApiModelProperty(value = "결제건수", position = 4)
    private long payCnt;

    @ApiModelProperty(value = "결제금액", position = 5)
    private long payAmt;

    @ApiModelProperty(value = "객단가", position = 6)
    private long userUnitPrice;
}
