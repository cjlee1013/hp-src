package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단 관리 > 무이자혜택관리")
public class FreeInterestApplyDto {
    @ApiModelProperty(value = "적용순번")
    private Long applySeq;

    @ApiModelProperty(value = "카드무이자관리순번")
    @NotNull(message = "카드무이자관리순번")
    private Long freeInterestMngSeq;

    @ApiModelProperty(value = "카드사")
    @NotBlank(message = "카드사")
    private String methodCd;

    @ApiModelProperty(value = "결제금액")
    @Max(value = 10000, message = "결제금액")
    @Min(value = 1, message = "결제금액")
    private Integer paymentAmt;

    @ApiModelProperty(value = "무이자유형")
    @NotNull(message = "무이자유형")
    @Pattern(regexp = "(N|P)", message="무이자유형(무이자:N,부문무이자:P)")
    private String freeInterestType;

    @ApiModelProperty(value = "할부기간")
    @NotNull(message = "할부기간")
    @Pattern(regexp="(([1-9]{1}[0-9]{0,1})(\\,)){1,}", message="할부기간")
    @Size(min=1, max=50, message="할부기간")
    private String installmentTerm;

    @ApiModelProperty(value = "등록자ID")
    private String regId;
    @ApiModelProperty(value = "등록일자")
    private String regDt;
}