package kr.co.homeplus.escrowmng.pg.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.pg.model.ExcelItem;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodTreeDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyApplyDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyApplySelectDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyHistDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyHistSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyManageDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyManageSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentPolicyMethodSelectDto;
import org.apache.ibatis.annotations.Param;

@EscrowSlaveConnection
public interface PaymentPolicyManageSlaveMapper {
    List<PaymentPolicyManageDto> selectPaymentPolicyMng(PaymentPolicyManageSelectDto paymentPolicyManageSelectDto);
    List<PaymentMethodTreeDto> selectPaymentPolicyMethodTree(PaymentPolicyMethodSelectDto paymentPolicyMethodSelectDto);
    List<PaymentPolicyApplyDto> selectPaymentPolicyApplyList(PaymentPolicyApplySelectDto paymentPolicyApplySelectDtoDto);
    List<PaymentPolicyHistDto> selectPaymentPolicyHistory(PaymentPolicyHistSelectDto paymentPolicyHistSelectDto);
    List<ExcelItem> selectItemValidCheck(@Param("itemNoList") List<String> itemNoList);
}
