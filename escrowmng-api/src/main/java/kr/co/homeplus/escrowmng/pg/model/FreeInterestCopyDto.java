package kr.co.homeplus.escrowmng.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단 관리 > 무이자 혜택 관리 DTO")
public class FreeInterestCopyDto {
    @ApiModelProperty(value = "무이자혜택기본정보")
    private FreeInterestDto freeInterestDto;

    @ApiModelProperty(value = "카드적용대상")
    private List<FreeInterestApplyDto> freeInterestApplyDtoList;

    @ApiModelProperty(value = "등록자id")
    private String regId;
    @ApiModelProperty(value = "등록일자")
    private String regDt;
    @ApiModelProperty(value = "수정자id")
    private String chgId;
    @ApiModelProperty(value = "수정일자")
    private String chgDt;


}
