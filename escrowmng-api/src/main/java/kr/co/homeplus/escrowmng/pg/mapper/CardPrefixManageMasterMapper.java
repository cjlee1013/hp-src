package kr.co.homeplus.escrowmng.pg.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.pg.model.CardPrefixManageDto;

@EscrowMasterConnection
public interface CardPrefixManageMasterMapper {
    int updateCardPrefixManage(CardPrefixManageDto cardPrefixManageDto);
}
