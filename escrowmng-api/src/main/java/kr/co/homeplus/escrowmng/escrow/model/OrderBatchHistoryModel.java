package kr.co.homeplus.escrowmng.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "주문 배치 이력 model")
@Getter
@Setter
@EqualsAndHashCode
public class OrderBatchHistoryModel {
    @ApiModelProperty(value = "이력번호(Seq)", position = 1)
    private String historyNo;

    @ApiModelProperty(value = "배치번호(seq)", position = 2)
    private String batchNo;

    @ApiModelProperty(value = "배치시작일시", position = 3)
    private String batchStartDt;

    @ApiModelProperty(value = "배치종료일시", position = 4)
    private String batchEndDt;

    @ApiModelProperty(value = "배치진행시간", position = 5)
    private String batchTime;

    @ApiModelProperty(value = "실패건수", position = 6)
    private long failCount;

    @ApiModelProperty(value = "성공건수", position = 7)
    private long successCount;

    @ApiModelProperty(value = "전체건수", position = 8)
    private long totalCount;

    @ApiModelProperty(value = "오류메시지", position = 9)
    private String errorMessage;

    @ApiModelProperty(value = "메타필드1", position = 10)
    private String metaField1;

    @ApiModelProperty(value = "메타필드2", position = 11)
    private String metaField2;

    @ApiModelProperty(value = "메타필드3", position = 12)
    private String metaField3;

    @ApiModelProperty(value = "메타필드4", position = 13)
    private String metaField4;

    @ApiModelProperty(value = "메타필드5", position = 14)
    private String metaField5;
}
