package kr.co.homeplus.escrowmng.pg.service;

import kr.co.homeplus.escrowmng.constants.Constants;
import kr.co.homeplus.escrowmng.pg.mapper.PgCompareSlaveMapper;
import kr.co.homeplus.escrowmng.pg.model.PgCompareApprovalDiffDto;
import kr.co.homeplus.escrowmng.pg.model.PgCompareApprovalSelectDto;
import kr.co.homeplus.escrowmng.pg.model.PgCompareApprovalSumDto;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PgCompareApprovalService {

    private final PgCompareSlaveMapper pgCompareSlaveMapper;

    /**
     * PG 승인 합계내역 조회
     * @param pgCompareApprovalSelectDto
     * @return
     */
    public List<PgCompareApprovalSumDto> getPgCompareApprovalSum(PgCompareApprovalSelectDto pgCompareApprovalSelectDto) {
        pgCompareApprovalSelectDto.setSchFromDt(EscrowString.NVL(pgCompareApprovalSelectDto.getSchFromDt()).replace(Constants.HYPHEN,Constants.EMPTY_STRING));
        pgCompareApprovalSelectDto.setSchEndDt(EscrowString.NVL(pgCompareApprovalSelectDto.getSchEndDt()).replace(Constants.HYPHEN,Constants.EMPTY_STRING));
        return pgCompareSlaveMapper.selectPgCompareApprovalSum(pgCompareApprovalSelectDto);
    }

    /**
     * PG 승인 차이내역 조회
     * @param pgCompareApprovalSelectDto
     * @return
     */
    public List<PgCompareApprovalDiffDto> getPgCompareApprovalDiff(PgCompareApprovalSelectDto pgCompareApprovalSelectDto) {
        pgCompareApprovalSelectDto.setSchFromDt(EscrowString.NVL(pgCompareApprovalSelectDto.getSchFromDt()).replace(Constants.HYPHEN,Constants.EMPTY_STRING));
        pgCompareApprovalSelectDto.setSchEndDt(EscrowString.NVL(pgCompareApprovalSelectDto.getSchEndDt()).replace(Constants.HYPHEN,Constants.EMPTY_STRING));
        return pgCompareSlaveMapper.selectPgCompareApprovalDiff(pgCompareApprovalSelectDto);
    }
}
