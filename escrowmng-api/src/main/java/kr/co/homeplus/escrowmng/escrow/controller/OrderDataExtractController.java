package kr.co.homeplus.escrowmng.escrow.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.escrow.model.extract.ExtractAddCouponUseOrderDto;
import kr.co.homeplus.escrowmng.escrow.model.extract.ExtractCartCouponUseOrderDto;
import kr.co.homeplus.escrowmng.escrow.model.extract.ExtractPaymentInfoDto;
import kr.co.homeplus.escrowmng.escrow.model.extract.ExtractPickupOrderDto;
import kr.co.homeplus.escrowmng.escrow.model.extract.OrderDataExtractSelectDto;
import kr.co.homeplus.escrowmng.escrow.service.OrderDataExtractService;
import kr.co.homeplus.escrowmng.extract.service.ExtractHistoryService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/escrow/extract")
@Api(tags = "시스템관리 > 시스템관리 > 주문데이터추출")
public class OrderDataExtractController {
    private final ExtractHistoryService extractHistoryService;
    private final OrderDataExtractService orderDataExtractService;

    /**
     * 장바구니쿠폰(농활쿠폰) 사용주문 리스트 조회
     */
    @PostMapping("/getCartCouponUseOrderList")
    @ApiOperation(value = "장바구니쿠폰(농활쿠폰) 사용주문 리스트 조회")
    public ResponseObject<List<ExtractCartCouponUseOrderDto>> getCartCouponUseOrderList(@RequestBody @Valid OrderDataExtractSelectDto orderDataExtractSelectDto) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(orderDataExtractSelectDto.getExtractCommonSetDto());
        // 장바구니쿠폰 사용주문 리스트 조회
        return ResourceConverter.toResponseObject(orderDataExtractService.getCartCouponUseOrderList(orderDataExtractSelectDto));
    }

    /**
     * 픽업주문 리스트 조회
     */
    @PostMapping("/getPickupOrderList")
    @ApiOperation(value = "픽업주문 리스트 조회")
    public ResponseObject<List<ExtractPickupOrderDto>> getPickupOrderList(@RequestBody OrderDataExtractSelectDto orderDataExtractSelectDto) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(orderDataExtractSelectDto.getExtractCommonSetDto());
        // 픽업주문 리스트 조회
        return ResourceConverter.toResponseObject(orderDataExtractService.getPickupOrderList(orderDataExtractSelectDto));
    }
    
    /**
     * 결제수단별 결제정보 리스트 조회
     */
    @PostMapping("/getPaymentInfoListByMethod")
    @ApiOperation(value = "결제수단별 결제정보 리스트 조회")
    public ResponseObject<List<ExtractPaymentInfoDto>> getPaymentInfoListByMethod(@RequestBody OrderDataExtractSelectDto orderDataExtractSelectDto) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(orderDataExtractSelectDto.getExtractCommonSetDto());
        // 결제수단별 결제정보 리스트 조회
        return ResourceConverter.toResponseObject(orderDataExtractService.getPaymentInfoListByMethod(orderDataExtractSelectDto));
    }

    /**
     * 보조결제수단 결제정보 리스트 조회
     */
    @PostMapping("/getAssistancePaymentInfoList")
    @ApiOperation(value = "보조결제수단 결제정보 리스트 조회")
    public ResponseObject<List<ExtractPaymentInfoDto>> getAssistancePaymentInfoList(@RequestBody OrderDataExtractSelectDto orderDataExtractSelectDto) {
        return ResourceConverter.toResponseObject(orderDataExtractService.getAssistancePaymentInfoList(orderDataExtractSelectDto));
    }

    /**
     * 중복쿠폰 사용주문 리스트 조회
     */
    @PostMapping("/getAddCouponUseOrderList")
    @ApiOperation(value = "중복쿠폰 사용주문 리스트 조회")
    public ResponseObject<List<ExtractAddCouponUseOrderDto>> getAddCouponUseOrderList(@RequestBody @Valid OrderDataExtractSelectDto orderDataExtractSelectDto) {
        // 조회 히스토리 저장
        extractHistoryService.saveExtractHistory(orderDataExtractSelectDto.getExtractCommonSetDto());
        // 중복쿠폰 사용주문 리스트 조회
        return ResourceConverter.toResponseObject(orderDataExtractService.getAddCouponUseOrderList(orderDataExtractSelectDto));
    }
}
