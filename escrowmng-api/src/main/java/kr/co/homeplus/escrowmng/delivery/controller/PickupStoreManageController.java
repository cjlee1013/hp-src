package kr.co.homeplus.escrowmng.delivery.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.escrowmng.delivery.model.PickupStoreLockerModel;
import kr.co.homeplus.escrowmng.delivery.model.PickupStoreManageModel;
import kr.co.homeplus.escrowmng.delivery.model.PickupStoreManageSelectModel;
import kr.co.homeplus.escrowmng.delivery.service.PickupStoreManageService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/escrow/storeDelivery")
@Api(tags = "배송관리 > 점포배송정보 > 픽업점포관리")
public class PickupStoreManageController {
    private final PickupStoreManageService pickupStoreManageService;

    /**
     * 픽업점포관리 리스트 조회
     */
    @PostMapping("/getPickupStoreManageList")
    @ApiOperation(value = "픽업점포관리 리스트 조회")
    public ResponseObject<List<PickupStoreManageModel>> getPickupStoreManageList(@RequestBody PickupStoreManageSelectModel pickupStoreManageSelectModel) {
        return ResourceConverter.toResponseObject(pickupStoreManageService.getPickupStoreManageList(pickupStoreManageSelectModel));
    }

    /**
     * 픽업점포관리 락커정보 조회
     */
    @GetMapping("/getPickupStoreLockerList")
    @ApiOperation(value = "픽업점포 락커 리스트 조회")
    public ResponseObject<List<PickupStoreLockerModel>> getPickupStoreLockerList(@RequestParam(value = "storeId") int storeId) {
        return ResourceConverter.toResponseObject(pickupStoreManageService.getPickupStoreLockerList(storeId));
    }

}
