package kr.co.homeplus.escrowmng.claim.model.entity;

public class ClaimOptEntity {
    // 클레임 옵션번호
    public String claimOptNo;
    // 클레임 번호
    public String claimNo;
    // 클레임 묶음번호
    public String claimBundleNo;
    // 클레임 상품번호
    public String claimItemNo;
    // 주문 옵션번호
    public String orderOptNo;
    // 옵션번호
    public String optNo;
    // 옵션금액
    public String optPrice;
    // 클레임 옵션 수량
    public String claimOptQty;
    // 등록일
    public String regDt;
    // 마켓연동주문번호
    public String marketOrderNo;
    // 마켓연동상품주문번호
    public String marketOrderItemNo;
}
