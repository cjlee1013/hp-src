package kr.co.homeplus.escrowmng.delivery.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.delivery.model.StoreZipcodeModel;
import kr.co.homeplus.escrowmng.delivery.model.StoreZipcodeSelectModel;
import kr.co.homeplus.escrowmng.delivery.service.StoreZipcodeService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/escrow/storeDelivery")
@Api(tags = "배송관리 > 점포배송정보 > 점포별 우편번호 조회")
public class StoreZipcodeController {

    private final StoreZipcodeService storeZipcodeService;

    @GetMapping("/getStoreZipcodeList")
    @ApiOperation(value = "점포별 우편번호 조회", response = StoreZipcodeModel.class)
    public ResponseObject<List<StoreZipcodeModel>> getStoreZipcodeList(@Valid StoreZipcodeSelectModel storeZipcodeSelectModel) {
        return ResourceConverter.toResponseObject(storeZipcodeService.getStoreZipcodeList(storeZipcodeSelectModel));
    }

}
