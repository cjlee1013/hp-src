package kr.co.homeplus.escrowmng.order.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.escrowmng.core.exception.LogicException;
import kr.co.homeplus.escrowmng.enums.ExceptionCode;
import kr.co.homeplus.escrowmng.order.mapper.OrderReadMapper;
import kr.co.homeplus.escrowmng.order.mapper.OrderWriteMapper;
import kr.co.homeplus.escrowmng.order.model.management.OrderHistoryListDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchClaimDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchDetailDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchPaymentDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchPriceDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchProdDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchShipDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchGetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchSetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchSubstitutionDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchTckGetDto;
import kr.co.homeplus.escrowmng.order.model.management.OrderSearchTckSetDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.MultiOrderShipAddrInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderCashReceiptDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderCouponDiscountDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderDelayShipInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderGiftInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderPartnerInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderPromoInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderSaveInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderShipAddrInfoDto;
import kr.co.homeplus.escrowmng.order.model.managementPop.OrderShipTotalInfoDto;
import kr.co.homeplus.escrowmng.order.model.ship.MultiOrderShipAddrEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipInfoGetDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipInfoSetDto;
import kr.co.homeplus.escrowmng.order.model.ship.NoRcvShipSetGto;
import kr.co.homeplus.escrowmng.order.model.ship.OrderShipAddrEditDto;
import kr.co.homeplus.escrowmng.order.model.ship.OrderShipOriginInfo;
import kr.co.homeplus.escrowmng.order.model.ship.StoreShipMemoDto;
import kr.co.homeplus.escrowmng.order.model.ship.StoreShipMsgDto;
import kr.co.homeplus.escrowmng.order.service.OrderManagementMapperService;
import kr.co.homeplus.escrowmng.utils.SqlUtils;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderManagementMapperServiceImpl implements OrderManagementMapperService {

    private final OrderReadMapper readMapper;
    private final OrderWriteMapper writeMapper;

    @Override
    public List<OrderSearchGetDto> getOrderManagementInfo(OrderSearchSetDto orderSearchSetDto) {
        List<OrderSearchGetDto> orderSearchGetDtoList = readMapper.selectOrderSearchInfo(orderSearchSetDto);
        if (ObjectUtils.isNotEmpty(orderSearchGetDtoList)) {
            for (OrderSearchGetDto next : orderSearchGetDtoList) {
                next.setBuyerNm(PrivacyMaskingUtils.maskingUserName(next.getBuyerNm()));
                next.setBuyerMobileNo(PrivacyMaskingUtils.maskingPhone(next.getBuyerMobileNo()));
                next.setReceiverNm(PrivacyMaskingUtils.maskingUserName(next.getReceiverNm()));
                next.setReceiverMobileNo(PrivacyMaskingUtils.maskingPhone(next.getReceiverMobileNo()));
            }
        }
        return orderSearchGetDtoList;
    }

    @Override
    public OrderSearchProdDto getOrderDetailByUser(long purchaseOrderNo){
        return readMapper.selectOrderDetailUserInfo(purchaseOrderNo);
    }

    @Override
    public OrderSearchPriceDto getOrderDetailByPrice(long purchaseOrderNo){
        return readMapper.selectOrderDetailPriceInfo(purchaseOrderNo);
    }

    @Override
    public List<OrderSearchDetailDto> getOrderDetailByPurchase(long purchaseOrderNo) {
        List<OrderSearchDetailDto> resultDto = readMapper.selectOrderDetailPurchaseInfo(purchaseOrderNo);
        String claimCmbnYn = this.getCmbnClaimInfo(purchaseOrderNo);
        for(OrderSearchDetailDto dto : resultDto){
            dto.setCmbnClaimYn(claimCmbnYn);
        }
        return resultDto;
    }

    @Override
    public List<OrderSearchDetailDto> getMultiOrderDetailByPurchase(long purchaseOrderNo) {
        List<OrderSearchDetailDto> resultDto = readMapper.selectMultiOrderDetailPurchaseInfo(purchaseOrderNo);
        String claimCmbnYn = this.getCmbnClaimInfo(purchaseOrderNo);
        for(OrderSearchDetailDto dto : resultDto){
            dto.setCmbnClaimYn(claimCmbnYn);
        }
        return resultDto;
    }

    public List<OrderSearchPaymentDto> getOrderDetailByPayment(long purchaseOrderNo) {
        List<OrderSearchPaymentDto> paymentDtoList = readMapper.selectOrderDetailPaymentInfo(purchaseOrderNo);
        if (ObjectUtils.isNotEmpty(paymentDtoList)) {
            for(OrderSearchPaymentDto paymentDto : paymentDtoList){
                if(ObjectUtils.isNotEmpty(paymentDto.getReqIp())){
                    paymentDto.setReqIp(PrivacyMaskingUtils.maskingIP(paymentDto.getReqIp()));
                }
            }
        }
        return paymentDtoList;
    }

    @Override
    public List<OrderSearchShipDto> getOrderDetailByShipping(long purchaseOrderNo, String mallType) {
        return readMapper.selectOrderDetailShippingInfo(purchaseOrderNo, mallType);
    }

    @Override
    public List<OrderSearchShipDto> getMultiOrderDetailByShipping(long purchaseOrderNo) {
        return readMapper.selectMultiOrderDetailShippingInfo(purchaseOrderNo);
    }

    @Override
    public List<OrderSearchClaimDto> getOrderDetailByClaim(long purchaseOrderNo) {
        return readMapper.selectOrderDetailClaimInfo(purchaseOrderNo);
    }

    @Override
    public List<OrderShipAddrInfoDto> getOrderShipAddrInfo(long bundleNo) {
        return readMapper.selectOrderShipAddrInfo(bundleNo);
    }

    @Override
    public List<MultiOrderShipAddrInfoDto> getMultiOrderShipAddrInfo(long bundleNo) {
        return readMapper.selectMultiOrderShipAddrInfo(bundleNo);
    }

    @Override
    public List<OrderShipTotalInfoDto> getOrderShipTotalInfo(long bundleNo, String shipStatus) {
        return readMapper.selectOrderShipInfo(bundleNo, shipStatus);
    }

    @Override
    public Boolean modifyShipAddrChange(OrderShipAddrEditDto shipAddrEditDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateShippingAddrEditCheck(shipAddrEditDto));
    }

    @Override
    public Boolean modifyMultiOrderShipAddrInfo(MultiOrderShipAddrEditDto multiOrderShipAddrEditDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateMultiOrderShippingAddrInfo(multiOrderShipAddrEditDto));
    }

    @Override
    public Boolean addNoRcvShipInfo(NoRcvShipSetGto noRcvShipSetGto) throws Exception {
        try {
            return SqlUtils.isGreaterThanZero(writeMapper.insertNoRcvShip(noRcvShipSetGto));
        } catch (Exception e){
            log.debug("addNoRcvShipInfo SQL Exception ::: [{}]", e.getMessage());
            throw new LogicException(ExceptionCode.SYS_ERROR_CODE_9004);
        }
    }

    @Override
    public Boolean modifyNoRcvShipInfo(NoRcvShipEditDto noRcvShipEditDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateNoRcvShip(noRcvShipEditDto));
    }

    @Override
    public NoRcvShipInfoGetDto getNoRcvShipInfo(NoRcvShipInfoSetDto noRcvShipInfoSetDto) {
        return readMapper.selectNoRcvShipInfo(noRcvShipInfoSetDto);
    }

    @Override
    public LinkedHashMap<String, Object> getNoRcvShipRegMakeData(long bundleNo) {
        return readMapper.selectNoRcvMakeData(bundleNo);
    }

    @Override
    public HashMap<String, Object> getNoReceiveCheck(long bundleNo) {
        return readMapper.selectNoRcvDuplCheck(bundleNo);
    }

    @Override
    public OrderDelayShipInfoDto getDelayShipInfo(long shipNo) {
        return readMapper.selectShippingDelayInfo(shipNo);
    }

    @Override
    public OrderPartnerInfoDto getOrderPartnerInfo(String partnerId, long bundleNo) {
        return readMapper.selectOrderPartnerInfo(partnerId, bundleNo);
    }

    @Override
    public List<OrderPromoInfoDto> getOrderPromoPopInfo(long purchaseOrderNo) {
        return readMapper.selectOrderPromoPopInfo(purchaseOrderNo);
    }

    @Override
    public List<OrderCouponDiscountDto> getOrderCouponDiscountPopInfo(long purchaseOrderNo) {
        return readMapper.selectCouponDiscountPopInfo(purchaseOrderNo);
    }

    @Override
    public List<OrderGiftInfoDto> getOderGiftPopInfo(long purchaseOrderNo) {
        return readMapper.selectOrderGiftPopInfo(purchaseOrderNo);
    }

    @Override
    public List<OrderSearchSubstitutionDto> getOrderSubstitutionInfo(long purchaseOrderNo) {
        return readMapper.selectOrderSubstitutionData(purchaseOrderNo);
    }

    @Override
    public LinkedHashMap<String, Object> getSafetyIssueInfo(long bundleNo) {
        return readMapper.selectSafetyIssueInfo(bundleNo);
    }

    @Override
    public List<OrderSaveInfoDto> getOrderSavePopInfo(long purchaseOrderNo) {
        return readMapper.selectOrderSavePopInfo(purchaseOrderNo);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getShippingPresentStatus(long purchaseOrderNo){
        return readMapper.selectShippingPresentStatus(purchaseOrderNo);
    }

    @Override
    public String getCmbnClaimInfo(long purchaseOrderNo){
        return readMapper.selectCmbnClaimInfo(purchaseOrderNo);
    }

    @Override
    public List<OrderSearchTckGetDto> getOrderManagementInfoTck(OrderSearchTckSetDto orderSearchSetDto) {
        List<OrderSearchTckGetDto> orderSearchGetDtoList = readMapper.selectOrderSearchInfoTck(
            orderSearchSetDto);
        if (ObjectUtils.isNotEmpty(orderSearchGetDtoList)) {
            for (OrderSearchTckGetDto next : orderSearchGetDtoList) {
                next.setBuyerNm(PrivacyMaskingUtils.maskingUserName(next.getBuyerNm()));
                next.setBuyerMobileNo(PrivacyMaskingUtils.maskingPhone(next.getBuyerMobileNo()));
                next.setReceiverNm(PrivacyMaskingUtils.maskingUserName(next.getReceiverNm()));
                next.setReceiverMobileNo(
                    PrivacyMaskingUtils.maskingPhone(next.getReceiverMobileNo()));
            }
        }
        return orderSearchGetDtoList;
    }

    @Override
    public StoreShipMemoDto getStoreShipMemoInfo(long purchaseOrderNo, long bundleNo){
        return readMapper.selectStoreShipMemoInfo(purchaseOrderNo, bundleNo);
    }

    @Override
    public StoreShipMsgDto getStoreShipMsgInfo(long purchaseOrderNo) {
        return readMapper.selectStoreShipMsgInfo(purchaseOrderNo);
    }

    @Override
    public Boolean setStoreShipMemoInfo(StoreShipMemoDto memoDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateStoreShipMemo(memoDto));
    }

    @Override
    public List<OrderHistoryListDto> getOrderHistoryList(long purchaseOrderNo) {
        return readMapper.selectOrderHistoryList(purchaseOrderNo);
    }

    @Override
    public LinkedHashMap<String, Object> getNonShipCompleteInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectNonShipCompleteInfo(purchaseOrderNo, bundleNo);
    }

    @Override
    public OrderCashReceiptDto getOrderCashReceiptInfo(long purchaseOrderNo) {
        return readMapper.selectOrderCashReceiptInfo(purchaseOrderNo);
    }

    @Override
    public String getOrderTypeCheck(long purchaseOrderNo) {
        return readMapper.selectOrderTypeCheck(purchaseOrderNo);
    }

    @Override
    public OrderShipOriginInfo getOriginPurchaseBundleInfo(long purchaseOrderNo) {
        return readMapper.selectOriPurchaseOrderInfoFromCombine(purchaseOrderNo);
    }
}
