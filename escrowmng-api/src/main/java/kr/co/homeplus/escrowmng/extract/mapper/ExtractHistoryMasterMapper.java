package kr.co.homeplus.escrowmng.extract.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.extract.model.ExtractCommonSetDto;
import org.apache.ibatis.annotations.Param;

@EscrowMasterConnection
public interface ExtractHistoryMasterMapper {
    int insertExtractHistory(@Param("extractCommonSetDto") ExtractCommonSetDto extractCommonSetDto);
}
