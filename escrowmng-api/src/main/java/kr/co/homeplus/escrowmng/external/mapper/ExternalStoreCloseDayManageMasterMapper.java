package kr.co.homeplus.escrowmng.external.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.external.model.StoreCloseDayManage;
import org.apache.ibatis.annotations.Param;

@EscrowMasterConnection
public interface ExternalStoreCloseDayManageMasterMapper {

    int insertStoreCloseDayMng(StoreCloseDayManage storeCloseDayManage);

    void deleteStoreCloseDay(@Param("closeDaySeq") String closeDaySeq);

    void insertStoreCloseDay(@Param("closeDaySeq") String closeDaySeq, @Param("storeIdList") List<String> storeIdList, @Param("storeType") String storeType);

}
