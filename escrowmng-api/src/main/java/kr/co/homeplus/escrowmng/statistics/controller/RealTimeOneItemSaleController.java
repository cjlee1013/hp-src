package kr.co.homeplus.escrowmng.statistics.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.homeplus.escrowmng.core.exception.ExternalException;
import kr.co.homeplus.escrowmng.enums.ExceptionCode;
import kr.co.homeplus.escrowmng.statistics.model.RealTimeOneItemSaleGetDto;
import kr.co.homeplus.escrowmng.statistics.model.RealTimeOneItemSaleSearchSetDto;
import kr.co.homeplus.escrowmng.statistics.service.RealTimeOneItemSaleService;
import kr.co.homeplus.escrowmng.utils.EscrowDate;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/escrow/statistics")
@Api(tags = "통계 > 상품 주문통계 / 실시간 특정 상품 판매 현황")
public class RealTimeOneItemSaleController {
    private final RealTimeOneItemSaleService realTimeOneItemSaleService;

    /**
     * 실시간 특정 상품 판매 현황
     */
    @PostMapping("/getRealTimeOneItemSaleList")
    @ApiOperation(value = "실시간 특정 상품 판매 현황")
    public ResponseObject<List<RealTimeOneItemSaleGetDto>> getRealTimeOneItemSaleList(
        @RequestBody RealTimeOneItemSaleSearchSetDto realTimeOneItemSaleSearchSetDto) {

        if(StringUtils.isEmpty(realTimeOneItemSaleSearchSetDto.getSchStartDt())) {
            throw new ExternalException(ExceptionCode.ERROR_CODE_1003, "주문일시");
        }

        if(StringUtils.isEmpty(realTimeOneItemSaleSearchSetDto.getSchEndDt())) {
            throw new ExternalException(ExceptionCode.ERROR_CODE_1003, "주문일시");
        }

        int dateDiff = EscrowDate.compareDateTimeStr(realTimeOneItemSaleSearchSetDto.getSchEndDt() + ":00", realTimeOneItemSaleSearchSetDto.getSchStartDt() + ":00");
        if (dateDiff > 1) {
            throw new ExternalException(ExceptionCode.ERROR_CODE_1079, "최대 24시간 이내만 조회 가능합니다.");
        }
        if (dateDiff < 0) {
            throw new ExternalException(ExceptionCode.ERROR_CODE_1079, "시작일이 종료일보다 클 수 없습니다.");
        }

        if(StringUtils.isEmpty(realTimeOneItemSaleSearchSetDto.getSchItemNo())) {
            throw new ExternalException(ExceptionCode.ERROR_CODE_1003, "상품번호");
        }

        // 실시간 특정 상품 판매현황 조회
        return ResourceConverter.toResponseObject(realTimeOneItemSaleService.getRealTimeOneItemSaleList(realTimeOneItemSaleSearchSetDto));
    }

}
