package kr.co.homeplus.escrowmng.claim.model.outOfStock;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OutOfStockItemListGetDto {

    @ApiModelProperty(value = "점포명", position = 1)
    public String storeNm;

    @ApiModelProperty(value = "대체여부", position = 2)
    public String substitutionYn;

    @ApiModelProperty(value = "대체상태", position = 3)
    public String subStatus;

    @ApiModelProperty(value = "취소상태", position = 4)
    public String refundStatus;

    @ApiModelProperty(value = "단축번호", position = 4)
    public String sordNo;

    @ApiModelProperty(value = "주문번호", position = 5)
    public String purchaseOrderNo;

    @ApiModelProperty(value = "배송일", position = 6)
    public String shipDt;

    @ApiModelProperty(value = "배송shift", position = 7)
    public String shiftId;

    @ApiModelProperty(value = "주문상품ID", position = 8)
    public String itemNo;

    @ApiModelProperty(value = "주문일", position = 9)
    public String orderDt;

    @ApiModelProperty(value = "주문상품명", position = 10)
    public String itemNm;

    @ApiModelProperty(value = "주문수량", position = 11)
    public String itemQty;

    @ApiModelProperty(value = "결품수량", position = 12)
    public String oosQty;

    @ApiModelProperty(value = "금액", position = 13)
    public String itemPrice;

    @ApiModelProperty(value = "대체주문번호", position = 14)
    public String subPurchaseOrderNo;

    @ApiModelProperty(value = "대체상품ID", position = 15)
    public String subItemNo;

    @ApiModelProperty(value = "대체상품명", position = 16)
    public String subItemNm;

    @ApiModelProperty(value = "대체수량", position = 17)
    public String subItemQty;

    @ApiModelProperty(value = "대체금액", position = 18)
    private String subItemPrice;

    @ApiModelProperty(value = "실패사유(사유코드)", position = 19)
    private String returnMessage;
}
