package kr.co.homeplus.escrowmng.pg.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.pg.model.ChargeDiscountBenefitManageDto;
import kr.co.homeplus.escrowmng.pg.model.ChargeDiscountBenefitManageSelectDto;

@EscrowSlaveConnection
public interface ChargeDiscountBenefitSlaveMapper {
    List<ChargeDiscountBenefitManageDto> selectChargeDiscountBenefitManageList(ChargeDiscountBenefitManageSelectDto chargeDiscountBenefitManageSelectDto);
    int getChargeDiscountBenefitDuplicate(ChargeDiscountBenefitManageDto chargeDiscountBenefitManageDto);
}
