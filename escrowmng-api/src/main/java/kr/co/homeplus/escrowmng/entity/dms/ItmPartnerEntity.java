package kr.co.homeplus.escrowmng.entity.dms;

public class ItmPartnerEntity {
    //
    public String partnerId;
    //
    public String partnerType;
    //
    public String operatorType;
    //
    public String partnerGrade;
    //
    public String partnerStatus;
    //
    public String partnerNm;
    //
    public String partnerOwner;
    //
    public String partnerNo;
    //
    public String communityNotiNo;
    //
    public String businessConditions;
    //
    public String bizCateCd;
    //
    public String zipcode;
    //
    public String addr1;
    //
    public String addr2;
    //
    public String contractFlag;
    //
    public String partnerPw;
    //
    public String regDt;
    //
    public String regId;
    //
    public String chgDt;
    //
    public String chgId;
}
