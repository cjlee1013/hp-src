package kr.co.homeplus.escrowmng.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "주문 배치 이력 select model")
@Getter
@Setter
public class OrderBatchHistorySelectModel {
    @ApiModelProperty(value = "배치번호")
    private String batchNo;

    @ApiModelProperty(value = "조회시작일시", required = true)
    @NotEmpty(message = "조회시작일시")
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일시", required = true)
    @NotEmpty(message = "조회종료일시")
    private String schEndDt;

    @ApiModelProperty(value = "배치타입(=LGW 배치명)")
    private String batchType;
}
