package kr.co.homeplus.escrowmng.claim.model.entity;

public class ClaimRefundEntity {

    // 클레임번호(환불:클레임번호)
    public String claimNo;

    // 결제번호
    public String paymentNo;

    // 주문번호
    public String purchaseOrderNo;

    // 결제 상태(환불상태 F1:요청, F2:진행중(주결제 환불완료), F3:완료, F8: 실패 (주결제 환불실패), F9:실패 (포인트 환불실패)
    public String refundStatus;

    // 거래유형(1:미설정, 2:위탁, 3:직매입(점포)
    public String transType;

    // 환불 신청 금액((refund_amt - addition_amt - deduct_amt) = (pg_amt + point_amt + MHC_amt + DGV_amt + OCB_amt) (pg_amt + point_amt + MHC + DGV + OCB) = 0 인경우 claim_payment 생성안함)
    public String refundAmt;

    // 클레임 차감금액 - 배송비/쿠폰비/즉시할인((refund_amt - addition_amt - deduct_amt) = (pg_amt + point_amt + MHC_amt + DGV_amt + OCB_amt))
    public String additionAmt;

    // 클레임 차감금액 - 공제금((refund_amt - addition_amt - deduct_amt) = (pg_amt + point_amt + MHC_amt + DGV_amt + OCB_amt))
    public String deductAmt;

    // 주결제수단 환불금액((refund_amt - addition_amt - deduct_amt) = (pg_amt + point_amt + MHC_amt + DGV_amt + OCB_amt))
    public String pgAmt;

    // 마일리지 환불금액((refund_amt - addition_amt - deduct_amt) = (pg_amt + point_amt + MHC_amt + DGV_amt + OCB_amt))
    public String mileageAmt;

    // MHC 포인트 환불금액((refund_amt - addition_amt - deduct_amt) = (pg_amt + point_amt + MHC_amt + DGV_amt + OCB_amt))
    public String mhcAmt;

    // DGV 상품권 환불금액((refund_amt - addition_amt - deduct_amt) = (pg_amt + point_amt + MHC_amt + DGV_amt + OCB_amt))
    public String dgvAmt;

    // OCB 포인트 환불금액((refund_amt - addition_amt - deduct_amt) = (pg_amt + point_amt + MHC_amt + DGV_amt + OCB_amt))
    public String ocbAmt;

    // 기타 결제 환불금액(추가 결제수단 대비용)
    public String etcAmt;

    // 등록일
    public String regDt;

    // 등록아이디
    public String regId;

    // 수정일
    public String chgDt;

    // 수정아이디
    public String chgId;
}
