package kr.co.homeplus.escrowmng.external.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import kr.co.homeplus.escrowmng.core.valid.AllowInput;
import lombok.Data;

@Data
@ApiModel(description = "[옴니연동] 점포별 우편번호 관리")
public class StoreZipcodeManage {

    @ApiModelProperty(notes = "우편번호(5자리)", position = 1, required = true)
    @NotEmpty(message = "우편번호(5자리)")
    @Pattern(regexp = PatternConstants.ZIPCODE_PATTERN, message = "우편번호(5자리)")
    @AllowInput(types = {PatternConstants.NUM}, message = "우편번호(5자리)")
    private String zipcodeCd;

    @ApiModelProperty(notes = "점포유형", position = 2, required = true)
    @NotEmpty(message = "점포유형(HYPER,EXP,CLUB)")
    @Pattern(regexp = PatternConstants.STORE_TYPE_PATTERN, message = "점포유형(HYPER,EXP,CLUB)")
    private String storeType;

    @ApiModelProperty(notes = "점포ID", position = 3, required = true)
    @NotEmpty(message = "점포ID")
    @AllowInput(types = {PatternConstants.NUM}, message = "점포ID")
    private String storeId;

    @ApiModelProperty(notes = "우편번호 순번", position = 4, required = true)
    @NotEmpty(message = "우편번호 순번")
    @AllowInput(types = {PatternConstants.NUM}, message = "우편번호 순번")
    private String zipcodeSeq;

    @ApiModelProperty(notes = "배송불가여부", position = 5, required = true)
    @NotEmpty(message = "배송불가여부")
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "배송불가여부(Y|N)")
    private String shipUnableYn;

    @ApiModelProperty(notes = "사용여부", position = 6, required = true)
    @NotEmpty(message = "사용여부")
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "사용여부(Y|N)")
    private String useYn;

    @ApiModelProperty(notes = "원거리배송여부", position = 7, required = true)
    @NotEmpty(message = "원거리배송여부")
    @Pattern(regexp = PatternConstants.YN_PATTERN, message = "원거리배송여부(Y|N)")
    private String remoteShipYn;

    @JsonIgnore
    private long apiFulltextSeq;

    public String toString() {
        return "{zipcodeCd:" + zipcodeCd + ",storeType:" + storeType + ",storeId:" + storeId + ",zipcodeSeq:" + zipcodeSeq + ", shipUnableYn:" + shipUnableYn + ", useYn:" + useYn + ", remoteShipYn:" + remoteShipYn + "}";
    }
}