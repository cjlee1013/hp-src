package kr.co.homeplus.escrowmng.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ExtractType {
    SEARCH, EXCEL_DOWN;
}
