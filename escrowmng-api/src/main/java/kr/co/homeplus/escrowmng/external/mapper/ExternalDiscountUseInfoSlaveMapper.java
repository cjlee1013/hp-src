package kr.co.homeplus.escrowmng.external.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.external.model.DiscountUseInfo;
import kr.co.homeplus.escrowmng.external.model.DiscountUseInfoSelect;

@EscrowSlaveConnection
public interface ExternalDiscountUseInfoSlaveMapper {

    List<DiscountUseInfo> selectDiscountUseInfoList(DiscountUseInfoSelect discountUseInfoSelect);

}
