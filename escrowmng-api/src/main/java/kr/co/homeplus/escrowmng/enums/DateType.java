package kr.co.homeplus.escrowmng.enums;

public enum DateType {
    YEAR, MONTH, DAY, HOUR, MIN, SEC
}
