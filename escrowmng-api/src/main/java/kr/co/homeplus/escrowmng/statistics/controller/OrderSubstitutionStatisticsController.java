package kr.co.homeplus.escrowmng.statistics.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.statistics.model.CartCouponStatisticsModel;
import kr.co.homeplus.escrowmng.statistics.model.CartCouponStatisticsSelectModel;
import kr.co.homeplus.escrowmng.statistics.model.OrderSubstitutionStatisticsGetDto;
import kr.co.homeplus.escrowmng.statistics.model.OrderSubstitutionStatisticsModel;
import kr.co.homeplus.escrowmng.statistics.service.CartCouponStatisticsService;
import kr.co.homeplus.escrowmng.statistics.service.OrderSubstitutionStatisticsService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/statistics/substitution")
@Api(tags = "통계 > 프로모션통계 > 대체공제쿠폰통계")
public class OrderSubstitutionStatisticsController {

    private final OrderSubstitutionStatisticsService orderSubstitutionStatisticsService;

    @PostMapping("/getOrderSubstitutionStatistics")
    @ApiOperation(value = "대체공제 쿠폰 통계 조회", response = OrderSubstitutionStatisticsModel.class)
    public ResponseObject<List<OrderSubstitutionStatisticsGetDto>> getCartCouponStatistics(@RequestBody @Valid OrderSubstitutionStatisticsModel orderSubstitutionStatisticsModel) {
        log.info("orderSubstitutionStatisticsModel: {}", orderSubstitutionStatisticsModel);
        return ResourceConverter.toResponseObject(orderSubstitutionStatisticsService.getOrderSubstitutionStatistics(orderSubstitutionStatisticsModel));
    }
}
