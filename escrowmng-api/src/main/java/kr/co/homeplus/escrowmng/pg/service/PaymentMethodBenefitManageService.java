package kr.co.homeplus.escrowmng.pg.service;

import java.util.List;
import kr.co.homeplus.escrowmng.constants.Constants;
import kr.co.homeplus.escrowmng.core.exception.BusinessLogicException;
import kr.co.homeplus.escrowmng.enums.BusinessExceptionCode;
import kr.co.homeplus.escrowmng.pg.mapper.PaymentMethodBenefitManageMasterMapper;
import kr.co.homeplus.escrowmng.pg.mapper.PaymentMethodBenefitManageSlaveMapper;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodBenefitManageDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodBenefitManageSelectDto;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PaymentMethodBenefitManageService {

    private final PaymentMethodBenefitManageMasterMapper manageMasterMapper;
    private final PaymentMethodBenefitManageSlaveMapper manageSlaveMapper;

    /**
     * 결제수단별 혜택 조회
     *
     * @param paymentMethodBenefitManageSelectDto
     */
    public List<PaymentMethodBenefitManageDto> getPaymentMethodBenefitManage(PaymentMethodBenefitManageSelectDto paymentMethodBenefitManageSelectDto) {
        return manageSlaveMapper.selectPaymentMethodBenefitManage(paymentMethodBenefitManageSelectDto);
    }

    /**
     * 결제수단별 혜택 등록
     *
     * @param paymentMethodBenefitManageDto
     */
    public int savePaymentMethodBenefitManage(PaymentMethodBenefitManageDto paymentMethodBenefitManageDto) {
        // 동일 기간내 등록된 혜택이 있는지 확인
        if (EscrowString.equals(Constants.USE_Y, paymentMethodBenefitManageDto.getUseYn())
            && this.getPaymentMethodBenefitDuplicate(paymentMethodBenefitManageDto) > 0) {
            throw new BusinessLogicException(BusinessExceptionCode.ADMIN_VALIDATION_10000);
        }

        return manageMasterMapper.insertPaymentMethodBenefitManage(paymentMethodBenefitManageDto);
    }

    /**
     * 동일 기간내 등록된 혜택이 있는지 확인
     *
     * @param paymentMethodBenefitManageDto
     */
    private int getPaymentMethodBenefitDuplicate(PaymentMethodBenefitManageDto paymentMethodBenefitManageDto) {
        return manageSlaveMapper.getPaymentMethodBenefitDuplicate(paymentMethodBenefitManageDto);
    }
}
