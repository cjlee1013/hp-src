package kr.co.homeplus.escrowmng.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "원거리배송 점포 slot 정보 노출용")
public class RemoteShipSlotModel {

    @ApiModelProperty(value = "배송일", position = 1)
    private String shipDt;

    @ApiModelProperty(value = "shiftId", position = 2)
    private String shiftId;

    @ApiModelProperty(value = "slotId", position = 3)
    private String slotId;

    @ApiModelProperty(value = "slot순서", position = 4)
    private String slotSeq;

    @ApiModelProperty(value = "slot배송시작시간", position = 5)
    private String slotShipStartTime;

    @ApiModelProperty(value = "slot배송종료시간", position = 6)
    private String slotShipEndTime;
}
