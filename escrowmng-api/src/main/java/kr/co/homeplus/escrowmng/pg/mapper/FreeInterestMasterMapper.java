package kr.co.homeplus.escrowmng.pg.mapper;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.pg.model.FreeInterestApplyDto;
import kr.co.homeplus.escrowmng.pg.model.FreeInterestDto;

@EscrowMasterConnection
public interface FreeInterestMasterMapper {

    int insertFreeInterestMng(FreeInterestDto freeInterestDto);

    int updateFreeInterestMng(FreeInterestDto freeInterestDto);

    int insertFreeInterestApply(FreeInterestApplyDto freeInterestApplyDto);

    int updateFreeInterestApply(FreeInterestApplyDto freeInterestApplyDto);

    int deleteFreeInterestApply(FreeInterestApplyDto freeInterestApplyDto);
}
