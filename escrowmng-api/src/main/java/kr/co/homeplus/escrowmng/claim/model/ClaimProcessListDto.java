package kr.co.homeplus.escrowmng.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class ClaimProcessListDto {

    @ApiModelProperty(value = "구분", position = 1)
    public String processType;

    @ApiModelProperty(value = "처리일시", position = 2)
    public String processDt;

    @ApiModelProperty(value = "처리채널", position = 3)
    public String regChannel;

    @ApiModelProperty(value = "처리자", position = 4)
    public String regId;

    public ClaimProcessListDto(String processType, String processDt, String regChannel, String regId){
        this.processType = processType;
        this.processDt = processDt;
        this.regChannel = regChannel;
        this.regId = regId;
    }
}
