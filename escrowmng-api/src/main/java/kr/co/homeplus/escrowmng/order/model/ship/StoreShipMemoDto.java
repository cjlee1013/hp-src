package kr.co.homeplus.escrowmng.order.model.ship;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StoreShipMemoDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;
    @ApiModelProperty(value = "배송번", position = 2)
    private long bundleNo;
    @ApiModelProperty(value = "배송메모", position = 3)
    private String shipMemo;
    @ApiModelProperty(value = "배송주소관리번호", position = 4)
    private long shipAddrNo;
    @ApiModelProperty(value = "수정자ID", position = 5)
    private String chgId;
}
