package kr.co.homeplus.escrowmng.statistics.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.escrowmng.statistics.model.CombineOrderStatusDto;
import kr.co.homeplus.escrowmng.statistics.model.CombineOrderStatusSelectDto;
import kr.co.homeplus.escrowmng.statistics.service.CombineOrderStatusService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Api(tags = "통계 > 점포상품통계")
public class StoreItemStatisticsController {

    private final CombineOrderStatusService combineOrderStatusService;

    @PostMapping("/statistics/storeItem/getCombineOrderStatus")
    @ApiOperation("합배송 주문 현황")
    public ResponseObject<List<CombineOrderStatusDto>> getCombineOrderStatus(@RequestBody CombineOrderStatusSelectDto combineOrderStatusSelectDto) {
        return ResourceConverter.toResponseObject(combineOrderStatusService.getCombineOrderStatus(combineOrderStatusSelectDto));
    }
}
