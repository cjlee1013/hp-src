package kr.co.homeplus.escrowmng.statistics.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowBatchConnection;
import kr.co.homeplus.escrowmng.statistics.model.ZipcodeOrderStatistics;
import kr.co.homeplus.escrowmng.statistics.model.ZipcodeOrderStatisticsSelect;

@EscrowBatchConnection
public interface ZipcodeOrderStatisticsSlaveMapper {
    List<ZipcodeOrderStatistics> selectZipcodeOrderStatisticsList(ZipcodeOrderStatisticsSelect zipcodeOrderStatisticsSelect);
}
