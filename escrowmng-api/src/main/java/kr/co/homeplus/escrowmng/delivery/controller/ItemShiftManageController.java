package kr.co.homeplus.escrowmng.delivery.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.delivery.model.ItemShiftManageInsertModel;
import kr.co.homeplus.escrowmng.delivery.model.ItemShiftManageModel;
import kr.co.homeplus.escrowmng.delivery.model.ItemShiftManageSelectModel;
import kr.co.homeplus.escrowmng.delivery.service.ItemShiftManageService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/escrow/storeDelivery")
@Api(tags = "배송관리 > 점포배송정보 > 상품별 Shift 관리")
public class ItemShiftManageController {

    private final ItemShiftManageService itemShiftManageService;

    @GetMapping("/getItemShiftManageList")
    @ApiOperation(value = "상품별 Shift 리스트 조회", response = ItemShiftManageModel.class)
    public ResponseObject<List<ItemShiftManageModel>> getItemShiftManageList(@Valid ItemShiftManageSelectModel itemShiftManageSelectModel) {
        return ResourceConverter.toResponseObject(itemShiftManageService.getItemShiftManageList(itemShiftManageSelectModel));
    }

    @PostMapping("/saveItemShiftList")
    @ApiOperation(value = "단일상품 Shift 등록 (다중점포)")
    public ResponseObject<Integer> saveItemShiftList(@Valid @RequestBody List<ItemShiftManageInsertModel> itemShiftManageInsertModelList) {
        return ResourceConverter.toResponseObject(itemShiftManageService.saveItemShiftList(itemShiftManageInsertModelList));
    }

    @PostMapping("/saveItemShift")
    @ApiOperation(value = "단일상품 Shift 등록 (단일점포)")
    public ResponseObject<Integer> saveItemShift(@Valid @RequestBody ItemShiftManageInsertModel itemShiftManageInsertModel) {
        return ResourceConverter.toResponseObject(itemShiftManageService.saveItemShift(itemShiftManageInsertModel));
    }

    @PostMapping("/deleteItemShiftList")
    @ApiOperation(value = "상품별 Shift 관리 리스트 삭제")
    public void deleteItemShiftList(@Valid @RequestBody List<ItemShiftManageInsertModel> itemShiftManageInsertModelList) {
        itemShiftManageService.deleteItemShiftList(itemShiftManageInsertModelList);
    }
}
