package kr.co.homeplus.escrowmng.statistics.service;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.escrowmng.statistics.mapper.OrderSubstitutionStatisticsSlaveMapper;
import kr.co.homeplus.escrowmng.statistics.model.OrderSubstitutionStatisticsGetDto;
import kr.co.homeplus.escrowmng.statistics.model.OrderSubstitutionStatisticsModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderSubstitutionStatisticsService {
    private final OrderSubstitutionStatisticsSlaveMapper mapper;

    public List<OrderSubstitutionStatisticsGetDto> getOrderSubstitutionStatistics (OrderSubstitutionStatisticsModel orderSubstitutionStatisticsModel) {
        List<OrderSubstitutionStatisticsGetDto> returnList = mapper.getOrderSubstitutionStatistics(orderSubstitutionStatisticsModel);
        log.info("{}", returnList);
        return returnList;
    }
}
