package kr.co.homeplus.escrowmng.core.db.construct;

import kr.co.homeplus.escrowmng.core.db.annotation.EscrowBatchConnection;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowMasterConnection;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowSlaveConnection;
import kr.co.homeplus.escrowmng.core.db.properties.DataSourceProperties;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

class MyBatisConfig {
    public static final String BASE_PACKAGE = "kr.co.homeplus";
}

@Configuration
@EnableTransactionManagement
@MapperScan(basePackages = MyBatisConfig.BASE_PACKAGE, annotationClass = EscrowMasterConnection.class, sqlSessionFactoryRef = "escrowMasterSqlSessionFactory")
class EscrowMasterMyBatisConfig {
    @Bean(name = "escrowMasterSqlSessionFactory")
    public SqlSessionFactory escrowMasterSqlSessionFactory(@Qualifier("escrowMasterDataSource") BasicDataSource escrowMasterDataSource) throws Exception {
        return SqlSessionFactoryBuilder.build(escrowMasterDataSource);
    }

    @Primary
    @Bean(name = "escrowMasterDataSource", destroyMethod = "")
    public BasicDataSource dataSource(@Qualifier("escrowMasterDatabaseProperties") DataSourceProperties dataSourceProperties) {
        //dataSourceProperties.setDefaultReadOnly(false);
        return DataSourceBuilder.build(dataSourceProperties);
    }

    @Bean(name = TransactionManagerName.ESCROW)
    public PlatformTransactionManager transactionManager(@Qualifier("escrowMasterDataSource") BasicDataSource escrowMasterDataSource) {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(escrowMasterDataSource);
        transactionManager.setGlobalRollbackOnParticipationFailure(false);
        return transactionManager;
    }

    @Bean
    @ConfigurationProperties("spring.datasource.escrow-master")
    public DataSourceProperties escrowMasterDatabaseProperties() {
        return new DataSourceProperties();
    }
}

@Configuration
@MapperScan(basePackages = MyBatisConfig.BASE_PACKAGE, annotationClass = EscrowSlaveConnection.class, sqlSessionFactoryRef = "escrowSlaveSqlSessionFactory")
class EscrowSlaveMyBatisConfig {
    @Bean(name = "escrowSlaveSqlSessionFactory")
    public SqlSessionFactory escrowSlaveSqlSessionFactory(@Qualifier("escrowSlaveDataSource") BasicDataSource escrowSlaveDataSource) throws Exception {
        return SqlSessionFactoryBuilder.build(escrowSlaveDataSource);
    }

    @Bean(name = "escrowSlaveDataSource", destroyMethod = "")
    public BasicDataSource dataSource(@Qualifier("escrowSlaveDatabaseProperties") DataSourceProperties dataSourceProperties) {
        return DataSourceBuilder.build(dataSourceProperties);
    }

    @Bean
    @ConfigurationProperties("spring.datasource.escrow-slave")
    public DataSourceProperties escrowSlaveDatabaseProperties() {
        return new DataSourceProperties();
    }
}

@Configuration
@EnableTransactionManagement
@MapperScan(basePackages = MyBatisConfig.BASE_PACKAGE, annotationClass = EscrowBatchConnection.class, sqlSessionFactoryRef = "escrowBatchSqlSessionFactory")
class EscrowBatchMyBatisConfig {
    @Bean(name = "escrowBatchSqlSessionFactory")
    public SqlSessionFactory escrowBatchSqlSessionFactory(@Qualifier("escrowBatchDataSource") BasicDataSource escrowBatchDataSource) throws Exception {
        return SqlSessionFactoryBuilder.build(escrowBatchDataSource);
    }

    @Bean(name = "escrowBatchDataSource", destroyMethod = "")
    public BasicDataSource dataSource(@Qualifier("escrowBatchDatabaseProperties") DataSourceProperties dataSourceProperties) {
        //dataSourceProperties.setDefaultReadOnly(false);
        return DataSourceBuilder.build(dataSourceProperties);
    }

    @Bean(name = "escrowBatchDatabaseProperties")
    @ConfigurationProperties("spring.datasource.escrow-batch")
    public DataSourceProperties escrowBatchDatabaseProperties() {
        return new DataSourceProperties();
    }
}