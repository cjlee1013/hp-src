package kr.co.homeplus.escrowmng.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 점포배송정보 > 점포별 휴일 조회 DTO")
public class StoreCloseDayModel {
    @ApiModelProperty(notes = "등록번호", position = 1)
    private String closeDaySeq;
    @ApiModelProperty(notes = "점포유형(HYPER,EXP,CLUB)", position = 2)
    private String storeType;
    @ApiModelProperty(notes = "휴무유형(F:자율휴무,R:정기휴무,S:설,C:추석,E:기타,T:임시)", position = 3)
    private String closeDayType;
    @ApiModelProperty(notes = "등록제목", position = 4)
    private String titleNm;
    @ApiModelProperty(notes = "진행기간", position = 5)
    private String closeDt;
    @ApiModelProperty(notes = "허용점포개수", position = 6)
    private String applyStoreCnt;
    @ApiModelProperty(notes = "허용점포명", position = 7)
    private String applyStoreNm;
    @ApiModelProperty(notes = "진행상태(Y:정상,N:취소)", position = 8)
    private String useYn;
    @ApiModelProperty(notes = "등록일", position = 9)
    private String regDt;
    @ApiModelProperty(notes = "등록자", position = 10)
    private String regId;
}
