package kr.co.homeplus.escrowmng.core.valid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import kr.co.homeplus.escrowmng.utils.EscrowDate;

public class DateValidator implements ConstraintValidator<Date, String>{

    private String format;

    @Override
    public void initialize(Date constraintAnnotation) {
        this.format = constraintAnnotation.format();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return EscrowDate.isValidParseDateStrictly(s, format);
    }
}
