package kr.co.homeplus.escrowmng.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.escrowmng.constants.PatternConstants;
import lombok.Data;

@Data
@ApiModel(description = "통계 > 프로모션통계 > 대체공제쿠폰통계 SELECT DTO")
public class OrderSubstitutionStatisticsModel {
    @ApiModelProperty(value = "조회시작일", position = 1, required = true)
    @NotEmpty(message = "조회시작일")
    private String schStartDt;
    @ApiModelProperty(value = "조회종료일", position = 2, required = true)
    @NotEmpty(message = "조회종료일")
    private String schEndDt;
    @ApiModelProperty(value = "점포유형", position = 3)
    @Pattern(regexp = PatternConstants.STORE_TYPE_PATTERN, message="점포유형(HYPER,EXP,CLUB)")
    private String schStoreType;
    @ApiModelProperty(value = "상점아이디", position = 4)
    private String schStoreId;
}
