package kr.co.homeplus.escrowmng.claim.model.partner;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimPartnerBoardCountGetDto {

    @ApiModelProperty(value="클레임신청수건")
    private long claimRequestCnt;

    @ApiModelProperty(value="클레임지연건")
    private long claimDelayCnt;

    @ApiModelProperty(value="클레임완료건")
    private long claimCompleteCnt;

    @ApiModelProperty(value="클레임수거완료건")
    private long claimPickupCnt;

    @ApiModelProperty(value="클레임보류건")
    private long claimPendingCnt;
}
