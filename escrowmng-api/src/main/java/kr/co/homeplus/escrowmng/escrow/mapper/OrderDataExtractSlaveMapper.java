package kr.co.homeplus.escrowmng.escrow.mapper;

import java.util.List;
import kr.co.homeplus.escrowmng.core.db.annotation.EscrowBatchConnection;
import kr.co.homeplus.escrowmng.escrow.model.extract.ExtractAddCouponUseOrderDto;
import kr.co.homeplus.escrowmng.escrow.model.extract.ExtractCartCouponUseOrderDto;
import kr.co.homeplus.escrowmng.escrow.model.extract.ExtractPaymentInfoDto;
import kr.co.homeplus.escrowmng.escrow.model.extract.ExtractPickupOrderDto;
import kr.co.homeplus.escrowmng.escrow.model.extract.OrderDataExtractSelectDto;

@EscrowBatchConnection
public interface OrderDataExtractSlaveMapper {
    // 장바구니쿠폰(농활쿠폰) 사용주문 리스트 조회
    List<ExtractCartCouponUseOrderDto> selectCartCouponUseOrderList(OrderDataExtractSelectDto orderDataExtractSelectDto);

    // 픽업주문 리스트 조회
    List<ExtractPickupOrderDto> selectPickupOrderList(OrderDataExtractSelectDto orderDataExtractSelectDto);

    // 결제수단별 결제정보 리스트 조회
    List<ExtractPaymentInfoDto> selectPaymentInfoListByMethod(OrderDataExtractSelectDto orderDataExtractSelectDto);

    // 보조결제수단 결제정보 리스트 조회
    List<ExtractPaymentInfoDto> selectAssistancePaymentInfoList(OrderDataExtractSelectDto orderDataExtractSelectDto);

    // 중복쿠폰 사용주문 리스트 조회
    List<ExtractAddCouponUseOrderDto> selectAddCouponUseOrderList(OrderDataExtractSelectDto orderDataExtractSelectDto);
}
