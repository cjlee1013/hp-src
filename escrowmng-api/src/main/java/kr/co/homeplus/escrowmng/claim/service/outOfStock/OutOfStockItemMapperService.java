package kr.co.homeplus.escrowmng.claim.service.outOfStock;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.OutOfStockItemListGetDto;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.OutOfStockItemListSetDto;
import kr.co.homeplus.escrowmng.claim.model.outOfStock.StoreShiftListSetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListSetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteSetDto;


public interface OutOfStockItemMapperService {

    List<OutOfStockItemListGetDto> getOutOfStockItemList(OutOfStockItemListSetDto outOfStockItemListSetDto);

    List<String> getStoreShiftList(StoreShiftListSetDto storeShiftListSetDto);

}
