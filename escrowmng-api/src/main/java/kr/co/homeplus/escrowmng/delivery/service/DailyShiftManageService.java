package kr.co.homeplus.escrowmng.delivery.service;

import java.util.List;
import kr.co.homeplus.escrowmng.delivery.mapper.DailyShiftManageMasterMapper;
import kr.co.homeplus.escrowmng.delivery.mapper.DailyShiftManageSlaveMapper;
import kr.co.homeplus.escrowmng.delivery.model.DailyShiftDuplicateCheckModel;
import kr.co.homeplus.escrowmng.delivery.model.DailyShiftManageModel;
import kr.co.homeplus.escrowmng.delivery.model.DailyShiftManageSelectModel;
import kr.co.homeplus.escrowmng.delivery.model.DailyShiftStoreManageModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DailyShiftManageService {
    private final DailyShiftManageSlaveMapper dailyShiftManageSlaveMapper;
    private final DailyShiftManageMasterMapper dailyShiftManageMasterMapper;

    /**
     * 일자별 Shift 정보 조회
     * @param dailyShiftManageSelectModel
     * @return
     */
    public List<DailyShiftManageModel> getDailyShiftManageList(DailyShiftManageSelectModel dailyShiftManageSelectModel) {
        return dailyShiftManageSlaveMapper.selectDailyShiftManageList(dailyShiftManageSelectModel);
    }

    /**
     * 일자별 Shift 점포 정보 조회
     * @param dailyShiftMngSeq
     * @return
     */
    public List<DailyShiftStoreManageModel> getDailyShiftStoreManageList(long dailyShiftMngSeq) {
        return dailyShiftManageSlaveMapper.selectDailyShiftStoreManageList(dailyShiftMngSeq);
    }

    /**
     * 일자별 Shift 관리 중복체크
     * @param dailyShiftDuplicateCheckModel
     * @return
     */
    public int checkDuplicateDailyShiftManage(DailyShiftDuplicateCheckModel dailyShiftDuplicateCheckModel) {
        return dailyShiftManageSlaveMapper.checkDuplicateDailyShiftManage(dailyShiftDuplicateCheckModel);
    }

    /**
     * 일자별 Shift 정보 저장
     * @param dailyShiftManageModel
     * @return
     */
    public DailyShiftManageModel saveDailyShiftManage(DailyShiftManageModel dailyShiftManageModel) {
        dailyShiftManageMasterMapper.insertDailyShiftManage(dailyShiftManageModel);
        return dailyShiftManageModel;
    }

    /**
     * 일자별 Shift 점포 정보 저장
     * @param dailyShiftStoreManageModelList
     * @return
     */
    public int saveDailyShiftStoreManageList(List<DailyShiftStoreManageModel> dailyShiftStoreManageModelList) {
        return dailyShiftManageMasterMapper.insertDailyShiftStoreManageList(dailyShiftStoreManageModelList);
    }

    /**
     * 일자별 Shift 정보 삭제
     * -. 일자별 Shift 정보의 사용여부 N 처리
     * @param dailyShiftManageModelList
     * @return
     */
    public int deleteDailyShiftManage(List<DailyShiftManageModel> dailyShiftManageModelList) {
        return dailyShiftManageMasterMapper.updateDailyShiftManageUseYn(dailyShiftManageModelList);
    }
}
