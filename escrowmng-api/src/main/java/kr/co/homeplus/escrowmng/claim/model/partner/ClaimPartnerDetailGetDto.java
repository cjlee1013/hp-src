package kr.co.homeplus.escrowmng.claim.model.partner;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.ClaimExchShippingDto;
import kr.co.homeplus.escrowmng.claim.model.ClaimPickShippingDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimPartnerDetailGetDto {

    @ApiModelProperty(value = "클레임타입코드")
    private String claimTypeCode;

    @ApiModelProperty(value = "클레임타입")
    private String claimType;

    @ApiModelProperty(value = "처리상태")
    private String claimStatus;

    @ApiModelProperty(value = "처리상태")
    private String claimStatusCode;

    @ApiModelProperty(value = "배송번호")
    private String bundleNo;

    @ApiModelProperty(value = "클레임번호")
    private long claimBundleNo;

    @ApiModelProperty(value = "클레임 사유")
    private String claimReasonType;

    @ApiModelProperty(value = "클레임 상세사유")
    private String claimReasonDetail;

    @ApiModelProperty(value = "첨부파일1")
    private String uploadFileName;

    @ApiModelProperty(value = "첨부파일2")
    private String uploadFileName2;

    @ApiModelProperty(value = "첨부파일3")
    private String uploadFileName3;

    @ApiModelProperty(value = "배송비 동봉 여부 Y:택배동봉, N:환불금액 차감")
    private String claimShipFeeEnclose;

    @ApiModelProperty(value = "클레임 귀책자")
    private String whoReason;

    @ApiModelProperty(value = "추가 배송비")
    private long totAddShipPrice;

    @ApiModelProperty(value = "추가 도서산간 배송비")
    private long totAddIslandShipPrice;

    @ApiModelProperty(value="반품 수거지")
    private ClaimPickShippingDto claimPickShippingDto;

    @ApiModelProperty(value="교환 수거지")
    private ClaimExchShippingDto claimExchShippingDto;

    @ApiModelProperty(value = "클레임 상품 리스트")
    private List<ClaimPartnerDetailItem> claimDetailItemList;

}
