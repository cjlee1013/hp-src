package kr.co.homeplus.escrowmng.claim.service.refundFail;


import java.util.List;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RefundFailListSetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteGetDto;
import kr.co.homeplus.escrowmng.claim.model.refundFail.RequestRefundCompleteSetDto;


public interface RefundFailMapperService {

    List<RefundFailListGetDto> getRefundFailList(RefundFailListSetDto refundFailListSetDto);

    RequestRefundCompleteGetDto getRefundCompleteResultCount(RequestRefundCompleteSetDto requestRefundCompleteSetDto);

    Boolean setReqRefundComplete(RequestRefundCompleteSetDto requestRefundCompleteSetDto);


}
