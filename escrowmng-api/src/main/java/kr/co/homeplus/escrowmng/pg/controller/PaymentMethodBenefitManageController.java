package kr.co.homeplus.escrowmng.pg.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodBenefitManageDto;
import kr.co.homeplus.escrowmng.pg.model.PaymentMethodBenefitManageSelectDto;
import kr.co.homeplus.escrowmng.pg.service.PaymentMethodBenefitManageService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pg/paymentMethod")
@Api(tags = "결제관리 > 결제수단관리 > 결제수단별 혜택관리")
public class PaymentMethodBenefitManageController {

    private final PaymentMethodBenefitManageService paymentMethodBenefitManageService;

    @PostMapping("/getPaymentMethodBenefitManage")
    @ApiOperation(value = "결제수단별 혜택 조회", response = PaymentMethodBenefitManageDto.class)
    public ResponseObject getPaymentMethodBenefitManage(@Valid @RequestBody PaymentMethodBenefitManageSelectDto paymentMethodBenefitManageSelectDto) {
        return ResourceConverter.toResponseObject(paymentMethodBenefitManageService.getPaymentMethodBenefitManage(paymentMethodBenefitManageSelectDto));
    }

    @PostMapping("/savePaymentMethodBenefitManage")
    @ApiOperation(value = "결제수단별 혜택 등록")
    public ResponseObject savePaymentMethodBenefitManage(@Valid @RequestBody PaymentMethodBenefitManageDto paymentMethodBenefitManageDto) {
        return ResourceConverter.toResponseObject(paymentMethodBenefitManageService.savePaymentMethodBenefitManage(paymentMethodBenefitManageDto));
    }
}