package kr.co.homeplus.escrowmng.claim.model.entity;

public class ClaimAccumulateEntity {
    // 구매주문번호
    public String claimNo;
    // 구매주문번호
    public String purchaseOrderNo;
    // 고객번호
    public String userNo;
    // 포인트구분(MHC/MILEAGE)
    public String pointKind;
    // 사이트유형(HMP:홈플러스,CLUB:더클럽)
    public String siteType;
    // 주문일자(YYYYMMDD)
    public String orderDt;
    // 취소일자(YYYYMMDD)
    public String claimDt;
    // (매출용 실제점포ID:0024,0003 등)
    public String originStoreId;
    // 기본적립율
    public String baseAccRate;
    // 추가적립율
    public String addAccRate;
    // 기본적립금액
    public String baseAccAmt;
    // 추가적립금액
    public String addAccAmt;
    // 적립대상금액
    public String accTargetAmt;
    // 클레임기본적립금액
    public String claimBaseAccAmt;
    // 클레임추가적립금액
    public String claimAddAccAmt;
    // 적립취소대상금액
    public String claimAccTargetAmt;
    // 누적취소금액
    public String accCancelAmt;
    // 총주문금액
    public String totOrderAmt;
    // 총클레임금액
    public String totClaimAmt;
    // MHC카드번호암호화(MHC 대체카드번호 RKM 암호화)
    public String mhcCardNoEnc;
    // MHC카드번호HMAC(MHC 대체카드번호 RKM HMAC)
    public String mhcCardNoHmac;
    // 제휴카드구분
    public String affiliateCardKind;
    // 요청전송여부(N:미전송,I:요청중,Y:전송성공,E:전송실패)
    public String reqSendYn;
    // 원결과코드
    public String orgResultCd;
    // 원승인일자
    public String orgApprovalDt;
    // 원승인번호
    public String orgApprovalNo;
    // 적립취소결과코드
    public String resultCd;
    // 적립취소승인일자
    public String approvalDt;
    // 적립취소승인번호
    public String approvalNo;
    // 적립취소 결과포인트
    public String resultPoint;
    // 등록일시
    public String regDt;
    // 수정일시
    public String chgDt;
}