package kr.co.homeplus.escrowmng.escrow.model.message;

import io.swagger.annotations.ApiModelProperty;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasicRecipient {
    @ApiModelProperty(value = "회원번호 또는 캠페인 별 유저 고유 식별 키", required = true, position = 1)
    private String identifier;

    @ApiModelProperty(value = "수신주소정보(sms일 경우 전화번호, 메일일 경우 이메일주소 등)", required = true, position = 2)
    private String toToken;

    @ApiModelProperty(value = "개인 매핑 데이터", position = 3)
    private Map<String, String> mappingData;
}
