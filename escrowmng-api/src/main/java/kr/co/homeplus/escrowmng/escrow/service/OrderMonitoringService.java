package kr.co.homeplus.escrowmng.escrow.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.escrowmng.enums.ApiName;
import kr.co.homeplus.escrowmng.enums.MarketType;
import kr.co.homeplus.escrowmng.enums.MessageTemplate;
import kr.co.homeplus.escrowmng.escrow.mapper.OrderMonitoringSlaveMapper;
import kr.co.homeplus.escrowmng.escrow.model.MngCodeGetModel;
import kr.co.homeplus.escrowmng.escrow.model.message.BasicRecipient;
import kr.co.homeplus.escrowmng.utils.EscrowDate;
import kr.co.homeplus.escrowmng.utils.EscrowString;
import kr.co.homeplus.escrowmng.utils.SetParameter;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import static kr.co.homeplus.escrowmng.constants.Constants.PROFILES_PRD;
import static kr.co.homeplus.escrowmng.constants.Constants.RESULT_MSG_SUCCESS;
import static kr.co.homeplus.escrowmng.constants.Constants.ZERO;
import static kr.co.homeplus.escrowmng.constants.ResourceRouteName.ITEM;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderMonitoringService {

    @Value("${spring.profiles.active}")
    private String PROFILES_ACTIVE;

    private final OrderMonitoringSlaveMapper orderMonitoringSlaveMapper;
    private final SlackService slackService;
    private final ResourceClient resourceClient;
    private final MessageService messageService;

    /**
     * 주문 건수 체크
     * @return
     */
    public String getOrderCountCheck() {

        checkOrderCount();

        if (!EscrowString.equals(PROFILES_ACTIVE, PROFILES_PRD) || EscrowDate.isBeforeNow("2021-12-19 01:00:00")) { // 운영환경에서는 오픈 이후 체크
            for (MarketType marketType : MarketType.getUsableMarketTypeList()) {
                checkMarketOrderCollectCount(marketType);
                checkMarketOrderCreateCount(marketType);
            }
        }

        return  RESULT_MSG_SUCCESS;
    }

    /**
     * 주문건수가 0이하면 슬랙 발송
     */
    private void checkOrderCount() {
        // 주문건수
        long orderCount = orderMonitoringSlaveMapper.selectOrderCount();
        // 주문건수가 없으면 slack notify
        if (orderCount <= ZERO) {
            slackService.noOrderCountSendCheck();
            this.sendCheckOrderSMS("HOMEPLUS");
        }
    }

    /**
     * 마켓연동주문 수집건수가 0이하면 슬랙 발송
     * @param marketType
     */
    private void checkMarketOrderCollectCount(MarketType marketType) {
        long marketOrderCollectCount = orderMonitoringSlaveMapper.countMarketOrderCollect(marketType.getType());
        if (marketOrderCollectCount <= ZERO) {
            slackService.noMarketOrderCollectCountSendCheck(marketType);
            this.sendCheckOrderSMS("MARKET");
        }
    }

    /**
     * 마켓연동주문 생성건수가 0이하면 슬랙 발송
     * @param marketType
     */
    private void checkMarketOrderCreateCount(MarketType marketType) {
        long marketOrderCreatePurchaseOrderCount = orderMonitoringSlaveMapper.countMarketOrderCreatePurchaseOrder(marketType.getType());
        if (marketOrderCreatePurchaseOrderCount <= ZERO) {
            slackService.noMarketOrderCreatePurchasetCountSendCheck(marketType);
            this.sendCheckOrderSMS("MARKET");
        }
    }

    private void sendCheckOrderSMS(String orderType) {
        List<MngCodeGetModel> orderAlarmSendList = this.getOrderAlarmSendList();
        try {
            this.sendSmsOrderCancel(orderType, orderAlarmSendList);
        } catch (Exception e) {
            log.error("주문건수 없음 sms 발송 실패 orderType:" + orderType);
        }
    }

    /**
     * 주문 건수 없을 때 sms 발송 리스트 대상 조회
     * @return
     */
    private List<MngCodeGetModel> getOrderAlarmSendList() {

        List<SetParameter> setParameterList = Arrays.asList(
            SetParameter.create("typeCode", "order_alarm_sms")
        );
        String apiUri = ApiName.GET_COMMON_CODE.getApiUri() + EscrowString.getParameter(setParameterList);

        ResourceClientRequest<List<MngCodeGetModel>> request = ResourceClientRequest.<List<MngCodeGetModel>>getBuilder()
            .apiId(ITEM)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        List<MngCodeGetModel> data = resourceClient.get(request, new TimeoutConfig()).getBody().getData();

        return data;
    }

    /**
     * 주문건수 없음 문자 발송
     * @param orderType
     * @param smsTargetList
     * @throws Exception
     */
    private void sendSmsOrderCancel(String orderType, List<MngCodeGetModel> smsTargetList) throws Exception{

        MessageTemplate messageTemplate;

        switch (orderType) {
            case "MARKET":
                messageTemplate = MessageTemplate.CHECK_MARKET_ORDER;
                break;
            default:
                messageTemplate = MessageTemplate.CHECK_HOMEPLUS_ORDER;
                break;
        }

        log.info("[주문건수없음문자발송] orderType:" + orderType + ", sendList:" + smsTargetList.size());

        List<BasicRecipient> basicRecipientList = new ArrayList<>();

        for (MngCodeGetModel smsTarget :smsTargetList) {
            BasicRecipient basicRecipient = new BasicRecipient();
            basicRecipient.setIdentifier("");
            basicRecipient.setToToken(smsTarget.getMcNm());
            Map<String, String> replaceData = new HashMap<>();
            replaceData.put("실패 날짜, 시간", EscrowDate.getCurrentYmdHis());
            basicRecipient.setMappingData(replaceData);

            basicRecipientList.add(basicRecipient);
        }

        messageService.sendSms(messageTemplate, "SYSTEM", basicRecipientList);
    }
}

