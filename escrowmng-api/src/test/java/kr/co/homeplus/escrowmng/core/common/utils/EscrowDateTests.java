package kr.co.homeplus.escrowmng.core.common.utils;

import java.time.LocalDateTime;
import kr.co.homeplus.escrowmng.utils.EscrowDate;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EscrowDateTests {
    private static final Logger log = LoggerFactory.getLogger(EscrowDateTests.class);

    @Test
    public void DateTimeUtils_포멧별_테스트() {
        // getCurrentYmd
        log.info("\n\n // {}", "getCurrentYmd");
        log.info("getCurrentYmd : {}", EscrowDate.getCurrentYmd());
        log.info("getCurrentYmd : {}", EscrowDate.getCurrentYmd("yyyy/MM/dd"));
        log.info("getCurrentYmd : {}", EscrowDate.getCurrentYmd("yyyy.MM.dd"));

        // getToday
        log.info("\n\n // {}", "getToday");
        log.info("getToday : {}", EscrowDate.getToday());

        // getCurrentYmdHis
        log.info("\n\n // {}", "getCurrentYmdHis");
        log.info("getCurrentYmdHis : {}", EscrowDate.getCurrentYmdHis());
        log.info("getCurrentYmdHis : {}", EscrowDate.getCurrentYmdHis("yyyy.MM.dd H:m:s"));

        // parseDateTimeToStr
        log.info("\n\n // {}", "parseDateTimeToStr");
        log.info("parseDateTimeToStr : {}", EscrowDate.parseDateTimeToStr(LocalDateTime.now()));
        log.info("parseDateTimeToStr : {}", EscrowDate.parseDateTimeToStr(LocalDateTime.now(), "yyyy.MM.dd H:m:s"));

        // changeDateStrFormat
        log.info("\n\n // {}", "changeDateStrFormat");
        log.info("changeDateStrFormat : {} -> {}", "2020-02-29",
            EscrowDate.changeDateStrFormat("2020-02-29", EscrowDate.DEFAULT_YMD, "yyyy/MM/dd"));

        // changeDateTimeStrFormat
        log.info("\n\n // {}", "changeDateTimeStrFormat");
        log.info("changeDateTimeStrFormat : {} -> {}", "2020-02-29 01:23:45",
            EscrowDate.changeDateTimeStrFormat("2020-02-29 01:23:45", EscrowDate.DEFAULT_YMD_HIS, "yyyy.MM.dd HH/mm/ss")
        );

        // changeDateStrToTimeStr(set hour, minutes, seconds)
        log.info("\n\n // {}", "changeDateStrToTimeStr(set hour, minutes, seconds)");
        log.info("changeDateStrToTimeStr(set 23:59:59) : {} -> {}", "2020-02-29",
            EscrowDate.changeDateStrToTimeStr("2020-02-29", EscrowDate.DEFAULT_YMD, EscrowDate.DEFAULT_YMD_HIS, 23, 59, 59)
        );

        // isBeforeNow
        log.info("\n\n // {}", "isBeforeNow");
        log.info("isBeforeNow '{}' ? {}", "2019-02-28 00:00:01",
            EscrowDate.isBeforeNow("2019-02-28 00:00:01")
        );
        log.info("isBeforeNow '{}' ? {}", "2020-02-28 00:00:01",
            EscrowDate.isBeforeNow("2020-02-28 00:00:01")
        );
        log.info("isBeforeNow '{}' ? {}", "2019/02/28 00:00:01",
            EscrowDate.isBeforeNow("2019/02/28 00:00:01", "yyyy/MM/dd HH:mm:ss")
        );
        log.info("isBeforeNow '{}' ? {}", "2020/02/28 00:00:01",
            EscrowDate.isBeforeNow("2020/02/28 00:00:01", "yyyy/MM/dd HH:mm:ss")
        );

        // isAfterNow
        log.info("\n\n // {}", "isAfterNow");
        log.info("isAfterNow '{}' ? {}", "2019-02-28 00:00:01",
            EscrowDate.isAfterNow("2019-02-28 00:00:01")
        );
        log.info("isAfterNow '{}' ? {}", "2020-02-28 00:00:01",
            EscrowDate.isAfterNow("2020-02-28 00:00:01")
        );
        log.info("isAfterNow '{}' ? {}", "2019/02/28 00:00:01",
            EscrowDate.isAfterNow("2019/02/28 00:00:01", "yyyy/MM/dd HH:mm:ss")
        );
        log.info("isAfterNow '{}' ? {}", "2020/02/28 00:00:01",
            EscrowDate.isAfterNow("2020/02/28 00:00:01", "yyyy/MM/dd HH:mm:ss")
        );

        // compareDateTimeStr
        log.info("\n\n // {}", "compareDateTimeStr");
        log.info("compareDateTimeStr '{} compareTo {}' ? {}", "2020-02-29 00:00:00", "2020-02-29 23:59:59",
            EscrowDate.compareDateTimeStr("2020-02-29 00:00:00", "2020-02-29 23:59:59")
        );
        log.info("compareDateTimeStr '{} compareTo {}' ? {}", "2020-02-29 00:00:00", "2020-02-29 00:00:00",
            EscrowDate.compareDateTimeStr("2020-02-29 00:00:00", "2020-02-29 00:00:00")
        );
        log.info("compareDateTimeStr '{} compareTo {}' ? {}", "2020-02-29 23:59:59", "2020-02-29 00:00:00",
            EscrowDate.compareDateTimeStr("2020-02-29 23:59:59", "2020-02-29 00:00:00")
        );
        log.info("compareDateTimeStr '{} compareTo {}' ? {}", "2020/02/29 00:00:00", "2020/02/29 23:59:59",
            EscrowDate.compareDateTimeStr("2020/02/29 00:00:00", "2020/02/29 23:59:59", "yyyy/MM/dd HH:mm:ss")
        );
        log.info("compareDateTimeStr '{} compareTo {}' ? {}", "2020/02/29 00:00:00", "2020/02/29 00:00:00",
            EscrowDate.compareDateTimeStr("2020/02/29 00:00:00", "2020/02/29 00:00:00", "yyyy/MM/dd HH:mm:ss")
        );
        log.info("compareDateTimeStr '{} compareTo {}' ? {}", "2020/02/29 23:59:59", "2020/02/29 00:00:00",
            EscrowDate.compareDateTimeStr("2020/02/29 23:59:59", "2020/02/29 00:00:00", "yyyy/MM/dd HH:mm:ss")
        );

        // plusMinutes
        log.info("\n\n // {}", "plusMinutes");
        log.info("plusMinutes now : {} >>> plus : {}", EscrowDate.getCurrentYmdHis(),
            EscrowDate.plusMinutes(EscrowDate.getCurrentYmdHis(), 60)
        );
        log.info("plusMinutes now : {} >>> plus : {}", "2020/02/29 00:00:59",
            EscrowDate.plusMinutes("2020/02/29 00:00:59", 60, "yyyy/MM/dd HH:mm:ss")
        );

        // plusDays
        log.info("\n\n // {}", "plusDays");
        log.info("plusDays today : {} >>> plus : {}", EscrowDate.getCurrentYmdHis(),
            EscrowDate.plusDays(EscrowDate.getCurrentYmdHis(), 2)
        );
        log.info("plusDays today : {} >>> plus : {}", "2020/02/29 00:00:59",
            EscrowDate.plusDays("2020/02/29 00:00:59", 2, "yyyy/MM/dd HH:mm:ss")
        );

        // minusMinutes
        log.info("\n\n // {}", "minusMinutes");
        log.info("minusMinutes now : {} >>> minus : {}", EscrowDate.getCurrentYmdHis(),
            EscrowDate.minusMinutes(EscrowDate.getCurrentYmdHis(), 60)
        );
        log.info("minusMinutes now : {} >>> minus : {}", "2020/02/29 00:00:59",
            EscrowDate.minusMinutes("2020/02/29 00:00:59", 60, "yyyy/MM/dd HH:mm:ss")
        );

        // minusDays
        log.info("\n\n // {}", "minusDays");
        log.info("minusDays today : {} >>> minus : {}", EscrowDate.getCurrentYmdHis(),
            EscrowDate.minusDays(EscrowDate.getCurrentYmdHis(), 2)
        );
        log.info("minusDays today : {} >>> minus : {}", "2020/02/29 00:00:59",
            EscrowDate.minusDays("2020/02/29 00:00:59", 2, "yyyy/MM/dd HH:mm:ss")
        );

        // isExpireCheckMin - 주문서 체류 시간 확인.
        log.info("\n\n // {}", "isExpireCheckMin");
        log.info("isExpireCheckMin '{}' 60min : {}", "2019-12-26 09:00:00",
            EscrowDate.isExpireCheckMin("2019-12-26 09:00:00", 60)
        );
        log.info("isExpireCheckMin '{}' 60min : {}", "2019-12-26 08:40:00",
            EscrowDate.isExpireCheckMin("2019-12-26 08:40:00", 60)
        );
        log.info("isExpireCheckMin '{}' 60min : {}", "2020-12-26 08:40:00",
            EscrowDate.isExpireCheckMin("2020-12-26 08:40:00", 60)
        );

        // isExpireCheckDay - 성인 인증 만료 일자 확인
        log.info("\n\n // {}", "isExpireCheckDay");
        log.info("isExpireCheckDay '{}' 365day : {}", "2018-12-25 09:00:00",
            EscrowDate.isExpireCheckDay("2018-12-25 09:00:00", 365)
        );
        log.info("isExpireCheckDay '{}' 365day : {}", "2018-12-26 09:46:00",
            EscrowDate.isExpireCheckDay("2018-12-26 09:46:00", 365)
        );
        log.info("isExpireCheckDay '{}' 365day : {}", "2020-12-26 08:40:00",
            EscrowDate.isExpireCheckDay("2020-12-26 08:40:00", 365)
        );

        // checkStartEndDt
        log.info("\n\n // {}", "checkStartEndDt");
        log.info("checkStartEndDt startDt-'{}', endDt-'{}' : {}", "2019-12-25 09:00:00", "2019-12-26 09:00:00",
            EscrowDate.checkStartEndDt("2019-12-25 09:00:00", "2019-12-26 09:00:00")
        ); //true
        log.info("checkStartEndDt startDt-'{}', endDt-'{}' : {}", "2019-12-25 09:00:00", "2019-12-25 09:00:00",
            EscrowDate.checkStartEndDt("2019-12-25 09:00:00", "2019-12-25 09:00:00")
        ); //true? false?
        log.info("checkStartEndDt startDt-'{}', endDt-'{}' : {}", "2019-12-25 09:00:00", "2019-12-20 09:00:00",
            EscrowDate.checkStartEndDt("2019-12-25 09:00:00", "2019-12-20 09:00:00")
        ); //false
        log.info("checkStartEndDt startDt-'{}', endDt-'{}' : {}", "2019/12/25 09:00:00", "2019/12/26 09:00:00",
            EscrowDate.checkStartEndDt("2019/12/25 09:00:00", "2019/12/26 09:00:00", "yyyy/MM/dd HH:mm:ss")
        ); //true
        log.info("checkStartEndDt startDt-'{}', endDt-'{}' : {}", "2019/12/25 09:00:00", "2019/12/25 09:00:00",
            EscrowDate.checkStartEndDt("2019/12/25 09:00:00", "2019/12/25 09:00:00", "yyyy/MM/dd HH:mm:ss")
        ); //true? false?
        log.info("checkStartEndDt startDt-'{}', endDt-'{}' : {}", "2019/12/25 09:00:00", "2019/12/20 09:00:00",
            EscrowDate.checkStartEndDt("2019/12/25 09:00:00", "2019/12/20 09:00:00", "yyyy/MM/dd HH:mm:ss")
        ); //false

        // checkStartEndDt
        log.info("\n\n // {}", "getEndOfMonth");
        log.info("getEndOfMonth : {}",
            EscrowDate.getEndOfMonth(2020, 2)
        );
    }
}
