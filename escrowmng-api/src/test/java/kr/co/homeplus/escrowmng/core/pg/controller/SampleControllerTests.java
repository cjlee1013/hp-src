package kr.co.homeplus.escrowmng.core.pg.controller;

import com.google.re2j.Matcher;
import com.google.re2j.Pattern;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedHashMap;
import kr.co.homeplus.escrowmng.claim.model.external.ShipSlotStockConfirmDto;
import kr.co.homeplus.escrowmng.claim.model.external.ShipSlotStockChangeDto;
import kr.co.homeplus.escrowmng.claim.service.ClaimProcessService;
import kr.co.homeplus.escrowmng.claim.service.ExternalService;
import kr.co.homeplus.escrowmng.claim.sql.ClaimInfoListSql;
import kr.co.homeplus.escrowmng.claim.sql.ClaimManagementSql;
import kr.co.homeplus.escrowmng.claim.sql.ClaimMessageSql;
import kr.co.homeplus.escrowmng.claim.sql.ClaimOrderCancelSql;
import kr.co.homeplus.escrowmng.core.TestConfig;
import kr.co.homeplus.escrowmng.order.model.ship.OrderShipAddrEditDto;
import kr.co.homeplus.escrowmng.order.sql.OrderSearchSql;
import kr.co.homeplus.escrowmng.order.sql.OrderShipSql;
import kr.co.homeplus.escrowmng.utils.EscrowDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * SampleController test
 * https://github.com/json-path/JsonPath 참조.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Suite.SuiteClasses({TestConfig.class})
public class SampleControllerTests {

    @Autowired
    private ClaimProcessService claimProcessService;

    @Autowired
    private ExternalService externalService;

    @Test
    public void test_sample() throws Exception {
//        System.out.println(new BigDecimal(2990).setScale(-3, RoundingMode.DOWN).multiply(new BigDecimal(1.9)).divide(new BigDecimal(100), RoundingMode.DOWN).intValue());
//        System.out.println(new BigDecimal(29900).divide(new BigDecimal(100), 0, RoundingMode.FLOOR).multiply(new BigDecimal(1.9)).intValue());
        Matcher matcher = Pattern.compile("(\\d{3})(\\d{2})(\\d{5,6})").matcher("2208160348");
        if(matcher.find()){
            System.out.println(matcher.group(1) + '-' + matcher.group(2) + '-' + matcher.group(3));
        }
    }

    @Test
    public void test_mileage() throws Exception {
//        System.out.println("01".contains("1111"));
//        System.out.println(ClaimOrderCancelSql.selectPrevClaimOrderCancelCheck(3000014689L, 3000017694L));
//        System.out.println(OrderSearchSql.selectOrderSavePopInfo(3000013065L));
//        System.out.println(EscrowDate.getCurrentYmd("yyyyMMdd"));
        System.out.println(OrderSearchSql.selectOrderDetailPriceInfo(3000016213L));
    }

    @Test
    public void test_sql_toString() throws Exception {
//        System.out.println(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, null, ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.MULTIPLY, ObjectUtils.toArray("itemPrice", "claimItemQty"), null))));
        String AAA = "UPDATE claim_bundle\n"
            + "SET claim_status = 'C2', chg_dt = NOW()\n"
            + "WHERE (claim_no = 12 AND claim_bundle_no = 12) \n"
            + "AND (claim_status = 'C1') \n"
            + "OR (claim_status = 'C8')";

        System.out.println(AAA.replaceAll("\\) \nOR \\(", " OR ")) ;
    }

    @Test
    public void test_sql() throws Exception {
        System.out.println(
            externalService.externalProcess(
                ShipSlotStockChangeDto.builder()
                    .purchaseOrderNo(403)
                    .userNo("12484584")
                    .slotStockRegType("ORD")
                    .chgShipDt("2020-06-12")
                    .chgShiftId("610")
                    .chgSlotId("6101")
                    .build()
            ));

        Thread.sleep(2000);

        System.out.println(
            externalService.externalProcess(
                ShipSlotStockConfirmDto.builder()
                    .purchaseOrderNo(403)
                    .userNo("12484584")
                    .slotStockRegType("ORD")
                    .chgShipDt("2020-06-12")
                    .chgShiftId("610")
                    .chgSlotId("6101")
                    .build()
            ));
    }
}

