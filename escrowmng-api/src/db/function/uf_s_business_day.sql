CREATE DEFINER=`app_escrow`@`%` FUNCTION `uf_s_business_day`(few_day varchar(10), add_day int, p_holiday_yn char, p_non_delivery_yn char, p_non_settle_yn char, p_ignore_prev_day char) RETURNS datetime
BEGIN
	DECLARE v_cnt INT DEFAULT 0;
    DECLARE v_loop INT DEFAULT 0;
    DECLARE v_sum_days INT DEFAULT 0;
    DECLARE v_return_value VARCHAR(10);
    DECLARE v_incr INT DEFAULT 0;
    DECLARE v_add_days INT DEFAULT 0;
    DECLARE v_check_prev_day VARCHAR(10);
    DECLARE v_prev_add_day INT DEFAULT 1;
    set v_sum_days = 0;
    set add_day = IFNULL(add_day, 0);
    set v_incr = CASE add_day WHEN 0 THEN 1 ELSE IFNULL(add_day / ABS(add_day), 1) END;
    IF (add_day = 0) THEN
        set v_loop = 0;
    ELSEIF (v_incr = -1) THEN
        set v_loop = -1;
    ELSE
        set v_loop = 1;
    END IF;
    IF (add_day = 0) THEN
        set v_add_days = v_incr;
    ELSE
        set v_add_days = add_day;
    END IF;
    IF LOWER(p_holiday_yn) != 'y' THEN
       set p_holiday_yn = 'n';
    END IF;
    IF LOWER(p_non_delivery_yn) != 'y' THEN
       set p_non_delivery_yn = 'n';
    END IF;
    IF LOWER(p_non_settle_yn) != 'y' THEN
       set p_non_settle_yn = 'n';
    END IF;
    IF LOWER(p_ignore_prev_day) != 'y' THEN
       set p_ignore_prev_day = 'n';
    END IF;
    REPEAT
      select date_add(few_day, interval v_loop day) into v_return_value;
      IF (p_holiday_yn = 'y' AND p_non_delivery_yn = 'n' and p_non_settle_yn = 'n') THEN
        select count(*) into v_cnt from business_day
          where business_day = v_return_value
            and holiday_yn = 'y'
            and non_delivery_yn = 'y'
            and non_settle_yn = 'y'
            and use_yn = 'y';
      ELSEIF (p_holiday_yn = 'n' AND p_non_delivery_yn = 'y' and p_non_settle_yn = 'n') THEN
        select count(*) into v_cnt from business_day
          where business_day = v_return_value
            and holiday_yn = 'n'
            and non_delivery_yn = 'y'
            and non_settle_yn = 'n'
            and use_yn = 'y';
      ELSEIF (p_holiday_yn = 'n' AND p_non_delivery_yn = 'n' and p_non_settle_yn = 'y') THEN
        select count(*) into v_cnt from business_day
          where business_day = v_return_value
            and holiday_yn = 'n'
            and non_delivery_yn = 'n'
            and non_settle_yn = 'y'
            and use_yn = 'y';
      ELSEIF (p_holiday_yn = 'n' AND p_non_delivery_yn = 'y' and p_non_settle_yn = 'y') THEN
        select count(*) into v_cnt from business_day
          where business_day = v_return_value
            and holiday_yn = 'n'
            and non_delivery_yn = 'y'
            and non_settle_yn = 'y'
            and use_yn = 'y';
      ELSEIF (p_holiday_yn = 'y' AND p_non_delivery_yn = 'y' and p_non_settle_yn = 'n') THEN
        select count(*) into v_cnt from business_day
          where business_day = v_return_value
            and use_yn = 'y'
            and ((holiday_yn = 'y'
                  and non_delivery_yn = 'y'
                  and non_settle_yn = 'y')
              or (holiday_yn = 'n'
                  and non_delivery_yn = 'y'));
      ELSEIF (p_holiday_yn = 'y' AND p_non_delivery_yn = 'y' and p_non_settle_yn = 'y') THEN
        select count(*) into v_cnt from business_day
          where business_day = v_return_value
            and use_yn = 'y'
            and ( holiday_yn = 'y'
                  or non_delivery_yn = 'y'
                  or non_settle_yn = 'y');
      ELSE
        select count(*) into v_cnt from business_day
          where business_day = v_return_value
            and holiday_yn = p_holiday_yn
            and holiday_yn = 'n'
            and non_delivery_yn = 'n'
            and non_settle_yn = 'n'
            and use_yn = 'y';
      END IF;
      IF (v_cnt = 0) THEN
         set v_sum_days = v_sum_days + v_incr;
      END IF;
      set v_loop = v_loop + v_incr;
    UNTIL v_sum_days = v_add_days END REPEAT;
    IF (p_ignore_prev_day = 'y') THEN
        set v_loop = 0;
        REPEAT
            select date_add(v_return_value, interval v_loop + v_incr day) into v_check_prev_day;
            IF (p_holiday_yn = 'y' AND p_non_delivery_yn = 'y' and p_non_settle_yn = 'n') THEN
                select count(*) into v_cnt
                from business_day
                  where business_day = v_check_prev_day
                    and use_yn = 'y'
                    and ((holiday_yn = 'y'
                          and non_delivery_yn = 'y'
                          and non_settle_yn = 'y')
                      or ( holiday_yn = 'n'
                          and non_delivery_yn = 'y'
                          and non_settle_yn = 'n'));
            ELSE
                select count(*) into v_cnt from business_day
                  where business_day = v_check_prev_day
                    and holiday_yn = 'y'
                    and non_delivery_yn = 'y'
                    and non_settle_yn = 'y'
                    and use_yn = 'y';
            END IF;
            set v_loop = v_loop + v_incr;
            IF v_cnt = 0 THEN
              set v_prev_add_day = v_prev_add_day + 1;
            ELSE
              set v_prev_add_day = v_prev_add_day * 0;
            END IF;
        UNTIL v_prev_add_day = 2 END REPEAT;
        IF v_loop > 1 THEN
            select date_add(v_check_prev_day, interval -1 day) into v_return_value;
        END IF;
    END IF;
RETURN STR_TO_DATE(concat(v_return_value, '23:59:59'),'%Y-%m-%d %H:%i:%s');
END