CREATE DEFINER=`app_escrow`@`%` PROCEDURE `escrow`.`sp_after_pos_receipt_issue`()
    COMMENT '영수증번호 채번 '
AFTER_POS_RECEIPT:BEGIN
/****************************************************************************************
- 목적/용도
	1. 정상적으로 생성된 주문에 대해서 점포별 영수증번호 채번을 함.
    2. 영수증번호 채번 시작은 3번 부터 해야함.
    3. 1,2번은 SOD에서 채번을 함.
    4. 9000번까지 채번되면 그날 점포 주문은 막힘
    -. 자바에서는 9200번까지 체크함
    5. 선물세트몰
    	-. HYPER : 784 Pos번호 사용
    	-. CLUB : 792 Pos번호 사
    6. 영수증번호 채번대상 조건
		-. purchase_order.pine_if_yn = 'S' and purchase_store_info.trade_no = null
	7. 영수증번호 채번대상 상태 변경
		-. purchase_order.pine_if_yn = 'C'

- 작성규칙
	1. SQL작성시 예약어만 대문자로 작성
    2. 변수명 작성은 카멜규칙을 따름

- 작성일자/내용/작성자
	1. 2020-11-20 / 최초작성 /  송영준
	2. 2020-12-28 / CLUB 선물세트(예약주문) POS번호 적용 792 POS번호 사용 / 송영준
	3. 2021-08-05 / order_type에 ORD_MARKET(마켓연동주문) 추가 / 홍주영
***************************************************************************************/


    DECLARE vHomeBasicPosNo781 INT DEFAULT 781;
    DECLARE vHomeBasicPosNo782 INT DEFAULT 782;
    DECLARE vHomeBasicPosNo783 INT DEFAULT 783;

    DECLARE vClubBasicPosNo791 INT DEFAULT 791;
--     DECLARE vClubBasicPosNo792 INT DEFAULT 792;

	DECLARE vExpBasicPosNo718 INT DEFAULT 718;
    DECLARE vExpBasicPosNo719 INT DEFAULT 719;

	DECLARE vHyperGiftPosNo784 INT DEFAULT 784;
	DECLARE vClubGiftPosNo792 INT DEFAULT 792;

    #TODO : 운영시에는 9000번 사용
    DECLARE vTradeNoMax INT DEFAULT 9000;

	## 0. 프로시저 공통 변수 및 커서 선언
	DECLARE vDone                					INT DEFAULT FALSE;	## 대상주문조회완료여부
    DECLARE vDoneFinal                				INT DEFAULT FALSE;	## 영수증번호 채번 완료 대상 상태 변경여부
	DECLARE vTargetCount         					INT DEFAULT 0;		## 대상건수
	DECLARE vCheckRowCnt         					INT DEFAULT 0;		## 데이터검증용건수
	DECLARE vPurchaseOrderNo     					VARCHAR(30);		## 구매주문번호
	DECLARE vPurchaseStoreInfoNo 					VARCHAR(30);		## 구매점포번호
    DECLARE vOriginStoreId			 				VARCHAR(4);			## 점포번호
    DECLARE vPosNo				 					VARCHAR(6);			## 포스번호
    DECLARE vTradeDt			 					VARCHAR(8);			## 거래일자(YYYYMMDD)
    DECLARE vTradeNo			 					VARCHAR(4);			## 영수증번호
    DECLARE vReserveYn			 					VARCHAR(1);			## 예약주문(명절선물세트) Y:명절선물세트주문, N:일반주문
    DECLARE vShipMethod			 					VARCHAR(10);		## 배송방법(TD:TD_DRCT,TD_PICK,TD_QUICK, DS:DS)

	DECLARE vLgwBatchNo           					BIGINT DEFAULT 0;  ## 이력번호(order_batch_history)
    DECLARE vFailCount           					BIGINT DEFAULT 0;  ## 실패건수(order_batch_history)
    DECLARE vSuccessCount        					BIGINT DEFAULT 0;  ## 성공건수(order_batch_history)
    DECLARE vTotalCount          					BIGINT DEFAULT 0;  ## 전체건수(order_batch_history)
    DECLARE vErrorMessage        					VARCHAR(2000);     ## 오류메시지(order_batch_history)
	DECLARE vProcedureNm		 					VARCHAR(100) DEFAULT 'sp_after_pos_receipt_issue';		## 프로시져명

	DECLARE vErrorNo             					INT DEFAULT 0;		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlState            					VARCHAR(1000);		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlErrorNo          					INT DEFAULT 0;		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlMessage          					VARCHAR(1000);		## SQLEXCEPTION 핸들러 변수
    DECLARE vLogPurchaseStoreInfo 					VARCHAR(1000);	## 전송대상 주문번호 / 점포주문번호

    DECLARE vLogLevelINFO		VARCHAR(20) DEFAULT 'INFO';
    DECLARE vLogLevelERROR		VARCHAR(20) DEFAULT 'ERROR';

	##영수증번호 발급대상 주문 조회
	DECLARE cursorPurchaseOrder CURSOR FOR
		SELECT po.purchase_order_no
		FROM purchase_order po
		INNER JOIN payment p ON po.payment_no = p.payment_no AND p.payment_status in('P3','P5')
		WHERE 	po.pine_if_yn = 'S'
        AND 	po.order_type in ('ORD_NOR','ORD_COMBINE','ORD_TICKET','ORD_MULTI','ORD_SUB','ORD_MARKET')
		;

	##영수증번호 발급완료 주문 상태 변경
	DECLARE cursorPurchaseOrderFinal CURSOR FOR
		SELECT po.purchase_order_no
		FROM purchase_order po
		INNER JOIN payment p ON po.payment_no = p.payment_no AND p.payment_status in('P3','P5')
		WHERE 	po.pine_if_yn = 'C'
        AND 	po.order_type in ('ORD_NOR','ORD_COMBINE','ORD_TICKET','ORD_MULTI','ORD_SUB','ORD_MARKET')
		;


	##영수증번호 발급
	DECLARE cursorPurchaseStoreInfo CURSOR FOR      ## 구매점포커서
		SELECT
				psi.purchase_order_no					AS purchase_order_no
			,	psi.purchase_store_info_no				AS purchase_store_info_no
			, 	psi.pos_no								AS pos_no
            ,	psi.origin_store_id						AS origin_store_id
            ,	psi.trade_dt							AS trade_dt
            ,	po.reserve_yn							AS reserve_yn
            ,	psi.ship_method							AS ship_method
		FROM purchase_order po
		INNER JOIN purchase_store_info psi ON po.purchase_order_no = psi.purchase_order_no
		WHERE po.pine_if_yn = 'C'
		AND psi.trade_no IS NULL
		;

	#CURSOR 루프 핸들러, CURSOR대상이 없을경우 변경 조건
	DECLARE CONTINUE HANDLER FOR NOT FOUND
		SET vDone = TRUE;

	#SQLEXCEPTION 핸들러 (오류발생시 처리)
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
            GET DIAGNOSTICS CONDITION 1 vSqlState = RETURNED_SQLSTATE, vSqlErrorNo = MYSQL_ERRNO, vSqlMessage = MESSAGE_TEXT;
            SET vErrorNo =  -1;
        END;

    ## tx_isolation변경
    SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;

	## 배치로그 시작
	CALL sp_lgw_batch_history(vProcedureNm, 'START', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
	SET vLgwBatchNo = @outLgwBatchNo;

	PURCHASE_ORDER:BEGIN
		## 커서 시작 : cursorPurchaseOrder
		OPEN cursorPurchaseOrder;
			SELECT FOUND_ROWS() INTO vTargetCount;

			## 인터페이스 대상이 없을 경우 프로시저 종료
			IF vTargetCount = 0 THEN
				## log insert - 종료: 대상없음
				CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwBatchNo, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
                SET vDone = FALSE;
				LEAVE PURCHASE_ORDER;
			END IF;

			## 작업대상 데이터 업데이트 (pine_if_yn : S(영수증번호발급대상주문) -> C(영수증번호발급대기 변경))
			read_po_loop: LOOP

				FETCH cursorPurchaseOrder INTO vPurchaseOrderNo;
					IF NOT vDone THEN
						UPDATE purchase_order
						SET
								pine_if_yn = 'C'
							,	omni_if_yn = 'C'
                            , 	pine_if_dt = NOW()
                            ,	omni_if_dt = NOW()
						WHERE purchase_order_no = vPurchaseOrderNo
						AND pine_if_yn = 'S';
					END IF;

					IF vDone THEN
						LEAVE read_po_loop;
					END IF;

			END LOOP read_po_loop;

			SET vDone = FALSE;
		CLOSE cursorPurchaseOrder;
	END PURCHASE_ORDER;


	## 영수증번호 채번 시작
	OPEN cursorPurchaseStoreInfo;
		SELECT FOUND_ROWS() INTO vTargetCount;
		## 프로시저 시작 로그
		-- CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, 'purchaseStoreInfo START', NULL, NULL, NULL, NULL, NULL);
		read_psi_loop: REPEAT

		FETCH cursorPurchaseStoreInfo INTO vPurchaseOrderNo, vPurchaseStoreInfoNo, vPosNo, vOriginStoreId, vTradeDt, vReserveYn, vShipMethod;


		SET vErrorNo = 0;
		SET vCheckRowCnt = 0;
		SET vLogPurchaseStoreInfo = CONCAT('[주문번호]:',vPurchaseOrderNo,'[점포주문번호]:',vPurchaseStoreInfoNo);

			IF NOT vDone THEN
				SET vTotalCount = vTotalCount + 1;

				## 프로시저 작업대상 처리시작
				START TRANSACTION;

				SET @rtnPosNo = '';
				SET @rtnTradeNo = 0;

				#예약주문 & 명절선물세트(배송방법:TD_DLV)일경우만 포스번호 784 사용
				IF vReserveYn = 'Y' AND vShipMethod = 'TD_DLV' AND vPosNo = '784' THEN
					SET vPosNo = vHyperGiftPosNo784;
				ELSEIF vReserveYn = 'Y' AND vShipMethod = 'TD_DLV' AND vPosNo = '792' THEN
					SET vPosNo = vClubGiftPosNo792;
				END IF;

				## 점포+거래일자+포스번호 기준에 다음 영수증 번호 채번
				SELECT 	IFNULL(MAX(trade_no),2)+1
				INTO 	vTradeNo
				FROM 	pos_receipt_issue
				WHERE 	store_id = vOriginStoreId
				AND		trade_dt = vTradeDt
				AND		pos_no = vPosNo;

				#영수증 채번 기준 POS번호(Update 대상)
				SET @rtnPosNo = vPosNo;

				#SITE HOME(HYPER/EXP) 781/718 기본POS 채번
				IF vPosNo = vHomeBasicPosNo781 OR vPosNo = vExpBasicPosNo718 THEN
					#Express 기본POS(718)번호가 9000번 넘는지 확인
					IF vPosNo = vExpBasicPosNo718 THEN
						IF vTradeNo > vTradeNoMax THEN
							#719 pos번호로 영수증번호 다시 채번
							SELECT 	IFNULL(MAX(trade_no),2)+1
							INTO 	vTradeNo
							FROM 	pos_receipt_issue
							WHERE 	store_id = vOriginStoreId
							AND		trade_dt = vTradeDt
							AND		pos_no = vExpBasicPosNo719;

							SET @rtnPosNo = vExpBasicPosNo719;
						END IF;
					ELSE
						#기본POS(781)번호가 9000번 넘는지 확인
						IF vTradeNo > vTradeNoMax THEN
							#782 pos번호로 영수증번호 다시 채번
							SELECT 	IFNULL(MAX(trade_no),2)+1
							INTO 	vTradeNo
							FROM 	pos_receipt_issue
							WHERE 	store_id = vOriginStoreId
							AND		trade_dt = vTradeDt
							AND		pos_no = vHomeBasicPosNo782;

							SET @rtnPosNo = vHomeBasicPosNo782;

							#782 영수증번호 9000번 넘는지 확인
							IF vTradeNo > vTradeNoMax THEN
								#783 pos번호로 영수증번호 다시 채번
								SELECT 	IFNULL(MAX(trade_no),2)+1
								INTO 	vTradeNo
								FROM 	pos_receipt_issue
								WHERE 	store_id = vOriginStoreId
								AND		trade_dt = vTradeDt
								AND		pos_no = vHomeBasicPosNo783;

								SET @rtnPosNo = vHomeBasicPosNo783;
							END IF;
						END IF;
					END IF;

				#SiteType CLUB일경우 처리
-- 				ELSE

				-- 2020/12/28 CLUB POS번호는 한개인 관계로 로직 주석 처리함.
-- 					IF vPosNo = vClubBasicPosNo791 THEN
-- 						#기본POS(791)번호가 9000번 넘는지 확인
-- 						IF vTradeNo > vTradeNoMax THEN
-- 							#792 pos번호로 영수증번호 다시 채번
-- 							SELECT 	IFNULL(MAX(trade_no),2)+1
-- 							INTO 	vTradeNo
-- 							FROM 	pos_receipt_issue
-- 							WHERE 	store_id = vOriginStoreId
-- 							AND		trade_dt = vTradeDt
-- 							AND		pos_no = vClubBasicPosNo792;
--
-- 							SET @rtnPosNo = vClubBasicPosNo792;
-- 						END IF;
-- 					END IF;

				END IF;


				##채번된 점포에 영수증 번호 입력
				INSERT INTO pos_receipt_issue
				(
                    store_id, 					trade_dt,
                    pos_no, 					trade_no
				)
				VALUES
				(
					vOriginStoreId, 			vTradeDt,
                    @rtnPosNo, 					vTradeNo
				)
                ;
				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (C:영수증번호발급대기 -> Q:영수증번호발급실패)
					UPDATE purchase_order
					SET
							pine_if_yn = 'Q'
						,	omni_if_yn = 'Q'
						, 	pine_if_dt = NOW()
						, 	omni_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'C';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT pos_receipt_issue' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;

                #발급된 영수증번호 Update
				UPDATE purchase_store_info
				SET	pos_no = @rtnPosNo
					, trade_no = vTradeNo
				WHERE purchase_store_info_no = vPurchaseStoreInfoNo
				;
				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (C:영수증번호발급대기 -> Q:영수증번호발급실패)
					UPDATE purchase_order
					SET
							pine_if_yn = 'Q'
						,	omni_if_yn = 'Q'
						, 	pine_if_dt = NOW()
						, 	omni_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'C';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT purchase_store_info' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				COMMIT; ## Transaction 최종 커밋

				SET vSuccessCount = vSuccessCount + 1;
				SET vDone = FALSE;

			END IF;

		UNTIL vDone
		END REPEAT read_psi_loop;

	CLOSE cursorPurchaseStoreInfo;


PURCHASE_ORDER_FINAL:BEGIN
	#CURSOR 루프 핸들러, CURSOR대상이 없을경우 변경 조건
	DECLARE CONTINUE HANDLER FOR NOT FOUND
		BEGIN
			SET vDoneFinal = TRUE;
		END;

		## 커서 시작 : cursorPurchaseOrderFinal
		OPEN cursorPurchaseOrderFinal;
			SELECT FOUND_ROWS() INTO vTargetCount;

			## 작업완료 주문 Pine/Omni전송상태 변경 업데이트 (C -> N)
			read_po_final_loop: LOOP

				FETCH cursorPurchaseOrderFinal INTO vPurchaseOrderNo;
					IF NOT vDoneFinal THEN
						UPDATE purchase_order
						SET
								pine_if_yn = 'N'
							,	omni_if_yn = 'N'
                            , 	pine_if_dt = NOW()
                            ,	omni_if_dt = NOW()
						WHERE purchase_order_no = vPurchaseOrderNo
						AND pine_if_yn = 'C';
					END IF;

					IF vDoneFinal THEN
						LEAVE read_po_final_loop;
					END IF;

			END LOOP read_po_final_loop;

		CLOSE cursorPurchaseOrderFinal;
	END PURCHASE_ORDER_FINAL;

	## tx_isolation변경
    SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;

	## 배치로그 종료
	CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwBatchNo, vFailCount, vSuccessCount,vTotalCount, vErrorMessage, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);

END AFTER_POS_RECEIPT