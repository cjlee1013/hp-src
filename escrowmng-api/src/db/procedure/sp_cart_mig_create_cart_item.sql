## 0으로 시작하는 user_no 없음
## cart 생성용 쿼리 실행 (2천 5백만건 10분 소요 예상)
INSERT INTO
    escrow.cart (
        user_no
    )
SELECT DISTINCT user_no FROM ifdms.refit_sback_dtl_real rsdl;


## cart_item Migration procedure
CREATE DEFINER=`app_escrow`@`%` PROCEDURE `escrow`.`sp_cart_mig_create_cart_item`()
    COMMENT '장바구최초마이그레이션 - cart_item 마이그레이션 '
CART_PROCESS:begin

	## 0. 프로시저 공통 변수 및 커서 선언
	DECLARE vDone         INT DEFAULT FALSE;   ## 작업완료여부
	DECLARE vTargetCount  INT DEFAULT 0;       ## 대상건수

    DECLARE vSuccessCount BIGINT DEFAULT 0;    ## 성공건수(order_batch_history)

	DECLARE vErrorNo      INT DEFAULT 0;	   ## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlState     VARCHAR(1000);	   ## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlErrorNo   INT DEFAULT 0;	   ## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlMessage   VARCHAR(1000);	   ## SQLEXCEPTION 핸들러 변수

	## CURSOR FETCH 대상
	DECLARE vUserNo         VARCHAR(9);        ## 고객번호 (refit_sback_dtl_real 조회용이라 varchar)
	DECLARE vCartNo			BIGINT;			   ## 장바구니번호

	## MAIN CURSOR
	DECLARE cursorCartInfo CURSOR FOR
		SELECT
			car.user_no 					   AS user_no
			, car.cart_no					   AS cart_no
		FROM cart car;

	## CURSOR HANDLER
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION  ## SQLEXCEPTION 핸들러
		BEGIN
            GET DIAGNOSTICS CONDITION 1 vSqlState = RETURNED_SQLSTATE, vSqlErrorNo = MYSQL_ERRNO, vSqlMessage = MESSAGE_TEXT;
            SET vErrorNo =  -1;
        END;

    ## 루프 핸들러
    DECLARE CONTINUE HANDLER FOR NOT FOUND
		SET vDone = TRUE;



	#### 프로시저 시작
	CALL sp_cart_mig_history('sp_cart_mig_create_cart_item', '#00 cart_item 마이그레이션 프로시저 시작');

	## 확인용 변수
	set @TotalCartItemCnt = 0;
    SET @CartItemCnt = 0;

	#### 01 cart_item 생성 시작
	OPEN cursorCartInfo;
		SELECT FOUND_ROWS() INTO vTargetCount;

		IF vTargetCount = 0 THEN
			CALL sp_cart_mig_history('### ERROR 01: 마이그레이션 대상 없음');
			CALL sp_cart_mig_history('sp_cart_mig_create_cart_item', '#99 cart 생성 프로시저 종료 ');
			LEAVE CART_PROCESS;
		END IF;
		CALL sp_cart_mig_history('sp_cart_mig_create_cart_item', CONCAT('#01 cart_item 데이터 마이그레이션 시작 - target: ', vTargetCount, ' 건'));

		read_cart_item_loop: REPEAT

			FETCH cursorCartInfo INTO vUserNo, vCartNo;
			SET vErrorNo = 0;

			IF NOT vDone THEN
				START TRANSACTION;

				INSERT INTO cart_item (
					cart_no
					, site_type
					, store_type
					, store_kind
					, item_no
					, opt_no
					, qty
					, cart_keep_yn
				) SELECT
					MIN(car.cart_no)													AS cart_no
					, rsdr.site_type													AS site_type
					, CASE WHEN rsdr.site_type = 'CLUB' THEN 'CLUB' ELSE 'HYPER' END	AS store_type
					, 'NOR' 															AS store_kind
					, rsdr.item_no														AS item_no
					, 0																	AS opt_no
					, MAX(rsdr.item_qty)												AS qty
					, MAX(rsdr.cart_keep_yn) 											AS cart_keep_yn
					FROM cart car
					INNER JOIN ifdms.refit_sback_dtl_real rsdr ON car.user_no = rsdr.user_no
					WHERE rsdr.user_no = vUserNo /* varchar 형태 012345678 */
					GROUP BY rsdr.user_no, rsdr.site_type, rsdr.item_no
					ORDER BY null;

				#SQLERROR 처리
				IF vErrorNo < 0 THEN
					ROLLBACK;
					CALL sp_cart_mig_history('sp_mig_create_cart', concat('### ERROR 02: [',vSqlErrorNo,'] ', vSqlMessage, '(',vSqlstate,')'));
					CALL sp_cart_mig_history('sp_mig_create_cart', '#99 cart 생성 프로시저 종료 ');
					LEAVE CART_PROCESS;
				END IF;
				SELECT ROW_COUNT() INTO @CartItemCnt;
			    set @TotalCartItemCnt = @TotalCartItemCnt + @CartItemCnt;

				#MIGRATION 완료 FLAG update
				SET vErrorNo = 0;
				UPDATE
					cart
				SET
					migration_yn = 'Y'
				WHERE cart_no = vCartNo;

				#SQLERROR 처리
				IF vErrorNo < 0 THEN
					ROLLBACK;
					CALL sp_cart_mig_history('sp_mig_create_cart', CONCAT('### ERROR 02: [',vSqlErrorNo,'] ', vSqlMessage, '(',vSqlstate,')'));
					CALL sp_cart_mig_history('sp_mig_create_cart', '#99 cart 생성 프로시저 종료 ');
					LEAVE CART_PROCESS;
				END IF;

				COMMIT;

				SET vSuccessCount = vSuccessCount + 1;
				if vSuccessCount % 10000 = 0 THEN
					CALL sp_cart_mig_history('sp_mig_create_cart', CONCAT('##### ', vSuccessCount, '건 진행 ...'));
				end if;
				SET vDone = FALSE;
			END if;

		UNTIL vDone
		END REPEAT read_cart_item_loop;

	CLOSE cursorCartInfo;

	CALL sp_cart_mig_history('sp_cart_mig_create_cart_item', CONCAT('#02 cart_item 데이터 마이그레이션 완료 - total item: ', @TotalCartItemCnt, ' 건', ', ', vSuccessCount, '/', vTargetCount, '(성공 / 대상)'));


	## 프로시저 종료 로그
	CALL sp_cart_mig_history('sp_cart_mig_create_cart_item', '#99 cart_item 마이그레이션 프로시저 종료 ');
end CART_PROCESS