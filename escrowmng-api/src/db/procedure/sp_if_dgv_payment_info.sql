CREATE DEFINER=`app_escrow`@`%` PROCEDURE `escrow`.`sp_if_dgv_payment_info`()
    COMMENT 'DGV 결제정보를 GVS IF 테이블에 입력'
DGV_PROCESS:BEGIN
/****************************************************************************************
- 목적/용도: Refit DGV 결제정보 GVS 시스템 연동 프로시저 (Refit IF 테이블에 데이터 적재)
- 작성일자/작성자 : 2020-10-28 / 박준수
- 수정일자/수정내용/수정자
- 2020-10-29 / payment_info 테이블의 DGV 전송여부 flag 조건 반영 / 박준수
  2020-11-17 / 모든 대상 테이블에 reg_dt 추가 적용 / 박준수
****************************************************************************************/

	## 프로시저 공통 변수 및 커서 선언
	DECLARE vPaymentInfoSeq BIGINT DEFAULT 0;    ## payment_info PK
	DECLARE vDone           INT DEFAULT FALSE;   ## 작업완료여부
	DECLARE vTargetCount    INT DEFAULT 0;       ## 대상건수

	DECLARE vErrorNo        INT DEFAULT 0;	     ## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlState       VARCHAR(1000);	     ## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlErrorNo     INT DEFAULT 0;	     ## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlMessage     VARCHAR(1000);	     ## SQLEXCEPTION 핸들러 변수

	DECLARE vLgwHistoryNo   BIGINT DEFAULT 0;    ## 이력번호(lgw_batch_history)
    DECLARE vFailCount      BIGINT DEFAULT 0;    ## 실패건수(lgw_batch_history)
    DECLARE vSuccessCount   BIGINT DEFAULT 0;    ## 성공건수(lgw_batch_history)
    DECLARE vTotalCount     BIGINT DEFAULT 0;    ## 전체건수(lgw_batch_history)
    DECLARE vErrorMessage   VARCHAR(2000);       ## 오류메시지(lgw_batch_history)
    DECLARE vLogDgvInfo		VARCHAR(1000);		 ## 오류저장 로그변수

    DECLARE vProcedureNm	VARCHAR(100) DEFAULT 'sp_if_dgv_payment_info';  ## 프로시져명
    DECLARE vLogLevelINFO	VARCHAR(20)  DEFAULT 'INFO';                    ## 로그레벨 INFO
    DECLARE vLogLevelERROR	VARCHAR(20)  DEFAULT 'ERROR';                   ## 로그레벨 ERROR

    ## 커서 선언 (작업 대상 전송여부: N -> I -> Y/E)
	DECLARE cursorPaymentInfo CURSOR FOR         ## payment_info 커서, 7일이내 DGV 결제완료건 조회
		SELECT pi.payment_info_seq
		FROM payment_info pi
			INNER JOIN purchase_store_info psi ON pi.payment_no = psi.payment_no AND psi.trade_no IS NOT NULL
		WHERE pi.payment_fsh_dt >= DATE_SUB(NOW(), INTERVAL 7 DAY)
		AND pi.payment_type = 'DGV'
		AND pi.payment_status = 'P3'
		AND pi.dgv_if_yn = 'N'
		GROUP BY pi.payment_info_seq;

	DECLARE CONTINUE HANDLER FOR NOT FOUND       ## 루프 핸들러
		SET vDone = TRUE;

	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION    ## SQLEXCEPTION 핸들러
		BEGIN
            GET DIAGNOSTICS CONDITION 1 vSqlState = RETURNED_SQLSTATE, vSqlErrorNo = MYSQL_ERRNO, vSqlMessage = MESSAGE_TEXT;
            SET vErrorNo =  -1;
        END;

	## tx_isolation변경
    SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;

    ## 배치로그 시작
    CALL sp_lgw_batch_history(vProcedureNm, 'START', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchHistoryNo);
	SET vLgwHistoryNo = @outLgwBatchHistoryNo;


	## 커서 시작 - 적재작업 시작
	OPEN cursorPaymentInfo;
		SELECT FOUND_ROWS() INTO vTargetCount;

		## 대상이 없을 경우 프로시저 종료
		IF vTargetCount = 0 THEN
			## 배치로그 update - 종료: 대상없음
			CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwHistoryNo, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchHistoryNo);
			## tx_isolation원복
    		SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
			LEAVE DGV_PROCESS;
		END IF;

		## 적재작업 loop 시작
		dgv_insert_loop: REPEAT

			FETCH cursorPaymentInfo INTO vPaymentInfoSeq;
			SET vErrorNo = 0;
			SET vLogDgvInfo = CONCAT('[paymentInfoSeq]:', vPaymentInfoSeq);

			IF NOT vDone THEN
				SET vTotalCount = vTotalCount + 1;

				## 작업시작전 대상 업데이트 (dgv_if_yn : 'N' -> 'I')
				## 해당 업데이트 구문은 auto commit 됨.
				UPDATE payment_info
				SET dgv_if_yn = 'I'
				WHERE payment_info_seq = vPaymentInfoSeq
				AND dgv_if_yn = 'N';

				## transaction 시작
				## 해당 transaction 은 아래에서 commit 또는 rollback 만나야 종료됨.
				START TRANSACTION;

				## payment_info 대상건 rf_t_collect_dgv 적재 (기 적재건 제외)
				INSERT INTO rf_t_collect_dgv (
					collect_no
					, branch_code
					, pos_no
					, receipt_no
					, collect_detail_no
					, collect_amt
					, item_no
					, collect_type
					, input_type
					, tr_ftype
					, app_no
					, app_date
					, app_time
					, van_code
					, org_collect_date
					, org_collect_branch
					, org_collect_pos_no
					, org_collect_receipt_no
					, org_collect_app_no
					, create_time
					, balance
					, tran_yn
					, reg_dt
				)
				SELECT
					DATE_FORMAT(pi.payment_fsh_dt, '%Y%m%d')            AS collect_no
					, '0901'                                            AS branch_code
					, LPAD(pi.dgv_pos_no, 6, '0')                       AS pos_no
					, LPAD(pi.dgv_trade_no, 4, '0')                     AS receipt_no
					, 1                                                 AS collect_detail_no
					, pi.amt                                            AS collect_amt
					, AES_DECRYPT(UNHEX(pi.dgv_card_no),'!@homeplus#$') AS item_no
					, '01'                                              AS collect_type
					, '01'                                              AS input_type
					, '0100'                                            AS tr_ftype
					, pi.approval_no                                    AS app_no
					, pi.dgv_approval_dt                                AS app_date
					, pi.dgv_approval_time                              AS app_time
					, '01'                                              AS van_code
					, DATE_FORMAT(pi.payment_fsh_dt, '%Y%m%d')          AS org_collect_date
					, '0901'                                            AS org_collect_branch
					, LPAD(pi.dgv_pos_no, 6, '0')                       AS org_collect_pos_no
					, LPAD(pi.dgv_trade_no, 4, '0')                     AS org_collect_receipt_no
					, pi.approval_no                                    AS org_collect_app_no
					, NOW()                                             AS create_time
					, pi.dgv_remain_amt                                 AS balance
					, 'N'                                               AS tran_yn
					, NOW()                                             AS reg_dt
				FROM payment_info pi
				WHERE pi.payment_info_seq = vPaymentInfoSeq
				AND pi.dgv_if_yn = 'I'; /* 전송여부 확인용 flag */

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업 실패건 업데이트 (dgv_if_yn : 'I' -> 'E')
					UPDATE payment_info
					SET dgv_if_yn = 'E'
					WHERE payment_info_seq = vPaymentInfoSeq
					AND dgv_if_yn = 'I';

					## 프로시저 로그 insert - 에러 종료
                    SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwHistoryNo, vLogLevelERROR, vLogDgvInfo,'INSERT rf_t_collect_dgv', vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE dgv_insert_loop;
                END IF;

                ## 작업 완료건 업데이트 (dgv_if_yn : 'I' -> 'Y')
                UPDATE payment_info
				SET dgv_if_yn = 'Y'
				WHERE payment_info_seq = vPaymentInfoSeq
				AND dgv_if_yn = 'I';

                ## Transaction 커밋
                COMMIT;

				SET vSuccessCount = vSuccessCount + 1;
				SET vDone = FALSE;

			END IF;

		UNTIL vDone
		END REPEAT dgv_insert_loop;

	## 커서 종료
	CLOSE cursorPaymentInfo;

	## tx_isolation원복
	SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;

	## 배치로그 update - 종료: 최종 건수 기록
	CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwHistoryNo, vFailCount, vSuccessCount, vTotalCount, vErrorMessage, NULL, NULL, NULL, NULL, NULL, @outLgwBatchHistoryNo);

END DGV_PROCESS