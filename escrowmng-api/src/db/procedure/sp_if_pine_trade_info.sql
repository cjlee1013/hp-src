CREATE DEFINER=`app_escrow`@`%` PROCEDURE `escrow`.`sp_if_pine_trade_info`()
    COMMENT 'PINE 매출연동 대상 데이터 IF 테이블에 입력'
MAIN_PROCESS:BEGIN
/****************************************************************************************
- 목적/용도: Refit 주문 데이터 PINE 매출연동 프로시저 (Refit IF 테이블에 데이터 적재)
- 작성규칙
	1. SQL작성시 예약어만 대문자로 작성
    2. 변수명 작성은 카멜규칙을 따름
- 작성일자/작성자 : 2020-05-25, 박준수
- 수정일자/수정내용/수정자
    2020-11-17 / 최초작성 / 박준수
    2021-05-07 / 추가중복쿠폰(TD/DS)추가 / 송영준
***************************************************************************************/

	## 0. 프로시저 공통 변수 및 커서 선언
	DECLARE vDone                							INT DEFAULT FALSE;	## 주문전송대상 Fectch완료여부
	DECLARE vDoneFinal             							INT DEFAULT FALSE;	## 전송완료변경대상 Fectch완료여부

	DECLARE vTargetCount         							INT DEFAULT 0;		## 대상건수
	DECLARE vCheckRowCnt         							INT DEFAULT 0;		## 데이터검증용건수
	DECLARE vPurchaseOrderNo     							VARCHAR(30);		## 구매주문번호
	DECLARE vPurchaseStoreInfoNo 							VARCHAR(30);		## 구매점포번호
    DECLARE vStoreId			 							VARCHAR(4);		## 점포번호
    DECLARE vPosNo				 							VARCHAR(6);		## 포스번호
    DECLARE vTradeDt			 							VARCHAR(8);		## 거래일자(YYYYMMDD)
    DECLARE vTradeNo			 							VARCHAR(4);		## 영수증번호

	DECLARE vCardPrefixNo		 							VARCHAR(20);		## 카드Prefix번호
    DECLARE vCardCompanyNm		 							VARCHAR(20);		## 카드사명
	DECLARE vLgwBatchNo           							BIGINT DEFAULT 0;  ## 이력번호(order_batch_history)
    DECLARE vFailCount           							BIGINT DEFAULT 0;  ## 실패건수(order_batch_history)
    DECLARE vSuccessCount        							BIGINT DEFAULT 0;  ## 성공건수(order_batch_history)
    DECLARE vTotalCount          							BIGINT DEFAULT 0;  ## 전체건수(order_batch_history)
    DECLARE vErrorMessage        							VARCHAR(2000);     ## 오류메시지(order_batch_history)
	DECLARE vProcedureNm		 							VARCHAR(100) DEFAULT 'sp_if_pine_trade_info';		## 프로시져명

	DECLARE vErrorNo             							INT DEFAULT 0;		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlState            							VARCHAR(1000);		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlErrorNo          							INT DEFAULT 0;		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlMessage          							VARCHAR(1000);		## SQLEXCEPTION 핸들러 변수
    DECLARE vLogPurchaseStoreInfo 							VARCHAR(1000);	## 전송대상 주문번호 / 점포주문번호

    DECLARE vLogLevelINFO									VARCHAR(20) DEFAULT 'INFO';
    DECLARE vLogLevelERROR									VARCHAR(20) DEFAULT 'ERROR';

	#주문전송대상
	DECLARE cursorPurchaseOrder CURSOR FOR
		SELECT po.purchase_order_no
		FROM purchase_order po
		INNER JOIN payment p ON po.payment_no = p.payment_no AND p.payment_status in('P3','P5')
		WHERE	po.pine_if_yn = 'N'
		AND 	po.order_type in ('ORD_NOR','ORD_COMBINE','ORD_TICKET','ORD_MULTI','ORD_SUB','ORD_MARKET')
		;

	#전송완료 변경대상
	DECLARE cursorPurchaseOrderFinal CURSOR FOR
		SELECT po.purchase_order_no
		FROM purchase_order po
		INNER JOIN payment p ON po.payment_no = p.payment_no AND p.payment_status in('P3','P5')
		WHERE 	po.pine_if_yn = 'I'
		AND 	po.order_type in ('ORD_NOR','ORD_COMBINE','ORD_TICKET','ORD_MULTI','ORD_SUB','ORD_MARKET')
		;


	#점포주문번호 전송대상
	DECLARE cursorPurchaseStoreInfo CURSOR FOR
		SELECT psi.purchase_order_no					AS purchase_order_no
		     , psi.purchase_store_info_no				AS purchase_store_info_no
			 , LPAD(psi.origin_store_id, 4, '0')       	AS store_id
			 , LPAD(psi.pos_no, 6, '0')                	AS pos_no
			 , DATE_FORMAT(psi.trade_dt, '%Y%m%d')      AS trade_dt
			 , LPAD(psi.trade_no, 4, '0')              	AS trade_no
		FROM purchase_order po
			INNER JOIN purchase_store_info psi ON po.purchase_order_no = psi.purchase_order_no
		WHERE po.pine_if_yn = 'I'
		;

	DECLARE CONTINUE HANDLER FOR NOT FOUND          ## 루프 핸들러
		SET vDone = TRUE;

	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION       ## SQLEXCEPTION 핸들러
		BEGIN
            GET DIAGNOSTICS CONDITION 1 vSqlState = RETURNED_SQLSTATE, vSqlErrorNo = MYSQL_ERRNO, vSqlMessage = MESSAGE_TEXT;
            SET vErrorNo =  -1;
        END;

    ## tx_isolation변경
    SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;

	## 배치로그 시작
	CALL sp_lgw_batch_history(vProcedureNm, 'START', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
	SET vLgwBatchNo = @outLgwBatchNo;

	PURCHASE_ORDER:BEGIN
		## 1. PINE 인터페이스 대상 데이터 인터페이스여부 업데이트
		## 커서 시작 : cursorPurchaseOrder
		OPEN cursorPurchaseOrder;
			SELECT FOUND_ROWS() INTO vTargetCount;

			## 인터페이스 대상이 없을 경우 프로시저 종료
			IF vTargetCount = 0 THEN
				## log insert - 종료: 대상없음
				CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwBatchNo, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
                SET vDone = FALSE;
				LEAVE PURCHASE_ORDER;
			END IF;

			## 작업대상 데이터 업데이트 (pine_if_yn : N(처리대기) -> I(처리중))
			## 2020-07-31 comment : 처리 실패(E) 및 비대상(주문유형 일반/대체 외)에 대한 처리방안 필요함.
			read_po_loop: LOOP

				FETCH cursorPurchaseOrder INTO vPurchaseOrderNo;

				IF NOT vDone THEN
					UPDATE purchase_order
					SET pine_if_yn = 'I'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'N';
				END IF;

				IF vDone THEN
					LEAVE read_po_loop;
				END IF;

			END LOOP read_po_loop;

			SET vDone = FALSE;
		CLOSE cursorPurchaseOrder;
	END PURCHASE_ORDER;


	## 2. PINE 인터페이스 대상 데이터 적재
	## 커서 시작 : cursorPurchaseStoreInfo
	OPEN cursorPurchaseStoreInfo;
		SELECT FOUND_ROWS() INTO vTargetCount;
		## 프로시저 시작 로그
		-- CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, 'purchaseStoreInfo START', NULL, NULL, NULL, NULL, NULL);
		read_psi_loop: REPEAT

		FETCH cursorPurchaseStoreInfo INTO vPurchaseOrderNo, vPurchaseStoreInfoNo, vStoreId, vPosNo, vTradeDt, vTradeNo;

		SET vErrorNo = 0;
		SET vCheckRowCnt = 0;
		SET vLogPurchaseStoreInfo = CONCAT('[주문번호]:',vPurchaseOrderNo,'[점포주문번호]:',vPurchaseStoreInfoNo);

			IF NOT vDone THEN
				SET vTotalCount = vTotalCount + 1;

				## 프로시저 작업대상 처리시작
				START TRANSACTION;

				SET @vItemSeq = 0;
				## 2-1) TD상품 적재
				INSERT INTO rf_trade_item (
				       ord_no
				     , good_id
					 , store_id
					 , pos_no
					 , trade_date
					 , trade_no
					 , item_seq
					 , print_item_name
					 , delivery_dt
					 , shift_id
					 , slot_id
					 , link_fraction
					 , trade_fraction
					 , input_fraction
					 , item_fraction
					 , plu_fraction
					 , plu_search_fraction
					 , event_fraction
					 , plu_code
					 , source_code
					 , item_qty
					 , item_cost
					 , item_common_cost
					 , modify_cost
					 , normal_cost
					 , special_rate
					 , special_cost
					 , dc_rate
					 , dc_cost
					 , sub_dc_rate
					 , sub_dc_cost
					 , link_cost
					 , link_code
					 , vender_code
					 , uom_code
					 , weight_uom
					 , tax_amount
					 , tax_fraction
					 , tax_rate
					 , rental_flag
					 , promo_no
					 , promo_detail_no
					 , promo_type
					 , promo_dc_type
					 , promo_dc_amount
					 , coupon_type
					 , coupon_dc_amount
					 , coupon_apply_qty
					 , mix_match_type
					 , multi_unit_qty
					 , multi_unit_price
					 , reason_code
					 , sum_result_amount
					 , card_dc_amount
					 , card_apply_qty
					 , emp_dc_amount
					 , mall_style_type
					 , fc_good_yn
					 , reg_dt
				)
				SELECT /* RF_TRADE_ITEM 대상조회 - TD상품 */
				       psi.purchase_store_info_no              AS ORD_NO              /*주문번호*/
				     , oi.item_no                              AS GOOD_ID             /*상품번호*/
                     , vStoreId									AS STORE_ID           /*점포ID*/
				     , vPosNo									AS POS_NO              /*POS번호*/
				     , vTradeDt									AS TRADE_DATE          /*주문일자(YYYYMMDD)*/
				     , vtradeNo         						AS TRADE_NO            /*영수증번호*/
				     , @vItemSeq := @vItemSeq+1                AS ITEM_SEQ            /*상품순번*/ -- 동일 purchaseOrderNo 기준 item 순번
				     , LEFT(TRIM(oi.item_nm1), 20)             AS PRINT_ITEM_NAME     /*상품명칭*/
				     , psi.ship_dt                             AS DELIVERY_DT         /*배송요청일*/
				     , psi.shift_id                            AS SHIFT_ID            /*주문쉬프트ID*/
				     , psi.slot_id                             AS SLOT_ID             /*주문슬롯ID*/
				     , '0'                                     AS LINK_FRACTION       /*공병구분*/
				     , '0'                                     AS TRADE_FRACTION      /*거래구분*/
				     , '1'                                     AS INPUT_FRACTION      /*등록구분*/
				     , '0'                                     AS ITEM_FRACTION       /*상품구분*/
				     , '0'                                     AS PLU_FRACTION        /*상품종류*/
				     , '2'                                     AS PLU_SEARCH_FRACTION /*단품조회구분*/
				     , oi.promo_apply_yn                       AS EVENT_FRACTION      /*행사구분(Y:적용,N:미적용)*/ -- RMS 행사 적용시 'Y'
				     , CONCAT('2000',oi.item_no)               AS PLU_CODE            /*단품코드*/
				     , oi.source_cd                            AS SOURCE_CODE         /*소스코드*/
				     , oi.item_qty                             AS ITEM_QTY            /*수량(TD=item_qty,DS=1)*/
				     , oi.item_price                           AS ITEM_COST           /*단가(TD=item_price,DS=SUM(price * qty)-SUM(discount_amt))*/
				     , oi.item_price                           AS ITEM_COMMON_COST    /*구단가*/ -- 추가 확인중
				     , 0                                       AS MODIFY_COST         /*매가변경금액(TD=NULL,DS=ITEM_COST)*/ -- 확인: null, 0
				     , oi.unit_retail                          AS normal_cost         /*정상단가*/ -- item_loc.unit_retail = ITEM_COST
				     , 0                                       AS SPECIAL_RATE        /*할인특매율*/
				     , 0                                       AS SPECIAL_COST        /*할인특매금액*/
				     , FLOOR((oi.item_discount_amt
							  + oi.item_coupon_amt
				              + oi.promo_discount_amt
				              + oi.emp_discount_amt
                              + oi.card_item_discount_amt)
				             / (oi.item_price * oi.item_qty)
				             * 100)                            AS DC_RATE             /*에누리율*/ -- (에누리금액/(단가*수량))*100, 소수점버림
				     , oi.item_discount_amt
					   + oi.item_coupon_amt
				       + oi.promo_discount_amt
				       + oi.emp_discount_amt
                       + oi.card_item_discount_amt             AS DC_COST             /*에누리금액*/ -- 할인에 대한 추가검토 필요.
				     , 0                                       AS SUB_DC_RATE
				     , 0                                       AS SUB_DC_COST
				     , 0                                       AS LINK_COST           /*공병단가*/
				     , NULL                                    AS LINK_CODE           /*공병코드*/
				     , oi.supplier                             AS VENDER_CODE         /*거래처코드*/
				     , CASE TRIM(oi.sale_uom)
				       WHEN 'EA' THEN '01'
				       WHEN ''   THEN '01'
				       ELSE '02'
				       END                                     AS UOM_CODE            /*UOM Code*/
				     , CASE TRIM(oi.sale_uom)
				       WHEN 'EA' THEN '1'
				       WHEN 'HG' THEN '100'
				       WHEN 'KG' THEN '1000'
				       WHEN ''   THEN '1000'
				       ELSE '1'
				       END                                     AS WEIGHT_UOM          /*중량단위*/
				     , IFNULL(oi.tax_vat_amt, 0)               AS TAX_AMOUNT          /*과세금액*/ -- 상품:order_item.tax_vat_amt
				     , CASE oi.tax_yn
				       WHEN 'Y' THEN '0'
				       WHEN 'Z' THEN '1'
				       WHEN 'N' THEN '2'
				       ELSE '2'
				       END                                     AS TAX_FRACTION        /*과세구분(0:과세,1:영세,2:면세)*/
				     , CASE oi.tax_yn
				       WHEN 'Y' THEN 1000
				       ELSE 0
				       END                                     AS TAX_RATE            /*과세율*/
				     , NULL                                    AS RENTAL_FLAG         /*임대구분*/
				     , oi.promo_no                             AS PROMO_NO            /*프로모션번호*/ -- simple 일때도 입력!
				     , oi.promo_detail_no                      AS PROMO_DETAIL_NO     /*프로모션상세번호*/
				     , CASE (SELECT promo_type
				             FROM order_promo
				             WHERE purchase_order_no = po.purchase_order_no
				             AND promo_no = oi.promo_no
				             LIMIT 1)
				       WHEN 'MIX'   THEN '2'
				       WHEN 'THRES' THEN '3'
				       ELSE ''
				       END                                     AS PROMO_TYPE          /*프로모션타입*/
				     , CASE (SELECT discount_type
				             FROM order_promo
				             WHERE purchase_order_no = po.purchase_order_no
				             AND promo_no = oi.promo_no
				             AND promo_type <> 'SIMP'
				             LIMIT 1)
				       WHEN '0' THEN '1'
				       WHEN '1' THEN '2'
				       WHEN '2' THEN '3'
				       ELSE ''
				       END 	                                   AS PROMO_DC_TYPE       /*프로모션DC타입*/
				     , IFNULL(oi.promo_discount_amt,0)         AS PROMO_DC_AMOUNT     /*프로모션DC금액*/
				       -- COUPON_TYPE : order_discount.discount_kind 값을 기준으로 쿠폰타입 정의할지 확인.
                     , CASE WHEN oi.card_item_discount_amt > 0 AND oi.emp_discount_amt > 0 THEN '6'
							WHEN oi.card_item_discount_amt > 0 THEN '5'
                            WHEN (oi.item_discount_amt > 0 OR oi.item_coupon_amt > 0) THEN '1'
                            WHEN oi.emp_discount_amt > 0 THEN '4'
						END                                    	AS COUPON_TYPE         /*쿠폰타입*/ -- 추가확인중. 상품쿠폰&그룹상품쿠폰 사용 시 정의 필요.
				     , oi.item_discount_amt
                     + oi.item_coupon_amt						AS coupon_dc_amount    /*쿠폰DC금액*/ -- 상품별 쿠폰할인 금액
				     , (SELECT od.coupon_apply_qty
						FROM order_discount od
                        WHERE od.purchase_order_no = oi.purchase_order_no
                        AND		od.bundle_no = oi.bundle_no
                        AND		od.order_item_no = oi.order_item_no
                        AND		od.discount_kind in ('item_discount','item_coupon')
                        )                                      AS coupon_apply_qty    /*쿠폰적용수량*/ -- item_qty 중 쿠폰이 적용된 수량
				     , oi.mix_match_type                       AS mix_match_type      /*MIX_MATCH 타입*/ -- 상품TF와 확인중.
				     , 0                                       AS multi_unit_qty      /*MULTI_UNIT 단위수량*/
				     , 0                                       AS multi_unit_price    /*MULTI_UNIT 금액*/
				     , ''                                      AS reason_code         /*사유코드*/
				     , oi.order_price
				       - oi.item_discount_amt
                       - oi.item_coupon_amt
				       - oi.promo_discount_amt
				       - oi.emp_discount_amt
                       - oi.card_item_discount_amt             AS sum_result_amount   /*상품최종금액*/ -- 단가 * 수량 - 총에누리금액
				     , oi.card_item_discount_amt               AS card_dc_amount     /*카드즉시할인금액*/
				     , CASE WHEN oi.card_item_discount_amt > 0 THEN oi.item_qty
						ELSE 0
						END 									AS card_apply_qty      /*카드즉시할인수량*/
				     , oi.emp_discount_amt                     	AS emp_dc_amount       /*직원할인금액*/
				     , '19010'                                 	AS mall_style_type     /*TD DS*/
				     , IFNULL(oi.fc_item_yn,'N')               	AS fc_good_yn          /*FC상품여부*/
				     , NOW()                                    AS reg_dt
				FROM purchase_store_info psi
					INNER JOIN purchase_order po ON psi.purchase_order_no = po.purchase_order_no
                    INNER JOIN bundle bun ON bun.purchase_order_no = psi.purchase_order_no AND bun.purchase_store_info_no = psi.purchase_store_info_no
					INNER JOIN order_item oi ON po.purchase_order_no = oi.purchase_order_no AND bun.bundle_no = oi.bundle_no AND oi.mall_type = 'TD'
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo
				ORDER BY psi.purchase_order_no , psi.purchase_store_info_no;


				## 대상건수 기록 : rf_trade_item (TD)
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
                    SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_item(TD) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


                ## 2-2) TD상품 배송비 적재
                # 다중배송비는 기본배송비 금액이 1,000원에 상품수량을 N개 하여 배송비금액이 산출 되게 함.
                INSERT INTO rf_trade_item (
				       ord_no
				     , good_id
					 , store_id
					 , pos_no
					 , trade_date
					 , trade_no
					 , item_seq
					 , print_item_name
					 , delivery_dt
					 , shift_id
					 , slot_id
					 , link_fraction
					 , trade_fraction
					 , input_fraction
					 , item_fraction
					 , plu_fraction
					 , plu_search_fraction
					 , event_fraction
					 , plu_code
					 , source_code
					 , item_qty
					 , item_cost
					 , item_common_cost
					 , modify_cost
					 , normal_cost
					 , special_rate
					 , special_cost
					 , dc_rate
					 , dc_cost
					 , sub_dc_rate
					 , sub_dc_cost
					 , link_cost
					 , link_code
					 , vender_code
					 , uom_code
					 , weight_uom
					 , tax_amount
					 , tax_fraction
					 , tax_rate
					 , rental_flag
					 , promo_no
					 , promo_detail_no
					 , promo_type
					 , promo_dc_type
					 , promo_dc_amount
					 , coupon_type
					 , coupon_dc_amount
					 , coupon_apply_qty
					 , mix_match_type
					 , multi_unit_qty
					 , multi_unit_price
					 , reason_code
					 , sum_result_amount
					 , card_dc_amount
					 , card_apply_qty
					 , emp_dc_amount
					 , mall_style_type
					 , fc_good_yn
					 , reg_dt
				)
				SELECT /* RF_TRADE_ITEM 대상조회 - 배송비 */
				       psi.purchase_store_info_no              							AS ord_no              /*주문번호*/
				     , bun.ship_price_item_no                    						AS good_id             /*상품번호*/
				     , vStoreId															AS store_id           /*점포ID*/
				     , vPosNo															AS pos_no              /*POS번호*/
				     , vTradeDt															AS trade_date          /*주문일자(YYYYMMDD)*/
				     , vtradeNo         												AS trade_no            /*영수증번호*/
                     , @vItemSeq := @vItemSeq+1                							AS item_seq            /*상품순번*/ -- 동일 purchaseOrderNo 기준 item 순번
				     , (SELECT LEFT(TRIM(imc.mc_nm), 20)
				        FROM dms.itm_mng_code imc
				        WHERE imc.gmc_cd = 'ship_price_item'
				        AND imc.ref_1 = bun.ship_price_item_no)  						AS print_item_name     /*상품명칭*/
				     , psi.ship_dt                             							AS delivery_dt         /*배송요청일*/
				     , psi.shift_id                            							AS shift_id            /*주문쉬프트ID*/
				     , psi.slot_id                             							AS slot_id             /*주문슬롯ID*/
				     , '0'                                     							AS LINK_FRACTION       /*공병구분*/
				     , '0'                                     							AS TRADE_FRACTION      /*거래구분*/
				     , '1'                                     							AS INPUT_FRACTION      /*등록구분*/
				     , '0'                                    		 					AS ITEM_FRACTION       /*상품구분*/
				     , '0'                                     							AS PLU_FRACTION        /*상품종류*/
				     , '2'                                     							AS PLU_SEARCH_FRACTION /*단품조회구분*/
				     , 'N'                                     							AS EVENT_FRACTION      /*행사구분(Y:적용,N:미적용)*/ -- RMS 행사는 simp,M&M,Thres 뿐
				     , CONCAT('2000',bun.ship_price_item_no)     						AS PLU_CODE            /*단품코드*/
				     , CONCAT('2000',bun.ship_price_item_no)     						AS SOURCE_CODE         /*소스코드*/
				     , CASE WHEN bun.multi_order_yn = 'Y' THEN (bun.ship_price + bun.island_ship_price + bun.ship_discount_price) / 1000
							ELSE 1
						END                                      						AS ITEM_QTY            /*수량(배송비=1)*/
				     , CASE WHEN bun.multi_order_yn = 'Y' THEN 1000
						ELSE ( bun.ship_price + bun.island_ship_price + bun.ship_discount_price)
                        END 															AS ITEM_COST           /*단가(할인후 배송비 + 할인후 도서산간배송비 + 배송비할인금액)*/
					 ,  CASE WHEN bun.multi_order_yn = 'Y' THEN 1000
						ELSE ( bun.ship_price + bun.island_ship_price + bun.ship_discount_price)
						END																AS ITEM_COMMN_COST    /*구단가*/ -- (=ITEM_COST) 추가 확인
				     , 0                                       							AS MODIFY_COST         /*매가변경금액(배송비=0)*/
				     ,  CASE WHEN bun.multi_order_yn = 'Y' THEN 1000
						ELSE ( bun.ship_price + bun.island_ship_price + bun.ship_discount_price)
                        END                 											AS NORMAL_COST         /*정상단가*/ -- (=ITEM_COST)
				     , 0                                       							AS SPECIAL_RATE        /*할인특매율*/
				     , 0                                       							AS SPECIAL_COST        /*할인특매금액*/
				     , FLOOR(bun.ship_discount_price
				             / (bun.ship_price
				                + bun.island_ship_price
				                + bun.ship_discount_price)
				             * 100)                            		AS DC_RATE             /*에누리율*/ -- (배송비할인금액/(전체배송비))*100, 소수점버림
				     , bun.ship_discount_price                   	AS DC_COST             /*에누리금액*/ -- 배송비할인금액
				     , 0                                       	AS SUB_DC_RATE
				     , 0                                       	AS SUB_DC_COST
				     , 0                                       	AS LINK_COST           /*공병단가*/
				     , NULL                                    	AS LINK_CODE           /*공병코드*/
				     , '199999'                                	AS VENDER_CODE         /*거래처코드*/ -- 배송비 거래처코드 확인필요.
				     , '01'                                    	AS UOM_CODE            /*UOM Code*/
				     , '1'                                     	AS WEIGHT_UOM          /*중량단위*/
				     , bun.tax_vat_amt                          AS TAX_AMOUNT          /*과세금액*/ -- 배송비:bundle.tax_vat_amt
				     , '0'                                     	AS TAX_FRACTION        /*과세구분(0:과세,1:영세,2:면세)*/
				     , 1000                                 	AS TAX_RATE            /*과세율*/
				     , NULL                                    	AS RENTAL_FLAG         /*임대구분*/
				     , NULL                                    	AS PROMO_NO            /*프로모션번호*/
				     , NULL                                    	AS PROMO_DETAIL_NO     /*프로모션상세번호*/
				     , NULL                                    	AS PROMO_TYPE          /*프로모션타입*/
				     , NULL                                    	AS PROMO_DC_TYPE       /*프로모션DC타입*/
				     , 0                                       	AS PROMO_DC_AMOUNT     /*프로모션DC금액*/
				       -- COUPON_TYPE : order_discount.discount_kind 값을 기준으로 쿠폰타입 정의할지 확인.
				     , CASE WHEN bun.ship_discount_price > 0 THEN '1'
							ELSE NULL
						END 									AS COUPON_TYPE         /*쿠폰타입*/ -- 배송비 쿠폰 TYPE 1로 세팅
				     , bun.ship_discount_price	                AS COUPON_DC_AMOUNT    /*쿠폰DC금액*/ -- 상품별 쿠폰할인 금액
				     , CASE WHEN bun.ship_discount_price > 0 THEN '1'
							ELSE '0'
						END                                     AS COUPON_APPLY_QTY    /*쿠폰적용수량*/ -- item_qty 중 쿠폰이 적용된 수량
				     , NULL                                    AS MIX_MATCH_TYPE      /*MIX_MATCH 타입*/
				     , 0                                       AS MULTI_UNIT_QTY      /*MULTI_UNIT 단위수량*/
				     , 0                                       AS MULTI_UNIT_PRICE    /*MULTI_UNIT 금액*/
				     , ''                                      AS REASON_CODE         /*사유코드*/
				     , bun.ship_price + bun.island_ship_price      AS SUM_RESULT_AMOUNT   /*상품최종금액(할인제외금액)*/
				     , 0                                       AS CARD_DC_AMOUNTT     /*카드즉시할인금액*/
				     , 0                                       AS CARD_APPLY_QTY      /*카드즉시할인수량*/
				     , 0                                       AS EMP_DC_AMOUNT       /*직원할인금액*/
				     , '19010'                                 AS MALL_STYLE_TYPE     /*TD DS*/
				     , 'N'                                     AS FC_GOOD_YN          /*FC상품여부*/
				     , NOW()                                   AS REG_DT
				FROM purchase_store_info psi
					INNER JOIN purchase_order po ON psi.purchase_order_no = po.purchase_order_no
					INNER JOIN bundle bun ON psi.purchase_store_info_no = bun.purchase_store_info_no
					                    AND (bun.ship_price + bun.island_ship_price + bun.ship_discount_price) > 0
                                        AND bun.ship_method in ('TD_DRCT','TD_DLV','TD_PICK','TD_QUICK')
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo;

                ## 대상건수 기록 : rf_trade_item (TD배송비)
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_item (TD-배송비) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				/*
                 * 2-3) : DS상품 적재
                 * DS상품은 상품 + 배송비 전체 합산해서 한개 아이템으로 전송
                 * 적용된 할인금액을 제외하고 합산한다.
                 */
				INSERT INTO rf_trade_item (
				       ord_no
				     , good_id
					 , store_id
					 , pos_no
					 , trade_date
					 , trade_no
					 , item_seq
					 , print_item_name
					 , delivery_dt
					 , shift_id
					 , slot_id
					 , link_fraction
					 , trade_fraction
					 , input_fraction
					 , item_fraction
					 , plu_fraction
					 , plu_search_fraction
					 , event_fraction
					 , plu_code
					 , source_code
					 , item_qty
					 , item_cost
					 , item_common_cost
					 , modify_cost
					 , normal_cost
					 , special_rate
					 , special_cost
					 , dc_rate
					 , dc_cost
					 , sub_dc_rate
					 , sub_dc_cost
					 , link_cost
					 , link_code
					 , vender_code
					 , uom_code
					 , weight_uom
					 , tax_amount
					 , tax_fraction
					 , tax_rate
					 , rental_flag
					 , promo_no
					 , promo_detail_no
					 , promo_type
					 , promo_dc_type
					 , promo_dc_amount
					 , coupon_type
					 , coupon_dc_amount
					 , coupon_apply_qty
					 , mix_match_type
					 , multi_unit_qty
					 , multi_unit_price
					 , reason_code
					 , sum_result_amount
					 , card_dc_amount
					 , card_apply_qty
					 , emp_dc_amount
					 , mall_style_type
					 , fc_good_yn
					 , reg_dt
				)
				SELECT /* RF_TRADE_ITEM 대상조회 - DS상품 */
				       psi.purchase_store_info_no              						AS ORD_NO              /*주문번호*/
				     , '127399175'                             						AS GOOD_ID             /*상품번호*/ -- RMS대표상품번호, Refit용 별도 생성 필요???
                     , vStoreId														AS STORE_ID           /*점포ID*/
				     , vPosNo														AS POS_NO              /*POS번호*/
				     , vTradeDt														AS TRADE_DATE          /*주문일자(YYYYMMDD)*/
				     , vtradeNo         											AS TRADE_NO            /*영수증번호*/
				     , @vItemSeq := @vItemSeq+1                						AS ITEM_SEQ            /*상품순번*/ -- 동일 purchaseOrderNo 기준 item 순번
				     , 'GHS+ 판매선수금'                          						AS PRINT_ITEM_NAME     /*상품명칭*/
				     , NULL                                    						AS DELIVERY_DT         /*배송요청일*/
				     , '0000'                            							AS shift_id            /*주문쉬프트ID*/
				     , '0000'                            							AS slot_id             /*주문슬롯ID*/
				     , '0'                                     						AS LINK_FRACTION       /*공병구분*/
				     , '0'                                     						AS TRADE_FRACTION      /*거래구분*/
				     , '1'                                     						AS INPUT_FRACTION      /*등록구분*/
				     , '0'                                     						AS ITEM_FRACTION       /*상품구분*/
				     , '0'                                     						AS PLU_FRACTION        /*상품종류*/
				     , '2'                                     						AS PLU_SEARCH_FRACTION /*단품조회구분*/
				     , 'N'                                     						AS EVENT_FRACTION      /*행사구분(Y:적용,N:미적용)*/ -- RMS 행사 적용시 'Y'
				     , '2000127399175'                         						AS PLU_CODE            /*단품코드*/
				     , '2000127399175'                         						AS SOURCE_CODE         /*소스코드*/
				     , 1                                       						AS ITEM_QTY            /*수량(TD=item_qty,DS=1)*/
                     , (psi.tot_item_price + psi.tot_ship_price + psi.tot_island_ship_price
						- psi.tot_item_discount_amt - psi.tot_ship_discount_amt
                        - psi.tot_promo_discount_amt - psi.tot_emp_discount_amt
                        - psi.tot_card_discount_amt - psi.tot_cart_discount_amt
                        - IFNULL(psi.tot_add_td_discount_amt,0) - IFNULL(psi.tot_add_ds_discount_amt,0))			AS item_cost
                     , (psi.tot_item_price + psi.tot_ship_price + psi.tot_island_ship_price
						- psi.tot_item_discount_amt - psi.tot_ship_discount_amt
                        - psi.tot_promo_discount_amt - psi.tot_emp_discount_amt
                        - psi.tot_card_discount_amt - psi.tot_cart_discount_amt
                        - IFNULL(psi.tot_add_td_discount_amt,0) - IFNULL(psi.tot_add_ds_discount_amt,0))			AS item_common_cost
				     , (psi.tot_item_price + psi.tot_ship_price + psi.tot_island_ship_price
						- psi.tot_item_discount_amt - psi.tot_ship_discount_amt
                        - psi.tot_promo_discount_amt - psi.tot_emp_discount_amt
                        - psi.tot_card_discount_amt - psi.tot_cart_discount_amt
                        - IFNULL(psi.tot_add_td_discount_amt,0) - IFNULL(psi.tot_add_ds_discount_amt,0))			AS modify_cost         /*매가변경금액(TD=NULL,DS=ITEM_COST)*/
				     , '100'														AS normal_cost         /* DS 상품 '127399175'는 100으로 세팅되어 있음. 기준상품이 변경되면 가격도 변경해야함. */
				     , 0                                       						AS SPECIAL_RATE        /*할인특매율*/
				     , 0                                       						AS SPECIAL_COST        /*할인특매금액*/
				     , 0                                       						AS DC_RATE             /*에누리율*/ -- DS는 단가에 미리 계산하므로 0
				     , 0                                       						AS DC_COST             /*에누리금액*/ -- DS는 단가에 미리 계산하므로 0
				     , 0                                       						AS SUB_DC_RATE
				     , 0                                       						AS SUB_DC_COST
				     , 0                                       						AS LINK_COST           /*공병단가*/
				     , NULL                                    						AS LINK_CODE           /*공병코드*/
				     , ''                                     	 					AS VENDER_CODE         /*거래처코드*/ -- (06/16: MAX값 처리할지 검토중.)
				     , '01'                                     					AS UOM_CODE            /*UOM Code DS상품 01 입력*/
				     , '1'                                							AS WEIGHT_UOM          /*중량단위 DS상품 1 입력 */
				     , 0                                       						AS TAX_AMOUNT          /*과세금액*/ -- 면세?과세? sum(oi.tax+b.tax)
				     , '2'                                     						AS TAX_FRACTION        /*과세구분(0:과세,1:영세,2:면세)*/ -- (06/16: MAX값 처리할지 검토중.)
				     , 0                                       						AS TAX_RATE            /*과세율*/ -- (06/16: MAX값 처리할지 검토중.)
				     , NULL                                    						AS RENTAL_FLAG         /*임대구분*/
				     , NULL                                    						AS promo_no            /*프로모션번호*/
				     , NULL                                     					AS promo_detail_no     /*프로모션상세번호*/
				     , ''                                      						AS promo_type          /*프로모션타입*/
				     , ''                                      						AS promo_dc_type       /*프로모션DC타입*/
				     , 0                                       						AS promo_dc_amount     /*프로모션DC금액*/
				     , NULL                                    						AS coupon_type         /*쿠폰타입*/ -- 추가확인중. 상품쿠폰&그룹상품쿠폰 사용 시 정의 필요.
				     , 0                                       						AS coupon_dc_amount    /*쿠폰DC금액*/ -- 설계중. 상품별 쿠폰할인 금액
				     , 0                                       						AS coupon_apply_qty    /*쿠폰적용수량*/ -- 설계중. item_qty 중 쿠폰이 적용된 수량
				     , ''                                      						AS mix_match_type      /*MIX_MATCH 타입*/
				     , 0                                       						AS multi_unit_qty      /*MULTI_UNIT 단위수량*/
				     , 0                                       						AS multi_unit_price    /*MULTI_UNIT 금액*/
				     , ''                                      						AS reason_code         /*사유코드*/
				     , (psi.tot_item_price + psi.tot_ship_price + psi.tot_island_ship_price
						- psi.tot_item_discount_amt - psi.tot_ship_discount_amt
                        - psi.tot_promo_discount_amt - psi.tot_emp_discount_amt
                        - psi.tot_card_discount_amt - psi.tot_cart_discount_amt
                        - IFNULL(psi.tot_add_td_discount_amt,0) - IFNULL(psi.tot_add_ds_discount_amt,0))			AS sum_result_amount   /*상품최종금액*/
				     , 0          													AS card_dc_amount      /*카드즉시할인금액*/
				     , 0 															AS card_apply_qty      /*카드즉시할인수량*/
				     , 0                                       						AS emp_dc_amount       /*직원할인금액*/ -- DS 임직원할인 정책결정 후 변경 예정.
				     , '19020'                                 						AS mall_style_type    	/*DS*/
				     , 'N'                                     						AS fc_good_yn          /*FC상품여부*/
				     , NOW()                                                        AS reg_dt
				FROM purchase_store_info psi
					INNER JOIN purchase_order po ON psi.purchase_order_no = po.purchase_order_no
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo
                AND psi.ship_method in ('DS_DLV','DS_QUICK','DS_PICK','DS_DRCT','DS_POST')
				;

				## 대상건수 기록 : rf_trade_item (DS)
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;


				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_item (DS) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;


                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## rf_trade_ticket 적재 - 장바구니쿠폰
				SET @vTicketSeq = 0;
				INSERT INTO rf_trade_ticket (
				       ord_no
				     , ticket_seq
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , ftype_code
				     , ticket_fraction
				     , ticket_company_code
				     , ticket_sign_amount
				     , ticket_sale_amount
				     , ticket_change_amount
				     , ticket_no
				     , ticket_cnt
				     , ticket_kind
				     , approval_no
				     , reg_dt
				)
				SELECT /* RF_TRADE_TICKET 대상조회 (장바구니쿠폰) */
				       psi.purchase_store_info_no                  			AS ord_no               /*주문번호*/
				     , @vTicketSeq := @vTicketSeq+1                			AS ticket_seq           /*상품권순번*/
				     , vStoreId												AS store_id          /*점포ID*/
				     , vPosNo												AS pos_no              /*POS번호*/
				     , vTradeDt												AS trade_date          /*주문일자(YYYYMMDD)*/
				     , vtradeNo         									AS trade_no            /*영수증번호*/
				     , '1'                                         			AS ftype_code           /*거래구분(1:통상,2:반품)*/
				     , '87'                                        			AS ticket_fraction      /*상품권구분(81:자사금권,87:타사금권)*/
				     , '52'                                        			AS ticket_company_code  /*상품권회사코드(52:사은쿠폰,60:자사상품권)*/
				     , 1                             						AS ticket_sign_amount   /*상품권액면금액-쿠폰최소적용금액 : 장바구니쿠폰 번들단위 TRAN 전송으로 최소구매금액 1로 세팅함*/
				     , psi.tot_cart_discount_amt                    		AS ticket_sale_amount   /*상품권매출금액-장바구니쿠폰사용금액*/
				     , psi.tot_item_price
				       + psi.tot_ship_price
				       + psi.tot_island_ship_price
				       - psi.tot_item_discount_amt
				       - psi.tot_promo_discount_amt
				       - psi.tot_emp_discount_amt
				       - psi.tot_card_discount_amt
                       - psi.tot_ship_discount_amt							AS ticket_change_amount	/* 상품금액 + 배송비금액 - 상품할인 - 행사할인 - 임직원할인 - 카드상품할인 - 배송비할인 금액임 */
				     , od.barcode                                  			AS ticket_no            /*상품권번호*/
				     , 1                                           			AS ticket_cnt           /*상품권수량*/
				     , '00'                                        			AS ticket_kind          /*상품권권종(00:사은쿠폰,##:그외)*/
				     , ''                                          			AS approval_no          /*승인번호*/
				     , NOW()                                                AS reg_dt
				FROM purchase_store_info psi
					INNER JOIN order_discount od ON psi.purchase_order_no = od.purchase_order_no AND od.discount_kind = 'cart_coupon' AND psi.store_id = od.store_id
                    INNER JOIN bundle bun ON psi.purchase_store_info_no = bun.purchase_store_info_no AND od.bundle_no = bun.bundle_no
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo;

				## 대상건수 기록 : rf_trade_ticket
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_ticket (장바구니쿠폰) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;

   				## rf_trade_ticket 적재 - 추가중복쿠폰TD
				INSERT INTO rf_trade_ticket (
				       ord_no
				     , ticket_seq
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , ftype_code
				     , ticket_fraction
				     , ticket_company_code
				     , ticket_sign_amount
				     , ticket_sale_amount
				     , ticket_change_amount
				     , ticket_no
				     , ticket_cnt
				     , ticket_kind
				     , approval_no
				     , reg_dt
				)
				SELECT /* RF_TRADE_TICKET 대상조회 (추가중복쿠폰TD) */
				       psi.purchase_store_info_no                  			AS ord_no               /*주문번호*/
				     , @vTicketSeq := @vTicketSeq+1                			AS ticket_seq           /*상품권순번*/
				     , vStoreId												AS store_id          /*점포ID*/
				     , vPosNo												AS pos_no              /*POS번호*/
				     , vTradeDt												AS trade_date          /*주문일자(YYYYMMDD)*/
				     , vtradeNo         									AS trade_no            /*영수증번호*/
				     , '1'                                         			AS ftype_code           /*거래구분(1:통상,2:반품)*/
				     , '87'                                        			AS ticket_fraction      /*상품권구분(81:자사금권,87:타사금권)*/
				     , '52'                                        			AS ticket_company_code  /*상품권회사코드(52:사은쿠폰,60:자사상품권)*/
				     , 1                             						AS ticket_sign_amount   /*상품권액면금액-쿠폰최소적용금액 : 장바구니쿠폰 번들단위 TRAN 전송으로 최소구매금액 1로 세팅함*/
				     , psi.tot_add_td_discount_amt                    		AS ticket_sale_amount   /*상품권매출금액-추가중복쿠폰TD사용금액*/
				     , psi.tot_item_price
				       + psi.tot_ship_price
				       + psi.tot_island_ship_price
				       - psi.tot_item_discount_amt
				       - psi.tot_promo_discount_amt
				       - psi.tot_emp_discount_amt
				       - psi.tot_card_discount_amt
                       - psi.tot_ship_discount_amt							AS ticket_change_amount	/* 상품금액 + 배송비금액 - 상품할인 - 행사할인 - 임직원할인 - 카드상품할인 - 배송비할인 금액임 */
				     , od.barcode                                  			AS ticket_no            /*상품권번호*/
				     , 1                                           			AS ticket_cnt           /*상품권수량*/
				     , '00'                                        			AS ticket_kind          /*상품권권종(00:사은쿠폰,##:그외)*/
				     , ''                                          			AS approval_no          /*승인번호*/
				     , NOW()                                                AS reg_dt
				FROM purchase_store_info psi
					INNER JOIN order_discount od ON psi.purchase_order_no = od.purchase_order_no AND od.discount_kind = 'add_td_coupon' AND psi.store_id = od.store_id
                    INNER JOIN bundle bun ON psi.purchase_store_info_no = bun.purchase_store_info_no AND od.bundle_no = bun.bundle_no
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo;

				## 대상건수 기록 : rf_trade_ticket
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_ticket (추가중복쿠폰TD) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## rf_trade_ticket 적재 - 마일리지 사용금액 적재
				INSERT INTO rf_trade_ticket (
				       ord_no
				     , ticket_seq
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , ftype_code
				     , ticket_fraction
				     , ticket_company_code
				     , ticket_sign_amount
				     , ticket_sale_amount
				     , ticket_change_amount
				     , ticket_no
				     , ticket_cnt
				     , ticket_kind
				     , approval_no
				     , reg_dt
				)
				SELECT /* RF_TRADE_TICKET 대상조회 (마일리지 사용금액) */
				       psi.purchase_store_info_no                  	AS ord_no               /*주문번호*/
				     , @vTicketSeq := @vTicketSeq+1                	AS ticket_seq           /*상품권순번*/
				     , vStoreId										AS store_id          /*점포ID*/
				     , vPosNo										AS pos_no              /*POS번호*/
				     , vTradeDt										AS trade_date          /*주문일자(YYYYMMDD)*/
				     , vtradeNo         							AS trade_no            /*영수증번호*/
				     , '1'                                         	AS ftype_code           /*거래구분(1:통상,2:반품)*/
				     , '87'                                        	AS ticket_fraction      /*상품권구분(81:자사금권,87:타사금권)*/
				     , '52'                                        	AS ticket_company_code  /*상품권회사코드(52:사은쿠폰,60:자사상품권)*/
				     , 10                             				AS ticket_sign_amount   /* 마일리지 사용금액 최소구매금액 10으로 세팅함.*/
				     , psi.mileage_amt                    			AS ticket_sale_amount   /*상품권매출금액-장바구니쿠폰사용금액*/
                     , psi.tot_item_price
				       + psi.tot_ship_price
				       + psi.tot_island_ship_price
				       - psi.tot_item_discount_amt
				       - psi.tot_promo_discount_amt
				       - psi.tot_emp_discount_amt
				       - psi.tot_card_discount_amt
                       - psi.tot_ship_discount_amt
                       - IFNULL(psi.tot_add_ds_discount_amt,0) 		AS ticket_change_amount	/* 상품금액 + 배송비금액 - 상품할인 - 행사할인 - 임직원할인 - 카드상품할인 - 배송비할인 */
				     , '2651200027939'                              AS ticket_no           /* 마일리지 사용시 바코드 하드코딩 기존:2651170003384 => 2651200027939 변경 (~2050.12.31 까지만 가능함)*/
				     , 1                                            AS ticket_cnt           /*상품권수량*/
				     , '00'                                         AS ticket_kind          /*상품권권종(00:사은쿠폰,##:그외)*/
				     , ''                                           AS approval_no          /*승인번호*/
				     , NOW()                                        AS reg_dt
				FROM purchase_store_info psi
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo
                AND		psi.mileage_amt > 0;

				## 대상건수 기록 : rf_trade_ticket
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_ticket (마일리지) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## rf_trade_coupon 적재 - 쿠폰마스터:장바구니쿠폰
				INSERT INTO rf_trade_coupon (
				       ord_no
				     , coupon_code
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , coupon_name
				     , coupon_startdate
				     , coupon_enddate
				     , coupon_type
				     , coupon_value
				     , coupon_limit
				     , coupon_condition
				     , item_qty
				     , coupon_item_qty
				     , coupon_qty
				     , coupon_amount
				     , unit_id
				     , reg_dt
				)
				SELECT /* RF_TRADE_COUPON 대상조회 (쿠폰마스터 - 장바구니쿠폰) */
				       psi.purchase_store_info_no                      		AS ord_no
				     , od.barcode                                      		AS coupon_code
				     , vStoreId												AS store_id
				     , vPosNo												AS pos_no
				     , vTradeDt												AS trade_date
				     , vtradeNo         									AS trade_no
				     , LEFT(od.coupon_nm, 30)                         		AS coupon_name
				     , date_format(od.coupon_valid_start_dt, '%Y%m%d') 		AS coupon_startdate
				     , date_format(od.coupon_valid_end_dt, '%Y%m%d')   		AS coupon_enddate
				     , od.coupon_type                                     	AS coupon_type
				     , od.discount_amt                                 		AS coupon_value
				     , 0                                               		AS coupon_limit
				     , 0                                               		AS coupon_condition
				     , 0                                               		AS item_qty
				     , 0                                               		AS coupon_item_qty
				     , 1                                               		AS coupon_qty
				     , od.discount_amt                                 		AS coupon_amount
				     , od.dept_cd                              				AS unit_id
				     , NOW()                                                AS reg_dt
				FROM purchase_store_info psi
					INNER JOIN order_discount od ON psi.purchase_order_no = od.purchase_order_no AND psi.store_id = od.store_id AND od.discount_kind = 'cart_coupon'
                    INNER JOIN bundle bun ON psi.purchase_store_info_no = bun.purchase_store_info_no AND od.bundle_no = bun.bundle_no
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo;

				## 대상건수 기록 : rf_trade_coupon - cart_coupon
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_coupon (장바구니) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


   				## rf_trade_coupon 적재 - 쿠폰마스터:추가중복쿠폰TD
				INSERT INTO rf_trade_coupon (
				       ord_no
				     , coupon_code
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , coupon_name
				     , coupon_startdate
				     , coupon_enddate
				     , coupon_type
				     , coupon_value
				     , coupon_limit
				     , coupon_condition
				     , item_qty
				     , coupon_item_qty
				     , coupon_qty
				     , coupon_amount
				     , unit_id
				     , reg_dt
				)
				SELECT /* RF_TRADE_COUPON 대상조회 (쿠폰마스터 - 추가중복쿠폰TD) */
				       psi.purchase_store_info_no                      		AS ord_no
				     , od.barcode                                      		AS coupon_code
				     , vStoreId												AS store_id
				     , vPosNo												AS pos_no
				     , vTradeDt												AS trade_date
				     , vtradeNo         									AS trade_no
				     , LEFT(od.coupon_nm, 30)                         		AS coupon_name
				     , date_format(od.coupon_valid_start_dt, '%Y%m%d') 		AS coupon_startdate
				     , date_format(od.coupon_valid_end_dt, '%Y%m%d')   		AS coupon_enddate
				     , od.coupon_type                                     	AS coupon_type
				     , od.discount_amt                                 		AS coupon_value
				     , 0                                               		AS coupon_limit
				     , 0                                               		AS coupon_condition
				     , 0                                               		AS item_qty
				     , 0                                               		AS coupon_item_qty
				     , 1                                               		AS coupon_qty
				     , od.discount_amt                                 		AS coupon_amount
				     , od.dept_cd                              				AS unit_id
				     , NOW()                                                AS reg_dt
				FROM purchase_store_info psi
					INNER JOIN order_discount od ON psi.purchase_order_no = od.purchase_order_no AND psi.store_id = od.store_id AND od.discount_kind = 'add_td_coupon'
                    INNER JOIN bundle bun ON psi.purchase_store_info_no = bun.purchase_store_info_no AND od.bundle_no = bun.bundle_no
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo;

				## 대상건수 기록 : rf_trade_coupon - add_td_coupon
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_coupon (추가중복쿠폰TD) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;



				## rf_trade_coupon 적재 - 마일리지 사용금액
				INSERT INTO rf_trade_coupon (
				       ord_no
				     , coupon_code
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , coupon_name
				     , coupon_startdate
				     , coupon_enddate
				     , coupon_type
				     , coupon_value
				     , coupon_limit
				     , coupon_condition
				     , item_qty
				     , coupon_item_qty
				     , coupon_qty
				     , coupon_amount
				     , unit_id
				     , reg_dt
				)
				SELECT /* RF_TRADE_COUPON 대상조회 (쿠폰마스터 - 마일리지사용금액) */
				       psi.purchase_store_info_no                      	AS ord_no
				     , '2651200027939'                               	AS coupon_code
				     , vStoreId											AS store_id
				     , vPosNo											AS pos_no
				     , vTradeDt											AS trade_date
				     , vtradeNo         								AS trade_no
				     , '적립쿠폰 신규 대표 바코드'                         	AS coupon_name
				     , '20200905'										AS coupon_startdate
				     , '20501231'										AS coupon_enddate
				     , '51'                                         	AS coupon_type
				     , psi.mileage_amt                                 	AS coupon_value
				     , 0                                               	AS coupon_limit
				     , 0                                               	AS coupon_condition
				     , 0                                               	AS item_qty
				     , 0                                               	AS coupon_item_qty
				     , 1                                               	AS coupon_qty
				     , psi.mileage_amt                                 	AS coupon_amount
				     , '31'                              				AS unit_id
				     , NOW()                                            AS reg_dt
				FROM purchase_store_info psi
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo
                AND		psi.mileage_amt > 0;

				## 대상건수 기록 : rf_trade_coupon - cart_coupon
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_coupon (마일리지) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


                ## 2-8) rf_trade_coupon 적재 - 쿠폰마스터: 상품할인 / 상품쿠폰
                ## 여러상품에 쿠폰이 N개 적용되도, 동일쿠폰정보는 한개만 적재한다.
                ## 할인금액등은 SUM 한다.
                ## 3. DS 상품쿠폰은 미전송한다.
				INSERT INTO rf_trade_coupon (
				       ord_no
				     , coupon_code
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , coupon_name
				     , coupon_startdate
				     , coupon_enddate
				     , coupon_type
				     , coupon_value
				     , coupon_limit
				     , coupon_condition
				     , item_qty
				     , coupon_item_qty
				     , coupon_qty
				     , coupon_amount
				     , unit_id
				     , reg_dt
				)
				SELECT /* RF_TRADE_COUPON 대상조회 (쿠폰마스터 - 상품할인/상품쿠폰/배송비쿠폰) */
				       psi.purchase_store_info_no                					AS ord_no
				     , od.barcode		                                      		AS coupon_code
   				     , vStoreId														AS store_id          /*점포ID*/
				     , vPosNo														AS pos_no              /*POS번호*/
				     , vTradeDt														AS trade_date          /*주문일자(YYYYMMDD)*/
				     , vtradeNo         											AS trade_no            /*영수증번호*/
				     , MAX(LEFT(od.coupon_nm, 30))                         			AS coupon_name
				     , MAX(date_format(od.coupon_valid_start_dt, '%Y%m%d')) 		AS coupon_startdate
				     , MAX(date_format(od.coupon_valid_end_dt, '%Y%m%d'))   		AS coupon_enddate
                     , MAX(CASE WHEN od.discount_type = '1' THEN '03'
							ELSE '01'
						END)                                        				AS coupon_type		-- 정률(1) : 03, 정액(2) : 01
				     , SUM(od.discount_amt)                                 		AS coupon_value
				     , 999                                             				AS coupon_limit
				     , 0                                               				AS coupon_condition
				     , SUM(od.item_qty)                                      		AS item_qty
				     , SUM(od.coupon_apply_qty)                              		AS coupon_item_qty
				     , 1                                               				AS coupon_qty
				     , SUM(od.discount_amt)                         				AS coupon_amount
				     , MAX(od.dept_cd)                              				AS unit_id
				     , NOW()                                                        AS reg_dt
				FROM purchase_store_info psi
					INNER JOIN bundle bun ON psi.purchase_store_info_no = bun.purchase_store_info_no
					INNER JOIN order_discount od ON psi.purchase_order_no = od.purchase_order_no AND bun.bundle_no = od.bundle_no AND psi.store_id = od.store_id AND od.discount_kind in('item_discount','item_coupon')
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo
                AND bun.ship_method in ('TD_DRCT','TD_PICK','TD_DLV','TD_QUICK')
                GROUP BY psi.purchase_store_info_no, od.barcode;

				## 대상건수 기록 : rf_trade_coupon - item_discount/item_coupon
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_coupon (상품할인,상품쿠폰) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;



                ## 2-9) rf_trade_coupon 적재 - 쿠폰마스터: 배송비쿠폰
                ## 1. 여러상품에 쿠폰이 N개 적용되도, 동일쿠폰정보는 한개만 적재한다.
                ## 2. 할인금액등은 SUM 한다.
                ## 3. DS 배송비 할인은 미전송한다.
				INSERT INTO rf_trade_coupon (
				       ord_no
				     , coupon_code
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , coupon_name
				     , coupon_startdate
				     , coupon_enddate
				     , coupon_type
				     , coupon_value
				     , coupon_limit
				     , coupon_condition
				     , item_qty
				     , coupon_item_qty
				     , coupon_qty
				     , coupon_amount
				     , unit_id
				     , reg_dt
				)
				SELECT /* RF_TRADE_COUPON 대상조회 (쿠폰마스터 - 배송비쿠폰) */
				       psi.purchase_store_info_no                					AS ord_no
				     , od.barcode		                                      		AS coupon_code
   				     , vStoreId														AS store_id          /*점포ID*/
				     , vPosNo														AS pos_no              /*POS번호*/
				     , vTradeDt														AS trade_date          /*주문일자(YYYYMMDD)*/
				     , vtradeNo         											AS trade_no            /*영수증번호*/
				     , MAX(LEFT(od.coupon_nm, 30))                         		    AS coupon_name
				     , MAX(date_format(od.coupon_valid_start_dt, '%Y%m%d')) 		AS coupon_startdate
				     , MAX(date_format(od.coupon_valid_end_dt, '%Y%m%d'))   		AS coupon_enddate
                     , '01'                                        					AS coupon_type		-- 정률(1) : 03, 정액(2) : 01
				     , SUM(od.discount_amt)                                 		AS coupon_value
				     , 999                                             				AS coupon_limit
				     , 0                                               				AS coupon_condition
				     , 1                                      						AS item_qty
				     , 1                              								AS coupon_item_qty
				     , 1                                               				AS coupon_qty
				     , SUM(od.discount_amt)                         				AS coupon_amount
				     , MAX(od.dept_cd)                              				AS unit_id
				     , NOW()                                                        AS reg_dt
				FROM purchase_store_info psi
					INNER JOIN bundle bun ON psi.purchase_store_info_no = bun.purchase_store_info_no
					INNER JOIN order_discount od ON psi.purchase_order_no = od.purchase_order_no AND bun.bundle_no = od.bundle_no AND psi.store_id = od.store_id AND od.discount_kind = 'ship_coupon'
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo
                AND bun.ship_method in ('TD_DRCT','TD_PICK','TD_DLV','TD_QUICK')
                GROUP BY psi.purchase_store_info_no, od.barcode;

				## 대상건수 기록 : rf_trade_coupon - ship_coupon
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_coupon (배송비쿠폰) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## 2-10) rf_trade_coupon_item 적재 - 상품할인/상품쿠폰(사용아이템 적재)
				## 1. DS상품 상품쿠폰 사용아이템 미전송
				INSERT INTO rf_trade_coupon_item (
				       ord_no
				     , coupon_code
				     , plu_code
				     , plu_seq
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , coupon_type
				     , item_qty
				     , coupon_item_qty
				     , coupon_qty
				     , coupon_amount
				     , reg_dt
				)
                /* 상품할인, 상품쿠폰 저장 */
				SELECT /* RF_TRADE_COUPON_ITEM 대상조회 */
						psi.purchase_store_info_no			AS ord_no
					,	od.barcode							AS coupon_code
                    ,	iti.plu_code						AS plu_code
                    ,	iti.item_seq						AS plu_seq
					, 	vStoreId							AS store_id          /*점포ID*/
				    , 	vPosNo								AS pos_no              /*POS번호*/
				    , 	vTradeDt							AS trade_date          /*주문일자(YYYYMMDD)*/
				    , 	vtradeNo         					AS trade_no            /*영수증번호*/
                    ,	CASE WHEN od.discount_type = '1' THEN '03'
							ELSE '01'
						END									AS coupon_type
					,	od.item_qty							AS item_qty
                    ,	od.coupon_apply_qty					AS coupon_item_qty
                    ,	1									AS coupon_qty
                    ,	od.discount_amt						AS coupon_amt
                    ,   NOW()                               AS reg_dt
				FROM rf_trade_item iti
				INNER JOIN purchase_store_info psi ON iti.ord_no = psi.purchase_store_info_no
				INNER JOIN bundle bun ON bun.purchase_store_info_no = psi.purchase_store_info_no
				INNER JOIN order_item oi ON bun.bundle_no = oi.bundle_no AND iti.good_id = oi.item_no AND psi.store_id = oi.store_id
				INNER JOIN order_discount od ON oi.order_item_no = od.order_item_no AND od.discount_kind in ('item_discount','item_coupon')
				WHERE iti.ord_no = vPurchaseStoreInfoNo
                AND bun.ship_method in ('TD_DRCT','TD_PICK','TD_DLV','TD_QUICK')
                ;

				## 대상건수 기록 : rf_trade_coupon_item
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_coupon_item (상품할인,상품쿠폰 ITEM) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## 2-11) rf_trade_coupon_item 적재 - 배송비쿠폰
                ## 1. DS상품은 배송비쿠폰 사용아이템 미전송
				INSERT INTO rf_trade_coupon_item (
				       ord_no
				     , coupon_code
				     , plu_code
				     , plu_seq
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , coupon_type
				     , item_qty
				     , coupon_item_qty
				     , coupon_qty
				     , coupon_amount
				     , reg_dt
				)
				/* 배송비쿠폰 저장 */
                SELECT /* RF_TRADE_COUPON_ITEM 대상조회 */
						psi.purchase_store_info_no					AS ord_no
					,	od.barcode									AS coupon_code
                    ,	iti.plu_code								AS plu_code
                    ,	iti.item_seq								AS plu_seq
				    , vStoreId										AS store_id          /*점포ID*/
				    , vPosNo										AS pos_no              /*POS번호*/
				    , vTradeDt										AS trade_date          /*주문일자(YYYYMMDD)*/
					, vtradeNo         								AS trade_no            /*영수증번호*/
                    ,	'01'										AS coupon_type
					,	1											AS item_qty
                    ,	1											AS coupon_item_qty
                    ,	1											AS coupon_qty
                    ,	od.discount_amt								AS coupon_amt
                    ,   NOW()                                       AS reg_dt
				FROM rf_trade_item iti
				INNER JOIN purchase_store_info psi ON iti.ord_no = psi.purchase_store_info_no
				INNER JOIN bundle bun ON bun.purchase_store_info_no = psi.purchase_store_info_no AND bun.store_id = psi.store_id
				INNER JOIN order_discount od ON bun.bundle_no = od.bundle_no AND iti.good_id = bun.ship_price_item_no AND od.discount_kind = 'ship_coupon'
				WHERE iti.ord_no = vPurchaseStoreInfoNo
                AND bun.ship_method in ('TD_DRCT','TD_PICK','TD_DLV','TD_QUICK')
                ;


				## 대상건수 기록 : rf_trade_coupon_item
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_coupon_item (배송비쿠폰 ITEM) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;




				## 2-12) rf_trade_dc_coupon 적재 - 상품쿠폰(+배송비쿠폰)
				SET @vCouponSeq = 0;

				INSERT INTO rf_trade_dc_coupon(
				       ord_no
				     , coupon_seq
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , coupon_fraction
				     , coupon_no
				     , plu_code
				     , dc_amount
				     , reg_dt
				)
                SELECT /* RF_TRADE_DC_COUPON 대상조회 (상품할인, 상품쿠폰) */
						psi.purchase_store_info_no					AS ord_no			/*주문번호*/
					,	@vCouponSeq := @vCouponSeq+1        		AS coupon_seq      /*쿠폰순번*/
					, 	vStoreId									AS store_id          /*점포ID*/
				    , 	vPosNo										AS pos_no              /*POS번호*/
				    , 	vTradeDt									AS trade_date          /*주문일자(YYYYMMDD)*/
				    , 	vtradeNo         							AS trade_no            /*영수증번호*/
					,	'1'											AS coupon_fraction		/*쿠폰구분(1:에누리,2:CMS)*/
					,	od.barcode									AS coupon_code			/*쿠폰번호(barcode)*/
                    ,	CONCAT('2000',oi.item_no)					AS plu_code				/*단품코드*/
                    ,	od.discount_amt								AS coupon_amt 			/*에누리한 순수금액*/
                    ,   NOW()                                       AS reg_dt
				FROM  purchase_store_info psi
				INNER JOIN bundle bun ON bun.purchase_store_info_no = psi.purchase_store_info_no
				INNER JOIN order_item oi ON bun.bundle_no = oi.bundle_no AND psi.store_id = oi.store_id
				INNER JOIN order_discount od ON psi.purchase_order_no = od.purchase_order_no AND oi.order_item_no = od.order_item_no AND od.discount_kind in ('item_discount','item_coupon')
                WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo
                AND psi.ship_method in ('TD_DRCT','TD_PICK','TD_DLV','TD_QUICK')

                UNION ALL

                SELECT /* RF_TRADE_DC_COUPON 대상조회 (배송비쿠폰) */
						psi.purchase_store_info_no						AS ord_no
					, 	@vCouponSeq := @vCouponSeq+1        			AS coupon_seq      /*쿠폰순번*/
					, 	vStoreId										AS store_id          /*점포ID*/
				    , 	vPosNo											AS pos_no              /*POS번호*/
				    , 	vTradeDt										AS trade_date          /*주문일자(YYYYMMDD)*/
				    , 	vtradeNo         								AS trade_no            /*영수증번호*/
					,	'1'												AS coupon_fraction		/*쿠폰구분(1:에누리,2:CMS)*/
					,	od.barcode										AS coupon_code			/*쿠폰번호(barcode)*/
                    ,	CONCAT('2000',bun.ship_price_item_no)			AS plu_code				/*단품코드*/
                    ,	od.discount_amt									AS coupon_amt
                    ,   NOW()                                           AS reg_dt
				FROM purchase_store_info psi
				INNER JOIN bundle bun ON bun.purchase_store_info_no = psi.purchase_store_info_no AND bun.store_id = psi.store_id
				INNER JOIN order_discount od ON bun.bundle_no = od.bundle_no AND od.discount_kind = 'ship_coupon'
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo
                AND psi.ship_method in ('TD_DRCT','TD_PICK','TD_DLV','TD_QUICK')

                ;



				## 대상건수 기록 : rf_trade_dc_coupon
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_dc_coupon (상품할인/상품쿠폰/배송비쿠폰) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## 2-13) rf_trade_dc_emp 적재 - 임직원할인
				INSERT INTO rf_trade_dc_emp (
				       ord_no
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , dc_type
				     , item_qty
				     , emp_id
				     , emp_nm
				     , emp_kind
				     , emp_dc_amount
				     , trade_amount
				     , dc_balance
				     , point_deduction
				     , reg_dt
				)
				SELECT /* RF_TRADE_DC_EMP 대상조회 (TD 상품만 대상임) */
				       psi.purchase_store_info_no          			AS ord_no
                     , vStoreId										AS store_id          /*점포ID*/
				     , vPosNo										AS pos_no              /*POS번호*/
				     , vTradeDt										AS trade_date          /*주문일자(YYYYMMDD)*/
				     , vtradeNo         							AS trade_no            /*영수증번호*/
                     , '1'                                 			AS dc_type
				     , (SELECT COUNT(oi.order_item_no)
						FROM order_item oi
                        WHERE oi.bundle_no = bun.bundle_no
                        AND   oi.emp_discount_amt >0 )  			AS item_qty
				     , po.emp_no                           			AS emp_id
				     , LEFT(TRIM(po.emp_nm), 10)           			AS emp_nm
				     , po.emp_kind                         			AS emp_kind
				     , psi.tot_emp_discount_amt             		AS emp_dc_amount
                      , psi.tot_item_price
				       + psi.tot_ship_price
				       + psi.tot_island_ship_price
				       - psi.tot_item_discount_amt
				       - psi.tot_ship_discount_amt
				       - psi.tot_promo_discount_amt
				       - psi.tot_emp_discount_amt
				       - psi.tot_card_discount_amt         			AS trade_amount          /*거래금액(할인후 총액)*/
				     , psi.emp_remain_amt                  			AS dc_balance
				     , '0'                                 			AS point_deduction
				     , NOW()                                        AS reg_dt
				FROM purchase_store_info psi
					INNER JOIN purchase_order po ON psi.purchase_order_no = po.purchase_order_no
					INNER JOIN bundle bun ON bun.purchase_store_info_no = psi.purchase_store_info_no
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo
                AND psi.tot_emp_discount_amt > 0
                AND psi.ship_method in ('TD_DRCT','TD_PICK','TD_DLV','TD_QUICK')
                ;


				## 대상건수 기록 : rf_trade_dc_emp
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_dc_emp (임직원할인) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## 2-14) rf_trade_dc_emp_item 적재 - 임직원할인 상품
				INSERT INTO rf_trade_dc_emp_item (
				       ord_no
				     , plu_code
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , emp_seq
				     , goodkr_nm2
				     , item_qty
				     , item_sum_amount
				     , dc_rate
				     , dc_amount
				     , link_amount
				     , section_no
				     , reg_dt
				)
				SELECT /* RF_TRADE_DC_EMP_ITEM 대상조회 */
				       iti.ord_no                          			AS ord_no
				     , iti.plu_code                        			AS plu_code
                     , vStoreId										AS store_id          /*점포ID*/
				     , vPosNo										AS pos_no              /*POS번호*/
				     , vTradeDt										AS trade_date          /*주문일자(YYYYMMDD)*/
				     , vtradeNo         							AS trade_no            /*영수증번호*/
				     , iti.item_seq                        			AS emp_seq
				     , iti.print_item_name                 			AS goodkr_nm2
				     , iti.item_qty                        			AS item_qty
				     , iti.item_cost                       			AS item_sum_amount
				     , oi.emp_discount_rate                			AS dc_rate
				     , oi.emp_discount_amt                 			AS dc_amount
				     , '0'                                 			AS link_amount
				     , oi.dept_no                          			AS section_no
				     , NOW()                                        AS reg_dt
				FROM rf_trade_item iti
					INNER JOIN purchase_store_info psi ON iti.ord_no = psi.purchase_store_info_no
					INNER JOIN bundle bun ON bun.purchase_store_info_no = psi.purchase_store_info_no
					INNER JOIN order_item oi ON oi.bundle_no = bun.bundle_no AND iti.good_id = oi.item_no AND oi.mall_type = 'TD'
				WHERE iti.ord_no = vPurchaseStoreInfoNo
				AND IFNULL(iti.emp_dc_amount,0) > 0;

				## 대상건수 기록 : rf_trade_dc_emp_item
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_dc_emp_item (임직원할인-ITEM) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;

				##카드상품할인 카드정보 조회
                SELECT
						substr(pi.card_prefix_no,1,6)
					, 	substr(cpm.card_iden_nm,1,20)
				INTO
						vCardPrefixNo
					,	vCardCompanyNm
				FROM payment_info pi
                INNER JOIN purchase_order po ON po.payment_no = pi.payment_no
				INNER JOIN card_prefix_mng cpm ON cpm.card_prefix_no = substr(pi.card_prefix_no,1,6)
				WHERE pi.payment_type ='PG'
				AND pi.parent_method_cd ='CARD'
				AND pi.card_no IS NOT NULL
                AND po.purchase_order_no = vPurchaseOrderNo
				LIMIT 0,1
				;

				## 카드정보 세팅 실패한 경우 재조회
				IF vCardPrefixNo IS NULL OR vCardCompanyNm IS NULL THEN
					SELECT
							substr(pi.card_prefix_no,1,6)
						, 	substr(pm.method_nm,1,20)
					INTO
							vCardPrefixNo
						,	vCardCompanyNm
					FROM payment_info pi
					    INNER JOIN purchase_order po ON po.payment_no = pi.payment_no
						INNER JOIN payment_method pm ON pm.method_cd = pi.method_cd
					WHERE pi.payment_type = 'PG'
					AND pi.parent_method_cd = 'CARD'
					AND pi.card_no IS NOT NULL
				    AND po.purchase_order_no = vPurchaseOrderNo
					LIMIT 0,1
					;
				END IF;

				## 2-15) rf_trade_dc_card 적재 - 카드상품할인
                # DS상품은 Refit상품코드와 Pine전송용 상품코드가 다름, DS는 RMS 대표상품코드로 전송됨.
                # DS는 카드상품할인 정보를 Pine에 미전송함.
				INSERT INTO rf_trade_dc_card (
				       ord_no
				     , good_id
				     , plu_seq
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , plu_code
				     , item_qty
				     , item_cost
				     , coupon_qty
				     , supp_ty
				     , card_dc_amount
				     , couponbar_cd
				     , card_type
				     , card_type_nm
				     , card_dc_prefix
				     , reg_dt
				)
				SELECT /* RF_TRADE_DC_CARD 대상조회 - trade_item_tran */
				       iti.ord_no                         				AS ord_no
				     , iti.good_id                        				AS good_id
				     , iti.item_seq                       				AS plu_seq
                     , vStoreId											AS store_id
				     , vPosNo											AS pos_no
				     , vTradeDt											AS trade_date
				     , vtradeNo         								AS trade_no
				     , iti.plu_code                       				AS plu_code
				     , iti.item_qty                       				AS item_qty
				     , iti.item_cost                      				AS item_cost
				     , iti.card_apply_qty                 				AS coupon_qty
				     , CASE odc.discount_type
				       WHEN '1' THEN '1'
				       WHEN '2' THEN '2'
				       ELSE ''
				       END                                				AS supp_ty
				     , iti.card_dc_amount                 				AS card_dc_amount
				     , odc.barcode                         				AS couponbar_cd
				     , IFNULL(od.card_group_cd, odc.card_group_cd)		AS card_type
				     , vCardCompanyNm                                 	AS card_type_nm
				     , vCardPrefixNo                          			AS card_dc_prefix
				     , NOW()                                            AS reg_dt
				FROM rf_trade_item iti
					INNER JOIN purchase_store_info psi ON iti.ord_no = psi.purchase_store_info_no
					INNER JOIN order_item oi ON psi.purchase_order_no = oi.purchase_order_no AND oi.item_no = iti.good_id
					INNER JOIN order_discount od ON od.purchase_order_no = psi.purchase_order_no AND od.discount_kind = 'card_discount'
                    INNER JOIN order_discount_cart odc ON od.order_discount_no = odc.order_discount_no AND oi.item_no = odc.item_no
 				WHERE iti.ord_no = vPurchaseStoreInfoNo
                AND psi.ship_method in ('TD_DRCT','TD_PICK','TD_DLV','TD_QUICK')
				AND iti.card_dc_amount > 0;

				## 대상건수 기록 : rf_trade_dc_card
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_dc_card (카드상품할인) ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## 2-16) rf_trade_media 적재 - 지불수단
				INSERT INTO rf_trade_media (
				       ord_no
				     , tran_media_fraction
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , reg_dt
				)
				SELECT /* RF_TRADE_MEDIA 대상조회 PG */
				       psi.purchase_store_info_no          				AS ord_no
				     , '31'                                				AS tran_media_fraction -- (6:장바구니쿠폰,7:상품할인쿠폰,11:제휴,31:PG)
                     , vStoreId											AS store_id
				     , vPosNo											AS pos_no
				     , vTradeDt											AS trade_date
				     , vtradeNo         								AS trade_no
				     , NOW()                                            AS reg_dt
				FROM purchase_store_info psi
                WHERE (psi.pg_amt > 0 OR psi.mhc_amt > 0 OR psi.dgv_amt > 0 OR psi.ocb_amt > 0)
                AND	psi.purchase_store_info_no = vPurchaseStoreInfoNo

				UNION ALL

				SELECT /* RF_TRADE_MEDIA 대상조회 장바구니쿠폰,Mileage */
				       psi.purchase_store_info_no          				AS ord_no
				     , '6'                                 				AS tran_media_fraction -- (6:장바구니쿠폰,6:Mileage,7:상품할인쿠폰,11:제휴,31:PG)
                     , vStoreId											AS store_id
				     , vPosNo											AS pos_no
				     , vTradeDt											AS trade_date
				     , vtradeNo         								AS trade_no
				     , NOW()                                            AS reg_dt
				FROM purchase_store_info psi
				WHERE (psi.tot_cart_discount_amt > 0 OR psi.mileage_amt > 0 OR psi.tot_add_td_discount_amt > 0)
                AND	psi.purchase_store_info_no = vPurchaseStoreInfoNo

				UNION ALL

                SELECT /* RF_TRADE_MEDIA 대상조회 상품할인/상품쿠폰/배송비쿠폰 */
				       psi.purchase_store_info_no          				AS ord_no
				     , '7'                                 				AS tran_media_fraction -- (6:장바구니쿠폰,7:상품할인쿠폰,11:제휴,31:PG)
                     , vStoreId											AS store_id
				     , vPosNo											AS pos_no
				     , vTradeDt											AS trade_date
				     , vtradeNo         								AS trade_no
				     , NOW()                                            AS reg_dt
				FROM purchase_store_info psi
				WHERE (psi.tot_item_discount_amt > 0 OR psi.tot_ship_discount_amt > 0)
                AND psi.ship_method in ('TD_DRCT','TD_PICK','TD_DLV','TD_QUICK')
                AND	psi.purchase_store_info_no = vPurchaseStoreInfoNo
                ;

				## 대상건수 기록 : rf_trade_media
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_media ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


                ## 2-17) rf_trade_header 적재 : 마지막으로 위치 이동
				INSERT INTO rf_trade_header (
				       ord_no
					 , cust_ord_no
					 , user_id
					 , store_id
					 , pos_no
					 , trade_date
					 , trade_no
					 , trade_time
					 , ftype_code
					 , account_fraction
					 , summary_fraction
					 , checker_no
					 , delivery_dt
					 , delivery_cd
					 , shift_id
					 , slot_id
					 , van_id
					 , ord_ty
					 , trade_fraction
					 , trade_count
					 , trade_change_amount
					 , trade_delivery_amount
					 , trade_bottle_amount
					 , trade_onetool_amount
					 , trade_tot_amount
					 , trade_amount
					 , trade_dc_amount
					 , trade_receive_amount
					 , rtn_ord_no
					 , rtn_store_code
					 , rtn_pos_no
					 , rtn_trade_date
					 , rtn_trade_no
					 , rtn_fault_fraction
					 , part_cancel
					 , part_cancel_flag
					 , subtotal_discount_rate
					 , subtotal_discount_amount
					 , subtotal_discount_fraction
					 , pg_amount
					 , special_amount
					 , card_pay
					 , digital_pay
					 , card_dc_amount
					 , emp_dc_amount
					 , head_office_code
					 , area_code
					 , noncust_yn
					 , mall_delivery_type
					 , rcptzip_txt
					 , rcptadd1_txt
					 , fc_store_id
					 , check_row_cnt
					 , reg_dt
				)
				SELECT /* RF_TRADE_HEADER 대상조회 */
				       psi.purchase_store_info_no          							AS ord_no                /*매출주문번호*/
				     , po.purchase_order_no                							AS cust_ord_no           /*고객주문번호*/
				     , po.user_no                          							AS user_id               /*고객ID*/
                     , vStoreId														AS store_id
				     , vPosNo														AS pos_no
				     , vTradeDt														AS trade_date
				     , vtradeNo         											AS trade_no
				     , DATE_FORMAT(po.order_dt, '%H%i%s')  							AS trade_time            /*주문시간(HHmmss)*/
				     , '1'                                 							AS ftype_code            /*거래구분(1:주문,2:취소/반품)*/
				     , '0'                                 							AS account_fraction      /*회계구분(0:정상매출(=주문),3:특판(=제휴),SOD(37:POS개시,32:SIGN-ON), EOD(33:SIGN-OFF,정산:35) */
				     , '0'                                 							AS summary_fraction      /*집계구분*/
				     , '1010101010100'                     							AS checker_no            /*체커NO*/
                     , CASE WHEN (psi.ship_method = 'TD_DRCT' OR psi.ship_method = 'TD_PICK' OR psi.ship_method = 'TD_DLV' OR psi.ship_method = 'TD_QUICK') THEN psi.ship_dt
							ELSE NULL
					        END                                     				AS DELIVERY_DT
				       -- 배송방법 (DS_DLV:택배배송,DS_DRCT:직접배송,DS_PICK:방문수령/픽업,DS_POST:우편배송,DS_QUICK:퀵배송
				       -- ,TD_DLV:택배배송,TD_DRCT:직접배송,TD_PICK:방문수령,TD_POST:우편배송,TD_QUICK:퀵배송,TD_LOCKER:락커수령)
				     , CASE WHEN psi.ship_method = 'TD_DRCT' THEN '37010'
				            WHEN psi.ship_method = 'TD_PICK' THEN '37030'
				            ELSE '37020'
				            END                                     				AS DELIVERY_CD           /*배송구분(37010:자차,37020:택배,37030:픽업)*/
				     , CASE WHEN (psi.ship_method = 'TD_DRCT' OR psi.ship_method = 'TD_PICK' OR psi.ship_method = 'TD_DLV' OR psi.ship_method = 'TD_QUICK') THEN psi.shift_id
						    ELSE NULL
						    END 													AS SHIFT_ID			 /*주문쉬프트ID, DS:null*/
				     , CASE WHEN (psi.ship_method = 'TD_DRCT' OR psi.ship_method = 'TD_PICK' OR psi.ship_method = 'TD_DLV' OR psi.ship_method = 'TD_QUICK') THEN psi.slot_id
							ELSE NULL
						    END 													AS SLOT_ID			  /*주문슬롯ID, DS:null*/
				     , ''                                           				AS VAN_ID                /*차량ID*/
				     , CASE po.platform
				       WHEN 'PC' THEN '44010'
				       WHEN 'APP' THEN '44070'
				       WHEN 'MWEB' THEN '44090'
				       ELSE '44010'
				       END                                 							AS ORD_TY                /*주문구분*/
				     , 6                                   							AS TRADE_FRACTION        /*결재구분(1:미결당사,6:통산)*/
				     , psi.trade_qty 					   							AS TRADE_COUNT           /*거래수량*/
				     , 0                                   							AS TRADE_CHANGE_AMOUNT   /*거스름돈*/
				     , 0                                   							AS TRADE_DELIVERY_AMOUNT /*배송료*/
				     , 0                                   							AS TRADE_BOTTLE_AMOUNT   /*공병가*/
				     , 0                                   							AS TRADE_ONETOOL_AMOUNT  /*일회용품가격*/
                     , CASE WHEN SUBSTR(psi.ship_method,1,3) = 'DS_' THEN
								psi.tot_item_price
								+ psi.tot_ship_price
								+ psi.tot_island_ship_price
                                - psi.tot_item_discount_amt
								- psi.tot_ship_discount_amt
								- psi.tot_promo_discount_amt
								- psi.tot_emp_discount_amt
								- psi.tot_card_discount_amt
								- IFNULL(psi.tot_add_ds_discount_amt,0)
							ELSE
								psi.tot_item_price
								+ psi.tot_ship_price
								+ psi.tot_island_ship_price
						END 														AS TRADE_TOT_AMOUNT      /*거래합계금액(할인전 총액)*/

				     , psi.tot_item_price
				       + psi.tot_ship_price
				       + psi.tot_island_ship_price
				       - psi.tot_item_discount_amt
				       - psi.tot_ship_discount_amt
				       - psi.tot_promo_discount_amt
				       - psi.tot_emp_discount_amt
				       - psi.tot_card_discount_amt
					   - IFNULL(psi.tot_add_ds_discount_amt,0)						AS TRADE_AMOUNT          /*거래금액(할인후 총액)*/

					, CASE WHEN SUBSTR(psi.ship_method,1,3) = 'DS_' THEN
								0
							ELSE
								psi.tot_item_discount_amt
							   + psi.tot_ship_discount_amt
							   + psi.tot_promo_discount_amt
							   + psi.tot_emp_discount_amt
							   + psi.tot_card_discount_amt
					  END		 													AS TRADE_DC_AMOUNT       /*소계전에누리(할인합계)*/

				     , psi.tot_item_price
				       + psi.tot_ship_price
				       + psi.tot_island_ship_price
				       - psi.tot_item_discount_amt
				       - psi.tot_ship_discount_amt
				       - psi.tot_promo_discount_amt
				       - psi.tot_emp_discount_amt
				       - psi.tot_card_discount_amt
				       - IFNULL(psi.tot_add_ds_discount_amt,0)						AS trade_receive_amount  /*받은돈*/

				     , CASE WHEN po.order_type = 'ORD_SUB' THEN psi.org_purchase_store_info_no
						ELSE NULL
                        END 																			AS rtn_ord_no            /*원주문번호(=purchase_store_info_no)*/
				     , CASE WHEN po.order_type = 'ORD_SUB' THEN LPAD(psi.origin_store_id, 4, '0')
						ELSE NULL
                        END                                   											AS rtn_store_code        /*원주문점포ID*/
				     , CASE WHEN po.order_type = 'ORD_SUB' THEN LPAD(psi.org_pos_no, 6, '0')
						ELSE NULL
                        END                                   											AS rtn_pos_no            /*원주문POS번호*/
				     , CASE WHEN po.order_type = 'ORD_SUB' THEN psi.org_trade_dt
						ELSE NULL
                        END                                   											AS rtn_trade_date        /*원주문주문일*/
				     , CASE WHEN po.order_type = 'ORD_SUB' THEN LPAD(psi.org_trade_no, 4, '0')
						ELSE NULL
                        END                                   											AS rtn_trade_no          /*원주문영수증번호*/
				     , '0'                                 												AS rtn_fault_fraction    /*반품오타구분*/
				     , '0'                                 												AS part_cancel           /*부분취소상태(2:부분취소,4:부분반품)*/
				     , NULL                                												AS part_cancel_flag      /*부분취소관련주문여부*/
				     , 0                                   												AS subtotal_discount_rate
				     , 0                                   												AS subtotal_discount_amount
				     , '0'                                 												AS subtotal_discount_fraction
				       -- 결제수단별 합산 대신 payment.tot_amt 로 대체할지 검토. [=IFNULL(p.tot_amt, 0)]
				     , psi.pg_amt
				       + psi.mhc_amt
				       + psi.dgv_amt
				       + psi.ocb_amt             								AS pg_amount             /*통합 TENDER 금액(카드+MHC+DGV+OCB)*/
				     , 0                                   						AS special_amount        /*할인특매*/
				     , psi.pg_amt                								AS card_pay              /*신용카드지불액*/
				     , psi.dgv_amt               								AS digital_pay           /*디지털상품권지불액*/
                      , CASE WHEN (psi.ship_method = 'TD_DRCT' OR psi.ship_method = 'TD_PICK' OR psi.ship_method = 'TD_DLV' OR psi.ship_method = 'TD_QUICK') THEN psi.tot_card_discount_amt
						ELSE 0
						END            											AS card_dc_amount        /*카드할인금액*/
					  , CASE WHEN (psi.ship_method = 'TD_DRCT' OR psi.ship_method = 'TD_PICK' OR psi.ship_method = 'TD_DLV' OR psi.ship_method = 'TD_QUICK') THEN psi.tot_emp_discount_amt
						ELSE 0
						END 													AS emp_dc_amount         /*직원할인금액*/
				     , (SELECT swi.head_office_code
				        FROM rf_sto_winpos_info swi
				        WHERE swi.store_id = LPAD(psi.origin_store_id, 4, '0')
				        AND swi.pos_no = LPAD(psi.pos_no, 6, '0')
				        LIMIT 1)                            AS HEAD_OFFICE_CODE      /*본부코드*/
				     , (SELECT swi.area_code
				        FROM rf_sto_winpos_info swi
				        WHERE swi.store_id = LPAD(psi.origin_store_id, 4, '0')
				        AND swi.pos_no = LPAD(psi.pos_no, 6, '0')
				        LIMIT 1)                           	AS AREA_CODE             /*지역코드*/
				     , po.nomem_order_yn       				AS NONCUST_YN            /*비회원여부*/
				     , CASE WHEN INSTR(psi.ship_method,'TD_') >= 1 THEN '31010'
							WHEN INSTR(psi.ship_method,'DS_') >= 1 THEN '31020'
                            ELSE '31030'
					   END                                  AS MALL_DELIVERY_TYPE    /*("31010":TD, "31020":DS, "31030":TD/DS)*/
					 , (SELECT sa.zipcode
						FROM shipping_addr sa
						WHERE sa.bundle_no = b.bundle_no LIMIT 0, 1
                        )									AS RCPTZIP_TXT          /*수취인우편번호*/
                     , (SELECT LEFT(sa2.road_base_addr,100)
						FROM shipping_addr sa2
						WHERE sa2.bundle_no = b.bundle_no LIMIT 0, 1
                        )									AS RCPTADD1_TXT          /*수취인우편주소*/
					 , psi.fc_store_id                     	AS FC_STORE_ID           /*FC점포코드*/ -- FC점포 존재하는 매장만 FC store_id 입력
					 , vCheckRowCnt                        	AS CHECK_ROW_CNT         /*전송테이블검증*/ -- 헤더 제외 모든 row count, vCheckRowCnt 누적건수 입력
					 , NOW()                                AS reg_dt
				FROM purchase_store_info psi
					INNER JOIN purchase_order po ON psi.purchase_order_no = po.purchase_order_no
					INNER JOIN payment p ON po.payment_no = p.payment_no AND p.payment_status in('P3','P5')
					INNER JOIN bundle b ON po.purchase_order_no = b.purchase_order_no
										AND psi.purchase_store_info_no = b.purchase_store_info_no
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE purchase_order
					SET pine_if_yn = 'E'
					  , pine_if_dt = NOW()
					WHERE purchase_order_no = vPurchaseOrderNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_header ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


/* 11/27 Cursor 처리로 변경
	## 3. 후처리 진행
				## 완료건 업데이트
				UPDATE purchase_order
				SET pine_if_yn = 'Y'
				  , pine_if_dt = now()
				WHERE purchase_order_no = vPurchaseOrderNo
				AND pine_if_yn = 'I';
*/
				COMMIT; ## Transaction 최종 커밋

				SET vSuccessCount = vSuccessCount + 1;
				SET vDone = FALSE;

			END IF;

		UNTIL vDone
		END REPEAT read_psi_loop;

	CLOSE cursorPurchaseStoreInfo;


	PURCHASE_ORDER_FINAL:BEGIN
    	DECLARE CONTINUE HANDLER FOR NOT FOUND
			BEGIN
				SET vDoneFinal = TRUE;
			END;

		#전송완료변경대상 OPEN
		OPEN cursorPurchaseOrderFinal;
			SELECT FOUND_ROWS() INTO vTargetCount;

			## 인터페이스 대상이 없을 경우 프로시저 종료
			IF vTargetCount = 0 THEN
                SET vDoneFinal = FALSE;
				LEAVE PURCHASE_ORDER_FINAL;
			END IF;

			## 작업대상 데이터 업데이트 (pine_if_yn : N(처리대기) -> I(처리중))
			read_po_loop_final: LOOP

				FETCH cursorPurchaseOrderFinal INTO vPurchaseOrderNo;

					IF NOT vDoneFinal THEN
						## 완료건 업데이트
						UPDATE purchase_order
						SET pine_if_yn = 'Y'
						  , pine_if_dt = now()
						WHERE purchase_order_no = vPurchaseOrderNo
						AND pine_if_yn = 'I';
					END IF;

					IF vDoneFinal THEN
						LEAVE read_po_loop_final;
					END IF;

			END LOOP read_po_loop_final;

		CLOSE cursorPurchaseOrderFinal;
	END PURCHASE_ORDER_FINAL;

	## tx_isolation원복
    SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;

	## 배치로그 종료
	CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwBatchNo, vFailCount, vSuccessCount,vTotalCount, vErrorMessage, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);

END MAIN_PROCESS