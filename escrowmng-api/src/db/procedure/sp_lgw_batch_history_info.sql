CREATE DEFINER=`app_escrow`@`%` PROCEDURE `sp_lgw_batch_history_info`(
    IN in_procedure_nm VARCHAR(1000),           -- 프로시져 배치명
    IN in_lgw_bath_history_no BIGINT,         	-- 이력번호(lgw_batch_history)
    IN in_log_level VARCHAR(20),				-- 로그레벨(INFO,ERROR)
    IN in_val1 VARCHAR(1000),
    IN in_val2 VARCHAR(1000),
    IN in_val3 VARCHAR(1000),
    IN in_val4 VARCHAR(1000),
    IN in_val5 VARCHAR(1000)
    )
BEGIN

	-- 프로시져 중간 디버깅을 위한 로그 추가
	INSERT INTO lgw_batch_history_info(procedure_nm, lgw_batch_history_no, log_level, val1, val2, val3, val4, val5)
	VALUES (in_procedure_nm, in_lgw_bath_history_no, in_log_level, in_val1, in_val2, in_val3, in_val4, in_val5);

END