CREATE DEFINER=`app_escrow`@`%` PROCEDURE `sp_lgw_batch_history`(
    IN in_procedure_nm VARCHAR(100),                  -- 프로시져 배치명
    IN in_log_type VARCHAR(10),                 -- 로그타입(START:시작, END:종료, ADDLOG:추가)
    IN in_lgw_bath_history_no BIGINT,                  	-- 이력번호(lgw_batch_history)
    IN in_fail_count BIGINT,                    -- 실패건수(lgw_batch_history)
    IN in_success_count BIGINT,                 -- 성공건수(lgw_batch_history)
    IN in_total_count BIGINT,                   -- 전체건수(lgw_batch_history)
    IN in_error_message VARCHAR(2000),          -- 오류메시지(lgw_batch_history)

    IN in_val1 VARCHAR(1000),
    IN in_val2 VARCHAR(1000),
    IN in_val3 VARCHAR(1000),
    IN in_val4 VARCHAR(1000),
    IN in_val5 VARCHAR(1000),
    OUT out_lgw_batch_history_no BIGINT
    )
BEGIN

    IF in_log_type = 'START' THEN
		-- 배치시작 시점 저장
        INSERT
			INTO lgw_batch_history
            (
				 batch_nm, 				batch_start_dt,
				 meta_field1, 			meta_field2,
				 meta_field3, 			meta_field4,
				 meta_field5
             )
		VALUES
			(
				in_procedure_nm,			NOW(),
                in_val1,					in_val2,
                in_val3,					in_val4,
                in_val5
			)
            ;

        SELECT  LAST_INSERT_ID()
        INTO    out_lgw_batch_history_no;

    ELSE
        IF in_log_type = 'END' THEN
            UPDATE  lgw_batch_history
            SET      batch_end_dt = NOW()
                    ,batch_time = CONCAT(
                                    LPAD(TIMESTAMPDIFF(HOUR,batch_start_dt,NOW()),2,'0'), ':',
                                    LPAD(MOD(TIMESTAMPDIFF(MINUTE,batch_start_dt,NOW()), 60),2,'0'), ':',
                                    LPAD(MOD(TIMESTAMPDIFF(SECOND,batch_start_dt,NOW()), 60),2,'0')
                                  )
                    ,fail_count = in_fail_count
                    ,success_count = in_success_count
                    ,total_count = in_total_count
                    ,error_message = in_error_message
                    ,meta_field1 = IFNULL(in_val1, meta_field1)
                    ,meta_field2 = IFNULL(in_val2, meta_field2)
                    ,meta_field3 = IFNULL(in_val3, meta_field3)
                    ,meta_field4 = IFNULL(in_val4, meta_field4)
                    ,meta_field5 = IFNULL(in_val5, meta_field5)
            WHERE   lgw_batch_history_no = in_lgw_bath_history_no;
        END IF;
    END IF;

END