CREATE DEFINER=`app_escrow`@`%` PROCEDURE `escrow`.`sp_reorder_if_pine_trade_info`()
    COMMENT 'PINE 재주문 대상 데이터 IF 테이블에 입력'
REORDER_PROCESS:BEGIN
/****************************************************************************************
- 목적/용도: 클레임 재주문 데이터 PINE 전송 프로시저 (재주문 프로시저 #3)
	1. 재전송 POS 번호에 맞추어 재주문 데이터를 생성함.
	2. 재주문을 위한 구매점포정보를 reorder_purchase_store_info 에서 조회함.
	   -. reorder_purchase_store_info.pine_if_yn = 'N' 데이터에 대해 재주문 데이터를 생성함.
	   -. 조회키값 : reorder_purchase_store_info.claim_store_info_no
	   -. 조회항목 : order_no, pos_no, trade_no, trade_dt
	   -. 재주문 데이터 생성 후 pine_if_yn : N -> Y 업데이트
	3. 재주문 데이터는 기 생성된 클레임 TRAN 데이터를 기준으로 주문 TRAN 데이터 생성함.

- 작성규칙
	1. SQL작성시 예약어만 대문자로 작성
    2. 변수명 작성은 카멜규칙을 따름

- 수정일자/수정내용/수정자
    2021-01-26 / 최초작성 / 박준수
***************************************************************************************/

	## 0. 프로시저 공통 변수 및 커서 선언
	DECLARE vDone                							INT DEFAULT FALSE;	## 주문전송대상 Fectch완료여부
	DECLARE vDoneFinal             							INT DEFAULT FALSE;	## 전송완료변경대상 Fectch완료여부

	DECLARE vTargetCount         							INT DEFAULT 0;		## 대상건수
	DECLARE vCheckRowCnt         							INT DEFAULT 0;		## 데이터검증용건수
	DECLARE vClaimNo     									VARCHAR(30);		## 클레임번호
	DECLARE vClaimStoreInfoNo 								VARCHAR(30);		## 클레임점포번호
    DECLARE vStoreId			 							VARCHAR(4);			## 점포번호
    DECLARE vPosNo				 							VARCHAR(6);			## 포스번호
    DECLARE vTradeDt			 							VARCHAR(8);			## 거래일자(YYYYMMDD)
    DECLARE vTradeNo			 							VARCHAR(4);			## 영수증번호
    DECLARE vReorderOrderNo									VARCHAR(30);		## 재주문 주문번호 (신규채번값)

	DECLARE vLgwBatchNo           							BIGINT DEFAULT 0;	## 이력번호(order_batch_history)
    DECLARE vFailCount           							BIGINT DEFAULT 0;	## 실패건수(order_batch_history)
    DECLARE vSuccessCount        							BIGINT DEFAULT 0;	## 성공건수(order_batch_history)
    DECLARE vTotalCount          							BIGINT DEFAULT 0;	## 전체건수(order_batch_history)
    DECLARE vErrorMessage        							VARCHAR(2000);		## 오류메시지(order_batch_history)
	DECLARE vProcedureNm		 							VARCHAR(100) DEFAULT 'sp_reorder_if_pine_trade_info';	## 프로시져명

	DECLARE vErrorNo             							INT DEFAULT 0;		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlState            							VARCHAR(1000);		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlErrorNo          							INT DEFAULT 0;		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlMessage          							VARCHAR(1000);		## SQLEXCEPTION 핸들러 변수
    DECLARE vLogPurchaseStoreInfo 							VARCHAR(1000);		## 전송대상 주문번호 / 클레임점포주문번호

    DECLARE vLogLevelINFO									VARCHAR(20) DEFAULT 'INFO';
    DECLARE vLogLevelERROR									VARCHAR(20) DEFAULT 'ERROR';

	#주문전송대상
	DECLARE cursorPurchaseOrder CURSOR FOR
		SELECT rpsi.claim_store_info_no
		FROM reorder_purchase_store_info rpsi
			INNER JOIN rf_trade_header rth ON rth.ord_no = CAST(rpsi.claim_store_info_no AS CHAR(30))
			AND rth.ftype_code = '2'  /*클레임데이터만 조회*/
		WHERE rpsi.reg_dt >= CURDATE()
		AND rpsi.pine_if_yn = 'N'
		;

	#전송완료 변경대상
	DECLARE cursorPurchaseOrderFinal CURSOR FOR
		SELECT rpsi.claim_store_info_no
		FROM reorder_purchase_store_info rpsi
		WHERE rpsi.reg_dt >= CURDATE()
		AND rpsi.pine_if_yn = 'I'
		;

	#점포주문번호 전송대상
	DECLARE cursorPurchaseStoreInfo CURSOR FOR
		SELECT rpsi.order_no                         AS reorder_order_no
		     , rpsi.claim_no                         AS claim_no
		     , rpsi.claim_store_info_no              AS claim_store_info_no
			 , LPAD(rpsi.origin_store_id, 4, '0')    AS store_id
			 , LPAD(rpsi.pos_no, 6, '0')             AS pos_no
			 , DATE_FORMAT(rpsi.trade_dt, '%Y%m%d')  AS trade_dt
			 , LPAD(rpsi.trade_no, 4, '0')           AS trade_no
		FROM reorder_purchase_store_info rpsi
		WHERE rpsi.reg_dt >= CURDATE()
		AND rpsi.pine_if_yn = 'I'
		;

	DECLARE CONTINUE HANDLER FOR NOT FOUND          ## 루프 핸들러
		SET vDone = TRUE;

	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION       ## SQLEXCEPTION 핸들러
		BEGIN
            GET DIAGNOSTICS CONDITION 1 vSqlState = RETURNED_SQLSTATE, vSqlErrorNo = MYSQL_ERRNO, vSqlMessage = MESSAGE_TEXT;
            SET vErrorNo =  -1;
        END;

    ## tx_isolation변경
    SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;

    ## 배치로그 시작
	CALL sp_lgw_batch_history(vProcedureNm, 'START', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
	SET vLgwBatchNo = @outLgwBatchNo;

	PURCHASE_ORDER:BEGIN
		## 1. PINE 인터페이스 대상 데이터 인터페이스여부 업데이트
		## 커서 시작 : cursorPurchaseOrder
		OPEN cursorPurchaseOrder;
			SELECT FOUND_ROWS() INTO vTargetCount;

			## 인터페이스 대상이 없을 경우 프로시저 종료
			IF vTargetCount = 0 THEN
				## log insert - 종료: 대상없음
				CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwBatchNo, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
                SET vDone = FALSE;
				LEAVE PURCHASE_ORDER;
			END IF;

			## 작업대상 데이터 업데이트 (pine_if_yn : N(처리대기) -> I(처리중))
			read_po_loop: LOOP

				FETCH cursorPurchaseOrder INTO vClaimStoreInfoNo;

				IF NOT vDone THEN
					UPDATE reorder_purchase_store_info
					SET pine_if_yn = 'I'
					  , chg_dt = NOW()
					WHERE claim_store_info_no = vClaimStoreInfoNo
					AND pine_if_yn = 'N';
				END IF;

				IF vDone THEN
					LEAVE read_po_loop;
				END IF;

			END LOOP read_po_loop;

			SET vDone = FALSE;
		CLOSE cursorPurchaseOrder;
	END PURCHASE_ORDER;


	########## TRAN 데이터 생성 (시작)

	## 2. PINE 인터페이스 대상 데이터 적재
	## 커서 시작 : cursorPurchaseStoreInfo
	OPEN cursorPurchaseStoreInfo;
		SELECT FOUND_ROWS() INTO vTargetCount;

		read_psi_loop: REPEAT

		FETCH cursorPurchaseStoreInfo INTO vReorderOrderNo, vClaimNo, vClaimStoreInfoNo, vStoreId, vPosNo, vTradeDt, vTradeNo;

		SET vErrorNo = 0;
		SET vCheckRowCnt = 0;
		SET vLogPurchaseStoreInfo = CONCAT('[주문번호]:',vReorderOrderNo,'[클레임점포주문번호]:',vClaimStoreInfoNo);

			IF NOT vDone THEN
				SET vTotalCount = vTotalCount + 1;

				## 프로시저 작업대상 처리시작
				START TRANSACTION;

				## 2-1) rf_trade_item 적재 : 2021-01-26, dc_rate/dc_cost/tax_rate/tax_amount 확인중 (전용석 차장님)
				INSERT INTO rf_trade_item (
				       ord_no
				     , good_id
					 , store_id
					 , pos_no
					 , trade_date
					 , trade_no
					 , item_seq
					 , print_item_name
					 , delivery_dt
					 , shift_id
					 , slot_id
					 , link_fraction
					 , trade_fraction
					 , input_fraction
					 , item_fraction
					 , plu_fraction
					 , plu_search_fraction
					 , event_fraction
					 , plu_code
					 , source_code
					 , item_qty
					 , item_cost
					 , item_common_cost
					 , modify_cost
					 , normal_cost
					 , special_rate
					 , special_cost
					 , dc_rate
					 , dc_cost
					 , sub_dc_rate
					 , sub_dc_cost
					 , link_cost
					 , link_code
					 , vender_code
					 , uom_code
					 , weight_uom
					 , tax_amount
					 , tax_fraction
					 , tax_rate
					 , rental_flag
					 , promo_no
					 , promo_detail_no
					 , promo_type
					 , promo_dc_type
					 , promo_dc_amount
					 , coupon_type
					 , coupon_dc_amount
					 , coupon_apply_qty
					 , mix_match_type
					 , multi_unit_qty
					 , multi_unit_price
					 , reason_code
					 , sum_result_amount
					 , card_dc_amount
					 , card_apply_qty
					 , emp_dc_amount
					 , mall_style_type
					 , fc_good_yn
					 , reg_dt
				)
				SELECT /* RF_TRADE_ITEM 대상조회 */
				       vReorderOrderNo                         AS ord_no              /*주문번호*/
				     , rti.good_id                             AS good_id             /*상품번호*/
                     , vStoreId								   AS store_id            /*점포ID*/
				     , vPosNo								   AS pos_no              /*POS번호*/
				     , vTradeDt								   AS trade_date          /*주문일자(YYYYMMDD)*/
				     , vTradeNo         					   AS trade_no            /*영수증번호*/
				     , rti.item_seq                            AS item_seq            /*상품순번*/
				     , rti.print_item_name                     AS print_item_name     /*상품명칭*/
				     , rti.delivery_dt                         AS delivery_dt         /*배송요청일*/
				     , rti.shift_id                            AS shift_id            /*주문쉬프트ID*/
				     , rti.slot_id                             AS slot_id             /*주문슬롯ID*/
				     , rti.link_fraction                       AS link_fraction       /*공병구분*/
				     , rti.trade_fraction                      AS trade_fraction      /*거래구분*/
				     , rti.input_fraction                      AS input_fraction      /*등록구분*/
				     , rti.item_fraction                       AS item_fraction       /*상품구분*/
				     , rti.plu_fraction                        AS plu_fraction        /*상품종류*/
				     , rti.plu_search_fraction                 AS plu_search_fraction /*단품조회구분*/
				     , rti.event_fraction                      AS event_fraction      /*행사구분(Y:적용,N:미적용)*/
				     , rti.plu_code                            AS plu_code            /*단품코드*/
				     , rti.source_code                         AS source_code         /*소스코드*/
				     , rti.item_qty                            AS item_qty            /*수량(TD=item_qty,DS=1)*/
				     , rti.item_cost                           AS item_cost           /*단가(TD=item_price,DS=SUM(price * qty)-SUM(discount_amt))*/
				     , rti.item_common_cost                    AS item_common_cost    /*구단가*/
				     , rti.modify_cost                         AS modify_cost         /*매가변경금액(TD=NULL,DS=0)*/
				     , rti.normal_cost                         AS normal_cost         /*정상단가*/
				     , rti.special_rate                        AS special_rate        /*할인특매율*/
				     , rti.special_cost                        AS special_cost        /*할인특매금액*/
				     , rti.dc_rate                             AS dc_rate             /*에누리율*/ -- (에누리금액/(단가*수량))*100, 소수점버림
				     , rti.dc_cost                             AS dc_cost             /*에누리금액*/
				     , rti.sub_dc_rate                         AS sub_dc_rate
				     , rti.sub_dc_cost                         AS sub_dc_cost
				     , rti.link_cost                           AS link_cost           /*공병단가*/
				     , rti.link_code                           AS link_code           /*공병코드*/
				     , rti.vender_code                         AS vender_code         /*거래처코드*/
				     , rti.uom_code                            AS uom_code            /*UOM Code*/
				     , rti.weight_uom                          AS weight_uom          /*중량단위*/
				     , IFNULL(rti.tax_amount,0)                AS tax_amount          /*과세금액*/
				     , rti.tax_fraction                        AS tax_fraction        /*과세구분(0:과세,1:영세,2:면세)*/
				     , rti.tax_rate                            AS tax_rate            /*과세율*/
				     , rti.rental_flag                         AS rental_flag         /*임대구분*/
				     , rti.promo_no                            AS promo_no            /*프로모션번호*/
				     , rti.promo_detail_no                     AS promo_detail_no     /*프로모션상세번호*/
				     , rti.promo_type                          AS promo_type          /*프로모션타입*/
				     , rti.promo_dc_type                       AS promo_dc_type       /*프로모션DC타입*/
				     , IFNULL(rti.promo_dc_amount,0)           AS promo_dc_amount     /*프로모션DC금액*/
                     , rti.coupon_type                     	   AS coupon_type         /*쿠폰타입*/
				     , rti.coupon_dc_amount  			       AS coupon_dc_amount    /*쿠폰DC금액*/
				     , rti.coupon_apply_qty                    AS coupon_apply_qty    /*쿠폰적용수량*/
				     , rti.mix_match_type                      AS mix_match_type      /*MIX_MATCH 타입*/
				     , rti.multi_unit_qty                      AS multi_unit_qty      /*MULTI_UNIT 단위수량*/
				     , rti.multi_unit_price                    AS multi_unit_price    /*MULTI_UNIT 금액*/
				     , ''                                      AS reason_code         /*사유코드*/
				     , rti.sum_result_amount                   AS sum_result_amount   /*상품최종금액*/ -- 단가 * 수량 - 총에누리금액
				     , rti.card_dc_amount                      AS card_dc_amount      /*카드즉시할인금액*/
				     , rti.card_apply_qty                      AS card_apply_qty      /*카드즉시할인수량*/
				     , rti.emp_dc_amount               	       AS emp_dc_amount       /*직원할인금액*/
				     , rti.mall_style_type                 	   AS mall_style_type     /*TD DS*/
				     , IFNULL(rti.fc_good_yn,'N')              AS fc_good_yn          /*FC상품여부*/
				     , NOW()                                   AS reg_dt
				FROM rf_trade_item rti
				WHERE rti.ord_no = vClaimStoreInfoNo;

				## 대상건수 기록 : rf_trade_item
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE reorder_purchase_store_info
					SET pine_if_yn = 'E'
					  , chg_dt = NOW()
					WHERE claim_store_info_no = vClaimStoreInfoNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_item ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


                ## 2-2) rf_trade_ticket 적재 : ftype_code, ticket_no 확인 필요
                INSERT INTO rf_trade_ticket (
				       ord_no
				     , ticket_seq
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , ftype_code
				     , ticket_fraction
				     , ticket_company_code
				     , ticket_sign_amount
				     , ticket_sale_amount
				     , ticket_change_amount
				     , ticket_no
				     , ticket_cnt
				     , ticket_kind
				     , approval_no
				     , reg_dt
				)
				SELECT /* RF_TRADE_TICKET 대상조회 */
				       vReorderOrderNo                  AS ord_no               /*주문번호*/
				     , rtt.ticket_seq               	AS ticket_seq           /*상품권순번*/
				     , vStoreId							AS store_id             /*점포ID*/
				     , vPosNo							AS pos_no               /*POS번호*/
				     , vTradeDt							AS trade_date           /*주문일자(YYYYMMDD)*/
				     , vTradeNo         				AS trade_no             /*영수증번호*/
				     , '1'                              AS ftype_code           /*거래구분(1:통상,2:반품)*/
				     , '87'                             AS ticket_fraction      /*상품권구분(81:자사금권,87:타사금권)*/
				     , '52'                             AS ticket_company_code  /*상품권회사코드(52:사은쿠폰,60:자사상품권)*/
				     , rtt.ticket_sign_amount           AS ticket_sign_amount   /*상품권액면금액-쿠폰최소적용금액 : 장바구니쿠폰 1, 마일리지 10*/
				     , rtt.ticket_sale_amount           AS ticket_sale_amount   /*상품권매출금액-장바구니쿠폰사용금액*/
				     , rtt.ticket_change_amount			AS ticket_change_amount	/*상품금액 + 배송비금액 - 상품할인 - 행사할인 - 임직원할인 - 카드상품할인 - 배송비할인 금액임 */
				     , rtt.ticket_no                    AS ticket_no            /*상품권번호*/
				     , 1                                AS ticket_cnt           /*상품권수량*/
				     , '00'                             AS ticket_kind          /*상품권권종(00:사은쿠폰,##:그외)*/
				     , ''                               AS approval_no          /*승인번호*/
				     , NOW()                            AS reg_dt
				FROM rf_trade_ticket rtt
				WHERE rtt.ord_no = vClaimStoreInfoNo;

				## 대상건수 기록 : rf_trade_ticket
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE reorder_purchase_store_info
					SET pine_if_yn = 'E'
					  , chg_dt = NOW()
					WHERE claim_store_info_no = vClaimStoreInfoNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_ticket ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## 2-3) rf_trade_coupon 적재
				INSERT INTO rf_trade_coupon (
				       ord_no
				     , coupon_code
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , coupon_name
				     , coupon_startdate
				     , coupon_enddate
				     , coupon_type
				     , coupon_value
				     , coupon_limit
				     , coupon_condition
				     , item_qty
				     , coupon_item_qty
				     , coupon_qty
				     , coupon_amount
				     , unit_id
				     , reg_dt
				)
				SELECT /* RF_TRADE_COUPON 대상조회 (쿠폰마스터) */
				       vReorderOrderNo              AS ord_no
				     , rtc.coupon_code 		        AS coupon_code
   				     , vStoreId						AS store_id
				     , vPosNo						AS pos_no
				     , vTradeDt						AS trade_date
				     , vTradeNo         			AS trade_no
				     , rtc.coupon_name              AS coupon_name
				     , rtc.coupon_startdate			AS coupon_startdate
				     , rtc.coupon_enddate   		AS coupon_enddate
                     , rtc.coupon_type              AS coupon_type
				     , rtc.coupon_value             AS coupon_value
				     , rtc.coupon_limit             AS coupon_limit
				     , rtc.coupon_condition         AS coupon_condition
				     , rtc.item_qty                 AS item_qty
				     , rtc.coupon_item_qty          AS coupon_item_qty
				     , rtc.coupon_qty               AS coupon_qty
				     , rtc.coupon_amount            AS coupon_amount
				     , rtc.unit_id                  AS unit_id
				     , NOW()                        AS reg_dt
				FROM rf_trade_coupon rtc
				WHERE rtc.ord_no = vClaimStoreInfoNo;

				## 대상건수 기록 : rf_trade_coupon
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE reorder_purchase_store_info
					SET pine_if_yn = 'E'
					  , chg_dt = NOW()
					WHERE claim_store_info_no = vClaimStoreInfoNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_coupon ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## 2-4) rf_trade_coupon_item 적재
				INSERT INTO rf_trade_coupon_item (
				       ord_no
				     , coupon_code
				     , plu_code
				     , plu_seq
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , coupon_type
				     , item_qty
				     , coupon_item_qty
				     , coupon_qty
				     , coupon_amount
				     , reg_dt
				)
				SELECT /* RF_TRADE_COUPON_ITEM 대상조회 */
				       vReorderOrderNo				AS ord_no
				     , rtci.coupon_code				AS coupon_code
				     , rtci.plu_code 				AS plu_code
				     , rtci.plu_seq  				AS plu_seq
				     , vStoreId						AS store_id
				     , vPosNo						AS pos_no
				     , vTradeDt						AS trade_date
				     , vTradeNo         			AS trade_no
				     , rtci.coupon_type 			AS coupon_type
				     , rtci.item_qty 				AS item_qty
				     , rtci.coupon_item_qty			AS coupon_item_qty
				     , rtci.coupon_qty 				AS coupon_qty
				     , rtci.coupon_amount 			AS coupon_amount
				     , NOW()                        AS reg_dt
				FROM rf_trade_coupon_item rtci
				WHERE rtci.ord_no = vClaimStoreInfoNo;

				## 대상건수 기록 : rf_trade_coupon_item
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE reorder_purchase_store_info
					SET pine_if_yn = 'E'
					  , chg_dt = NOW()
					WHERE claim_store_info_no = vClaimStoreInfoNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_coupon_item ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## 2-5) rf_trade_dc_coupon 적재
				INSERT INTO rf_trade_dc_coupon(
				       ord_no
				     , coupon_seq
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , coupon_fraction
				     , coupon_no
				     , plu_code
				     , dc_amount
				     , reg_dt
				)
                SELECT /* RF_TRADE_DC_COUPON 대상조회 */
				       vReorderOrderNo				AS ord_no				/*주문번호*/
				     , rtdc.coupon_seq         		AS coupon_seq			/*쿠폰순번*/
				     , vStoreId						AS store_id				/*점포ID*/
				     , vPosNo						AS pos_no				/*POS번호*/
				     , vTradeDt						AS trade_date			/*주문일자(YYYYMMDD)*/
				     , vTradeNo         			AS trade_no				/*영수증번호*/
				     , rtdc.coupon_fraction			AS coupon_fraction		/*쿠폰구분(1:에누리,2:CMS)*/
				     , rtdc.coupon_no   			AS coupon_no			/*쿠폰번호(barcode)*/
				     , rtdc.plu_code 				AS plu_code				/*단품코드*/
				     , rtdc.dc_amount   			AS dc_amount  			/*에누리한 순수금액*/
				     , NOW()                        AS reg_dt
				FROM rf_trade_dc_coupon rtdc
				WHERE rtdc.ord_no = vClaimStoreInfoNo;

				## 대상건수 기록 : rf_trade_dc_coupon
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE reorder_purchase_store_info
					SET pine_if_yn = 'E'
					  , chg_dt = NOW()
					WHERE claim_store_info_no = vClaimStoreInfoNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_dc_coupon ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## 2-6) rf_trade_dc_emp 적재
				INSERT INTO rf_trade_dc_emp (
				       ord_no
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , dc_type
				     , item_qty
				     , emp_id
				     , emp_nm
				     , emp_kind
				     , emp_dc_amount
				     , trade_amount
				     , dc_balance
				     , point_deduction
				     , reg_dt
				)
				SELECT /* RF_TRADE_DC_EMP 대상조회 */
				       vReorderOrderNo              AS ord_no
                     , vStoreId						AS store_id
				     , vPosNo						AS pos_no
				     , vTradeDt						AS trade_date
				     , vTradeNo         			AS trade_no
                     , rtde.dc_type                 AS dc_type
				     , rtde.item_qty  				AS item_qty
				     , rtde.emp_id                  AS emp_id
				     , rtde.emp_nm           		AS emp_nm
				     , rtde.emp_kind                AS emp_kind
				     , rtde.emp_dc_amount           AS emp_dc_amount
				     , rtde.trade_amount         	AS trade_amount
				     , rtde.dc_balance              AS dc_balance
				     , rtde.point_deduction			AS point_deduction
				     , NOW()                        AS reg_dt
				FROM rf_trade_dc_emp rtde
				WHERE rtde.ord_no = vClaimStoreInfoNo;

				## 대상건수 기록 : rf_trade_dc_emp
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE reorder_purchase_store_info
					SET pine_if_yn = 'E'
					  , chg_dt = NOW()
					WHERE claim_store_info_no = vClaimStoreInfoNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_dc_emp ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## 2-7) rf_trade_dc_emp_item 적재 - 임직원할인 상품
				INSERT INTO rf_trade_dc_emp_item (
				       ord_no
				     , plu_code
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , emp_seq
				     , goodkr_nm2
				     , item_qty
				     , item_sum_amount
				     , dc_rate
				     , dc_amount
				     , link_amount
				     , section_no
				     , reg_dt
				)
				SELECT /* RF_TRADE_DC_EMP_ITEM 대상조회 */
				       vReorderOrderNo                  AS ord_no
				     , rtdei.plu_code                   AS plu_code
                     , vStoreId							AS store_id
				     , vPosNo							AS pos_no
				     , vTradeDt							AS trade_date
				     , vTradeNo         				AS trade_no
				     , rtdei.emp_seq                    AS emp_seq
				     , rtdei.goodkr_nm2                 AS goodkr_nm2
				     , rtdei.item_qty                   AS item_qty
				     , rtdei.item_sum_amount			AS item_sum_amount
				     , rtdei.dc_rate                	AS dc_rate
				     , rtdei.dc_amount                 	AS dc_amount
				     , rtdei.link_amount                AS link_amount
				     , rtdei.section_no                 AS section_no
				     , NOW()                            AS reg_dt
				FROM rf_trade_dc_emp_item rtdei
				WHERE rtdei.ord_no = vClaimStoreInfoNo;

				## 대상건수 기록 : rf_trade_dc_emp_item
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE reorder_purchase_store_info
					SET pine_if_yn = 'E'
					  , chg_dt = NOW()
					WHERE claim_store_info_no = vClaimStoreInfoNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_dc_emp_item ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## 2-8) rf_trade_dc_card 적재 - 카드상품할인
				INSERT INTO rf_trade_dc_card (
				       ord_no
				     , good_id
				     , plu_seq
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , plu_code
				     , item_qty
				     , item_cost
				     , coupon_qty
				     , supp_ty
				     , card_dc_amount
				     , couponbar_cd
				     , card_type
				     , card_type_nm
				     , card_dc_prefix
				     , reg_dt
				)
				SELECT /* RF_TRADE_DC_CARD 대상조회 */
				       vReorderOrderNo                  AS ord_no
				     , rtdc.good_id                     AS good_id
				     , rtdc.plu_seq                     AS plu_seq
                     , vStoreId							AS store_id
				     , vPosNo							AS pos_no
				     , vTradeDt							AS trade_date
				     , vTradeNo         				AS trade_no
				     , rtdc.plu_code                    AS plu_code
				     , rtdc.item_qty                    AS item_qty
				     , rtdc.item_cost                   AS item_cost
				     , rtdc.coupon_qty                 	AS coupon_qty
				     , rtdc.supp_ty                     AS supp_ty
				     , rtdc.card_dc_amount			  	AS card_dc_amount
				     , rtdc.couponbar_cd                AS couponbar_cd
				     , rtdc.card_type                   AS card_type
				     , rtdc.card_type_nm                AS card_type_nm
				     , rtdc.card_dc_prefix              AS card_dc_prefix
				     , NOW()                            AS reg_dt
				FROM rf_trade_dc_card rtdc
				WHERE rtdc.ord_no = vClaimStoreInfoNo;

				## 대상건수 기록 : rf_trade_dc_card
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE reorder_purchase_store_info
					SET pine_if_yn = 'E'
					  , chg_dt = NOW()
					WHERE claim_store_info_no = vClaimStoreInfoNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_dc_card ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## 2-9) rf_trade_media 적재 - 지불수단
				INSERT INTO rf_trade_media (
				       ord_no
				     , tran_media_fraction
				     , store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , reg_dt
				)
				SELECT /* RF_TRADE_MEDIA 대상조회 PG */
				       vReorderOrderNo					AS ord_no
				     , rtm.tran_media_fraction			AS tran_media_fraction /* (6:장바구니쿠폰,7:상품할인쿠폰,11:제휴,31:PG) */
                     , vStoreId							AS store_id
				     , vPosNo							AS pos_no
				     , vTradeDt							AS trade_date
				     , vTradeNo         				AS trade_no
				     , NOW()                            AS reg_dt
				FROM rf_trade_media rtm
				WHERE rtm.ord_no = vClaimStoreInfoNo;

				## 대상건수 기록 : rf_trade_media
				SELECT FOUND_ROWS() INTO vTargetCount;
				SET vCheckRowCnt = vCheckRowCnt + vTargetCount;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE reorder_purchase_store_info
					SET pine_if_yn = 'E'
					  , chg_dt = NOW()
					WHERE claim_store_info_no = vClaimStoreInfoNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_media ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;


				## 2-10) rf_trade_header 적재 - 헤더 : 마지막
				INSERT INTO rf_trade_header (
				       ord_no
					 , cust_ord_no
					 , user_id
					 , store_id
					 , pos_no
					 , trade_date
					 , trade_no
					 , trade_time
					 , ftype_code
					 , account_fraction
					 , summary_fraction
					 , checker_no
					 , delivery_dt
					 , delivery_cd
					 , shift_id
					 , slot_id
					 , van_id
					 , ord_ty
					 , trade_fraction
					 , trade_count
					 , trade_change_amount
					 , trade_delivery_amount
					 , trade_bottle_amount
					 , trade_onetool_amount
					 , trade_tot_amount
					 , trade_amount
					 , trade_dc_amount
					 , trade_receive_amount
					 , rtn_ord_no
					 , rtn_store_code
					 , rtn_pos_no
					 , rtn_trade_date
					 , rtn_trade_no
					 , rtn_fault_fraction
					 , part_cancel
					 , part_cancel_flag
					 , subtotal_discount_rate
					 , subtotal_discount_amount
					 , subtotal_discount_fraction
					 , pg_amount
					 , special_amount
					 , card_pay
					 , digital_pay
					 , card_dc_amount
					 , emp_dc_amount
					 , head_office_code
					 , area_code
					 , noncust_yn
					 , mall_delivery_type
					 , rcptzip_txt
					 , rcptadd1_txt
					 , fc_store_id
					 , check_row_cnt
					 , reg_dt
				)
				SELECT /* RF_TRADE_HEADER 대상조회 */
				       vReorderOrderNo					AS ord_no                /*매출주문번호*/
				     , vClaimNo                			AS cust_ord_no           /*고객주문번호*/
				     , rth.user_id                      AS user_id               /*고객ID*/
                     , vStoreId							AS store_id
				     , vPosNo							AS pos_no
				     , vTradeDt							AS trade_date
				     , vTradeNo         				AS trade_no
				     , rth.trade_time					AS trade_time            /*주문시간(HHmmss)*/
				     , '1'                              AS ftype_code            /*거래구분(1:주문,2:취소/반품)*/
				     , rth.account_fraction				AS account_fraction		 /*회계구분(0:정상매출(=주문),3:특판(=제휴),SOD(37:POS개시,32:SIGN-ON), EOD(33:SIGN-OFF,정산:35) */
				     , '0'                              AS summary_fraction		 /*집계구분(0:정상처리,1:일괄취소)*/
				     , '1010101010100'                  AS checker_no            /*체커NO*/
                     , rth.delivery_dt					AS delivery_dt
				     , rth.delivery_cd					AS delivery_cd           /*배송구분(37010:자차,37020:택배,37030:픽업)*/
				     , rth.shift_id						AS shift_id				 /*주문쉬프트ID, DS:null*/
				     , rth.slot_id						AS slot_id				 /*주문슬롯ID, DS:null*/
				     , rth.van_id						AS van_id                /*차량ID*/
				     , rth.ord_ty						AS ord_ty                /*주문구분*/
				     , rth.trade_fraction 				AS trade_fraction        /*결재구분(1:미결당사,6:통산)*/
				     , rth.trade_count					AS trade_count           /*거래수량*/
				     , rth.trade_change_amount			AS trade_change_amount   /*거스름돈*/
				     , rth.trade_delivery_amount        AS trade_delivery_amount /*배송료*/
				     , rth.trade_bottle_amount			AS trade_bottle_amount   /*공병가*/
				     , rth.trade_onetool_amount			AS trade_onetool_amount  /*일회용품가격*/
                     , rth.trade_tot_amount				AS trade_tot_amount      /*거래합계금액(할인전 총액)*/
				     , rth.trade_amount					AS trade_amount          /*거래금액(할인후 총액)*/
					 , rth.trade_dc_amount				AS trade_dc_amount       /*소계전에누리(할인합계)*/
				     , rth.trade_receive_amount			AS trade_receive_amount  /*받은돈*/
				     , NULL								AS rtn_ord_no            /*원주문번호(재주문이므로 NULL처리)*/
				     , NULL								AS rtn_store_code        /*원주문점포ID(재주문이므로 NULL처리)*/
				     , NULL								AS rtn_pos_no            /*원주문POS번호(재주문이므로 NULL처리)*/
				     , NULL								AS rtn_trade_date        /*원주문주문일(재주문이므로 NULL처리)*/
				     , NULL								AS rtn_trade_no          /*원주문영수증번호(재주문이므로 NULL처리)*/
				     , rth.rtn_fault_fraction			AS rtn_fault_fraction    /*반품오타구분*/
				     , '0'								AS part_cancel           /*부분취소상태(2:부분취소,4:부분반품)*/
				     , NULL								AS part_cancel_flag      /*부분취소관련주문여부*/
				     , rth.subtotal_discount_rate		AS subtotal_discount_rate
				     , rth.subtotal_discount_amount		AS subtotal_discount_amount
				     , rth.subtotal_discount_fraction	AS subtotal_discount_fraction
				     , rth.pg_amount					AS pg_amount             /*통합 TENDER 금액(카드+MHC+DGV+OCB)*/
				     , rth.special_amount				AS special_amount        /*할인특매*/
				     , rth.card_pay						AS card_pay              /*신용카드지불액*/
				     , rth.digital_pay					AS digital_pay           /*디지털상품권지불액*/
				     , rth.card_dc_amount				AS card_dc_amount        /*카드할인금액*/
				     , rth.emp_dc_amount				AS emp_dc_amount         /*직원할인금액*/
				     , (SELECT swi.head_office_code
				        FROM rf_sto_winpos_info swi
				        WHERE swi.store_id = vStoreId
				        AND swi.pos_no = vPosNo
				        LIMIT 1)						AS head_office_code      /*본부코드(세팅된 재전송 POS번호로 조회)*/
				     , (SELECT swi.area_code
				        FROM rf_sto_winpos_info swi
				        WHERE swi.store_id = vStoreId
				        AND swi.pos_no = vPosNo
				        LIMIT 1)						AS area_code             /*지역코드(세팅된 재전송 POS번호로 조회)*/
				     , rth.noncust_yn					AS noncust_yn            /*비회원여부*/
				     , rth.mall_delivery_type			AS mall_delivery_type    /*("31010":TD, "31020":DS, "31030":TD/DS)*/
					 , rth.rcptzip_txt					AS rcptzip_txt           /*수취인우편번호*/
                     , rth.rcptadd1_txt					AS rcptadd1_txt          /*수취인우편주소*/
					 , rth.fc_store_id					AS fc_store_id           /*FC점포코드*/ -- FC점포 존재하는 매장만 FC store_id 입력
					 , vCheckRowCnt						AS check_row_cnt         /*전송테이블검증*/ -- vCheckRowCnt(헤더 제외 모든 row count 누적건수)
					 , NOW()							AS reg_dt
				FROM rf_trade_header rth
				WHERE rth.ord_no = vClaimStoreInfoNo;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (pine_if_yn : I(처리중) -> E(에러))
					UPDATE reorder_purchase_store_info
					SET pine_if_yn = 'E'
					  , chg_dt = NOW()
					WHERE claim_store_info_no = vClaimStoreInfoNo
					AND pine_if_yn = 'I';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo,'INSERT rf_trade_header ' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_psi_loop;
                END IF;

                COMMIT; ## Transaction 최종 커밋

				SET vSuccessCount = vSuccessCount + 1;
				SET vDone = FALSE;

			END IF;

		UNTIL vDone
		END REPEAT read_psi_loop;

	CLOSE cursorPurchaseStoreInfo;

	########## TRAN 데이터 생성 (종료)


	PURCHASE_ORDER_FINAL:BEGIN
    	DECLARE CONTINUE HANDLER FOR NOT FOUND
			BEGIN
				SET vDoneFinal = TRUE;
			END;

		## 3. PINE 인터페이스 완료건 인터페이스여부 업데이트
		#전송완료변경대상 OPEN
		OPEN cursorPurchaseOrderFinal;
			SELECT FOUND_ROWS() INTO vTargetCount;

			## 인터페이스 대상이 없을 경우 프로시저 종료
			IF vTargetCount = 0 THEN
                SET vDoneFinal = FALSE;
				LEAVE PURCHASE_ORDER_FINAL;
			END IF;

			## 작업대상 데이터 업데이트 (pine_if_yn : I(처리중) -> Y(처리완료))
			read_po_loop_final: LOOP

				FETCH cursorPurchaseOrderFinal INTO vClaimStoreInfoNo;

					IF NOT vDoneFinal THEN
						## 완료건 업데이트
						UPDATE reorder_purchase_store_info
						SET pine_if_yn = 'Y'
						  , chg_dt = NOW()
						WHERE claim_store_info_no = vClaimStoreInfoNo
						AND pine_if_yn = 'I';
					END IF;

					IF vDoneFinal THEN
						LEAVE read_po_loop_final;
					END IF;

			END LOOP read_po_loop_final;

		CLOSE cursorPurchaseOrderFinal;
	END PURCHASE_ORDER_FINAL;

	## tx_isolation원복
    SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;

	## 배치로그 종료
	CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwBatchNo, vFailCount, vSuccessCount, vTotalCount, vErrorMessage, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);

END REORDER_PROCESS