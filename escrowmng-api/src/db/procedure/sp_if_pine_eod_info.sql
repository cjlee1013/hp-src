CREATE DEFINER=`app_escrow`@`%` PROCEDURE `escrow`.`sp_if_pine_eod_info`()
    COMMENT 'PINE 매출연동 Refit POS별 EOD 데이터 생성'
MAIN_PROCESS:BEGIN
/****************************************************************************************
- 목적/용도: Refit POS별 EOD 데이터 생성 프로시저 (Refit IF 테이블에 데이터 적재)
- 작성일자/작성자 : 2020-06-11 / 박준수
- 수정일자/수정내용/수정자 : 2020-08-13 / 인터페이스 테이블명 LGW 에 맞추어 변경 / 박준수
- 2020-08-18 / transaction commit Level 변경 : READ_COMMITTED / 송영준
  2020-11-17 / 모든 대상 테이블에 reg_dt 추가 적용 / 박준수
  2021-01-14 / 매출 재전송 POS 분기 추가 / 박준수
  2021-02-09 / 영업종료일에 EOD 생성하고 영업일자는 업데이트 하지 않음 / 박준수
****************************************************************************************/

	## 0. 프로시저 공통 변수 및 커서 선언
	DECLARE vDone           INT DEFAULT FALSE; ## 작업완료여부
	DECLARE vTargetCount    INT DEFAULT 0;     ## 대상건수
	DECLARE vLogSeq         INT DEFAULT 1;     ## 로그순번
	DECLARE vStoreId        VARCHAR(4);        ## 점포ID
	DECLARE vPosNo          VARCHAR(6);        ## POS번호

	DECLARE vSignOffTradeNo INT DEFAULT 0;     ## Sign-Off 영수증번호
	DECLARE vSettleTradeNo  INT DEFAULT 0;     ## 정산 영수증번호
	DECLARE vAccountOrdNo   VARCHAR(30);       ## Account 주문번호(rf_trade_account)
	DECLARE vAccountTradeNo VARCHAR(6);        ## Account 영수증번호(rf_trade_account)

	DECLARE vLgwHistoryNo      BIGINT DEFAULT 0;  ## 이력번호(lgw_batch_history)
    DECLARE vFailCount      BIGINT DEFAULT 0;  ## 실패건수(lgw_batch_history)
    DECLARE vSuccessCount   BIGINT DEFAULT 0;  ## 성공건수(lgw_batch_history)
    DECLARE vTotalCount     BIGINT DEFAULT 0;  ## 전체건수(lgw_batch_history)
    DECLARE vErrorMessage   VARCHAR(2000);     ## 오류메시지(lgw_batch_history)
    DECLARE vProcedureNm		 VARCHAR(100) DEFAULT 'sp_if_pine_eod_info';		## 프로시져명

	DECLARE vErrorNo        INT DEFAULT 0;	   ## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlState       VARCHAR(1000);	   ## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlErrorNo     INT DEFAULT 0;	   ## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlMessage     VARCHAR(1000);	   ## SQLEXCEPTION 핸들러 변수
    DECLARE vLogEodInfo		VARCHAR(1000);		## 오류저장 로그변수

    DECLARE tempStroeId		INT DEFAULT 0;		## 영수증 채번 테이블 index 수행을 위한 형변환 임시변수
    DECLARE tempPosNo		INT DEFAULT 0;		## 영수증 채번 테이블 index 수행을 위한 형변환 임시변수
    DECLARE strPosNo		VARCHAR(6);			## 영수증 채번 테이블 index 수행을 위한 형변환 임시변수

    DECLARE vBeforeTradeDt		VARCHAR(8);			##전일거래일자 (YYYYMMDD)

    DECLARE vLogLevelINFO		VARCHAR(20) DEFAULT 'INFO';
    DECLARE vLogLevelERROR		VARCHAR(20) DEFAULT 'ERROR';

	DECLARE cursorStoWinposInfoEod CURSOR FOR  ## POS마스터 커서
		SELECT swi.store_id
		     , swi.pos_no
		FROM rf_sto_winpos_info swi
		WHERE swi.pos_no NOT IN('000790') -- FC POS 제외
		AND CURDATE() BETWEEN swi.pos_active_start_date AND DATE_ADD(swi.pos_active_end_date, INTERVAL 1 DAY)
		ORDER BY swi.store_id, swi.pos_no;

	DECLARE CONTINUE HANDLER FOR NOT FOUND     ## 루프 핸들러
		SET vDone = TRUE;

	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION  ## SQLEXCEPTION 핸들러
		BEGIN
            GET DIAGNOSTICS CONDITION 1 vSqlState = RETURNED_SQLSTATE, vSqlErrorNo = MYSQL_ERRNO, vSqlMessage = MESSAGE_TEXT;
            SET vErrorNo =  -1;
        END;

	## tx_isolation변경
    SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;

	## 배치로그 시작
	CALL sp_lgw_batch_history(vProcedureNm, 'START', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
	SET vLgwHistoryNo = @outLgwBatchNo;

    SET vBeforeTradeDt = DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -1 DAY), '%Y%m%d');

	## 0. POS 마스터 정보 기준으로 작업 시작
	## 커서 시작 : cursorStoWinposInfoEod
	OPEN cursorStoWinposInfoEod;
		SELECT FOUND_ROWS() INTO vTargetCount;

		## 인터페이스 대상이 없을 경우 프로시저 종료
		IF vTargetCount = 0 THEN
			## log insert - 종료: 대상없음
            CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwHistoryNo, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
			## tx_isolation원복
			SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
			LEAVE MAIN_PROCESS;
		END IF;

		## 커서 내 loop 시작
		read_swi_loop: REPEAT

			FETCH cursorStoWinposInfoEod INTO vStoreId, vPosNo;

            SET vErrorNo = 0;
			SET vLogEodInfo = CONCAT('[storeId]:',vStoreId,'[posNo]: ',vPosNo);

			IF NOT vDone THEN
				SET vTotalCount = vTotalCount + 1;

				START TRANSACTION;
                -- pos_receipt_issue / claim_pos_receipt_issue Primary Key Index 수행을 위한 형변환(2020.10.10)
				SET tempStroeId = vStoreId+0;
                SET tempPosNo = vPosNo+0;
                SET strPosNo = CONVERT(tempPosNo,CHAR);

				## rf_trade_sign 적재 전, trade_no 전일자 마지막번호 +1, +2 세팅
				IF vPosNo = '000785' OR vPosNo = '000786' OR vPosNo = '000789' OR vPosNo = '000793' OR vPosNo = '000794' OR vPosNo = '000720' THEN
					# Cliam 영수증번호 END 처리
					SELECT MAX(pri.trade_no)+1
						 , MAX(pri.trade_no)+2
					INTO vSignOffTradeNo
					   , vSettleTradeNo
					FROM claim_pos_receipt_issue pri
					WHERE pri.store_id = tempStroeId
					AND pri.pos_no = strPosNo
					AND pri.trade_dt = vBeforeTradeDt;

                ELSE

                	IF vPosNo = '000787' OR vPosNo = '000788' OR vPosNo = '000796' OR vPosNo = '000700' OR vPosNo = '000769' THEN
                		# 매출 재전송 영수증번호 END 처리
						SELECT MAX(pri.trade_no)+1
							 , MAX(pri.trade_no)+2
						INTO vSignOffTradeNo
						   , vSettleTradeNo
						FROM resend_pos_receipt_issue pri
						WHERE pri.store_id = tempStroeId
						AND pri.pos_no = strPosNo
						AND pri.trade_dt = vBeforeTradeDt;

					ELSE
						# 주문 영수증번호 END 처리
						SELECT MAX(pri.trade_no)+1
							 , MAX(pri.trade_no)+2
						INTO vSignOffTradeNo
						   , vSettleTradeNo
						FROM pos_receipt_issue pri
						WHERE pri.store_id = tempStroeId
						AND pri.pos_no = strPosNo
						AND pri.trade_dt = vBeforeTradeDt;

					END IF;

                END IF;


				## 1. rf_trade_sign 적재
				INSERT INTO rf_trade_sign (
				       store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , sign_fraction
				     , checker_no
				     , reg_dt
				)
				VALUES (
				       vStoreId
				     , vPosNo
				     , vBeforeTradeDt
				     , LPAD(IFNULL(vSignOffTradeNo, 3), 4, '0')
				     , '2' -- sign-off: header.account_fraction=33
				     , '1010101010100'
				     , NOW()
				),
				       (
				       vStoreId
				     , vPosNo
				     , vBeforeTradeDt
				     , LPAD(IFNULL(vSettleTradeNo, 4), 4, '0')
				     , '4' -- 정산: header.account_fraction=35
				     , '1010101010100'
				     , NOW()
				);


				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## log insert - 에러 종료
                    SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwHistoryNo, vLogLevelINFO, vLogEodInfo,'INSERT rf_trade_sign' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_swi_loop;
                END IF;


                ## 2. rf_trade_header 적재
                INSERT INTO rf_trade_header (
				       ord_no
					 , cust_ord_no
					 , store_id
					 , pos_no
					 , trade_date
					 , trade_no
					 , trade_time
					 , ftype_code
					 , account_fraction
					 , summary_fraction
					 , checker_no
					 , delivery_cd
					 , trade_fraction
					 , trade_count
					 , trade_change_amount
					 , trade_delivery_amount
					 , trade_bottle_amount
					 , trade_onetool_amount
					 , rtn_fault_fraction
					 , part_cancel
					 , part_cancel_flag
					 , subtotal_discount_rate
					 , subtotal_discount_amount
					 , subtotal_discount_fraction
					 , special_amount
					 , head_office_code
					 , area_code
					 , noncust_yn
					 , mall_delivery_type
					 , check_row_cnt
					 , reg_dt
				)
				SELECT
				       its.ord_no                   AS ORD_NO
				     , its.ord_no                   AS CUST_ORD_NO
				     , its.store_id                 AS STORE_ID
				     , its.pos_no                   AS POS_NO
				     , its.trade_date               AS TRADE_DATE
				     , its.trade_no                 AS TRADE_NO
				     , DATE_FORMAT(NOW(), '%H%i%s') AS TRADE_TIME
				     , CASE its.sign_fraction
				       WHEN '2' THEN '1'
				       WHEN '4' THEN '4'
				       ELSE ''
				       END                          AS FTYPE_CODE
				     , CASE its.sign_fraction
				       WHEN '2' THEN '33'
				       WHEN '4' THEN '35'
				       ELSE ''
				       END                          AS ACCOUNT_FRACTION
				     , '0'                          AS SUMMARY_FRACTION
				     , '1010101010100'              AS CHECKER_NO
				     , '37010'                      AS DELIVERY_CD
					 , 6                            AS TRADE_FRACTION
					 , 0                            AS TRADE_COUNT
					 , 0                            AS TRADE_CHANGE_AMOUNT
					 , 0                            AS TRADE_DELIVERY_AMOUNT
					 , 0                            AS TRADE_BOTTLE_AMOUNT
					 , 0                            AS TRADE_ONETOOL_AMOUNT
					 , '0'                          AS RTN_FAULT_FRACTION
					 , '0'                          AS PART_CANCEL
					 , 'N'                          AS part_cancel_flag
					 , 0                            AS SUBTOTAL_DISCOUNT_RATE
					 , 0                            AS SUBTOTAL_DISCOUNT_AMOUNT
					 , '0'                          AS SUBTOTAL_DISCOUNT_FRACTION
					 , 0                            AS SPECIAL_AMOUNT
					 , swi.head_office_code         AS HEAD_OFFICE_CODE
					 , swi.area_code                AS AREA_CODE
					 , 'N'                          AS NONCUST_YN
					 , '31010'                      AS MALL_DELIVERY_TYPE
					 , CASE its.sign_fraction
				       WHEN '2' THEN 1
				       WHEN '4' THEN 2
				       ELSE 0
				       END                          AS CHECK_ROW_CNT
				     , NOW()                        AS REG_DT
				FROM rf_trade_sign its
					INNER JOIN rf_sto_winpos_info swi ON its.store_id = swi.store_id AND its.pos_no = swi.pos_no
			    WHERE its.store_id = vStoreId
			    AND its.pos_no = vPosNo
			    AND its.trade_date = vBeforeTradeDt
			    AND its.sign_fraction IN ('2', '4');

			    ## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## log insert - 에러 종료
                    SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwHistoryNo, vLogLevelINFO, vLogEodInfo,'INSERT rf_trade_header' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_swi_loop;
                END IF;


                ## 정산TRAN 적재용 주문번호, 영수증번호 조회
				SELECT its.ord_no
				     , its.trade_no
				INTO vAccountOrdNo
				   , vAccountTradeNo
				FROM rf_trade_sign its
				WHERE its.store_id = vStoreId
				AND its.pos_no = vPosNo
				AND its.trade_date = vBeforeTradeDt
				AND its.sign_fraction = '4';

				## 3. rf_trade_account 적재 - 정산TRAN만 해당
				## 특이사항 : 특판주문은 GHS에서 발생 예정 -> 추후 Refit에서 특판주문 발생 시 rf_trade_special 적재 예정
                INSERT INTO rf_trade_account (
                       ord_no
                     , account_code
                     , store_id
                     , pos_no
                     , trade_date
                     , trade_no
                     , account_amount
                     , account_count
                     , account_fraction
                     , checker_no
                     , reg_dt
                )
                SELECT /*1:총판매금액(반품금액, 오타금액 공병회수,봉투회수제외)*/
				       vAccountOrdNo                       AS ORD_NO
				     , '1'                                 AS ACCOUNT_CODE
				     , vStoreId                            AS STORE_ID
				     , vPosNo                              AS POS_NO
				     , vBeforeTradeDt  AS TRADE_DATE
				     , vAccountTradeNo                     AS TRADE_NO
				     , IFNULL(SUM(ith.trade_tot_amount),0) AS ACCOUNT_AMOUNT
				     , COUNT(*)                            AS ACCOUNT_COUNT
				     , '35'                                AS ACCOUNT_FRACTION
				     , '1010101010100'                     AS CHECKER_NO
				     , NOW()                               AS REG_DT
				FROM rf_trade_header ith
				WHERE ith.store_id = vStoreId
				AND ith.pos_no = vPosNo
				AND ith.trade_date = vBeforeTradeDt -- 영업일(전일자)
				AND ith.ftype_code = '1'
				AND ith.summary_fraction = '0'
				UNION ALL
				SELECT /*4:반품금액(공병회수,봉투회수제외)*/
				       vAccountOrdNo                       AS ORD_NO
				     , '4'                                 AS ACCOUNT_CODE
				     , vStoreId                            AS STORE_ID
				     , vPosNo                              AS POS_NO
				     , vBeforeTradeDt  AS TRADE_DATE
				     , vAccountTradeNo                     AS TRADE_NO
				     , IFNULL(SUM(ith.trade_tot_amount),0) AS ACCOUNT_AMOUNT
				     , COUNT(*)                            AS ACCOUNT_COUNT
				     , '35'                                AS ACCOUNT_FRACTION
				     , '1010101010100'                     AS CHECKER_NO
				     , NOW()                               AS REG_DT
				FROM rf_trade_header ith
				WHERE ith.store_id = vStoreId
				AND ith.pos_no = vPosNo
				AND ith.trade_date = vBeforeTradeDt -- 영업일(전일자)
				AND ith.ftype_code = '2'
				AND ith.summary_fraction = '0'
				UNION ALL
				SELECT /*8:총매출액(1-4)*/
				       vAccountOrdNo                       AS ORD_NO
				     , '8'                                 AS ACCOUNT_CODE
				     , vStoreId                            AS STORE_ID
				     , vPosNo                              AS POS_NO
				     , vBeforeTradeDt  AS TRADE_DATE
				     , vAccountTradeNo                     AS TRADE_NO
				     , SUM(CASE ith.ftype_code
				           WHEN '1' THEN ith.trade_tot_amount
				           ELSE ith.trade_tot_amount * (-1)
				           END)                            AS ACCOUNT_AMOUNT
				     , COUNT(*)                            AS ACCOUNT_COUNT
				     , '35'                                AS ACCOUNT_FRACTION
				     , '1010101010100'                     AS CHECKER_NO
				     , NOW()                               AS REG_DT
				FROM rf_trade_header ith
				WHERE ith.store_id = vStoreId
				AND ith.pos_no = vPosNo
				AND ith.trade_date = vBeforeTradeDt -- 영업일(전일자)
				AND ith.ftype_code IN ('1','2')
				AND ith.summary_fraction = '0'
				UNION ALL
				SELECT /*10:에누리 정상할인-반품,오타*/
				       vAccountOrdNo                       	AS ORD_NO
				     , '10'                                	AS ACCOUNT_CODE
				     , vStoreId                            	AS STORE_ID
				     , vPosNo                              	AS POS_NO
				     , vBeforeTradeDt  						AS TRADE_DATE
				     , vAccountTradeNo                     	AS TRADE_NO
				     , SUM(CASE ith.ftype_code
				           WHEN '1' THEN ith.trade_dc_amount
				           ELSE ith.trade_dc_amount * (-1)
				           END)                            AS ACCOUNT_AMOUNT
				     , COUNT(*)                            AS ACCOUNT_COUNT
				     , '35'                                AS ACCOUNT_FRACTION
				     , '1010101010100'                     AS CHECKER_NO
				     , NOW()                               AS REG_DT
				FROM rf_trade_header ith
				WHERE ith.store_id = vStoreId
				AND ith.pos_no = vPosNo
				AND ith.trade_date = vBeforeTradeDt -- 영업일(전일자)
				AND ith.ftype_code IN ('1','2')
				AND ith.summary_fraction = '0'
				UNION ALL
				SELECT /*11:순매출액(8-10)*/
				       vAccountOrdNo                       	AS ORD_NO
				     , '11'                                	AS ACCOUNT_CODE
				     , vStoreId                            	AS STORE_ID
				     , vPosNo                              	AS POS_NO
				     , vBeforeTradeDt  						AS TRADE_DATE
				     , vAccountTradeNo                     	AS TRADE_NO
				     , SUM(CASE ith.ftype_code
				           WHEN '1' THEN ith.trade_amount
				           ELSE ith.trade_amount * (-1)
				           END)                            	AS ACCOUNT_AMOUNT
				     , COUNT(*)                            	AS ACCOUNT_COUNT
				     , '35'                                	AS ACCOUNT_FRACTION
				     , '1010101010100'                     	AS CHECKER_NO
				     , NOW()                                AS REG_DT
				FROM rf_trade_header ith
				WHERE ith.store_id = vStoreId
				AND ith.pos_no = vPosNo
				AND ith.trade_date = vBeforeTradeDt -- 영업일(전일자)
				AND ith.ftype_code IN ('1','2')
				AND ith.summary_fraction = '0'
				UNION ALL
				SELECT /*15:수취대상금액(11+12+13+14)*/
				       vAccountOrdNo                       AS ORD_NO
				     , '15'                                AS ACCOUNT_CODE
				     , vStoreId                            AS STORE_ID
				     , vPosNo                              AS POS_NO
				     , vBeforeTradeDt  						AS TRADE_DATE
				     , vAccountTradeNo                     AS TRADE_NO
				     , SUM(CASE ith.ftype_code
				           WHEN '1' THEN ith.trade_amount
				           ELSE ith.trade_amount * (-1)
				           END)                            AS ACCOUNT_AMOUNT
				     , COUNT(*)                            AS ACCOUNT_COUNT
				     , '35'                                AS ACCOUNT_FRACTION
				     , '1010101010100'                     AS CHECKER_NO
				     , NOW()                               AS REG_DT
				FROM rf_trade_header ith
				WHERE ith.store_id = vStoreId
				AND ith.pos_no = vPosNo
				AND ith.trade_date = vBeforeTradeDt -- 영업일(전일자)
				AND ith.ftype_code IN ('1','2')
				AND ith.summary_fraction = '0'
				UNION ALL
				SELECT /*20:타사상품권*/
				       vAccountOrdNo                       AS ORD_NO
				     , '20'                                AS ACCOUNT_CODE
				     , vStoreId                            AS STORE_ID
				     , vPosNo                              AS POS_NO
				     , vBeforeTradeDt  						AS TRADE_DATE
				     , vAccountTradeNo                     AS TRADE_NO
				     , IFNULL(SUM(CASE ith.ftype_code
				                  WHEN '1' THEN itt.ticket_sale_amount
				                  ELSE itt.ticket_sale_amount * (-1)
				                  END),0)                  AS ACCOUNT_AMOUNT
				     , COUNT(*)                            AS ACCOUNT_COUNT
				     , '35'                                AS ACCOUNT_FRACTION
				     , '1010101010100'                     AS CHECKER_NO
				     , NOW()                               AS REG_DT
				FROM rf_trade_header ith
					INNER JOIN rf_trade_ticket itt ON ith.ord_no = itt.ord_no
				WHERE ith.store_id = vStoreId
				AND ith.pos_no = vPosNo
				AND ith.trade_date = vBeforeTradeDt -- 영업일(전일자)
				AND ith.summary_fraction = '0'
				-- UNION ALL -- 특판 테이블 생성 전.
				-- SELECT /*23:특판*/
				--        vAccountOrdNo                       AS ORD_NO
				--      , '23'                                AS ACCOUNT_CODE
				--      , vStoreId                            AS STORE_ID
				--      , vPosNo                              AS POS_NO
				--      , vBeforeTradeDt  AS TRADE_DATE
				--      , vAccountTradeNo                     AS TRADE_NO
				--      , IFNULL(SUM(CASE ith.ftype_code
				--                   WHEN '1' THEN its.special_amount
				--                   ELSE its.special_amount * (-1)
				--                   END),0)                  AS ACCOUNT_AMOUNT
				--      , COUNT(*)                            AS ACCOUNT_COUNT
				--      , '35'                                AS ACCOUNT_FRACTION
				--      , '1010101010100'                     AS CHECKER_NO
				--      , NOW()                               AS REG_DT
				-- FROM rf_trade_header ith
				-- 	INNER JOIN rf_trade_special its ON ith.ord_no = its.ord_no
				-- WHERE ith.store_id = vStoreId
				-- AND ith.pos_no = vPosNo
				-- AND ith.trade_date = vBeforeTradeDt -- 영업일(전일자)
				-- AND ith.summary_fraction = '0'
				UNION ALL
				SELECT /*27:대체수취계-통합텐더+20+23*/
				       vAccountOrdNo                       AS ORD_NO
				     , '27'                                AS ACCOUNT_CODE
				     , vStoreId                            AS STORE_ID
				     , vPosNo                              AS POS_NO
				     , vBeforeTradeDt  						AS TRADE_DATE
				     , vAccountTradeNo                     AS TRADE_NO
				--      , SUM(CASE ith.ftype_code
				--            WHEN '1' THEN IFNULL(ith.pg_amount,0)+IFNULL(itt.ticket_sale_amount,0)+IFNULL(its.special_amount,0)
				--            ELSE (IFNULL(ith.pg_amount,0)+IFNULL(itt.ticket_sale_amount,0)+IFNULL(its.special_amount,0)) * (-1)
				--            END)                            AS ACCOUNT_AMOUNT
				     , SUM(CASE ith.ftype_code
				           WHEN '1' THEN IFNULL(ith.pg_amount,0)+IFNULL(itt.ticket_sale_amount,0)
				           ELSE (IFNULL(ith.pg_amount,0)+IFNULL(itt.ticket_sale_amount,0)) * (-1)
				           END)                            AS ACCOUNT_AMOUNT
				     , COUNT(*)                            AS ACCOUNT_COUNT
				     , '35'                                AS ACCOUNT_FRACTION
				     , '1010101010100'                     AS CHECKER_NO
				     , NOW()                               AS REG_DT
				FROM rf_trade_header ith
					INNER JOIN rf_trade_ticket itt ON ith.ord_no = itt.ord_no
				-- 	INNER JOIN rf_trade_special its ON ith.ord_no = its.ord_no
				WHERE ith.store_id = vStoreId
				AND ith.pos_no = vPosNo
				AND ith.trade_date = vBeforeTradeDt -- 영업일(전일자)
				AND ith.summary_fraction = '0';

				# rf_trade_account 적재 후 check_row_cnt 업데이트
				SELECT FOUND_ROWS() INTO vTargetCount;
				UPDATE rf_trade_header
				SET check_row_cnt = vTargetCount+1
				WHERE ord_no = vAccountOrdNo;

			    ## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwHistoryNo, vLogLevelINFO, vLogEodInfo,'INSERT rf_trade_account' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_swi_loop;
                END IF;


                ## 4. 데이터 검증
                ## Insert 데이터 확인 - rf_trade_sign
                SELECT count(1)
                INTO vTargetCount
                FROM rf_trade_sign its
                WHERE its.store_id = vStoreId
				AND its.pos_no = vPosNo
				AND its.trade_date = vBeforeTradeDt
				AND its.sign_fraction IN ('2', '4');

				IF vTargetCount < 2 THEN
					ROLLBACK;

					## log insert - 검증실패
					SET vErrorMessage = 'rf_trade_sign 검증실패 (2건이하 발생)';
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwHistoryNo, vLogLevelINFO, vLogEodInfo,'rf_trade_sign 테이블 검증 실패 (2건이하임)' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_swi_loop;
				END IF;

				## Insert 데이터 확인 - rf_trade_account
				SELECT count(1)
				INTO vTargetCount
				FROM rf_trade_account ita
				WHERE ita.store_id = vStoreId
				AND ita.pos_no = vPosNo
				AND ita.trade_date = vBeforeTradeDt
				AND ita.account_fraction = '35';

				IF vTargetCount < 1 THEN
					ROLLBACK;

					## log insert - 검증실패
					SET vErrorMessage = 'rf_trade_account 검증실패 (1건이하 발생)';
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwHistoryNo, vLogLevelINFO, vLogEodInfo,'INSERT rf_trade_account 테이블 검증 실패 (1건이하 발생)' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_swi_loop;
				END IF;


				## 5. 작업완료 POS정보 업데이트 : rf_sto_winpos_info
				UPDATE rf_sto_winpos_info
				SET sale_date = DATE_FORMAT(CURDATE(), '%Y%m%d')
				  , sale_date_time = NOW()
				WHERE store_id = vStoreId
				AND pos_no = vPosNo
				AND CURDATE() BETWEEN pos_active_start_date AND pos_active_end_date;

				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwHistoryNo, vLogLevelINFO, vLogEodInfo,'UPDATE rf_sto_winpos_info' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_swi_loop;
                END IF;

                COMMIT; ## Transaction 최종 커밋

				SET vSuccessCount = vSuccessCount + 1;
				SET vDone = FALSE;

			END IF;

		until vDone
		END REPEAT read_swi_loop;

	CLOSE cursorStoWinposInfoEod;

	## 6. FC POS 영업일 업데이트 : rf_sto_winpos_info -> ITR 요청으로 제외. 데이터 확인 후 삭제예정.
	#UPDATE rf_sto_winpos_info
	#SET sale_date = DATE_FORMAT(CURDATE(), '%Y%m%d')
	#  , sale_date_time = NOW()
	#  , if_lastdate = NOW()
	#WHERE pos_no = '000790';

	## tx_isolation원복
	SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;

	## 배치로그 종료
    CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwHistoryNo, vFailCount, vSuccessCount, vTotalCount, vErrorMessage, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);


END MAIN_PROCESS