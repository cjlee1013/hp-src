CREATE DEFINER=`master`@`%` PROCEDURE `escrow`.`sp_resend_purchase_store_info`()
    COMMENT '재전송 구매점포정보 생성'
SET_RESEND_PURCHASE_STORE_INFO:BEGIN
/****************************************************************************************
- 목적/용도: PINE 매출누락분 재전송을 위한 Refit 구매점포정보 생성 프로시저. (재전송 프로시저 #1)
	1. resend_purchase_store_info 테이블의 pine_if_yn = null 인 데이터를 대상으로 작업함.
	2. 재전송 구매점포정보를 purchase_store_info 기준으로 입력함.
	   -. 재전송 주문번호는 pine_resend_order_issue 에서 order_no 채번.
	   -. purchase_store_info_no, purchase_order_no, origin_store_id 그대로 입력.
	   -. 매출일자(trade_dt)는 처리 당일로 입력.
	   -. POS번호(pos_no)는 재전송용 POS번호를 입력.
	   -. 생성 완료 데이터는 pine_if_yn : 'H' -> 'S' 업데이트.
	3. POS 점포구분 기준 (purchase_order.site_type, order_item.store_type)
       -. HYPER : (HOME, HYPER) / (HOME, DS)
       -. CLUB : (CLUB, CLUB)
       -. EXP : (HOME, EXP)
    4. POS번호 (재전송 기본 POS)
       -. HYPER : 787
       -. CLUB : 796
       -. EXP : 700

- 작성규칙
	1. SQL작성시 예약어만 대문자로 작성
    2. 변수명 작성은 카멜규칙을 따름

- 작성일자/내용/작성자
    1. 2021-01-13 / 최초작성 / 박준수
***************************************************************************************/

	## 매출 재전송용 POS 선언
	DECLARE vHyperBasicPosNo787 INT DEFAULT 787;
	DECLARE vClubBasicPosNo796  INT DEFAULT 796;
	DECLARE vExpBasicPosNo700   INT DEFAULT 700;

	## 0. 프로시저 공통 변수 및 커서 선언
	DECLARE vDone                					INT DEFAULT FALSE;	## 대상주문조회완료여부
	DECLARE vTargetCount         					INT DEFAULT 0;		## 대상건수
	DECLARE vPurchaseOrderNo     					VARCHAR(30);		## 구매주문번호
	DECLARE vPurchaseStoreInfoNo 					VARCHAR(30);		## 구매점포번호
    DECLARE vOriginStoreId			 				INT DEFAULT 0;		## 원점포번호
    DECLARE vPosNo				 					VARCHAR(6);			## 포스번호
    DECLARE vSiteType			 					VARCHAR(10);		## 사이트유형(HOME,CLUB)
    DECLARE vStoreType			 					VARCHAR(10);		## 점포유형(HYPER,DS,CLUB,EXP)
    DECLARE vOrderNo                                VARCHAR(30);		## 매출재전송용 주문번호

	DECLARE vLgwBatchNo           					BIGINT DEFAULT 0;	## 이력번호(order_batch_history)
    DECLARE vFailCount           					BIGINT DEFAULT 0;	## 실패건수(order_batch_history)
    DECLARE vSuccessCount        					BIGINT DEFAULT 0;	## 성공건수(order_batch_history)
    DECLARE vTotalCount          					BIGINT DEFAULT 0;	## 전체건수(order_batch_history)
    DECLARE vErrorMessage        					VARCHAR(2000);		## 오류메시지(order_batch_history)
	DECLARE vProcedureNm		 					VARCHAR(100) DEFAULT 'sp_resend_purchase_store_info';	## 프로시져명

	DECLARE vErrorNo             					INT DEFAULT 0;		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlState            					VARCHAR(1000);		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlErrorNo          					INT DEFAULT 0;		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlMessage          					VARCHAR(1000);		## SQLEXCEPTION 핸들러 변수
    DECLARE vLogPurchaseStoreInfo 					VARCHAR(1000);		## 전송대상 주문번호 / 점포주문번호

    DECLARE vLogLevelINFO		VARCHAR(20) DEFAULT 'INFO';
    DECLARE vLogLevelERROR		VARCHAR(20) DEFAULT 'ERROR';

    ## 재전송 구매점포정보 생성대상 커서
    DECLARE cursorCreateTarget CURSOR FOR
    	SELECT
    		rpsi.purchase_store_info_no
    	FROM resend_purchase_store_info rpsi
    	WHERE rpsi.reg_dt >= CURDATE()
    	AND rpsi.pine_if_yn IS NULL;

    ## 재전송 구매점포정보 생성완료건 조회 커서
    DECLARE cursorResendPurchaseStoreInfo CURSOR FOR
    	SELECT
    		rpsi.purchase_store_info_no
    	FROM resend_purchase_store_info rpsi
    	WHERE rpsi.reg_dt >= CURDATE()
    	AND rpsi.pine_if_yn = 'H';

    #CURSOR 루프 핸들러, CURSOR대상이 없을경우 변경 조건
	DECLARE CONTINUE HANDLER FOR NOT FOUND
		SET vDone = TRUE;

	#SQLEXCEPTION 핸들러 (오류발생시 처리)
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
            GET DIAGNOSTICS CONDITION 1 vSqlState = RETURNED_SQLSTATE, vSqlErrorNo = MYSQL_ERRNO, vSqlMessage = MESSAGE_TEXT;
            SET vErrorNo =  -1;
        END;

    ## tx_isolation level 변경
    SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;

	## 배치로그 시작
	CALL sp_lgw_batch_history(vProcedureNm, 'START', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
	SET vLgwBatchNo = @outLgwBatchNo;

	## 1. 정보 생성 대상 flag 업데이트 (pine_if_yn : NULL -> H)
	CREATE_TARGET:BEGIN
		# 커서 시작 : cursorCreateTarget
		OPEN cursorCreateTarget;
			SELECT FOUND_ROWS() INTO vTargetCount;

			## 인터페이스 대상이 없을 경우 프로시저 종료
			IF vTargetCount = 0 THEN
				## log insert - 종료: 대상없음
				CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwBatchNo, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
                SET vDone = FALSE;
				LEAVE CREATE_TARGET;
			END IF;

			## 작업대상 데이터 업데이트 (NULL:최초생성 -> H:대기)
			read_ct_loop: LOOP

				FETCH cursorCreateTarget INTO vPurchaseStoreInfoNo;
					IF NOT vDone THEN
						UPDATE resend_purchase_store_info
						SET
							  pine_if_yn = 'H'
							, chg_dt = NOW()
						WHERE purchase_store_info_no = vPurchaseStoreInfoNo
						AND pine_if_yn IS NULL;
					END IF;

					IF vDone THEN
						LEAVE read_ct_loop;
					END IF;

			END LOOP read_ct_loop;

			SET vDone = FALSE;
		CLOSE cursorCreateTarget;
	END CREATE_TARGET;

	## 2. 재전송 구매점포정보 생성 시작
	# 커서 시작 : cursorResendPurchaseStoreInfo
	OPEN cursorResendPurchaseStoreInfo;
		SELECT FOUND_ROWS() INTO vTargetCount;

		## 작업대상 데이터 업데이트 (H:대기 -> S:영수증번호발급대상주문)
		read_rpsi_loop: REPEAT

			FETCH cursorResendPurchaseStoreInfo INTO vPurchaseStoreInfoNo;

			SET vErrorNo = 0;
			SET vLogPurchaseStoreInfo = CONCAT('[점포주문번호]:',vPurchaseStoreInfoNo);

			IF NOT vDone THEN
				SET vTotalCount = vTotalCount + 1;

				## 프로시저 작업대상 처리시작
				START TRANSACTION;

				# 대상 구매점포정보 조회
				SELECT
					  psi.purchase_order_no
					, psi.origin_store_id
					, po.site_type
					, oi.store_type
				INTO
					  vPurchaseOrderNo
					, vOriginStoreId
					, vSiteType
					, vStoreType
				FROM purchase_store_info psi
					INNER JOIN purchase_order po ON po.purchase_order_no = psi.purchase_order_no
					INNER JOIN bundle bun ON bun.purchase_order_no = psi.purchase_order_no AND bun.purchase_store_info_no = psi.purchase_store_info_no
					INNER JOIN order_item oi ON oi.purchase_order_no = bun.purchase_order_no AND oi.bundle_no = bun.bundle_no
				WHERE psi.purchase_store_info_no = vPurchaseStoreInfoNo
				GROUP BY psi.purchase_order_no, psi.origin_store_id, po.site_type, oi.store_type;

				# 대상 구매점포정보 기준으로 POS번호 세팅
				IF vSiteType = 'HOME' THEN
					IF vStoreType = 'EXP' THEN
						# Express 매출 재전송 POS 세팅
						SET vPosNo = vExpBasicPosNo700;
					ELSE
						# Hyper 매출 재전송 POS 세팅
						SET vPosNo = vHyperBasicPosNo787;
					END IF;
				ELSE
					# CLUB 매출 재전송 POS 세팅
					SET vPosNo = vClubBasicPosNo796;
				END IF;

				# 재전송 주문번호 채번
				INSERT INTO pine_resend_order_issue (
					  purchase_store_info_no
					, store_id
					, pos_no
					, trade_dt
				)
				VALUES (
					  vPurchaseStoreInfoNo
					, vOriginStoreId
					, vPosNo
					, DATE_FORMAT(NOW(), '%Y%m%d')
				)
				;
				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (H:대기 -> E:데이터생성에러)
					UPDATE resend_purchase_store_info
					SET
						  pine_if_yn = 'E'
						, chg_dt = NOW()
					WHERE purchase_store_info_no = vPurchaseStoreInfoNo
					AND pine_if_yn = 'H';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo, 'INSERT pine_resend_order_issue', vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_rpsi_loop;
                END IF;

                # 채번한 재전송 주문번호 조회
                SELECT MAX(proi.order_no)
                INTO vOrderNo
                FROM pine_resend_order_issue proi
                WHERE proi.purchase_store_info_no = vPurchaseStoreInfoNo
                AND reg_dt >= CURDATE();

				# 재전송 구매점포정보에 대상 구매점포정보 Update (H:대기 -> S:영수증발급대기)
				UPDATE resend_purchase_store_info
				SET
					  purchase_order_no = vPurchaseOrderNo
					, order_no = vOrderNo
					, origin_store_id = vOriginStoreId
					, trade_dt = DATE_FORMAT(NOW(), '%Y%m%d')
					, pos_no = vPosNo
					, pine_if_yn = 'S'
					, chg_dt = NOW()
				WHERE purchase_store_info_no = vPurchaseStoreInfoNo
				AND pine_if_yn = 'H'
				;
				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (H:대기 -> E:데이터생성에러)
					UPDATE resend_purchase_store_info
					SET
						  pine_if_yn = 'E'
						, chg_dt = NOW()
					WHERE purchase_store_info_no = vPurchaseStoreInfoNo
					AND pine_if_yn = 'H';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo, 'UPDATE resend_purchase_store_info', vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_rpsi_loop;
                END IF;

                COMMIT; ## Transaction 최종 커밋

                SET vSuccessCount = vSuccessCount + 1;
				SET vDone = FALSE;

			END IF;

		UNTIL vDone
		END REPEAT read_rpsi_loop;

	CLOSE cursorResendPurchaseStoreInfo;

    ## tx_isolation level 원복
    SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;

    ## 배치로그 종료
	CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwBatchNo, vFailCount, vSuccessCount, vTotalCount, vErrorMessage, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);

END SET_RESEND_PURCHASE_STORE_INFO