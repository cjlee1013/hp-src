CREATE DEFINER=`app_escrow`@`%` PROCEDURE `escrow`.`sp_reorder_pos_receipt_issue`()
    COMMENT '재주문 영수증번호 채번'
REORDER_POS_RECEIPT:BEGIN
/****************************************************************************************
- 목적/용도 : 재주문 매출 전송을 위한 점포별 영수증번호 채번 수행 프로시저. (재주문 프로시저 #2)
    1. 영수증번호 채번 시작은 3번 부터 해야함. (1,2번은 SOD에서 채번을 함.)
    2. 9000번까지 채번되면 다음 POS번호에서 3번부터 채번함. (CLUB은 POS번호 하나라서 채번불가)
    3. 재전송 기본POS번호(787,796,700) 세팅은 sp_reorder_purchase_store_info() 프로시저에서 사전작업함.
    4. 재전송 POS번호
       -. HYPER : 787, 788
       -. CLUB : 796
       -. EXP : 700, 769
    5. 영수증번호 채번대상 조건
	   -. reorder_purchase_store_info.pine_if_yn = 'S' and reorder_purchase_store_info.trade_no = null
	6. 영수증번호 채번대상 상태 변경
	   -. reorder_purchase_store_info.pine_if_yn = 'C'

- 작성규칙
	1. SQL작성시 예약어만 대문자로 작성
    2. 변수명 작성은 카멜규칙을 따름

- 작성일자/내용/작성자
	1. 2021-01-26 / 최초작성 / 박준수
***************************************************************************************/

	## 매출 재전송용 POS 선언
	DECLARE vHyperBasicPosNo787 INT DEFAULT 787;
	DECLARE vHyperBasicPosNo788 INT DEFAULT 788;

	DECLARE vClubBasicPosNo796  INT DEFAULT 796;

	DECLARE vExpBasicPosNo700   INT DEFAULT 700;
	DECLARE vExpBasicPosNo769   INT DEFAULT 769;

	DECLARE vTradeNoMax         INT DEFAULT 9000; ## 영수증번호 MAX값

	## 0. 프로시저 공통 변수 및 커서 선언
	DECLARE vDone                					INT DEFAULT FALSE;	## 대상주문조회완료여부
    DECLARE vDoneFinal                				INT DEFAULT FALSE;	## 영수증번호 채번 완료 대상 상태 변경여부
	DECLARE vTargetCount         					INT DEFAULT 0;		## 대상건수
	DECLARE vReorderOrderNo							VARCHAR(30);		## 재주문 주문번호 (신규채번값)
	DECLARE vClaimStoreInfoNo 						BIGINT DEFAULT 0;	## 클레임점포번호
    DECLARE vOriginStoreId			 				INT DEFAULT 0;		## 원점포번호
    DECLARE vPosNo				 					VARCHAR(6);			## 포스번호
    DECLARE vTradeDt			 					VARCHAR(8);			## 거래일자(YYYYMMDD)
    DECLARE vTradeNo			 					VARCHAR(4);			## 영수증번호

	DECLARE vLgwBatchNo           					BIGINT DEFAULT 0;	## 이력번호(order_batch_history)
    DECLARE vFailCount           					BIGINT DEFAULT 0;	## 실패건수(order_batch_history)
    DECLARE vSuccessCount        					BIGINT DEFAULT 0;	## 성공건수(order_batch_history)
    DECLARE vTotalCount          					BIGINT DEFAULT 0;	## 전체건수(order_batch_history)
    DECLARE vErrorMessage        					VARCHAR(2000);		## 오류메시지(order_batch_history)
	DECLARE vProcedureNm		 					VARCHAR(100) DEFAULT 'sp_reorder_pos_receipt_issue';	## 프로시져명

	DECLARE vErrorNo             					INT DEFAULT 0;		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlState            					VARCHAR(1000);		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlErrorNo          					INT DEFAULT 0;		## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlMessage          					VARCHAR(1000);		## SQLEXCEPTION 핸들러 변수
    DECLARE vLogPurchaseStoreInfo 					VARCHAR(1000);		## 전송대상 주문번호 / 클레임점포주문번호

    DECLARE vLogLevelINFO		VARCHAR(20) DEFAULT 'INFO';
    DECLARE vLogLevelERROR		VARCHAR(20) DEFAULT 'ERROR';

    ## 영수증번호 발급대상 커서
    DECLARE cursorReorderTarget CURSOR FOR
    	SELECT
    		rpsi.claim_store_info_no
    	FROM reorder_purchase_store_info rpsi
    	WHERE rpsi.reg_dt >= CURDATE()
    	AND rpsi.pine_if_yn = 'S';

    ## 영수증번호 발급완료 주문정보 커서
    DECLARE cursorReorderTargetFinal CURSOR FOR
    	SELECT
    		rpsi.claim_store_info_no
    	FROM reorder_purchase_store_info rpsi
    	WHERE rpsi.reg_dt >= CURDATE()
    	AND rpsi.pine_if_yn = 'C';

	## 영수증번호 발급용 재주문 구매점포 정보 커서
	DECLARE cursorReorderPurchaseStoreInfo CURSOR FOR
		SELECT
			  rpsi.order_no
			, rpsi.claim_store_info_no
			, rpsi.pos_no
			, rpsi.origin_store_id
			, rpsi.trade_dt
		FROM reorder_purchase_store_info rpsi
		WHERE rpsi.reg_dt >= CURDATE()
		AND rpsi.pine_if_yn = 'C'
		AND rpsi.trade_no IS NULL;

	#CURSOR 루프 핸들러, CURSOR대상이 없을경우 변경 조건
	DECLARE CONTINUE HANDLER FOR NOT FOUND
		SET vDone = TRUE;

	#SQLEXCEPTION 핸들러 (오류발생시 처리)
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
            GET DIAGNOSTICS CONDITION 1 vSqlState = RETURNED_SQLSTATE, vSqlErrorNo = MYSQL_ERRNO, vSqlMessage = MESSAGE_TEXT;
            SET vErrorNo =  -1;
        END;

    ## tx_isolation level 변경
    SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;

    ## 배치로그 시작
	CALL sp_lgw_batch_history(vProcedureNm, 'START', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
	SET vLgwBatchNo = @outLgwBatchNo;

	## 1. 재주문 전송 대상 flag 업데이트 (pine_if_yn : S -> C)
	REORDER_TARGET:BEGIN
		## 커서 시작 : cursorReorderTarget
		OPEN cursorReorderTarget;
			SELECT FOUND_ROWS() INTO vTargetCount;

			## 대상이 없을 경우 프로시저 종료
			IF vTargetCount = 0 THEN
				## log insert - 종료: 대상없음
				CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwBatchNo, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
                SET vDone = FALSE;
				LEAVE REORDER_TARGET;
			END IF;

			## 작업대상 데이터 업데이트 (S:영수증번호발급대상주문 -> C:영수증번호발급대기)
			read_rt_loop: LOOP

				FETCH cursorReorderTarget INTO vClaimStoreInfoNo;

				IF NOT vDone THEN
					UPDATE reorder_purchase_store_info
					SET
						  pine_if_yn = 'C'
						, chg_dt = NOW()
					WHERE claim_store_info_no = vClaimStoreInfoNo
					AND pine_if_yn = 'S';
				END IF;

				IF vDone THEN
					LEAVE read_rt_loop;
				END IF;

			END LOOP read_rt_loop;

			SET vDone = FALSE;
		CLOSE cursorReorderTarget;
	END REORDER_TARGET;

	## 2. 영수증번호 채번 시작
	OPEN cursorReorderPurchaseStoreInfo;
		SELECT FOUND_ROWS() INTO vTargetCount;

		read_rpsi_loop: REPEAT

			FETCH cursorReorderPurchaseStoreInfo INTO vReorderOrderNo, vClaimStoreInfoNo, vPosNo, vOriginStoreId, vTradeDt;

			SET vErrorNo = 0;
			SET vLogPurchaseStoreInfo = CONCAT('[주문번호]:',vReorderOrderNo,'[클레임점포주문번호]:',vClaimStoreInfoNo);

			IF NOT vDone THEN
				SET vTotalCount = vTotalCount + 1;

				## 프로시저 작업대상 처리시작
				START TRANSACTION;

				SET @rtnPosNo = '';
				SET @rtnTradeNo = 0;

				## 재전송 영수증채번 테이블에서 다음 영수증번호 채번
				SELECT IFNULL(MAX(trade_no),2)+1
				INTO vTradeNo
				FROM resend_pos_receipt_issue
				WHERE store_id = vOriginStoreId
				AND trade_dt = vTradeDt
				AND pos_no = vPosNo;


				# 영수증번호 채번 기준 POS번호(Update 대상)
				SET @rtnPosNo = vPosNo;

				## POS별 영수증번호 MAX값 도달시 다음 POS번호로 세팅(CLUB POS는 1개이므로 제외)
				IF vPosNo = vHyperBasicPosNo787 OR vPosNo = vExpBasicPosNo700 THEN
					#Express 기본POS(700)번호가 9000번 넘는지 확인
					IF vPosNo = vExpBasicPosNo700 THEN
						IF vTradeNo > vTradeNoMax THEN
							#769 pos번호로 영수증번호 다시 채번
							SELECT 	IFNULL(MAX(trade_no),2)+1
							INTO 	vTradeNo
							FROM 	resend_pos_receipt_issue
							WHERE 	store_id = vOriginStoreId
							AND		trade_dt = vTradeDt
							AND		pos_no = vExpBasicPosNo769;

							SET @rtnPosNo = vExpBasicPosNo769;
						END IF;
					ELSE
						#Hyper 기본POS(787)번호가 9000번 넘는지 확인
						IF vTradeNo > vTradeNoMax THEN
							#788 pos번호로 영수증번호 다시 채번
							SELECT 	IFNULL(MAX(trade_no),2)+1
							INTO 	vTradeNo
							FROM 	resend_pos_receipt_issue
							WHERE 	store_id = vOriginStoreId
							AND		trade_dt = vTradeDt
							AND		pos_no = vHyperBasicPosNo788;

							SET @rtnPosNo = vHyperBasicPosNo788;
						END IF;
					END IF;
				END IF;

				## 채번된 점포에 영수증번호 입력
				INSERT INTO resend_pos_receipt_issue
				(
                    store_id, 					trade_dt,
                    pos_no, 					trade_no
				)
				VALUES
				(
					vOriginStoreId, 			vTradeDt,
                    @rtnPosNo, 					vTradeNo
				)
                ;
                ## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (C:영수증번호발급대기 -> Q:영수증번호발급실패)
					UPDATE reorder_purchase_store_info
					SET
						  pine_if_yn = 'Q'
						, chg_dt = NOW()
					WHERE claim_store_info_no = vClaimStoreInfoNo
					AND pine_if_yn = 'C';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo, 'INSERT resend_pos_receipt_issue', vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_rpsi_loop;
                END IF;

                #발급된 영수증번호 Update
                UPDATE reorder_purchase_store_info
                SET
                	  pos_no = @rtnPosNo
                	, trade_no = vTradeNo
                WHERE claim_store_info_no = vClaimStoreInfoNo
                ;
                ## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## 작업대상 업데이트 (C:영수증번호발급대기 -> Q:영수증번호발급실패)
					UPDATE reorder_purchase_store_info
					SET
						  pine_if_yn = 'Q'
						, chg_dt = NOW()
					WHERE claim_store_info_no = vClaimStoreInfoNo
					AND pine_if_yn = 'C';

					## log insert - 에러 종료
					SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, vLogLevelERROR, vLogPurchaseStoreInfo, 'UPDATE reorder_purchase_store_info', vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_rpsi_loop;
                END IF;

				COMMIT; ## Transaction 최종 커밋

				SET vSuccessCount = vSuccessCount + 1;
				SET vDone = FALSE;

			END IF;

		UNTIL vDone
		END REPEAT read_rpsi_loop;

	CLOSE cursorReorderPurchaseStoreInfo;

	## 3. 재주문 최종 데이터 상태 업데이트
	REORDER_TARGET_FINAL:BEGIN
		#CURSOR 루프 핸들러, CURSOR대상이 없을경우 변경 조건
		DECLARE CONTINUE HANDLER FOR NOT FOUND
			BEGIN
				SET vDoneFinal = TRUE;
			END;

		## 커서 시작 : cursorReorderTargetFinal
		OPEN cursorReorderTargetFinal;
			SELECT FOUND_ROWS() INTO vTargetCount;

			## 작업완료 주문 Pine전송상태 변경 업데이트 (C -> N)
			read_rt_final_loop: LOOP

				FETCH cursorReorderTargetFinal INTO vClaimStoreInfoNo;
					IF NOT vDoneFinal THEN
						UPDATE reorder_purchase_store_info
						SET
							  pine_if_yn = 'N'
                            , chg_dt = NOW()
						WHERE claim_store_info_no = vClaimStoreInfoNo
						AND pine_if_yn = 'C';
					END IF;

					IF vDoneFinal THEN
						LEAVE read_rt_final_loop;
					END IF;

			END LOOP read_rt_final_loop;

		CLOSE cursorReorderTargetFinal;
	END REORDER_TARGET_FINAL;

    ## tx_isolation level 원복
    SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;

    ## 배치로그 종료
	CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwBatchNo, vFailCount, vSuccessCount, vTotalCount, vErrorMessage, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);

END REORDER_POS_RECEIPT