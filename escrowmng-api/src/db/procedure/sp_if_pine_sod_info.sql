CREATE DEFINER=`app_escrow`@`%` PROCEDURE `escrow`.`sp_if_pine_sod_info`()
    COMMENT 'PINE 매출연동 Refit POS별 SOD 데이터 생성'
MAIN_PROCESS:BEGIN
/****************************************************************************************
- 목적/용도: Refit POS별 SOD 데이터 생성 프로시저 (Refit IF 테이블에 데이터 적재)
- 작성일자/작성자 : 2020-06-09 / 박준수
- 수정일자/수정내용/수정자 : 2020-08-13 / 인터페이스 테이블명 LGW 에 맞추어 변경 / 박준수
- 2020-08-18 / transaction commit Level 변경 : READ_COMMITTED / 송영준
  2020-11-17 / 모든 대상 테이블에 reg_dt 추가 적용 / 박준수
****************************************************************************************/

	## 0. 프로시저 공통 변수 및 커서 선언
	DECLARE vDone         INT DEFAULT FALSE;   ## 작업완료여부
	DECLARE vTargetCount  INT DEFAULT 0;       ## 대상건수
	DECLARE vLogSeq       INT DEFAULT 1;       ## 로그순번
	DECLARE vStoreId      VARCHAR(4);          ## 점포ID
	DECLARE vPosNo        VARCHAR(6);          ## POS번호

	DECLARE vLgwHistoryNo    BIGINT DEFAULT 0;    ## 이력번호(lgw_batch_history)
    DECLARE vFailCount    BIGINT DEFAULT 0;    ## 실패건수(lgw_batch_history)
    DECLARE vSuccessCount BIGINT DEFAULT 0;    ## 성공건수(lgw_batch_history)
    DECLARE vTotalCount   BIGINT DEFAULT 0;    ## 전체건수(lgw_batch_history)
    DECLARE vErrorMessage VARCHAR(2000);       ## 오류메시지(lgw_batch_history)
    DECLARE vProcedureNm		 VARCHAR(100) DEFAULT 'sp_if_pine_sod_info';		## 프로시져명

	DECLARE vErrorNo      INT DEFAULT 0;	   ## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlState     VARCHAR(1000);	   ## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlErrorNo   INT DEFAULT 0;	   ## SQLEXCEPTION 핸들러 변수
	DECLARE vSqlMessage   VARCHAR(1000);	   ## SQLEXCEPTION 핸들러 변수
	DECLARE vLogSodInfo		VARCHAR(1000);		## 오류저장 로그변수

    DECLARE vLogLevelINFO		VARCHAR(20) DEFAULT 'INFO';
    DECLARE vLogLevelERROR		VARCHAR(20) DEFAULT 'ERROR';

	DECLARE cursorStoWinposInfo CURSOR FOR     ## POS마스터 커서
		SELECT swi.store_id
		     , swi.pos_no
		FROM rf_sto_winpos_info swi
		WHERE swi.pos_no <> '000790' -- FC POS 제외
		AND CURDATE() BETWEEN swi.pos_active_start_date AND swi.pos_active_end_date
		ORDER BY swi.store_id, swi.pos_no;

	DECLARE CONTINUE HANDLER FOR NOT FOUND     ## 루프 핸들러
		SET vDone = TRUE;

	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION  ## SQLEXCEPTION 핸들러
		BEGIN
            GET DIAGNOSTICS CONDITION 1 vSqlState = RETURNED_SQLSTATE, vSqlErrorNo = MYSQL_ERRNO, vSqlMessage = MESSAGE_TEXT;
            SET vErrorNo =  -1;
        END;

	## tx_isolation변경
    SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;

    ## 배치로그 시작
    CALL sp_lgw_batch_history(vProcedureNm, 'START', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
	SET vLgwHistoryNo = @outLgwBatchNo;


    ## 0. POS 마스터 정보 기준으로 작업 시작
    ## 커서 시작 : cursorStoWinposInfo

    OPEN cursorStoWinposInfo;
		SELECT FOUND_ROWS() INTO vTargetCount;

		## 인터페이스 대상이 없을 경우 프로시저 종료
		IF vTargetCount = 0 THEN
			## log insert - 종료: 대상없음
			CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwHistoryNo, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
			LEAVE MAIN_PROCESS;
			## tx_isolation원복
    		SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
		END IF;

		## 커서 내 loop 시작
		read_swi_loop: REPEAT

			FETCH cursorStoWinposInfo INTO vStoreId, vPosNo;

            SET vErrorNo = 0;
			SET vLogSodInfo = CONCAT('[storeId]:',vStoreId,'[posNo]: ',vPosNo);

			IF NOT vDone THEN
				SET vTotalCount = vTotalCount + 1;

				START TRANSACTION;

				## 1. rf_trade_sign 적재
				INSERT INTO rf_trade_sign (
				       store_id
				     , pos_no
				     , trade_date
				     , trade_no
				     , sign_fraction
				     , checker_no
				     , reg_dt
				)
				VALUES (
				       vStoreId
				     , vPosNo
				     , DATE_FORMAT(CURDATE(), '%Y%m%d')
				     , '0001'
				     , '3' -- 개설: header.account_fraction=37
				     , '1010101010100'
				     , NOW()
				),
				       (
				       vStoreId
				     , vPosNo
				     , DATE_FORMAT(CURDATE(), '%Y%m%d')
				     , '0002'
				     , '1' -- sign-on: header.account_fraction=32
				     , '1010101010100'
				     , NOW()
				);


				## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## log insert - 에러 종료
                    SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwHistoryNo, vLogLevelERROR, vLogSodInfo,'INSERT rf_trade_sign' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_swi_loop;
                END IF;


				## 2. rf_trade_header 적재
				INSERT INTO rf_trade_header (
				       ord_no
					 , cust_ord_no
					 , store_id
					 , pos_no
					 , trade_date
					 , trade_no
					 , trade_time
					 , ftype_code
					 , account_fraction
					 , summary_fraction
					 , checker_no
					 , delivery_cd
					 , trade_fraction
					 , trade_count
					 , trade_change_amount
					 , trade_delivery_amount
					 , trade_bottle_amount
					 , trade_onetool_amount
					 , rtn_fault_fraction
					 , part_cancel
					 , part_cancel_flag
					 , subtotal_discount_rate
					 , subtotal_discount_amount
					 , subtotal_discount_fraction
					 , special_amount
					 , head_office_code
					 , area_code
					 , noncust_yn
					 , mall_delivery_type
					 , check_row_cnt
					 , reg_dt
				)
				SELECT
				       its.ord_no                   AS ORD_NO
				     , its.ord_no                   AS CUST_ORD_NO
				     , its.store_id                 AS STORE_ID
				     , its.pos_no                   AS POS_NO
				     , its.trade_date               AS TRADE_DATE
				     , its.trade_no                 AS TRADE_NO
				     , DATE_FORMAT(NOW(), '%H%i%s') AS TRADE_TIME
				     , '1'                          AS FTYPE_CODE
				     , CASE its.sign_fraction
				       WHEN '3' THEN '37'
				       WHEN '1' THEN '32'
				       ELSE ''
				       END                          AS ACCOUNT_FRACTION
				     , '0'                          AS SUMMARY_FRACTION
				     , '1010101010100'              AS CHECKER_NO
				     , '37010'                      AS DELIVERY_CD
					 , 6                            AS TRADE_FRACTION
					 , 0                            AS TRADE_COUNT
					 , 0                            AS TRADE_CHANGE_AMOUNT
					 , 0                            AS TRADE_DELIVERY_AMOUNT
					 , 0                            AS TRADE_BOTTLE_AMOUNT
					 , 0                            AS TRADE_ONETOOL_AMOUNT
					 , '0'                          AS RTN_FAULT_FRACTION
					 , '0'                          AS PART_CANCEL
					 , 'N'                          AS part_cancel_flag
					 , 0                            AS SUBTOTAL_DISCOUNT_RATE
					 , 0                            AS SUBTOTAL_DISCOUNT_AMOUNT
					 , '0'                          AS SUBTOTAL_DISCOUNT_FRACTION
					 , 0                            AS SPECIAL_AMOUNT
					 , swi.head_office_code         AS HEAD_OFFICE_CODE
					 , swi.area_code                AS AREA_CODE
					 , 'N'                          AS NONCUST_YN
					 , '31010'                      AS MALL_DELIVERY_TYPE
					 , 1                            AS CHECK_ROW_CNT
					 , NOW()                        AS REG_DT
				FROM rf_trade_sign its
					INNER JOIN rf_sto_winpos_info swi ON its.store_id = swi.store_id AND its.pos_no = swi.pos_no
			    WHERE its.store_id = vStoreId
			    AND its.pos_no = vPosNo
			    AND its.trade_date = DATE_FORMAT(CURDATE(), '%Y%m%d')
			    AND its.sign_fraction IN ('1', '3');

			    ## SQLEXCEPTION 처리
				IF vErrorNo < 0 THEN
					ROLLBACK; ## ROLLBACK & Transaction 종료

					## log insert - 에러 종료
                    SET vErrorMessage = CONCAT('ERROR: ',vSqlErrorNo,', MESSAGE: ',vSqlMessage,', (',vSqlstate,')');
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwHistoryNo, vLogLevelERROR, vLogSodInfo,'INSERT rf_trade_header' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_swi_loop;
                END IF;


				## 3. 데이터 검증
                ## Insert 데이터 확인
                SELECT count(1)
                INTO vTargetCount
                FROM rf_trade_sign its
                WHERE its.store_id = vStoreId
				AND its.pos_no = vPosNo
				AND its.trade_date = DATE_FORMAT(CURDATE(), '%Y%m%d')
				AND its.sign_fraction IN ('1', '3');

				IF vTargetCount < 2 THEN
					ROLLBACK;

					## log insert - 검증실패
                    SET vErrorMessage = 'rf_trade_sign 검증실패 (2건이하 발생)';
                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwHistoryNo, vLogLevelERROR, vLogSodInfo,'rf_trade_sign 검증실패 (2건이하 발생)' , vErrorMessage, NULL, NULL);
                    SET vFailCount = vFailCount + 1;

                    ## REPEAT 처음부터 재시작
                    ITERATE read_swi_loop;
				END IF;


				COMMIT; ## Transaction 최종 커밋

				SET vSuccessCount = vSuccessCount + 1;
				SET vDone = FALSE;

			END IF;

		until vDone
		END REPEAT read_swi_loop;

	CLOSE cursorStoWinposInfo;

	## tx_isolation원복
	SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;

	## 배치로그 종료
	CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwHistoryNo, vFailCount, vSuccessCount, vTotalCount, vErrorMessage, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);

END MAIN_PROCESS