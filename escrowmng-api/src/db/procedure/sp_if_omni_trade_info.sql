CREATE DEFINER=`app_escrow`@`%` PROCEDURE `escrow`.`sp_if_omni_trade_info`()
MAIN_PROCESS:BEGIN
/********************************************************************************************************************************
- 목적/용도: Refit 주문 데이터 옴니플랫폼팀 연동 프로시저, 주문정보를 옴니플랫폼 I/F 테이블에 저장
- 작성일자/작성자 : 2020-05-21, 홍주영
- 수정일자/수정내용/수정자 : 2020-10-23 / 안심번호 미발급 상태도 옴니 전송 / 홍주영
					   2020-10-29 / 대체주문(ORD_SUB) 전송 대상 추가 / 홍주영
					   2020-10-29 / 픽업주문의 장소번호, 락커번호 전송 항목 추가 / 홍주영
					   2020-11-04 / pos_no, trade_no 전송 항목 추가 / 홍주영
					   2020-11-16 / rf_omni_purchase_gift 컬럼 삭제 / 홍주영
					   2020-11-24 / 배송방법(TD_QUICK) 추가 / 홍주영
					   2020-11-25 / 에러 발생시 옴니인터페이스여부(omni_if_yn)를 'E'로 업데이트 / 홍주영
					   2020-11-25 / 점포유형(store_type) 항목 추가 / 홍주영
					   2020-12-03 / trade_time, trade_tot_amount, trade_amount 전송 항목 추가 / 홍주영
					   2020-12-23 / rf_omni_ord_pickup_info(픽업정보) 테이블 연동 추가 / 홍주영
					   2020-12-29 / rf_omni_trade_item_tran 테이블 crossmsv_yn, buy_std_qty 컬럼 연동 / 홍주영
					   2021-01-06 / rf_omni_trade_header_tran.pickupgroup_cd, rf_omni_trade_item_tran.evngift_msg NULL 입력 / 홍주영
					   2021-01-13 / rf_omni_trade_header_tran.delivery_st 익스프레스는 26020(상품준비중) 그외는 26010(결제완료)로 전송 / 홍주영
					   2021-01-22 / rf_omni_trade_header_tran.card_gb(법인주문여부) 컬럼 추가 / 홍주영
					   2021-01-27 / rf_omni_trade_header_tran.delivery_cd와 deliver_typ 컬럼의 코드값을 익스프레스 주문일 경우 구분 / 홍주영
					   2021-02-18 / rf_omni_trade_header_tran.noncust_yn(비회원여부) 컬럼 추가 / 홍주영
					   2021-02-24 / rf_omni_trade_header_tran.delivery_dt EXP 주문시 날짜 포맷 수정 / 이효신
					   2021-03-17 / rf_omni_ord_sback_useopt(용도별옵션) 저장 대상 ship_method에 TD_QUICK 추가 / 홍주영
					   2021-08-05 / order_type에 ORD_MARKET(마켓연동주문) 추가 / 홍주영
					   2021-09-27 / rf_omni_trade_header_tran.no_night_talk_send_yn 컬럼연동 / 송영준
					   2021-10-29 / rf_omni_trade_header_tran 마켓연동 업체코드(partner_company), 마켓주문번호 연동 / 송영준
*********************************************************************************************************************************/

    DECLARE vNoMoreRows BOOLEAN DEFAULT FALSE;          -- FETCH 가능여부
    DECLARE vSizeOfCursor INT DEFAULT 0;                -- 커서 사이즈
    DECLARE vPurchaseOrderNo VARCHAR(30);               -- 구매주문번호
    DECLARE vPurchaseStoreInfoNo VARCHAR(30);           -- 구매점포번호
    DECLARE vSafetyUseYn CHAR(1);                       -- 안심번호사용여부
    DECLARE vIssuePhoneNo VARCHAR(14);                  -- 안심번호
    DECLARE vElapseMinute INT DEFAULT 0;                -- 경과시간
    DECLARE vHyperCnt INT DEFAULT 0;                    -- HYPER 상품수
    DECLARE vFcCnt INT DEFAULT 0;                       -- FC 상품수
    DECLARE vPurchaseStoreInfoCnt INT DEFAULT 0;        -- 구매점포 건수
    DECLARE vTradeHeaderTranCnt INT DEFAULT 0;          -- 주문헤더인터페이스 건수
    DECLARE vStoreType VARCHAR(10);						-- 점포유형

    DECLARE vLgwBatchNo BIGINT DEFAULT 0;               -- 배치번호(lgw_batch_history)
    DECLARE vFailCount BIGINT DEFAULT 0;                -- 실패건수(lgw_batch_history)
    DECLARE vSuccessCount BIGINT DEFAULT 0;             -- 성공건수(lgw_batch_history)
    DECLARE vTotalCount BIGINT DEFAULT 0;               -- 전체건수(lgw_batch_history)
    DECLARE vErrorMessage VARCHAR(2000);                -- 오류메시지(lgw_batch_history)
    DECLARE vProcedureNm VARCHAR(100) DEFAULT 'sp_if_omni_trade_info'; -- 프로시져명

    DECLARE vErrorNo INT DEFAULT 0;                     -- SQLEXCEPTION 핸들러 변수
    DECLARE vSqlState VARCHAR(1000);                    -- SQLEXCEPTION 핸들러 변수
    DECLARE vSqlErrno INT;                              -- SQLEXCEPTION 핸들러 변수
    DECLARE vSqlMessage VARCHAR(1000);                  -- SQLEXCEPTION 핸들러 변수

    DECLARE vCntItemTran INT DEFAULT 0;                 -- 해당 주문번호의 RF_OMNI_TRADE_ITEM_TRAN 데이터 개수
    DECLARE vCntCombineInfo INT DEFAULT 0;              -- 해당 주문번호의 RF_OMNI_ORD_COMBINE_INFO 데이터 개수
    DECLARE vCntSbackUseOpt INT DEFAULT 0;              -- 해당 주문번호의 RF_OMNI_ORD_SBACK_USEOPT 데이터 개수
    DECLARE vCntGift INT DEFAULT 0;                     -- 해당 주문번호의 RF_OMNI_PURCHASE_GIFT 데이터 개수
    DECLARE vCntGiftItem INT DEFAULT 0;                 -- 해당 주문번호의 RF_OMNI_PURCHASE_GIFT_ITEM 데이터 개수
    DECLARE vCntPickupInfo INT DEFAULT 0;				-- 해당 주문번호의 RF_OMNI_ORD_PICKUP_INFO 데이터 개수

    DECLARE cursorPurChaseOrder CURSOR FOR              -- 인터페이스 대상 구매주문 커서
        SELECT  po.purchase_order_no
        FROM    purchase_order po
                INNER JOIN payment pay
        ON 		po.payment_no = pay.payment_no
        AND		pay.payment_status = 'P3'								   				 -- 결제완료(입금확인)
        WHERE   po.order_type IN ('ORD_NOR', 'ORD_COMBINE', 'ORD_RESERVE', 'ORD_MARKET') -- 일반주문,합주문,예약주문,마켓연동주문만 전송 대상
        AND     po.omni_if_yn = 'N'
        UNION ALL
        SELECT  po.purchase_order_no
        FROM    purchase_order po
                INNER JOIN payment pay
        ON 		po.payment_no = pay.payment_no
        AND		pay.payment_status = 'P5'	 -- 가상결제
        WHERE   po.order_type IN ('ORD_SUB') -- 대체주문만 전송 대상
        AND     po.omni_if_yn = 'N'
        ORDER BY purchase_order_no;


    DECLARE cursorPurchaseStoreInfo CURSOR FOR          -- 인터페이스 대상 구매점포 커서
        SELECT   psi.purchase_order_no
                ,psi.purchase_store_info_no
        FROM    purchase_order po
                INNER JOIN purchase_store_info psi
        ON      po.purchase_order_no = psi.purchase_order_no
        AND     psi.ship_method IN ('TD_DRCT', 'TD_PICK', 'TD_LOCKER', 'TD_QUICK')
        WHERE   po.omni_if_yn = 'I'
        ORDER BY psi.purchase_order_no, psi.purchase_store_info_no;

    DECLARE CONTINUE HANDLER FOR NOT FOUND              -- 루프 핸들러
        SET vNoMoreRows = TRUE;

    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION           -- SQLEXCEPTION 핸들러
        BEGIN
            GET DIAGNOSTICS CONDITION 1 vSqlState = RETURNED_SQLSTATE, vSqlErrno = MYSQL_ERRNO, vSqlMessage = MESSAGE_TEXT;
            SET vErrorNo =  -1;
        END;

    -- ISOLATION LEVEL 설정
    SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;

    CALL sp_lgw_batch_history(vProcedureNm, 'START', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);
    SET vLgwBatchNo = @outLgwBatchNo;

    -- 1. 옴니플랫폼 인터페이스 대상 데이터 추출
    --    1) purchase_order에서 옴니인터페이스여부(omni_if_yn)가 'N'인 주문 가져오기
    --    2) TD 상품이 없는 주문일 경우 옴니인터페이스여부(omni_if_yn)를 'X(비대상)'로 업데이트
    --    3) TD 상품이 있는 주문일 경우 옴니인터페이스여부(omni_if_yn)를 'I(진행중)'로 업데이트

    OPEN cursorPurChaseOrder;

        REPEAT
            FETCH cursorPurChaseOrder INTO vPurchaseOrderNo;
            IF NOT vNoMoreRows THEN
                SELECT  COUNT(*)
                INTO    vPurchaseStoreInfoCnt
                FROM    purchase_store_info
                WHERE   purchase_order_no = vPurchaseOrderNo
                AND     ship_method IN ('TD_DRCT', 'TD_PICK', 'TD_LOCKER', 'TD_QUICK');

                IF vPurchaseStoreInfoCnt = 0 THEN
                    UPDATE  purchase_order
                    SET     omni_if_yn = 'X'    -- 인터페이스 비대상
                            ,omni_if_dt = NOW()
                    WHERE   purchase_order_no = vPurchaseOrderNo;
                ELSE
                    UPDATE  purchase_order
                    SET     omni_if_yn = 'I'    -- 인터페이스 진행중
                            ,omni_if_dt = NOW()
                    WHERE   purchase_order_no = vPurchaseOrderNo;
                END IF;

                SET vNoMoreRows = FALSE;
            END IF;
            UNTIL vNoMoreRows
        END REPEAT;

        SET vNoMoreRows = FALSE;

    CLOSE cursorPurChaseOrder;


    -- 2. 옴니플랫폼 인터페이스 대상 데이터 적재
    --    1) rf_omni_trade_item_tran(주문아이템) 저장
    --    2) rf_omni_ord_sback_useopt(용도별옵션) 저장
    --    3) rf_omni_ord_combine_info(합배송정보) 저장
    --    4) rf_omni_purchase_gift(사은품) 저장
    --    5) rf_omni_purchase_gift_item(사은품상품) 저장
    --    6  rf_omni_ord_pickup_info(픽업정보) 저장
    --    7) rf_omni_trade_header_tran(주문헤더) 저장
    --    8) purchase_order 업데이트

    OPEN cursorPurchaseStoreInfo;
        SELECT FOUND_ROWS() INTO vSizeOfCursor;

        IF vSizeOfCursor = 0 THEN
            CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwBatchNo, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);

			-- ISOLATION LEVEL 설정 원복
			SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;

			LEAVE MAIN_PROCESS;
        END IF;

        REPEAT_PURCHASE_STORE_INFO: REPEAT
            FETCH cursorPurchaseStoreInfo
            INTO  vPurchaseOrderNo, vPurchaseStoreInfoNo;

            SET vErrorNo = 0;

            IF NOT vNoMoreRows THEN
                SET vTotalCount = vTotalCount + 1;

                -- 안심번호 대상여부 및 발급여부 확인
                SELECT   sa.safety_use_yn
                        ,IFNULL(saf.issue_phone_no,'')
                        ,TIMESTAMPDIFF(MINUTE, sa.reg_dt, NOW())
                INTO     vSafetyUseYn
                        ,vIssuePhoneNo
                        ,vElapseMinute
                FROM    purchase_store_info psi
                        INNER JOIN bundle bun
                ON		psi.purchase_store_info_no = bun.purchase_store_info_no
                AND     psi.purchase_order_no = bun.purchase_order_no
                        INNER JOIN shipping_addr sa
                ON		bun.bundle_no = sa.bundle_no
                        LEFT OUTER JOIN safety_issue saf
                ON		sa.ship_addr_no = saf.ship_addr_no
                WHERE	psi.purchase_store_info_no = vPurchaseStoreInfoNo;

                -- 안심번호 미발급이어도 옴니 전송
                -- 미발급이면서 경과시간이 10분 미만인 경우 I/F 대상 제외
                -- IF vSafetyUseYn = 'Y' AND LENGTH(vIssuePhoneNo) = 0 AND vElapseMinute < 10 THEN
                    -- CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, CONCAT('purchase_order_no: ',vPurchaseOrderNo,', purchase_store_info_no: ',vPurchaseStoreInfoNo), CONCAT('안심번호 매핑 안됨, 경과시간(분): ',vElapseMinute), NULL, NULL, NULL);
                    -- SET vErrorMessage = CONCAT('purchase_order_no: ',vPurchaseOrderNo,', purchase_store_info_no: ',vPurchaseStoreInfoNo,', 안심번호 매핑 안됨, 경과시간(분): ',vElapseMinute);
                    -- SET vFailCount = vFailCount + 1;

                    -- ITERATE REPEAT_PURCHASE_STORE_INFO;
                -- END IF;

                START TRANSACTION;

                -- INSERT rf_omni_trade_item_tran(주문아이템)
                SET @itemSeq = 0; -- item_seq 컬럼용 변수 선언

                INSERT INTO rf_omni_trade_item_tran(
                     ord_no
                    ,store_id
                    ,good_id
                    ,evngift_msg
                    ,delivery_dt
                    ,shift_id
                    ,slot_id
                    ,item_qty
                    ,item_cost
                    ,print_item_name
                    ,promo_no
                    ,delivery_st
                    ,item_seq
                    ,goodkr_nm3
                    ,buy_std_qty
                    ,delivery_typ
                    ,crossmsv_yn
                    ,substitution_yn
                    ,fc_good_yn
                    ,trade_date
                )
                SELECT	 psi.purchase_store_info_no AS ord_no
                        ,CASE WHEN psi.origin_store_id < 1000 THEN LPAD(psi.origin_store_id,4,'0')
                              ELSE psi.origin_store_id
                         END AS store_id
                        ,oi.item_no AS good_id
                        ,NULL AS evngift_msg
                        ,DATE_FORMAT(psi.ship_dt, '%Y-%m-%d 00:00:00') AS delivery_dt
                        ,psi.shift_id
                        ,psi.slot_id
                        ,oi.item_qty
                        ,oi.item_price AS item_cost
                        ,LEFT(oi.item_nm1,30) AS print_item_name
                        ,oi.promo_no
                        ,CASE WHEN si.ship_status = 'D1' THEN '26010'
                              WHEN si.ship_status = 'D2' THEN '26020'
                              WHEN si.ship_status = 'D3' THEN '26040'
                              WHEN si.ship_status = 'D4' THEN '26060'
                         END AS delivery_st -- NN:대기, D1:주문확인전(결제완료)-26010, D2:주문확인(상품준비중)-26020, D3:배송중-26040, D4:배송완료-24060, D5:구매확정
                        ,@itemSeq := @itemSeq+1 AS item_seq
                        ,oi.item_nm1 AS goodkr_nm3
                        ,IFNULL(oi.mix_match_std_val, 0) AS buy_std_qty
                        ,'100' AS delivery_typ -- 100: 점포배송(자차), 200: 업체배송, 300: 대형가전
                        ,IFNULL(oi.mix_match_pick_yn, 'N') AS crossmsv_yn
                        ,oi.substitution_yn
                        ,oi.fc_item_yn AS fc_good_yn
                        ,DATE_FORMAT(po.order_dt, '%Y%m%d') AS trade_date
                FROM 	purchase_store_info psi
                        INNER JOIN purchase_order po
                ON		psi.purchase_order_no = po.purchase_order_no
                        INNER JOIN order_item oi
                ON		po.purchase_order_no = oi.purchase_order_no
                        INNER JOIN shipping_item si
                ON		oi.order_item_no = si.order_item_no
                AND     si.ship_method IN ('TD_DRCT', 'TD_PICK', 'TD_LOCKER', 'TD_QUICK')
                WHERE 	psi.purchase_store_info_no = vPurchaseStoreInfoNo
                ORDER BY oi.order_item_no;

                IF vErrorNo < 0 THEN
                    ROLLBACK;

                    UPDATE  purchase_order
                    SET      omni_if_yn = 'E'
                            ,omni_if_dt = NOW()
                    WHERE   purchase_order_no = vPurchaseOrderNo;

                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, 'ERROR', CONCAT('purchase_order_no: ',vPurchaseOrderNo,', purchase_store_info_no: ',vPurchaseStoreInfoNo), CONCAT('ERROR ',vSqlErrno,' (',vSqlstate,'): ',vSqlMessage), 'INSERT INTO rf_omni_trade_item_tran Fail', NULL, NULL);
                    SET vErrorMessage = CONCAT('ERROR ',vSqlErrno,' (',vSqlstate,'): ',vSqlMessage);
                    SET vFailCount = vFailCount + 1;

                    ITERATE REPEAT_PURCHASE_STORE_INFO;
                END IF;

                SELECT FOUND_ROWS() INTO vCntItemTran;

                -- 점포유형(storeType) 조회
                SELECT	oi.store_type
                INTO	vStoreType
                FROM 	purchase_store_info psi
                        INNER JOIN purchase_order po
                ON		psi.purchase_order_no = po.purchase_order_no
                        INNER JOIN order_item oi
                ON		po.purchase_order_no = oi.purchase_order_no
                        INNER JOIN shipping_item si
                ON		oi.order_item_no = si.order_item_no
                AND     si.ship_method IN ('TD_DRCT', 'TD_PICK', 'TD_LOCKER', 'TD_QUICK')
                WHERE	psi.purchase_store_info_no = vPurchaseStoreInfoNo
                LIMIT 1;

                -- INSERT rf_omni_ord_sback_useopt(용도별옵션)
                INSERT INTO rf_omni_ord_sback_useopt(
                     sback_no
                    ,good_id
                    ,opt
                    ,item_qty
                    ,ord_no
                )
                SELECT 	 psi.purchase_store_info_no AS sback_no
						,oi.item_no AS good_id
						,CONCAT(
							 IF(LENGTH(oo.sel_opt1_title)>0,CONCAT(oo.sel_opt1_title,'-'),''), IFNULL(oo.sel_opt1_val,'') -- 선택옵션1
							,IF(LENGTH(oo.sel_opt1_val)>0 AND LENGTH(oo.sel_opt2_val)>0, ', ', '')
							,IF(LENGTH(oo.sel_opt2_title)>0,CONCAT(oo.sel_opt2_title,'-'),''), IFNULL(oo.sel_opt2_val,'') -- 선택옵션2
						 ) AS opt
						,oo.opt_qty AS item_qty
						,psi.purchase_store_info_no AS ord_no
				FROM 	purchase_store_info psi
				INNER JOIN bundle bun ON bun.purchase_store_info_no = psi.purchase_store_info_no AND bun.ship_method in ('TD_DRCT','TD_PICK','TD_QUICK')
				INNER JOIN order_item oi ON	bun.bundle_no = oi.bundle_no
				INNER JOIN order_opt oo ON	oi.order_item_no = oo.order_item_no
				AND     oo.opt_no > 0
				WHERE 	psi.purchase_store_info_no = vPurchaseStoreInfoNo;
                /*
                SELECT 	 psi.purchase_store_info_no AS sback_no -- 확인 필요(장바구니번호)
                        ,oi.item_no AS good_id
                        ,CONCAT(
                             IF(LENGTH(oo.sel_opt1_title)>0,CONCAT(oo.sel_opt1_title,'-'),''), IFNULL(oo.sel_opt1_val,'') -- 선택옵션1
                            ,IF(LENGTH(oo.sel_opt1_val)>0 AND LENGTH(oo.sel_opt2_val)>0, ', ', '')
                            ,IF(LENGTH(oo.sel_opt2_title)>0,CONCAT(oo.sel_opt2_title,'-'),''), IFNULL(oo.sel_opt2_val,'') -- 선택옵션2
                         ) AS opt
                        ,oo.opt_qty AS item_qty
                        ,psi.purchase_store_info_no AS ord_no
                FROM 	purchase_store_info psi
                        INNER JOIN purchase_order po
                ON 		psi.purchase_order_no = po.purchase_order_no
                        INNER JOIN order_item oi
                ON 		po.purchase_order_no = oi.purchase_order_no
                        INNER JOIN order_opt oo
                ON 		oi.order_item_no = oo.order_item_no
                AND     oo.opt_no > 0
                WHERE 	psi.purchase_store_info_no = vPurchaseStoreInfoNo;
				*/
                IF vErrorNo < 0 THEN
                    ROLLBACK;

                    UPDATE  purchase_order
                    SET      omni_if_yn = 'E'
                            ,omni_if_dt = NOW()
                    WHERE   purchase_order_no = vPurchaseOrderNo;

                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, 'ERROR', CONCAT('purchase_order_no: ',vPurchaseOrderNo,', purchase_store_info_no: ',vPurchaseStoreInfoNo), CONCAT('ERROR ',vSqlErrno,' (',vSqlstate,'): ',vSqlMessage), 'INSERT INTO rf_omni_ord_sback_useopt Fail', NULL, NULL);
                    SET vErrorMessage = CONCAT('ERROR ',vSqlErrno,' (',vSqlstate,'): ',vSqlMessage);
                    SET vFailCount = vFailCount + 1;

                    ITERATE REPEAT_PURCHASE_STORE_INFO;
                END IF;

                SELECT FOUND_ROWS() INTO vCntSbackUseOpt;


                -- INSERT rf_omni_ord_combine_info(합배송정보)
                -- Refit 주문번호가 아닌 Omni쪽 주문번호를 전송할 수 있도록 쿼리 수정
                INSERT INTO rf_omni_ord_combine_info(
                     orgn_ord_no
                    ,cmbn_ord_no
                    ,gift_yn
                    ,gift_ty
                )
                SELECT
					org_psi.purchase_store_info_no AS orgn_ord_no,
					cmbn_psi.purchase_store_info_no AS cmbn_ord_no,
					IFNULL(coi.gift_yn, 'N') AS gift_yn,
					coi.gift_type AS gift_ty
				FROM
					purchase_store_info cmbn_psi
					INNER JOIN combine_order_info coi ON cmbn_psi.purchase_order_no = coi.cmbn_purchase_order_no
					INNER JOIN purchase_store_info org_psi ON org_psi.purchase_order_no = coi.org_purchase_order_no AND org_psi.ship_method IN ('TD_DRCT' , 'TD_PICK')
				WHERE
					cmbn_psi.purchase_store_info_no = vPurchaseStoreInfoNo
				AND cmbn_psi.cmbn_yn = 'Y';
                /*
                SELECT   coi.org_purchase_order_no AS orgn_ord_no
                        ,coi.cmbn_purchase_order_no AS cmbn_ord_no
                        ,IFNULL(coi.gift_yn,'N') AS gift_yn
                        ,coi.gift_type AS gift_ty
                FROM    combine_order_info coi
                WHERE   coi.cmbn_purchase_order_no = vPurchaseOrderNo;
                */


                IF vErrorNo < 0 THEN
                    ROLLBACK;

                    UPDATE  purchase_order
                    SET      omni_if_yn = 'E'
                            ,omni_if_dt = NOW()
                    WHERE   purchase_order_no = vPurchaseOrderNo;

                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, 'ERROR', CONCAT('purchase_order_no: ',vPurchaseOrderNo,', purchase_store_info_no: ',vPurchaseStoreInfoNo), CONCAT('ERROR ',vSqlErrno,' (',vSqlstate,'): ',vSqlMessage), 'INSERT INTO rf_omni_ord_combine_info Fail', NULL, NULL);
                    SET vErrorMessage = CONCAT('ERROR ',vSqlErrno,' (',vSqlstate,'): ',vSqlMessage);
                    SET vFailCount = vFailCount + 1;

                    ITERATE REPEAT_PURCHASE_STORE_INFO;
                END IF;

                SELECT FOUND_ROWS() INTO vCntCombineInfo;


                -- INSERT rf_omni_purchase_gift(사은품)
                INSERT INTO rf_omni_purchase_gift(
                     purchase_gift_no
                    ,purchase_store_info_no
                    ,purchase_order_no
                    ,store_id
                    ,gift_no
                    ,gift_msg
                    ,gift_target_type
                    ,gift_give_type
                    ,std_val
                )
                SELECT   purchase_gift_no
                        ,purchase_store_info_no
                        ,purchase_order_no
                        ,store_id
                        ,gift_no
                        ,gift_msg
                        ,gift_target_type
                        ,gift_give_type
                        ,std_val
                FROM    purchase_gift gift
                WHERE   gift.purchase_order_no = vPurchaseOrderNo
                AND     gift.purchase_store_info_no = vPurchaseStoreInfoNo;

                IF vErrorNo < 0 THEN
                    ROLLBACK;

                    UPDATE  purchase_order
                    SET      omni_if_yn = 'E'
                            ,omni_if_dt = NOW()
                    WHERE   purchase_order_no = vPurchaseOrderNo;

                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, 'ERROR', CONCAT('purchase_order_no: ',vPurchaseOrderNo,', purchase_store_info_no: ',vPurchaseStoreInfoNo), CONCAT('ERROR ',vSqlErrno,' (',vSqlstate,'): ',vSqlMessage), 'INSERT INTO rf_omni_purchase_gift Fail', NULL, NULL);
                    SET vErrorMessage = CONCAT('ERROR ',vSqlErrno,' (',vSqlstate,'): ',vSqlMessage);
                    SET vFailCount = vFailCount + 1;

                    ITERATE REPEAT_PURCHASE_STORE_INFO;
                END IF;

                SELECT FOUND_ROWS() INTO vCntGift;


                -- INSERT rf_omni_purchase_gift_item(사은품상품)
                INSERT INTO rf_omni_purchase_gift_item(
                     purchase_gift_item_seq
                    ,purchase_gift_no
                    ,item_no
                )
                SELECT   item.purchase_gift_item_seq
                        ,item.purchase_gift_no
                        ,item.item_no
                FROM    purchase_gift gift
                        INNER JOIN purchase_gift_item item
                ON      gift.purchase_gift_no = item.purchase_gift_no
                WHERE   gift.purchase_order_no = vPurchaseOrderNo
                AND     gift.purchase_store_info_no = vPurchaseStoreInfoNo;

                IF vErrorNo < 0 THEN
                    ROLLBACK;

                    UPDATE  purchase_order
                    SET      omni_if_yn = 'E'
                            ,omni_if_dt = NOW()
                    WHERE   purchase_order_no = vPurchaseOrderNo;

                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, 'ERROR', CONCAT('purchase_order_no: ',vPurchaseOrderNo,', purchase_store_info_no: ',vPurchaseStoreInfoNo), CONCAT('ERROR ',vSqlErrno,' (',vSqlstate,'): ',vSqlMessage), 'INSERT INTO rf_omni_purchase_gift_item Fail', NULL, NULL);
                    SET vErrorMessage = CONCAT('ERROR ',vSqlErrno,' (',vSqlstate,'): ',vSqlMessage);
                    SET vFailCount = vFailCount + 1;

                    ITERATE REPEAT_PURCHASE_STORE_INFO;
                END IF;

                SELECT FOUND_ROWS() INTO vCntGiftItem;


                -- INSERT rf_omni_ord_pickup_info(픽업정보)
                INSERT INTO rf_omni_ord_pickup_info(
                	 ord_no
                	,store_id
                	,place_typ
                	,locker_no
                	,locker_pwd
                	,delivery_dt
                	,place_addr_txt
                	,send_sms_yn
                	,rcptname_txt
                	,rcptmobil_txt
                	,safen_yn
                	,safen_rcpt_mobil
                	,reg_dt
                )
                SELECT	  psi.purchase_store_info_no 						AS ord_no
						, LPAD(psi.origin_store_id,4,'0') 					AS store_id
						, opi.place_no 										AS place_typ
						, opi.locker_no 									AS locker_no
						, opi.locker_passwd 								AS locker_pwd
						, psi.ship_dt 										AS delivery_dt
						, CONCAT(opi.place_nm,' ',place_addr,' ',tel_no) 	AS place_addr_txt
						, 'N' 												AS send_sms_yn
						, sa.receiver_nm 									AS rcptname_txt
						, sa.ship_mobile_no 								AS rcptmobil_txt
						, sa.safety_use_yn 									AS safen_yn
						, (SELECT si.issue_phone_no
						   FROM   safety_issue si
						   WHERE  si.bundle_no = bun.bundle_no
						   LIMIT 1
						  ) 												AS safen_rcpt_mobil
						, NOW() 											AS reg_dt
				FROM	purchase_store_info psi
						, bundle bun
						, shipping_addr sa
						, order_pickup_info opi
				WHERE 	psi.purchase_store_info_no = bun.purchase_store_info_no
				AND 	bun.bundle_no = sa.bundle_no
				AND 	opi.bundle_no = sa.bundle_no
				AND 	psi.ship_method IN ('TD_DRCT','TD_PICK','TD_QUICK')
				AND 	psi.purchase_store_info_no = vPurchaseStoreInfoNo;

				IF vErrorNo < 0 THEN
                    ROLLBACK;

                    UPDATE  purchase_order
                    SET      omni_if_yn = 'E'
                            ,omni_if_dt = NOW()
                    WHERE   purchase_order_no = vPurchaseOrderNo;

                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, 'ERROR', CONCAT('purchase_order_no: ',vPurchaseOrderNo,', purchase_store_info_no: ',vPurchaseStoreInfoNo), CONCAT('ERROR ',vSqlErrno,' (',vSqlstate,'): ',vSqlMessage), 'INSERT INTO rf_omni_ord_pickup_info Fail', NULL, NULL);
                    SET vErrorMessage = CONCAT('ERROR ',vSqlErrno,' (',vSqlstate,'): ',vSqlMessage);
                    SET vFailCount = vFailCount + 1;

                    ITERATE REPEAT_PURCHASE_STORE_INFO;
                END IF;

                SELECT FOUND_ROWS() INTO vCntPickupInfo;


                -- INSERT rf_omni_trade_header_tran(주문헤더)
                -- HYPER/FC 주문구분(hyper_fc_ty) 확인
                SELECT   IFNULL(SUM(CASE WHEN IFNULL(oi.fc_item_yn,'N') = 'N' THEN 1 ELSE 0 END),0)
                        ,IFNULL(SUM(CASE WHEN IFNULL(oi.fc_item_yn,'N') = 'Y' THEN 1 ELSE 0 END),0)
                INTO     vHyperCnt
                        ,vFcCnt
                FROM    purchase_store_info psi
                        INNER JOIN purchase_order po
                ON		psi.purchase_order_no = po.purchase_order_no
                        INNER JOIN order_item oi
                ON      po.purchase_order_no = oi.purchase_order_no
                AND     psi.store_id = oi.store_id
                WHERE   psi.purchase_store_info_no = vPurchaseStoreInfoNo;

                INSERT INTO rf_omni_trade_header_tran(
                     ord_no
                    ,refit_ord_no
                    ,store_id
                    ,user_id
                    ,ord_ty
                    ,sendname_txt
                    ,rcptname_txt
                    ,rcptmobil_txt
                    ,rcptzip_txt
                    ,rcptadd1_txt
                    ,rcptadd2_txt
                    ,delivery_dt
                    ,shift_id
                    ,slot_id
                    ,order_st
                    ,delivery_st
                    ,trade_date
                    ,trade_time
                    ,pos_no
                    ,trade_no
                    ,ftype_code
                    ,rtn_ord_no
                    ,place_no
                    ,locker_no
                    ,summary_fraction
                    ,delivery_cd
                    ,part_cancel
                    ,part_cancel_flag
                    ,partner_company
                    ,delivery_typ
                    ,pickupgroup_cd
                    ,click_collect_yn
                    ,safen_yn
                    ,safen_rcpt_mobil
                    ,mall_delivery_type
                    ,hyper_fc_ty
                    ,fc_store_id
                    ,virtual_tran_yn
                    ,delivery_msg
                    ,trade_tot_amount
                    ,trade_amount
                    ,store_type
                    ,cnt_item_tran
                    ,cnt_combine_info
                    ,cnt_sback_useopt
                    ,cnt_gift
                    ,cnt_gift_item
                    ,cnt_pickup_info
                    ,card_gb
                    ,noncust_yn
                    ,aurora_ship_msg_cd
                    ,aurora_ship_msg
                    ,partner_ord_no
                    ,no_night_talk_send_yn
                )
                SELECT 	 psi.purchase_store_info_no AS ord_no
                        ,po.purchase_order_no AS refit_ord_no
                        ,LPAD(psi.origin_store_id,4,'0') AS store_id
                        ,po.user_no AS user_id
                        ,CASE WHEN po.platform = 'PC' THEN '44010'
                              WHEN po.platform = 'APP' THEN '44070'
                              WHEN po.platform = 'MWEB' THEN '44090'
                         END AS ord_ty
                        ,po.buyer_nm AS sendname_txt
                        ,sa.receiver_nm AS rcptname_txt
                        ,TRIM(sa.ship_mobile_no) AS rcptmobil_txt
                        ,sa.zipcode AS rcptzip_txt
                        ,ifnull(sa.road_base_addr, sa.base_addr) AS rcptadd1_txt
                        ,ifnull(sa.road_detail_addr, sa.detail_addr) AS rcptadd2_txt
                        ,CASE WHEN vStoreType = 'EXP' THEN DATE_FORMAT(psi.ship_dt, '%Y-%m-%d') ELSE psi.ship_dt END AS delivery_dt
                        ,psi.shift_id
                        ,psi.slot_id
                        ,CASE WHEN pay.payment_status = 'P3' THEN '24010'
                              WHEN pay.payment_status = 'P5' THEN '24010'
                         END AS order_st -- P1:결제요청, P3:결제완료(입금확인), P4:결제실패, P5:가상결제
                        ,CASE WHEN vStoreType = 'EXP' THEN '26020'
                              ELSE '26010'
                         END AS delivery_st -- NN:대기, D1:주문확인전(결제완료)-26010, D2:주문확인(상품준비중)-26020, D3:배송중-26040, D4:배송완료-24060, D5:구매확정
                        ,DATE_FORMAT(psi.trade_dt, '%Y%m%d') AS trade_date
                        ,DATE_FORMAT(po.order_dt, '%H%i%s') AS trade_time
                        ,LPAD(psi.pos_no,6,'0') AS pos_no
                        ,LPAD(psi.trade_no,4,'0') AS trade_no
                        ,'1' AS ftype_code -- 1:통상,2:반품,4:정산,5:공병
                        ,org_psi.purchase_store_info_no AS rtn_ord_no -- 원주문번호(대체주문일 경우)
                        ,opi.place_no
                        ,opi.locker_no
                        ,'0' AS summary_fraction -- 0:정상처리
                        ,CASE WHEN vStoreType = 'EXP' THEN '37040'
                              ELSE '37010'
                         END AS delivery_cd -- TD_DLV:택배배송,TD_DRCT:직접배송,TD_PICK:방문수령,TD_POST:우편배송,TD_QUICK:퀵배송,TD_LOCKER:락커수령
                        ,'0' AS part_cancel
                        ,'N' AS part_cancel_flag
                        , CASE WHEN po.market_type = 'NAVER'  THEN '54300'
                        	   WHEN po.market_type = 'ELEVEN' THEN '54310'
                       		   ELSE NULL
                       	 END AS  partner_company -- 제휴사코드
                        ,CASE WHEN vStoreType = 'EXP' THEN '300'
                              ELSE '100'
                         END AS delivery_typ -- (옴니 100: 점포배송(자차), 200: 업체배송, 대형가전, 300: 점포배송, 업체배송 or 대형가전 혼합주문)
                        ,NULL AS pickupgroup_cd -- (옴니 00003: 사전예약상품(절임,명절), 00004: 대량주문(메뉴얼))
                        ,psi.pickup_yn AS click_collect_yn
                        ,sa.safety_use_yn AS safen_yn
                        ,saf.issue_phone_no AS safen_rcpt_mobil
                        ,'31010' AS mall_delivery_type -- 31010:TD, 31020:DC/DS, 31030:TD/DC or DS
                        ,CASE WHEN vHyperCnt > 0 AND vFcCnt > 0 THEN 'A2003'
                              WHEN vHyperCnt > 0 THEN 'A2001'
                              WHEN vFcCnt > 0 THEN 'A2002'
                         END AS hyper_fc_ty -- (옴니 A2001: Hyper(+ DC/DS), A2002: FC(+ DC/DS), A2003: Hyper + FC(+ DC/DS), A2004: DC/DS만)
                        ,psi.fc_store_id
                        ,CASE WHEN vFcCnt > 0 THEN 'Y'
                              ELSE 'N'
                         END AS virtual_tran_yn
                        ,sa.store_ship_msg AS delivery_msg
                        ,psi.tot_item_price
                        	+ psi.tot_ship_price
                        	+ psi.tot_island_ship_price AS trade_tot_amount
                        ,psi.tot_item_price
				        	+ psi.tot_ship_price
				       		+ psi.tot_island_ship_price
				       		- psi.tot_item_discount_amt
				       		- psi.tot_ship_discount_amt
				       		- psi.tot_promo_discount_amt
				       		- psi.tot_emp_discount_amt
				       		- psi.tot_card_discount_amt AS trade_amount
                        ,vStoreType
                        ,vCntItemTran
                        ,vCntCombineInfo
                        ,vCntSbackUseOpt
                        ,vCntGift
                        ,vCntGiftItem
                        ,vCntPickupInfo
                        ,CASE WHEN sa.corp_order_yn = 'Y' THEN '1'
                              ELSE NULL
                         END AS card_gb
                        ,po.nomem_order_yn AS noncust_yn
                        ,sa.aurora_ship_msg_cd
                        ,sa.aurora_ship_msg
                        ,po.market_order_no
                        ,IFNULL(po.delivery_complete_alarm_yn,'N')
                FROM 	purchase_store_info psi
                        INNER JOIN purchase_order po
                ON		psi.purchase_order_no = po.purchase_order_no
                        INNER JOIN bundle bun
                ON		psi.purchase_store_info_no = bun.purchase_store_info_no
                AND     psi.purchase_order_no = bun.purchase_order_no
                        INNER JOIN shipping_addr sa
                ON		bun.bundle_no = sa.bundle_no
                        LEFT OUTER JOIN safety_issue saf
                ON		sa.ship_addr_no = saf.ship_addr_no
                        INNER JOIN payment pay
                ON 		po.payment_no = pay.payment_no
                AND		pay.payment_status IN ('P3','P5')
                		LEFT OUTER JOIN purchase_store_info org_psi
                ON		po.order_type = 'ORD_SUB'
                AND		po.org_purchase_order_no = org_psi.purchase_order_no
                AND		org_psi.ship_method IN ('TD_DRCT', 'TD_PICK', 'TD_LOCKER', 'TD_QUICK')
                		LEFT OUTER JOIN order_pickup_info opi
                ON		psi.purchase_order_no = opi.purchase_order_no
                WHERE	psi.purchase_store_info_no = vPurchaseStoreInfoNo;

                IF vErrorNo < 0 THEN
                    ROLLBACK;

                    UPDATE  purchase_order
                    SET      omni_if_yn = 'E'
                            ,omni_if_dt = NOW()
                    WHERE   purchase_order_no = vPurchaseOrderNo;

                    CALL sp_lgw_batch_history_info(vProcedureNm, vLgwBatchNo, 'ERROR', CONCAT('purchase_order_no: ',vPurchaseOrderNo,', purchase_store_info_no: ',vPurchaseStoreInfoNo), CONCAT('ERROR ',vSqlErrno,' (',vSqlstate,'): ',vSqlMessage), 'INSERT INTO rf_omni_trade_header_tran Fail', NULL, NULL);
                    SET vErrorMessage = CONCAT('ERROR ',vSqlErrno,' (',vSqlstate,'): ',vSqlMessage);
                    SET vFailCount = vFailCount + 1;

                    ITERATE REPEAT_PURCHASE_STORE_INFO;
                END IF;

                -- UPDATE purchase_order
                SELECT  COUNT(*)
                INTO    vPurchaseStoreInfoCnt
                FROM    purchase_store_info
                WHERE   purchase_order_no = vPurchaseOrderNo
                AND     ship_method IN ('TD_DRCT', 'TD_PICK', 'TD_LOCKER', 'TD_QUICK');

                SELECT  COUNT(*)
                INTO    vTradeHeaderTranCnt
                FROM    rf_omni_trade_header_tran
                WHERE   refit_ord_no = vPurchaseOrderNo;

                IF vPurchaseStoreInfoCnt = vTradeHeaderTranCnt THEN
                    UPDATE  purchase_order
                    SET      omni_if_yn = 'Y'
                            ,omni_if_dt = NOW()
                    WHERE   purchase_order_no = vPurchaseOrderNo
                    AND     omni_if_yn = 'I';
                END IF;

                SET vSuccessCount = vSuccessCount + 1;

                COMMIT;

                SET vNoMoreRows = FALSE;
            END IF;

            UNTIL vNoMoreRows
        END REPEAT;

    CLOSE cursorPurchaseStoreInfo;

	-- ISOLATION LEVEL 설정 원복
    SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;

    CALL sp_lgw_batch_history(vProcedureNm, 'END', vLgwBatchNo, vFailCount, vSuccessCount,vTotalCount, vErrorMessage, NULL, NULL, NULL, NULL, NULL, @outLgwBatchNo);

END MAIN_PROCESS