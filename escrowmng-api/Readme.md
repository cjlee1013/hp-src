# ESCROWMNG-API

---
주문/결제/클레임 Admin API 시스템 입니다.

### Environment
####- Version
- PlusFramework : 1.0.0 RELEASE
- SpringBoot : 2.1.6.RELEASE
- Spring : 5.1.8 RELEASE
- Gradle : 5.6.4
- Java : 1.11 (use openJDK)
- Tomcat : 9.x

-. 문의 : 모바일플랫폼혁신팀 주문/결제/클레임 업무 담당자

