package kr.co.homeplus.partner.web.common.service.impl;

import java.util.List;
import kr.co.homeplus.partner.web.common.model.home.SalesActInfo;
import kr.co.homeplus.partner.web.common.service.HomeDashboardService;
import kr.co.homeplus.partner.web.core.PartnerWebApplication;
import kr.co.homeplus.partner.web.manage.model.PartnerHarmfulItemListSelectDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles(value = "local")
@SpringBootTest(classes = PartnerWebApplication.class)
public class HomeDashboardServiceImplTests {
    @Autowired
    private HomeDashboardService homeDashboardService;

    @Test
    public void test_getSalesActInfo() {
        SalesActInfo salesActInfo = homeDashboardService.getSalesActInfo("namshop");
        Assert.assertNotNull(salesActInfo);
    }

    @Test
    public void test_getHarmfulItemList() {
        List<PartnerHarmfulItemListSelectDto> harmfulItemList = homeDashboardService
            .getPartnerHarmfulItemList("namshop");
        Assert.assertNotNull(harmfulItemList);
    }
}