package kr.co.homeplus.partner.web.core.controller;

import kr.co.homeplus.partner.web.core.annotation.CertificationNeedLess;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LbStatusController {

    @GetMapping("/lbStatusCheck")
    @CertificationNeedLess
    public ResponseEntity<String> getLbStatusCheck() {
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }
}