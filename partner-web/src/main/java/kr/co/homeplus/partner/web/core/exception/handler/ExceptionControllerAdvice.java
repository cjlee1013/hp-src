package kr.co.homeplus.partner.web.core.exception.handler;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.co.homeplus.partner.web.common.exception.NotAllowedUrlDownloadFileException;
import kr.co.homeplus.partner.web.common.exception.UrlDownloadFileFailedException;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.config.PartnerProperties;
import kr.co.homeplus.partner.web.core.exception.CertificationException;
import kr.co.homeplus.partner.web.core.exception.Check90DaysSamePasswordInterceptorException;
import kr.co.homeplus.partner.web.core.exception.LoginFailCountExceededLockException;
import kr.co.homeplus.partner.web.core.exception.LoginIdSaveCookieServiceException;
import kr.co.homeplus.partner.web.core.exception.LongTermUnusedLockException;
import kr.co.homeplus.partner.web.core.exception.MobileCertNoVerifyFailedException;
import kr.co.homeplus.partner.web.core.exception.SellerAgreeInterceptorException;
import kr.co.homeplus.partner.web.core.exception.SessionNotFoundException;
import kr.co.homeplus.partner.web.core.service.impl.MaintenanceException;
import kr.co.homeplus.partner.web.core.usermenu.exception.MenuLoadFailException;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.exception.ResourceClientException;
import kr.co.homeplus.plus.api.support.client.exception.ResourceTimeoutException;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.tomcat.util.http.fileupload.FileUploadBase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Slf4j
@ControllerAdvice
@RequiredArgsConstructor
public class ExceptionControllerAdvice {
    private final CertificationService certificationService;
    private final PartnerProperties partnerProperties;

    @Value("${spring.profiles.active}")
    private String profilesActive;

    /** 500 error page view name **/
    private static final String ERROR_PAGE_PATH = "/core/error/error";

    @ExceptionHandler(Throwable.class)
    public Object handleServerError(final HttpServletRequest req, final Exception ex) {
        //응답할 ResponseObject 설정
        log.error("Throwable Error! <uri:{}>,<trace:{}>", req.getRequestURI(),
            ExceptionUtils.getStackTrace(ex));

        //응답할 ResponseObject 설정
        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .data(null)
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9001.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9001.getDesc())
            .build();

        return returnResponse(req.getRequestURI(), ex, responseObject, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Resttemplate 사용 시 resource api 호출 이거나, gateway 를 통한 호출 시 발생하는 예외를 처리하는 핸들러<p> <p/> 전달되는
     * {@link Exception}의 type에 따라 리턴되는 값이 변경된다. 세부내용은 하단의 내용을 참조한다. <p/>
     * <pre>
     * 1. {@link RestClientResponseException} 이 발생 할 경우
     *  - 리턴 : status, ResponseBody 그대로 전달
     *
     * 2. 기타 그외의 exception
     *  - 리턴 : status만 변경(500), ResponseObject를 전달
     *   * unknownHostException, {@link java.io.IOException} 등의 기타 {@link Exception} 처리
     *   * 에러 내용을 {@link ResponseObject}로 담아 전달
     * </pre>
     *
     * @param req HttpServletRequest
     * @param e  ResourceClientException
     * @return responseEntity
     * @see RestClientResponseException
     * @see ResourceClientException
     */
    @ExceptionHandler(ResourceClientException.class)
    public Object handleResourceClientException(final HttpServletRequest req,
        final HttpServletResponse res, final ResourceClientException e) {
        final Throwable cause = e.getCause();

        /*
         * 1. RestClientResponseException 예외가 발생 할 경우
         *  1.1 url이 .json 일 경우 : status, ResponseBody 그대로 전달
         *  1.2 url이 .json이 아닐 경우 : 에러 페이지로 이동
         */
        if (cause instanceof RestClientResponseException) {
            res.setStatus(e.getHttpStatus());
            final String responseBodyAsString = ((RestClientResponseException) cause)
                .getResponseBodyAsString();

            log.error(
                "ResourceClientException Error! <uri:{}>,<resourceUrl:{}>,<httpStatus:{}>,<responseBody:{}>",
                req.getRequestURI(), e.getResourceUrl(), e.getHttpStatus(), responseBodyAsString);
            return returnResponse(req.getRequestURI(), e, e.getResponseBody(),
                HttpStatus.valueOf(e.getHttpStatus()));
        }
        /*
         * 2. 기타 그외의 exception
         *  2.1 url이 .json 일 경우 : status 지정(500), 에러 내용은 {@link ResponseObject}로 담아 전달
         *  2.2 url이 .json이 아닐 경우 : 에러 페이지로 이동
         */
        else {
            res.setStatus(500);
            ResponseObject<String> responseObject = setResponseForNotRestClientResponseException(e);

            log.error(
                "ResourceClientException Error! <uri:{}>,<resourceUrl:{}>,<httpStatus:{}>,<responseObject:{}>,<trace:{}>",
                req.getRequestURI(), e.getResourceUrl(), e.getHttpStatus(), responseObject,
                ExceptionUtils.getStackTrace(e));
            return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * RestClientResponseException 이 아닌 Exception의 유형에 따라 에러코드와 문구가 담긴 ResponseObject 를 반환합니다.
     * @param e {@link ResourceClientException}
     * @return 에러코드, 문구가 담긴 ResponseObject 를 반환합니다.
     */
    private ResponseObject<String> setResponseForNotRestClientResponseException(
        final ResourceClientException e) {

        String returnCode;
        String returnMessage;

        if ( e.getCause() instanceof ResourceAccessException) {
            returnCode = ExceptionCode.SYS_ERROR_CODE_9103.getCode();
            returnMessage = ExceptionCode.SYS_ERROR_CODE_9103.getDesc();
        } else {
            returnCode = ExceptionCode.SYS_ERROR_CODE_9102.getCode();
            returnMessage = ExceptionCode.SYS_ERROR_CODE_9102.getDesc();
        }
        return ResourceConverter
            .toResponseObject(null, HttpStatus.INTERNAL_SERVER_ERROR, returnCode, returnMessage);
    }

    /**
     * {@link ResourceTimeoutException} 발생 시 예외를 처리하는 핸들러 입니다.<p>
     * <p>
     * {@link ResourceTimeoutException}은 SocketTimeoutException, ConnectTimeoutException 발생 할 경우
     * throw 하는 {@link Exception} 입니다.<br>
     * <pre>
     *  1. url이 .json 일 경우 : status 지정(408), 에러 내용은 {@link ResponseObject}로 담아 전달
     *  2. url이 .json이 아닐 경우 : 에러 페이지로 이동
     * </pre>
     *
     * @param req HttpServletRequest
     * @param ex  ResourceTimeoutException
     * @return responseEntity
     */
    @ExceptionHandler(ResourceTimeoutException.class)
    public Object handleResourceTimeoutException(final HttpServletRequest req,
        final ResourceTimeoutException ex) {
        log.error("ResourceTimeoutException Error! <uri:{}>,<httpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.REQUEST_TIMEOUT, ExceptionUtils.getStackTrace(ex));

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .data(null)
            .status(HttpStatus.REQUEST_TIMEOUT)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9104.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9104.getDesc())
            .build();

        return returnResponse(req.getRequestURI(), ex, responseObject, HttpStatus.REQUEST_TIMEOUT);
    }

    /**
     * 404 error not found 예외처리<p>
     *
     * @param req {@link HttpServletRequest}
     * @param ex  NoHandlerFoundException
     * @return Url의 suffix가 .json이면 'httpStatus'와 json 타입으로 return, 아닐 경우 error Page로 이동
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Object noHandlerFoundException(final HttpServletRequest req,
        final NoHandlerFoundException ex) {
        log.error("404 Not Found Error!!! <uri:{}>,<requestMethod:{}>,<httpStatus:{}>",
            req.getRequestURI(), req.getMethod(), HttpStatus.NOT_FOUND.value());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .data(null)
            .status(HttpStatus.NOT_FOUND)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9105.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9105.getDesc())
            .build();
        return returnResponse(req.getRequestURI(), ex, responseObject, HttpStatus.NOT_FOUND,
            "/core/error/error404");
    }

    @ExceptionHandler(MaintenanceException.class)
    public ModelAndView maintenanceExceptionError() {
        ModelAndView mav = new ModelAndView();
        mav.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        mav.setViewName("/core/error/error503");
        return mav;
    }

    /**
     * 메뉴 파일 로드시 예외 처리
     * @param e MenuLoadFailException
     * @return ModelAndView
     */
    @ExceptionHandler(MenuLoadFailException.class)
    public Object handleMenuLoadFailException(final MenuLoadFailException e) {
        log.error("menu.json load fail! trace:{}", ExceptionUtils.getStackTrace(e));

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .data(null)
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9001.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9001.getDesc())
            .build();

        ModelAndView mav = new ModelAndView();
        setErrorModelAndView(mav, HttpStatus.INTERNAL_SERVER_ERROR,
            ExceptionUtils.getStackTrace(e), responseObject);
        mav.setViewName(ERROR_PAGE_PATH);
        return mav;
    }

    /**
     * {@link LoginIdSaveCookieServiceException} 예외처리
     *
     * @param req HttpServletRequest
     * @param e   LoginIdSaveCookieServiceException
     * @return ResponseEntity
     */
    @ExceptionHandler(LoginIdSaveCookieServiceException.class)
    public Object idSaveCookieServiceException(final HttpServletRequest req,
        final LoginIdSaveCookieServiceException e) {
        log.error("LoginIdSaveCookieServiceException! <uri:{}>,<trace:{}>",
            req.getRequestURI(), ExceptionUtils.getStackTrace(e));

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.UNAUTHORIZED)
            .data(e.getMessage())
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9209.getCode())
            .returnMessage(StringUtils
                .defaultString(e.getMessage(), ExceptionCode.SYS_ERROR_CODE_9209.getDesc()))
            .build();

        if (isRestUri(req.getRequestURI())) {
            return new ResponseEntity<>(responseObject, createHttpHeaders(),
                HttpStatus.UNAUTHORIZED);
        } else {
            ModelAndView mav = new ModelAndView();
            mav.setViewName("redirect:" + partnerProperties.getCertificationRedirectUrl());
            return mav;
        }
    }

    /**
     * 로그인 인증 실패 시 예외 처리
     *
     * @param req HttpServletRequest
     * @param e  CertificationException
     * @return Url의 suffix가 '.json'이면 json 타입으로 return, 아닐 경우 error Page로 이동
     */
    @ExceptionHandler(CertificationException.class)
    public Object certificationException(final HttpServletRequest req,
        final CertificationException e, final RedirectAttributes redirectAttributes) {
        log.error("CertificationException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.UNAUTHORIZED.value(), ExceptionUtils.getStackTrace(e));

        String partnerId = "";
        String partnerPw = "";

        if (ObjectUtils.isNotEmpty(e.getLoginParam())) {
            partnerId = StringUtils.defaultString(e.getLoginParam().getPartnerId());
            partnerPw = StringUtils.defaultString(e.getLoginParam().getPartnerPw());
        }

        final Map<String, Object> data = Map.of("partnerId", partnerId, "partnerPw", partnerPw);

        final ResponseObject<Map<String, Object>> responseObject = ResponseObject.Builder.<Map<String, Object>>builder()
            .status(HttpStatus.UNAUTHORIZED)
            .data(data)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9200.getCode())
            .returnMessage(StringUtils
                .defaultString(e.getMessage(), ExceptionCode.SYS_ERROR_CODE_9200.getDesc()))
            .build();

        //인증실패 시 세션이 만료된 로그인 쿠키가 존재할 경우를 대비하여 삭제 처리
        certificationService.processUserLogout();

        if (isRestUri(req.getRequestURI())) {
            HttpHeaders httpHeaders = createHttpHeaders();
            return new ResponseEntity<>(responseObject, httpHeaders, HttpStatus.UNAUTHORIZED);
        } else {
            ModelAndView mav = new ModelAndView();
            redirectAttributes.addFlashAttribute("returnMessage", StringUtils
                .defaultString(e.getMessage(), ExceptionCode.SYS_ERROR_CODE_9200.getDesc()));
            redirectAttributes.addFlashAttribute("partnerId",
                StringUtils.defaultString(partnerId));
            redirectAttributes.addFlashAttribute("partnerPw",
                StringUtils.defaultString(partnerPw));
            mav.setViewName("redirect:" + partnerProperties.getCertificationRedirectUrl());
            return mav;
        }
    }

    /**
     * 로그인 실패횟수 초과로 잠금 처리 된 리턴 코드일 경우 예외 처리
     *
     * @param req HttpServletRequest
     * @param e  {@link LoginFailCountExceededLockException}
     * @return Url의 suffix가 '.json'이면 json 타입으로 return, 아닐 경우 error Page로 이동
     */
    @ExceptionHandler(LoginFailCountExceededLockException.class)
    public ResponseEntity<ResponseObject<Map<String, String>>> loginFailCountExceededLockException(
        final HttpServletRequest req, final LoginFailCountExceededLockException e) {
        log.error("LoginFailCountExceededLockException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.UNAUTHORIZED.value(), ExceptionUtils.getStackTrace(e));

        String partnerId = "";
        String partnerPw = "";

        if (ObjectUtils.isNotEmpty(e.getLoginParam())) {
            partnerId = StringUtils.defaultString(e.getLoginParam().getPartnerId());
            partnerPw = StringUtils.defaultString(e.getLoginParam().getPartnerPw());
        }

        final Map<String, String> data = Map.of("partnerId", partnerId, "partnerPw", partnerPw);

        final ResponseObject<Map<String, String>> responseObject = ResponseObject.Builder.<Map<String, String>>builder()
            .status(HttpStatus.UNAUTHORIZED)
            .data(data)
            .returnCode(e.getReturnCode())
            .returnMessage(StringUtils
                .defaultString(e.getMessage(), ExceptionCode.SYS_ERROR_CODE_9206.getDesc()))
            .build();

        //인증실패 시 세션이 만료된 로그인 쿠키가 존재할 경우를 대비하여 삭제 처리
        certificationService.processUserLogout();

        return new ResponseEntity<>(responseObject, createHttpHeaders(), HttpStatus.UNAUTHORIZED);
    }

    /**
     * 장기 미사용 잠금 처리 된 리턴 코드일 경우 예외 처리를 합니다.
     *
     * @param req HttpServletRequest
     * @param e  {@link LongTermUnusedLockException}
     * @return 에러코드 및 노출문구를 json 타입으로 리턴합니다.
     */
    @ExceptionHandler(LongTermUnusedLockException.class)
    public ResponseEntity<ResponseObject<Map<String, String>>> longTermUnusedLockException(
        final HttpServletRequest req, final LongTermUnusedLockException e) {
        log.error("LongTermUnusedLockException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.UNAUTHORIZED.value(), ExceptionUtils.getStackTrace(e));

        String partnerId = "";
        String partnerPw = "";

        if (ObjectUtils.isNotEmpty(e.getLoginParam())) {
            partnerId = StringUtils.defaultString(e.getLoginParam().getPartnerId());
            partnerPw = StringUtils.defaultString(e.getLoginParam().getPartnerPw());
        }

        final Map<String, String> data = Map.of("partnerId", partnerId, "partnerPw", partnerPw);

        final ResponseObject<Map<String, String>> responseObject = ResponseObject.Builder.<Map<String, String>>builder()
            .status(HttpStatus.UNAUTHORIZED)
            .data(data)
            .returnCode(e.getReturnCode())
            .returnMessage(StringUtils
                .defaultString(e.getMessage(), ExceptionCode.SYS_ERROR_CODE_9207.getDesc()))
            .build();

        //인증실패 시 세션이 만료된 로그인 쿠키가 존재할 경우를 대비하여 삭제 처리
        certificationService.processUserLogout();

        return new ResponseEntity<>(responseObject, createHttpHeaders(), HttpStatus.UNAUTHORIZED);
    }

    /**
     * {@link SessionNotFoundException} 예외처리
     *
     * @param req HttpServletRequest
     * @param e   {@link SessionNotFoundException}
     * @return ResponseEntity
     */
    @ExceptionHandler(SessionNotFoundException.class)
    public Object sessionNotFoundExceptionHandler(final HttpServletRequest req,
        final SessionNotFoundException e) {
        log.error("SessionNotFoundException! <uri:{}>,<trace:{}>",
            req.getRequestURI(), ExceptionUtils.getStackTrace(e));

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.UNAUTHORIZED)
            .data(e.getMessage())
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9201.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9201.getDesc())
            .build();

        //인증실패 시 세션이 만료된 로그인 쿠키가 존재할 경우를 대비하여 삭제 처리
        certificationService.processUserLogout();

        if (isRestUri(req.getRequestURI())) {
            HttpHeaders httpHeaders = createHttpHeaders();
            return new ResponseEntity<>(responseObject, httpHeaders, HttpStatus.UNAUTHORIZED);
        } else {
            return new ModelAndView(
                "redirect:" + partnerProperties.getCertificationRedirectUrl());
        }
    }

    /**
     * {@link SellerAgreeInterceptorException} 예외처리
     *
     * @param req HttpServletRequest
     * @param e   {@link SellerAgreeInterceptorException}
     * @return ResponseEntity
     */
    @ExceptionHandler(SellerAgreeInterceptorException.class)
    public Object sellerAgreeInterceptorException(final HttpServletRequest req,
        final SellerAgreeInterceptorException e) {
        log.error("SellerAgreeInterceptorException! <uri:{}>,<message:{}>",
            req.getRequestURI(), e.getMessage());

        //인증실패 시 세션이 만료된 로그인 쿠키가 존재할 경우를 대비하여 삭제 처리
        certificationService.processUserLogout();

        return new ModelAndView(
            "redirect:" + partnerProperties.getCertificationRedirectUrl());
    }

    /**
     * {@link Check90DaysSamePasswordInterceptorException} 예외처리
     *
     * @param req HttpServletRequest
     * @param e   {@link Check90DaysSamePasswordInterceptorException}
     * @return ResponseEntity
     */
    @ExceptionHandler(Check90DaysSamePasswordInterceptorException.class)
    public Object check90DaysSamePasswordInterceptorException(final HttpServletRequest req,
        final Check90DaysSamePasswordInterceptorException e) {
        log.error("Check90DaysSamePasswordInterceptorException! <uri:{}>,<message:{}>",
            req.getRequestURI(), e.getMessage());

        //인증실패 시 세션이 만료된 로그인 쿠키가 존재할 경우를 대비하여 삭제 처리
        certificationService.processUserLogout();
        return new ModelAndView(
            "redirect:" + partnerProperties.getCertificationRedirectUrl());
    }

    /**
     * 로그인 시 모바일 인증번호 검증실패할 경우 예외 처리를 합니다.
     *
     * @param req HttpServletRequest
     * @param e  {@link MobileCertNoVerifyFailedException}
     * @return 에러코드 및 노출문구를 json 타입으로 리턴합니다.
     */
    @ExceptionHandler(MobileCertNoVerifyFailedException.class)
    public ResponseEntity<ResponseObject<Void>> mobileCertNoVerifyFailedException(
        final HttpServletRequest req, final MobileCertNoVerifyFailedException e) {
        log.error("MobileCertNoVerifyFailedException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.UNPROCESSABLE_ENTITY.value(),
            ExceptionUtils.getStackTrace(e));

        final ResponseObject<Void> responseObject = ResponseObject.Builder.<Void>builder()
            .status(HttpStatus.UNPROCESSABLE_ENTITY)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9212.getCode())
            .returnMessage(StringUtils
                .defaultString(e.getMessage(), ExceptionCode.SYS_ERROR_CODE_9212.getDesc()))
            .build();

        return new ResponseEntity<>(responseObject, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * {@code @Valid}를 통해 유효성 검증시 발생되는 Validation Exception
     *
     * @param req HttpServletRequest
     * @param e MethodArgumentNotValidException
     * @return 에러 내용을 ResponseEntity 담아 리턴
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Object methodArgumentNotValidExceptionHandler(final HttpServletRequest req
        , final MethodArgumentNotValidException e) {
        final List<ObjectError> allErrors = e.getBindingResult().getAllErrors();
        log.error("MethodArgumentNotValidException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.UNPROCESSABLE_ENTITY)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9302.getCode())
            .returnMessage(StringUtils.defaultString(allErrors.get(0).getDefaultMessage(),
                ExceptionCode.SYS_ERROR_CODE_9302.getDesc()))
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * {@code @RequestBody}를 통해 바인딩 된 파라미터가 null 일 경우 발생 된 예외 처리
     *
     * @param req HttpServletRequest
     * @param e {@link HttpMessageNotReadableException}
     * @return 에러 내용을 ResponseEntity 담아 리턴
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public Object httpMessageNotReadableExceptionHandler(final HttpServletRequest req
        , final HttpMessageNotReadableException e) {
        log.error("HttpMessageNotReadableException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.BAD_REQUEST.value(), e.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.BAD_REQUEST)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9303.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9303.getDesc())
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.BAD_REQUEST);
    }

    /**
     * {@code @RequestBody}를 통해 바인딩 된 파라미터가 null 일 경우 발생 된 예외 처리
     *
     * @param req HttpServletRequest
     * @param e {@link HttpMessageNotReadableException}
     * @return 에러 내용을 ResponseEntity 담아 리턴
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public Object httpRequestMethodNotSupportedExceptionHandler(final HttpServletRequest req
        , final HttpRequestMethodNotSupportedException e) {
        log.error("HttpRequestMethodNotSupportedException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.METHOD_NOT_ALLOWED.value(), e.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.METHOD_NOT_ALLOWED)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9106.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9106.getDesc())
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.METHOD_NOT_ALLOWED);
    }

    /**
     * multipart 관련 에러를 처리하는 핸들러입니다.<p> 전달되는 {@link Exception}의 type에 따라 리턴되는 값이 변경됩니다. 분류내용은 하단의
     * 내용을 참조하시길 바랍니다. <p/>
     * <pre>
     * 1. 업로드 한 multipart의 사이즈가 application.yml에 설정 된 multipart 사이즈 크기를 초과할 경우
     *   - 리턴 : status : 413(PAYLOAD_TOO_LARGE), ResponseObject : 에러 정보
     *
     * 2. 그 외의 {@link Exception}
     *   - 리턴 : status : 500(INTERNAL_SERVER_ERROR), ResponseObject : 에러 정보
     * </pre>
     *
     * @param req {@link HttpServletRequest}
     * @param ex  {@link MultipartException}
     * @return Url의 suffix가 .json이면 'httpStatus'와 json 타입으로 return, 아닐 경우 error Page로 이동
     */
    @ExceptionHandler(MultipartException.class)
    public Object multipartException(final HttpServletRequest req, final Exception ex) {
        final Throwable cause = ex.getCause();
        HttpStatus httpStatus;
        ResponseObject<String> responseObject;

        if (cause.getCause() instanceof FileUploadBase.SizeLimitExceededException) {
            httpStatus = HttpStatus.PAYLOAD_TOO_LARGE;
            log.error(
                "MultipartException ERROR - Payload Too Large!!! <uri:{}>,"
                    + "<httpMethod:{}>,<HttpStatus:{}>,<requestSize:{}>,<PermittedSize:{}>",
                req.getRequestURI(), req.getMethod(), httpStatus.value(),
                ((FileUploadBase.SizeLimitExceededException) cause.getCause()).getActualSize(),
                ((FileUploadBase.SizeLimitExceededException) cause.getCause()).getPermittedSize());

            responseObject = ResponseObject.Builder.<String>builder()
                .status(httpStatus)
                .data(null)
                .returnCode(ExceptionCode.SYS_ERROR_CODE_9002.getCode())
                .returnMessage(ExceptionCode.SYS_ERROR_CODE_9002.getDesc())
                .build();
            return returnResponse(req.getRequestURI(), ex, responseObject, httpStatus);

        } else {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            log.error(
                "MultipartException ERROR - Internal Server Error!!! <uri:{}>,<httpMethod:{}>,<HttpStatus:{}>",
                req.getRequestURI(), req.getMethod(), httpStatus.value());

            responseObject = ResponseObject.Builder.<String>builder()
                .status(httpStatus)
                .data(null)
                .returnCode(ExceptionCode.SYS_ERROR_CODE_9003.getCode())
                .returnMessage(ExceptionCode.SYS_ERROR_CODE_9003.getDesc())
                .build();
            return returnResponse(req.getRequestURI(), ex, responseObject, httpStatus);
        }
    }

    /**
     * {@link org.springframework.web.bind.annotation.RequestParam}과 바인딩되는 파라미터가 null일 경우 예외처리
     * @param req HttpServletRequest
     * @param e MissingServletRequestParameterException
     * @see org.springframework.web.bind.annotation.RequestParam
     * @return
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Object handleMissingServletRequestParameterException(final HttpServletRequest req,
        final MissingServletRequestParameterException e) {
        log.error("MissingServletRequestParameterException!<uri:{}>,<trace:{}>",
            req.getRequestURI(), ExceptionUtils.getStackTrace(e));

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.UNPROCESSABLE_ENTITY)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9301.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9301.getDesc())
            .build();

        HttpHeaders httpHeaders = createHttpHeaders();
        return new ResponseEntity<>(responseObject, httpHeaders, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * 파라미터 바인딩시 발생 되는 Exception handler
     *
     * @param req HttpServletRequest
     * @param e BindException
     * @return 에러 내용을 ResponseEntity 담아 리턴
     */
    @ExceptionHandler({BindException.class})
    public Object handleBindException(final HttpServletRequest req, final BindException e) {
        final List<ObjectError> allErrors = e.getBindingResult().getAllErrors();
        log.error("BindException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.UNPROCESSABLE_ENTITY)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9302.getCode())
            .returnMessage(StringUtils.defaultString(allErrors.get(0).getDefaultMessage(),
                ExceptionCode.SYS_ERROR_CODE_9302.getDesc()))
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * NumberFormatException 를 처리하는 handler
     *
     * @param req HttpServletRequest
     * @param e NumberFormatException
     * @return 에러 내용을 ResponseEntity 담아 리턴
     */
    @ExceptionHandler({NumberFormatException.class})
    public Object handleNumberFormatException(final HttpServletRequest req,
        final NumberFormatException e) {
        log.error("NumberFormatException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.UNPROCESSABLE_ENTITY)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9303.getCode())
            .returnMessage(StringUtils.defaultString(e.getMessage(),
                ExceptionCode.SYS_ERROR_CODE_9303.getDesc()))
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * 파일 다운로드 실패시 발생되는 Exception handler
     *
     * @param req HttpServletRequest
     * @param e UrlDownloadFileFailedException
     * @return 에러 내용을 ResponseEntity 담아 리턴
     * @see kr.co.homeplus.partner.web.common.controller.UploadController
     */
    @ExceptionHandler({UrlDownloadFileFailedException.class})
    public Object handleUrlDownloadFileFailedException(final HttpServletRequest req,
        final UrlDownloadFileFailedException e) {
        log.error("UrlDownloadFileFailedException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9401.getCode())
            .returnMessage(StringUtils.defaultString(e.getMessage(),
                ExceptionCode.SYS_ERROR_CODE_9401.getDesc()))
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * 허용되지 않은 URL 주소로 파일 다운로드 시 발생되는 Exception handler
     *
     * @param req HttpServletRequest
     * @param e NotAllowedUrlDownloadFileException
     * @return 에러 내용을 ResponseEntity 담아 리턴
     * @see kr.co.homeplus.partner.web.common.controller.UploadController
     */
    @ExceptionHandler({NotAllowedUrlDownloadFileException.class})
    public Object handleNotAllowedUrlDownloadFileException(final HttpServletRequest req,
        final NotAllowedUrlDownloadFileException e) {
        log.error("NotAllowedUrlDownloadFileException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.FORBIDDEN.value(), e.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.FORBIDDEN)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9401.getCode())
            .returnMessage(StringUtils.defaultString(e.getMessage(),
                ExceptionCode.SYS_ERROR_CODE_9401.getDesc()))
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.FORBIDDEN);
    }

    /**
     * 예외 처리후 requestURI 유형에 따라 {@link HttpEntity} 또는 {@link ModelAndView} 로 반환합니다.
     * @param requestUri request URI
     * @param e {@link Exception}
     * @param responseBody ResponseBody
     * @param status {@link HttpStatus}
     * @param <T> T
     * @return URI의 suffix 가 .json 일 경우 {@link HttpEntity}로 아닐경우 {@link ModelAndView} 반환합니다.
     */
    private <T> Object returnResponse(final String requestUri, final Exception e,
        final T responseBody, final HttpStatus status) {
        return returnResponse(requestUri, e, responseBody, status, ERROR_PAGE_PATH);
    }

    /**
     * 예외 처리후 requestURI 유형에 따라 {@link HttpEntity} 또는 {@link ModelAndView} 로 반환합니다.
     * @param uri request URI
     * @param e {@link Exception}
     * @param responseBody ResponseBody
     * @param status {@link HttpStatus}
     * @param viewName viewName
     * @return URI의 suffix 가 .json 일 경우 {@link HttpEntity}로 아닐경우 {@link ModelAndView} 반환합니다.
     */
    private <T> Object returnResponse(final String uri, final Exception e, final T responseBody,
        final HttpStatus status, final String viewName) {
        if (isRestUri(uri)) {
            final HttpHeaders httpHeaders = createHttpHeaders();
            return new ResponseEntity<>(responseBody, httpHeaders, status);
        } else {
            final ModelAndView mav = new ModelAndView();
            setErrorModelAndView(mav, status, ExceptionUtils.getStackTrace(e), responseBody);
            mav.setViewName(viewName);
            return mav;
        }
    }


    private boolean isRestUri(final String requestUrl) {
        return StringUtils.endsWith(requestUrl, ".json");
    }

    private <T> void setErrorModelAndView(final ModelAndView mav, final HttpStatus status,
        final String stackTrace, final T responseObject) {
        mav.setStatus(status);
        mav.addObject("profilesActive", profilesActive);
        mav.addObject("responseObject", responseObject);
        mav.addObject("stackTrace", stackTrace);
    }

    private HttpHeaders createHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        Charset utf8 = StandardCharsets.UTF_8;
        MediaType mediaType = new MediaType(MediaType.APPLICATION_JSON, utf8);
        httpHeaders.setContentType(mediaType);

        return httpHeaders;
    }
}
