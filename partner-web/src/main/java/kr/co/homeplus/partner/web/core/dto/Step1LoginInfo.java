package kr.co.homeplus.partner.web.core.dto;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * usermng-api -> item-api를 통해 조회한 파트너 1단계 로그인 정보 모델
 */
@Setter
@Getter
public class Step1LoginInfo {
    /** 판매업체ID **/
    private String partnerId;
    /** 판매업체명 **/
    private String businessNm;
    /** 계정상태 **/
    private String accountStatus;
    /** 로그인 실패횟수 **/
    private int loginFailCnt;
    /** OF 승인 상태 : 대기(B) / 완료(A) **/
    private String ofVendorStatus;
    /** 판매업체 상태 **/
    private String partnerStatus;
    /** 파트너 구분(AFFI:제휴사, COOP:마켓연동)-공통코드:partner_type **/
    private String partnerType;

    /** 파트너 2단계 로그인 인증 담당자 정보리스트 **/
    private List<PartnerManagerInfo> managerList;
}
