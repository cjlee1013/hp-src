package kr.co.homeplus.partner.web.common.model.pop;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddressPopupDto {
    private String totalCount;
    private List<Address> dataList;
}
