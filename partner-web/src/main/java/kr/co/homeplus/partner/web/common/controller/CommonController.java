package kr.co.homeplus.partner.web.common.controller;

import kr.co.homeplus.partner.web.common.model.common.MobileOccupationSendParamDto;
import kr.co.homeplus.partner.web.common.model.common.MobileOccupationValidParamDto;
import kr.co.homeplus.partner.web.core.annotation.CertificationNeedLess;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 팝업 컨트롤러
 *
 */
@Controller
@Slf4j
@RequestMapping("/common")
public class CommonController {
    private final ResourceClient resourceClient;
    private final CertificationService certificationService;


    public CommonController(ResourceClient resourceClient ,
        CertificationService certificationService)
    {
        this.resourceClient = resourceClient;
        this.certificationService = certificationService;
    }

    /**
     * 점유인증 인증번호 발송
     */
    @ResponseBody
    @GetMapping(value = {"/mobile/certno/send.json"})
    @CertificationNeedLess
    public ResponseObject getMobileCertNoSend(@RequestParam(value = "mobile") String mobile) {
        String apiUri = "/common/mobile/certno/send";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject>() {
        };

        return resourceClient
                .postForResponseObject(ResourceRouteName.USER,
                        MobileOccupationSendParamDto.builder().mobile(mobile).build(), apiUri,
                        typeReference);
    }

    /**
     * 점유인증 인증번호 인증
     */
    @ResponseBody
    @PostMapping(value = {"/mobile/certno/valid.json"})
    @CertificationNeedLess
    public ResponseObject getMobileCertNoValid(@RequestBody MobileOccupationValidParamDto mobileOccupationValidParamDto) {
        String apiUri = "/common/mobile/certno/valid";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject>() {
        };

        return resourceClient
                .postForResponseObject(ResourceRouteName.USER,
                        mobileOccupationValidParamDto, apiUri,
                        typeReference);
    }

}