package kr.co.homeplus.partner.web.core.usermenu.model;

import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class Menus {
    private List<MainMenus> data;
}
