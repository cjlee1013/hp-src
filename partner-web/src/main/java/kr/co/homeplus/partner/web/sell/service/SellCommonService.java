package kr.co.homeplus.partner.web.sell.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.partner.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class SellCommonService {

  private final CertificationService certificationService;
  private final CodeService codeService;

  /**
   * UserCd 조회
   */
  public String getUserCd() {
    return certificationService.getLoginUerInfo().getPartnerId();
  }

  /**
   * Code 조회
   */
  public List<MngCodeGetDto> getCode(String gmcCd) {
    try {
      Map<String, List<MngCodeGetDto>> code = codeService.getCode(gmcCd);

      return code.get(gmcCd);
    } catch (Exception e) {
      return new ArrayList<>();
    }
  }

}
