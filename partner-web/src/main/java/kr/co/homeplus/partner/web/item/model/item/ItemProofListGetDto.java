package kr.co.homeplus.partner.web.item.model.item;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품관리 증빙서류")
public class ItemProofListGetDto {
    @ApiModelProperty(value = "파일시퀀스")
    private String fileSeq;

    @ApiModelProperty(value = "파일유형")
    private String fileType;

    @ApiModelProperty(value = "파일유형명", hidden = true)
    private String fileTypeNm;

    @ApiModelProperty(value = "파일명")
    private String fileNm;

    @ApiModelProperty(value = "파일URL")
    private String fileUrl;
}
