package kr.co.homeplus.partner.web.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임 수거상태변경 요청 파라미터")
public class ClaimShippingStatusSetDto {

    @NotNull(message = "클레임번들번호")
    @ApiModelProperty(value = "클레임번들번호", position = 1, example = "1500000001")
    private long claimBundleNo;

    @ApiModelProperty(value = "반품 수거상태 (NN:요청대기, N0: 수거예약, P0:자동접수요청, P1: 요청완료-송장등록, P2: 수거중, P3: 수거완료, P8 :수거실패 (반품자동접수실패)", position = 2, example = "NN")
    private String pickStatus;

    @ApiModelProperty(value = "수정자 정보", position = 3, example = "PARTNER")
    private String chgId;

    @ApiModelProperty(value = "클레임번호", position = 4, example = "1500000011")
    private long claimNo;
}
