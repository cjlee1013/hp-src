package kr.co.homeplus.partner.web.manage.model;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PartnerHarmfulItemListSelectDto {

    @ApiModelProperty(value = "문서번호")
    @RealGridColumnInfo(headText = "문서번호", width = 0 , hidden = true)
    private String noDocument;

    @ApiModelProperty(value = "차수")
    @RealGridColumnInfo(headText = "차수", width = 0 , hidden = true)
    private String seq;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 0 , hidden = true)
    private String itemNo;

    @ApiModelProperty(value = "유형")
    @RealGridColumnInfo(headText = "유형", width = 50)
    private String nmRpttype;

    @ApiModelProperty(value = "검사기관")
    @RealGridColumnInfo(headText = "검사기관", width = 50)
    private String instituteTxt;

    @ApiModelProperty(value = "검사기관코드")
    @RealGridColumnInfo(headText = "검사기관코드", width = 0 , hidden = true)
    private String cdInsptmachi;

    @ApiModelProperty(value = "제품명")
    @RealGridColumnInfo(headText = "제품명", width = 50)
    private String itemNm;

    @ApiModelProperty(value = "제조업소")
    @RealGridColumnInfo(headText = "제조업소", width = 50)
    private String nmManufacupso;

    @ApiModelProperty(value = "판매업소")
    @RealGridColumnInfo(headText = "판매업소", width = 50)
    private String nmSalerupso;

    @ApiModelProperty(value = "등록일시")
    @RealGridColumnInfo(headText = "판매등록일시업소", width = 50)
    private String regDt;
}
