package kr.co.homeplus.partner.web.common.exception;

public class NotAllowedUrlDownloadFileException extends RuntimeException {

    public NotAllowedUrlDownloadFileException(String message) {
        super(message);
    }

    public NotAllowedUrlDownloadFileException(Exception e) {
        super(e);
    }
}
