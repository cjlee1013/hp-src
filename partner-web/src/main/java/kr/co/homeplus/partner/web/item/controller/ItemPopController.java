package kr.co.homeplus.partner.web.item.controller;

import kr.co.homeplus.partner.web.common.model.common.ResponseResult;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ChannelConstants;
import kr.co.homeplus.partner.web.core.constants.ItemConstants;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.item.model.item.ItemDcPriceSetDto;
import kr.co.homeplus.partner.web.item.model.item.ItemDispYnSetDto;
import kr.co.homeplus.partner.web.item.model.item.ItemSaleDateSetDto;
import kr.co.homeplus.partner.web.item.model.item.ItemSalePriceSetDto;
import kr.co.homeplus.partner.web.item.model.item.ItemShipPolicySetDto;
import kr.co.homeplus.partner.web.item.model.item.ItemStatusSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/pop")
public class ItemPopController {

    private final ResourceClient resourceClient;
    private final CertificationService certificationService;

    public ItemPopController(ResourceClient resourceClient,
        CertificationService certificationService
    ) {
        this.resourceClient = resourceClient;
        this.certificationService = certificationService;
    }

    /**
     * 상품 일시중지
     * @param itemStatusSetDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/setItemStop.json")
    public ResponseResult setItemStop(@RequestBody ItemStatusSetDto itemStatusSetDto) {

        itemStatusSetDto.setUserId(ChannelConstants.USERID);
        itemStatusSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        itemStatusSetDto.setItemStatus(ItemConstants.STOP);

        String apiUri = "/po/item/pop/setItemStop";
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemStatusSetDto, apiUri,
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }
    /**
     * 상품 일시중지 해제
     * @param itemStatusSetDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/setItemActive.json")
    public ResponseResult setItemActive(@RequestBody ItemStatusSetDto itemStatusSetDto) {

        itemStatusSetDto.setUserId(ChannelConstants.USERID);
        itemStatusSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        itemStatusSetDto.setItemStatus(ItemConstants.ACTIVE);

        String apiUri = "/po/item/pop/setItemActive";
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemStatusSetDto, apiUri,
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }
    /**
     * 노출여부 변경
     * @param itemDispYnSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/setItemDispYn.json")
    public ResponseResult setItemDispYn(@RequestBody ItemDispYnSetDto itemDispYnSetDto) throws Exception {

        itemDispYnSetDto.setUserId(ChannelConstants.USERID);
        itemDispYnSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());

        String apiUri = "/po/item/pop/setItemDispYn";

       return resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemDispYnSetDto, apiUri,
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }
    /**
     * 판매가 변경
     * @param itemSalePriceSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/setItemSalePrice.json")
    public ResponseResult setItemSalePrice(@RequestBody ItemSalePriceSetDto itemSalePriceSetDto) throws Exception {

        itemSalePriceSetDto.setUserId(ChannelConstants.USERID);
        itemSalePriceSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());

        String apiUri = "/po/item/pop/setItemSalePrice";

       return resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemSalePriceSetDto,  apiUri,
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }
    /**
     * 즉시할인 변경
     * @param itemDcPriceSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/setItemDcPrice.json")
    public ResponseResult setItemDcPrice(@RequestBody ItemDcPriceSetDto itemDcPriceSetDto) throws Exception {

        itemDcPriceSetDto.setUserId(ChannelConstants.USERID);
        itemDcPriceSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        String apiUri = "/po/item/pop/setItemDcPrice";

        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemDcPriceSetDto,  apiUri,
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }
    /**
     * 판매기간 변경
     * @param itemSaleDateSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/setItemSaleDate.json")
    public ResponseResult setItemSaleDate(@RequestBody ItemSaleDateSetDto itemSaleDateSetDto) throws Exception {

        itemSaleDateSetDto.setUserId(ChannelConstants.USERID);
        itemSaleDateSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());

        String apiUri = "/po/item/pop/setItemSaleDate";

        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemSaleDateSetDto,  apiUri,
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }
    /**
     * 배송정책 변경
     * @param itemShipPolicySetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/setItemShipPolicy.json")
    public ResponseResult setItemShipPolicy(@RequestBody ItemShipPolicySetDto itemShipPolicySetDto) throws Exception {

        itemShipPolicySetDto.setUserId(ChannelConstants.USERID);
        itemShipPolicySetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        String apiUri = "/po/item/pop/setItemShipPolicy";

        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemShipPolicySetDto,  apiUri,
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }
}
