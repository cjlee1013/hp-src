package kr.co.homeplus.partner.web.item.model.item;

import java.math.BigDecimal;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 기본정보
 */
@Getter
@Setter
public class ItemGetDto {
	private String itemNo;
	private String itemNm;
	private String itemDivision;
	private String itemType;
	private String itemTypeNm;
	private String itemStatus;
	private String StatusDesc;
	private String sellerItemCd;
	private String businessNm;
	private String saleType;
	private String mallType;
	private String mallTypeNm;
	private String saleStatusDt;
	private String adultType;
	private String imgDispYn;
	private String dispYn;
	private String dispYnNm;
	private Long brandNo;
	private Long makerNo;
	private String optSelUseYn;
	private String optTxtUseYn;
	private String listSplitDispYn;
	private String shipAttr;
	private String regId;
	private String regDt;
	private String chgId;
	private String chgDt;

	private String storeId;
	private String storeNm;
	private String storeType;
	private String storeTypeNm;

	private int dcateCd;
	private int scateCd;
	private int mcateCd;
	private int lcateCd;
	private String dcateNm;
	private String scateNm;
	private String mcateNm;
	private String lcateNm;

	private String regNm;
	private String chgNm;
	private String transTypeNm;
	private String itemStatusNm;
	private String saleStatusNm;
	private String brandNm;
	private String makerNm;
	private String makeDt;
	private String expDt;

	/**
	 * 옵션정보
	 */
	List<ItemOptionValueGetDto> optList;

	/**
	 * 판매정보
	 */
	private String taxYn;
	private Long originPrice;
	private long purchasePrice;
	private long salePrice;
	private long dcPrice;
	private String saleStartDt;
	private String saleEndDt;
	private String commissionType;
	private BigDecimal commissionRate;
	private int commissionPrice;
	private String dcYn;
	private String dcPeriodYn;
	private String dcStartDt;
	private String dcEndDt;

	private String salePeriodYn;
	private String rsvYn;
	private List<ItemRsvDetailGetDto> rsvList;
	private Integer saleRsvNo;
	private String rsvStartDt;
	private String rsvEndDt;
	private String shipStartDt;
	private String shipEndDt;
	//예약 구매제한 개수 여부
	private String rsvPurchaseLimitYn;
	//예약 구매제한 개수(1회) - ?개
	private Integer rsvPurchaseLimitQty;

	private int stockQty;
	private int remainStockQty;
	private int saleUnit;
	private int purchaseMinQty;
	private String purchaseLimitYn;
	private String purchaseLimitDuration;
	private int purchaseLimitDay;
	private int purchaseLimitQty;
	private String cartLimitYn;

	/**
	 * 이미지 정보
	 */
	private List<ItemImgGetDto> imgList;

	/**
	 * 배송정보
	 */
    private long shipPolicyNo;
    private Integer releaseDay;
    private Integer releaseTime;
    private String holidayExceptYn;
    private Integer claimShipFee;
    private String releaseZipcode;
    private String releaseAddr1;
    private String releaseAddr2;
    private String returnZipcode;
    private String returnAddr1;
    private String returnAddr2;
    private String safeNumberUseYn;

    private String shipPolicyNm;
    private String shipKindNm;
    private String shipMethodNm;
    private String shipTypeNm;
    private int shipFee;
    private String prepaymentYnNm;
    private String shipDispYnNm;
    private String diffYn;
    private String diffQty;

	/**
	 * 상품 이미지정보
	 */
	private String itemDesc;

	/**
	 * 옵션 타이틀 정보
	 */
	private int    optTitleDepth;
	private String opt1Title;
	private String opt2Title;

	/**
	 * 텍스트옵션 정보
	 */
	private int    optTxtDepth;
	private String opt1Text;
	private String opt2Text;

	/**
	 * 부가정보
	 */
	private String epYn;
	private String linkageYn;
	private String srchKeyword;
	private String globalAgcyYn;
	private Long gnoticeNo;
	private List<ItemNoticeGetDto> noticeList;
	private Integer rentalFee;
	private Integer rentalPeriod;
	private Integer rentalRegFee;
	private String giftYn;
	private List<ItemProofListGetDto> proofFileList;
	private List<ItemCertGetDto> certList;
	private String isbn;

	//연관상품 번호
	private Integer relationNo;

	/**
	 * 제한여부
	 */
	private String cartYn;
	private String adultLimitYn;
	private String isbnYn;

}
