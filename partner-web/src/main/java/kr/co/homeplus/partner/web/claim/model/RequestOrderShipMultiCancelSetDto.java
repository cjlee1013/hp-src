package kr.co.homeplus.partner.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestOrderShipMultiCancelSetDto {

    @ApiModelProperty(value = "일괄취소 dto 리스트", position = 1)
    List<OrderShipMultiCancelSetDto> itemList;

    @ApiModelProperty(value = "클레임사유코드", position = 2)
    private String claimReasonType;

    @ApiModelProperty(value = "클레임사유상세", position = 3)
    private String claimReasonDetail;

    @ApiModelProperty(value = "요청자I", position = 4)
    private String regId;

    @Getter
    @Setter
    public static class OrderShipMultiCancelSetDto {
        @ApiModelProperty(value = "주문번호", position = 1)
        private long purchaseOrderNo;

        @ApiModelProperty(value = "배송번호", position = 2)
        private long bundleNo;

        @ApiModelProperty(value = "상품주문번호", position = 3)
        private long orderItemNo;


        public OrderShipMultiCancelSetDto(){
            this.purchaseOrderNo = 0;
            this.bundleNo = 0;
            this.orderItemNo = 0;
        }
    }

}
