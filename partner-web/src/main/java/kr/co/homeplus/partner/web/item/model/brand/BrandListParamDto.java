package kr.co.homeplus.partner.web.item.model.brand;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BrandListParamDto {
    private String searchType;
    private String searchKeyword;
    private String callType;
}
