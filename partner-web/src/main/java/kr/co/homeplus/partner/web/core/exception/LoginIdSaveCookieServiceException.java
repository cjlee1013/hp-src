package kr.co.homeplus.partner.web.core.exception;

public class LoginIdSaveCookieServiceException extends RuntimeException {

    public LoginIdSaveCookieServiceException(String message) {
        super(message);
    }

    public LoginIdSaveCookieServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
