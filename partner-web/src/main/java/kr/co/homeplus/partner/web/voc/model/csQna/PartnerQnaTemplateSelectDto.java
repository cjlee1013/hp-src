package kr.co.homeplus.partner.web.voc.model.csQna;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class PartnerQnaTemplateSelectDto {

    @RealGridColumnInfo(headText = "템플릿번호", sortable = true, hidden = true)
    private String templateNo;

    @RealGridColumnInfo(headText = "제목", sortable = true)
    private String templateTitle;

    @RealGridColumnInfo(headText = "내용", sortable = true)
    private String templateContents;

    @RealGridColumnInfo(headText = "등록/수정일", sortable = true)
    private String chgDt;

}
