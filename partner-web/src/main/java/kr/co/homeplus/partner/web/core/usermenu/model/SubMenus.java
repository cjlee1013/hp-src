package kr.co.homeplus.partner.web.core.usermenu.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.gson.annotations.SerializedName;
import kr.co.homeplus.partner.web.core.usermenu.model.SubMenus.SubMenusBuilders;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder(builderClassName = "SubMenusBuilders", toBuilder = true)
@JsonDeserialize(builder = SubMenusBuilders.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubMenus {
    private String menuNm;
    @SerializedName("isMenuView")
    private boolean menuView;
    private String tabId;
    private String link;

    @JsonPOJOBuilder(withPrefix = "")
    public static class SubMenusBuilders {
    }
}
