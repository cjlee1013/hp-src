package kr.co.homeplus.partner.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemBoardCountDto {

    private String itemStatus;
    private long count;

}
