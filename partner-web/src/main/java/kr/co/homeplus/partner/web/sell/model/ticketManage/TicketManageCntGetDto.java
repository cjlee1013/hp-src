package kr.co.homeplus.partner.web.sell.model.ticketManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 이티켓판매관리 카운트 응답 DTO")
public class TicketManageCntGetDto {
    @ApiModelProperty(notes = "발급대기 건수")
    private String readyCnt;

    @ApiModelProperty(notes = "발급중 건수")
    private String callCnt;

    @ApiModelProperty(notes = "발급실패 건수")
    private String failCnt;
}