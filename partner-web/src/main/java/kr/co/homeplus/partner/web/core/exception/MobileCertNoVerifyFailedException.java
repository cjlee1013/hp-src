package kr.co.homeplus.partner.web.core.exception;

/**
 * 파트너 로그인 모바일 인증번호 예외처리 Exception
 */
public class MobileCertNoVerifyFailedException extends RuntimeException {

    public MobileCertNoVerifyFailedException(final String message) {
        super(message);
    }
}
