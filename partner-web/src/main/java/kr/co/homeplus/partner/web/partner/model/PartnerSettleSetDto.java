package kr.co.homeplus.partner.web.partner.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerSettleSetDto implements Serializable {

	private String bankCd;
	private String bankAccountNo;
	private String depositor;
	private String settleCycleType;

}
