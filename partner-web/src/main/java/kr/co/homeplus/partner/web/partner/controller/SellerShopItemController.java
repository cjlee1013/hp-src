package kr.co.homeplus.partner.web.partner.controller;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.partner.web.common.model.common.ResponseResult;
import kr.co.homeplus.partner.web.common.model.pop.ItemPopupListGetDto;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridColumn;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.partner.web.partner.model.sellerShopItem.PartnerSellerShopItemGetDto;
import kr.co.homeplus.partner.web.partner.model.sellerShopItem.SellerShopItemListSelectDto;
import kr.co.homeplus.partner.web.partner.model.sellerShopItem.SellerShopItemSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/partner/sellerShopItem")
public class SellerShopItemController {


    private final ResourceClient resourceClient;
    private final CertificationService certificationService;

    private static final List<RealGridColumn> SELLER_SHOP_ITEM_LIST_GRID_COLUMN = new ArrayList<>();
    private static final RealGridOption SELLER_SHOP_ITEM_LIST_GRID_OPTION = new RealGridOption("fill", false, true, false);



    public SellerShopItemController(ResourceClient resourceClient,
        CodeService codeService,
        CertificationService certificationService
    ) {
        this.resourceClient = resourceClient;
        this.certificationService = certificationService;

        SELLER_SHOP_ITEM_LIST_GRID_COLUMN.add(new RealGridColumn("itemNo", "itemNo", "상품번호", RealGridColumnType.BASIC, 120, true, true));
        SELLER_SHOP_ITEM_LIST_GRID_COLUMN.add(new RealGridColumn("itemNm", "itemNm", "상품명", RealGridColumnType.BASIC, 120, true, true));
        SELLER_SHOP_ITEM_LIST_GRID_COLUMN.add(new RealGridColumn("regNm", "regNm", "등록자", RealGridColumnType.BASIC, 100, true, true));
        SELLER_SHOP_ITEM_LIST_GRID_COLUMN.add(new RealGridColumn("regDt", "regDt", "등록일", RealGridColumnType.DATETIME, 120, true, true));
        SELLER_SHOP_ITEM_LIST_GRID_COLUMN.add(new RealGridColumn("priority", "priority", "NO", RealGridColumnType.NUMBER_CENTER, 0, false, true));

    }

    /**
     *  셀러샵관리 > 추천상품 전시관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/sellerShopItemMain", method = RequestMethod.GET)
    public String sellerShopMain(Model model) {

        model.addAttribute("sellerShopItemListGridBaseInfo", new RealGridBaseInfo("sellerShopItemListGridBaseInfo", SELLER_SHOP_ITEM_LIST_GRID_COLUMN, SELLER_SHOP_ITEM_LIST_GRID_OPTION).toString());
        model.addAllAttributes(RealGridHelper.create("itemPopGridBaseInfo", ItemPopupListGetDto.class));
        return "/partner/sellerShopItemMain";
    }
    /**
     *  셀러샵관리 > 추천상품 전시관리 셀러샵 아이템 리스트
     *
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getSellerShopItemList.json"}, method = RequestMethod.GET)
    public PartnerSellerShopItemGetDto getSellerShopItemList() {
        String apiUri = "/po/partner/sellerShopItem/getSellerShopItemList?partnerId=" + certificationService.getLoginUerInfo().getPartnerId();

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<PartnerSellerShopItemGetDto>>() {};
        return (PartnerSellerShopItemGetDto) resourceClient.getForResponseObject(
            ResourceRouteName.ITEM,apiUri, typeReference).getData();
    }
    /**
     *  셀러샵관리 > 추천상품 전시관리 > 셀러샵 추천 등록/수정
     *
     * @param
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setSellerShopItem.json"}, method = RequestMethod.POST)
    public ResponseResult setSellerShopItem(@RequestBody SellerShopItemSetParamDto setParamDto) throws Exception {
        String apiUri = "/po/partner/sellerShopItem/setSellerShopItem?";
        setParamDto.setRegId(certificationService.getLoginUerInfo().getPartnerId());
        setParamDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, setParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }


}
