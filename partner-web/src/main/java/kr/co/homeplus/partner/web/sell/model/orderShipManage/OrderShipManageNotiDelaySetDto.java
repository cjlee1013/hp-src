package kr.co.homeplus.partner.web.sell.model.orderShipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ApiModel(description = "판매관리 > 발주/발송관리 발송지연안내 요청 DTO")
public class OrderShipManageNotiDelaySetDto {
    @ApiModelProperty(notes = "배송번호 리스트")
    private String bundleNo;

    @ApiModelProperty(notes = "수정자")
    private String chgId;

    @ApiModelProperty(notes = "발송지연사유코드")
    private String delayShipCd;

    @ApiModelProperty(notes = "발송지연사유메시지")
    private String delayShipMsg;

    @ApiModelProperty(notes = "2차발송기한")
    private String delayShipDt;
}
