package kr.co.homeplus.partner.web.partner.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 파트너 유형
 */
@RequiredArgsConstructor
public enum PartnerType {

		SELLER("SELL", "판매업체")
	,   AFFILIATE("AFFI", "제휴업체")
	;

	@Getter
	private final String type;

	@Getter
	private final String description;
}
