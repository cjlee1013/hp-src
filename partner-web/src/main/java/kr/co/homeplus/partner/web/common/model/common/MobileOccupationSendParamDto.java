package kr.co.homeplus.partner.web.common.model.common;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class MobileOccupationSendParamDto {
	private String mobile;
}
