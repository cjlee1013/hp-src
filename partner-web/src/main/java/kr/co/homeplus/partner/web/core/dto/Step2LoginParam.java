package kr.co.homeplus.partner.web.core.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Step2LoginParam extends Step1LoginParam {
    @ApiModelProperty(value = "아이디 저장여부", required = true)
    private boolean chkSavePartnerId;

    @NotEmpty
    @ApiModelProperty("휴대폰 인증완료 체크토큰")
    private String verifiedToken;

    @NotNull
    @ApiModelProperty("휴대폰번호")
    private String mobile;
}
