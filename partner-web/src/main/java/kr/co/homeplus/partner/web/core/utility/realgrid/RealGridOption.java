package kr.co.homeplus.partner.web.core.utility.realgrid;

import lombok.Getter;
import lombok.Setter;

@Deprecated(forRemoval = true)
@Getter
@Setter
public class RealGridOption {
    private final String COMMA = ",";
    private String displayStyle; // fill or none
    private boolean indicator;
    private boolean checkBar;
    private boolean hideDeletedRows;
    private RealGridType realGridType;

    public RealGridOption(String displayStyle, boolean indicator, boolean checkBar, boolean hideDeletedRows) {
        this.displayStyle = displayStyle;
        this.indicator = indicator;
        this.checkBar = checkBar;
        this.hideDeletedRows = hideDeletedRows;
        this.realGridType = RealGridType.MultiRowSelect;
    }

    public RealGridOption(String displayStyle, boolean indicator, boolean checkBar, boolean hideDeletedRows, RealGridType realGridType) {
        this.displayStyle = displayStyle;
        this.indicator = indicator;
        this.checkBar = checkBar;
        this.hideDeletedRows = hideDeletedRows;
        this.realGridType = realGridType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{");
        sb.append("\"hideDeletedRows\": " + Boolean.valueOf(this.hideDeletedRows).toString()).append(COMMA);
        sb.append("\"display\": { \"fitStyle\": \"" + this.displayStyle + "\" }").append(COMMA);
        sb.append("\"indicator\": { \"visible\": " + Boolean.valueOf(this.indicator).toString() + ", \"headText\" : \"No\" }").append(COMMA);
        sb.append("\"checkBar\": { \"visible\": " + Boolean.valueOf(this.checkBar).toString() + " }").append(COMMA);
        sb.append("\"panel\": { \"visible\": false }").append(COMMA);
        sb.append("\"stateBar\": { \"visible\": false }").append(COMMA);
        sb.append("\"footer\": { \"visible\": false }").append(COMMA);
        sb.append("\"edit\": { \"editable\": false, \"updatable\": false }").append(COMMA);
        sb.append("\"displayOptions\": { \"columnResizable\": false, \"columnMovable\": false }").append(COMMA);
        sb.append("\"copy\":"+realGridType.getSingleMode()).append(COMMA);
        sb.append("\"paste\": { \"enabled\": false }").append(COMMA);
        sb.append("\"header\": { \"styles\": { \"background\": \"#E0E0E0\" } }").append(COMMA);
        sb.append("\"select\":"+realGridType.getOption());
        sb.append("}");

        return sb.toString();
    }
}
