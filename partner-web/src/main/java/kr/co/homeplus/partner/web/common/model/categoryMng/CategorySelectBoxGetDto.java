package kr.co.homeplus.partner.web.common.model.categoryMng;

import lombok.Data;


@Data
/**
 * 카테고리 SelectBox 조회 Entry
 */
public class CategorySelectBoxGetDto {

	// 카테고리 코드
	private String cateCd;

	// 카테고리 명
	private String cateNm;
}
