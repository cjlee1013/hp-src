package kr.co.homeplus.partner.web.core.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

/**
 * 파트너 휴대폰 인증번호 검증요청 파라미터(usermng-api로 요청)
 */
@Setter
@Getter
public class MobileOccupationValidParam {
    /** 발송된 인증번호 **/
    @NotEmpty
    @Min(100000)
    @Max(999999)
    private String certNo;

    /** 인증토큰 **/
    @NotEmpty
    @ApiModelProperty("인증토큰")
    private String certToken;

    /** 휴대폰번호 **/
    @NotNull
    private String mobile;
}
