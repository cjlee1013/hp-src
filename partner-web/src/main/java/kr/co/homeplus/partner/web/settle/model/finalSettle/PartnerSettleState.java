package kr.co.homeplus.partner.web.settle.model.finalSettle;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum PartnerSettleState {
    //정신관리 > 파트너 정산상태
    SETTLE_STATUS_WAIT("1", "정산대기"),
    SETTLE_STATUS_COMPLETE("2", "정산완료"),
    SETTLE_STATUS_POSTPONE("4", "정산보류"),;

    private String code;
    private String name;
}
