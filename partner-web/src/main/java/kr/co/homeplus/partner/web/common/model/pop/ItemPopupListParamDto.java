package kr.co.homeplus.partner.web.common.model.pop;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ItemPopupListParamDto {

	private String schLcateCd;
	private String schMcateCd;
	private String schScateCd;
	private String schDcateCd;
	private String schItemNo;
	private String schItemNm;
	private String schPartnerType = "PARTNERID";
	private String schPartnerValue;
	private String schschMallType ="DS";
}

