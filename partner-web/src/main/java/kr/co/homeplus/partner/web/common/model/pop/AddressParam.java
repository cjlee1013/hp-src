package kr.co.homeplus.partner.web.common.model.pop;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddressParam {
    private String page;
    private Integer perPage;
    private String query;
}
