package kr.co.homeplus.partner.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryCommissionParamDto {

    //대분류 카테고리
    private String schLcateCd;

    //증분류 카테고리
    private String schMcateCd;

    //소분류 카테고리
    private String schScateCd;

    //세분류 카테고리
    private String schDcateCd;

    //판매자ID
    private String partnerId;
}
