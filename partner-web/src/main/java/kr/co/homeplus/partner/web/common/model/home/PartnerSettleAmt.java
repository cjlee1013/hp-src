package kr.co.homeplus.partner.web.common.model.home;

import lombok.Getter;
import lombok.Setter;

/**
 * 파트너 홈메인 대시보드 정산예정
 */
@Setter
@Getter
public class PartnerSettleAmt {
    //정산예정금액
    private Long settleAmt;

    private String partnerSettleMngUrl;
}
