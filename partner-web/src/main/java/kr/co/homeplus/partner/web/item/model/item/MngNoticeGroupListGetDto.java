package kr.co.homeplus.partner.web.item.model.item;

import lombok.Getter;
import lombok.Setter;


/**
 * 상품정보고시관리 그룹 Get Entry
 */
@Getter
@Setter
public class MngNoticeGroupListGetDto {

    //정책코드
    private int gnoticeNo;

    //정책명
    private String gnoticeNm;

}
