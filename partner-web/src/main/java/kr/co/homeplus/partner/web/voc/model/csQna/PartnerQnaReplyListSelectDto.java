package kr.co.homeplus.partner.web.voc.model.csQna;

import lombok.Data;

@Data
public class PartnerQnaReplyListSelectDto {

    private String qnaReplyNo;

    private String qnaNo;

    private String displayStatus;

    private String displayStatusTxt;

    private String qnaDisplayStatus;

    private String replyArea;

    private String qnaReply;

    private String regNm;

    private String partnerNm;

    private String partnerId;

    private String regDt;
}
