package kr.co.homeplus.partner.web.core.utility.realgrid;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Real Grid 기본 정보
 */
@Deprecated(forRemoval = true)
public class RealGridBaseInfo {
	private String baseInfoName;                                        // Grid 기본정보 이름 (자바스크립트 변수명)
	private List<RealGridColumn> columnList;                            // Grid Header
	private RealGridOption option;                                      // Grid Options
	private RealGridType realGridType;
	private List<RealGridGroupColumn> groupColumnList;

	public RealGridBaseInfo(String baseInfoName, List<RealGridColumn> columnList, RealGridOption option) {
		this.baseInfoName = baseInfoName;
		this.columnList = columnList;
		this.option = option;
		this.realGridType = RealGridType.MultiRowSelect;
	}

	public RealGridBaseInfo(String baseInfoName, List<RealGridColumn> columnList, RealGridOption option, RealGridType realGridType) {
		this.baseInfoName = baseInfoName;
		this.columnList = columnList;
		this.option = option;
		this.realGridType = realGridType;
	}

	public RealGridBaseInfo(String baseInfoName, RealGridOption option, List<RealGridGroupColumn> groupColumnList) {
		this.baseInfoName = baseInfoName;
		this.option = option;
		this.groupColumnList = groupColumnList;
		this.realGridType = RealGridType.MultiRowSelect;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("var " + this.baseInfoName + " = { realgrid : {}, dataProvider: {}};\n");
		// 그리드 설정 옵션
		result.append(this.baseInfoName + ".realgrid.styles = " + this.getGridStyles() + ";\n");
		result.append(this.baseInfoName + ".realgrid.displayOptions = " + this.getDisplayOptions() + ";\n");
		result.append(this.baseInfoName + ".realgrid.options = " + this.option.toString() + ";\n");
		result.append(this.baseInfoName + ".realgrid.columns = " + this.getColumnInfo() + ";\n");

		// dataProvider 설정 옵션
		result.append(this.baseInfoName + ".dataProvider.fields = " + this.getDataProviderFields() + ";\n");
		result.append(this.baseInfoName + ".dataProvider.options = " + this.getDataProviderOption() + ";\n");

		return result.toString();
	}

	public String toStringGroup() {
		StringBuilder result = new StringBuilder();
		result.append("var " + this.baseInfoName + " = { realgrid : {}, dataProvider: {}};\n");
		// 그리드 설정 옵션
		result.append(this.baseInfoName + ".realgrid.styles = " + this.getGridStylesForGroup() + ";\n");
		result.append(this.baseInfoName + ".realgrid.displayOptions = " + this.getDisplayOptions() + ";\n");
		result.append(this.baseInfoName + ".realgrid.options = " + this.option.toString() + ";\n");
		result.append(this.baseInfoName + ".realgrid.columns = " + "[" + this.getColumnInfoForGroup() + "]" + ";\n");

		// dataProvider 설정 옵션
		result.append(this.baseInfoName + ".dataProvider.fields = " + this.getDataProviderFieldsForGroup() + ";\n");
		result.append(this.baseInfoName + ".dataProvider.options = " + this.getDataProviderOption() + ";\n");

		return result.toString();
	}

	private String getGridStyles() {
		return "{\n" +
				"    \"grid\": {\n" +
				"        \"border\": \"#bfbfbf\"\n" +
				"    },\n" +
				"    \"body\": {\n" +
				"        \"paddingTop\": \"6\",\n" +
				"        \"paddingBottom\": \"6\",\n" +
				"        \"fontFamily\": \"Tahoma,Helvetica\",\n" +
				"        \"empty\": {\n" +
				"          \"textAlignment\": \"center\",\n" +
				"          \"lineAlignment\": \"center\",\n" +
				"          \"fontSize\": 15\n" +
				"        }\n" +
				"    },\n" +
				"    \"header\": {\n" +
				"        \"background\": \"#f4f4f4\",\n" +
				"        \"selectedBackground\": \"#dbdbdb\",\n" +
				"        \"selectedForeground\": \"#000000\",\n" +
				"        \"borderLeft\": \"#bfbfbf\",\n" +
				"        \"borderRight\": \"#bfbfbf\",\n" +
				"        \"borderBottom\": \"#53abe6,2\",\n" +
				"        \"fontFamily\": \"Tahoma,Helvetica\",\n" +
				"        \"fontBold\": \"true\",\n" +
				"        \"paddingTop\": \"7\",\n" +
				"        \"paddingBottom\": \"7\"\n" +
				"    }\n" +
				"}";
	}

	/**
	 * grid style group header style 추가
	 * header 안에 group header style 속성 정
	 * @return
	 */
	private String getGridStylesForGroup() {
		return "{\n" +
			"    \"grid\": {\n" +
			"        \"border\": \"#bfbfbf\"\n" +
			"    },\n" +
			"    \"body\": {\n" +
			"        \"paddingTop\": \"6\",\n" +
			"        \"paddingBottom\": \"6\",\n" +
			"        \"fontFamily\": \"Tahoma,Helvetica\",\n" +
			"        \"empty\": {\n" +
			"          \"textAlignment\": \"center\",\n" +
			"          \"lineAlignment\": \"center\",\n" +
			"          \"fontSize\": 15\n" +
			"        }\n" +
			"    },\n" +
			"    \"header\": {\n" +
			"        \"background\": \"#f4f4f4\",\n" +
			"        \"selectedBackground\": \"#dbdbdb\",\n" +
			"        \"selectedForeground\": \"#000000\",\n" +
			"        \"borderLeft\": \"#bfbfbf\",\n" +
			"        \"borderRight\": \"#bfbfbf\",\n" +
			"        \"borderBottom\": \"#53abe6,2\",\n" +
			"        \"fontFamily\": \"Tahoma,Helvetica\",\n" +
			"        \"fontBold\": \"true\",\n" +
			"        \"paddingTop\": \"7\",\n" +
			"        \"paddingBottom\": \"7\",\n" +
			"		 \"group\": { \n" +
			"        		\"background\": \"#dbdbdb\",\n" +
			"        		\"selectedBackground\": \"#dbdbdb\",\n" +
			"        		\"selectedForeground\": \"#000000\",\n" +
			"        		\"borderLeft\": \"#bfbfbf\",\n" +
			"        		\"borderRight\": \"#bfbfbf\",\n" +
			"        		\"borderBottom\": \"#53abe6,2\",\n" +
			"        		\"fontFamily\": \"Tahoma,Helvetica\",\n" +
			"        		\"fontBold\": \"true\",\n" +
			"        		\"paddingTop\": \"7\",\n" +
			"        		\"paddingBottom\": \"7\"\n" +
			"		   }\n" +
			"    }\n" +
			"}";
	}

	private String getDisplayOptions() {
				return realGridType.getBaseInfo();
	}

	private String getColumnInfo() {
		return columnList.stream().map(c -> c.toString()).collect(Collectors.joining(", ", "[", "]"));
	}

	/**
	 *
	 * 그룹명이 같은 컬럼 끼리 그룹핑 해준다.
	 * @return 리얼그리드 컬럼 리스트
	 */
	public String getColumnInfoForGroup(List<RealGridGroupColumn> addGroupColumnList) {

		StringBuilder groupColumn = new StringBuilder();

		// 그룹명. 헤더 노출용
		List<String> groupNameList = addGroupColumnList.stream().map(RealGridGroupColumn::getGroupName).collect(Collectors.toList());

		int idx = 0;
		for (RealGridGroupColumn realGridGroupColumn : groupColumnList) {
			if (realGridGroupColumn.getGroupName().isEmpty()) { // 그룹명이 없으면 일반 컬럼
				groupColumn.append(realGridGroupColumn.toString()).append(",");
			} else {	// 그룹명이 있으면 그룹핑 필요한 컬럼
				if (groupNameList.indexOf(realGridGroupColumn.getGroupName()) == idx) { //group 시작 일 때 start tag 생성
					groupColumn.append("{\n")
						.append("                \"type\": \"group\",\n")
						.append("                \"name\": \"")
						.append(realGridGroupColumn.getGroupName())
						.append("\",\n")
						.append("                \"columns\": [\n");
				}

				groupColumn.append(realGridGroupColumn.toString());

				if (groupNameList.lastIndexOf(realGridGroupColumn.getGroupName()) != idx) { //group 마지막 일 때 end tag 생성
					groupColumn.append(",");
				} else {
					groupColumn.append("                ]\n")
						.append("            },\n");
				}
			}
			idx++;
		}

		return groupColumn.toString().substring(0, groupColumn.length()-1);
	}

	/**
	 *
	 * 그룹명이 같은 컬럼 끼리 그룹핑 해준다.
	 * @return 리얼그리드 컬럼 리스트
	 */
	private String getColumnInfoForGroup() {
		StringBuilder groupColumn = new StringBuilder();

		// 그룹명. 헤더 노출용
		List<String> groupNameList = groupColumnList.stream().map(RealGridGroupColumn::getGroupName).collect(Collectors.toList());

		int idx = 0;
		for (RealGridGroupColumn realGridGroupColumn : groupColumnList) {
			if (realGridGroupColumn.getGroupName().isEmpty()) { // 그룹명이 없으면 일반 컬럼
				groupColumn.append(realGridGroupColumn.toString()).append(",");
			} else {	// 그룹명이 있으면 그룹핑 필요한 컬럼
				if (groupNameList.indexOf(realGridGroupColumn.getGroupName()) == idx) { //group 시작 일 때 start tag 생성
					groupColumn.append("{\n")
						.append("                \"type\": \"group\",\n")
						.append("                \"name\": \"")
						.append(realGridGroupColumn.getGroupName())
						.append("\",\n")
						.append("                \"columns\": [\n");
				}

				groupColumn.append(realGridGroupColumn.toString());

				if (groupNameList.lastIndexOf(realGridGroupColumn.getGroupName()) != idx) { //group 마지막 일 때 end tag 생성
					groupColumn.append(",");
				} else {
					groupColumn.append("                ]\n")
						.append("            },\n");
				}
			}
			idx++;
		}

		return groupColumn.toString().substring(0, groupColumn.length()-1);
	}

	/**
	 * 리얼 그리드 데이터 필드 문자열 반환
	 */
	private String getDataProviderFields() {
		return columnList.stream().map(c -> "{\"fieldName\" : \"" + c.getFieldName() + "\", \"dataType\" : \"" + c.getFieldType() + "\"}").collect(Collectors.joining(",", "[", "]"));
	}

	/**
	 * 리얼 그리드 데이터 필드 문자열 반환. (그룹 컬럼 존재 시 사용)
	 */
	private String getDataProviderFieldsForGroup() {
		return this.groupColumnList.stream().map(c -> "{\"fieldName\" : \"" + c.getFieldName() + "\", \"dataType\" : \"" + c.getFieldType() + "\"}").collect(Collectors.joining(",", "[", "]"));
	}

	private String getDataProviderOption() {
		return "{\"softDeleting\": " + Boolean.valueOf(this.option.isHideDeletedRows()).toString() + " }";
	}

}
