package kr.co.homeplus.partner.web.claim.controller;

import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.partner.web.claim.model.ClaimBoardCountGetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimCancelListGetDto;
import kr.co.homeplus.partner.web.claim.model.RequestMultiCancelGetDto;
import kr.co.homeplus.partner.web.claim.model.RequestRentalMultiCancelSetDto;
import kr.co.homeplus.partner.web.claim.model.RequestOrderShipMultiCancelSetDto;
import kr.co.homeplus.partner.web.claim.model.RequestTicketMultiCancelSetDto;
import kr.co.homeplus.partner.web.claim.model.TicketRentalCancelListGetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimExchangeListGetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimListSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimDetailListGetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimDetailGetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimPreRefundGetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimPreRefundSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimRegSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimReturnListGetDto;
import kr.co.homeplus.partner.web.claim.model.OrderShipAddrInfoDto;
import kr.co.homeplus.partner.web.claim.service.ClaimService;
import kr.co.homeplus.partner.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/claim")
public class ClaimController {

    private final CodeService codeService;
    private final ClaimService claimService;

    @GetMapping("/claimMain")
    public String itemMain(Model model, @RequestParam(value="claimType") String claimType, @RequestParam(value="schStatus") String schStatus) throws Exception {
        String url = "";

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "claim_search_type",   //검색어
            "claim_status",                 //클레임 상태
            "dlv_cd",                       // 택배사 코드
            "claim_reason_type",            //클레임 타입
            "ship_method"

        );

        //코드정보
        model.addAttribute("dlvCd", code.get("dlv_cd"));
        model.addAttribute("claimSearchType", code.get("claim_search_type"));
        model.addAttribute("claimStatus", code.get("claim_status"));
        model.addAttribute("claimReasonType", code.get("claim_reason_type"));
        model.addAttribute("shipMethod", code.get("ship_method"));
        model.addAttribute("schStatus", schStatus);

        switch (claimType){
            case "C" :
                url = "/claim/claimCancelMain";
                model.addAllAttributes(RealGridHelper.create("claimCancelListGridBaseInfo", ClaimCancelListGetDto.class));
                break;
            case "R" :
                url = "/claim/claimReturnMain";
                model.addAllAttributes(RealGridHelper.create("claimReturnListGridBaseInfo", ClaimReturnListGetDto.class));
                break;
            case "X" :
                url = "/claim/claimExchangeMain";
                model.addAllAttributes(RealGridHelper.create("claimExchangeListGridBaseInfo", ClaimExchangeListGetDto.class));
                break;
            case "E" :
                url = "/claim/ticketRentalCancelMain";
                model.addAllAttributes(RealGridHelper.create("ticketRentalCancelListGridBaseInfo", TicketRentalCancelListGetDto.class));
                break;
        }
        return url;
    }


    @ResponseBody
    @GetMapping("/getClaimBoardCount.json")
    @ApiOperation(value = "클레임 건수 조회", response = ClaimBoardCountGetDto.class)
    public ClaimBoardCountGetDto getClaimBoardCount(@RequestParam(name="claimType")String claimType) {
        return claimService.getClaimBoardCount(claimType);
    }

    @ResponseBody
    @GetMapping("/getTicketRentalClaimBoardCount.json")
    @ApiOperation(value = "클레임 건수 조회", response = ClaimBoardCountGetDto.class)
    public ClaimBoardCountGetDto getTicketRentalClaimBoardCount(@RequestParam(name="claimType")String claimType) {
        return claimService.getTicketRentalClaimBoardCount(claimType);
    }
    @ResponseBody
    @PostMapping("/getClaimList.json")
    @ApiOperation(value = "주문상세 내역 - 취소 리스트 조회", response = ClaimCancelListGetDto.class)
    public List<ClaimCancelListGetDto> getClaimList(@RequestBody ClaimListSetDto claimListSetDto) {
        return claimService.getClaimList(claimListSetDto);
    }

    @ResponseBody
    @PostMapping("/getClaimListCnt.json")
    @ApiOperation(value = "주문상세 내역 - 취소 리스트 조회")
    public Integer getClaimListCnt(@RequestBody ClaimListSetDto claimListSetDto) {
        return claimService.getClaimListCnt(claimListSetDto);
    }

    @ResponseBody
    @PostMapping("/getTicketRentalCancelList.json")
    @ApiOperation(value = "주문상세 내역 - 이티켓/렌탄 취소 리스트 조회", response = ClaimCancelListGetDto.class)
    public List<TicketRentalCancelListGetDto> getTicketRentalCancelList(@RequestBody ClaimListSetDto claimListSetDto) {
        return claimService.getTicketRentalCancelList(claimListSetDto);
    }

    @ResponseBody
    @PostMapping("/getReturnClaimList.json")
    @ApiOperation(value = "주문상세 내역 - 반품 리스트 조회", response = ClaimReturnListGetDto.class)
    public List<ClaimReturnListGetDto> getReturnClaimList(@RequestBody ClaimListSetDto claimListSetDto) {
        return claimService.getReturnClaimList(claimListSetDto);
    }

    @ResponseBody
    @PostMapping("/getExchangeClaimList.json")
    @ApiOperation(value = "주문상세 내역 - 반품 리스트 조회", response = ClaimReturnListGetDto.class)
    public List<ClaimExchangeListGetDto> getExchangeClaimList(@RequestBody ClaimListSetDto claimListSetDto) {
        return claimService.getExchangeClaimList(claimListSetDto);
    }

    @ResponseBody
    @GetMapping("getClaimDetailList.json")
    @ApiOperation(value = "클레임 상세정보 리스트 조회", response = ClaimDetailListGetDto.class)
    public List<ClaimDetailListGetDto> getClaimDetailList(@RequestParam(name="purchaseOrderNo")long purchaseOrderNo, @RequestParam(name="bundleNo")long bundleNo) {
        return claimService.getClaimDetailList(purchaseOrderNo, bundleNo);
    }

    @ResponseBody
    @GetMapping("getClaimDetail.json")
    @ApiOperation(value = "클레임 팝업 - 클레임 단건 상세 조회", response = ClaimDetailListGetDto.class)
    public ClaimDetailGetDto getClaimDetail(@RequestParam(name="claimBundleNo")long claimBundleNo, @RequestParam(name="claimType")String claimType) {
        return claimService.getClaimDetail(claimBundleNo, claimType);
    }

    @ResponseBody
    @GetMapping("/getOrderShipAddrInfo.json")
    public List<OrderShipAddrInfoDto> getOrderShipAddrInfo(@RequestParam("bundleNo") long bundleNo){
        return claimService.getOrderShipAddrInfo(bundleNo);
    }


    @ResponseBody
    @PostMapping("/preRefundPrice.json")
    @ApiOperation(value = "판매관리 - 취소/반품 팝업 - 환불예정금액 조회", response = ClaimPreRefundGetDto.class)
    public ClaimPreRefundGetDto getPreRefundPrice(@RequestBody ClaimPreRefundSetDto claimPreRefundSetDto) {
        return claimService.getPreRefundPrice(claimPreRefundSetDto);
    }

    @ResponseBody
    @PostMapping("/claimRegister.json")
    @ApiOperation(value = "판매관리 - 취소/반품 팝업 - 클레임 신청")
    public ResponseObject<String> claimRegister(@RequestBody ClaimRegSetDto claimRegSetDto) {
        return claimService.claimRegister(claimRegSetDto);
    }

    @ResponseBody
    @PostMapping("/rentalMultiCancel.json")
    @ApiOperation(value = "렌탈판매관리 - 주문취소")
    public ResponseObject<RequestMultiCancelGetDto> reqMultiCancel(@RequestBody RequestRentalMultiCancelSetDto requestRentalMultiCancelSetDto) {
        return claimService.reqMultiCancel(requestRentalMultiCancelSetDto);
    }

    @ResponseBody
    @PostMapping("/ticketMultiCancel.json")
    @ApiOperation(value = "이티켓판매관리 - 주문취소")
    public ResponseObject<RequestMultiCancelGetDto> ticketMultiCancel(@RequestBody RequestTicketMultiCancelSetDto requestTicketMultiCancelSetDto) {
        return claimService.ticketMultiCancel(requestTicketMultiCancelSetDto);
    }

    @ResponseBody
    @PostMapping("/orderShipMultiCancel.json")
    @ApiOperation(value = "발주/발송관리 - 주문취소")
    public ResponseObject<RequestMultiCancelGetDto> orderShipMultiCancel(@RequestBody RequestOrderShipMultiCancelSetDto requestMultiCancelSetDto) {
        return claimService.orderShipMultiCancel(requestMultiCancelSetDto);
    }

}
