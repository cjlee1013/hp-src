package kr.co.homeplus.partner.web.common.model.home;

import lombok.Getter;
import lombok.Setter;

/**
 * 파트너 나의 판매활 모델
 */
@Setter
@Getter
public class SalesActInfo {

    /** 미답변 상품 Q&A : 문의 작성일 기준 최근 3개월 대상으로 집계, 퍼센트는 제거 **/
    private Integer notReplyQnaCount;

    /** 판매중인 상품 : 상품 등록일 기준으로 최근 3개월 대상으로 집계 **/
    private Integer saleItemQty;

    /** 재고 10개 이하 상품 : 상품 등록일 기준으로 최근 3개월 대상으로 집계 **/
    private Integer underStockQty;

    /** 판매종료 임박 상품 : 상품 등록일 기준으로 최근 3개월 대상으로 집계, 판매종료일 7일 미만 남은 것 카운트 **/
    private Integer endImpnItemQty;
}
