package kr.co.homeplus.partner.web.core.config;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * application.yml 설정을 바인딩한 프로퍼티<p>
 * 권한체크 인터셉터에서 메뉴 접근권한을 체크할 때 사용합니다.
 *
 * @see ConfigurationProperties
 */
@Data
@Component
@ConfigurationProperties(prefix = "authorization")
public class AuthorizationProperties {

    /**
     * 인터셉터 접근권한 체크 사용여부
     */
    private boolean interceptorCheckEnable;

    /**
     * application.yml에 authorization.menu-check-enable 설정에 따라 "메인 좌측의 메뉴" 노출 여부를 관리합니다.<p>
     * - false로 할 경우 : 메뉴 권한을 소유하고 있지 않을 경우 메뉴를 노출합니다.<br>
     * - true로 할 경우 : 메뉴 권한을 소유하고 있지 않을 경우 메뉴를 미 노출 합니다.
     */
    private boolean menuCheckEnable;

    /**
     * 접근권한 체크 제외할 URL 목록
     */
    private List<String> ignoreAuthCheckUris;

    /**
     * 망분리 접근제어 여부
     */
    private boolean networkSeparateAccessCheckEnable;

    /**
     * 망분리 접근 제어시 체크를 위한 '업무용 VDI' 아이피 대역 정보
     *
     * ex) 192.168.2.1, 192.168.2.0/24 ...
     */
    private String networkSeparateAllowVdiIp;
    /**
     * 망분리 접근 제어시 체크를 위한 고객만족센터 아이피 대역 정보
     *
     * ex) 192.168.2.1, 192.168.2.0/24 ...
     */
    private String networkSeparateAllowCustomerIp;
}
