package kr.co.homeplus.partner.web.partner.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import lombok.Data;

@Data
@ApiModel("판매자관리 > 판매 상품유형 Set Entry")
public class PartnerAuthGetDto implements Serializable {

    @ApiModelProperty(value = "판매유형")
    private String saleType;

    List<PartnerTypesGetDto> partnerTypeList;
}
