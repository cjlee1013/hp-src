package kr.co.homeplus.partner.web.common.controller;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.co.homeplus.partner.web.common.exception.NotAllowedUrlDownloadFileException;
import kr.co.homeplus.partner.web.common.exception.UrlDownloadFileFailedException;
import kr.co.homeplus.partner.web.core.annotation.CertificationNeedLess;
import kr.co.homeplus.partner.web.core.dto.ImageFileInfoDto;
import kr.co.homeplus.partner.web.core.dto.UploadImageFileDto;
import kr.co.homeplus.partner.web.core.exception.handler.ExceptionCode;
import kr.co.homeplus.partner.web.core.service.UploadService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 공통 업로드
 */
@Slf4j
@Controller
@RequestMapping(value = "/common")
@RequiredArgsConstructor
public class UploadController {

	@Value("${plus.resource-routes.upload.url}")
	private String uploadUrl;

	private final UploadService uploadService;

	/**
	 * 허용 된 다운로드 URL 리스트
	 */
	private final List<String> allowedDownloadUrlList = List.of("/provide/view");
	private final AntPathMatcher antPathMatcher = new AntPathMatcher();

	/** URL 다운로드 용 connect timeout **/
	private static final int URL_DOWNLOAD_CONNECT_TIMEOUT = 5 * 1000;
	/** URL 다운로드 용 read timeout **/
	private static final int URL_DOWNLOAD_READ_TIMEOUT = 10 * 1000;

	/**
	 * 이미지 업로드
	 * @param request
	 * @param processKey
	 * @return
	 * @throws Exception
	 */
	@CertificationNeedLess
	@ResponseBody
	@RequestMapping(value = {"/upload.json"}, method = RequestMethod.POST)
	public Map<String, List<ImageFileInfoDto>> upload(HttpServletRequest request
			, @RequestParam(value = "fieldName", required = false) String fieldName
			, @RequestParam(value = "processKey") String processKey
			, @RequestParam(value = "fileName", required = false) String fileName
			, @RequestParam(value = "mode", required = false) String mode
			, @RequestParam(value = "imgUrl", required = false) String imgUrl) throws Exception {

		UploadImageFileDto uploadImageFileDto = new UploadImageFileDto(processKey, fileName, imgUrl, mode);
		if (StringUtils.isEmpty(fieldName)) {
			uploadImageFileDto.setImageFieldName("fileArr");
		} else {
			uploadImageFileDto.setImageFieldName(fieldName);
		}

		Map<String, List<ImageFileInfoDto>> result = uploadService
			.getUploadImageFileInfo(request, Arrays.asList(uploadImageFileDto));

		return result;
	}

	/**
	 * URL 파일 다운로드
	 * @param response HttpServletResponse
	 * @param fileUrl 파일 다운로드 URL(storage-api)
	 * @param fileName 파일명
	 * @throws NotAllowedUrlDownloadFileException 허용된 다운로드 URL 경로가 아닐 경우 발생하는 Exception
	 * @throws UrlDownloadFileFailedException URL 파일 다운로드 실패 Exception
	 */
	@CertificationNeedLess
	@PostMapping(value = "/getUrlDownloadFile")
	public void getUrlDownloadFile(final HttpServletResponse response,
		@RequestParam(value = "fileUrl") final String fileUrl,
		@RequestParam(value = "fileName") final String fileName,
		@RequestParam(value = "fileId") final String fileId) {

		if (isNotAllowedDownloadUrl(fileUrl)) {
			log.error("getUrlDownloadFile failed! fileUrl:{},fileName:{},fileId:{}", fileUrl,
				fileName, fileId);
			throw new NotAllowedUrlDownloadFileException(
				ExceptionCode.SYS_ERROR_CODE_9401.getDesc());
		}

		URL url;
		try {
			url = new URL(uploadUrl + fileUrl + "&fileId=" + fileId);
		} catch (MalformedURLException e) {
			throw new UrlDownloadFileFailedException(e);
		}

		URLConnection uCon;
		try {
			uCon = url.openConnection();
			uCon.setConnectTimeout(URL_DOWNLOAD_CONNECT_TIMEOUT);
			uCon.setReadTimeout(URL_DOWNLOAD_READ_TIMEOUT);
		} catch (IOException e) {
			throw new UrlDownloadFileFailedException(e);
		}

		try (final InputStream is = uCon.getInputStream();
			final OutputStream os = response.getOutputStream()){

			final byte[] buf = new byte[1024];
			int byteWritten = 0;
			int byteRead;

			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment; fileName=\""
				+ URLEncoder.encode(fileName, StandardCharsets.UTF_8) + "\";");
			response.setHeader("Content-Transfer-Encoding", "binary");

			while ((byteRead = is.read(buf)) != -1) {
				os.write(buf, 0, byteRead);
				byteWritten += byteRead;
			}

			response.setContentLength(byteWritten);
			os.flush();
		} catch (Exception e) {
			throw new UrlDownloadFileFailedException(e);
		}
	}

	/**
	 * 위험한 형식의 파일을 원격지로부터 다운로드를 방지하기 위해 fileUrl 이 허용된 다운로드 URL 경로가 아닌지 체크합니다.
	 *
	 * @param fileUrl 파일 다운로드 URL
	 * @return {@link UploadController#allowedDownloadUrlList} 에 등록된 url이 아닐 경우 true,
	 * 등록된 url일 경우 false를 리턴합니다.
	 * @see UploadController#allowedDownloadUrlList
	 */
	private boolean isNotAllowedDownloadUrl(final String fileUrl) {
		if (StringUtils.isEmpty(fileUrl)) {
			throw new UrlDownloadFileFailedException(ExceptionCode.SYS_ERROR_CODE_9403.getDesc());
		}
		return allowedDownloadUrlList.stream()
			.noneMatch(allowUrl -> antPathMatcher.match(allowUrl, getUriPath(fileUrl)));
	}

	/**
	 * url에서 uri path 만 추출하여 리턴합니다.<p>
	 * 불필요한 경로 등은(여러개의 슬래시가 들어올 경우 등) 정규화 처리 합니다.(ex. "//notice/prodNotice///notice")
	 *
	 * @param fileUrl fileUrl
	 * @return uri
	 */
	private String getUriPath(final String fileUrl) {
		URI uri;
		try {
			uri = new URI(fileUrl);
		}
		catch (URISyntaxException e) {
			log.error("UploadController.getUriPath() failed! fileUrl:{}", fileUrl);
			throw new UrlDownloadFileFailedException(e);
		}

		//불필요한 경로 등을(여러개의 슬래시가 들어올 경우 등) 정규화 처리(ex. "//notice/prodNotice///notice")
		uri = uri.normalize();
		return uri.getPath();
	}

	/**
	 * 이미지 업로드 IE 전용
	 * @param request
	 * @param processKey
	 * @param baseKeyCd
	 * @param mode
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = {"/uploadImageIE.json"}, method = RequestMethod.POST)
	public void uploadImageIE(HttpServletRequest request, HttpServletResponse response
		, @RequestParam(value = "processKey", required = true) String processKey
		, @RequestParam(value = "baseKeyCd", required = false) String baseKeyCd
		, @RequestParam(value = "mode", required = false) String mode
		, @RequestParam(value = "fileName", required = false) String fileName) throws Exception {

		Map<String, List<ImageFileInfoDto>> result = this.upload(request, "", processKey, fileName, "IMG", null);

		// IE 9에서 application/json을 지원하지 않기 때문에 text/html로 변경함
		response.setContentType("text/html; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(new Gson().toJson(result));
	}

	/**
	 * froala Editor Proxy
	 * @param request
	 * @param fieldName
	 * @param processKey
	 * @param fileName
	 * @param mode
	 * @param imgUrl
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"/imageCORSProxy/upload.json"}, method = {RequestMethod.GET})
	public String imageCORSProxyUpload(HttpServletRequest request
		, @RequestParam(value = "fieldName", required = false) String fieldName
		, @RequestParam(value = "processKey") String processKey
		, @RequestParam(value = "fileName", required = false) String fileName
		, @RequestParam(value = "mode", required = false) String mode
		, @RequestParam(value = "imgUrl", required = false) String imgUrl) {

		return "";
	}

}
