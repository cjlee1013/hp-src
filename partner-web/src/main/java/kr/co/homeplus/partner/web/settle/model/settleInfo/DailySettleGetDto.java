package kr.co.homeplus.partner.web.settle.model.settleInfo;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DailySettleGetDto {
    @ApiModelProperty(value = "매출일")
    @RealGridColumnInfo(headText = "매출일", width = 100, columnType = RealGridColumnType.DATE, sortable = true)
    private String basicDt;

    @ApiModelProperty(value = "구분")
    @RealGridColumnInfo(headText = "구분", width = 100, sortable = true)
    private String gubun;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 100, sortable = true)
    private long itemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", width = 150, sortable = true)
    private String itemNm;

    @ApiModelProperty(value = "과면세구분")
    @RealGridColumnInfo(headText = "과면세구분", width = 100, sortable = true)
    private String taxYn;

    @ApiModelProperty(value = "집계수량")
    @RealGridColumnInfo(headText = "집계수량", width = 100, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipCompleteQty;

    @ApiModelProperty(value = "집계금액")
    @RealGridColumnInfo(headText = "집계금액", width = 100, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipCompleteAmt;

    @ApiModelProperty(value = "판매수수료")
    @RealGridColumnInfo(headText = "판매수수료", width = 100, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long saleAgencyFee;

    @ApiModelProperty(value = "배송비")
    @RealGridColumnInfo(headText = "배송비", width = 100, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipAmt;

    @ApiModelProperty(value = "상품할인")
    @RealGridColumnInfo(headText = "상품할인", width = 100, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponChargeAmt;

    @ApiModelProperty(value = "배송비할인")
    @RealGridColumnInfo(headText = "배송비할인", width = 100, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long chargeShipAmt;

    @ApiModelProperty(value = "정산대상금액")
    @RealGridColumnInfo(headText = "정산대상금액", width = 100, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long settleAmt;

}
