package kr.co.homeplus.partner.web.core.usermenu.exception;

/**
 * 메뉴 로드 실패 관련 Exception
 */
public class MenuLoadFailException extends RuntimeException {

    public MenuLoadFailException() {
    }

    public MenuLoadFailException(String message) {
        super(message);
    }

    public MenuLoadFailException(Exception e) {
        super(e);
    }

    public MenuLoadFailException(String message, Exception e) {
        super(message, e);
    }
}
