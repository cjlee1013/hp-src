package kr.co.homeplus.partner.web.settle.controller;

import java.util.List;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.dto.UserInfo;
import kr.co.homeplus.partner.web.settle.model.salesTrend.SalesByAgeInfo;
import kr.co.homeplus.partner.web.settle.model.salesTrend.SalesByDailyInfo;
import kr.co.homeplus.partner.web.settle.model.salesTrend.SalesByGenderInfo;
import kr.co.homeplus.partner.web.settle.model.salesTrend.SalesTrendSummaryInfo;
import kr.co.homeplus.partner.web.settle.model.salesTrend.SearchConditionDto;
import kr.co.homeplus.partner.web.settle.service.PartnerSalesTrendService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/salesTrend")
@RequiredArgsConstructor
public class PartnerSalesTrendController {

    private final ResourceClient resourceClient;

    private final CertificationService certificationService;

    private final PartnerSalesTrendService partnerSalesTrendService;

    @GetMapping("/salesTrendMain")
    public String salesTrendMain(Model model) {
        UserInfo userInfo = certificationService.getLoginUerInfo();
        model.addAttribute("sellerId", userInfo.getPartnerId());
        return "/settle/salesTrendMain";
    }

    @ResponseBody
    @PostMapping(value = "/summary.json")
    public SalesTrendSummaryInfo searchSummaryInfo(
        @RequestBody final SearchConditionDto searchConditionDto) {
        return resourceClient.postForResponseObject(ResourceRouteName.SETTLE, searchConditionDto,
            "/partner/salesTrend/summary",
            new ParameterizedTypeReference<ResponseObject<SalesTrendSummaryInfo>>() {
            }).getData();
    }

    @ResponseBody
    @PostMapping(value = "/daily.json")
    public List<SalesByDailyInfo> searchSalesByDaily(
        @RequestBody final SearchConditionDto searchConditionDto) {
        return partnerSalesTrendService.searchSalesByDaily(searchConditionDto);
    }

    @ResponseBody
    @PostMapping(value = "/gender.json")
    public List<SalesByGenderInfo> searchSalesByGender(
        @RequestBody final SearchConditionDto searchConditionDto) {
        return resourceClient.postForResponseObject(ResourceRouteName.SETTLE, searchConditionDto,
            "/partner/salesTrend/gender",
            new ParameterizedTypeReference<ResponseObject<List<SalesByGenderInfo>>>() {
            }).getData();
    }

    @ResponseBody
    @PostMapping(value = "/age.json")
    public List<SalesByAgeInfo> searchSalesByAge(
        @RequestBody final SearchConditionDto searchConditionDto) {
        return resourceClient.postForResponseObject(ResourceRouteName.SETTLE, searchConditionDto,
            "/partner/salesTrend/age",
            new ParameterizedTypeReference<ResponseObject<List<SalesByAgeInfo>>>() {
            }).getData();
    }
}
