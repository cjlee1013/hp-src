package kr.co.homeplus.partner.web.partner.model;

import lombok.Data;

@Data
public class PartnerManagerSetDto {

    private String seq;
    private String mngCd;
    private String mngNm;
    private String mngMobile;
    private String mngPhone;
    private String mngEmail;
    private String useYn;

}
