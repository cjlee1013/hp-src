package kr.co.homeplus.partner.web.sell.model.shipStatus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageListGetDto;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 배송현황 카운트 응답 DTO")
public class ShipStatusCntGetDto {
  @ApiModelProperty(notes = "배송중 건수")
  private String shippingCnt;

  @ApiModelProperty(notes = "배송완료 건수")
  private String completeCnt;

  @ApiModelProperty(notes = "미수취신고 건수")
  private String noReceiveCnt;
}