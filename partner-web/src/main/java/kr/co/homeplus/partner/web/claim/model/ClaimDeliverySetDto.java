package kr.co.homeplus.partner.web.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임 배송정보변경 요청 파라미터")
public class ClaimDeliverySetDto {

    @ApiModelProperty(value = "그룹클레임번호", position = 1, example = "1500000022")
    public long claimNo;

    @ApiModelProperty(value = "클레임 번들 번호", position = 2, example = "1500000022")
    public long claimBundleNo;

    @ApiModelProperty(value = "배송수단타입(NONE:변경안함/C:택배/D:직접배송/P:택배/N:배송안함)", position = 3, example = "C")
    public String exchShipType;

    @ApiModelProperty(value = "교환택배사코드", position = 4, example = "002")
    public String exchDlvCd;

    @ApiModelProperty(value = "교환송장번호", position = 5, example = "1123123123")
    public String exchInvoiceNo;

    @ApiModelProperty(value = "교환메모", position = 6, example = "우편배송 메모")
    public String exchMemo;

    @ApiModelProperty(value="교환일자", position = 7, example = "2020-12-29")
    public String shipDt;
}
