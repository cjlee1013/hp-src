package kr.co.homeplus.partner.web.partner.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//판매업체 비밀번호 인증 조회 파라미터
public class PartnerCheckPwParamDto {

    //패스워드
    private String password;

    //판매자ID
    private String partnerId;

    //판매자유형
    private String partnerType;
}
