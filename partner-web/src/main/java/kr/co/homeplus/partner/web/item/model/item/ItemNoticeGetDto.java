package kr.co.homeplus.partner.web.item.model.item;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품정보고시 정보")
public class ItemNoticeGetDto {

    @ApiModelProperty(value = "고시항목번호")
    private int noticeNo;

    @ApiModelProperty(value = "고시항목명", hidden = true)
    private String noticeNm;

    @ApiModelProperty(value = "정보고시항목명")
    @Size(min = 1, max = 50, message = "정보고시항목명")
    private String noticeDesc;

    @ApiModelProperty(value = "정보고시항목명 (TD용)")
    private String noticeHtml;
}
