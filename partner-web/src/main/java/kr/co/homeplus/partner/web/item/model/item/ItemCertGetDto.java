package kr.co.homeplus.partner.web.item.model.item;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

/**
 * 인증 정보
 */
@Getter
@Setter
public class ItemCertGetDto {

	@ApiModelProperty(value = "상품인증 시퀀스")
	private Integer itemCertSeq;

	@ApiModelProperty(value = "인증여부 (Y:인증대상, N:인증대상아님, E:면제대상)")
	@NotEmpty(message = "인증여부")
	@Pattern(regexp = "(Y|N|E)", message="구매제한 타입")
	private String isCert;

	@ApiModelProperty(value = "인증그룹 (공통코드 : item_cert_group) - (어린이제품:KD,생활용품:LF,전기용품:ER,방송통신기자재:RP)")
	private String	certGroup;

	@ApiModelProperty(value = "면제유형 (item_exempt_type) PI: 병행수입 면제, PA: 구매대행 면제")
	private String 	certExemptType;

	@ApiModelProperty(value = "인증유형 (공통코드 : item_cert_type)")
	private String 	certType;

	@ApiModelProperty(value = "인증번호")
	private String 	certNo;

	@ApiModelProperty(value = "히스토리 등록용", hidden = true)
	private String isCertNm;
	private String certGroupNm;
	private String certExemptTypeNm;
	private String certTypeNm;

}
