package kr.co.homeplus.partner.web.voc.model.csQna;

import lombok.Data;

@Data
public class PartnerQnaTemplateSetDto {

    private String templateNo;

    private String templateTitle;

    private String templateContents;

    private String useYn;

    private String partnerId;

}
