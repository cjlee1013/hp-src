package kr.co.homeplus.partner.web.settle.model.salesTrend;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import lombok.Data;

@ApiModel("판매 트렌드 검색 조건")
@Data
public class SearchConditionDto {

    @ApiModelProperty(value = "검색 조건", allowableValues = "sales", required = true)
    private String searchType;

    @ApiModelProperty(value = "검색 시작 일", required = true)
    private LocalDate startDate;

    @ApiModelProperty(value = "검색 종료 일", required = true)
    private LocalDate endDate;

    @ApiModelProperty(value = "판매자 id", required = true)
    private String sellerId;
}
