package kr.co.homeplus.partner.web.item.model.itemRelation;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemRelationSetDto {

    @ApiModelProperty(value = "연관상품관리번호")
    private Long relationNo;

    @ApiModelProperty(value = "연관명")
    @NotEmpty(message = "연관명")
    private String relationNm;

    @ApiModelProperty(value = "노출여부")
    @NotEmpty(message = "노출여부")
    private String dispYn;

    @ApiModelProperty(value = "PC노출기준")
    private String dispPcType;

    @ApiModelProperty(value = "모바일노출기준")
    private String dispMobileType;

    @ApiModelProperty(value = "소분류 카테고리")
    private Integer scateCd;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "인트로이미지URL")
    private String imgUrl;

    @ApiModelProperty(value = "인트로이미지명")
    private String imgNm;

    @ApiModelProperty(value = "인트로이미지 사용여부")
    private String imgUseYn;

    @ApiModelProperty(value = "인트로이미지 사용여부")
    private String useYn;

    @ApiModelProperty(value = "등록/수정자")
    private String userId;

    @ApiModelProperty(value = "연관상품 리스트")
    List<String> relationItemList;

}
