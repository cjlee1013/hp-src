package kr.co.homeplus.partner.web.claim.controller;


import io.swagger.annotations.ApiOperation;
import kr.co.homeplus.partner.web.claim.model.ClaimApproveListSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimApproveSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimDeliverySetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimImageInfoDto;
import kr.co.homeplus.partner.web.claim.model.ClaimPendingSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimRejectSetDto;
import kr.co.homeplus.partner.web.claim.model.ExchCompleteSetDto;
import kr.co.homeplus.partner.web.claim.model.ExchShippingSetDto;
import kr.co.homeplus.partner.web.claim.model.PickCompleteSetDto;
import kr.co.homeplus.partner.web.claim.model.PickRequestSetDto;
import kr.co.homeplus.partner.web.claim.model.PickShippingSetDto;
import kr.co.homeplus.partner.web.claim.service.ClaimPopService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/claim")
public class ClaimPopController {

    private final ClaimPopService claimPopService;

    @ResponseBody
    @PostMapping("/requestClaimApprove.json")
    @ApiOperation(value = "일괄 클레임 승인 요청")
    public ResponseObject<String> requestClaimApprove(@RequestBody ClaimApproveListSetDto claimApproveListSetDto) {
        return claimPopService.requestClaimApprove(claimApproveListSetDto);
    }

    @ResponseBody
    @PostMapping("/setClaimApprove.json")
    @ApiOperation(value = "클레임 승인 요청")
    public ResponseObject<String> setClaimApprove(@RequestBody ClaimApproveSetDto claimApproveSetDto) throws Exception {
        return claimPopService.setClaimApprove(claimApproveSetDto);
    }

    @ResponseBody
    @PostMapping("/setClaimPending.json")
    @ApiOperation(value = "클레임 보류 요청")
    public ResponseObject<String> setClaimPending(@RequestBody ClaimPendingSetDto claimPendingSetDto) throws Exception {
        return claimPopService.setClaimPending(claimPendingSetDto);
    }

    @ResponseBody
    @PostMapping("/setClaimReject.json")
    @ApiOperation(value = "클레임 거절 요청")
    public ResponseObject<String> setClaimReject(@RequestBody ClaimRejectSetDto claimRejectSetDto) throws Exception {
        return claimPopService.setClaimReject(claimRejectSetDto);
    }

    /**
     * 클레임 수거요청
     * @param pickRequestSetDto
     */
    @ResponseBody
    @PostMapping("/setPickRequest.json")
    @ApiOperation(value = "클레임 수거요청")
    public ResponseObject<String> setPickRequest(@RequestBody PickRequestSetDto pickRequestSetDto) {
        return claimPopService.setPickRequest(pickRequestSetDto);
    }

    /**
     * 클레임 수거정보변경
     * @param pickShippingSetDto
     */
    @ResponseBody
    @PostMapping("/setPickShipping.json")
    @ApiOperation(value = "클레임 수거정보변경")
    public ResponseObject<String> setPickShipping(@RequestBody PickShippingSetDto pickShippingSetDto) {
        return claimPopService.setPickShipping(pickShippingSetDto);
    }

    /**
     * 클레임 수거완료요청
     * @param pickCompleteSetDto
     */
    @PostMapping("/setPickComplete.json")
    @ResponseBody
    @ApiOperation(value = "클레임 수거완료요청")
    public ResponseObject<Boolean> setPickComplete(@RequestBody PickCompleteSetDto pickCompleteSetDto) throws Exception {
        return claimPopService.setPickComplete(pickCompleteSetDto);
    }

    /**
     * 교환 배송 요청
     * @param claimDeliverySetDto
     */
    @ResponseBody
    @PostMapping("/setClaimDelivery.json")
    @ApiOperation(value = "교환 배송 요청")
    public ResponseObject<String> setClaimDelivery(@RequestBody ClaimDeliverySetDto claimDeliverySetDto) {
        return claimPopService.setClaimDelivery(claimDeliverySetDto);
    }


    /**
     * 클레임 교환배송 정보변경
     * @param exchShippingSetDto
     */
    @ResponseBody
    @PostMapping("/setExchShipping.json")
    @ApiOperation(value = "클레임 교환배송 정보변경")
    public ResponseObject<String> setExchShipping(@RequestBody ExchShippingSetDto exchShippingSetDto) {
        return claimPopService.setExchShipping(exchShippingSetDto);
    }

    /**
     * 클레임 교환완료 요청
     * @param exchCompleteSetDto
     */
    @ResponseBody
    @PostMapping("/setExchComplete.json")
    @ApiOperation(value = "클레임 교환완료 요청")
    public ResponseObject<String> setExchComplete(@RequestBody ExchCompleteSetDto exchCompleteSetDto) {
        return claimPopService.setExchComplete(exchCompleteSetDto);
    }

    /**
     * 이미지 정보 조회
     * **/
    @GetMapping("/getClaimImageInfo.json")
    @ResponseBody
    public ClaimImageInfoDto getClaimImageInfo(@RequestParam(value = "fileId") String fileId){
        return claimPopService.getClaimImageInfo(fileId);
    }


}
