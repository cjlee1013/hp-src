package kr.co.homeplus.partner.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimExchShippingDto {

    @ApiModelProperty(value = "클레임 교환접수 번호", position = 1, example = "23")
    public String claimExchShippingNo;

    @ApiModelProperty(value = "교환-이름", position = 2, example = "홍길동")
    public String exchReceiverNm;

    @ApiModelProperty(value = "교환-휴대폰", position = 3, example = "010-1111-1111")
    public String exchMobileNo;

    @ApiModelProperty(value = "교환 우편번호", position = 4, example = "07297")
    public String exchZipcode;

    @ApiModelProperty(value = "교환 기본주소", position = 5, example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    public String exchAddr;

    @ApiModelProperty(value = "교환 상세주소", position = 6, example = "금천전 홈플러스 1층")
    public String exchAddrDetail;

    @ApiModelProperty(value = "교환 송장번호", position = 7, example = "123123124122")
    public String exchInvoiceNo;

    @ApiModelProperty(value = "교환배송수단(C:택배배송 P:우편배송  D:업체직배송 N:배송없음)", position = 8, example = "택배배송")
    public String exchShipType;

    @ApiModelProperty(value = "교환배송수단코드(C:택배배송 P:우편배송  D:업체직배송 N:배송없음)", position = 9, example = "C")
    public String exchShipTypeCode;

    @ApiModelProperty(value = "교환 배송_상태(D1: 상품출고(송장등록) D2: 배송중 D3: 배송완료)", position = 11, example = "배송중")
    public String exchStatus;

    @ApiModelProperty(value = "교환 배송_상태 코드(D1: 상품출고(송장등록) D2: 배송중 D3: 배송완료)", position = 110, example = "D21")
    public String exchStatusCode;

    @ApiModelProperty(value = "교환배송 택배사코드", position = 12, example = "002")
    public String exchDlvCd;

    @ApiModelProperty(value = "교환배송 택배사명", position = 13, example = "한진택배")
    public String exchDlv;

    @ApiModelProperty(value = "교환배송 메모", position = 14, example = "우편배송 메모")
    public String exchMemo;

    @ApiModelProperty(value = "업체직배송의 배송예정일 최초등록일", position = 15, example = "2020-12-29 00:00:00")
    public String exchD0Dt;

    @ApiModelProperty(value = "교환배송 송장등록일(업체직배송의 경우 배송예정일)", position = 16, example = "2020-12-29 00:00:00")
    public String exchD1Dt;

    @ApiModelProperty(value = "교환배송 시작일", position = 17, example = "2020-12-29 00:00:00")
    public String exchD2Dt;

    @ApiModelProperty(value = "교환배송 완료일", position = 18, example = "2020-12-29 00:00:00")
    public String exchD3Dt;

    @ApiModelProperty(value="수거지 지번주소", position = 19, example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    public String exchBaseAddr;

}
