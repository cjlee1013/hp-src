package kr.co.homeplus.partner.web.voc.controller;

import java.util.List;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.utility.StringUtil;
import kr.co.homeplus.partner.web.voc.model.csQna.PartnerQnaListSelectDto;
import kr.co.homeplus.partner.web.voc.model.csQna.PartnerQnaTemplateSelectDto;
import kr.co.homeplus.partner.web.voc.model.itemReview.ItemReviewDetailGetDto;
import kr.co.homeplus.partner.web.voc.model.itemReview.ItemReviewListParamDto;
import kr.co.homeplus.partner.web.voc.model.itemReview.ItemReviewSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/voc/itemReview")
public class ItemReviewController {

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    private final ResourceClient resourceClient;
    private final CodeService codeService;
    private final CertificationService certificationService;

    public ItemReviewController(ResourceClient resourceClient,
        CodeService codeService,
        CertificationService certificationService
    ) {
        this.resourceClient = resourceClient;
        this.codeService = codeService;
        this.certificationService = certificationService;
    }

    /**
     *  고객관리 > 상품평 관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/itemReviewMain", method = RequestMethod.GET)
    public String itemReviewMain(Model model) {

        codeService.getCodeModel(model,
            "store_type"   // 스토어 타입
            , "block_yn"            // 미노출여부
            , "reserve_yn"          // 적립여부
        );

        model.addAllAttributes(RealGridHelper.create("itemReviewListGridBaseInfo", ItemReviewSelectDto.class));

        return "/voc/itemReviewMain";
    }

    /**
     *  고객관리 > 상품평 상
     */
    @RequestMapping(value = "/itemReviewDetailMain", method = RequestMethod.GET)
    public String itemReviewDetailMain(Model model, @RequestParam(value="reviewNo") Long reviewNo) {
        model.addAttribute("reviewNo", reviewNo);
        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        return "/voc/itemReviewDetailMain";
    }

    /**
     * 고객관리 > 상품평 관리 > 상품평 리스트 조회
     *
     * @param itemReviewListParamDto
     * @return List<ItemReviewListSelectDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/getItemReviewList.json"}, method = RequestMethod.GET)
    public List<ItemReviewSelectDto> getItemReviewList(ItemReviewListParamDto itemReviewListParamDto) {
        String apiUri = "/po/voc/itemReview/getItemReviewList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemReviewSelectDto>>>() {};
        itemReviewListParamDto.setSearchPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        return (List<ItemReviewSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, ItemReviewListParamDto.class, itemReviewListParamDto), typeReference).getData();
    }

    /**
     * 고객관리 > 상품평 관리 > 상품평 상 조회
     *
     * @param reviewNo
     * @return ItemReviewDetailGetDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getItemReviewDetail.json"}, method = RequestMethod.GET)
    public ItemReviewDetailGetDto getItemReviewDetail(@RequestParam(value = "reviewNo") Long reviewNo) {
        String apiUri = "/po/voc/itemReview/getItemReviewDetail?reviewNo=" + reviewNo;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ItemReviewDetailGetDto>>() {};
        return (ItemReviewDetailGetDto) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri, typeReference).getData();
    }

}
