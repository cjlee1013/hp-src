package kr.co.homeplus.partner.web.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "반품교환 상태 수정.")
public class ClaimExchShippingModifyDto {

    @ApiModelProperty(value = "클레임 번들 번호")
    public long claimBundleNo;

    @ApiModelProperty(value = "반품교환 수정 타입 [주소:ADDR, 상태:STATUS, 배송타입:TYPE, 전부:ALL]")
    public String modifyType;

    @ApiModelProperty(value = "교환지-이름")
    public String exchReceiverNm;

    @ApiModelProperty(value = "교환지-휴대폰")
    public String exchMobileNo;

    @ApiModelProperty(value = "교환지 우편번호")
    public String exchZipcode;

    @ApiModelProperty(value = "교환지 기본주소")
    public String exchRoadBaseAddr;

    @ApiModelProperty(value = "교환지 샹세주소")
    public String exchRoadAddrDetail;

    @ApiModelProperty(value = "교환지 기본주소")
    public String exchBaseAddr;

    @ApiModelProperty(value = "교환지 샹세주소")
    public String exchDetailAddr;

    @ApiModelProperty(value = "교환배송수단")
    public String exchShipType;

    @ApiModelProperty(value = "교환배송수단코드")
    public String exchShipTypeCode;

    @ApiModelProperty(value = "교환택배사코드")
    public String exchDlvCd;

    @ApiModelProperty(value = "교환택배사명")
    public String exchDlv;

    @ApiModelProperty(value = "교환송장번호")
    public String exchInvoiceNo;

    @ApiModelProperty(value = "교환메모")
    public String exchMemo;

    @ApiModelProperty(value="교환상태")
    public String exchStatus;

    @ApiModelProperty(value="배송타입")
    public String shipType;

    @ApiModelProperty(value="교환일자")
    public String shipDt;

    @ApiModelProperty(value="교환 예약접수자")
    public String exchP0Id;

    @ApiModelProperty(value="교환 예약일(업체직배송인 경우 교환예정일 최초등록일)")
    public String exchP0Dt;

    @ApiModelProperty(value="교환 송장등록일(업체직배송의 경우 교환예정일)")
    public String exchP1Dt;

    @ApiModelProperty(value="교환 시작일")
    public String exchP2Dt;

    @ApiModelProperty(value="교환 완료일")
    public String exchP3Dt;

    @ApiModelProperty(value = "수정ID")
    public String chgId;


    public ClaimExchShippingModifyDto(ExchCompleteSetDto exchCompleteSetDto){
        this.claimBundleNo = exchCompleteSetDto.getClaimBundleNo();
        this.exchShipType = "";
        this.exchStatus = "D3";
        this.modifyType = "STATUS";
    }


    public ClaimExchShippingModifyDto(ClaimDeliverySetDto claimDeliverySetDto){
        this.claimBundleNo = claimDeliverySetDto.getClaimBundleNo();
        this.exchShipType = claimDeliverySetDto.getExchShipType();
        this.exchDlvCd = claimDeliverySetDto.getExchDlvCd();
        this.exchInvoiceNo = claimDeliverySetDto.getExchInvoiceNo();
        this.exchMemo = claimDeliverySetDto.getExchMemo();
        this.exchRoadBaseAddr = "";
        this.exchStatus = "D1";
        this.modifyType = "ALL";
        this.shipDt = claimDeliverySetDto.getShipDt();
        this.shipType = "DS";
    }

    public ClaimExchShippingModifyDto(ExchShippingSetDto exchShippingSetDto){
        this.claimBundleNo = exchShippingSetDto.getClaimBundleNo();
        this.exchReceiverNm = exchShippingSetDto.getExchReceiverNm();
        this.exchMobileNo = exchShippingSetDto.getExchMobileNo();
        this.exchZipcode = exchShippingSetDto.getExchZipcode();
        this.exchRoadBaseAddr = exchShippingSetDto.getExchRoadBaseAddr();
        this.exchRoadAddrDetail = exchShippingSetDto.getExchRoadAddrDetail();
        this.exchBaseAddr = exchShippingSetDto.getExchBaseAddr();
        this.exchDetailAddr = exchShippingSetDto.getExchDetailAddr();
        this.exchShipType = exchShippingSetDto.getExchShipType();
        this.exchDlvCd = exchShippingSetDto.getExchDlvCd();
        this.exchInvoiceNo = exchShippingSetDto.getExchInvoiceNo();
        this.exchMemo = exchShippingSetDto.getExchMemo();
        this.exchStatus = "";
        if(exchShippingSetDto.getExchShipType().equalsIgnoreCase("NONE")){
            this.modifyType = "ADDR";
        }else{
            this.modifyType = "ALL";
        }
        this.shipDt = exchShippingSetDto.getShipDt();
        this.shipType = "DS";
    }

}