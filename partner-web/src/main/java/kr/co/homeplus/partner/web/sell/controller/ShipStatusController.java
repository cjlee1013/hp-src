package kr.co.homeplus.partner.web.sell.controller;

import com.nhncorp.lucy.security.xss.XssPreventer;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridColumn;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.partner.web.sell.model.ShipHistoryGetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusCntGetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusListGetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusListSetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusNoReceiveInfoGetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusNoReceiveInfoSetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusNoReceiveSetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusShipCompletePopListGetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusShipCompleteSetDto;
import kr.co.homeplus.partner.web.sell.service.SellCommonService;
import kr.co.homeplus.partner.web.sell.service.ShipStatusService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 판매관리 > 배송현황
 */
@Controller
@RequestMapping("/sell/shipStatus")
public class ShipStatusController {

    @Value("${front.homeplus.url}")
    private String homeplusFrontUrl;

    @Value("${front.theclub.url}")
    private String theclubFrontUrl;

    @Autowired
    private ShipStatusService shipStatusService;
    @Autowired
    private SellCommonService sellCommonService;

    // 배송현황 그리드
    private static final List<RealGridColumn> SHIP_STATUS_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption SHIP_STATUS_GRID_OPTION = new RealGridOption("fill", false, true, false);

    // 선택발송처리 팝업 그리드
    private static final List<RealGridColumn> SHIP_ALL_POP_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption SHIP_ALL_POP_GRID_OPTION = new RealGridOption("fill", false,  false, false);

    // 미수취신고 철회요청 팝업 그리드
    private static final List<RealGridColumn> NO_RECEIVE_POP_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption NO_RECEIVE_POP_GRID_OPTION = new RealGridOption("fill", false, false, false);

    // 배송완료 팝업 그리드
    private static final List<RealGridColumn> SHIP_COMPLETE_POP_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption SHIP_COMPLETE_POP_GRID_OPTION = new RealGridOption("fill", false, false, false);

    public ShipStatusController() {
        // 배송현황 그리드
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("shipNo", "shipNo", "배송SEQ", RealGridColumnType.BASIC, 80, false, false));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("purchaseOrderNo", "purchaseOrderNo", "주문번호", RealGridColumnType.BASIC, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("bundleNo", "bundleNo", "배송번호", RealGridColumnType.BASIC, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("orderItemNo", "orderItemNo", "상품주문번호", RealGridColumnType.BASIC, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("orderDt", "orderDt", "주문일시", RealGridColumnType.BASIC, 150, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("shipStatusNm", "shipStatusNm", "주문상태", RealGridColumnType.BASIC, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("shipStatus", "shipStatus", "주문상태코드", RealGridColumnType.BASIC, 80, false, false));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("itemNo", "itemNo", "상품번호", RealGridColumnType.BASIC, 120, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("itemNm1", "itemNm1", "상품명", RealGridColumnType.NONE, 350, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("txtOptVal", "txtOptVal", "옵션", RealGridColumnType.NONE, 250, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("itemQty", "itemQty", "수량", RealGridColumnType.BASIC, 60, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("itemPrice", "itemPrice", "상품금액", RealGridColumnType.PRICE, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("orderPrice", "orderPrice", "총상품금액", RealGridColumnType.PRICE, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("shipPrice", "shipPrice", "배송비", RealGridColumnType.PRICE, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("islandShipPrice", "islandShipPrice", "도서산간배송비", RealGridColumnType.PRICE, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("commissionPrice", "commissionPrice", "수수료", RealGridColumnType.PRICE, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("sellerChargePrice", "sellerChargePrice", "판매자쿠폰할인", RealGridColumnType.PRICE, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("buyerNm", "buyerNm", "구매자", RealGridColumnType.BASIC, 80, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("buyerMobileNo", "buyerMobileNo", "구매자 연락처", RealGridColumnType.BASIC, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("receiverNm", "receiverNm", "수령인", RealGridColumnType.BASIC, 80, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("shipMobileNo", "shipMobileNo", "수령인 연락처", RealGridColumnType.BASIC, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("shipMethodNm", "shipMethodNm", "배송방법", RealGridColumnType.BASIC, 90, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("shipMethod", "shipMethod", "배송방법코드", RealGridColumnType.BASIC, 80, false, false));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("dlvCdNm", "dlvCdNm", "택배사", RealGridColumnType.BASIC, 90, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("dlvCd", "dlvCd", "택배사코드", RealGridColumnType.BASIC, 90, false, false));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("invoiceNo", "invoiceNo", "송장번호", RealGridColumnType.BASIC, 120, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("shipReqDt", "shipReqDt", "배송요청일", RealGridColumnType.BASIC, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("reserveShipDt", "scheduleShipDt", "배송예정일", RealGridColumnType.BASIC, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("zipcode", "zipcode", "우편번호", RealGridColumnType.BASIC, 80, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("addr", "addr", "주소", RealGridColumnType.NONE, 350, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("dlvShipMsg", "dlvShipMsg", "배송메세지", RealGridColumnType.NONE, 200, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("shipPriceType", "shipPriceType", "배송비지급", RealGridColumnType.BASIC, 80, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("paymentFshDt", "paymentFshDt", "결제일시", RealGridColumnType.BASIC, 150, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("confirmDt", "confirmDt", "주문확인일시", RealGridColumnType.BASIC, 150, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("orgShipDt", "orgShipDt", "발송기한", RealGridColumnType.BASIC, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("delayShipDt", "delayShipDt", "2차발송기한", RealGridColumnType.BASIC, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("completeDt", "completeDt", "배송완료일", RealGridColumnType.BASIC, 150, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("noRcvDeclrDt", "noRcvDeclrDt", "미수취신고일", RealGridColumnType.BASIC, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("noRcvProcessNm", "noRcvProcessNm", "미수취신고처리", RealGridColumnType.BASIC, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("sellerItemCd", "sellerItemCd", "업체상품코드", RealGridColumnType.BASIC, 100, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("personalOverseaNo", "personalOverseaNo", "개인통관고유번호", RealGridColumnType.BASIC, 120, true, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("userNo", "userNo", "회원번호", RealGridColumnType.BASIC, 120, false, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("noRcvProcessType", "noRcvProcessType", "미수취처리유형", RealGridColumnType.BASIC, 120, false, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("completeAfter3MonthYn", "completeAfter3MonthYn", "배송완료3개월 경과 여부", RealGridColumnType.BASIC, 120, false, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("siteType", "siteType", "사이트유형", RealGridColumnType.BASIC, 120, false, true));
        SHIP_STATUS_GRID_HEAD.add(new RealGridColumn("claimYn", "claimYn", "클레임가능여부", RealGridColumnType.BASIC, 120, false, true));

        // 선택발송처리 팝업 그리드
        SHIP_ALL_POP_GRID_HEAD.add(new RealGridColumn("orderItemNo", "orderItemNo", "상품주문번호", RealGridColumnType.BASIC, 90, true, true));
        SHIP_ALL_POP_GRID_HEAD.add(new RealGridColumn("bundleNo", "bundleNo", "배송번호", RealGridColumnType.BASIC, 90, true, true));
        SHIP_ALL_POP_GRID_HEAD.add(new RealGridColumn("itemNm1", "itemNm1", "상품명", RealGridColumnType.BASIC, 180, true, true));

        // 미수취신고 철회요청 팝업 그리드
        NO_RECEIVE_POP_GRID_HEAD.add(new RealGridColumn("bundleNo", "bundleNo", "배송번호", RealGridColumnType.BASIC, 90, true, true));
        NO_RECEIVE_POP_GRID_HEAD.add(new RealGridColumn("orderItemNo", "orderItemNo", "상품주문번호", RealGridColumnType.BASIC, 90, true, true));
        NO_RECEIVE_POP_GRID_HEAD.add(new RealGridColumn("itemNm1", "itemNm1", "상품명", RealGridColumnType.BASIC, 180, true, true));

        // 배송완료 팝업 그리드
        SHIP_COMPLETE_POP_GRID_HEAD.add(new RealGridColumn("shipNo", "shipNo", "배송SEQ", RealGridColumnType.BASIC, 80, false, false));
        SHIP_COMPLETE_POP_GRID_HEAD.add(new RealGridColumn("bundleNo", "bundleNo", "배송번호", RealGridColumnType.BASIC, 90, true, true));
        SHIP_COMPLETE_POP_GRID_HEAD.add(new RealGridColumn("orderItemNo", "orderItemNo", "상품주문번호", RealGridColumnType.BASIC, 90, true, true));
        SHIP_COMPLETE_POP_GRID_HEAD.add(new RealGridColumn("receiverNm", "receiverNm", "수령인", RealGridColumnType.BASIC, 90, true, true));
        SHIP_COMPLETE_POP_GRID_HEAD.add(new RealGridColumn("itemNm1", "itemNm1", "상품명", RealGridColumnType.BASIC, 180, true, true));
    }

    /**
     * 배송현황 Main Page
     */
    @GetMapping("/shipStatusMain")
    public String shipStatusMain(Model model, @RequestParam(value = "schType", required = false, defaultValue = "") String schType) throws Exception {
        model.addAttribute("parnterId", sellCommonService.getUserCd());
        model.addAttribute("shipStatus", sellCommonService.getCode("ship_status"));
        model.addAttribute("dlvCd", sellCommonService.getCode("dlv_cd"));
        model.addAttribute("shipMethod", sellCommonService.getCode("ship_method"));
        model.addAttribute("shipAllPopTitle", "배송방법/송장수정");
        model.addAttribute("theclubFrontUrl", theclubFrontUrl);
        model.addAttribute("homeplusFrontUrl", homeplusFrontUrl);
        model.addAttribute("claimReasonType", sellCommonService.getCode("claim_reason_type"));
        model.addAttribute("schType", schType);

        model.addAttribute("shipAllPopGridBaseInfo", new RealGridBaseInfo("shipAllPopGridBaseInfo", SHIP_ALL_POP_GRID_HEAD, SHIP_ALL_POP_GRID_OPTION).toString());
        model.addAttribute("noReceivePopGridBaseInfo", new RealGridBaseInfo("noReceivePopGridBaseInfo", NO_RECEIVE_POP_GRID_HEAD, NO_RECEIVE_POP_GRID_OPTION).toString());
        model.addAttribute("shipCompletePopGridBaseInfo", new RealGridBaseInfo("shipCompletePopGridBaseInfo", SHIP_COMPLETE_POP_GRID_HEAD, SHIP_COMPLETE_POP_GRID_OPTION).toString());
        model.addAttribute("shipStatusGridBaseInfo", new RealGridBaseInfo("shipStatusGridBaseInfo", SHIP_STATUS_GRID_HEAD, SHIP_STATUS_GRID_OPTION).toString());
        return "/sell/shipStatusMain";
    }

    /**
     * 배송현황 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getShipStatusList.json", method = RequestMethod.GET)
    public List<ShipStatusListGetDto> getShipStatusList(ShipStatusListSetDto shipStatusListSetDto) {
        return shipStatusService.getShipStatusList(shipStatusListSetDto);
    }

    /**
     * 배송현황 카운트 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getShipStatusCntList.json", method = RequestMethod.GET)
    public List<ShipStatusListGetDto> getShipStatusCntList(@RequestParam(value = "schType") String schType) {
        return shipStatusService.getShipStatusCntList(XssPreventer.unescape(schType));
    }

    /**
     * 배송현황 카운트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getShipStatusCnt.json", method = RequestMethod.GET)
    public ShipStatusCntGetDto getShipStatusCnt() {
        return shipStatusService.getShipStatusCnt();
    }

    /**
     * 미수취 신고 정보 조회
     */
    @ResponseBody
    @PostMapping("/getNoReceiveInfo.json")
    public ShipStatusNoReceiveInfoGetDto getNoReceiveInfo(@RequestBody ShipStatusNoReceiveInfoSetDto shipStatusNoReceiveInfoSetDto) throws Exception {
        return shipStatusService.getNoReceiveInfo(shipStatusNoReceiveInfoSetDto);
    }

    /**
     * 미수취 신고 철회 요청
     */
    @ResponseBody
    @PostMapping("/setNoReceive.json")
    public ResponseObject<String> setNoReceive(@RequestBody ShipStatusNoReceiveSetDto shipStatusNoReceiveSetDto) throws Exception {
        return shipStatusService.setNoReceive(shipStatusNoReceiveSetDto);
    }

    /**
     * 배송완료 팝업 리스트 조회
     */
    @ResponseBody
    @PostMapping("/getShipCompletePopList.json")
    public List<ShipStatusShipCompletePopListGetDto> getShipCompletePopList(@RequestBody ShipStatusShipCompleteSetDto shipStatusShipCompleteSetDto) {
        return shipStatusService.getShipCompletePopList(shipStatusShipCompleteSetDto);
    }

    /**
     * 배송완료
     */
    @ResponseBody
    @PostMapping(value = "/setShipComplete.json")
    public ResponseObject<Object> setShipComplete(@RequestBody ShipStatusShipCompleteSetDto shipStatusShipCompleteSetDto) throws Exception {
        return shipStatusService.setShipComplete(shipStatusShipCompleteSetDto);
    }

    /**
     * 배송 히스토리 조회
     */
    @ResponseBody
    @GetMapping("/getShipHistory.json")
    public List<ShipHistoryGetDto> getShipHistory(@RequestParam(value = "dlvCd") String dlvCd
                                                ,@RequestParam(value = "invoiceNo") String invoiceNo) {
        return shipStatusService.getShipHistory(dlvCd, invoiceNo);
    }
}
