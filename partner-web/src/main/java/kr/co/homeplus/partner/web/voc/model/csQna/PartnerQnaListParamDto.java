package kr.co.homeplus.partner.web.voc.model.csQna;

import lombok.Data;

@Data
public class PartnerQnaListParamDto {

    private String searchStartDt;

    private String searchEndDt;

    private String searchQnaStatus;

    private String searchQnaType;

    private String searchType;

    private String searchKeyword;

    private String searchPartnerId;

    private String searchArea;
}
