package kr.co.homeplus.partner.web.item.model.item;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

@ApiModel("이미지 정보")
@Getter
@Setter
public class ItemImgGetDto {
	@ApiModelProperty(value = "이미지 번호", position = 1)
	private Integer	imgNo;

	@ApiModelProperty(value = "대표이미지 여부", position = 2)
	private String	mainYn;

	@ApiModelProperty(value = "이미지 URL", required = true, position = 3)
	private String 	imgUrl;

	@ApiModelProperty(value = "이미지명", required = true, position = 4)
	private String 	imgNm;

	@ApiModelProperty(value = "이미지 수정여부", required = true, position = 5)
	private String 	changeYn;

	@ApiModelProperty(value = "이미지 유형 (MN: 메인, LST: 리스트)", required = true, position = 6)
	@Pattern(regexp = "(MN|LST)", message="이미지 유형")
	private String 	imgType;

	@ApiModelProperty(value = "이미지 유형명 (MN: 메인, LST: 리스트)", required = true, position = 7)
	private String 	imgTypeNm;

	@ApiModelProperty(value = "우선순위", hidden = true, position = 8)
	private String priority;
}
