package kr.co.homeplus.partner.web.core.dto.imageFile;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadFileDto {

    private String fileName;
    private String fileId;
    private String width;
    private String height;
    private String etag;

}
