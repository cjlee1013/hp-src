package kr.co.homeplus.partner.web.claim.model;


import io.swagger.annotations.ApiModelProperty;
import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimListSetDto {

    @ApiModelProperty(value = "처리상태 (전체:ALL/신청:C1/승인:C2/완료:C3/실패:P4/철회:C4/보류:C8/거부:C9)", position = 1, example = "C2")
    private String schClaimStatus;

    @ApiModelProperty(value = "클레임 검색기간 (신청일:REQUEST/완료일:COMPLETE/주문일:ORDER/보류일:PENDING)", position = 2, example = "COMPLETE")
    private String claimDateType;

    @ApiModelProperty(value = "조회시작일", position = 3, example = "2020-12-29")
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", position = 4, example = "2020-12-29")
    private String schEndDt;

    @ApiModelProperty(value = "검색어 (클레임번호:CLAIMBUNDLENO/전화번호:MOBILENO/주문번호:PURCHASEORDERNO/배송번호:BUNDLENO/상품주문번호:ORDERITEMNO/상품번호:ITEMNO/클레임그룹번호:CLAIMNO)", position = 5, example = "CLAIMBUNDLENO")
    private String claimSearchType;

    @ApiModelProperty(value = "검색어", position = 6, example = "3000000012")
    private String schClaimSearch;

    @ApiModelProperty(value = "신청자", position = 7, example = "PARTNER")
    private String schClaimChannel;

    @ApiModelProperty(value = "신청자", position = 8, example = "partnerId")
    private String partnerId;

    @ApiModelProperty(value = "보드 건 수 클레임 상태", position = 9, example = "REQUEST")
    private String schStatus;

    @ApiModelProperty(value = "수거 상태 (전체:ALL/수거대기:NN/수거요청완료:P0/수거예정:P1/수거중:P2/수거완료:P3/수거실패:P8)", position = 10, example = "P1")
    private String schPickupStatus;

    @ApiModelProperty(value = "배송 상태 (전체:ALL/상품출고대기:D0/상품출고:D1/배송중:D2/배송완료:D3)", position = 11, example = "D1")
    private String schExchStatus;

    @ApiModelProperty(value = "클레임타입", position = 12, example = "C")
    private String claimType;

}
