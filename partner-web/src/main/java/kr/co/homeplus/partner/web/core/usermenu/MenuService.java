package kr.co.homeplus.partner.web.core.usermenu;

import kr.co.homeplus.partner.web.core.usermenu.model.Menus;

public interface MenuService {

    Menus loadMenusFromJson();

    void makeMenus();

    Menus getMenus();
}
