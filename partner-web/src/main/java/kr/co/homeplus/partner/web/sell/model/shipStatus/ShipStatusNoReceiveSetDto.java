package kr.co.homeplus.partner.web.sell.model.shipStatus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "판매관리 > 배송현황 미수취 신고 철회 요청 DTO")
public class ShipStatusNoReceiveSetDto {

    @ApiModelProperty(value= "구매주문번호", position = 1)
    @NotNull(message = "구매주문번호")
    @Min(value = 1, message = "구매주문번호")
    private long purchaseOrderNo;

    @ApiModelProperty(value= "주문상품번호", position = 2)
    @NotNull(message = "주문상품번호")
    @Min(value = 1, message = "주문상품번호")
    private long orderItemNo;

    @ApiModelProperty(value= "운송번호", position = 3)
    @NotNull(message = "운송번호")
    @Min(value = 1, message = "운송번호")
    private long shipNo;

    @ApiModelProperty(value= "배송번호", position = 4)
    @NotNull(message = "배송번호")
    @Min(value = 1, message = "배송번호")
    private long bundleNo;

    @ApiModelProperty(value= "미수취처리유형(C : 미수취신고철회요청, W : 미수취신고철회)", position = 5)
    @Pattern(regexp = "W|C", message = "요청타입")
    @NotNull(message = "미수취처리유형")
    private String noRcvProcessType;

    @ApiModelProperty(value= "처리내용", position = 6)
    private String noRcvProcessCntnt;

    @ApiModelProperty(value= "미수취 수정자(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  admin : 사번으로 인식하여 사원정보 노출)", position = 9)
    @NotNull(message = "미수취 수정자")
    @NotEmpty(message = "미수취 수정자")
    private String chgId;

}
