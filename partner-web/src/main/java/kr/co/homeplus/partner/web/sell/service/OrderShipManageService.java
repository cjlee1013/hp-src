package kr.co.homeplus.partner.web.sell.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.utility.SetParameter;
import kr.co.homeplus.partner.web.core.utility.StringUtil;
import kr.co.homeplus.partner.web.sell.model.OrderDetailGetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.ClaimOrderDetailInfoGetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageCntGetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageConfirmOrderSetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageListGetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageListSetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageNotiDelaySetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageShipAddrInfoGetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageShipAddrInfoSetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageShipAllExcelSetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageShipAllPopListGetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageShipAllPopListSetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageShipAllSetDto;
import kr.co.homeplus.partner.web.sell.utils.ShippigUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderShipManageService {

  private final ResourceClient resourceClient;
  private final ExcelUploadService excelUploadService;
  private final SellCommonService sellCommonService;

  /**
   * 발주/발송관리 리스트 조회
   */
  public List<OrderShipManageListGetDto> getOrderShipManageList(OrderShipManageListSetDto orderShipManageListSetDto) {
    orderShipManageListSetDto.setPartnerId(sellCommonService.getUserCd());

    return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            orderShipManageListSetDto,
            "/partner/sell/getOrderShipManageList",
            new ParameterizedTypeReference<ResponseObject<List<OrderShipManageListGetDto>>>(){}).getData();
  }

  /**
   * 발주/발송관리 카운트 리스트 조회
   */
  public List<OrderShipManageListGetDto> getOrderShipManageCntList(String schType) {
    String partnerId = sellCommonService.getUserCd();

    return resourceClient.getForResponseObject(
        ResourceRouteName.SHIPPING,
        "/partner/sell/getOrderShipManageCntList?schType="+schType+"&partnerId="+partnerId,
        new ParameterizedTypeReference<ResponseObject<List<OrderShipManageListGetDto>>>() {}).getData();
  }

  /**
   * 발주/발송관리 카운트 조회
   */
  public OrderShipManageCntGetDto getOrderShipManageCnt() {
    String partnerId = sellCommonService.getUserCd();

    return resourceClient.getForResponseObject(
        ResourceRouteName.SHIPPING,
        "/partner/sell/getOrderShipManageCnt?partnerId="+partnerId,
        new ParameterizedTypeReference<ResponseObject<OrderShipManageCntGetDto>>() {}).getData();
  }

  /**
   * 주문확인
   */
  public ResponseObject<Object> setConfirmOrder(OrderShipManageConfirmOrderSetDto orderShipManageConfirmOrderSetDto) throws Exception {
    orderShipManageConfirmOrderSetDto.setChgId(sellCommonService.getUserCd());

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        orderShipManageConfirmOrderSetDto,
        "/partner/sell/setConfirmOrder",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

  /**
   * 발송지연안내
   */
  public ResponseObject<Object> setNotiDelay(OrderShipManageNotiDelaySetDto orderShipManageNotiDelaySetDto) throws Exception {
    orderShipManageNotiDelaySetDto.setChgId(sellCommonService.getUserCd());

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        orderShipManageNotiDelaySetDto,
        "/partner/sell/setNotiDelay",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

  /**
   * 일괄발송처리
   */
  public ResponseObject<Map<String, Object>> setShipAllExcel(MultipartHttpServletRequest multipartHttpServletRequest) throws Exception {
    List<OrderShipManageShipAllExcelSetDto> orderShipManageShipAllExcelSetDtoList = new ArrayList<>();
    ResponseObject<Map<String, Object>> responseObject = new ResponseObject<>();
    Map<String, Object> dataMap = new HashMap<>();

    // 엑셀 파일에서 데이터 추출
    this.setShipAllDataFromExcel(multipartHttpServletRequest, orderShipManageShipAllExcelSetDtoList, dataMap);

    // 엑셀 추출 중 에러
    if ("-1".equals(dataMap.get("excelSuccessCnt").toString())) {
      responseObject.setReturnCode("FAIL");
      responseObject.setReturnMessage("업로드한 양식을 다시 한 번 확인해 주세요");
      return responseObject;
    }

    // 검증 완료 데이터 없음, 종료
    if ("0".equals(dataMap.get("excelSuccessCnt").toString())) {
      dataMap.put("failList", orderShipManageShipAllExcelSetDtoList);
      dataMap.put("successCnt", "0");

      responseObject.setReturnCode("SUCCESS");
      responseObject.setData(dataMap);

      return responseObject;
    }

    // 등록ID SET
    orderShipManageShipAllExcelSetDtoList.forEach(x -> x.setChgId(sellCommonService.getUserCd()));

    // 엑셀 데이터 업데이트
    responseObject = resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        orderShipManageShipAllExcelSetDtoList,
        "/partner/sell/setShipAllExcel",
        new ParameterizedTypeReference<ResponseObject<Map<String, Object>>>(){});

    // API 처리 에러
    if (!"SUCCESS".equals(responseObject.getReturnCode())) {
      return responseObject;
    }

    // 실패 리스트 추출, 엑셀 export용
    ObjectMapper mapper = new ObjectMapper();
    orderShipManageShipAllExcelSetDtoList = mapper.convertValue(responseObject.getData().get("resultList"), new TypeReference<List<OrderShipManageShipAllExcelSetDto>>() {})
        .stream()
        .filter(x -> ObjectUtils.isNotEmpty(x.getFailMsg()))
        .collect(Collectors.toList());

    // RETURN DATA
    dataMap.put("failList", orderShipManageShipAllExcelSetDtoList);
    dataMap.put("successCnt", responseObject.getData().get("successCnt"));
    responseObject.setData(dataMap);

    return responseObject;
  }

  /**
   * 선택발송처리 팝업 리스트 조회
   */
  public List<OrderShipManageShipAllPopListGetDto> getShipAllPopList(OrderShipManageShipAllPopListSetDto orderShipManageShipAllPopListSetDto) {
    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        orderShipManageShipAllPopListSetDto,
        "/partner/sell/getShipAllPopList",
        new ParameterizedTypeReference<ResponseObject<List<OrderShipManageShipAllPopListGetDto>>>(){}).getData();
  }

  /**
   * 선택발송처리
   */
  public ResponseObject<Object> setShipAll(OrderShipManageShipAllSetDto orderShipManageShipAllSetDto) throws Exception {
    orderShipManageShipAllSetDto.setChgId(sellCommonService.getUserCd());

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        orderShipManageShipAllSetDto,
        "/partner/sell/setShipAll",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

  /**
   * 일괄발송처리 엑셀 업로드
   */
  private void setShipAllDataFromExcel(MultipartHttpServletRequest multipartHttpServletRequest
                                      , List<OrderShipManageShipAllExcelSetDto> orderShipManageShipAllExcelSetDtoList
                                      , Map<String, Object> dataMap) {
    try {
      // 파일을 읽기위해 input type file 태그 name 작성
      MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");

      // 파라미터 추출
      String shipMethod = multipartHttpServletRequest.getParameter("shipMethod");
      String dlvCd = multipartHttpServletRequest.getParameter("dlvCd");

      ExcelHeaders headers;

      if (ShippigUtil.isDlv(shipMethod)) {
        headers = new ExcelHeaders.Builder()
            .header(0, "배송번호", "bundleNo", true)
            .header(1, "송장번호", "invoiceNo", true)
            .build();
      } else if(ShippigUtil.isDrct(shipMethod)){
        headers = new ExcelHeaders.Builder()
            .header(0, "배송번호", "bundleNo", true)
            .header(1, "배송예정일", "scheduleShipDt", true)
            .build();
      } else {
        throw new Exception("배송방법 에러 : " + shipMethod);
      }

      // 엑셀 메타정보 입력
      ExcelUploadOption<OrderShipManageShipAllExcelSetDto> excelUploadOption = new ExcelUploadOption.Builder<>(
          OrderShipManageShipAllExcelSetDto.class, headers, 1, 0).build();

      // 엑셀 조회. multipartFile과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력.
      List<OrderShipManageShipAllExcelSetDto> result = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
      if (result.isEmpty()) {
        dataMap.put("excelTotalCnt", -1);
        dataMap.put("excelSuccessCnt", -1);
        dataMap.put("excelErrorCnt", -1);

        return;
      }

      // 데이터 검증 결과 data를 array에 담기
      int totalCnt = result.size();
      int successCnt = 0;
      int errorCnt = 0;

      for (OrderShipManageShipAllExcelSetDto orderShipManageShipAllExcelSetDto : result) {
        if (this.validExcel(orderShipManageShipAllExcelSetDto, shipMethod, dlvCd)) {
          successCnt++;
        } else {
          errorCnt++;
        }

        orderShipManageShipAllExcelSetDtoList.add(orderShipManageShipAllExcelSetDto);
      }

      dataMap.put("excelTotalCnt", totalCnt);
      dataMap.put("excelSuccessCnt", successCnt);
      dataMap.put("excelErrorCnt", errorCnt);
    } catch (Exception e) {
      log.error("일괄발송처리 엑셀 데이터 추출 에러 : {}", ExceptionUtils.getStackTrace(e));
      dataMap.put("excelTotalCnt", -1);
      dataMap.put("excelSuccessCnt", -1);
      dataMap.put("excelErrorCnt", -1);
    }
  }

  /**
   * 일괄발송처리 데이터 검증
   */
  private boolean validExcel(OrderShipManageShipAllExcelSetDto orderShipManageShipAllExcelSetDto, String shipMethod, String dlvCd) {

    /* 공통 */
    String bundleNo = orderShipManageShipAllExcelSetDto.getBundleNo();

    // 배송방법 SET
    orderShipManageShipAllExcelSetDto.setShipMethod(shipMethod);

    // 배송번호 - NULL, 공백, 숫자
    if (!StringUtils.isNumeric(bundleNo)) {
      orderShipManageShipAllExcelSetDto.setFailMsg("배송번호를 확인하세요");
      return false;
    }

    /* 택배배송 */
    if (ShippigUtil.isDlv(shipMethod)) {
      String invoiceNo = orderShipManageShipAllExcelSetDto.getInvoiceNo().trim();

      // 송장번호 - NULL, 공백, 30자이상, 영문+숫자조합
      if (StringUtils.isEmpty(invoiceNo)
          || ShippigUtil.spaceCheck(invoiceNo)
          || invoiceNo.length() > 30
          || !invoiceNo.matches("^[a-zA-Z0-9]*$") ) {
        orderShipManageShipAllExcelSetDto.setFailMsg("송장번호를 확인하세요");
        return false;
      }

      // 택배사 - NULL
      if (StringUtils.isEmpty(dlvCd)) {
        orderShipManageShipAllExcelSetDto.setFailMsg("택배사를 확인하세요");
        return false;
      }

      // 검증된 데이터 SET
      orderShipManageShipAllExcelSetDto.setDlvCd(dlvCd);
      orderShipManageShipAllExcelSetDto.setInvoiceNo(invoiceNo);
    }

    /* 직접배송 */
    if (ShippigUtil.isDrct(shipMethod)) {

      String scheduleShipDt = orderShipManageShipAllExcelSetDto.getScheduleShipDt();
      String scheduleShipDtRestFormat = "";

      // 엑셀 추출시 현재 공통모듈에서 yyyy-MM-dd HH:mm:ss 로 포맷 변경하고 있어 yyyy-mm-dd 까지 분리한 후 확인
      // 확인 후 형식이 잘못 됐으면 다시 합쳐놓는다. (처리 결과 엑셀 추출 시 원본데이터 유지해서 제공)
      if (scheduleShipDt.length() > 10) {
        scheduleShipDtRestFormat = scheduleShipDt.substring(10);
        scheduleShipDt = scheduleShipDt.substring(0,10);
      }

      // 배송예정일 - NULL, 공백, yyyy-mm-dd 형식
      if (StringUtils.isEmpty(scheduleShipDt)
          || ShippigUtil.spaceCheck(scheduleShipDt)
          || !scheduleShipDt.matches("\\d{4}-\\d{2}-\\d{2}")) {

        orderShipManageShipAllExcelSetDto.setScheduleShipDt(scheduleShipDt + scheduleShipDtRestFormat);
        orderShipManageShipAllExcelSetDto.setFailMsg("배송예정일을 확인하세요");
        return false;
      }

      // 검증된 데이터 SET
      orderShipManageShipAllExcelSetDto.setScheduleShipDt(scheduleShipDt);
    }

    return true;
  }

  /**
   * 배송지 수정 팝업 정보 조회
   */
  public List<OrderShipManageShipAddrInfoGetDto> getOrderShipAddrInfo(String bundleNo) {
    return resourceClient.getForResponseObject(
        ResourceRouteName.ESCROWMNG,
        "/order/orderSearch/detail/pop/shipAddr/"+ bundleNo,
        new ParameterizedTypeReference<ResponseObject<List<OrderShipManageShipAddrInfoGetDto>>>() {}).getData();
  }

  /**
   * 배송지 수정 팝업 정보 저장
   */
  public ResponseObject<String> setOrderShipAddrInfo(OrderShipManageShipAddrInfoSetDto orderShipManageShipAddrInfoSetDto) throws Exception {
    return resourceClient.postForResponseObject(
        ResourceRouteName.ESCROWMNG,
        orderShipManageShipAddrInfoSetDto,
        "/order/orderSearch/ship/orderShipChange",
        new ParameterizedTypeReference<ResponseObject<String>>(){});
  }

  /**
   * 상품 상세정보 팝업 조회
   */
  public ResponseObject<OrderDetailGetDto> getOrderDetailPop(String bundleNo) {
    String partnerId = sellCommonService.getUserCd();

    return resourceClient.getForResponseObject(
        ResourceRouteName.SHIPPING,
        "/partner/sell/getOrderDetailPop?bundleNo="+bundleNo+"&partnerId="+partnerId,
        new ParameterizedTypeReference<ResponseObject<OrderDetailGetDto>>() {});
  }

    /**
   * 상품 상세정보 팝업 조회
   */
  public List<ClaimOrderDetailInfoGetDto> getOrderClaimListPop(long purchaseOrderNo,long bundleNo, String claimType) {

        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("purchaseOrderNo", purchaseOrderNo));
        setParameterList.add(SetParameter.create("bundleNo", bundleNo));
        setParameterList.add(SetParameter.create("claimType", claimType));

        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            null,
            "/claim/orderCancel" + StringUtil.getParameter(setParameterList),
            new ParameterizedTypeReference<ResponseObject<List<ClaimOrderDetailInfoGetDto>>>() {}
        ).getData();
  }

}