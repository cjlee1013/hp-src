package kr.co.homeplus.partner.web.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageShipAllSetDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임 상태 변경 요청 파라미터")
public class ClaimPendingSetDto {

    @NotNull(message = "클레임번호")
    @ApiModelProperty(value = "클레임그룹번호", position = 1, example = "1500000011")
    private long claimNo;

    @NotNull(message = "클레임번들번호")
    @ApiModelProperty(value = "클레임번호", position = 2, example = "1500000001")
    private long claimBundleNo;

    @ApiModelProperty(value = "클레임사유코드", position = 3, example = "P001")
    private String reasonType;

    @ApiModelProperty(value = "클레임상세사유", position = 4, example = "보류상세사유")
    private String reasonTypeDetail;

    @ApiModelProperty(value = "클레임타입", position = 5, example = "X")
    private String claimType;
}
