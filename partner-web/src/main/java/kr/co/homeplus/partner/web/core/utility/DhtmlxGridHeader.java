package kr.co.homeplus.partner.web.core.utility;

import lombok.Data;

/**
 * DHTMLX Grid Header
 */
@Data
public class DhtmlxGridHeader {
	private String value;                       // 컬럼 타이틀
	private String colName;                     // 컬럼명
	private String width        = "100";        // 가로 사이즈
	private String type         = "txt";        // 타입
	private String align        = "left";       // 좌우 정렬
	private String sort         = "str";        // 정렬
	private boolean hidden      = false;        // hidden 여부
	private boolean decimal      = false;       // 소수점 여부

	public DhtmlxGridHeader(String value, String colName, String width, String type, String align, String sort) {
		this.value      = value;
		this.colName    = colName;
		this.width      = width;
		this.type       = type;
		this.align      = align;
		this.sort       = this.getSortType(type, sort);
	}

	public DhtmlxGridHeader(String value, String colName, String width, String type, String align, String sort, boolean hidden) {
		this.value      = value;
		this.colName    = colName;
		this.width      = width;
		this.type       = type;
		this.align      = align;
		this.sort       = this.getSortType(type, sort);
		this.hidden     = hidden;
	}

	public DhtmlxGridHeader(String value, String colName, String width, String type, String align, String sort, boolean hidden, boolean decimal) {
		this.value      = value;
		this.colName    = colName;
		this.width      = width;
		this.type       = type;
		this.align      = align;
        this.sort       = this.getSortType(type, sort);
		this.hidden     = hidden;
		this.decimal     = decimal;
	}

    //ron, dyn 타입이면 int로 강제 sort 지정
	private String getSortType(String type, String sort) {
        String sortType = sort;
        if(type.equals("ron") || type.equals("dyn")) {
            sortType = "int";
        }

        return sortType;
    }
}
