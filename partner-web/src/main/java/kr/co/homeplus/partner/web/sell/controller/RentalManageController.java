package kr.co.homeplus.partner.web.sell.controller;

import com.nhncorp.lucy.security.xss.XssPreventer;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridColumn;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.partner.web.sell.model.rentalManage.RentalManageCntGetDto;
import kr.co.homeplus.partner.web.sell.model.rentalManage.RentalManageCounselCompleteSetDto;
import kr.co.homeplus.partner.web.sell.model.rentalManage.RentalManageCounselReservationSetDto;
import kr.co.homeplus.partner.web.sell.model.rentalManage.RentalManageListGetDto;
import kr.co.homeplus.partner.web.sell.model.rentalManage.RentalManageListSetDto;
import kr.co.homeplus.partner.web.sell.service.RentalManageService;
import kr.co.homeplus.partner.web.sell.service.SellCommonService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 판매관리 > 렌탈판매관리
 */
@Controller
@RequestMapping("/sell/rentalManage")
public class RentalManageController {

    @Value("${front.homeplus.url}")
    private String homeplusFrontUrl;

    @Value("${front.theclub.url}")
    private String theclubFrontUrl;

    @Autowired
    private RentalManageService rentalManageService;
    @Autowired
    private SellCommonService sellCommonService;

    // 렌탈판매관리 그리드
    private static final List<RealGridColumn> RENTAL_MANAGE_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption RENTAL_MANAGE_GRID_OPTION = new RealGridOption("fill", false, true, false);

    public RentalManageController() {
        // 렌탈판매관리 그리드
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("orderItemNo", "orderItemNo", "상품주문번호", RealGridColumnType.BASIC, 100, true, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("orderDt", "orderDt", "주문일시", RealGridColumnType.BASIC, 150, true, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("shipStatusNm", "shipStatusNm", "주문상태", RealGridColumnType.BASIC, 100, true, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("purchaseOrderNo", "purchaseOrderNo", "주문번호", RealGridColumnType.BASIC, 100, true, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("itemNo", "itemNo", "상품번호", RealGridColumnType.BASIC, 120, true, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("itemNm1", "itemNm1", "상품명", RealGridColumnType.NONE, 350, true, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("itemQty", "itemQty", "수량", RealGridColumnType.BASIC, 100, true, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("txtOptVal", "txtOptVal", "옵션", RealGridColumnType.NONE, 250, true, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("buyerNm", "buyerNm", "구매자", RealGridColumnType.BASIC, 80, true, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("buyerMobileNo", "buyerMobileNo", "구매자 연락처", RealGridColumnType.BASIC, 100, true, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("receiverNm", "receiverNm", "수령인", RealGridColumnType.BASIC, 80, true, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("shipMobileNo", "shipMobileNo", "수령인 연락처", RealGridColumnType.BASIC, 100, true, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("sellerItemCd", "sellerItemCd", "업체상품코드", RealGridColumnType.BASIC, 100, true, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("shipNo", "shipNo", "배송SEQ", RealGridColumnType.BASIC, 80, false, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("bundleNo", "bundleNo", "배송번호", RealGridColumnType.BASIC, 100, false, true));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("shipStatus", "shipStatus", "주문상태코드", RealGridColumnType.BASIC, 80, false, false));
        RENTAL_MANAGE_GRID_HEAD.add(new RealGridColumn("siteType", "siteType", "사이트유형", RealGridColumnType.BASIC, 80, false, false));
    }

    /**
     * 렌탈판매관리 Main Page
     */
    @GetMapping("/rentalManageMain")
    public String shipStatusMain(Model model) throws Exception {
        model.addAttribute("shipStatus", sellCommonService.getCode("ship_status"));
        model.addAttribute("theclubFrontUrl", theclubFrontUrl);
        model.addAttribute("homeplusFrontUrl", homeplusFrontUrl);
        model.addAttribute("rentalManageGridBaseInfo", new RealGridBaseInfo("rentalManageGridBaseInfo", RENTAL_MANAGE_GRID_HEAD, RENTAL_MANAGE_GRID_OPTION).toString());

        return "/sell/rentalManageMain";
    }

    /**
     * 렌탈판매관리 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getRentalManageList.json", method = RequestMethod.GET)
    public List<RentalManageListGetDto> getRentalManageList(RentalManageListSetDto rentalManageListSetDto) {
        return rentalManageService.getRentalManageList(rentalManageListSetDto);
    }

    /**
     * 렌탈판매관리 카운트 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getRentalManageCntList.json", method = RequestMethod.GET)
    public List<RentalManageListGetDto> getRentalManageCntList(@RequestParam(value = "schType") String schType) {
        return rentalManageService.getRentalManageCntList(XssPreventer.unescape(schType));
    }

    /**
     * 렌탈판매관리 카운트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getRentalManageCnt.json", method = RequestMethod.GET)
    public RentalManageCntGetDto getRentalManageCnt() {
        return rentalManageService.getRentalManageCnt();
    }

    /**
     * 상담예약
     */
    @ResponseBody
    @PostMapping(value = "/setCounselReservation.json")
    public ResponseObject<Object> setCounselReservation(@RequestBody RentalManageCounselReservationSetDto rentalManageCounselReservationSetDto) throws Exception {
        return rentalManageService.setCounselReservation(rentalManageCounselReservationSetDto);
    }

    /**
     * 상담완료
     */
    @ResponseBody
    @PostMapping(value = "/setCounselComplete.json")
    public ResponseObject<Object> setCounselComplete(@RequestBody RentalManageCounselCompleteSetDto rentalManageCounselCompleteSetDto) throws Exception {
        return rentalManageService.setCounselComplete(rentalManageCounselCompleteSetDto);
    }

}
