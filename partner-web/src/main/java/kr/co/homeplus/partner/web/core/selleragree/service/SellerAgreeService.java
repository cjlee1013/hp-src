package kr.co.homeplus.partner.web.core.selleragree.service;

import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.dto.UserInfo;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 파트너 동의팝업 서비스
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SellerAgreeService {

    private final ResourceClient resourceClient;
    private final CertificationService certificationService;

    public String getCheckAcceptYn(final String partnerId) {

        String acceptYn = "N";
        ResponseObject responseObject = resourceClient.getForResponseObject(ResourceRouteName.ITEM,
            "/po/partner/checkAcceptTermsSeller?partnerId=" + partnerId);

        if (ResponseObject.RETURN_CODE_SUCCESS.equals(responseObject.getReturnCode())) {
            boolean isAccept = (boolean) responseObject.getData();
            log.debug("accept : {}", isAccept);

            if (isAccept) {
                acceptYn = "Y";
            }

        }
        return acceptYn;
    }

    public ResponseObject setAccept() {

        UserInfo userInfo = certificationService.getLoginUerInfo();
        String partnerId = userInfo.getPartnerId();

        ResponseObject responseObject = resourceClient
            .postForResponseObject(ResourceRouteName.ITEM, null,
                "/po/partner/setAcceptTermsSeller?partnerId=" + partnerId);
        return responseObject;
    }
}
