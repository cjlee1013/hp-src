package kr.co.homeplus.partner.web.manage.model;

import lombok.Data;

@Data
public class PartnerHarmfulItemDetailSelectDto {

    private String instituteTxt;

    private String itemNm;

    private String nmManufacupso;

    private String nmProdtype;

    private String unitPack;

    private String barcode;

    private String prdValid;

    private String nmManufaccntr;

    private String dtMake;

    private String nmRpttype;

    private String nmInspttype;

    private String nmSalerupso;

    private String dtTake;

    private String nmTakemachi;

    private String nmRptrmachi;

    private String imgAttach01;

    private String imgAttach02;

    private String imgAttach03;

    private String imgAttach04;

    private String imgAttach05;

}