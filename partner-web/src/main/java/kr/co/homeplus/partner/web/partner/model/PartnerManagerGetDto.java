package kr.co.homeplus.partner.web.partner.model;

import lombok.Data;

@Data
// 판매자관리 > 담당자정보 Get Entry
public class PartnerManagerGetDto {

    // 담당자 시퀀스
    private String seq;

    // 담당자 분류코드
    private String mngCd;

    // 담당자 이름
    private String mngNm;

    // 담당자 휴대폰번호
    private String mngMobile;

    // 담당자 연락처", required = false)
    private String mngPhone;

    // 담당자 이메일
    private String mngEmail;
}
