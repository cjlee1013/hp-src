package kr.co.homeplus.partner.web.voc.model.itemReview;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품평 리스트 조회")
public class ItemReviewListParamDto {

    private String searchStartDt;
    private String searchEndDt;
    private String searchType;
    private String searchKeyword;
    private String searchPartnerId;

}
