package kr.co.homeplus.partner.web.common.service.impl;

import com.google.common.base.CaseFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.partner.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
public class CodeServiceImpl implements CodeService {

	final private ResourceClient resourceClient;

	public CodeServiceImpl(ResourceClient resourceClient) {
		this.resourceClient = resourceClient;
	}

	/**
	 * 공통코드 조회
	 *
	 * @param gmcCd
	 * @return
	 */
	public Map<String, List<MngCodeGetDto>> getCode(String... gmcCd) {
		return resourceClient.getForResponseObject(
				ResourceRouteName.ITEM,
				"/common/getCodeInfoList?typeCode=" + Arrays.stream(gmcCd).collect(Collectors.joining("&typeCode=")),
				new ParameterizedTypeReference<ResponseObject<Map<String, List<MngCodeGetDto>>>>() {}).getData();
	}

	/**
	 * 공통코드 조회 (Model 저장)
	 * @param model
	 * @param gmcCd
	 */
	public Map<String, List<MngCodeGetDto>> getCodeModel(Model model, String... gmcCd) {
		Map<String, List<MngCodeGetDto>> mngCodeGetDtoMap = resourceClient.getForResponseObject(
				ResourceRouteName.ITEM,
				"/common/getCodeInfoList?typeCode=" + Arrays.stream(gmcCd).collect(Collectors.joining("&typeCode=")),
				new ParameterizedTypeReference<ResponseObject<Map<String, List<MngCodeGetDto>>>>() {}).getData();

		mngCodeGetDtoMap.forEach((key, value) -> {
			model.addAttribute(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, key), value);
		});

		return mngCodeGetDtoMap;
	}
}
