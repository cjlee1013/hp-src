package kr.co.homeplus.partner.web.core.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PartnerCookieDto {
    private String cookieName;
    private String cookieValue;
    private int maxAge;
}
