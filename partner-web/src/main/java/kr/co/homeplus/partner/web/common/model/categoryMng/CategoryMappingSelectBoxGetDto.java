package kr.co.homeplus.partner.web.common.model.categoryMng;

import lombok.Data;

/**
 * 카테고리 Mapping SelectBox 조회 Entry
 */
@Data
public class CategoryMappingSelectBoxGetDto {

	// 1depth 코드
	private String division;

	// 2depth 코드
	private String groupNo;

	// 3depth 코드
	private String dept;

	// 4depth 코드
	private String classCd;

	// 5depth 코드
	private String subclass;

	// 카테고리 명
	private String cateNm;
}
