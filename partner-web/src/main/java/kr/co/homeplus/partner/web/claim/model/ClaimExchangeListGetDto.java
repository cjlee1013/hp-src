package kr.co.homeplus.partner.web.claim.model;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RealGridOptionInfo(checkBar = true)
public class ClaimExchangeListGetDto {


    @ApiModelProperty(value = "클레임번호", example = "1500000001")
    @RealGridColumnInfo(headText = "클레임번호", columnType = RealGridColumnType.SERIAL, width = 80)
    private String claimBundleNo;

    @ApiModelProperty(value = "클레임그룹번호", example = "1500000011")
    @RealGridColumnInfo(headText = "클레임그룹번호", hidden = true)
    private String claimNo;

    @ApiModelProperty(value = "클레임요청번호", example = "1500000011")
    @RealGridColumnInfo(headText = "클레임요청번호", hidden = true)
    private String claimReqNo;

    @ApiModelProperty(value = "주문번호", example = "3000004971")
    @RealGridColumnInfo(headText = "주문번호", columnType = RealGridColumnType.SERIAL, width = 80)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "배송번호", example = "3000000004")
    @RealGridColumnInfo(headText = "배송번호", columnType = RealGridColumnType.SERIAL, width = 80)
    private String bundleNo;

    @ApiModelProperty(value = "상품 주문번호", example = "3000012917")
    @RealGridColumnInfo(headText = "상품 주문번호", columnType = RealGridColumnType.SERIAL, width = 120)
    private String orderItemNo;

    @ApiModelProperty(value = "처리상태", example = "신청")
    @RealGridColumnInfo(headText = "처리상태", width = 60)
    private String claimStatus;

    @ApiModelProperty(value = "처리상태코드", example = "C1")
    @RealGridColumnInfo(headText = "처리상태코드", hidden = true)
    private String claimStatusCode;

    @ApiModelProperty(value = "클레임타입", example = "C")
    @RealGridColumnInfo(headText = "클레임타입", hidden = true)
    private String claimType;

    @ApiModelProperty(value = "신청일", example = "2020-12-23 13:20:54")
    @RealGridColumnInfo(headText = "신청일시", columnType = RealGridColumnType.DATETIME, width = 150)
    private String requestDt;

    @ApiModelProperty(value = "신청사유", example = "다른 상품으로 재주문")
    @RealGridColumnInfo(headText = "신청사유", width = 180)
    private String claimReasonType;

    @ApiModelProperty(value = "접수채널", example = "MYPAGE")
    @RealGridColumnInfo(headText = "접수채널", width = 80)
    private String requestId;

    @ApiModelProperty(value = "수거상태", example = "수거요청")
    @RealGridColumnInfo(headText = "수거상태", width = 80)
    private String pickStatus;

    @ApiModelProperty(value = "수거상태코드", example = "P1")
    @RealGridColumnInfo(headText = "수거상태코드", hidden = true)
    private String pickStatusCode;

    @ApiModelProperty(value = "수거완료일", example = "2020-12-23 13:20:54")
    @RealGridColumnInfo(headText = "수거완료일", columnType = RealGridColumnType.DATETIME, width = 150)
    private String pickP3Dt;

    @ApiModelProperty(value = "교환배송비", example = "3000")
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "교환배송비", columnType = RealGridColumnType.PRICE)
    private String returnShipAmt;

    @ApiModelProperty(value = "처리일", example = "2020-12-23 13:20:54")
    @RealGridColumnInfo(headText = "처리일", columnType = RealGridColumnType.DATETIME, width = 150)
    private String processDt;

    @ApiModelProperty(value = "승인채널", example = "PARTNER")
    @RealGridColumnInfo(headText = "승인채널", width = 80)
    private String approveId;

    @ApiModelProperty(value = "클레임비용결제 동봉/차감", example = "동봉")
    @RealGridColumnInfo(headText = "클레임비용결제", width = 80)
    private String claimShipFeeEnclose;

    @ApiModelProperty(value = "교환배송상태코드", example = "D1")
    @RealGridColumnInfo(headText = "교환배송상태코드", width = 80, hidden = true)
    private String exchStatusCode;

    @ApiModelProperty(value = "교환배송상태", example = "상품출고(송장등록)")
    @RealGridColumnInfo(headText = "교환배송상태", width = 80)
    private String exchStatus;

    @ApiModelProperty(value = "교환배송비주체", example = "구매자")
    @RealGridColumnInfo(headText = "교환배송비주체", width = 80)
    private String whoReason;

    @ApiModelProperty(value = "상품번호", example = "10000300000265")
    @RealGridColumnInfo(headText = "상품번호", columnType = RealGridColumnType.SERIAL, width = 120)
    private String itemNo;

    @ApiModelProperty(value = "상품명", example = "[상품] 테스트_사라_알라크림치즈")
    @RealGridColumnInfo(headText = "상품명", width = 320)
    private String itemName;

    @ApiModelProperty(value = "옵션명", example = "1번옵션 : 옵션입니다. | 2번옵션 : 옵션입니다.")
    @RealGridColumnInfo(headText = "옵션명", width = 320)
    private String optItemNm;

    @ApiModelProperty(value = "신청수량", example = "1")
    @RealGridColumnInfo(headText = "신청수량", columnType = RealGridColumnType.SERIAL, width = 60)
    private String claimItemQty;

    @ApiModelProperty(value = "상품금액", example = "2000")
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "상품금액", columnType = RealGridColumnType.PRICE)
    private String itemPrice;

    @ApiModelProperty(value = "총 상품금액", example = "480000")
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "총 상품금액", columnType = RealGridColumnType.PRICE)
    private String totItemPrice;

    @ApiModelProperty(value = "구매자", example = "홍길동")
    @RealGridColumnInfo(headText = "구매자", width = 80)
    private String buyerNm;

    @ApiModelProperty(value = "구매자연락처", example = "010-1111-1111")
    @RealGridColumnInfo(headText = "구매자연락처", width = 120)
    private String buyerMobileNo;

    @ApiModelProperty(value = "수령인", example = "홍길동")
    @RealGridColumnInfo(headText = "수령인", width = 80)
    private String receiverNm;

    @ApiModelProperty(value = "수령인연락처", example = "010-1111-1111")
    @RealGridColumnInfo(headText = "수령인연락처", width = 120)
    private String shipMobileNo;

    @ApiModelProperty(value = "배송비지급", example = "선불")
    @RealGridColumnInfo(headText = "배송비지급", width = 80)
    private String shipPriceType;

    @ApiModelProperty(value = "배송방법", example = "택배배송")
    @RealGridColumnInfo(headText = "배송방법", width = 80)
    private String shipMethod;

    @ApiModelProperty(value = "택배사", example = "CJ대한통운")
    @RealGridColumnInfo(headText = "택배사", width = 100)
    private String dlvCdNm;

    @ApiModelProperty(value = "택배사코드", example = "001")
    @RealGridColumnInfo(headText = "택배사코드", hidden = true)
    private String dlvCd;

    @ApiModelProperty(value = "송장번호", example = "12312312313")
    @RealGridColumnInfo(headText = "송장번호", width = 100)
    private String invoiceNo;

    @ApiModelProperty(value = "수거방법", example = "업체직배송")
    @RealGridColumnInfo(headText = "수거방법", width = 80)
    private String pickShipType;

    @ApiModelProperty(value = "수거택배사코드", example = "001")
    @RealGridColumnInfo(headText = "수거택배사코드",  hidden = true)
    private String pickDlvCd;

    @ApiModelProperty(value = "수거택배사", example = "한진택배")
    @RealGridColumnInfo(headText = "수거택배사", width = 100)
    private String pickDlvCdNm;

    @ApiModelProperty(value = "수거송장번호", example = "12312312313")
    @RealGridColumnInfo(headText = "수거송장번호", width = 100)
    private String pickInvoiceNo;

    @ApiModelProperty(value = "보류설정일", example = "2020-12-29 14:20:05")
    @RealGridColumnInfo(headText = "보류설정일", columnType = RealGridColumnType.DATETIME, width = 150)
    private String pendingDt;

    @ApiModelProperty(value = "거부설정일", example = "2020-12-29 14:20:05")
    @RealGridColumnInfo(headText = "거부설정일", columnType = RealGridColumnType.DATETIME, width = 150)
    private String rejectDt;

    @ApiModelProperty(value = "수거지 우편번호", example = "07297")
    @RealGridColumnInfo(headText = "수거지 우편번호", width = 80)
    private String pickZipCode;

    @ApiModelProperty(value = "수거지주소", example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    @RealGridColumnInfo(headText = "수거지주소", width = 300)
    private String pickAddr;

    @ApiModelProperty(value = "교환배송 방법", example = "택배배송")
    @RealGridColumnInfo(headText = "교환배송 방법", width = 80)
    private String exchShipType;

    @ApiModelProperty(value = "교환배송 택배사", example = "한진택배")
    @RealGridColumnInfo(headText = "교환배송 택배사", width = 100)
    private String exchDlvCdNm;

    @ApiModelProperty(value = "교환배송 택배사 코드", example = "001")
    @RealGridColumnInfo(headText = "교환배송 택배사 코드", hidden = true)
    private String exchDlvCd;

    @ApiModelProperty(value = "교환배송 송장번호", example = "123123123")
    @RealGridColumnInfo(headText = "교환배송 송장번호", width = 100)
    private String exchInvoiceNo;

    @ApiModelProperty(value = "교환배송 처리일", example = "2020-12-29 00:00:00")
    @RealGridColumnInfo(headText = "교환배송 처리일", columnType = RealGridColumnType.DATETIME, width = 150)
    private String exchD0Dt;

    @ApiModelProperty(value = "교환배송지 우편번호", example = "08584")
    @RealGridColumnInfo(headText = "교환배송지 우편번호", width = 80)
    private String exchZipcode;

    @ApiModelProperty(value = "교환배송지주소", example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    @RealGridColumnInfo(headText = "교환배송지주소", width = 300)
    private String exchAddr;

    @ApiModelProperty(value = "업체상품코드", example = "")
    @RealGridColumnInfo(headText = "업체상품코드", width = 80)
    private String sellerItemCd;

    @ApiModelProperty(value = "업체옵션코드", example = "")
    @RealGridColumnInfo(headText = "업체옵션코드", width = 80)
    private String sellerOptCd;

    @ApiModelProperty(value = "관리코드", example = "")
    @RealGridColumnInfo(headText = "관리코드", width = 80)
    private String sellerManageCd;


}
