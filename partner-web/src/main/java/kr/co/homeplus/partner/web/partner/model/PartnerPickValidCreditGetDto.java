package kr.co.homeplus.partner.web.partner.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * 업체관리 > 파트너관리 > 판매업체관리 > 판매업체 택배사 신용코드 유효성체크
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerPickValidCreditGetDto {
	private String success;
	private String dlvCd;
	private String creditCd;
	private String detail;
}
