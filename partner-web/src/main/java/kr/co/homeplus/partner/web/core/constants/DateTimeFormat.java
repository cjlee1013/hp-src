package kr.co.homeplus.partner.web.core.constants;

public class DateTimeFormat {
    public static final String TIME_ZONE_KST = "Asia/Seoul";
    public static final String DATE_FORMAT_DIGIT_YMD = "yyyyMMdd";
}
