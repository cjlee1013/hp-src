package kr.co.homeplus.partner.web.common.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.partner.web.common.model.categoryMng.CategoryGetDto;
import kr.co.homeplus.partner.web.common.model.categoryMng.CategoryMappingSelectBoxGetDto;
import kr.co.homeplus.partner.web.common.model.categoryMng.CategoryNmGetDto;
import kr.co.homeplus.partner.web.common.model.categoryMng.CategorySelectBoxGetDto;
import kr.co.homeplus.partner.web.common.model.categoryMng.CategorySelectDto;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.partner.web.core.annotation.CertificationNeedLess;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.utility.GridUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 카테고리 관리 시스템 > WEB
 */
@Slf4j
@Controller
public class CategoryMngController {

	final private ResourceClient resourceClient;
	final private CodeService codeService;

	public CategoryMngController(ResourceClient resourceClient,
			CodeService codeService) {
		this.resourceClient = resourceClient;
		this.codeService = codeService;
	}

	/**
	 * 카테고리관리 > 카테고리 등록/수정 > 페이지
	 *
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = { "/common/categoryMain"}, method = RequestMethod.GET)
	public String categoryMain(Model model) throws Exception {

		codeService.getCodeModel(model,
				"use_yn"				// 사용여부
				, 	"limit_yn"					// 제한여부
				, 	"category_disp_yn"			// 노출범위
				, 	"isbn_yn"					// ISBN 노출여부
				, 	"review_display"			// 리뷰노출여부
		);

		return "/common/categoryMain";
	}

	/**
	 * 카테고리관리 > 카테고리 등록/수정 > 카테고리 리스트
	 *
	 * @param categorySelectDto
	 * @return Object
	 */
	@ResponseBody @RequestMapping(value = {"/common/category/getCategory.json"}, method = RequestMethod.GET)
	public List<Object> getCategory(CategorySelectDto categorySelectDto) throws Exception {

		String apiUri = "/po/common/CategoryMng/getCategory";
		apiUri += "?lcateCd=" + categorySelectDto.getLcateCd();
		apiUri += "&mcateCd=" + categorySelectDto.getMcateCd();
		apiUri += "&scateCd=" + categorySelectDto.getScateCd();
		apiUri += "&dcateCd=" + categorySelectDto.getDcateCd();

		ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<Map<String, Object>>>>() {};

		ResponseObject<List<Map<String, Object>>> responseObject = resourceClient.getForResponseObject(
				ResourceRouteName.ITEM, apiUri, typeReference);
		List<Map<String, Object>> responseData = responseObject.getData();

		return GridUtil.convertTreeGridData(responseData, null, "cateCd", "parentCateCd", "depth");
	}

	/**
	 * 카테고리관리 > 카테고리 등록/수정 > 카테고리 등록/수정 selectBox
	 *
	 * @param depth
	 * @param parentCateCd
	 * @return Map<String, Object>
	 */
	@ResponseBody @RequestMapping(value = {"/common/category/getAllCategoryForSelectBox.json"}, method = RequestMethod.GET)
	public List<CategorySelectBoxGetDto> getAllCategoryForSelectBox(
			@RequestParam(name = "depth") String depth,
			@RequestParam(name = "parentCateCd") String parentCateCd
			) {

		String apiUri = "/po/common/CategoryMng/getCategoryForSelectBox";
		apiUri += "?depth=" + depth;
		apiUri += "&parentCateCd=" + parentCateCd;
		List<CategorySelectBoxGetDto> data = (List<CategorySelectBoxGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<CategorySelectBoxGetDto>>>() {
		}).getData();

		return data;
	}

	/**
	 * 카테고리관리 > 카테고리 등록/수정 > 카테고리 등록/수정 mapping selectBox
	 *
	 * @param depth
	 * @param division
	 * @param groupNo
	 * @param dept
	 * @param classCd
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"/common/category/getCategoryForMappingSelectBox.json"}, method = RequestMethod.GET)
	public List<CategoryMappingSelectBoxGetDto> getCategoryForMappingSelectBox(
				@RequestParam(name = "depth") String depth
			,	@RequestParam(required = false, defaultValue = "") String division
			,	@RequestParam(required = false, defaultValue = "") String groupNo
			,	@RequestParam(required = false, defaultValue = "") String dept
			,	@RequestParam(required = false, defaultValue = "") String classCd
	) {

		String apiUri = "/po/common/CategoryMng/getCategoryForMappingSelectBox";
		apiUri += "?depth=" + depth;
		apiUri += "&division=" + division;
		apiUri += "&groupNo=" + groupNo;
		apiUri += "&dept=" + dept;
		apiUri += "&classCd=" + classCd;

		List<CategoryMappingSelectBoxGetDto> data = (List<CategoryMappingSelectBoxGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<CategoryMappingSelectBoxGetDto>>>() {
		}).getData();

		return data;
	}

	/**
	 * 공통 카테고리 리스트 (selectbox)
	 *
	 * @param depth
	 * @param parentCateCd
	 * @return Map<String, Object>
	 */
	@CertificationNeedLess
	@ResponseBody
	@RequestMapping(value = {"/common/category/getCategoryForSelectBox.json"}, method = RequestMethod.GET)
	public List<Map<String, Object>> getCategoryForSelectBox(
			@RequestParam(name = "depth") String depth,
			@RequestParam(name = "parentCateCd") String parentCateCd
			) {

		String apiUri = "/po/common/CategoryMng/getCategoryForSelectBox";
		apiUri += "?depth=" + depth;
		apiUri += "&parentCateCd=" + parentCateCd;

		List<Map<String, Object>> data = (List<Map<String, Object>>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<Map<String, Object>>>>() {
		}).getData();

		return data;
	}

	/**
	 * 공통 카테고리 세분류 정보 조회
	 *
	 * @param dcateCd
	 * @return String
	 */
	@ResponseBody @RequestMapping(value = {"/common/category/getCategoryDivide.json"}, method = RequestMethod.GET)
	public CategoryGetDto getCategoryForSelectBox(
			@RequestParam(name = "dcateCd") String dcateCd) {

		String apiUri = "/po/common/CategoryMng/getCategoryDivide";
		apiUri += "?dcateCd=" + dcateCd;

		return (CategoryGetDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<CategoryGetDto>>() {}).getData();
	}

	/**
	 * 카테고리 뎁스별 상세조회
	 *
	 * @param depth
	 * @param cateCd
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = {"/common/category/getCategoryDetail.json"}, method = RequestMethod.GET)
	public CategoryGetDto getCategoryDetail(@RequestParam(value = "depth") String depth,
											@RequestParam(value = "cateCd") String cateCd) {
		return (CategoryGetDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM,
				"/po/common/CategoryMng/getCategoryDetail?depth=" + depth + "&cateCd=" + cateCd
				,new ParameterizedTypeReference<ResponseObject<CategoryGetDto>>() {}).getData();
	}

	/**
	 * 카테고리명 조회
	 * @param cateSchNm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"/common/category/getCategoryNm.json"}, method = RequestMethod.GET)
	public List<CategoryNmGetDto> getCategoryDetail(@RequestParam(value = "cateSchNm") String cateSchNm) {
		String apiUri = "/po/common/CategoryMng/getPartnerCategoryNm?cateSchNm=" +cateSchNm;
		return resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<CategoryNmGetDto>>>() {
		}).getData();

	}

}
