package kr.co.homeplus.partner.web.item.model.banner;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridButtonType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ItemBannerMatchListGetDto {

    @ApiModelProperty(value = "삭제", position = 1 )
    @RealGridColumnInfo(headText = "삭제", width = 60, columnType = RealGridColumnType.BUTTON, alwaysShowButton=true, buttonType= RealGridButtonType.ACTION)
    private String delBtn;

    @ApiModelProperty(value = "배너번호", position = 1)
    @RealGridColumnInfo(headText = "배너번호", hidden = true)
    private Long bannerNo;

    @ApiModelProperty(value = "상품번호", position = 1)
    @RealGridColumnInfo(headText = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "상품번호", position = 2)
    @RealGridColumnInfo(headText = "상품번호", width = 100)
    private String itemNm;

    @ApiModelProperty(value = "댜분류", position = 3)
    @RealGridColumnInfo(headText = "대분류", width = 100)
    private String lcateNm;

    @ApiModelProperty(value = "중분류", position = 4)
    @RealGridColumnInfo(headText = "중분류", width = 100)
    private String mcateNm;

    @ApiModelProperty(value = "소분류", position = 5)
    @RealGridColumnInfo(headText = "소분류", width = 100)
    private String scateNm;

    @ApiModelProperty(value = "세분류", position = 6)
    @RealGridColumnInfo(headText = "세분류", width = 100)
    private String dcateNm;

    @ApiModelProperty(value = "세분류", position = 7)
    @RealGridColumnInfo(headText = "세분류", hidden = true)
    private String dcateCd;

    @ApiModelProperty(value = "노출여부", position = 8)
    @RealGridColumnInfo(headText = "노출여부", hidden = true)
    private String useYn;

}
