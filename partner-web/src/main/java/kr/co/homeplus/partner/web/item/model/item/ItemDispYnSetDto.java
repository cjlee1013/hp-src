package kr.co.homeplus.partner.web.item.model.item;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 노출여부 수정
 */
@Getter
@Setter
public class ItemDispYnSetDto {

	//상품번호
	private List<String> itemNo;

	//노출여부
	private String dispYn;

	//판매자ID
	private String partnerId;

	//등록|수정자
	private String userId;
}
