package kr.co.homeplus.partner.web.core.usermenu.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import kr.co.homeplus.partner.web.core.usermenu.model.MainMenus.MainMenusBuilders;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder(builderClassName = "MainMenusBuilders", toBuilder = true)
@JsonDeserialize(builder = MainMenusBuilders.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MainMenus {
    private String mainMenuNm;
    @SerializedName("isMenuView")
    private boolean menuView;
    private List<SubMenus> subMenus;

    @JsonPOJOBuilder(withPrefix = "")
    public static class MainMenusBuilders {
    }
}
