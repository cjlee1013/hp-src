package kr.co.homeplus.partner.web.sell.model.shipStatus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "판매관리 > 배송현황 미수취 신고 정보 요청 DTO")
public class ShipStatusNoReceiveInfoSetDto {

    @ApiModelProperty(value= "구매주문번호", position = 1)
    @NotNull(message = "구매주문번호")
    @Min(value = 1, message = "구매주문번호")
    private long purchaseOrderNo;

    @ApiModelProperty(value= "주문상품번호", position = 2)
    @NotNull(message = "주문상품번호")
    @Min(value = 1, message = "주문상품번호")
    private long orderItemNo;

    @ApiModelProperty(value= "운송번호", position = 3)
    @NotNull(message = "운송번호")
    @Min(value = 1, message = "운송번호")
    private long shipNo;

    @ApiModelProperty(value= "배송번호", position = 4)
    @NotNull(message = "배송번호")
    @Min(value = 1, message = "배송번호")
    private long bundleNo;

    @ApiModelProperty(value= "고객번호", position = 5)
    @NotNull(message = "고객번호")
    @Min(value = 1, message = "고객번호")
    private long userNo;

}