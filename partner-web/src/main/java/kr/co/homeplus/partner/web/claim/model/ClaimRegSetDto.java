package kr.co.homeplus.partner.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.partner.web.claim.model.ClaimPreRefundSetDto.ClaimPreRefundItemList;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClaimRegSetDto {

    @NotNull(message = "주문번호가 없습니다.")
    @ApiModelProperty(value = "주문번호", position = 1, example = "3000004971")
    private String purchaseOrderNo;

    @NotNull(message = "배송번호가 없습니다.")
    @ApiModelProperty(value = "배송번호", position = 2, example = "3000000004")
    private String bundleNo;

    @ApiModelProperty(value = "상품번호", position = 3)
    private List<ClaimPreRefundItemList> claimItemList;

    @NotNull(message = "귀책사유가 없습니다.")
    @ApiModelProperty(value = "귀책사유(B:바이어(고객)/S:판매자/H:홈플러스)", position = 4, example = "S")
    @Pattern(regexp = "B|S|H", message = "귀책사유")
    private String whoReason;

    @NotNull(message = "취소타입이 없습니다.")
    @ApiModelProperty(value = "취소타입(O:주문취소/R:반품/X:교환)", position = 5, example = "O")
    @Pattern(regexp = "O|R", message = "취소타입")
    private String cancelType;

    @NotNull(message = "클레임타입이 없습니다.")
    @ApiModelProperty(value = "클레임타입(C:취소/R:반품/X:교환)", position = 6, example = "X")
    private String claimType;

    @NotNull(message = "클레임사유코드가 없습니다.")
    @ApiModelProperty(value = "클레임사유코드", position = 8, example = "C002")
    @Length(max = 4, message = "클레임사유코드")
    private String claimReasonType;

    @ApiModelProperty(value = "클레임사유상세", position = 9, example = "클레임 상세사유를 입력해주세요")
    private String claimReasonDetail;

    @NotNull(message = "등록아이디가 없습니다.")
    @ApiModelProperty(value = "등록아이디", position = 10, example = "PARTNER")
    private String regId;

    @ApiModelProperty(value = "부분취소 여부 Y:부분취소/N:전체취소", position = 11, example = "Y")
    private String claimPartYn;

    @ApiModelProperty(value = "주문취소 여부 Y:주문취소/N:클레임신청", position = 12, example = "N")
    private String orderCancelYn;

    @ApiModelProperty(value = "클레임상세데이터(교환/반품)", position = 13)
    private ClaimRegDetailDto claimDetail;

    @ApiModelProperty(value = "행사미차감 여부(Y:미차감,N:차감)", position = 14, example = "Y")
    private String piDeductPromoYn;

    @ApiModelProperty(value = "할인 미차감 여부(Y:미차감,N:차감)", position = 15, example = "Y")
    private String piDeductDiscountYn;

    @ApiModelProperty(value = "배송비 미차감 여부(Y:미차감,N:차감)", position = 16, example = "Y")
    private String piDeductShipYn;

}
