package kr.co.homeplus.partner.web.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임등록시 교환/반품 상세 정보.")
public class ClaimRegDetailDto {

    @ApiModelProperty(value = "동봉여부 Y:동봉/N:환불금차감", position = 1, example = "Y")
    private String isEnclose;

    @ApiModelProperty(value = "판매자발송여부 Y:발송/N:미발송", position = 2, example = "Y")
    private String isPick;

    @ApiModelProperty(value = "수거여부", position = 3, example = "Y")
    private String isPickReq;

    @ApiModelProperty(value = "수거일자", position = 4, example = "2020-12-29 00:00:00")
    private String shipDt;

    @ApiModelProperty(value = "수거ShiftId", position = 5, example = "42421")
    private String shiftId;

    @ApiModelProperty(value = "수거slotId", position = 6, example = "223124")
    private String slotId;

    @ApiModelProperty(value = "수거송장번호(구매자가직접보낼경우에만)", position = 7, example = "1231231223")
    private String pickInvoiceNo;

    @ApiModelProperty(value = "수거택배사코드(구매자가직접보낼경우에만)", position = 8, example = "002")
    private String pickDlvCd;

    @ApiModelProperty(value = "수거고객명", position = 9, example = "홍길동")
    private String pickReceiverNm;

    @ApiModelProperty(value = "수거우편번호", position = 10, example = "07297")
    private String pickZipCode;

    @ApiModelProperty(value = "수거도로명주소", position = 11, example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    private String pickRoadBaseAddr;

    @ApiModelProperty(value = "수거도로명상세주소", position = 12, example = "금천전 홈플러스 1층")
    private String pickRoadDetailAddr;

    @ApiModelProperty(value = "수거일반주소", position = 13, example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    private String pickBaseAddr;

    @ApiModelProperty(value = "수거일반상세주소", position = 14, example = "금천전 홈플러스 1층")
    private String pickBaseDetailAddr;

    @ApiModelProperty(value = "수거대상연락처", position = 15, example = "010-1111-1111")
    private String pickMobileNo;

    @ApiModelProperty(value = "교환수신자명", position = 10, example = "홍길동")
    private String exchReceiverNm;

    @ApiModelProperty(value = "교환우편번호", position = 10, example = "07297")
    private String exchZipCode;

    @ApiModelProperty(value = "교환도로명주소", position = 11, example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    private String exchRoadBaseAddr;

    @ApiModelProperty(value = "교환도로명상세주소", position = 12, example = "금천전 홈플러스 1층")
    private String exchRoadDetailAddr;

    @ApiModelProperty(value = "교환일반주소", position = 13, example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    private String exchBaseAddr;

    @ApiModelProperty(value = "교환일반상세주소", position = 14, example = "금천전 홈플러스 1층")
    private String exchBaseDetailAddr;

    @ApiModelProperty(value = "교환받는사람연락처", position = 15, example = "010-1111-1111")
    private String exchMobileNo;

    @ApiModelProperty(value = "사진첨부1", position = 16, example = "/cl/f103d615-8150-4a57-945d-c4ee4ba053d4")
    private String uploadFileName;

    @ApiModelProperty(value = "사진첨부2", position = 17, example = "/cl/f103d615-8150-4a57-945d-c4ee4ba053d4")
    private String uploadFileName2;

    @ApiModelProperty(value = "사진첨부3", position = 18, example = "/cl/f103d615-8150-4a57-945d-c4ee4ba053d4")
    private String uploadFileName3;

    @ApiModelProperty(value = "수거등록자아이디", position = 19, example = "PARTNER")
    private String pickRegId;

    @ApiModelProperty(value = "고객번호", position = 20, hidden = true, example = "21515155")
    private String userNo;

    @ApiModelProperty(value = "배송타입", position = 21, hidden = true, example = "DS")
    private String shipType;
}
