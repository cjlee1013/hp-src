package kr.co.homeplus.partner.web.item.model.item;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 판매기간 일괄 변경
 */
@Getter
@Setter
public class ItemSaleDateSetDto {

	//상품번호
	private List<String> itemNo;

	//판매기간 기준-설정함/설정안함(Y/N)
	private String salePeriodYn;

	//노출 시작일
	private String saleStartDt;

	//노출 종료일
	private String saleEndDt;

	//판매자ID
	private String partnerId;

	//등록|수정자
	private String userId;
}
