package kr.co.homeplus.partner.web.item.model.banner;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridButtonType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ItemBannerListGetDto {

    @ApiModelProperty(value = "수정")
    @RealGridColumnInfo(headText = "수정", width = 60, columnType = RealGridColumnType.BUTTON, alwaysShowButton=true, buttonType= RealGridButtonType.ACTION)
    private String editBtn;

    @ApiModelProperty(value = "배너번호")
    @RealGridColumnInfo(headText = "배너번호", hidden = true)
    private Long bannerNo;

    @ApiModelProperty(value = "제목")
    @RealGridColumnInfo(headText = "제목", width = 150, sortable = true)
    private String bannerNm;

    @ApiModelProperty(value = "구분")
    @RealGridColumnInfo(headText = "구분", hidden = true)
    private String dispType;

    @ApiModelProperty(value = "구분")
    @RealGridColumnInfo(headText = "구분", width = 100, sortable = true)
    private String dispTypeNm;

    @ApiModelProperty(value = "대분류")
    @RealGridColumnInfo(headText = "대분류", width = 100, sortable = true)
    private String lcateNm;

    @ApiModelProperty(value = "중분류")
    @RealGridColumnInfo(headText = "중분류", width = 100, sortable = true)
    private String mcateNm;

    @ApiModelProperty(value = "소분류")
    @RealGridColumnInfo(headText = "소분류", width = 100, sortable = true)
    private String scateNm;

    @ApiModelProperty(value = "세분류")
    @RealGridColumnInfo(headText = "세분류", width = 100, sortable = true)
    private String dcateNm;

    @ApiModelProperty(value = "세분류")
    @RealGridColumnInfo(headText = "세분류", hidden = true)
    private String dcateCd;

    @ApiModelProperty(value = "노출여부")
    @RealGridColumnInfo(headText = "노출여부", hidden = true)
    private String dispYn;

    @ApiModelProperty(value = "노출여부")
    @RealGridColumnInfo(headText = "노출여부", width = 100, sortable = true)
    private String dispYnNm;

    @ApiModelProperty(value = "노출시작일")
    @RealGridColumnInfo(headText = "노출시작일", width = 150, sortable = true)
    private String dispStartDt;

    @ApiModelProperty(value = "노출종료일")
    @RealGridColumnInfo(headText = "노출종료일", width = 150, sortable = true)
    private String dispEndDt;

    @ApiModelProperty(value = "등록일")
    @RealGridColumnInfo(headText = "등록일", width = 150, sortable = true)
    private String regDt;

    @ApiModelProperty(value = "등록자")
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regNm;

    @ApiModelProperty(value = "수정일")
    @RealGridColumnInfo(headText = "수정일", width = 150, sortable = true)
    private String chgDt;

    @ApiModelProperty(value = "수정자")
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgNm;


}
