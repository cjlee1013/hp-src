package kr.co.homeplus.partner.web.common.model.common;

import lombok.Data;


@Data
public class MobileOccupationValidParamDto {
	private String certNo;
	private String certToken;
	private String mobile;
}
