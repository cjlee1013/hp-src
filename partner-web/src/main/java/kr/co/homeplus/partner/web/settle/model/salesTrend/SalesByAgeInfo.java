package kr.co.homeplus.partner.web.settle.model.salesTrend;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("연령별 판매 비중")
@Data
public class SalesByAgeInfo {

    @ApiModelProperty("연령대")
    private String age;

    @ApiModelProperty("비율")
    private float rate;
}
