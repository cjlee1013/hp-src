package kr.co.homeplus.partner.web.voc.model.itemReview;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품평 상세 조회")
public class ItemReviewDetailGetDto {

    private Long reviewNo;
    private String itemNo;
    private String itemNm;
    private String userNm;
    private String regDt;
    private int grade;
    private String gradeStatusNm;
    private String gradeAccuracyNm;
    private String gradePriceNm;
    private String contents;
    private String imgList;

}
