package kr.co.homeplus.partner.web.core.exception;

import kr.co.homeplus.partner.web.core.dto.Step1LoginParam;

public class CertificationException extends RuntimeException {
    private final Step1LoginParam loginParam;

    public CertificationException(Step1LoginParam loginParam, String message) {
        super(message);
        this.loginParam = loginParam;
    }

    public Step1LoginParam getLoginParam() {
        return loginParam;
    }
}
