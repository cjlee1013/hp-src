package kr.co.homeplus.partner.web.common.model.codeMng;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MngCodeGetDto {
	private String gmcCd;
	private String mcCd;
	private String mcNm;
	int	priority;
	private String useYn;
	private String ref1;
	private String ref2;
	private String ref3;
	private String ref4;
	private String lgcCodeCd;
	private String lgcUpcodeCd;
}
