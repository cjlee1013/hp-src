package kr.co.homeplus.partner.web.claim.model;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RealGridOptionInfo(checkBar = true)
public class TicketRentalCancelListGetDto {

    @ApiModelProperty(value = "클레임번호")
    @RealGridColumnInfo(headText = "클레임번호", columnType = RealGridColumnType.SERIAL, width = 80)
    private String claimBundleNo;

    @ApiModelProperty(value = "클레임번호")
    @RealGridColumnInfo(headText = "클레임번호", hidden = true)
    private String claimNo;

    @ApiModelProperty(value = "클레임신청번호")
    @RealGridColumnInfo(headText = "클레임번호", hidden = true)
    private String claimReqNo;

    @ApiModelProperty(value = "주문번호")
    @RealGridColumnInfo(headText = "주문번호", columnType = RealGridColumnType.SERIAL, width = 80)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "배송번호")
    @RealGridColumnInfo(headText = "배송번호", columnType = RealGridColumnType.SERIAL, width = 80)
    private String bundleNo;

    @ApiModelProperty(value = "상품 주문번호")
    @RealGridColumnInfo(headText = "상품 주문번호", columnType = RealGridColumnType.SERIAL, width = 120)
    private String orderItemNo;

    @ApiModelProperty(value = "처리상태")
    @RealGridColumnInfo(headText = "처리상태", width = 60)
    private String claimStatus;

    @ApiModelProperty(value = "처리상태코드")
    @RealGridColumnInfo(headText = "처리상태코드", hidden = true)
    private String claimStatusCode;


    @ApiModelProperty(value = "신청일")
    @RealGridColumnInfo(headText = "신청일시", columnType = RealGridColumnType.DATETIME, width = 150)
    private String requestDt;

    @ApiModelProperty(value = "신청사유")
    @RealGridColumnInfo(headText = "신청사유", width = 180)
    private String claimReasonType;

    @ApiModelProperty(value = "접수채널")
    @RealGridColumnInfo(headText = "접수채널", width = 80)
    private String requestId;

    @ApiModelProperty(value = "처리일")
    @RealGridColumnInfo(headText = "처리일", columnType = RealGridColumnType.DATETIME, width = 150)
    private String processDt;

    @ApiModelProperty(value = "승인채널")
    @RealGridColumnInfo(headText = "승인채널", width = 80)
    private String approveId;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", columnType = RealGridColumnType.SERIAL, width = 120)
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", width = 180)
    private String itemName;

    @ApiModelProperty(value = "옵션명")
    @RealGridColumnInfo(headText = "옵션명", width = 180)
    private String optItemNm;

    @ApiModelProperty(value = "신청수량")
    @RealGridColumnInfo(headText = "신청수량", columnType = RealGridColumnType.SERIAL, width = 60)
    private String claimItemQty;

    @ApiModelProperty(value = "상품금액")
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "상품금액", columnType = RealGridColumnType.PRICE)
    private String itemPrice;

    @ApiModelProperty(value = "총 상품금액")
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "총 상품금액", columnType = RealGridColumnType.PRICE)
    private String totItemPrice;

    @ApiModelProperty(value = "거부사유")
    @RealGridColumnInfo(headText = "거부사유", width = 80)
    private String rejectReasonType;

    @ApiModelProperty(value = "구매자이름")
    @RealGridColumnInfo(headText = "구매자이름", width = 80)
    private String buyerNm;

    @ApiModelProperty(value = "구매자연락처")
    @RealGridColumnInfo(headText = "구매자연락처", width = 120)
    private String buyerMobileNo;

    @ApiModelProperty(value = "수령인이름")
    @RealGridColumnInfo(headText = "수령인이름", width = 80)
    private String receiverNm;

    @ApiModelProperty(value = "수령인연락처")
    @RealGridColumnInfo(headText = "수령인연락처", width = 120)
    private String shipMobileNo;

    @ApiModelProperty(value = "업체상품코드")
    @RealGridColumnInfo(headText = "업체상품코드", width = 80)
    private String sellerItemCd;

    @ApiModelProperty(value = "업체옵션코드")
    @RealGridColumnInfo(headText = "업체옵션코드", width = 80)
    private String sellerOptCd;

    @ApiModelProperty(value = "관리코드")
    @RealGridColumnInfo(headText = "관리코드", width = 80)
    private String sellerManageCd;
}
