package kr.co.homeplus.partner.web.item.model.item;

import java.math.BigDecimal;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 DS정보 Set Entry
 */
@Getter
@Setter
public class ItemSetDto {

	//유입채널 (등록/수정) : (ADMIN: 어드민, PARTNER: 파트너, INBOUND: 인바운드)
	private String channel;

	/**
	 * 기본정보
	 */
	//상품번호
	private String itemNo;

	//상품명
	private String itemNm;

	//상품유형 (B:반품(리세일), N:새상품, R:리퍼, U:중고)
	private String itemType;

	//업체상품코드
	private String sellerItemCd;

	//세카테고리
	private int    dcateCd;

	//판매업체ID
	private String partnerId;

	//거래유형 (DS : 업체상품 , TD : 매장상품)
	private String mallType;

	//성인상품유형 (NORMAL: 일반, ADULT: 성인, LOCAL_LIQUOR: 전통주)
	private String adultType;

	//이미지노출 여부 (Y:노출, N:노출안함)
	private String imgDispYn;

	//노출여부
	private String dispYn;

	//브랜드번호
	private Long   brandNo;

	//제조사번호
	private Long   makerNo;

	//텍스트옵션사용여부 (사용:Y,미사용:N)
	private String optTxtUseYn;

	//등록/수정자
	private String userId;

	//제조일자
	private String makeDt;

	//유효일자
	private String expDt;

	/**
	 * 판매정보
	 */
	//과세여부(Y:과세,N:비과세)
	private String taxYn;

	//정상가격
	private Long   originPrice;

	//판매가격
	private Long   salePrice;

	//매입가
	private Long   purchasePrice;

	//판매기간시작일
	private String saleStartDt;

	//판매기간종료일
	private String saleEndDt;

	//수수료 부과 기준(정률:Rate,정액:Amount,없음:None)
	private String commissionType;

	//수수료율(%)
	private BigDecimal commissionRate;

	//수수료금액
	private Integer commissionPrice;

	//할인판매가
	private Integer dcPrice;

	//할인기간여부
	private String dcYn;

	//특정할인기간여부
	private String dcPeriodYn;

	//할인시작일시
	private String dcStartDt;

	//할인종료일시
	private String dcEndDt;

	//할인수수료타입
	private String dcCommissionType;

	//할인수수료율
	private BigDecimal dcCommissionRate;

	//할인수수료액
	private Integer dcCommissionPrice;

	//판매기간 기준-설정함/설정안함(Y/N)
	private String salePeriodYn;

	//예약판매사용여부
	private String rsvYn;

	//예약판매번호
	private Integer saleRsvNo;

	//예약판매시작일시
	private String rsvStartDt;

	//예약판매종료일시
	private String rsvEndDt;

	//예약상품출하시작일시
	private String shipStartDt;

	//예약상품출하종료일시
	private String shipEndDt;

	//재고수량
	private Integer stockQty;

	//재고수량 변경여부 (Y: 변경, N|NULL: 미변경)
	private String chgStockQtyYn;

	//판매단위
	private Integer saleUnit;

	//구매제한 - 최소구매수량
	private Integer purchaseMinQty;

	//1인당 구매제한 사용여부 - 미사용:Y / 사용:N
	private String purchaseLimitYn;

	//구매제한 타입 - 1회 : O / 기간제한 : P
	private String purchaseLimitDuration;

	//구매제한 일자 - ?일
	private Integer purchaseLimitDay;

	//구매제한 개수 - ?개
	private Integer purchaseLimitQty;

	//장바구니 제한여부
	private String cartLimitYn;

	//점포ID
	private int storeId;

	//점포유형
	private String storeType;

	//예약여부 조회용
	private int	 rsvCount;

	/**
	 * 이미지 정보
	 */
	private List<ItemImgGetDto> imgList;

	/**
	 * 배송정보
	 */
	//배송정책번호
	private Long shipPolicyNo;

	//출고기한(-1:미정)
	private Integer releaseDay;

	//출고기한 1일 인 경우 출고시간(1~24)
	private Integer releaseTime;

	//휴일제외여부
	private String  holidayExceptYn;

	//반품/교환 배송비
	private Integer claimShipFee;

	//출고지 우편번호
	private String  releaseZipcode;

	//출고지 주소1(우편번호제공)
	private String  releaseAddr1;

	//출고지 주소2(직접입력)
	private String  releaseAddr2;

	//회수지 우편번호
	private String  returnZipcode;

	//회수지 주소1(우편번호제공)
	private String  returnAddr1;

	//회수지 주소2(직접입력)
	private String  returnAddr2;

	//안전번호 여부
	private String  safeNumberUseYn;

	//배송 정책명
	private String 	shipPolicyNm;

	//배송유형명(ITEM:상품별/BUNDLE:묶음배송)
	private String 	shipKindNm;

	//배송방법명 (DS/TD_DLV/DRCT/POST/PICK)
	private String 	shipMethodNm;

	//배송비 유형명 ( FREE : 무료 / FIXED : 유료 / COND : 조건부 무료 )
	private String 	shipTypeNm;

	//배송비
	private String 	shipFee;

	//선결제여부명
	private String 	prepaymentYnNm;

	//배송비노출여부명
	private String 	shipDispYnNm;

	//차등설정
	private String 	diffYn;

	//차등개수
	private String 	diffQty;

	/**
	 * 상품 상세정보
	 */
	//상품상세정보
	private String itemDesc;

	/**
	 * 텍스트옵션 정보
	 */
	//텍스트형 옵션단계
	private int    optTxtDepth;

	//텍스트형 1단계 타이틀
	private String opt1Text;

	//텍스트형 2단계 타이틀
	private String opt2Text;

	/**
	 * 부가정보
	 */
	//속성번호 리스트
	private List<Integer> attrNoList;

	//상품고시정보 정책코드
	private Integer gnoticeNo;

	//상품고시정보 리스트
	private List<ItemNoticeGetDto> noticeList;

	//가격비교 등록여부
	private String epYn;

	//외부연동 사이트
	private String linkageYn;

	//검색 키워드
	private String srchKeyword;

	//해외배송 대행여부
	private String globalAgcyYn;

	//월렌탈료
	private Integer rentalFee;

	//의무사용기간
	private Integer rentalPeriod;

	//렌탈 등록비
	private Integer rentalRegFee;

	//선물하기사용여부 (Y: 사용, N: 사용안함)
	private String giftYn;

	//증빙서류 리스트
	private List<ItemProofListGetDto> proofFileList;

	//임시저장여부
	private String tempYn;

	//인증리스트
	private List<ItemCertGetDto> certList;

	//isbn
	private String isbn;

}
