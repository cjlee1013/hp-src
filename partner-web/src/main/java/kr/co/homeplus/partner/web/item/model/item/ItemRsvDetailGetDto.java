package kr.co.homeplus.partner.web.item.model.item;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 예약 판매정보
 */
@ApiModel("상품 예약 판매정보")
@Getter
@Setter
public class ItemRsvDetailGetDto {

	//예약판매번호
	@ApiModelProperty(value = "예약판매번호")
	private long saleRsvNo;

	//예약판매시작일시
	@ApiModelProperty(value = "예약판매시작일시")
	private String rsvStartDt;

	//예약판매종료일시
	@ApiModelProperty(value = "예약판매종료일시")
	private String rsvEndDt;

	//예약상품출하시작일시
	@ApiModelProperty(value = "예약상품출하시작일시")
	private String shipStartDt;

	//예약상품출하종료일시
	@ApiModelProperty(value = "예약상품출하종료일시")
	private String shipEndDt;

}
