package kr.co.homeplus.partner.web.core.dto;

import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PartnerPasswordValidDto {

    @NotEmpty(message = "판매자ID")
    private String partnerId;

    @NotEmpty(message = "영업담당자 성명")
    private String affiMngNm;

    @NotEmpty(message = "휴대폰 번호")
    private String affiMobile;
}
