package kr.co.homeplus.partner.web.core.config;

import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.interceptor.CertificationInterceptor;
import kr.co.homeplus.partner.web.core.interceptor.Check90DaysSamePasswordInterceptor;
import kr.co.homeplus.partner.web.core.interceptor.SellerAgreeInterceptor;
import kr.co.homeplus.partner.web.core.interceptor.StaticResourceInterceptor;
import kr.co.homeplus.partner.web.core.selleragree.service.SellerAgreeService;
import kr.co.homeplus.partner.web.core.service.Check90daysSamePasswordService;
import kr.co.homeplus.partner.web.core.view.excel.ExcelDataXlsxStreamingDownloadView;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;

/**
 * servlet context 설정을 위한 config
 *
 * <p>dispatcher-servlet.xml의 역할을 대체하는 클래스로 파트너 web 내의
 * servlet context와 관련된 설정을 관리한다.
 *
 * <p>해당 config 내에서는 Resources, Interceptors, ViewResolver, excelDataDownloadView를 등록하여 관리하고 있다.
 *
 *
 * @see #addResourceHandlers(ResourceHandlerRegistry)
 * @see #addInterceptors(InterceptorRegistry)
 * @see WebMvcConfigurer
 * @see TilesView
 * @see TilesConfigurer
 * @see ExcelDataXlsxStreamingDownloadView
 * @see CertificationInterceptor
 */
@Configuration
@RequiredArgsConstructor
public class ServletConfig implements WebMvcConfigurer {

    private final CertificationService certificationService;
    private final RealGridProperties realGridProperties;
    private final PartnerProperties partnerProperties;
    private final SellerAgreeService sellerAgreeService;
    private final AuthorizationProperties authorizationProperties;
    private final Check90daysSamePasswordService check90daysSamePasswordService;

    /**
     * mvc:resources 설정<br>
     * mapping = "/static/**", location = "/static/"
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
    }

    /**
     * interceptors 설정<br>
     * mapping path="/**", {@link CertificationInterceptor} 를 등록
     *
     * @param registry {@link InterceptorRegistry}
     */
    @Override
    public void addInterceptors(final InterceptorRegistry registry) {
        registry.addInterceptor(certificationInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(sellerAgreeInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(check90DaysSamePasswordInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(staticResourceInterceptor()).addPathPatterns("/**");
    }

    /**
     * TilesViewResolver 설정<br>
     * viewResolver order = 0
     *
     * @return tiewsViewResolver
     */
    @Bean
    public UrlBasedViewResolver viewResolver() {
        UrlBasedViewResolver urlBasedViewResolver = new UrlBasedViewResolver();
        urlBasedViewResolver.setViewClass(TilesView.class); //tilesView
        urlBasedViewResolver.setOrder(0);
        return urlBasedViewResolver;
    }

    /**
     * BeannameviewResolver 설정<br>
     * viewResolver order = 1
     *
     * @return beanNameViewResolver
     */
    @Bean
    public BeanNameViewResolver beanNameViewResolver() {
        BeanNameViewResolver beanNameViewResolver = new BeanNameViewResolver();
        beanNameViewResolver.setOrder(1);
        return beanNameViewResolver;
    }

    /**
     * TilesConfigurer 설정
     *
     * @return tilesConfigurer
     */
    @Bean
    public TilesConfigurer tilesConfigurer() {
        TilesConfigurer tilesConfigurer = new TilesConfigurer();
        tilesConfigurer.setDefinitions("/WEB-INF/views/**/tiles.xml");
        return tilesConfigurer;
    }

    /**
     * excelDataDownloadView 설정
     *
     * @return ExcelDataXlsxStreamingDownloadView excelDataDownloadView
     */
    @Bean(name = "excelDataDownloadView")
    public ExcelDataXlsxStreamingDownloadView excelDataDownloadView() {
        return new ExcelDataXlsxStreamingDownloadView();
    }

    /**
     * 인증 체크를 위한 인터셉터
     *
     * @return {@link CertificationInterceptor}
     */
    @Bean
    public CertificationInterceptor certificationInterceptor() {
        return new CertificationInterceptor(certificationService, partnerProperties);
    }

    /**
     * 약관동의 체크를 위한 인터셉터
     *
     * @return {@link SellerAgreeInterceptor}
     */
    @Bean
    public SellerAgreeInterceptor sellerAgreeInterceptor() {
        return new SellerAgreeInterceptor(sellerAgreeService, certificationService,
            authorizationProperties);
    }

    /**
     * 동일 비밀번호 90일 이상 사용여부를 체크하기 위한 인터셉터
     *
     * @return {@link Check90DaysSamePasswordInterceptor}
     */
    @Bean
    public Check90DaysSamePasswordInterceptor check90DaysSamePasswordInterceptor() {
        return new Check90DaysSamePasswordInterceptor(check90daysSamePasswordService,
            certificationService, authorizationProperties);
    }

    /**
     * 정적파일 버전(js,css, ...)과 리얼그리드 관련 정보(라이센스, 경로 등...)를 관리하는 인터셉터 입니다.
     * @return {@link StaticResourceInterceptor}
     */
    @Bean
    public StaticResourceInterceptor staticResourceInterceptor() {
        return new StaticResourceInterceptor(realGridProperties, partnerProperties);
    }
}