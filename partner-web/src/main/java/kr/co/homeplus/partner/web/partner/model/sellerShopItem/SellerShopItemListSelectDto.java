package kr.co.homeplus.partner.web.partner.model.sellerShopItem;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridConstants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SellerShopItemListSelectDto {

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 200)
    private String itemNo;
    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", width = 200)
    private String itemNm;
    @ApiModelProperty(value = "우선순위")
    @RealGridColumnInfo(headText = "우선순위", width = 0 , hidden = true)
    private String priority;
    @ApiModelProperty(value = "등록자")
    @RealGridColumnInfo(headText = "등록자", width = 200)
    private String regNm;
    @ApiModelProperty(value = "등록일")
    @RealGridColumnInfo(headText = "등록일", width = 200)
    private String regDt;

}
