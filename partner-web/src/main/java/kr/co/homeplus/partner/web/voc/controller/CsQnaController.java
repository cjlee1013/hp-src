package kr.co.homeplus.partner.web.voc.controller;

import java.util.List;
import kr.co.homeplus.partner.web.common.model.common.ResponseResult;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.utility.StringUtil;
import kr.co.homeplus.partner.web.voc.model.csQna.PartnerQnaBoardCountDto;
import kr.co.homeplus.partner.web.voc.model.csQna.PartnerQnaDetailSelectDto;
import kr.co.homeplus.partner.web.voc.model.csQna.PartnerQnaListParamDto;
import kr.co.homeplus.partner.web.voc.model.csQna.PartnerQnaListSelectDto;
import kr.co.homeplus.partner.web.voc.model.csQna.PartnerQnaReplySetParamDto;
import kr.co.homeplus.partner.web.voc.model.csQna.PartnerQnaTemplateSelectDto;
import kr.co.homeplus.partner.web.voc.model.csQna.PartnerQnaTemplateSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/voc/csQna")
public class CsQnaController {

    private final ResourceClient resourceClient;
    private final CodeService codeService;
    private final CertificationService certificationService;

    public CsQnaController(ResourceClient resourceClient,
        CodeService codeService,
        CertificationService certificationService
    ) {
        this.resourceClient = resourceClient;
        this.codeService = codeService;
        this.certificationService = certificationService;
    }
    /**
     * 고객관리 > 상품QnA관리
     */
    @RequestMapping(value = "/csQnaMain", method = RequestMethod.GET)
    public String csQnaMain(Model model, @RequestParam(value="qnaStatus", required = false) String qnaStatus) throws Exception {

        codeService.getCodeModel(model,
                "qna_status"                // qna 처리상태
            , "qna_type"                    // qna타입
            , "qna_po_search_type"           // PO qna 검색타입

        );
        model.addAttribute("qnaStatus", qnaStatus);
        model.addAllAttributes(
            RealGridHelper.create("csQnaListGridBaseInfo", PartnerQnaListSelectDto.class));

        model.addAllAttributes(
            RealGridHelper.create("csQnaTemplatePopGridBaseInfo", PartnerQnaTemplateSelectDto.class));


        return "/voc/csQnaMain";
    }
    /**
     * 고객관리 > 상품QnA관리 보드
     */
    @ResponseBody
    @RequestMapping(value = {"/getQnaBoardCount.json"}, method = RequestMethod.GET)
    public List<PartnerQnaBoardCountDto> getItemBoardCount() {

        String apiUri = "/po/voc/csQna/getQnaBoardCount";
        String partnerId = certificationService.getLoginUerInfo().getPartnerId();

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PartnerQnaBoardCountDto>>>() {};
        return (List<PartnerQnaBoardCountDto>) resourceClient
            .getForResponseObject(ResourceRouteName.MANAGE, apiUri + "?partnerId=" + partnerId,
                typeReference).getData();
    }

    /**
     * 고객관리 > 상품QnA관리 > QnA리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = {"/getQnaList.json"}, method = RequestMethod.GET)
    public List<PartnerQnaListSelectDto> getQnaList(PartnerQnaListParamDto partnerQnaListParamDto) {

        String apiUri = "/po/voc/csQna/getQnaList";
        partnerQnaListParamDto.setSearchPartnerId(certificationService.getLoginUerInfo().getPartnerId());

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PartnerQnaListSelectDto>>>() {};
        return (List<PartnerQnaListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil
            .getRequestString(apiUri, PartnerQnaListParamDto.class, partnerQnaListParamDto), typeReference).getData();
    }

    /**
     * 고객관리 > 상품QnA관리 > QnA 템플릿 리스트 조회
     * @param
     */
    @ResponseBody
    @RequestMapping(value = {"/getTemplateList.json"}, method = RequestMethod.GET)
    public List<PartnerQnaTemplateSelectDto> getTemplateList() {

        String apiUri = "/po/voc/csQna/getQnaTemplate?partnerId="+certificationService.getLoginUerInfo().getPartnerId();

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PartnerQnaTemplateSelectDto>>>() {};
        return (List<PartnerQnaTemplateSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE,apiUri, typeReference).getData();
    }

    /**
     * 고객관리 > 상품QnA관리 > 템플릿 저장/수정/삭제
     * @param partnerQnaTemplateSetDto
     */
    @ResponseBody
    @RequestMapping(value = {"/setQnaTemplate.json"}, method = RequestMethod.POST)
    public ResponseResult setQnaTemplate(@RequestBody PartnerQnaTemplateSetDto partnerQnaTemplateSetDto) {

        String apiUri = "/po/voc/csQna/setQnaTemplate";

        partnerQnaTemplateSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());

        return  (ResponseResult) resourceClient
            .postForResponseObject(ResourceRouteName.MANAGE, partnerQnaTemplateSetDto, apiUri,
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }

    /**
     * 고객관리 > 상품QnA관리 > QnA 상세메인
     */
    @RequestMapping(value = "/csQnaDetailMain", method = RequestMethod.GET)
    public String setReplyMain(Model model
        , @RequestParam(value="qnaNo") String qnaNo
    ) throws Exception {
        model.addAttribute("qnaNo", qnaNo);
        return "/voc/csQnaDetailMain";
    }

    /**
     * 고객관리 > 상품QnA관리 > QnA 상세조회
     * @param
     */
    @ResponseBody
    @RequestMapping(value = {"/getQnaDetail.json"}, method = RequestMethod.GET)
    public PartnerQnaDetailSelectDto getQnaDetail(Model model
        , @RequestParam(value="qnaNo") String qnaNo
    ) throws Exception {

        String apiUri = "/po/voc/csQna/getQnaDetail?partnerId=";
        String partnerId = certificationService.getLoginUerInfo().getPartnerId();
        apiUri += partnerId + "&qnaNo="+qnaNo;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<PartnerQnaDetailSelectDto>>() {};
        return (PartnerQnaDetailSelectDto) resourceClient.getForResponseObject(ResourceRouteName.MANAGE,apiUri, typeReference).getData();
    }

    /**
     * 고객관리 > 상품QnA관리 > QnA 답변등록
     * @param partnerQnaReplySetParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setQnaReply.json"}, method = RequestMethod.POST)
    public ResponseResult setQnaReply(@RequestBody PartnerQnaReplySetParamDto  partnerQnaReplySetParamDto) throws Exception {

        String apiUri = "/po/voc/csQna/setQnaReply";
        partnerQnaReplySetParamDto.setRegId(certificationService.getLoginUerInfo().getPartnerId());
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, partnerQnaReplySetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }



}
