package kr.co.homeplus.partner.web.common.model.upload;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
//URL로부터 이미지 파일 업로드
public class UploadUrlParamDto {

    //이미지를 가져 올 URL
    private String url;

    //파일 검사 및 조작 시 사용할 설정 정보
    private String processKey;

    //파일 이름
    private String fileName;
}
