package kr.co.homeplus.partner.web.partner.model;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 기본정보
 */

@Getter
@Setter
public class PartnerSetDto {

	/**
	 * 기본정보
	 */
	private String partnerId;
	private String originPassword;
	private String newPassword;
	private String partnerType;
	private String operatorType;
	private String partnerGrade;
	private String partnerStatus;
	private String userId;
	private String isMod;

	/**
	 * 사업자정보
	 */
	private String partnerNm;
	private String partnerOwner;
	private String partnerNo;
	private String communityNotiNo;
	private String businessConditions;
	private String bizCateCd;
	private String zipcode;
	private String addr1;
	private String addr2;
	private String roadAddr1;
	private String roadAddr2;

	/**
	 * 판매자정보
	 */
	private PartnerSellerSetDto sellerInfo;

	/**
	 * 반품택배정보
	 */
	private List<PartnerSellerDeliveryGetDto> deliveryList = new ArrayList<>();

	/**
	 * 정산정보
	 */
	private PartnerSettleSetDto settleInfo;

	/**
	 * 담당자정보
	 */
	private List<PartnerManagerSetDto> managerList = new ArrayList<>();

	/**
	 * 인증 담당자정보
	 */
	private List<CertificationManagerSetDto> certificationManagerList = new ArrayList<>();

	/**
	 * 파일정보
	 */
	private List<PartnerFileListGetDto> partnerFileList;

	/**
	 * 비밀번호
	 */
	private String password;
}
