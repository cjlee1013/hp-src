package kr.co.homeplus.partner.web.item.model.item;

import lombok.Getter;
import lombok.Setter;


//상품정보고시관리 Get Entry
@Getter
@Setter
public class MngNoticeListGetDto {

    //고시항목번호
    private int noticeNo;

    //정보고시항목명
    private String noticeNm;

    //정보고시항목 세부항목
    private String noticeDetail;

    //기본문구
    private String commentNm;

    //기본타이틀
    private String commentTitle;
}
