package kr.co.homeplus.partner.web.core.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JoinParam {

    //사업자번호
    private String partnerNo;

    //상호
    private String partnerNm;

    //대표자명
    private String partnerOwner;
}
