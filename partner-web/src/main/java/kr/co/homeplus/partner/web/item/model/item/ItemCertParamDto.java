package kr.co.homeplus.partner.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

/**
 * 안전인증 조회
 */
@Getter
@Setter
public class ItemCertParamDto {

	private String certGroup;
	private String certNo;

}
