package kr.co.homeplus.partner.web.core.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import kr.co.homeplus.partner.web.core.dto.UserInfo;
import org.slf4j.MDC;
import org.springframework.util.StringUtils;

public class TraceLogFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	/**
	 * Logger 에 로그인 사용자 정보를 삽입 처리한다.
	 *
	 * @param request
	 * @param response
	 * @param chain
	 * @throws IOException
	 * @throws ServletException
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpSession session = httpRequest.getSession();

		UserInfo userInfo = session.getAttribute("userInfo") != null ? (UserInfo) session.getAttribute("userInfo") : null;
		String userCode = "";
		String userDepartmentCode = "";
		if (userInfo != null) {
			userCode = userInfo.getUserCd();
			userDepartmentCode = userInfo.getUserDepartmentCd();
		}

		// empNo, deptNo, clientIp
		String metaLog = String.format("[%s;%s;%s]", userCode, userDepartmentCode, getClientIp(request));

		try {
			MDC.put("homeplusHeader", metaLog);
			chain.doFilter(request, response);
		}
		finally {
			MDC.clear();
		}

	}

	/**
	 * request 를 전송한 클라이언트의 IP 주소를 반환한다.
	 *
	 * @param request
	 * @return
	 */
	private String getClientIp(ServletRequest request) {
		String ip = ((HttpServletRequest) request).getHeader("X-FORWARDED-FOR");
		if (StringUtils.isEmpty(ip)) {
			ip = ((HttpServletRequest) request).getHeader("Proxy-Client-IP");
		}

		if (StringUtils.isEmpty(ip)) {
			ip = ((HttpServletRequest) request).getHeader("WL-Proxy-Client-IP");  // 웹로직일 경우
		}

		if (StringUtils.isEmpty(ip)) {
			ip = request.getRemoteAddr();
		}

		return ip;
	}

	@Override
	public void destroy() {
	}
}
