package kr.co.homeplus.partner.web.core.constants;

/**
 * 코드 관련 공통 상수
 */
public class CodeConstants {

    // 공통 > 시스템 구분자
    public static final String SYS_DIVISION = "∑";

    // 상품 > 금칙어 유효성체크 시 구분자
    public static final String BANNED_WORD_STRING_DIVISION = SYS_DIVISION;

}
