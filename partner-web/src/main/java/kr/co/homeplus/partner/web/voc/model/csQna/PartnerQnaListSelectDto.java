package kr.co.homeplus.partner.web.voc.model.csQna;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel("QNA 리스트")
public class PartnerQnaListSelectDto {

    @ApiModelProperty(value = "Qna 일련번호", position = 1 )
    @RealGridColumnInfo(headText = "문의번호", width = 100)
    private String qnaNo;

    @ApiModelProperty(value = "상품번호", position = 2 )
    @RealGridColumnInfo(headText = "상품번호", width = 150)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 3 )
    @RealGridColumnInfo(headText = "상품명", width = 150)
    private String itemNm;

    @ApiModelProperty(value = "문의유형", position = 4 )
    @RealGridColumnInfo(headText = "문의유형", width = 150)
    private String qnaType;

    @ApiModelProperty(value = "문의내용", position = 5 )
    @RealGridColumnInfo(headText = "문의내용", width = 150)
    private String qnaContents;

    @ApiModelProperty(value = "비밀글여부", position = 6 )
    @RealGridColumnInfo(headText = "비밀글여부", width = 100)
    private String secretYn;

    @ApiModelProperty(value = "구매여부", position = 7 )
    @RealGridColumnInfo(headText = "구매여부", width = 100)
    private String purchaseYn;

    @ApiModelProperty(value = "회원명", position = 8 )
    @RealGridColumnInfo(headText = "작성자", width = 150)
    private String userNm;

    @ApiModelProperty(value = "등록일", position = 9 )
    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170, sortable = true)
    private String regDt;

    @ApiModelProperty(value = "답변여부", position = 110 )
    @RealGridColumnInfo(headText = "답변여부", width = 150)
    private String qnaStatus;

    @ApiModelProperty(value = "노출여부", position = 11 )
    @RealGridColumnInfo(headText = "노출여부", width = 150)
    private String qnaDispStatus;

    @ApiModelProperty(value = "처리영역(ADMIN/PARTNER)", position = 12 )
    @RealGridColumnInfo(headText = "처리자", width = 150)
    private String compArea;

    @ApiModelProperty(value = "처리일시", position = 13 )
    @RealGridColumnInfo(headText = "처리일시", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170, sortable = true)
    private String compDt;

}
