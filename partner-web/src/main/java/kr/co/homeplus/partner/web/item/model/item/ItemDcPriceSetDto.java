package kr.co.homeplus.partner.web.item.model.item;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 할인가격 일괄변경
 */
@Getter
@Setter
public class ItemDcPriceSetDto {

	//상품번호
	private List<String> itemNo;

	//할인여부
	private String dcYn;

	//할인액
	private long dcPrice;

	//특정기간할인
	private String dcPeriodYn;

	//할인시작일
	private String dcStartDt;

	//할인종료일
	private String dcEndDt;

	//판매자ID
	private String partnerId;

	//등록|수정자
	private String userId;
}
