package kr.co.homeplus.partner.web.partner.model.shipPolicy;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShipiPolicySetDto implements Serializable {

    //판매업체명
    private String partnerId;

    //점포
    private Integer storeId;

    //기본여부(Y/N)
    private String defaultYn;

    //배송정책번호
    private Long shipPolicyNo ;

    //배송정책명
    private String shipPolicyNm ;

    //배송방법
    private String shipMethod;

    //출고기한
    private String releaseDay;

    //출고시간
    private String releaseTime;

    //휴일제외여부
    private String holidayExceptYn;

    //배송비유형
    private String shipKind;

    //배송유형
    private String shipType;

    //배송비
    private String shipFee;

    //무료조건비
    private String freeCondition;

    //배송비노출여부(Y/N)
    private String dispYn;

    //차등설정
    private String diffYn;

    //차등갯수
    private String diffQty;

    //선결제여부(Y/N)
    private String prepaymentYn;

    //반품/교환비
    private String claimShipFee;

    //제주추가배송비
    private Integer extraJejuFee;

    //도서산간배송비
    private Integer extraIslandFee;

    //배송가능지역
    private String shipArea;

    //출고지우편번호
    private String releaseZipcode;

    //출고지주소1
    private String releaseAddr1;

    //출고지주소2
    private String releaseAddr2;

    //회수지우편번호
    private String returnZipcode;

    //회수지주소1
    private String returnAddr1;

    //회수지주소2
    private String returnAddr2;

    //사용여부
    private String useYn;

    //안심번호사용여부
    private String safeNumberUseYn;

    //등록일
    private String regDt;

    //등록/수정자
    private String userId;
}
