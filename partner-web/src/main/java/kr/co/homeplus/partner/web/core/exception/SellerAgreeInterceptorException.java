package kr.co.homeplus.partner.web.core.exception;

/**
 * 약관동의 인터셉터 내 예외처리 용 Exception
 */
public class SellerAgreeInterceptorException extends RuntimeException {

    public SellerAgreeInterceptorException(final Exception e) {
        super(e);
    }

    public SellerAgreeInterceptorException(final String message) {
        super(message);
    }
}
