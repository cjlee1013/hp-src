package kr.co.homeplus.partner.web.claim.model;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RealGridOptionInfo(checkBar = true)
public class ClaimCancelListGetDto {


    @ApiModelProperty(value = "클레임번호", example = "1500000001")
    @RealGridColumnInfo(headText = "클레임번호", columnType = RealGridColumnType.SERIAL, width = 80)
    private String claimBundleNo;

    @ApiModelProperty(value = "클레임번호", example = "1500000011")
    @RealGridColumnInfo(headText = "클레임번호", hidden = true)
    private String claimNo;

    @ApiModelProperty(value = "클레임요청번호", example = "1500000011")
    @RealGridColumnInfo(headText = "클레임요청번호", hidden = true)
    private String claimReqNo;

    @ApiModelProperty(value = "주문번호", example = "3000004971")
    @RealGridColumnInfo(headText = "주문번호", columnType = RealGridColumnType.SERIAL, width = 80)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "배송번호", example = "3000000004")
    @RealGridColumnInfo(headText = "배송번호", columnType = RealGridColumnType.SERIAL, width = 80)
    private String bundleNo;

    @ApiModelProperty(value = "상품 주문번호", example = "3000012917")
    @RealGridColumnInfo(headText = "상품 주문번호", columnType = RealGridColumnType.SERIAL, width = 120)
    private String orderItemNo;

    @ApiModelProperty(value = "처리상태", example = "신청")
    @RealGridColumnInfo(headText = "처리상태", width = 60)
    private String claimStatus;

    @ApiModelProperty(value = "처리상태코드", example = "C1")
    @RealGridColumnInfo(headText = "처리상태코드", hidden = true)
    private String claimStatusCode;

    @ApiModelProperty(value = "클레임타입", example = "C")
    @RealGridColumnInfo(headText = "클레임타입", hidden = true)
    private String claimType;

    @ApiModelProperty(value = "신청일", example = "2020-12-23 13:20:54")
    @RealGridColumnInfo(headText = "신청일시", columnType = RealGridColumnType.DATETIME, width = 150)
    private String requestDt;

    @ApiModelProperty(value = "신청사유", example = "다른 상품으로 재주문")
    @RealGridColumnInfo(headText = "신청사유", width = 180)
    private String claimReasonType;

    @ApiModelProperty(value = "접수채널", example = "MYPAGE")
    @RealGridColumnInfo(headText = "접수채널", width = 80)
    private String requestId;

    @ApiModelProperty(value = "처리일", example = "2020-12-23 13:20:54")
    @RealGridColumnInfo(headText = "처리일", columnType = RealGridColumnType.DATETIME, width = 150)
    private String processDt;

    @ApiModelProperty(value = "승인채널", example = "MYPAGE")
    @RealGridColumnInfo(headText = "승인채널", width = 80)
    private String approveId;

    @ApiModelProperty(value = "상품번호", example = "10000300000265")
    @RealGridColumnInfo(headText = "상품번호", columnType = RealGridColumnType.SERIAL, width = 120)
    private String itemNo;

    @ApiModelProperty(value = "상품명", example = "[상품] 테스트_사라_알라크림치즈")
    @RealGridColumnInfo(headText = "상품명", width = 320)
    private String itemName;

    @ApiModelProperty(value = "옵션명", example = "1번옵션 : 옵션입니다. | 2번옵션 : 옵션입니다.")
    @RealGridColumnInfo(headText = "옵션명", width = 320)
    private String optItemNm;

    @ApiModelProperty(value = "신청수량", example = "1")
    @RealGridColumnInfo(headText = "신청수량", columnType = RealGridColumnType.SERIAL, width = 60)
    private String claimItemQty;

    @ApiModelProperty(value = "상품금액", example = "20000")
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "상품금액", columnType = RealGridColumnType.PRICE)
    private String itemPrice;

    @ApiModelProperty(value = "총 상품금액", example = "480000")
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER,headText = "총 상품금액", columnType = RealGridColumnType.PRICE)
    private String totItemPrice;

    @ApiModelProperty(value = "거부사유", example = "상품 발송처리")
    @RealGridColumnInfo(headText = "거부사유", width = 80)
    private String rejectReasonType;

    @ApiModelProperty(value = "구매자", example = "홍길동")
    @RealGridColumnInfo(headText = "구매자이름", width = 80)
    private String buyerNm;

    @ApiModelProperty(value = "구매자연락처", example = "010-1111-1111")
    @RealGridColumnInfo(headText = "구매자연락처", width = 120)
    private String buyerMobileNo;

    @ApiModelProperty(value = "수령인", example = "홍길동")
    @RealGridColumnInfo(headText = "수령인이름", width = 80)
    private String receiverNm;

    @ApiModelProperty(value = "수령인연락처", example = "010-1111-1111")
    @RealGridColumnInfo(headText = "수령인연락처", width = 120)
    private String shipMobileNo;

    @ApiModelProperty(value = "우편번호", example = "07297")
    @RealGridColumnInfo(headText = "우편번호", width = 60)
    private String zipcode;

    @ApiModelProperty(value = "주소", example = "서울특별시 영등포구 당산로 42 (문래동3가)")
    @RealGridColumnInfo(headText = "주소", width = 300)
    private String addr;

    @ApiModelProperty(value = "배송메시지", example = "직접 받고 부재 시 문 앞에 놓아주세요")
    @RealGridColumnInfo(headText = "배송메시지", width = 250)
    private String dlvShipMsg;

    @ApiModelProperty(value = "배송비지급", example = "선불")
    @RealGridColumnInfo(headText = "배송비지급", width = 80)
    private String shipPriceType;

    @ApiModelProperty(value = "업체상품코드", example = "400332")
    @RealGridColumnInfo(headText = "업체상품코드", width = 80)
    private String sellerItemCd;

    @ApiModelProperty(value = "업체옵션코드")
    @RealGridColumnInfo(headText = "업체옵션코드", width = 80)
    private String sellerOptCd;

    @ApiModelProperty(value = "관리코드")
    @RealGridColumnInfo(headText = "관리코드", width = 80)
    private String sellerManageCd;
}
