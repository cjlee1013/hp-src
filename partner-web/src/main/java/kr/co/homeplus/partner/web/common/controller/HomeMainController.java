package kr.co.homeplus.partner.web.common.controller;

import java.util.List;
import kr.co.homeplus.partner.web.common.model.home.ClaimDashBoardCount;
import kr.co.homeplus.partner.web.common.model.home.HomeNoticeSelectDto;
import kr.co.homeplus.partner.web.common.model.home.PartnerPopupDto;
import kr.co.homeplus.partner.web.common.model.home.PartnerSettleAmt;
import kr.co.homeplus.partner.web.common.model.home.SalesActInfo;
import kr.co.homeplus.partner.web.common.model.home.SalesProgress;
import kr.co.homeplus.partner.web.common.service.HomeDashboardService;
import kr.co.homeplus.partner.web.common.service.PartnerPopupService;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.manage.model.PartnerHarmfulItemListSelectDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Slf4j
@Controller
@RequiredArgsConstructor
public class HomeMainController {

    @Value("${plus.resource-routes.imageFront.url}")
    private String imgFrontUrl;

    private final CertificationService certificationService;
    private final PartnerPopupService partnerPopupService;
    private final HomeDashboardService homeDashboardService;

    @GetMapping(value =  "/home")
    public String home(Model model) {
        final String partnerId = certificationService.getLoginUerInfo().getPartnerId();

        //대시보드 정보 조회
        SalesActInfo salesActInfo = homeDashboardService.getSalesActInfo(partnerId);
        model.addAttribute("salesActInfo", salesActInfo);

        HomeNoticeSelectDto homeNoticeSelectDto = homeDashboardService.getBoardList(partnerId);

        model.addAttribute("generalNoticeList", homeNoticeSelectDto.getGeneralNoticeList());
        model.addAttribute("menualNoticeList", homeNoticeSelectDto.getMenualNoticeList());
        model.addAttribute("faqList", homeNoticeSelectDto.getFaqList());

        List<PartnerHarmfulItemListSelectDto> harmfulList = homeDashboardService
            .getPartnerHarmfulItemList(partnerId);

        model.addAttribute("harmfulList", harmfulList);

        //파트너 팝업 조회
        List<PartnerPopupDto> partnerPopupList = partnerPopupService.getPartnerPopupList();
        model.addAttribute("partnerPopupList", partnerPopupList);
        model.addAttribute("imgFrontUrl", imgFrontUrl);

        return "/common/homeMain";
    }

    /**
     * 홈 대시보드 판매진행현황
     * @return 판매진행현황 건수
     */
    @ResponseBody
    @GetMapping(value = "/home/salesProgress.json")
    public SalesProgress getSalesProgress() {
        final String partnerId = certificationService.getLoginUerInfo().getPartnerId();
        SalesProgress salesProgress = homeDashboardService.getSalesProgress(partnerId);
        return salesProgress;
    }

    /**
     * 홈 대시보드 정산예정 건수
     * @return 정산 예정건수 조회
     */
    @ResponseBody
    @GetMapping(value = "/home/partnerSettleAmt.json")
    public PartnerSettleAmt getPartnerSettleAmt() {
        final String partnerId = certificationService.getLoginUerInfo().getPartnerId();
        PartnerSettleAmt settleAmt = homeDashboardService.getPartnerSettleAmt(partnerId);
        return settleAmt;
    }

    /**
     * 홈 대시보드 클레임 건수(반품, 교환, 주문쉬소)
     * @return 홈 대시보드 클레임 건수 조회
     */
    @ResponseBody
    @GetMapping(value = "/home/claimDashBoardCount.json")
    public ClaimDashBoardCount getClaimDashBoardCount() {
        final String partnerId = certificationService.getLoginUerInfo().getPartnerId();
        ClaimDashBoardCount claimDashBoardCount = homeDashboardService.getClaimCount(partnerId);
        return claimDashBoardCount;
    }
}