package kr.co.homeplus.partner.web.voc.model.itemReview;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품평 리스트")
public class ItemReviewSelectDto {

    @ApiModelProperty(value = "상품평 번호", position = 1 )
    @RealGridColumnInfo(headText = "상품평 번호", width = 60)
    private String reviewNo;

    @ApiModelProperty(value = "상품번호", position = 2 )
    @RealGridColumnInfo(headText = "상품번호", width = 100)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 3 )
    @RealGridColumnInfo(headText = "상품명", width = 150)
    private String itemNm;

    @ApiModelProperty(value = "내용", position = 4 )
    @RealGridColumnInfo(headText = "내용", width = 150)
    private String contents;

    @ApiModelProperty(value = "작성자", position = 5 )
    @RealGridColumnInfo(headText = "작성자", width = 80)
    private String userNm;

    @ApiModelProperty(value = "등록일", position = 6 )
    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 120, sortable = true)
    private String regDt;

    @ApiModelProperty(value = "이미지 여부", position = 7 )
    @RealGridColumnInfo(headText = "이미지 여부", width = 50)
    private String imgYn;

    @ApiModelProperty(value = "노출 여부", position = 8 )
    @RealGridColumnInfo(headText = "노출 여부", width = 50)
    private String blockYn;

    @ApiModelProperty(value = "미노출 사유", position = 9)
    @RealGridColumnInfo(headText = "미노출 사유", width = 150)
    private String blockContents;

    @ApiModelProperty(value = "수정자", position = 10)
    @RealGridColumnInfo(headText = "수정자", width = 70)
    private String chgNm;

    @ApiModelProperty(value = "수정일시", position = 11 )
    @RealGridColumnInfo(headText = "수정일시", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 120, sortable = true)
    private String chgDt;

}
