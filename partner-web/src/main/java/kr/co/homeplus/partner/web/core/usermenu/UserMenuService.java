package kr.co.homeplus.partner.web.core.usermenu;

import java.util.List;
import kr.co.homeplus.partner.web.core.dto.AuthMenuListDto;
import kr.co.homeplus.partner.web.core.dto.UserInfo;

public interface UserMenuService {
    List<AuthMenuListDto> getUserPermitMenuList(UserInfo userInfo) throws Exception;
}
