package kr.co.homeplus.partner.web.common.model.pop;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class ItemPopupListGetDto {
	@RealGridColumnInfo(headText = "상품번호", sortable = true)
	private String itemNo;
	@RealGridColumnInfo(headText = "상품명", sortable = true)
	private String itemNm1;
	@RealGridColumnInfo(headText = "대분류", sortable = true)
	private String lcateNm;
	@RealGridColumnInfo(headText = "중분류", sortable = true)
	private String mcateNm;
	@RealGridColumnInfo(headText = "소분류", sortable = true)
	private String scateNm;
	@RealGridColumnInfo(headText = "세분류", sortable = true)
	private String dcateNm;
}
