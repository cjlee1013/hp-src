package kr.co.homeplus.partner.web.core.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import kr.co.homeplus.partner.web.common.model.common.ResponseResult;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.partner.web.core.annotation.CertificationNeedLess;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.config.PartnerProperties;
import kr.co.homeplus.partner.web.core.constants.ChannelConstants;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.dto.JoinParam;
import kr.co.homeplus.partner.web.core.dto.LoginInitInfo;
import kr.co.homeplus.partner.web.core.dto.MobileOccupationValidParam;
import kr.co.homeplus.partner.web.core.dto.PartnerAccountStatusSetDto;
import kr.co.homeplus.partner.web.core.dto.PartnerIdConfirmDto;
import kr.co.homeplus.partner.web.core.dto.PartnerPasswordParamSetDto;
import kr.co.homeplus.partner.web.core.dto.PartnerPasswordSetDto;
import kr.co.homeplus.partner.web.core.dto.PartnerPasswordValidDto;
import kr.co.homeplus.partner.web.core.dto.Step1LoginInfo;
import kr.co.homeplus.partner.web.core.dto.Step1LoginParam;
import kr.co.homeplus.partner.web.core.dto.Step2LoginParam;
import kr.co.homeplus.partner.web.core.dto.UserIdGetDto;
import kr.co.homeplus.partner.web.core.dto.UserIdParamGetDto;
import kr.co.homeplus.partner.web.core.dto.UserInfo;
import kr.co.homeplus.partner.web.core.dto.UserPwGetDto;
import kr.co.homeplus.partner.web.core.dto.UserPwParamGetDto;
import kr.co.homeplus.partner.web.core.dto.UserTempPwParamSetDto;
import kr.co.homeplus.partner.web.core.service.LoginCookieService;
import kr.co.homeplus.partner.web.partner.enums.PartnerStatus;
import kr.co.homeplus.partner.web.partner.enums.PartnerType;
import kr.co.homeplus.partner.web.partner.model.PartnerSellerDeliveryGetDto;
import kr.co.homeplus.partner.web.partner.model.PartnerSellerGetDto;
import kr.co.homeplus.partner.web.partner.model.PartnerSetDto;
import kr.co.homeplus.partner.web.partner.service.PartnerService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.plus.util.ServletUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 로그인 처리를 위한 컨트롤러
 */
@Slf4j
@Controller
@RequiredArgsConstructor
public class LoginController {

    private final CertificationService certificationService;
    private final PartnerProperties partnerProperties;
    private final ResourceClient resourceClient;
    private final PartnerService partnerService;
    private final CodeService codeService;
    private final LoginCookieService loginCookieService;
    private final RefitCryptoService refitCryptoService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    @CertificationNeedLess
    @GetMapping(value = "/login")
    public String webServiceLoginStartNoFrame(final Model model) {
        final LoginInitInfo info = loginCookieService.getLoginInitInfo();

        model.addAttribute("savePartnerId", info.getSavePartnerId());
        model.addAttribute("chkSavePartnerId", info.isChkSavePartnerId());
        return "/login/login";
    }

    @Deprecated
    @CertificationNeedLess
    @PostMapping(value = "/login/action", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String webServiceLoginAction(Model model, @Validated Step2LoginParam step2LoginParam,
        BindingResult bindingResult) {
        certificationService.getStep2Login(step2LoginParam, bindingResult);

        model.addAttribute("goUrl", partnerProperties.getStartUrl());
        return "/login/action";
    }

    /**
     * 2단계 로그인
     * @param request HttpServletRequest
     * @param step2LoginParam 로그인 파라미터
     * @param bindingResult BindingResult
     * @return 로그인 처리후 진입할 URL을 리턴합니다.
     */
    @ResponseBody
    @CertificationNeedLess
    @PostMapping(value = "/step2/login.json")
    public ResponseObject<String> loginProcess(final HttpServletRequest request,
        @Valid final Step2LoginParam step2LoginParam, final BindingResult bindingResult) {

        final String clientIp = ServletUtils.clientIP(request);
        step2LoginParam.setIp(clientIp);

        certificationService.getStep2Login(step2LoginParam, bindingResult);

        final String goUrl = partnerProperties.getStartUrl();

        return ResponseObject.Builder.<String>builder()
            .status(ResponseObject.STATUS_SUCCESS)
            .data(goUrl)
            .returnCode(ResponseObject.RETURN_CODE_SUCCESS)
            .build();
    }

    /**
     * 1단계 로그인 요청
     * @param request request
     * @param loginParam 로그인 파라미터
     * @param bindingResult BindingResult
     * @return {@link Step1LoginInfo}
     */
    @ResponseBody
    @CertificationNeedLess
    @PostMapping(value = "/step1/login.json")
    public ResponseObject<Step1LoginInfo> getStep1Login(final HttpServletRequest request,
        @Valid final Step1LoginParam loginParam, final BindingResult bindingResult) {

        final String clientIp = ServletUtils.clientIP(request);
        loginParam.setIp(clientIp);

        Step1LoginInfo loginInfo = certificationService.getStep1Login(loginParam, bindingResult);

        return ResponseObject.Builder.<Step1LoginInfo>builder()
            .status(ResponseObject.STATUS_SUCCESS)
            .data(loginInfo)
            .returnCode(ResponseObject.RETURN_CODE_SUCCESS)
            .build();
    }

    /**
     * 파트너 로그인 휴대폰 인증번호 검증요청
     * @param param 인증번호 파라미터
     * @return 인증완료 체크용 토큰을 리턴합니다.
     */
    @ResponseBody
    @CertificationNeedLess
    @PostMapping(value = "/verify/mobile/certNo/send.json")
    public ResponseObject<String> verifyMobileCertNo(
        @RequestBody @Valid final MobileOccupationValidParam param) {

        final String verifiedToken = certificationService.verifyMobileCertNo(param);

        return ResponseObject.Builder.<String>builder()
            .status(ResponseObject.STATUS_SUCCESS)
            .data(verifiedToken)
            .returnCode(ResponseObject.RETURN_CODE_SUCCESS)
            .build();
    }

    @CertificationNeedLess
    @GetMapping(value = "/logout")
    public String webServiceLogoutAction(final Model model,
        final HttpServletRequest request) {
        // 세션 로그인 정보 삭제
        certificationService.processUserLogout();

        model.addAttribute("certificationRedirectParam",
            partnerProperties.getCertificationRedirectUrl());

        // 로그인 페이지로 이동
        return "/login/logout";
    }

    /**
     * 로그인 팝업을 여는 컨트롤러<p>
     * <p>
     * AJAX 통신 중 로그인 세션 만료 시 사용합니다.
     *
     * @param model   model
     * @return loginPop
     */
    @CertificationNeedLess
    @GetMapping("/login/popup.json")
    public String webServiceLoginStartNoFramePopup(final Model model) {
        model.addAttribute("returnUrl", partnerProperties.getCertificationRedirectUrl());
        return "/login/loginPop";
    }

    /**
     * 로그인 세션을 연장합니다.
     * @see UserInfo
     * @return {@code ResponseObject<UserInfo>}
     */
    @GetMapping("/login/session/extend.json")
    @ResponseBody
    public boolean extendLoginSession() {
        return certificationService.getLoginExtension();
    }

    /**
     * 판매업체 > 아이디 찾기
     * @return
     */
    @CertificationNeedLess
    @GetMapping(value = "/login/getUserId")
    public String getUserId() {
        return "/login/getUserId";
    }

    /**
     * 판매업체 > 아이디 찾기 조회
     * @param userIdParamGetDto
     * @return
     */
    @CertificationNeedLess
    @ResponseBody
    @GetMapping(value = {"/login/getUserId.json"})
    public List<UserIdGetDto> getUserId(
            UserIdParamGetDto userIdParamGetDto
    ) {
        String apiUri = "/po/partner/getUserId";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<UserIdGetDto>>>() {
        };

        return (List<UserIdGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM,
                apiUri + "?partnerNo=" + userIdParamGetDto.getPartnerNo() + "&affiManagerMobile="
                        + userIdParamGetDto.getAffiManagerMobile() + "&affiManagerEmail="
                        + userIdParamGetDto.getAffiManagerEmail(), typeReference).getData();
    }

    /**
     * 판매업체 > 비밀번호 찾기
     * @return
     */
    @CertificationNeedLess
    @GetMapping(value = "/login/getUserPw")
    public String getUserPw() {
        return "/login/getUserPw";
    }

    /**
     * 판매업체 > 비밀번호 찾기 조회
     * @param userPwParamGetDto
     * @return
     */
    @CertificationNeedLess
    @ResponseBody
    @PostMapping(value = {"/login/getUserPw.json"})
    public UserPwGetDto getUserPw(
            @RequestBody UserPwParamGetDto userPwParamGetDto
    ) {

        ResponseObject<UserPwGetDto> responseObject = resourceClient.postForResponseObject(
                ResourceRouteName.ITEM, userPwParamGetDto
                , "/po/partner/getUserPw", new ParameterizedTypeReference<ResponseObject<UserPwGetDto>>() {});

        UserPwGetDto responseData = responseObject.getData();

        return responseData;
    }

    /**
     * 판매업체 > 임시패스워스 발급
     * @param userTempPwParamSetDto
     * @return
     */
    @CertificationNeedLess
    @ResponseBody
    @PostMapping(value = {"/login/setPartnerTempPw.json"})
    public ResponseResult setPartnerTempPw(UserTempPwParamSetDto userTempPwParamSetDto) {
        userTempPwParamSetDto.setUserId("SYSTEM");

        ResponseObject<ResponseResult> responseObject  = resourceClient.postForResponseObject(ResourceRouteName.ITEM
                , userTempPwParamSetDto
                , "/partner/setPartnerTempPw"
                , new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {});

        ResponseResult responseData = responseObject.getData();

        return responseData;
    }

    /**
     * 회원가입 step1
     * @return
     */
    @CertificationNeedLess
    @GetMapping(value = "/join/step1")
    public String joinStep1() {
        return "/join/joinStep1";
    }

    /**
     * 회원가입 step2
     * @param joinParam
     * @param model
     * @return
     */
    @CertificationNeedLess
    @PostMapping(value = "/join/step2")
    public String joinStep2(JoinParam joinParam, final Model model) {

        codeService.getCodeModel(model,
                "bank_code"                 //은행코드
                ,       "partner_file_type"       //판매업체파일유형코드
        );

        model.addAttribute("partnerNo", joinParam.getPartnerNo());
        model.addAttribute("partnerNm", joinParam.getPartnerNm());
        model.addAttribute("partnerOwner", joinParam.getPartnerOwner());
        model.addAttribute("hmpImgUrl", hmpImgUrl);

        return "/join/joinStep2";
    }

    /**
     * 회원가입 step3
     * @param model
     * @param mngNm
     * @param mngEmail
     * @param mngMobile
     * @param mngPhone
     * @return
     */
    @CertificationNeedLess
    @PostMapping(value = "/join/step3")
    public String joinStep3(final Model model,
            @RequestParam(name = "mngNm") String mngNm,
            @RequestParam(name = "mngEmail") String mngEmail,
            @RequestParam(name = "mngMobile") String mngMobile,
            @RequestParam(name = "mngPhone", required = false) String mngPhone) {

        model.addAttribute("mngNm", mngNm);
        model.addAttribute("mngEmail", mngEmail);
        model.addAttribute("mngMobile", mngMobile);
        model.addAttribute("mngPhone", mngPhone);

        return "/join/joinStep3";
    }

    /**
     * 판매자 아이디조회
     * @param partnerId
     * @return
     */
    @CertificationNeedLess
    @ResponseBody
    @GetMapping(value = {"/join/checkSeller.json"})
    public Boolean checkSeller(@RequestParam(name = "partnerId") String partnerId) {
        String apiUri = "/partner/checkSeller";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Boolean>>() {};

        return (Boolean) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?partnerId=" + partnerId , typeReference).getData();
    }

    /**
     * 회원가입 > 판매자 등록
     *
     * @param partnerSetDto
     * @return
     * @throws Exception
     */
    @CertificationNeedLess
    @ResponseBody
    @PostMapping(value = "/join/setJoinPartner.json")
    public ResponseResult setPartnerMng(@RequestBody PartnerSetDto partnerSetDto) {
        partnerSetDto.setUserId(partnerSetDto.getPartnerId());
        partnerSetDto.setPartnerType(PartnerType.SELLER.getType());
        partnerSetDto.setIsMod("false");

        ResponseObject<ResponseResult> responseObject = resourceClient.postForResponseObject(
                ResourceRouteName.ITEM, partnerSetDto
                , "/po/partner/setPartner", new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {});

        ResponseResult responseData = responseObject.getData();

        return responseData;
    }

    /**
     * 회원정보 조회/수정
     * @param model
     * @return
     */
    @GetMapping(value = "/user/verify")
    public String userVerify(final Model model) {

        model.addAttribute("partnerId", certificationService.getLoginUerInfo().getPartnerId());

        return "/user/verify";
    }

    /**
     * 회원 비밀번호 인증 조회
     * @param loginParam
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/user/checkPartnerPassword.json")
    public Boolean checkPartnerPassword(@Valid Step1LoginParam loginParam) {
        final Boolean loginResult = certificationService.checkLoginFromUsermng(loginParam);

        return loginResult;
    }

    @PostMapping(value = "/user/modify")
    public String userModify(final Model model, @Valid Step1LoginParam loginParam) throws Exception {

        final Boolean loginResult = certificationService.checkLoginFromUsermng(loginParam);

        if (loginResult) { //로그인 성공
            codeService.getCodeModel(model,
                    "bank_code"    // 은행코드
                    , "settle_cycle_type"   // 정산주기
                    , "partner_file_type"   // 판매업체파일유형코드
                    , "plusapi_agency_cd"   // 대행업체코드
            );

            ObjectMapper mapper = new ObjectMapper();
            String partnerId = certificationService.getLoginUerInfo().getPartnerId();
            List<PartnerSellerDeliveryGetDto> resDeliveryList  = resourceClient.getForResponseObject(ResourceRouteName.SHIPPING
                    , "/admin/shipManage/getReturnDlvCompanyList"
                    , new ParameterizedTypeReference<ResponseObject<List<PartnerSellerDeliveryGetDto>>>() {}).getData();

            List<PartnerSellerDeliveryGetDto> resDeliveryInfo  = resourceClient.getForResponseObject(ResourceRouteName.ITEM
                    , "/partner/getPartnerDeliveryList?partnerId=" + partnerId
                    , new ParameterizedTypeReference<ResponseObject<List<PartnerSellerDeliveryGetDto>>>() {}).getData();

            PartnerSellerGetDto partnerSellerGetDto = partnerService.getSeller(partnerId);
            partnerId = PrivacyMaskingUtils.maskingUserId(partnerId);

            partnerSellerGetDto.setPartnerId(partnerId);
            partnerSellerGetDto.setBankAccountNo(PrivacyMaskingUtils.maskingBankAccount(partnerSellerGetDto.getBankAccountNo()));

            model.addAttribute("deliveryList",  new Gson().toJson(resDeliveryList));
            model.addAttribute("deliveryInfo",  new Gson().toJson(resDeliveryInfo));
            model.addAttribute("partnerId", partnerId);
            model.addAttribute("hmpImgUrl", hmpImgUrl);
            model.addAttribute("saleType", partnerSellerGetDto.getSaleType());
            model.addAttribute("partnerInfo", mapper.writeValueAsString(partnerSellerGetDto));

            return "/user/modify";
        } else {
            model.addAttribute("isLoginResult", false);
            model.addAttribute("returnMessage", "비밀번호를 입력해주세요.");
            model.addAttribute("goUrl", partnerProperties.getStartUrl());
            return "/login/action";
        }
    }

    /**
     * 비밀번호 변경 팝업창
     * @param model
     * @param partnerPasswordParamSetDto
     * @return
     * @throws Exception
     */
    @CertificationNeedLess
    @PostMapping(value = "/user/setPartnerPasswordPop")
    public String setPartnerPasswordPop(final Model model, @Valid PartnerPasswordParamSetDto partnerPasswordParamSetDto) throws Exception{

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject>() {
        };

        //신규 api호출 적용안됨
        Boolean isMobileValid = (Boolean) resourceClient
                .postForResponseObject(ResourceRouteName.USER,
                        partnerPasswordParamSetDto, "/common/mobile/certno/valid",
                        typeReference).getData();

        if (ObjectUtils.isNotEmpty(isMobileValid)) {
            model.addAttribute("userSubInfo", refitCryptoService.encrypt(partnerPasswordParamSetDto.getPartnerId() + "|" + partnerPasswordParamSetDto.getAffiMngNm() + "|" + partnerPasswordParamSetDto.getMobile()));
        }

        return "/user/pop/passwordPop";
    }

    @CertificationNeedLess
    @ResponseBody
    @PostMapping(value = "/user/setIdActivatePop.json")
    public ResponseResult setIdActivatePop(@RequestBody @Valid PartnerPasswordParamSetDto partnerPasswordParamSetDto) {
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject>() {
        };

        //신규 api호출 적용안됨 성공값과 실패값의 구조가 다름 성공값 리턴시 Boolean 실패값 리턴시 ResponseObject
        Boolean isMobileValid = (Boolean) resourceClient
                .postForResponseObject(ResourceRouteName.USER,
                        partnerPasswordParamSetDto, "/common/mobile/certno/valid",
                        typeReference).getData();

        if (ObjectUtils.isNotEmpty(isMobileValid)) {

            String apiUri = "/partner/setAccountStatus";

            ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                    .apiId(ResourceRouteName.ITEM)
                    .uri(apiUri)
                    .postObject(PartnerAccountStatusSetDto.builder().partnerId(partnerPasswordParamSetDto.getPartnerId()).accountStatus(
                            PartnerStatus.NORMAL.getStatus()).regReason("계정활성화").userId(partnerPasswordParamSetDto.getPartnerId()).build())
                    .typeReference(new ParameterizedTypeReference<>() {})
                    .build();

            return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
        } else {
            ResponseResult responseResult = new ResponseResult();
            responseResult.setReturnCode("1");
            responseResult.setReturnMsg("인증번호를 입력해주세요.");
            return responseResult;
        }
    }

    /**
     * 비밀번호 변경 기능
     * @param partnerPasswordSetDto
     * @return
     * @throws Exception
     */
    @CertificationNeedLess
    @ResponseBody
    @PostMapping(value = "/user/setPartnerPassword.json")
    public ResponseResult setPartnerPassword(@RequestBody @Valid PartnerPasswordSetDto partnerPasswordSetDto) throws Exception {

        partnerPasswordSetDto.setUserId(ChannelConstants.USERID);
        String[] userSubInfo = refitCryptoService.decrypt(partnerPasswordSetDto.getUserSubInfo()).split("\\|");
        partnerPasswordSetDto.setPartnerId(userSubInfo[0]);
        partnerPasswordSetDto.setAffiMngNm(userSubInfo[1]);
        partnerPasswordSetDto.setAffiMobile(userSubInfo[2]);

        String apiUri = "/partner/setPartnerPwPop";

        ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri(apiUri)
                .postObject(partnerPasswordSetDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    @ResponseBody
    @PostMapping(value = "/user/setPartnerPasswordExceed.json")
    public ResponseResult setPartnerPasswordExceed(@RequestBody @Valid PartnerPasswordSetDto partnerPasswordSetDto) throws Exception {

        partnerPasswordSetDto.setUserId(ChannelConstants.USERID);
        partnerPasswordSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());

        String apiUri = "/partner/setPartnerPwExceed";

        ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri(apiUri)
                .postObject(partnerPasswordSetDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 비밀번호 유효성검사 (팝업 띄우기 전 유효성)
     * @param partnerPasswordValidDto
     * @return
     * @throws Exception
     */
    @CertificationNeedLess
    @ResponseBody
    @PostMapping(value = "/user/validPartnerPassword.json")
    public ResponseResult validPartnerPassword(@RequestBody @Valid PartnerPasswordValidDto partnerPasswordValidDto) {

        String apiUri = "/partner/validPartnerPassword";

        ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri(apiUri)
                .postObject(partnerPasswordValidDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    @CertificationNeedLess
    @ResponseBody
    @PostMapping(value = "/user/validPartnerIdAffi.json")
    public ResponseResult validPartnerIdAffi(@RequestBody @Valid PartnerIdConfirmDto partnerIdConfirmDto) {

        String apiUri = "/partner/validPartnerIdAffi";

        ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri(apiUri)
                .postObject(partnerIdConfirmDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 이용약관 / 개인정보처리방침
     * @param type
     * @return
     */
    @CertificationNeedLess
    @GetMapping(value = "/terms")
    public String terms(@RequestParam(name = "type") String type) {

        // 개인정보처리방침
        if (type.equalsIgnoreCase("personal")) {
            return "/empty/personal";
        }
        // 입점문의
        else if (type.equalsIgnoreCase("contact")) {
            return "/empty/contact";
        } else {
        //  이용약관
            return "/empty/basic";
        }

    }

}