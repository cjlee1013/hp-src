package kr.co.homeplus.partner.web.voc.model.csQna;

import lombok.Data;

@Data
public class PartnerQnaReplySetParamDto {

    private Integer qnaReplyNo;

    private Integer qnaNo;

    private String replyArea;

    private String qnaReply;

    private String displayStatus;

    private String regId;

}
