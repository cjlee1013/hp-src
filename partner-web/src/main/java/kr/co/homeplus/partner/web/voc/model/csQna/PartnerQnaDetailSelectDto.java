package kr.co.homeplus.partner.web.voc.model.csQna;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel("QNA 상세조회")
public class PartnerQnaDetailSelectDto {

    @ApiModelProperty(value = "Qna 일련번호")
    private String qnaNo;

    @ApiModelProperty(value = "문의 유형")
    private String qnaType;

    @ApiModelProperty(value = "문의내용")
    private String qnaContents;

    @ApiModelProperty(value = "Qna 노출여부")
    private String displayStatus;

    @ApiModelProperty(value = "회원명")
    private String userNm;

    @ApiModelProperty(value = "회원 아이디")
    private String userId;

    @ApiModelProperty(value = "등록일시")
    private String regDt;

    @ApiModelProperty(value = "처리일시")
    private String compDt;

    @ApiModelProperty(value = "답변 리스트")
    List<PartnerQnaReplyListSelectDto> qnaReplyList;

    @ApiModelProperty(value = "템플릿 리스트")
    List<PartnerQnaTemplateSelectDto> qnaTemplateList;

    @ApiModelProperty(value = "파트너 아이디")
    private String partnerId;
}
