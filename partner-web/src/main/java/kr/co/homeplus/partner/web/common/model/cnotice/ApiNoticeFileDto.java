package kr.co.homeplus.partner.web.common.model.cnotice;

import lombok.Data;

@Data
public class ApiNoticeFileDto {
    private String fileUrl;
    private String fileNm;
}
