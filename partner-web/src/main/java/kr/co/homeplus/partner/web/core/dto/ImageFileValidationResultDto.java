package kr.co.homeplus.partner.web.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * 이미지 서버의 이미지 validation 기능 호출 후 응답 결과를 담는 Dto
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImageFileValidationResultDto {

    /**
     * 응답 결과
     */
    private String result;

    /**
     * error 정보. null 일 경우 에러가 없음.
     */
    private String error;

    /**
     * errors 가 있는지 확인하여 true, false 반환.
     */
    public boolean hasError() {
        return this.error != null;
    }
}
