package kr.co.homeplus.partner.web.item.model.item;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

//셀러상품 등록/수정 조회
@Getter
@Setter
public class ItemListParamDto {

    //상품명
    private String schItemNm;

    //검색 조건
    private String schType;

    //검색 키워드
    private String schKeyword;

    //대분류 카테고리
    private String schLcateCd;

    //증분류 카테고리
    private String schMcateCd;

    //소분류 카테고리
    private String schScateCd;

    //세분류 카테고리
    private String schDcateCd;

    //검색 날짜유형
    private String schDate;

    //검색 시작일
    private String schStartDate;

    //검색 종류일
    private String schEndDate;

    //판매자ID
    private String partnerId;

    //재고
    private Integer schStockQty;

    //상품상태(T: 임시저장, A:판매중, S:판매중지)
    private List<String> schItemStatus;
}
