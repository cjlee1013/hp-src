package kr.co.homeplus.partner.web.item.model.item;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridButtonType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RealGridOptionInfo(checkBar = true)
public class ItemListGetDto {

    @ApiModelProperty(value = "수정", position = 1 )
    @RealGridColumnInfo(headText = "수정", width = 80, columnType = RealGridColumnType.BUTTON, alwaysShowButton=true, buttonType= RealGridButtonType.ACTION)
    private String editBtn;

    @ApiModelProperty(value = "상품번호", position = 2)
    @RealGridColumnInfo(headText = "상품번호", width = 150, sortable = true)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 3 )
    @RealGridColumnInfo(headText = "상품명", width = 200, columnType = RealGridColumnType.NAME, sortable = true)
    private String itemNm;

    @ApiModelProperty(value = "상품연관번호" , position = 4)
    @RealGridColumnInfo(headText = "상품연관번호", sortable = true)
    private String relationNo;

    @ApiModelProperty(value = "판매상태", position = 5)
    @RealGridColumnInfo(headText = "판매상태", hidden = true)
    private String itemStatus;

    @ApiModelProperty(value = "판매상태", position = 5)
    @RealGridColumnInfo(headText = "판매상태", sortable = true)
    private String itemStatusNm;

    @ApiModelProperty(value = "판매자 상품코드", position = 6)
    @RealGridColumnInfo(headText = "판매자 상품코드", width = 100, sortable = true)
    private String sellerItemCd;

    @ApiModelProperty(value = "상품유형", position = 7)
    @RealGridColumnInfo(headText = "상품유형", width = 100, sortable = true)
    private String itemTypeNm;

    @ApiModelProperty(value = "판매가격", position = 8)
    @RealGridColumnInfo(headText = "판매가격", width = 100, sortable = true)
    private Long salePrice;

    @ApiModelProperty(value = "할인가격", position = 9)
    @RealGridColumnInfo(headText = "할인가격", width = 100, sortable = true)
    private String dcPrice;

    @ApiModelProperty(value = "재고수량", position = 10 )
    @RealGridColumnInfo(headText = "재고수량", width = 100, columnType = RealGridColumnType.NUMBER_CENTER, sortable = true)
    private String stockQty;

    @ApiModelProperty(value = "판매시작일", position = 11)
    @RealGridColumnInfo(headText = "판매시작일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170, sortable = true)
    private String saleStartDt;

    @ApiModelProperty(value = "판매종료일", position = 12)
    @RealGridColumnInfo(headText = "판매종료일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170, sortable = true)
    private String saleEndDt;

    @ApiModelProperty(value = "대분류", position = 13)
    @RealGridColumnInfo(headText = "대분류", width = 100, sortable = true)
    private String lcateNm;

    @ApiModelProperty(value = "중분류", position = 14)
    @RealGridColumnInfo(headText = "중분류", width = 100, sortable = true)
    private String mcateNm;

    @ApiModelProperty(value = "소분류", position = 15)
    @RealGridColumnInfo(headText = "소분류", width = 100, sortable = true)
    private String scateNm;

    @ApiModelProperty(value = "세분류", position = 16)
    @RealGridColumnInfo(headText = "세분류", width = 100, sortable = true)
    private String dcateNm;

    @ApiModelProperty(value = "세분류", position = 17)
    @RealGridColumnInfo(headText = "세분류" , hidden = true)
    private String dcateCd;

    @ApiModelProperty(value = "배송정책번호", position = 8)
    private Long shipPolicyNo;

    @ApiModelProperty(value = "배송방법", position = 18)
    @RealGridColumnInfo(headText = "배송방법", width = 100, sortable = true)
    private String shipMethodNm;

    @ApiModelProperty(value = "배송유형", position = 19)
    @RealGridColumnInfo(headText = "배송유형", width = 100, sortable = true)
    private String shipTypeNm;

    @ApiModelProperty(value = "배송비종류", position = 20)
    @RealGridColumnInfo(headText = "배송비종류", width = 100, sortable = true)
    private String shipKindNm;

    @ApiModelProperty(value = "배송비", position = 21)
    @RealGridColumnInfo(headText = "배송비", width = 100, sortable = true)
    private String shipFee;

    @ApiModelProperty(value = "등록일", position = 22)
    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170, sortable = true)
    private String regDt;

    @ApiModelProperty(value = "등록자", position = 23)
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regNm;

    @ApiModelProperty(value = "수정일", position = 24)
    @RealGridColumnInfo(headText = "수정일" , columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170, sortable = true)
    private String chgDt;

    @ApiModelProperty(value = "수정자", position = 25)
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgNm;


}
