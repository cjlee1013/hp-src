package kr.co.homeplus.partner.web.sell.model.rentalManage;

import io.swagger.annotations.ApiModel;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageListGetDto;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 렌탈판매관리 리스트 응답 DTO")
public class RentalManageListGetDto extends OrderShipManageListGetDto {

}