package kr.co.homeplus.partner.web.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(description = "환불완료 결과 DTO")
public class RequestTicketMultiCancelGetDto {

    @ApiModelProperty(value = "등록수", position = 1)
    private long totalCnt;

    @ApiModelProperty(value = "성공수", position = 2)
    private long successCnt;

    @ApiModelProperty(value = "실패수", position = 3)
    private long failCnt;

}
