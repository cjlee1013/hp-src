package kr.co.homeplus.partner.web.core.dto;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginResult {
    private List<PartnerCookieDto> setCookieList;
}
