package kr.co.homeplus.partner.web.common.model.pop;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ItemPopupOptionListGetDto {
	private String itemNo;
	private String itemNm1;
	private String optNo;
	private String opt1Val;
	private String lcateNm;
	private String mcateNm;
	private String scateNm;
	private String dcateNm;
	private String regNm;
	private String regDt;
}
