package kr.co.homeplus.partner.web.settle.model.settleInfo;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class SettleInfoGetDto {
    @ApiModelProperty(value = "매출일")
    @RealGridColumnInfo(headText = "매출일", width = 100)
    private String basicDt;

    @ApiModelProperty(value = "집계구분")
    @RealGridColumnInfo(headText = "집계구분", width = 100)
    private String gubun;

    @ApiModelProperty(value = "주문번호")
    @RealGridColumnInfo(headText = "주문번호", width = 100)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "배송번호")
    @RealGridColumnInfo(headText = "배송번호", width = 100)
    private long bundleNo;

    @ApiModelProperty(value = "구분")
    @RealGridColumnInfo(headText = "구분", width = 100)
    private String orderType;

    @ApiModelProperty(value = "상품주문번호")
    @RealGridColumnInfo(headText = "상품주문번호", width = 100)
    private long orderItemNo;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 120)
    private long itemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", width = 250)
    private String itemNm;

    @ApiModelProperty(value = "과면세구분")
    @RealGridColumnInfo(headText = "과면세구분", width = 100)
    private String taxYn;

    @ApiModelProperty(value = "판매가")
    @RealGridColumnInfo(headText = "판매가", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long itemSaleAmt;

    @ApiModelProperty(value = "수수료율")
    @RealGridColumnInfo(headText = "수수료율", width = 100)
    private String feeRate;

    @ApiModelProperty(value = "수량")
    @RealGridColumnInfo(headText = "수량", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipCompleteQty;

    @ApiModelProperty(value = "금액")
    @RealGridColumnInfo(headText = "금액", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipCompleteAmt;

    @ApiModelProperty(value = "판매수수료")
    @RealGridColumnInfo(headText = "판매수수료", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long saleAgencyFee;

    @ApiModelProperty(value = "배송비")
    @RealGridColumnInfo(headText = "배송비", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipAmt;

    @ApiModelProperty(value = "반품배송비")
    @RealGridColumnInfo(headText = "반품배송비", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimShipAmt;

    @ApiModelProperty(value = "상품할인(업체)")
    @RealGridColumnInfo(headText = "쿠폰할인(업체)", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponSellerChargeAmt;

    @ApiModelProperty(value = "배송비할인(업체)")
    @RealGridColumnInfo(headText = "배송비할인(업체)", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long sellerChargeShipAmt;

    @ApiModelProperty(value = "주문자명")
    @RealGridColumnInfo(headText = "주문자명", width = 100)
    private String userNm;

    @ApiModelProperty(value = "결제일")
    @RealGridColumnInfo(headText = "결제일", width = 120)
    private String paymentCompleteDt;

}
