package kr.co.homeplus.partner.web.common.model.home;

import lombok.Getter;
import lombok.Setter;

/**
 * 파트너 홈메인 대시보드 클레임관련
 */
@Setter
@Getter
public class ClaimDashBoardCount {
    /** 취소요청건수 **/
    private Long cancelRequestCnt;

    /** 반품요청건수 **/
    private Long returnRequestCnt;

    /** 교환요청건수 **/
    private Long exchangeRequestCnt;
}
