package kr.co.homeplus.partner.web.common.service;

import java.util.List;
import kr.co.homeplus.partner.web.common.model.home.ClaimDashBoardCount;
import kr.co.homeplus.partner.web.common.model.home.HomeNoticeSelectDto;
import kr.co.homeplus.partner.web.common.model.home.PartnerSettleAmt;
import kr.co.homeplus.partner.web.common.model.home.SalesActInfo;
import kr.co.homeplus.partner.web.common.model.home.SalesProgress;
import kr.co.homeplus.partner.web.manage.model.PartnerHarmfulItemListSelectDto;

public interface HomeDashboardService {

    /**
     * '나의 판매활동' 내역을 item-api에서 조회합니다.
     * @param partnerId 파트너 아이디
     * @return {@link SalesActInfo}
     */
    SalesActInfo getSalesActInfo(String partnerId);

    /**
     * 일반공지 내역을 item-api에서 조회합니다.
     * @param partnerId 파트너 아이디
     * @return {@link HomeNoticeSelectDto}
     */
    HomeNoticeSelectDto getBoardList(String partnerId);

    /**
     * 위해상품 내역을 item-api에서 조회합니다.
     * @param partnerId 파트너 아이디
     * @return {@link PartnerHarmfulItemListSelectDto}
     */
    List<PartnerHarmfulItemListSelectDto> getPartnerHarmfulItemList(String partnerId);

    /**
     * '판매진행현황'을 shipping-api에서 조회합니다.
     * @param partnerId 파트너 아이디
     * @return {@link SalesProgress}
     */
    SalesProgress getSalesProgress(String partnerId);

    /**
     * '판매진행현황 > 정산예정' 금액을 settle-api에서 조회합니다.
     * @param partnerId 파트너 아이디
     * @return {@link PartnerSettleAmt}
     */
    PartnerSettleAmt getPartnerSettleAmt(String partnerId);

    /**
     * '판매진행현황 > 클레임관련(반품, 교환, 주문취소) 건수 조회
     * @param partnerId 파트너 아이디
     * @return {@link ClaimDashBoardCount}
     */
    ClaimDashBoardCount getClaimCount(String partnerId);
}
