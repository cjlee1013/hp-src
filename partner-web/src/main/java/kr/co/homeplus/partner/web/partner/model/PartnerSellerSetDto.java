package kr.co.homeplus.partner.web.partner.model;

import lombok.Data;

@Data
public class PartnerSellerSetDto {

	private String businessNm;
	private String infoCenter;
	private String email;
	private String phone1;
	private String phone2;
	private String companyInfo;
	private String homepage;
	private Integer categoryCd;
	private String cultureDeductionYn;
	private String cultureDeductionNo;
	private String smsYn;
	private String emailYn;
	private String returnDeliveryYn;
}
