package kr.co.homeplus.partner.web.common.model.home;


import java.util.List;
import kr.co.homeplus.partner.web.manage.model.PartnerFaqListSelectDto;
import kr.co.homeplus.partner.web.manage.model.PartnerNoticeListSelectDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 홈 메인화면 공지사항 관련 모델
 */
@Setter
@Getter
public class HomeNoticeSelectDto {

    private List<PartnerNoticeListSelectDto> generalNoticeList;

    private List<PartnerNoticeListSelectDto> menualNoticeList;

    private List<PartnerFaqListSelectDto> faqList;
}
