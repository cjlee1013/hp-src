package kr.co.homeplus.partner.web.partner.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
// 판매업체
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerSellerGetDto {

    // 판매업체ID
    private String partnerId;

    // 업체명
    private String businessNm;

    // 사업자유형
    private String operatorType;

    // 회원등급
    private String partnerGrade;

    // 회원상태
    private String partnerStatus;

    // 승인상태
    private String approvalStatus;

    // 상호
    private String partnerNm;

    // 대표자명
    private String partnerOwner;

    // 사업자등록번호
    private String partnerNo;

    // 통신판매업 신고번호
    private String communityNotiNo;

    // 업태
    private String businessConditions;

    // 종목
    private String bizCateCd;

    // 종목명
    private String bizCateCdNm;

    // 우편번호
    private String zipcode;

    // 주소1
    private String addr1;

    // 주소2
    private String addr2;

    // 도로1
    private String roadAddr1;

    // 도로2
    private String roadAddr2;

    // 대표번호1
    private String phone1;

    // 대표번호2
    private String phone2;

    // 고객문의 연락처
    private String infoCenter;

    // 대표이메일
    private String email;

    // 홈페이지
    private String homepage;

    // 회사소개
    private String companyInfo;

    // 대표 카테고리
    private String categoryCd;

    // 도서/공연 소득공제여부
    private String cultureDeductionYn;

    // 도서/공연 소득공제 사업자 식별번호
    private String cultureDeductionNo;

    // 반품택배사여부
    private String returnDeliveryYn;

    // sms 수신여부
    private String smsYn;

    // email 수신여부
    private String emailYn;

    // sms 수신여부명
    private String smsYnNm;

    // email 수신여부명
    private String emailYnNm;

    // 판매유형
    private String saleType;

    // 은행코드
    private String bankCd;

    // 은행코드명
    private String bankCdNm;

    // 은행계좌
    private String bankAccountNo;

    // 예금주
    private String depositor;

    // 정산주기
    private String settleCycleType;

    // 담당자정보
    private List<PartnerManagerGetDto> managerList;

    // 첨부파일
    private List<PartnerFileListGetDto> partnerFileList;

    //인증 담당자 정보
    private List<CertificationManagerGetDto> certificationManagerList;

    // 대행업체코드
    private String agencyCd;

    // apiKey
    private String apiKey;

    // apiKey 등록일
    private String apiKeyRegDt;

}
