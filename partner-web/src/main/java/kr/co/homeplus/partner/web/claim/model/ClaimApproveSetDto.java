package kr.co.homeplus.partner.web.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageShipAllSetDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임 상태 변경 요청 파라미터")
public class ClaimApproveSetDto {

    @NotNull(message = "클레임번호")
    @ApiModelProperty(value = "클레임번호", position = 1, example = "1500000022")
    private long claimNo;

    @NotNull(message = "클레임번들번호")
    @ApiModelProperty(value = "클레임번들번호", position = 2, example = "1500000022")
    private long claimBundleNo;

    @NotNull(message = "클레임 타입")
    @ApiModelProperty(value = "클레임 타입", position = 3, example = "R")
    private String claimType;

}
