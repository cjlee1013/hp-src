package kr.co.homeplus.partner.web.sell.model.orderShipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 발주/발송관리 배송지수정 정보 수정 요청 dto")
public class OrderShipManageShipAddrInfoSetDto {

    @ApiModelProperty(value = "배송번호", position = 1, hidden = true)
    private long bundleNo;

    @ApiModelProperty(value = "이름", position = 2)
    private String receiverNm;

    @ApiModelProperty(value = "개인통관부호", position = 3)
    private String personalOverseaNo;

    @ApiModelProperty(value = "연락처", position = 4)
    private String shipMobileNo;

    @ApiModelProperty(value = "우편번호", position = 5)
    private String zipCode;

    @ApiModelProperty(value = "도로명", position = 6)
    private String roadBaseAddr;

    @ApiModelProperty(value = "도로명상세", position = 7)
    private String roadDetailAddr;

    @ApiModelProperty(value = "지번", position = 8)
    private String baseAddr;

    @ApiModelProperty(value = "지번상세", position = 9)
    private String detailAddr;

    @ApiModelProperty(value = "배송요청사항코드", position = 10)
    private String shipMsgCd;

    @ApiModelProperty(value = "배송요청사항", position = 11)
    private String shipMsg;

    @ApiModelProperty(value = "배송구분", position = 12)
    private String shipType;

}
