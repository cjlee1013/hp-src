package kr.co.homeplus.partner.web.common.model.categoryMng;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
@ApiModel("카테고리 관리 > 카테고리명 조회 Entry")
public class CategoryNmGetDto {

	@ApiModelProperty(value = "대분류 카테고리 코드")
	private String lcateCd;

	@ApiModelProperty(value = "대분류 카테고리명")
	private String lcateNm;

	@ApiModelProperty(value = "중분류 카테고리 코드")
	private String mcateCd;

	@ApiModelProperty(value = "중분류 카테고리명")
	private String mcateNm;

	@ApiModelProperty(value = "소분류 카테고리 코드")
	private String scateCd;

	@ApiModelProperty(value = "소분류 카테고리명")
	private String scateNm;

	@ApiModelProperty(value = "세분류 카테고리 코드")
	private String dcateCd;

	@ApiModelProperty(value = "세분류 카테고리명")
	private String dcateNm;

	@ApiModelProperty(value = "카테고리 풀명")
	private String cateFullNm;

}
