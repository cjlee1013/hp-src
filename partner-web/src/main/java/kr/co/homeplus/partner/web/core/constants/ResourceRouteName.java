package kr.co.homeplus.partner.web.core.constants;

public class ResourceRouteName {
    /**
     * image upload
     */
    public static final String UPLOAD = "upload";

    /**
     * image upload
     */
    public static final String IMAGE = "image";

    /**
     * product api
     */
    public static final String ITEM = "item";

    /**
     * manage api
     */
    public static final String MANAGE = "manage";
    /**
     * authority api
     */
    public static final String AUTHORITY = "ams";

    /**
     * image Upload Validation api
     */
    public static final String IMAGE_UPLOAD_VALIDATION = "imageUploadValidation";

    /**
     * user api
     */
    public static final String USER = "user";

    /**
     * escrowmng api
     */
    public static final String ESCROWMNG = "escrowmng";

    /***
     * searchmng api
     */
    public static final String SEARCHMNG = "searchmng";

    /***
     * mileage api
     */
    public static final String MILEAGE = "mileage";

    /**
     * address api
     */
    public static final String ADDRESS = "address";

    /**
     * usermng api
     **/
    public static final String USERMNG = "usermng";

    /**
     * shipping api
     **/
    public static final String SHIPPING = "shipping";

    /**
     * settle api
     **/
    public static final String SETTLE = "settle";

}
