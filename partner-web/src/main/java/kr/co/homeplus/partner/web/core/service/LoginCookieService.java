package kr.co.homeplus.partner.web.core.service;

import static kr.co.homeplus.plus.util.WebCookieUtils.addCookie;
import static kr.co.homeplus.plus.util.WebCookieUtils.deleteCookie;
import static kr.co.homeplus.plus.util.WebCookieUtils.getCookieValue;

import kr.co.homeplus.partner.web.core.constants.PartnerConstants;
import kr.co.homeplus.partner.web.core.dto.LoginInitInfo;
import kr.co.homeplus.partner.web.core.exception.LoginIdSaveCookieServiceException;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import kr.co.homeplus.plus.crypto.exception.CryptoClientException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoginCookieService {
    private final RefitCryptoService refitCryptoService;

    /**
     * 로그인 페이지에서 초기정보(아이디 저장여부, 저장 아이디)를 반환합니다.
     * @return {@link LoginInitInfo} 로그인 페이지 초기정보
     */
    public LoginInitInfo getLoginInitInfo() {
        final String encryptUserId = getCookieValue(
            PartnerConstants.PARTNER_LOGIN_ID_SAVE_COOKIE_NAME);

        if (StringUtils.isNotEmpty(encryptUserId)) {
            String decryptUserId;
            try {
                decryptUserId = refitCryptoService.decrypt(encryptUserId);
            } catch (CryptoClientException e) {
                removeIdSaveCookie();
                throw new LoginIdSaveCookieServiceException(
                    "An error occurred while decrypting a idSaveCookie.", e);
            }
            return new LoginInitInfo(true, decryptUserId);
        }
        return new LoginInitInfo();
    }

    /**
     * 아이디 저장 쿠키를 삭제합니다.
     */
    public void removeIdSaveCookie() {
        deleteCookie(PartnerConstants.PARTNER_LOGIN_ID_SAVE_COOKIE_NAME);
    }

    /**
     * 아이디 저장 쿠키를 생성합니다.
     * @param userId 사용자 아이디
     */
    public void createIdSaveCookie(final String userId) {
        String encryptUserId;
        try {
            encryptUserId = refitCryptoService.encrypt(userId);
        } catch (CryptoClientException e) {
            log.error("createIdSaveCookie encrypt failed!:<{}>", userId);
            throw new LoginIdSaveCookieServiceException(e.getMessage(), e);
        }

        addCookie(PartnerConstants.PARTNER_LOGIN_ID_SAVE_COOKIE_NAME, encryptUserId,
            PartnerConstants.PARTNER_LOGIN_ID_SAVE_COOKIE_MAX_AGE);
    }
}
