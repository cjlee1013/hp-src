package kr.co.homeplus.partner.web.partner.model.sellerShopItem;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PartnerSellerShopItemGetDto {

    private String dispType;

    private List<SellerShopItemListSelectDto> itemList;

}
