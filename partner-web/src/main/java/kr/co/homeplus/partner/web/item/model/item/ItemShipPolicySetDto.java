package kr.co.homeplus.partner.web.item.model.item;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 할인가격 일괄변경
 */
@Getter
@Setter
public class ItemShipPolicySetDto {

	//상품번호
	private List<String> itemNo;

	//배송정척
	private Long shipPolicyNo;

	//반품/교환 배송비
	private int claimShipFee;

	//출고지 우편번호
	private String releaseZipcode;

	//출고지 주소1(우편번호제공)
	private String releaseAddr1;

	//출고지 주소2(직접입력)
	private String releaseAddr2;

	//회수지 우편번호
	private String returnZipcode;

	//회수지 주소1(우편번호제공)
	private String returnAddr1;

	//회수지 주소2(직접입력)
	private String returnAddr2;

	//판매자ID
	private String partnerId;

	//등록|수정자
	private String userId;
}
