package kr.co.homeplus.partner.web.item.model.category;

import lombok.Getter;
import lombok.Setter;

//상품속성 리스트
@Getter
@Setter
public class AttributeItemListSelectDto {

    //그룹속성번호
    private Long gattrNo;

    //그룹속성명
    private String gattrNm;

    //멀티선택여부
    private String multiYn;

    //속성번호
    private Long attrNo;

    //속성명
    private String attrNm;

    //사용여부
    private String useYn;

}
