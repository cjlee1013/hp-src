package kr.co.homeplus.partner.web.partner.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PartnerNoInfoGetDto {

    private String statusCode;
    private String compName;
    private String repName;

}
