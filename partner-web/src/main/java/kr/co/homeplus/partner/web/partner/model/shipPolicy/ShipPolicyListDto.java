package kr.co.homeplus.partner.web.partner.model.shipPolicy;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShipPolicyListDto {

    @ApiModelProperty(value = "배송정책번호")
    @RealGridColumnInfo(headText = "배송정책번호" , width = 100, sortable = true)
    private Long shipPolicyNo ;

    @ApiModelProperty(value = "기본여부")
    @RealGridColumnInfo(headText = "기본여부", hidden = true)
    private String defaultYn;

    @ApiModelProperty(value = "기본")
    @RealGridColumnInfo(headText = "기본", width = 50, sortable = true)
    private String defaultYnNm;

    @ApiModelProperty(value = "배송정책명")
    @RealGridColumnInfo(headText = "배송정책명" , width = 100, sortable = true)
    private String shipPolicyNm;

    @ApiModelProperty(value = "배송방법")
    @RealGridColumnInfo(headText = "배송방법", hidden = true)
    private String shipMethod;

    @ApiModelProperty(value = "배송방법")
    @RealGridColumnInfo(headText = "배송방법" , width = 100, sortable = true)
    private String shipMethodNm;

    @ApiModelProperty(value = "출고기한")
    @RealGridColumnInfo(headText = "출고기한", hidden = true)
    private String releaseDay;

    @ApiModelProperty(value = "출고시간")
    @RealGridColumnInfo(headText = "출고시간", hidden = true)
    private String releaseTime;

    @ApiModelProperty(value = "휴일제외여부")
    @RealGridColumnInfo(headText = "휴일제외여부", hidden = true)
    private String holidayExceptYn;

    @ApiModelProperty(value = "배송비종류")
    @RealGridColumnInfo(headText = "배송비종류", hidden = true)
    private String shipKind;

    @ApiModelProperty(value = "배송비종류")
    @RealGridColumnInfo(headText = "배송비종류" , width = 100, sortable = true)
    private String shipKindNm;

    @ApiModelProperty(value = "배송유형")
    @RealGridColumnInfo(headText = "배송유형", hidden = true)
    private String shipType;

    @ApiModelProperty(value = "배송비유형")
    @RealGridColumnInfo(headText = "배송유형" , width = 100, sortable = true)
    private String shipTypeNm;

    @ApiModelProperty(value = "배송비")
    @RealGridColumnInfo(headText = "배송비" , width = 100, sortable = true)
    private String shipFee;

    @ApiModelProperty(value = "무료조건비")
    @RealGridColumnInfo(headText = "무료조건비", hidden = true)
    private String freeCondition;

    @ApiModelProperty(value = "차등설정")
    @RealGridColumnInfo(headText = "차등설정", hidden = true)
    private String diffYn;

    @ApiModelProperty(value = "차등갯수")
    @RealGridColumnInfo(headText = "차등갯수", hidden = true)
    private String diffQty;

    @ApiModelProperty(value = "배송비노출여부")
    @RealGridColumnInfo(headText = "배송비노출여부", hidden = true)
    private String dispYn;

    @ApiModelProperty(value = "선결제여부")
    @RealGridColumnInfo(headText = "선결제여부", hidden = true)
    private String prepaymentYn;

    @ApiModelProperty(value = "반품/교환비")
    @RealGridColumnInfo(headText = "반품/교환비" , width = 100, sortable = true)
    private String claimShipFee;

    @ApiModelProperty(value = "제주추가배송비")
    @RealGridColumnInfo(headText = "제주추가배송비", hidden = true)
    private String extraJejuFee;

    @ApiModelProperty(value = "도서산간배송비")
    @RealGridColumnInfo(headText = "도서산간배송비", hidden = true)
    private String extraIslandFee;

    @ApiModelProperty(value = "배송가능지역")
    @RealGridColumnInfo(headText = "배송가능지역", hidden = true)
    private String shipArea;

    @ApiModelProperty(value = "출고지우편번호")
    @RealGridColumnInfo(headText = "출가지우편번호", hidden = true)
    private String releaseZipcode;

    @ApiModelProperty(value = "출고지주소1")
    @RealGridColumnInfo(headText = "출고지주소1", hidden = true)
    private String releaseAddr1;

    @ApiModelProperty(value = "출고지주소2")
    @RealGridColumnInfo(headText = "출고지주소2", hidden = true)
    private String releaseAddr2;

    @ApiModelProperty(value = "회수지우편번호")
    @RealGridColumnInfo(headText = "회수지우편번호", hidden = true)
    private String returnZipcode;

    @ApiModelProperty(value = "회수지주소1")
    @RealGridColumnInfo(headText = "회수지주소1", hidden = true)
    private String returnAddr1;

    @ApiModelProperty(value = "회수지주소2")
    @RealGridColumnInfo(headText = "기본여부", hidden = true)
    private String returnAddr2;

    @ApiModelProperty(value = "안심번호사용여부")
    @RealGridColumnInfo(headText = "안심번호사용여부", hidden = true)
    private String safeNumberUseYn;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", hidden = true)
    private String useYn;

    @ApiModelProperty(value = "등록일", position = 22)
    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170, sortable = true)
    private String regDt;

    @ApiModelProperty(value = "등록자", position = 23)
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regNm;

    @ApiModelProperty(value = "수정일", position = 24)
    @RealGridColumnInfo(headText = "수정일" , columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170, sortable = true)
    private String chgDt;

    @ApiModelProperty(value = "수정자", position = 25)
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgNm;

}
