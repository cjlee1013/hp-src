package kr.co.homeplus.partner.web.sell.controller;

import com.nhncorp.lucy.security.xss.XssPreventer;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridColumn;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.partner.web.sell.model.rentalManage.RentalManageCounselReservationSetDto;
import kr.co.homeplus.partner.web.sell.model.ticketManage.TicketManageCntGetDto;
import kr.co.homeplus.partner.web.sell.model.ticketManage.TicketManageCompleteSetDto;
import kr.co.homeplus.partner.web.sell.model.ticketManage.TicketManageConfirmOrderSetDto;
import kr.co.homeplus.partner.web.sell.model.ticketManage.TicketManageListGetDto;
import kr.co.homeplus.partner.web.sell.model.ticketManage.TicketManageListSetDto;
import kr.co.homeplus.partner.web.sell.service.SellCommonService;
import kr.co.homeplus.partner.web.sell.service.TicketManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 판매관리 > 이티켓판매관리
 */
@Controller
@RequestMapping("/sell/ticketManage")
public class TicketManageController {

    @Value("${front.homeplus.url}")
    private String homeplusFrontUrl;

    @Value("${front.theclub.url}")
    private String theclubFrontUrl;

    @Autowired
    private TicketManageService ticketManageService;
    @Autowired
    private SellCommonService sellCommonService;

    // 이티켓판매관리 그리드
    private static final List<RealGridColumn> TICKET_MANAGE_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption TICKET_MANAGE_GRID_OPTION = new RealGridOption("fill", false, true, false);

    public TicketManageController() {
        // 이티켓판매관리 그리드
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("orderItemNo", "orderItemNo", "상품주문번호", RealGridColumnType.BASIC, 100, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("orderDt", "orderDt", "주문일시", RealGridColumnType.BASIC, 150, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("issueDt", "issueDt", "발급일시", RealGridColumnType.BASIC, 150, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("shipStatusNm", "shipStatusNm", "주문상태", RealGridColumnType.BASIC, 100, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("ticketStatusNm", "ticketStatusNm", "티켓상태", RealGridColumnType.BASIC, 100, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("issueStatusNm", "issueStatusNm", "발급상태", RealGridColumnType.BASIC, 100, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("purchaseOrderNo", "purchaseOrderNo", "주문번호", RealGridColumnType.BASIC, 100, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("itemNo", "itemNo", "상품번호", RealGridColumnType.BASIC, 120, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("orderTicketNo", "orderTicketNo", "주문티켓번호", RealGridColumnType.BASIC, 120, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("itemNm1", "itemNm1", "상품명", RealGridColumnType.NONE, 300, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("buyerNm", "buyerNm", "구매자", RealGridColumnType.BASIC, 80, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("buyerMobileNo", "buyerMobileNo", "구매자 연락처", RealGridColumnType.BASIC, 100, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("receiverNm", "receiverNm", "수령인", RealGridColumnType.BASIC, 80, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("shipMobileNo", "shipMobileNo", "수령인 연락처", RealGridColumnType.BASIC, 100, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("sellerItemCd", "sellerItemCd", "업체상품코드", RealGridColumnType.BASIC, 100, true, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("bundleNo", "bundleNo", "배송번호", RealGridColumnType.BASIC, 100, false, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("shipNo", "shipNo", "배송SEQ", RealGridColumnType.BASIC, 80, false, false));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("ticketStatus", "ticketStatus", "티켓상태코드", RealGridColumnType.BASIC, 100, false, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("issueStatus", "issueStatus", "발급상태코드", RealGridColumnType.BASIC, 100, false, true));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("shipStatus", "shipStatus", "주문상태코드", RealGridColumnType.BASIC, 80, false, false));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("linkType", "linkType", "연동유형", RealGridColumnType.BASIC, 80, false, false));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("issueChannel", "issueChannel", "발급채널", RealGridColumnType.BASIC, 80, false, false));
        TICKET_MANAGE_GRID_HEAD.add(new RealGridColumn("siteType", "siteType", "사이트유형", RealGridColumnType.BASIC, 80, false, false));
    }

    /**
     * 이티켓판매관리 Main Page
     */
    @GetMapping("/ticketManageMain")
    public String shipStatusMain(Model model) throws Exception {
        model.addAttribute("shipStatus", sellCommonService.getCode("ship_status"));
        model.addAttribute("ticketStatus", sellCommonService.getCode("ticket_status"));
        model.addAttribute("issueStatus", sellCommonService.getCode("issue_status"));
        model.addAttribute("theclubFrontUrl", theclubFrontUrl);
        model.addAttribute("homeplusFrontUrl", homeplusFrontUrl);
        model.addAttribute("ticketManageGridBaseInfo", new RealGridBaseInfo("ticketManageGridBaseInfo", TICKET_MANAGE_GRID_HEAD, TICKET_MANAGE_GRID_OPTION).toString());

        return "/sell/ticketManageMain";
    }

    /**
     * 이티켓판매관리 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getTicketManageList.json", method = RequestMethod.GET)
    public List<TicketManageListGetDto> getTicketManageList(TicketManageListSetDto ticketManageListSetDto) {
        return ticketManageService.getTicketManageList(ticketManageListSetDto);
    }

    /**
     * 이티켓판매관리 카운트 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getTicketManageCntList.json", method = RequestMethod.GET)
    public List<TicketManageListGetDto> getTicketManageCntList(@RequestParam(value = "schType") String schType) {
        return ticketManageService.getTicketManageCntList(XssPreventer.unescape(schType));
    }

    /**
     * 이티켓판매관리 카운트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getTicketManageCnt.json", method = RequestMethod.GET)
    public TicketManageCntGetDto getTicketManageCnt() {
        return ticketManageService.getTicketManageCnt();
    }

    /**
     * 주문확인
     */
    @ResponseBody
    @PostMapping(value = "/setTicketConfirmOrder.json")
    public ResponseObject<Object> setTicketConfirmOrder(@RequestBody TicketManageConfirmOrderSetDto ticketManageConfirmOrderSetDto) throws Exception {
        return ticketManageService.setTicketConfirmOrder(ticketManageConfirmOrderSetDto);
    }

    /**
     * 발급완료
     */
    @ResponseBody
    @PostMapping(value = "/setTicketComplete.json")
    public ResponseObject<Object> setTicketComplete(@RequestBody TicketManageCompleteSetDto ticketManageCompleteSetDto) throws Exception {
        return ticketManageService.setTicketComplete(ticketManageCompleteSetDto);
    }
}
