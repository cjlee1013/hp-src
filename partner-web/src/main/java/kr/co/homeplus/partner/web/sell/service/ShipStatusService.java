package kr.co.homeplus.partner.web.sell.service;

import java.util.List;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.sell.model.ShipHistoryGetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusCntGetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusListGetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusListSetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusNoReceiveInfoGetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusNoReceiveInfoSetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusNoReceiveSetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusShipCompletePopListGetDto;
import kr.co.homeplus.partner.web.sell.model.shipStatus.ShipStatusShipCompleteSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipStatusService {

  private final ResourceClient resourceClient;
  private final SellCommonService sellCommonService;

  /**
   * 배송현황 리스트 조회
   */
  public List<ShipStatusListGetDto> getShipStatusList(ShipStatusListSetDto shipStatusListSetDto) {
    shipStatusListSetDto.setPartnerId(sellCommonService.getUserCd());

    return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            shipStatusListSetDto,
            "/partner/sell/getShipStatusList",
            new ParameterizedTypeReference<ResponseObject<List<ShipStatusListGetDto>>>(){}).getData();
  }

  /**
   * 배송현황 카운트 리스트 조회
   */
  public List<ShipStatusListGetDto> getShipStatusCntList(String schType) {
    String partnerId = sellCommonService.getUserCd();

    return resourceClient.getForResponseObject(
        ResourceRouteName.SHIPPING,
        "/partner/sell/getShipStatusCntList?schType="+schType+"&partnerId="+partnerId,
        new ParameterizedTypeReference<ResponseObject<List<ShipStatusListGetDto>>>() {}).getData();
  }

  /**
   * 배송현황 카운트 조회
   */
  public ShipStatusCntGetDto getShipStatusCnt() {
    String partnerId = sellCommonService.getUserCd();

    return resourceClient.getForResponseObject(
        ResourceRouteName.SHIPPING,
        "/partner/sell/getShipStatusCnt?partnerId="+partnerId,
        new ParameterizedTypeReference<ResponseObject<ShipStatusCntGetDto>>() {}).getData();
  }

  /**
   * 미수취 신고 정보 조회
   */
  public ShipStatusNoReceiveInfoGetDto getNoReceiveInfo(ShipStatusNoReceiveInfoSetDto shipStatusNoReceiveInfoSetDto) {
    return resourceClient.postForResponseObject(
        ResourceRouteName.ESCROWMNG,
        shipStatusNoReceiveInfoSetDto,
        "/order/orderSearch/noRcvList",
        new ParameterizedTypeReference<ResponseObject<ShipStatusNoReceiveInfoGetDto>>(){}).getData();
  }

  /**
   * 미수취 신고 철회 요청
   */
  public ResponseObject<String> setNoReceive(ShipStatusNoReceiveSetDto shipStatusNoReceiveSetDto) {
    shipStatusNoReceiveSetDto.setChgId("PARTNER");
    return resourceClient.postForResponseObject(
        ResourceRouteName.ESCROWMNG,
        shipStatusNoReceiveSetDto,
        "/order/orderSearch/modifyNoRcvInfo",
        new ParameterizedTypeReference<ResponseObject<String>>(){});
  }

  /**
   * 배송완료 팝업 리스트 조회
   */
  public List<ShipStatusShipCompletePopListGetDto> getShipCompletePopList(ShipStatusShipCompleteSetDto shipStatusShipCompleteSetDto) {
    shipStatusShipCompleteSetDto.setChgId(sellCommonService.getUserCd());

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        shipStatusShipCompleteSetDto,
        "/admin/shipManage/getShipCompletePopList",
        new ParameterizedTypeReference<ResponseObject<List<ShipStatusShipCompletePopListGetDto>>>(){}).getData();
  }

  /**
   * 배송완료
   */
  public ResponseObject<Object> setShipComplete(ShipStatusShipCompleteSetDto shipStatusShipCompleteSetDto) throws Exception {
    shipStatusShipCompleteSetDto.setChgId(sellCommonService.getUserCd());

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        shipStatusShipCompleteSetDto,
        "/partner/sell/setShipComplete",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

  /**
   * 배송 히스토리 조회
   */
  public List<ShipHistoryGetDto> getShipHistory(String dlvCd, String invoiceNo) {
    return resourceClient.getForResponseObject(
        ResourceRouteName.SHIPPING,
        "/admin/shipManage/getShipHistoryList?dlvCd=" + dlvCd + "&invoiceNo=" + invoiceNo,
        new ParameterizedTypeReference<ResponseObject<List<ShipHistoryGetDto>>>() {}).getData();
  }

}