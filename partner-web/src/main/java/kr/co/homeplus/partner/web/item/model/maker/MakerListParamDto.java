package kr.co.homeplus.partner.web.item.model.maker;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MakerListParamDto {
    private String searchType;
    private String searchKeyword;
    private String useYn;
    private String callType;
}
