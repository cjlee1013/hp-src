package kr.co.homeplus.partner.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimDetailItem {

    @NotNull(message = "상품주문번호가 없습니다.")
    @ApiModelProperty(value = "상품주문번호", position = 1, example = "3000012917")
    private String orderItemNo;

    @ApiModelProperty(value = "상품번호", position = 2, example = "10000300000265")
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 3, example = "[상품] 테스트_사라_알라크림치즈")
    private String itemName;

    @ApiModelProperty(value = "옵션명", position = 4, example = "1번옵션 : 옵션입니다. | 2번옵션 : 옵션입니다.")
    private String optItemName;

    @ApiModelProperty(value = "클레임수량", position = 5, example = "1")
    private String claimItemQty;

    @ApiModelProperty(value = "상품금액", position = 6, example = "2000")
    private String itemPrice;

    @ApiModelProperty(value = "클레임 상품번호", position = 7, example = "10000300000265")
    private String claimItemNo;

}
