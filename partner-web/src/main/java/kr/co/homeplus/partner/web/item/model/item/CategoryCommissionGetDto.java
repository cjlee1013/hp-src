package kr.co.homeplus.partner.web.item.model.item;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CategoryCommissionGetDto {

    @ApiModelProperty(value = "대분류", position = 1)
    @RealGridColumnInfo(headText = "대분류", width = 100, sortable = true)
    private String lcateNm;

    @ApiModelProperty(value = "중분류", position = 2)
    @RealGridColumnInfo(headText = "중분류", width = 100, sortable = true)
    private String mcateNm;

    @ApiModelProperty(value = "소분류", position = 3)
    @RealGridColumnInfo(headText = "소분류", width = 100, sortable = true)
    private String scateNm;

    @ApiModelProperty(value = "세분류", position = 4)
    @RealGridColumnInfo(headText = "세분류", width = 100, sortable = true)
    private String dcateNm;

    @ApiModelProperty(value = "수수료", position = 5)
    @RealGridColumnInfo(headText = "수수료", width=100, sortable = true)
    private String commissionRate;

}
