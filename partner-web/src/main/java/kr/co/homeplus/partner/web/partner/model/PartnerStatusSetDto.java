package kr.co.homeplus.partner.web.partner.model;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@ApiModel("판매업체 상태 Set Entry")
@Getter
@Setter
@Builder
public class PartnerStatusSetDto {

	//판매업체ID
	private String partnerId;

	//판매업체 상태
	private String partnerStatus;

	//변경사유
	private String regReason;

	//수정자
	private String userId;
}
