package kr.co.homeplus.partner.web.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@ApiModel("판매업체 계정상태 Set Entry")
@Getter
@Setter
@Builder
public class PartnerAccountStatusSetDto {

	@ApiModelProperty(value = "판매업체ID", required = true)
	@NotEmpty(message = "판매업체ID")
	private String partnerId;

	@ApiModelProperty(value = "판매업체 계정상태", required = true)
	@NotEmpty(message = "판매업체 계정상태")
	private String accountStatus;

	@ApiModelProperty(value = "변경사유", required = true)
	@NotEmpty(message = "변경사유")
	private String regReason;

	@ApiModelProperty(value = "수정자", required = true)
	@NotEmpty(message = "수정자")
	private String userId;
}
