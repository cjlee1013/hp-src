package kr.co.homeplus.partner.web.harm.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * 위해상품 dto
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HarmProdDto {

    private String cdInsptmachiName;

    private String processorName;

    private String cdInsptmachi;

    private String noDocument;

    private String seq;

    private String nmRpttype;

    private String nmProd;

    private String nmManufacupso;

    private String biznoManufacupso;

    private String nmSalerupso;

    private String biznoSalerupso;

    private String barcode;

    private String dtRecv;

    private String tmRecv;

    private String dhRecv;

    private String statusResultCode;

    private String statusResult;

    private String dtSendresult;

    private String tmSendresult;

    private String dhSendresult;
}
