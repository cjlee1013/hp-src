package kr.co.homeplus.partner.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//셀러상품 상세 조회
public class ItemParamDto {

    //상품번호
    private String itemNo;

    //거래유형 (DS : 업체상품 , TD : 매장상품)
    private String mallType;

    //점포ID
    private Integer storeId;

    //점포유형
    private String storeType;

    //판매업체 ID
    private String partnerId;
}
