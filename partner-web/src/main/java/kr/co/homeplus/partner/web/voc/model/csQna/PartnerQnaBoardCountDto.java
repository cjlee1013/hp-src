package kr.co.homeplus.partner.web.voc.model.csQna;

import lombok.Data;

@Data
public class PartnerQnaBoardCountDto {

    private String qnaStatus;
    private long cnt;

}
