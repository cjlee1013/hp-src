package kr.co.homeplus.partner.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimDetailGetDto {

    @ApiModelProperty(value = "클레임타입코드", example = "C", position = 1)
    private String claimTypeCode;

    @ApiModelProperty(value = "클레임타입", example = "취소", position = 2)
    private String claimType;

    @ApiModelProperty(value = "처리상태", example = "신청", position = 3)
    private String claimStatus;

    @ApiModelProperty(value = "처리상태", example = "C4", position = 5)
    private String claimStatusCode;

    @ApiModelProperty(value = "배송번호", example = "6", position = 7)
    private String bundleNo;

    @ApiModelProperty(value = "클레임번호", example = "8", position = 9)
    private long claimBundleNo;

    @ApiModelProperty(value = "클레임 사유", example = "결품(체크아웃누락)", position = 10)
    private String claimReasonType;

    @ApiModelProperty(value = "클레임 상세사유", example = "구매취소합니다.", position = 11)
    private String claimReasonDetail;

    @ApiModelProperty(value = "첨부파일1", example = "/cl/f103d615-8150-4a57-945d-c4ee4ba053d4", position = 12)
    private String uploadFileName;

    @ApiModelProperty(value = "첨부파일2", example = "/cl/f103d615-8150-4a57-945d-c4ee4ba053d4", position = 13)
    private String uploadFileName2;

    @ApiModelProperty(value = "첨부파일3", example = "/cl/f103d615-8150-4a57-945d-c4ee4ba053d4", position = 14)
    private String uploadFileName3;

    @ApiModelProperty(value = "배송비 동봉 여부 Y:택배동봉, N:환불금액 차감", example = "Y", position = 15)
    private String claimShipFeeEnclose;

    @ApiModelProperty(value = "클레임 귀책자", example = "판매자", position = 16)
    private String whoReason;

    @ApiModelProperty(value = "추가 배송비", example = "3000", position = 17)
    private long totAddShipPrice;

    @ApiModelProperty(value = "추가 도서산간 배송비", example = "3000", position = 18)
    private long totAddIslandShipPrice;

    @ApiModelProperty(value="반품 수거지", position = 19)
    private ClaimPickShippingDto claimPickShippingDto;

    @ApiModelProperty(value="교환 수거지", position = 20)
    private ClaimExchShippingDto claimExchShippingDto;

    @ApiModelProperty(value = "클레임 상품 리스트", position = 21)
    private List<ClaimDetailItem> claimDetailItemList;

}
