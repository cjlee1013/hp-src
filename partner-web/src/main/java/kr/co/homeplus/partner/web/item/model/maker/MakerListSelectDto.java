package kr.co.homeplus.partner.web.item.model.maker;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MakerListSelectDto {

    private Long makerNo;
    private String makerNm;
    private String siteUrl;
    private String useYn;
    private String useYnTxt;
    private String regNm;
    private String regDt;
    private String chgNm;
    private String chgDt;
}
