package kr.co.homeplus.partner.web.core.controller;

import javax.annotation.PostConstruct;
import kr.co.homeplus.partner.web.core.selleragree.service.SellerAgreeService;
import kr.co.homeplus.partner.web.core.service.Check90daysSamePasswordService;
import kr.co.homeplus.partner.web.core.usermenu.MenuService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 파트너 홈 컨트롤러
 */
@Slf4j
@Controller
@RequiredArgsConstructor
public class HomeController {

    private final MenuService menuService;
    private final SellerAgreeService sellerAgreeService;

    @PostConstruct
    public void init() {
        menuService.makeMenus();
    }

    /**
     * 메인화면(홈)
     *
     * @return
     */
    @GetMapping(value = "/")
    public String home(Model model) {
        model.addAttribute(menuService.getMenus());
        return "/home/main";
    }

    /**
     * 파트너 동의처리
     */
    @ResponseBody
    @GetMapping(value = "/partner/setAccept.json", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseObject setAccept() {
        return sellerAgreeService.setAccept();
    }


}