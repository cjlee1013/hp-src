package kr.co.homeplus.partner.web.core.dto;

import java.util.List;
import lombok.Data;

@Data
public class AuthMenuListDto {
    // 메뉴관련 정보
    private int menuSeq;
    private int systemSeq;
    private int menuDepth;

    // 상위부서
    private int parentsMenuSeq;
    private int menuOrder;

    // 메뉴 출력 및 사용관련 정보
    private String menuUrl;
    private String menuName;
    private String isPersonal;
    private String isExcel;

    // 메뉴 출력관련 정보 : 소유한 Role 번호
    private int roleSeq;

    // 메뉴 출력관련 정보 : 사용가능 여부 ( y | n )
    private String isHold;

    // 하위메뉴 리스트
    private List<AuthMenuListDto> childMenuList;

    /** 업무용 VDI 접속 여부(y|n) **/
    private String isAccessVdi;

    /** 고객만족센터 접속 여부(y|n) **/
    private String isAccessCustomerCenter;
}
