package kr.co.homeplus.partner.web.core.config;

import ch.qos.logback.classic.helpers.MDCInsertingServletFilter;
import com.navercorp.lucy.security.xss.servletfilter.XssEscapeServletFilter;
import javax.servlet.Filter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * web config
 */
@Configuration
public class WebConfig {
    @Bean
    public FilterRegistrationBean<Filter> xssEscapeServletFilter() {
        FilterRegistrationBean<Filter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new XssEscapeServletFilter());
        registrationBean.addUrlPatterns("/*");
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<Filter> mdcInsertingServletFilter() {
        FilterRegistrationBean<Filter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new MDCInsertingServletFilter());
        registrationBean.addUrlPatterns("/*");
        return registrationBean;
    }
}
