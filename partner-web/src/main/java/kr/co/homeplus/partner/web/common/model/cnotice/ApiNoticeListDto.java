package kr.co.homeplus.partner.web.common.model.cnotice;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiNoticeListDto {
    private Long num;
    private Long noticeNo;
    private String kindNm;
    private String topYn;
    private String noticeTitle;
    private String hits;
    private String regDt;
}
