package kr.co.homeplus.partner.web.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserPwParamGetDto {

    //제휴업체핸드폰
    private String partnerId;

    //상호명
    private String partnerNm;

    //사업자번호
    private String partnerNo;

}
