package kr.co.homeplus.partner.web.core.exception;

/**
 * 동일 비밀번호를 90일 이상 사용여부를 체크하는 인터셉터에서 발생된 예외 처리용 Exception
 */
public class Check90DaysSamePasswordInterceptorException extends RuntimeException {

    public Check90DaysSamePasswordInterceptorException(String message) {
        super(message);
    }
}
