package kr.co.homeplus.partner.web.item.model.item;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품관리 점포정보 Get Entry")
public class ItemStoreListGetDto {

    @ApiModelProperty(value = "점포ID")
    @NotEmpty(message = "점포ID")
    private String storeId;

    @ApiModelProperty(value = "점포명")
    private String storeNm;

    @ApiModelProperty(value = "점포타입")
    private String storeType;

    @ApiModelProperty(value = "점포타입명")
    private String storeTypeNm;

    @ApiModelProperty(value = "점포구분(일반점:NOR, 택배점:DLV)")
    private String storeKind;

    @ApiModelProperty(value = "점포구분명(일반점:NOR, 택배점:DLV)")
    private String storeKindNm;

    @ApiModelProperty(value = "판매가격")
    private String salePrice;

    @ApiModelProperty(value = "재고수량")
    private Integer stockQty;

    @ApiModelProperty(value = "재고관리유형")
    private String stockType;

    @ApiModelProperty(value = "재고관리유형명")
    private String stockTypeNm;

    @ApiModelProperty(value = "온라인취급여부명")
    private String pfrYnNm;

    @ApiModelProperty(value = "취급중지")
    private String stopDealYn;

    @ApiModelProperty(value = "취급중지명")
    private String stopDealYnNm;

    @ApiModelProperty(value = "중지제한여부 (Y: 무제한, N: 기간제한)")
    private String stopLimitYn;

    @ApiModelProperty(value = "중지시작일")
    private String stopStartDt;

    @ApiModelProperty(value = "중지종료일")
    private String stopEndDt;

    @ApiModelProperty(value = "중지사유")
    private String stopReason;

    @ApiModelProperty(value = "자차배송여부")
    private String drctYn;

    @ApiModelProperty(value = "자차배송여부명")
    private String drctYnNm;

    @ApiModelProperty(value = "자차배송 배송정택번호")
    private Long drctShipPolicyNo;

    @ApiModelProperty(value = "매장택배여부")
    private String dlvYn;

    @ApiModelProperty(value = "매장택배여부명")
    private String dlvYnNm;

    @ApiModelProperty(value = "퀵배송여부")
    @NotEmpty(message = "퀵배송여부")
    private String quickYn;

    @ApiModelProperty(value = "퀵배송여부명")
    private String quickYnNm;

    @ApiModelProperty(value = "퀵배송 배송정책번호")
    private Long quickShipPolicyNo;

    @ApiModelProperty(value = "매장택배 배송정책번호")
    private Long dlvShipPolicyNo;

    @ApiModelProperty(value = "방문수령여부")
    private String pickYn;

    @ApiModelProperty(value = "방문수령여부명")
    private String pickYnNm;

    @ApiModelProperty(value = "방문수령 배송정책번호")
    private Long pickShipPolicyNo;

    @ApiModelProperty(value = "안심번호 사용여부명")
    private String safeNumberUseYnNm;

    @ApiModelProperty(value = "FC 상태구분(00:미등록,10:IN승인대기,15:IN승인거부,20:IN승인완료,30:IN확정,40:FC판매,50:OUT확정대기,60:OUT확정,99:PACK미등록)-GOD_FC_MST.STATUS_FLAG")
    private String fcStatusFlagNm;

    @ApiModelProperty(value = "새벽배송여부(Y: 설정, N: 설정안함)")
    private String auroraYnNm;

    @ApiModelProperty(value = "등록일")
    private String regDt;

    @ApiModelProperty(value = "등록자")
    private String regNm;

}
