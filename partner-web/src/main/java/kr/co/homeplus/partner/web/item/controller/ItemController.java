package kr.co.homeplus.partner.web.item.controller;

import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.partner.web.common.model.common.ResponseResult;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ChannelConstants;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.utility.StringUtil;
import kr.co.homeplus.partner.web.item.model.item.CategoryCommissionGetDto;
import kr.co.homeplus.partner.web.item.model.item.ItemBoardCountDto;
import kr.co.homeplus.partner.web.item.model.item.ItemCertParamDto;
import kr.co.homeplus.partner.web.item.model.item.ItemGetDto;
import kr.co.homeplus.partner.web.item.model.item.ItemListGetDto;
import kr.co.homeplus.partner.web.item.model.item.ItemListParamDto;
import kr.co.homeplus.partner.web.item.model.item.ItemParamDto;
import kr.co.homeplus.partner.web.item.model.item.ItemSetDto;
import kr.co.homeplus.partner.web.item.model.item.MngNoticeGroupListGetDto;
import kr.co.homeplus.partner.web.item.model.item.MngNoticeListGetDto;
import kr.co.homeplus.partner.web.partner.model.PartnerAuthGetDto;
import kr.co.homeplus.partner.web.partner.model.shipPolicy.ShipPolicyListDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Slf4j
@RequestMapping("/item")
public class ItemController {

    private final ResourceClient resourceClient;
    private final CodeService codeService;
    private final CertificationService certificationService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUploadUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgUrl;

    @Value("${plus.resource-routes.upload.url}")
    private String uploadUrl;


    public ItemController(ResourceClient resourceClient,
        CodeService codeService,
        CertificationService certificationService
    ) {
        this.resourceClient = resourceClient;
        this.codeService = codeService;
        this.certificationService = certificationService;
    }

    /**
     * 상품관리 > 상품공지관리
     */
    @RequestMapping(value = "/itemMain", method = RequestMethod.GET)
    public String itemMain(Model model , @RequestParam(value="itemStatus", required = false) String itemStatus ) throws Exception {

        codeService.getCodeModel(model,
            "use_yn"               // 검색일자타입
            , "partner_item_status"         // 판매상태
            , "disp_yn"                     // 노출여부
            , "set_yn"                      // 설정여부
        );

        model.addAttribute("itemStatus", itemStatus);
        model.addAllAttributes(
            RealGridHelper.create("itemListGridBaseInfo", ItemListGetDto.class));

        return "/item/itemMain";
    }

    /**
     * 상품관리 > 상품공지관리
     */
    @RequestMapping(value = "/itemSetMain", method = RequestMethod.GET)
    public String itemSetMain(Model model
        , @RequestParam(value="itemNo") String itemNo) throws Exception {

        codeService.getCodeModel(model,
            "use_yn"         // 사용여부
            ,   "item_status"         // 상품상태
            ,   "disp_yn"             // 노출여부
            ,   "set_yn"              // 설정여부
            ,   "sale_period_yn"      // 판매기간 설정여부
            ,   "limit_yn"            // 제한여부
            ,   "gift_yn"             // 선물하기여부
            ,   "target_yn"           // 대상여부
            ,   "item_img_type"       // 상품이미지유형
            ,   "tax_yn"              // 과세여부
            ,   "commission_type"     // 수수료유형
            ,   "limit_duration"      // 기간제한
            ,   "holiday_except"      // 공휴일제외유형
            ,   "pr_type"             // 셀링포인트유형
            ,   "rsv_type"            // 예약유형
            ,   "reg_yn"              // 등록여부
            ,   "stock_type"          // 재고관리유형
            ,   "adult_type"          // 성인상품유형
            ,   "proof_file"          // 증빙서류
            ,   "item_is_cert"        // 상품인증여부
            ,   "item_cert_type"      // 상품읹증유형
            ,   "item_cert_group"     // 상품인증그룹
            ,   "item_exempt_type"    // 상품인증면제유형
            ,   "item_cert_et_cer"    // 안전인증 소비효율등급
            ,   "item_ship_attr"      // 상품배송속성

            ,   "ship_method"         // 배송유형
            ,   "ship_kind"           // 배송비유형
            ,   "ship_type"           // 배송유형
            ,   "prepayment_yn"       // 선결제여부
            ,   "ship_area"           // 배송가능지역
            ,   "diff_yn"             // 수량별 차등
            ,   "holiday_except_yn"   // 휴일여부
            ,   "safe_number_use_yn"  // 안심번호 사용여부
        );

        model.addAttribute("itemNo", itemNo);
        if( StringUtil.isEmpty(itemNo) ){
            model.addAttribute("isMod", "N");
            model.addAttribute("title", "상품등록");
        }else {
            model.addAttribute("isMod", "Y");
            model.addAttribute("title", "상품수정");
        }

        model.addAttribute("hmpImgUrl", this.hmpImgUrl);

        //파트너별 상품 권한관련 조회
        String partnerId = certificationService.getLoginUerInfo().getPartnerId();

        PartnerAuthGetDto partnerAuthGetDto = resourceClient.getForResponseObject(
            ResourceRouteName.ITEM
            , "/po/partner/getPartnerTypesList?partnerId=" + partnerId
            , new ParameterizedTypeReference<ResponseObject<PartnerAuthGetDto>>() {
            }).getData();

        model.addAttribute("itemType", partnerAuthGetDto.getPartnerTypeList());
        model.addAttribute("saleType", partnerAuthGetDto.getSaleType());

        //고시정보
       List<MngNoticeGroupListGetDto> gnoticeList = resourceClient.getForResponseObject(ResourceRouteName.ITEM,
                "/po/item/getMngNoticeGroupList",
                new ParameterizedTypeReference<ResponseObject<List<MngNoticeGroupListGetDto>>>() {
                }).getData();

       model.addAttribute("gnoticeList" , gnoticeList);

        //그리드
        model.addAllAttributes(
            RealGridHelper.create("shipPolicyListGridBaseInfo", ShipPolicyListDto.class));

        model.addAllAttributes(
            RealGridHelper.create("categoryCommissionListGridBaseInfo", CategoryCommissionGetDto.class));

        return "/item/itemSetMain";
    }

    @ResponseBody
    @RequestMapping(value = {"/getItemList.json"}, method = RequestMethod.POST)
    public List<ItemListGetDto> getItemList(@RequestBody @Valid ItemListParamDto itemListParamDto) {
        String apiUri = "/po/item/getItemList";
        itemListParamDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemListGetDto>>>() {};
        return (List<ItemListGetDto>) resourceClient.postForResponseObject(ResourceRouteName.ITEM,itemListParamDto, apiUri, typeReference).getData();
    }

    @ResponseBody
    @RequestMapping(value = {"/getItemBoardCount.json"}, method = RequestMethod.GET)
    public List<ItemBoardCountDto> getItemBoardCount() {

        String apiUri = "/po/item/getItemBoardCount";
        String partnerId = certificationService.getLoginUerInfo().getPartnerId();

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemBoardCountDto>>>() {};
        return (List<ItemBoardCountDto>) resourceClient
            .getForResponseObject(ResourceRouteName.ITEM, apiUri + "?partnerId=" + partnerId,
                typeReference).getData();
    }

    /**
     * 상품관리 > 상품 등록/수정 > 상품조회(단일)
     * @param itemParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getDetail.json", method = RequestMethod.GET)
    public ItemGetDto getDetail(ItemParamDto itemParamDto) {

        itemParamDto.setMallType("DS");
        itemParamDto.setStoreId(0);
        itemParamDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM,
            StringUtil.getRequestString("/po/item/getDetail", ItemParamDto.class, itemParamDto),
            new ParameterizedTypeReference<ResponseObject<ItemGetDto>>() {
            }).getData();
    }

    /**
     * 상품정보고시관리 그룹 조회
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getMngNoticeGroupList.json", method = RequestMethod.GET)
    public List<MngNoticeGroupListGetDto> getMngNoticeGroupList() {
        return resourceClient.getForResponseObject(ResourceRouteName.ITEM,
            "/po/item/getMngNoticeGroupList",
            new ParameterizedTypeReference<ResponseObject<List<MngNoticeGroupListGetDto>>>() {
            }).getData();
    }

    /**
     * 상품정보고시관리 조회
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getMngNoticeList.json", method = RequestMethod.GET)
    public List<MngNoticeListGetDto> getMngNoticeList(
        @RequestParam(name = "gnoticeNo") Integer gnoticeNo
    ) {
        return resourceClient.getForResponseObject(ResourceRouteName.ITEM,
            "/po/item/getMngNoticeList?gnoticeNo="+gnoticeNo,
            new ParameterizedTypeReference<ResponseObject<List<MngNoticeListGetDto>>>() {
            }).getData();
    }

    /**
     * 상품관리 > 상품 등록/수정 > 상품등록
     * @param itemSetDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/setItem.json"}, method = RequestMethod.POST)
    public Object setItem(@RequestBody @Valid ItemSetDto itemSetDto) {

        itemSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        itemSetDto.setUserId(ChannelConstants.USERID);
        itemSetDto.setChannel(ChannelConstants.PARTNER);

        String apiUri = "/po/item/setItem";

        if (itemSetDto.getTempYn().equals("Y")) {
            apiUri = "/po/item/setItemTemp";
        }

        ResponseObject<ResponseResult> responseObject = resourceClient
            .postForResponseObject(ResourceRouteName.ITEM, itemSetDto, apiUri,
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                });

        return responseObject.getData();
    }

    /**
     * 상품 안전인증 조회
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/checkCert.json", method = RequestMethod.GET)
    public ResponseObject checkItemCert(ItemCertParamDto itemCertParamDto) {
        ResponseObject res = resourceClient.getForResponseObject(ResourceRouteName.ITEM,
            StringUtil.getRequestString("/po/item/checkItemCert", ItemCertParamDto.class, itemCertParamDto));
        return res;
    }
}