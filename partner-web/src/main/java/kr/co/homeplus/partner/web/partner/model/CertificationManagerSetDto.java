package kr.co.homeplus.partner.web.partner.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CertificationManagerSetDto {

    private String certificationSeq;
    private String certificationMngNm;
    private String certificationMngPhone;
    private String certificationState;

}
