package kr.co.homeplus.partner.web.partner.model.shipPolicy;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShipPolicyParamDto {

    //판매업체ID
    private String partnerId;

    //점포ID
    private long storeId;

    //배송정책번호
    private Long shipPolicyNo;
}
