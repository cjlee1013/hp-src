package kr.co.homeplus.partner.web.core.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 1단계 로그인 파라미터
 */
@Getter
@Setter
public class Step1LoginParam {

    @NotNull(message = "아이디를 입력하세요.")
    @Length(min = 5, max = 12, message = "아이디는 5에서 12자리 이내로 입력해주세요.")
    private String partnerId;

    @NotNull(message = "비밀번호를 입력하세요.")
    private String partnerPw;

    @ApiModelProperty(value = "클라이언트 ip")
    private String ip;
}
