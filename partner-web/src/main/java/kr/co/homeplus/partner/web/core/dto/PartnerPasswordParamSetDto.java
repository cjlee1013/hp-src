package kr.co.homeplus.partner.web.core.dto;

import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PartnerPasswordParamSetDto {

    @NotEmpty(message = "판매자ID")
    private String partnerId;

    @NotEmpty(message = "영업담당자 성명")
    private String affiMngNm;

    @NotEmpty(message = "모바일토큰")
    private String certToken;

    @NotEmpty(message = "인증번호")
    private String certNo;

    @NotEmpty(message = "휴대폰 번호")
    private String mobile;
}
