package kr.co.homeplus.partner.web.partner.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
//외부연동 > API KEY 발급 Set Entry
public class PartnerApiKeySetDto {

    private String userId;

    private String partnerId;

    private String agencyCd;

    private long plusapiSeq;

    private String encKey;

}
