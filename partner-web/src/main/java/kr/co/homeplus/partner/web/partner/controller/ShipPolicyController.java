package kr.co.homeplus.partner.web.partner.controller;

import java.util.List;
import kr.co.homeplus.partner.web.common.model.common.ResponseResult;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ChannelConstants;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.utility.StringUtil;
import kr.co.homeplus.partner.web.partner.model.shipPolicy.PartnerSellerShipListGetDto;
import kr.co.homeplus.partner.web.partner.model.shipPolicy.ShipPolicyListDto;
import kr.co.homeplus.partner.web.partner.model.shipPolicy.ShipPolicyParamDto;
import kr.co.homeplus.partner.web.partner.model.shipPolicy.ShipiPolicySetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/partner")
public class ShipPolicyController {

    private final ResourceClient resourceClient;
    private final CertificationService certificationService;

    public ShipPolicyController(ResourceClient resourceClient,
        CertificationService certificationService
    ) {
        this.resourceClient = resourceClient;
        this.certificationService = certificationService;
    }

    /**
     * 배송정책 리스트 조회
     * @param shipPolicyParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getShipPolicyList.json"}, method = RequestMethod.GET)
    public List<ShipPolicyListDto> getShipPolicyList(ShipPolicyParamDto shipPolicyParamDto) {
        String apiUrl = "/po/partner/getPartnerShipPolicyList";

        shipPolicyParamDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        shipPolicyParamDto.setStoreId(0);

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ShipPolicyListDto>>>(){};
        return (List<ShipPolicyListDto>)resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUrl, ShipPolicyParamDto.class, shipPolicyParamDto), typeReference).getData();
    }

    /**
     * 배송정책 Select Box 리스트 조회
     * @param useYn
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getSellerShipList.json", method = RequestMethod.GET)
    public List<PartnerSellerShipListGetDto> getSellerShipList(@RequestParam(value = "useYn", defaultValue = "", required = false) String useYn) {
        String addParam = "";
        if(useYn != "") {
            addParam = "&useYn=" + useYn;
        }
        String partnerId = certificationService.getLoginUerInfo().getPartnerId();

        String apiUri = "/partner/getSellerShipList?partnerId=" + partnerId + addParam;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PartnerSellerShipListGetDto>>>() {};
        return (List<PartnerSellerShipListGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, typeReference).getData();
    }

    /**
     * 배송정책 등록/수정
     * @param shipiPolicySetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/setShipPolicy.json")
    public ResponseResult setShipPolicy(ShipiPolicySetDto shipiPolicySetDto) {

        String apiUrl = "/po/partner/setShipPolicy";

        shipiPolicySetDto.setStoreId(0);
        shipiPolicySetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        shipiPolicySetDto.setUserId(ChannelConstants.USERID);

        shipiPolicySetDto.setDefaultYn(StringUtil.nvl(shipiPolicySetDto.getDefaultYn(),"N"));
        shipiPolicySetDto.setHolidayExceptYn(StringUtil.nvl(shipiPolicySetDto.getHolidayExceptYn(),"N"));

        return resourceClient.postForResponseObject(ResourceRouteName.ITEM,
            shipiPolicySetDto
            , apiUrl, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }
}
