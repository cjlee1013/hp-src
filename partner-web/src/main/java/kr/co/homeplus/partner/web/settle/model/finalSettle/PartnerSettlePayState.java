package kr.co.homeplus.partner.web.settle.model.finalSettle;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum PartnerSettlePayState {
    //정신관리 > 파트너 정산 지급상태
    SETTLE_PAY_STATUS_WAIT("1", "지급대기"),
    SETTLE_PAY_STATUS_COMPLETE("2", "지급완료"),
    SETTLE_PAY_STATUS_POSTPONE("3", "지급보류"),;

    private String code;
    private String name;
}
