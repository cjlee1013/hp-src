package kr.co.homeplus.partner.web.item.model.item;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

/**
 * 옵션 정보
 */
@Getter
@Setter
@ApiModel("옵션정보")
public class ItemOptionValueGetDto {
	@ApiModelProperty(value = "옵션번호")
	private long   optNo;

	@ApiModelProperty(value = "상품번호")
	private long   itemNo;

	@ApiModelProperty(value = "옵션1값")
	@NotEmpty(message = "옵션1값")
	@Size(max = 25, message = "옵션1값")
	private String opt1Val;

	@ApiModelProperty(value = "옵션2값")
	@Size(max = 25, message = "옵션2값")
	private String opt2Val;

	@ApiModelProperty(value = "옵션의 상대가격")
	private int    optPrice;

	@ApiModelProperty(value = "우선순위")
	private int    priority;

	@ApiModelProperty(value = "노출여부(Y:노출,N:비노출)")
	private String dispYn;

	@ApiModelProperty(value = "Hyper 전시여부")
	private String hyperDispYn;

	@ApiModelProperty(value = "CLUB 전시여부")
	private String clubDispYn;

	@ApiModelProperty(value = "Express 전시여부")
	private String expDispYn;

	@ApiModelProperty(value = "재고수량(DS용)")
	@Min(value = 0, message = "재고수량(DS용)")
	private int    stockQty;

	@ApiModelProperty(value = "업체관리코드")
	@Size(max = 50, message = "업체관리코드")
	private String sellerOptCd;

	@ApiModelProperty(value = "재고수량 연동 (B: 대기, A: 성공, F: 실패) escrow-api")
	private String syncStock;

	@ApiModelProperty(value = "등록자")
	private String regId;

	@ApiModelProperty(value = "등록일")
	private String regDt;

	@ApiModelProperty(value = "수정자")
	private String chgId;

	@ApiModelProperty(value = "수정일")
	private String chgDt;

	@ApiModelProperty(value = "등록자명")
	private String regNm;

	@ApiModelProperty(value = "수정자명")
	private String chgNm;

	@ApiModelProperty(value = "노출여부명")
	private String dispYnNm;

	@ApiModelProperty(value = "사용여부")
	private String useYn;

	@ApiModelProperty(value = "사용여부명")
	private String useYnNm;

	/**
	 * 판매된 수량
	 */
	@ApiModelProperty(value = "판매된 수량")
	private int salesQty;
}
