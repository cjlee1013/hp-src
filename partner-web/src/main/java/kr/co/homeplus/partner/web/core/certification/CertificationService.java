package kr.co.homeplus.partner.web.core.certification;

import kr.co.homeplus.partner.web.core.dto.MobileOccupationValidParam;
import kr.co.homeplus.partner.web.core.dto.Step2LoginParam;
import kr.co.homeplus.partner.web.core.dto.Step1LoginInfo;
import kr.co.homeplus.partner.web.core.dto.Step1LoginParam;
import kr.co.homeplus.partner.web.core.dto.UserInfo;
import org.springframework.validation.BindingResult;

/**
 * 인증 관련기능 서비스
 */
public interface CertificationService {

    /**
     * 쿠키를 가지고 유저의 로그인 처리 진행.
     *
     * @param step2LoginParam LoginParam
     * @return LoginResult
     */
    boolean getStep2Login(Step2LoginParam step2LoginParam, BindingResult bindingResult);

    /**
     * 파트너 1단계 로그인
     *
     * @param param 로그인 파라미터
     * @param bindingResult bindingResult
     * @return {@link Step1LoginInfo}
     */
    Step1LoginInfo getStep1Login(Step1LoginParam param, BindingResult bindingResult);

    /**
     * 로그인 얀장을 위해 로그인한 유저의 로그인정보를 조회합니다.
     *
     * @return UserInfo 유저 정보
     */
    boolean getLoginExtension();


    /**
     * 로그인된 유저의 로그인정보 반환
     *
     * @return UserInfo
     */
    UserInfo getLoginUerInfo();

    /**
     * 로그아웃, 세션쿠키 및 로그인 세션 캐시를 만료처리 합니다.
     */
    void processUserLogout();

    /**
     * 회원 비밀번호 검사
     * @param loginParam
     * @return
     */
    Boolean checkLoginFromUsermng(Step1LoginParam loginParam);

    /**
     * 파트너 로그인 휴대폰 인증번호 검증요청
     * @param param 파라미터
     * @return 인증 성공시 '인증완료 체크용 토큰'을 응답
     */
    String verifyMobileCertNo(MobileOccupationValidParam param);
}
