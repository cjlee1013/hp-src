package kr.co.homeplus.partner.web.core.usermenu.impl;

import java.util.List;
import kr.co.homeplus.partner.web.core.constants.PartnerConstants;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.dto.AuthMenuListDto;
import kr.co.homeplus.partner.web.core.dto.UserInfo;
import kr.co.homeplus.partner.web.core.usermenu.UserMenuService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserMenuServiceImpl implements UserMenuService {

    private final ResourceClient resourceClient;

    public UserMenuServiceImpl(ResourceClient resourceClient) {
        this.resourceClient = resourceClient;
    }

    @Override
    public List<AuthMenuListDto> getUserPermitMenuList(UserInfo userInfo) {

        //uri 설정 : "/permit/menus"
        StringBuilder uri = new StringBuilder(PartnerConstants.AUTH_PERMIT_MENU_URI);
        uri.append("?userCd=").append(userInfo.getUserCd());
        uri.append("&systemScope=").append(userInfo.getSystemScope());

        //사용자가 접근가능한 메뉴리스트 조회
        ResponseObject<List<AuthMenuListDto>> responseObject = resourceClient.getForResponseObject(
                ResourceRouteName.AUTHORITY
                , uri.toString()
                , new ParameterizedTypeReference<>() {
            }
        );

        return responseObject.getData();
    }
}
