package kr.co.homeplus.partner.web.core.exception;

import kr.co.homeplus.partner.web.core.dto.Step1LoginParam;

public class LongTermUnusedLockException extends RuntimeException {

    private final Step1LoginParam loginParam;
    private String returnCode;

    public LongTermUnusedLockException(final Step1LoginParam loginParam, final String returnCode,
        String returnMessage) {

        super(returnMessage);
        this.loginParam = loginParam;
        this.returnCode = returnCode;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public Step1LoginParam getLoginParam() {
        return loginParam;
    }
}
