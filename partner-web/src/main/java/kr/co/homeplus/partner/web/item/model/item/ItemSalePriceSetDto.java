package kr.co.homeplus.partner.web.item.model.item;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 판배가격 일괄 변경
 */
@Getter
@Setter
public class ItemSalePriceSetDto {

	//상품번호
	private List<String> itemNo;

	//노출여부
	private String salePrice;

	//판매자ID
	private String partnerId;

	//등록|수정자
	private String userId;
}
