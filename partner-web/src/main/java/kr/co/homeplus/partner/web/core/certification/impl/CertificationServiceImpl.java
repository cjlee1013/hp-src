package kr.co.homeplus.partner.web.core.certification.impl;


import java.util.List;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.PartnerConstants;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.dto.MobileOccupationValidParam;
import kr.co.homeplus.partner.web.core.dto.Step2LoginParam;
import kr.co.homeplus.partner.web.core.dto.LoginResult;
import kr.co.homeplus.partner.web.core.dto.PartnerCookieDto;
import kr.co.homeplus.partner.web.core.dto.Step1LoginInfo;
import kr.co.homeplus.partner.web.core.dto.Step1LoginParam;
import kr.co.homeplus.partner.web.core.dto.UserInfo;
import kr.co.homeplus.partner.web.core.exception.CertificationException;
import kr.co.homeplus.partner.web.core.exception.LoginFailCountExceededLockException;
import kr.co.homeplus.partner.web.core.exception.MobileCertNoVerifyFailedException;
import kr.co.homeplus.partner.web.core.exception.SessionNotFoundException;
import kr.co.homeplus.partner.web.core.exception.LongTermUnusedLockException;
import kr.co.homeplus.partner.web.core.service.LoginCookieService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.header.WebHeaderKey;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.WebCookieUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

/**
 * 인증 관련기능의 서비스 구현체
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CertificationServiceImpl implements CertificationService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    private static final String RETURN_CODE_LOGIN_FAIL_COUNT_EXCEEDED_LOCK = "1014";
    private static final String RETURN_CODE_LONG_TERM_UNUSED_LOCK = "1019";

    @Override
    public boolean getStep2Login(final Step2LoginParam param, final BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            List<ObjectError> errorList = bindingResult.getAllErrors();
            throw new CertificationException(param, errorList.get(0).getDefaultMessage());
        }

        final LoginResult loginResult = getStep2LoginFromUsermng(param);

        //로그인 쿠키 생성
        createPartnerLoginCookies(loginResult.getSetCookieList());

        //아이디 저장 쿠키생성
        if (param.isChkSavePartnerId()) {
            loginCookieService.createIdSaveCookie(param.getPartnerId());
        } else {
            loginCookieService.removeIdSaveCookie();
        }
        //로그인 일시 업데이트
        updatePartnerLoginDateFromItem(param);

        return true;
    }

    @Override
    public Step1LoginInfo getStep1Login(final Step1LoginParam param,
        final BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            List<ObjectError> errorList = bindingResult.getAllErrors();
            throw new CertificationException(param, errorList.get(0).getDefaultMessage());
        }

        final Step1LoginInfo loginInfo = getStep1LoginFromUsermng(param);

        return loginInfo;
    }

    @Override
    public boolean getLoginExtension() {
        final String sessionId = WebCookieUtils
            .getCookieValue(PartnerConstants.PARTNER_SESSION_COOKIE_NAME);

        if (StringUtils.isEmpty(sessionId)) {
            throw new SessionNotFoundException(sessionId + " is empty");
        }
        return getLoginExtensionFromUsermng(sessionId);
    }

    /**
     * 세션아이디로 usermng-api를 통해 elasticCache에 적재된 세션 정보를 조회하여 연장처리 합니다.
     *
     * @param sessionId 세션 아이디
     * @return {@code ResponseObject<UserInfo>}
     */
    private boolean getLoginExtensionFromUsermng(final String sessionId) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        httpHeaders.set(WebHeaderKey.X_AUTH_TOKEN, sessionId);

        ResponseObject<UserInfo> responseObject = resourceClient
            .getForResponseObject(ResourceRouteName.USERMNG,
                PartnerConstants.PARTNER_LOGIN_INFO_URI,
                httpHeaders, new ParameterizedTypeReference<>() {
                });

        //data 추출
        if (isResponseSuccess(responseObject.getReturnCode())) {
            return true;
        } else {
            log.error("getLoginExtensionFromUsermng() failed:{}",
                responseObject.getReturnMessage());
            return false;
        }
    }

    /**
     * 인자로 전달받은 쿠키정보로 파트너 로그인 쿠키를 생성합니다.
     *
     * @param cookieList {@code List<PartnerCookieDto>}
     */
    private void createPartnerLoginCookies(final List<PartnerCookieDto> cookieList) {
        cookieList.forEach(
            setCookie -> {
                final String cookieName = setCookie.getCookieName();
                final String cookieValue = setCookie.getCookieValue();

                WebCookieUtils
                    .addCookie(cookieName, cookieValue, setCookie.getMaxAge());
                log.debug("setCookie:{},{},{}", setCookie.getCookieName(),
                    setCookie.getCookieValue(), setCookie.getMaxAge());
            }
        );
    }

    /**
     * usemng-api를 호출하여 1단계 로그인 인증 및 담당자 정보를 조회합니다.
     *
     * @param loginParam loginParam
     * @return {@link Step1LoginInfo}
     */
    private Step1LoginInfo getStep1LoginFromUsermng(final Step1LoginParam loginParam) {
        final ResponseObject<Step1LoginInfo> responseObject = resourceClient
            .postForResponseObject(ResourceRouteName.USERMNG, loginParam,
                PartnerConstants.PARTNER_LOGIN_STEP1_URI,
                new ParameterizedTypeReference<>() {
                });

        //data 추출
        if (isResponseSuccess(responseObject.getReturnCode())) {
            return responseObject.getData();
            // 계정 잠금처리 될 경우
        } else if (isResponseLoginFailCountExceededLock(responseObject.getReturnCode())) {
            throw new LoginFailCountExceededLockException(loginParam, responseObject.getReturnCode(),
                responseObject.getReturnMessage());

        } else if (isResponseLongTermUnusedLock(responseObject.getReturnCode())) {
            throw new LongTermUnusedLockException(loginParam, responseObject.getReturnCode(),
                responseObject.getReturnMessage());
        } else {
            throw new CertificationException(loginParam, responseObject.getReturnMessage());
        }
    }

    /**
     * usemng-api를 호출하여 2단계 로그인 인증 및 정보조회 요청합니다.
     *
     * @param step2LoginParam loginParam
     * @return {@link LoginResult}
     */
    private LoginResult getStep2LoginFromUsermng(final Step2LoginParam step2LoginParam) {
        final ResponseObject<LoginResult> responseObject = resourceClient
            .postForResponseObject(ResourceRouteName.USERMNG, step2LoginParam,
                PartnerConstants.PARTNER_LOGIN_STEP2_URI,
                new ParameterizedTypeReference<>() {
                });

        return getLoginResult(step2LoginParam, responseObject);
    }

    /**
     * usemng-api를 호출하여 응답받은 리턴코드에 따라 결과를 반환합니다.
     * @param step2LoginParam {@link Step2LoginParam}
     * @param responseObject {@link ResponseObject}
     * @return {@link LoginResult}
     */
    private LoginResult getLoginResult(final Step2LoginParam step2LoginParam,
        final ResponseObject<LoginResult> responseObject) {

        if (isResponseSuccess(responseObject.getReturnCode())) {
            return responseObject.getData();
        } else if (isResponseLoginFailCountExceededLock(responseObject.getReturnCode())) {
            throw new LoginFailCountExceededLockException(step2LoginParam, responseObject.getReturnCode(),
                responseObject.getReturnMessage());

        } else if (isResponseLongTermUnusedLock(responseObject.getReturnCode())) {
            throw new LongTermUnusedLockException(step2LoginParam, responseObject.getReturnCode(),
                responseObject.getReturnMessage());
        } else {
            throw new CertificationException(step2LoginParam, responseObject.getReturnMessage());
        }
    }

    /**
     * 로그인 시 잠금처리 된 리턴 코드인지 체크합니다.
     *
     * @param returnCode 리턴코드
     * @return boolean
     */
    private boolean isResponseLongTermUnusedLock(final String returnCode) {
        return returnCode.equals(RETURN_CODE_LONG_TERM_UNUSED_LOCK);
    }

    /**
     * 로그인 실패횟수 초과로 잠금처리 된 리턴 코드인지 체크합니다.
     * @param returnCode 리턴코드
     * @return boolean
     */
    private boolean isResponseLoginFailCountExceededLock(final String returnCode) {
        return returnCode.equals(RETURN_CODE_LOGIN_FAIL_COUNT_EXCEEDED_LOCK);
    }

    /**
     * 회원 비밀번호 검사
     *
     * @param loginParam
     * @return
     */
    public Boolean checkLoginFromUsermng(final Step1LoginParam loginParam) {
        final ResponseObject<LoginResult> responseObject = resourceClient
            .postForResponseObject(ResourceRouteName.USERMNG, loginParam,
                PartnerConstants.PARTNER_LOGIN_STEP1_URI,
                new ParameterizedTypeReference<>() {
                });

        return isResponseSuccess(responseObject.getReturnCode());
    }

    @Override
    public String verifyMobileCertNo(MobileOccupationValidParam param) {
        final ResponseObject<String> responseObject = resourceClient
            .postForResponseObject(ResourceRouteName.USERMNG,
                param, PartnerConstants.PARTNER_LOGIN_VERIFY_MOBILE_CERT_NO_URI,
                new ParameterizedTypeReference<>() {});

        if (isResponseSuccess(responseObject.getReturnCode())) {
            return responseObject.getData();
        } else {
            throw new MobileCertNoVerifyFailedException(responseObject.getReturnMessage());
        }
    }

    @Override
    public UserInfo getLoginUerInfo() {
        final String sessionId = WebCookieUtils
            .getCookieValue(PartnerConstants.PARTNER_SESSION_COOKIE_NAME);

        if (StringUtils.isEmpty(sessionId)) {
            return null;
        }

        return getUserInfoFromUsermng(sessionId);
    }

    /**
     * ResponseObject의 returnCode가 "SUCCESS" 인지 체크합니다.
     *
     * @param returnCode returnCode
     * @return boolean
     */
    private boolean isResponseSuccess(final String returnCode) {
        return returnCode.equals(ResponseObject.RETURN_CODE_SUCCESS);
    }

    /**
     * 세션아이디로 usermng-api를 통해 elasticCache에 적재된 로그인 정보를 조회합니다.
     *
     * @param sessionId 세션 아이디
     * @return {@link UserInfo}
     * @throws SessionNotFoundException
     */
    private UserInfo getUserInfoFromUsermng(final String sessionId) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        httpHeaders.set(WebHeaderKey.X_AUTH_TOKEN, sessionId);

        final ResponseObject<UserInfo> responseObject = resourceClient
            .getForResponseObject(ResourceRouteName.USERMNG,
                PartnerConstants.PARTNER_LOGIN_INFO_URI,
                httpHeaders, new ParameterizedTypeReference<>() {
                });

        if (isResponseSuccess(responseObject.getReturnCode())) {
            return responseObject.getData();
        } else {
            log.error("getUserInfo() get session failed:<{}>", responseObject.getReturnMessage());
            throw new SessionNotFoundException(responseObject.getReturnMessage());
        }
    }

    @Override
    public void processUserLogout() {
        final String sessionId = WebCookieUtils
            .getCookieValue(PartnerConstants.PARTNER_SESSION_COOKIE_NAME);

        WebCookieUtils.deleteCookie(PartnerConstants.PARTNER_SESSION_COOKIE_NAME);

        if (StringUtils.isNotEmpty(sessionId)) {
            getUserLogoutFromUsermng(sessionId);
        }
    }

    /**
     * usermng-api에 요청하여 로그아웃할 세션정보를 만료처리 합니다.
     *
     * @param sessionId 세션 아이디
     * @return boolean
     */
    private boolean getUserLogoutFromUsermng(final String sessionId) {
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        httpHeaders.set(WebHeaderKey.X_AUTH_TOKEN, sessionId);

        final ResponseObject<Boolean> responseObject = resourceClient
            .getForResponseObject(ResourceRouteName.USERMNG, PartnerConstants.PARTNER_LOGOUT_URI,
                httpHeaders, new ParameterizedTypeReference<>() {
                });

        return isResponseSuccess(responseObject.getReturnCode());
    }

    /**
     * item-api에 요청하여 파트너 로그인 일시 업데이트
     * @param param 파트너 아이디
     */
    private Boolean updatePartnerLoginDateFromItem(final Step2LoginParam param) {
        final ResponseObject<Boolean> responseObject = resourceClient
            .getForResponseObject(ResourceRouteName.ITEM,
            PartnerConstants.UPDATE_PARTNER_LOGIN_DATE_URI + param.getPartnerId(),
                new ParameterizedTypeReference<>() {
                });

        if (isResponseSuccess(responseObject.getReturnCode())) {
            return responseObject.getData();
        } else {
            throw new CertificationException(param, responseObject.getReturnMessage());
        }
    }

}
