package kr.co.homeplus.partner.web.sell.model.orderShipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 발주/발송관리 리스트 요청 DTO")
public class OrderShipManageListSetDto {
    @ApiModelProperty(notes = "배송상태")
    private String schShipStatus;

    @ApiModelProperty(notes = "검색기간 타입")
    private String schShipDtType;

    @ApiModelProperty(notes = "시작일")
    private String schShipStartDt;

    @ApiModelProperty(notes = "종료일")
    private String schShipEndDt;

    @ApiModelProperty(notes = "검색조건 타입")
    private String schKeywordType;

    @ApiModelProperty(notes = "검색어")
    private String schKeyword;

    @ApiModelProperty(notes = "발송지연안내")
    private String schShipDelay;

    @ApiModelProperty(notes = "파트너ID")
    private String partnerId;
}
