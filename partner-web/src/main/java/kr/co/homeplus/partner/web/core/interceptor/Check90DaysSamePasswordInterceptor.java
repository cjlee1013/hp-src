package kr.co.homeplus.partner.web.core.interceptor;

import static org.springframework.util.ObjectUtils.isEmpty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.co.homeplus.partner.web.core.annotation.CertificationNeedLess;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.config.AuthorizationProperties;
import kr.co.homeplus.partner.web.core.dto.UserInfo;
import kr.co.homeplus.partner.web.core.exception.Check90DaysSamePasswordInterceptorException;
import kr.co.homeplus.partner.web.core.exception.handler.ExceptionCode;
import kr.co.homeplus.partner.web.core.service.Check90daysSamePasswordService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


/**
 * 동일 비밀번호를 90일 이상 사용여부를 체크하는 인터셉터
 */
@Slf4j
@RequiredArgsConstructor
public class Check90DaysSamePasswordInterceptor extends HandlerInterceptorAdapter {

    private final Check90daysSamePasswordService check90DaysSamePasswordService;
    private final CertificationService certificationService;
    private final AuthorizationProperties properties;

    //파트너 ajax 호출 시 규약 된 uri suffix
    private static final String REST_URI_SUFFIX = ".json";
    private static final String ATTRIBUTE_NAME_UNDER_90_DAYS = "isUnder90DaysSamePassword";

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response,
        final Object handler) throws Exception {

        if (!(handler instanceof HandlerMethod)) {
            return super.preHandle(request, response, handler);
        }

        UserInfo userInfo = certificationService.getLoginUerInfo();

        if (isNotCertificationNeedLess((HandlerMethod) handler) && isLogin(userInfo)) {
            final String partnerId = userInfo.getPartnerId();

            final boolean isUnder90Days = check90DaysSamePasswordService.isUnder90DaysSamePassword(partnerId);

            final String requestUri = request.getRequestURI();
            AntPathMatcher matcher = new AntPathMatcher();

            /*
             * 현재 request uri가 체크 제외대상 uri(ajax 호출, 메인, 홈, 로그인 등)인지 검증합니다.
             * 동일 비밀번호를 90일 이상(isUnder90day = false) 사용 상태에서 다른 메뉴 접근시 강제 로그아웃 처리 합니다.
             **/
            if (!isUnder90Days && isNotValidateUri(requestUri, (HandlerMethod) handler, matcher)) {
                log.error("Access Denied! using the same password for over 90days. access this uri:<{}>", requestUri);

                throw new Check90DaysSamePasswordInterceptorException(
                    ExceptionCode.SYS_ERROR_CODE_9211.getDesc());
            }

            // 메인화면 에서 사용할 정보 세팅.
            request.setAttribute(ATTRIBUTE_NAME_UNDER_90_DAYS, isUnder90Days);
        }
        return super.preHandle(request, response, handler);
    }

    /**
     * 로그인 상태일 경우를 체크합니다.
     * @param userInfo UserInfo
     * @return 로그인 상태인 경우 true를 반환, 로그인 상태일 경우 false를 반환합니다.
     */
    private boolean isLogin(final UserInfo userInfo) {
        return !isEmpty(userInfo);
    }

    /**
     * <p>
     * {@link CertificationNeedLess} annotation 태깅 여부를 체크합니다.
     * </p>
     * 해당 어노테이션이 클래스나 메서드에 붙어있지 않은 경우를 체크합니다.
     *
     * @param handlerMethod HandlerMethod
     * @return boolean
     */
    private boolean isNotCertificationNeedLess(final HandlerMethod handlerMethod) {
        return !handlerMethod.getBeanType().isAnnotationPresent(CertificationNeedLess.class)
            || !handlerMethod.getMethod().isAnnotationPresent(CertificationNeedLess.class);
    }

    /**
     * 현재 접근한 URI가 체크 제외대상 URI 인지 체크합니다.
     * @param requestUri request URI
     * @param handler  HandlerMethod
     * @param antPathMatcher AntPathMatcher
     * @return Boolean
     */
    private boolean isNotValidateUri(final String requestUri, final HandlerMethod handler,
        final AntPathMatcher antPathMatcher) {
        return isNotAjaxUri(requestUri) && isNotIgnoreUris(requestUri, antPathMatcher, handler);
    }

    /**
     * request uri가 웹의 메뉴를 접근하는지, ajax 호출을 하는지 체크합니다.<p>
     *
     * requestd의 suffix가 .json 이면 false, 아니면 true 리턴합니다.<br>
     *
     * @param requestUri request uri
     * @return boolean suffix가 .json 이면 false, 아니면 true 리턴합니다.
     */
    private boolean isNotAjaxUri(final String requestUri) {
        return !StringUtils.endsWith(requestUri, REST_URI_SUFFIX);
    }

    /**
     * 현재 접근하는 URI가 체크 제외대상 uri가 아닌지 검증
     *
     * @param requestUri request URI
     * @param handler HandlerMethod
     * @return 제외 대상이 아니면 true, 제외 대상이면 false를 리턴합니다.
     */
    private boolean isNotIgnoreUris(final String requestUri, final AntPathMatcher antPathMatcher,
        final HandlerMethod handler) {

        // CertificationNeedLess annotation 이 클래스나 메서드에 붙어 있을 경우 체크 무시
        if (handler.getBeanType().isAnnotationPresent(CertificationNeedLess.class)
            || handler.getMethod().isAnnotationPresent(CertificationNeedLess.class)) {
            return false;
        }

        // 체크 제외대상 uri가 아닌지 검증
        return properties.getIgnoreAuthCheckUris().stream()
            .noneMatch(ignoreUrl -> antPathMatcher.match(ignoreUrl, requestUri));
    }
}