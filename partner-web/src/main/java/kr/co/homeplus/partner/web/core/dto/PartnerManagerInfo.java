package kr.co.homeplus.partner.web.core.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *  파트너 로그인 인증담당자 정보
 */
@Setter
@Getter
public class PartnerManagerInfo {
    /** 담당자 이름**/
    private String mngNm;
    /**담당자 휴대폰번호**/
    private String mngMobile;
    /** 마스킹 처리 된 담당자 휴대폰번호 **/
    private String mskMngMobile;
}
