package kr.co.homeplus.partner.web.manage.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PartnerFaqDetailSelectDto {

    private String dspNo;

    private String kindTxt;

    private String title;

    private String contents;

    private String topYn;

    private String newYn;

    private String chgDt;


}
