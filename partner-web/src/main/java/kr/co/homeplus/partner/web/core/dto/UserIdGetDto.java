package kr.co.homeplus.partner.web.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserIdGetDto {

    //사용자 아이디
    private String partnerId;

    //가입일
    private String regDt;
}
