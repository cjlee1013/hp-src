package kr.co.homeplus.partner.web.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"kr.co.homeplus.partner.web", "kr.co.homeplus.plus"})
public class PartnerWebApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(PartnerWebApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PartnerWebApplication.class);
    }
}
