package kr.co.homeplus.partner.web.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserIdParamGetDto {

    //사업자번호
    private String partnerNo;

    //제휴업체핸드폰
    private String affiManagerMobile;

    //제휴업체이메일
    private String affiManagerEmail;

}
