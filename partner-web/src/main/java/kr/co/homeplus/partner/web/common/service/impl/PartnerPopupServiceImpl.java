package kr.co.homeplus.partner.web.common.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import kr.co.homeplus.partner.web.common.model.home.PartnerPopupDto;
import kr.co.homeplus.partner.web.common.service.PartnerPopupService;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.dto.UserInfo;
import kr.co.homeplus.partner.web.core.usermenu.MenuService;
import kr.co.homeplus.partner.web.core.usermenu.model.MainMenus;
import kr.co.homeplus.partner.web.core.usermenu.model.Menus;
import kr.co.homeplus.partner.web.core.usermenu.model.SubMenus;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class PartnerPopupServiceImpl implements PartnerPopupService {

    private final ResourceClient resourceClient;
    private final CertificationService certificationService;
    private final MenuService menuService;

    @Override
    public List<PartnerPopupDto> getPartnerPopupList() {

        UserInfo userInfo = certificationService.getLoginUerInfo();
        String partnerId = userInfo.getPartnerId();

        ResponseObject<List<PartnerPopupDto>> responseObject = resourceClient
            .getForResponseObject(ResourceRouteName.MANAGE,
                "/po/manage/dspPopup/getDisplayPopupList?partnerId=" + partnerId,
                new ParameterizedTypeReference<ResponseObject<List<PartnerPopupDto>>>() {
                });

        List<PartnerPopupDto> orgPopupList = Collections.emptyList();

        if (ResponseObject.RETURN_CODE_SUCCESS.equals(responseObject.getReturnCode())) {
            orgPopupList = responseObject.getData();
        }
        log.debug("orgPopupList : {}", orgPopupList);

        // 메뉴 데이터를 가져오고
        Menus menus = menuService.getMenus();
        List<MainMenus> menuList = menus.getData();

        // 새로운 리스트에 탭링크를 만든다
        List<PartnerPopupDto> popupList = new ArrayList<>();
        Iterator<PartnerPopupDto> it = orgPopupList.iterator();
        while (it.hasNext()) {
            PartnerPopupDto popup = it.next();
            if ("MENU".equals(popup.getLinkType()) && !StringUtils.isEmpty(popup.getLinkInfo())) {
                SubMenus targetMenuData = getTargetMenuData(menuList, popup.getLinkInfo());
                popup.setLinkInfo(
                    "\"parent.initialTab.addTab('" + targetMenuData.getTabId() + "', '"
                        + targetMenuData.getMenuNm() + "', '" + targetMenuData.getLink() + "');\"");
            }
            popupList.add(popup);
        }
        log.debug("popupList : {}", popupList);

        return popupList;
    }

    private SubMenus getTargetMenuData(List<MainMenus> menuList, String tabId) {
        SubMenus targetMenuData = null;
        for (MainMenus mainMenus : menuList) {
            List<SubMenus> subMenuList = mainMenus.getSubMenus();
            for (SubMenus subMenus : subMenuList) {
                if (subMenus.getTabId().equals(tabId)) {
                    targetMenuData = SubMenus.builder().menuNm(subMenus.getMenuNm())
                        .tabId(subMenus.getTabId()).link(subMenus.getLink()).build();
                    break;
                }
            }
        }
        log.debug("targetMenu : {}", targetMenuData);
        return targetMenuData;
    }
}
