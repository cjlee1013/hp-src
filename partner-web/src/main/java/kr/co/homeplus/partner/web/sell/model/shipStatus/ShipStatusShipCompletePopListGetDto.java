package kr.co.homeplus.partner.web.sell.model.shipStatus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 배송현황 배송완료 팝업 목록 응답 DTO")
public class ShipStatusShipCompletePopListGetDto {
    @ApiModelProperty(notes = "배송SEQ")
    private String shipNo;

    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;

    @ApiModelProperty(notes = "상품주문번호")
    private String orderItemNo;

    @ApiModelProperty(notes = "수령인")
    private String receiverNm;

    @ApiModelProperty(notes = "상품명")
    private String itemNm1;
}
