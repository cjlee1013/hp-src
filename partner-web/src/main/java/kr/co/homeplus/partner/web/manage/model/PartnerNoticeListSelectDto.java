package kr.co.homeplus.partner.web.manage.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(value = "PATNER-WEB 공지사항 리스트")
public class PartnerNoticeListSelectDto {

    @ApiModelProperty(value = "공지사항 일련번호")
    @RealGridColumnInfo(headText = "번호", width = 0 , hidden = true)
    private String dspNo;

    @ApiModelProperty(value = "공지사항 구분")
    @RealGridColumnInfo(headText = "번호", width = 0 , hidden = true)
    private String kind;

    @ApiModelProperty(value = "공지사항 구분 TXT")
    @RealGridColumnInfo(headText = "구분", width = 100)
    private String kindTxt;

    @ApiModelProperty(value = "공지사항 제목")
    @RealGridColumnInfo(headText = "제목", width = 300)
    private String title;

    @ApiModelProperty(value = "중요공지여부")
    @RealGridColumnInfo(headText = "중요공지여부", width = 0, hidden = true)
    private String topYn;

    @ApiModelProperty(value = "신규여부 ")
    @RealGridColumnInfo(headText = "신규여부", width = 0, hidden = true)
    private String newYn;

    @ApiModelProperty(value = "수정일시")
    @RealGridColumnInfo(headText = "수정일시", width = 100)
    private String chgDt;

}
