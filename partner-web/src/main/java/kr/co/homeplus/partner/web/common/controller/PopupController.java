package kr.co.homeplus.partner.web.common.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.partner.web.common.model.pop.AddressParam;
import kr.co.homeplus.partner.web.common.model.pop.AddressPopupDto;
import kr.co.homeplus.partner.web.common.model.pop.ItemPopupListGetDto;
import kr.co.homeplus.partner.web.common.model.pop.ItemPopupListParamDto;
import kr.co.homeplus.partner.web.core.annotation.CertificationNeedLess;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.item.model.item.CategoryCommissionGetDto;
import kr.co.homeplus.partner.web.item.model.item.CategoryCommissionParamDto;
import kr.co.homeplus.partner.web.voc.model.csQna.PartnerQnaTemplateSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 팝업 컨트롤러
 *
 */
@Controller
@Slf4j
public class PopupController {
    private final ResourceClient resourceClient;
    private final CertificationService certificationService;

    public PopupController(ResourceClient resourceClient ,
        CertificationService certificationService)
    {
        this.resourceClient = resourceClient;
        this.certificationService = certificationService;
    }
    /**
     * 우편번호 검색 시스템 팝업
     *
     * 인자로 callBack 스크립트를 받음
     *
     * @param callBackScript callback script
     * @return 우편번호 검색 시스템 팝업
     */
    @CertificationNeedLess
    @GetMapping("/common/zipCodePop")
    public String zipCodePopSample(HttpServletRequest request, Model model,
                                   @RequestParam(value = "callBack", defaultValue = "") String callBackScript) {
        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("title", "우편번호 검색");
        return "/common/pop/zipCodePop";
    }
    /**
     * 우편번호 주소검색 데이터 조회
     *
     * @param param 우편번호 주소검색 파라미터
     * @return 우편번호 주소목록 반환
     * @throws Exception
     */
    @ResponseBody
    @CertificationNeedLess
    @GetMapping(value = "/common/getAddressList.json")
    public ResponseObject<AddressPopupDto> getAddressList(@ModelAttribute AddressParam param) {
        StringBuilder searchAddressUrl = new StringBuilder("/address/V1_0/search");
        searchAddressUrl.append("?page=").append(param.getPage())
            .append("&perPage=").append(param.getPerPage())
            .append("&query=").append(param.getQuery());

        return resourceClient
            .getForResponseObject(ResourceRouteName.ADDRESS, searchAddressUrl.toString(),
                new ParameterizedTypeReference<>() {
                });

    }
    /**
     * 제주/도서산간지역 확인 팝업
     *
     * @return 제주/도서산간지역 확인 팝업
     */
    @CertificationNeedLess
    @GetMapping(value = "/common/checkIslandPop")
    public String checkIslandPop(final Model model) {
        model.addAttribute("title", "제주/도서산간지역 확인");
        return "/common/pop/checkIslandPop";
    }
    /**
     * 제주/도서산간지역 팝업용도 주소검색 데이터 조회
     */
    @CertificationNeedLess
    @ResponseBody
    @GetMapping(value = "/common/getAddressListByIsland.json")
    public ResponseObject<AddressPopupDto> getAddressListByIsland(
        @ModelAttribute final AddressParam param) {
        final StringBuilder addressUri = new StringBuilder("/address/V1_0/search");
        addressUri.append("?page=").append(param.getPage())
            .append("&perPage=").append(param.getPerPage())
            .append("&query=").append(param.getQuery());

        return resourceClient
            .getForResponseObject(ResourceRouteName.ADDRESS, addressUri.toString(),
                new ParameterizedTypeReference<>() {
                });
    }
    /**
     * 상품조회 공통팝업
     */
    @RequestMapping(value = "/common/itemPop", method = RequestMethod.GET)
    public String itemPop(Model model
        , @RequestParam(value = "callback", defaultValue = "") String callBackScript
        , @RequestParam(value = "isMulti", defaultValue = "") String isMulti
    )throws Exception {

        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("isMulti", isMulti);

        model.addAllAttributes(RealGridHelper.create("itemPopGridBaseInfo", ItemPopupListGetDto.class));

        return "/common/pop/itemPop";
    }
    /**
     * 상품 정보 조회
     */
    @ResponseBody
    @PostMapping(value = {"/common/getItemPopList.json"})
    public List<ItemPopupListGetDto> getItemPopList(ItemPopupListParamDto itemPopupListParamDto) {
        String apiUri = "/common/popup/getPartnerItemPopList";

        itemPopupListParamDto.setSchPartnerValue(certificationService.getLoginUerInfo().getPartnerId());
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemPopupListGetDto>>>() {};

        return (List<ItemPopupListGetDto>) resourceClient
            .postForResponseObject(ResourceRouteName.ITEM, itemPopupListParamDto, apiUri,
                typeReference).getData();
    }
    /**
     * 상품조회 공통팝업
     */
    @RequestMapping(value = "/common/csQnaTemplatePop", method = RequestMethod.GET)
    public String csQnaTemplatePop(Model model
        , @RequestParam(value = "callback", defaultValue = "") String callBackScript
        , @RequestParam(value = "isMulti", defaultValue = "") String isMulti
    )throws Exception {

        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("isMulti", isMulti);

        model.addAllAttributes(RealGridHelper.create("csQnaTemplatePopGridBaseInfo", PartnerQnaTemplateSelectDto.class));

        return "/common/pop/itemPop";
    }
    /**
     * 이미지 미리보기 팝업
     */
    @RequestMapping(value = "/common/previewImg", method = RequestMethod.GET)
    public String previewImg() {
        return "/common/pop/previewImgPop";
    }

    /**
     * 카테고리별 수수료 팝업
     * (홈플러스 카테고리 기준 수수료 (업체 수수료 아님))
     */
    @ResponseBody
    @GetMapping(value = {"/common/getCategoryCommissionList.json"})
    public List<CategoryCommissionGetDto> getCommissionPopList(
        CategoryCommissionParamDto categoryCommissionParamDto) {
        String apiUri = "/common/popup/getPartnerCategoryCommissionPopList";

        categoryCommissionParamDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CategoryCommissionGetDto>>>() {};

        return (List<CategoryCommissionGetDto>) resourceClient
            .postForResponseObject(ResourceRouteName.ITEM, categoryCommissionParamDto, apiUri,
                typeReference).getData();
    }

}