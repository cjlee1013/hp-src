package kr.co.homeplus.partner.web.core.utility;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.navercorp.lucy.security.xss.servletfilter.XssEscapeServletFilterWrapper;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import kr.co.homeplus.partner.web.core.filter.HomeplusXssFilterWrapper;
import kr.co.homeplus.plus.api.support.client.exception.ResourceClientException;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

public class UploadFileUtil {

    // MultipartFile 을 받아서 ByteArrayResource 를 반환. fileName 은 UTF-8 URLEncode 된 상태로 넘어감.
    public static List<ByteArrayResource> getResourceFromMultipartFile(List<MultipartFile> files)
            throws IOException {
        if (files == null) {
            throw new IllegalArgumentException("file is null");
        }
        List<ByteArrayResource> result = new ArrayList<>();

        for (MultipartFile file : files) {
            result.add(getByteArrayResource(file));
        }
        return result;
    }

    private static ByteArrayResource getByteArrayResource(final MultipartFile file)
            throws IOException {
        return new ByteArrayResource(file.getBytes()) {
            @Override
            public String getFilename() throws IllegalStateException {
                return URLEncoder.encode(file.getOriginalFilename(), StandardCharsets.UTF_8);
            }
        };
    }

    /**
     * http request 로 부터 multipart file 목록을 추출.
     *
     * @param request
     * @param fieldName
     * @return
     */
    public static List<MultipartFile> getMultiPartFilesFromRequest(HttpServletRequest request,
            String fieldName) {
        validateParamsForMultipartFile(request, fieldName);

        StandardMultipartHttpServletRequest multipartHttpServletRequest = convertToStandardMultipartHttpServletRequest(
                request);

        return multipartHttpServletRequest.getFiles(fieldName);
    }

    /**
     * http request 로 부터 multipart file 을 추출.
     *
     * @param request
     * @param fieldName
     * @return
     */
    public static MultipartFile getMultiPartFileFromRequest(HttpServletRequest request,
            String fieldName) {
        validateParamsForMultipartFile(request, fieldName);

        StandardMultipartHttpServletRequest multipartHttpServletRequest = convertToStandardMultipartHttpServletRequest(
                request);

        return multipartHttpServletRequest.getFile(fieldName);
    }

    private static void validateParamsForMultipartFile(HttpServletRequest request,
            String fieldName) {
        if (request == null) {
            throw new IllegalArgumentException("request is null!");
        }

        if (StringUtils.isEmpty(fieldName)) {
            throw new IllegalArgumentException("fieldName is Empty!");
        }
    }

    private static StandardMultipartHttpServletRequest convertToStandardMultipartHttpServletRequest(
            HttpServletRequest request) {
        StandardMultipartHttpServletRequest multipartHttpServletRequest;

        if (request instanceof HomeplusXssFilterWrapper
                || request instanceof XssEscapeServletFilterWrapper) {
            multipartHttpServletRequest = (StandardMultipartHttpServletRequest) ((HttpServletRequestWrapper) request)
                    .getRequest();
        } else {
            multipartHttpServletRequest = (StandardMultipartHttpServletRequest) request;
        }
        return multipartHttpServletRequest;
    }

    /**
     * 파일 업로드시 resourceClientException 발생 시 처리
     *
     * @param e resourceClientException
     * @return
     */
    public static String setUploadFileResponseError(ResourceClientException e) {
        JsonObject jsonObject = new JsonParser().parse(new String(e.getResponseBody()))
                .getAsJsonObject();
        JsonArray jsonArray = jsonObject.getAsJsonArray("errors");

        return jsonArray.get(0).getAsJsonObject().get("detail").getAsString();
    }
}
