package kr.co.homeplus.partner.web.settle.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.settle.model.salesTrend.SalesByDailyInfo;
import kr.co.homeplus.partner.web.settle.model.salesTrend.SearchConditionDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class PartnerSalesTrendService {

    // 날짜의 간격 기준, DIV_X_TERM_DAY 사이 마다 날짜의 간격이 1일씩 늘어남
    private static final int DIV_X_TERM_DAY = 14;

    private final ResourceClient resourceClient;

    public List<SalesByDailyInfo> searchSalesByDaily(final SearchConditionDto searchConditionDto) {
        List<SalesByDailyInfo> list = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE, searchConditionDto,
            "/partner/salesTrend/daily",
            new ParameterizedTypeReference<ResponseObject<List<SalesByDailyInfo>>>() {
            }).getData();
        return CollectionUtils.isEmpty(list) ? emptyList(searchConditionDto) : list;

        /*
        LocalDate s = searchConditionDto.getStartDate();
        LocalDate e = searchConditionDto.getEndDate();
        LocalDate current = LocalDate.ofEpochDay(s.toEpochDay());

        int term = getTerms(s, e);

        List<LocalDate> xAxisDates = new ArrayList<>(List.of(current));
        while (current.isBefore(e)) {
            current = current.plusDays(term);
            xAxisDates.add(current);
        }

        return list.stream().peek(salesByDailyInfo -> {
            if (!xAxisDates.contains(salesByDailyInfo.getRegDt())) {
                salesByDailyInfo.setRegDt(null);
            }
        }).collect(Collectors.toList());
        */
    }

    private List<SalesByDailyInfo> emptyList(final SearchConditionDto searchConditionDto) {
        LocalDate s = searchConditionDto.getStartDate();
        LocalDate e = searchConditionDto.getEndDate();
        long days = getBetweenDays(s, e);
        List<SalesByDailyInfo> list = new ArrayList<>();
        for (int i = 0; i < days; i++) {
            SalesByDailyInfo salesByDailyInfo = new SalesByDailyInfo();
            salesByDailyInfo.setRegDt(s.plusDays(i));
            salesByDailyInfo.setEventCnt(0);
            salesByDailyInfo.setPaymentCnt(0);
            list.add(salesByDailyInfo);
        }
        return list;
    }

    public long getBetweenDays(LocalDate s, LocalDate e) {
        return ChronoUnit.DAYS.between(s, e) + 1L;
    }

    public int getTerms(LocalDate s, LocalDate e) {
        long days = getBetweenDays(s, e);

        /* 간격이 불규칙 해지는 경우 필요 할 수 있으므로 주석처리
        if (days >= 0 && days <= 13) {
            return 1;
        } else if (days >= 14 && days <= 27) {
            return 2;
        } else if (days >= 28 && days <= 41) {
            return 3;
        } else if (days >= 42 && days <= 55) {
            return 4;
        } else if (days >= 56 && days <= 69) {
            return 5;
        } else if (days >= 70 && days <= 83) {
            return 6;
        } else if (days >= 84 && days <= 97) {
            return 7;
        } else if (days >= 98 && days <= 111) {
            return 8;
        } else if (days >= 112 && days <= 125) {
            return 9;
        } else if (days >= 126 && days <= 139) {
            return 10;
        } else if (days >= 140 && days <= 153) {
            return 11;
        } else if (days >= 154 && days <= 167) {
            return 12;
        } else if (days >= 168 && days <= 180) {
            return 13;
        } else {
            return 14;
        }
        */
        return (int) (Math.floor((double) days / (double) DIV_X_TERM_DAY) + 1);
    }
}
