package kr.co.homeplus.partner.web.common.service.impl;

import java.util.Collections;
import java.util.List;
import kr.co.homeplus.partner.web.common.model.home.ClaimDashBoardCount;
import kr.co.homeplus.partner.web.common.model.home.HomeNoticeSelectDto;
import kr.co.homeplus.partner.web.common.model.home.PartnerSettleAmt;
import kr.co.homeplus.partner.web.common.model.home.SalesActInfo;
import kr.co.homeplus.partner.web.common.model.home.SalesProgress;
import kr.co.homeplus.partner.web.common.service.HomeDashboardService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.manage.model.PartnerHarmfulItemListSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

/**
 * 홈 메인화면을 관리하는 서비스 입니다.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class HomeDashboardServiceImpl implements HomeDashboardService {
    private final ResourceClient resourceClient;

    @Override
    public SalesActInfo getSalesActInfo(final String partnerId) {

        ResponseObject<SalesActInfo> responseObject = resourceClient.getForResponseObject(
            ResourceRouteName.ITEM, "/partner/out/getPartnerHome?partnerId=" + partnerId,
            new ParameterizedTypeReference<>() {
            });

        if (isResponseSuccess(responseObject.getReturnCode())) {
            return responseObject.getData();
        }
        return new SalesActInfo();
    }

    @Override
    public HomeNoticeSelectDto getBoardList(final String partnerId) {
        //일반공지 조회
        final String noticeApiUri = "/po/home/getBoardList?partnerId=" + partnerId;

        final ParameterizedTypeReference<ResponseObject<HomeNoticeSelectDto>> typeReference
            = new ParameterizedTypeReference<>() {
        };

        ResponseObject<HomeNoticeSelectDto> responseObject = resourceClient.getForResponseObject(
            ResourceRouteName.MANAGE,noticeApiUri, typeReference);

        if (isResponseSuccess(responseObject.getReturnCode())) {
            return responseObject.getData();
        }
        return new HomeNoticeSelectDto();
    }

    @Override
    public List<PartnerHarmfulItemListSelectDto> getPartnerHarmfulItemList(final String partnerId) {
        //위해상품 조회
        final String harmfulApiUri =
            "/po/item/harmful/getPartnerHarmfulItemList?partnerId=" + partnerId
                + "&harmfulArea=MAIN";
        final ParameterizedTypeReference<ResponseObject<List<PartnerHarmfulItemListSelectDto>>> typeReference
            = new ParameterizedTypeReference<>() {
        };

        ResponseObject<List<PartnerHarmfulItemListSelectDto>> responseObject = resourceClient
            .getForResponseObject(
                ResourceRouteName.ITEM, harmfulApiUri, typeReference);

        if (isResponseSuccess(responseObject.getReturnCode())) {
            return responseObject.getData();
        }
        return Collections.singletonList(new PartnerHarmfulItemListSelectDto());
    }

    @Override
    public SalesProgress getSalesProgress(final String partnerId) {
        ResponseObject<SalesProgress> responseObject = resourceClient.getForResponseObject(
            ResourceRouteName.SHIPPING, "/partner/sell/getHomeMainCnt?partnerId=" + partnerId,
            new ParameterizedTypeReference<>() {
            });

        if (isResponseSuccess(responseObject.getReturnCode())) {
            return responseObject.getData();
        }
        return new SalesProgress();
    }

    @Override
    public PartnerSettleAmt getPartnerSettleAmt(final String partnerId) {
        ResponseObject<PartnerSettleAmt> responseObject = resourceClient.getForResponseObject(
            ResourceRouteName.SETTLE, "/partner/settleInfo/getPartnerSettleAmt?partnerId=" + partnerId,
            new ParameterizedTypeReference<>() {
            });

        if (isResponseSuccess(responseObject.getReturnCode())) {
            return responseObject.getData();
        }
        return new PartnerSettleAmt();
    }

    @Override
    public ClaimDashBoardCount getClaimCount(final String partnerId) {
        ResponseObject<ClaimDashBoardCount> responseObject = resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG, "/po/claim/getPOMainClaimRequestCount?partnerId=" + partnerId,
            new ParameterizedTypeReference<>() {
            });

        if (isResponseSuccess(responseObject.getReturnCode())) {
            return responseObject.getData();
        }
        return new ClaimDashBoardCount();
    }

    /**
     * ResponseObject의 returnCode가 "SUCCESS" 인지 체크합니다.
     * @param returnCode ResponseObject 의 returnCode
     * @return boolean
     */
    private boolean isResponseSuccess(final String returnCode) {
        return returnCode.equals(ResponseObject.RETURN_CODE_SUCCESS) || returnCode.equals("0000");
    }

}
