package kr.co.homeplus.partner.web.partner.model.sellerShop;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SellerShopSetParamDto {

    private String partnerId;
    private String shopNm;
    private String shopUrl;
    private String shopProfileImg;
    private String shopLogoImg;
    private String shopInfo;
    private String csInfo;
    private String csUseYn;
    private String useYn;
    private String regId;

}
