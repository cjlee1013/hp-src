package kr.co.homeplus.partner.web.core.dto;

import lombok.Data;

@Data
public class UserTempPwParamSetDto {

	//판매업체ID
	private String partnerId;

	//판매업체명
	private String businessNm;

	//이메일 선택
	private String emailCheck;

	//휴대폰 선택
	private String mobileCheck;

	//등록/수정자
	private String userId;

}
