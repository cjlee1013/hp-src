package kr.co.homeplus.partner.web.partner.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

@Data
@ApiModel("판매자관리 > 판매 상품유형 Set Entry")
public class PartnerTypesGetDto implements Serializable {

    @ApiModelProperty(value = "유형종류")
    private String kind;

    @ApiModelProperty(value = "유형")
    private String types;

    @ApiModelProperty(value = "유형코드", required = true)
    private String gmcCd;

    @ApiModelProperty(value = "코드", required = true)
    private String mcCd;

    @ApiModelProperty(value = "코드명", required = true)
    private String mcNm;

    @ApiModelProperty(value = "참조1", required = false)
    private String ref1;

    @ApiModelProperty(value = "참조2", required = false)
    private String ref2;

    @ApiModelProperty(value = "참조3", required = false)
    private String ref3;

    @ApiModelProperty(value = "참조4", required = false)
    private String ref4;
}
