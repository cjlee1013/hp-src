package kr.co.homeplus.partner.web.settle.controller;

import java.util.Arrays;
import java.util.List;
import kr.co.homeplus.partner.web.settle.model.finalSettle.PartnerFinalSettleGetDto;
import kr.co.homeplus.partner.web.settle.model.finalSettle.PartnerFinalSettleSetDto;
import kr.co.homeplus.partner.web.settle.model.finalSettle.PartnerSettlePayState;
import kr.co.homeplus.partner.web.settle.model.finalSettle.PartnerSettleState;
import kr.co.homeplus.partner.web.settle.model.salesVAT.PartnerSalesAdjustListGetDto;
import kr.co.homeplus.partner.web.settle.model.salesVAT.PartnerSalesVATListGetDto;
import kr.co.homeplus.partner.web.settle.model.salesVAT.PartnerSalesVATOrderGetDto;
import kr.co.homeplus.partner.web.settle.model.salesVAT.PartnerSettleListSetDto;
import kr.co.homeplus.partner.web.settle.model.settleInfo.DailySettleGetDto;
import kr.co.homeplus.partner.web.settle.model.settleInfo.DailySettleSetDto;
import kr.co.homeplus.partner.web.settle.model.settleInfo.SettleInfoGetDto;
import kr.co.homeplus.partner.web.settle.model.taxBill.PartnerTaxBillListGetDto;
import kr.co.homeplus.partner.web.settle.service.PartnerSettleInfoService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle")
public class PartnerSettleInfoController {
    private final PartnerSettleInfoService partnerSettleService;

    public PartnerSettleInfoController(PartnerSettleInfoService partnerSettleService) {
        this.partnerSettleService = partnerSettleService;
    }

    /**
     * 정산관리 > 매출현황
     */
    @RequestMapping(value = "/dailySettleList", method = RequestMethod.GET)
    public String dailySettleList(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.create("dailySettleListBaseInfo", DailySettleGetDto.class));
        model.addAllAttributes(RealGridHelper.create("dailySettleInfoBaseInfo", SettleInfoGetDto.class));

        return "/settle/dailySettleList";
    }

    /**
     * 정산관리 > 정산현황
     */
    @RequestMapping(value = "/partnerSettleList", method = RequestMethod.GET)
    public String partnerSettleList(Model model) throws Exception {
        //정산상태
        model.addAttribute("settleState", Arrays.asList(PartnerSettleState.values()));
        //지급상태
        model.addAttribute("settlePayState", Arrays.asList(PartnerSettlePayState.values()));

        model.addAllAttributes(RealGridHelper.create("partnerSettleBaseInfo", PartnerFinalSettleGetDto.class));

        return "/settle/partnerSettleList";
    }

    /**
     * 정산관리 > 세금계산서 조회
     */
    @RequestMapping(value = "/taxBillList", method = RequestMethod.GET)
    public String taxbillList(Model model) throws Exception {
        //년도 조회
        model.addAttribute("getYear", partnerSettleService.getDateCalculate("Y", null, null, 2020));
        //월 조회
        model.addAttribute("getMonth", partnerSettleService.getDateCalculate("M", null, null, 0));

        model.addAllAttributes(RealGridHelper.create("partnerTaxBillBaseInfo", PartnerTaxBillListGetDto.class));

        return "/settle/taxBillList";
    }

    /**
     * 정산관리 > 부가세신고 내역
     */
    @RequestMapping(value = "/salesVATList", method = RequestMethod.GET)
    public String salesVATList(Model model) throws Exception {
        //년도 조회
        model.addAttribute("getYear", partnerSettleService.getDateCalculate("Y", null, null, 2020));
        //월 조회
        model.addAttribute("getMonth", partnerSettleService.getDateCalculate("M", null, null, 0));
        
        model.addAllAttributes(RealGridHelper.createForGroup("salesVATListBaseInfo", PartnerSalesVATListGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("salesVATOrderBaseInfo", PartnerSalesVATOrderGetDto.class));
        model.addAllAttributes(RealGridHelper.create("adjustListBaseInfo", PartnerSalesAdjustListGetDto.class));

        return "/settle/salesVATList";
    }

    /**
     * 정산관리 > 매출현황 메인 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getDailySettle.json", method = RequestMethod.GET)
    public List<DailySettleGetDto> getDailySettle(DailySettleSetDto listParamDto) throws Exception {
        return partnerSettleService.getDailySettle(listParamDto);
    }

    /**
     * 정산관리 > 매출현황 주문상세 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getSettleInfo.json", method = RequestMethod.GET)
    public List<SettleInfoGetDto> getSettleInfo(DailySettleSetDto listParamDto) throws Exception {
        return partnerSettleService.getSettleInfo(listParamDto);
    }

    /**
     * 정산관리 > 정산현황
     */
    @ResponseBody
    @RequestMapping(value = "/getFinalSettle.json", method = RequestMethod.GET)
    public List<PartnerFinalSettleGetDto> getFinalSettle(PartnerFinalSettleSetDto listParamDto) throws Exception {
        return partnerSettleService.getFinalSettle(listParamDto);
    }

    /**
     * 정산관리 > 세금계산서 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getTaxBillList.json", method = RequestMethod.GET)
    public List<PartnerTaxBillListGetDto> getTaxBillList(PartnerSettleListSetDto listParamDto) throws Exception {
        return partnerSettleService.getTaxBillList(listParamDto);
    }

    /**
     * 정산관리 > 부가세 신고내역 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getSalesVATList.json", method = RequestMethod.GET)
    public List<PartnerSalesVATListGetDto> getSalesVATList(PartnerSettleListSetDto listParamDto) throws Exception {
        return partnerSettleService.getSalesVATList(listParamDto);
    }

    /**
     * 정산관리 > 부가세 신고내역 조회 > 주문 상세내역
     */
    @ResponseBody
    @RequestMapping(value = "/getSalesVATOrder.json", method = RequestMethod.GET)
    public List<PartnerSalesVATOrderGetDto> getSalesVATOrder(PartnerSettleListSetDto listParamDto) throws Exception {
        return partnerSettleService.getSalesVATOrder(listParamDto);
    }

    /**
     * 정산관리 > 부가세 신고내역 조회 > 정산조정 상세내역
     */
    @ResponseBody
    @RequestMapping(value = "/getSalesAdjustList.json", method = RequestMethod.GET)
    public List<PartnerSalesAdjustListGetDto> getSalesAdjustList(PartnerSettleListSetDto listParamDto) throws Exception {
        return partnerSettleService.getSalesAdjustList(listParamDto);
    }
}
