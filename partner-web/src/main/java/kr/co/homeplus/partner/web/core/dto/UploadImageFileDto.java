package kr.co.homeplus.partner.web.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * image upload.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadImageFileDto {
    // 필수 - 이미지 업로드 시에 필수값으로 들어가는 key 값.
    private String processKey;

    // 모드 - (IMG: 이미지, FILE: 파일)
    private String mode;

    // 이미지를 업로드 할 때 사용할 이미지의 form field name
    private String imageFieldName;

    private String file;

    // url 을 전달해서 업로드 할 경우 사용. -> imageFieldName 이 있을 경우 무시.
    private String imageUrl;

    // 옵션으로 들어갈 데이터를 추가한다.
    private String fileName;

    public UploadImageFileDto(String processKey, String fileName, String imgUrl, String mode) {
        this.fileName = fileName;
        this.mode = mode;
        this.processKey = processKey;
        this.imageUrl = imgUrl;
    }
}
