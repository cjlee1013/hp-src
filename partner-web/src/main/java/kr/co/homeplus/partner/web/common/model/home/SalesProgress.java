package kr.co.homeplus.partner.web.common.model.home;

import lombok.Getter;
import lombok.Setter;

/**
 * 판매진행형황 카운트 응답 DTO
 */
@Setter
@Getter
public class SalesProgress {
    /** 신규주문 건수 **/
    private String newCnt;

    /** 발송대기 건수 **/
    private String readyCnt;

    /** 주문확인지연 건수 **/
    private String newDelayCnt;

    /** 발송지연 건수 **/
    private String shipDelayCnt;

    /** 배송중 건수 **/
    private String shippingCnt;
}
