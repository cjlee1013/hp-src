package kr.co.homeplus.partner.web.common.model.cnotice;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiNoticeDescSelectDto {
    private Long noticeNo;
    private String kindNm;
    private String topYn;
    private String noticeTitle;
    private String noticeDesc;
    private String regDt;
    private Long nextNoticeNo;
    private Long prevNoticeNo;
    private List<ApiNoticeFileDto> file;
}
