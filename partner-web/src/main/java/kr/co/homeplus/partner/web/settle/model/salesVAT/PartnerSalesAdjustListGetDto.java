package kr.co.homeplus.partner.web.settle.model.salesVAT;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PartnerSalesAdjustListGetDto {
    @RealGridColumnInfo(headText = "등록일", width = 120)
    @ApiModelProperty(notes = "등록일")
    private String regDt;

    @RealGridColumnInfo(headText = "상품번호", width = 100)
    @ApiModelProperty(notes = "상품번호")
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 350)
    @ApiModelProperty(notes = "상품명")
    private String itemNm;

    @RealGridColumnInfo(headText = "유형", width = 120)
    @ApiModelProperty(notes = "유형")
    private String adjustType;

    @RealGridColumnInfo(headText = "구분", width = 90)
    @ApiModelProperty(notes = "구분")
    private String gubun;

    @RealGridColumnInfo(headText = "조정금액", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    @ApiModelProperty(notes = "조정금액")
    private String adjustAmt;

    @RealGridColumnInfo(headText = "조정수수료", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    @ApiModelProperty(notes = "조정수수료")
    private String adjustFee;

    @RealGridColumnInfo(headText = "설명", width = 200)
    @ApiModelProperty(notes = "설명")
    private String comment;

    @RealGridColumnInfo(headText = "별도정산", width = 100)
    @ApiModelProperty(notes = "별도정산")
    private String separateYn;

}
