package kr.co.homeplus.partner.web.common.service;


import java.util.List;
import java.util.Map;
import kr.co.homeplus.partner.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.partner.web.common.model.codeMng.MngCodeGetDto;
import org.springframework.ui.Model;

public interface CodeService {
	Map<String, List<MngCodeGetDto>> getCode(String... gmcCd) throws Exception;

	Map<String, List<MngCodeGetDto>> getCodeModel(Model model, String... gmcCd);
}
