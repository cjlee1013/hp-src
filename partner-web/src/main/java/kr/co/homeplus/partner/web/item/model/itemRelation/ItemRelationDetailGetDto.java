package kr.co.homeplus.partner.web.item.model.itemRelation;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true, indicator = true)
@ApiModel("연관 상품관리 상세 Get Entry")
public class ItemRelationDetailGetDto {

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 120, sortable = true)
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", columnType = RealGridColumnType.NAME, width = 250)
    private String itemNm;

    @ApiModelProperty(value = "상품상태명")
    @RealGridColumnInfo(headText = "상품상태명", width = 100, sortable = true)
    private String itemStatusNm;

    @ApiModelProperty(value = "판매가격")
    @RealGridColumnInfo(headText = "판매가격", width = 100, sortable = true)
    private String salePrice;

    @ApiModelProperty(value = "판매시작일")
    @RealGridColumnInfo(headText = "판매시작일", width = 170, sortable = true)
    private String saleStartDt;

    @ApiModelProperty(value = "판매종료일")
    @RealGridColumnInfo(headText = "판매종료일", width = 170, sortable = true)
    private String saleEndDt;

    @ApiModelProperty(value = "성인상품유형명")
    @RealGridColumnInfo(headText = "성인상품유형", width = 100, sortable = true)
    private String adultTypeNm;

    @ApiModelProperty(value = "장바구니 제한여부명")
    @RealGridColumnInfo(headText = "장바구니제한", width = 100, sortable = true)
    private String cartLimitYnNm;

    @ApiModelProperty(value="장바구니 제한여부")
    @RealGridColumnInfo(headText = "장바구니제한여부", hidden = true)
    private String cartLimitYn;

    @ApiModelProperty(value="성인상품유형")
    @RealGridColumnInfo(headText = "성인상품유형",  hidden = true)
    private String adultType;

    @ApiModelProperty(value="텍스트옵션")
    @RealGridColumnInfo(headText = "텍스트옵션",  hidden = true)
    private String optTxtUseYn;

    @ApiModelProperty(value="텍스트옵션")
    @RealGridColumnInfo(headText = "텍스트옵션", width = 100, sortable = true)
    private String optTxtUseYnNm;

    @ApiModelProperty(value = "대분류 카테고리명")
    @RealGridColumnInfo(headText = "대분류", width = 150, sortable = true)
    private String lcateNm;

    @ApiModelProperty(value = "중분류 카테고리명")
    @RealGridColumnInfo(headText = "중분류", width = 150, sortable = true)
    private String mcateNm;

    @ApiModelProperty(value = "소분류 카테고리명")
    @RealGridColumnInfo(headText = "소분류", width = 150, sortable = true)
    private String scateNm;

    @ApiModelProperty(value="소분류 카테고리")
    @RealGridColumnInfo(headText = "소분류",  hidden = true)
    private String scateCd;

}
