package kr.co.homeplus.partner.web.core.constants;

/**
 * 어드민 내 상수를 저장하는 클래스
 */
public class AdminConstants {
    public static final String ADMIN_USER_TOKEN = "userInfo";

    /** 상품 이미지 업로드시에 사용하는 uri */
    public static final String ADMIN_IMAGE_UPLOAD_URI = "/store/single/image";

    /** 상품 이미지 업로드시에 사용하는 uri (URL 업로드) */
    public static final String ADMIN_IMAGE_UPLOAD_URL_URI = "/store/url/image";

    /** 상품 이미지 업로드시에 사용하는 파일의 key name */
    public static final String ADMIN_IMAGE_UPLOAD_FIELD_NAME = "file";

    /** 상품 이미지 업로드시에 사용하는 server key name */
    public static final String ADMIN_IMAGE_UPLOAD_KEY = "processKey";

    /** role 권한 인증 시에 사용할 URI -> {roleSeq} 를 치환하여 사용. */
    public static final String ADMIN_ROLE_CHECK_URI = "/has/{roleSeq}/auth";

    public static final String ADMIN_PRODUCT_API_CHANNEL = "Admin";

    /** 이미지 업로드 시 사용할 url 필드 명. */
    public static final String ADMIN_IMAGE_UPLOAD_FILE_URL = "url";

    /** 이미지 업로드 시 유효성 검사에 사용할 URI */
    public static final String ADMIN_IMAGE_UPLOAD_VALIDATOR = "/store/single/image";

    /** 이미지 업로드 시 유효성 검사에 사용되는 필수 값 'mode'에서 사용 */
    public static final String ADMIN_IMAGE_UPLOAD_VALIDATOR_MOD = "validate";

    /** 사원 검색 시 사용할 URI */
    public static final String ADMIN_MEMBER_SEARCH = "/common/users";

    /** 프론트 회원 검색시 사용할 URI */
    public static final String FRONT_USER_SEARCH = "/admin/popup/findFrontUserList";

    /** 부서검색에서 사용할 URI */
    public static final String ADMIN_DEPARTMENT_SEARCH = "/common/departmentList";

    /** 파일 업로드시에 사용하는 uri */
    public static final String ADMIN_FILE_UPLOAD_URI = "/store/single/file";

    /** 파일 업로드시에 사용하는 파일의 key name */
    public static final String ADMIN_FILE_UPLOAD_FIELD_NAME = "file";

    /** 파일 업로드시에 사용하는 server key name */
    public static final String ADMIN_FILE_UPLOAD_KEY = "file_key";

    /** 파일 업로드 시 사용할 url 필드 명. */
    public static final String ADMIN_FILE_UPLOAD_FILE_URL = "file_url";
}
