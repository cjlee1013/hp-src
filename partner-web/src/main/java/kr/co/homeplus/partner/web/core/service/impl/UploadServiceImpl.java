package kr.co.homeplus.partner.web.core.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import kr.co.homeplus.partner.web.common.model.upload.UploadUrlParamDto;
import kr.co.homeplus.partner.web.core.constants.AdminConstants;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.dto.ImageFileInfoDto;
import kr.co.homeplus.partner.web.core.dto.ImageFileValidationResultDto;
import kr.co.homeplus.partner.web.core.dto.ImageFileValidationUploadDto;
import kr.co.homeplus.partner.web.core.dto.MultpartFileImageSizeInfo;
import kr.co.homeplus.partner.web.core.dto.UploadImageFileDto;
import kr.co.homeplus.partner.web.core.service.UploadService;
import kr.co.homeplus.partner.web.core.utility.UploadFileUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

@Component
@RequiredArgsConstructor
@Slf4j
public class UploadServiceImpl implements UploadService {

    private final ResourceClient resourceClient;

    @Override
    public Map<String, List<ImageFileInfoDto>> getUploadImageFileInfo(HttpServletRequest request,
            List<UploadImageFileDto> fileList) {
        Map<String, List<ImageFileInfoDto>> result = new HashMap<>();

        for (UploadImageFileDto uploadFile : fileList) {
            try{
                if (!StringUtils.isEmpty(uploadFile.getImageUrl())) {
                    // 파일이 아닌 url 을 통해 업로드한 경우.
                    UploadUrlParamDto params = UploadUrlParamDto.builder()
                            .processKey(uploadFile.getProcessKey())
                            .url(uploadFile.getImageUrl())
                            .fileName(uploadFile.getFileName())
                            .build();

                    ResponseObject<ImageFileInfoDto> res = getResponse(params);

                    if (ObjectUtils.isEmpty(res.getData())) {
                        ImageFileInfoDto imageFileInfoDto = new ImageFileInfoDto();
                        imageFileInfoDto.setError(res.getReturnMessage());

                        result.put(uploadFile.getImageFieldName(),
                                Arrays.asList(imageFileInfoDto));
                    } else {
                        result.put(uploadFile.getImageFieldName(),
                                Arrays.asList(res.getData()));
                    }
                } else if (!StringUtils.isEmpty(uploadFile.getImageFieldName())) {
                    // 파일 입력. -> 없을 경우 image_url 로 사용 가능.
                    List<MultipartFile> fileLists = UploadFileUtil
                            .getMultiPartFilesFromRequest(request, uploadFile.getImageFieldName());
                    List<ByteArrayResource> resources = UploadFileUtil
                            .getResourceFromMultipartFile(fileLists);

                    // 파일을 업로드한 경우.
                    List<ImageFileInfoDto> list = getImageFileInfoDtos(uploadFile, resources);

                    result.put("fileArr", list);
                } else {
                    // file 이 없고 url 도 없을 경우에는 에러 메시지를 생성하여 강제 세팅.
                    ImageFileInfoDto curFile = new ImageFileInfoDto();
                    curFile.setError("image file or image url is not exist!");

                    result.put(uploadFile.getImageFieldName(), Arrays.asList(curFile));
                }
            } catch (Throwable e) {
                // 예외 발생 시에 발생한 Exception 을 ResponseError 형태로 만들어 넣어준다.
                // 여러 개의 파일 처리 중 하나만 발생한 경우 그 파일에서만 에러 처리 가능.
                if (result.get(uploadFile.getImageFieldName()) == null) {
                    while (e.getCause() != null) {
                        e = e.getCause();
                    }

                    ImageFileInfoDto curFile = new ImageFileInfoDto();
                    curFile.setError(e.getMessage());
                    result.put(uploadFile.getImageFieldName(), Arrays.asList(curFile));
                }
            }
        }

        return result;
    }

    @Override
    public List<ImageFileValidationResultDto> validateUploadImageFile(
            HttpServletRequest httpServletRequest, List<UploadImageFileDto> fileList) {
        List<ImageFileValidationResultDto> result = new ArrayList<>();

        for (UploadImageFileDto uploadFile : fileList) {
            try {
                List<MultipartFile> list = UploadFileUtil
                        .getMultiPartFilesFromRequest(httpServletRequest,
                                uploadFile.getImageFieldName());

                // 파일이 존재 할 경우
                if (!list.isEmpty() && multipartFilesListValidate(list)) {
                    result.addAll(getImageFileValidationResponse(uploadFile, list));

                    // 파일이 아닌 url 을 통해 업로드한 경우.
                } else if (!StringUtils.isEmpty(uploadFile.getImageUrl())) {
                    result.add(getImageInformationValidationResponse(
                            getURLImageInformation(uploadFile.getImageUrl(),
                                    uploadFile.getProcessKey())));
                } else {
                    //file, url이 없을 경우에는 에러 메시지를 강제 생성.
                    ImageFileValidationResultDto curFile = new ImageFileValidationResultDto();
                    curFile.setError("image file or image url is not exist!");
                    result.add(curFile);
                }
            } catch (Throwable e) {
                // 예외 발생 시에 발생한 Exception 을 ResponseError 형태로 생성
                while (e.getCause() != null) {
                    e = e.getCause();
                }

                ImageFileValidationResultDto curFile = new ImageFileValidationResultDto();
                curFile.setError(e.getMessage());
                result.add(curFile);
            }
        }
        return result;
    }

    /**
     * 하나의 필드명에 여러개의 파일이 배열로 넘어올 수 있기 때문에 하나의 필드에 대한 각각의 resource 를 배열로 입력하여
     * List<ImageFileInfoDto>반환.
     *
     * @param uploadFile
     * @param resources
     * @return
     * @throws IOException
     */
    private List<ImageFileInfoDto> getImageFileInfoDtos(UploadImageFileDto uploadFile,
            List<ByteArrayResource> resources) throws Exception {
        List<ImageFileInfoDto> list = new ArrayList<>();
        String uri = AdminConstants.ADMIN_IMAGE_UPLOAD_URI;

        if (uploadFile.getMode().equals("FILE")) {
            uri = AdminConstants.ADMIN_FILE_UPLOAD_URI;
        }

        for (ByteArrayResource resource : resources) {
            MultiValueMap<String, Object> data = getImageMultiValueMap(uploadFile);
            data.add(AdminConstants.ADMIN_IMAGE_UPLOAD_FIELD_NAME, resource);

            ResponseObject<ImageFileInfoDto> responseObject = getImageFileInfoDtoResponseObject(
                    data, uri);
            list.add(getImageFileInfoDto(responseObject));
        }
        return list;
    }

    /**
     * ResponseObject<ImageFileInfoDto> 결과를 넘겨서 하나의 이미지 정보 반환.
     *
     * @param responseObject
     * @return
     */
    private ImageFileInfoDto getImageFileInfoDto(ResponseObject<?> responseObject) throws Exception {
        ImageFileInfoDto curFile = new ImageFileInfoDto();
        if (responseObject.getReturnStatus() == HttpStatus.OK.value() && responseObject.getReturnCode().equals("SUCCESS")) {
            // 정상일 경우 결과 세팅.
            curFile = (ImageFileInfoDto) responseObject.getData();
            curFile.getFileStoreInfo().setFileName(
                    URLDecoder.decode(curFile.getFileStoreInfo().getFileName(),
                            StandardCharsets.UTF_8));
        } else {
            // error 가 있을 경우 받아온 에러 그대로 세팅.
            curFile.setError(responseObject.getReturnMessage());
        }
        return curFile;
    }

    /**
     * UploadImageFileDto 를 받아서 기본 정보를 세팅한 MultiValueMap<String, Object> 를 반환한다.
     *
     * @param uploadFile
     * @return
     */
    private MultiValueMap<String, Object> getImageMultiValueMap(UploadImageFileDto uploadFile) {
        MultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
        data.add(AdminConstants.ADMIN_IMAGE_UPLOAD_KEY, uploadFile.getProcessKey());
        data.add("fileName", uploadFile.getFileName());

        return data;
    }

    /**
     * MultiValueMap<String, Object> 을 받아서 resourceClient.postForFileUploadResponseObject 를 호출하는
     * 함수.
     *
     * @param data
     * @return
     */
    private ResponseObject<ImageFileInfoDto> getImageFileInfoDtoResponseObject(
            MultiValueMap<String, Object> data,
            String uri) {
        ResponseObject<Object> responseObject = resourceClient
                .postForFileUploadResponseObject(
                        ResourceRouteName.UPLOAD,
                        data,
                        uri,
                        new ParameterizedTypeReference<>() {
                        });

        return parseResponseObject(responseObject);
    }

    private ResponseObject<ImageFileInfoDto> getResponse(UploadUrlParamDto params) {
        ResponseObject<Object> responseObject = resourceClient
                .postForResponseObject(ResourceRouteName.UPLOAD,
                        params,
                        AdminConstants.ADMIN_IMAGE_UPLOAD_URL_URI,
                        new ParameterizedTypeReference<>() {
                        });
        return parseResponseObject(responseObject);
    }

    private ResponseObject<ImageFileInfoDto> parseResponseObject(ResponseObject<Object> responseObject) {
        ResponseObject<ImageFileInfoDto> result = new ResponseObject<>();
        result.setReturnCode(responseObject.getReturnCode());
        result.setReturnStatus(responseObject.getReturnStatus());

        if (StringUtils.equals(responseObject.getReturnCode(), "SUCCESS")) {
            ObjectMapper objectMapper = new ObjectMapper();
            ImageFileInfoDto dto = objectMapper.convertValue(responseObject.getData(), ImageFileInfoDto.class);
            result.setData(dto);
        } else {
            result.setReturnMessage(responseObject.getReturnMessage());
        }
        return result;
    }

    /**
     * 업로드 한 이미지의 파일정보를 읽어들여 {@link ImageFileValidationResultDto} 객체에 입력하여 반환한다.
     *
     * @param uploadImageFileDto 업로드 파일정보
     * @param fileList           추출대상 Multipartfile List
     * @return imageFileValidationDtoList
     */
    private List<ImageFileValidationResultDto> getImageFileValidationResponse(
            UploadImageFileDto uploadImageFileDto, List<MultipartFile> fileList) throws IOException {
        List<ImageFileValidationResultDto> imageFileInfoDtoList = new ArrayList<>();

        for (MultipartFile multipartFile : fileList) {
            //이미지 파일 정보 저장
            ImageFileValidationUploadDto uploadFileDto = new ImageFileValidationUploadDto();
            uploadFileDto.setImageKey(uploadImageFileDto.getProcessKey());
            uploadFileDto.setMimeType(multipartFile.getContentType());
            uploadFileDto.setSize(multipartFile.getSize());

            //content-type이 image일 경우만 multipartFile내 이미지의 사이즈를 반환(BufferedImage 사용)
            if (org.apache.commons.lang3.StringUtils
                    .contains(multipartFile.getContentType(), "image")) {
                MultpartFileImageSizeInfo multipartfileImageSize = getMultipartfileImageSize(
                        multipartFile);
                uploadFileDto.setWidth(multipartfileImageSize.getWidth());
                uploadFileDto.setHeight(multipartfileImageSize.getHeight());
            }

            //이미지 서버에 검증 요청하여 imageFileInfoDto에 결과를 넘겨 반환
            imageFileInfoDtoList.add(getImageInformationValidationResponse(uploadFileDto));
        }
        return imageFileInfoDtoList;
    }

    /**
     * URL로 업로드 한 이미지의 파일정보를 읽어들여 {@link ImageFileValidationResultDto} 객체에 입력하여 반환한다.
     *
     * @param uploadFileDto 추출대상 ImageFileInfoDto
     * @return imageFileValidationDtoList
     */
    private ImageFileValidationResultDto getImageInformationValidationResponse(
            ImageFileValidationUploadDto uploadFileDto) throws IOException {
        MultiValueMap<String, Object> postImageFileInformation = new LinkedMultiValueMap<>();
        postImageFileInformation
                .add("mode", AdminConstants.ADMIN_IMAGE_UPLOAD_VALIDATOR_MOD);  //'validate'
        postImageFileInformation.add("image_key", uploadFileDto.getImageKey());
        postImageFileInformation.add("type", uploadFileDto.getMimeType());
        postImageFileInformation.add("size", uploadFileDto.getSize());
        postImageFileInformation.add("width", uploadFileDto.getWidth());
        postImageFileInformation.add("height", uploadFileDto.getHeight());

        //이미지 서버에 검증 요청
        ResponseObject<ImageFileValidationResultDto> responseObject = getImageFileValidationDtoResponseObject(
                postImageFileInformation);

        //imageFileInfoDto에 결과를 넘겨 반환
        return getResponseToImageFileValidationResult(responseObject);
    }

    /**
     * MultipartFile를 받아 이미지 정보를 반환 한다.<br> BufferedImage를 사용하여 이미지 사이즈를 추출한다.
     *
     * @param multipartFile 추출대상 Multipartfile
     * @return imageInformation 이미지 파일 정보({@link MultpartFileImageSizeInfo} )
     */
    private MultpartFileImageSizeInfo getMultipartfileImageSize(MultipartFile multipartFile)
            throws IOException {
        //BufferedImage를 이용한 가로, 세로 크기 조회
        BufferedImage image = ImageIO.read(multipartFile.getInputStream());

        MultpartFileImageSizeInfo imageInformation = new MultpartFileImageSizeInfo();
        imageInformation.setHeight(image.getHeight());
        imageInformation.setWidth(image.getWidth());

        return imageInformation;
    }

    /**
     * URL을 받아 이미지 정보를 반환 한다. BufferedImage를 사용하여 이미지 사이즈를 추출한다.
     *
     * @param imageUrl 추출대상 URL
     * @return imageInformation 이미지 파일 정보({@link ImageFileValidationUploadDto} )
     */
    private ImageFileValidationUploadDto getURLImageInformation(String imageUrl, String imageKey)
            throws IOException {
        //url connection을 이용한 정보 조회
        URL url = new URL(imageUrl);
        URLConnection urlConnection = url.openConnection();
        String mimeType = urlConnection.getContentType();
        long fileSize = urlConnection.getContentLength();

        ImageFileValidationUploadDto imageInformation = new ImageFileValidationUploadDto();
        imageInformation.setImageKey(imageKey);
        imageInformation.setMimeType(mimeType);
        imageInformation.setSize(fileSize);

        //content-type이 image일 경우만 BufferedImage를 이용해 가로, 세로 크기 조회
        if (org.apache.commons.lang3.StringUtils.contains(mimeType, "image")) {
            BufferedImage image = ImageIO.read(url);
            imageInformation.setWidth(image.getWidth());
            imageInformation.setHeight(image.getHeight());
        }
        return imageInformation;
    }

    /**
     * 이미지의 정보를 resourceClient을 이용하여 이미지 서버의 Validation Api에 전달하여 유효성 검사결과를 반환한다
     *
     * @param data 이미지 정보(MultiValueMap)
     * @return responseObject 유효성 검사 결과({@link ImageFileInfoDto}
     */
    private ResponseObject<ImageFileValidationResultDto> getImageFileValidationDtoResponseObject(
            MultiValueMap<String, Object> data) {
        return resourceClient.postForFileUploadResponseObject(
                ResourceRouteName.IMAGE_UPLOAD_VALIDATION,
                data,
                AdminConstants.ADMIN_IMAGE_UPLOAD_VALIDATOR,
                new ParameterizedTypeReference<>() {
                });
    }

    /**
     * ResponseObject<ImageFileValidationResultDto> 응답 결과를 {@link ImageFileValidationResultDto}에
     * 입력하여 반환한다.<br> 만약 응답결과 내에 <tt>에러</tt>가 있을 경우 에러(errors)에 그대로 세팅한다.
     *
     * @param responseObject 유효성 검사결과
     * @return ImageFileValidationResultDto
     */
    private ImageFileValidationResultDto getResponseToImageFileValidationResult(
            ResponseObject<?> responseObject) {
        ImageFileValidationResultDto currentValidationResponse = new ImageFileValidationResultDto();
        if (responseObject.getReturnStatus() == HttpStatus.OK.value()) {
            // 정상일 경우 result : success
            currentValidationResponse
                    .setResult(((ImageFileValidationResultDto) responseObject.getData()).getResult());
        } else {
            // 에러일 경우 result : fail
            String data = (String) responseObject.getData();
            currentValidationResponse.setResult(data);
            // error 가 있을 경우 받아온 에러 그대로 세팅.
            currentValidationResponse.setError(data);
        }
        return currentValidationResponse;
    }

    /**
     * 업로드 된 multipartFile가 빈 값(empty List)인지 검증한다.<p>
     * <p>
     * 값이 비어있는 multipart List가 넘어올 경우를 대비하여 생성한 메소드
     *
     * @param multipartFileList 업로드 된 multipartfile list
     * @return multipart에 내에 파일 이름이 있는경우 true, 아니면("") false
     */
    private Boolean multipartFilesListValidate(List<MultipartFile> multipartFileList) {
        for (MultipartFile multipartFile : multipartFileList) {
            if (StringUtils.isEmpty(multipartFile.getOriginalFilename())) {
                return false;
            }
        }
        return true;
    }
}