package kr.co.homeplus.partner.web.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * MultipartFile 내의 이미지 사이즈 정보를 담는 클래스
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MultpartFileImageSizeInfo {
    /**
     * MultipartFile Image의 가로 사이즈(너비)
     */
    private int width;
    /**
     * MultipartFIle Image의 세로 사이즈(높이)
     */
    private int height;
}
