package kr.co.homeplus.partner.web.partner.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 파트너관리 > 판매업체관리 > 종목 조회
 */
@Getter
@Setter
public class PartnerBizCateCdGetDto {

    private String typeCode;

    private String codeNm;

}
