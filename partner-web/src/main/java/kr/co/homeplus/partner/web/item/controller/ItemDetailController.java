package kr.co.homeplus.partner.web.item.controller;

import java.util.List;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.utility.StringUtil;
import kr.co.homeplus.partner.web.item.model.brand.BrandListParamDto;
import kr.co.homeplus.partner.web.item.model.brand.BrandListSelectDto;
import kr.co.homeplus.partner.web.item.model.category.AttributeItemListSelectDto;
import kr.co.homeplus.partner.web.item.model.item.ItemProviderGetDto;
import kr.co.homeplus.partner.web.item.model.maker.MakerListParamDto;
import kr.co.homeplus.partner.web.item.model.maker.MakerListSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item")
public class ItemDetailController {

    private final ResourceClient resourceClient;
    private final CodeService codeService;
    private final CertificationService certificationService;

    public ItemDetailController(ResourceClient resourceClient,
        CodeService codeService,
        CertificationService certificationService
    ) {
        this.resourceClient = resourceClient;
        this.codeService = codeService;
        this.certificationService = certificationService;
    }

    /**
     * 브랜드 리스트 조회
     * @param listParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/brand/getBrandList.json"}, method = RequestMethod.GET)
    public List<BrandListSelectDto> getBrandList(BrandListParamDto listParamDto) {

        String apiUri = "/po/item/brand/getBrandList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<BrandListSelectDto>>>() {};
        return (List<BrandListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, BrandListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * 제조사 리스트 조회
     * @param listParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/maker/getMakerList.json"}, method = RequestMethod.GET)
    public List<MakerListSelectDto> getMakerList(MakerListParamDto listParamDto) {
        String apiUri = "/po/item/maker/getMakerList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<MakerListSelectDto>>>() {};
        return (List<MakerListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil
            .getRequestString(apiUri, MakerListParamDto.class, listParamDto), typeReference).getData();
    }


    /**
     * 삼품속성 조회
     * @param gattrType
     * @param itemNo
     * @param scateCd
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/attribute/getAttributeItemList.json"}, method = RequestMethod.GET)
    public List<AttributeItemListSelectDto> getAttributeList(
        @RequestParam String gattrType,
        @RequestParam(required = false, defaultValue = "") String itemNo,
        @RequestParam(required = false, defaultValue = "") String scateCd
    ) {

        String apiUri = "/po/item/attribute/getAttributeItemList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<AttributeItemListSelectDto>>>() {};
        return (List<AttributeItemListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?gattrType=" + gattrType + "&itemNo=" + itemNo + "&scateCd=" + scateCd , typeReference).getData();
    }

    /**
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getItemProviderList.json", method = RequestMethod.GET)
    public List<ItemProviderGetDto> getItemProviderList() {
        return resourceClient.getForResponseObject(ResourceRouteName.ITEM,
            "/po/item/getItemProviderList",
            new ParameterizedTypeReference<ResponseObject<List<ItemProviderGetDto>>>() {
            }).getData();
    }
}
