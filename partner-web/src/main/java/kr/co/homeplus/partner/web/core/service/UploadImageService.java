package kr.co.homeplus.partner.web.core.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.partner.web.core.dto.ImageFileInfoDto;
import kr.co.homeplus.partner.web.core.dto.ImageFileValidationResultDto;
import kr.co.homeplus.partner.web.core.dto.UploadImageFileDto;

/**
 * 이미지 업로드와 관련된 기능을 제공하는 서비스
 */
public interface UploadImageService {
    /**
     * 이미지 업로드를 할 경우 사용하는 서비스.
     * UploadImageFileDto List 를 받아서 한개씩 업로드를 하고 결과를 Map<String, List<ImageFileInfoDto>> 형식으로 반환한다.
     * 하나의 fieldName 에 파일 업로드 시에 multiple 옵션을 사용하면 배열 형태로 업로드가 가능하기 때문.
     *
     * @param request
     * @param fileList
     * @return
     * @throws IOException
     */
    Map<String, List<ImageFileInfoDto>> getUploadImageFileInfo(HttpServletRequest request,
        List<UploadImageFileDto> fileList);

    /**
     * <p>
     * 이미지 업로드 전 유효성 검사를 제공하는 서비스<p>
     * 이미지 파일을 업로드 하기 전 이미지 서버의 유효성 검사 기능을 호출하여<br>
     * 유효성 검사결과를 리턴한다.
     *
     * @param httpServletRequest 전달 된 request
     * @param fileList 이미지 업로드 대상 MultiPartFile List
     * @return
     */
    List<ImageFileValidationResultDto> validateUploadImageFile(
        HttpServletRequest httpServletRequest, List<UploadImageFileDto> fileList);
}
