package kr.co.homeplus.partner.web.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.HashMap;
import java.util.Map;
import lombok.Data;

/**
 * file upload.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadFileParamDto {
    /** 필수 - 파일 업로드 시에 필수값으로 들어가는 key 값. */
    private String fileKey;

    /** 파일 업로드 할 때 사용할 이미지의 form field name */
    private String fileFieldName;

    /** url 을 전달해서 업로드 할 경우 사용. -> fileFieldName 이 있을 경우 무시. */
    private String fileUrl;

    /** 옵션으로 들어갈 데이터를 추가한다. */
    private Map<String, Object> optionData;

    public UploadFileParamDto(String file_key) {
        this.optionData = new HashMap<>();
        this.fileKey = file_key;
    }

    public void addOptionData(String key, Object value) {
        this.optionData.put(key, value);
    }
}
