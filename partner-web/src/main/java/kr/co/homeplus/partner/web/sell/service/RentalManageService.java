package kr.co.homeplus.partner.web.sell.service;

import java.util.List;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.sell.model.rentalManage.RentalManageCntGetDto;
import kr.co.homeplus.partner.web.sell.model.rentalManage.RentalManageCounselCompleteSetDto;
import kr.co.homeplus.partner.web.sell.model.rentalManage.RentalManageCounselReservationSetDto;
import kr.co.homeplus.partner.web.sell.model.rentalManage.RentalManageListGetDto;
import kr.co.homeplus.partner.web.sell.model.rentalManage.RentalManageListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class RentalManageService {

  private final ResourceClient resourceClient;
  private final SellCommonService sellCommonService;

  /**
   * 렌탈판매관리 리스트 조회
   */
  public List<RentalManageListGetDto> getRentalManageList(RentalManageListSetDto rentalManageListSetDto) {
    rentalManageListSetDto.setPartnerId(sellCommonService.getUserCd());

    List<RentalManageListGetDto> rentalManageListGetDtoList = resourceClient.postForResponseObject(
                                                              ResourceRouteName.SHIPPING,
                                                              rentalManageListSetDto,
                                                              "/partner/sell/getRentalManageList",
                                                              new ParameterizedTypeReference<ResponseObject<List<RentalManageListGetDto>>>(){}).getData();

    for(RentalManageListGetDto dto : rentalManageListGetDtoList) {
      dto.setShipStatusNm(this.getRentalStatusNm(dto.getShipStatus()));
    }

    return rentalManageListGetDtoList;
  }

  /**
   * 렌탈판매관리 카운트 리스트 조회
   */
  public List<RentalManageListGetDto> getRentalManageCntList(String schType) {
    String partnerId = sellCommonService.getUserCd();

    List<RentalManageListGetDto> rentalManageListGetDtoList = resourceClient.getForResponseObject(
                                                              ResourceRouteName.SHIPPING,
                                                              "/partner/sell/getRentalManageCntList?schType="+schType+"&partnerId="+partnerId,
                                                              new ParameterizedTypeReference<ResponseObject<List<RentalManageListGetDto>>>() {}).getData();

    for(RentalManageListGetDto dto : rentalManageListGetDtoList) {
        dto.setShipStatusNm(this.getRentalStatusNm(dto.getShipStatus()));
    }

    return rentalManageListGetDtoList;
  }

  /**
   * 렌탈판매관리 카운트 조회
   */
  public RentalManageCntGetDto getRentalManageCnt() {
    String partnerId = sellCommonService.getUserCd();

    return resourceClient.getForResponseObject(
        ResourceRouteName.SHIPPING,
        "/partner/sell/getRentalManageCnt?partnerId="+partnerId,
        new ParameterizedTypeReference<ResponseObject<RentalManageCntGetDto>>() {}).getData();
  }

  /**
   * 상담예약
   */
  public ResponseObject<Object> setCounselReservation(RentalManageCounselReservationSetDto rentalManageCounselReservationSetDto) throws Exception {
    rentalManageCounselReservationSetDto.setChgId(sellCommonService.getUserCd());

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        rentalManageCounselReservationSetDto,
        "/partner/sell/setCounselReservation",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

  /**
   * 상담완료
   */
  public ResponseObject<Object> setCounselComplete(RentalManageCounselCompleteSetDto rentalManageCounselCompleteSetDto) throws Exception {
    rentalManageCounselCompleteSetDto.setChgId(sellCommonService.getUserCd());

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        rentalManageCounselCompleteSetDto,
        "/partner/sell/setCounselComplete",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

  /**
   * 렌탈 주문 상태명 가져오기
   */
  private String getRentalStatusNm(String shipStatusCd) {
    switch (shipStatusCd) {
      case "D1" : return "상담요청";
      case "D3" : return "상담예약";
      case "D4" : return "상담완료";
      default: return shipStatusCd;
    }
  }
}