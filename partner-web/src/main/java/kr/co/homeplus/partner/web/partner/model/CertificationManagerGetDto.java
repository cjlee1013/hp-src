package kr.co.homeplus.partner.web.partner.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CertificationManagerGetDto {

    private String certificationSeq;
    private String certificationMngNm;
    private String certificationMngPhone;

}
