package kr.co.homeplus.partner.web.sell.model.orderShipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 발주/발송관리 선택발송처리 팝업 리스트 응답 DTO")
public class OrderShipManageShipAllPopListGetDto {
    @ApiModelProperty(notes = "상품주문번호")
    private String orderItemNo;

    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;

    @ApiModelProperty(notes = "상품명")
    private String itemNm1;
}
