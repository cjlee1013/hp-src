package kr.co.homeplus.partner.web.core.utility.realgrid;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Deprecated(forRemoval = true)
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum RealGridButtonType {
    POPUP("popup"),
    ACTION("action");

    private String value;

    public static RealGridButtonType getType(String value) {
        for(RealGridButtonType type : RealGridButtonType.values()) {
            if(type.value.equals(value)) {
                return type;
            }
        }
        return null;
    }
}
