package kr.co.homeplus.partner.web.item.model.itemRelation;

import java.util.List;
import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemRelationDelDto {

	//연관상품번호
	@NotEmpty(message = "연관상품번호를 선택해주세요.")
	private List<Long> relationNo;

	//파트너ID
	private String partnerId;

	//등록,수정
	private String userId;
}
