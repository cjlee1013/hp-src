package kr.co.homeplus.partner.web.core.exception;

public class SessionNotFoundException extends RuntimeException {

    public SessionNotFoundException(String message) {
        super(message);
    }

    public SessionNotFoundException(String message, Exception e) {
        super(message, e);
    }
}
