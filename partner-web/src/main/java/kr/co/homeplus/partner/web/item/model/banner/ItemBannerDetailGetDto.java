package kr.co.homeplus.partner.web.item.model.banner;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemBannerDetailGetDto {

    private Long bannerNo;

    private String bannerNm;

    private String dispType;

    private String dispTypeNm;

    private String bannerDesc;

    private String lcateNm;

    private String mcateNm;

    private String scateNm;

    private String dcateNm;

    private String dcateCd;

    private String dispYn;

    private String dispStartDt;

    private String dispEndDt;

    List<ItemBannerMatchListGetDto> itemList;

}
