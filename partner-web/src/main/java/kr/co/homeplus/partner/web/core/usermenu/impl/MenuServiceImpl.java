package kr.co.homeplus.partner.web.core.usermenu.impl;

import com.google.gson.Gson;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import kr.co.homeplus.partner.web.core.config.PartnerProperties;
import kr.co.homeplus.partner.web.core.usermenu.MenuService;
import kr.co.homeplus.partner.web.core.usermenu.exception.MenuLoadFailException;
import kr.co.homeplus.partner.web.core.usermenu.model.Menus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

/**
 * LNB 메뉴 서비스
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class MenuServiceImpl implements MenuService {
    private final PartnerProperties partnerProperties;

    private Menus menus = null;

    @Override
    public Menus loadMenusFromJson() {
        final Menus menu;
        try {
            final Resource resource = new ClassPathResource(File.separator +
                partnerProperties.getMenuConfigFilePath() + partnerProperties.getMenuConfigFileName());

            menu = new Gson().fromJson(new InputStreamReader(resource.getInputStream(),
                StandardCharsets.UTF_8), Menus.class);
        } catch (Exception e) {
            throw new MenuLoadFailException("menu.json load fail", e);
        }
        return Optional.ofNullable(menu).orElseGet(Menus::new);
    }

    @Override
    public void makeMenus() {
        this.menus = this.loadMenusFromJson();
    }

    @Override
    public Menus getMenus() {
        return this.menus;
    }
}