package kr.co.homeplus.partner.web.core.interceptor;

import static org.springframework.util.ObjectUtils.isEmpty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.co.homeplus.partner.web.core.annotation.CertificationNeedLess;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.config.AuthorizationProperties;
import kr.co.homeplus.partner.web.core.dto.UserInfo;
import kr.co.homeplus.partner.web.core.exception.SellerAgreeInterceptorException;
import kr.co.homeplus.partner.web.core.exception.handler.ExceptionCode;
import kr.co.homeplus.partner.web.core.selleragree.service.SellerAgreeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


/**
 * 파트너 약관동의 체크용 인터셉터
 */
@Slf4j
@RequiredArgsConstructor
public class SellerAgreeInterceptor extends HandlerInterceptorAdapter {

    private final SellerAgreeService sellerAgreeService;
    private final CertificationService certificationService;
    private final AuthorizationProperties properties;

    //파트너 동의처리가 N일 경우
    private static final String ACCEPT_YN_N_CODE = "N";
    //파트너 ajax 호출 시 규약 된 uri suffix
    private static final String REST_URI_SUFFIX = ".json";
    private static final String ATTRIBUTE_NAME_ACCEPT_YN = "acceptYn";

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response,
        final Object handler) throws Exception {

        if (!(handler instanceof HandlerMethod)) {
            return super.preHandle(request, response, handler);
        }

        UserInfo userInfo = certificationService.getLoginUerInfo();

        if (isNotCertificationNeedLess((HandlerMethod) handler) && isLogin(userInfo)) {
            final String partnerId = userInfo.getPartnerId();

            final String acceptYn = sellerAgreeService.getCheckAcceptYn(partnerId);
            final String requestUri = request.getRequestURI();
            AntPathMatcher matcher = new AntPathMatcher();

            /*
             * 현재 request uri가 체크 제외대상 uri(ajax 호출, 메인, 홈, 로그인 등)인지 검증합니다.
             * 동의처리가 되지 않은("N") 상태에서 다른 메뉴 접근시 로그아웃 처리 합니다.
             **/
            if (acceptYn.equals(ACCEPT_YN_N_CODE)
                && isNotValidateUri(requestUri, (HandlerMethod) handler, matcher)) {
                log.error("Access Denied! Seller Not Agree. access this uri:<{}>", requestUri);

                throw new SellerAgreeInterceptorException(
                    ExceptionCode.SYS_ERROR_CODE_9210.getDesc());
            }

            // 메인화면 에서 사용할 정보 세팅.
            request.setAttribute(ATTRIBUTE_NAME_ACCEPT_YN, acceptYn);
        }
        return super.preHandle(request, response, handler);
    }

    /**
     * 로그인 상태일 경우를 체크합니다.
     * @param userInfo UserInfo
     * @return 로그인 상태인 경우 true를 반환, 로그인 상태일 경우 false를 반환합니다.
     */
    private boolean isLogin(final UserInfo userInfo) {
        return !isEmpty(userInfo);
    }

    /**
     * <p>
     * {@link CertificationNeedLess} annotation 태깅 여부를 체크합니다.
     * </p>
     * 해당 어노테이션이 클래스나 메서드에 붙어있지 않은 경우를 체크합니다.
     *
     * @param handlerMethod HandlerMethod
     * @return boolean
     */
    private boolean isNotCertificationNeedLess(final HandlerMethod handlerMethod) {
        return !handlerMethod.getBeanType().isAnnotationPresent(CertificationNeedLess.class)
            || !handlerMethod.getMethod().isAnnotationPresent(CertificationNeedLess.class);
    }

    /**
     * 현재 접근한 URI가 체크 제외대상 URI 인지 체크합니다.
     * @param requestUri request URI
     * @param handler  HandlerMethod
     * @param antPathMatcher AntPathMatcher
     * @return Boolean
     */
    private boolean isNotValidateUri(final String requestUri, final HandlerMethod handler,
        final AntPathMatcher antPathMatcher) {
        return isNotAjaxUri(requestUri) && isNotIgnoreUris(requestUri, antPathMatcher, handler);
    }

    /**
     * request uri가 웹의 메뉴를 접근하는지, ajax 호출을 하는지 체크합니다.<p>
     *
     * requestd의 suffix가 .json 이면 false, 아니면 true 리턴합니다.<br>
     *
     * @param requestUri request uri
     * @return boolean suffix가 .json 이면 false, 아니면 true 리턴합니다.
     */
    private boolean isNotAjaxUri(final String requestUri) {
        return !StringUtils.endsWith(requestUri, REST_URI_SUFFIX);
    }

    /**
     * 현재 접근하는 URI가 체크 제외대상 uri가 아닌지 검증
     *
     * @param requestUri request URI
     * @param handler HandlerMethod
     * @return 제외 대상이 아니면 true, 제외 대상이면 false를 리턴합니다.
     */
    private boolean isNotIgnoreUris(final String requestUri, final AntPathMatcher antPathMatcher,
        final HandlerMethod handler) {

        // CertificationNeedLess annotation 이 클래스나 메서드에 붙어 있을 경우 체크 무시
        if (handler.getBeanType().isAnnotationPresent(CertificationNeedLess.class)
            || handler.getMethod().isAnnotationPresent(CertificationNeedLess.class)) {
            return false;
        }

        // 체크 제외대상 uri가 아닌지 검증
        return properties.getIgnoreAuthCheckUris().stream()
            .noneMatch(ignoreUrl -> antPathMatcher.match(ignoreUrl, requestUri));
    }
}