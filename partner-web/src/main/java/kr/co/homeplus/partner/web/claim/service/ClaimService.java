package kr.co.homeplus.partner.web.claim.service;


import java.util.List;
import kr.co.homeplus.partner.web.claim.model.ClaimBoardCountGetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimCancelListGetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimExchangeListGetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimListSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimDetailListGetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimDetailGetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimPreRefundGetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimPreRefundSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimRegSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimReturnListGetDto;
import kr.co.homeplus.partner.web.claim.model.OrderShipAddrInfoDto;
import kr.co.homeplus.partner.web.claim.model.RequestMultiCancelGetDto;
import kr.co.homeplus.partner.web.claim.model.RequestOrderShipMultiCancelSetDto;
import kr.co.homeplus.partner.web.claim.model.RequestRentalMultiCancelSetDto;
import kr.co.homeplus.partner.web.claim.model.RequestTicketMultiCancelSetDto;
import kr.co.homeplus.partner.web.claim.model.TicketRentalCancelListGetDto;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimService {

    private final ResourceClient resourceClient;
    private final CertificationService certificationService;

    /**
     * 클레임 건수 조회
     * **/
    public ClaimBoardCountGetDto getClaimBoardCount(String claimType) {
        String apiUri = "/po/claim/getClaimBoardCount";
        String partnerId = certificationService.getLoginUerInfo().getPartnerId();
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG, apiUri + "?partnerId=" + partnerId + "&claimType=" + claimType,
            new ParameterizedTypeReference<ResponseObject<ClaimBoardCountGetDto>>() {}).getData();
    }

    /**
     * 이티켓/렌탈 클레임 건수 조회
     * **/
    public ClaimBoardCountGetDto getTicketRentalClaimBoardCount(String claimType) {
        String apiUri = "/po/claim/getTicketRentalClaimBoardCount";
        String partnerId = certificationService.getLoginUerInfo().getPartnerId();
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG, apiUri + "?partnerId=" + partnerId + "&claimType=" + claimType,
            new ParameterizedTypeReference<ResponseObject<ClaimBoardCountGetDto>>() {}).getData();
    }

    /**
     * PO 클레임 리스트 조회
     * **/
    public List<ClaimCancelListGetDto> getClaimList(ClaimListSetDto claimListSetDto) {
        String apiUri = "/po/claim/getPOClaimList";
        claimListSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimListSetDto,apiUri,
            new ParameterizedTypeReference<ResponseObject<List<ClaimCancelListGetDto>>>() {}).getData();
    }

    /**
     * PO 클레임 리스트 조회
     * **/
    public Integer getClaimListCnt(ClaimListSetDto claimListSetDto) {
        String apiUri = "/po/claim/getPOClaimListCnt";
        claimListSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());

        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimListSetDto,apiUri,
            new ParameterizedTypeReference<ResponseObject<Integer>>() {}).getData();
    }



    /**
     * PO 클레임 리스트 조회
     * **/
    public List<TicketRentalCancelListGetDto> getTicketRentalCancelList(ClaimListSetDto claimListSetDto) {
        String apiUri = "/po/claim/getTicketRentalCancelList";
        claimListSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());

        //TODO 임시로 partnerId를 homeplus로 적용함 나중에 주석풀어여함!!
        //claimListSetDto.setPartnerId("homeplus");
        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimListSetDto,apiUri,
            new ParameterizedTypeReference<ResponseObject<List<TicketRentalCancelListGetDto>>>() {}).getData();
    }


    /**
     * PO 클레임 반품 리스트 조회
     * **/
    public List<ClaimReturnListGetDto> getReturnClaimList(ClaimListSetDto claimListSetDto) {
        String apiUri = "/po/claim/getPOClaimList";
        claimListSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());

        //TODO 임시로 partnerId를 homeplus로 적용함 나중에 주석풀어여함!!
        //claimListSetDto.setPartnerId("homeplus");
        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimListSetDto,apiUri,
            new ParameterizedTypeReference<ResponseObject<List<ClaimReturnListGetDto>>>() {}).getData();
    }

    /**
     * PO 클레임 교환 리스트 조회
     * **/
    public List<ClaimExchangeListGetDto> getExchangeClaimList(ClaimListSetDto claimListSetDto) {
        String apiUri = "/po/claim/getPOClaimList";
        claimListSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());

        //TODO 임시로 partnerId를 homeplus로 적용함 나중에 주석풀어여함!!
        //claimListSetDto.setPartnerId("homeplus");
        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimListSetDto,apiUri,
            new ParameterizedTypeReference<ResponseObject<List<ClaimExchangeListGetDto>>>() {}).getData();
    }

    /**
     * PO 클레임 상세 조회
     * **/
    public List<ClaimDetailListGetDto> getClaimDetailList(long purchaseOrderNo, long bundleNo) {
        String apiUri = "/po/claim/getClaimDetailList";
        return resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri + "?purchaseOrderNo=" + purchaseOrderNo + "&bundleNo=" + bundleNo,
            new ParameterizedTypeReference<ResponseObject<List<ClaimDetailListGetDto>>>() {}).getData();
    }

    /**
     * PO 클레임 팝업 - 클레임 단건 상세 조회
     * **/
    public ClaimDetailGetDto getClaimDetail(long claimBundleNo, String claimType) {
        String apiUri = "/po/claim/getClaimDetail";
        return resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri + "?claimBundleNo=" + claimBundleNo + "&claimType="+ claimType,
            new ParameterizedTypeReference<ResponseObject<ClaimDetailGetDto>>() {}).getData();
    }

    /**
     * PO 반품/교환 신청 팝업 - 배송정보 조회
     * **/
    public List<OrderShipAddrInfoDto> getOrderShipAddrInfo(long bundleNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            "/order/orderSearch/detail/pop/shipAddr/" + bundleNo,
            new ParameterizedTypeReference<ResponseObject<List<OrderShipAddrInfoDto>>>() {}
        ).getData();
    }

    /**
     * PO 판매관리 > 발주발송관리 > 취소/반품 신청 > 환불예정금액 조회
     * **/
    public ClaimPreRefundGetDto getPreRefundPrice(ClaimPreRefundSetDto claimPreRefundSetDto){
        claimPreRefundSetDto.setIsMultiOrder(Boolean.FALSE);
        claimPreRefundSetDto.setMultiBundleNo(null);
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            claimPreRefundSetDto,
            "/claim/preRefundPrice",
            new ParameterizedTypeReference<ResponseObject<ClaimPreRefundGetDto>>() {}
        ).getData();
    }

    /**
     * PO 판매관리 > 발주발송관리 > 취소/반품 신청 > 클레임 신청
     * **/
    public ResponseObject<String> claimRegister(ClaimRegSetDto claimRegSetDto){

        // 등록자 추가
        claimRegSetDto.setRegId("PARTNER");

        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            claimRegSetDto,
            "/claim/claimCancelReg",
            new ParameterizedTypeReference<ResponseObject<String>>() {
            }
        );
    }

    /**
     * PO 판매관리 > 렌탈판매관리 - 주문취소
     * **/
    public ResponseObject<RequestMultiCancelGetDto> reqMultiCancel(
        RequestRentalMultiCancelSetDto requestRentalMultiCancelSetDto){
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            requestRentalMultiCancelSetDto,
            "/claim/claimAllCancelReq",
            new ParameterizedTypeReference<ResponseObject<RequestMultiCancelGetDto>>() {
            }
        );
    }

    /**
     * PO 판매관리 > 발주발송관리 > 취소/반품 신청 > 클레임 신청
     * **/
    public ResponseObject<RequestMultiCancelGetDto> ticketMultiCancel(RequestTicketMultiCancelSetDto requestTicketMultiCancelSetDto){

        // 등록자 추가
        requestTicketMultiCancelSetDto.setRegId("PARTNER");

        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            requestTicketMultiCancelSetDto,
            "/claim/ticketAllCancelReq",
            new ParameterizedTypeReference<ResponseObject<RequestMultiCancelGetDto>>() {
            }
        );
    }

     /**
     * PO 판매관리 > 발주발송관리 > 취소/반품 신청 > 클레임 신청
     * **/
    public ResponseObject<RequestMultiCancelGetDto> orderShipMultiCancel(RequestOrderShipMultiCancelSetDto requestOrderShipMultiCancelSetDto){
        // 등록자 추가
        requestOrderShipMultiCancelSetDto.setRegId("PARTNER");
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            requestOrderShipMultiCancelSetDto,
            "/claim/orderShipMultiCancelReq",
            new ParameterizedTypeReference<ResponseObject<RequestMultiCancelGetDto>>() {
            }
        );
    }






}
