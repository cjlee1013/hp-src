package kr.co.homeplus.partner.web.core.exception.handler;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * [공통] 에러코드 정의
 */
@RequiredArgsConstructor
public enum ExceptionCode {
	//TODO 코드명, 코드값, 문구 고민이 필요합니다.
		SYS_ERROR_CODE_9001("-9001", "시스템 에러 : 관리자에게 문의해 주세요.")
	,   SYS_ERROR_CODE_9002("-9002", "시스템 에러 : 첨부파일 크기가 허용한도를 초과합니다.")
	,   SYS_ERROR_CODE_9003("-9003", "시스템 에러 : 첨부파일 등록 처리중 문제가 발생했습니다.")

	,	SYS_ERROR_CODE_9101("-9101", "통신 에러 : 잘못된 URL을 호출 하였습니다.")
	,   SYS_ERROR_CODE_9102("-9102", "통신 에러 : 내부 오류가 발생하였습니다.")
	,   SYS_ERROR_CODE_9103("-9103", "통신 에러 : API 연결에 실패하였습니다.")
	,   SYS_ERROR_CODE_9104("-9104", "통신 에러 : API 요청 대기시간이 만료되었습니다.")
	,   SYS_ERROR_CODE_9105("-9105", "통신 에러 : 잘못된 URL을 호출 하였습니다.")
	,   SYS_ERROR_CODE_9106("-9106", "통신 에러 : 요청이 허용되지 않는 메소드입니다.")

	,   SYS_ERROR_CODE_9200("-9200", "로그인 인증에 실패하였습니다.")
	,   SYS_ERROR_CODE_9201("-9201", "로그인 세션이 만료되었습니다. 다시 로그인 하시기 바랍니다.")
	,   SYS_ERROR_CODE_9206("-9206", "로그인 실패횟수 초과로 계정정보 확인 후 다시 로그인 하실 수 있습니다.")
	,   SYS_ERROR_CODE_9207("-9207", "로그인 하신 계정이 장기간 미사용 되어 잠금처리 되었습니다. 계정 사용처리 후 로그인 하실 수 있습니다.")
	,   SYS_ERROR_CODE_9209("-9209", "인증 에러 : 아이디 저장 쿠키에서 오류가 발생하였습니다.")
	,   SYS_ERROR_CODE_9210("-9210", "인증 에러 : 약관동의 없이 다른 메뉴에 접근하였습니다.")
	,   SYS_ERROR_CODE_9211("-9211", "인증 에러 : 90일동안 동일한 비밀번호를 사용중입니다.")
	,   SYS_ERROR_CODE_9212("-9212", "인증번호가 일치하지 않습니다. 다시 입력해주세요.")


	,   SYS_ERROR_CODE_9301("-9301", "필수 파라미터가 누락되었습니다.")
	,   SYS_ERROR_CODE_9302("-9302", "파라미터 유효성 검사 오류입니다.")
	,   SYS_ERROR_CODE_9303("-9303", "파라미터가 형식에 맞지 않습니다.")

	,   SYS_ERROR_CODE_9401("-9401", "허용되지 않은 URL 주소로 파일 다운로드를 시도하였습니다.")
	,   SYS_ERROR_CODE_9402("-9402", "파일 다운로드에 실패하였습니다.")
	,   SYS_ERROR_CODE_9403("-9403", "파일 다운로드 URL이 입력되지 않았습니다.")
	;

	@Getter
	private final String code;
	@Getter
	private final String desc;

	@Override
	public String toString() {
		return code + ": " + desc;
	}
}
