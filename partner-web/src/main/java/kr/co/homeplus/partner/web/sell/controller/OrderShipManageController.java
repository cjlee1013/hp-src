package kr.co.homeplus.partner.web.sell.controller;

import com.nhncorp.lucy.security.xss.XssPreventer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.partner.web.core.utility.realgrid.*;
import kr.co.homeplus.partner.web.sell.model.OrderDetailGetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.ClaimOrderDetailInfoGetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageCntGetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageConfirmOrderSetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageListGetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageListSetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageNotiDelaySetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageShipAddrInfoGetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageShipAddrInfoSetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageShipAllPopListGetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageShipAllPopListSetDto;
import kr.co.homeplus.partner.web.sell.model.orderShipManage.OrderShipManageShipAllSetDto;
import kr.co.homeplus.partner.web.sell.service.OrderShipManageService;
import kr.co.homeplus.partner.web.sell.service.SellCommonService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 판매관리 > 발주/발송관리
 */
@Controller
@RequestMapping("/sell/orderShipManage")
public class OrderShipManageController {

    @Value("${front.homeplus.url}")
    private String homeplusFrontUrl;

    @Value("${front.theclub.url}")
    private String theclubFrontUrl;

    @Autowired
    private OrderShipManageService orderShipManageService;
    @Autowired
    private SellCommonService sellCommonService;

    // 발주/발송관리 그리드
    private static final List<RealGridColumn> ORDER_SHIP_MANAGE_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption ORDER_SHIP_MANAGE_GRID_OPTION = new RealGridOption("fill", false, true, false);

    // 일괄발송처리 결과 엑셀용 그리드
    private static final List<RealGridColumn> SHIP_ALL_EXCEL_POP_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption SHIP_ALL_EXCEL_POP_GRID_OPTION = new RealGridOption("fill", false, true, false);

    // 선택발송처리 팝업 그리드
    private static final List<RealGridColumn> SHIP_ALL_POP_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption SHIP_ALL_POP_GRID_OPTION = new RealGridOption("fill", false, false, false);

    public OrderShipManageController() {
        // 발주/발송관리 그리드
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("shipNo", "shipNo", "배송SEQ", RealGridColumnType.BASIC, 80, false, false));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("purchaseOrderNo", "purchaseOrderNo", "주문번호", RealGridColumnType.BASIC, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("bundleNo", "bundleNo", "배송번호", RealGridColumnType.BASIC, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("orderItemNo", "orderItemNo", "상품주문번호", RealGridColumnType.BASIC, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("orderDt", "orderDt", "주문일시", RealGridColumnType.BASIC, 150, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("shipStatusNm", "shipStatusNm", "주문상태", RealGridColumnType.BASIC, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("shipStatus", "shipStatus", "주문상태코드", RealGridColumnType.BASIC, 80, false, false));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("itemNo", "itemNo", "상품번호", RealGridColumnType.BASIC, 120, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("itemNm1", "itemNm1", "상품명", RealGridColumnType.NONE, 350, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("txtOptVal", "txtOptVal", "옵션", RealGridColumnType.NONE, 250, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("itemQty", "itemQty", "수량", RealGridColumnType.BASIC, 60, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("itemPrice", "itemPrice", "상품금액", RealGridColumnType.PRICE, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("orderPrice", "orderPrice", "총상품금액", RealGridColumnType.PRICE, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("shipPrice", "shipPrice", "배송비", RealGridColumnType.PRICE, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("islandShipPrice", "islandShipPrice", "도서산간배송비", RealGridColumnType.PRICE, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("commissionPrice", "commissionPrice", "수수료", RealGridColumnType.PRICE, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("sellerChargePrice", "sellerChargePrice", "판매자쿠폰할인", RealGridColumnType.PRICE, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("buyerNm", "buyerNm", "구매자", RealGridColumnType.BASIC, 80, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("buyerMobileNo", "buyerMobileNo", "구매자 연락처", RealGridColumnType.BASIC, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("receiverNm", "receiverNm", "수령인", RealGridColumnType.BASIC, 80, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("shipMobileNo", "shipMobileNo", "수령인 연락처", RealGridColumnType.BASIC, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("shipMethodNm", "shipMethodNm", "배송방법", RealGridColumnType.BASIC, 90, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("shipMethod", "shipMethod", "배송방법코드", RealGridColumnType.BASIC, 80, false, false));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("dlvCd", "dlvCd", "택배사", RealGridColumnType.BASIC, 90, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("invoiceNo", "invoiceNo", "송장번호", RealGridColumnType.BASIC, 120, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("shipReqDt", "shipReqDt", "배송요청일", RealGridColumnType.BASIC, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("reserveShipDt", "scheduleShipDt", "배송예정일", RealGridColumnType.BASIC, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("zipcode", "zipcode", "우편번호", RealGridColumnType.BASIC, 80, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("addr", "addr", "주소", RealGridColumnType.NONE, 350, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("dlvShipMsg", "dlvShipMsg", "배송메세지", RealGridColumnType.NONE, 200, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("shipPriceType", "shipPriceType", "배송비지급", RealGridColumnType.BASIC, 80, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("paymentFshDt", "paymentFshDt", "결제일시", RealGridColumnType.BASIC, 150, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("confirmDt", "confirmDt", "주문확인일시", RealGridColumnType.BASIC, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("orgShipDt", "orgShipDt", "발송기한", RealGridColumnType.BASIC, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("delayShipDt", "delayShipDt", "2차발송기한", RealGridColumnType.BASIC, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("completeDt", "completeDt", "배송완료일", RealGridColumnType.BASIC, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("noRcvDeclrDt", "noRcvDeclrDt", "미수취신고일", RealGridColumnType.BASIC, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("sellerItemCd", "sellerItemCd", "업체상품코드", RealGridColumnType.BASIC, 100, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("personalOverseaNo", "personalOverseaNo", "개인통관고유번호", RealGridColumnType.BASIC, 120, true, true));
        ORDER_SHIP_MANAGE_GRID_HEAD.add(new RealGridColumn("siteType", "siteType", "사이트유형", RealGridColumnType.BASIC, 120, false, true));

        // 일괄발송처리 결과 엑셀용 그리드
        SHIP_ALL_EXCEL_POP_GRID_HEAD.add(new RealGridColumn("bundleNo", "bundleNo", "배송번호", RealGridColumnType.BASIC, 120, true, true));
        SHIP_ALL_EXCEL_POP_GRID_HEAD.add(new RealGridColumn("invoiceNo", "invoiceNo", "송장번호", RealGridColumnType.BASIC, 120, true, true));
        SHIP_ALL_EXCEL_POP_GRID_HEAD.add(new RealGridColumn("scheduleShipDt", "scheduleShipDt", "배송예정일", RealGridColumnType.BASIC, 120, true, true));
        SHIP_ALL_EXCEL_POP_GRID_HEAD.add(new RealGridColumn("failMsg", "failMsg", "실패메세지", RealGridColumnType.BASIC, 200, true, true));

        // 선택발송처리 팝업 그리드
        SHIP_ALL_POP_GRID_HEAD.add(new RealGridColumn("orderItemNo", "orderItemNo", "상품주문번호", RealGridColumnType.BASIC, 90, true, true));
        SHIP_ALL_POP_GRID_HEAD.add(new RealGridColumn("bundleNo", "bundleNo", "배송번호", RealGridColumnType.BASIC, 90, true, true));
        SHIP_ALL_POP_GRID_HEAD.add(new RealGridColumn("itemNm1", "itemNm1", "상품명", RealGridColumnType.BASIC, 180, true, true));
    }

    /**
     * 발주/발송관리 Main Page
     */
    @GetMapping("/orderShipManageMain")
    public String orderShipManageMain(Model model, @RequestParam(value = "schType", required = false, defaultValue = "") String schType) throws Exception {
        model.addAttribute("shipStatus", sellCommonService.getCode("ship_status"));
        model.addAttribute("delayShipCd", sellCommonService.getCode("delay_ship_cd"));
        model.addAttribute("dlvCd", sellCommonService.getCode("dlv_cd"));
        model.addAttribute("shipMethod", sellCommonService.getCode("ship_method"));
        model.addAttribute("shipAllPopTitle", "선택건발송처리");
        model.addAttribute("theclubFrontUrl", theclubFrontUrl);
        model.addAttribute("homeplusFrontUrl", homeplusFrontUrl);
        model.addAttribute("schType", schType);

        model.addAttribute("orderShipManageGridBaseInfo", new RealGridBaseInfo("orderShipManageGridBaseInfo", ORDER_SHIP_MANAGE_GRID_HEAD, ORDER_SHIP_MANAGE_GRID_OPTION).toString());
        model.addAttribute("shipAllExcelPopGridBaseInfo", new RealGridBaseInfo("shipAllExcelPopGridBaseInfo", SHIP_ALL_EXCEL_POP_GRID_HEAD, SHIP_ALL_EXCEL_POP_GRID_OPTION).toString());
        model.addAttribute("shipAllPopGridBaseInfo", new RealGridBaseInfo("shipAllPopGridBaseInfo", SHIP_ALL_POP_GRID_HEAD, SHIP_ALL_POP_GRID_OPTION).toString());

        return "/sell/orderShipManageMain";
    }

    /**
     * 발주/발송관리 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getOrderShipManageList.json", method = RequestMethod.GET)
    public List<OrderShipManageListGetDto> getOrderShipManageList(OrderShipManageListSetDto orderShipManageListSetDto) {
        return orderShipManageService.getOrderShipManageList(orderShipManageListSetDto);
    }

    /**
     * 발주/발송관리 카운트 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getOrderShipManageCntList.json", method = RequestMethod.GET)
    public List<OrderShipManageListGetDto> getOrderShipManageCntList(@RequestParam(value = "schType") String schType) {
        return orderShipManageService.getOrderShipManageCntList(XssPreventer.unescape(schType));
    }

    /**
     * 발주/발송관리 카운트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getOrderShipManageCnt.json", method = RequestMethod.GET)
    public OrderShipManageCntGetDto getOrderShipManageCnt() {
        return orderShipManageService.getOrderShipManageCnt();
    }

    /**
     * 주문 확인
     */
    @ResponseBody
    @PostMapping(value = "/setConfirmOrder.json")
    public ResponseObject<Object> setConfirmOrder(@RequestBody OrderShipManageConfirmOrderSetDto orderShipManageConfirmOrderSetDto) throws Exception {
        return orderShipManageService.setConfirmOrder(orderShipManageConfirmOrderSetDto);
    }

    /**
     * 발송지연안내
     */
    @ResponseBody
    @PostMapping(value = "/setNotiDelay.json")
    public ResponseObject<Object> setNotiDelay(@RequestBody OrderShipManageNotiDelaySetDto orderShipManageNotiDelaySetDto) throws Exception {
        return orderShipManageService.setNotiDelay(orderShipManageNotiDelaySetDto);
    }

    /**
     * 일괄발송처리
     */
    @ResponseBody
    @PostMapping("/setShipAllExcel.json")
    public ResponseObject<Map<String, Object>> setShipAllExcel(MultipartHttpServletRequest multipartHttpServletRequest) throws Exception {
        return orderShipManageService.setShipAllExcel(multipartHttpServletRequest);
    }

    /**
     * 선택발송처리 (배송현황 > 배송방법/송장수정)
     */
    @ResponseBody
    @PostMapping("/setShipAll.json")
    public ResponseObject<Object> setShipAll(@RequestBody OrderShipManageShipAllSetDto orderShipManageShipAllSetDto) throws Exception {
        return orderShipManageService.setShipAll(orderShipManageShipAllSetDto);
    }

    /**
     * 발송지연안내 팝업
     */
    @GetMapping("/getNotiDelayPop")
    public String getNotiDelayPop(Model model, @RequestParam(value="callback") String callBackScript) throws Exception {
        model.addAttribute("delayShipCd", sellCommonService.getCode("delay_ship_cd"));
        model.addAttribute("callBackScript",callBackScript);

        return "notiDelayPop";
    }

    /**
     * 선택발송처리 팝업 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getShipAllPopList.json", method = RequestMethod.GET)
    public List<OrderShipManageShipAllPopListGetDto> getShipAllPopList(OrderShipManageShipAllPopListSetDto orderShipManageShipAllPopListSetDto) {
        return orderShipManageService.getShipAllPopList(orderShipManageShipAllPopListSetDto);
    }

    /**
     * 배송지 수정 팝업 정보 조회
     */
    @ResponseBody
    @GetMapping("/getOrderShipAddrInfo.json")
    public List<OrderShipManageShipAddrInfoGetDto> getOrderShipAddrInfo(@RequestParam("bundleNo") String bundleNo){
        return orderShipManageService.getOrderShipAddrInfo(bundleNo);
    }

    /**
     * 배송지 수정 팝업 저장
     */
    @ResponseBody
    @PostMapping("/setOrderShipAddrInfo.json")
    public ResponseObject<String> setOrderShipAddrInfo(@RequestBody OrderShipManageShipAddrInfoSetDto orderShipManageShipAddrInfoSetDto) throws Exception {
        return orderShipManageService.setOrderShipAddrInfo(orderShipManageShipAddrInfoSetDto);
    }

    /**
     * 상품 상세정보 팝업 조회
     */
    @ResponseBody
    @GetMapping("/getOrderDetailPop.json")
    public ResponseObject<OrderDetailGetDto> getOrderDetailPop(@RequestParam("bundleNo") String bundleNo) {
        return orderShipManageService.getOrderDetailPop(bundleNo);
    }

    /**
     * 품절 주문취소 팝업 조회
     */
    @ResponseBody
    @GetMapping("/getOrderClaimListPop.json")
    public List<ClaimOrderDetailInfoGetDto> getOrderClaimListPop(@RequestParam("purchaseOrderNo") long purchaseOrderNo, @RequestParam("bundleNo") long bundleNo, @RequestParam("claimType") String claimType) {
        return orderShipManageService.getOrderClaimListPop(purchaseOrderNo, bundleNo, claimType);
    }
}
