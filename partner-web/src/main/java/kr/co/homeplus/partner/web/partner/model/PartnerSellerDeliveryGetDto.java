package kr.co.homeplus.partner.web.partner.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * 업체관리 > 파트너관리 > 판매업체관리 > 판매업체 배송 Get Entry
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerSellerDeliveryGetDto {
	private String partnerId;
	private String dlvNm;
	private String dlvCd;
	private String creditCd;
	private String useYn;
}
