package kr.co.homeplus.partner.web.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임 배송정보변경 요청 파라미터")
public class ExchShippingSetDto {

    @ApiModelProperty(value = "클레임그룹번호", position = 1, example = "1500000011")
    public long claimNo;

    @ApiModelProperty(value = "클레임번호", position = 2, example = "1500000001")
    public long claimBundleNo;

    @ApiModelProperty(value = "교환지-이름", position = 3, example = "홍길동")
    public String exchReceiverNm;

    @ApiModelProperty(value = "교환지-휴대폰", position = 4, example = "010-1111-1111")
    public String exchMobileNo;

    @ApiModelProperty(value = "교환지 우편번호", position = 5, example = "07297")
    public String exchZipcode;

    @ApiModelProperty(value = "교환지 기본주소", position = 6, example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    public String exchRoadBaseAddr;

    @ApiModelProperty(value = "교환지 샹세주소", position = 7, example = "금천전 홈플러스 1층")
    public String exchRoadAddrDetail;

    @ApiModelProperty(value = "교환지 기본주소", position = 8, example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    public String exchBaseAddr;

    @ApiModelProperty(value = "교환지 샹세주소", position = 9, example = "금천전 홈플러스 1층")
    public String exchDetailAddr;

    @ApiModelProperty(value = "교환배송수단", position = 10, example = "C")
    public String exchShipType;

    @ApiModelProperty(value = "교환택배사코드", position = 11, example = "002")
    public String exchDlvCd;

    @ApiModelProperty(value = "교환송장번호", position = 12, example = "1123123")
    public String exchInvoiceNo;

    @ApiModelProperty(value = "교환메모", position = 13, example = "우편배송 메모입니다.")
    public String exchMemo;

    @ApiModelProperty(value="교환일자", position = 14, example = "2020-12-29 00:00:00")
    public String shipDt;

}
