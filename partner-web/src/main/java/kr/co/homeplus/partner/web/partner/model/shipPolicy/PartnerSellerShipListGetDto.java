package kr.co.homeplus.partner.web.partner.model.shipPolicy;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerSellerShipListGetDto {

    private Long shipPolicyNo;
    private String defaultYn;
    private String shipPolicyNm;
    private Integer shipFee;
    private Integer claimShipFee;

}
