package kr.co.homeplus.partner.web.sell.model.shipStatus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "판매관리 > 배송현황 미수취 신고 정보 응답 DTO")
public class ShipStatusNoReceiveInfoGetDto {

    @ApiModelProperty(value= "미수취 신고 유형", position = 1)
    private String noRcvDeclrType;

    @ApiModelProperty(value= "미수취 상세 사유", position = 2)
    private String noRcvDetailReason;

    @ApiModelProperty(value= "미수취 신고 일자", position = 3)
    private String noRcvDeclrDt;

    @ApiModelProperty(value= "미수취 처리 일자", position = 4)
    private String noRcvProcessDt;

    @ApiModelProperty(value= "미수취처리유형", position = 5)
    private String noRcvProcessType;

    @ApiModelProperty(value= "처리내용", position = 6)
    private String noRcvProcessCntnt;

    @ApiModelProperty(value= "처리결과", position = 7)
    private String noRcvProcessResult;

    @ApiModelProperty(value= "등록자", position = 8)
    private String noRcvRegId;

    @ApiModelProperty(value= "처리자", position = 9)
    private String noRcvChgId;

    private void setNoRcvDeclrType(String noRcvDeclrType) {
        String noRcvDeclrTypeMsg = "";

        switch (noRcvDeclrType) {
            case "NA" : noRcvDeclrTypeMsg = "상품이 아직 도착하지 않았습니다."; break;
            case "NR" : noRcvDeclrTypeMsg = "도난이 의심됩니다."; break;
            case "NT" : noRcvDeclrTypeMsg = "배송 조회가 되지 않습니다."; break;
            case "NE" : noRcvDeclrTypeMsg = "기타"; break;
            default: noRcvDeclrTypeMsg = "";
        }

        this.noRcvDeclrType = noRcvDeclrTypeMsg;
    }
}
