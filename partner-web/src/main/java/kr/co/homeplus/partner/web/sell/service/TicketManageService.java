package kr.co.homeplus.partner.web.sell.service;

import java.util.List;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.sell.model.rentalManage.RentalManageCounselReservationSetDto;
import kr.co.homeplus.partner.web.sell.model.ticketManage.TicketManageCntGetDto;
import kr.co.homeplus.partner.web.sell.model.ticketManage.TicketManageCompleteSetDto;
import kr.co.homeplus.partner.web.sell.model.ticketManage.TicketManageConfirmOrderSetDto;
import kr.co.homeplus.partner.web.sell.model.ticketManage.TicketManageListGetDto;
import kr.co.homeplus.partner.web.sell.model.ticketManage.TicketManageListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TicketManageService {

  private final ResourceClient resourceClient;
  private final SellCommonService sellCommonService;

  /**
   * 이티켓판매관리 리스트 조회
   */
  public List<TicketManageListGetDto> getTicketManageList(TicketManageListSetDto ticketManageListSetDto) {
    ticketManageListSetDto.setPartnerId(sellCommonService.getUserCd());

    List<TicketManageListGetDto> ticketManageListGetDtoList = resourceClient.postForResponseObject(
                                                              ResourceRouteName.SHIPPING,
                                                              ticketManageListSetDto,
                                                              "/partner/sell/getTicketManageList",
                                                              new ParameterizedTypeReference<ResponseObject<List<TicketManageListGetDto>>>(){}).getData();
    return ticketManageListGetDtoList;
  }

  /**
   * 이티켓판매관리 카운트 리스트 조회
   */
  public List<TicketManageListGetDto> getTicketManageCntList(String schType) {
    String partnerId = sellCommonService.getUserCd();

    List<TicketManageListGetDto> ticketManageListGetDtoList = resourceClient.getForResponseObject(
                                                              ResourceRouteName.SHIPPING,
                                                              "/partner/sell/getTicketManageCntList?schType="+schType+"&partnerId="+partnerId,
                                                              new ParameterizedTypeReference<ResponseObject<List<TicketManageListGetDto>>>() {}).getData();
    return ticketManageListGetDtoList;
  }

  /**
   * 이티켓판매관리 카운트 조회
   */
  public TicketManageCntGetDto getTicketManageCnt() {
    String partnerId = sellCommonService.getUserCd();

    return resourceClient.getForResponseObject(
        ResourceRouteName.SHIPPING,
        "/partner/sell/getTicketManageCnt?partnerId="+partnerId,
        new ParameterizedTypeReference<ResponseObject<TicketManageCntGetDto>>() {}).getData();
  }

  /**
   * 주문확인
   */
  public ResponseObject<Object> setTicketConfirmOrder(TicketManageConfirmOrderSetDto ticketManageConfirmOrderSetDto) throws Exception {
    ticketManageConfirmOrderSetDto.setChgId(sellCommonService.getUserCd());

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        ticketManageConfirmOrderSetDto,
        "/partner/sell/setTicketConfirmOrder",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

  /**
   * 발급완료
   */
  public ResponseObject<Object> setTicketComplete(TicketManageCompleteSetDto ticketManageCompleteSetDto) throws Exception {
    ticketManageCompleteSetDto.setChgId(sellCommonService.getUserCd());

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        ticketManageCompleteSetDto,
        "/partner/sell/setTicketComplete",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }
}