package kr.co.homeplus.partner.web.item.controller;

import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.partner.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.partner.web.common.model.common.ResponseResult;
import kr.co.homeplus.partner.web.common.model.pop.ItemPopupListGetDto;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ChannelConstants;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.utility.StringUtil;
import kr.co.homeplus.partner.web.item.model.banner.ItemBannerDetailGetDto;
import kr.co.homeplus.partner.web.item.model.banner.ItemBannerListGetDto;
import kr.co.homeplus.partner.web.item.model.banner.ItemBannerListParamDto;
import kr.co.homeplus.partner.web.item.model.banner.ItemBannerMatchListGetDto;
import kr.co.homeplus.partner.web.item.model.banner.ItemBannerSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/notice")
public class ItemBannerController {

    private final ResourceClient resourceClient;
    private final CodeService codeService;
    private final CertificationService certificationService;

    public ItemBannerController(ResourceClient resourceClient,
                                  CodeService codeService,
                                    CertificationService certificationService
        ) {
        this.resourceClient = resourceClient;
        this.codeService = codeService;
        this.certificationService = certificationService;
    }

    /**
     * 상품관리 > 상품공지관리
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/itemBannerMain", method = RequestMethod.GET)
    public String itemBannereMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                "banner_period_type"   // 검색일자타입
                , "disp_yn"                     // 노출여부
                , "disp_type_seller"            // disp_type
        );

        //코드정보
        model.addAttribute("bannerPeriodType", code.get("banner_period_type"));
        model.addAttribute("dispYn", code.get("disp_yn"));
        model.addAttribute("dispType", code.get("disp_type_seller"));

        model.addAllAttributes(RealGridHelper.create("itemBannerListGridBaseInfo", ItemBannerListGetDto.class));

        return "/item/itemBannerMain";
    }

    @RequestMapping(value = "/itemBannerSetMain", method = RequestMethod.GET)
    public String itemBannereSetMain(Model model
        , @RequestParam(value="bannerNo") Long bannerNo ) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
             "disp_yn"                     // 노출여부
            , "disp_type_seller"             // 배송방법
            , "link_type"                   // 링크타입
        );

        //코드정보
        model.addAttribute("dispYn", code.get("disp_yn"));
        model.addAttribute("dispType", code.get("disp_type_seller"));
        model.addAttribute("priority", code.get("priority"));
        model.addAttribute("bannerNo", bannerNo);
        model.addAttribute("isMod", StringUtil.isEmpty(bannerNo) ? "N" : "Y");

        //popup
        model.addAttribute("isMulti", "N");

        //그리드
        model.addAllAttributes(RealGridHelper.create("bannerDetailListGridBaseInfo", ItemBannerMatchListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("itemPopGridBaseInfo", ItemPopupListGetDto.class));

        return "/item/itemBannerSetMain";
    }

    /**
     * 상품관리> 상품공지 > 리스트 조회
     * @param itemBannerListParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getItemBannerList.json"}, method = RequestMethod.POST)
    public List<ItemBannerListGetDto> getSellerBannerList(@RequestBody ItemBannerListParamDto itemBannerListParamDto) {
        String apiUri = "/po/item/banner/getSellerItemBannerList";
        itemBannerListParamDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemBannerListGetDto>>>() {};

        return (List<ItemBannerListGetDto>) resourceClient.postForResponseObject(
            ResourceRouteName.ITEM, itemBannerListParamDto, apiUri, typeReference).getData();
    }

    /**
     * 상품관리 > 상품공지 > 조회
     * @param bannerNo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getItemBannerDetail.json"}, method = RequestMethod.GET)
    public ItemBannerDetailGetDto getItemBannerDetail( @RequestParam(value="bannerNo") Long bannerNo) {
        String apiUri = "/po/item/banner/getSellerItemBannerDetail";
        String partnerId = certificationService.getLoginUerInfo().getPartnerId();
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ItemBannerDetailGetDto>>() {
        };
        return (ItemBannerDetailGetDto) resourceClient
            .getForResponseObject(ResourceRouteName.ITEM, apiUri + "?bannerNo=" + bannerNo + "&partnerId=" + partnerId,
                typeReference).getData();
    }

    @ResponseBody
    @RequestMapping(value = {"/setItemBanner.json"}, method = RequestMethod.POST)
    public ResponseResult setItemBanner (@RequestBody @Valid ItemBannerSetDto itemBannerSetDto) throws Exception {
        String apiUri = "/po/item/banner/setSellerItemBanner";

        itemBannerSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        itemBannerSetDto.setUserId(ChannelConstants.USERID);

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {};
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM,
            itemBannerSetDto, apiUri, typeReference).getData();
    }

}
