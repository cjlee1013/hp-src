package kr.co.homeplus.partner.web.item.model.itemRelation;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("연관 상품관리 조회 파라미터 Get Entry")
public class ItemRelationListParamDto {

    //연관명
    private String schRelationNm;

    //연관번호
    private String schRelationNo;

    //대분류 카테고리
    private String schLcateCd;

    //증분류 카테고리
    private String schMcateCd;

    //소분류 카테고리
    private String schScateCd;

    //검색일
    private String schDate ="regDt";

    //검색타입
    private String schType;

    //검색 시작일
    private String schStartDate;

    //검색 종류일
    private String schEndDate;

    //판매자ID
    private String schPartnerId;

    //노출여부
    private String schDispYn;

}
