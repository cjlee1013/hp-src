package kr.co.homeplus.partner.web.core.dto;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Setter
@Getter
public class PartnerPasswordSetDto {

    private String partnerId;

    @NotNull(message = "비밀번호를 입력하세요.")
    @Length(min = 10, max = 15, message = "비밀번호는 10에서 15자리 이내로 입력해주세요.")
    private String partnerPw;

    //영업담당자 성명
    private String affiMngNm;

    //휴대폰 번호
    private String affiMobile;

    //제휴담당자 정보
    private String userSubInfo;

    private String userId;
}
