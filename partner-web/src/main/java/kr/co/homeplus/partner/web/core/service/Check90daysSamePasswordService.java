package kr.co.homeplus.partner.web.core.service;

import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 파트너 로그인 시 90일 미만동안 동일 비밀번호를 사용 했는지 체크하는 서비스
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class Check90daysSamePasswordService {
    private final ResourceClient resourceClient;

    /**
     * 90일 미만동안 동일 비밀번호를 사용했는지 체크합니다.<br>
     * 초과 할 경우 비밀번호 변경 레이어 팝업을 노출시킵니다.
     *
     * @param partnerId 파트너 아이디
     * @return 90일 미만동안 동일 비밀번호를 사용한 경우 true, 초과할 경우 false
     */
    public boolean isUnder90DaysSamePassword(final String partnerId) {

        ResponseObject responseObject = resourceClient.getForResponseObject(ResourceRouteName.ITEM,
            "/partner/out/getCheckPartnerLoginDate?partnerId=" + partnerId);

        if (ResponseObject.RETURN_CODE_SUCCESS.equals(responseObject.getReturnCode())) {
            boolean isResult = (boolean) responseObject.getData();
            log.debug("getCheck90DaysSamePassword() isResult : {}", isResult);
            return isResult;
        }
        return true;
    }
}
