package kr.co.homeplus.partner.web.partner.model.sellerShopItem;

import java.util.List;
import lombok.Data;

@Data
public class SellerShopItemSetParamDto {

    private String partnerId;

    private String dispType;

    private List<SellerShopItemListSelectDto> itemList;

    private String regId;

}
