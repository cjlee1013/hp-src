package kr.co.homeplus.partner.web.manage.controller;

import java.util.List;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.manage.model.PartnerFaqDetailSelectDto;
import kr.co.homeplus.partner.web.manage.model.PartnerFaqListSelectDto;
import kr.co.homeplus.partner.web.manage.model.PartnerHarmfulItemDetailSelectDto;
import kr.co.homeplus.partner.web.manage.model.PartnerHarmfulItemListSelectDto;
import kr.co.homeplus.partner.web.manage.model.PartnerNoticeDetailSelectDto;
import kr.co.homeplus.partner.web.manage.model.PartnerNoticeListSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/manage/notice")
public class NoticeController {

    private final ResourceClient resourceClient;
    private final CodeService codeService;
    private final CertificationService certificationService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    public NoticeController(ResourceClient resourceClient,
        CodeService codeService,
        CertificationService certificationService
    ) {
        this.resourceClient = resourceClient;
        this.codeService = codeService;
        this.certificationService = certificationService;
    }
    /**
     * 공지사항 > 공지사항메인
     */
    @RequestMapping(value = "/noticeMain", method = RequestMethod.GET)
    public String noticeMain(Model model) throws Exception {

        codeService.getCodeModel(model,
            "faq_class_partner"                // 파트너 faq 유형
        );

        model.addAllAttributes(
            RealGridHelper.create("noticeListGridBaseInfo", PartnerNoticeListSelectDto.class));
        model.addAllAttributes(
            RealGridHelper.create("harmfulItemListGridBaseInfo", PartnerHarmfulItemListSelectDto.class));

        return "/manage/noticeMain";
    }
    /**
     *공지사항 > 일반/매뉴얼 조회
     * @param
     */
    @ResponseBody
    @RequestMapping(value = {"/getNoticeList.json"}, method = RequestMethod.GET)
    public List<PartnerNoticeListSelectDto> getNoticeList(@RequestParam(name = "noticeKind") String noticeKind) {

        String apiUri = "/po/manage/notice/getPartnerNoticeList?partnerId="+certificationService.getLoginUerInfo().getPartnerId();
        apiUri += "&noticeKind="+noticeKind;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PartnerNoticeListSelectDto>>>() {};
        return (List<PartnerNoticeListSelectDto>) resourceClient.getForResponseObject(
            ResourceRouteName.MANAGE,apiUri, typeReference).getData();
    }
    /**
     *공지사항 > FAQ 조회
     * @param
     */
    @ResponseBody
    @RequestMapping(value = {"/getFaqList.json"}, method = RequestMethod.GET)
    public List<PartnerFaqListSelectDto> getFaqList(@RequestParam(name = "faqClass",required = false) String faqClass) {

        String apiUri = "/po/manage/faq/getPartnerFaqList?faqClass="+faqClass;

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PartnerFaqListSelectDto>>>() {};
        return (List<PartnerFaqListSelectDto>) resourceClient.getForResponseObject(
            ResourceRouteName.MANAGE,apiUri, typeReference).getData();
    }
    /**
     *공지사항 > 위해상품 조회
     * @param
     */
    @ResponseBody
    @RequestMapping(value = {"/getHarmfulItemList.json"}, method = RequestMethod.GET)
    public List<PartnerHarmfulItemListSelectDto> getHarmfulItemList() {

        String apiUri = "/po/item/harmful/getPartnerHarmfulItemList?partnerId="+certificationService.getLoginUerInfo().getPartnerId();

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PartnerHarmfulItemListSelectDto>>>() {};
        return (List<PartnerHarmfulItemListSelectDto>) resourceClient.getForResponseObject(
            ResourceRouteName.ITEM,apiUri, typeReference).getData();
    }
    /**
     *공지사항 > FAQ/공지사항 상세
     * @param
     */
    @RequestMapping(value = "/noticeDetailMain", method = RequestMethod.GET)
    public String noticeDetailMain(Model model
        , @RequestParam(value="dspNo") String dspNo
        , @RequestParam(value="kind") String kind
    ) throws Exception {
        model.addAttribute("dspNo", dspNo);
        model.addAttribute("kind", kind);
        model.addAttribute("hmpImgUrl", hmpImgUrl);
        return "/manage/noticeDetailMain";
    }

    /**
     *공지사항 > 공지사항 상세 조회
     * @param
     */
    @ResponseBody
    @RequestMapping(value = {"/getNoticeDetail.json"}, method = RequestMethod.GET)
    public PartnerNoticeDetailSelectDto getNoticeDetail(
        @RequestParam(value="dspNo") String dspNo
    ) throws Exception {

        String apiUri = "/po/manage/notice/getPartnerNoticeDetail?dspNo="+dspNo;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<PartnerNoticeDetailSelectDto>>() {};
        return (PartnerNoticeDetailSelectDto) resourceClient.getForResponseObject(ResourceRouteName.MANAGE,apiUri, typeReference).getData();
    }
    /**
     *공지사항 > FAQ 상세 조회
     * @param
     */
    @ResponseBody
    @RequestMapping(value = {"/getFaqDetail.json"}, method = RequestMethod.GET)
    public PartnerFaqDetailSelectDto getFaqDetail(
        @RequestParam(value="dspNo") String dspNo
    ) throws Exception {

        String apiUri = "/po/manage/faq/getPartnerFaqDetail?dspNo="+dspNo;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<PartnerFaqDetailSelectDto>>() {};
        return (PartnerFaqDetailSelectDto) resourceClient.getForResponseObject(ResourceRouteName.MANAGE,apiUri, typeReference).getData();
    }
    /**
     *공지사항 >  위해 상품 상세
     * @param
     */
    @RequestMapping(value = "/harmfulDetailMain", method = RequestMethod.GET)
    public String harmfulDetailMain(Model model
        ,@RequestParam(value="noDocument") String noDocument
        ,@RequestParam(value="seq") String seq
        ,@RequestParam(value="cdInsptmachi") String cdInsptmachi
    ) throws Exception {
        model.addAttribute("noDocument", noDocument);
        model.addAttribute("seq", seq);
        model.addAttribute("cdInsptmachi", cdInsptmachi);

        return "/manage/harmfulDetailMain";
    }
    /**
     *공지사항 > 위해상품 상세 조회
     * @param
     */
    @ResponseBody
    @RequestMapping(value = {"/getHarmfulDetail.json"}, method = RequestMethod.GET)
    public PartnerHarmfulItemDetailSelectDto getHarmfulDetail(
         @RequestParam(value="noDocument") String noDocument
        ,@RequestParam(value="seq") String seq
        ,@RequestParam(value="cdInsptmachi") String cdInsptmachi
    ) throws Exception {

        String param ="noDocument="+noDocument
            +"&seq="+ seq
            +"&cdInsptmachi="+ cdInsptmachi;

        String apiUri = "/po/item/harmful/getPartnerHarmfulDetail?"+param;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<PartnerHarmfulItemDetailSelectDto>>() {};
        return (PartnerHarmfulItemDetailSelectDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM,apiUri, typeReference).getData();
    }
}
