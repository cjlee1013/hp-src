package kr.co.homeplus.partner.web.core.dto;

import lombok.Getter;

@Getter
public class LoginInitInfo {
    /**
     * 아이디 저장여부
     */
    private boolean chkSavePartnerId;
    /**
     * 저장된 아이디
     */
    private String savePartnerId;

    public LoginInitInfo(final boolean chkSavePartnerId, final String savePartnerId) {
        this.chkSavePartnerId = chkSavePartnerId;
        this.savePartnerId = savePartnerId;
    }

    public LoginInitInfo() {
        savePartnerId = "";
        chkSavePartnerId = false;
    }
}
