package kr.co.homeplus.partner.web.partner.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.partner.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.partner.web.common.model.common.ResponseResult;
import kr.co.homeplus.partner.web.core.annotation.CertificationNeedLess;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ChannelConstants;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.utility.StringUtil;
import kr.co.homeplus.partner.web.partner.enums.PartnerType;
import kr.co.homeplus.partner.web.partner.model.PartnerApiKeySetDto;
import kr.co.homeplus.partner.web.partner.model.PartnerBankAccntNoCertifyParamGetDto;
import kr.co.homeplus.partner.web.partner.model.PartnerBizCateCdGetDto;
import kr.co.homeplus.partner.web.partner.model.PartnerCheckPwParamDto;
import kr.co.homeplus.partner.web.partner.model.PartnerNoInfoGetDto;
import kr.co.homeplus.partner.web.partner.model.PartnerPickValidCreditGetDto;
import kr.co.homeplus.partner.web.partner.model.PartnerSellerAddressDto;
import kr.co.homeplus.partner.web.partner.model.PartnerSellerDeliveryGetDto;
import kr.co.homeplus.partner.web.partner.model.PartnerSetDto;
import kr.co.homeplus.partner.web.partner.model.commission.CommissionParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/partner")
public class PartnerController {

    final private ResourceClient resourceClient;
    final private CertificationService certificationService;

    public PartnerController(ResourceClient resourceClient,
            CertificationService certificationService) {
        this.resourceClient = resourceClient;
        this.certificationService = certificationService;
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > 종목 조회
     * @param partnerBizCateCdGetDto
     * @return
     */
    @CertificationNeedLess
    @ResponseBody
    @PostMapping(value = "/getBizCateCdList.json")
    public List<MngCodeGetDto> getBizCateCdList(@RequestBody PartnerBizCateCdGetDto partnerBizCateCdGetDto) {

        partnerBizCateCdGetDto.setTypeCode("biz_cate_cd");
        ResponseObject<List<MngCodeGetDto>> responseObject = resourceClient.postForResponseObject(ResourceRouteName.ITEM, partnerBizCateCdGetDto
                , "/common/getCodeListByNm", new ParameterizedTypeReference<>() {});

        List<MngCodeGetDto> responseData = responseObject.getData();
        return responseData;
    }
    /**
     * 파트너관리 > 통신판매업신고번호 조회
     * @param partnerNo
     * @return
     */
    @CertificationNeedLess
    @ResponseBody
    @GetMapping("/getCommuniNotiNo.json")
    public ResponseResult getCommuniNotiNo(@RequestParam String partnerNo) {
        return resourceClient.getForResponseObject(
                ResourceRouteName.ITEM
                , "/partner/getCommuniNotiNo?partnerNo=" + partnerNo
                , new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }

    /**
     * 판매자 주소 조회
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getSellerAddress.json", method = RequestMethod.GET)
    public PartnerSellerAddressDto getSellerAddress() {
        String partnerId = certificationService.getLoginUerInfo().getPartnerId();
        String apiUri = "/partner/getSellerAddress?partnerId=" + partnerId;

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<PartnerSellerAddressDto>>() {};
        return (PartnerSellerAddressDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, typeReference).getData();
    }

    /**
     * 판매자 기준 수수료 조회
     * @param paramDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = "/getBaseCommissionRate.json")
    public BigDecimal getBaseCommissionRate(CommissionParamDto paramDto) {

        String apiUri = "/po/partner/getBaseCommissionRate";
        paramDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<BigDecimal>>() {};
        return (BigDecimal) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil
            .getRequestString(apiUri, CommissionParamDto.class, paramDto), typeReference).getData();
    }

    /**
     * 판매자 수정
     *
     * @param partnerSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/setPartner.json")
    public ResponseResult setPartnerMng(@RequestBody PartnerSetDto partnerSetDto) {
        String partnerId = certificationService.getLoginUerInfo().getPartnerId();

        partnerSetDto.setPartnerId(partnerId);
        partnerSetDto.setUserId(partnerId);
        partnerSetDto.setIsMod("true");

        ResponseObject<ResponseResult> responseObject = resourceClient.postForResponseObject(ResourceRouteName.ITEM, partnerSetDto
                , "/po/partner/setPartner", new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {});

        ResponseResult responseData = responseObject.getData();

        return responseData;
    }

    /**
     * 파트너관리 > 판매자관리 > 택배사 신용코드 유효성 검사
     * @param dlvCd
     * @param creditCd
     * @return
     */
    @ResponseBody
    @GetMapping("/pop/getCompanyCreditValidInfo.json")
    public List<PartnerPickValidCreditGetDto> getCompanyCreditValidInfo(@RequestParam(value = "dlvCd") String dlvCd, @RequestParam(value = "creditCd") String creditCd) {
        List<PartnerSellerDeliveryGetDto> partnerSellerDeliveryGetDtoList = new ArrayList<>();

        PartnerSellerDeliveryGetDto partnerSellerDeliveryGetDto = new PartnerSellerDeliveryGetDto();
        partnerSellerDeliveryGetDto.setDlvCd(dlvCd);
        partnerSellerDeliveryGetDto.setCreditCd(creditCd);
        partnerSellerDeliveryGetDtoList.add(partnerSellerDeliveryGetDto);

        return resourceClient.postForResponseObject(ResourceRouteName.SHIPPING
                ,   partnerSellerDeliveryGetDtoList
                , "/tracking/pick/getInvalidCredit"
                , new ParameterizedTypeReference<ResponseObject<List<PartnerPickValidCreditGetDto>>>() {}).getData();
    }

    @ResponseBody
    @PostMapping("/getCheckOriginPassword.json")
    public Boolean getCheckOriginPassword(@RequestBody PartnerCheckPwParamDto partnerCheckPwParamDto) {
        partnerCheckPwParamDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        partnerCheckPwParamDto.setPartnerType(PartnerType.SELLER.getType());

        ResponseObject<Boolean> responseObject = resourceClient.postForResponseObject(ResourceRouteName.ITEM, partnerCheckPwParamDto
                , "/partner/getCheckPassword", new ParameterizedTypeReference<ResponseObject<Boolean>>() {});

        return responseObject.getData();
    }

    @ResponseBody
    @PostMapping("/getCheckPassword.json")
    public Boolean getCheckPassword(@RequestBody PartnerCheckPwParamDto partnerCheckPwParamDto) {
        return partnerCheckPwParamDto.getPassword().equals(certificationService.getLoginUerInfo().getPartnerId());
    }

    /**
     * 계좌인증 조회
     * @param partnerBankAccntNoCertifyParamGetDto
     * @return
     */
    @CertificationNeedLess
    @ResponseBody
    @RequestMapping(value = {"/getBankAccntNoCertify.json"}, method = {RequestMethod.POST, RequestMethod.GET})
    public PartnerBankAccntNoCertifyParamGetDto getBankAccntNoCertify(
            PartnerBankAccntNoCertifyParamGetDto partnerBankAccntNoCertifyParamGetDto) {
        return resourceClient.postForResponseObject(
                ResourceRouteName.ITEM
                , partnerBankAccntNoCertifyParamGetDto
                , "/partner/getBankAccntNoCertify"
                , new ParameterizedTypeReference<ResponseObject<PartnerBankAccntNoCertifyParamGetDto>>() {
                }).getData();
    }
    /**
     * 사업자번호 인증 조회
     * @param partnerNo
     * @return
     */
    @CertificationNeedLess
    @ResponseBody
    @GetMapping(value = {"/checkPartnerNo.json"})
    public PartnerNoInfoGetDto checkBusinessNo(@RequestParam(value = "partnerNo") String partnerNo) {
        String apiUri = "/partner/checkPartnerNo";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<PartnerNoInfoGetDto>>() {};
        return (PartnerNoInfoGetDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?partnerNo=" + partnerNo + "&partnerType=SELL" + "&channel=" + ChannelConstants.PARTNER , typeReference).getData();
    }

    /**
     * API KEY 발급
     * @param partnerApiKeySetDto
     * @return
     */
    @ResponseBody
    @PostMapping("/setApiKey.json")
    public PartnerApiKeySetDto setApiKey(@RequestBody PartnerApiKeySetDto partnerApiKeySetDto) {
        partnerApiKeySetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        partnerApiKeySetDto.setUserId(certificationService.getLoginUerInfo().getPartnerId());

        ResponseObject<PartnerApiKeySetDto> responseObject = resourceClient.postForResponseObject(ResourceRouteName.ITEM, partnerApiKeySetDto
                , "/inbound/out/setApiKey", new ParameterizedTypeReference<ResponseObject<PartnerApiKeySetDto>>() {});

        return responseObject.getData();
    }

}
