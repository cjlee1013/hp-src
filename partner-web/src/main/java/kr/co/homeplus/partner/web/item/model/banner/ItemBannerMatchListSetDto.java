package kr.co.homeplus.partner.web.item.model.banner;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemBannerMatchListSetDto {

    //배너번호
    private Long bannerNo;

    //상품번호
    @NotNull(message = "상품번호")
    private String itemNo;

    //사용여부
    @Pattern(regexp = "[Y|N]{1}", message="사용여부를 확인해주세요.")
    @NotNull(message = "사용여부를 확인해주세요.")
    private String useYn;

    //등록자
    private String userId;

}
