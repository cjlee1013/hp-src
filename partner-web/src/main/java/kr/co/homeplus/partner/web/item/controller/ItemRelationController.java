package kr.co.homeplus.partner.web.item.controller;

import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.partner.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.partner.web.common.model.common.ResponseResult;
import kr.co.homeplus.partner.web.common.model.pop.ItemPopupListGetDto;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ChannelConstants;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.utility.StringUtil;
import kr.co.homeplus.partner.web.item.model.itemRelation.ItemRelationDelDto;
import kr.co.homeplus.partner.web.item.model.itemRelation.ItemRelationDetailGetDto;
import kr.co.homeplus.partner.web.item.model.itemRelation.ItemRelationListGetDto;
import kr.co.homeplus.partner.web.item.model.itemRelation.ItemRelationListParamDto;
import kr.co.homeplus.partner.web.item.model.itemRelation.ItemRelationSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item")
public class ItemRelationController {

    private final ResourceClient resourceClient;
    private final CodeService codeService;
    private final CertificationService certificationService;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgUrl;

    public ItemRelationController(ResourceClient resourceClient,
        CodeService codeService,
        CertificationService certificationService
    ) {
        this.resourceClient = resourceClient;
        this.codeService = codeService;
        this.certificationService = certificationService;
    }
    /**
     * 상품관리 > 셀러상품관리 > 연관 상품관리
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/itemRelationMain", method = RequestMethod.GET)
    public String itemRelationMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "disp_yn"                     // 노출여부
        );

        model.addAttribute("dispYn", code.get("disp_yn"));

        model.addAllAttributes(RealGridHelper.create("itemRelationListGridBaseInfo", ItemRelationListGetDto.class));
        return "/item/itemRelationMain";
    }

    @RequestMapping(value = "/itemRelationSetMain", method = RequestMethod.GET)
    public String itemRelationSetMain (Model model
        , @RequestParam(value="relationNo") Long relationNo ) throws Exception {

        codeService.getCodeModel(model,
            "disp_yn"              // 노출여부
            ,   "po_disp_pc_type"          //PC노출기준
            ,   "po_disp_mobile_type"      //모바일노출기준
        );

        model.addAttribute("relationNo", relationNo);
        model.addAttribute("isMod", StringUtil.isEmpty(relationNo) ? "N" : "Y");
        model.addAttribute("hmpImgUrl", this.hmpImgUrl);

        //popup
        model.addAttribute("isMulti", "Y");

        model.addAllAttributes(RealGridHelper.create("itemRelationDetailListGridBaseInfo", ItemRelationDetailGetDto.class));
        model.addAllAttributes(RealGridHelper.create("itemPopGridBaseInfo", ItemPopupListGetDto.class));

        return "/item/itemRelationSetMain";
    }

    /**
     * 연관상품 리스트 조회
     * @param itemRelationListParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getItemRelationList.json"}, method = RequestMethod.POST)
    public List<ItemRelationListGetDto> getItemRelationList(@RequestBody ItemRelationListParamDto itemRelationListParamDto) {

        String apiUri = "/po/item/getItemRelationList";

        itemRelationListParamDto.setSchPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        itemRelationListParamDto.setSchDate("regDt");
        itemRelationListParamDto.setSchType("relationNm");

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemRelationListGetDto>>>() {};

        return (List<ItemRelationListGetDto>) resourceClient.postForResponseObject(
            ResourceRouteName.ITEM, itemRelationListParamDto, apiUri, typeReference).getData();
    }

    /**
     * 연관상품 조회
     * @param relationNo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getItemRelation.json"}, method = RequestMethod.GET)
    public ItemRelationListGetDto getItemRelationList (@RequestParam(name = "relationNo") Long relationNo) {
        String apiUri = "/po/item/getItemRelation";
        String partnerId = certificationService.getLoginUerInfo().getPartnerId();

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ItemRelationListGetDto>>() {};
        return (ItemRelationListGetDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?relationNo=" + relationNo + "&partnerId=" + partnerId , typeReference).getData();
    }

    /**
     * 상품관리 > 연관상품상세 리스트 조회
     * @param relationNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getItemRelationDetailList.json"})
    public List<ItemRelationDetailGetDto> getItemRelationDetailList(@RequestParam(name = "relationNo") Long relationNo) {
        String apiUri = "/po/item/getItemRelationDetailList";
        String partnerId = certificationService.getLoginUerInfo().getPartnerId();

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemRelationDetailGetDto>>>() {};
        return (List<ItemRelationDetailGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?relationNo=" + relationNo + "&partnerId=" + partnerId , typeReference).getData();
    }

    /**
     * 상품관리 > 연관상품상세 조회
     * @param itemNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getItemRelationDetail.json"})
    public ItemRelationDetailGetDto getItemRelationDetail(@RequestParam(name = "itemNo") String itemNo) {
        String apiUri = "/po/item/getItemRelationDetail";
        String partnerId = certificationService.getLoginUerInfo().getPartnerId();

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ItemRelationDetailGetDto>>() {};
        return (ItemRelationDetailGetDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?itemNo=" + itemNo + "&partnerId=" + partnerId , typeReference).getData();
    }

    /**
     * 상품관리 > 연관
     * @param itemRelationSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setItemRelation.json"}, method = RequestMethod.POST)
    public ResponseResult setItemRelation (@RequestBody @Valid ItemRelationSetDto itemRelationSetDto) throws Exception {
        String apiUri = "/po/item/setItemRelation";

        itemRelationSetDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        itemRelationSetDto.setUserId(ChannelConstants.USERID);

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {};
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemRelationSetDto, apiUri, typeReference).getData();

    }

    @ResponseBody
    @RequestMapping(value = {"/delItemRelation.json"}, method = RequestMethod.POST)
    public ResponseResult delItemRelation(@RequestBody @Valid ItemRelationDelDto itemRelationDelDto) {

        itemRelationDelDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        itemRelationDelDto.setUserId(ChannelConstants.USERID);

        String apiUri = "/po/item/delItemRelation";
        ResponseObject<ResponseResult> responseObject = resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemRelationDelDto, apiUri,
            new ParameterizedTypeReference<>() {
            });

        return responseObject.getData();
    }
}
