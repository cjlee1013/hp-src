package kr.co.homeplus.partner.web.sell.model.rentalManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 렌탈판매관리 상담완료 요청 파라미터")
public class RentalManageCounselCompleteSetDto {
    @ApiModelProperty(notes = "배송번호 리스트")
    private List<String> shipNoList;

    @ApiModelProperty(notes = "수정자")
    private String chgId;
}