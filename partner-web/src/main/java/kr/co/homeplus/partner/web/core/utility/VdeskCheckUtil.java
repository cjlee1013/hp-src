package kr.co.homeplus.partner.web.core.utility;


import lombok.extern.slf4j.Slf4j;
//import org.springframework.security.web.util.matcher.IpAddressMatcher;

@Slf4j
public class VdeskCheckUtil {
  private static final String ALLOW_VDI_IP       	 = "10.200.208.0/255";

  /** 업무용 vdi Ip 대역대 matcher **/
//  public static IpAddressMatcher vdiAddressMatcher = new IpAddressMatcher(ALLOW_VDI_IP);

  /**
   * 망분리 된 메뉴의 경우 접속자의 아이피를 확인하여 허용 된 대역대일 경우 true, 아니면 false를 리턴합니다.
   *
   * @param clientIp 접속자 아이피
   * @return 허용 된 대역대일 경우 true, 아니면 false
   */
  public static boolean isAllowVdiIp(String clientIp) {
    log.info("Access Client IP address : <{}>", clientIp);
//    return vdiAddressMatcher.matches(clientIp);
    return true;
  }

}
