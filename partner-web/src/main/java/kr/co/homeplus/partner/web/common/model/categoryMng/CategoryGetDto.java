package kr.co.homeplus.partner.web.common.model.categoryMng;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryGetDto {

	private String lcateCd;
	private String mcateCd;
	private String scateCd;
	private String dcateCd;
	private String lcateNm;
	private String mcateNm;
	private String scateNm;
	private String dcateNm;
	private String cateCd;
	private String cateName;
	private String parentCateCd;
	private String depth;
	private String priority;
	private String cartYn;
	private String cartYnNm;
	private String adultLimitYn;
	private String adultLimitYnNm;
	private String dispYn;
	private String dispYnNm;
	private String useYn;
	private String useYnNm;
	private String isbnYn;
	private String reviewDisplay;

	private String cateNm;
	private String ldispStartDt;
	private String ldispEndDt;
	private String mdispStartDt;
	private String mdispEndDt;

	// 레거시 1depth
	private String division;

	// 레거시 2depth
	private String groupNo;

	// 레거시 3depth
	private String dept;

	// 레거시 4depth
	private String classCd;

	// 레거시 5depth
	private String subclass;
}
