package kr.co.homeplus.partner.web.core.constants;

/**
 * 파트너 내 상수를 저장하는 클래스
 */
public class PartnerConstants {

    public static final String USER_TOKEN = "userInfo";

    /** 파일 업로드시에 사용하는 uri */
    public static final String FILE_UPLOAD_URI = "/File/Receiver";

    /** 파일 업로드시에 사용하는 파일의 key name */
    public static final String FILE_UPLOAD_FIELD_NAME = "file";

    /** 파일 업로드시에 사용하는 server key name */
    public static final String FILE_UPLOAD_KEY = "file_key";

    /** 파일 업로드 시 사용할 url 필드 명. */
    public static final String FILE_UPLOAD_FILE_URL = "file_url";

    /**
     * 권한 api에서 사용자 접근 가능한 메뉴 조회 시 사용할 URI
     */
    public static final String AUTH_PERMIT_MENU_URI = "/menus";

    /** 파트너 로그인 세션 쿠키명 **/
    public static final String PARTNER_SESSION_COOKIE_NAME = "PartnerPlus";
    /** 파트너  아이디 저장 쿠키 명 **/
    public static final String PARTNER_LOGIN_ID_SAVE_COOKIE_NAME = "PartnerPlusExtra";
    /** 파트너 아이디 저장 쿠키 만료시간 **/
    public static final int PARTNER_LOGIN_ID_SAVE_COOKIE_MAX_AGE = -1;


    /**
     * 파트너 로그인처리 및 로그인정보조회
     */
    public static final String PARTNER_LOGIN_URI = "/web/partner/login/setResult";
    public static final String PARTNER_LOGIN_STEP1_URI = "/web/partner/login/step1";
    public static final String PARTNER_LOGIN_STEP2_URI = "/web/partner/login/step2";
    public static final String PARTNER_LOGOUT_URI = "/web/partner/logout";
    public static final String PARTNER_LOGIN_INFO_URI = "/web/partner/login/getLoginInfo";
    /** 파트너 로그인시 발송된 휴대폰 인증번호 검증요청**/
    public static final String PARTNER_LOGIN_VERIFY_MOBILE_CERT_NO_URI = "/web/partner/verify/mobile/certNo";
    /** 파트너 로그인 인증 성공시 일시 업데이트 **/
    public static final String UPDATE_PARTNER_LOGIN_DATE_URI = "/partner/out/setPartnerLoginDate?partnerId=";
}