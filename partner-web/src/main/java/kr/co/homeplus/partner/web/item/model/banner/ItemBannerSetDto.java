package kr.co.homeplus.partner.web.item.model.banner;


import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import kr.co.homeplus.partner.web.core.constants.PatternConstants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemBannerSetDto {

    //상품상세공지번호
    private Long bannerNo;

    //공지타입(HMP:홈플러스/SELLER:판매자별
    private String bannerType;

    //구분(ALL:전체,CATE:카테고리별,ITEM:상품별,SELLER:판매자별
    @Pattern(regexp = "(ALL|CATE|ITEM)", message="구매제한 타입을 확인해주세요.")
    private String dispType;

    //공지제목
    @NotEmpty (message = "공지제목을 입력해주세요.")
    @Size(max = 50, message = "공지제목은 50자 이하로 등록해주세요.")
    private String bannerNm;

    //세카테고리 선택 값
    private Integer selectCateCd4;

    //점포유형
    private String storeType;

    //판매자 ID
    private String partnerId;

    //공지내용
    private String bannerDesc;

    //노출여부
    @NotNull(message = "노출여부를 확인해주세요")
    @Pattern(regexp = "[Y|N]{1}", message="노출여부를 확인해주세요")
    private String dispYn;

    //노출시작일시
    @NotNull(message = "노출시작일시")
    @Pattern(regexp = PatternConstants.DATE_TIME_PATTERN_BASIC, message = "노출시작일시 날짜 형식을 확인해주세요 : yyyy-MM-dd HH:00:00")
    private String dispStartDt;

    //노출종료일시
    @NotNull(message = "노출종료일시")
    @Pattern(regexp = PatternConstants.DATE_END_TIME_PATTERN, message = "노출종료일시 날짜 형식을 확인해주세요 : yyyy-MM-dd HH:59:59")
    private String dispEndDt;

    //등록/수정자
    private String userId;

    //수정여부
    private String isMod;

    /**
     * 상품별 > 등록상품
     */
    @Valid
    List<ItemBannerMatchListSetDto> itemList;

}
