package kr.co.homeplus.partner.web.core.view.excel;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.partner.web.core.utility.DhtmlxGridHeader;
import kr.co.homeplus.partner.web.core.utility.realgrid.RealGridColumn;
import org.apache.poi.ss.usermodel.CellType;
import org.springframework.util.ReflectionUtils;

/**
 * 엑셀에 바인딩할 정보를 가지고 있는 클래스.
 * 엑셀 다운로드 뷰에서 사용됨. (ExcelDataXlsxStreamingDownloadView.class 등)
 */
public class ExcelBindInfo {
    /**
     * 다운로드할 파일 명
     */
    private String downloadExcelFileName = "NoFileName";

    /**
     * 엑셀 시트 명
     */
    private String sheetName = "NoSheetName";

    /**
     * 셀 헤더 정보 (엑셀 데이터 맨 상단에 상단에 출력됨)
     */
    private List<String> cellHeaders = new ArrayList<>();

    /**
     * 셀 데이터 목록
     */
    private List<List<String>> rows = new ArrayList<>();

    /**
     * 컬럼 너비 목록
     */
    private Map<Integer, Integer> columnWidths = new HashMap<>();

    /**
     * 셀 타입 목록
     */
    private Map<Integer, CellType> cellTypes = new HashMap<>();

    /**
     * dhtmlx 그리드의 적용될 셀 필드 목록
     */
    private List<Field> fieldList = new ArrayList<>();

    public void setDownloadExcelFileName(String downloadExcelFileName) {
        this.downloadExcelFileName = downloadExcelFileName;
    }

    public String getDownloadExcelFileName() {
        return downloadExcelFileName;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public List<String> getCellHeaders() {
        return cellHeaders;
    }

    public void setCellHeaders(String... title) {
        cellHeaders = Arrays.asList(title);
    }

    public List<List<String>> getRows() {
        return rows;
    }

    public void addRows(String... row) { rows.add(Arrays.asList(row)); }

    public Map<Integer, Integer> getColumnWidths() { return columnWidths; }

    public Map<Integer, CellType> getCellTypes() { return cellTypes; }

    /**
     * dhtmlx 그리드 헤더의 정보를 참조하여 히든여부에 따라 fieldList 적용
     */
    public void setCellGridHeaders(List<DhtmlxGridHeader> gridHeader, Class dto) throws Exception {
        List<String> headerList = new ArrayList();
        for (DhtmlxGridHeader grid : gridHeader) {
            if(grid.isHidden() == false && !grid.getType().equals("ch")) {
                headerList.add(grid.getValue());
                Field f = dto.getDeclaredField(grid.getColName());
                ReflectionUtils.makeAccessible(f);
                fieldList.add(f);
            }
        }
        cellHeaders = headerList;
    }

    /**
     * dhtmlx 그리드 리턴 객체를 참조하여 fieldList에 적용된 필드만 추출하여 row 생성
     */
    public void addGridRows(List<Object> responseValue) throws Exception {
        for (Object val : responseValue) {
            List<String> excelRow = new ArrayList();
            for (Field field : fieldList) {
                excelRow.add(String.valueOf(field.get(val)));
            }
            rows.add(excelRow);
        }
    }

    /**
     * 컬럼 너비, 셀 타입 추가 : NUMERIC
     */
    public void addNumColumn(Integer idx, Integer width) {
        columnWidths.put(idx, width);
        cellTypes.put(idx, CellType.NUMERIC);
    }

    /**
     * 컬럼 너비, 셀 타입 추가 : STRING
     */
    public void addStrColumn(Integer idx, Integer width) {
        columnWidths.put(idx, width);
        cellTypes.put(idx, CellType.STRING);
    }

    /**
     * 컬럼 너비, 셀 타입 : 일반
     */
    public void addNormalColumn(Integer idx, Integer width) {
        columnWidths.put(idx, width);
    }

    /**
     * real 그리드 헤더의 정보를 참조하여 히든여부에 따라 fieldList 적용 : partner-web에서 리얼그리드 사용 시 이 메소드를 적용해야 함
     */
    public void setCellRealGridHeaders(List<RealGridColumn> gridHeader, Class dto) throws Exception {
        List<String> headerList = new ArrayList();
        for (RealGridColumn grid : gridHeader) {
            if(grid.isVisible() == true) {
                headerList.add(grid.getHeaderText());
                Field f = dto.getDeclaredField(grid.getFieldName());
                ReflectionUtils.makeAccessible(f);
                fieldList.add(f);
            }
        }
        cellHeaders = headerList;
    }

}
