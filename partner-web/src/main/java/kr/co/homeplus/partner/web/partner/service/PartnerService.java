package kr.co.homeplus.partner.web.partner.service;

import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.partner.model.PartnerSellerGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

@Service
public class PartnerService {

	final private ResourceClient resourceClient;

	public PartnerService(ResourceClient resourceClient) {
		this.resourceClient = resourceClient;
	}

	/**
	 * 업체관리 > 파트너관리 > 판매업체관리 > 판매업체 상세조회
	 *
	 * @param partnerId
	 * @return
	 */
	public PartnerSellerGetDto getSeller(String partnerId) {

		String apiUri = "/partner/getSeller";

		return resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?partnerId=" + partnerId,
						new ParameterizedTypeReference<ResponseObject<PartnerSellerGetDto>>() {
						}).getData();
	}
}
