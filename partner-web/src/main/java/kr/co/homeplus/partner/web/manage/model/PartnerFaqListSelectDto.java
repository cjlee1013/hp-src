package kr.co.homeplus.partner.web.manage.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PartnerFaqListSelectDto {

    private String dspNo;

    private String kind;

    private String kindTxt;

    private String title;

    private String topYn;

    private String newYn;

    private String chgDt;

}
