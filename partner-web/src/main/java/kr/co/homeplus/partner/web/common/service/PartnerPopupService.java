package kr.co.homeplus.partner.web.common.service;

import java.util.List;
import kr.co.homeplus.partner.web.common.model.home.PartnerPopupDto;

public interface PartnerPopupService {

    List<PartnerPopupDto> getPartnerPopupList();
}
