package kr.co.homeplus.partner.web.sell.model.ticketManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 이티켓판매관리 발급완료 요청 파라미터")
public class TicketManageCompleteSetDto {
    @ApiModelProperty(notes = "배송SEQ 리스트")
    private List<TicketManageCompleteArgDto> shipNoList;

    @ApiModelProperty(notes = "수정자")
    private String chgId;
}