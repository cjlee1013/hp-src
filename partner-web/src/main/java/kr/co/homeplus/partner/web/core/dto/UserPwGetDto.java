package kr.co.homeplus.partner.web.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserPwGetDto {

    //사용자 아이디
    private String partnerId;

    //상호명
    private String partnerNm;

    //제휴업체 이메일
    private String mngEmail;

    //제휴업체 휴대폰
    private String mngMobile;
}
