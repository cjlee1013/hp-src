package kr.co.homeplus.partner.web.item.model.banner;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemBannerListParamDto {

    //검색일 타입(등록일/노출일)
    private String schDateType;

    //검색시작일
    private String schStartDt;

    //검색종료일
    private String schEndDt;

    //구분
    private String schDispType;

    //노출여부
    private String schDispYn;

    //대카테고리
    private String schCateCd1;

    //중카테고리
    private String schCateCd2;

    //소카테고리
    private String schCateCd3;

    //세카테고리
    private String schCateCd4;

    //배너제목
    private String schBannerNm;

    // 판매자 아이디
    private String partnerId;

}
