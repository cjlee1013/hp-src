package kr.co.homeplus.partner.web.notice.model.channelNotice;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChannelNoticeAdminSearchDto {
    private String kind = "GENERAL";
    private String startDate;
    private String endDate;
    private String search;
    private String keyword;
}
