package kr.co.homeplus.partner.web.item.model.brand;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BrandListSelectDto {

    private Long brandNo;
    private String brandNm;
    private String brandNmEng;
    private String imgUrl;
    private String brandDesc;
    private String useYn;
    private String useYnTxt;
    private String itemDispYn;
    private String siteUrl;
    private String dispNm;
    private String initial;
    private String regNm;
    private String regDt;
    private String chgNm;
    private String chgDt;
}
