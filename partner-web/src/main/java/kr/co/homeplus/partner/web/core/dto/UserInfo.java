package kr.co.homeplus.partner.web.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserInfo {

    private String partnerId;

    private String businessNm;

    // 사용자 사번
    private String userCd;

    // 사용자 아이디
    private String userId;

    // 사용자 이름
    private String userName;

    // 사용자 메일
    private String userMail;

    // 사용자 부서코드
    private String userDepartmentCd;

    // 사용자 부서이름
    private String userDepartmentName;

    // 사용자 직급
    private String position;

    // 사용자 직책
    private String onesDuty;

    // 로그인 요청한 시스템 코드
    private String systemScope;

    // 모바일 번호
    private String userMobile;

    // 직무명
    private String job;

    // 직책코드
    private String onesDutyCd;

    // 직급코드
    private String positionCd;

    // 직무코드
    private String jobCd;

    // 팀장여부
    private String isBoss;

    // 겸직대상여부
    private String hasConcurrent;

    // 겸직레코드 여부
    private String isConcurrent;
}
