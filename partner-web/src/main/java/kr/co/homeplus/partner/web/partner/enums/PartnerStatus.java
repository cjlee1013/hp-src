package kr.co.homeplus.partner.web.partner.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 파트너 상태
 */
@RequiredArgsConstructor
public enum PartnerStatus {

		NORMAL("NORMAL", "정상")
	,   PAUSE("PAUSE", "일시정지")
	,   STOP("STOP", "잠금")
	,   WITHDRAW("WITHDRAW", "탈퇴")
	;

	@Getter
	private final String status;

	@Getter
	private final String description;
}
