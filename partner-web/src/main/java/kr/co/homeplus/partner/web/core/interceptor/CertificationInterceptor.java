package kr.co.homeplus.partner.web.core.interceptor;

import static org.springframework.util.ObjectUtils.isEmpty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.co.homeplus.partner.web.core.annotation.CertificationNeedLess;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.config.PartnerProperties;
import kr.co.homeplus.partner.web.core.dto.UserInfo;
import kr.co.homeplus.partner.web.core.exception.SessionNotFoundException;
import kr.co.homeplus.partner.web.core.exception.handler.ExceptionCode;
import kr.co.homeplus.partner.web.core.service.impl.MaintenanceException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


/**
 * 인증 확인을 위한 인터셉터
 */
@Slf4j
@RequiredArgsConstructor
public class CertificationInterceptor extends HandlerInterceptorAdapter {

    private final CertificationService certificationService;

    private final PartnerProperties partnerProperties;

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response,
        final Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return super.preHandle(request, response, handler);
        }
        // 점검중 페이지 노출여부
        isMaintenancePageDisplay(request.getRequestURI());

        // 로그인정보 조회
        final UserInfo userInfo = certificationService.getLoginUerInfo();

        // CertificationNeedLess annotation 이 클래스나 메서드에 붙어 있을 경우 로그인이 필요없음.
        if (isCertificationNeedLess((HandlerMethod) handler)) {
            // 로그인이 된 상태에서 로그인 페이지로 갔을 경우 home 으로 리다이렉트 처리.
            if (isAccessLoginPage(request.getRequestURI(), userInfo)) {
                log.debug("access login page!, sendRedirect start page.");
                response.sendRedirect(partnerProperties.getStartUrl());
            }
            return super.preHandle(request, response, handler);
        }

        //인증여부 체크
        if (isNotLogin(userInfo)) {
            log.warn("session Not Found Exception! Your session has timed out. <uri:{}>",
                request.getRequestURI());
            throw new SessionNotFoundException(ExceptionCode.SYS_ERROR_CODE_9201.getDesc());
        }

        // view 에서 사용할 정보 세팅.
        setUserInfoToRequest(request, userInfo);
        return super.preHandle(request, response, handler);
    }

    /**
     * 로그인 상태에서 로그인 페이지 접근여부를 체크합니다.
     * @param requestUri request URI
     * @param userInfo UserInfo
     * @return 로그인 상태에서 로그인 페이지 접근할 경우 true, 아닐 경우 false를 리턴합니다.
     */
    private boolean isAccessLoginPage(final String requestUri, final UserInfo userInfo) {
        return isLogin(userInfo) && partnerProperties.getCertificationRedirectUrl()
            .equals(requestUri);
    }

    /**
     * 로그인 상태일 경우를 체크합니다.
     * @param userInfo UserInfo
     * @return 로그인 상태인 경우 true를 반환, 로그인 상태일 경우 false를 반환합니다.
     */
    private boolean isLogin(final UserInfo userInfo) {
        return !isNotLogin(userInfo);
    }

    /**
     * 로그인 상태가 아닐 경우를 체크합니다.
     * @param userInfo UserInfo
     * @return 로그인 상태가 아닐 경우 true를 반환, 로그인 상태일 경우 false를 반환합니다.
     */
    private boolean isNotLogin(final UserInfo userInfo) {
        return isEmpty(userInfo);
    }

    /**
     * <p>
     * {@link CertificationNeedLess} annotation 태깅 여부를 체크합니다.
     * </p>
     * 해당 어노테이션이 클래스나 메서드에 붙어 있을 경우 로그인 인증을 체크하지 않습니다.
     *
     * @param handlerMethod HandlerMethod
     * @return boolean
     */
    private boolean isCertificationNeedLess(final HandlerMethod handlerMethod) {
        return handlerMethod.getBeanType().isAnnotationPresent(CertificationNeedLess.class)
            || handlerMethod.getMethod().isAnnotationPresent(CertificationNeedLess.class);
    }

    /**
     * 점검중 페이지 노출여부
     *
     * @param requestUri request URI
     * @exception MaintenanceException
     */
    private void isMaintenancePageDisplay(final String requestUri) {
        if (partnerProperties.isMaintenancePageDisplayEnable()) {
            //lbStatusCheck url을 제외한 나머지 URI 접근시 점검중 페이지로 노출합니다.
            AntPathMatcher antPathMatcher = new AntPathMatcher();
            if (!antPathMatcher.match("/lbStatusCheck", requestUri)) {
                throw new MaintenanceException();
            }
        }
    }

    /**
     * 공통으로 사용할 정보를 request 에 추가.
     *
     * @param request HttpServletRequest
     * @throws Exception
     */
    private void setUserInfoToRequest(final HttpServletRequest request, final UserInfo userInfo) {
        // 유저 기본 정보.
        if (isLogin(userInfo)) {
            request.setAttribute("userInfo", userInfo);
        }
        //로그인 만료시간 정보
        request.setAttribute("loginExpirationMin", partnerProperties.getLoginExpirationMin());
    }
}