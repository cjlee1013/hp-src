package kr.co.homeplus.partner.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimApproveListSetDto {

    @ApiModelProperty(notes = "취소 승인 요청 dto")
    private List<ClaimApproveSetDto> claimRequestSetList;

}
