package kr.co.homeplus.partner.web.common.model.home;

import lombok.Data;

@Data
public class PartnerPopupDto {

    private int popupNo;
    private String popupNm;
    private String dispStartDt;
    private String dispEndDt;
    private String clostBtnType;
    private int closeBtnPeriod;
    private String imgNm;
    private String imgUrl;
    private int imgWidth;
    private int imgHeight;
    private String linkType;
    private String linkInfo;

}
