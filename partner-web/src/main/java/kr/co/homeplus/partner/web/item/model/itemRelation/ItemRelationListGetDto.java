package kr.co.homeplus.partner.web.item.model.itemRelation;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class ItemRelationListGetDto {

    @ApiModelProperty(value="연관번호")
    @RealGridColumnInfo(headText = "연관번호", width = 100 , sortable = true)
    private Long relationNo;

    @ApiModelProperty(value="연관명")
    @RealGridColumnInfo(headText = "연관명", width = 170 , sortable = true)
    private String relationNm;

    @ApiModelProperty(value="상품개수")
    @RealGridColumnInfo(headText = "상품개수", width = 100 , sortable = true)
    private String itemCount;

    @ApiModelProperty(value="노출여부" )
    @RealGridColumnInfo(headText = "노출여부" , hidden = true)
    private String dispYn;

    @ApiModelProperty(value="노출여부명")
    @RealGridColumnInfo(headText = "노출여부", width = 100 , sortable = true)
    private String dispYnNm;

    @ApiModelProperty(value = "대분류 카테고리명")
    @RealGridColumnInfo(headText = "대분류", width = 150 , sortable = true)
    private String lcateNm;

    @ApiModelProperty(value = "중분류 카테고리명")
    @RealGridColumnInfo(headText = "중분류", width = 150 , sortable = true)
    private String mcateNm;

    @ApiModelProperty(value = "소분류 카테고리명")
    @RealGridColumnInfo(headText = "소분류", width = 150 , sortable = true)
    private String scateNm;

    @ApiModelProperty(value="소분류 카테고리")
    @RealGridColumnInfo(headText = "소분류" , hidden = true)
    private String scateCd;

    @ApiModelProperty(value="PC노출기준")
    @RealGridColumnInfo(headText = "PC노출기준" , hidden = true)
    private String dispPcType;

    @ApiModelProperty(value="모바일노출기준")
    @RealGridColumnInfo(headText = "모바일노출기준" , hidden = true)
    private String dispMobileType;

    @ApiModelProperty(value = "인트로이미지URL")
    private String imgUrl;

    @ApiModelProperty(value = "인트로이미지명")
    private String imgNm;

    @ApiModelProperty(value = "등록일")
    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170, sortable = true)
    private String regDt;

    @ApiModelProperty(value = "등록자")
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regNm;

    @ApiModelProperty(value = "수정일")
    @RealGridColumnInfo(headText = "수정일" , columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170, sortable = true)
    private String chgDt;

    @ApiModelProperty(value = "수정자")
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgNm;

}
