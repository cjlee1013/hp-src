package kr.co.homeplus.partner.web.harm.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * 위해상품 검색 param
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HarmProdSearchParam {

    private String startDate;
    private String endDate;
    private String statusResultCode;
    private String searchType;
    private String searchValue;
    // 홈 공지사항 - 위해상품공지 검색어
    private String schType;
    private String schValue;
}
