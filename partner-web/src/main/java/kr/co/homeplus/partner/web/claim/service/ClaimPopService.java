package kr.co.homeplus.partner.web.claim.service;


import kr.co.homeplus.partner.web.claim.model.ClaimApproveListSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimApproveSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimDeliverySetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimExchShippingModifyDto;
import kr.co.homeplus.partner.web.claim.model.ClaimImageInfoDto;
import kr.co.homeplus.partner.web.claim.model.ClaimPendingSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimPickShippingModifyDto;
import kr.co.homeplus.partner.web.claim.model.ClaimRejectSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimRequestSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimShippingSetDto;
import kr.co.homeplus.partner.web.claim.model.ClaimShippingStatusSetDto;
import kr.co.homeplus.partner.web.claim.model.ExchCompleteSetDto;
import kr.co.homeplus.partner.web.claim.model.ExchShippingSetDto;
import kr.co.homeplus.partner.web.claim.model.PickCompleteSetDto;
import kr.co.homeplus.partner.web.claim.model.PickRequestSetDto;
import kr.co.homeplus.partner.web.claim.model.PickShippingSetDto;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.sell.service.OrderShipManageService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClaimPopService {

    private final ResourceClient resourceClient;
    private final CertificationService certificationService;
    private final OrderShipManageService orderShipManageService;


    /**
     * 클레임 취소 승인 요청
     * parameter가 List 형이라 별도 method로 개발
     * **/
    public ResponseObject<String> requestClaimApprove(ClaimApproveListSetDto claimApproveListSetDto){
        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG,
            claimApproveListSetDto, "/po/claim/requestClaimApprove",
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 클레임 상태변경 요청
     * **/
    public ResponseObject<String> setClaimPending(ClaimPendingSetDto claimPendingSetDto) throws Exception {
        ClaimRequestSetDto claimRequestSetDto
            = ClaimRequestSetDto.builder()
                .claimNo(claimPendingSetDto.getClaimNo())
                .claimBundleNo(claimPendingSetDto.getClaimBundleNo())
                .reasonType(claimPendingSetDto.getReasonType())
                .reasonTypeDetail(claimPendingSetDto.getReasonTypeDetail())
                .claimReqType("CH")
                .regId("PARTNER")
                .claimType(claimPendingSetDto.getClaimType())
                .build();

        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimRequestSetDto, "/claim/reqChange", new ParameterizedTypeReference<ResponseObject<String>>() {});
    }

    /**
     * 클레임 상태변경 요청
     * **/
    public ResponseObject<String> setClaimApprove(ClaimApproveSetDto claimApproveSetDto) throws Exception {
        ClaimRequestSetDto claimRequestSetDto
            = ClaimRequestSetDto.builder()
                .claimNo(claimApproveSetDto.getClaimNo())
                .claimBundleNo(claimApproveSetDto.getClaimBundleNo())
                .claimType(claimApproveSetDto.getClaimType())
                .claimReqType("CA")
                .regId("PARTNER")
                .build();
        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimRequestSetDto, "/claim/reqChange", new ParameterizedTypeReference<ResponseObject<String>>() {});
    }

    /**
     * 클레임 거절 상태변경 요청
     * **/
    public ResponseObject<String> setClaimReject(ClaimRejectSetDto claimRejectSetDto) throws Exception {

        ResponseObject<String> result = new ResponseObject<>();

        /* 1.클레임 상태변경 요청 */
        ClaimRequestSetDto claimRequestSetDto
            = ClaimRequestSetDto.builder()
                .claimNo(claimRejectSetDto.getClaimNo())
                .claimBundleNo(claimRejectSetDto.getClaimBundleNo())
                .reasonType(claimRejectSetDto.getReasonType())
                .reasonTypeDetail(claimRejectSetDto.getReasonTypeDetail())
                .claimReqType("CD")
                .regId("PARTNER")
                .claimType(claimRejectSetDto.getClaimType())
                .build();
        log.info("[setClaimReject] PO 취소 거부 요청 : {}", claimRequestSetDto);
        ResponseObject<Object> claimStatusResult = resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimRequestSetDto, "/claim/reqChange", new ParameterizedTypeReference<ResponseObject<Object>>() {});

        if(!claimStatusResult.getReturnCode().equalsIgnoreCase("0000")){
            log.info("[setClaimReject] PO 취소 거부 실패: {}", claimStatusResult.getReturnMessage());
            result.setReturnCode(claimStatusResult.getReturnCode());
            result.setReturnMessage(claimStatusResult.getReturnMessage());
            return result;
        }

        /* 1-1. 반품인경우 발송요청 없이 return */
        if(claimRejectSetDto.getClaimType().equalsIgnoreCase("R") || claimRejectSetDto.getClaimType().equalsIgnoreCase("X")){
            result.setReturnCode("0000");
            result.setReturnMessage("반품거부 성공");
            return result;
        }

        /* 2.발송요청
        *  claimReqType = CD / 상태 변경 타입 = 거부
        *  shipMethod != N / 배송방법 != 배송없음
        * */
        if(!claimRejectSetDto.getOrderShipManageShipAllSetDto().getShipMethod().equalsIgnoreCase("N")){
            claimRejectSetDto.getOrderShipManageShipAllSetDto().setChgId(certificationService.getLoginUerInfo().getPartnerId());
            log.info("[setClaimReject] PO 취소 거부 - 클레임 배송 요청: {}", claimRejectSetDto.getOrderShipManageShipAllSetDto());
            ResponseObject<Object> setShipResult = resourceClient.postForResponseObject(ResourceRouteName.SHIPPING, claimRejectSetDto.getOrderShipManageShipAllSetDto(), "/partner/sell/setShipAllExClaim", new ParameterizedTypeReference<ResponseObject<Object>>() {});

            if(!setShipResult.getReturnCode().equalsIgnoreCase("SUCCESS")){
                log.info("[setClaimReject] PO 취소 거부 - 배송 요청 실패: {}", setShipResult.getReturnMessage());
                result.setReturnCode(setShipResult.getReturnCode());
                result.setReturnMessage("클레임 거부상태 변경성공\n 배송요청 실패 : " + setShipResult.getReturnMessage() + "\n 관리자에게 문의 바랍니다.");
                return result;
            }
        }

        /* return */
        result.setReturnCode("0000");
        result.setReturnMessage("클레임 상태변경 성공");
        return result;
    }

    /**
     * 클레임 수거요청
     * **/
    public ResponseObject<String> setPickRequest(PickRequestSetDto pickRequestSetDto){
        ClaimShippingSetDto claimShippingSetDto = new ClaimShippingSetDto();
        claimShippingSetDto.setClaimNo(pickRequestSetDto.getClaimNo());
        claimShippingSetDto.setClaimBundleNo(pickRequestSetDto.getClaimBundleNo());
        claimShippingSetDto.setClaimShippingReqType("P");
        claimShippingSetDto.setChgId("PARTNER");
        ClaimPickShippingModifyDto claimPickShippingModifyDto = new ClaimPickShippingModifyDto(pickRequestSetDto);
        claimShippingSetDto.setClaimPickShippingModifyDto(claimPickShippingModifyDto);

        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimShippingSetDto, "/claim/reqClaimShippingChange",
            new ParameterizedTypeReference<ResponseObject<String>>() {});
    }

    /**
     * 클레임 수거완료요청
     * **/
    public ResponseObject<Boolean> setPickComplete(PickCompleteSetDto pickCompleteSetDto){

        ClaimShippingStatusSetDto claimShippingStatusSetDto = new ClaimShippingStatusSetDto();

        claimShippingStatusSetDto.setClaimNo(pickCompleteSetDto.getClaimNo());
        claimShippingStatusSetDto.setClaimBundleNo(pickCompleteSetDto.getClaimBundleNo());
        claimShippingStatusSetDto.setPickStatus("P3");
        claimShippingStatusSetDto.setChgId("PARTNER");
        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimShippingStatusSetDto, "/claim/reqClaimShippingStatus",
            new ParameterizedTypeReference<ResponseObject<Boolean>>() {});
    }

    /**
     * 클레임 수거정보변경
     * **/
    public ResponseObject<String> setPickShipping(PickShippingSetDto pickShippingSetDto){
        ClaimShippingSetDto claimShippingSetDto = new ClaimShippingSetDto();
        claimShippingSetDto.setClaimNo(pickShippingSetDto.getClaimNo());
        claimShippingSetDto.setClaimBundleNo(pickShippingSetDto.getClaimBundleNo());
        claimShippingSetDto.setClaimShippingReqType("P");
        claimShippingSetDto.setChgId("PARTNER");
        ClaimPickShippingModifyDto claimPickShippingModifyDto = new ClaimPickShippingModifyDto(pickShippingSetDto);
        claimShippingSetDto.setClaimPickShippingModifyDto(claimPickShippingModifyDto);

        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimShippingSetDto, "/claim/reqClaimShippingChange",
            new ParameterizedTypeReference<ResponseObject<String>>() {});
    }

    /**
     * 클레임 교환배송정보 변경
     * **/
    public ResponseObject<String> setClaimDelivery(ClaimDeliverySetDto claimDeliverySetDto){
        ClaimShippingSetDto claimShippingSetDto = new ClaimShippingSetDto();
        claimShippingSetDto.setClaimNo(claimDeliverySetDto.getClaimNo());
        claimShippingSetDto.setClaimBundleNo(claimDeliverySetDto.getClaimBundleNo());
        claimShippingSetDto.setClaimShippingReqType("E");
        claimShippingSetDto.setChgId("PARTNER");
        ClaimExchShippingModifyDto claimExchShippingModifyDto = new ClaimExchShippingModifyDto(claimDeliverySetDto);
        claimShippingSetDto.setClaimExchShippingModifyDto(claimExchShippingModifyDto);

        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimShippingSetDto, "/claim/reqClaimShippingChange",
            new ParameterizedTypeReference<ResponseObject<String>>() {});
    }


    /**
     * 클레임 교환배송정보 변경
     * **/
    public ResponseObject<String> setExchShipping(ExchShippingSetDto exchShippingSetDto){
        ClaimShippingSetDto claimShippingSetDto = new ClaimShippingSetDto();
        claimShippingSetDto.setClaimNo(exchShippingSetDto.getClaimNo());
        claimShippingSetDto.setClaimBundleNo(exchShippingSetDto.getClaimBundleNo());
        claimShippingSetDto.setClaimShippingReqType("E");
        claimShippingSetDto.setChgId("PARTNER");
        ClaimExchShippingModifyDto claimExchShippingModifyDto = new ClaimExchShippingModifyDto(exchShippingSetDto);
        claimShippingSetDto.setClaimExchShippingModifyDto(claimExchShippingModifyDto);

        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimShippingSetDto, "/claim/reqClaimShippingChange",
            new ParameterizedTypeReference<ResponseObject<String>>() {});
    }

    /**
     * 클레임 교환배송정보 변경
     * **/
    public ResponseObject<String> setExchComplete(ExchCompleteSetDto exchCompleteSetDto){
        ResponseObject<String> result = new ResponseObject<>();
        ClaimRequestSetDto claimRequestSetDto
            = ClaimRequestSetDto.builder()
                .claimNo(exchCompleteSetDto.getClaimNo())
                .claimBundleNo(exchCompleteSetDto.getClaimBundleNo())
                .claimReqType("CA")
                .regId("PARTNER")
                .claimType("X")
                .build();

        /* 1.교환완료 요청 */
        ResponseObject<Object> claimStatusResult = resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimRequestSetDto, "/claim/reqChange", new ParameterizedTypeReference<ResponseObject<Object>>() {});

        if(!claimStatusResult.getReturnCode().equalsIgnoreCase("0000")){
            result.setReturnCode(claimStatusResult.getReturnCode());
            result.setReturnMessage(claimStatusResult.getReturnMessage());
            return result;
        }
        ClaimShippingSetDto claimShippingSetDto = new ClaimShippingSetDto();
        claimShippingSetDto.setClaimNo(exchCompleteSetDto.getClaimNo());
        claimShippingSetDto.setClaimBundleNo(exchCompleteSetDto.getClaimBundleNo());
        claimShippingSetDto.setClaimShippingReqType("E");
        claimShippingSetDto.setChgId("PARTNER");
        ClaimExchShippingModifyDto claimExchShippingModifyDto = new ClaimExchShippingModifyDto(exchCompleteSetDto);
        claimShippingSetDto.setClaimExchShippingModifyDto(claimExchShippingModifyDto);

        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimShippingSetDto, "/claim/reqClaimShippingChange",
            new ParameterizedTypeReference<ResponseObject<String>>() {});
    }





    /**
     * 클레임 상태변경 요청
     * **/
    public ResponseObject<String> reqChgClaimStatus(ClaimRequestSetDto claimRequestSetDto) throws Exception {
        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimRequestSetDto, "/claim/reqChange", new ParameterizedTypeReference<ResponseObject<String>>() {});
    }


    /**
     * 클레임 배송 상태변경 요청
     * **/
    public ResponseObject<String> chgClaimShipping(ClaimShippingSetDto claimShippingSetDto){
        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimShippingSetDto, "/claim/reqClaimShippingChange",
            new ParameterizedTypeReference<ResponseObject<String>>() {});
    }

    /**
    * 등록된 이미지 정보 조회
    *
    * @param fileId
    * @return ClaimImageInfoDto
    */
    public ClaimImageInfoDto getClaimImageInfo(String fileId) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.UPLOAD,
            "/provide/info?processKey=ClaimAddImage&fileId="+fileId,
            new ParameterizedTypeReference<ResponseObject<ClaimImageInfoDto>>() {}
        ).getData();
    }
}
