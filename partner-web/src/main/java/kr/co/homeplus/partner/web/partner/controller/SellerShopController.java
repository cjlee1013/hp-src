package kr.co.homeplus.partner.web.partner.controller;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.partner.web.common.model.common.ResponseResult;
import kr.co.homeplus.partner.web.common.service.CodeService;
import kr.co.homeplus.partner.web.core.certification.CertificationService;
import kr.co.homeplus.partner.web.core.constants.ResourceRouteName;
import kr.co.homeplus.partner.web.core.utility.SetParameter;
import kr.co.homeplus.partner.web.core.utility.StringUtil;
import kr.co.homeplus.partner.web.partner.model.sellerShop.SellerShopDetailGetDto;
import kr.co.homeplus.partner.web.partner.model.sellerShop.SellerShopSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/partner/sellerShop")
public class SellerShopController {

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    private final ResourceClient resourceClient;
    private final CertificationService certificationService;

    public SellerShopController(ResourceClient resourceClient,
        CodeService codeService,
        CertificationService certificationService
    ) {
        this.resourceClient = resourceClient;
        this.certificationService = certificationService;
    }

    /**
     *  셀러샵관리 > 기본정보관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/sellerShopMain", method = RequestMethod.GET)
    public String sellerShopMain(Model model) {
        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAttribute("partnerId", certificationService.getLoginUerInfo().getPartnerId());
        return "/partner/sellerShopMain";
    }

    /**
     * 셀러샵관리 > 기본정보관리 > 셀러샵 중복확인
     *
     * @param
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/setSellerShopValidation.json"})
    public ResponseResult validationByShopNmOrShopUrl(@RequestParam(value = "type") String type, @RequestParam(value = "checkKeyword") String checkKeyword) {

        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create(type, checkKeyword));
        setParameterList.add(SetParameter.create("partnerId", certificationService.getLoginUerInfo().getPartnerId()));

        return resourceClient.getForResponseObject(
            ResourceRouteName.ITEM
            , "/partner/setSellerShopValidation" + StringUtil.getParameterByUtf8(setParameterList)
            , new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}
        ).getData();
    }

    /**
     * 셀러샵관리 > 기본정보관리 > 셀러샵 등록/수정
     *
     * @param
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setSellerShop.json"}, method = RequestMethod.POST)
    public ResponseResult setSellerShop(@RequestBody SellerShopSetParamDto setParamDto) {
        String apiUri = "/po//partner/sellerShop/setSellerShop";
        setParamDto.setPartnerId(certificationService.getLoginUerInfo().getPartnerId());
        setParamDto.setRegId("PARTNER");
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, setParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 셀러샵관리 > 기본정보관리 > 셀러샵 중복확인
     *
     * @param
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getSellerShopDetail.json"})
    public SellerShopDetailGetDto getSellerShopDetail() {

        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("partnerId", certificationService.getLoginUerInfo().getPartnerId()));

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , "/po/partner/sellerShop/getSellerShopDetail"  + StringUtil.getParameterByUtf8(setParameterList)
            , new ParameterizedTypeReference<ResponseObject<SellerShopDetailGetDto>>(){}
        ).getData();
    }

}