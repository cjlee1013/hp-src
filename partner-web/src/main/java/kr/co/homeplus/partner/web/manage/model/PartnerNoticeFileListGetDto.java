package kr.co.homeplus.partner.web.manage.model;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PartnerNoticeFileListGetDto {

    private String fileName;

    private String fileId;

}
