package kr.co.homeplus.partner.web.sell.model.orderShipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 발주/발송관리 선택발송처리 팝업 리스트 요청 DTO")
public class OrderShipManageShipAllPopListSetDto {
    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;
}
