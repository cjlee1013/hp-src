package kr.co.homeplus.partner.web.partner.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//판매업체 첨부서류 Get Entry
public class PartnerFileListGetDto {
    //파일시퀀스
    private String fileSeq;

    //파일유형
    private String fileType;

    //파일명
    private String fileNm;

    //파일URL
    private String fileUrl;
}
