package kr.co.homeplus.partner.web.core.interceptor;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.co.homeplus.partner.web.core.config.PartnerProperties;
import kr.co.homeplus.partner.web.core.config.RealGridProperties;
import kr.co.homeplus.partner.web.core.constants.DateTimeFormat;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 정적파일 버전(js,css, ...)과 리얼그리드 관련 정보(라이센스, 경로 등...)를 관리하는 인터셉터 입니다.
 */
@Slf4j
@RequiredArgsConstructor
public class StaticResourceInterceptor extends HandlerInterceptorAdapter {

    private final RealGridProperties realGridProperties;
    private final PartnerProperties adminProperties;

    private static final ZoneId ZONE_ID = ZoneId.of(DateTimeFormat.TIME_ZONE_KST);

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response,
        final Object handler) throws Exception {

        // 공통 css, js 버전.
        String version = adminProperties.getFileVersion();
        // fileVersionFormat 이 있으면 포맷 정보대로 사용한다.
        if (StringUtils.isNotEmpty(adminProperties.getFileVersionFormat())) {
            LocalDateTime dateTime = LocalDateTime.now(ZONE_ID);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DateTimeFormat.DATE_FORMAT_DIGIT_YMD);

            try {
                formatter = DateTimeFormatter.ofPattern(adminProperties.getFileVersionFormat());
            } catch (IllegalArgumentException e) {
                log.warn("DateTimeFormat IllegalArgumentException 'partner.file.version.format' check!");
            }
            version = dateTime.format(formatter);
        }
        request.setAttribute("fileVersion", version);

        // 리얼 그리드 라이센스 경로
        request.setAttribute("realGridRootPath", realGridProperties.getRootPath());
        request.setAttribute("realGridLicensePath", realGridProperties.getLicensePath());
        request.setAttribute("realGridMainJsPath", realGridProperties.getMainJsPath());
        request.setAttribute("realGridApiJsPath", realGridProperties.getApiJsPath());
        request.setAttribute("realGridJsZipPath", realGridProperties.getJsZipPath());

        return super.preHandle(request, response, handler);
    }
}
