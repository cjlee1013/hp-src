package kr.co.homeplus.partner.web.core.service.impl;

import org.springframework.http.HttpStatus;

/**
 * 리스트 검색 결과 제한 예외 처리
 *
 */
public class ListLimitServiceException extends RuntimeException {
    private HttpStatus httpStatus;
    private String errorMessage;

    public ListLimitServiceException(String errorMessage) {
        super(errorMessage);
    }

    public ListLimitServiceException(String errorMessage, Exception e) {
        super(errorMessage, e);
    }

    public ListLimitServiceException(String errorMessage, HttpStatus httpStatus) {
        super(errorMessage);
        this.httpStatus = httpStatus;
    }

    public ListLimitServiceException(String errorMessage, HttpStatus httpStatus, Exception e) {
        super(errorMessage, e);
        this.httpStatus = httpStatus;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
