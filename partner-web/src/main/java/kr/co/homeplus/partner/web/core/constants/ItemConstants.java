package kr.co.homeplus.partner.web.core.constants;

/**
 * 채널 관련 공통 상수
 */
public class ItemConstants {
    public static final String STOP = "P";

	public static final String ACTIVE = "A";
}
