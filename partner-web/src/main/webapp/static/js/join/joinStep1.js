
let join = {
    partnerNoCertified : false,
    init : function() {
        $('#partnerNo').allowInput("keyup", ["NUM"]);
        join.event();
    },
    event : function() {
        $('#allChk').on('click', function() {
            if ($(this).is(':checked')) {
                $('#agreeArea input[type="checkbox"]').prop('checked', true);
            } else {
                $('#agreeArea input[type="checkbox"]').prop('checked', false);
            }
        });

        $('[id^=joinChk0]').on('click', function() {
            if (!$(this).is(':checked')) {
                $('#allChk').prop('checked', false);
            }
        });

        $('#partnerNo').on('propertychange change keyup paste input', function(){
            $('#partnerNoStatus').text($('#partnerNoStatus').data('txt'));
            join.partnerNoCertified = false;
        });
    },
    getPartnerNo : function() {
        if ($.jUtil.isEmpty($('#partnerNo').val())) {
            alert('사업자 등록번호를 입력해주세요.');
        } else {
            var partnerNo = $('#partnerNo').val().replace(/[^0-9]/g,"");
            if($.jUtil.isEmpty(partnerNo)) {
                return $.jUtil.alert('사업자번호를 입력해주세요.');
            }

            if (partnerNo.length < 10) {
                return $.jUtil.alert('사업자등록번호 자리수를 확인해 주세요.');
            }

            CommonAjax.basic({
                url:'/partner/checkPartnerNo.json',
                data: {
                    'partnerNo' : partnerNo,
                },
                method:'get',
                callbackFunc:function(ret) {
                    $('#partnerNm').val(ret.compName);
                    $('#partnerOwner').val(ret.repName);
                    $('#partnerNoStatus').text('인증완료');
                    join.partnerNoCertified = true;
                }
            });
        }
    },
    setJoin : function() {
        let checkAgree = false;

        if (join.partnerNoCertified == false) {
            return $.jUtil.alert('사업자번호 인증 성공 후 다음 단계 이동이 가능합니다.');
        }

        $('#agreeArea input[type="checkbox"]').each(function(){
            if ($(this).is(':checked') == false) {
                checkAgree = true;
                return false;
            }
        });

        if (checkAgree) {
            return $.jUtil.alert('모든 약관에 동의 후 다음 단계 이동이 가능합니다.');
        }

        $('input[name="partnerNo"]').val($('#partnerNo').val());
        $('input[name="partnerNm"]').val($('#partnerNm').val());
        $('input[name="partnerOwner"]').val($('#partnerOwner').val());

        $("#step2Form").attr("method", "post").submit();
    }
};