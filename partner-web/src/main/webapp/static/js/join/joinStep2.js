
var join = {
    checkId: false,
    timer       	: 0,
    smsCertToken	: "",		// sms 토큰
    smsSuccessToken	: "",		// sms 성공 토큰
    checkInfo		: "",
    smsCountdownTimer	: null,
    mobile			: "",
    smsCheck        : false,
    isCheckBankAccnt    : false,
    init: function () {
        //사업자번호 셋팅
        $('#partnerNo_1').val(partnerNo.substr(0, 3));
        $('#partnerNo_2').val(partnerNo.substr(3, 2));
        $('#partnerNo_3').val(partnerNo.substr(5, 5));
        $('#partnerNm').val(partnerNm);
        $('#partnerOwner').val(partnerOwner);
        commonCategory.setCategorySelectBox(1, '', '', $('#categoryCd'));
        $('#businessNm').allowInput("keyup", ["ENG", "KOR", "NUM", "SPC_1"]);
        $("#bankAccountNo, [id^=phone1_], [id^=infoCenter_], [id^=mngMobile], [id^=mngPhone]").allowInput("keyup", ["NUM"]);
        join.event();
        $('input[name="sellerInfo.cultureDeductionYn"][value="N"]').trigger('click');
    },
    event: function () {
        $('#getBizCateCd').bindClick(join.getBizCateCd);
        $("input[name='sellerInfo.cultureDeductionYn']").bindClick(join.setCultureDeductionYn);

        $('#partnerId').on('focusout', function () {
            join.checkSeller($(this).val());
        });

        $('#password').on('focusout', function () {
            join.checkPassword($(this).val());
            $('#passwordConfirm').trigger('focusout');
        });

        $('#passwordConfirm').on('focusout', function () {
            join.checkPassword($('#password').val(), $(this).val());
        });

        $('#copyAffi').on('change', function () {
            if ($(this).is(':checked')) {
                for (var i = 1; i < 4; i++) {
                    $('#mngNm' + i).val($('#mngNm0').val());
                    $.jUtil.valSplit($.jUtil.valNotSplit('mngMobile0', '-'), '-',
                            ('mngMobile' + i));
                    $.jUtil.valSplit($.jUtil.valNotSplit('mngPhone0', '-'), '-',
                            ('mngPhone' + i));
                    $('#mngEmail' + i).val($('#mngEmail0').val());
                }
            } else {
                for (var i = 1; i < 4; i++) {
                    $('#managerDiv' + i).find('input').not('input[type="hidden"]').val('');
                }
            }
        });

        // 종목 추가
        $(document).on("click", "button[id^=categorySearchList_]", function () {
            join.addSchCategory(this);
        });

        // 종목 삭제
        $(document).on("click", "button[id^=categoryDelete_]", function () {
            join.delSchCategory(this);
        });

        // 통신판매업 신고번호 불러오기
        $("#communityNotiNoBtn").on("click", function () {
            join.getCommunityNotiNo($.jUtil.valNotSplit('partnerNo', ''));
        });

        // 통신판매업 신고번호 추가
        $(document).on("click", "button[id^=communityNotiNoList_]", function () {
            $('#communityNotiNo').val($(this).text());
        });

        // 인증하기
        $("#certiCheck").on("click", function() {
            if (join.affiManagerPhoneCheck()){
                // 인증번호 호출
                var mobileNum = $.jUtil.valNotSplit('mngMobile0', '-');
                join.setCertified(mobileNum);
            }
        });

        // 재발송
        $("#reCertiCheck").on("click", function() {
            if (join.affiManagerPhoneCheck()){
                // interval clear
                clearInterval(join.timer);
                // 인증번호 호출
                var mobileNum = $.jUtil.valNotSplit('mngMobile0', '-');
                join.setCertified(mobileNum);
            }
        });

        // 인증하기 - 완료
        $("#certiCheckWar").on("click", function() {
            if ($("#checkInfo").val() == "") {
                alert("인증번호를 입력해 주세요.");
                $("#checkInfo").focus();
                return;
            }

            if ($("#checkInfo").val().length != 6) {
                alert("6자리 인증번호를 입력해 주세요.");
                $("#checkInfo").focus();
                return;
            }

            var mobileNum = $.jUtil.valNotSplit('mngMobile0', '-');
            join.checkCertified(mobileNum);
        });

        // 인증해제
        $("#releaseCerti").on("click", function () {
            if (!confirm("등록한 휴대폰 번호의 인증을 해제하고 새로운 번호로 등록하시겠습니까?")) {
                return;
            }

            // 영업담당자 인증 flag 변경
            join.smsCheck = false;

            // 휴대폰 번호 입력 활성화
            $("#mngMobile0_1").attr("readonly", false);
            $("#mngMobile0_2").attr("readonly", false);
            $("#mngMobile0_3").attr("readonly", false);

            // button view
            $("#certiCheck").show();
            $("#reCertiCheck").hide();
            $("#releaseCerti").hide();
            $("#divCertiCheck").hide();
        });

        $('#getAccAuthenticationBtn').on('click', function() {
            if (join.isCheckBankAccnt) {
                join.isCheckBankAccnt = false;
                $(this).text('계좌인증');
                $('#bankStatus').text('인증대기');
                $('#bankCd, #bankAccountNo').prop('disabled', false).val('');
                $('#depositor').val('');
            } else {
                join.getBankAccntNoCertify();
            }
        });
    },
    checkSeller: function (partnerId) {
        join.checkId = false;
        var msgPartnerId = '';
        var checkId = partnerId.substr(0, 4).toLowerCase();

        if ($.jUtil.isEmpty(partnerId)) {
            msgPartnerId = '사용할 아이디를 입력해주세요.';
        } else if (!$.jUtil.isAllowInput(partnerId, ['ENG_LOWER', 'NUM']) || $.jUtil.isNotAllowInput($('#partnerId').val(), ['SPACE']) || partnerId.length
                < 5 || partnerId.length > 12) {
            msgPartnerId = '영문 소문자 5~12자와 숫자로만 조합하여 사용가능합니다.';
        } else if (checkId == 'coop' || checkId == 'affi') {
            msgPartnerId = 'coop 또는 affi로 시작하는 ID는 사용불가합니다.';
        }

        if (msgPartnerId) {
            $('#partnerId').addClass('err');
            $('#partnerIdTxt').removeClass('txt-success').addClass('txt-err').text(
                    msgPartnerId).show();
            return false;
        }

        CommonAjax.basic({
            url: '/join/checkSeller.json',
            data: {partnerId: partnerId},
            method: 'GET',
            callbackFunc: function (res) {
                if (res) {
                    $('#partnerId').addClass('err');
                    $('#partnerIdTxt').removeClass('txt-success').addClass('txt-err').text(
                            '이미 사용중인 아이디입니다.').show();
                } else {
                    $('#partnerId').removeClass('err');
                    $('#partnerIdTxt').removeClass('txt-err').addClass('txt-success').text(
                            '사용 가능 합니다.').show();
                    join.checkId = true;
                }
            }
        });
    },
    checkPassword: function (password, passwordConfirm) {
        if (passwordConfirm) {
            if (password == passwordConfirm) {
                $('#passwordConfirm').removeClass('err');
                $('#passwordConfirmTxt').removeClass('txt-err').addClass('txt-success').text(
                        '일치합니다.').show();
            } else {
                $('#passwordConfirm').addClass('err');
                $('#passwordConfirmTxt').removeClass('txt-success').addClass('txt-err').text(
                        '일치하지 않습니다.').show();
            }
        } else if (password) {
            var partnerId = $('#partnerId').val();
            var msgPassword = '';

            if (!$.jUtil.isAllowInput(password, ['ENG_LOWER', 'NUM', 'PASSWORD'])
                    || password == partnerId
                    || /(.)\1\1/.test(password)
                    || password.length < 10 || password.length > 15) {
                msgPassword = '비밀번호 생성규칙을 확인해주세요.';
            }

            var num = password.search(/[0-9]/g);
            var eng = password.search(/[a-z]/ig);
            var spe = password.search(/[!@#%&\\*\\^\\$]/gi);

            if( (num < 0 && eng < 0) || (eng < 0 && spe < 0) || (spe < 0 && num < 0) ) {
                msgPassword = "영문,숫자, 특수문자 중 2가지 이상을 혼합하여 입력해주세요.";
            }

            if (msgPassword) {
                $('#password').addClass('err');
                $('#passwordTxt').removeClass('txt-success').addClass('txt-err').text(
                        msgPassword).show();
            } else {
                $('#password').removeClass('err');
                $('#passwordTxt').removeClass('txt-err').addClass('txt-success').text(
                        '사용 가능 합니다.').show();
            }
        }
    },
    setJoin: function () {
        if (join.valid.set()) {
            $('#partnerNo').val($.jUtil.valNotSplit('partnerNo', '-'));
            $('#phone1').val($.jUtil.valNotSplit('phone1', '-'));
            $('#infoCenter').val($.jUtil.valNotSplit('infoCenter', '-'));

            for (var i = 0; i < 4; i++) {
                $('#mngMobile' + i).val($.jUtil.valNotSplit('mngMobile' + i, '-'));
                $('#mngPhone' + i).val($.jUtil.valNotSplit('mngPhone' + i, '-'));
                $('#mngEmail' + i).val($('#mngEmail' + i ).val());
            }

            var sellerForm = $('#sellerForm').serializeObject();
            // sellerForm.sellerInfo.email = $('#email').val() + '@' + $('#email1').val();

            CommonAjax.basic({
                url: "/join/setJoinPartner.json",
                data: JSON.stringify(sellerForm),
                method: "POST",
                contentType: "application/json",
                callbackFunc: function (res) {
                    alert('가입이 완료되었습니다.');
                    $.jUtil.redirectPost("/join/step3",  { mngNm: $('#mngNm0').val(), mngEmail:$('#mngEmail0').val(), mngMobile: $('#mngMobile0').val(), mngPhone: $('#mngPhone0').val() });
                }
            });
        }
    },
    zipcode: function (res) {
        $('#zipcode').val(res.zipCode);
        $('#addr1').val(res.roadAddr);
    },
    /**
     * 종목 조회
     */
    getBizCateCd: function () {
        CommonAjax.basic({
            url: "/partner/getBizCateCdList.json",
            data: JSON.stringify({
                codeNm: $("#bizCateCdNm").val()
            }),
            method: "POST",
            contentType: "application/json",
            callbackFunc: function (res) {
                var resultCount = res.length;

                if (resultCount == 0) {
                    $("#searchCategory").html("");
                    $("#searchCategory").append(
                            '<li><button type="button">검색결과가 없습니다.</button></li>');
                } else {
                    $("#searchCategory").html("");

                    for (var idx = 0; resultCount > idx; idx++) {
                        var data = res[idx];

                        $("#searchCategory").append(
                                '<li><button type="button" id="categorySearchList_'
                                + data.mcCd + '">' + data.mcNm + "</button></li>");
                    }
                }
                $('#searchCategory').closest('.layer-srh').addClass('on');
            }
        });
    },
    /**
     * 소득공제 제공여부
     */
    setCultureDeductionYn : function(cultureDeductionYn) {
        if (typeof cultureDeductionYn == 'object') {
            cultureDeductionYn = $(cultureDeductionYn).val();
        }

        if (cultureDeductionYn == 'Y') {
            $('#cultureDeductionNo').prop('readonly', false);
        } else {
            $('#cultureDeductionNo').val('').prop('readonly', true);
        }
    },
    /**
     * 종목 추가
     * @param obj
     */
    addSchCategory: function (obj) {
        var idArr = $(obj).attr("id").split("_");
        var categoryCd = idArr[1];
        var categoryCdNm = $(obj).html();

        join.addSchCategoryHtml(categoryCd, categoryCdNm);
    },
    /**
     * 종목 추가 html
     * @param categoryCd
     * @param categoryCdNm
     * @returns {boolean}
     */
    addSchCategoryHtml: function (categoryCd, categoryCdNm) {
        $('#bizCateCd').val(categoryCd);
        $('#bizCateCdNm').val(categoryCdNm);

        if ($("#bizCateCdDiv_" + categoryCd).length > 0) {
            $("#bizCateCdDiv_" + categoryCd).show();
        } else {
            var html = "<span id='bizCateCdDiv_" + categoryCd + "' class='tag'>";
            html += "<span class='mg-r-10 mg-l-10 mg-t-10'>" + categoryCdNm + "</span>";
            html += "<button type='button' class='btn-close' id='categoryDelete_" + categoryCd
                    + "'><i class='hide'>태그삭제</i></button>";
            html += "</span>";

            $("#bizCateCdListId").html(html);
        }
    },
    /**
     * 종목 삭제
     * @param obj
     */
    delSchCategory: function (obj) {
        var idArr = $(obj).attr("id").split("_");
        var categoryCd = idArr[1];

        $("#bizCateCdDiv_" + categoryCd).hide();
    },
    /**
     * 파트너관리 > 통신판매업신고번호 조회
     * @param partnerNo
     */
    getCommunityNotiNo: function (partnerNo) {
        CommonAjax.basic({
            url: '/partner/getCommuniNotiNo.json',
            data: {partnerNo: partnerNo},
            method: 'GET',
            callbackFunc: function (res) {
                if (res.returnCode === "1") {
                    if (res.returnKey) {
                        $("#communityNotiNo").val(res.returnKey);
                    } else {
                        $("#searchCommunityNotiNo").html("");
                        for (var i in res.resultList) {

                            $("#searchCommunityNotiNo").append(
                                    '<li><button type="button" id="communityNotiNoList_'
                                    + i + '">' + res.resultList[i] + "</button></li>");

                        }

                        $('#searchCommunityNotiNo').closest('.layer-srh').addClass('on');
                    }
                } else {
                    alert("해당 사업자등록번호로 인증된 통신판매업신고번호가 없습니다.");
                    $("#communityNotiNo").focus();
                }
            }
        });
    },
    /**
     * 영업담당자 휴대전화 체크
     * @returns {boolean}
     */
    affiManagerPhoneCheck : function(){
        if ($("#mngMobile0_1").val() == "" || $("#mngMobile0_2").val() == "" || $("#mngMobile0_3").val() == "") {
            alert("휴대폰 번호를 입력해주세요.");
            return false;
        }
        if($.jUtil.phoneValidate($("#mngMobile0_1").val() + '-' + $("#mngMobile0_2").val() + '-' + $("#mngMobile0_3").val(), true)) {
            return true;
        } else {
            alert('휴대폰 번호 형식이 잘못되었습니다. 다시 확인해 주세요.');
            return false;
        }
    },
    /**
     * 영업담당자 인증번호 생성
     * @param mobile
     */
    setCertified : function(mobile) {
        CommonAjax.basic({
            url:"/common/mobile/certno/send.json?mobile="+mobile
            , method:"GET"
            , callbackFunc:function(res) {
                if (res.hasOwnProperty("data")) {
                    join.smsCertToken = res.data;

                    // 타이머 호출
                    join.checkTimer();
                    alert("인증번호가 전송되었습니다.");

                    // 재발송 view
                    $("#certiCheck").hide();
                    $("#reCertiCheck").show();
                    $("#divCertiCheck").show();

                }
            }
            , errorCallbackFunc : function() {
                clearInterval(join.timer);

                // 인증 view
                $("#certiCheck").show();
                $("#reCertiCheck").hide();
                $("#divCertiCheck").hide();
            }
        });
    },
    /**
     * 남은 시간 카운터
     */
    checkTimer : function() {
        var endTime = moment(new Date()).add(3, 'minutes');
        if(join.smsCountdownTimer != null)
        {
            join.smsCountdownTimer.endTimer();
        }
        join.smsCountdownTimer = countdownTimer($('#spanTimeOut'),endTime,'ms','0:00',function(){
            if (join.smsCheck == false) {
                alert('인증번호 입력시간이 지났습니다. \r\n다시 시도해 주세요.');
                join.smsCheck = false;
            }
        });
    },
    /**
     * 인증번호 확인
     * @param mobile
     */
    checkCertified : function(mobile) {
        var param = {
                certToken: join.smsCertToken
            ,   certNo: $("#checkInfo").val()
            ,   mobile: mobile
        };
        CommonAjax.basic({
            url: "/common/mobile/certno/valid.json"
            , data: JSON.stringify(param)
            , contentType: 'application/json'
            , method:"POST"
            , callbackFunc:function(res) {
                if (res.hasOwnProperty("data") && res.data == true) {
                    alert("휴대폰 인증에 성공하였습니다.");

                    join.smsSuccessToken = res.data;
                    join.checkInfo = $("#checkInfo").val();
                    join.mobile = mobile;
                    join.smsCheck = true;
                    clearInterval(join.timer);

                    $("#checkInfo").val("");

                    // 휴대폰 번호 입력 활성화
                    $("#mngMobile0_1").attr("readonly", true);
                    $("#mngMobile0_2").attr("readonly", true);
                    $("#mngMobile0_3").attr("readonly", true);

                    // button view
                    $("#certiCheck").hide();
                    $("#reCertiCheck").hide();
                    $("#releaseCerti").show();
                    $("#divCertiCheck").hide();
                } else {
                    alert(res.returnMessage);
                }
            }
        });
    },
    /**
     * 계좌실명 인증 조회
     * @returns {*|boolean}
     */
    getBankAccntNoCertify : function() {
        if ($.jUtil.isEmpty($("#bankCd").val())) {
            return $.jUtil.alert('은행을 선택해주세요.', 'bankCd');
        }

        if ($.jUtil.isEmpty($("#bankAccountNo").val())) {
            return $.jUtil.alert('계좌번호를 입력해주세요.', 'bankAccountNo');
        }

        CommonAjax.basic({
            url: "/partner/getBankAccntNoCertify.json",
            data: {
                bankCd: $("#bankCd").val(),
                bankAccntNo: $("#bankAccountNo").val()
            },
            method: "POST",
            callbackFunc: function (res) {
                if ($.jUtil.isNotEmpty(res.depositor)) {
                    alert('인증되었습니다.');
                    $('#bankStatus').text('인증완료');
                    $('#getAccAuthenticationBtn').text('재설정');
                    $('#bankCd, #bankAccountNo').prop('disabled', true);
                    $('#depositor').val(res.depositor);
                } else {
                    alert('인증 실패하였습니다.');
                    $('#bankStatus').show();
                }

                join.isCheckBankAccnt = res;
            }
        });
    }
};

join.valid = {
    set: function () {
        var isValid = true;

        var checkObj = {
            partnerId: {
                types: ['ENG_LOWER', 'NUM'],
                msg: '아이디와 비밀번호를 확인해주세요.',
                min: 6,
                max: 12
            },
            password: {
                types: ['ENG', 'NUM', 'PASSWORD'],
                msg: '아이디와 비밀번호를 확인해주세요.',
                min: 6,
                max: 15
            },
            businessNm: {   //판매업체명
                types: ['KOR', 'ENG', 'NUM', "SPC_1"],
                msg: '필수입력항목을 모두 입력해주세요.',
                min: 2,
                max: 50
            },
            partnerOwner: { //대표자명
                types: ['KOR', 'ENG', 'COMMA'],
                msg: '필수입력항목을 모두 입력해주세요.',
                min: 2,
                max: 50
            },
            businessConditions: { //업태
                types: ['KOR', 'ENG', 'NUM'],
                msg: '필수입력항목을 모두 입력해주세요.',
                min: 2,
                max: 50
            }
        };

        let alertInfo = {msg:"", id:""};

        for (var id in checkObj) {
            var checkVal = $('#' + id).val();

            if (!$.jUtil.isAllowInput(checkVal, checkObj[id].types) || checkVal.length
                    < checkObj[id].min || checkVal.length > checkObj[id].max) {

                alertInfo.msg = checkObj[id].msg;
                alertInfo.id  = id;
                isValid = false;
                break;
            }
        }

        $('.checkVal').each(function () {
            if ($.jUtil.isEmpty($(this).val())) {
                isValid = false;
                alertInfo.msg = '필수입력항목을 모두 입력해주세요.';
                alertInfo.id  = $(this).prop('id');
                return isValid;
            }
        });

        $('[id^=mngEmail]').each(function(){
            if (!_V.chkEmail($(this).val())) {
                isValid = false;
                alertInfo.msg = '(담당자 이메일) 형식에 맞지 않습니다.';
                alertInfo.id  = $(this).prop('id');
                return isValid;
            }
        });

        if (isValid) {

            if ($.jUtil.isEmpty($('#zipcode').val())) {
                return $.jUtil.alert('필수입력항목을 모두 입력해주세요.', 'addr2');
            } else if (!$('input[name="sellerInfo.smsYn"]').is(':checked')) {
                return $.jUtil.alert('필수입력항목을 모두 입력해주세요.', 'lbAgree01_01');
            } else if (!$('input[name="sellerInfo.emailYn"]').is(':checked')) {
                return $.jUtil.alert('필수입력항목을 모두 입력해주세요.', 'lbAgree02_01');
            } else if ($.jUtil.isEmpty($('#bizCateCd').val())) {
                return $.jUtil.alert('필수입력항목을 모두 입력해주세요.', 'bizCateCdNm');
            } else if ($.jUtil.isEmpty($('#passwordConfirm').val())) {
                return $.jUtil.alert('필수입력항목을 모두 입력해주세요.', 'passwordConfirm');
            }

            if (join.smsCheck == false) {
                return $.jUtil.alert('영업담당자 휴대폰 인증을 진행해주세요.', 'mngMobile0_1');
            }

            if ($('.txt-err').length > 0) {
                return $.jUtil.alert('비밀번호를 확인해주세요.', 'password');
            }

            if (!join.isCheckBankAccnt) {
                return $.jUtil.alert('입금계좌의 계좌실명인증을 완료해주세요.', 'depositor');
            }
            return isValid;
        } else {
            return $.jUtil.alert(alertInfo.msg, alertInfo.id);
        }
    }
};

let partner = {
    file : {
        init : function() {
            partner.file.event();
        },
        event : function() {
            //파일선택시
            $('input[name="partnerFile1"], input[name="partnerFile2"], input[name="partnerFile3"], input[name="partnerFile4"], input[name="partnerFile5"]').change(function() {
                partner.file.addFile($(this).data('file-id'), $(this).prop('name'));
            });

            //판일삭제
            $('[id^=uploadPartnerFileSpan_]').on('click', '.btn-close', function() {
                if (confirm('등록된 파일을 삭제하시겠습니까?')) {
                    $(this).closest('.add-file').remove();
                }
            });

            $('#partnerFileTable').on('click', '.partnerDownLoad', function() {
                $.jUtil.downloadFile($(this).data('url'), $(this).data('file-nm'), $(this).data('process-key'));
            });
        },
        clickFile : function(obj) {
            let limit = Number($('input[name="'+obj.data('field-name')+'"]').data('limit'));

            if ($('#uploadPartnerFileSpan_'+obj.data('file-type')).find('.btn-close').length >= limit) {
                return $.jUtil.alert("파일 추가는 최대 "+limit+"개까지 가능합니다.");
            }

            $('input[name="'+obj.data('field-name')+'"]').click();
        },
        /**
         * 파일업로드
         */
        addFile : function(id, fieldName) {
            let file = $('input[name="'+fieldName+'"]');

            let params = {
                processKey : "PartnerJoinBinary",
                fieldName : fieldName,
                mode : "FILE"
            };

            CommonAjax.upload({
                file: file,
                data: params,
                callbackFunc: function (resData) {
                    let errorMsg = "";
                    for (let i in resData.fileArr) {
                        let f = resData.fileArr[i];

                        if (f.hasOwnProperty("error")) {
                            errorMsg += f.error;
                        } else {
                            partner.file.appendPartnerFile(f.fileStoreInfo, id);
                        }
                    }
                    if (!$.jUtil.isEmpty(errorMsg)) {
                        alert(errorMsg);
                    }
                }
            });
        },
        appendPartnerFile : function(f, id) {
            if (id) { //등록
                let html = '<li class="add-file">'
                        + '<input type="hidden" class="fileSeq" name="partnerFileList[].fileSeq" value=""/>'
                        + '<input type="hidden" class="fileType" name="partnerFileList[].fileType" value="'+$('#'+id).data('file-type')+'"/>'
                        + '<input type="hidden" class="fileUrl" name="partnerFileList[].fileUrl" value="'+f.fileId+'"/>'
                        + '<input type="hidden" class="fileNm" name="partnerFileList[].fileNm" value="'+f.fileName+'"/>'
                        + '<a href="javascript:void(0);" class="partnerDownLoad file-link" data-url="'+ f.fileId + '" '
                        + 'data-process-key="PartnerJoinBinary" '
                        + 'data-file-nm="' + f.fileName + '" '
                        + '" >' + f.fileName
                        + '</a><button type="button" class="btn-close"><i class="hide">첨부파일삭제</i></button></li>';

                $('#'+id).append(html);
            }
        }
    }
};