var sellUtil = {
  initSearchDateCustom : function(startId, endId, startDt, endDt) {
    var setDateVal = Calendar.datePickerRange(startId, endId);
    setDateVal.setEndDate(endDt);
    setDateVal.setStartDate(startDt);
    $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
  },

  onRealGridPaging : function(_gridView, _pagingArea, _pageView, _pagingCount) {
    _gridView.setPaging(false);

    //체크박스 설정값을 실행.
    _gridView.applyCheckables();
    _gridView.setPaging(true, _pageView, -1, 'items');

    sellUtil.onRealGridDrawPaging(_gridView, _pagingArea, _pagingCount);

    //page이동할 때마다 호출.
    _gridView.onPageChanged = function (grid, page) {

      page = page + 1;

      //버튼 Active속성 삭제
      $('#' + _pagingArea).find('a').removeClass("active");

      //버튼 중에 현재 선택된 페이지를 찾는다.
      $('#' + _pagingArea).find('a').each(function(){
        if($(this).html() == page){
          //선택된 페이지에 Active속성 지정
          $(this).addClass("active");
        }
      });

      //첫페이지와 마지막 페이지 일경우에만 다시 그려준다.
      if(page % _pagingCount == 1 || (page % _pagingCount == 0)){
        sellUtil.onRealGridDrawPaging(_gridView, _pagingArea, _pagingCount);
      }
    };
  },

  onRealGridDrawPaging : function (_gridView, _pagingArea, _pagingCount) {
    //paging Div Clear
    $('#' + _pagingArea).empty();

    //페이징의 맨 처음페이지 혹은 이전페이징의 마지막페이지 으로 이동
    var firstButton = sellUtil.gridPagingButton("prev-all", "#none", false);
    //버튼역할에 맞는 Click 속성 추가
    firstButton.onclick = function () {
      //현재페이지 / paging View 수 에 -1을 하고 paging View를 곱한다.
      //예) 현재 페이지가 12 페이지 이고 paging View 가 10 경우
      // ((12 / 10) - 1) * 10 -> (1-1) * 10 -> 0 * 10 ==> 0 처음 페이지
      _gridView.setPage((Math.ceil(_gridView.getPage() / _pagingCount) - 1) * _pagingCount);
    };

    //이전페이지로 이동
    var prevButton = sellUtil.gridPagingButton("prev", "#none", false);
    //버튼역할에 맞는 Click 속성 추가
    prevButton.onclick = function () {
      //현제 페이지의 직전 페이지로 이동
      _gridView.setPage(_gridView.getPage() - 1);
    };

    //paging Div에 버튼 추가
    $('#' + _pagingArea).append(firstButton);
    $('#' + _pagingArea).append(prevButton);

    //현재 paging 의 마지막 버튼의 값을 계산한다.
    var lastButtonText = (Math.ceil((_gridView.getPage() + 1) / _pagingCount) * 10) > _gridView.getPageCount() ? _gridView.getPageCount() : (Math.ceil((_gridView.getPage() + 1) / _pagingCount) * 10);
    //현재 paging 의 첫번째 버튼의 값을 계산한다.
    var firstButtonText = (lastButtonText - _pagingCount) + ((lastButtonText - _pagingCount) % _pagingCount == 0 ? 0 : _pagingCount - (lastButtonText % _pagingCount));
    //보정(페이징의 전체 페이지가 0인경우)
    if(_gridView.getPageCount() == 0){
      firstButtonText = _gridView.getPageCount();
      lastButtonText = _gridView.getPageCount() + 1;
    }

    //이동 페이지 버튼 생성
    $('#' + _pagingArea).append(document.createElement("span"));
    for(var idx = firstButtonText; idx < lastButtonText; idx++){
      //숫자 버튼 생성
      var pageButton = sellUtil.gridPagingButton("page", "#none", (_gridView.getPage() == idx) ? true : false);

      //숫자 버튼의 숫자 지정
      pageButton.innerHTML = (idx + 1);
      //숫자 버튼의 숫자 속성 지정
      pageButton.pageNum = idx;
      //숫자 버튼의 Click 속성 지정
      pageButton.onclick = function () {
        //숫자 버튼에 지정된 페이지로 이동
        _gridView.setPage(this.pageNum);
      };

      //paging Div에 숫자 버튼 추가
      $('#' + _pagingArea).find('span').append(pageButton);
    }

    //다음페이지 이동
    var nextButton = sellUtil.gridPagingButton("next", "#none", false);
    //버튼역할에 맞는 Click 속성 추가
    nextButton.onclick = function () {
      //현재 페이지의 다음 페이지로 이동
      _gridView.setPage(_gridView.getPage() + 1);
    };

    //다음페이지의 첫페이지 혹은 현재페이징의 마지막페이지(마지막페이징인경우)로 이동
    var lastButton = sellUtil.gridPagingButton("next-all", "#none", false);
    //버튼역할에 맞는 Click 속성 추가
    lastButton.onclick = function () {
      //현재 페이지의 다음 paging의 첫 페이지로 이동
      //math.ceil로 올림 기능을 사용하여 ((현재페이지 / paging View 수) * paging View 수) + 1 로 이동
      //예) 현재 페이지가 3페이지이고 paging View 가 10인 경우
      //(Math.ceil(3+1/10) * 10) -> Math.ceil(0.4) * 10) Math.ceil 함수로 인해 (1*10) ==> 10 다음 paging에 첫 페이지
      var pageNum = (Math.ceil((_gridView.getPage() + 1) / _pagingCount) * _pagingCount);
      _gridView.setPage(pageNum);
    };

    $('#' + _pagingArea).append(nextButton);
    $('#' + _pagingArea).append(lastButton);

  },

  /* 그리드의 버튼을 그리기 위함. */
  gridPagingButton : function (_type, _linkUrl, _active) {
    if(_type != 'page'){
      //링크 아이콘 생성
      var button = document.createElement('button');
      var buttonLinkIcon = document.createElement("i");
      //타입에 따른 버튼의 Class 지정.
      button.setAttribute("type", "button");
      button.setAttribute("class", "btn-page " + _type);
      buttonLinkIcon.setAttribute("class","hide");

      switch (_type) {
        case "prev-all" : buttonLinkIcon.append("맨 처음 목록으로"); break;
        case "prev" : buttonLinkIcon.append("이전 목록으로"); break;
        case "next" : buttonLinkIcon.append("다음 목록으로"); break;
        case "next-all" : buttonLinkIcon.append("맨 마지막 목록으로"); break;
        default : break;
      }

      button.appendChild(buttonLinkIcon);
    }else{
      var button = document.createElement('a');
      //page버튼인 경우 active여부에 따라 Class 지정.
      button.setAttribute("class", ((_active) ? "active" : ""));
    }
    //링크URL
    button.setAttribute("href", _linkUrl);

    //버튼 정보 리턴.
    return button;
  },

  calculationOfDate : function (basicDate, flag) {
    const pattern = /([+\-]?[0-9]+)\s*(s|S|h|H|d|D|w|W|m|M|y|Y)?/g;
    const matches = pattern.exec(flag);
    return moment(basicDate).add(matches[1], matches[2])
  },
  calculationOfDateWithFormat : function ({basicDate, flag, format}) {
    const pattern = /([+\-]?[0-9]+)\s*(s|S|h|H|d|D|w|W|m|M|y|Y)?/g;
    const matches = pattern.exec(flag);
    return moment(basicDate).add(matches[1], matches[2]).format(format);
  },
  setLinkTab : function (_tabId, _tabName, _url, _tabSize) {
    const subTabBar = parent.Tabbar;

    if(_tabSize == null){
      _tabSize = 150;
    }

    if(subTabBar.getAllTabs().indexOf(_tabId) !== -1) {  //탭이 이미 있다면 데이터 갱신 후 활성화
      subTabBar.tabs(_tabId).attachURL(_url);
      subTabBar.tabs(_tabId).setActive();
    }
    else {
      subTabBar.addTab(_tabId, _tabName, _tabSize, null, true, true);
      subTabBar.tabs(_tabId).attachURL(_url);
      subTabBar.enableAutoReSize();
    }
  },
  setRealGridColumnLink : function(_gridView, _columnIds, _url, _linkView){
    let _columnArray = _columnIds;
    if(!Array.isArray(_columnIds)){
      _columnArray = [];
      _columnArray = [_columnIds];
    }
    for(let _columnId of _columnArray){
      _gridView.gridView.setColumnProperty(_columnId, "renderer", {
        type : "link",
        url : _url,
        requiredFields : _columnId,
        showUrl : _linkView
      });
    }

  },
  getRealGridColumnValue : function (_gridView, _findText, _rowId) {
    let columnValue = "";
    $.each(_gridView.gridView.getColumns(), function (idx, data) {
      if(data.type === "group"){
        $.each(data.columns, function (_gIdx, _dData) {
          if(_gridView.gridView.getColumnProperty(_dData, "header").text === _findText){
            columnValue = _gridView.dataProvider.getJsonRow(_rowId)[_dData.name];
            return false;
          }
        });
      }else{
        if(_gridView.gridView.getColumnProperty(data, "header").text === _findText){
          columnValue = _gridView.dataProvider.getJsonRow(_rowId)[data.name];
          return false;
        }
      }
    });

    return columnValue;
  },
  writeToSpan : function (_formId, _data) {
    let value = "";
    const spanId = $('#' + _formId).find('span');
    for(let idx = 0; idx < spanId.length; idx++){
      value = _data[spanId[idx].id];
      if($.jUtil.isEmpty(value)){
        value = '-';
      }
      const checkId = spanId[idx].id.toLowerCase();
      if(checkId.indexOf("amt") > -1 || checkId.indexOf("price") > -1){
        if(!isNaN(Number(value))){
          value = $.jUtil.comma(Number(value));
        }
      }
      if(!$.jUtil.isEmpty(spanId[idx].id)){
        $(spanId[idx]).html(value);
      }
    }
  },
  writeToData : function (_dataArray) {
    for(const data of _dataArray){
      for(const key of Object.keys(data)){
        const keyValue = $('#' + key);
        if(keyValue.length){
          if($('input[id=' + key + "]").length || $('#' + key + ' option:selected').length){
            keyValue.val(data[key]);
          } else {
            keyValue.text(data[key]);
          }
        }
      }
    }
  },
  checkBoxChecked : function (_selectedInfo, _allValue){
    const unCheckedBox = $('input:checkbox[name=' + _selectedInfo.attr('name') + ']');
    const checkedBox = $('input:checkbox[name=' + _selectedInfo.attr('name') + ']:checked');

    if(_selectedInfo.val() === _allValue){
      console.log(_selectedInfo.val(),  _allValue);
      unCheckedBox.each(function (_idx, _value) {
        if(_value.value !== _allValue){
          this.checked = _selectedInfo.is(':checked');
        }
      });
    } else {
      const checkedValue = checkedBox.map(function () {
        return this.value;
      }).get();
      if(checkedBox.length === (unCheckedBox.length - 1)){
        let isAllCheck = false;
        if(checkedValue.indexOf(_allValue) === -1){
          isAllCheck = true;
        }
        unCheckedBox.each(function () {
          console.log(isAllCheck, this.value);
          if(this.value === _allValue){
            this.checked = isAllCheck;
          }
        });
      }
    }
  },
  buttonOnOff : function (_buttonArea, _onButtonIdArray) {
    const buttonInfo = $('#' + _buttonArea).children('button');
    buttonInfo.each(function () {
      if(_onButtonIdArray != null && _onButtonIdArray.indexOf($(this).html()) > -1){
        $(this).removeAttr('disabled');
      } else {
        if($(this).attr('id') !== 'claimAllBtn') {
          $(this).attr('disabled', true);
        }
      }
    })
  },
  setRealGridCheckable : function (_grid, _compareFieldName) {
    // 비교를 위한 데이터 취득 용
    let compareValue = 0;
    let checkable = '';
    for(let _idx = 0; _idx < _grid.dataProvider.getRowCount(); _idx++){
      if(compareValue === _grid.dataProvider.getValue(_idx, _compareFieldName)){
        if(checkable.length !== 0){
          checkable += ' AND '
        }
        checkable += '(row <> ' + _idx + ')';
      }
      compareValue = _grid.dataProvider.getValue(_idx, _compareFieldName);
    }
    return checkable;
  },
  setRealGridArraysCheckable : function (_grid, _array, _compareFieldName) {
    // 비교를 위한 데이터 취득 용
    let compareValue = 0;
    let checkable = '';
    let notCheckedBundle = _array.join(",");
    for(let _idx = 0; _idx < _grid.dataProvider.getRowCount(); _idx++){
      let findValue = _grid.dataProvider.getValue(_idx, _compareFieldName);
      if(compareValue === findValue || notCheckedBundle.indexOf(findValue) > -1){
        if(checkable.length !== 0){
          checkable += ' AND '
        }
        checkable += '(row <> ' + _idx + ')';
      }
      compareValue = findValue;
    }
    return checkable;
  },
  setRealGridCheckBarDisabled : function (_grid) {
    // 비교를 위한 데이터 취득 용
    let checkable = '';
    for(let _idx = 0; _idx < _grid.dataProvider.getRowCount(); _idx++){
      if(checkable.length !== 0){
        checkable += ' AND '
      }
      checkable += '(row <> ' + _idx + ')';
    }
    return checkable;
  },
  getRealGridMergeData : function (_grid, _mergeColumn, _mergeData) {
    let returnMergeData = [];
    for(let _idx = 0; _idx < _grid.dataProvider.getRowCount(); _idx++){
      if(_mergeData === _grid.dataProvider.getValue(_idx, _mergeColumn)){
        returnMergeData[_idx] = _idx;
      }
    }
    return $.grep(returnMergeData, function (n) { return n === 0 || n });
  },
  getWinPopup  : function (url, _width, _height, _target) {
    // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
    const _left = ($(window).width()/2)-(_width/2);
    const _top = ($(window).height()/2)-(_height/2);
    if(_target === 'undefined' || _target == null || _target === '') {
      _target ='popup-search';
    }
    windowPopupOpen(url, _target, _width, _height, 'yes', 'no');
  },
  setRealGridMerge : function (_grid, _mergeColumns, _mergeRule, _mergeCondition) {
    for(let _mergeColumn of _mergeColumns){
      const criteria = {
        criteria : $.jUtil.isEmpty(_mergeCondition) ? "value" : "values['".concat(_mergeCondition).concat("']")
      };
      _grid.gridView.setColumnProperty(_mergeColumn, _mergeRule, criteria );
    }
  },
  setRadioCheck : function () {
    if(arguments.length > 0){
      for(const argument of arguments){
        argument.gridView.setCheckBar({"exclusive": "true", "showGroup":"true"});
      }
    }
  },
  setCheckBarHeader : function (_grid, _headerText, _headerImg) {
    for(grid of _grid){
      if(!$.jUtil.isEmpty(_headerText)){
        grid.gridView.setCheckBar({"width" : 40, "showAll" : false, "headText" : _headerText, "footText" : null, "headImageUrl" : null, "footImageUrl": null });
      } else {
        grid.gridView.setCheckBar({"showAll" : "false", "headText" : "null", "footText" : "null", "headImageUrl" : _headerImg, "footImageUrl": "null" });
      }
    }
  },
  getSplitData : function (_data, _separator) {
    return _data.split($.jUtil.isEmpty(_separator) ? (/[,\\|:_\-]/g) : _separator);
  },
  getCamelCaseSplit : function (_camelcaseStr) {
    return this.getSplitData(_camelcaseStr.replace(/([a-zA-Z0-9])(?=[A-Z])/g, '$1|'));
  },
  setWindowResize : function (_x, _y) {
    return window.resizeBy(_x, _y);
  },
  setWindowAutoResize : function () {
    const innerDivHeight = $('div:first').height();
    const width = $( document ).outerWidth(true) - $( window ).outerWidth(true);
    const height = (window.outerHeight - window.innerHeight);
    this.setWindowResize(width, - window.innerHeight);
    this.setWindowResize(width, (innerDivHeight + (height/2)));
  },
  setToggleDisplay : function (_selector, _toggle) {
    // _toggle 이 없는 경우 false
    if($.jUtil.isEmpty(_toggle) ? false : _toggle){
      _selector.show();
    } else {
      _selector.hide();
    }
  },
  setToggleOnDisplay : function () {
    if(arguments.length > 0) {
      for(const _argument of arguments) {
        if(_argument.length > 0){
          this.setToggleDisplay(_argument, true);
        }
      }
    }
  },
  setToggleOffDisplay : function () {
    if(arguments.length > 0) {
      for(const _argument of arguments) {
        if(_argument.length > 0){
          this.setToggleDisplay(_argument, false);
        }
      }
    }
  },
  writeToSlotTable : function (_slotData) {
    const frontSlotHeader = this.setDictionaryToArray(_slotData.frontSlotHeader);
    const frontSlotMap = this.setDictionaryToArray(_slotData.frontSlotMap);
    const storePickupPlaceList = this.setDictionaryToArray(_slotData.storePickupPlaceList);

    const table = $('<table>');
    const tableColGroup = $('<colgroup>');
    const thead = $('<thead>');
    const tbody = $('<tbody>');
    let tableRow = $('<tr>');

    if(frontSlotHeader.size <= 0){
      alert('배송지 SLOT 정보가 없습니다.');
      return false;
    }
    let dataChecker = 0;
    // table caption 추가
    table.append($('<caption>').html('배송SLOT 테이블'));
    // 배송시간 표시용 col을 추가한다.
    tableColGroup.append($('<col>').attr('width', '220px'));
    tableColGroup.append($('<col>').attr('span', 5).attr('width', '196px'));
    table.append(tableColGroup);

    // 헤더 설정
    for(const [key, value] of frontSlotHeader) {
      if(dataChecker++ === 0) {
        thead.append($('<th>').html('배송시간대'));
      }
      thead.append($('<th>').html(new Date() === new Date(key) ? '오늘'.concat('(').concat(value).concat(')') : key.concat('(').concat(value).concat(')')));
    }
    // 총 5일치에 대한 슬롯정보 중 모자란 경우 td로 padding
    while(dataChecker++ < 5){
      thead.append($('<td>'));
    }
    table.append(thead);
    // 주문마감/가능 여부 표시
    for(const [key, value] of frontSlotMap) {
      tableRow = $('<tr>');
      tableRow.append($('<th>').text(key).attr('scope', 'row'));
      for(const valueData of value) {
        const valueText = [
          ($.jUtil.isEmpty(valueData.placeNo) ? '주문' : '픽업'),
          valueData.slotStatus === 'END' ? '마감' : '가능'
        ];
        let td = $('<td>');
        const radioId = [
          ($.jUtil.isEmpty(valueData.placeNo) ? 'dict' : 'pick'),
          valueData.shipDt.split('-').join(''),
          valueData.shiftId,
          valueData.slotId
        ].join("_");
        if(valueText[1] === '마감') {
          td.attr('class', 'finish').text(valueText.join(''));
          tableRow.append(td);
          continue;
        }
        td.append($('<input>').attr('type', 'radio').attr('name', 'changeSlotInfo').attr('id', radioId));
        td.append('&nbsp;');
        // td.append($('<span>').attr('class', 'text').text(valueText));
        td.append($('<label>').attr('for', radioId).attr('class', 'lb-radio').text(valueText.join('')));
        tableRow.append(td);
        //<label for="drct_20200803_2502" class="lb-radio">주문가능</label>
      }
      tbody.append(tableRow);
    }
    table.append(tbody);
    return table;
  },
  setDictionaryToArray : function (_dictionary) {
    if(!$.jUtil.isEmpty(_dictionary)){
      let returnMap = new Map();
      for( const [key, value] of Object.entries(_dictionary)) {
        returnMap.set(key, value);
      }
      return returnMap;
    }
    return null;
  },
  sortByStringDesc : function (_data) {
    _data.sort(function ( a, b ) {
      return a > b ? -1 : a < b ? 1 : 0;
    });
    return _data;
  },
  sortByStringAsc : function (_data) {
    _data.sort(function ( a, b ) {
      return a < b ? -1 : a > b ? 1 : 0;
    });
    return _data;
  },
  toDateFormatting : function (_date) {
    return _date.replace(/(\d{4})(\d{2})(\d{2})/g, '$1-$2-$3');
  },
  phoneFormatter : function (num){
    if(/^[0-9_-]*$/.test(num) === false){
      alert("전화번호에는 문자가 포함될수 없습니다");
      return;
    }

    var formatNum = '';
    num = num.replace(/[^0-9]/g, '');
    if(num.length === 12){
      formatNum = num.replace(/(\d{4})(\d{4})(\d{4})/, '$1-$2-$3');
    }
    else if(num.length==8){
      formatNum = num.replace(/(\d{4})(\d{4})/, '$1-$2');
    }else{
      if(num.indexOf('02')==0){
        formatNum = num.replace(/(\d{2})(\d{3,4})(\d{4})/, '$1-$2-$3');
      }else{
        formatNum = num.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
      }
    }
    return formatNum;
  },
  /**
   * 일반 전화번호 체크
   * barAllowFlag (true/false) : '-' 허용 여부
   **/
  chkTelNum : function(str, barAllowFlag) {
    var regExp;

    if (barAllowFlag) {
      regExp = /^[0-9]{2,4}-[0-9]{3,4}-[0-9]{4}$/;
    } else {
      regExp = /^[0-9]{2,4}[0-9]{3,4}[0-9]{4}$/;
    }
    return regExp.test(str);
  },

  /**
   * 말일 체크
   */
  isLastDay : function () {
    var now = new Date();
    var lastDate = new Date(now.getFullYear(),now.getMonth()+1,0);

    var nowD = now.getDate();
    var lastD = lastDate.getDate();

    return nowD == lastD;
  }
}