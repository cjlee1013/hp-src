/** Main Script */
var rentalManageMain = {
    /**
     * init 이벤트
     */
    init: function() {
        rentalManageMain.setBoardCount();
        rentalManageMain.bindingEvent();
        sellUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $main = rentalManageMain.getRoot();

        // 새로고침 버튼
        $main.find("#boardRefresh").on("click", function() { rentalManageMain.setBoardCount() });
        // 보드 카운트 클릭 조회
        $main.find("#callCnt, #reservationCnt, #completeCnt").on("click", function() { rentalManageMain.searchBoard($(this)) });
        // 검색 버튼
        $main.find("#schBtn").bindClick(rentalManageMain.search);
        // 초기화 버튼
        $main.find("#schResetBtn").bindClick(rentalManageMain.reset, "rentalManageSearchForm");
        // 엑셀다운 버튼
        $main.find("#schExcelDownloadBtn").bindClick(rentalManageMain.excelDownload);

        // 달력 - [오늘] 버튼
        $main.find("#setTodayBtn").on("click", () => {sellUtil.initSearchDateCustom("schStartDt", "schEndDt","0", "0")});
        // 달력 - [1주일] 버튼
        $main.find("#setOneWeekBtn").on("click", () => {sellUtil.initSearchDateCustom("schStartDt", "schEndDt","-1w", "0")});
        // 달력 - [1개월] 버튼
        $main.find("#setOneMonthBtn").on("click", () => {sellUtil.initSearchDateCustom("schStartDt", "schEndDt","-1m", "0")});

        // 상담예약 버튼
        $main.find("#counselReservation").bindClick(rentalManageMain.setCounselReservation);
        // 상담완료 버튼
        $main.find("#counselComplete").bindClick(rentalManageMain.setCounselComplete);
        // 주문취소 버튼
        $main.find("#orderCancel").bindClick(rentalManageMain.setOrderCancel);
    },

    /**
     * Board 카운트 설정
     */
    setBoardCount: function() {
        var $main = rentalManageMain.getRoot();

        CommonAjax.basic({
            url         : '/sell/rentalManage/getRentalManageCnt.json',
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                rentalManageMain.mapEach(res);
            }
        });
    },

    /**
     * Board 데이터 검색
     */
    searchBoard: function($this) {
        var $main = rentalManageMain.getRoot();
        var schType = $this.attr('id');

        CommonAjax.basic({
            url         : '/sell/rentalManage/getRentalManageCntList.json?schType=' + schType,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                rentalManageGrid.setData(res);
                $main.find('#rentalManageSearchCnt').html($.jUtil.comma(rentalManageGrid.gridView.getItemCount()));
            },
            errorCallbackFunc: function () {
                alert("렌탈판매관리 카운트 리스트 조회 실패");
            }
        });
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var $main = rentalManageMain.getRoot();

        if (!validCheck.search()) {
            return;
        }

        var reqData = $main.find("#rentalManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/sell/rentalManage/getRentalManageList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                rentalManageGrid.setData(res);
                $main.find('#rentalManageSearchCnt').html($.jUtil.comma(rentalManageGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 상담예약
     */
    setCounselReservation : function() {
        var checkRowIds = rentalManageGrid.gridView.getCheckedRows();
        var reqData = new Object();
        var shipNoList = new Array();
        var reqCnt = 0;

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        for (var checkRowId of checkRowIds) {
            var checkedRows = rentalManageGrid.dataProvider.getJsonRow(checkRowId);

            if (checkedRows.shipStatus != 'D1') {
                alert("상담요청 건만 진행할 수 있습니다.");
                return;
            }

            shipNoList.push(checkedRows.shipNo);
        }

        if (shipNoList.length === 0) {
            alert("상담예약 가능한 건이 없습니다. 주문상태를 확인해 주세요.");
            return;
        }

        reqData.shipNoList = shipNoList;
        reqCnt = shipNoList.length;

        if (confirm("선택하신 주문건에 대해서 상담예약을 진행하겠습니까?")) {
            CommonAjax.basic({
                url         : '/sell/rentalManage/setCounselReservation.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    var failCnt = parseInt(reqCnt) - parseInt(res.data);

                    if (res.returnCode != "SUCCESS" || failCnt > 0) {
                        alert("상담예약 처리 중 "+failCnt+"건 실패했습니다.");
                    } else {
                        alert("상담예약을 했습니다.");
                    }

                    rentalManageMain.callback();
                }
            });
        }
    },

    /**
     * 상담완료
     */
    setCounselComplete : function() {
        var checkRowIds = rentalManageGrid.gridView.getCheckedRows();
        var reqData = new Object();
        var shipNoList = new Array();
        var reqCnt = 0;

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        for (var checkRowId of checkRowIds) {
            var checkedRows = rentalManageGrid.dataProvider.getJsonRow(checkRowId);

            if (checkedRows.shipStatus != 'D3') {
                alert("상담예약 건만 진행할 수 있습니다.");
                return;
            }

            shipNoList.push(checkedRows.shipNo);
        }

        if (shipNoList.length === 0) {
            alert("상담완료 가능한 건이 없습니다. 주문상태를 확인해 주세요.");
            return;
        }

        reqData.shipNoList = shipNoList;
        reqCnt = shipNoList.length;

        if (confirm("선택하신 상담건으로 완료처리하시겠습니까?\n완료된 건은 주문취소가 불가하며 상태값을 되돌릴 수 없습니다.")) {
            CommonAjax.basic({
                url         : '/sell/rentalManage/setCounselComplete.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    var failCnt = parseInt(reqCnt) - parseInt(res.data);

                    if (res.returnCode != "SUCCESS" || failCnt > 0) {
                        alert("상담완료 처리 중 "+failCnt+"건 실패했습니다.");
                    } else {
                        alert("상담완료을 했습니다.");
                    }

                    rentalManageMain.callback();
                }
            });
        }
    },

    /**
     * 주문취소
     */
    setOrderCancel : function() {

        var checkRowIds = rentalManageGrid.gridView.getCheckedRows();

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }
        multiCancelPop.openModal();
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (rentalManageGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }
        var confirmMsg = "구매자의 개인 정보는 암호화 하여야 하며, 판매 목적을 달성한 경우 즉시 파기 되어야 합니다.\n"
            + "구매자의 개인정보를 이용하여 광고 홍보등의 수집 목적 외로 사용해서는 안됩니다.\n"
            + "상기 사항 등 관계 법령 위반으로 발생하는 모든 민,형사상 책임은 판매자 본인에게 있습니다.\n\n"
            + "상기 내용을 숙지하였으며, 이에 동의합니다.";
        if (confirm(confirmMsg)) {
            var _date = new Date();
            var fileName = "렌탈판매관리_" + _date.getFullYear() + (_date.getMonth()
                + 1) + _date.getDate();
            rentalManageGrid.gridView.exportGrid({
                type: "excel",
                target: "local",
                fileName: fileName + ".xlsx",
                showProgress: true,
                progressMessage: "엑셀 Export 중 입니다."
            });
        }
    },

    /**
     * mapData key로 id매핑
     */
    mapEach: function (map) {
        var $main = rentalManageMain.getRoot();

        $.each(map,function(key, value){
            $main.find('#'+key).html(value);
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();
        sellUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");
    },

    /**
     * root selector
     */
    getRoot: function() {
        return $('#rentalManageMain');
    },

    /**
     * callback function
     */
    callback : function () {
        rentalManageMain.search();
        rentalManageMain.setBoardCount();
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {
        var $main = rentalManageMain.getRoot();

        //  검색조건 (상품주문번호, 주문번호)
        //  선택 후 검색어 입력시, 다른 검색조건 무시
        var schKeywordVal = $main.find("#schKeyword").val();
        var schKeywordTypeVal = $main.find("#schKeywordType").val();

        if (!$.jUtil.isEmpty(schKeywordVal)
            && (schKeywordTypeVal == 'orderItemNo'
                || schKeywordTypeVal == 'purchaseOrderNo')) {
            return true;
        }

        // 검색기간은 최대 3개월
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            //sellUtil.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
            return false;
        }

        if (schEndDt.diff(schStartDt, "month", true) > 3) {
            alert("3개월 이내만 검색 가능합니다.");
            //$schStartDt.val(schEndDt.add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        return true;
    }
}

/** Grid Script */
var rentalManageGrid = {
    gridView: new RealGridJS.GridView("rentalManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        rentalManageGrid.initGrid();
        rentalManageGrid.initDataProvider();
        rentalManageGrid.event();
    },
    initGrid: function () {
        rentalManageGrid.gridView.setDataSource(rentalManageGrid.dataProvider);
        rentalManageGrid.gridView.setStyles(rentalManageGridBaseInfo.realgrid.styles);
        rentalManageGrid.gridView.setDisplayOptions(rentalManageGridBaseInfo.realgrid.displayOptions);
        rentalManageGrid.gridView.setColumns(rentalManageGridBaseInfo.realgrid.columns);
        rentalManageGrid.gridView.setOptions(rentalManageGridBaseInfo.realgrid.options);

        // 상품번호 렌더링
        rentalManageGrid.gridView.setColumnProperty("itemNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "itemNo", showUrl: false});
    },
    initDataProvider: function () {
        rentalManageGrid.dataProvider.setFields(rentalManageGridBaseInfo.dataProvider.fields);
        rentalManageGrid.dataProvider.setOptions(rentalManageGridBaseInfo.dataProvider.options);
    },
    event: function() {
        rentalManageGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var itemNo = grid.getValue(index.itemIndex, "itemNo");
            var siteType = grid.getValue(index.itemIndex, "siteType");

            // 상품상세 이동
            if (index.fieldName == "itemNo") {
                var frontUrl = siteType == "CLUB" ? theclubFrontUrl : homeplusFrontUrl;
                window.open(frontUrl + "/item?itemNo=" + itemNo + "&storeType=DS");
            }

        };
    },
    setData: function (dataList) {
        rentalManageGrid.dataProvider.clearRows();
        rentalManageGrid.dataProvider.setRows(dataList);

        //페이징 처리
        sellUtil.onRealGridPaging(rentalManageGrid.gridView, "pagingArea", $('#pageViewCnt').val(), 10);
    }
};