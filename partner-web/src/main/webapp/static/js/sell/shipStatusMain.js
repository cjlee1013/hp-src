/** Main Script */
var shipStatusMain = {
    /**
     * init 이벤트
     */
    init: function() {
        shipStatusMain.setBoardCount();
        shipStatusMain.bindingEvent();
        sellUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");

        // 파라미터로 schType 있으면 해당 보드 카운트 클릭
        if (!$.jUtil.isEmpty(schType)) {
            $("#"+schType).trigger("click");
        }
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $main = shipStatusMain.getRoot();

        // 새로고침 버튼
        $main.find("#boardRefresh").on("click", function() { shipStatusMain.setBoardCount() });
        // 보드 카운트 클릭 조회
        $main.find("#shippingCnt, #completeCnt, #noReceiveCnt").on("click", function() { shipStatusMain.searchBoard($(this)) });
        // 검색 버튼
        $main.find("#schBtn").bindClick(shipStatusMain.search);
        // 초기화 버튼
        $main.find("#schResetBtn").bindClick(shipStatusMain.reset, "shipStatusSearchForm");
        // 엑셀다운 버튼
        $main.find("#schExcelDownloadBtn").bindClick(shipStatusMain.excelDownload);

        // 달력 - [오늘] 버튼
        $main.find("#setTodayBtn").on("click", () => {sellUtil.initSearchDateCustom("schStartDt", "schEndDt","0", "0")});
        // 달력 - [1주일] 버튼
        $main.find("#setOneWeekBtn").on("click", () => {sellUtil.initSearchDateCustom("schStartDt", "schEndDt","-1w", "0")});
        // 달력 - [1개월] 버튼
        $main.find("#setOneMonthBtn").on("click", () => {sellUtil.initSearchDateCustom("schStartDt", "schEndDt","-1m", "0")});

        // 배송방법/송장수정 버튼
        $main.find("#editShipInfo").bindClick(shipStatusMain.openEditShipInfoPopup);
        // 미수취 신고 철회요청 버튼
        $main.find("#noReceiveCancel").bindClick(shipStatusMain.openNoReceivePopup);
        // 배송완료 버튼
        $main.find("#shipComplete").bindClick(shipStatusMain.openShipCompletePopup);

        // 반품신청 버튼
        $('#claimReturn').bindClick(() => { shipStatusMain.openClaimPopup('R')});

        // 교환신청 버튼
        $("#claimExchange").bindClick(() => { shipStatusMain.openClaimPopup('X')});
    },

    /**
     * Board 카운트 설정
     */
    setBoardCount: function() {
        var $main = shipStatusMain.getRoot();

        CommonAjax.basic({
            url         : '/sell/shipStatus/getShipStatusCnt.json',
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                shipStatusMain.mapEach(res);
            }
        });
    },

    /**
     * Board 데이터 검색
     */
    searchBoard: function($this) {
        var $main = shipStatusMain.getRoot();
        var schType = $this.attr('id');

        CommonAjax.basic({
            url         : '/sell/shipStatus/getShipStatusCntList.json?schType=' + schType,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                shipStatusGrid.setData(res);
                $main.find('#shipStatusSearchCnt').html($.jUtil.comma(shipStatusGrid.gridView.getItemCount()));
            },
            errorCallbackFunc: function () {
                alert("배송현황 카운트 리스트 조회 실패");
            }
        });
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var $main = shipStatusMain.getRoot();

        if (!validCheck.search()) {
            return;
        }

        var reqData = $main.find("#shipStatusSearchForm").serialize();
        CommonAjax.basic({
            url         : '/sell/shipStatus/getShipStatusList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                shipStatusGrid.setData(res);
                $main.find('#shipStatusSearchCnt').html($.jUtil.comma(shipStatusGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     *  배송방법/송장수정 팝업창 open
     */
    openEditShipInfoPopup: function() {
        var checkRowIds = shipStatusGrid.gridView.getCheckedRows();

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        if (checkRowIds.length > 1 ) {
            alert("1개의 배송건만 선택해주세요.");
            return;
        }

        var checkedRows = shipStatusGrid.dataProvider.getJsonRow(checkRowIds[0]);

        if (checkedRows.shipStatus != 'D2' && checkedRows.shipStatus != 'D3' && checkedRows.shipStatus != 'D4') {
            alert("상품준비중/배송중/배송완료 건만 진행할 수 있습니다.");
            return;
        }

        shipAllPop.openModal();
    },

    /**
     * 미수취 신고 철회요청 팝업창 open
     */
    openNoReceivePopup: function() {
        var checkRowIds = shipStatusGrid.gridView.getCheckedRows();

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        if (checkRowIds.length > 1) {
            alert("1건의 주문만 선택해 주세요.");
            return;
        }

        var noRcvDeclrDt = shipStatusGrid.dataProvider.getValue(checkRowIds[0],"noRcvDeclrDt");
        if ($.jUtil.isEmpty(noRcvDeclrDt)) {
            alert("미수취 신고 철회 요청이 불가능한 건입니다.");
            return;
        }

        var noRcvProcessType = shipStatusGrid.dataProvider.getValue(checkRowIds[0],"noRcvProcessType");
        if (!$.jUtil.isEmpty(noRcvProcessType)) {
            alert("미수취 신고 철회 요청이 불가능한 건입니다.");
            return;
        }

        noReceivePop.openModal();
    },

    /**
     * 배송완료 팝업창 open
     */
    openShipCompletePopup: function() {
        var checkRowIds = shipStatusGrid.gridView.getCheckedRows();

        // samsung 말일 배송완료 금지
        if (parnterId == "samsung" && sellUtil.isLastDay()) {
            alert("매월 말일은 배송완료 처리가 불가합니다.\n다음 달 1일 이후 완료처리 진행 부탁드립니다.");
            return;
        }

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        for (var checkRowId of checkRowIds) {
            var checkedRows = shipStatusGrid.dataProvider.getJsonRow(checkRowId);

            if (checkedRows.shipStatus != 'D3') {
                alert("배송중인 건만 진행할 수 있습니다.");
                return;
            }

            if (checkedRows.shipMethod == 'DS_DLV') {
                alert("택배배송은 완료할 수 없습니다.");
                return;
            }

        }

        shipCompletePop.openModal();
    },

    /**
     * 반품신청 팝업창 open
     */
    openClaimPopup: function(claimType) {

        var checkRowIds = shipStatusGrid.gridView.getCheckedRows();

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        if (checkRowIds.length > 1) {
            alert("1개의 주문을 선택해주세요.");
            return;
        }

        for (var checkRowId of checkRowIds) {
            var checkedRows = shipStatusGrid.dataProvider.getJsonRow(checkRowId);
            console.log(checkedRows.claimYn);
            if (checkedRows.claimYn == 'N') {
                alert("구매확정 90일이 지난 건은 교환/반품이 불가합니다 .");
                return;
            }
        }

        orderClaimPop.openModal(claimType);
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (shipStatusGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var confirmMsg = "구매자의 개인 정보는 암호화 하여야 하며, 판매 목적을 달성한 경우 즉시 파기 되어야 합니다.\n"
            + "구매자의 개인정보를 이용하여 광고 홍보등의 수집 목적 외로 사용해서는 안됩니다.\n"
            + "상기 사항 등 관계 법령 위반으로 발생하는 모든 민,형사상 책임은 판매자 본인에게 있습니다.\n\n"
            + "상기 내용을 숙지하였으며, 이에 동의합니다.";
        if (confirm(confirmMsg)) {
            var _date = new Date();
            var fileName = "배송현황_" + _date.getFullYear() + (_date.getMonth()
                + 1) + _date.getDate();
            shipStatusGrid.gridView.exportGrid({
                type: "excel",
                target: "local",
                fileName: fileName + ".xlsx",
                showProgress: true,
                progressMessage: "엑셀 Export 중 입니다."
            });
        }
    },

    /**
     * mapData key로 id매핑
     */
    mapEach: function (map) {
        var $main = shipStatusMain.getRoot();

        $.each(map,function(key, value){
            $main.find('#'+key).html(value);
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();

        sellUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");
    },

    /**
     * root selector
     */
    getRoot: function() {
        return $('#shipStatusMain');
    },

    /**
     * callback function
     */
    callback : function () {
        shipStatusMain.search();
        shipStatusMain.setBoardCount();
    },

    /**
     * 반품신청 우편번호 검색 후 값 세팅
     * **/
    orderReturnSetZipcode : function(res){
        orderClaimPop.orderReturnSetZipcode(res);
    },

    /**
     * 반품신청 우편번호 검색 후 값 세팅
     * **/
    orderExchangeSetZipcode : function(res){
        orderClaimPop.orderExchangeSetZipcode(res);
    },
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {
        var $main = shipStatusMain.getRoot();

        //  검색조건 (상품주문번호, 주문번호)
        //  선택 후 검색어 입력시, 다른 검색조건 무시
        var schKeywordVal = $main.find("#schKeyword").val();
        var schKeywordTypeVal = $main.find("#schKeywordType").val();

        if (!$.jUtil.isEmpty(schKeywordVal)
            && (schKeywordTypeVal == 'orderItemNo'
                || schKeywordTypeVal == 'purchaseOrderNo'
                || schKeywordTypeVal == 'bundleNo')) {
            return true;
        }

        // 검색기간은 최대 3개월
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            //sellUtil.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
            return false;
        }

        if (schEndDt.diff(schStartDt, "month", true) > 3) {
            alert("3개월 이내만 검색 가능합니다.");
            //$schStartDt.val(schEndDt.add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        return true;
    }
}

/** Grid Script */
var shipStatusGrid = {
    gridView: new RealGridJS.GridView("shipStatusGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        shipStatusGrid.initGrid();
        shipStatusGrid.initDataProvider();
        shipStatusGrid.event();
    },
    initGrid: function () {
        shipStatusGrid.gridView.setDataSource(shipStatusGrid.dataProvider);
        shipStatusGrid.gridView.setStyles(shipStatusGridBaseInfo.realgrid.styles);
        shipStatusGrid.gridView.setDisplayOptions(shipStatusGridBaseInfo.realgrid.displayOptions);
        shipStatusGrid.gridView.setColumns(shipStatusGridBaseInfo.realgrid.columns);
        shipStatusGrid.gridView.setOptions(shipStatusGridBaseInfo.realgrid.options);

        var sameDataMerge = ['purchaseOrderNo','bundleNo'];
        var valueSameMerge = [ 'bundleNo', 'orderItemNo'];
        sellUtil.setRealGridMerge(shipStatusGrid, sameDataMerge, "mergeRule");
        sellUtil.setRealGridMerge(shipStatusGrid, valueSameMerge, "mergeRule");

        shipStatusGrid.gridView.setColumnProperty("bundleNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "bundleNo", showUrl: false});
        shipStatusGrid.gridView.setColumnProperty("noRcvDeclrDt", "renderer", {type: "link", url: "unnecessary", requiredFields: "noRcvDeclrDt", showUrl: false});
        shipStatusGrid.gridView.setColumnProperty("invoiceNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "invoiceNo", showUrl: false});
        shipStatusGrid.gridView.setColumnProperty("itemNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "itemNo", showUrl: false});

        //sellUtil.setCheckBarHeader([shipStatusGrid], "선택", null);
    },
    initDataProvider: function () {
        shipStatusGrid.dataProvider.setFields(shipStatusGridBaseInfo.dataProvider.fields);
        shipStatusGrid.dataProvider.setOptions(shipStatusGridBaseInfo.dataProvider.options);
    },
    event: function() {
        shipStatusGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var purchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");
            var completeAfter3MonthYn = grid.getValue(index.itemIndex, "completeAfter3MonthYn");
            var noRcvDeclrDt = grid.getValue(index.itemIndex, "noRcvDeclrDt");
            var invoiceNo = grid.getValue(index.itemIndex, "invoiceNo");
            var itemNo = grid.getValue(index.itemIndex, "itemNo");
            var siteType = grid.getValue(index.itemIndex, "siteType");

            if (index.fieldName == "bundleNo" && completeAfter3MonthYn == "N") {
                itemClaimDetailPop.openModal(0);
            } else if (index.fieldName == "noRcvDeclrDt" && !$.jUtil.isEmpty(noRcvDeclrDt)) {
                noReceiveInfoPop.openModal();
            } else if (index.fieldName == "invoiceNo" && !$.jUtil.isEmpty(invoiceNo)) {
                shipHistoryPop.openModal();
            } else if (index.fieldName == "itemNo") {
                var frontUrl = siteType == "CLUB" ? theclubFrontUrl : homeplusFrontUrl;
                window.open(frontUrl + "/item?itemNo=" + itemNo + "&storeType=DS");
            }
        };
    },
    setData: function (dataList) {
        shipStatusGrid.dataProvider.clearRows();
        shipStatusGrid.dataProvider.setRows(dataList);

        var bundleCheckArray = [];

        shipStatusGrid.gridView.setCheckableExpression(
            sellUtil.setRealGridArraysCheckable(shipStatusGrid, bundleCheckArray, 'bundleNo'), true);

        //페이징 처리
        sellUtil.onRealGridPaging(shipStatusGrid.gridView, "pagingArea", $('#pageViewCnt').val(), 10);
    }
};