/** Main Script */
var orderShipManageMain = {
    /**
     * init 이벤트
     */
    init: function() {
        orderShipManageMain.setBoardCount();
        orderShipManageMain.bindingEvent();
        sellUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");

        // 파라미터로 schType 있으면 해당 보드 카운트 클릭
        if (!$.jUtil.isEmpty(schType)) {
            $("#"+schType).trigger("click");
        }
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $main = orderShipManageMain.getRoot();

        // 새로고침 버튼
        $main.find("#boardRefresh").on("click", function() { orderShipManageMain.setBoardCount() });
        // 보드 카운트 클릭 조회
        $main.find("#newCnt, #readyCnt, #newDelayCnt, #shipDelayCnt, #shipDelayNotiCnt, #shipTodayCnt, #claimCnt").on("click", function() { orderShipManageMain.searchBoard($(this)) });
        // 검색 버튼
        $main.find("#schBtn").bindClick(orderShipManageMain.search);
        // 초기화 버튼
        $main.find("#schResetBtn").bindClick(orderShipManageMain.reset, "orderShipManageSearchForm");
        // 엑셀다운 버튼
        $main.find("#schExcelDownloadBtn").bindClick(orderShipManageMain.excelDownload);

        // 달력 - [오늘] 버튼
        $main.find("#setTodayBtn").on("click", () => {sellUtil.initSearchDateCustom("schStartDt", "schEndDt","0", "0")});
        // 달력 - [1주일] 버튼
        $main.find("#setOneWeekBtn").on("click", () => {sellUtil.initSearchDateCustom("schStartDt", "schEndDt","-1w", "0")});
        // 달력 - [1개월] 버튼
        $main.find("#setOneMonthBtn").on("click", () => {sellUtil.initSearchDateCustom("schStartDt", "schEndDt","-1m", "0")});

        // 주문확인 버튼
        $main.find("#confirmOrder").bindClick(orderShipManageMain.setConfirmOrder);
        // 발송지연안내 버튼
        $main.find("#notiDelay").bindClick(orderShipManageMain.openNotiDelayPopup);
        // 선택발송처리 버튼
        $main.find("#shipAll").bindClick(orderShipManageMain.openShipAllPopup);
        // 일괄발송처리 버튼
        $main.find("#shipAllExcel").bindClick(orderShipManageMain.openShipAllExcelPopup);
        // 배송지수정 버튼
        $main.find("#openShipAddr").bindClick(orderShipManageMain.openShipAddr);
        // 품절주문취소 버튼
        $main.find("#openClaim").bindClick(orderShipManageMain.openClaim);
    },

    /**
     * Board 카운트 설정
     */
    setBoardCount: function() {
        var $main = orderShipManageMain.getRoot();

        CommonAjax.basic({
            url         : '/sell/orderShipManage/getOrderShipManageCnt.json',
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                orderShipManageMain.mapEach(res);
            }
        });
    },

    /**
     * Board 데이터 검색
     */
    searchBoard: function($this) {
        var $main = orderShipManageMain.getRoot();
        var schType = $this.attr('id');

        if(schType == "claimCnt") {
            sellUtil.setLinkTab("Tab_cancelMain", "취소관리","/claim/claimMain?claimType=C&schStatus=");
        } else {
            CommonAjax.basic({
                url         : '/sell/orderShipManage/getOrderShipManageCntList.json?schType=' + schType,
                data        : null,
                method      : "GET",
                callbackFunc: function(res) {
                    orderShipManageGrid.setData(res);
                    $main.find('#orderShipManageSearchCnt').html($.jUtil.comma(orderShipManageGrid.gridView.getItemCount()));
                },
                errorCallbackFunc: function () {
                    alert("발주발송관리 카운트 리스트 조회 실패");
                }
            });
        }
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var $main = orderShipManageMain.getRoot();

        if (!validCheck.search()) {
            return;
        }

        var reqData = $main.find("#orderShipManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/sell/orderShipManage/getOrderShipManageList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                orderShipManageGrid.setData(res);
                $main.find('#orderShipManageSearchCnt').html($.jUtil.comma(orderShipManageGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 주문확인
     */
    setConfirmOrder: function () {
        var checkRowIds = orderShipManageGrid.gridView.getCheckedRows();
        var reqData = new Object();
        var reqCnt = 0;
        var bundleNoList = new Array();

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        for (var checkRowId of checkRowIds) {
            var checkedRows = orderShipManageGrid.dataProvider.getJsonRow(checkRowId);

            if (checkedRows.shipStatus != 'D1') {
                alert("신규주문 건만 진행할 수 있습니다.");
                return;
            }

            bundleNoList.push(checkedRows.bundleNo);
        }

        if (bundleNoList.length === 0) {
            alert("주문확인 가능한 건이 없습니다. 주문상태를 확인해 주세요.");
            return;
        }

        // 중복 제거
        bundleNoList = $.unique(bundleNoList);

        reqData.bundleNoList = bundleNoList;
        reqCnt = bundleNoList.length;
        if (confirm("선택하신 주문건에 대해서 주문확인을 진행하겠습니까?")) {
            CommonAjax.basic({
                url         : '/sell/orderShipManage/setConfirmOrder.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    var failCnt = parseInt(reqCnt) - parseInt(res.data);

                    if (res.returnCode != "SUCCESS" || failCnt > 0) {
                        alert("주문확인 처리 중 "+failCnt+"건 실패했습니다.");
                    } else {
                        alert("주문확인을 했습니다.");
                    }

                    orderShipManageMain.callback();
                }
            });
        }
    },

    /**
     * 발송지연안내 팝업창 open
     */
    openNotiDelayPopup: function() {
        var checkRowIds = orderShipManageGrid.gridView.getCheckedRows();
        var bundleNoList = new Array();

        for (var idx in checkRowIds) {
            var checkedRows = orderShipManageGrid.dataProvider.getJsonRow(checkRowIds[idx]);

            bundleNoList.push(checkedRows.bundleNo);
        }

        // 중복 제거
        bundleNoList = $.unique(bundleNoList);

        if (bundleNoList.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        if (bundleNoList.length > 1) {
            alert("동일 배송번호만 가능합니다.");
            return;
        }

        var bundleNo = bundleNoList[0];

        for (var idx in checkRowIds) {
            var checkedRows = orderShipManageGrid.dataProvider.getJsonRow(checkRowIds[idx]);

            if (bundleNo == checkedRows.bundleNo) {
                if (checkedRows.delayShipDt != null) {
                    alert("발송지연 안내는 1회만 가능합니다.");
                    return;
                } else if (checkedRows.shipStatus != 'D1' && checkedRows.shipStatus != 'D2') {
                    alert("신규주문/상품준비중 건만 진행할 수 있습니다.");
                    return;
                }
            }
        }

        notiDealyPop.openModal();
    },

    /**
     * 일괄발송처리 팝업창 open
     */
    openShipAllExcelPopup: function() {
        shipAllExcelPop.openModal();
    },

    /**
     * 선택발송처리 팝업창 open
     */
    openShipAllPopup: function() {
        var checkRowIds = orderShipManageGrid.gridView.getCheckedRows();

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        var bundleNo = orderShipManageGrid.dataProvider.getValue(checkRowIds[0],"bundleNo");

        for (var checkRowId of checkRowIds) {
            var checkedRows = orderShipManageGrid.dataProvider.getJsonRow(checkRowId);

            if (checkedRows.shipStatus != 'D2') {
                alert("상품준비중 건만 진행할 수 있습니다.");
                return;
            }

            if (bundleNo != checkedRows.bundleNo) {
                alert("배송번호가 동일한 건을 선택해 주세요.");
                return;
            }
        }

        shipAllPop.openModal();
    },

    /**
     * 배송지 수정 팝업창 open
     */
    openShipAddr: function() {
        var checkRowIds = orderShipManageGrid.gridView.getCheckedRows();
        var bundleNoList = new Array();

        for (var idx in checkRowIds) {
            var checkedRows = orderShipManageGrid.dataProvider.getJsonRow(checkRowIds[idx]);

            bundleNoList.push(checkedRows.bundleNo);
        }

        // 중복 제거
        bundleNoList = $.unique(bundleNoList);

        if (checkRowIds.length === 0) {
            alert("주문을 선택해 주세요.");
            return;
        }

        if (bundleNoList.length > 1) {
            alert("동일 배송건을 선택하세요.");
            return;
        }

        shipAddrPop.openModal();
    },

    /**
     * 품절 주문취소 팝업창 open
     */
    openClaim: function() {

        var checkRowIds = orderShipManageGrid.gridView.getCheckedRows();

        if (checkRowIds.length === 0) {
            alert("주문을 선택해 주세요.");
            return;
        }

        multiCancelPop.openModal();
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (orderShipManageGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var confirmMsg = "구매자의 개인 정보는 암호화 하여야 하며, 판매 목적을 달성한 경우 즉시 파기 되어야 합니다.\n"
            + "구매자의 개인정보를 이용하여 광고 홍보등의 수집 목적 외로 사용해서는 안됩니다.\n"
            + "상기 사항 등 관계 법령 위반으로 발생하는 모든 민,형사상 책임은 판매자 본인에게 있습니다.\n\n"
            + "상기 내용을 숙지하였으며, 이에 동의합니다.";
        if (confirm(confirmMsg)) {
            var _date = new Date();
            var fileName = "발주발송관리_" + _date.getFullYear() + (_date.getMonth()
                + 1) + _date.getDate();
            orderShipManageGrid.gridView.exportGrid({
                type: "excel",
                target: "local",
                fileName: fileName + ".xlsx",
                showProgress: true,
                progressMessage: "엑셀 Export 중 입니다."
            });
        }
    },

    /**
     * mapData key로 id매핑
     */
    mapEach: function (map) {
        var $main = orderShipManageMain.getRoot();

        $.each(map,function(key, value){
            $main.find('#'+key).html(value);
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();

        sellUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");
    },

    /**
     * root selector
     */
    getRoot: function() {
        return $('#orderShipManageMain');
    },

    /**
     * callback function
     */
    callback : function () {
        orderShipManageMain.search();
        orderShipManageMain.setBoardCount();
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {
        var $main = orderShipManageMain.getRoot();

        //  검색조건 (상품주문번호, 주문번호, 배송번호)
        //  선택 후 검색어 입력시, 다른 검색조건 무시
        var schKeywordVal = $main.find("#schKeyword").val();
        var schKeywordTypeVal = $main.find("#schKeywordType").val();

        if (!$.jUtil.isEmpty(schKeywordVal)
            && (schKeywordTypeVal == 'orderItemNo'
                || schKeywordTypeVal == 'purchaseOrderNo'
                || schKeywordTypeVal == 'bundleNo')) {
            return true;
        }

        // 검색기간은 최대 3개월
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            //sellUtil.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
            return false;
        }

        if (schEndDt.diff(schStartDt, "month", true) > 3) {
            alert("3개월 이내만 검색 가능합니다.");
            //$schStartDt.val(schEndDt.add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        return true;
    }
}

/** Grid Script */
var orderShipManageGrid = {
    gridView: new RealGridJS.GridView("orderShipManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        orderShipManageGrid.initGrid();
        orderShipManageGrid.initDataProvider();
        orderShipManageGrid.event();
    },
    initGrid: function () {
        orderShipManageGrid.gridView.setDataSource(orderShipManageGrid.dataProvider);
        orderShipManageGrid.gridView.setStyles(orderShipManageGridBaseInfo.realgrid.styles);
        orderShipManageGrid.gridView.setDisplayOptions(orderShipManageGridBaseInfo.realgrid.displayOptions);
        orderShipManageGrid.gridView.setColumns(orderShipManageGridBaseInfo.realgrid.columns);
        orderShipManageGrid.gridView.setOptions(orderShipManageGridBaseInfo.realgrid.options);

        var sameDataMerge = ['purchaseOrderNo','bundleNo'];
        var valueSameMerge = [ 'bundleNo', 'orderItemNo'];
        sellUtil.setRealGridMerge(orderShipManageGrid, sameDataMerge, "mergeRule");
        sellUtil.setRealGridMerge(orderShipManageGrid, valueSameMerge, "mergeRule");

        orderShipManageGrid.gridView.setColumnProperty("bundleNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "bundleNo", showUrl: false});
        orderShipManageGrid.gridView.setColumnProperty("itemNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "itemNo", showUrl: false});

        //sellUtil.setCheckBarHeader([orderShipManageGrid], "선택", null);
    },
    initDataProvider: function () {
        orderShipManageGrid.dataProvider.setFields(orderShipManageGridBaseInfo.dataProvider.fields);
        orderShipManageGrid.dataProvider.setOptions(orderShipManageGridBaseInfo.dataProvider.options);
    },
    event: function() {
        orderShipManageGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var purchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");
            var itemNo = grid.getValue(index.itemIndex, "itemNo");
            var siteType = grid.getValue(index.itemIndex, "siteType");

            if (index.fieldName == "bundleNo") {
                itemClaimDetailPop.openModal(0);
            } else if (index.fieldName == "itemNo") {
                var frontUrl = siteType == "CLUB" ? theclubFrontUrl : homeplusFrontUrl;
                window.open(frontUrl + "/item?itemNo=" + itemNo + "&storeType=DS");
            }
        };
    },
    setData: function (dataList) {
        orderShipManageGrid.dataProvider.clearRows();
        orderShipManageGrid.dataProvider.setRows(dataList);
        var bundleCheckArray = [];

        //orderShipManageGrid.gridView.setCheckableExpression(
        //    sellUtil.setRealGridArraysCheckable(orderShipManageGrid, bundleCheckArray, 'bundleNo'), true);

        //페이징 처리
        sellUtil.onRealGridPaging(orderShipManageGrid.gridView, "pagingArea", $('#pageViewCnt').val(), 10);
    }
};