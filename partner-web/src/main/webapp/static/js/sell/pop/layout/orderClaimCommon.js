/**
 * 주문관리 > 취소/교환/환불 상품 정보 리스트 -> 취소/환불/교환 팝업 환불예정금액 조회 layout
 */

// 환불정보
var orderClaimCommon = {

    /**
     * 환불예정금액 파라미터 생성
     */
    setRefundAmtParam: function ($modal, cancelType, whoReason) {

        $('input[id$="_totalReqOptQty"]').val(0);

        var claimPreRefundItemList = [];
        var claimPreRefundItem;

        $('#itemInfoTable tbody tr').each(function () {
            var orderQty = $(this).find("select").eq(0).val();
            if (orderQty > 0) {
                claimPreRefundItem = {
                    "itemNo": $(this).find("td").eq(0).html(),      //상품번호
                    "orderOptNo": $(this).find("td").eq(4).html(),  //옵션번호
                    "orderItemNo": $(this).find("td").eq(6).html(), //상품주문번호
                    "claimQty": orderQty     //신청수량
                }
                claimPreRefundItemList.push(claimPreRefundItem);
            }
        });

        //주문수량 체크
        if (claimPreRefundItemList.length === 0) {
            alert('신청수량을 선택해주세요');
            return false;
        }

        //parameter 세팅
        var param = {
            "purchaseOrderNo": $('#purchaseOrderNo').val(),
            "claimItemList": claimPreRefundItemList,
            "whoReason": whoReason,
            "cancelType": cancelType,
            "claimPartYn": "Y",
            "claimReasonType": $('#claimReasonType').val(),
            "piDeductPromoYn" : 'N',
            "piDeductDiscountYn" : 'N',
            "piDeductShipYn" : 'N'
        };
        return param;
    },

    /**
     * 클레임 신청 파라미터 생성
     * **/
    getClaimReqParam: function ($modal, _readTableId) {
      var claimPreRefundItemList = [];
      var claimPreRefundItem;

      $modal.find('#' + _readTableId + ' tbody tr').each(function () {
          var orderQty = $(this).find("select").eq(0).val();
          if (orderQty > 0) {
            claimPreRefundItem = {
              "itemNo": $(this).find("td").eq(0).html(),   //상품번호
              "orderOptNo": $(this).find("td").eq(4).html(),    //옵션번호
              "orderItemNo": $(this).find("td").eq(6).html(), //상품주문번호
              "claimQty": orderQty,                        //신청수량
            }
            claimPreRefundItemList.push(claimPreRefundItem);
          }
      });
      return claimPreRefundItemList;
  }

};

/**
 * 이벤트 이미지 관련
 */
var claimImg = {

    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function ($modal, seq, params) {
        $modal.find('#imgBtnView' + seq).show();
        $modal.find('#imgBtnUpload' + seq).hide();

        $modal.find('#fileName'+seq).text(params.fileName);
        $modal.find('#uploadFileName'+seq).val(params.imgId);
    }
    /**
     * 이미지 세팅
     * @param obj
     */
    , addImg : function ($modal, seq) {

        var fileTemp = /(.*?)\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/;

        if ($("#claimImgFile" + seq).get(0).files[0].size > 5242880) {
            return $.jUtil.alert("첨부가능한 최대용량을 초과하였습니다.");
        }

        if (!$("#claimImgFile" + seq).get(0).files[0].name.match(fileTemp)) {
            alert('첨부 가능한 파일형식이 아닙니다.')
            $("#imgFile" + claimImg.seq).val('')
            return false;
        }
        claimImg.setImg($modal, seq,{
            'fileName' : $("#claimImgFile" + seq).get(0).files[0].name,
            'imgId': ''
        })
    }
    /**
     * 업로드 에러 발생시 메세지 처리
     * @param error error
     * @returns {string}
     */
    , alertMsg: function (error) {
        var errorMsg = '';

        if (error.indexOf('확장자') > -1) {
            errorMsg = "첨부 가능한 파일형식이 아닙니다.";
        } else {
            errorMsg = error;
        }

        if ($.jUtil.isNotEmpty(errorMsg)) {
            alert(errorMsg);
        }
    }
    /**
     * 이미지 업로드
     * @param obj
     */
    , uploadImg : function (seq, file) {

        var $uploadForm = $("<form></form>");
        file.clone(true).appendTo($uploadForm);
        file.val('');
        $uploadForm.append('<input type="hidden" name="processKey" value="ClaimAddImage">');
        $uploadForm.append('<input type="hidden" name="mode" value="IMG">');

        $uploadForm.ajaxForm({
            type: "POST",
            url: "/common/upload.json",
            async : false,
            success: function (resData) {

                for(var i=0; i<resData.fileArr.length; i++){
                    var f = resData.fileArr[i];
                    $('#uploadFileName' + seq).val(f.fileStoreInfo.fileId);
                }
            }
        }).submit();
    }
    /**
     * 이미지 삭제 (초기화)
     */
    , deleteImg : function ($modal, seq) {
        $('#fileName'+seq).text('');
        $('#uploadFileName'+seq).val('');
        $('#imgBtnView' + seq).hide();
        $('#imgBtnUpload' + seq).show();
    }

};