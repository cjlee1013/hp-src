/**
 * 판매관리 > 취소/교환/환불 상품 정보 리스트
 */

var itemInfoLayout = {

    /**
     * 상품정보 리스트 조회
     */
    getOrderInfoList: function () {
        CommonAjax.basic(
            {
                url: '/sell/orderShipManage/getOrderClaimListPop.json?purchaseOrderNo=' + $('#purchaseOrderNo').val() + "&bundleNo=" + $('#bundleNo').val()+ "&claimType=" + $('#claimType').val(),
                data: null,
                method: 'GET',
                successMsg: null,
                callbackFunc: function (res) {
                    var nonOptItemList = [];//옵션없는 주문정보 리스트

                    if (res.length === 0) {
                        alert('조회된 결과가 없습니다.');
                        window.close();
                        return false;
                    }

                    //orderType : 2 옵션일 경우 itemNm1값이 빈값이면 옵션정보가 없는것으로 간주
                    //주문정보 row에 상품금액/상품 수량 표기
                    for(var i=0; i<res.length; i++){
                        if(res[i].orderType == '2'){
                            if(res[i].itemNm1 == '' || res[i].itemNm1 == null){
                                nonOptItemList.push(res[i]);//옵션이 없는 상품
                            }
                        }
                    }

                    $('#purchaseOrderNo').val(res[0].purchaseOrderNo);
                    $('#titleOrderInfo').text('주문번호 : ' + $('#purchaseOrderNo').val() + '    |    배송번호 : ' + $('#bundleNo').val());


                    for(var i=0; i<res.length; i++){
                        var tableRow = $('<tr>');
                        var nonOptItemFlag = false;//옵션없는 상품 flag, true : 옵션없음, false : 옵션있음
                        var nonOptItemObject;//옵션없는 상품용 object
                        var selectOrderQty;


                        //옵션이 없는 상품 체크여부

                        for(var index=0; index<nonOptItemList.length; index++){
                            //옵션이 없는 상품
                            if(res[i].itemNo == nonOptItemList[index].itemNo){
                                nonOptItemFlag = true;
                                nonOptItemObject = nonOptItemList[index];
                            }
                        }

                        switch (res[i].orderType) {
                            case '1':
                                var itemInfomation = '';
                                tableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; text-align: center; font-size: 12px').text(res[i].itemNo));

                                //안내사항 정보 표기
                                //최소구매수량 or 수량별 배송비 차등 수량 값이 있을 경우 표기
                                if(res[i].purchaseMinQty > 0 || parseInt(res[i].diffQty) > 0){
                                    itemInfomation += "<span class='text' style='font-size: 12px; font-weight: normal'>[안내사항]</span>"
                                    itemInfomation += res[i].purchaseMinQty > 0 ? "<span class='text' style='font-size: 12px; font-weight: normal'> 최소구매수량  "+ res[i].purchaseMinQty +"개</span>" : ''//최소구매수량 표기
                                    itemInfomation += res[i].purchaseMinQty > 0 && res[i].diffQty > 0 ? ' / ' : ''
                                    itemInfomation += res[i].diffQty > 0 ? "<span class='text' style='font-size: 12px; font-weight: normal'> 수량별 배송비 차등  "+ parseInt(res[i].diffQty) +"개</span>" : ''//수량별 배송비 차등 수량 표기
                                }
                                tableRow.append($('<td>').attr('style', 'font-weight: bold; font-size: 14px; overflow:hidden;white-space:nowrap;text-overflow:ellipsis;')
                                .append(res[i].itemNm1)
                                .append(itemInfomation != '' ? '<br>' + itemInfomation : ''));//안내사항 정보 표기

                                //옵션없는 상품이라 상품금액/신청수량을 표기한다.
                                if(nonOptItemFlag){
                                    selectOrderQty = nonOptItemObject.orderQty - nonOptItemObject.claimQty//총주문 개수 - 클레임 건수
                                    tableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; text-align: center; font-size: 14px').text($.jUtil.comma(nonOptItemObject.orderPrice) + '원'));
                                    itemInfoLayout.getOrderQtySelectBox(nonOptItemObject, selectOrderQty, tableRow, res[i].promoNo);
                                }
                                break;
                            default :
                                //옵션이 있을경우 다음row에 옵션 표기
                                if(!nonOptItemFlag){
                                    selectOrderQty = res[i].orderQty - res[i].claimQty//총주문 개수 - 클레임 건수
                                    tableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; text-align: center; font-size: 12px').text(res[i].itemNo));
                                    tableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; font-size: 12px').text('\t' + res[i].itemNm1));
                                    tableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; text-align: center; font-size: 14px').text($.jUtil.comma(res[i].orderPrice) + '원'));
                                    itemInfoLayout.getOrderQtySelectBox(res[i], selectOrderQty, tableRow, null)
                                }
                                break;
                        }
                        $('#itemInfoTable > tbody:last').append(tableRow);
                    }
                }
            });
    },
    getOrderQtySelectBox : function(itemInfoObject, selectOrderQty, tableRow, promoNo){
       var select = $('<select></select>').addClass('ui input medium  mg-r-10');

       if(($('#claimPartYn').val() == 'N' && $('#orderCancelYn').val() == 'Y') || selectOrderQty == 0){
           tableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; font-size: 14px; text-align:center').text(selectOrderQty));
       }else{//전체 취소가 아닐 시  신청수량을 select box로 표시
           //신청수량에 표시될 수량 - 총 상품주문 수량에서 클레임처리된 건수를 뺀 수량을 보여준다.
           var purchaseMinQtyCnt = 1;
           var purchaseMinQty = itemInfoObject.purchaseMinQty;
           select.append( $('<option>').val('0').text('선택'));

           //행사상품 여부 체크
           //행사가 있는 경우 신청 수량선택시 구매한 수량만 선택가능
           if(promoNo != null){
               select.last().append($('<option>').attr('value', selectOrderQty).text(selectOrderQty));
           }else{
               while(purchaseMinQtyCnt <= selectOrderQty){
                   //최소구매수량에 따른 신청 수량 적용
                   if(purchaseMinQtyCnt <= selectOrderQty - purchaseMinQty || selectOrderQty == purchaseMinQtyCnt){
                       var option = $('<option>').attr('value', purchaseMinQtyCnt).text(purchaseMinQtyCnt);
                       select.last().append(option);
                   }
                   purchaseMinQtyCnt++;
               }
           }
           tableRow.append($('<td>').append(select).append($('<input type="hidden">').attr('name', 'orderQtyCompare').val('0')));
       }
       tableRow.append($('<td hidden>').html(itemInfoObject.orderOptNo));
       tableRow.append($('<td hidden>').html(itemInfoObject.orderType));
       tableRow.append($('<td hidden>').html(itemInfoObject.orderItemNo));
       tableRow.append($('<td hidden>').html(itemInfoObject.claimQty));

    }

}