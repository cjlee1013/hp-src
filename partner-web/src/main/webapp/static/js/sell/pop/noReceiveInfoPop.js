/** Main Script */
var noReceiveInfoPop = {
    /**
     * init 이벤트
     */
    init: function() {
        noReceiveInfoPop.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = noReceiveInfoPop.getRoot();
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var checkRowIds = parentGrid.gridView.getSelectedRows();
        var reqData = new Object();

        reqData.bundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"bundleNo");
        reqData.orderItemNo = parentGrid.dataProvider.getValue(checkRowIds[0],"orderItemNo");
        reqData.itemNm1 = parentGrid.dataProvider.getValue(checkRowIds[0],"itemNm1");
        reqData.userNo = parentGrid.dataProvider.getValue(checkRowIds[0],"userNo");
        reqData.purchaseOrderNo = parentGrid.dataProvider.getValue(checkRowIds[0],"purchaseOrderNo");
        reqData.shipNo = parentGrid.dataProvider.getValue(checkRowIds[0],"shipNo");

        CommonAjax.basic({
            url:'/sell/shipStatus/getNoReceiveInfo.json',
            data: JSON.stringify(reqData),
            contentType: 'application/json',
            method: "POST",
            callbackFunc: function(res) {
                if(res.noRcvProcessType === 'C') {
                    $('#noReceiveInfoPopTitle').html("미수취 신고처리 내역");
                } else {
                    $('#noReceiveInfoPopTitle').html("미수취 신고");
                }

                if(!$.jUtil.isEmpty(res.noRcvProcessCntnt)){
                    $('#noRcvProcessCntntArea').show();
                } else {
                    $('#noRcvProcessCntntArea').hide();
                }

                noReceiveInfoPop.mapEach(res);
            }
        });
    },

    /**
     * mapData key로 id매핑
     */
    mapEach: function (map) {
        var $modal = noReceiveInfoPop.getRoot();

        $.each(map,function(key, value){
            $modal.find('#'+key).html(value);
        });
    },

    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('noReceiveInfoPop',noReceiveInfoPop.search());
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('noReceiveInfoPop');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#noReceiveInfoPop');
    }
};