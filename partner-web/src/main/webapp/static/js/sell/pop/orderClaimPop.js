$(document).ready(function() {
  orderClaimPop.init();
});

/** Main Script */
var orderClaimPop = {
    /**
     * init 이벤트
     */
    init: function() {
        orderClaimPop.bindingEvent();
        claimMainCommon.chgDeliveryType(orderClaimPop.getRoot(), 'C');
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = orderClaimPop.getRoot();
        // [등록] 버튼
        $modal.find("#saveBtn").bindClick(orderClaimPop.save);

        //수거방법 변경
        $modal.find('#pickShipType').bindChange(function() { orderClaimPop.chgShipType($modal, $modal.find('#pickShipType').val())});

        //연락처 - 숫자만 입력
        $modal.find("#pickMobileNo_1,#pickMobileNo_2,pickMobileNo_3").allowInput("keyup", ["NUM"], $(this).attr("id"));

        //연락처 - 숫자만 입력
        $modal.find("#exchMobileNo_1,#exchMobileNo_2,exchMobileNo_3").allowInput("keyup", ["NUM"], $(this).attr("id"));

        //송장번호 - 숫자만 입력
        $modal.find("#pickInvoiceNo").allowInput("keyup", ["NUM","NOT_SPACE"], $(this).attr("id"));

        // 이미지1 등록
        $('#claimImgFile').change(function() {
            claimImg.addImg($modal,'', 'claimImgFile');
        });
        // 이미지2 등록
        $('#claimImgFile2').change(function() {
            claimImg.addImg($modal,2, 'claimImgFile2');
        });
        // 이미지3 등록
        $('#claimImgFile3').change(function() {
            claimImg.addImg($modal,3, 'claimImgFile3');
        });

    },

    /**
     * [수거/배송방법] 선택 값에 따른 문구 설정
     * reqType C:택배 D:직배송 N:배송없음 P:우편배송
     * **/
    chgShipType : function($modal, pickShipTypeCode){
        switch (pickShipTypeCode) {
            case 'C' ://택배배송
                $modal.find("#parcelDiv").show();
                break;
            case 'N' :
                $modal.find("#parcelDiv").hide();
                break;
        }
    },

    /**
     * 데이터 검색
     */
    set: function(claimType) {
        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var $modal = orderClaimPop.getRoot();
        var claimTypeName = claimType == 'R' ? '반품' : '교환';

        $('#bundleNo').val(parentGrid.dataProvider.getValue(checkRowIds[0],"bundleNo"));
        $('#purchaseOrderNo').val(parentGrid.dataProvider.getValue(checkRowIds[0],"purchaseOrderNo"));
        $modal.find('#claimType').val(claimType);
        $modal.find('#shipStatusNm').val(parentGrid.dataProvider.getValue(checkRowIds[0],"shipStatusNm"));
        $modal.find('#shipNo').val(parentGrid.dataProvider.getValue(checkRowIds[0],"shipNo"));
        itemInfoLayout.getOrderInfoList();

        //클레임 사유 select box 세팅
        $modal.find('#claimReasonType').append("<option value=''>"+claimTypeName+" 사유를 선택하세요</option>")
        $.each(claimReasonList, function(key, value){
            if(value.claimType == claimType){
                $modal.find('#claimReasonType').append("<option value="+value.mcCd+">"+value.mcNm+"</option>")
            }
        });

        $modal.find('#claimPopTitle,#reqBtnTitle').text(claimTypeName+'신청');
        $modal.find('#claimPopTitle2').text('> '+claimTypeName+'신청 정보');
        $modal.find('#pickRequestTitle').text(claimTypeName+'수거지');

        if(claimType == 'X'){
            $('#exchangeTableRow').show();
        }


        CommonAjax.basic(
            {
                url:'/claim/getOrderShipAddrInfo.json?bundleNo=' + $('#bundleNo').val(),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {

                    $modal.find("input[name$='ReceiverNm']" ).val(res[0].receiverNm);
                    $modal.find("input[name$='MobileNo']" ).val(res[0].shipMobileNo);
                    $.jUtil.valSplit($modal.find('#pickMobileNo').val(), '-', 'pickMobileNo');
                    $.jUtil.valSplit($modal.find('#exchMobileNo').val(), '-', 'exchMobileNo');
                    $modal.find("input[name$='ZipCode']" ).val(res[0].zipCode);
                    $modal.find("input[name$='RoadBaseAddr']" ).val(res[0].roadBaseAddr);
                    $modal.find("input[name$='RoadDetailAddr']" ).val(res[0].roadDetailAddr);
                    $modal.find("input[name$='BaseAddr']" ).val(res[0].baseAddr);
                    $modal.find("input[name$='BaseDetailAddr']" ).val(res[0].detailAddr);

                }
            });
    },

    /**
     * 반품신청 유효성 검사
     * **/
    validation : function(){

        var $modal = orderClaimPop.getRoot();
        var claimTypeName = $modal.find('#claimType').val() == 'R' ? '반품' : '교환';


        if($.jUtil.isEmpty($modal.find('#claimReasonType').val())){
              alert(claimTypeName + ' 사유를 선택해주세요');
              return false;
        }

        if($.jUtil.isEmpty($modal.find('#pickReceiverNm').val())){
            alert('받는사람 이름을 입력해주세요');
            return false;
        }

        if($modal.find('#pickReceiverNm').val().length < 2) {
            alert('받는사람 이름을 2자 이상 입력해주세요.');
            return false;
        }

        if($.jUtil.isEmpty($modal.find('#pickMobileNo').val())){
            alert('연락처를 입력해주세요');
            return false;
        }

        if($modal.find('#pickMobileNo').val().replace('-','').length < 10) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if ($.jUtil.isEmpty($modal.find('#pickZipCode').val())) {
            alert('수거지 우편번호를 입력주세요');
            return false;
        }
        //수거지 주소
        if ($.jUtil.isEmpty($modal.find('#pickRoadBaseAddr').val())) {
            alert('주소를 입력주세요');
            return false;
        }
        //수거지 상세주소
        if ($.jUtil.isEmpty($modal.find('#pickBaseDetailAddr').val())) {
            alert('상세주소를 입력주세요');
            return false;
        }

        //택배수거인 경우
        if($modal.find('#pickShipType').val() == 'C'){
            if ($.jUtil.isEmpty($modal.find('#pickDlvCd').val())) {
                alert('택배사를 선택해주세요');
                return false;
            }
            if ($.jUtil.isEmpty($modal.find('#pickInvoiceNo').val())) {
                alert('송장번호를 입력해주세요');
                return false;
            }
            $modal.find('#isPick').val('Y')
        }
        //수거안함인 경우 발송여부 N
        if($modal.find('#pickShipType').val() == 'N'){
            $modal.find('#isPick').val('N')
            $modal.find('#isPickReq').val('N')
        }

        //교환신청인 경우
        if($modal.find('#claimType').val() == 'X'){

            if($.jUtil.isEmpty($modal.find('#pickReceiverNm').val())){
                alert('보내는 사람 이름을 입력해주세요');
                return false;
            }

            if($modal.find('#pickReceiverNm').val().length < 2) {
                alert('보내는 사람 이름을 2자 이상 입력해주세요.');
                return false;
            }

            if($.jUtil.isEmpty($modal.find('#exchMobileNo').val())){
                alert('보내는 연락처를 입력해주세요');
                return false;
            }

            if($modal.find('#exchMobileNo').val().replace('-','').length < 10) {
                alert('보내는 연락처를 다시 한번 확인해 주세요');
                return false;
            }

            if ($.jUtil.isEmpty($modal.find('#exchZipCode').val())) {
                alert('발송지 우편번호를 입력주세요');
                return false;
            }

            //수거지 주소
            if ($.jUtil.isEmpty($modal.find('#exchRoadBaseAddr').val())) {
                alert('발송지 주소를 입력주세요');
                return false;
            }
            //수거지 상세주소
            if ($.jUtil.isEmpty($modal.find('#exchBaseDetailAddr').val())) {
                alert('발송지 상세주소를 입력주세요');
                return false;
            }

        }

        return true;
    },

    /**
     * 저장
     */
    save: function() {
        var $modal = orderClaimPop.getRoot();
        $modal.find('#pickMobileNo').val($.jUtil.valNotSplit('pickMobileNo', '-'));
        $modal.find('#exchMobileNo').val($.jUtil.valNotSplit('exchMobileNo', '-'));

        $modal.find('#pickRoadDetailAddr').val($modal.find('#pickBaseDetailAddr').val());
        var claimTypeName = $modal.find('#claimType').val() == 'R' ? '반품' : '교환';

        var detailReasonRequired = '';
        var cancelType = '';
        var whoReason = '';

        //귀책 사유 조회
        //상세 필수 여부 조회
        //취소 타입 조회
        $.each(claimReasonList, function(key, value){
            if(value.mcCd == $modal.find('#claimReasonType').val()){
                detailReasonRequired = value.detailReasonRequired;//상세 필수여부
                cancelType = value.cancelType;//취소타입
                whoReason = value.ref2;//귀책사유
            }
        });

        if(detailReasonRequired === 'Y'){
            if($.jUtil.isEmpty($modal.find('#claimReasonDetail').val())){
                alert(claimTypeName + '상세사유를 입력하세요.')
                return false;
            }
        }

        //유효성 검사
        if(!orderClaimPop.validation()){
            return false;
        }

        //환불예정금액 조회 파라미터 세팅
        var preRefundAmtParam = orderClaimCommon.setRefundAmtParam($modal,cancelType, whoReason);
        if(!preRefundAmtParam){
            return false;
        }


        //이미지 업로드
        if (!$.jUtil.isEmpty($('#fileName').text())) {
            claimImg.uploadImg('', $('#claimImgFile'));
        }
        if (!$.jUtil.isEmpty($('#fileName2').text())) {
            claimImg.uploadImg('2', $('#claimImgFile2'));
        }
        if (!$.jUtil.isEmpty($('#fileName3').text())) {
            claimImg.uploadImg('3', $('#claimImgFile3'));
        }


        //환불예정금액 api 호출
        CommonAjax.basic(
            {
                url: '/claim/preRefundPrice.json',
                data: JSON.stringify(preRefundAmtParam),
                contentType: 'application/json',
                method: 'POST',
                callbackFunc: function (res) {

                    //환불예정금액 조회 결과 0 : 성공, 나머지 : 실패
                    if(res.returnValue != '0') {
                        alert('환불예정금액 조회 실패 [' + res.returnMsg + ']');
                        return false;
                    }
                    if(res.refundAmt < 0){
                        alert('환불예정금액이 0원보다 작아 반품신청이 불가능합니다. \n 환불예정금액 확인이 필요하신 경우 고객센터로 문의 부탁드립니다.');
                        return false;
                    }


                    /**
                     * 반품/교환 요청 파라미터 설정
                     * **/
                    var claimParam =
                        {
                            "purchaseOrderNo": $('#purchaseOrderNo').val(),
                            "claimItemList": orderClaimCommon.getClaimReqParam($modal, 'itemInfoTable'),
                            "cancelType": cancelType,
                            "claimType": $modal.find('#claimType').val(),
                            "bundleNo": $('#bundleNo').val(),
                            "claimReasonType": $modal.find('#claimReasonType').val(),
                            "claimReasonDetail" : $modal.find('#claimReasonDetail').val(),
                            "claimPartYn": "Y",
                            "whoReason": whoReason,
                            "orderCancelYn" : "N",
                            "claimDetail" : $modal.find('#claimRequestForm').serializeObject(),
                            "piDeductPromoYn" : 'N',
                            "piDeductDiscountYn" : 'N',
                            "piDeductShipYn" : 'N'
                        };

                    if (!confirm(claimTypeName + '신청 하시겠습니까?')) {
                        return false;
                    }

                    //반품요청
                    CommonAjax.basic(
                        {
                            url: '/claim/claimRegister.json',
                            data: JSON.stringify(claimParam),
                            method: "POST",
                            contentType: 'application/json',
                            successMsg: null,
                            callbackFunc: function (res) {
                                if('0000,SUCCESS'.indexOf(res.returnCode) > -1) {
                                    alert(claimTypeName + '신청이 완료되었습니다.')
                                    orderClaimPop.closeModal();
                                }else{
                                    alert(claimTypeName + '신청실패 [' + res.returnMsg + ']');
                                    return false;
                                }

                            }
                        });
                }
            }
        );
    },

    /**
     * 주소정보 세팅
     * **/
    orderReturnSetZipcode : function (res){
        var $modal = orderClaimPop.getRoot();
        $modal.find("#pickZipCode").val(res.zipCode);
        $modal.find("#pickRoadBaseAddr").val(res.roadAddr);
        $modal.find('#pickBaseAddr').val(res.gibunAddr);
    },

    /**
     * 주소정보 세팅
     * **/
    orderExchangeSetZipcode : function (res){
        var $modal = orderClaimPop.getRoot();
        $modal.find("#exchZipCode").val(res.zipCode);
        $modal.find("#exchRoadBaseAddr").val(res.roadAddr);
        $modal.find('#exchBaseAddr').val(res.gibunAddr);
    },

    /**
     * modal open
     */
    openModal: function(claimType) {
        $ui.modalOpen('orderClaimPop', orderClaimPop.set(claimType));
    },
    /**
     * form reset
     */
    formReset: function() {
        var $modal = orderClaimPop.getRoot();
        $modal.find('#itemInfoTable > tbody').empty();
        $modal.find('#claimReasonType,#claimReasonDetail').val('');
        $modal.find('#claimReasonType').children('option').remove();
        $modal.find('#claimRequestForm').resetForm();
        $('#exchangeTableRow').hide();

    },

    /**
     * modal close
     */
    closeModal: function() {
        orderClaimPop.formReset();
        claimMainCommon.chgDeliveryType(orderClaimPop.getRoot(), 'C');
        claimImg.deleteImg(orderClaimPop.getRoot(), '');
        claimImg.deleteImg(orderClaimPop.getRoot(), 2);
        claimImg.deleteImg(orderClaimPop.getRoot(), 3);
        parentMain.callback();
        $ui.modalClose('orderClaimPop');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#orderClaimPop');
    },

    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function (seq) {
        $('#claimImgFile'+seq).click();
    },

    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function (seq) {
       $("#returnImgFile" + seq).val('')
        claimImg.deleteImg(orderClaimPop.getRoot(), seq);
    }
};
