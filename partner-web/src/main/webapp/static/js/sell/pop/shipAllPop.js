/** Main Script */
var shipAllPop = {
    /**
     * init 이벤트
     */
    init: function() {
        var $modal = shipAllPop.getRoot();

        shipAllPop.bindingEvent();
        Calendar.datePicker("scheduleShipDt");
        $modal.find('#scheduleShipDt').next().hide();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = shipAllPop.getRoot();

        // 배송방법 선택 시 필수값 입력 노출
        $modal.find("#shipMethod").bindChange(shipAllPop.changeInputForm);
        // [등록] 버튼
        $modal.find("#saveBtn").bindClick(shipAllPop.save);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var bundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"bundleNo");
        var reqData = "bundleNo="+bundleNo;

        CommonAjax.basic({
            url         : '/sell/orderShipManage/getShipAllPopList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                shipAllPopGrid.setData(res);
            }
        });
    },

    /**
     * 배송방법 선택 시 입력 FORM 노출
     */
    changeInputForm: function() {
        var $modal = shipAllPop.getRoot();
        var selectVal = $modal.find('#shipMethod').val();

        switch (selectVal) {
            case "DS_DLV" :
                $modal.find('#dlvCd').show();
                $modal.find('#invoiceNo').show();
                $modal.find('#scheduleShipDt').hide().val("");
                $modal.find('#scheduleShipDt').next().hide();
                break;
            case "DS_DRCT" :
                $modal.find('#dlvCd').hide().val("");
                $modal.find('#invoiceNo').hide().val("");
                $modal.find('#scheduleShipDt').show();
                $modal.find('#scheduleShipDt').next().show();
                break;
            default :
                $modal.find('#dlvCd').hide().val("");
                $modal.find('#invoiceNo').hide().val("");
                $modal.find('#scheduleShipDt').hide().val("");
                $modal.find('#scheduleShipDt').next().hide();
                break;
        }
    },

    /**
     * 저장
     */
    save: function() {
        var $modal = shipAllPop.getRoot();
        var checkRowIds = shipAllPopGrid.gridView.getCheckedRows();
        var reqData = new Object();
        var shipMethod = $modal.find('#shipMethod').val();
        var dlvCd = $modal.find('#dlvCd').val();
        var invoiceNo = $modal.find('#invoiceNo').val();
        var scheduleShipDt = $modal.find('#scheduleShipDt').val();
        var bundleNo = shipAllPopGrid.dataProvider.getValue(0,"bundleNo");

        if ($.jUtil.isEmpty(shipMethod)) {
            alert("배송방법을 선택해 주세요");
            return;
        }

        if (shipMethod == "DS_DLV") {
            if ($.jUtil.isEmpty(dlvCd)) {
                alert("택배사를 선택해 주세요");
                return;
            }

            if ($.jUtil.isEmpty(invoiceNo)) {
                alert("송장번호를 입력해 주세요");
                return;
            }

            if (!/^[a-zA-Z0-9]*$/.test(invoiceNo)) {
                alert("송장번호 형식이 올바르지 않습니다");
                return;
            }
        }

        if (shipMethod == "DS_DRCT") {
            if ($.jUtil.isEmpty(scheduleShipDt)) {
                alert("배송예정일을 입력해 주세요");
                return;
            }
        }

        reqData.shipMethod = shipMethod;
        reqData.dlvCd = dlvCd;
        reqData.invoiceNo = invoiceNo;
        reqData.scheduleShipDt = scheduleShipDt;
        reqData.bundleNo = bundleNo;

        if (confirm("저장하시겠습니까?")) {
            CommonAjax.basic({
                url         : '/sell/orderShipManage/setShipAll.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "SUCCESS") {
                        alert(res.returnMessage);
                    } else {
                        alert("등록 완료되었습니다.");
                        parentMain.callback();
                    }

                    shipAllPop.closeModal();
                }
            });
        }
    },

    /**
     * form reset
     */
    formReset: function() {
        var $modal = shipAllPop.getRoot();

        $modal.find("#shipMethod option:eq(0)").prop("selected", true);
        shipAllPop.changeInputForm();
    },

    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('shipAllPop', shipAllPop.search);
        shipAllPopGrid.gridView.resetSize();
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('shipAllPop', shipAllPop.formReset);
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#shipAllPop');
    }
};

/** Grid Script */
var shipAllPopGrid = {
    gridView: new RealGridJS.GridView("shipAllPopGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        shipAllPopGrid.initGrid();
        shipAllPopGrid.initDataProvider();
        shipAllPopGrid.event();
    },
    initGrid: function () {
        shipAllPopGrid.gridView.setDataSource(shipAllPopGrid.dataProvider);
        shipAllPopGrid.gridView.setStyles(shipAllPopGridBaseInfo.realgrid.styles);
        shipAllPopGrid.gridView.setDisplayOptions(shipAllPopGridBaseInfo.realgrid.displayOptions);
        shipAllPopGrid.gridView.setColumns(shipAllPopGridBaseInfo.realgrid.columns);
        shipAllPopGrid.gridView.setOptions(shipAllPopGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        shipAllPopGrid.dataProvider.setFields(shipAllPopGridBaseInfo.dataProvider.fields);
        shipAllPopGrid.dataProvider.setOptions(shipAllPopGridBaseInfo.dataProvider.options);
    },
    event: function() {

    },
    setData: function (dataList) {
        shipAllPopGrid.dataProvider.clearRows();
        shipAllPopGrid.dataProvider.setRows(dataList);
    }
};