/** Main Script */
var notiDealyPop = {
    /**
     * init 이벤트
     */
    init: function() {
        notiDealyPop.bindingEvent();
        Calendar.datePicker("delayShipDt");
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = notiDealyPop.getRoot();

        // 기타 선택 시 textarea 노출
        $modal.find("#delayShipCd").bindChange(notiDealyPop.selectDelayMsg);
        // [저장] 버튼
        $modal.find("#saveBtn").bindClick(notiDealyPop.save);
        // 지연사유 글자수 카운트
        $modal.find("#delayShipMsg").keyup(function(){ notiDealyPop.textCount() });
    },

    /**
     * 저장
     */
    save: function() {
        var $modal = notiDealyPop.getRoot();
        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var reqData = new Object();

        var checkedRow = parentGrid.dataProvider.getJsonRow(checkRowIds[0]);
        var bundleNo = checkedRow.bundleNo;
        var delayShipCd =  $modal.find('#delayShipCd').val();
        var delayShipDt =  $modal.find('#delayShipDt').val();
        var delayShipMsg =  $modal.find('#delayShipMsg').val();

        if (delayShipDt == "") {
            alert("발송기한을 입력해주세요.");
            return;
        }

        if (delayShipCd == ""
                || (delayShipCd == "ET" && delayShipMsg == '')) {
            alert("사유를 입력해주세요.");
            return;
        }

        reqData.bundleNo = bundleNo;
        reqData.delayShipCd = delayShipCd;
        reqData.delayShipDt = delayShipDt;
        reqData.delayShipMsg = delayShipMsg;

        if (confirm("저장하시겠습니까?")) {
            CommonAjax.basic({
                url         : '/sell/orderShipManage/setNotiDelay.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "SUCCESS") {
                        alert("발송지연안내를 실패했습니다.");
                        notiDealyPop.closeModal();
                    } else {
                        //notiDealyPop.resultTemplate(reqCount, res.data);
                        orderShipManageMain.callback();
                        alert("발송지연안내를 처리했습니다.");
                        notiDealyPop.closeModal();
                        //notiDealyPop.openResultModal();
                    }
                }
            });
        }
    },

    /**
     * 저장 후 팝업 템플릿
     */
    resultTemplate: function(totalCnt, successCnt) {
        var $resultModal = notiDealyPop.getResultRoot();
        var failCnt = parseInt(totalCnt) - parseInt(successCnt);

        $resultModal.find('#totalCnt').text(totalCnt);
        $resultModal.find('#successCnt').text(successCnt);
        $resultModal.find('#failCnt').text(failCnt);
    },

    /**
     * 셀렉트 박스 선택 이벤트
     * 기타 선택 시, 지연사유 입력 영역 SHOW
     */
    selectDelayMsg: function() {
        var $modal = notiDealyPop.getRoot();
        var selectVal = $modal.find('#delayShipCd').val();
        var $delayShipMsg = $modal.find("#delayShipMsg");

        if (selectVal == "ET") {
            $delayShipMsg.val("")
            $delayShipMsg.closest("div").show();
        } else {
            $delayShipMsg.val("");
            $delayShipMsg.closest("div").hide();
        }
    },

    /**
     * 지연사유 글자수 카운트
     */
    textCount: function() {
        var $modal = notiDealyPop.getRoot();
        var $delayShipMsg = $modal.find("#delayShipMsg");
        var content = $delayShipMsg.val();
        var count = content.length;

        $('#msgCount').text(count);
    },

    /**
     * form reset
     */
    formReset: function() {
        var $modal = notiDealyPop.getRoot();

        $modal.find('#delayShipCd').val("");
        $modal.find('#delayShipDt').val("");
        $modal.find("#delayShipCd option:eq(0)").prop("selected", true);
        $modal.find('#msgCount').text(0);

        notiDealyPop.selectDelayMsg();
    },

    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('notiDelayPop');
    },

    /**
     * result modal open
     */
    openResultModal: function() {
        $ui.modalOpen('notiDelayResultPop');
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('notiDelayPop', notiDealyPop.formReset);
    },

    /**
     * result modal close
     */
    closeResultModal: function() {
        $ui.modalClose('notiDelayResultPop');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#notiDelayPop');
    },

    /**
     * result modal root selector
     */
    getResultRoot: function() {
        return $('#notiDelayResultPop');
    }

};