$(document).ready(function() {
  orderCancelPop.init();
});

/** Main Script */
var orderCancelPop = {
    /**
     * init 이벤트
     */
    init: function() {
        orderCancelPop.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = orderCancelPop.getRoot();
        // [등록] 버튼
        $modal.find("#saveBtn").bindClick(orderCancelPop.save);
    },

    /**
     * 데이터 검색
     */
    set: function() {
        var checkRowIds = parentGrid.gridView.getCheckedRows();

        $('#bundleNo').val(parentGrid.dataProvider.getValue(checkRowIds[0],"bundleNo"));
        $('#purchaseOrderNo').val(parentGrid.dataProvider.getValue(checkRowIds[0],"purchaseOrderNo"));
        $('#shipStatus').val(parentGrid.dataProvider.getValue(checkRowIds[0],"shipStatus"));

        itemInfoLayout.getOrderInfoList();
    },

    /**
     * 저장
     */
    save: function() {

        var preRefundAmtParam = orderClaimCommon.setRefundAmtParam(orderCancelPop.getRoot(), 'O', 'S');

        //환불예정금액 api 호출
        CommonAjax.basic(
            {
                url: '/claim/preRefundPrice.json',
                data: JSON.stringify(preRefundAmtParam),
                contentType: 'application/json',
                method: 'POST',
                callbackFunc: function (res) {

                    //환불예정금액 조회 결과 0 : 성공, 나머지 : 실패
                    if(res.returnValue != '0') {
                        alert('환불예정금액 조회 실패 [' + res.returnMsg + ']');
                        return false;
                    }
                    if(res.refundAmt < 0){
                        alert('환불예정금액이 0원보다 작아 주문취소가 불가능합니다. 고객센터로 문의 부탁드립니다.');
                        return false;
                    }

                    if (!confirm('주문취소 하시겠습니까?')) {
                        return false;
                    }

                    var orderCancleParam =
                        {
                            "purchaseOrderNo": $('#purchaseOrderNo').val(),
                            "claimItemList": orderClaimCommon.getClaimReqParam(orderCancelPop.getRoot(),'itemInfoTable'),
                            "claimReasonType": $('#claimReasonType').val(),
                            "cancelType": "O",
                            "claimType": "C",
                            "bundleNo": $('#bundleNo').val(),
                            "claimReasonDetail" : $('#claimReasonDetail').val(),
                            "claimPartYn": "Y",
                            "whoReason": "S",
                            "orderCancelYn" : $('#shipStatus').val() == 'D1' ? 'Y' : "N",
                            "piDeductPromoYn" : 'N',
                            "piDeductDiscountYn" : 'N',
                            "piDeductShipYn" : 'N'
                        };

                    //취소요청
                    CommonAjax.basic(
                        {
                            url: '/claim/claimRegister.json',
                            data: JSON.stringify(orderCancleParam),
                            method: "POST",
                            contentType: 'application/json',
                            successMsg: null,
                            callbackFunc: function (res) {
                                if('0000,SUCCESS'.indexOf(res.returnCode) > -1) {
                                    alert('주문취소가 완료되었습니다.')
                                    orderShipManageMain.callback();
                                    orderCancelPop.closeModal();
                                }else{
                                    alert('주문취소실패 [' + res.returnMsg + ']');
                                    return false;
                                }

                            }
                        });
                }
            }
        );



    },
    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('orderCancelPop', orderCancelPop.set);
    },

    /**
     * modal close
     */
    closeModal: function() {
        var $modal = orderCancelPop.getRoot();
        $modal.find('#itemInfoTable > tbody').empty();
        $modal.find('#claimReasonDetail').val('');

        $ui.modalClose('orderCancelPop');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#orderCancelPop');
    }
};
