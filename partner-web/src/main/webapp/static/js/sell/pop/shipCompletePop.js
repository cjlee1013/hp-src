/** Main Script */
var shipCompletePop = {
    /**
     * init 이벤트
     */
    init: function() {
        shipCompletePop.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = shipCompletePop.getRoot();

        // [등록] 버튼
        $modal.find("#saveBtn").bindClick(shipCompletePop.save);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var reqData = new Object();
        var bundleNoList = new Array();

        for (var checkRowId of checkRowIds) {
            var checkedRows = parentGrid.dataProvider.getJsonRow(checkRowId);

            bundleNoList.push(checkedRows.bundleNo);
        }

        reqData.bundleNoList = $.unique(bundleNoList);

        CommonAjax.basic({
            url:'/sell/shipStatus/getShipCompletePopList.json',
            data: JSON.stringify(reqData),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
               shipCompletePopGrid.setData(res);
            }
        });
    },

    /**
     * 저장
     */
    save: function() {
        var rows = shipCompletePopGrid.dataProvider.getJsonRows();
        var reqData = new Object();
        var bundleNoList = new Array();
        var reqCount = 0;

        for (var idx in rows) {
            bundleNoList.push(rows[idx].bundleNo);
        }

        reqData.bundleNoList = $.unique(bundleNoList);;

        reqCount = bundleNoList.length;
        if (confirm('배송완료를 하시겠습니까?')) {
            CommonAjax.basic({
                url         : '/sell/shipStatus/setShipComplete.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "SUCCESS") {
                        alert(res.returnMessage);
                        shipCompletePop.closeModal();
                    } else {
                        shipCompletePop.resultTemplate(reqCount, res.data);
                        parentMain.callback();
                        shipCompletePop.closeModal();
                        shipCompletePop.openResultModal();
                    }
                }
            });
        }
    },

    /**
     * 저장 후 팝업 템플릿
     */
    resultTemplate: function(totalCnt, successCnt) {
        var $resultModal = shipCompletePop.getResultRoot();
        var failCnt = parseInt(totalCnt) - parseInt(successCnt);

        $resultModal.find('#totalCnt').text(totalCnt);
        $resultModal.find('#successCnt').text(successCnt);
        $resultModal.find('#failCnt').text(failCnt);
    },

    /**
     * form reset
     */
    formReset: function() {
        var $modal = shipCompletePop.getRoot();
    },

    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('shipCompletePop', shipCompletePop.search);
        shipCompletePopGrid.gridView.resetSize();
    },

    /**
     * result modal open
     */
    openResultModal: function() {
        $ui.modalOpen('shipCompleteResultPop');
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('shipCompletePop', shipCompletePop.formReset);
    },

    /**
     * result modal close
     */
    closeResultModal: function() {
        $ui.modalClose('shipCompleteResultPop');
    },

    /**
     * result modal root selector
     */
    getResultRoot: function() {
        return $('#shipCompleteResultPop');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#shipCompletePop');
    }
};

/** Grid Script */
var shipCompletePopGrid = {
    gridView: new RealGridJS.GridView("shipCompletePopGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        shipCompletePopGrid.initGrid();
        shipCompletePopGrid.initDataProvider();
        shipCompletePopGrid.customGrid();
        shipCompletePopGrid.event();
    },
    initGrid: function () {
        shipCompletePopGrid.gridView.setDataSource(shipCompletePopGrid.dataProvider);
        shipCompletePopGrid.gridView.setStyles(shipCompletePopGridBaseInfo.realgrid.styles);
        shipCompletePopGrid.gridView.setDisplayOptions(shipCompletePopGridBaseInfo.realgrid.displayOptions);
        shipCompletePopGrid.gridView.setColumns(shipCompletePopGridBaseInfo.realgrid.columns);
        shipCompletePopGrid.gridView.setOptions(shipCompletePopGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        shipCompletePopGrid.dataProvider.setFields(shipCompletePopGridBaseInfo.dataProvider.fields);
        shipCompletePopGrid.dataProvider.setOptions(shipCompletePopGridBaseInfo.dataProvider.options);
    },
    customGrid: function () {
        shipCompletePopGrid.gridView.setColumnProperty("bundleNo", "mergeRule", {criteria:"value"});
    },
    event: function() {

    },
    setData: function (dataList) {
        shipCompletePopGrid.dataProvider.clearRows();
        shipCompletePopGrid.dataProvider.setRows(dataList);
    }
};