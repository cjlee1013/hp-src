/** Main Script */
var itemClaimDetailPop = {
    /**
     * init 이벤트
     */
    init: function() {
        itemClaimDetailPop.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = itemClaimDetailPop.getRoot();
    },

    /**
     * 데이터 검색 (주문)
     */
    search: function() {
        var $modal = itemClaimDetailPop.getRoot();
        var checkRowIds = parentGrid.gridView.getSelectedRows();
        var bundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"bundleNo");

        //상품 상세정보 조회
        CommonAjax.basic({
            url:'/sell/orderShipManage/getOrderDetailPop.json?bundleNo=' + bundleNo,
            data: null,
            contentType: 'application/json',
            method: "GET",
            callbackFunc: function(res) {
                // 주문기본정보
                var orderDetailBase = res.data.orderDetailBase;
                itemClaimDetailPop.mapEach(orderDetailBase);

                // 주문상품정보
                var orderDetailItemList = res.data.orderDetailItemList;
                for(var i=0; i<orderDetailItemList.length; i++){

                    var completeDt = $.jUtil.isEmpty(orderDetailItemList[i].completeDt) ? '' : '<br>(배송완료일 : ' + orderDetailItemList[i].completeDt + ')';
                    var invoiceNo = $.jUtil.isEmpty(orderDetailItemList[i].invoiceNo) ? '' : '<button type="button" onclick="shipHistoryPop.openModal()">' + orderDetailItemList[i].invoiceNo + '</button>';
                    var dlvCd = $.jUtil.isEmpty(orderDetailItemList[i].dlvCd) ? '' : orderDetailItemList[i].dlvCd;

                    orderDetailItemList[i].dlvCd_invoiceNo_completeDt = dlvCd + " " + invoiceNo + completeDt;
                    orderDetailItemList[i].itemPrice = $.jUtil.comma(orderDetailItemList[i].itemPrice) + '원';
                    orderDetailItemList[i].itemQty = $.jUtil.comma(orderDetailItemList[i].itemQty);
                    orderDetailItemList[i].orderPrice = $.jUtil.comma(orderDetailItemList[i].orderPrice) + '원';

                    itemClaimDetailPop.detailItemTemplate(orderDetailItemList[i]);
                }

                // 배송정보
                var orderDetailShip = res.data.orderDetailShip;
                orderDetailShip.shipPriceType_totShipPrice = orderDetailShip.shipPriceType + ' (' + $.jUtil.comma(orderDetailShip.totShipPrice) + '원)';
                orderDetailShip.islandShipPrice = $.jUtil.comma(orderDetailShip.islandShipPrice) + '원';
                orderDetailShip.delayShipDt_delayShipNm = orderDetailShip.delayShipDt == null ? '' : orderDetailShip.delayShipDt + ' ' + orderDetailShip.delayShipNm;
                orderDetailShip.zipcode_addr = '(' + orderDetailShip.zipcode + ') ' + orderDetailShip.addr;
                orderDetailShip.noRcvDeclrDt = orderDetailShip.noRcvDeclrDt == null ? '' : '<button type="button" onclick="noReceiveInfoPop.openModal()">신고일 : ' + orderDetailShip.noRcvDeclrDt + '</button>';

                itemClaimDetailPop.mapEach(orderDetailShip);

                itemClaimDetailPop.searchClaim();
            }
        });
    },

    /**
     * 데이터 검색 (취소)
     */
    searchClaim: function() {
        var $modal = itemClaimDetailPop.getRoot();
        var checkRowIds = parentGrid.gridView.getSelectedRows();
        var purchaseOrderNo = parentGrid.dataProvider.getValue(checkRowIds[0],"purchaseOrderNo");
        var bundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"bundleNo");

        //클레임 상세정보 조회
        CommonAjax.basic({
            url:'/claim/getClaimDetailList.json?purchaseOrderNo=' + purchaseOrderNo + "&bundleNo=" + bundleNo,
            data: null,
            contentType: 'application/json',
            method: "GET",
            callbackFunc: function(res) {

                //클레임 건수 체크
                if(res.length == 0){
                    $('#claimDetailTap').hide();
                }else{
                    $('#nonClaimText').hide();
                    var cnt = 1;
                    for(var i=0; i<res.length; i++){
                        res[i].purchaseOrderNo = purchaseOrderNo;
                        itemClaimDetailPop.detailClaimTemplate(res[i], cnt);
                        cnt++;
                    }
                }

                // modal open
                $ui.modalOpen('itemClaimDetailPop');
            }
        });
    },

    /**
     * mapData key로 id매핑
     */
    mapEach: function (map) {
        var $modal = itemClaimDetailPop.getRoot();

        $.each(map,function(key, value){
            $modal.find('#'+key).html(value);
        });
    },

    /**
     * 주문상품정보 템플릿
     */
    detailItemTemplate: function(orderDetailItem) {
        var $modal = itemClaimDetailPop.getRoot();
        var innerHtml = [];

        innerHtml.push('<tr style="border-top: 1px solid #e0e0e0">');
        innerHtml.push('    <th scope="row">상품번호</th>');
        innerHtml.push('    <td id="itemNo">' + orderDetailItem.itemNo + '</td>');
        innerHtml.push('    <th scope="row">상품주문번호</th>');
        innerHtml.push('    <td id="orderItemNo">' + orderDetailItem.orderItemNo + '</td>');
        innerHtml.push('    <th scope="row">주문상태</th>');
        innerHtml.push('    <td id="shipStatusNm">' + orderDetailItem.shipStatusNm + '</td>');
        innerHtml.push('</tr>');
        innerHtml.push('<tr>');
        innerHtml.push('    <th scope="row">상품명</th>');
        innerHtml.push('    <td colspan="6" id="itemNm1">' + orderDetailItem.itemNm1 + '</td>');
        innerHtml.push('</tr>');
        innerHtml.push('<tr>');
        innerHtml.push('    <th scope="row">옵션명</th>');
        innerHtml.push('    <td colspan="6" id="txtOptVal">' + orderDetailItem.txtOptVal + '</td>');
        innerHtml.push('</tr>');
        innerHtml.push('<tr>');
        innerHtml.push('    <th scope="row">상품금액</th>');
        innerHtml.push('    <td id="itemPrice">' + orderDetailItem.itemPrice + '</td>');
        innerHtml.push('    <th scope="row">수량(최초/최종)</th>');
        innerHtml.push('    <td id="itemQty">' + orderDetailItem.orgItemQty + '/' + orderDetailItem.itemQty + '</td>');
        innerHtml.push('    <th scope="row">총상품금액</th>');
        innerHtml.push('    <td id="orderPrice">' + orderDetailItem.orderPrice + '</td>');
        innerHtml.push('</tr>');
        innerHtml.push('<tr>');
        innerHtml.push('    <th scope="row">배송방법</th>');
        innerHtml.push('    <td id="shipMethodNm">' + orderDetailItem.shipMethodNm + '</td>');
        innerHtml.push('    <th scope="row">택배사/송장</th>');
        innerHtml.push('    <td colspan="3" id="dlvCd_invoiceNo_completeDt">' + orderDetailItem.dlvCd_invoiceNo_completeDt + '</td>');
        innerHtml.push('</tr>');
        innerHtml.push('<tr style="height: 20px">');
        innerHtml.push('</tr >');

        $modal.find('#tbodyItemList').append(innerHtml.join(''));
    },

    /**
     * 클레임상세정보 템플릿
     */
    detailClaimTemplate: function(claimDetail, cnt) {
        var $modal = itemClaimDetailPop.getRoot();
        var innerHtml = [];

        //클레임 사유 null 체크, db select 시 null 체크가 힘들어서 여기서 함
        var claimReasonType = $.jUtil.isEmpty(claimDetail.claimReasonType) ? '' : claimDetail.claimReasonType;
        var pendingReasonType = $.jUtil.isEmpty(claimDetail.pendingReasonType) ? '' : claimDetail.pendingReasonType;
        var rejectReasonType = $.jUtil.isEmpty(claimDetail.rejectReasonType) ? '' : claimDetail.rejectReasonType;

        innerHtml.push('<tr style="border-top: 1px solid #e0e0e0">');
        innerHtml.push('    <th scope="row">순번</th>');
        innerHtml.push('    <td>' + cnt + '</td>');
        innerHtml.push('    <th scope="row">클레임 구분</th>');
        innerHtml.push('    <td>주문' + claimDetail.claimType + '</td>');
        innerHtml.push('    <th scope="row">처리 상태</th>');
        innerHtml.push('    <td>' + claimDetail.claimType + '' + claimDetail.claimStatus + '</td>');
        innerHtml.push('</tr>');
        innerHtml.push('<tr>');
        innerHtml.push('    <th scope="row">클레임번호</th>');
        innerHtml.push('    <td>' + claimDetail.claimBundleNo + '</td>');
        innerHtml.push('    <th scope="row">배송번호</th>');
        innerHtml.push('    <td>' + claimDetail.bundleNo + '</td>');
        innerHtml.push('    <th scope="row">주문번호</th>');
        innerHtml.push('    <td>' + claimDetail.purchaseOrderNo + '</td>');
        innerHtml.push('</tr>');
        innerHtml.push('<tr>');
        innerHtml.push('    <th scope="row">신청일시</th>');
        innerHtml.push('    <td colspan="2">' + claimDetail.requestDt + '</td>');
        innerHtml.push('    <th scope="row">신청자</th>');
        innerHtml.push('    <td colspan="2">' + claimDetail.requestId + '</td>');
        innerHtml.push('</tr>');
        innerHtml.push('<tr>');
        innerHtml.push('    <th scope="row">신청사유</th>');
        innerHtml.push('    <td colspan="6">' + claimReasonType + '</td>');
        innerHtml.push('</tr>');
        innerHtml.push('<tr>');
        innerHtml.push('    <th scope="row">신청상세사유</th>');
        innerHtml.push('    <td colspan="6">' + claimDetail.claimReasonDetail + '</td>');
        innerHtml.push('</tr>');

        if(!$.jUtil.isEmpty(rejectReasonType)){
            innerHtml.push('<tr>');
            innerHtml.push('    <th scope="row">거부사유</th>');
            innerHtml.push('    <td colspan="6">' + rejectReasonType + '</td>');
            innerHtml.push('</tr>');
            innerHtml.push('<tr>');
            innerHtml.push('    <th scope="row">처리일시</th>');
            innerHtml.push('    <td colspan="2">' + claimDetail.rejectDt + '</td>');
            innerHtml.push('    <th scope="row">처리자</th>');
            innerHtml.push('    <td colspan="2">' + claimDetail.rejectId + '</td>');
            innerHtml.push('</tr>');
            innerHtml.push('<tr>');
            innerHtml.push('    <th scope="row">거부상세사유</th>');
            innerHtml.push('    <td colspan="6">' + claimDetail.rejectReasonDetail + '</td>');
            innerHtml.push('</tr>');
        }

        if(!$.jUtil.isEmpty(pendingReasonType)){
            innerHtml.push('<tr>');
            innerHtml.push('    <th scope="row">보류사유</th>');
            innerHtml.push('    <td colspan="6">' + pendingReasonType + '</td>');
            innerHtml.push('</tr>');
            innerHtml.push('<tr>');
            innerHtml.push('    <th scope="row">처리일시</th>');
            innerHtml.push('    <td colspan="2">' + claimDetail.pendingDt + '</td>');
            innerHtml.push('    <th scope="row">처리자</th>');
            innerHtml.push('    <td colspan="2">' + claimDetail.pendingId + '</td>');
            innerHtml.push('</tr>');
            innerHtml.push('<tr>');
            innerHtml.push('    <th scope="row">보류상세사유</th>');
            innerHtml.push('    <td colspan="6">' + claimDetail.pendingReasonDetail + '</td>');
            innerHtml.push('</tr>');
        }

        innerHtml.push('<tr>');
        innerHtml.push('    <th scope="row">상품주문번호</th>');
        innerHtml.push('    <th scope="row">상품번호</th>');
        innerHtml.push('    <th scope="row" colspan="2">상품명(옵션명)</th>');
        innerHtml.push('    <th scope="row">신청수량</th>');
        innerHtml.push('    <th scope="row">상품금액</th>');
        innerHtml.push('</tr>');

        for(var i=0;i<claimDetail.claimDetailItemList.length;i++){
            var optItemName = '';
            if(!$.jUtil.isEmpty(claimDetail.claimDetailItemList[i].optItemName)){
                optItemName = '(' + claimDetail.claimDetailItemList[i].optItemName + ')';
            }

            innerHtml.push('<tr>');
            innerHtml.push('    <td style="font-size: 12px">' + claimDetail.claimDetailItemList[i].orderItemNo + '</td>');
            innerHtml.push('    <td style="font-size: 12px">' + claimDetail.claimDetailItemList[i].itemNo + '</td>');
            innerHtml.push('    <td style="font-size: 12px; overflow:hidden;white-space:nowrap;text-overflow:ellipsis;" colspan="2">' +   claimDetail.claimDetailItemList[i].itemName + optItemName +'</td>');
            innerHtml.push('    <td style="font-size: 12px">' + claimDetail.claimDetailItemList[i].claimItemQty + '</td>');
            innerHtml.push('    <td style="font-size: 12px">' + $.jUtil.comma(claimDetail.claimDetailItemList[i].itemPrice) + '원 </td>');
            innerHtml.push('</tr>');
        };

        innerHtml.push('<tr>');
        innerHtml.push('    <th scope="row">추가배송비</th>');
        innerHtml.push('    <td colspan="2">' + $.jUtil.comma(claimDetail.addShipPrice) + '원 </td>');
        innerHtml.push('    <th scope="row">클레임 책임</th>');
        innerHtml.push('    <td colspan="2">' + claimDetail.whoReason + '</td>');
        innerHtml.push('</tr>');
        innerHtml.push('<tr style="height: 20px">');
        innerHtml.push('</tr >');

        $modal.find('#tbodyClaimList').append(innerHtml.join(''));
    },

    /**
     * modal open
     */
    openModal: function(tabIndex) {
        var $modal = itemClaimDetailPop.getRoot();

        // 클레임에서 넘어오면 탭 변경
        if(tabIndex != 0) {
            tabIndex = 1;
        }

        // 탭 이벤트
        $modal.find("#itemClaimDetailPopTab").uiTab( {current : tabIndex});

        itemClaimDetailPop.search();
    },

    /**
     * modal close
     */
    closeModal: function() {
        var $modal = itemClaimDetailPop.getRoot();

        $ui.modalClose('itemClaimDetailPop');

        $modal.find('#tbodyItemList').html("");
        $modal.find('#tbodyClaimList').html("");
        $modal.find('.tab-btn').removeClass('active');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#itemClaimDetailPop');
    }
};