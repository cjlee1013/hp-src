
/** Main Script */
var rentalMultiCancelResultPop;
var rentalMultiCancelPop = {

    /**
     * 일괄 취소 요청
     * **/
    save : function(){
        var $modal = rentalMultiCancelPop.getRoot();
        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var multiCancelSetList = [];

        for(var i=0; i< checkRowIds.length; i++){
            var checkedRows = parentGrid.dataProvider.getJsonRow(checkRowIds[i]);
            var multiCancelParam = {
                "purchaseOrderNo" : checkedRows.purchaseOrderNo,
                "bundleNo" : checkedRows.bundleNo,
            }
            multiCancelSetList.push(multiCancelParam);
        }
        var param = {
            "claimReasonType" : $modal.find('#claimReasonType').val(),
            "claimReasonDetail" : $modal.find('#claimReasonDetail').val(),
            "regId" : "PARTNER",
            "itemList" : multiCancelSetList
        }

        if (!confirm('일괄취소 하시겠습니까?')) {
            return false;
        }

        CommonAjax.basic(
        {
            url:'/claim/rentalMultiCancel.json',
            data: JSON.stringify(param),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {

                if(res.returnCode != '0000'){
                    alert('일괄취소 실패 [' + res.returnMessage + ']');
                    return false;
                }
                rentalMultiCancelPop.resultTemplate(multiCancelSetList.length, res.data);
                parentMain.callback();
                rentalMultiCancelPop.closeModal();
                rentalMultiCancelPop.openResultModal();
            }
        });
     },

    /**
     * 저장 후 팝업 템플릿
     */
    resultTemplate: function(totalCnt, resultData) {
        $('#rentalMultiCancelTotalCnt').text(totalCnt);
        $('#rentalMultiCancelSuccessCnt').text(resultData.successCnt);
        $('#rentalMultiCancelFailCnt').text(resultData.failCnt);
    },

    /**
     * 일괄취소 결과 modal open
     */
    openResultModal: function() {
        $ui.modalOpen('rentalMultiCancelResultPop');
    },

    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('rentalMultiCancelPop');
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('rentalMultiCancelPop');
    },

    /**
     * result modal close
     */
    closeResultModal: function() {
        $ui.modalClose('rentalMultiCancelResultPop');
    },
    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#rentalMultiCancelPop');
    }
};
