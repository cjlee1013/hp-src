$(document).ready(function() {
  ticketMultiCancelPop.init();
});



/** Main Script */
var ticketMultiCancelPop = {

    /**
     * init 이벤트
     */
    init: function() {
        ticketMultiCancelPop.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = ticketMultiCancelPop.getRoot();
        // [등록] 버튼
        $modal.find("#saveBtn").bindClick(ticketMultiCancelPop.save);
    },

    /**
     * 일괄 취소 요청
     * **/
    save : function(){
        var $modal = ticketMultiCancelPop.getRoot();

        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var multiCancelList = [];

        for(var i=0; i< checkRowIds.length; i++){
            var checkedRows = parentGrid.dataProvider.getJsonRow(checkRowIds[i]);
            var multiCancelParam = {
                "purchaseOrderNo" : checkedRows.purchaseOrderNo,
                "orderTicketNo" : checkedRows.orderTicketNo,
                "bundleNo" : checkedRows.bundleNo,
            }
            multiCancelList.push(multiCancelParam);
        }
        var param = {
            "claimReasonType" : $modal.find('#claimReasonType').val(),
            "claimReasonDetail" : $modal.find('#claimReasonDetail').val(),
            "regId" : "PARTNER",
            "itemList" : multiCancelList

        }


        console.log(JSON.stringify(param));
        if (!confirm('일괄취소 하시겠습니까?')) {
            return false;
        }

        CommonAjax.basic(
        {
            url:'/claim/ticketMultiCancel.json',
            data: JSON.stringify(param),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {

                if(res.returnCode != '0000'){
                    alert('일괄취소 실패 [' + res.returnMessage + ']');
                    return false;
                }
                ticketMultiCancelPop.resultTemplate(multiCancelList.length, res.data);
                parentMain.callback();
                ticketMultiCancelPop.closeModal();
                ticketMultiCancelPop.openResultModal();
            }
        });

    },

    /**
     * 저장 후 팝업 템플릿
     */
    resultTemplate: function(totalCnt, resultData) {
        $('#ticketCancelTotalCnt').text(totalCnt);
        $('#ticketCancelSuccessCnt').text(resultData.successCnt);
        $('#ticketCancelFailCnt').text(resultData.failCnt);
    },

    /**
     * 승인 결과 modal open
     */
    openResultModal: function() {
        $ui.modalOpen('ticketMultiCancelResultPop');
    },

    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('ticketMultiCancelPop');
    },

    /**
     * modal close
     */
    closeModal: function() {
        const $modal = ticketMultiCancelPop.getRoot();
        $ui.modalClose('ticketMultiCancelPop');
    },

    /**
     * result modal close
     */
    closeResultModal: function() {
        $ui.modalClose('ticketMultiCancelResultPop');
    },
    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#ticketMultiCancelPop');
    }
};
