/** Main Script */
var noReceivePop = {
    /**
     * init 이벤트
     */
    init: function() {
        noReceivePop.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = noReceivePop.getRoot();

        // [등록] 버튼
        $modal.find("#saveBtn").bindClick(noReceivePop.save);
        // 글자수 카운트
        $modal.find("#noRcvProcessCntnt").keyup(function(){ noReceivePop.textCount() });
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var reqData = new Object();

        var bundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"bundleNo");
        var orderItemNo = parentGrid.dataProvider.getValue(checkRowIds[0],"orderItemNo");
        var itemNm1 = parentGrid.dataProvider.getValue(checkRowIds[0],"itemNm1");
        var userNo = parentGrid.dataProvider.getValue(checkRowIds[0],"userNo");
        var purchaseOrderNo = parentGrid.dataProvider.getValue(checkRowIds[0],"purchaseOrderNo");
        var shipNo = parentGrid.dataProvider.getValue(checkRowIds[0],"shipNo");

        var res = [{"bundleNo":bundleNo,"orderItemNo":orderItemNo,"itemNm1":itemNm1}];

        noReceivePopGrid.setData(res);

        reqData.bundleNo = bundleNo;
        reqData.orderItemNo = orderItemNo;
        reqData.itemNm1 = itemNm1;
        reqData.userNo = userNo;
        reqData.purchaseOrderNo = purchaseOrderNo;
        reqData.shipNo = shipNo;

        CommonAjax.basic({
            url:'/sell/shipStatus/getNoReceiveInfo.json',
            data: JSON.stringify(reqData),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                $('#noRcvDeclrDt').html(res.noRcvDeclrDt);
                $('#noRcvRegId').html(res.noRcvRegId);
                $('#noRcvDeclrType').html(res.noRcvDeclrType);
                $('#noRcvDetailReason').html(res.noRcvDetailReason);
            }
        });
    },

    /**
     * 저장
     */
    save: function() {
        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var reqData = new Object();

        var noRcvProcessCntnt = $('#noRcvProcessCntnt');
        if($.jUtil.isEmpty(noRcvProcessCntnt.val().length)){
            alert('미수취 신고 처리 내용을 입력해 주세요.');
            return ;
        }

        reqData.purchaseOrderNo = parentGrid.dataProvider.getValue(checkRowIds[0],"purchaseOrderNo");
        reqData.orderItemNo = parentGrid.dataProvider.getValue(checkRowIds[0],"orderItemNo");
        reqData.shipNo = parentGrid.dataProvider.getValue(checkRowIds[0],"shipNo");
        reqData.bundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"bundleNo");
        reqData.noRcvProcessType = "C";
        reqData.noRcvProcessCntnt = $('#noRcvProcessCntnt').val();

        if (confirm('미수취 신고 철회요청을 하시겠습니까?')) {
            CommonAjax.basic({
                url         : '/sell/shipStatus/setNoReceive.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    alert(res.returnMessage);
                    parentMain.callback();
                    noReceivePop.closeModal();
                }
            });
        }
    },

    /**
     * 글자수 카운트
     */
    textCount: function() {
        var $modal = noReceivePop.getRoot();
        var $delayShipMsg = $modal.find("#noRcvProcessCntnt");
        var content = $delayShipMsg.val();
        var count = content.length;

        $('#msgCount').text(count);
    },

    /**
     * form reset
     */
    formReset: function() {
        var $modal = noReceivePop.getRoot();

        $modal.find('#noRcvProcessCntnt').val("");
        $modal.find('#msgCount').text(0);
    },

    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('noReceivePop', noReceivePop.search);
        noReceivePopGrid.gridView.resetSize();
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('noReceivePop', noReceivePop.formReset);
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#noReceivePop');
    }
};

/** Grid Script */
var noReceivePopGrid = {
    gridView: new RealGridJS.GridView("noReceivePopGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        noReceivePopGrid.initGrid();
        noReceivePopGrid.initDataProvider();
        noReceivePopGrid.customGrid();
        noReceivePopGrid.event();
    },
    initGrid: function () {
        noReceivePopGrid.gridView.setDataSource(noReceivePopGrid.dataProvider);
        noReceivePopGrid.gridView.setStyles(noReceivePopGridBaseInfo.realgrid.styles);
        noReceivePopGrid.gridView.setDisplayOptions(noReceivePopGridBaseInfo.realgrid.displayOptions);
        noReceivePopGrid.gridView.setColumns(noReceivePopGridBaseInfo.realgrid.columns);
        noReceivePopGrid.gridView.setOptions(noReceivePopGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        noReceivePopGrid.dataProvider.setFields(noReceivePopGridBaseInfo.dataProvider.fields);
        noReceivePopGrid.dataProvider.setOptions(noReceivePopGridBaseInfo.dataProvider.options);
    },
    customGrid: function () {
    },
    event: function() {

    },
    setData: function (dataList) {
        noReceivePopGrid.dataProvider.clearRows();
        noReceivePopGrid.dataProvider.setRows(dataList);
    }
};