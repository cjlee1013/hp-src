/** Main Script */
var shipAllExcelPop = {
    /**
     * init 이벤트
     */
    init: function() {
        shipAllExcelPop.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = shipAllExcelPop.getRoot();

        // [파일찾기] 버튼
        $modal.find("#searchUploadFile").bindClick(shipAllExcelPop.searchFile);
        // [저장] 버튼
        $modal.find("#saveBtn").bindClick(shipAllExcelPop.save);
        // [엑셀양식 다운로드] 버튼
        $modal.find("#templateDlv, #templateDrct").on("click", function() { shipAllExcelPop.templateDownload($(this)) });
        // [배송방법] 버튼
        $modal.find("input[name='shipMethod']").on("click", function() { shipAllExcelPop.setShipMethod($(this)) });
    },

    /**
     * [파일찾기] 버튼 클릭 (윈도우 파일 찾기 open)
     */
    searchFile: function() {
        var $modal = shipAllExcelPop.getRoot();

        $modal.find("#uploadFile").click();
    },

    /**
     * 선택한 파일 정보 불러오기
     */
    getFile: function(_obj) {
        var $modal = shipAllExcelPop.getRoot();
        var fileInfo = _obj[0].files[0];
        var fileName = fileInfo.name;

        $modal.find("#uploadFileNm").text(fileName);
    },

    /**
     * 엑셀양식 다운로드
     */
    templateDownload: function($this) {
        var id = $this.attr("id");

        if (id == "templateDlv") {
            location.href = "/static/templates/shipAll_dlv_template.xlsx";
        } else {
            location.href = "/static/templates/shipAll_drct_template.xlsx";
        }
    },

    /**
     * 배송방법 클릭
     */
    setShipMethod: function($this) {
        var $modal = shipAllExcelPop.getRoot();
        var method = $this.val();

        if (method == "DLV") {
            $modal.find("#dlvCd").prop('disabled',false);
        } else {
            $modal.find("#dlvCd option:eq(0)").prop("selected", true);
            $modal.find("#dlvCd").prop('disabled',true);
        }
    },

    /**
     * 저장
     */
    save: function() {
        var $modal = shipAllExcelPop.getRoot();
        var uploadFileVal = $modal.find("#uploadFile").val();
        var uploadFileNmVal = $modal.find("#uploadFileNm").text();
        var fileExt = uploadFileNmVal.split(".")[1];
        var dlvCd = $modal.find("#dlvCd").val();
        var shipMethod = $modal.find("input[name='shipMethod']:checked").val();

        if ($.jUtil.isEmpty(shipMethod)) {
            alert("배송방법을 입력해주세요.");
            return;
        }

        if ($.jUtil.isEmpty(dlvCd) && shipMethod == "DLV") {
            alert("택배사를 선택해주세요.");
            return;
        }

        if ($.jUtil.isEmpty(uploadFileVal) || $.jUtil.isEmpty(uploadFileNmVal)) {
            alert("파일을 등록해주세요.");
            return;
        }

        if (fileExt.indexOf("xlsx") < 0 && fileExt.indexOf("xls") < 0) {
            alert("지정된 엑셀 양식(.xlsx .xls)으로 업로드해주세요.");
            return false;
        }

        if (confirm("저장하시겠습니까?")) {
            $modal.find("#fileUploadForm").ajaxForm({
                url     : "/sell/orderShipManage/setShipAllExcel.json",
                method  : "POST",
                enctype : "multipart/form-data",
                success : function(res) {
                    var resData = res.data;

                    if (res.returnCode != "SUCCESS") {
                        alert(res.returnMessage);
                    } else {
                        shipAllExcelPop.resultTemplate(resData.excelTotalCnt, resData.successCnt);
                        shipAllExcelPopGrid.init(shipMethod);
                        shipAllExcelPopGrid.setData(resData.failList);

                        orderShipManageMain.callback();
                        shipAllExcelPop.openResultModal();
                    }

                    shipAllExcelPop.closeModal();
                },
                error : function(resError) {
                    alert('오류가 발생하였습니다.');
                    shipAllExcelPop.closeModal();
                }
            }).submit();
        }
    },

    /**
     * 저장 후 팝업 템플릿
     */
    resultTemplate: function(totalCnt, successCnt) {
        var $resultModal = shipAllExcelPop.getResultRoot();
        var failCnt = parseInt(totalCnt) - parseInt(successCnt);

        $resultModal.find('#totalCnt').text(totalCnt);
        $resultModal.find('#successCnt').text(successCnt);
        $resultModal.find('#failCnt').text(failCnt);
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (shipAllExcelPopGrid.gridView.getItemCount() == 0) {
            alert("실패건이 없습니다.");
            return;
        }

        var _date = new Date();
        var fileName =  "일괄발송처리실패목록_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        shipAllExcelPopGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * form reset
     */
    formReset: function() {
        var $modal = shipAllExcelPop.getRoot();

        $modal.find('input:radio[name=shipMethod]').prop("checked", false);
        $modal.find("#dlvCd option:eq(0)").prop("selected", true);
        $modal.find("#uploadFileNm").text("");
        $modal.find("#uploadFile").val("");
    },

    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('shipAllExcelPop');
    },

    /**
     * result modal open
     */
    openResultModal: function() {
        $ui.modalOpen('shipAllExcelResultPop');
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('shipAllExcelPop', shipAllExcelPop.formReset);
    },

    /**
     * result modal close
     */
    closeResultModal: function() {
        $ui.modalClose('shipAllExcelResultPop');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#shipAllExcelPop');
    },

    /**
     * result modal root selector
     */
    getResultRoot: function() {
        return $('#shipAllExcelResultPop');
    }

}

/** Grid Script */
var shipAllExcelPopGrid = {
    gridView: new RealGridJS.GridView("shipAllExcelPopGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function (shipMethod) {
        shipAllExcelPopGrid.initGrid();
        shipAllExcelPopGrid.initDataProvider();
        shipAllExcelPopGrid.event();

        // 택배배송, 직접배송 필요한 컬럼만 남긴다
        if (shipMethod == "DLV") {
            shipAllExcelPopGrid.gridView.removeColumn("scheduleShipDt");
        } else {
            shipAllExcelPopGrid.gridView.removeColumn("invoiceNo");
        }
    },
    initGrid: function () {
        shipAllExcelPopGrid.gridView.setDataSource(shipAllExcelPopGrid.dataProvider);
        shipAllExcelPopGrid.gridView.setStyles(shipAllExcelPopGridBaseInfo.realgrid.styles);
        shipAllExcelPopGrid.gridView.setDisplayOptions(shipAllExcelPopGridBaseInfo.realgrid.displayOptions);
        shipAllExcelPopGrid.gridView.setColumns(shipAllExcelPopGridBaseInfo.realgrid.columns);
        shipAllExcelPopGrid.gridView.setOptions(shipAllExcelPopGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        shipAllExcelPopGrid.dataProvider.setFields(shipAllExcelPopGridBaseInfo.dataProvider.fields);
        shipAllExcelPopGrid.dataProvider.setOptions(shipAllExcelPopGridBaseInfo.dataProvider.options);
    },
    event: function() {
        shipAllExcelPopGrid.gridView.onDataCellClicked = function(gridView, index) {
            //shipManageMain.gridRowSelect(index.dataRow);
        };
    },
    setData: function (dataList) {
        shipAllExcelPopGrid.dataProvider.clearRows();
        shipAllExcelPopGrid.dataProvider.setRows(dataList);
    }
}