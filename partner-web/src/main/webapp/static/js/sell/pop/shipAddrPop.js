/** Main Script */
var shipAddrPop = {
    /**
     * init 이벤트
     */
    init: function() {
        var $modal = shipAddrPop.getRoot();

        shipAddrPop.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = shipAddrPop.getRoot();

        // [등록] 버튼
        $modal.find("#saveBtn").bindClick(shipAddrPop.save);

        // [우편번호(주소검색)] 버튼
        $modal.find("#searchAddr").bindClick(shipAddrPop.searchAddr);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var bundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"bundleNo");

        CommonAjax.basic({
            url         : '/sell/orderShipManage/getOrderShipAddrInfo.json?bundleNo=' + bundleNo,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                var data = res[0];

                for(const key of Object.keys(data)){
                    const keyValue = $('#' + key);
                    if(keyValue.length){
                        if($('input[id=' + key + "]").length){
                            keyValue.val(data[key]);
                        } else {
                            keyValue.text(data[key]);
                        }
                    }
                }

                $('#addr').text("[지번] " + data.baseAddr + " " + data.detailAddr);
                $.jUtil.valSplit($('#shipMobileNo').val(), '-', 'shipMobileNo');
            }
        });
    },

    /**
     * 주소 검색
     */
    searchAddr: function() {
        zipCodePopup("shipAddrPop.callback");
    },

    /**
     * 저장
     */
    save: function() {
        var $modal = shipAddrPop.getRoot();
        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var bundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"bundleNo");

        var receiverNm = $modal.find('#receiverNm').val();
        if ($.jUtil.isEmpty(receiverNm)) {
            alert("수령인을 입력해 주세요");
            return;
        }

        var shipMobileNo = $modal.find('#shipMobileNo_1').val() + "-" + $modal.find('#shipMobileNo_2').val() + "-" + $modal.find('#shipMobileNo_3').val()
        if (!_V.chkCellPhoneNum(shipMobileNo,true)) {
            alert("연락처를 확인해 주세요");
            return;
        }

        var roadDetailAddr = $modal.find('#roadDetailAddr').val();
        if ($.jUtil.isEmpty(roadDetailAddr)) {
            alert("상세주소를 입력해 주세요");
            return;
        }

        var reqData = $modal.find('#shipAddrPopForm').serializeObject();

        reqData.bundleNo = bundleNo;
        reqData.shipMobileNo = shipMobileNo;
        reqData.detailAddr = roadDetailAddr;

        if (confirm("저장하시겠습니까?")) {
            CommonAjax.basic({
                url         : '/sell/orderShipManage/setOrderShipAddrInfo.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    alert(res.returnMessage);
                    if(res.returnCode === 'SUCCESS' || res.returnCode === '0000'){
                        orderShipManageMain.callback();
                        shipAddrPop.closeModal();
                    }
                }
            });
        }
    },

    /**
     * callback function
     */
    callback : function (returnObj) {
        var $modal = shipAddrPop.getRoot();

        $modal.find('#addr').text("[지번] " + returnObj.gibunAddr);
        $modal.find('#baseAddr').val(returnObj.gibunAddr);
        $modal.find('#roadBaseAddr').val(returnObj.roadAddr);
    },

    /**
     * form reset
     */
    formReset: function() {
        var $modal = shipAddrPop.getRoot();

        $modal.find('#shipAddrPopForm').resetForm();
    },

    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('shipAddrPop', shipAddrPop.search);
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('shipAddrPop', shipAddrPop.formReset);
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#shipAddrPop');
    }
};