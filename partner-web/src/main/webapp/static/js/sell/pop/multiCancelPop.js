
/** Main Script */
var multiCancelResultPop;
var multiCancelPop = {

    /**
     * 일괄 취소 요청
     * **/
    save : function(){
        var $modal = multiCancelPop.getRoot();
        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var multiCancelSetList = [];

        for(var i=0; i< checkRowIds.length; i++){
            var checkedRows = parentGrid.dataProvider.getJsonRow(checkRowIds[i]);
            var multiCancelParam = {
                "purchaseOrderNo" : checkedRows.purchaseOrderNo,
                "bundleNo" : checkedRows.bundleNo,
                "orderItemNo" : checkedRows.orderItemNo

            }
            multiCancelSetList.push(multiCancelParam);
        }
        var param = {
            "claimReasonType" : $modal.find('#claimReasonType').val(),
            "claimReasonDetail" : $modal.find('#claimReasonDetail').val(),
            "regId" : "PARTNER",
            "itemList" : multiCancelSetList
        }

        if (!confirm('일괄취소 하시겠습니까?')) {
            return false;
        }

        CommonAjax.basic(
        {
            url:'/claim/orderShipMultiCancel.json',
            data: JSON.stringify(param),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {

                if(res.returnCode != '0000'){
                    alert('일괄취소 실패 [' + res.returnMessage + ']');
                    return false;
                }
                multiCancelPop.resultTemplate(multiCancelSetList.length, res.data);
                parentMain.callback();
                multiCancelPop.closeModal();
                multiCancelPop.openResultModal();
            }
        });
     },

    /**
     * 저장 후 팝업 템플릿
     */
    resultTemplate: function(totalCnt, resultData) {
        $('#multiCancelTotalCnt').text(totalCnt);
        $('#multiCancelSuccessCnt').text(resultData.successCnt);
        $('#multiCancelFailCnt').text(resultData.failCnt);
    },

    /**
     * 일괄취소 결과 modal open
     */
    openResultModal: function() {
        $ui.modalOpen('multiCancelResultPop');
    },

    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('multiCancelPop');
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('multiCancelPop');
    },

    /**
     * result modal close
     */
    closeResultModal: function() {
        $ui.modalClose('multiCancelResultPop');
    },
    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#multiCancelPop');
    }
};
