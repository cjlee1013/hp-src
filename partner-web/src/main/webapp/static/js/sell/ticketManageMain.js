/** Main Script */
var ticketManageMain = {
    /**
     * init 이벤트
     */
    init: function() {
        ticketManageMain.setBoardCount();
        ticketManageMain.bindingEvent();
        sellUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $main = ticketManageMain.getRoot();

        // 새로고침 버튼
        $main.find("#boardRefresh").on("click", function() { ticketManageMain.setBoardCount() });
        // 보드 카운트 클릭 조회
        $main.find("#readyCnt, #callCnt, #failCnt").on("click", function() { ticketManageMain.searchBoard($(this)) });
        // 검색 버튼
        $main.find("#schBtn").bindClick(ticketManageMain.search);
        // 초기화 버튼
        $main.find("#schResetBtn").bindClick(ticketManageMain.reset, "ticketManageSearchForm");
        // 엑셀다운 버튼
        $main.find("#schExcelDownloadBtn").bindClick(ticketManageMain.excelDownload);

        // 달력 - [오늘] 버튼
        $main.find("#setTodayBtn").on("click", () => {sellUtil.initSearchDateCustom("schStartDt", "schEndDt","0", "0")});
        // 달력 - [1주일] 버튼
        $main.find("#setOneWeekBtn").on("click", () => {sellUtil.initSearchDateCustom("schStartDt", "schEndDt","-1w", "0")});
        // 달력 - [1개월] 버튼
        $main.find("#setOneMonthBtn").on("click", () => {sellUtil.initSearchDateCustom("schStartDt", "schEndDt","-1m", "0")});

        // 주문취소 버튼
        $main.find("#orderCancel").bindClick(ticketManageMain.setOrderCancel);
        // 주문확인 버튼
        $main.find("#orderConfirm").bindClick(ticketManageMain.setOrderConfirm);
        // 발급완료 버튼
        $main.find("#ticketComplete").bindClick(ticketManageMain.setTicketComplete);
    },

    /**
     * Board 카운트 설정
     */
    setBoardCount: function() {
        var $main = ticketManageMain.getRoot();

        CommonAjax.basic({
            url         : '/sell/ticketManage/getTicketManageCnt.json',
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                ticketManageMain.mapEach(res);
            }
        });
    },

    /**
     * Board 데이터 검색
     */
    searchBoard: function($this) {
        var $main = ticketManageMain.getRoot();
        var schType = $this.attr('id');

        CommonAjax.basic({
            url         : '/sell/ticketManage/getTicketManageCntList.json?schType=' + schType,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                ticketManageGrid.setData(res);
                $main.find('#ticketManageSearchCnt').html($.jUtil.comma(ticketManageGrid.gridView.getItemCount()));
            },
            errorCallbackFunc: function () {
                alert("이티켓판매관리 카운트 리스트 조회 실패");
            }
        });
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var $main = ticketManageMain.getRoot();

        if (!validCheck.search()) {
            return;
        }

        var reqData = $main.find("#ticketManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/sell/ticketManage/getTicketManageList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                ticketManageGrid.setData(res);
                $main.find('#ticketManageSearchCnt').html($.jUtil.comma(ticketManageGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 주문취소
     */
    setOrderCancel : function() {
        var checkRowIds = ticketManageGrid.gridView.getCheckedRows();

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }
        ticketMultiCancelPop.openModal();
    },

    /**
     * 주문확인
     */
    setOrderConfirm : function() {
        var checkRowIds = ticketManageGrid.gridView.getCheckedRows();
        var reqData = new Object();
        var shipNoList = new Array();
        var reqCnt = 0;

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        for (var checkRowId of checkRowIds) {
            var checkedRows = ticketManageGrid.dataProvider.getJsonRow(checkRowId);

            if (checkedRows.linkType != 'MANUAL' && checkedRows.issueChannel != 'ETC') {
                alert("자체판매상품만 가능합니다.");
                return;
            }

            if (checkedRows.shipStatus != 'D1') {
                alert("신규주문 건만 진행할 수 있습니다.");
                return;
            }

            shipNoList.push(checkedRows.shipNo);
        }

        // shipNo 중복 제거
        shipNoList = Array.from(new Set(shipNoList));
        reqData.shipNoList = shipNoList;

        reqCnt = shipNoList.length;

        if (confirm("선택하신 주문건에 대해서 주문확인을 진행하겠습니까?")) {
            CommonAjax.basic({
                url         : '/sell/ticketManage/setTicketConfirmOrder.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    var failCnt = parseInt(reqCnt) - parseInt(res.data);

                    if (res.returnCode != "SUCCESS" || failCnt > 0) {
                        alert("주문확인 처리 중 "+failCnt+"건 실패했습니다.");
                    } else {
                        alert("주문확인을 했습니다.");
                    }

                    ticketManageMain.callback();
                }
            });
        }
    },

    /**
     * 발급완료
     */
    setTicketComplete : function() {
        var checkRowIds = ticketManageGrid.gridView.getCheckedRows();
        var reqData = new Object();
        var allShipNoList = new Array();
        var shipNoList = new Array();
        var reqCnt = 0;

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        for (var checkRowId of checkRowIds) {
            var checkedRows = ticketManageGrid.dataProvider.getJsonRow(checkRowId);

            if (checkedRows.linkType != 'MANUAL' && !checkedRows.issueChannel != 'ETC') {
                alert("자체판매상품만 가능합니다.");
                return;
            }

            if (checkedRows.issueStatus != 'I1') {
                alert("발급대기 건만 진행할 수 있습니다.");
                return;
            }

            if (checkedRows.ticketStatus != 'T0') {
                alert(ticketStatusNm + " 상태는 진행할 수 없습니다.");
                return;
            }

            allShipNoList.push(checkedRows.shipNo);
        }

        // shipNo 중복 제거
        allShipNoList = Array.from(new Set(allShipNoList));

        for (var shipNo of allShipNoList) {
            var completeArgData = new Object();
            var orderTicketNoList = new Array();

            completeArgData.shipNo = shipNo;

            for (var checkRowId of checkRowIds) {
                var checkedRows = ticketManageGrid.dataProvider.getJsonRow(checkRowId);

                if (shipNo == checkedRows.shipNo) {
                    orderTicketNoList.push(checkedRows.orderTicketNo);
                }
            }

            completeArgData.orderTicketNoList = orderTicketNoList;
            shipNoList.push(completeArgData);
        }

        reqData.shipNoList = shipNoList;
        reqCnt = shipNoList.length;

        if (confirm("선택하신 주문건에 대해서 발급완료를 진행하겠습니까?")) {
            CommonAjax.basic({
                url         : '/sell/ticketManage/setTicketComplete.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    var failCnt = parseInt(reqCnt) - parseInt(res.data);

                    if (res.returnCode != "SUCCESS" || failCnt > 0) {
                        alert("발급완료 처리 중 "+failCnt+"건 실패했습니다.");
                    } else {
                        alert("발급완료을 했습니다.");
                    }

                    ticketManageMain.callback();
                }
            });
        }
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (ticketManageGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var confirmMsg = "구매자의 개인 정보는 암호화 하여야 하며, 판매 목적을 달성한 경우 즉시 파기 되어야 합니다.\n"
            + "구매자의 개인정보를 이용하여 광고 홍보등의 수집 목적 외로 사용해서는 안됩니다.\n"
            + "상기 사항 등 관계 법령 위반으로 발생하는 모든 민,형사상 책임은 판매자 본인에게 있습니다.\n\n"
            + "상기 내용을 숙지하였으며, 이에 동의합니다.";
        if (confirm(confirmMsg)) {
            var _date = new Date();
            var fileName = "이티켓판매관리_" + _date.getFullYear() + (_date.getMonth()
                + 1) + _date.getDate();
            ticketManageGrid.gridView.exportGrid({
                type: "excel",
                target: "local",
                fileName: fileName + ".xlsx",
                showProgress: true,
                progressMessage: "엑셀 Export 중 입니다."
            });
        }
    },


    /**
     * mapData key로 id매핑
     */
    mapEach: function (map) {
        var $main = ticketManageMain.getRoot();

        $.each(map,function(key, value){
            $main.find('#'+key).html(value);
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();

        sellUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");
    },

    /**
     * root selector
     */
    getRoot: function() {
        return $('#ticketManageMain');
    },

    /**
     * callback function
     */
    callback : function () {
        ticketManageMain.search();
        ticketManageMain.setBoardCount();
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {
        var $main = ticketManageMain.getRoot();

        //  검색조건 (상품주문번호, 주문번호)
        //  선택 후 검색어 입력시, 다른 검색조건 무시
        var schKeywordVal = $main.find("#schKeyword").val();
        var schKeywordTypeVal = $main.find("#schKeywordType").val();

        if (!$.jUtil.isEmpty(schKeywordVal)
            && (schKeywordTypeVal == 'orderItemNo'
                || schKeywordTypeVal == 'purchaseOrderNo')) {
            return true;
        }

        // 검색기간은 최대 3개월
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            //sellUtil.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
            return false;
        }

        if (schEndDt.diff(schStartDt, "month", true) > 3) {
            alert("3개월 이내만 검색 가능합니다.");
            //$schStartDt.val(schEndDt.add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        return true;
    }
}

/** Grid Script */
var ticketManageGrid = {
    gridView: new RealGridJS.GridView("ticketManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        ticketManageGrid.initGrid();
        ticketManageGrid.initDataProvider();
        ticketManageGrid.event();
    },
    initGrid: function () {
        ticketManageGrid.gridView.setDataSource(ticketManageGrid.dataProvider);
        ticketManageGrid.gridView.setStyles(ticketManageGridBaseInfo.realgrid.styles);
        ticketManageGrid.gridView.setDisplayOptions(ticketManageGridBaseInfo.realgrid.displayOptions);
        ticketManageGrid.gridView.setColumns(ticketManageGridBaseInfo.realgrid.columns);
        ticketManageGrid.gridView.setOptions(ticketManageGridBaseInfo.realgrid.options);

        // 상품번호 렌더링
        ticketManageGrid.gridView.setColumnProperty("itemNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "itemNo", showUrl: false});
    },
    initDataProvider: function () {
        ticketManageGrid.dataProvider.setFields(ticketManageGridBaseInfo.dataProvider.fields);
        ticketManageGrid.dataProvider.setOptions(ticketManageGridBaseInfo.dataProvider.options);
    },
    event: function() {
        ticketManageGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var itemNo = grid.getValue(index.itemIndex, "itemNo");
            var siteType = grid.getValue(index.itemIndex, "siteType");

            // 상품상세 이동
            if (index.fieldName == "itemNo") {
                var frontUrl = siteType == "CLUB" ? theclubFrontUrl : homeplusFrontUrl;
                window.open(frontUrl + "/item?itemNo=" + itemNo + "&storeType=DS");
            }
        };
    },
    setData: function (dataList) {
        ticketManageGrid.dataProvider.clearRows();
        ticketManageGrid.dataProvider.setRows(dataList);

        //페이징 처리
        sellUtil.onRealGridPaging(ticketManageGrid.gridView, "pagingArea", $('#pageViewCnt').val(), 10);

        for(let idx in dataList){
            if (dataList[idx].issueChannel == 'COOP') {
                ticketManageGrid.gridView.setCheckable(idx, false);
            }
        }
    }
};