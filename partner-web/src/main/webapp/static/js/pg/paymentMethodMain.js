/**
 * 결제수단 관리
 */

/**
 * Real Grid 초기화
 */
var paymentMethodGrid = {
    realGrid: new RealGridJS.GridView("paymentMethodGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        paymentMethodGrid.initGrid();
        paymentMethodGrid.initDataProvider();
        paymentMethodGrid.event();
    },
    initGrid: function () {
        paymentMethodGrid.realGrid.setDataSource(paymentMethodGrid.dataProvider);
        paymentMethodGrid.realGrid.setStyles(paymentMethodGridBaseInfo.realgrid.styles);
        paymentMethodGrid.realGrid.setDisplayOptions(paymentMethodGridBaseInfo.realgrid.displayOptions);
        paymentMethodGrid.realGrid.setColumns(paymentMethodGridBaseInfo.realgrid.columns);
        paymentMethodGrid.realGrid.setOptions(paymentMethodGridBaseInfo.realgrid.options);
        paymentMethodGrid.realGrid.setEditOptions();
    },
    initDataProvider: function () {
        paymentMethodGrid.dataProvider.setFields(paymentMethodGridBaseInfo.dataProvider.fields);
        paymentMethodGrid.dataProvider.setOptions(paymentMethodGridBaseInfo.dataProvider.options);
    },
    event: function () {
        paymentMethodGrid.realGrid.onDataCellClicked = function () {
            paymentMethodMng.drawInputForm(paymentMethodGrid.dataProvider.getJsonRow(paymentMethodGrid.realGrid.getCurrent().dataRow));
        };
    },
    setData: function (dataList) {
        paymentMethodGrid.dataProvider.clearRows();
        paymentMethodGrid.dataProvider.setRows(dataList);
    },
    excelDownloadAll: function () {
        // 전체 결제수단 조회
        paymentMethodGrid.dataProvider.clearRows();
        CommonAjax.basic({
            url: "/pg/paymentMethod/getPaymentMethodList.json?schParentMethodCd=ALL&schSiteType=ALL"
            , method: "GET"
            , callbackFunc: function (dataList) {
                paymentMethodGrid.dataProvider.addRows(dataList);
            }
        });

        // 전체 결제수단 엑셀다운
        setTimeout(function () {
            if (paymentMethodGrid.realGrid.getItemCount() == 0) {
                alert("전체 결제수단이 조회되지 않았습니다.");
                return false;
            }

            var _date = new Date();
            var fileName = "결제수단관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

            paymentMethodGrid.realGrid.exportGrid({
                type: "excel",
                target: "local",
                fileName: fileName + ".xlsx",
                showProgress: true,
                progressMessage: "엑셀 Export중입니다."
            });
        }, 1500);
    }
};

var paymentMethodMng = {
    init: function () {
        paymentMethodMng.bindEvents();
    },
    /** event binding */
    bindEvents: function () {
        // 저장
        $("#savePaymentMethodBtn").on("click", function () {
            // 입력값 validation
            if (!paymentMethodMng.valid()) {
                return false;
            }
            if(!confirm("저장 하시겠습니까?")) {
                return false;
            }
            paymentMethodMng.insert();
        });
        // 초기화
        $("#searchResetBtn").on("click", function () {
            paymentMethodMng.reset();
        });
        // 엑셀 다운로드
        $("#excelDownloadAllBtn").on("click", function () {
            paymentMethodGrid.excelDownloadAll();
        });
        // 상세결제수단코드 변경 시 중복체크
        $("#methodCd").change(function () {
            if ($("#siteType").val() == "" || $("#siteType").val() == null) {
                alert("사이트를 먼저 선택해주세요.");
                $("#methodCd").val("");
                $('select[name^="siteType"]').focus();
            } else if ($("#orgCode").val() != $("#methodCd").val()) {
                CommonAjax.basic({
                    url: "/pg/paymentMethod/checkDuplicateCode.json?methodCd=" + $("#methodCd").val() + "&siteType=" + $("#siteType").val()
                    , method: "GET"
                    , datatype: "json"
                    , callbackFunc: function (res) {
                        if ($.jUtil.objSize(res) > 0) {
                            alert("중복 코드입니다. 변경해주세요.");
                            $("#methodCd").val($("#orgCode").val());
                            $("#methodCd").focus();
                        }
                    }
                });
            }
        });
        // 사용여부 변경 시 노출순서 값 고정 (99999)
        $("#useYn").change(function () {
            if ($("#useYn").val() == "N") {
                $("#sortSeq").val("99999");
            }
        });

        // 이미지 관련 주석처리
        // $(".deleteImgBtn").click(function () {
        //     if (confirm("삭제하시겠습니까?")) {
        //         $(this).siblings(".uploadType").find(".imgUseYn").val("N");
        //         $(this).siblings(".uploadType").find(".imgUrlTag").attr("src", "");
        //         $(this).parents(".imgDisplayView").css("display", "none");
        //         $(this).parents(".imgDisplayView").siblings(".imgDisplayReg").css("display", "block");
        //     }
        // });
        // $(".uploadType").click(function () {
        //     if ($("#methodCd").val().trim() == null || $("#methodCd").val().trim() == "") {
        //         alert("상세 결제수단코드를 입력한 후 등록해주세요.");
        //         $("#methodCd").focus();
        //         return;
        //     }
        //     $("input[name=fileArr]").click();
        // });
        // $(".uploadType2").click(function () {
        //     if ($("#methodCd").val().trim() == null || $("#methodCd").val().trim() == "") {
        //         alert("상세 결제수단코드를 입력한 후 등록해주세요.");
        //         $("#methodCd").focus();
        //         return;
        //     }
        //     $("input[name=paymentImageFile]").click();
        // });
    },
    /** 결제수단 조회 */
    search: function (schParentMethodCd, schSiteType) {
        CommonAjax.basic({
            url: "/pg/paymentMethod/getPaymentMethodList.json?schParentMethodCd=" + schParentMethodCd + "&schSiteType=" + schSiteType
            , method: "GET"
            , callbackFunc: function (res) {
                console.log(res);
                paymentMethodGrid.setData(res);
                paymentMethodMng.reset();
                paymentMethodMng.selectGrid(schParentMethodCd, schSiteType);
            }
        });
    },
    /** 결제수단 조회 후 그리드 동적관리 */
    selectGrid: function (parentMethodCd, siteType) {
        $("#parentMethodCd").val(parentMethodCd);
        $("#siteType").val(siteType);

        if (parentMethodCd == "RBANK" || parentMethodCd == "PHONE") {
            $("#imageArea").hide();
            $("#pgCodeArea1").show();
        } else if (parentMethodCd == "KAKAOPAY" || parentMethodCd == "NAVERPAY" || parentMethodCd == "SAMSUNGPAY") {
            $("#imageArea").show();
            $("#pgCodeArea1").hide();
        } else {
            $("#imageArea").show();
            $("#pgCodeArea1").show();
        }
    },
    /** 조회결과 선택 시 하단상세(inputForm) 세팅 */
    drawInputForm: function (data) {
        $("#siteType").val(data.siteType);
        $("#siteType").prop("disabled", true);
        $("#useYn").val(data.useYn);
        $("#platform").val(data.platform);
        $("#sortSeq").val(data.sortSeq);
        $("#methodCd").val(data.methodCd);
        $("#methodCd").prop("disabled", true);
        $("#methodNm").val(data.methodNm);
        $("#inicisCd").val(data.inicisCd);
        $("#kiccCd").val(data.kiccCd);

        $("#parentMethodCd").val(data.parentMethodCd);
        $("#pgCertifyKind").val(data.pgCertifyKind);
        $("#orgCode").val(data.methodCd);

        if (data.imgUrl != "" && data.imgUrl != null) {
            $("#thumbUrl").attr("src", data.imgUrl);
            $("#thumbArea").show();
            $("#uploadArea").hide();
            $("#imgUrl").val(data.imgUrl);
        } else {
            $("#thumbUrl").attr("src", "");
            $("#thumbArea").hide();
            $("#uploadArea").show();
            $("#fileArr").val();
        }

        if (data.imgUrlMobile != "" && data.imgUrlMobile != null) {
            $("#thumbUrl2").attr("src", data.imgUrlMobile);
            $("#thumbArea2").show();
            $("#uploadArea2").hide();
            $("#imgUrlMobile").val(data.imgUrlMobile);
        } else {
            $("#thumbUrl2").attr("src", "");
            $("#thumbArea2").hide();
            $("#uploadArea2").show();
            $("#paymentImageFile").val("");
        }
    },
    /** 저장 */
    insert: function () {
        var schParentMethod = $("#parentMethodCd").val();
        var schSiteType = $("#siteType").val();
        $("#siteType").prop("disabled", false);
        $("#methodCd").prop("disabled", false);
        CommonAjax.basic({
            url         : '/pg/paymentMethod/savePaymentMethod.json',
            data        : _F.getFormDataByJson($("#inputForm")),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                console.log(res);
                alert("저장하였습니다.");
                paymentMethodMng.search(schParentMethod, schSiteType);
            }
        });
    },
    /** 저장 전 입력값 validation */
    valid: function () {
        // 결제수단 미선택 여부 체크
        if ($("#parentMethodCd").val() == "" || $("#parentMethodCd").val() == null) {
            alert("결제수단이 선택되지 않았습니다. 선택 후, 다시 진행해주세요.");
            return false;
        // PG사 코드 미등록 여부 체크
        } else if ($("#parentMethodCd").val() != "KAKAOPAY" && $("#parentMethodCd").val() != "NAVERPAY" && $("#parentMethodCd").val() != "SAMSUNGPAY") {
            if (
                ($("#inicisCd").val().trim() == "" || $("#inicisCd").val().trim() == null) &&
                ($("#kiccCd").val().trim() == "" || $("#kiccCd").val().trim() == null)) {
                alert("PG사 코드는 1개 이상 입력해야 합니다. 다시 확인해주세요.");
                return false;
            }
        }

        // 상세결제수단 코드 입력여부 체크
        if ($("#methodCd").val().trim() == null || $("#methodCd").val().trim() == "") {
            alert("상세결제수단 코드가 입력되지 않았습니다. 다시 확인해주세요");
            return false;
        // 상세결제수단명 입력여부 체크
        } else if ($("#methodNm").val().trim() == null || $("#methodNm").val().trim() == "") {
            alert("상세결제수단명이 입력되지 않았습니다. 다시 확인해주세요");
            return false;
        // 노출순서 입력여부 체크
        } else if ($("#sortSeq").val().trim() == null || $("#sortSeq").val().trim() == "") {
            alert("노출순서가 입력되지 않았습니다. 다시 확인해주세요.");
            return false;
        // 노출순서 값 체크
        } else if ($("#sortSeq").val() < 1) {
            alert("노출순서가 0보다 커야 합니다.");
            return false;
        // 노출범위 설정여부 체크
        } else if ($("#useYn").val() == "Y" && ($("#platform").val().trim() == null || $("#platform").val().trim() == "")) {
            alert("노출범위가 설정되지 않았습니다. 활성 상태에서 노출범위는 필수 설정항목 입니다.");
            return false;
        }

        // 이미지 업로드 공통 api 연동안됨. 연동 후 주석 해제 예정.
        // 이미지 관련 validation 체크
        // if ($("#parentMethodCd").val() == "CARD" || $("#parentMethodCd").val() == "EASY") {
        //     if ($("#imgUrl").val() == "" || $("#imgUrlMobile").val() == "") {
        //         alert("노출이미지가 등록되지 않았습니다. 노출이미지(PC/모바일)는 필수 항목입니다.");
        //         return false;
        //     }
        // }

        // PG사 코드 없을 경우 "-" 입력
        if ($("#inicisCd").val() == "" || $("#inicisCd").val() == null) {
            $("#inicisCd").val("-");
        }
        // PG사 코드 없을 경우 "-" 입력
        if ($("#kiccCd").val() == "" || $("#kiccCd").val() == null) {
            $("#kiccCd").val("-");
        }
        return true;
    },
    /** 초기화 */
    reset: function () {
        $("#siteType").val("");
        $("#siteType").prop("disabled", false);
        $("#useYn").val("N");
        $("#platform").val("");
        $("#sortSeq").val("");
        $("#methodCd").val("");
        $("#methodCd").prop("disabled", false);
        $("#methodNm").val("");
        $("#inicisCd").val("");
        $("#kiccCd").val("");
        $("#orgCode").val("");

        $("#thumbUrl").attr("src", "");
        $("#thumbArea").hide();
        $("#uploadArea").show();
        $("#thumbUrl2").attr("src", "");
        $("#thumbArea2").hide();
        $("#uploadArea2").show();

        $("#paymentImageFile").val("");
        $("#fileArr").val("");
        $("#imgUrl").val("");
        $("#imgUrlMobile").val("");

    },
    // PC 노출 이미지 등록
    // uploadImage: function () {
    //     CommonAjax.uploadImage({
    //         file: $("input[name=fileArr]"),
    //         data: {
    //             imgKey: "PaymentMethod",
    //             mode: "upload",
    //             fileName: $("#methodCd").val(),
    //             baseKeyCd: $("#methodCd").val()
    //         },
    //         callbackFunc: function (resData) {
    //             var errorMsg = "";
    //             for (var i in resData.fileArr) {
    //                 var f = resData.fileArr[i];
    //
    //                 if (f.hasOwnProperty("errors")) {
    //                     errorMsg += f.errors[0].detail + "\n";
    //                 } else {
    //                     // $("#thumbUrl").attr("src", f.upload_file.url);
    //                     // $("#thumbArea").show();
    //                     // $("#uploadArea").hide();
    //                     $("#imgUrl").val(f.upload_file.url);
    //
    //                     $("#isImageUpdate").val("1");
    //                     $("#imageName").val(f.name);
    //                     $("#size").val(f.size);
    //                     $("#imgWidth").val(f.upload_file.width);
    //                     $("#imgHeight").val(f.upload_file.height);
    //                     break;
    //                 }
    //             }
    //             if (!$.jUtil.isEmpty(errorMsg)) {
    //                 alert(errorMsg);
    //             }
    //             $("#hiddenBtn").click();
    //         }
    //     });
    // },
    // Mobile 노출 이미지 등록
    // uploadImage2: function () {
    //     CommonAjax.uploadImage({
    //         file: $("input[name=paymentImageFile]"),
    //         data: {
    //             imgKey: "PaymentMethod",
    //             mode: "upload",
    //             fileName: "M" + $("#methodCd").val().substring(1, 4),
    //             baseKeyCd: "M" + $("#methodCd").val().substring(1, 4)
    //         },
    //         callbackFunc: function (resData) {
    //             var errorMsg = "";
    //             for (var i in resData.paymentImageFile) {
    //                 var f = resData.paymentImageFile[i];
    //
    //                 if (f.hasOwnProperty("errors")) {
    //                     errorMsg += f.errors[0].detail + "\n";
    //                 } else {
    //                     // $("#thumbUrl").attr("src", f.upload_file.url);
    //                     // $("#thumbArea").show();
    //                     // $("#uploadArea").hide();
    //                     $("#imgUrlMobile").val(f.upload_file.url);
    //
    //                     $("#isImageUpdate2").val("1");
    //                     $("#imageName").val(f.name);
    //                     break;
    //                 }
    //             }
    //             if (!$.jUtil.isEmpty(errorMsg)) {
    //                 alert(errorMsg);
    //             }
    //             $("#hiddenBtn").click();
    //         }
    //     });
    // },
};

// var paymentImg = {
//     init: function () {
//         paymentImg.event();
//     },
//     unsetImage: function () {
//         $("#thumbUrl").attr("src", "");
//         $("#thumbArea").hide();
//         $("#uploadArea").show();
//         $("#fileArr").val("");
//         $("#imgUrl").val("");
//     },
//     unsetImage2: function () {
//         $("#thumbUrl2").attr("src", "");
//         $("#thumbArea2").hide();
//         $("#uploadArea2").show();
//         $("#paymentImageFile").val("");
//         $("#imgUrlMobile").val("");
//     },
//     event: function () {
//         $("#deleteImageBtn").click(function () {
//             paymentImg.unsetImage();
//         });
//
//         $("#deleteImageBtn2").click(function () {
//             paymentImg.unsetImage2();
//         });
//
//         $("input[name=fileArr]").change(function () {
//             var URL = window.URL || window.webkitURL;
//             var file = this.files[0];
//
//             var img = new Image();
//             img.onload = function () {
//                 CommonAjax.basic({
//                     url: "/common/imageValidator.json",
//                     data: {
//                         imageName: file.name,
//                         imageKey: "PaymentMethod",
//                         mimeType: file.type,
//                         size: file.size,
//                         width: this.width,
//                         height: this.height
//                     },
//                     method: "post",
//                     callbackFunc: function (resData) {
//                         if (resData.data.result == "success") {
//                             var reader = new FileReader();
//                             reader.onload = function (e) {
//                                 $("#thumbUrl").attr("src", e.target.result);
//                                 $("#thumbArea").show();
//                                 $("#uploadArea").hide();
//                                 $("#isImageUpdate").val("1");
//                                 paymentMethodMng.uploadImage();
//                             }
//                             reader.readAsDataURL(file);
//                         } else {
//                             alert("이미지 검증에 실패하였습니다. 잠시 후에 시도해주세요.");
//                         }
//                     }
//                 });
//                 URL.revokeObjectURL(file);
//             };
//             img.src = URL.createObjectURL(file);
//         });
//
//         $("input[name=paymentImageFile]").change(function () {
//             var URL = window.URL || window.webkitURL;
//             var file = this.files[0];
//
//             var img = new Image();
//
//             img.onload = function () {
//                 CommonAjax.basic({
//                     url: "/common/imageValidator.json",
//                     data: {
//                         imageName: file.name,
//                         imageKey: "PaymentMethod",
//                         mimeType: file.type,
//                         size: file.size,
//                         width: this.width,
//                         height: this.height
//                     },
//                     method: "post",
//                     callbackFunc: function (resData) {
//                         if (resData.data.result == "success") {
//                             var reader = new FileReader();
//                             reader.onload = function (e) {
//                                 $("#thumbUrl2").attr("src", e.target.result);
//                                 $("#thumbArea2").show();
//                                 $("#uploadArea2").hide();
//                                 $("#isImageUpdate2").val("1");
//                                 paymentMethodMng.uploadImage2();
//                             }
//                             reader.readAsDataURL(file);
//                         } else {
//                             alert("이미지 검증에 실패하였습니다. 잠시 후에 시도해주세요.");
//                         }
//                     }
//                 });
//                 URL.revokeObjectURL(file);
//             };
//             img.src = URL.createObjectURL(file);
//         });
//     }
// };
