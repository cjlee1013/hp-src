/** Main Script */
var cardBinManageMain = {
    /**
     * init 이벤트
     */
    init: function() {
        cardBinManageMain.bindingEvent();
    },

    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $("#cardBinNo").allowInput("keyup", ["NUM"], $(this).attr("id"));
        //상단 검색영역검색
        $("#schBtn").bindClick(cardBinManageMain.search);
        //상단 검색영역 초기화
        $("#schResetBtn").bindClick(cardBinManageMain.schReset);
        //기본정보 초기화
        $("#resetBtn").bindClick(cardBinManageMain.resetInfo);
        //기본정보 저장
        $("#addCardBinManageBtn").bindClick(cardBinManageMain.add);

    },
    /**
     * 상단검색영역초기화
     */
    schReset:function() {
        $("#cardBinManageSearchForm select").find('option:first').prop('selected', true);
    },
    /**
     * 기본정보 초기화
     */
    resetInfo:function() {
        // if(!confirm("초기화 하시겠습니까?")) {
        //     return false;
        // }
        $("#cardBinManageForm select").find('option:first').prop('selected', true);
        $("#cardBinManageForm input[type='text']").val("");
        $("#cardBinManageForm input[type='hidden']").val("");
        $("#useY").prop("checked", true);
        $("#useN").prop("checked", false);
    },
    /**
     * 데이터 조회
     */
    search: function() {
        var data = $("#cardBinManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/pg/paymentMethod/getCardBinManageList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(dataList) {
                console.log(dataList);
                cardBinManageGrid.setData(dataList);
                $('#cardBinManageSearchCnt').html($.jUtil.comma(cardBinManageGrid.gridView.getItemCount()));
                //기본정보 초기화
                cardBinManageMain.resetInfo();
            }
        });
    },
    /**
     * 데이터 등록
     */
    add: function() {
        // Form 데이터 유효성 체크
        if (!cardBinManageMain.valid()) {
            return false;
        }
        if(!confirm("저장 하시겠습니까?")) {
            return false;
        }

        CommonAjax.basic({
            url         : '/pg/paymentMethod/saveCardBinManage.json',
            data        : _F.getFormDataByJson($("#cardBinManageForm")),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(data) {
                console.log(data);
                alert("저장하였습니다.");
                cardBinManageMain.search();
            }
        });
    },
    /**
     * 데이터 등록 전 Form 데이터 유효성 체크
     */
    valid: function() {
        // 1.카드사선택
        if ($.jUtil.isEmpty($("#methodCd").val())) {
            alert("카드사를 선택해주세요.");
            return false;
        // 2.사용여부
        } else if ($.jUtil.isEmpty($("input[name=useYn]:checked").val())) {
            alert("사용여부를 선택해주세요.");
            return false;
        // 3.카드명
        } else if ($.jUtil.isEmpty($("#cardNm").val())) {
            alert("카드명을 입력해주세요.");
            return false;
        } else if ($("#cardNm").val().length > 50) {
            alert("카드명은 50자 이내로 입력해주세요.");
            return false;
        // 4.카드구분
        } else if ($.jUtil.isEmpty($("#cardBinKind").val())) {
            alert("카드구분을 선택해 주세요.");
            return false;
        // 5.카드BIN번호
        } else if (!$.jUtil.isEmpty($("#cardBinNo").val())) {
            if ($("#cardBinNo").val().length > 7) {
                alert("카드BIN번호는 7자 이내로 입력해주세요.");
                return false;
            } else if (!$.jUtil.isAllowInput($("#cardBinNo").val(), ['NUM'])) {
                alert("카드BIN번호는 숫자만 입력해주세요.");
                return false;
            }
        } else if ($.jUtil.isEmpty($("#cardBinNo").val())) {
            alert("카드BIN번호는 6-7자 이내로 입력해주세요.");
            return false;
        }
        return true;
    },

    /**
     * 그리드에서 클릭한 정보 하단 정보영역에 그려주기
     */
    cardBinMngSelect: function (data) {
        $("#cardBinNoSeq").val(data.cardBinNoSeq);
        $("#methodCd").val(data.methodCd);
        $("#cardNm").val(data.cardNm);
        $("#cardBinKind").val(data.cardBinKind);
        $("#cardBinNo").val(data.cardBinNo);
        $('input:radio[name="useYn"][value="'+data.useYn+'"]').trigger('click');
    }
};

/** Grid Script */
var cardBinManageGrid = {
    gridView: new RealGridJS.GridView("cardBinManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        cardBinManageGrid.initGrid();
        cardBinManageGrid.initDataProvider();
        cardBinManageGrid.event();
        setColumnLookupOption(cardBinManageGrid.gridView, "methodCd", $("#methodCd"));
        setColumnLookupOption(cardBinManageGrid.gridView, "cardBinKind", $("#cardBinKind"));
    },
    initGrid: function () {
        cardBinManageGrid.gridView.setDataSource(cardBinManageGrid.dataProvider);
        cardBinManageGrid.gridView.setStyles(cardBinManageGridBaseInfo.realgrid.styles);
        cardBinManageGrid.gridView.setDisplayOptions(cardBinManageGridBaseInfo.realgrid.displayOptions);
        cardBinManageGrid.gridView.setColumns(cardBinManageGridBaseInfo.realgrid.columns);
        cardBinManageGrid.gridView.setOptions(cardBinManageGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        cardBinManageGrid.dataProvider.setFields(cardBinManageGridBaseInfo.dataProvider.fields);
        cardBinManageGrid.dataProvider.setOptions(cardBinManageGridBaseInfo.dataProvider.options);
    },
    event: function () {
        //그리드 클릭 이벤트
        cardBinManageGrid.gridView.onDataCellClicked = function (gridView, index) {
            var data = cardBinManageGrid.dataProvider.getJsonRow(index.dataRow);
            cardBinManageMain.cardBinMngSelect(data);
        };
    },
    setData: function (dataList) {
        cardBinManageGrid.dataProvider.clearRows();
        cardBinManageGrid.dataProvider.setRows(dataList);
    }
};