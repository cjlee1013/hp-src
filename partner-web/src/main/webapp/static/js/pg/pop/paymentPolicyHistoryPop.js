/**
 * 결제정책 히스토리 팝업
 */
var policyHistoryPopGrid = {
    gridView : new RealGridJS.GridView("policyHistoryPopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        policyHistoryPopGrid.initGrid();
        policyHistoryPopGrid.initDataProvider();
    },
    initGrid : function() {
        policyHistoryPopGrid.gridView.setDataSource(policyHistoryPopGrid.dataProvider);
        policyHistoryPopGrid.gridView.setStyles(paymentPolicyHistoryPopBaseInfo.realgrid.styles);
        policyHistoryPopGrid.gridView.setDisplayOptions(paymentPolicyHistoryPopBaseInfo.realgrid.displayOptions);
        policyHistoryPopGrid.gridView.setColumns(paymentPolicyHistoryPopBaseInfo.realgrid.columns);
        policyHistoryPopGrid.gridView.setOptions(paymentPolicyHistoryPopBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        policyHistoryPopGrid.dataProvider.setFields(paymentPolicyHistoryPopBaseInfo.dataProvider.fields);
        policyHistoryPopGrid.dataProvider.setOptions(paymentPolicyHistoryPopBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        policyHistoryPopGrid.dataProvider.clearRows();
        policyHistoryPopGrid.dataProvider.setRows(dataList);
    }
};

var historyPopMng = {
    init: function () {
        historyPopMng.bindEvent();
        historyPopMng.resetHistorySearchForm();
        historyPopMng.manageSearchForm();
        historyPopMng.initSchDt('-7d');
    },
    /** 이벤트 바인딩 */
    bindEvent: function () {
        // 검색 버튼
        $("#schBtn").bindClick(historyPopMng.searchHistory);
        // 초기화 버튼
        $("#schResetBtn").bindClick(historyPopMng.resetHistorySearchForm);
    },
    /** 조회기간 초기세팅 */
    initSchDt: function (flag) {
        historyPopMng.setDate = Calendar.datePickerRange('schFromDt', 'schEndDt');
        historyPopMng.setDate.setEndDate(0);
        historyPopMng.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schFromDt").val());
    },
    /** 검색 Form 초기화 */
    resetHistorySearchForm: function () {
        $("#historySearchForm").resetForm();
        historyPopMng.initSchDt('-7d');

        commonCategory.setCategorySelectBox(1, "", "", $("#schCateCd1"));
        commonCategory.setCategorySelectBox(2, "", "", $("#schCateCd2"),true);
        commonCategory.setCategorySelectBox(3, "", "", $("#schCateCd3"),true);
        commonCategory.setCategorySelectBox(4, "", "", $("#schCateCd4"),true);
    },
    /** 화면 동적관리 */
    manageSearchForm: function () {
        var applyCdHeader = policyHistoryPopGrid.gridView.getColumnProperty("applyCd", "header");
        var applyTargetNmHeader = policyHistoryPopGrid.gridView.getColumnProperty("applyTargetNm", "header");
        if (applyTarget === "상품번호") {
            $("#itemDiv").show();
            $('#cateDiv, #affiDiv').hide();
            applyCdHeader.text = "상품번호";
            applyTargetNmHeader.text = "상품명";
        } else if (applyTarget === "카테고리") {
            $("#cateDiv").show();
            $('#itemDiv, #affiDiv').hide();
            applyCdHeader.text = "카테고리번호";
            applyTargetNmHeader.text = "카테고리명";
        } else if (applyTarget === "제휴채널") {
            $("#affiDiv").show();
            $('#itemDiv, #cateDiv').hide();
            applyCdHeader.text = "채널ID";
            applyTargetNmHeader.text = "채널명";
        } else {
            $("#cateDiv").show();
            $('#itemDiv, #affiDiv').hide();
            applyCdHeader.text = "카테고리번호";
            applyTargetNmHeader.text = "카테고리명";
        }
        policyHistoryPopGrid.gridView.setColumnProperty("applyCd", "header", applyCdHeader);
        policyHistoryPopGrid.gridView.setColumnProperty("applyTargetNm", "header", applyTargetNmHeader);
    },
    /** 히스토리 검색 */
    searchHistory: function () {
        var data = $("#historySearchForm").serialize();
        CommonAjax.basic({
            url         : '/pg/pop/getPaymentPolicyHistory.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                policyHistoryPopGrid.setData(res);
            }
        });
    },
};