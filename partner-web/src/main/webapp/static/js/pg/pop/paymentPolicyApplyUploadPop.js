var uploadPopMng = {
    /** 초기화 */
    init: function () {
        uploadPopMng.bindPopEvent();
    },
    /** 팝업 이벤트 바인딩 */
    bindPopEvent: function () {
        // 취소버튼 - 창닫기
        $("#closeBtn").bindClick(uploadPopMng.closePop);
        // 업로드할 파일찾기
        $("#searchUploadFile").bindClick(uploadPopMng.searchUploadFile);
        // 선택버튼
        $("#uploadFileBtn").bindClick(uploadPopMng.uploadFile);
        // 업로드양식 다운로드
        $("#downloadTemplate").bindClick(uploadPopMng.downloadTemplate);
        // 파일찾기 후 이벤트
        $("input[name='uploadFile']").on("change", function() {
            var fileInfo = $(this)[0].files[0];
            var fileExt = fileInfo.name.split(".");
            if (fileInfo == "" || fileInfo == null) {
                alert("파일을 선택해주세요.");
                return false;
            }
            if (fileExt.indexOf("xlsx") < 0) {
                alert("엑셀 (xlsx) 파일만 업로드 가능합니다.");
                return false;
            }
            $("#uploadFileNm").val($(this)[0].files[0].name);
        });
    },
    /** 취소-창닫기 */
    closePop: function () {
        window.close();
    },
    /** 파일찾기 */
    searchUploadFile: function () {
        $("input[name=uploadFile]").trigger("click");
    },
    /** 선택-파일업로드 */
    uploadFile: function () {
        if ($("#uploadFile").val() == "") {
            alert("파일을 선택해주세요.");
            return false;
        }

        if (!confirm("등록하시겠습니까?")) return false;

        $("#excelItemUploadForm").prop("onsubmit", null);

        $('form[name="excelItemUploadForm"]').ajaxForm({
            type   : "POST",
            url    : "/pg/pop/excelItemApply.json",
            enctype: "multipart/form-data",
            success: function(res) {
                // item 테이블 동기화 후 삭제할 로직 [start]
                for(var i=0; i<res.length; i++) {
                    res[i].itemNm = "임시상품명";
                }
                // item 테이블 동기화 후 삭제할 로직 [end]
                eval("opener." + callBackScript + "(res);");
                self.close();
            },
            error: function(res){
                console.log(res);
            }
        });
    },
    /** 양식받기 */
    downloadTemplate: function () {
        var fileUrl = '/static/templates/itemUploadExcel_template.xlsx';

        if (fileUrl != "") {
            location.href = fileUrl;
        } else {
            alert("양식 파일이 없습니다.");
            return false;
        }
    }
};