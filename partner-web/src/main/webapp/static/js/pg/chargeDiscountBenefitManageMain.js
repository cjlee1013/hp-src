/** Main Script */
var chargeDiscountBenefitManageMain = {
    /**
     * init 이벤트
     */
    init: function() {
        chargeDiscountBenefitManageMain.bindingEvent();
        chargeDiscountBenefitManageMain.bindingAllowEvent();
        deliveryCore.initSearchDate("0", "+1m");
        deliveryCore.initApplyDate("0", "0");
    },

    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        // 상단 검색영역 검색
        $("#schBtn").bindClick(chargeDiscountBenefitManageMain.search);
        // 상단 검색영역 초기화
        $("#schResetBtn").bindClick(chargeDiscountBenefitManageMain.reset,"search");
        // 기본정보 저장
        $("#addChargeDiscountBenefitManageBtn").bindClick(chargeDiscountBenefitManageMain.basicSave);
        // 기본정보 초기화
        $("#resetBtn").bindClick(chargeDiscountBenefitManageMain.resetBasicInfo);

        //할인방법 변경시 노출 영역 변경
        $("#discountType").on("change", function() {
            let selDiscountType = $(this).val();
            if (selDiscountType === "RATE") {
                $("#discountTypeRate").show();
                $("#discountTypePrice").hide();
            } else {
                $("#discountTypeRate").hide();
                $("#discountTypePrice").show();
            }
        });

        $("#siteType").on("change", function() {
            let selSiteType = $(this).val();
            if (selSiteType === "CLUB") {
                $("#methodCdHMP").hide();
                $("#methodCdCLUB").show();
            } else {
                $("#methodCdHMP").show();
                $("#methodCdCLUB").hide();
            }
        });

        // 청구할인 명 엔터 키 이벤트
        $("#schChargeDiscountNm").keydown(function(key) {
            if (!$.jUtil.isEmpty($(this).val())) {
                if (key.keyCode == 13) {
                    chargeDiscountBenefitManageMain.search();
                }
            }
        });
    },

    /**
     * 입력값 제약조건 바인딩
     */
    bindingAllowEvent : function() {
        $("#discountRate").allowInput("keyup", ["NUM"], $(this).attr("id"));
        $("#discountMaxPrice").allowInput("keyup", ["NUM"], $(this).attr("id"));
        $("#discountPrice").allowInput("keyup", ["NUM"], $(this).attr("id"));
        $("#discountMinPrice").allowInput("keyup", ["NUM"], $(this).attr("id"));
    },

    /**
     * 검색 폼 초기화
     * @param pos : search(상단 검색영역), info(하단 정보영역)
     */
    reset: function(pos) {
        // 상단 검색영역 초기화
        if (pos === "search") {
            $("#chargeDiscountBenefitManageSearchForm").resetForm();
            deliveryCore.initSearchDate("0", "+1m");
        }
        // 하단 정보영역 초기화
        else if (pos === "info") {
            $("#chargeDiscountBenefitManageForm").resetForm();
            deliveryCore.initApplyDate("0", "0");
            $("#siteType").trigger('change');       //사이트유형 change event 발생
            $("#discountType").trigger('change');   //할인방법 change event 발생
        }
    },

    /**
     * 데이터 조회
     */
    search: function() {
        if (!deliveryCore.dateValid($("#schApplyStartDt"), $("#schApplyEndDt"), ["isValid"])) {
            return false;
        }

        var data = $("#chargeDiscountBenefitManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/pg/paymentMethod/getChargeDiscountBenefitManage.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                chargeDiscountBenefitManageGrid.setData(res);
                $('#chargeDiscountBenefitManageSearchCnt').html($.jUtil.comma(chargeDiscountBenefitManageGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 기본정보 초기화 클릭 시
     * @returns {boolean}
     */
    resetBasicInfo: function() {
        if(!confirm("초기화 하시겠습니까?")) {
            return false;
        }
        chargeDiscountBenefitManageMain.reset("info");
    },

    /**
     * 데이터 등록
     */
    basicSave: function() {
        // Form 데이터 유효성 체크
        if (!chargeDiscountBenefitManageMain.valid()) {
            return;
        }
        if(!confirm("저장 하시겠습니까?")) {
            return false;
        }

        CommonAjax.basic({
            url         : '/pg/paymentMethod/saveChargeDiscountBenefitManage.json',
            data        : _F.getFormDataByJson($("#chargeDiscountBenefitManageForm")),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                console.log(res);
                alert("저장하였습니다.");
                chargeDiscountBenefitManageMain.search();
                chargeDiscountBenefitManageMain.reset("info");
            }
        });
    },

    /**
     * 데이터 등록 전 Form 데이터 유효성 체크
     */
    valid: function() {
        //사이트
        let selSiteType = $("#siteType").val();
        let applyStartDt = moment($("#applyStartDt").val(), 'YYYY-MM-DD', true);
        let applyEndDt = moment($("#applyEndDt").val(), 'YYYY-MM-DD', true);

        if ($.jUtil.isEmpty(selSiteType)) {
            alert("사이트를 선택해주세요.");
            return false;
        }
        //청구할인명
        if ($("#chargeDiscountNm").val().length < 2 || $("#chargeDiscountNm").val().length > 100 ) {
            alert("청구할인명은 2자 이상 100자 이내로 입력해주세요.");
            return false;
        }

        //카드사
        let selMethodCd = $("#methodCd"+selSiteType).val();

        if ($.jUtil.isEmpty(selMethodCd)) {
            alert("카드사를 선택해주세요.");
            return false;
        } else {
            $("#methodCd").val(selMethodCd);
        }

        //적용기간
        if(!applyStartDt.isValid() || !applyEndDt.isValid()){
            alert("적용기간은 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }

        //할인방법
        if ($.jUtil.isEmpty($("#discountType").val())) {
            alert("할인방법을 선택해주세요.");
            return false;
        } else if ($("#discountType").val() === "PRICE" ) {
            if ($.jUtil.isEmpty($("#discountPrice").val()) || $("#discountPrice").val() === "0") {
                alert("할인 금액을 입력해 주세요.(금액은 1~99999999까지 입력 가능합니다.)");
                return false;
            }
            if (!$.jUtil.isAllowInput($("#discountPrice").val(), ['NUM'])) {
                alert("할인 금액은 숫자만 입력해주세요.");
                return false;
            }
            //정률 선택시 필요한 값 초기화.
            $("#discountRate").val("0");
            $("#discountMaxPrice").val("0");

        } else if ($("#discountType").val() === "RATE" ) {
            if ($.jUtil.isEmpty($("#discountRate").val()) || $("#discountRate").val() === "0") {
                alert("할인율을 입력해 주세요.(할인방법 정률은 1~99%까지 입력 가능합니다.)");
                return false;
            }
            if (!$.jUtil.isAllowInput($("#discountRate").val(), ['NUM'])) {
                alert("할인율은 숫자만 입력해주세요.");
                return false;
            }
            if($.jUtil.isPercentInput($('#discountRate').val()) == false) {
                alert("할인방법 정률은 1~99%까지 입력 가능합니다.");
                return false;
            }
            if ($.jUtil.isEmpty($("#discountMaxPrice").val()) || $("#discountMaxPrice").val() === "0") {
                alert("최대할인 금액을 입력해 주세요.(금액은 1~99999999까지 입력 가능합니다.)");
                return false;
            }
            if (!$.jUtil.isAllowInput($("#discountMaxPrice").val(), ['NUM'])) {
                alert("최대할인 금액은 숫자만 입력해주세요.");
                return false;
            }
            //정액 선택시 필요한 값 초기화.
            $("#discountPrice").val("0");

        }
        //최소구매금액
        if ($.jUtil.isEmpty($("#discountMinPrice").val()) || $("#discountMinPrice").val() === "0") {
            alert("최소구매 금액을 입력해주세요.(금액은 1~99999999까지 입력 가능합니다.)");
            return false;
        } else if (!$.jUtil.isAllowInput($("#discountMinPrice").val(), ['NUM'])) {
            alert("최소구매 금액은 숫자만 입력해주세요.");
            return false;
        }

        return true;
    },

    /**
     * 그리드에서 클릭한 정보 하단 정보영역에 그려주기
     */
    drawChargeDiscountBenefitManageDetail: function (gridData) {
        console.log(gridData);

        // 번호
        $("#chargeDiscountBenefitMngNo").val(gridData.chargeDiscountBenefitMngNo);

        // 사이트
        $("#siteType").val(gridData.siteType).trigger('change');

        // 청구할인명
        $("#chargeDiscountNm").val(gridData.chargeDiscountNm);

        // 사용여부 (Y:사용/N:사용안함)
        $('input:radio[name=useYn]:input[value="'+ gridData.useYn + '"]').prop("checked", true);

        // 적용기간
        $('#applyStartDt').val($.datepicker.formatDate('yy-mm-dd', gridData.applyStartDt));
        $('#applyEndDt').val($.datepicker.formatDate('yy-mm-dd', gridData.applyEndDt));

        // 카드사
        $("#methodCd"+gridData.siteType).val(gridData.methodCd);

        // 할인방법
        $("#discountType").val(gridData.discountType).trigger('change');

        // 할인율
        $("#discountRate").val(gridData.discountRate);

        // 최대할인금액
        $("#discountMaxPrice").val(gridData.discountMaxPrice);

        // 할인금액
        $("#discountPrice").val(gridData.discountPrice);

        // 최소구매금액
        $("#discountMinPrice").val(gridData.discountMinPrice);
    }
};

/** Grid Script */
var chargeDiscountBenefitManageGrid = {
    gridView: new RealGridJS.GridView("chargeDiscountBenefitManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        chargeDiscountBenefitManageGrid.initGrid();
        chargeDiscountBenefitManageGrid.initDataProvider();
        chargeDiscountBenefitManageGrid.event();
    },
    initGrid: function () {
        chargeDiscountBenefitManageGrid.gridView.setDataSource(chargeDiscountBenefitManageGrid.dataProvider);
        chargeDiscountBenefitManageGrid.gridView.setStyles(chargeDiscountBenefitManageGridBaseInfo.realgrid.styles);
        chargeDiscountBenefitManageGrid.gridView.setDisplayOptions(chargeDiscountBenefitManageGridBaseInfo.realgrid.displayOptions);
        chargeDiscountBenefitManageGrid.gridView.setColumns(chargeDiscountBenefitManageGridBaseInfo.realgrid.columns);
        chargeDiscountBenefitManageGrid.gridView.setOptions(chargeDiscountBenefitManageGridBaseInfo.realgrid.options);
        setColumnLookupOption(chargeDiscountBenefitManageGrid.gridView, "siteType", $("#siteType"));     //siteType grid 에 노출시 이름으로 변경.
        setColumnLookupOption(chargeDiscountBenefitManageGrid.gridView, "useYn", $("#schUseYn"));
    },
    initDataProvider: function () {
        chargeDiscountBenefitManageGrid.dataProvider.setFields(chargeDiscountBenefitManageGridBaseInfo.dataProvider.fields);
        chargeDiscountBenefitManageGrid.dataProvider.setOptions(chargeDiscountBenefitManageGridBaseInfo.dataProvider.options);
    },
    event: function () {
        chargeDiscountBenefitManageGrid.gridView.onDataCellClicked = function (gridView, index) {
            chargeDiscountBenefitManageMain.drawChargeDiscountBenefitManageDetail(chargeDiscountBenefitManageGrid.dataProvider.getJsonRow(chargeDiscountBenefitManageGrid.gridView.getCurrent().dataRow));
        };
    },
    setData: function (dataList) {
        chargeDiscountBenefitManageGrid.dataProvider.clearRows();
        chargeDiscountBenefitManageGrid.dataProvider.setRows(dataList);
    }
};