/** Main Script */
var closeDayImagePopMain = {
    /**
     * init 이벤트
     */
    init: function() {
        closeDayImagePopMain.drawForm("HYPER");
        closeDayImagePopMain.bindingEvent();
    },
    /**
     * JSON 으로 되어있는 값 알맞게 그려주기
     */
    drawForm: function(siteType) {
        var siteImageList = siteType === "HYPER" ? hyperImageList : clubImageList;
        // reset
        $("td[data-tagType='dynamic_content']:not(.close_day_img) span").text("");
        $("td[data-tagType='dynamic_content'].close_day_img span[id*='_']").children().attr("src", "");

        // draw
        for (var siteImage of siteImageList) {
            var closeDayTypeTagsId = siteImage.closeDayType + "_";
            $("td[data-tagType='dynamic_content'] span[id^='"+closeDayTypeTagsId+"']").each(function() {
                var tagId = $(this).attr("id");       //'F_pcImgUrl'
                var tagPropNm = tagId.split("_")[1];  //'pcImgUrl'
                var tagProp = siteImage[tagPropNm];   //'http...'
                if ($(this).children().prop("tagName") === "IMG") {
                    $(this).children().attr("src", tagProp);
                } else {
                    $(this).text(tagProp);
                }
            });
        }
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // Hyper 탭
        $("#storeType #HYPER").bindClick(closeDayImagePopMain.goTab, "HYPER");
        // Club 탭
        $("#storeType #CLUB").bindClick(closeDayImagePopMain.goTab, "CLUB");
        // 이미지 [등록] 버튼
        $("td[data-tagType='dynamic_content'] button").bindClick(closeDayImagePopMain.callImageFile);
        // [저장] 버튼
        $("#saveBtn").bindClick(closeDayImagePopMain.saveAll);
    },

    /**
     * 탭으로 이동하는 것 처럼 보이게 style change & Form draw
     * @param tabId
     */
    goTab: function(tabId) {
        // 선택된 탭이 HYPER 인 경우
        if (tabId === "HYPER") {
            $("#storeType #HYPER").addClass("tab-on");
            $("#storeType #CLUB").removeClass("tab-on");
            closeDayImagePopMain.drawForm("HYPER");
        // 선택된 탭이 CLUB 인 경우
        } else if (tabId === "CLUB") {
            $("#storeType #HYPER").removeClass("tab-on");
            $("#storeType #CLUB").addClass("tab-on");
            closeDayImagePopMain.drawForm("CLUB");
        }
    },

    /**
     * 이미지 불러오기
     */
    callImageFile: function() {
        alert("이미지는 나중에!");
    },

    /**
     * 휴일이미지 저장
     */
    saveAll: function() {
        if (!confirm("저장 하시겠습니까?")) {
            return false;
        }
        alert("이미지 저장은 나중에!");
    },
    /**
     * 데이터 검색 전 Form 데이터 유효성 체크
     */
    valid: function() {
        return false;
    }
};