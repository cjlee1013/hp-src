/** Main Script */
var excelUploadPopMain = {
    /**
     * init 이벤트
     */
    init: function() {
        excelUploadPopMain.bindingEvent();
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [파일찾기] 버튼
        $("#searchUploadFile").bindClick(excelUploadPopMain.searchFile);
        // [저장] 버튼
        $("#saveBtn").bindClick(excelUploadPopMain.save);
    },
    /**
     * [파일찾기] 버튼 클릭 (윈도우 파일 찾기 open)
     */
    searchFile: function() {
        $("#uploadFile").click();
    },
    /**
     * 선택한 파일 정보 불러오기
     */
    getFile: function(_obj) {
        var fileInfo = _obj[0].files[0];
        var fileName = fileInfo.name;
        $("#uploadFileNm").val(fileName);
    },
    /**
     * 파일 저장, callBack 함수 호출
     */
    save: function() {
        if (!excelUploadPopMain.valid()) {
            return false;
        }

        $("#fileUploadForm").ajaxForm({
            url         : "/escrow/storeDelivery/saveItemShiftListForExcel.json",
            method      : "POST",
            enctype     : "multipart/form-data",
            timeout     : 30000,
            success     : function(res) {
                // df message alert
                if ($.jUtil.isEmpty(callBackScript)) {
                    alert("완료되었습니다.");
                // call back 호출
                } else {
                    eval('opener.' + callBackScript + '(res);');
                }
                // close
                self.close();
            },
            error       : function(resError) {
                // error message alert
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.message != null) {
                        alert(resError.responseJSON.message);
                    } else {
                        alert(CommonErrorMsg.dfErrorMsg);
                    }
                } else if (resError.status !== 200) {
                    alert(CommonErrorMsg.dfErrorMsg);
                }
                // close
                self.close();
            },
        });
        $("#fileUploadForm").submit();
    },
    /**
     * 파일 저장 전, 유효성 검증
     * @return {boolean}
     */
    valid: function() {
        var uploadFileVal = $("#uploadFile").val();
        var uploadFileNmVal = $("#uploadFileNm").val();
        if ($.jUtil.isEmpty(uploadFileVal) || $.jUtil.isEmpty(uploadFileNmVal)) {
            alert("파일을 등록해주세요.");
            return false;
        }
        return true;
    }
};