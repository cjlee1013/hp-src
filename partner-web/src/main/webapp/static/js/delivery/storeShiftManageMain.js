var storeShiftManageMain = {

    /**
     * init 이벤트
     */
    init: function() {
        storeShiftManageMain.bindingEvent();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "+1w");
    },

    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        // 상단 검색영역 검색
        $("#schBtn").bindClick(storeShiftManageMain.search);

        // 상단 검색영역 초기화
        $("#schResetBtn").bindClick(storeShiftManageMain.resetSch);

        // 오늘
        $("#setTodayBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", "0", "0") ;
        });
        // 1주일
        $("#setOneWeekBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", "0", "+1w") ;
        });
        // 1개월
        $("#setOneMonthBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", "0", "+1m") ;
        });
    },

    /**
     * 검색영역 초기화
     */
    resetSch : function () {
        $("#storeShiftSearchForm").resetForm();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "+1w");
    },

    /**
     * shift 검색
     * @returns {boolean}
     */
    search: function() {
        if (!validCheck.search()) {
            return false;
        }

        var data = $("#storeShiftSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/storeDelivery/getShiftManageList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                storeShiftGrid.setData(res);
                $('#storeShiftSearchCnt').html($.jUtil.comma(storeShiftGrid.gridView.getItemCount()));
            }
        });
    },
};

//조회/검색 검증
var validCheck = {
    search: function () {
        if (!deliveryCore.dateValid($("#schStartDt"), $("#schEndDt"), ["isValid", "overOneMonthRange"])) {
            return false;
        }
        var schStoreId = $("#schStoreId").val();
        if ($.jUtil.isEmpty(schStoreId)) {
            alert("점포를 선택해 주세요.");
            return false;
        }
        $("#schStoreId").val(schStoreId.lpad(4,"0"));
        return true;
    },
}

/** Grid Script */
var storeShiftGrid = {
    gridView: new RealGridJS.GridView("storeShiftGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        storeShiftGrid.initGrid();
        storeShiftGrid.initDataProvider();
    },
    initGrid: function () {
        storeShiftGrid.gridView.setDataSource(storeShiftGrid.dataProvider);
        storeShiftGrid.gridView.setStyles(storeShiftGridBaseInfo.realgrid.styles);
        storeShiftGrid.gridView.setDisplayOptions(storeShiftGridBaseInfo.realgrid.displayOptions);
        storeShiftGrid.gridView.setColumns(storeShiftGridBaseInfo.realgrid.columns);
        storeShiftGrid.gridView.setOptions(storeShiftGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(storeShiftGrid.gridView, "shipWeekday", weekdayJson);
    },
    initDataProvider: function () {
        storeShiftGrid.dataProvider.setFields(storeShiftGridBaseInfo.dataProvider.fields);
        storeShiftGrid.dataProvider.setOptions(storeShiftGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        storeShiftGrid.dataProvider.clearRows();
        storeShiftGrid.dataProvider.setRows(dataList);
    }
};