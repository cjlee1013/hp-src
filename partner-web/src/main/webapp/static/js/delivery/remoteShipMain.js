var remoteShipMain = {

    /**
     * init 이벤트
     */
    init: function() {
        remoteShipMain.bindingEvent();
    },

    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        // 상단 검색영역 검색
        $("#schBtn").bindClick(remoteShipMain.search);

        // 상단 검색영역 초기화
        $("#schResetBtn").bindClick(remoteShipMain.resetSch);

        // [주소] 시도명 변경시마다 해당 시군구명 조회
        $("#schSidoType").bindChange(remoteShipMain.getSigunguList);

        // 엑셀다운로드
        $("#excelDownloadBtn").bindClick(remoteShipMain.excelDownload);

        // 상단 요일 전체 선택
        $("#weekdayAll").bindChange(remoteShipMain.chkWeekday);

        // 우편번호 숫자 입력
        $("#schZipcode").allowInput("keyup", ["NUM"], $(this).attr("id"));

    },

    /**
     * 검색영역 초기화
     */
    resetSch : function () {
        $("#remoteShipSearchForm").resetForm();
    },

    /**
     * slot 검색
     * @returns {boolean}
     */
    search: function() {
        if (!validCheck.search()) {
            return false;
        }

        var data = $("#remoteShipSearchForm").serialize();

        CommonAjax.basic({
            url         : '/escrow/storeDelivery/getRemoteShipList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {

                if (res.returnStatus === "SUCCESS") {
                    console.log(res.remoteShipGridBaseInfo);
                    eval(res.remoteShipGridBaseInfo);

                    remoteShipGrid.init(remoteShipGridBaseInfo);
                    remoteShipGrid.setData(res.remoteShipGridData);

                    $('#remoteShipSearchCnt').html(
                        $.jUtil.comma(remoteShipGrid.gridView.getItemCount()));
                } else {
                    alert('검색결과가 존재하지 않습니다.');
                }

            }
        });
    },


    /**
     * 데이터 엑셀 다운로드
     */
    excelDownload: function() {
        if(remoteShipGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "원거리배송관리_" + $("#schStoreId").val() + "_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        remoteShipGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 요일 전체 checkbox 선택/해제 시 요일 리스트 선택/해제
     */
    chkWeekday: function () {
        $("input[name='shipWeekdayList']").each(function(i) {
            $(this).prop("checked", $("#weekdayAll").prop("checked")).trigger('change');
        });
    },

    /**
     * '시/도' 를 선택하면 시군구 리스트를 불러와야함
     */
    getSigunguList: function() {
        var sidoTypeVal = $("#schSidoType").val();
        // df box 면 조회하지 않음
        if (sidoTypeVal === "") {
            return;
        }
        // 부르기 전에 초기화 작업
        $("#schSigunguType option").remove();
        $("#schSigunguType").html("<option value=''>시군구 전체</option>");
        CommonAjax.basic({
            url         : '/escrow/standard/getZipcodeSigunguList.json?sidoNm=' + sidoTypeVal,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                remoteShipMain.drawSigunguList(res);
            }
        });
    },
    /**
     * 시군구 리스트 그리기
     */
    drawSigunguList: function(res) {
        // 시군구가 없는 경우 (ex.세종특별자치시) 전체 option 만 그리기
        if (res.length <= 1) {
            $("#schSigunguType").append("<option value='ALL'>전체</option>");
            return;
        }

        res.forEach(function(sigungu) {
            var appendVal = "<option value='" + sigungu + "'>" + sigungu + "</option>";
            $("#schSigunguType").append(appendVal);
        });
    },
};

//조회/검색/저장 검증
var validCheck = {
    search: function () {
        var schStoreIdVal = $("#schStoreId").val();
        var schZipcodeVal = $("#schZipcode").val();
        var schAddrKeywordVal = $("#schAddrKeyword").val();

        if ($.jUtil.isEmpty(schStoreIdVal)) {
            alert("점포를 선택해 주세요.");
            return false;
        }

        var selWeekdayCnt = 0;
        $("input[name='shipWeekdayList']:checked").each(function(i) {
            selWeekdayCnt++;
        });

        if (selWeekdayCnt === 0) {
            alert('요일을 한개 이상 선택해 주세요.');
            return false;
        }

        // 우편번호에 입력한 값의 글자 & 입력문자 체크
        if (!$.jUtil.isEmpty(schZipcodeVal)) {
            if (!$.jUtil.isAllowInput(schZipcodeVal, ['NUM'])) {
                alert("우편번호는 숫자만 입력해주세요.");
                return false;
            }
            if (schZipcodeVal.length !== 5) {
                alert("우편번호는 5자리만 입력해주세요.");
                return false;
            }
        }
        // 도로명에 입력한 값이 2글자 미만인 경우
        if (!$.jUtil.isEmpty(schAddrKeywordVal) && schAddrKeywordVal.length < 2) {
            alert("도로명은 2글자 이상 입력해주세요.");
            return false;
        }

        $("#schStoreId").val(schStoreIdVal.lpad(4,"0"));
        return true;
    }
}

/** Grid Script */
var remoteShipGrid = {
    gridView: new RealGridJS.GridView("remoteShipGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function (remoteShipGridBaseInfo) {
        remoteShipGrid.initGrid(remoteShipGridBaseInfo);
        remoteShipGrid.initDataProvider(remoteShipGridBaseInfo);
    },
    initGrid: function (remoteShipGridBaseInfo) {
        remoteShipGrid.gridView.setDataSource(remoteShipGrid.dataProvider);
        remoteShipGrid.gridView.setStyles(remoteShipGridBaseInfo.realgrid.styles);
        remoteShipGrid.gridView.setDisplayOptions(remoteShipGridBaseInfo.realgrid.displayOptions);
        remoteShipGrid.gridView.setColumns(remoteShipGridBaseInfo.realgrid.columns);
        remoteShipGrid.gridView.setOptions(remoteShipGridBaseInfo.realgrid.options);

        // 그룹컬럼 width 설정.
        for (var gridGroupName of remoteShipGrid.gridView.getGroupNames()) {
            remoteShipGrid.gridView.setColumnProperty(gridGroupName, "width", 270);
        }

    },
    initDataProvider: function (remoteShipGridBaseInfo) {
        remoteShipGrid.dataProvider.setFields(remoteShipGridBaseInfo.dataProvider.fields);
        remoteShipGrid.dataProvider.setOptions(remoteShipGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        remoteShipGrid.dataProvider.clearRows();
        remoteShipGrid.dataProvider.setRows(dataList);
    }
};