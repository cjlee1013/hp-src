/** Main Script */
var itemShiftManageMain = {
    /**
     * init 이벤트
     */
    init: function() {
        itemShiftManageMain.bindingEvent();
        deliveryCore_category.initCategory();
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // 상단 [검색] 버튼
        $("#schBtn").bindClick(itemShiftManageMain.search);
        // 상단 [초기화] 버튼
        $("#schResetBtn").bindClick(itemShiftManageMain.reset, "itemShiftManageSearchForm");
        // 상단 [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(itemShiftManageMain.excelDownload);

        // 중간 [엑셀일괄등록] 버튼
        $("#excelUploadInsert").bindClick(itemShiftManageMain.openExcelPopup, "insert");
        // 중간 [엑셀일괄수정] 버튼
        $("#excelUploadUpdate").bindClick(itemShiftManageMain.openExcelPopup, "update");
        // 중간 [삭제] 버튼
        $("#removeBtn").bindClick(itemShiftManageMain.remove);

        // 하단 [저장] 버튼
        $("#saveBtn").bindClick(itemShiftManageMain.save);
        // 하단 [초기화] 버튼
        $("#resetBtn").bindClick(itemShiftManageMain.reset, "applyItemShiftManageForm");
    },

    /**
     * 데이터 검색
     */
    search: function() {
        // Form 데이터 유효성 체크
        if (!itemShiftManageMain.valid()) {
            return;
        }

        var data = $("#itemShiftManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/storeDelivery/getItemShiftManageList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                itemShiftManageGrid.setData(res);
                $('#itemShiftManageSearchCnt').html($.jUtil.comma(itemShiftManageGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 데이터 검색 전 Form 데이터 유효성 체크
     */
    valid: function() {
        var schStoreTypeVal = $("#schStoreType").val();
        if ($.jUtil.isEmpty(schStoreTypeVal)) {
            alert("점포유형을 올바르게 선택해 주세요.");
            return false;
        }
        var schKeywordTypeVal = $("#schKeywordType").val();
        if ($.jUtil.isEmpty(schKeywordTypeVal)) {
            alert("검색타입을 올바르게 선택해 주세요.");
            return false;
        }

        var schStoreIdVal = $("#schStoreId").val();
        var schKeywordVal = $("#schKeyword").val();
        if ($.jUtil.isEmpty(schStoreIdVal) && $.jUtil.isEmpty(schKeywordVal)) {
            alert("점포를 선택하거나 상품정보를 입력해 주세요.");
            return false;
        }

        // 점포를 선택했을 때만 lpad 후 호출되도록 분기처리
        if (!$.jUtil.isEmpty(schStoreIdVal)) {
            $("#schStoreId").val(schStoreIdVal.lpad(4, "0"));
        }
        return true;
    },
    /**
     * 검색영역/상세정보영역 폼 초기화
     * @param formId
     */
    reset: function(formId) {
        $("#"+formId).resetForm();

        // 상세정보 폼 reset 추가 작업
        if (formId === "applyItemShiftManageForm") {
            $("#selectItemPopBtn").show();
            $("#selectStorePopBtn").show(); $("#selectStorePopResult").show();
            $("#applyItemNo").text("");
            $("#applyItemNm").text("");
            $("#applyStoreType").text("");
            $("#applyStoreId").text("");
            $("#applyStoreNm").text("");
            $("#applyStoreList").text("");
            $("#applyStoreListCount").text("0");
        }
    },
    /**
     * 데이터 엑셀 다운로드
     */
    excelDownload: function() {
        if(itemShiftManageGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "상품별Shift관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        itemShiftManageGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 엑셀 일괄업로드 팝업창 open
     * @param funcType
     */
    openExcelPopup: function(funcType) {
        var templateUrl = "/static/templates/itemShiftManage_template.xlsx";
        if (funcType === "insert") {
            deliveryCore_popup.openExcelInsertPop(templateUrl, 'itemShiftManageMain.callBackSuccess');
        } else if (funcType === "update") {
            deliveryCore_popup.openExcelUpdatePop(templateUrl, 'itemShiftManageMain.callBackSuccess');
        }
    },
    callBackSuccess: function(res) {
        alert("- 요청: " + res.totalCnt + "\n- 성공: " + res.successCnt + "\n- 실패: " + res.errorCnt);
    },
    /**
     * 데이터 삭제
     */
    remove: function() {
        var checkRowIds = itemShiftManageGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("선택된 행이 없습니다.");
            return false;
        }

        var data = new Array();
        for (var checkRowId of checkRowIds) {
            var checkedRow = itemShiftManageGrid.dataProvider.getJsonRow(checkRowId);
            var tmpData = new Object();
            tmpData.storeType = checkedRow.storeType;
            tmpData.storeId = checkedRow.storeId;
            tmpData.itemNo = checkedRow.itemNo;
            data.push(tmpData);
        }

        CommonAjax.basic({
            url         : "/escrow/storeDelivery/deleteItemShiftList.json",
            data        : JSON.stringify(data),
            method      : "POST",
            contentType : 'application/json',
            callbackFunc: function() {
                itemShiftManageMain.search();
            }
        });
    },

    /**
     * 데이터 저장
     */
    save: function() {
        if (!itemShiftManageMain.saveValid()) {
            return false;
        }
        if (!confirm("저장하시겠습니까?")) {
            return false;
        }

        // 저장용 데이터 생성 (다건,단건)
        var data = "";
        var url = "";
        if ($.jUtil.isEmpty($("#applyStoreList").text())) {
            data = itemShiftManageMain.makeUpdateData();
            url = "/escrow/storeDelivery/saveItemShift.json";
        } else {
            data = itemShiftManageMain.makeInsertData();
            url = "/escrow/storeDelivery/saveItemShiftList.json";
        }
        CommonAjax.basic({
            url         : url,
            data        : JSON.stringify(data),
            method      : "POST",
            contentType : 'application/json',
            callbackFunc: function(res) {
                if (res.returnStatus === 200) {
                    alert("저장되었습니다.");
                } else {
                    console.log(res);
                }
            }
        });
    },
    /**
     * 데이터 저장 전 Form 데이터 유효성 체크
     */
    saveValid: function() {
        // 상품정보 check
        var itemNoVal = $("#applyItemNo").text();
        if ($.jUtil.isEmpty(itemNoVal)) {
            alert("저장할 상품을 선택해 주세요.");
            return false;
        }
        if (!$.jUtil.isAllowInput(itemNoVal, ['NUM'])) {
            alert("상품정보가 잘못되었습니다. 상품을 다시 선택해 주세요.");
            return false;
        }
        // 점포정보 check
        var selectedStoreType = $("#applyStoreType").text();
        var selectedStoreId = $("#applyStoreId").text();
        var selectedStoreList = $("#applyStoreList").text();
        if ($.jUtil.isEmpty(selectedStoreType) || ($.jUtil.isEmpty(selectedStoreList) && $.jUtil.isEmpty(selectedStoreId))) {
            alert("점포를 선택해 주세요.");
            return false;
        }
        return true;
    },
    /**
     * 저장용 데이터 생성 (insert, 다건)
     */
    makeInsertData: function() {
        var data = new Array();
        for (var storeInfo of JSON.parse($("#applyStoreList").text())) {
            var tmpData = new Object();
            tmpData.storeType = $("#applyStoreType").text();
            tmpData.storeId = storeInfo.storeId.lpad(4, "0");
            tmpData.itemNo = $("#applyItemNo").text();
            tmpData.shift1 = $("#shift1").is(":checked") ? "Y" : "N";
            tmpData.shift2 = $("#shift2").is(":checked") ? "Y" : "N";
            tmpData.shift3 = $("#shift3").is(":checked") ? "Y" : "N";
            tmpData.shift4 = $("#shift4").is(":checked") ? "Y" : "N";
            tmpData.shift5 = $("#shift5").is(":checked") ? "Y" : "N";
            tmpData.shift6 = $("#shift6").is(":checked") ? "Y" : "N";
            data.push(tmpData);
        }
        return data;
    },
    /**
     * 저장용 데이터 생성 (update, 단건)
     */
    makeUpdateData: function() {
        var data = new Object();
        data.storeType = $("#applyStoreType").text();
        data.storeId = $("#applyStoreId").text();
        data.itemNo = $("#applyItemNo").text();
        data.shift1 = $("#shift1").is(":checked") ? "Y" : "N";
        data.shift2 = $("#shift2").is(":checked") ? "Y" : "N";
        data.shift3 = $("#shift3").is(":checked") ? "Y" : "N";
        data.shift4 = $("#shift4").is(":checked") ? "Y" : "N";
        data.shift5 = $("#shift5").is(":checked") ? "Y" : "N";
        data.shift6 = $("#shift6").is(":checked") ? "Y" : "N";
        return data;
    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = itemShiftManageGrid.dataProvider.getJsonRow(selectRowId);
        console.log(rowDataJson);
        $("#selectItemPopBtn").hide();
        $("#selectStorePopBtn").hide(); $("#selectStorePopResult").hide();
        $("#applyItemNo").text(rowDataJson.itemNo);
        $("#applyItemNm").text(rowDataJson.itemNm);
        $("#applyStoreType").text(rowDataJson.storeType);
        $("#applyStoreId").text(rowDataJson.storeId);
        $("#applyStoreNm").text(rowDataJson.storeId); // todo.. name 으로 바꿔야함
        rowDataJson.shift1 === "Y" ? $("#shift1").prop("checked", true) : $("#shift1").prop("checked", false);
        rowDataJson.shift2 === "Y" ? $("#shift2").prop("checked", true) : $("#shift2").prop("checked", false);
        rowDataJson.shift3 === "Y" ? $("#shift3").prop("checked", true) : $("#shift3").prop("checked", false);
        rowDataJson.shift4 === "Y" ? $("#shift4").prop("checked", true) : $("#shift4").prop("checked", false);
        rowDataJson.shift5 === "Y" ? $("#shift5").prop("checked", true) : $("#shift5").prop("checked", false);
        rowDataJson.shift6 === "Y" ? $("#shift6").prop("checked", true) : $("#shift6").prop("checked", false);
    }
};

/** Grid Script */
var itemShiftManageGrid = {
    gridView: new RealGridJS.GridView("itemShiftManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        itemShiftManageGrid.initGrid();
        itemShiftManageGrid.initDataProvider();
        itemShiftManageGrid.event();
    },
    initGrid: function () {
        itemShiftManageGrid.gridView.setDataSource(itemShiftManageGrid.dataProvider);
        itemShiftManageGrid.gridView.setStyles(itemShiftManageGridBaseInfo.realgrid.styles);
        itemShiftManageGrid.gridView.setDisplayOptions(itemShiftManageGridBaseInfo.realgrid.displayOptions);
        itemShiftManageGrid.gridView.setColumns(itemShiftManageGridBaseInfo.realgrid.columns);
        itemShiftManageGrid.gridView.setOptions(itemShiftManageGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        itemShiftManageGrid.dataProvider.setFields(itemShiftManageGridBaseInfo.dataProvider.fields);
        itemShiftManageGrid.dataProvider.setOptions(itemShiftManageGridBaseInfo.dataProvider.options);
    },
    event: function() {
        itemShiftManageGrid.gridView.onDataCellClicked = function(gridView, index) {
            itemShiftManageMain.gridRowSelect(index.dataRow);
        };
    },
    setData: function (dataList) {
        itemShiftManageGrid.dataProvider.clearRows();
        itemShiftManageGrid.dataProvider.setRows(dataList);
    }
};