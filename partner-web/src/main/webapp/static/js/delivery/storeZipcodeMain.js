/** Main Script */
var storeZipcodeMain = {
    /**
     * init 이벤트
     */
    init: function() {
        storeZipcodeMain.bindingEvent();
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // 우편번호 검색어 숫자만 입력가능하도록 함
        $("#schZipcode").allowInput("keyup", ['NUM'], $(this).attr("id"));

        // [검색] 버튼
        $("#schBtn").bindClick(storeZipcodeMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(storeZipcodeMain.reset);
        // [주소] 시도명 변경시마다 해당 시군구명 조회
        $("#schSidoType").bindChange(storeZipcodeMain.getSigunguList);
    },

    /**
     * '시/도' 를 선택하면 시군구 리스트를 불러와야함
     */
    getSigunguList: function() {
        var sidoTypeVal = $("#schSidoType").val();
        // df box 면 조회하지 않음
        if (sidoTypeVal === "") {
            return;
        }
        // 부르기 전에 초기화 작업
        $("#schSigunguType option").remove();
        $("#schSigunguType").html("<option value=''>시군구 전체</option>");
        CommonAjax.basic({
            url         : '/escrow/standard/getZipcodeSigunguList.json?sidoNm=' + sidoTypeVal,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                storeZipcodeMain.drawSigunguList(res);
            }
        });
    },
    /**
     * 시군구 리스트 그리기
     */
    drawSigunguList: function(res) {
        // 시군구가 없는 경우 (ex.세종특별자치시) 전체 option 만 그리기
        if (res.length <= 1) {
            $("#schSigunguType").append("<option value='ALL'>전체</option>");
            return;
        }

        res.forEach(function(sigungu) {
            var appendVal = "<option value='" + sigungu + "'>" + sigungu + "</option>";
            $("#schSigunguType").append(appendVal);
        });
    },

    /**
     * 데이터 검색
     */
    search: function() {
        // Form 데이터 유효성 체크
        if (!storeZipcodeMain.valid()) {
            return;
        }

        var data = $("#storeZipcodeSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/storeDelivery/getStoreZipcodeList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                storeZipcodeGrid.setData(res);
                $('#storeZipcodeSearchCnt').html($.jUtil.comma(storeZipcodeGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 데이터 검색 전 Form 데이터 유효성 체크
     */
    valid: function() {
        // 우편번호도 입력안하고 점포도 선택안하고 주소도 입력안한 경우
        var schZipcodeVal = $("#schZipcode").val();
        var schStoreIdVal = $("#schStoreId").val();
        var schAddrKeywordVal = $("#schAddrKeyword").val();
        var schSidoTypeVal = $("#schSidoType").val();
        if ($.jUtil.isEmpty(schZipcodeVal) && $.jUtil.isEmpty(schStoreIdVal)
            && $.jUtil.isEmpty(schAddrKeywordVal) && $.jUtil.isEmpty(schSidoTypeVal)) {
            alert("점포, 우편번호, 주소조건을 하나 이상 입력해 주세요.");
            return;
        }
        // 우편번호에 입력한 값의 글자 & 입력문자 체크
        if (!$.jUtil.isEmpty(schZipcodeVal)) {
            if (!$.jUtil.isAllowInput(schZipcodeVal, ['NUM'])) {
                alert("우편번호는 숫자만 입력해주세요.");
                return false;
            }
            if (schZipcodeVal.length !== 5) {
                alert("우편번호는 5자리만 입력해주세요.");
                return false;
            }
        }
        // 도로명/건물명에 입력한 값이 2글자 미만인 경우
        if (!$.jUtil.isEmpty(schAddrKeywordVal) && schAddrKeywordVal.length < 2) {
            alert("도로명/건물명은 2글자 이상 입력해주세요.");
            return false;
        }

        // 점포를 선택했을 때만 lpad 후 호출되도록 분기처리
        if (!$.jUtil.isEmpty(schStoreIdVal)) {
            $("#schStoreId").val(schStoreIdVal.lpad(4, "0"));
        }
        return true;
    },
    /**
     * 검색영역 폼 초기화
     */
    reset: function() {
        $("#storeZipcodeSearchForm").resetForm();
    },
};

/** Grid Script */
var storeZipcodeGrid = {
    gridView: new RealGridJS.GridView("storeZipcodeGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        storeZipcodeGrid.initGrid();
        storeZipcodeGrid.initDataProvider();
    },
    initGrid: function () {
        storeZipcodeGrid.gridView.setDataSource(storeZipcodeGrid.dataProvider);
        storeZipcodeGrid.gridView.setStyles(storeZipcodeGridBaseInfo.realgrid.styles);
        storeZipcodeGrid.gridView.setDisplayOptions(storeZipcodeGridBaseInfo.realgrid.displayOptions);
        storeZipcodeGrid.gridView.setColumns(storeZipcodeGridBaseInfo.realgrid.columns);
        storeZipcodeGrid.gridView.setOptions(storeZipcodeGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        storeZipcodeGrid.dataProvider.setFields(storeZipcodeGridBaseInfo.dataProvider.fields);
        storeZipcodeGrid.dataProvider.setOptions(storeZipcodeGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        storeZipcodeGrid.dataProvider.clearRows();
        storeZipcodeGrid.dataProvider.setRows(dataList);
    }
};