/**
 * 업체관리 > 셀러샵관리 > 셀러샵 등록/수정
 */
$(document).ready(function() {
    sellerShop.init();
});

// sellerShop 관리
var sellerShop = {
    isCheckShopNm : false // 중복확인 여부 ( 셀러샵 이름 )
    , isCheckShopUrl : false // 중복확인 여부 ( 셀러샵 URL )
    , isUpdate : false
    /**
     * 초기화
     */
    , init : function() {
        this.bindingEvent();
        this.getSellerShopInfo();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#setBtn').bindClick(sellerShop.set); // sellerShop 등록,수정
        $('#resetBtn').bindClick(sellerShop.resetForm); // sellerShop 입력폼 초기화
        $('#shopInfo').calcTextLength('keyup', '#shopInfoCnt');

        // 파일 선택시
        $('input[name="fileArr"]').change(function() {
            sellerShop.img.addImg();
        });

        // 셀러샵 로고 사용 여부 ( 삭제 요청 )
        // $('input[name="shopLogoYn"]').change(function() {
        //     var shopLogoYn = $('input[name="shopLogoYn"]:checked').val();
        //     if(shopLogoYn === 'Y') {
        //         $('#displayLogoDiv').show();
        //     } else {
        //         $('#displayLogoDiv').hide();
        //         sellerShop.img.deleteImg('logo');
        //     }
        // });

        // 고객센터 사용 여부
        $('input[name="csUseYn"]').change(function() {
            var csUseYn = $('input[name="csUseYn"]:checked').val();
            if(csUseYn === 'Y') {
                $('#csInfoDiv').show();
            } else {
                $('#csInfoDiv').hide();
                $('#csInfo').val('')
            }
        });
    },
    /**
     * 셀러샵 정보 가져오기.
     */
    getSellerShopInfo : function() {
        CommonAjax.basic({
            url : "/partner/sellerShop/getSellerShopDetail.json",
            data : null,
            method : "GET",
            callbackFunc : function (res) {
                if(!$.jUtil.isEmpty(res)) {
                    sellerShop.isUpdate = true;
                    sellerShop.img.setImg('profile', {
                        'imgUrl': res.shopProfileImg,
                        'src'   : hmpImgUrl + "/provide/view?processKey=SellerProfileImage&fileId=" + res.shopProfileImg,
                        'changeYn' : 'Y'
                    });
                    if(res.shopLogoYn === 'Y') {
                        sellerShop.img.setImg('logo', {
                            'imgUrl': res.shopLogoImg,
                            'src'   : hmpImgUrl + "/provide/view?processKey=SellerLogoImage&fileId=" + res.shopLogoImg,
                            'changeYn' : 'Y'
                        });
                    }
                    sellerShop.isCheckShopNm = true;
                    $('#shopNm').attr('readonly', true);
                    $('#shopNm').val(res.shopNm);
                    $('#nameValidationBtn').hide();
                    sellerShop.isCheckShopUrl = true;
                    $('#shopUrl').attr('readonly', true);
                    $('#shopUrl').val(res.shopUrl);
                    $('#urlValidationBtn').hide();
                    $('#sellerShopUrlPc, #sellerShopUrlMobile').text(res.shopUrl);
                    $('#shopInfo').val(res.shopInfo);
                    $('#shopInfoCnt').html(res.shopInfo.length);
                    $('input[name="csUseYn"][value="' + res.csUseYn + '"]').trigger('click');
                    $('input[name="shopLogoYn"][value="' + res.shopLogoYn + '"]').trigger('click');
                    $('#csInfo').val(res.csInfo);
                    $('input[name="useYn"][value="' + res.useYn + '"]').trigger('click');
                    $('#shopUrlDiv').hide();

                }
            }
        });
    },
    /**
     * Url 정보 생성
     */
    setHttpUrl : function(text) {
        $('#sellerShopUrlPc, #sellerShopUrlMobile').html(text);
    },
    /**
     * 셀러샵 이름 OR 셀러샵 URL 중복확인
     */
    validation : function(type) {
        var checkKeyword;
        switch (type) {
            case 'shopNm' :
                checkKeyword = $('#shopNm').val();
                if(!$.jUtil.isAllowInput(checkKeyword, ['ENG', 'NUM', 'KOR']) || checkKeyword.length > 10) {
                    alert("최대 10자리의 한글, 영문, 숫자, 특수문자 사용가능합니다.");
                    $("#shopNm").focus();
                    return;
                } else if(sellerShop.isCheckShopNm == true) {
                    alert("이미 중복 확인이 완료 되었습니다.");
                    return false;
                }
                break;
            case 'shopUrl' :
                checkKeyword = $('#shopUrl').val();
                if(!$.jUtil.isAllowInput(checkKeyword, ['ENG_LOWER', 'NUM', 'NOT_SPACE']) || checkKeyword.length > 12) {
                    alert("최대 12자리의 영문 소문자, 숫자만 사용가능합니다.");
                    $("#shopUrl").focus();
                    return;
                } else if(sellerShop.isCheckShopUrl == true) {
                    alert("이미 중복 확인이 완료 되었습니다.");
                    return false;
                }
                break;
        }

        CommonAjax.basic({
            url : "/partner/sellerShop/setSellerShopValidation.json",
            data : { type : type, checkKeyword : checkKeyword },
            method : "GET",
            callbackFunc : function (res) {
                switch (type) {
                    case 'shopNm' :
                        if (res.returnCnt == 0) {
                            sellerShop.isCheckShopNm = true;
                            $('#shopNm').attr('readonly', true);
                            alert("사용 가능합니다.");
                        } else {
                            alert("이미 사용 중인 셀러샵 이름(닉네임) 입니다.");
                        }
                        break;
                    case 'shopUrl' :
                        if (res.returnCnt == 0) {
                            sellerShop.isCheckShopUrl = true;
                            $('#shopUrl').attr('readonly', true);
                            alert("사용 가능합니다.");
                        } else {
                            alert("이미 사용 중인 셀러샵 URL 입니다.");
                        }
                        break;
                }
            }
        });
    },
    /**
     * sellerShop 등록/수정
     */
    set : function() {

        $('#shopNm').attr('class', 'inp-base w-490');
        $('#shopUrl').attr('class', 'inp-base w-325');

        if(!sellerShop.valid.set()) {
            return false;
        }

        CommonAjax.basic({
            url:'/partner/sellerShop/setSellerShop.json',
            data : JSON.stringify($('#sellerShopSetForm').serializeObject())
            , method : "POST"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                alert(res.returnMsg);
                sellerShop.getSellerShopInfo();
            }
        });
    },
    /**
     * sellerShop 입력/수정 폼 초기화
     */
    resetForm : function() {
        if(sellerShop.isUpdate) {
            $('#shopInfo, #csInfo').val('');
            $('#shopInfoCnt').html('0');
            $('input:radio[name="csUseYn"]:input[value="N"]').trigger('click');
            $('input:radio[name="shopLogoYn"]:input[value="N"]').trigger('click');
            $('input:radio[name="useYn"]:input[value="Y"]').trigger('click');
            sellerShop.img.init();
        } else {
            sellerShop.isCheckShopNm = false;
            sellerShop.isCheckShopUrl = false;
            $('#shopNm, #shopUrl, #shopInfo, #csInfo').val('');
            $('#shopInfoCnt').html('0');
            $('#shopNm').attr('readonly', false);
            $('#shopUrl').attr('readonly', false);
            $('#sellerShopUrlPc, #sellerShopUrlMobile').text('');
            $('input:radio[name="csUseYn"]:input[value="N"]').trigger('click');
            $('input:radio[name="shopLogoYn"]:input[value="N"]').trigger('click');
            $('input:radio[name="useYn"]:input[value="Y"]').trigger('click');
            sellerShop.img.init();
        }
    }
};
/**
 * sellerShop 검색,입력,수정 validation check
 */
sellerShop.valid = {
    set : function () {
        if($.jUtil.isEmpty($('#shopNm').val())) {
            $('#shopNm').attr('class', 'inp-base w-490 err').focus();
            alert('셀러샵 이름을 입력해 주세요.');
            return false;
        } else {
            if(!sellerShop.isCheckShopNm) {
                $('#shopNm').focus();
                alert('중복 체크를 진행해 주세요.');
                return false;
            }
        }

        if($.jUtil.isEmpty($('#shopProfileImg').val())) {
            alert('프로필 이미지를 등록해 주세요.');
            return false;
        }

        if($.jUtil.isEmpty($('#shopLogoImg').val())) {
            alert('로고 이미지를 등록해 주세요.');
            return false;
        }

        if($.jUtil.isEmpty($('#shopUrl').val())) {
            $('#shopUrl').focus();
            alert('셀러샵 URL 입력해 주세요.');
            return false;
        } else {
            if(!sellerShop.isCheckShopUrl) {
                $('#shopUrl').attr('class', 'inp-base w-325 err').focus();
                alert('중복 체크를 진행해 주세요.');
                return false;
            }
        }

      return true;
    }
};

/**
 * 셀러샵관리 이미지
 */
sellerShop.img = {
    setImgType : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        sellerShop.img.setImg('profile', null);
        sellerShop.img.setImg('logo', null);
        commonEditor.inertImgurl = null;
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(imgType, params) {
        switch (imgType) {
            case 'profile':
                if (params) {
                    $('#profileImgUrlTag').prop('src', params.src);
                    $('#shopProfileImg').val(params.imgUrl);
                    $('#profileChangeYn').val(params.changeYn);

                    $('#displayProfileImgDiv').hide();
                    $('#displayNoneProfileImgDiv').show();
                } else {
                    $('#profileImgUrlTag').prop('src', '');
                    $('#shopProfileImg').val('');
                    $('#profileChangeYn').val('N');

                    $('#displayProfileImgDiv').show();
                    $('#displayNoneProfileImgDiv').hide();
                }
                break;
            case 'logo':
                if (params) {
                    $('#logoImgUrlTag').prop('src', params.src);
                    $('#shopLogoImg').val(params.imgUrl);
                    $('#logoChangeYn').val(params.changeYn);

                    $('#displayLogoImgDiv').hide();
                    $('#displayNoneLogoImgDiv').show();
                } else {
                    $('#logoImgUrlTag').prop('src', '');
                    $('#shopLogoImg').val('');
                    $('#logoChangeYn').val('N');

                    $('#displayLogoImgDiv').show();
                    $('#displayNoneLogoImgDiv').hide();
                }
                break;
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {
        var file = $('#imageFile');
        var ext = $('#imageFile').get(0).files[0].name.split('.');
        var params = {
            processKey : 'SellerProfileImage',
            mode : 'IMG'
        };

        if(sellerShop.img.setImgType === 'logo') {
            params.processKey = 'SellerLogoImage';
        }

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                var errorMsg = "";
                for (var i in resData.fileArr) {
                    var f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        sellerShop.img.setImg(sellerShop.img.setImgType, {
                            'imgUrl': f.fileStoreInfo.fileId,
                            'src'   : hmpImgUrl + "/provide/view?processKey=" + params.processKey + "&fileId=" + f.fileStoreInfo.fileId,
                            'ext'   : ext[1],
                            'changeYn' : 'Y'
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 미리보기 팝업
     * @param thisParam
     */
    imagePreview : function(obj) {
        var imgUrl = $(obj).find('img').prop('src');
        if(imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        sellerShop.img.setImg(imgType, null);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(type) {
        sellerShop.img.setImgType = type;
        $('input[name=fileArr]').click();
    }
};