$(document).ready(function() {

    sellerShopItemMain.init();
    sellerShopItemListGrid.init();
    CommonAjaxBlockUI.global();
    parentMain = sellerShopItemMain;
});



var sellerShopItemListGrid = {

    gridView : new RealGridJS.GridView("sellerShopItemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        sellerShopItemListGrid.initGrid();
        sellerShopItemListGrid.initDataProvider();
        sellerShopItemListGrid.event();

    },
    initGrid : function() {
        sellerShopItemListGrid.gridView.setDataSource(sellerShopItemListGrid.dataProvider);

        sellerShopItemListGrid.gridView.setStyles(sellerShopItemListGridBaseInfo.realgrid.styles);
        sellerShopItemListGrid.gridView.setDisplayOptions(sellerShopItemListGridBaseInfo.realgrid.displayOptions);
        sellerShopItemListGrid.gridView.setColumns(sellerShopItemListGridBaseInfo.realgrid.columns);
        sellerShopItemListGrid.gridView.setOptions(sellerShopItemListGridBaseInfo.realgrid.options);

    },
    initDataProvider : function() {
        sellerShopItemListGrid.dataProvider.setFields(sellerShopItemListGridBaseInfo.dataProvider.fields);
        sellerShopItemListGrid.dataProvider.setOptions(sellerShopItemListGridBaseInfo.dataProvider.options);
    },
    event : function() {


    },
    setData : function(dataList) {
        sellerShopItemListGrid.dataProvider.clearRows();
        sellerShopItemListGrid.dataProvider.setRows(dataList);
    },
    addDataList : function (dataList) {
        sellerShopItemListGrid.dataProvider.addRows(dataList);
    }

};
/**
 * 셀러샵관리 > 추천상품 전시관리 셀러샵 아이템 리스트
 */
var sellerShopItemMain = {
    /**
     * 초기화
     */
    init : function() {

        this.event();
        this.searchItemList();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $('#setSellerShopItmBtn').bindClick(sellerShopItemMain.setSellerShopItm); // 셀러샵 정보 저장
        $('#resetBtn').bindClick(sellerShopItemMain.resetForm); // 셀러샵 등록 정보 초기화
    },
    //일반, 매뉴얼 리스트 조회
    searchItemList : function () {


        CommonAjax.basic({url:'/partner/sellerShopItem/getSellerShopItemList.json', data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                sellerShopItemListGrid.setData(res.itemList);

                if(!$.jUtil.isEmpty(res.dispType)) {
                    $("input:radio[name='dispType']:input[value='" + res.dispType + "']").prop("checked", true);
                }

                $('#sellershopItemTotalCount').html($.jUtil.comma(sellerShopItemListGrid.gridView.getItemCount()));
            }});

    },
    /**
     * 셀러샵 전시 관리 아이템 등록/수정
     */
    setSellerShopItm : function() {


        var setItem = new Object();

        sellerShopItemMain.resetItemPriority();

        setItem.itemList = sellerShopItemListGrid.dataProvider.getJsonRows();
        setItem.dispType = $("input:radio[name='dispType']:checked").val();


        CommonAjax.basic({url:'/partner/sellerShopItem/setSellerShopItem.json',data: JSON.stringify(setItem), method:"POST", successMsg:null, contentType:"application/json",callbackFunc:function(res) {
                alert(res.returnMsg);
                sellerShopItemMain.searchItemList();
            }});


    },
    /**
     * 셀러샵 입력/수정 폼 초기화
     */
    resetForm : function() {
        sellerShopItemMain.searchItemList();
    },
    /**
     * 선택한 row 삭제
     */
    removeCheckData : function() {
        var check = sellerShopItemListGrid.gridView.getCheckedRows(true);

        if (check.length == 0) {
            alert("삭제하실 대상을 선택해 주세요.");
            return;
        }
        sellerShopItemListGrid.dataProvider.removeRows(check, false);

    },
    /**
     * 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    moveRow : function(obj) {
        var checkedRows = sellerShopItemListGrid.gridView.getCheckedRows(false);

        if (checkedRows.length == 0) {
            alert("순서변경할 상품 선택해 주세요.");
            return false;
        }
        realGridMoveRow(sellerShopItemListGrid, $(obj).attr("key"), checkedRows);
    },

    /**
     * 상품 우선순위 재 정렬
     */
    resetItemPriority : function(){
        // Priority 정렬
        sellerShopItemListGrid.dataProvider.getJsonRows(0,-1).forEach(function (data, index) {

            var itemInfo = sellerShopItemListGrid.gridView.getValues(index);
            itemInfo.priority = parseInt(index) + 1
            sellerShopItemListGrid.gridView.setValues(index , itemInfo ,true);

        });
    },

    /**
     * 상품리스트 설정
     */
    callback : function(itemList) {

        var gridList = sellerShopItemListGrid.dataProvider.getJsonRows();


        itemList.forEach(function (item) {
            gridList.forEach(function (gridItem, idx) {
                if(gridItem.itemNo == item.itemNo ){
                    gridList.splice(idx, 1)
                }
            });
        });
        sellerShopItemListGrid.setData(itemList);
        sellerShopItemListGrid.addDataList(gridList);
        sellerShopItemMain.resetItemPriority();



    }
};

