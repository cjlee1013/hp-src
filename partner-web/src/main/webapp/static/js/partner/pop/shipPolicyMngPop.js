$(document).ready(function() {

    shipPolicyListGrid.init();
    shipPolicyMngPop.init();
    CommonAjaxBlockUI.global();
});

//배송정책 그리드
var shipPolicyListGrid = {

    gridView : new RealGridJS.GridView("shipPolicyListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        shipPolicyListGrid.initGrid();
        shipPolicyListGrid.initDataProvider();
        shipPolicyListGrid.event();
    },
    initGrid : function () {
        shipPolicyListGrid.gridView.setDataSource(shipPolicyListGrid.dataProvider);

        shipPolicyListGrid.gridView.setStyles(shipPolicyListGridBaseInfo.realgrid.styles);
        shipPolicyListGrid.gridView.setDisplayOptions(shipPolicyListGridBaseInfo.realgrid.displayOptions);
        shipPolicyListGrid.gridView.setColumns(shipPolicyListGridBaseInfo.realgrid.columns);
        shipPolicyListGrid.gridView.setOptions(shipPolicyListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function () {
        shipPolicyListGrid.dataProvider.setFields(shipPolicyListGridBaseInfo.dataProvider.fields);
        shipPolicyListGrid.dataProvider.setOptions(shipPolicyListGridBaseInfo.dataProvider.options);
    },
    event : function () {
        shipPolicyListGrid.gridView.onDataCellClicked = function(gridView, index) {
            shipPolicyMngPop.shipPolicyGridRowSelect(index.dataRow);
        };
    },
    setData : function (dataList) {
        shipPolicyListGrid.dataProvider.clearRows();
        shipPolicyListGrid.dataProvider.setRows(dataList);
    }
};

/**
 * 배송정책 관리 팝업
 * @type {{init: shipPolicyMngPop.init, initCategorySelectBox: shipPolicyMngPop.initCategorySelectBox, initSearchDate: shipPolicyMngPop.initSearchDate, closePop: shipPolicyMngPop.closePop, event: shipPolicyMngPop.event}}
 */
var shipPolicyMngPop = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
        this.resetForm();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        var $main = shipPolicyMngPop.getRoot();

        $main.find('#shipFee, #claimShipFeeP, #extraIslandFee, #extraJejuFee, #diffQty, #freeCondition' ).allowInput("keyup", ["NUM"]);

        $main.find('#resetShipPolicyMngBtn').bindClick(shipPolicyMngPop.resetForm);
        $main.find('#setShipPolicyMngBtn').bindClick(shipPolicyMngPop.setShipPolicy)

        $main.find("input[name='shipKind']").on('click', function() {
            shipPolicyMngPop.changeShipKind($(this).val());
        });

        $main.find("input[name='shipType']").on('click', function() {
            shipPolicyMngPop.changeShipType($(this).val());
        });

        $main.find("input[name='diffYn']").on('click', function() {
            shipPolicyMngPop.changeDiffYn($(this).val());
        });

        $main.find("#shipMethodDS").on('change', function() {
            $main.find('#shipMethod').val($(this).val());
        });

        $main.find("#shipArea").on('change', function() {
            shipPolicyMngPop.changeShipArea($(this).val());
        });

        //출고기한 변경 이벤트
        $main.find('#releaseDayP').on('change', function(){
            shipPolicyMngPop.getReleaseDay($(this).val());
        });

        //사업자 주소 동일 적용
        $('#copyPartnerAddrPop').bindClick(shipPolicyMngPop.copyPartnerAddr);
        //출고지 주소 동일 적용
        $('#copyReleaseAddrPop').bindClick(shipPolicyMngPop.copyReleaseAddr);
    },
    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('shipPolicyMngPop', shipPolicyMngPop.getShipPolicyList);
        shipPolicyListGrid.gridView.resetSize();
    },

    /**
     * modal close
     */
    closeModal: function() {
        shipPolicyMngPop.resetForm();
        $ui.modalClose('shipPolicyMngPop');
    },
    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#shipPolicyMngPop');
    },
    /**
     * 배송정책 등록/수정
     */
    setShipPolicy: function () {

        var $main = shipPolicyMngPop.getRoot();

        if(!shipPolicyMngPop.valid()){
            return false;
        }

        CommonAjax.basic({
            url: "/partner/setShipPolicy.json",
            method: "POST",
            data: $('#shipPolicySetForm').serializeArray(),

            callbackFunc: function (res) {
                alert(res.returnMsg);
                $main.find('#shipPolicyNoInfo').text(res.returnKey);
                $main.find('#shipPolicyNoP').val(res.returnKey);


                //저장 후 팝업 종료 되기 때문에 배송방법은 disabled 처리 하지 않는다.
                $ui.modalClose('shipPolicyMngPop', itemCommon.ship.getSellerShipList(true, res.returnKey));
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 배송정책 조회
     */
    getShipPolicyList : function () {

        var $main = shipPolicyMngPop.getRoot();
        CommonAjax.basic({
            url: '/partner/getShipPolicyList.json?',
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                shipPolicyListGrid.setData(res);
                $main.find('#shipPolicyTotalCnt').html($.jUtil.comma(shipPolicyListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 배송정책 그리드 row 선택
     * @param selectRowId
     */
    shipPolicyGridRowSelect: function (selectRowId) {

        var $main = shipPolicyMngPop.getRoot();
        shipPolicyMngPop.resetForm();

        var rowData = shipPolicyListGrid.dataProvider.getJsonRow(selectRowId);

        $main.find('#shipPolicyNoInfo').text(rowData.shipPolicyNo); //배송정책번호
        $main.find('#shipPolicyNoP').val(rowData.shipPolicyNo); //배송정책번호

        $main.find('#shipPolicyNm').val(rowData.shipPolicyNm); //배송정보명
        $main.find('#shipMethod').val(rowData.shipMethod); //배송방법
        $main.find('#shipMethodDS').val(rowData.shipMethod); //배송방법
        $main.find('#shipMethodDS').prop('disabled', true);

        $main.find('input:radio[name="shipType"]:input[value="' + rowData.shipType + '"]').trigger('click'); //배송유형
        $main.find('input:radio[name="shipKind"]:input[value="' + rowData.shipKind + '"]').trigger('click'); //배송비종류
        $main.find('input:radio[name="diffYn"]:input[value="'+ rowData.diffYn + '"]').trigger('click'); //차등여부
        $main.find('input:radio[name="prepaymentYn"]:input[value="' + rowData.prepaymentYn + '"]').trigger('click'); //선결제여부
        $main.find('input:radio[name="dispYn"]:input[value="' + rowData.dispYn + '"]').trigger('click');

        $main.find('#shipFee').val(rowData.shipFee); //배송비
        $main.find('#freeCondition').val(rowData.freeCondition); //무료조건비
        $main.find('#diffQty').val(rowData.diffQty > 0 ? rowData.diffQty : ""); //수량별차등
        $main.find('#shipArea').val(rowData.shipArea);
        $main.find('#extraJejuFee').val(rowData.extraJejuFee);
        $main.find('#extraIslandFee').val(rowData.extraIslandFee);
        $main.find('#safeNumberUseYn').val(rowData.safeNumberUseYn);
        $main.find('#useYn').val(rowData.useYn);

        //배송비추가정보
        $main.find('#claimShipFeeP').val(rowData.claimShipFee);
        $main.find('#releaseZipcodeP').val(rowData.releaseZipcode);
        $main.find('#releaseAddr1P').val(rowData.releaseAddr1);
        $main.find('#releaseAddr2P').val(rowData.releaseAddr2);

        $main.find('#returnZipcodeP').val(rowData.returnZipcode);
        $main.find('#returnAddr1P').val(rowData.returnAddr1);
        $main.find('#returnAddr2P').val(rowData.returnAddr2);

        $main.find('#releaseDayP').val(rowData.releaseDay);
        $main.find('#releaseTimeP').val(rowData.releaseTime);
        shipPolicyMngPop.getReleaseDay(rowData.releaseDay);

        $main.find("input:checkbox[id='holidayExceptYn']").prop("checked", rowData.holidayExceptYn == "Y" ? true : false);
        $main.find("input:checkbox[id='defaultYn']").prop("checked", rowData.defaultYn == "Y" ? true : false);

        shipPolicyMngPop.changeShipArea(rowData.shipArea);
        shipPolicyMngPop.getReleaseDay(rowData.releaseDay);
    },
    /**
     * 배송유형 변경
     * @param shipType
     */
    changeShipType : function (shipType) {

        var $main = shipPolicyMngPop.getRoot();
        switch (shipType) {
            case "ITEM" :
                var shipKind = $main.find('input[name="shipKind"]:checked').val();
                if(shipKind == "FIXED") {
                    $main.find('#diffYnY').prop("disabled", false);
                }
                break;
            case "BUNDLE" :
                $main.find('input:radio[name="diffYn"]:input[value="N"]').trigger('click');
                $main.find('#diffYnY').prop("disabled", true);
                break;
        }
    },
    /**
     * 배송비유형 (무료/유료/선택적항목)
     * 1) 배송비
     * 2) 무료배송 조건
     */
    changeShipKind : function(shipKind) {

        var $main = shipPolicyMngPop.getRoot();
        switch (shipKind) {
            case "FREE" :
                $main.find('#shipFee').prop('readonly', true);
                $main.find('#freeCondition').prop('readonly', true);
                $main.find('#shipFee, #freeCondition').val('');

                $main.find('input:radio[name="prepaymentYn"]:input[value="Y"]').trigger('click');
                $main.find('input:radio[name="diffYn"]:input[value="N"]').trigger('click');

                $main.find('#prepaymentYnN').prop("disabled", true);
                $main.find('#diffYnY').prop("disabled", true);
                break;

            case "FIXED" :
                $main.find('#shipFee').prop('readonly', false);
                $main.find('#freeCondition').prop('readonly', true);
                $main.find('#freeCondition').val('');
                $main.find('#prepaymentYnN').prop("disabled", false);

                var type = $main.find('input[name="shipType"]:checked').val();
                if(type == "ITEM") {
                    $main.find('#diffYnY').prop("disabled", false);
                }else{
                    //수량별차등 불가
                    $main.find('input:radio[name="diffYn"]:input[value="N"]').trigger('click');
                    $main.find('#diffYnY').prop("disabled", true);
                }
                break;

            case "COND" :
                $main.find('#shipFee').prop('readonly', false);
                $main.find('#freeCondition').prop('readonly', false);

                $main.find('input:radio[name="diffYn"]:input[value="N"]').trigger('click');
                $main.find('input:radio[name="prepaymentYn"]:input[value="Y"]').trigger('click');

                $main.find('#diffYnY').prop("disabled", true);
                $main.find('#prepaymentYnN').prop("disabled", true);

                break;
        }
    },
    /**
     * 수량별 차등 여부
     * 1) 수량별 차등 갯수
     */
    changeDiffYn :function(diffYn) {
        var $main = shipPolicyMngPop.getRoot();
        switch(diffYn) {
            case "Y" :
                $main.find('#diffQty').prop('readonly' , false);

                //수량별 차등 시 착불불가
                $main.find('input:radio[name="prepaymentYn"]:input[value="Y"]').trigger('click');
                $main.find('#prepaymentYnN').prop("disabled", true);

                //도서산간배송비 입력 불가
                $main.find('#extraJejuFee').prop("disabled", true);
                $main.find('#extraIslandFee').prop("disabled", true);
                break;

            case "N" :
                $main.find('#diffQty').prop('readonly' , true);
                $main.find('#diffQty').val('');

                var shipKind = $main.find('input[name="shipKind"]:checked').val();
                if(shipKind == 'FIXED') {
                    //유료일때만 착불 활성화
                    $main.find('#prepaymentYnN').prop("disabled", false);
                }

                $main.find('#extraJejuFee').prop("disabled", false);
                $main.find('#extraIslandFee').prop("disabled", false);
                break;
        }
    },
    /**
     * 배송가능지역(전국/도서산간지역제외)
     * 1) 제주추가 배송비
     * 2) 도서산간 추가배송비
     */
    changeShipArea : function(shipArea) {

        var $main = shipPolicyMngPop.getRoot();
        switch(shipArea) {
            case "ALL" :
                $main.find('#extraJejuFee').prop('readonly', false);
                $main.find('#extraIslandFee').prop('readonly', false);
                break;

            case "EXCLUDE" :
                $main.find('#extraJejuFee').val('').prop('readonly', true);
                $main.find('#extraIslandFee').val('').prop('readonly', true);
                break;

            default:
                $main.find('#extraJejuFee').val('').prop('readonly', true);
                $main.find('#extraIslandFee').val('').prop('readonly', true);
                break;
        }
    },
    /**
     * 출고기한 날짜 선택
     * @param val
     */
    getReleaseDay : function(val) {
        var $main = shipPolicyMngPop.getRoot();
        if (val != 1) {
            //오늘발송 외 기타
            $main.find(".releaseDetailInfo").hide();
        }else{
            //오늘발송
            $main.find('.releaseDetailInfo').show();
        }
    },
    /**
     * 배송정책 등록 초기화
     */
    resetForm: function () {

        var $main = shipPolicyMngPop.getRoot();
        $main.find('#shipPolicySetForm').resetForm();

        $main.find('#shipPolicyNoP').val("");
        $main.find('#shipPolicyNoInfo').text("");

        //radio
        $main.find('input:radio[name="shipType"]:input[value="BUNDLE"]').trigger('click');
        $main.find('input:radio[name="shipKind"]:input[value="FREE"]').trigger('click');
        $main.find('input:radio[name="diffYn"]:input[value="N"]').trigger('click');
        $main.find('input:radio[name="prepaymentYn"]:input[value="Y"]').trigger('click');
        $main.find('input:radio[name="dispYn"]:input[value="Y"]').trigger('click');

        $main.find("input:checkbox[id='defaultYn']").prop("checked", false);
        $main.find("input:checkbox[id='samePartnerAddr']").prop("checked", false);
        $main.find("input:checkbox[id='sameReleaseAddr']").prop("checked", false);
        $main.find("input:checkbox[id='holidayExceptYn']").prop("checked", false);

        $main.find('#shipMethodDS').val('');
        $main.find('#shipMethod').val('');
        $main.find('#shipMethodDS').prop('disabled', false);
        $main.find('#shipArea').val("ALL")
        $main.find('#useYn').val("Y");
        $main.find('#safeNumberUseYn').val("Y")

        shipPolicyMngPop.changeShipArea("ALL");
    },
    /**
     * 사업자 주소 동일적용
     */
    copyPartnerAddr : function() {
        var $main = shipPolicyMngPop.getRoot();

        if ($("#copyPartnerAddrPop").prop('checked') == true) {
            CommonAjax.basic({
                url:'/partner/getSellerAddress.json',
                method:'get',
                callbackFunc:function(resData) {
                    $main.find("#releaseZipcodeP").val(resData.zipCode);
                    $main.find("#releaseAddr1P").val(resData.addr1);
                    $main.find("#releaseAddr2P").val(resData.addr2);

                    if ($("#copyReleaseAddrPop").prop('checked') == true) {
                        $main.find("#returnZipcodeP").val($main.find("#releaseZipcodeP").val());
                        $main.find("#returnAddr1P").val($main.find("#releaseAddr1P").val());
                        $main.find("#returnAddr2P").val($main.find("#releaseAddr2P").val());
                    }
                },
                errorCallbackFunc : function() {
                    alert('등록된 사업자 주소가 없습니다!');
                    $("#copyPartnerAddrPop").prop("checked", false);
                }
            });
        } else {
            $main.find("#releaseZipcodeP").val("");
            $main.find("#releaseAddr1P").val("");
            $main.find("#releaseAddr2P").val("");

            if ($("#copyReleaseAddrPop").prop('checked') == true) {
                $main.find("#returnZipcodeP").val("");
                $main.find("#returnAddr1P").val("");
                $main.find("#returnAddr2P").val("");
            }
        }
    },
    /**
     * 출고지 주소 동일적용
     */
    copyReleaseAddr : function() {
        var $main = shipPolicyMngPop.getRoot();

        if ($("#copyReleaseAddrPop").prop('checked') == true) {
            $main.find("#returnAddr2P").prop('readonly', true);

            $main.find("#returnZipcodeP").val($main.find("#releaseZipcodeP").val());
            $main.find("#returnAddr1P").val($main.find("#releaseAddr1P").val());
            $main.find("#returnAddr2P").val($main.find("#releaseAddr2P").val());

        } else {
            $main.find("#returnAddr2P").prop('readonly', false);
            $main.find("#returnZipcodeP").val("");
            $main.find("#returnAddr1P").val("");
            $main.find("#returnAddr2P").val("");
        }
    },

    valid : function() {
        var $main = shipPolicyMngPop.getRoot();

        var shipPolicyNm = $main.find('#shipPolicyNm').val();

        //배송정책명
        if ($.jUtil.isEmpty(shipPolicyNm)) {
            return $.jUtil.alert('배송정책명을 입력해주세요.', 'shipPolicyNm');
        }

        //출하기한
        if ($.jUtil.isEmpty($main.find('#releaseDayP').val())) {
            alert("출고기한을 확인해주세요.");
            $main.find('#releaseDayP').focus();
            return false;
        }else{
            if($main.find('#releaseDayP').val() == "1") {
                if ($.jUtil.isEmpty($main.find('#releaseTimeP').val())) {
                    alert("출고기한을 확인해주세요 ")
                    return false;
                }
            }
        }
        //배송정보
        if ($.jUtil.isEmpty($main.find('#releaseZipcodeP').val())|| $.jUtil.isEmpty($main.find('#releaseAddr1P').val())){
            alert("출고지 정보를 입력해주세요.")
            return false;
        }

        if ($.jUtil.isEmpty($main.find('#returnZipcodeP').val()) || $.jUtil.isEmpty($main.find('#returnAddr1P').val())){
            alert("회수지 정보를 입력해주세요.")
            return false;
        }

        var shipKind = $main.find('input[name="shipKind"]:checked').val();
        //배송유형 (조건부일경우)
        if ("COND" == shipKind) {
            if ($.jUtil.isEmpty($main.find('#freeCondition').val()) || $main.find('#freeCondition').val() == "0") {
                alert("무료배송 조건을 입력해주세요 (금액은 1 ~ 9,999,999까지 입력 가능합니다)")
                $main.find('#freeCondition').focus();
                return false;
            }
        }

        //배송유형(고정일경우)
        if("FIXED" == shipKind || "COND" == shipKind) {
            if($.jUtil.isEmpty($main.find('#shipFee').val()) || $main.find('#shipFee').val() == "0" ) {
                return $.jUtil.alert('배송비를 입력해주세요 (금액은 1 ~ 999,999까지 입력 가능합니다.)', 'shipFee');
            }
        }

        //반품배송비
        if ($.jUtil.isEmpty($main.find("#claimShipFeeP").val())) {
            return $.jUtil.alert('반품/교환배송비를 입력해주세요. (금액은 0 ~ 999,999까지 입력 가능합니다.)', 'claimShipFeeP');
        }

        //수량별 차등
        if ($main.find('input[name="diffYn"]:checked').val() == 'Y' && !$.jUtil.isAllowInput($main.find("#diffQty").val(), ['NUM'])) {
            return $.jUtil.alert('수량별 차등수량은 숫자만 입력해주세요.', 'diffQty');
        }

        return true;
    },

    /**
     * 출하지 주소 callback
     * @param res
     */
    releaseZipcode : function (res) {
        var $main = shipPolicyMngPop.getRoot();
        $main.find("input:checkbox[id='copyPartnerAddrPop']").prop("checked", false);
        $main.find('#releaseZipcodeP').val(res.zipCode);
        $main.find('#releaseAddr1P').val(res.roadAddr);
    },
    /**
     * 회수지 주소 callback
     * @param res
     */
    returnZipcode : function (res) {
        var $main = shipPolicyMngPop.getRoot();
        $main.find("input:checkbox[id='copyReleaseAddrPop']").prop("checked", false);
        $main.find('#returnZipcodeP').val(res.zipCode);
        $main.find('#returnAddr1P').val(res.roadAddr);
    },
};


