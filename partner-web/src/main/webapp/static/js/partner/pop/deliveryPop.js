/**
 * 판매업체 - 택배사 팝업
 */

var delivery = {
    init : function() {
        this.event();
        this.initAll();
        this.getPostCodeStatus();
    },
    event : function() {

        // 유효성 검사
        $(document).on("click", "button[id^=getCreditCdBtn_]", function() {
            delivery.getCreditCd($(this).data("idx"));
        });

        // 초기화
        $(document).on("click", "button[id^=initCreditCdBtn_]", function() {
            delivery.initCreditCd($(this).data("idx"));
        });

        $(document).on('click', '#postCdRequestBtn', function(){
            delivery.postCdRequestPop();
        });

        // 적용 버튼
        $("#setDeliveryBtn").on("click", function() {
            delivery.setDelivery();
        });

        // 취소 버튼
        $("#cancelDeliveryBtn").on("click", function() {
            // delivery.drawDeliveryList();
        });
    },
    initAll : function () {
        $("#setForm").each(function () {
            this.reset();
        });

        delivery.drawDeliveryList();
    },
    // 택배사 정보 그리기
    drawDeliveryList : function () {
        if (deliveryList.length > 0) {
            var html = "";
            for (var idx in deliveryList) {
                var data = deliveryList[idx];

                html += "<tr id='tr_" + data.companyCd + "' data-index='" + idx + "'>";
                html += "    <td class='text-center'>" + data.companyNm + "</td>";
                html += "    <td>";
                if (data.companyCd == 'D001') { //우체국택배
                    html += "        <div class='ui form inline mg-l-60'>";
                    html += "            <input type='text' id='creditCd_" + idx + "' class='ui input medium mg-r-5' style='width: 150px;' readonly placeholder='우체국 점포코드'>";
                    html += "            <input type='hidden' id='deliveryNo_" + idx + "' >";
                    html += "            <input type='hidden' id='companyCd_" + idx + "' value='" + data.companyCd + "'>";
                    html += "            <button type='button' class='ui button medium' id='postCdRequestBtn'>이용신청</button>";
                    html += "        </div>";
                    html += "        <div class='mg-l-60 mg-t-10' style='display:none' id='postStatusDiv'></div>";
                } else {
                    html += "        <div class='ui form inline mg-l-60'>";
                    html += "            <input type='text' id='creditCd_" + idx + "' class='ui input medium mg-r-5' style='width: 150px;' maxlength='12'>";
                    html += "            <input type='hidden' id='deliveryNo_" + idx + "' >";
                    html += "            <input type='hidden' id='companyCd_" + idx + "' value='" + data.companyCd + "'>";
                    html += "            <input type='hidden' id='creditFlag_" + idx + "' value=false>";
                    html += "            <button type='button' class='ui button medium font-malgun mg-r-5' id='getCreditCdBtn_" + idx + "' data-idx='" + idx + "'>인증</button>";
                    html += "            <button type='button' class='ui button medium font-malgun' id='initCreditCdBtn_" + idx + "' data-idx='" + idx + "'>초기화</button>";
                    html += "        </div>";
                }
                html += "    </td>";
                html += "</tr>";
            }
            $("#deliveryTb > tbody:last").html(html);


            if (deliveryInfo != null && deliveryInfo.length > 0) {
                for (var idx in deliveryInfo) {
                    var info = deliveryInfo[idx];
                    var targetTrIndex = $('#tr_'+info.companyCd).data("index");
                    $("#deliveryNo_" + targetTrIndex).val(info.deliveryNo);
                    $("#creditCd_" + targetTrIndex).val(info.creditCd).prop("readonly", true);
                    $("#creditFlag_" + targetTrIndex).val("true");
                }
            }
        }
    },
    // 유효성 검사
    getCreditCd : function (idx) {
        if ($("#creditCd_" + idx).val() == "") {
            alert("택배사 계약코드를 입력하세요.");
            return;
        }

        CommonAjax.basic({
            url:"/partner/pop/getCompanyCreditValidInfo.json"
            , data:{companyCd:$("#companyCd_" + idx).val(), creditCd:$("#creditCd_" + idx).val()}
            , method : "GET"
            , callbackFunc:function(res) {
                if (res.returnCode != "1"){
                    $("#creditFlag_" + idx).val("false");
                    alert("유효하지 않은 코드 입니다.");
                    $("#creditCd_" + idx).attr("readOnly", false);
                }
                else {
                    $("#creditFlag_" + idx).val("true");
                    alert("유효한 계약 코드 입니다.");
                    $("#creditCd_" + idx).attr("readOnly", true);
                }
            }
        });
    },
    // 초기화
    initCreditCd : function (idx) {
        $("#creditCd_" + idx).val("");
        $("#creditFlag_" + idx).val("false");
        $("#creditCd_" + idx).attr("readOnly", false);
    },
    // 적용
    setDelivery : function () {
        var creditCheck = true;
        var dataArray = new Array();

        if (deliveryList.length > 0) {
            for (var idx in deliveryList) {
                if (deliveryList[idx].companyCd != 'D001') {
                    if ($("#creditCd_" + idx).val() != "" && $("#creditFlag_" + idx).val() == "false") {
                        creditCheck = false;
                        break;
                    }
                }

                var dataObj = new Object();

                if ($("#creditCd_" + idx).val() != "") {
                    dataObj.deliveryNo = $("#deliveryNo_" + idx).val();
                    dataObj.companyCd = $("#companyCd_" + idx).val();
                    dataObj.creditCd = $("#creditCd_" + idx).val();

                    dataArray.push(dataObj);
                }
            }

            if (!creditCheck) {
                alert("유효성을 확인해 주세요.");
                return;
            }

            if (!confirm("적용하시겠습니까?")) {
                return;
            }

            eval("opener." + callback)(dataArray);
            self.close();
        }
    },

    //우체국택배 이용신청 상태 조회
    getPostCodeStatus : function(){
        if (!partnerId) return;
        var targetTrIndex = $('#tr_D001').data("index");
        CommonAjax.basic({
            url : "/partner/pop/getReturnPostCodeRequest.json",
            data : { partnerId : partnerId },
            method : "POST",
            callbackFunc : function(res) {
                switch (res.requestStatus) {
                    case 'R1' :
                    case 'R2' :
                        $('#postStatusDiv').html('승인 처리 중입니다.').show();
                        $("#creditCd_" + targetTrIndex).val('');
                        break;
                    case 'R3' : //완료
                        $('#postStatusDiv').html('승인 되었습니다.<br>재신청 시 기존 신청 정보 삭제 후 신규 신청 됩니다.').show();
                        $("#creditCd_" + targetTrIndex).val(res.postStoreCd);
                        break;
                    case 'R8' : //실패
                        $('#postStatusDiv').html('승인에 실패하였습니다.<br>재신청 시 기존 신청 정보 삭제 후 신규 신청 됩니다.').show();
                        break;
                    default :
                        $('#postStatusDiv').hide();
                        break;
                }
            },
            errorCallbackFunc : function(res) { //요청 내역이 없는 경우
                $('#postStatusDiv').hide();
            }
         });
    },

    //우체국택배 이용신청 팝업 열기
    postCdRequestPop : function(){
        if (!partnerId) {
            alert('판매업체 등록 후 이용신청 가능합니다.');
            return;
        }
        windowPopupOpen('/partner/pop/postCdRequestPop?partnerId='+partnerId, 'postCdRequestPop', 606, 530);
    },

};
