$(document).ready(function() {

    returnMain.init();
    claimReturnListGrid.init();

    parentMain = returnMain;
    parentGrid = claimReturnListGrid;
    CommonAjaxBlockUI.global();

    // 파라미터로 schStatus 있으면 해당 보드 카운트 클릭
    if (!$.jUtil.isEmpty(schStatus)) {
        $("#"+schStatus).trigger("click");
    }
});

/**
 * 상품관리 > 셀러/점포상품관리 > 상품 등록/수정
 */
var returnMain = {
    /**
     * 초기화
     */
    init : function() {

        returnMain.event();
        claimMainCommon.initSearchDate('-1w', 'schStartDt', 'schEndDt');
        claimMainCommon.setClaimBoardCnt('R');
    },

    /**
     * 이벤트 바인딩
     */
    event : function() {

        // 보드 카운트 클릭 조회
        $("#request, #delay, #complete, #pickComplete,#pending").on("click", function() { returnMain.search($(this)) });

        // 검색 조건 초기화
        $('#searchResetBtn').bindClick(returnMain.searchFormReset);

        // 검색 버튼
        $('#searchBtn').bindClick(function(){ returnMain.search('')});

        // 엑셀 다운로드
        $('#excelDownloadBtn').bindClick(returnMain.excelDownload);

        // 건수 재조회
        $('#refreshClaimCnt').bindClick(function() {claimMainCommon.setClaimBoardCnt('R')});

        // 검색일자 radio 버튼 클릭
        $('input:radio[name="setSchDt"]').on('change', function() {
            returnMain.searchDate();
        });
    },
    searchDate : function () {
        var flag = $('input[name="setSchDt"]:checked').val();
        claimMainCommon.initSearchDate(flag, 'schStartDt' , 'schEndDt');
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        claimReturnListGrid.excelDownload();
    },

    /**
     * 반품 리스트 검색
     */
    search : function($this) {

        if(!$.jUtil.isEmpty($this)){
            $('#schStatus').val($this.attr('id'));
        }else{
            $('#schStatus').val('');
        }

        //검색 유효성 검사
        if(!claimMainCommon.claimListSearchValidation()){
            return false;
        }

        var data = _F.getFormDataByJson($("#claimSearchForm"));

        CommonAjax.basic({
                url : '/claim/getReturnClaimList.json',
                data : data,
                method : "POST",
                 contentType: 'application/json',
                successMsg:null,
                callbackFunc:function(res) {
                    claimReturnListGrid.setData(res);
                    var ClaimBundleArray = [];
                    var _idx = 0;
                    for(var i=0; i<res.length; i++){
                        if(_idx++ !== (res.length - 1)){
                            if(res[i].claimBundleNo !== res[_idx].claimBundleNo){
                                    ClaimBundleArray.push(res[i].bundleNo);
                            }
                        } else {
                            ClaimBundleArray.push(res[i].bundleNo);
                        }
                    }

                    claimReturnListGrid.gridView.setCheckableExpression(
                       claimMainCommon.setRealGridArraysCheckable(claimReturnListGrid, ClaimBundleArray, 'claimBundleNo'), true
                    );

                    $('#pickReqBtn,#pickCompleteBtn,#approveBtn,#pendingBtn,#chgPickInfoBtn,#rejectBtn').attr('disabled', true);
                }
        });

        CommonAjax.basic({
                url : '/claim/getClaimListCnt.json',
                data : data,
                method : "POST",
                 contentType: 'application/json',
                successMsg:null,
                callbackFunc:function(res) {
                    $('#claimTotalCount').html(res);
                }
        });

    },

    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {

        $("#claimSearchForm").resetForm();
        claimMainCommon.initSearchDate('-7d', 'schStartDt', 'schEndDt');

    },
    /**
     * 수거요청 팝업창 open
     */
    openPickReqPop : function(){

        var checkRowIds = claimReturnListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }

        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        pickRequest.openModal();
    },
    /**
     * 수거완료 팝업창 open
     */
    openPickCompletePop : function(){
        var checkRowIds = claimReturnListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }
        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        pickComplete.openModal();
    },

    /**
     * 반품승인 팝업창 open
     */
    openReturnApprovePop: function() {
        var checkRowIds = claimReturnListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }
        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        returnApprove.openModal();
    },

    /**
     * 반품보류 팝업창 open
     */
    openClaimPendingPop: function() {
        var checkRowIds = claimReturnListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }

        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        claimPending.openModal();
    },

    /**
     * 수거정보변경 팝업창 open
     */
    openChgPickShippingPop : function(){
        var checkRowIds = claimReturnListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }

        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        chgPickShipping.openModal();

    },

    /**
     * 반품승인 팝업창 open
     */
    openReturnRejectPop: function() {
        var checkRowIds = claimReturnListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }
        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        returnReject.openModal();
    },



    /**
     * callback function
     */
    callBackSearch : function () {
        returnMain.search();
        claimMainCommon.setClaimBoardCnt('R');
    },

    /**
    * 그리드 row 선택
    * @param selectRowId
    */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = parentGrid.dataProvider.getJsonRow(selectRowId);
        var claimStatusCode = rowDataJson.claimStatusCode;
        var pickStatusCode = rowDataJson.pickStatusCode;

       $('#pickReqBtn,#pickCompleteBtn,#approveBtn,#pendingBtn,#chgPickInfoBtn,#rejectBtn').attr('disabled', true);

       //수거요청 : 반품신청(C1), 반품승인(C2), 반품보류(C8) && 수거예약(N0), 수거대기(NN), 수거요청(P0), 수거실패(P8)
       if((claimStatusCode == 'C1' || claimStatusCode == 'C2' || claimStatusCode == 'C8') && (pickStatusCode == 'N0' ||pickStatusCode == 'NN' || pickStatusCode == 'P0' || pickStatusCode == 'P8')){
          $('#pickReqBtn').attr('disabled', false);
       }
       //수거완료 : 반품신청(C1), 반품승인(C2), 반품보류(C8) && 수거예약(N0), 수거대기(NN), 수거요청(P0), 요청완료(P1), 수거중(P2), 수거실패(P8)
       if((claimStatusCode == 'C1' || claimStatusCode == 'C2' || claimStatusCode == 'C8') && (pickStatusCode == 'N0' || pickStatusCode == 'NN' || pickStatusCode == 'P1' || pickStatusCode == 'P2' || pickStatusCode == 'P8')){
           $('#pickCompleteBtn').attr('disabled', false);

       }
       //반품승인 : 반품신청(C1), 반품승인(C2), 반품보류(C8) && 수거대기(NN), 요청완료(P1), 수거중(P2), 수거완료(P3), 수거실패(P8)
       if((claimStatusCode == 'C1' || claimStatusCode == 'C2' ||claimStatusCode == 'C8') && (pickStatusCode == 'N0' || pickStatusCode == 'NN' || pickStatusCode == 'P1' || pickStatusCode == 'P2' || pickStatusCode == 'P3' || pickStatusCode == 'P8')){
           $('#approveBtn').attr('disabled', false);
       }
       //반품보류 : 반품신청(C1) && 수거대기(NN), 요청완료(P1), 수거중(P2), 수거실패(P8)
       if(claimStatusCode == 'C1' && (pickStatusCode == 'N0' ||pickStatusCode == 'NN' || pickStatusCode == 'P1' || pickStatusCode == 'P2' || pickStatusCode == 'P8')){
           $('#pendingBtn').attr('disabled', false);
       }
       //수거지변경 : 반품신청(C1), 반품보류(C8) && 수거대기(NN), 수거중(P2), 수거실패(P8)
       if((claimStatusCode == 'C1' || claimStatusCode == 'C8') && (pickStatusCode == 'N0' ||pickStatusCode == 'NN' || pickStatusCode == 'P2' || pickStatusCode == 'P8')){
           $('#chgPickInfoBtn').attr('disabled', false);
       }
       //반품거부 : 반품신청(C1), 반품보류(C8)
        if(claimStatusCode == 'C1' || claimStatusCode == 'C8'){
            $('#rejectBtn').attr('disabled', false);
        }

    }
};


// 클레임관리 그리드
var claimReturnListGrid = {
  gridView : new RealGridJS.GridView("claimReturnListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
    claimReturnListGrid.initGrid();
    claimReturnListGrid.initDataProvider();
    claimReturnListGrid.event();
  },
  initGrid : function() {
    claimReturnListGrid.gridView.setDataSource(claimReturnListGrid.dataProvider);

    claimReturnListGrid.gridView.setStyles(claimReturnListGridBaseInfo.realgrid.styles);
    claimReturnListGrid.gridView.setDisplayOptions(claimReturnListGridBaseInfo.realgrid.displayOptions);
    claimReturnListGrid.gridView.setColumns(claimReturnListGridBaseInfo.realgrid.columns);
    claimReturnListGrid.gridView.setOptions(claimReturnListGridBaseInfo.realgrid.options);
    claimReturnListGrid.gridView.setCheckBar({"exclusive": "true", "showGroup":"true"})
    claimReturnListGrid.gridView.setColumnProperty("claimBundleNo", "mergeRule", {criteria:"value"});

  },
  initDataProvider : function() {
    claimReturnListGrid.dataProvider.setFields(claimReturnListGridBaseInfo.dataProvider.fields);
    claimReturnListGrid.dataProvider.setOptions(claimReturnListGridBaseInfo.dataProvider.options);
  },
  event : function() {

    // 배송번호 렌더링
    claimReturnListGrid.gridView.setColumnProperty("claimBundleNo", "renderer", {
          type: "link",
          url: "unnecessary",
          requiredFields: "claimBundleNo",
          showUrl: false
    });

     // 송장번호 렌더링
    claimReturnListGrid.gridView.setColumnProperty("invoiceNo", "renderer", {
          type: "link",
          url: "unnecessary",
          requiredFields: "invoiceNo",
          showUrl: false
    });

    // 수거 송장번호 렌더링
    claimReturnListGrid.gridView.setColumnProperty("pickInvoiceNo", "renderer", {
          type: "link",
          url: "unnecessary",
          requiredFields: "pickInvoiceNo",
          showUrl: false
    });

     // 그리드 선택
     claimReturnListGrid.gridView.onDataCellClicked = function(grid, index) {
        //클레임 상태에 따른 버튼 노출
        returnMain.gridRowSelect(index.dataRow);
     };

     // 그리드 선택
     claimReturnListGrid.gridView.onItemChecked = function(grid, index, checked) {
         //클레임 상태에 따른 버튼 노출
         if(checked){
             returnMain.gridRowSelect(index);
         }else{
             $('#pickReqBtn,#pickCompleteBtn,#approveBtn,#pendingBtn,#chgPickInfoBtn').attr('disabled', true);
         }
     }


    // 그리드 선택
    claimReturnListGrid.gridView.onLinkableCellClicked = function(grid, index, url) {
        var rowDataJson = claimReturnListGrid.dataProvider.getJsonRow(index.dataRow);
        var invoiceNo = null;
        var dlvCd = null;
        var dlvCdNm = null;

        if (index.fieldName == "claimBundleNo") {
            itemClaimDetailPop.openModal();
        }else if (index.fieldName == "invoiceNo" && !$.jUtil.isEmpty(rowDataJson.invoiceNo)) {
            invoiceNo = rowDataJson.invoiceNo;
            dlvCd = rowDataJson.dlvCd;
            dlvCdNm = rowDataJson.dlvCdNm;
            claimShipHistoryPop.openModal(invoiceNo, dlvCd, dlvCdNm);
        }else if (index.fieldName == "pickInvoiceNo" && !$.jUtil.isEmpty(rowDataJson.pickInvoiceNo)) {
            invoiceNo = rowDataJson.pickInvoiceNo;
            dlvCd = rowDataJson.pickDlvCd;
            dlvCdNm = rowDataJson.pickDlvCdNm;
            claimShipHistoryPop.openModal(invoiceNo, dlvCd, dlvCdNm);
        }else if (index.fieldName == "exchInvoiceNo" && !$.jUtil.isEmpty(rowDataJson.exchInvoiceNo)){
            invoiceNo = rowDataJson.exchInvoiceNo;
            dlvCd = rowDataJson.exchDlvCd;
            dlvCdNm = rowDataJson.exchDlvCdNm;
            claimShipHistoryPop.openModal(invoiceNo, dlvCd, dlvCdNm);
        }
    };
  },
  setData : function(dataList) {
    claimReturnListGrid.dataProvider.clearRows();
    claimReturnListGrid.dataProvider.setRows(dataList);
  },

  excelDownload: function() {
    if(claimReturnListGrid.gridView.getItemCount() == 0) {
      alert("검색 결과가 없습니다.");
      return false;
    }

    if(!confirm("구매자의 개인 정보는 암호화 하여야 하며, 판매 목적을 달성한 경우 즉시 파기 되어야 합니다.\n"
        + "구매자의 개인정보를 이용하여 광고 홍보등의 수집 목적 외로 사용해서는 안됩니다.\n"
        + "상기 사항 등 관계 법령 위반으로 발생하는 모든 민,형사상 책임은 판매자 본인에게 있습니다.\n\n"
        + "상기 내용을 숙지하였으며, 이에 동의합니다.")){
        return false;
    }

    var _date = new Date();
    var fileName =  "반품관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

    claimReturnListGrid.gridView.exportGrid({
      type: "excel",
      target: "local",
      fileName: fileName + ".xlsx",
      showProgress: true,
      progressMessage: "엑셀 Export중입니다."
    });
  }
};

