var claimPopCommon = {


    /**
     * 첨부파일 적용
     * uploadFileName 값이 있을경우
     * 버튼 활성화/unerline/link 적용
     * **/
    setUploadFile : function(res){
        if(!$.jUtil.isEmpty(res.uploadFileName)){
            $('#uploadFileName').css("text-decoration", "underline");
            $("#uploadFileNameUrl").val(res.uploadFileName);
            claimPopCommon.getUploadContentType($('#uploadFileNameUrl').val(), '');
        }else{
            $('#uploadFileName').hide();
        }

        if(!$.jUtil.isEmpty(res.uploadFileName2)){
            $('#uploadFileName2').css("text-decoration", "underline");
            $("#uploadFileNameUrl2").val(res.uploadFileName2);
            claimPopCommon.getUploadContentType($('#uploadFileNameUrl2').val(), '2');

        }else{
            $("#uploadFileName2").hide();
        }

        if(!$.jUtil.isEmpty(res.uploadFileName3)){
            $('#uploadFileName3').css("text-decoration", "underline");
            $("#uploadFileNameUrl3").val(res.uploadFileName3);
            claimPopCommon.getUploadContentType($('#uploadFileNameUrl3').val(), '3');

        }else{
            $("#uploadFileName3").hide();
        }

        if($.jUtil.isEmpty(res.uploadFileName) && $.jUtil.isEmpty(res.uploadFileName2) && $.jUtil.isEmpty(res.uploadFileName3)){
            $('#uploadRow').hide();
        }

    },
    /**
     * 첨부파일 확장자 조회
     * image/[확장자]
     * **/
    getUploadContentType : function(fileId, seq) {
        var dataParam = {
            'fileId': fileId
        };
        var contentType;

        //파라미터 세팅
        //취소요청
        CommonAjax.basic(
            {
                url: '/claim/info/getClaimImageInfo.json?'.concat(
                    jQuery.param(dataParam)),
                method: "GET",
                contentType: 'application/json?',
                successMsg: null,
                callbackFunc: function (res) {
                    $("#uploadFileNameType" + seq).val(
                        res.contentType.replace('image/', ''));
                }
            });

    }



}