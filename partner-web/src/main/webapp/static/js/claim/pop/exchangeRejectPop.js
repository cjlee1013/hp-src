
var exchangeReject = {

    init : function() {
        exchangeReject.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = exchangeReject.getRoot();

        $modal.find('#uploadFileName').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl').val(), '첨부파일1.'+$modal.find('#uploadFileNameType').val(), 'ClaimAddImage');
        });

        $modal.find('#uploadFileName2').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl2').val(), '첨부파일2.'+$modal.find('#uploadFileNameType2').val(), 'ClaimAddImage');
        });

        $modal.find('#uploadFileName3').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl3').val(), '첨부파일3.'+$modal.find('#uploadFileNameType3').val(), 'ClaimAddImage');
        });
    },

    /**
     * 팝업창 setting
     * **/
    set : function(){

        var $modal = exchangeReject.getRoot();

        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var claimBundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"claimBundleNo");
        var claimTypeCode = parentGrid.dataProvider.getValue(checkRowIds[0],"claimType");
        var buyerNm = parentGrid.dataProvider.getValue(checkRowIds[0],"buyerNm");
        var claimType = claimTypeCode == 'R' ? '반품' : claimTypeCode == 'X'? '교환' : '취소';

        //클레임 상세정보 조회
        CommonAjax.basic({
            url:'/claim/getClaimDetail.json?claimBundleNo=' + claimBundleNo + "&claimType="+ claimTypeCode,
            data: null,
            contentType: 'application/json',
            method: "GET",
            callbackFunc: function(res) {
                $modal.find('#title1').text('> '+ claimType +'상품 (배송번호 : '+res.bundleNo+ "/클레임 번호 : " + res.claimBundleNo + ")");//상품 정보 title
                $modal.find('#title2').text('> '+ claimType + '신청 정보');//신청정보 title
                $modal.find('#buyerNm').text(buyerNm);
                $modal.find('#claimReasonType').text("["+res.claimReasonType+"]");
                $modal.find('#claimReasonDetail').text(res.claimReasonDetail);

                //첨부파일 설정
                claimMainCommon.setUploadFile($modal, res);

                //반송배송지 설정
                claimMainCommon.setReturnShipAmt($modal , res);

                //상품 리스트 설정
                for(var i=0; i<res.claimDetailItemList.length; i++){
                    var tableRow = $('<tr>');
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].itemName));
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].optItemName));
                    tableRow.append($('<td style="text-align: center; ">').text(res.claimDetailItemList[i].claimItemQty));
                    $modal.find('#itemInfoTable > tbody:last').append(tableRow);
                }
            }
        });
    },


    /**
     * 클레임 거부 요청
     * **/
    save : function(){
        var $modal = exchangeReject.getRoot();

        //유효성 검사
        if($.jUtil.isEmpty($modal.find('#rejectReasonType').val())) {
            alert('거부사유를 선택해주세요.')
            return false;
        }else if($.jUtil.isEmpty($modal.find('#rejectReasonDetail').val())){
            alert('거부상세 사유를 입력해주세요.')
            return false;
        }




        if (!confirm('거부상태로 변경하시겠습니까?')) {
            return false;
        }

        var checkRowIds = parentGrid.gridView.getCheckedRows();

        var param = {
            "claimNo" : parentGrid.dataProvider.getValue(checkRowIds[0],"claimNo"),
            "claimBundleNo" : parentGrid.dataProvider.getValue(checkRowIds[0],"claimBundleNo"),
            "regId" : "PARTNER",
            "reasonType" : $modal.find('#rejectReasonType').val(),
            "reasonTypeDetail" : $modal.find('#rejectReasonDetail').val(),
            "claimType" : "X"
        }

        CommonAjax.basic({
                url         : '/claim/setClaimReject.json',
                data        : JSON.stringify(param),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "0000") {
                        alert(res.returnMessage);
                        exchangeReject.closeModal();
                    } else {
                        alert('거부 처리 성공');
                        exchangeReject.closeModal();

                    }
                }
            }
        );
    },

    /**
     * modal open
     * **/
    openModal : function () {
        $ui.modalOpen('exchangeRejectPop', exchangeReject.set());
    },
    /**
     * modal close
     */
    closeModal: function() {
        $('#itemInfoTable > tbody').empty();
        parentMain.callBackSearch();
        $ui.modalClose('exchangeRejectPop');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#exchangeRejectPop');
    }

}

