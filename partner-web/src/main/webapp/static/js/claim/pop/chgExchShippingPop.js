$(document).ready(function() {
  chgExchShipping.init();
});


var chgExchShipping = {

    init : function() {
        this.bindingEvent();
        this.initSearchDate();
    },

    initSearchDate : function () {
        Calendar.datePicker("chgExchShipDt");
        Calendar.setCalDate("chgExchShipDt", 0);
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = chgExchShipping.getRoot();
        // [수거/배송방법] 선택
        $modal.find('#selectDeliveryType').bindChange(function() { claimMainCommon.chgDeliveryType($modal, null)});

        //연락처 - 숫자만 입력
        $("#exchMobileNo_1,#exchMobileNo_2,exchMobileNo_3").allowInput("keyup", ["NUM","NOT_SPACE"], $(this).attr("id"));

        //송장번호 - 숫자만 입력
        $("#exchInvoiceNo").allowInput("keyup", ["NUM","NOT_SPACE"], $(this).attr("id"));
    },

    /**
     * 팝업창 setting
     * **/
    set : function(){

        var $modal = chgExchShipping.getRoot();

        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var claimBundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"claimBundleNo");
        var claimTypeCode = parentGrid.dataProvider.getValue(checkRowIds[0],"claimType");
        var claimType = claimTypeCode == 'R' ? '반품' : claimTypeCode == 'X'? '교환' : '취소';
        var claimNo = parentGrid.dataProvider.getValue(checkRowIds[0],"claimNo");

        //클레임 상세정보 조회
        CommonAjax.basic({
            url:'/claim/getClaimDetail.json?claimBundleNo=' + claimBundleNo + "&claimType="+ claimTypeCode,
            data: null,
            contentType: 'application/json',
            method: "GET",
            callbackFunc: function(res) {

                $modal.find('#title1').text('> '+ claimType +'상품 (배송번호 : '+res.bundleNo+ "/클레임 번호 : " + res.claimBundleNo + ")");//상품 정보 title
                $modal.find('#title2').text('> '+ claimType + '신청 정보');//신청정보 title


                $.each(res.claimExchShippingDto,function(key, value){
                    $modal.find('#'+key).val(value);
                });

                $modal.find('#claimBundleNo').val(res.claimBundleNo);
                $modal.find('#claimNo').val(claimNo);

                $.jUtil.valSplit($modal.find("input[name$='exchMobileNo']" ).val(), '-', 'exchMobileNo');

                //첨부파일 설정
                claimMainCommon.setUploadFile($modal, res);

                //수거 방법 초기화
                claimMainCommon.chgDeliveryType(chgExchShipping.getRoot(), res.claimExchShippingDto.exchShipTypeCode);

                 $modal.find('#exchShipType').text(res.claimExchShippingDto.exchShipType);

                var exchDlv = $.jUtil.isEmpty(res.claimExchShippingDto.exchDlv) === true ? '' : res.claimExchShippingDto.exchDlv;
                var exchInvoiceNo = $.jUtil.isEmpty(res.claimExchShippingDto.exchInvoiceNo) === true ? '' : res.claimExchShippingDto.exchInvoiceNo;
                var exchD0Dt = $.jUtil.isEmpty(res.claimExchShippingDto.exchD0Dt) === true ? '' : res.claimExchShippingDto.exchD0Dt.substr(0,10);

                //수거 방법 설정
                switch (res.claimExchShippingDto.exchShipTypeCode) {
                    case 'C' ://택배배송
                        //택배사명 + 수거접수타입 + 송장번호
                        $modal.find('#exchShipTypeText').text(" | " + exchDlv + " | " + exchInvoiceNo);
                        $modal.find("#shipSearchBtn").show();    //배송조회 버튼
                        break;
                    case 'D' ://직배송
                        $modal.find('#exchShipTypeText').text(" | " + exchD0Dt);
                        $modal.find('#chgExchShipDt').val(exchD0Dt);
                        break;
                    case 'N' ://배송없음
                        //현재는 빈값으로 노출
                        break;
                    case 'P' ://우편배송
                        if(!$.jUtil.isEmpty(res.claimExchShippingDto.exchMemo)){
                            $modal.find('#exchShipTypeText').text("| 메모 : " + res.claimExchShippingDto.exchMemo);
                        }
                        break;
                }

                //상품 리스트 설정
                for(var i=0; i<res.claimDetailItemList.length; i++){
                    var tableRow = $('<tr>');
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].itemName));
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].optItemName));
                    tableRow.append($('<td style="text-align: center; ">').text(res.claimDetailItemList[i].claimItemQty));
                    $modal.find('#itemInfoTable > tbody:last').append(tableRow);
                }
            }
        });
    },

    /**
     * 수거 요청
     * **/
    save : function(){
        var $modal = chgExchShipping.getRoot();


        $modal.find('#exchDetailAddr').val($modal.find('#exchAddrDetail').val());
        $modal.find('#exchMobileNo').val($.jUtil.valNotSplit('exchMobileNo', '-'));
        $modal.find('#exchInvoiceNo').val().replace(' ','');

        //수거 안함 선택 시 유효성 검사 skip
        if(!chgExchShipping.validation()){
            return false;
        }

        var pickShipType = $modal.find('#selectDeliveryType').val();//현재 선택된 수거배송 방법

        //수거 방법이 변경됐을 경우
        if(pickShipType != 'NONE'){
            $modal.find('#modifyType').val('ALL');//수거방법 변경 추가로 ALL로 변경
        };

        var claimExchShipping = $modal.find('#requestForm').serializeObject();

        var param = {
            "claimBundleNo": $modal.find('#claimBundleNo').val(),
            "claimNo": $modal.find('#claimNo').val(),
            "claimShippingReqType": "E",//반품/교환 요청타입, P:반품, E:교환, null:반품/교환
            "claimExchShippingModifyDto": claimExchShipping,
            "chgId": "PARTNER"
        };

        if (!confirm('배송정보를 변경하시겠습니까?')) {
            return false;
        }

        CommonAjax.basic({
            url         : '/claim/setExchShipping.json',
            data        : JSON.stringify(claimExchShipping),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                if (res.returnCode != "0000") {
                    alert(res.returnMessage);
                    return false;
                } else {
                    alert('배송정보 변경 성공');
                    chgExchShipping.closeModal();
                }
            }
        });
    },

    /**
     * 수거방법 변경 유효성검사
     * **/
    validation : function(){

        var $modal = chgExchShipping.getRoot();

        switch ($modal.find('#selectDeliveryType').val()) {
            case 'C' ://택배배송
                if($.jUtil.isEmpty($modal.find('#exchDlvCd').val())){
                    alert('택배사를 입력 해 주세요');
                    return false;
                }else if($.jUtil.isEmpty($modal.find('#exchInvoiceNo').val())){
                    alert('송장번호를 입력 해 주세요');
                    return false;
                };
                break;
            case 'D' ://직배송
                if($.jUtil.isEmpty($modal.find('#chgExchShipDt').val())){
                    alert('수거예정일을 입력 해 주세요');
                    return false;
                }
                break;
            case 'N' ://배송없음
                break;
            case 'P' ://우편배송
                if($.jUtil.isEmpty($modal.find('#exchMemo').val())){
                    alert('메모를 입력 해 주세요');
                    return false;
                }
                break;
        }

        if($.jUtil.isEmpty($modal.find('#exchReceiverNm').val())){
            alert('받는사람 이름을 입력해 주세요.')
            return false;
        }
        if($modal.find('#exchReceiverNm').val().length < 2) {
            alert('이름을 2자 이상 입력해주세요.');
            return false;
        }

        //연락처
        if ($.jUtil.isEmpty($modal.find('#exchMobileNo').val())) {
            alert('연락처를 입력주세요');
            return false;
        }

        if($modal.find('#exchMobileNo_1').val().length < 2) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($modal.find('#exchMobileNo_2').val().length < 3) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($modal.find('#exchMobileNo_3').val().length < 4) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($.jUtil.isEmpty($modal.find('#exchZipcode').val())){
            alert('우편번호를 입력해 주세요.')
            return false;
        }
        if($.jUtil.isEmpty($modal.find('#exchAddr').val())){
            alert('주소를 입력해 주세요.')
            return false;
        }
        if($.jUtil.isEmpty($modal.find('#exchAddrDetail').val())){
            alert('상세주소를 입력해 주세요.')
            return false;
        }
        return true;

    },

    /**
     * 주소정보 세팅
     * **/
    chgExchShippingSetZipcode : function (res){
        var $modal = chgExchShipping.getRoot();
        $modal.find("#exchZipCode").val(res.zipCode);
        $modal.find("#exchAddr").val(res.roadAddr);
        $modal.find('#exchBaseAddr').val(res.gibunAddr);
    },


    /**
     * modal open
     * **/
    openModal : function () {
        $ui.modalOpen('chgExchShippingPop', chgExchShipping.set());
    },
    /**
     * modal close
     */
    closeModal: function() {
        $('#itemInfoTable > tbody').empty();
        parentMain.callBackSearch();
        $ui.modalClose('chgExchShippingPop');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#chgExchShippingPop');
    }

}

