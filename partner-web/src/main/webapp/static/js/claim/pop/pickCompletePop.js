var pickComplete = {

    /**
     * 초기화
     */
    init : function() {
        pickComplete.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = pickComplete.getRoot();
        $modal.find('#uploadFileName').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl').val(), '첨부파일1.'+$modal.find('#uploadFileNameType').val(), 'ClaimAddImage');
        });

        $modal.find('#uploadFileName2').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl2').val(), '첨부파일2.'+$modal.find('#uploadFileNameType2').val(), 'ClaimAddImage');
        });

        $modal.find('#uploadFileName3').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl3').val(), '첨부파일3.'+$modal.find('#uploadFileNameType3').val(), 'ClaimAddImage');
        });
    },


    /**
     * 팝업창 setting
     * **/
    set : function(){

        var $modal = pickComplete.getRoot();

        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var claimBundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"claimBundleNo");
        var claimTypeCode = parentGrid.dataProvider.getValue(checkRowIds[0],"claimType");
        var buyerNm = parentGrid.dataProvider.getValue(checkRowIds[0],"buyerNm");
        var claimType = claimTypeCode == 'R' ? '반품' : claimTypeCode == 'X'? '교환' : '취소';
        var claimNo = parentGrid.dataProvider.getValue(checkRowIds[0],"claimNo");

        //클레임 상세정보 조회
        CommonAjax.basic({
            url:'/claim/getClaimDetail.json?claimBundleNo=' + claimBundleNo + "&claimType="+ claimTypeCode,
            data: null,
            contentType: 'application/json',
            method: "GET",
            callbackFunc: function(res) {
                $modal.find('#title1').text('> '+ claimType +'상품 (배송번호 : '+res.bundleNo+ "/클레임 번호 : " + res.claimBundleNo + ")");//상품 정보 title
                $modal.find('#title2').text('> '+ claimType + '신청 정보');//신청정보 title
                $modal.find('#claimReasonTitle').text(claimType + '사유');//사우 title
                $modal.find('#claimPickShippingTitle').text(claimType + '수거지');//수거지 title

                $modal.find('#buyerNm').text(buyerNm);
                $modal.find('#claimReasonType').text("["+res.claimReasonType+"]");
                $modal.find('#claimReasonDetail').text(res.claimReasonDetail);
                $modal.find('#claimPickShippingNo').val(res.claimPickShippingDto.claimPickShippingNo);
                $modal.find('#claimBundleNo').val(res.claimBundleNo);
                $modal.find('#claimNo').val(claimNo);

                //수거지 정보 설정
                $.each(res.claimPickShippingDto,function(key, value){
                    $modal.find('#'+key).text(value);
                });

                //첨부파일 설정
                claimMainCommon.setUploadFile($modal, res);

                //주소 설정
                $modal.find('#pickAddr').text(res.claimPickShippingDto.pickAddr + " " + res.claimPickShippingDto.pickAddrDetail);
                $modal.find('#pickShipType').text(res.claimPickShippingDto.pickShipType);

                var pickDlv = $.jUtil.isEmpty(res.claimPickShippingDto.pickDlv) === true ? '' : res.claimPickShippingDto.pickDlv;
                var pickRegisterType = $.jUtil.isEmpty(res.claimPickShippingDto.pickRegisterType) === true ? '' : res.claimPickShippingDto.pickRegisterType;
                var pickInvoiceNo = $.jUtil.isEmpty(res.claimPickShippingDto.pickInvoiceNo) === true ? '' : res.claimPickShippingDto.pickInvoiceNo;
                var shipDt = $.jUtil.isEmpty(res.claimPickShippingDto.shipDt) === true ? '' : res.claimPickShippingDto.shipDt;

                //수거 방법 설정
                switch (res.claimPickShippingDto.pickShipTypeCode) {
                    case 'C' ://택배배송
                        //택배사명 + 수거접수타입 + 송장번호
                        $modal.find('#pickShipTypeText').text(" | " + pickDlv + " | " + pickRegisterType + " | " + pickInvoiceNo);
                        $modal.find("#shipSearchBtn").show();    //배송조회 버튼
                        break;
                    case 'D' ://직배송
                        $modal.find('#pickShipTypeText').text("| 수거 예정일 : " + shipDt);
                        break;
                    case 'N' ://배송없음
                        //현재는 빈값으로 노출
                        break;
                    case 'P' ://우편배송
                        if(!$.jUtil.isEmpty(res.claimPickShippingDto.pickMemo)){
                            $modal.find('#pickShipTypeText').text("| 메모 : " + res.claimPickShippingDto.pickMemo);
                        }
                        break;
                }

                //상품 리스트 설정
                for(var i=0; i<res.claimDetailItemList.length; i++){
                    var tableRow = $('<tr>');
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].itemName));
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].optItemName));
                    tableRow.append($('<td style="text-align: center; ">').text(res.claimDetailItemList[i].claimItemQty));
                    $modal.find('#itemInfoTable > tbody:last').append(tableRow);
                }
            }
        });
    },

    /**
     * 수거 요청
     * **/
    save : function(){
        var $modal = pickComplete.getRoot();

        if (!confirm('수거완료 상태로 변경하시겠습니까?')) {
            return false;
        }

        CommonAjax.basic({
            url         : '/claim/setPickComplete.json',
            data        : JSON.stringify($modal.find('#requestForm').serializeObject()),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                if (res.returnCode != "0000") {
                    alert(res.returnMessage);
                    pickComplete.closeModal();
                } else {
                    alert('수거 완료 성공');
                    pickComplete.closeModal();
                }
            }
        });
    },

    /**
     * modal open
     * **/
    openModal : function () {
        $ui.modalOpen('pickCompletePop', pickComplete.set());
    },
    /**
     * modal close
     */
    closeModal: function() {
        $('#itemInfoTable > tbody').empty();
        parentMain.callBackSearch();
        $ui.modalClose('pickCompletePop');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#pickCompletePop');
    }

}

