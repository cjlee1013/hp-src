$(document).ready(function() {
  cancelReject.init();
});


var cancelReject = {

    init : function() {
        this.initSearchDate();
        this.bindingEvent();
        claimMainCommon.chgShipMethodType(cancelReject.getRoot(),'DS_DLV');

      },
    initSearchDate : function () {
        Calendar.datePicker("pickRequestDt");
        Calendar.setCalDate("pickRequestDt", 0);
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = cancelReject.getRoot();
        // [수거/배송방법] 선택
        $modal.find('#selectDeliveryType').bindChange(function() { claimMainCommon.chgShipMethodType($modal, null)});

        //송장번호 - 숫자만 입력
        $("#invoice").allowInput("keyup", ["NUM","NOT_SPACE"], $(this).attr("id"));
    },

    /**
     * 팝업창 setting
     * **/
    set : function(){
        var $modal = cancelReject.getRoot();

        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var claimBundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"claimBundleNo");

        //클레임 상세정보 조회
        CommonAjax.basic({
            url:'/claim/getClaimDetail.json?claimBundleNo=' + claimBundleNo + "&claimType=C",
            data: null,
            contentType: 'application/json',
            method: "GET",
            callbackFunc: function(res) {
                $('#cancelRejectTitle').text('> 취소 상품 (배송번호 : '+res.bundleNo+ "/클레임 번호 : " + res.claimBundleNo + ")");


                for(var i=0; i<res.claimDetailItemList.length; i++){
                    var tableRow = $('<tr>');
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].itemName));
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].optItemName));
                    tableRow.append($('<td style="text-align: center; ">').text(res.claimDetailItemList[i].claimItemQty));
                    tableRow.append($('<td hidden>').text(res.claimDetailItemList[i].orderItemNo));
                    $modal.find('#itemInfoTable > tbody:last').append(tableRow);
                }
            }
        });
    },
    /**
     * 취소거부 배소설정 유효성 검사
     * **/
    validation : function(){

        var $modal = cancelReject.getRoot();

        switch ($modal.find('#selectDeliveryType').val()) {
            case 'DS_DLV' ://택배배송
                if($.jUtil.isEmpty($modal.find('#selectDlvCd').val())){
                    alert('택배사를 선택해주세요');
                    return false;
                }
                if($.jUtil.isEmpty($modal.find('#invoice').val())){
                    alert('송장번호를 입력해주세요.');
                    return false;
                }
                break;
            case 'DS_DRCT' ://직배송
                if($.jUtil.isEmpty($modal.find('#pickRequestDt').val())) {
                    alert('배송예정일을 입력해주세요.');
                    return false;
                }
                break;
        }
        return true;

    },

    /**
     * 클레임 거부 요청
     * **/
    save : function(){


        var $modal = cancelReject.getRoot();

        var checkRowIds = parentGrid.gridView.getCheckedRows();

        //유효성 검사
        if(!cancelReject.validation()){
            return false;
        }

        var orderShipManageShipAllSetDto = {
            "bundleNo" : parentGrid.dataProvider.getValue(checkRowIds[0],"bundleNo"),
            "shipMethod" : $modal.find('#selectDeliveryType').val(),
            "dlvCd" : $modal.find('#selectDlvCd').val(),
            "invoiceNo" : $modal.find('#invoice').val(),
            "scheduleShipDt" : $modal.find('#pickRequestDt').val()
        }

        var param = {
            "claimNo" : parentGrid.dataProvider.getValue(checkRowIds[0],"claimNo"),
            "claimBundleNo" : parentGrid.dataProvider.getValue(checkRowIds[0],"claimBundleNo"),
            "regId" : "PARTNER",
            "reasonType" : "D001",
            "claimType" : "C",
            "orderShipManageShipAllSetDto" : orderShipManageShipAllSetDto
        }

        if (!confirm('배송설정 정보를 등록하시겠습니까?')) {
            return false;
        }

        CommonAjax.basic({
                url         : '/claim/setClaimReject.json',
                data        : JSON.stringify(param),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "0000") {
                        alert(res.returnMessage);
                        cancelReject.closeModal();
                    } else {
                        alert('취소거부 처리 성공');
                        cancelReject.closeModal();

                    }
                }
            }
            );
    },

    /**
     * modal open
     * **/
    openModal : function () {
        $ui.modalOpen('cancelRejectPop', cancelReject.set());
    },
    /**
     * modal close
     */
    closeModal: function() {
        $('#itemInfoTable > tbody').empty();
        $('#selectDeliveryType').val('DS_DLV');
        cancelReject.init();
        $('#selectDlvCd,#invoice,#postDiv').val('');
        parentMain.callBackSearch();
        $ui.modalClose('cancelRejectPop');
    },
    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#cancelRejectPop');
    }

}
