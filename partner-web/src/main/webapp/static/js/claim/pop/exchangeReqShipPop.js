$(document).ready(function() {
  exchangeRegShip.init();
});

var exchangeRegShip = {

    init : function() {
        this.bindingEvent();
        this.initSearchDate();
    },

    initSearchDate : function () {
        Calendar.datePicker("exchReqShipDt");
        Calendar.setCalDate("exchReqShipDt", 0);
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = exchangeRegShip.getRoot();
        // [수거/배송방법] 선택
        $modal.find('#selectDeliveryType').bindChange(function() { claimMainCommon.chgDeliveryType($modal, null)});

        $modal.find('#uploadFileName').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl').val(), '첨부파일1.'+$modal.find('#uploadFileNameType').val(), 'ClaimAddImage');
        });

        $modal.find('#uploadFileName2').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl2').val(), '첨부파일2.'+$modal.find('#uploadFileNameType2').val(), 'ClaimAddImage');
        });

        $modal.find('#uploadFileName3').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl3').val(), '첨부파일3.'+$modal.find('#uploadFileNameType3').val(), 'ClaimAddImage');
        });
    },

    /**
     * 팝업창 setting
     * **/
    set : function(){

        var $modal = exchangeRegShip.getRoot();

        var checkRowIds = parentGrid.gridView.getSelectedRows();
        var claimBundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"claimBundleNo");
        var claimTypeCode = parentGrid.dataProvider.getValue(checkRowIds[0],"claimType");
        var buyerNm = parentGrid.dataProvider.getValue(checkRowIds[0],"buyerNm");
        var claimType = claimTypeCode == 'R' ? '반품' : claimTypeCode == 'X'? '교환' : '취소';
        var claimNo = parentGrid.dataProvider.getValue(checkRowIds[0],"claimNo");

        //클레임 상세정보 조회
        CommonAjax.basic({
            url:'/claim/getClaimDetail.json?claimBundleNo=' + claimBundleNo + "&claimType="+ claimTypeCode,
            data: null,
            contentType: 'application/json',
            method: "GET",
            callbackFunc: function(res) {
                $modal.find('#title1').text('> '+ claimType +'상품 (배송번호 : '+res.bundleNo+ "/클레임 번호 : " + res.claimBundleNo + ")");//상품 정보 title
                $modal.find('#title2').text('> '+ claimType + '신청 정보');//신청정보 title
                $modal.find('#buyerNm').text(buyerNm);
                $modal.find('#claimReasonType').text("["+res.claimReasonType+"]");
                $modal.find('#claimReasonDetail').text(res.claimReasonDetail);

                $modal.find('#exchAddr').text(res.claimExchShippingDto.exchAddr + " " + res.claimExchShippingDto.exchAddrDetail);
                $modal.find('#claimBundleNo').val(res.claimBundleNo);
                $modal.find('#claimNo').val(claimNo);
                $modal.find('#claimExchShippingNo').val(res.claimExchShippingDto.claimExchShippingNo);


                $.each(res.claimExchShippingDto,function(key, value){
                    $modal.find('#'+key).val(value);
                });

                //첨부파일 설정
                claimMainCommon.setUploadFile($modal, res);

                //교환 배송비 설정
                claimMainCommon.setReturnShipAmt($modal , res);

                //수거 방법 초기화
                claimMainCommon.chgDeliveryType($modal, res.claimExchShippingDto.exchShipTypeCode);

                //상품 리스트 설정
                for(var i=0; i<res.claimDetailItemList.length; i++){
                    var tableRow = $('<tr>');
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].itemName));
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].optItemName));
                    tableRow.append($('<td style="text-align: center; ">').text(res.claimDetailItemList[i].claimItemQty));
                    $modal.find('#itemInfoTable > tbody:last').append(tableRow);
                }
            }
        });
    },



    /**
     * 교환배송 요청
     * **/
    save : function(){
        var $modal = exchangeRegShip.getRoot();

        //유효성 검사
        if(!exchangeRegShip.validation()){
            return false;
        }

        var claimExchShipping = $modal.find('#requestForm').serializeObject();


        if (!confirm('교환배송정보를 등록하시겠습니까?')) {
            return false;
        }

        CommonAjax.basic({
            url         : '/claim/setClaimDelivery.json',
            data        : JSON.stringify(claimExchShipping),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                if (res.returnCode != "0000") {
                    alert(res.returnMessage);
                    return false;
                } else {
                    alert('교환배송정보 변경 성공');
                    exchangeRegShip.closeModal();
                }
            }
        });
    },

        /**
     * 수거방법 변경 유효성검사
     * **/
    validation : function(){

        var $modal = exchangeRegShip.getRoot();

        switch ($modal.find('#selectDeliveryType').val()) {
            case 'C' ://택배배송
                if($.jUtil.isEmpty($modal.find('#exchDlvCd').val())){
                    alert('택배사를 입력 해 주세요');
                    return false;
                }else if($.jUtil.isEmpty($modal.find('#exchInvoiceNo').val())){
                    alert('송장번호를 입력 해 주세요');
                    return false;
                };
                break;
            case 'D' ://직배송
                if($.jUtil.isEmpty($modal.find('#exchReqShipDt').val())){
                    alert('수거예정일을 입력 해 주세요');
                    return false;
                }
                break;
            case 'N' ://배송없음
                break;
            case 'P' ://우편배송
                if($.jUtil.isEmpty($modal.find('#exchMemo').val())){
                    alert('메모를 입력 해 주세요');
                    return false;
                }
                break;
        }

        return true;

    },

    /**
     * modal open
     * **/
    openModal : function () {
        $ui.modalOpen('exchangeRegShipPop', exchangeRegShip.set());
    },
    /**
     * modal close
     */
    closeModal: function() {
        $('#itemInfoTable > tbody').empty();
        parentMain.callBackSearch();
        $ui.modalClose('exchangeRegShipPop');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#exchangeRegShipPop');
    }

}

