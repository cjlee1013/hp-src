
$(document).ready(function() {
  chgPickShipping.init();
});

var chgPickShipping = {

    init : function() {
        this.bindingEvent();
        this.initSearchDate();
    },

    initSearchDate : function () {
        Calendar.datePicker("chgPickShippingDt");
        Calendar.setCalDate("chgPickShippingDt", 0);
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = chgPickShipping.getRoot();
        // [수거/배송방법] 선택
        $modal.find('#selectDeliveryType').bindChange(function() { claimMainCommon.chgDeliveryType($modal, null)});

        //연락처 - 숫자만 입력
        $("#pickMobileNo_1,#pickMobileNo_2,pickMobileNo_3").allowInput("keyup", ["NUM","NOT_SPACE"], $(this).attr("id"));

        //송장번호 - 숫자만 입력
        $("#pickInvoiceNo").allowInput("keyup", ["NUM","NOT_SPACE"], $(this).attr("id"));
    },

    /**
     * 팝업창 setting
     * **/
    set : function(){
        var $modal = chgPickShipping.getRoot();

        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var claimBundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"claimBundleNo");
        var claimTypeCode = parentGrid.dataProvider.getValue(checkRowIds[0],"claimType");
        var claimType = claimTypeCode == 'R' ? '반품' : claimTypeCode == 'X'? '교환' : '취소';
        var claimNo = parentGrid.dataProvider.getValue(checkRowIds[0],"claimNo");

        //클레임 상세정보 조회
        CommonAjax.basic({
            url:'/claim/getClaimDetail.json?claimBundleNo=' + claimBundleNo + "&claimType="+ claimTypeCode,
            data: null,
            contentType: 'application/json',
            method: "GET",
            callbackFunc: function(res) {
                $modal.find('#title1').text('> '+ claimType +'상품 (배송번호 : '+res.bundleNo+ "/클레임 번호 : " + res.claimBundleNo + ")");//상품 정보 title
                $modal.find('#title2').text('> '+ claimType + '신청 정보');//신청정보 title
                $modal.find('#pickAddrTitle').text(claimType + '수거지');//신청정보 title

                //수거지 정보 설정
                $.each(res.claimPickShippingDto,function(key, value){
                    $modal.find('#'+key).val(value);
                });

                $modal.find('#claimBundleNo').val(res.claimBundleNo);
                $modal.find('#claimNo').val(claimNo);
                $.jUtil.valSplit($modal.find("input[name$='pickMobileNo']" ).val(), '-', 'pickMobileNo');

                //초기 배송타입
                //배송방법 변경 여부 판단을 위해 적용
                $modal.find("#initDeliveryType").val(res.claimPickShippingDto.pickShipTypeCode);

                //첨부파일 설정
                claimMainCommon.setUploadFile($modal, res);

                //수거방법명 설정
                $modal.find('#pickShipType').text(res.claimPickShippingDto.pickShipType);

                var pickDlv = $.jUtil.isEmpty(res.claimPickShippingDto.pickDlv) === true ? '' : res.claimPickShippingDto.pickDlv;
                var pickRegisterType = $.jUtil.isEmpty(res.claimPickShippingDto.pickRegisterType) === true ? '' : res.claimPickShippingDto.pickRegisterType;
                var pickInvoiceNo = $.jUtil.isEmpty(res.claimPickShippingDto.pickInvoiceNo) === true ? '' : res.claimPickShippingDto.pickInvoiceNo;
                var shipDt = $.jUtil.isEmpty(res.claimPickShippingDto.shipDt) === true ? '' : res.claimPickShippingDto.shipDt;

                //수거 방법 text 설정
                switch (res.claimPickShippingDto.pickShipTypeCode) {
                    case 'C' ://택배배송
                        //택배사명 + 수거접수타입 + 송장번호
                        $modal.find('#pickShipTypeText').text(" | " + pickDlv + " | " + pickRegisterType + " | " + pickInvoiceNo);
                        $modal.find("#shipSearchBtn").show();    //배송조회 버튼
                        break;
                    case 'D' ://직배송
                        $modal.find('#pickShipTypeText').text(" | 수거예정일 : " + shipDt);
                        break;
                    case 'N' ://배송없음
                        //현재는 빈값으로 노출
                        break;
                    case 'P' ://우편배송
                        if($.jUtil.isEmpty(res.claimPickShippingDto.pickMemo)){
                            $modal.find('#pickShipTypeText').text("| 메모 : " + res.claimPickShippingDto.pickMemo);
                        }
                        break;
                }

                //상품 리스트 설정
                for(var i=0; i<res.claimDetailItemList.length; i++){
                    var tableRow = $('<tr>');
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].itemName));
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].optItemName));
                    tableRow.append($('<td style="text-align: center; ">').text(res.claimDetailItemList[i].claimItemQty));
                    $modal.find('#itemInfoTable > tbody:last').append(tableRow);
                }
            }
        });
    },

    /**
     * 수거 요청
     * **/
    save : function(){
        var $modal = chgPickShipping.getRoot();

        $modal.find('#pickMobileNo').val($.jUtil.valNotSplit('pickMobileNo', '-'));
        $modal.find('#pickInvoiceNo').val().replace(' ','');

        if(!chgPickShipping.validation()){
            return false;
        }

        if (!confirm('저장하시겠습니까?')) {
            return false;
        }

        var claimPickShipping = $modal.find('#requestForm').serializeObject();

        CommonAjax.basic({
                url         : '/claim/setPickShipping.json',
                data        : JSON.stringify(claimPickShipping),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "0000") {
                        alert(res.returnMessage);
                        chgPickShipping.closeModal();
                    } else {
                        alert('수거 방법 변경 성공');
                        chgPickShipping.closeModal();
                    }
                }
            });
    },

    /**
     * 수거방법 변경 유효성검사
     * **/
    validation : function(){

        var $modal = chgPickShipping.getRoot();

        switch ($modal.find('#selectDeliveryType').val()) {
            case 'C' ://택배배송
                if($.jUtil.isEmpty($modal.find('#pickDlvCd').val())){
                    alert('택배사를 입력 해 주세요');
                    return false;
                }else if($.jUtil.isEmpty($modal.find('#pickInvoiceNo').val())){
                    alert('송장번호를 입력 해 주세요');
                    return false;
                };
                break;
            case 'D' ://직배송
                if($.jUtil.isEmpty($modal.find('#chgPickShippingDt').val())){
                    alert('수거예정일을 입력 해 주세요');
                    return false;
                }
                break;
            case 'N' ://배송없음
                break;
            case 'P' ://우편배송
                if($.jUtil.isEmpty($modal.find('#pickMemo').val())){
                    alert('메모를 입력 해 주세요');
                    return false;
                }
                break;
        }

        if($.jUtil.isEmpty($modal.find('#pickReceiverNm').val())){
            alert('받는사람 이름을 입력해 주세요.')
            return false;
        }
        if($modal.find('#pickReceiverNm').val().length < 2) {
            alert('이름을 2자 이상 입력해주세요.');
            return false;
        }

        //연락처
        if ($.jUtil.isEmpty($modal.find('#pickMobileNo').val())) {
            alert('연락처를 입력주세요');
            return false;
        }

        if($modal.find('#pickMobileNo_1').val().length < 2) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($modal.find('#pickMobileNo_2').val().length < 3) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($modal.find('#pickMobileNo_3').val().length < 4) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($.jUtil.isEmpty($modal.find('#pickZipcode').val())){
            alert('우편번호를 입력해 주세요.')
            return false;
        }
        if($.jUtil.isEmpty($modal.find('#pickAddr').val())){
            alert('주소를 입력해 주세요.')
            return false;
        }
        if($.jUtil.isEmpty($modal.find('#pickAddrDetail').val())){
            alert('상세주소를 입력해 주세요.')
            return false;
        }
        return true;

    },

    /**
     * 주소정보 세팅
     * **/
    chgPickShippingSetZipcode : function (res){
        var $modal = chgPickShipping.getRoot();
        $modal.find("#pickZipCode").val(res.zipCode);
        $modal.find("#pickAddr").val(res.roadAddr);
        $modal.find('#pickBaseAddr').val(res.gibunAddr);
    },

    /**
     * form reset
     */
    formReset: function() {
        var $modal = chgPickShipping.getRoot();

        $modal.find('#title1').text('');//상품 정보 title
        $modal.find('#title2').text('');//신청정보 title
        $modal.find('#pickAddrTitle').text('');//신청정보 title
        $modal.find('#pickReceiverNm').val('');
        $modal.find('#pickMobileNo').val('');
        $modal.find('#pickZipcode').val('');
        $modal.find('#pickAddr').val('');
        $modal.find('#pickAddr').val('');
        $modal.find('#chgPickShippingDt').val('');
        $modal.find('#pickAddrDetail').val('');
        $modal.find('#claimPickShippingNo').val('');
        $modal.find("#initDeliveryType").val('');
        $modal.find('#pickShipType').text('');
        $modal.find('#pickShipTypeText').text('');
        $modal.find('#shipSearchBtn').hide();

        chgPickShipping.initSearchDate();
        claimMainCommon.chgDeliveryType($modal, 'NONE')
    },

    /**
     * modal open
     * **/
    openModal : function () {
        $ui.modalOpen('chgPickShippingPop', chgPickShipping.set());
    },
    /**
     * modal close
     */
    closeModal: function() {
        $('#itemInfoTable > tbody').empty();
        chgPickShipping.formReset();
        parentMain.callBackSearch();
        $ui.modalClose('chgPickShippingPop');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#chgPickShippingPop');
    }

}

