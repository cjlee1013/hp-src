
var exchangeComplete = {

    init : function() {
        exchangeComplete.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = exchangeComplete.getRoot();

        $modal.find('#uploadFileName').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl').val(), '첨부파일1.'+$modal.find('#uploadFileNameType').val(), 'ClaimAddImage');
        });

        $modal.find('#uploadFileName2').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl2').val(), '첨부파일2.'+$modal.find('#uploadFileNameType2').val(), 'ClaimAddImage');
        });

        $modal.find('#uploadFileName3').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl3').val(), '첨부파일3.'+$modal.find('#uploadFileNameType3').val(), 'ClaimAddImage');
        });
    },

    /**
     * 팝업창 setting
     * **/
    set : function(){

        var $modal = exchangeComplete.getRoot();

        var checkRowIds = parentGrid.gridView.getSelectedRows();
        var claimBundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"claimBundleNo");
        var claimNo = parentGrid.dataProvider.getValue(checkRowIds[0],"claimNo");
        var claimTypeCode = parentGrid.dataProvider.getValue(checkRowIds[0],"claimType");
        var buyerNm = parentGrid.dataProvider.getValue(checkRowIds[0],"buyerNm");
        var claimType = claimTypeCode == 'R' ? '반품' : claimTypeCode == 'X'? '교환' : '취소';

        //클레임 상세정보 조회
        CommonAjax.basic({
            url:'/claim/getClaimDetail.json?claimBundleNo=' + claimBundleNo + "&claimType="+ claimTypeCode,
            data: null,
            contentType: 'application/json',
            method: "GET",
            callbackFunc: function(res) {
                $modal.find('#title1').text('> '+ claimType +'상품 (배송번호 : '+res.bundleNo+ "/클레임 번호 : " + res.claimBundleNo + ")");//상품 정보 title
                $modal.find('#title2').text('> '+ claimType + '신청 정보');//신청정보 title
                $modal.find('#buyerNm').text(buyerNm);
                $modal.find('#claimReasonType').text("["+res.claimReasonType+"]");
                $modal.find('#claimReasonDetail').text(res.claimReasonDetail);

                $modal.find('#claimExchShippingNo').val(res.claimExchShippingDto.claimExchShippingNo);
                $modal.find('#claimBundleNo').val(res.claimBundleNo);
                $modal.find('#claimNo').val(claimNo);

                //첨부파일 설정
                claimMainCommon.setUploadFile($modal, res);

                //교환 배송비 설정
                claimMainCommon.setReturnShipAmt($modal , res);

                $modal.find('#pickStatus').text(res.claimPickShippingDto.pickStatus);
                var pickDlv = $.jUtil.isEmpty(res.claimPickShippingDto.pickDlv) === true ? '' : res.claimPickShippingDto.pickDlv;
                var pickInvoiceNo = $.jUtil.isEmpty(res.claimPickShippingDto.pickInvoiceNo) === true ? '' : res.claimPickShippingDto.pickInvoiceNo;
                var shipDt = $.jUtil.isEmpty(res.claimPickShippingDto.shipDt) === true ? '' : res.claimPickShippingDto.shipDt;

                //수거 방법 설정
                switch (res.claimPickShippingDto.pickShipTypeCode) {
                    case 'C' ://택배배송
                        //택배사명 + 수거접수타입 + 송장번호
                        $modal.find('#pickShipTypeText').text("택배배송 | " + pickDlv + " / " + pickInvoiceNo);
                        $modal.find("#shipSearchBtn").show();    //배송조회 버튼
                        break;
                    case 'D' ://직배송
                        $modal.find('#pickShipTypeText').text("업체직배송 | 수거 예정일 : " + shipDt);
                        break;
                    case 'N' ://배송없음
                        //현재는 빈값으로 노출
                        break;
                    case 'P' ://우편배송
                        if(!$.jUtil.isEmpty(res.claimPickShippingDto.pickMemo)){
                            $modal.find('#pickShipTypeText').text("우편배송 | 메모 : " + res.claimPickShippingDto.pickMemo);
                        }
                        break;
                }


                $modal.find('#exchStatus').text(res.claimExchShippingDto.exchStatus);
                var exchDlv = $.jUtil.isEmpty(res.claimExchShippingDto.exchDlv) === true ? '' : res.claimExchShippingDto.exchDlv;
                var exchInvoiceNo = $.jUtil.isEmpty(res.claimExchShippingDto.exchInvoiceNo) === true ? '' : res.claimExchShippingDto.exchInvoiceNo;
                var exchD0Dt = $.jUtil.isEmpty(res.claimExchShippingDto.exchD0Dt) === true ? '' : res.claimExchShippingDto.exchD0Dt;

                //수거 방법 설정
                switch (res.claimExchShippingDto.exchShipTypeCode) {
                    case 'C' ://택배배송
                        //택배사명 + 수거접수타입 + 송장번호
                        $modal.find('#exchShipTypeText').text("택배배송 | " + exchDlv + " / " +  exchInvoiceNo);
                        $modal.find("#shipSearchBtn,#exchShipSearchBtn").show();    //배송조회 버튼
                        break;
                    case 'D' ://직배송
                        $modal.find('#exchShipTypeText').text("업체직배송 | 수거 예정일 : " + exchD0Dt);
                        break;
                    case 'N' ://배송없음
                        //현재는 빈값으로 노출
                        break;
                    case 'P' ://우편배송
                        if(!$.jUtil.isEmpty(res.claimExchShippingDto.exchMemo)){
                            $modal.find('#exchShipTypeText').text("우편배송 | 메모 : " + res.claimExchShippingDto.exchMemo);
                        }
                        break;
                }

                //상품 리스트 설정
                for(var i=0; i<res.claimDetailItemList.length; i++){
                    var tableRow = $('<tr>');
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].itemName));
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].optItemName));
                    tableRow.append($('<td style="text-align: center; ">').text(res.claimDetailItemList[i].claimItemQty));
                    $modal.find('#itemInfoTable > tbody:last').append(tableRow);
                }
            }
        });
    },



    /**
     * 수거 요청
     * **/
    save : function(){
        var $modal = exchangeComplete.getRoot();
        var param = $modal.find('#requestForm').serializeObject()

        if (!confirm('교환완료상태로 변경하시겠습니까?')) {
            return false;
        }

        CommonAjax.basic({
                url         : '/claim/setExchComplete.json',
                data        : JSON.stringify(param),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "0000") {
                        alert('교환완료 상태 변경 실패');
                        exchangeComplete.closeModal();
                    } else {
                        alert('교환완료 성공');
                        exchangeComplete.closeModal();
                    }
                }
            }
        );
    },

    getExchShippingParam : function(){
        var $modal = exchangeComplete.getRoot();

        var claimExchShipping = {
            "claimBundleNo" : $modal.find('#claimBundleNo').val(),
            "modifyType" : "STATUS",
            "exchShipType" : "",
            "exchStatus" : "D3"
        };

        var param = {
            "claimBundleNo": $modal.find('#claimBundleNo').val(),
            "claimNo": $modal.find('#claimNo').val(),
            "claimShippingReqType": "E",//반품/교환 요청타입, P:반품, E:교환, null:반품/교환
            "claimExchShippingModifyDto": claimExchShipping,
            "chgId": "PARTNER"
        };
        return param;
    },

    /**
     * modal open
     * **/
    openModal : function () {
        $ui.modalOpen('exchangeCompletePop', exchangeComplete.set());
    },
    /**
     * modal close
     */
    closeModal: function() {
        $('#itemInfoTable > tbody').empty();
        parentMain.callBackSearch();
        $ui.modalClose('exchangeCompletePop');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#exchangeCompletePop');
    }

}

