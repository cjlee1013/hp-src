/** Main Script */
var claimShipHistoryPop = {
    /**
     * init 이벤트
     */
    init: function() {
        claimShipHistoryPop.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = claimShipHistoryPop.getRoot();
    },

    /**
     * 데이터 검색
     */
    search: function(invoiceNo, dlvCd, dlvCdNm) {
        var $modal = claimShipHistoryPop.getRoot();

        var dlvCd = dlvCd;
        var dlvCdNm = dlvCdNm;
        var invoiceNo = invoiceNo;

        var params = new Object();

        params.dlvCd = dlvCd;
        params.invoiceNo = invoiceNo;

        CommonAjax.basic({
            url:'/sell/shipStatus/getShipHistory.json?' + jQuery.param(params),
            data: null,
            contentType: 'application/json',
            method: "GET",
            callbackFunc: function(res) {
                var shipHistoryList = res;

                if (shipHistoryList.length > 0) {
                    $modal.find('#invoiceInfo').text('송장번호 : ' + shipHistoryList[0].dlvNm + ' ' + shipHistoryList[0].invoiceNo);

                    for (var shipHistory of shipHistoryList) {
                        claimShipHistoryPop.historyTemplate(shipHistory);
                    }
                } else {
                    $modal.find('#invoiceInfo').text('송장번호 : ' + dlvCdNm+ ' ' + invoiceNo);
                    $modal.find('#ulList').html('<span style="padding-left: 110px">조회할 수 없습니다.</span>');
                }

                $ui.modalOpen('claimShipHistoryPop');
            }
        });
    },

    /**
     * 배송 히스토리 템플릿
     */
    historyTemplate: function(data) {
        var $modal = claimShipHistoryPop.getRoot();
        var innerHtml = [];

        // 택배위치
        if (!$.jUtil.isEmpty(data.shipWhere)) {
            data.shipWhere = '<span>(' +data.shipWhere+ ')</span>';
        }

        // 배송예정시간
        if (!$.jUtil.isEmpty(data.shipEstmateDt)) {
            data.shipEstmateDt = '<div class="info">' +
                                    '<p>배달예정시간 : ' + data.shipEstmateDt + '</p>' +
                                 '</div>';
        }

        // 배송기사
        if (!$.jUtil.isEmpty(data.shipMan)) {
            data.shipMan = '<div class="align-r">' +
                               '<p>' + data.shipMan + '</p>' +
                               '<p>' + $.jUtil.phoneFormatter(data.shipManPhone) + '</p>' +
                           '</div>';
        }

        innerHtml.push('<li>');
        innerHtml.push('    <div>');
        innerHtml.push('        <p class="date">' + data.shipTranDt + '</p>');
        innerHtml.push('        <p class="state">' + data.shipDetail + data.shipWhere + '</p>');
        innerHtml.push(         data.shipEstmateDt);
        innerHtml.push('    </div>');
        innerHtml.push(     data.shipMan);
        innerHtml.push('</li>');

        $modal.find('#ulList').append(innerHtml.join(''));
    },

    /**
     * modal open
     */
    openModal: function(invoiceNo, dlvCd, dlvCdNm) {
        claimShipHistoryPop.search(invoiceNo, dlvCd, dlvCdNm);
    },

    /**
     * modal close
     */
    closeModal: function() {
        var $modal = claimShipHistoryPop.getRoot();

        $ui.modalClose('claimShipHistoryPop');
        $modal.find('#ulList').html('');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#claimShipHistoryPop');
    }
};