
var cancelApprove = {

    /**
     * 클레임 승인 요청
     * **/
    save : function(){

        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var claimRequestSetList = [];

        for(var i=0; i<checkRowIds.length; i++){
            var checkedRows = parentGrid.dataProvider.getJsonRow(checkRowIds[i]);
            var cancelApproveParam = {
                "claimNo" : checkedRows.claimNo,
                "claimBundleNo" : checkedRows.claimBundleNo,
                "claimReqNo" : checkedRows.claimReqNo,
                "claimReqType" : "CA",
                "claimType" : "C",
                "regId" : "PARTNER"
            }
            claimRequestSetList.push(cancelApproveParam);
        }
        var param = {
            "claimRequestSetList" : claimRequestSetList
        }
        if (!confirm('승인 상태로 변경하시겠습니까?')) {
            return false;
        }
        CommonAjax.basic({
                url         : '/claim/requestClaimApprove.json',
                data        : JSON.stringify(param),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "0000") {
                        alert(res.returnMessage);
                        cancelApprove.closeModal();
                    } else {
                        cancelApprove.resultTemplate(claimRequestSetList.length, res.data);
                        cancelApprove.closeModal();
                        cancelApprove.openResultModal();
                    }
                }
            });

    },

    /**
     * 저장 후 팝업 템플릿
     */
    resultTemplate: function(totalCnt, successCnt) {
        var failCnt = parseInt(totalCnt) - parseInt(successCnt);

        $('#totalCnt').text(totalCnt);
        $('#successCnt').text(successCnt);
        $('#failCnt').text(failCnt);
    },

    /**
     * 승인 결과 modal open
     */
    openResultModal: function() {
        $ui.modalOpen('cancelApproveResultPop');
    },

    /**
     * modal open
     * **/
    openModal : function () {
        $ui.modalOpen('cancelApprovePop');
    },
    /**
     * modal close
     */
    closeModal: function() {
        parentMain.callBackSearch();
        $ui.modalClose('cancelApprovePop');
    },

    /**
     * result modal close
     */
    closeResultModal: function() {
        $ui.modalClose('cancelApproveResultPop');
    },


}
