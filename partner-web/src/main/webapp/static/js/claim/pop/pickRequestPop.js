$(document).ready(function() {
  pickRequest.init();
});

var pickRequest = {

    init : function() {
        this.bindingEvent();
        this.initSearchDate();
    },

    initSearchDate : function () {
        Calendar.datePicker("shipDt");
        Calendar.setCalDate("shipDt", 0);
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        var $modal = pickRequest.getRoot();
        // [수거/배송방법] 선택
        $modal.find('#selectDeliveryType').bindChange(function() { claimMainCommon.chgDeliveryType($modal, null)});

        $modal.find('#uploadFileName').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl').val(), '첨부파일1.'+$modal.find('#uploadFileNameType').val(), 'ClaimAddImage');
        });

        $modal.find('#uploadFileName2').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl2').val(), '첨부파일2.'+$modal.find('#uploadFileNameType2').val(), 'ClaimAddImage');
        });

        $modal.find('#uploadFileName3').on('click', function() {
            $.jUtil.downloadFile($modal.find('#uploadFileNameUrl3').val(), '첨부파일3.'+$modal.find('#uploadFileNameType3').val(), 'ClaimAddImage');
        });

        //연락처 - 숫자만 입력
        $("#pickRequestMobileNo_1,#pickRequestMobileNo_2,pickRequestMobileNo_3").allowInput("keyup", ["NUM","NOT_SPACE"], $(this).attr("id"));

        //송장번호 - 숫자만 입력
        $("#pickInvoiceNo").allowInput("keyup", ["NUM","NOT_SPACE"], $(this).attr("id"));
    },

    /**
     * 팝업창 setting
     * **/
    set : function(){

        var $modal = pickRequest.getRoot();

        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var claimBundleNo = parentGrid.dataProvider.getValue(checkRowIds[0],"claimBundleNo");
        var claimTypeCode = parentGrid.dataProvider.getValue(checkRowIds[0],"claimType");
        var buyerNm = parentGrid.dataProvider.getValue(checkRowIds[0],"buyerNm");
        var claimType = claimTypeCode == 'R' ? '반품' : claimTypeCode == 'X'? '교환' : '취소';
        var claimNo = parentGrid.dataProvider.getValue(checkRowIds[0],"claimNo");

        //클레임 상세정보 조회
        CommonAjax.basic({
            url:'/claim/getClaimDetail.json?claimBundleNo=' + claimBundleNo + "&claimType="+ claimTypeCode,
            data: null,
            contentType: 'application/json',
            method: "GET",
            callbackFunc: function(res) {

                $modal.find('#title1').text('> '+ claimType +'상품 (배송번호 : '+res.bundleNo+ "/클레임 번호 : " + res.claimBundleNo + ")");//상품 정보 title
                $modal.find('#title2').text('> '+ claimType + '신청 정보');//신청정보 title
                $modal.find('#claimReasonTitle').text(claimType + '사유');//사우 title
                $modal.find('#claimPickShippingTitle').text(claimType + '수거지');//수거지 title
                $modal.find('#buyerNm').text(buyerNm);//구매자
                $modal.find('#claimReasonType').text(res.claimReasonType);//반품사유
                $modal.find('#pickRequestMobileNo').val(res.claimPickShippingDto.pickMobileNo);
                $.jUtil.valSplit($modal.find("input[name$='pickRequestMobileNo']" ).val(), '-', 'pickRequestMobileNo');

                //첨부파일 설정
                claimMainCommon.setUploadFile($modal, res);

                //수거지 정보 설정
                $.each(res.claimPickShippingDto,function(key, value){
                    $modal.find('#'+key).val(value);
                });

                $modal.find('#claimBundleNo').val(res.claimBundleNo);
                $modal.find('#claimNo').val(claimNo);

                //수거 방법 초기화
                claimMainCommon.chgDeliveryType(pickRequest.getRoot(), res.claimPickShippingDto.pickShipTypeCode);

                //조회된 수거방법 값 설정
                switch (res.claimPickShippingDto.pickShipTypeCode) {
                    case 'C' ://택배배송
                        $modal.find('#pickDlvCd').val(res.claimPickShippingDto.pickDlvCd);//택배사
                        $modal.find('#pickInvoiceNo').val(res.claimPickShippingDto.pickInvoiceNo);//송장번호
                        break;
                    case 'D' ://직배송
                        $modal.find('#shipDt').val(res.claimPickShippingDto.shipDt);
                        break;
                    case 'N' ://배송없음
                        //현재는 빈값으로 노출
                        break;
                    case 'P' ://우편배송
                        $modal.find('#pickMemo').val(res.claimPickShippingDto.pickMemo);
                        break;
                }

                //상품 리스트 설정
                for(var i=0; i<res.claimDetailItemList.length; i++){
                    var tableRow = $('<tr>');
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].itemName));
                    tableRow.append($('<td style="text-align: center; border-right: 1px solid #eee;">').text(res.claimDetailItemList[i].optItemName));
                    tableRow.append($('<td style="text-align: center; ">').text(res.claimDetailItemList[i].claimItemQty));
                    $modal.find('#itemInfoTable > tbody:last').append(tableRow);
                }
            }
        });
    },

    /**
     * 우편번호 검색 후 값 세팅
     * **/
    pickRequestSetZipcode : function(res){
        var $modal = pickRequest.getRoot();
        $modal.find("#pickZipcode").val(res.zipCode);
        $modal.find("#pickAddr").val(res.roadAddr);
        $modal.find('#pickBaseAddr').val(res.gibunAddr);
    },

    /**
     * 수거방법 변경 유효성검사
     * **/
    validation : function(){

        var $modal = pickRequest.getRoot();

        switch ($modal.find('#selectDeliveryType').val()) {
            case 'C' ://택배배송
                if($.jUtil.isEmpty($modal.find('#pickDlvCd').val())){
                    alert('택배사를 입력 해 주세요');
                    return false;
                }else if($.jUtil.isEmpty($modal.find('#pickInvoiceNo').val())){
                    alert('송장번호를 입력 해 주세요');
                    return false;
                };
                break;
            case 'D' ://직배송
                if($.jUtil.isEmpty($modal.find('#shipDt').val())){
                    alert('수거예정일을 입력 해 주세요');
                    return false;
                }
                break;
            case 'N' ://배송없음
                break;
            case 'P' ://우편배송
                if($.jUtil.isEmpty($modal.find('#pickMemo').val())){
                    alert('메모를 입력 해 주세요');
                    return false;
                }
                break;
        }

        if($.jUtil.isEmpty($modal.find('#pickReceiverNm').val())){
            alert('받는사람 이름을 입력해 주세요.')
            return false;
        }
        if($modal.find('#pickReceiverNm').val().length < 2) {
            alert('이름을 2자 이상 입력해주세요.');
            return false;
        }

        //연락처
        if ($.jUtil.isEmpty($modal.find('#pickMobileNo').val())) {
            alert('연락처를 입력주세요');
            return false;
        }

        if($modal.find('#pickRequestMobileNo_1').val().length < 2) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($modal.find('#pickRequestMobileNo_2').val().length < 3) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($modal.find('#pickRequestMobileNo_3').val().length < 4) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($.jUtil.isEmpty($modal.find('#pickZipcode').val())){
            alert('우편번호를 입력해 주세요.')
            return false;
        }
        if($.jUtil.isEmpty($modal.find('#pickAddr').val())){
            alert('주소를 입력해 주세요.')
            return false;
        }
        if($.jUtil.isEmpty($modal.find('#pickAddrDetail').val())){
            alert('상세주소를 입력해 주세요.')
            return false;
        }
        return true;

    },

    /**
     * 수거 요청
     * **/
    save : function(){
        var $modal = pickRequest.getRoot();

        $modal.find('#pickRoadDetailAddr').val($modal.find('#pickDetailAddr').val());
        $modal.find('#pickMobileNo').val($.jUtil.valNotSplit('pickRequestMobileNo', '-'));

        if(!pickRequest.validation()){
            return false;
        }
        if(!confirm('수거요청 하시겠습니까?')){
            return false;
        };

        var claimPickShipping = $modal.find('#pickRequestForm').serializeObject();

        CommonAjax.basic({
                url         : '/claim/setPickRequest.json',
                data        : JSON.stringify(claimPickShipping),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "0000") {
                        alert(res.returnMessage);
                        pickRequest.closeModal();
                    } else {
                        alert('수거 요청 성공');
                        pickRequest.closeModal();
                    }
                }
            });
    },

    /**
     * form reset
     */
    formReset: function() {
        var $modal = pickRequest.getRoot();

        $modal.find('#buyerNm').text('');
        $modal.find('#claimReasonType').text('');
        $modal.find('#pickReceiverNm').val('');
        $modal.find('#pickMobileNo').val('');
        $modal.find('#pickZipcode').val('');
        $modal.find('#pickAddr').val('');
        $modal.find('#pickBaseAddr').val('');
        $modal.find('#pickAddrDetail').val('');
        $modal.find('#claimPickShippingNo').val('');

        $modal.find('#selectDlvCd').val('ALL');//택배사
        $modal.find('#invoice').val('');//송장번호
        $modal.find('#pickRequestPopDt').val('');
        $modal.find('#pickMemo').val('');

        pickRequest.initSearchDate();
        claimMainCommon.chgDeliveryType($modal, 'NONE')
    },

    /**
     * modal open
     * **/
    openModal : function () {
        $ui.modalOpen('pickRequestPop', pickRequest.set());
    },
    /**
     * modal close
     */
    closeModal: function() {
        $('#itemInfoTable > tbody').empty();
        pickRequest.formReset();
        parentMain.callBackSearch();
        $ui.modalClose('pickRequestPop');
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#pickRequestPop');
    }

}

