var claimMainCommon = {

    /**
     * 판매상태 별 상품수 요약
     */
    setClaimBoardCnt : function (claimType) {

        CommonAjax.basic({
            url:'/claim/getClaimBoardCount.json?claimType='+claimType,
            data:null,
            method:"GET",
            successMsg:null,
            callbackFunc:function(res) {
                claimMainCommon.writeToSpan("claimCntBoard", res);
            }
        });
    },

    /**
     * 반품신청 우편번호 검색 후 값 세팅
     * **/
    pickRequestSetZipcode : function(res){
        pickRequest.pickRequestSetZipcode(res);
    },

    /**
     * 반품신청 우편번호 검색 후 값 세팅
     * **/
    chgPickShippingSetZipcode : function(res){
        chgPickShipping.chgPickShippingSetZipcode(res);
    },

    /**
     * 배송정보 변경 우편번호 검색 후 값 세팅
     * **/
    chgExchShippingSetZipcode : function(res){
        chgExchShipping.chgExchShippingSetZipcode(res);
    },

    /**
     * [수거/배송방법] 선택 값에 따른 문구 설정
     * reqType C:택배 D:직배송 N:배송없음 P:우편배송
     * **/
    chgDeliveryType : function($modal, pickShipTypeCode){
        var pickShipType = pickShipTypeCode;

        if($.jUtil.isEmpty(pickShipType)){
            pickShipType = $modal.find('#selectDeliveryType').val();
        }

        switch (pickShipType) {
            case 'C' ://택배배송
                $modal.find("#parcelDiv").show();
                $modal.find("#pickDiv,#postDiv").hide();
                $modal.find("#pickDiv").find("img").hide();
                break;
            case 'D' ://직배송
                $modal.find('#pickDiv').show();
                $modal.find('#parcelDiv,#postDiv').hide();
                $modal.find("#pickDiv").find("img").show();
                break;
            case 'N' : case 'NONE' ://배송없음, 변경안함
                $modal.find("#parcelDiv,#postDiv,#pickDiv").hide();
                $modal.find("#pickDiv").find("img").hide();
                break;
            case 'P' ://우편배송
                $modal.find('#postDiv').show();
                $modal.find('#parcelDiv,#pickDiv').hide();
                $modal.find("#pickDiv").find("img").hide();
                break;

        }
        $modal.find('#selectDeliveryType').val(pickShipType);
    },


    /**
     * [수거/배송방법] 선택 값에 따른 문구 설정
     * reqType C:택배 D:직배송 N:배송없음 P:우편배송
     * **/
    chgShipMethodType : function($modal, pickShipTypeCode){
        var shipMethodType = pickShipTypeCode;

        if($.jUtil.isEmpty(shipMethodType)){
            shipMethodType = $modal.find('#selectDeliveryType').val();
        }

        switch (shipMethodType) {
            case 'DS_DLV' ://택배배송
                $modal.find("#parcelDiv").show();
                $modal.find("#pickDiv,#postDiv").hide();
                $modal.find("#pickDiv").find("img").hide();
                break;
            case 'DS_DRCT' ://직배송
                $modal.find('#pickDiv').show();
                $modal.find('#parcelDiv,#postDiv').hide();
                $modal.find("#pickDiv").find("img").show();
                break;
            case 'N' ://배송없음
                $modal.find("#parcelDiv,#postDiv,#pickDiv").hide();
                $modal.find("#pickDiv").find("img").hide();
                break;
            case 'DS_POST' ://우편배송
                $modal.find('#postDiv').show();
                $modal.find('#parcelDiv,#pickDiv').hide();
                $modal.find("#pickDiv").find("img").hide();
                break;
        }
        $modal.find('#selectDeliveryType').val(shipMethodType);
    },


    /**
     * 첨부파일 적용
     * uploadFileName 값이 있을경우
     * 버튼 활성화/unerline/link 적용
     * **/
    setUploadFile : function($modal, res){
        if(!$.jUtil.isEmpty(res.uploadFileName)){
            $modal.find('#uploadFileName').css("text-decoration", "underline");
            $modal.find("#uploadFileNameUrl").val(res.uploadFileName);
            claimMainCommon.getUploadContentType($modal, $modal.find('#uploadFileNameUrl').val(), '');
        }else{
            $modal.find('#uploadFileName').hide();
        }

        if(!$.jUtil.isEmpty(res.uploadFileName2)){
            $modal.find('#uploadFileName2').css("text-decoration", "underline");
            $modal.find("#uploadFileNameUrl2").val(res.uploadFileName2);
            claimMainCommon.getUploadContentType($modal, $modal.find('#uploadFileNameUrl2').val(), '2');

        }else{
            $modal.find("#uploadFileName2").hide();
        }

        if(!$.jUtil.isEmpty(res.uploadFileName3)){
            $modal.find('#uploadFileName3').css("text-decoration", "underline");
            $modal.find("#uploadFileNameUrl3").val(res.uploadFileName3);
            claimMainCommon.getUploadContentType($modal, $modal.find('#uploadFileNameUrl3').val(), '3');

        }else{
            $modal.find("#uploadFileName3").hide();
        }

        if($.jUtil.isEmpty(res.uploadFileName) && $.jUtil.isEmpty(res.uploadFileName2) && $.jUtil.isEmpty(res.uploadFileName3)){
            $modal.find('#uploadRow').hide();
        }

    },

    /**
     * 첨부파일 확장자 조회
     * image/[확장자]
     * **/
    getUploadContentType : function($modal, fileId, seq) {
        var dataParam = {
            'fileId': fileId
        };
        var contentType;

        //파라미터 세팅
        //취소요청
        CommonAjax.basic(
            {
                url: '/claim/getClaimImageInfo.json?'.concat(jQuery.param(dataParam)),
                method: "GET",
                contentType: 'application/json?',
                successMsg: null,
                callbackFunc: function (res) {
                    $modal.find("#uploadFileNameType" + seq).val(res.contentType.replace('image/', ''));
                }
            });

    },

    /**
     * 반품배송비 적용
     * **/
    setReturnShipAmt: function ($modal, res) {

        var returnShipAmtText = '';
        var returnShipAmt = 0;
        var whoReasonTag;

        if(res.totAddShipPrice > 0 || res.totAddIslandShipPrice > 0 ){
              returnShipAmtText = '('
              if (res.totAddShipPrice > 0) {
                  returnShipAmt += res.totAddShipPrice;
                  returnShipAmtText += '추가 배송비 ' + $.jUtil.comma(res.totAddShipPrice) + '원'
              }

              if (res.totAddIslandShipPrice > 0) {
                  (res.totAddShipPrice > 0 ? returnShipAmtText += ' + '
                      : returnShipAmtText += '');
                  returnShipAmt += res.totAddIslandShipPrice;
                  returnShipAmtText += '추가 반송도서산간 배송비 ' + $.jUtil.comma(res.totAddIslandShipPrice) + '원'
              }
              returnShipAmtText += ') |';
        }

        switch (res.whoReason) {
            case "B" :
                whoReasonTag = '구매자 책임';
                $modal.find('#shipFeeEnclose').text(res.claimShipFeeEnclose);
                break
            case "S" :
                whoReasonTag = '판매자 책임';
                break
            case "H" :
                whoReasonTag = '홈플러스 책임';
                break
        }
        $modal.find('#returnShipAmt').text($.jUtil.comma(returnShipAmt) + '원');
        $modal.find('#returnShipAmtText').text(returnShipAmtText);
        $modal.find('#whoReason').text(whoReasonTag);
    },

    claimListSearchValidation : function (){

        var schStartDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);


        //조회시간 범위 3개월 체크
        if (schEndDt.diff(schStartDt, "month", true) > 3) {
            alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
            $("#schStartDt").val(schEndDt.add(-3, "month").format("YYYY-MM-DD"));
            return false;
        }
        var date = new Date();
        var lastYear = date.getFullYear();
        var month = date.getMonth()+1;
        var day = date.getDate();

        if(month.toString().length == 1){
            month = "0"+month;
        }
        var todayDate = moment(lastYear + '-' + month + '-' + day, 'YYYY-MM-DD', true)

        //최근 1년내 취소건 여부 체크
        if (todayDate.diff(schStartDt, "month", true) > 12) {
            alert("최근 1년 내 취소 건만 조회 가능합니다.");
            claimMainCommon.initSearchDate('-30d', 'schStartDt', 'schEndDt');
            return false;
        }else if (todayDate.diff(schEndDt, "month", true) > 12) {
          alert("최근 1년 내 취소 건만 조회 가능합니다.");
          claimMainCommon.initSearchDate('-30d', 'schStartDt', 'schEndDt');
          return false;
        }

        //처리상태 조건이 전체인 경우 일주일 이내만 검색가능
        if($('#schClaimStatus').val() == 'ALL'){
            if (schEndDt.diff(schStartDt, "day", true) > 7) {
                alert("처리상태 전체 검색 시, 조회기간은 최대 7일까지 설정 가능합니다.");
                $("#schStartDt").val(schEndDt.add(-7, "day").format("YYYY-MM-DD"));
                $('input:radio[name="setSchDt"]:input[value="-1w"]').prop("checked", true);
                return false;
            }
        }

        return true;
    },


    /**
     * 날짜 초기화
     * @param flag
     * @param startId
     * @param endId
     */
    initSearchDate : function (flag, startId, endId) {

        claimMainCommon.setDate = Calendar.datePickerRange(startId, endId);
        claimMainCommon.setDate.setEndDate(0);
        claimMainCommon.setDate.setStartDate(flag);

        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },

    writeToSpan : function (_formId, _data) {
        var value = "";
        var spanId = $('#' + _formId).find('span');

        for (var idx = 0; idx < spanId.length; idx++) {
            if (!$.jUtil.isEmpty(spanId[idx].id)) {
                value = _data[spanId[idx].id];
                if ($.jUtil.isEmpty(value)) {
                    value = '';
                }
                var checkId = spanId[idx].id.toLowerCase();
                if (checkId.indexOf("amt") > -1 || checkId.indexOf("price") || checkId.indexOf("cnt")
                    > -1) {
                    if (!isNaN(Number(value))) {
                        value = $.jUtil.comma(Number(value));
                    } else {
                        value = 0;
                    }
                }
                $(spanId[idx]).html(value);
            }
        }
    },

    keyupMobileNo : function ($modal){

    },

    writeToInputModal : function ($modal,_formId, _data) {
        var value = "";
        var spanId = $modal.find('#' + _formId).find('input');

        for (var idx = 0; idx < spanId.length; idx++) {
            if (!$.jUtil.isEmpty(spanId[idx].id)) {
                value = _data[spanId[idx].id];
                if ($.jUtil.isEmpty(value)) {
                    value = '';
                }
                var checkId = spanId[idx].id.toLowerCase();
                if (checkId.indexOf("amt") > -1 || checkId.indexOf("price") || checkId.indexOf("cnt")
                    > -1) {
                    if (!isNaN(Number(value))) {
                        value = $.jUtil.comma(Number(value));
                    } else {
                        value = 0;
                    }
                }
                $(spanId[idx]).html(value);
            }
        }
    },
    setRealGridArraysCheckable : function (_grid, _array, _compareFieldName) {
        // 비교를 위한 데이터 취득 용
        var compareValue = 0;
        var checkable = '';
        var notCheckedBundle = _array.join(",");
        for(var _idx = 0; _idx < _grid.dataProvider.getRowCount(); _idx++){
            var findValue = _grid.dataProvider.getValue(_idx, _compareFieldName);
            if(compareValue === findValue || notCheckedBundle.indexOf(findValue) > -1){
                if(checkable.length !== 0){
                    checkable += ' AND '
                }
                checkable += '(row <> ' + _idx + ')';
            }
            compareValue = findValue;
        }
        return checkable;
    }
}