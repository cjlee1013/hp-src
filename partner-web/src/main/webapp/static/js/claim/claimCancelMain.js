$(document).ready(function() {

    cancelMain.init();
    claimCancelListGrid.init();
    parentMain = cancelMain;
    parentGrid = claimCancelListGrid;
    CommonAjaxBlockUI.global();


    // 파라미터로 schStatus 있으면 해당 보드 카운트 클릭
    if (!$.jUtil.isEmpty(schStatus)) {
        $("#"+schStatus).trigger("click");
    }
});

/**
 * 상품관리 > 셀러/점포상품관리 > 상품 등록/수정
 */
var cancelMain = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
        claimMainCommon.initSearchDate('-1w', 'schStartDt', 'schEndDt');
        claimMainCommon.setClaimBoardCnt('C');
    },

    /**
     * 이벤트 바인딩
     */
    event : function() {

        // 보드 카운트 클릭 조회
        $("#request, #delay, #complete").on("click", function() { cancelMain.search($(this)) });

        // 검색 조건 초기화
        $('#searchResetBtn').bindClick(cancelMain.searchFormReset);

        // 검색 버튼
        $('#searchBtn').bindClick(function() {cancelMain.search('')});

        // 엑셀 다운로드
        $('#excelDownloadBtn').bindClick(cancelMain.excelDownload);

        // 건수 재조회
        $('#refreshClaimCnt').bindClick(function() { claimMainCommon.setClaimBoardCnt('C')});

        // 검색일자 radio 버튼 클릭
        $('input:radio[name="setSchDt"]').on('change', function() {
            cancelMain.searchDate();
        });
    },
    searchDate : function () {
        var flag = $('input[name="setSchDt"]:checked').val();
        claimMainCommon.initSearchDate(flag, 'schStartDt' , 'schEndDt');
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        claimCancelListGrid.excelDownload();
    },

    /**
     * 취소 리스트 검색
     */
    search : function($this) {

        if(!$.jUtil.isEmpty($this)){
            $('#schStatus').val($this.attr('id'));
        }else{
            $('#schStatus').val('');
        }

        //검색 유효성 검사
        if(!claimMainCommon.claimListSearchValidation()){
            return false;
        }

        var data = _F.getFormDataByJson($("#claimSearchForm"));

        CommonAjax.basic({
                url : '/claim/getClaimList.json',
                data : data,
                method : "POST",
                 contentType: 'application/json',
                successMsg:null,
                callbackFunc:function(res) {
                    claimCancelListGrid.setData(res);
                    $('#approveBtn,#rejectBtn').attr('disabled', true);
                }
        });

        CommonAjax.basic({
                url : '/claim/getClaimListCnt.json',
                data : data,
                method : "POST",
                 contentType: 'application/json',
                successMsg:null,
                callbackFunc:function(res) {
                    $('#claimTotalCount').html(res);
                }
        });

    },

    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {

        $("#claimSearchForm").resetForm();
        claimMainCommon.initSearchDate('-7d', 'schStartDt', 'schEndDt');

    },
    /**
     * callback function
     * **/
    callBackSearch : function(){
        cancelMain.search();
        claimMainCommon.setClaimBoardCnt('C');
    },

    /**
     * 취소 승인 팝업창 open
     */
    openCancelApprovePop: function() {
        var checkRowIds = claimCancelListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }

        var flag = true;
        for(var i=0; i<checkRowIds.length; i++){
            var checkedRow = claimCancelListGrid.dataProvider.getJsonRow(checkRowIds[i]);
            if(checkedRow.claimStatusCode != 'C1'){
                alert('취소승인 불가능한 거래가 선택됐습니다. 다시 선택해주세요');
                flag = false;
                break;
            }
        }
        if(!flag){
            return false;
        }

        cancelApprove.openModal();
    },

    /**
     * 취소 거부 팝업창 open
     */
    openCancelRejectPop: function() {
        var checkRowIds = claimCancelListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }

        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        var flag = true;

        for(var i=0; i<checkRowIds.length; i++){
            var checkedRow = claimCancelListGrid.dataProvider.getJsonRow(checkRowIds[i]);
            switch (checkedRow.claimStatusCode) {
                case 'C1' : case 'C8' :
                   flag = true;
                    break;
                default :
                    flag = false;
                    break;
            }
        }
        if(!flag){
            alert('취소거부 불가능한 거래가 선택됐습니다. 다시 선택해주세요');
            return false;
        }

        cancelReject.openModal();
    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = parentGrid.dataProvider.getJsonRow(selectRowId);

        $('#approveBtn,#rejectBtn').attr('disabled', true);


        if(rowDataJson.claimStatusCode == 'C1') {
            $('#approveBtn,#rejectBtn').attr('disabled', false);
        }else if(rowDataJson.claimStatusCode == 'C8'){
            $('#approveBtn').attr('disabled', false);
        }
    }
};


// 클레임관리 그리드
var claimCancelListGrid = {
  gridView : new RealGridJS.GridView("claimCancelListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
    claimCancelListGrid.initGrid();
    claimCancelListGrid.initDataProvider();
    claimCancelListGrid.event();
  },
  initGrid : function() {
    claimCancelListGrid.gridView.setDataSource(claimCancelListGrid.dataProvider);

    claimCancelListGrid.gridView.setStyles(claimCancelListGridBaseInfo.realgrid.styles);
    claimCancelListGrid.gridView.setDisplayOptions(claimCancelListGridBaseInfo.realgrid.displayOptions);
    claimCancelListGrid.gridView.setColumns(claimCancelListGridBaseInfo.realgrid.columns);
    claimCancelListGrid.gridView.setOptions(claimCancelListGridBaseInfo.realgrid.options);

    claimCancelListGrid.gridView.setColumnProperty("claimBundleNo", "mergeRule", {criteria:"value"});
  },
  initDataProvider : function() {
    claimCancelListGrid.dataProvider.setFields(claimCancelListGridBaseInfo.dataProvider.fields);
    claimCancelListGrid.dataProvider.setOptions(claimCancelListGridBaseInfo.dataProvider.options);
  },
  event : function() {

    // 배송번호 렌더링
    claimCancelListGrid.gridView.setColumnProperty("claimBundleNo", "renderer", {
          type: "link",
          url: "unnecessary",
          requiredFields: "claimBundleNo",
          showUrl: false
    });

     // 그리드 선택
     claimCancelListGrid.gridView.onDataCellClicked = function(grid, index) {
        //클레임 상태에 따른 버튼 노출
        cancelMain.gridRowSelect(index.dataRow);
     }

     // checkbar 선택
     claimCancelListGrid.gridView.onItemChecked = function(grid, index, checked) {
         //클레임 상태에 따른 버튼 노출
         if(checked){
             cancelMain.gridRowSelect(index);
         }else{
            $('#approveBtn,#rejectBtn').attr('disabled', true);
         }
     }

     // 그리드 선택
     claimCancelListGrid.gridView.onColumnCheckedChanged = function(grid, index) {
        //클레임 상태에 따른 버튼 노출
        cancelMain.gridRowSelect(index.dataRow);
     }

    //클레임 번호 선택
    claimCancelListGrid.gridView.onLinkableCellClicked = function(grid, index, url) {
        if (index.fieldName == "claimBundleNo") {
            itemClaimDetailPop.openModal();
        }
    };
  },
  setData : function(dataList) {
    claimCancelListGrid.dataProvider.clearRows();
    claimCancelListGrid.dataProvider.setRows(dataList);
  },

  excelDownload: function() {

    if(claimCancelListGrid.gridView.getItemCount() == 0) {
      alert("검색 결과가 없습니다.");
      return false;
    }

    if(!confirm("구매자의 개인 정보는 암호화 하여야 하며, 판매 목적을 달성한 경우 즉시 파기 되어야 합니다.\n"
        + "구매자의 개인정보를 이용하여 광고 홍보등의 수집 목적 외로 사용해서는 안됩니다.\n"
        + "상기 사항 등 관계 법령 위반으로 발생하는 모든 민,형사상 책임은 판매자 본인에게 있습니다.\n\n"
        + "상기 내용을 숙지하였으며, 이에 동의합니다.")){
        return false;
    }

    var _date = new Date();
    var fileName =  "취소관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();



    claimCancelListGrid.gridView.exportGrid({
      type: "excel",
      target: "local",
      fileName: fileName + ".xlsx",
      showProgress: true,
      progressMessage: "엑셀 Export중입니다."
    });
  }
};

