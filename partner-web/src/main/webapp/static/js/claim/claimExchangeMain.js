$(document).ready(function() {

    exchangeMain.init();
    claimExchangeListGrid.init();
    parentMain = exchangeMain;
    parentGrid = claimExchangeListGrid;
    CommonAjaxBlockUI.global();

    // 파라미터로 schStatus 있으면 해당 보드 카운트 클릭
    if (!$.jUtil.isEmpty(schStatus)) {
        $("#"+schStatus).trigger("click");
    }
});

/**
 * 상품관리 > 셀러/점포상품관리 > 상품 등록/수정
 */
var exchangeMain = {
    /**
     * 초기화
     */
    init : function() {

        this.event();
        claimMainCommon.initSearchDate('-1w', 'schStartDt', 'schEndDt');
        claimMainCommon.setClaimBoardCnt('X');
    },

    /**
     * 이벤트 바인딩
     */
    event : function() {

        // 보드 카운트 클릭 조회
        $("#request, #complete, #pickComplete,#pending").on("click", function() { exchangeMain.search($(this)) });

        // 검색 조건 초기화
        $('#searchResetBtn').bindClick(exchangeMain.searchFormReset);

        // 검색 버튼
        $('#searchBtn').bindClick(function(){ exchangeMain.search('')});

        // 엑셀 다운로드
        $('#excelDownloadBtn').bindClick(exchangeMain.excelDownload);

        // 건수 재조회
        $('#refreshClaimCnt').bindClick(function() { claimMainCommon.setClaimBoardCnt('X')});


        // 검색일자 radio 버튼 클릭
        $('input:radio[name="setSchDt"]').on('change', function() {
            exchangeMain.searchDate();
        });
    },
    searchDate : function () {
        var flag = $('input[name="setSchDt"]:checked').val();
        claimMainCommon.initSearchDate(flag, 'schStartDt' , 'schEndDt');
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        claimExchangeListGrid.excelDownload();
    },

    /**
     * 반품 리스트 검색
     */
    search : function($this) {

        if(!$.jUtil.isEmpty($this)){
            $('#schStatus').val($this.attr('id'));
        }else{
            $('#schStatus').val('');
        }

        //검색 유효성 검사
        if(!claimMainCommon.claimListSearchValidation()){
            return false;
        }

        var data = _F.getFormDataByJson($("#claimSearchForm"));

        CommonAjax.basic({
                url : '/claim/getExchangeClaimList.json',
                data : data,
                method : "POST",
                 contentType: 'application/json',
                successMsg:null,
                callbackFunc:function(res) {
                    claimExchangeListGrid.setData(res);
                    var ClaimBundleArray = [];
                    var _idx = 0;
                    for(var i=0; i<res.length; i++){
                        if(_idx++ !== (res.length - 1)){
                            if(res[i].claimBundleNo !== res[_idx].claimBundleNo){
                                    ClaimBundleArray.push(res[i].bundleNo);
                            }
                        } else {
                            ClaimBundleArray.push(res[i].bundleNo);
                        }
                    }
                    claimExchangeListGrid.gridView.setCheckableExpression(
                       claimMainCommon.setRealGridArraysCheckable(claimExchangeListGrid, ClaimBundleArray, 'claimBundleNo'), true
                    );

                    $('#pickReqBtn,#pickCompleteBtn,#exchangeReqShipBtn,#pendingBtn,#exchCompleteBtn,#chgPickInfoBtn,#chgExchInfoBtn,#rejectBtn').attr('disabled', true);
                }
        });

        CommonAjax.basic({
                url : '/claim/getClaimListCnt.json',
                data : data,
                method : "POST",
                 contentType: 'application/json',
                successMsg:null,
                callbackFunc:function(res) {
                    $('#claimTotalCount').html(res);
                }
        });

    },

    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {

        $("#claimSearchForm").resetForm();
        claimMainCommon.initSearchDate('-7d', 'schStartDt', 'schEndDt');

    },
    /**
     * 수거요청 팝업창 open
     */
    openPickReqPop : function(){

        var checkRowIds = claimExchangeListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }

        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        pickRequest.openModal();
    },
    /**
     * 수거완료 팝업창 open
     */
    openPickCompletePop : function(){
        var checkRowIds = claimExchangeListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }

        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        pickComplete.openModal();
    },
    /**
     * 수거완료 팝업창 open
     */
    openExchangeReqShipPop : function(){
        var checkRowIds = claimExchangeListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }

        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        exchangeRegShip.openModal();
    },

    /**
     * 교환완료 팝업창 open
     */
    openExchangeCompletePop: function() {
        var checkRowIds = claimExchangeListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }

        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        exchangeComplete.openModal();
    },

    /**
     * 교환보류 팝업창 open
     */
    openClaimPendingPop: function() {
        var checkRowIds = claimExchangeListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }

        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        claimPending.openModal();
    },

    /**
     * 수거정보변경 팝업창 open
     */
    openChgPickShippingPop : function(){
        var checkRowIds = claimExchangeListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }

        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        chgPickShipping.openModal();

    },
    /**
     * 배송정보변경 팝업창 open
     */
    openChgExchShippingPop : function(){
        var checkRowIds = claimExchangeListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }

        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        chgExchShipping.openModal();

    },

    /**
     * 교환거부 팝업창 open
     */
    openExchangeRejectPop: function() {
        var checkRowIds = claimExchangeListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("클레임을 선택해 주세요.");
            return;
        }
        if (checkRowIds.length > 1) {
            alert("1개의 클레임을 선택해 주세요.");
            return;
        }
        exchangeReject.openModal();
    },


    /**
     * callback function
     * **/
    callBackSearch : function(){
        exchangeMain.search();
        claimMainCommon.setClaimBoardCnt('X');
    },

    /**
    * 그리드 row 선택
    * @param selectRowId
    */
    gridRowSelect : function(selectRowId) {
       var rowDataJson = parentGrid.dataProvider.getJsonRow(selectRowId);
       var claimStatusCode = rowDataJson.claimStatusCode;
       var pickStatusCode = rowDataJson.pickStatusCode;

       $('#pickReqBtn,#pickCompleteBtn,#exchangeReqShipBtn,#pendingBtn,#exchCompleteBtn,#chgPickInfoBtn,#chgExchInfoBtn,#rejectBtn').attr('disabled', true);


       //수거요청 : 교환신청(C1), 교환승인(C2) && 수거대기(NN), 수거실패(P8)
       if((claimStatusCode == 'C1' || claimStatusCode == 'C2' || claimStatusCode == 'C8') && (pickStatusCode == 'N0' || pickStatusCode == 'NN' || pickStatusCode == 'P0' || pickStatusCode == 'P8')){
          $('#pickReqBtn').attr('disabled', false);
       }
       //수거완료 : 교환신청(C1), 교환보류(C8) && 수거대기(NN), 요청완료(P1), 수거중(P2), 수거실패(P8)
       if((claimStatusCode == 'C1' || claimStatusCode == 'C2' || claimStatusCode == 'C8') && (pickStatusCode == 'N0' || pickStatusCode == 'NN' || pickStatusCode == 'P0' || pickStatusCode == 'P1' || pickStatusCode == 'P2' || pickStatusCode == 'P8')){
           $('#pickCompleteBtn').attr('disabled', false);
       }
       //교환배송 : 교환신청(C1) && 교환보류(C8)
       if((claimStatusCode == 'C1' || claimStatusCode == 'C8')){
           $('#exchangeReqShipBtn').attr('disabled', false);
       }

       //교환보류 : 교환신청(C1) && 수거대기(NN), 요청완료(P1), 수거중(P2), 수거완료(P3), 수거실패(P8)
       if(claimStatusCode == 'C1' && (pickStatus == 'N0' || pickStatusCode == 'NN' || pickStatusCode == 'P0' || pickStatusCode == 'P1' || pickStatusCode == 'P2' || pickStatusCode == 'P3'|| pickStatusCode == 'P8')){
           $('#pendingBtn').attr('disabled', false);
       }

       //교환완료 : 교환승인(C2) && 수거완료(P3)
        if(claimStatusCode == 'C1' || claimStatusCode == 'C8'){
           $('#exchCompleteBtn').attr('disabled', false);
       }

       //수거정보변경 : 교환신청(C1), 교환보류(C8) && 수거대기(NN), 수거중(P2), 수거실패(P8)
       if((claimStatusCode == 'C1' || claimStatusCode == 'C8') && (pickStatusCode == 'N0' || pickStatusCode == 'NN' || pickStatusCode == 'P2' || pickStatusCode == 'P8')){
           $('#chgPickInfoBtn').attr('disabled', false);
       }
       //배송지방법변경 : 교환신청(C1), 교환보류(C8), 교환승인(C2)
       if(claimStatusCode == 'C1' || claimStatusCode == 'C8' || claimStatusCode == 'C2' ){
           $('#chgExchInfoBtn').attr('disabled', false);
       }

       //교환거부 : 반품신청(C1), 반품보류(C8)
        if(claimStatusCode == 'C1' || claimStatusCode == 'C8'){
            $('#rejectBtn').attr('disabled', false);
        }
    }

};


// 클레임관리 그리드
var claimExchangeListGrid = {
  gridView : new RealGridJS.GridView("claimExchangeListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
    claimExchangeListGrid.initGrid();
    claimExchangeListGrid.initDataProvider();
    claimExchangeListGrid.event();
  },
  initGrid : function() {
    claimExchangeListGrid.gridView.setDataSource(claimExchangeListGrid.dataProvider);

    claimExchangeListGrid.gridView.setStyles(claimExchangeListGridBaseInfo.realgrid.styles);
    claimExchangeListGrid.gridView.setDisplayOptions(claimExchangeListGridBaseInfo.realgrid.displayOptions);
    claimExchangeListGrid.gridView.setColumns(claimExchangeListGridBaseInfo.realgrid.columns);
    claimExchangeListGrid.gridView.setOptions(claimExchangeListGridBaseInfo.realgrid.options);
    claimExchangeListGrid.gridView.setCheckBar({"exclusive": "true", "showGroup":"true"})

    claimExchangeListGrid.gridView.setColumnProperty("claimBundleNo", "mergeRule", {criteria:"value"});
  },
  initDataProvider : function() {
    claimExchangeListGrid.dataProvider.setFields(claimExchangeListGridBaseInfo.dataProvider.fields);
    claimExchangeListGrid.dataProvider.setOptions(claimExchangeListGridBaseInfo.dataProvider.options);
  },
  event : function() {

    // 배송번호 렌더링
    claimExchangeListGrid.gridView.setColumnProperty("claimBundleNo", "renderer", {
          type: "link",
          url: "unnecessary",
          requiredFields: "claimBundleNo",
          showUrl: false
    });

    // 반품수거 송장번호 렌더링
    claimExchangeListGrid.gridView.setColumnProperty("invoiceNo", "renderer", {
          type: "link",
          url: "unnecessary",
          requiredFields: "invoiceNo",
          showUrl: false
    });

    // 교환배송 송장번호 렌더링
    claimExchangeListGrid.gridView.setColumnProperty("pickInvoiceNo", "renderer", {
          type: "link",
          url: "unnecessary",
          requiredFields: "pickInvoiceNo",
          showUrl: false
    });

    // 교환 송장번호 렌더링
    claimExchangeListGrid.gridView.setColumnProperty("exchInvoiceNo", "renderer", {
          type: "link",
          url: "unnecessary",
          requiredFields: "exchInvoiceNo",
          showUrl: false
    });


    // 그리드 선택
     claimExchangeListGrid.gridView.onDataCellClicked = function(grid, index) {
        //클레임 상태에 따른 버튼 노출
        exchangeMain.gridRowSelect(index.dataRow);
     };

     // checkbar 선택
     claimExchangeListGrid.gridView.onItemChecked = function(grid, index, checked) {
         //클레임 상태에 따른 버튼 노출
         if(checked){
             exchangeMain.gridRowSelect(index);
         }else{
             $('#pickReqBtn,#pickCompleteBtn,#exchangeReqShipBtn,#pendingBtn,#exchCompleteBtn,#chgPickInfoBtn,#chgExchInfoBtn,#rejectBtn').attr('disabled', true);
         }
     }


    // 그리드 선택
    claimExchangeListGrid.gridView.onLinkableCellClicked = function(grid, index, url) {
        var rowDataJson = claimExchangeListGrid.dataProvider.getJsonRow(index.dataRow);
        var invoiceNo = null;
        var dlvCd = null;
        var dlvCdNm = null;

        if (index.fieldName == "claimBundleNo") {
            itemClaimDetailPop.openModal();
        }else if (index.fieldName == "invoiceNo" && !$.jUtil.isEmpty(rowDataJson.invoiceNo)) {
            invoiceNo = rowDataJson.invoiceNo;
            dlvCd = rowDataJson.dlvCd;
            dlvCdNm = rowDataJson.dlvCdNm;
            claimShipHistoryPop.openModal(invoiceNo, dlvCd, dlvCdNm);
        }else if (index.fieldName == "pickInvoiceNo" && !$.jUtil.isEmpty(rowDataJson.pickInvoiceNo)) {
            invoiceNo = rowDataJson.pickInvoiceNo;
            dlvCd = rowDataJson.pickDlvCd;
            dlvCdNm = rowDataJson.pickDlvCdNm;
            claimShipHistoryPop.openModal(invoiceNo, dlvCd, dlvCdNm);
        }else if (index.fieldName == "exchInvoiceNo" && !$.jUtil.isEmpty(rowDataJson.exchInvoiceNo)){
            invoiceNo = rowDataJson.exchInvoiceNo;
            dlvCd = rowDataJson.exchDlvCd;
            dlvCdNm = rowDataJson.exchDlvCdNm;
            claimShipHistoryPop.openModal(invoiceNo, dlvCd, dlvCdNm);
        }
    };
  },
  setData : function(dataList) {
    claimExchangeListGrid.dataProvider.clearRows();
    claimExchangeListGrid.dataProvider.setRows(dataList);
  },

  excelDownload: function() {
    if(claimExchangeListGrid.gridView.getItemCount() == 0) {
      alert("검색 결과가 없습니다.");
      return false;
    }

    if(!confirm("구매자의 개인 정보는 암호화 하여야 하며, 판매 목적을 달성한 경우 즉시 파기 되어야 합니다.\n"
        + "구매자의 개인정보를 이용하여 광고 홍보등의 수집 목적 외로 사용해서는 안됩니다.\n"
        + "상기 사항 등 관계 법령 위반으로 발생하는 모든 민,형사상 책임은 판매자 본인에게 있습니다.\n\n"
        + "상기 내용을 숙지하였으며, 이에 동의합니다.")){
        return false;
    }

    var _date = new Date();
    var fileName =  "교환관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

    claimExchangeListGrid.gridView.exportGrid({
      type: "excel",
      target: "local",
      fileName: fileName + ".xlsx",
      showProgress: true,
      progressMessage: "엑셀 Export중입니다."
    });
  }
};

