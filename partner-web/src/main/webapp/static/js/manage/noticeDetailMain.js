$(document).ready(function() {

    noticeDetailMain.init();
    CommonAjaxBlockUI.global();
});

var noticeDetailMain = {

    init : function() {

        this.getDetail();

    },
    getDetail : function(){

        var kind = $('#kind').val();
        var dspNo = $('#dspNo').val();

        switch (kind) {

            case "NOTICE" :
                noticeDetailMain.getNoticeDetail(dspNo);
                break;
            case "FAQ" :
                noticeDetailMain.getFaqDetail(dspNo);
                break;
        }

    },
    getNoticeDetail : function (dspNo) {

        CommonAjax.basic({url:'/manage/notice/getNoticeDetail.json?dspNo=' +dspNo, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                noticeDetailMain.setDetail(res);
            }});
    },
    getFaqDetail : function (dspNo) {
        CommonAjax.basic({url:'/manage/notice/getFaqDetail.json?dspNo=' +dspNo, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                noticeDetailMain.setDetail(res);
            }});

    },
    setDetail : function(data){

        var topYn = data.topYn;
        var title = data.title;

        if(topYn =='Y') {
            title = '<span class="flag type2 valign-t">중요</span>' + title;
        }
        $('#title').html(title)
        $('#regDt').html(data.chgDt)
        $('#kindTxt').html(data.kindTxt)
        $('#contents').html(data.contents)

        var fileList = data.fileList;

        for(var i in fileList){
            noticeDetailMain.appendFile(fileList[i])
        }

    },

    appendFile : function(f){

        var fileAppend= '<a href="'
        + hmpImgUrl + "/provide/view?processKey=ItemFile&fileId=" + f.fileId
        + '" target="_blank" class="btn-txt fc05" ">'
        + f.fileName
        $("#file").append(fileAppend);
    },
};

