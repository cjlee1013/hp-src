$(document).ready(function() {

    harmfulDetailMain.init();
    CommonAjaxBlockUI.global();
});

var harmfulDetailMain = {

    init : function() {
        this.bindingEvent();
        this.getDetail();
    }
    , bindingEvent : function() {
        $("#searchUpssPop").bindClick(harmfulDetailMain.searchUpssPop);
    }
    , getDetail : function(){

        var param = 'noDocument='+ $('#noDocument').val()
            + '&seq='+ $('#seq').val()
            + '&cdInsptmachi='+ $('#cdInsptmachi').val()

        CommonAjax.basic({url:'/manage/notice/getHarmfulDetail.json?' +param, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
            for (var item in res) {
                $('#'+item).html(res[ item ]);
            }
        }});
    }
    , searchUpssPop : function () {
        var noDocument = $("#noDocument").val();
        var seq = $("#seq").val();
        var cdInsptmachi = $("#cdInsptmachi").val();

        if( !$.jUtil.isEmpty(cdInsptmachi) && !$.jUtil.isEmpty(noDocument) && !$.jUtil.isEmpty(seq) ) {

            var popUrl = "http://upss.gs1kr.org/home/harzd/harzdDetail.gs1?cdInsptmachi=" + cdInsptmachi + "&seq=" + seq + "&noDocument=" + noDocument;
            var _width = 1350;
            var _height = 650;

            var popupNewWin = window.open(popUrl, "_blank", "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=" + _width + ",height=" + _height, "");
            if (popupNewWin == null) {
                alert("팝업 차단기능 혹은 팝업차단 프로그램이 동작중입니다. 팝업 차단 기능을 해제한 후 다시 시도하세요.");
            }

        } else {
            alert("선택 후 클릭해 주세요.");
        }
    }
};

