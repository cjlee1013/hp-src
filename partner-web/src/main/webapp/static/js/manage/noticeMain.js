$(document).ready(function() {

    noticeMain.init();
    noticeListGrid.init();
    harmfulItemListGrid.init();
    CommonAjaxBlockUI.global();
});
var harmfulItemListGrid = {

    gridView : new RealGridJS.GridView("harmfulItemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        harmfulItemListGrid.initGrid();
        harmfulItemListGrid.initDataProvider();
        harmfulItemListGrid.event();

    },
    initGrid : function() {
        harmfulItemListGrid.gridView.setDataSource(harmfulItemListGrid.dataProvider);

        harmfulItemListGrid.gridView.setStyles(harmfulItemListGridBaseInfo.realgrid.styles);
        harmfulItemListGrid.gridView.setDisplayOptions(harmfulItemListGridBaseInfo.realgrid.displayOptions);
        harmfulItemListGrid.gridView.setColumns(harmfulItemListGridBaseInfo.realgrid.columns);
        harmfulItemListGrid.gridView.setOptions(harmfulItemListGridBaseInfo.realgrid.options);
        harmfulItemListGrid.gridView.setColumnProperty( "itemNm", "styles" ,{
            "textAlignment": "near",
        });

    },
    initDataProvider : function() {
        harmfulItemListGrid.dataProvider.setFields(harmfulItemListGridBaseInfo.dataProvider.fields);
        harmfulItemListGrid.dataProvider.setOptions(harmfulItemListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        harmfulItemListGrid.gridView.onDataCellClicked = function(gridView, index) {
            noticeMain.harmfulDetailOpenTab(index.dataRow);
        };

    },
    setData : function(dataList) {
        harmfulItemListGrid.dataProvider.clearRows();
        harmfulItemListGrid.dataProvider.setRows(dataList);
    }

};


var noticeListGrid = {

    gridView : new RealGridJS.GridView("noticeListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        noticeListGrid.initGrid();
        noticeListGrid.initDataProvider();
        noticeListGrid.event();

    },
    initGrid : function() {
        noticeListGrid.gridView.setDataSource(noticeListGrid.dataProvider);

        noticeListGrid.gridView.setStyles(noticeListGridBaseInfo.realgrid.styles);
        noticeListGrid.gridView.setDisplayOptions(noticeListGridBaseInfo.realgrid.displayOptions);
        noticeListGrid.gridView.setColumns(noticeListGridBaseInfo.realgrid.columns);
        noticeListGrid.gridView.setOptions(noticeListGridBaseInfo.realgrid.options);

        noticeListGrid.gridView.setColumnProperty( "title", "styles" ,{
            "textAlignment": "near",
            "lineAlignment": "center",
            "iconLocation": "left",     //top, bottom, center, left, right
            "iconAlignment": "center",   //near, center, far  상중하
            "iconOffset": 5 ,           //셀라인과의 여백
            "iconPadding": "0",
        });
        noticeListGrid.gridView.setColumnProperty( "title", "renderer" ,{
            "type": "multiIcon",
            "showTooltip": false,
            "textVisible": true,
            "minWidth": 30,
            renderCallback:function(grid, index) {

                var value = noticeListGrid.dataProvider.getJsonRow(index.dataRow);
                var ret = [];
                if (value.topYn == "Y") {
                    ret.push('/static/images/icon_imp.png');
                }
                if (value.newYn == "Y") {
                    ret.push('/static/images/icon_new.png');
                }
                return ret;
            }
        });

    },
    initDataProvider : function() {
        noticeListGrid.dataProvider.setFields(noticeListGridBaseInfo.dataProvider.fields);
        noticeListGrid.dataProvider.setOptions(noticeListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        noticeListGrid.gridView.onDataCellClicked = function(gridView, index) {
            noticeMain.noticeDetailOpenTab(index.dataRow);
        };

    },
    setData : function(dataList) {
        noticeListGrid.dataProvider.clearRows();
        noticeListGrid.dataProvider.setRows(dataList);
    }

};
/**
 * 상품관리 > 셀러/점포상품관리 > 상품 등록/수정
 */
var noticeMain = {
    /**
     * 초기화
     */
    init : function() {

        this.event();
        this.searchNotice('GENERAL');
    },

    /**
     * 이벤트 바인딩
     */
    event : function() {

        $(document).on('click','.btn-group',function(){

            $(this).parent().find('.btn-group').removeClass('active').attr('aria-selected', false);
            $(this).addClass('active').attr('aria-selected', true);

            //그리드 데이터 삭제
            noticeListGrid.dataProvider.clearRows();
            harmfulItemListGrid.dataProvider.clearRows();

            $("#csNotiTotalCount").html('0');
            $("#harmfulItemTotalCount").html('0');

            $("#faqType").val('').hide();
            $("#harmTab").hide();
            var clickId = $(this).attr('id');

            if('FAQ' == clickId){
                $("#faqType").show();
            }
            if('HARMFUL' == clickId){
                $("#headerTitle").html('위해상품공지');
                $("#harmTab").show();
                $("#baseTab").hide();
                harmfulItemListGrid.gridView.resetSize();
            }else{
                $("#headerTitle").html('공지사항');
                $("#harmTab").hide();
                $("#baseTab").show();
                noticeListGrid.gridView.resetSize();
            }

            switch (clickId) {

                case "GENERAL":
                case "MANUAL" :
                    noticeMain.searchNotice(clickId);
                    break;
                case "FAQ":
                    noticeMain.searchFaq();
                    break;
                case "HARMFUL":
                    noticeMain.searchHarmful();
                    break;

            }

        });
        $('#faqType').bindChange(noticeMain.searchFaq);



    },
    //일반, 매뉴얼 리스트 조회
    searchNotice : function (clickId) {


        CommonAjax.basic({url:'/manage/notice/getNoticeList.json?noticeKind=' + clickId, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                noticeListGrid.setData(res);
                $('#csNotiTotalCount').html($.jUtil.comma(noticeListGrid.gridView.getItemCount()));
            }});

    },
    //FAQ 조회
    searchFaq : function () {

        var faqClass  = $('#faqType').val();

        CommonAjax.basic({url:'/manage/notice/getFaqList.json?faqClass=' + faqClass, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                noticeListGrid.setData(res);
                $('#csNotiTotalCount').html($.jUtil.comma(noticeListGrid.gridView.getItemCount()));
            }});

    },
    //위해상품공지 조회
    searchHarmful : function (clickId) {

        CommonAjax.basic({url:'/manage/notice/getHarmfulItemList.json', data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                harmfulItemListGrid.setData(res);
                $('#harmfulItemTotalCount').html($.jUtil.comma(harmfulItemListGrid.gridView.getItemCount()));
            }});

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    noticeDetailOpenTab : function(selectRowId) {
        var rowDataJson = noticeListGrid.dataProvider.getJsonRow(selectRowId);
        var param = "?dspNo="+rowDataJson.dspNo+"&kind="+rowDataJson.kind;

        UIUtil.addTabWindow('/manage/notice/noticeDetailMain' + param,'Tab_noticeDetail', '공지사항 상세', '140');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    harmfulDetailOpenTab : function(selectRowId) {
        var rowDataJson = harmfulItemListGrid.dataProvider.getJsonRow(selectRowId);
        var param = "?noDocument="+rowDataJson.noDocument;
            param += "&seq="+rowDataJson.seq;
            param += "&cdInsptmachi="+rowDataJson.cdInsptmachi;

        UIUtil.addTabWindow('manage/notice/harmfulDetailMain'+ param,'Tab_harmfulDetail', '위해상품공지 상세', '140');
    }
};

