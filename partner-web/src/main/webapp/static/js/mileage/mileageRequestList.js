/***
 * 마일리지 지급/회수관리 그리드 설정
 */
var mileageReqListGrid = {
    gridView : new RealGridJS.GridView("mileageListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        mileageReqListGrid.initGrid();
        mileageReqListGrid.initDataProvider();
        mileageReqListGrid.event();
    },
    initGrid : function () {
        mileageReqListGrid.gridView.setDataSource(mileageReqListGrid.dataProvider);
        mileageReqListGrid.gridView.setStyles(mileageListGridInfo.realgrid.styles);
        mileageReqListGrid.gridView.setDisplayOptions(mileageListGridInfo.realgrid.displayOptions);
        mileageReqListGrid.gridView.setColumns(mileageListGridInfo.realgrid.columns);
        mileageReqListGrid.gridView.setOptions(mileageListGridInfo.realgrid.options);
    },
    initDataProvider : function () {
        mileageReqListGrid.dataProvider.setFields(mileageListGridInfo.dataProvider.fields);
        mileageReqListGrid.dataProvider.setOptions(mileageListGridInfo.dataProvider.options);
    },
    event : function () {
        // 그리드 선택
        mileageReqListGrid.gridView.onDataCellClicked = function(gridView, index) {
            mileageReqListGrid.selectRow(index.dataRow)
        };
    },
    setData : function(dataList) {
        mileageReqListGrid.dataProvider.clearRows();
        mileageReqListGrid.dataProvider.setRows(dataList);
    },
    selectRow : function (selectRowId) {
        var rowDataJson = mileageReqListGrid.dataProvider.getJsonRow(selectRowId);
        $('#mileageReqNo').val(rowDataJson.mileageReqNo);

        mileageReqListGrid.setBasicData();
        mileageReqListGrid.setDetailData();
    },
    setBasicData : function () {
        var selectRowData = "mileageReqNo="+ $('#mileageReqNo').val();
        CommonAjax.basic(
            {
                url:'/mileage/payment/getMileageRequestBasic.json?'+ selectRowData,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    $("#mileageTypeNo").val(res.mileageTypeNo);
                    $("#requestType").val(res.requestType);
                    $("#processDt").val(res.processDt);
                    $("#directlyYn").val(res.directlyYn);
                    $("#requestReason").val(res.requestReason);
                    $("#displayMessage").val(res.displayMessage);
                    $("#useYn").val(res.useYn);
                    $("#requestMessage").val(res.requestMessage);
                    mileageReqList.checkDirectExec();
                    mileageReqListGrid.setMileageTypeDetail(res.mileageTypeNo);
                }
            });
    },
    setDetailData : function () {
        var selectRowData = "mileageReqNo="+ $('#mileageReqNo').val();
        CommonAjax.basic(
            {
                url:'/mileage/payment/getMileageRequestDetail.json?'+ selectRowData,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    $("#regUserNo").val(res.userNo);
                    $("#mileageDetailStatus").val(res.mileageDetailStatus);
                    $("#detailMessage").val(res.detailMessage);
                    $("#mileageReqDetailNo").val(res.mileageReqDetailNo);
                    mileageDetailGrid.setData(res);
                }
            });
    },
    setMileageTypeDetail : function (_data) {
        CommonAjax.basic({ //현재 사용여부 상관없이 조회
            url: "/mileage/type/getMileageTypeList.json?storeType=A&useYn=A&mileageKind=A&schDetailCode=MILEAGE_TYPE_NO&schDetailDesc="+ _data,
            contentType: 'application/json',
            data: null,
            method: "GET",
            callbackFunc: function (res) {
                $("#mileageTypeName").text(res.mileageTypeName);
            }
        });
    },
    excelDownload : function () {
        if(mileageReqListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "마일리지지급회수내역_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        mileageReqListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
/***
 * 마일리지 지급/회수 대상 회원 그리드
 */
var mileageDetailGrid = {
    gridView : new RealGridJS.GridView("mileageDetailGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        mileageDetailGrid.initGrid();
        mileageDetailGrid.initDataProvider();
        mileageDetailGrid.event();
    },
    initGrid : function () {
        mileageDetailGrid.gridView.setDataSource(mileageDetailGrid.dataProvider);
        mileageDetailGrid.gridView.setStyles(mileageDetailGridInfo.realgrid.styles);
        mileageDetailGrid.gridView.setDisplayOptions(mileageDetailGridInfo.realgrid.displayOptions);
        mileageDetailGrid.gridView.setOptions(mileageDetailGridInfo.realgrid.options);
        mileageDetailGrid.gridView.setIndicator({ visible: true });

        //그리드 에디터화
        mileageDetailGrid.gridView.setEditOptions({
            editable      : true,
            appendable    : true,
            insertable    : true,
            updatable     : true,
            deletable     : true,
            readOnly      : false
        });

        var gridColumns = [
            {
                name: "userNo",
                fieldName: "userNo",
                dataType: "text",
                header: {
                    text: "회원번호"
                }
            },
            {
                name: "userNm",
                fieldName: "userNm",
                dataType: "text",
                header: {
                    text: "회원이름"
                }
            },
            {
                name: "mileageAmt",
                fieldName: "mileageAmt",
                dataType: "number",
                header: {
                    text: "금액"
                }
            },
            {
                name: "mileageDetailStatus",
                fieldName: "mileageDetailStatus",
                dataType: "text",
                header: {
                    text: "상태"
                }
            },
            {
                name: "detailMessage",
                fieldName: "detailMessage",
                dataType: "text",
                //readOnly : true,
                header: {
                    text: "비고"
                }
            },
        ]
        mileageDetailGrid.gridView.setColumns(gridColumns); //edit columns

        //복사시 콤마 제거
        mileageDetailGrid.gridView.setPasteOptions({ numberChars: ","});
    },
    initDataProvider : function () {
        mileageDetailGrid.dataProvider.setFields(mileageDetailGridInfo.dataProvider.fields);
        mileageDetailGrid.dataProvider.setOptions(mileageDetailGridInfo.dataProvider.options);
    },
    event : function () {
        //줄번호 선택
        mileageDetailGrid.gridView.onIndicatorCellDblClicked =  function (gridView, index) {
            mileageDetailGrid.dataProvider.removeRow(index); //행 삭제
        };
    },
    setData : function(data) {
        mileageDetailGrid.dataProvider.clearRows();
        mileageDetailGrid.dataProvider.setRows(data);
    },
    addRow : function () {
        mileageDetailGrid.gridView.beginAppendRow();
        mileageDetailGrid.gridView.showEditor();
        mileageDetailGrid.gridView.setFocus();
        mileageDetailGrid.gridView.commit(true);
    },
    clearRow : function () {
        mileageDetailGrid.gridView.commit(true);
        $('#mileageReqNo').val()==""? mileageDetailGrid.dataProvider.clearRows(): mileageReqListGrid.setDetailData();
        $('#uploadFileNm, #uploadExcelForm input[name="uploadFile"]').val('');
        $("#mileageReqNo").val("");
    },
    excelDownload : function () {
        if(mileageDetailGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "마일리지대상회원_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        mileageDetailGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다.",
            indicator: "hidden"
        });
    },
};
var mileageReqList = {
    init : function () {
        this.initSearchDate('0d');
        this.initProcessDate();
        this.setMileageReqInfo();
    },
    initSearchDate : function (flag) {
        mileageReqList.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        mileageReqList.setDate.setEndDate(0);
        mileageReqList.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    initProcessDate : function () {
        mileageReqList.setDate = Calendar.datePickerRange('processDt', 'processDt');
        mileageReqList.setDate.setEndDate(0);
        mileageReqList.setDate.setStartDate(0);
        $("#processDt").datepicker("option", "minDate", $("#processDt").val());
    },
    bindingEvent : function () {
        $('#searchBtn').bindClick(mileageReqList.search);
        $('#searchResetBtn').bindClick(mileageReqList.resetSearchForm);
        $('#excelDownloadBtn').bindClick(mileageReqListGrid.excelDownload);
        $('#saveBtn').bindClick(mileageReqList.save);
        $('#resetRegBtn').bindClick(mileageReqList.resetRegForm);
        $('#searchMileageTypeBtn').bindClick(mileageReqList.searchMileageTypePop);

        $('#requestInfoBtn').bindClick(mileageReqList.setMileageReqInfo);
        $('#requestDtlBtn').bindClick(mileageReqList.setMileageReqDtl);
        $('#addSingle').bindClick(mileageDetailGrid.addRow);

        $('#addBulk').bindClick(mileageReqList.openFileSearch);
        $('#uploadFile').bindChange(mileageReqList.changeFileSearch);

        $('#deleteAll').bindClick(mileageDetailGrid.clearRow);
        $('#directlyYn').bindClick(mileageReqList.checkDirectExec);
        $('#userExcelDownloadBtn').bindClick(mileageDetailGrid.excelDownload);
    },
    search : function () {
        var schStartDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if (schEndDt.diff(schStartDt, "month", true) > 1) {
            alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
            $("#schStartDt").val(schEndDt.add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }
        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(schEndDt.format("YYYY-MM-DD"));
            return false;
        }

        var searchFormData = $("form[name=searchForm]").serialize();
        CommonAjax.basic(
            {
                url:'/mileage/payment/getMileageRequestList.json?'+ searchFormData,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    mileageReqListGrid.setData(res);
                    $('#mileageListTotalCount').html($.jUtil.comma(mileageReqListGrid.gridView.getItemCount()));
                }
            });
    },
    checkDirectExec: function () {
        if($("#directlyYn").is(":checked")) {
            $("#directlyYn").val("Y");
            $("#processDt").attr("disabled",true).attr("readonly",false);
        } else {
            $("#directlyYn").val("N");
            $("#processDt").removeAttr("disabled").removeAttr("readonly");
        }
    },
    // 파일 찾기 버튼 선택
    openFileSearch : function() {
        $('#uploadFile').trigger('click');
    },
    changeFileSearch : function() {
        var fileInfo = $('#uploadFile')[0].files[0];

        if (!fileInfo) {
            alert('업로드 대상 파일을 찾아주세요.');
            return false;
        }

        var fileExt = fileInfo.name.split(".")[1];

        if (fileExt.indexOf("xlsx") < 0 && fileExt.indexOf("xls") < 0) {
            alert("상품 일괄등록은 지정된 엑셀 양식(.xlsx .xls)으로 업로드해주세요.");
            $('#uploadFile').val('');
            return false;
        }

        $("#uploadFileNm").val($('#uploadFile')[0].files[0].name);

        console.log($("form[name=uploadExcelForm]"));
        $('#uploadExcelForm').ajaxForm({
            url: '/mileage/payment/uploadExcel.json',
            enctype: 'multipart/form-data',
            contentType: false,
            success: function(res) {
                alert(res.returnMsg);
                mileageDetailGrid.setData(res.resultList);
            },
            error: function() {
                alert('지정된 엑셀양식으로만 파일 업로드해주세요.');
            }
        }).submit();
    },
    checkValidGrid : function(_inputData){
        var check = true;
        var checkAmt = "mileageAmt";
        var alertMsg = "";

        $.each(_inputData, function (idx, data) {
            if($.jUtil.isEmpty(data)){
                if(checkAmt.indexOf(idx) > -1){
                    alertMsg = "금액을 입력하세요.";
                    check = false;
                }

                if(!check){
                    alert(alertMsg);
                    return check;
                }
            }
        });
        return check;
    },
    checkValid : function () {
        var processDate = moment($("#processDt").val(), 'YYYY-MM-DD', true);
        if(!processDate.isValid()){
            alert("실행일자가 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            $("#processDt").focus();
            return false;
        }

        if ($.jUtil.isEmpty($("#requestReason").val())) {
            alert("사유를 입력해주세요.");
            $("#requestReason").focus();
            return false;
        } else {
            if ($("#requestReason").val().length >= 20) {
                alert("사유는 20자내로 입력해주세요.");
                $("#requestReason").focus();
                return false;
            }
        }

        if ($.jUtil.isEmpty($("#displayMessage").val())) {
            alert("고객 노출 문구를 입력해주세요.");
            $("#displayMessage").focus();
            return false;
        } else {
            if ($("#displayMessage").val().length >= 10) {
                alert("고객 노출 문구는 10자내로 입력해주세요.");
                $("#displayMessage").focus();
                return false;
            }
        }

        if ($("#requestMessage").val().length >= 200) {
            alert("상세 사유는 200자내로 입력해주세요.");
            $("#requestMessage").focus();
            return false;
        }

        if ($.jUtil.isEmpty($("#mileageTypeNo").val())) {
            alert("마일리지 유형을 선택해주세요.");
            $("#mileageTypeNo").focus();
            return false;
        }

        var listCnt = mileageReqListGrid.gridView.getItemCount();
        if (listCnt == 0) {
            alert("대상 회원을 입력해주세요.");
            mileageReqList.setMileageReqDtl();
            return false;
        }
        return true;
    },
    /**
     * 등록/수정데이터 생성
     */
    setEditGridData : function (_dataProvider, _data) {
        var mileageReqDetailList = [];
        var returnData;
        var mileageReqNo = $("#mileageReqNo").val();

        var indexCompare = [RealGridJS.RowState.NONE];
        for(var idx in _data) {
            var state = _dataProvider.getRowState(idx);
            if(indexCompare.indexOf(state) < 0){
                _data[idx].useYn = "Y";
                if (mileageReqNo != "")
                    _data[idx].mileageReqNo = mileageReqNo;
                if([RealGridJS.RowState.UPDATED, RealGridJS.RowState.DELETED].indexOf(state) > -1) {
                    _data[idx].useYn = "N";
                    mileageReqDetailList.push(_data[idx]);
                }else if(RealGridJS.RowState.CREATED == state){
                    mileageReqDetailList.push(_data[idx]);
                }else if(RealGridJS.RowState.UPDATED == state){
                    _data[idx].state = RealGridJS.RowState.CREATED;
                    mileageReqDetailList.push(_data[idx]);
                }
            }
        }
        if ( mileageReqNo != "") {
            returnData = { "mileageReqNo": mileageReqNo,
                "mileageRequestDto" : {
                    "mileageTypeNo": $("#mileageTypeNo").val(),
                    "requestType": $("#requestType").val(),
                    "requestReason": $("#requestReason").val(),
                    "displayMessage": $("#displayMessage").val(),
                    "requestMessage": $("#requestMessage").val(),
                    "processDt": $("#processDt").val(),
                    "useYn": $("#useYn").val(),
                    "directlyYn": $("#directlyYn").val(),
                }, "mileageReqDetailList": mileageReqDetailList };
        } else {
            returnData = {
                "mileageRequestDto" : {
                    "mileageTypeNo": $("#mileageTypeNo").val(),
                    "requestType": $("#requestType").val(),
                    "requestReason": $("#requestReason").val(),
                    "displayMessage": $("#displayMessage").val(),
                    "requestMessage": $("#requestMessage").val(),
                    "processDt": $("#processDt").val(),
                    "useYn": $("#useYn").val(),
                    "directlyYn": $("#directlyYn").val(),
                }, "mileageReqDetailList": mileageReqDetailList };
        }
        return returnData;
    },
    save : function () {
        if(!mileageReqList.checkValidGrid()) return false;

        mileageDetailGrid.gridView.commit(true);

        //데이터 생성
        var _data = mileageReqList.setEditGridData(mileageDetailGrid.dataProvider, mileageDetailGrid.dataProvider.getJsonRows(0, -1));

        console.log(JSON.stringify(_data));
        var confirmMsg = ($("#mileageReqNo").val() == "")? "등록하시겠습니까?" : "수정하시겠습니까?";

        if(!confirm(confirmMsg)) return false;
        ($("#mileageReqNo").val() == "")? mileageReqList.add(_data) : mileageReqList.modify(_data);

        mileageReqList.resetRegForm();
        mileageReqList.search();
    },
    add : function (_data) {
        CommonAjax.basic(
            {
                url:'/mileage/payment/setMileageRequest.json',
                data: JSON.stringify(_data),
                contentType: 'application/json',
                method: "POST",
                successMsg: "등록하였습니다.",
            });
    },
    modify : function (_data) {
        CommonAjax.basic(
            {
                url:'/mileage/payment/modifyMileageRequest.json',
                data: JSON.stringify(_data),
                contentType: 'application/json',
                method: "POST",
                successMsg: "수정하였습니다.",
            });
    },
    setMileageReqInfo : function () {
        $("#requestInfoBtn").attr('class','ui button medium gray-dark font-malgun mg-r-5');
        $("#requestDtlBtn").attr('class','ui button medium white font-malgun mg-r-5');
        $("tr[name=requestInfo]").show();
        $("tr[name=requestDetail]").hide();
    },
    setMileageReqDtl : function () {
        $("#requestDtlBtn").attr('class','ui button medium gray-dark font-malgun mg-r-5');
        $("#requestInfoBtn").attr('class','ui button medium white font-malgun mg-r-5');
        $("tr[name=requestDetail]").show();
        $("tr[name=requestInfo]").hide();
    },
    resetRegForm : function () {
        $("#registerForm").each(function () {
            this.reset();
            mileageReqList.initProcessDate('0d');
            mileageReqList.checkDirectExec();
            mileageReqList.setMileageReqInfo();
        });
        $('#uploadFileNm, #uploadExcelForm input[name="uploadFile"]').val('');
        $("#mileageReqNo").val("");
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            mileageReqList.initSearchDate('0d');
            mileageReqList.resetRegForm();
        });
    },
    searchMileageTypePop : function () {
        var url = '/mileage/type/mileageTypeNamePop?mileageTypeNo=' + $("#mileageTypeNo").val() +"&mileageTypeName="+ $("#mileageTypeName").val();
        mileageReqList.windowOpen(url, 600, 430, 'mileageKindPop');
    },
    windowOpen  : function (url, _width, _height, _target) {
        // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
        var _left = ($(window).width()/2)-(_width/2);
        var _top = ($(window).height()/2)-(_height/2);
        if(_target == 'undefined' || _target == null || _target == '') {
            _target ='popup-search';
        }
        void(window.open(url, _target, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+ _width +', height='+ _height +', left=' + _left + ', top='+ _top ));
    },
};