/***
 * 마일리지 조회 리스트 그리드 설정
 * @type
 */
var mileageListGrid = {
    gridView : new RealGridJS.GridView("mileageListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        mileageListGrid.initGrid();
        mileageListGrid.initDataProvider();
        mileageListGrid.event();
    },
    initGrid : function () {
        mileageListGrid.gridView.setDataSource(mileageListGrid.dataProvider);

        mileageListGrid.gridView.setStyles(mileageListGridInfo.realgrid.styles);
        mileageListGrid.gridView.setDisplayOptions(mileageListGridInfo.realgrid.displayOptions);
        mileageListGrid.gridView.setColumns(mileageListGridInfo.realgrid.columns);
        mileageListGrid.gridView.setOptions(mileageListGridInfo.realgrid.options);
        mileageListGrid.gridView.setCheckBar({ visible: false });
        mileageListGrid.gridView.setSelectOptions({
            style: 'rows'
        });

    },
    initDataProvider : function() {
        mileageListGrid.dataProvider.setFields(mileageListGridInfo.dataProvider.fields);
        mileageListGrid.dataProvider.setOptions(mileageListGridInfo.dataProvider.options);
    },
    event : function() {

    },
    setData : function(dataList) {
        mileageListGrid.dataProvider.clearRows();
        mileageListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(mileageListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "마일리지내역_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        mileageListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};

var mileageList = {
    init : function () {
        this.initSearchDate('0d');
        $("#mileageKind").val("A");
        $("#userNo").val("");
    },
    initSearchDate : function (flag) {
        mileageList.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        mileageList.setDate.setEndDate(0);
        mileageList.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(mileageList.search);
        $('#searchResetBtn').bindClick(mileageList.resetSearchForm);
        $('#excelDownloadBtn').bindClick(mileageListGrid.excelDownload);
    },
    checkValid : function() {
        var schStartDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if (schEndDt.diff(schStartDt, "month", true) > 1) {
            alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
            $("#schStartDt").val(schEndDt.add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }
        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(schEndDt.format("YYYY-MM-DD"));
            return false;
        }
        return true;
    },
    search : function () {
        if(!mileageList.checkValid()) {
            return false;
        }
        //var mileCategoryList = new Array();
        //$("input[name='mileageCategory']:checked").each(function(){
        //    mileCategoryList.push($(this).val());
        //});

        var searchFormData = $("form[name=searchForm]").serialize();
        CommonAjax.basic(
            {
                url:'/mileage/history/getMileageList.json?' + searchFormData,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    mileageListGrid.setData(res);
                    $('#mileageListTotalCount').html($.jUtil.comma(mileageListGrid.gridView.getItemCount()));
                }
            });
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            mileageList.init();
        });
    },
};