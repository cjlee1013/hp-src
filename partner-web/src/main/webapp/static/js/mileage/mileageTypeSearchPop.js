/***
 * 마일리지 유형 조회 팝업 그리드
 * @type
 */

var mileageTypePopGrid = {
    gridView : new RealGridJS.GridView("mileageListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        mileageTypePopGrid.initGrid();
        mileageTypePopGrid.initDataProvider();
        mileageTypePopGrid.event();
    },
    initGrid : function () {
        mileageTypePopGrid.gridView.setDataSource(mileageTypePopGrid.dataProvider);

        mileageTypePopGrid.gridView.setStyles(mileageListGridInfo.realgrid.styles);
        mileageTypePopGrid.gridView.setDisplayOptions(mileageListGridInfo.realgrid.displayOptions);
        mileageTypePopGrid.gridView.setColumns(mileageListGridInfo.realgrid.columns);
        mileageTypePopGrid.gridView.setOptions(mileageListGridInfo.realgrid.options);
        mileageTypePopGrid.gridView.setCheckBar({ visible: false });
        mileageTypePopGrid.gridView.setSelectOptions({
            style: 'rows'
        });
    },
    initDataProvider : function() {
        mileageTypePopGrid.dataProvider.setFields(mileageListGridInfo.dataProvider.fields);
        mileageTypePopGrid.dataProvider.setOptions(mileageListGridInfo.dataProvider.options);
    },
    setData : function(dataList) {
        mileageTypePopGrid.dataProvider.clearRows();
        mileageTypePopGrid.dataProvider.setRows(dataList);
    },
    event : function() {
        // 그리드 선택
        mileageTypePopGrid.gridView.onDataCellClicked = function(gridView, index) {
            mileageTypePopGrid.selectRow(index.dataRow);
        };
    },
    selectRow: function (selectRowId) {
        var rowDataJson = mileageTypePopGrid.dataProvider.getJsonRow(selectRowId);
        $('#mileageTypeNo').val(rowDataJson.mileageTypeNo);
        $('#mileageTypeName').val(rowDataJson.mileageTypeName);
    },
};

var mileageTypePop = {
    init : function () {
        mileageTypePop.search();
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(mileageTypePop.search);
        $('#searchResetBtn').bindClick(mileageTypePop.resetSearchForm);
        $('#selectBtn').bindClick(mileageTypePop.select);
    },
    search : function () {
        var searchFormData = $("#mileageTypeName").val();

        CommonAjax.basic({
            url: "/mileage/type/getMileageTypeList.json?storeType=A&useYn=Y&mileageKind=A&schDetailCode=MILEAGE_TYPE_NAME&schDetailDesc="+ searchFormData,
            contentType: 'application/json',
            data: null,
            method: "GET",
            callbackFunc:function(res) {
                mileageTypePopGrid.setData(res);
            }
        });
    },
    select : function () {
        $('#mileageTypeName', opener.document).val($('#mileageTypeName').val());
        $('#mileageTypeNo', opener.document).val($('#mileageTypeNo').val());
        self.close();
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
        });
    },
}