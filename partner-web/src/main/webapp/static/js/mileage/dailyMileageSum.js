/***
 * 마일리지 통계 그리드 설정
 */
var dailyMileageSumGrid = {
    gridView : new RealGridJS.GridView("mileageListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dailyMileageSumGrid.initGrid();
        dailyMileageSumGrid.initDataProvider();
        dailyMileageSumGrid.event();
    },
    initGrid : function () {
        dailyMileageSumGrid.gridView.setDataSource(dailyMileageSumGrid.dataProvider);
        dailyMileageSumGrid.gridView.setStyles(mileageListGridInfo.realgrid.styles);
        dailyMileageSumGrid.gridView.setDisplayOptions(mileageListGridInfo.realgrid.displayOptions);
        dailyMileageSumGrid.gridView.setOptions(mileageListGridInfo.realgrid.options);

        var styleOption = {
            textAlignment: "far",
            numberFormat : "#,##0"
        };

        var headerOption = {
            background : "#E0E0E0"
        };

        var groupHeader = [
            {
                name: "basicDt",
                fieldName: "basicDt",
                width: 70,
                header: {
                    text: "일자"
                }
            },
            {
                name: "yesterdayAmt",
                fieldName: "yesterdayAmt",
                header: {
                    text: "전일잔액"
                }
            },
            {
                type: "group",
                name: "적립 (+)",
                width: 160,
                styles: styleOption,
                "columns": [
                    {
                        type: "group",
                        name: "saveAmt",
                        fieldName: "saveAmt",
                        styles: styleOption,
                        header: {
                            text: "적립",
                            styles: headerOption
                        }
                    },
                    {
                        type: "group",
                        name: "cancelAmt",
                        fieldName: "cancelAmt",
                        styles: styleOption,
                        header: {
                            text: "사용취소",
                            styles: headerOption
                        }
                    }
                ]
            },
            {
                name: "useAmt",
                fieldName: "useAmt",
                header: {
                    text: "사용"
                }
            },
            {
                type: "group",
                name: "차감 (-)",
                width: 160,
                "columns": [
                    {
                        type: "group",
                        name: "expiredAmt",
                        fieldName: "expiredAmt",
                        styles: styleOption,
                        header: {
                            text: "유효기간만료",
                            styles: headerOption
                        }
                    },
                    {
                        type: "group",
                        name: "withdrawAmt",
                        fieldName: "withdrawAmt",
                        styles: styleOption,
                        header: {
                            text: "회수",
                            styles: headerOption
                        }
                    }
                ]
            },
            {
                name: "remainAmt",
                fieldName: "remainAmt",
                header: {
                    text: "잔액",
                    styles: headerOption
                },
                styles: styleOption
            },
            {
                name: "totalAmt",
                fieldName: "totalAmt",
                header: {
                    text: "당일 기말 합계",
                    styles: headerOption
                },
                styles: styleOption
            },
        ]
        dailyMileageSumGrid.gridView.setColumns(groupHeader);
    },
    initDataProvider : function() {
        dailyMileageSumGrid.dataProvider.setFields(mileageListGridInfo.dataProvider.fields);
        dailyMileageSumGrid.dataProvider.setOptions(mileageListGridInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dailyMileageSumGrid.gridView.onDataCellClicked = function(gridView, index) {
            dailyMileageSumGrid.selectRow(index.dataRow)
        };
    },
    setData : function(dataList) {
        dailyMileageSumGrid.dataProvider.clearRows();
        dailyMileageSumGrid.dataProvider.setRows(dataList);
    },
    selectRow: function (selectRowId) {
        var rowDataJson = dailyMileageSumGrid.dataProvider.getJsonRow(selectRowId);

        CommonAjax.basic(
            {
                url:'/mileage/stat/getDailyMileageDetail.json?mileageStatSeq='+ rowDataJson.mileageStatSeq,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    dailyMileageDetailGrid.setData(res);
                }
            });
    },
    excelDownload: function() {
        if(dailyMileageSumGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "마일리지통계_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        dailyMileageSumGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
/***
 * 마일리지 타입별 통계 그리드 설정
 */
var dailyMileageDetailGrid = {
    gridView : new RealGridJS.GridView("mileageDetailGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dailyMileageDetailGrid.initGrid();
        dailyMileageDetailGrid.initDataProvider();
    },
    initGrid : function () {
        dailyMileageDetailGrid.gridView.setDataSource(dailyMileageDetailGrid.dataProvider);
        dailyMileageDetailGrid.gridView.setStyles(mileageDetailGridInfo.realgrid.styles);
        dailyMileageDetailGrid.gridView.setDisplayOptions(mileageDetailGridInfo.realgrid.displayOptions);
        dailyMileageDetailGrid.gridView.setOptions(mileageDetailGridInfo.realgrid.options);

        var styleOption = {
            textAlignment: "far",
            numberFormat : "#,##0"
        };

        var headerOption = {
            background : "#E0E0E0"
        };

        var groupHeader = [
            {
                name: "basicDt",
                fieldName: "basicDt",
                width: 70,
                header: {
                    text: "일자"
                }
            },
            {
                name: "mileageNo",
                fieldName: "mileageNo",
                width: 80,
                header: {
                    text: "마일리지 번호"
                }
            },
            {
                name: "mileageKind",
                fieldName: "mileageKind",
                width: 140,
                header: {
                    text: "마일리지 유형"
                }
            },
            {
                name: "mileageTypeName",
                fieldName: "mileageTypeName",
                header: {
                    text: "마일리지 종류"
                }
            },
            {
                type: "group",
                name: "적립 (+)",
                width: 140,
                styles: styleOption,
                "columns": [
                    {
                        type: "group",
                        name: "saveAmt",
                        fieldName: "saveAmt",
                        styles: styleOption,
                        header: {
                            text: "적립",
                            styles: headerOption
                        }
                    },
                    {
                        type: "group",
                        name: "cancelAmt",
                        fieldName: "cancelAmt",
                        styles: styleOption,
                        header: {
                            text: "사용취소",
                            styles: headerOption
                        }
                    }
                ]
            },
            {
                name: "useAmt",
                fieldName: "useAmt",
                width: 70,
                header: {
                    text: "사용"
                }
            },
            {
                type: "group",
                name: "차감 (-)",
                width: 140,
                "columns": [
                    {
                        type: "group",
                        name: "expiredAmt",
                        fieldName: "expiredAmt",
                        styles: styleOption,
                        header: {
                            text: "유효기간만료",
                            styles: headerOption
                        }
                    },
                    {
                        type: "group",
                        name: "withdrawAmt",
                        fieldName: "withdrawAmt",
                        styles: styleOption,
                        header: {
                            text: "회수",
                            styles: headerOption
                        }
                    }
                ]
            },
            {
                name: "remainAmt",
                fieldName: "remainAmt",
                width: 70,
                header: {
                    text: "잔액",
                    styles: headerOption
                },
                styles: styleOption
            },
        ]
        dailyMileageDetailGrid.gridView.setColumns(groupHeader);
    },
    initDataProvider : function() {
        dailyMileageDetailGrid.dataProvider.setFields(mileageDetailGridInfo.dataProvider.fields);
        dailyMileageDetailGrid.dataProvider.setOptions(mileageDetailGridInfo.dataProvider.options);
    },
    setData : function(dataList) {
        dailyMileageDetailGrid.dataProvider.clearRows();
        dailyMileageDetailGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(dailyMileageDetailGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "마일리지타입별통계_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        dailyMileageDetailGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};

var dailyMileageSum = {
    init : function () {
        this.initSearchDate('0d');
    },
    initSearchDate : function (flag) {
        dailyMileageSum.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        dailyMileageSum.setDate.setEndDate(0);
        dailyMileageSum.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    bindingEvent : function() {
        //$('#searchBtn').bindClick(dailyMileageSum.search);
        $('#searchResetBtn').bindClick(dailyMileageSum.resetSearchForm);
        $('#excelDownloadBtn').bindClick(dailyMileageSumGrid.excelDownload);
        $('#detailExcelDownloadBtn').bindClick(dailyMileageDetailGrid.excelDownload);
    },
    search : function () {
        var schStartDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if (schEndDt.diff(schStartDt, "month", true) > 1) {
            alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
            $("#schStartDt").val(schEndDt.add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }
        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(schEndDt.format("YYYY-MM-DD"));
            return false;
        }

        CommonAjax.basic(
            {
                url:'/mileage/stat/getDailyMileageSummary.json',
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    dailyMileageSumGrid.setData(res);
                    $("#mileageStatSeq").val(res.mileageStatSeq);
                }
            });

    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            dailyMileageSum.init();
        });
    }
};