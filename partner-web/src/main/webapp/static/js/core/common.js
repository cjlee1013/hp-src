/**
 * 공용 기본 스크립트 파일.
 */

/**
 * 우편번호 검색 팝업 호출 공통 함수.
 * @param callBackFuncName -> 팝업에서 호출할 부모 창의 스크립트를 문자열로 입력한다.
 * 정상적으로 팝업이 열린 경우 팝업의 object 를 반환.
 * callBackFuncName 에 하나의 object 로 결과가 넘어옴.
 * return value
 * {
        'zipCode': "우편번호",
        'gibunAddr': "지번주소(기본주소)",
        'roadAddr': "도로명주소",
        'gibunRep': "관련지번"
        'islandType : "도서산간여부(ALL:전국, JEJU:제주, ISLAND:산간)"
    }
 */
function zipCodePopup(callBackFuncName) {
    if (typeof callBackFuncName == 'undefined' || callBackFuncName == "") {
        alert('callBackFuncName 이 필요합니다.');
        return;
    }
    if (typeof callBackFuncName != 'string') {
        alert('callBackFuncName 은 문자열로 입력 가능합니다.');
        return;
    }
    let url = "/common/zipCodePop?callBack=" + callBackFuncName;
    return windowPopupOpen(url, callBackFuncName, 600, 780);
}

/**
 * 제주/도서산간지역 확인 팝업 호출 공통 함수.
 */
function checkIslandPop() {
    let url = "/common/checkIslandPop";
    return windowPopupOpen(url, "checkIslandPop", 600, 780);
}

/**
 * 강제로 팝업을 열기 위한 함수.
 * url - 팝업의 url
 * target - 이미 열려 있는 팝업일 경우 재사용.
 * w - width
 * h - height
 * s - scrollbars
 * r - resizeble
 */
function windowPopupOpen(url, target, w, h, s, r) {
    if (s) s = 'yes';
    else s = 'no';
    if (r) r = 'yes';
    else r = 'no';
    var newwin = window.open(url, target, 'width=' + w + ',height=' + h + ',top=0,left=0,status=no,scrollbars=' + s + ',resizable=' + r);
    if (newwin == null) {
        alert("팝업 차단기능 혹은 팝업차단 프로그램이 동작중입니다. 팝업 차단 기능을 해제한 후 다시 시도하세요.");
    } else {
        newwin.location.replace(get_full_url(url));
        return newwin;
    }
}

/**
 * full url로 변경하는 function
 *
 * 팝업 오픈 시 firefox에서 location.replace 또는 location.href 사용시
 * NS_ERROR_MALFORMED_URI 에러가 발생되는 것을 해결하기 위해 사용
 *
 * @param url_path url
 * @returns {string} full url
 */
function get_full_url(url_path)
{
    var loc = window.location;
    var url = "" + loc.protocol + "//" + loc.host + url_path;
    return url;
}
