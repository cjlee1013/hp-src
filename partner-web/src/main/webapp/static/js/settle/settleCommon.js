/**
 * 정산 공통 js
 */

var SettleCommon = {
    /** padding */
    padding : function(n, width) {
        n = n + '';
        if (n.length < 2) {
            return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
        } else {
            return n;
        }
    },
    /** 입력조건 생성 */
    getInputTypeSetter : function(_id, _showList){
        var selectBoxStr = '<option selected value="">입력조건 선택</option>';
        //조건값
        var strHtml = '';
        //리스트만들기
        if(_showList.length > 0){
            //콤마로 Parsing
            var _parameterList = _showList.split(',');

            for(var idx in _parameterList){
                strHtml = SettleCommon.selectList(_parameterList[idx]);
                if(strHtml != ''){
                    selectBoxStr += '<option value="' + _parameterList[idx] + '" >' + strHtml + '</option>';
                }
            }
        }
        $('#'+_id).html(selectBoxStr);
        $('#'+_id).trigger('change');
    },
    /** 입력조건 생성 -> 추후 DB화로 변경시 변환을 염려해두고 처리 */
    selectList : function(parameter) {
        var returnStr = '';
        if(parameter == 'PARTNER_ID'){
            returnStr = '판매업체ID';
        }else if(parameter == 'PARTNER_NM'){
            returnStr = '판매업체명';
        }else if(parameter == 'PARTNER_NO'){
            returnStr = '사업자번호';
        }else if(parameter == 'PARTNER_OWNER'){
            returnStr = '대표자';
        }else if(parameter == 'BANK_ACCOUNT_HOLDER'){
            returnStr = '예금주';
        }else if(parameter == 'SETTLE_PRE_DT'){
            returnStr = '지급예정일';
        }else if(parameter == 'REG_ID'){
            returnStr = '등록자';
        }else if(parameter == 'UPD_ID'){
            returnStr = '수정자';
        }else if(parameter == 'REG_NAME'){
            returnStr = '처리자명';
        }else if(parameter == 'DEPT_CD'){
            returnStr = '사업부';
        }else if(parameter == 'SETTLE_SRL'){
            returnStr = '지급SRL';
        }else if(parameter == 'ITEM_NO'){
            returnStr = '상품번호';
        }else if(parameter == 'ITEM_NM'){
            returnStr = '상품명';
        }else if(parameter == 'SUPPLIER'){
            returnStr = '업체코드';
        }else if(parameter == 'MNG_NM'){
            returnStr = '수취담당자명';
        }else if(parameter == 'MNG_EMAIL'){
            returnStr = '담당자이메일';
        }
        return returnStr;
    },
    setLinkTab : function (_tabId, _tabName, _url, _tabSize) {
        var subTabber = parent.Tabbar;

        if (_tabSize == null) {
            _tabSize = 150;
        }

        if (subTabber.getAllTabs().indexOf(_tabId) != -1) {  //탭이 이미 있다면 데이터 갱신 후 활성화
            subTabber.tabs(_tabId).attachURL(_url);
            subTabber.tabs(_tabId).setActive();
        } else {
            subTabber.addTab(_tabId, _tabName, _tabSize, null, true, true);
            subTabber.tabs(_tabId).attachURL(_url);
            subTabber.enableAutoReSize();
        }
    },
    /* 리얼그리드에서 사용하기 위한 func - 컬럼 hide/show */
    setRealGridColumnVisible : function (_gridView, _columnList, _visible) {
        for(var idx = 0; idx < _columnList.length; idx++){
            _gridView.setColumnProperty(_gridView.columnByName(_columnList[idx]), "visible", _visible);
        }
    },
    setRealGridColumnLink : function(_gridView, _columnId, _url, _linkView){
        _gridView.setColumnProperty(_columnId, "renderer", {
            type : "link",
            url : _url,
            requiredFields : _columnId,
            showUrl : _linkView
        });
    },
    getRealGridColumnName : function (_gridView, _findText) {
        //컬럼의 명으로 name을 찾는다.
        var columnName = "";
        $.each(_gridView.getColumns(), function (idx, data) {
            if(data.type == "group"){
                $.each(data.columns, function (_gIdx, _dData) {
                    if(_gridView.getColumnProperty(_dData, "header").text == _findText){
                        columnName = _dData.name;
                        return false;
                    }
                });
            }else{
                if(_gridView.getColumnProperty(data, "header").text == _findText){
                    columnName = data.name;
                    return false;
                }
            }
        });
        return columnName;
    },
    setRealGridCellButton : function (_gridView, _buttonInfo, _columnName) {
        //버튼을 언제나 보이도록 설정.
        _gridView.setColumnProperty(_columnName, "buttonVisibility", "always");
        //버튼을 사용가능하도록 컬럼 정보를 변경
        _gridView.setColumnProperty(_columnName, "button", "image");

        //버튼 renderer생성정보
        var buttonImage = {};
        //버튼에 대한 다이나믹스타일 정보
        var buttonDynamicStyleInfo = {};
        buttonDynamicStyleInfo.criteria = [];
        buttonDynamicStyleInfo.styles = [];

        //버튼 생성 정보가 버튼 조합 수 보다 적은 경우 오류
        if(_buttonInfo.buttonCnt != _buttonInfo.buttonCombine.length){
            alert("버튼생성이 잘못되었습니다. 관리자에게 문의하세요.");
            return false;
        }

        //버튼 생성 수 만큼 생성한다.
        for(var i = 0; i < _buttonInfo.buttonCnt; i++){
            //버튼ID
            buttonImage.id = (_buttonInfo.buttonName + (i+1));
            //버튼타입
            buttonImage.type = "imageButtons";
            //버튼 이미지 Gap
            buttonImage.imageGap = _buttonInfo.buttonGap;
            //버튼 정렬
            buttonImage.alignment = _buttonInfo.cellTextAlignment != null ? _buttonInfo.cellTextAlignment : "center";
            //버튼 마진정보
            buttonImage.margin = _buttonInfo.buttonMargin;
            //버튼 이미지 정보
            buttonImage.images = [];
            //버튼 조합만큼 이미지 정보를 생성.
            $.each(_buttonInfo.buttonCombine[i].split(","), function (combineIndex, combineData) {
                //버튼 이미지 정보와 조합정보를 비교하여 맞는 이미지번튼을 생성하도록 한다.
                $.each(_buttonInfo.buttonImg, function (imgIndex, imgData) {
                    if(combineData == imgData.name){
                        var img = {};
                        img.name = imgData.name;
                        img.up = imgData.img;
                        img.hover = imgData.img.split(".").join("_hover.");
                        img.down = imgData.img.split(".").join("_active.");
                        img.width = imgData.width;
                        img.cursor = "pointer";
                        buttonImage.images.push(img);
                    }
                });
            });
            //생성된 이미지버튼을 Renderer에 등록한다.
            _gridView.addCellRenderers(buttonImage);
            buttonDynamicStyleInfo.criteria.push("value" + (_buttonInfo.isValueNotOption ? "!=" : "=") + "'" + _buttonInfo.value[i] + "'");
            buttonDynamicStyleInfo.styles.push("renderer=" + buttonImage.id);
            //버튼 정보 초기화.
            buttonImage = {};
        }
        //버튼에 대한 다이나믹스타일 적용
        _gridView.setColumnProperty(_columnName, "dynamicStyles", [buttonDynamicStyleInfo]);
    },
    setMonth : function(_year,_id) {
        var nowDate = new Date();
        var maxMonth;
        if (_year.value == nowDate.getFullYear()) {
            maxMonth = nowDate.getMonth();
        } else {
            maxMonth = 11;
        }
        $('#'+_id).empty();
        for (var i=1;i<=maxMonth+1;i++) {
            if (i<10) {
                $('#'+_id).append("<option value='0" +i+ "'>0" + i + " 월</option>");
            } else  {
                $('#'+_id).append("<option value='" +i+ "'>" + i + " 월</option>");
            }
        }
    }
}