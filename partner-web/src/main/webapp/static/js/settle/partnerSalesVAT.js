/***
 * 부가세신고내역 그리드 설정
 */

$(document).ready(function() {
    partnerSalesVATForm.init();
    partnerSalesVATForm.bindingEvent();
    partnerSalesVATGrid.init();
    partnerSalesOrderGrid.init();
    CommonAjaxBlockUI.global();
    adjustListGrid.init();
});

var partnerSalesVATGrid = {
    gridView : new RealGridJS.GridView("partnerSalesVATGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        partnerSalesVATGrid.initGrid();
        partnerSalesVATGrid.initDataProvider();
        partnerSalesVATGrid.event();
    },
    initGrid : function () {
        partnerSalesVATGrid.gridView.setDataSource(partnerSalesVATGrid.dataProvider);
        partnerSalesVATGrid.gridView.setStyles(salesVATListBaseInfo.realgrid.styles);
        partnerSalesVATGrid.gridView.setDisplayOptions(salesVATListBaseInfo.realgrid.displayOptions);
        partnerSalesVATGrid.gridView.setOptions(salesVATListBaseInfo.realgrid.options);
        partnerSalesVATGrid.gridView.setColumns(salesVATListBaseInfo.realgrid.columns);

        //컬럼 그룹핑 너비 조정
        partnerSalesVATGrid.gridView.setColumnProperty("과세", "displayWidth", 700);
        partnerSalesVATGrid.gridView.setColumnProperty("면세", "displayWidth", 700);
    },
    initDataProvider : function() {
        partnerSalesVATGrid.dataProvider.setFields(salesVATListBaseInfo.dataProvider.fields);
        partnerSalesVATGrid.dataProvider.setOptions(salesVATListBaseInfo.dataProvider.options);
    },
    event : function() {
        //이미지 버튼 클릭시 Action
        partnerSalesVATGrid.gridView.onImageButtonClicked = function (grid, itemIndex, column, buttonIndex, name) {
            var basicDt = partnerSalesVATGrid.dataProvider.getJsonRow(grid.getCurrent().dataRow).basicDt;
            var startDt = basicDt + '-01';
            var endDt = moment(startDt, 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
            startDt = moment(startDt, 'YYYY-MM-DD', true).format("YYYYMMDD");

            var url = "/settle/getSalesVATOrder.json?startDt="+ startDt +"&endDt="+ endDt;

            CommonAjax.basic({
                url: url,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    partnerSalesOrderGrid.setData(res);

                    CommonAjax.basic({
                        url:'/settle/getSalesAdjustList.json?startDt='+ startDt +'&endDt='+ endDt,
                        data: null,
                        contentType: 'application/json',
                        method: "GET",
                        successMsg: null,
                        callbackFunc:function(res) {
                            adjustListGrid.setData(res);
                            partnerSalesOrderGrid.excelDownload();
                        }
                    });
                }
            });
        };
    },
    setData : function(dataList) {
        partnerSalesVATGrid.dataProvider.clearRows();
        partnerSalesVATGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(partnerSalesVATGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        SettleCommon.setRealGridColumnVisible(partnerSalesVATGrid.gridView, [SettleCommon.getRealGridColumnName(partnerSalesVATGrid.gridView, "상세내역")], false);
        var fileName = partnerSalesVATForm.setExcelFileName("부가세신고내역");
        partnerSalesVATGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            allColumns : false,
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });
        SettleCommon.setRealGridColumnVisible(partnerSalesVATGrid.gridView, [SettleCommon.getRealGridColumnName(partnerSalesVATGrid.gridView, "상세내역")], true);

    }
};
/***
 * 부가세신고내역 상세 그리드
 */
var partnerSalesOrderGrid = {
    gridView : new RealGridJS.GridView("partnerSalesOrderGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        partnerSalesOrderGrid.initGrid();
        partnerSalesOrderGrid.initDataProvider();
    },
    initGrid : function () {
        partnerSalesOrderGrid.gridView.setDataSource(partnerSalesOrderGrid.dataProvider);
        partnerSalesOrderGrid.gridView.setStyles(salesVATOrderBaseInfo.realgrid.styles);
        partnerSalesOrderGrid.gridView.setDisplayOptions(salesVATOrderBaseInfo.realgrid.displayOptions);
        partnerSalesOrderGrid.gridView.setOptions(salesVATOrderBaseInfo.realgrid.options);
        partnerSalesOrderGrid.gridView.setColumns(salesVATOrderBaseInfo.realgrid.columns);

        //컬럼 그룹핑 너비 조정
        partnerSalesOrderGrid.gridView.setColumnProperty("결제 금액", "displayWidth", 600);
    },
    initDataProvider : function() {
        partnerSalesOrderGrid.dataProvider.setFields(salesVATOrderBaseInfo.dataProvider.fields);
        partnerSalesOrderGrid.dataProvider.setOptions(salesVATOrderBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        partnerSalesOrderGrid.dataProvider.clearRows();
        partnerSalesOrderGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(partnerSalesOrderGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = partnerSalesVATForm.setExcelFileName("부가세신고내역_상세내역");
        RealGridJS.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다.",
            exportGrids:[
                { grid: partnerSalesOrderGrid.gridView, sheetName: "주문상세내역" },
                { grid: adjustListGrid.gridView, sheetName: "조정상세내역" }
            ]
        });

    }
};
/* 정산조정 상세 그리드 */
var adjustListGrid = {
    gridView : new RealGridJS.GridView("adjustListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        adjustListGrid.initGrid();
        adjustListGrid.initDataProvider();
    },
    initGrid : function() {
        adjustListGrid.gridView.setDataSource(adjustListGrid.dataProvider);

        adjustListGrid.gridView.setStyles(adjustListBaseInfo.realgrid.styles);
        adjustListGrid.gridView.setDisplayOptions(adjustListBaseInfo.realgrid.displayOptions);
        adjustListGrid.gridView.setColumns(adjustListBaseInfo.realgrid.columns);
        adjustListGrid.gridView.setOptions(adjustListBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        adjustListGrid.dataProvider.setFields(adjustListBaseInfo.dataProvider.fields);
        adjustListGrid.dataProvider.setOptions(adjustListBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        adjustListGrid.dataProvider.clearRows();
        adjustListGrid.dataProvider.setRows(dataList);
    },
};
/***
 * 부가세신고내역 폼
 */
var partnerSalesVATForm = {
    init : function () {
        this.initSearchYM();
        $('#searchField').val('');
    },
    initSearchYM : function () {
        var today = new Date();
        var year = moment(today, 'YYYY-MM-DD', true).add(-1, "month").format("YYYY");
        var month = moment(today, 'YYYY-MM-DD', true).add(-1, "month").format("MM");

        $('#startYear, #endYear').val(year);
        SettleCommon.setMonth(year,'startMonth');
        SettleCommon.setMonth(year,'endMonth');
        $('#startMonth, #endMonth').val(month);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(partnerSalesVATForm.search);
        $('#resetBtn').bindClick(partnerSalesVATForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(partnerSalesVATGrid.excelDownload);

        $('#pageViewCnt').change(function () {
            sellUtil.onRealGridPaging(partnerSalesVATGrid.realGrid, "pagingArea", $('#pageViewCnt').val(), 10);
        });
    },
    valid : function () {
        var startDt = $("#startYear").val() + '-' + $("#startMonth").val() + '-01';
        var endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
        startDt = moment(startDt, 'YYYY-MM-DD', true);
        endDt = moment(endDt, 'YYYY-MM-DD', true).add(1, "day");

        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            var year = $("#startYear").val();
            var month = $("#startMonth").val();
            $("#startYear").val($("#endYear").val());
            $("#startMonth").val($("#endMonth").val());
            $("#endYear").val(year);
            SettleCommon.setMonth(year,'endMonth');
            $("#endMonth").val(month);
            return false;
        }
        if (endDt.diff(startDt, "month", true) > 3) {
            alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
            var year = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("YYYY");
            var month = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("MM");
            $("#endYear").val(year);
            SettleCommon.setMonth(year,'endMonth');
            $("#endMonth").val(month);
            return false;
        }

        return true;
    },
    search : function () {
        if(!partnerSalesVATForm.valid()) return false;

        //주의! 버튼 생성시 동일이름으로 버튼 생성시에 주의 요망.
        //만들때마다 초기화되므로 위에서 생성한 버튼아이디값이 초기화 될 수 있음.
        var buttonInfo = {
            buttonCnt : 1,
            buttonName : "excelDown",
            buttonImg : [ {name : "orderDetail", img : "/static/images/settle/btn_excel.png", width:70} ],
            buttonGap : 0,
            buttonMargin : 0,
            buttonCombine : ["orderDetail"],
            value : [""],
            isValueNotOption : true
        };

        var searchForm = $("form[name=searchForm]").serialize();
        var startDt = $("#startYear").val() + $("#startMonth").val() + '01';
        var endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
        endDt = moment(endDt, 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        CommonAjax.basic(
        {
            url: '/settle/getSalesVATList.json?' + searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                partnerSalesVATGrid.setData(res);
                $('#searchCnt').html($.jUtil.comma(partnerSalesVATGrid.gridView.getItemCount()));

                //페이징 처리
                sellUtil.onRealGridPaging(partnerSalesVATGrid.gridView, "pagingArea", $('#pageViewCnt').val(), 100);

                //주문상세 엑셀다운로드
                var targetColumn = SettleCommon.getRealGridColumnName(partnerSalesVATGrid.gridView, "상세내역");
                $.each(res, function(_rowId, _data){
                    partnerSalesVATGrid.dataProvider.setValue(_rowId, targetColumn, "orderDetail");
                });

                //건수가 0건이 아닌 경우에만 수행
                if(partnerSalesVATGrid.gridView.getItemCount() > 0) {
                    //버튼생성
                    SettleCommon.setRealGridCellButton(partnerSalesVATGrid.gridView, buttonInfo, targetColumn);
                }
            }
        });
    },
    setExcelFileName : function (fileName) {
        var excelFileName = fileName +'_'+ $("#startYear").val() + $("#startMonth").val()+'_'+ $("#endYear").val() + $("#endMonth").val();
        return excelFileName;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function() {
            this.reset();
            partnerSalesVATForm.init();
        });
    }
};