/**
 * 통계 > 판매트렌드
 */
$(document).ready(function () {
    salesTrendMain.init();
    CommonAjaxBlockUI.global();
});

const salesTrendMain = {
    week: ['일', '월', '화', '수', '목', '금', '토'],
    getBracketDate: function (d) {
        return '(' + salesTrendMain.week[new Date(d).getDay()] + ')';
    },
    // 디자인시안 단독 차트 시작
    lineChartCtx: $('#lineChart'),
    eventChartCtx: $('#eventChart'),
    paymentChartCtx: $('#paymentChart'),
    agePieChartCtx: $('#agePieChart'),
    genderPieChartCtx: $('#genderPieChart'),
    // 디자인시안 단독 차트 종료
    chartColors: {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(201, 203, 207)'
    },
    lineChartConfig: {
        type: 'line',
        data: null,
        options: {
            responsive: true,
            interaction: {
                mode: 'index'
            },
            stacked: false,
            plugins: {
                title: {
                    display: true,
                    text: '상품 조회 및 결제건 수 추세'
                }
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        drawOnChartArea: false
                    }
                }],
                yAxes: [{
                    // 좌측 event
                    type: 'linear',
                    display: true,
                    position: 'left',
                    id: 'eventY',
                    gridLines: {
                        drawOnChartArea: false
                    }
                }, {
                    // 우측 payment
                    type: 'linear',
                    display: true,
                    position: 'right',
                    id: 'paymentY',
                    gridLines: {
                        drawOnChartArea: false
                    }
                }],
            }
        }
    },
    eventChartConfig: {
        type: 'line',
        data: null,
        options: {
            responsive: true,
            interaction: {
                mode: 'index'
            },
            stacked: false,
            plugins: {
                title: {
                    display: true,
                    text: '상품 조회 수 추세'
                }
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        drawOnChartArea: false
                    }
                }],
                yAxes: [{
                    // 좌측 event
                    type: 'linear',
                    display: true,
                    position: 'left',
                    id: 'eventY',
                    gridLines: {
                        drawOnChartArea: false
                    }
                }],
            }
        }
    },
    paymentChartConfig: {
        type: 'line',
        data: null,
        options: {
            responsive: true,
            interaction: {
                mode: 'index'
            },
            stacked: false,
            plugins: {
                title: {
                    display: true,
                    text: '결제건 수 추세'
                }
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        drawOnChartArea: false
                    }
                }],
                yAxes: [{
                    // 우측 payment
                    type: 'linear',
                    display: true,
                    position: 'left',
                    id: 'paymentY',
                    gridLines: {
                        drawOnChartArea: false
                    }
                }],
            }
        }
    },
    genderPieChartConfig: {
        type: 'pie',
        data: null,
        options: {
            responsive: true,
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: function(t, d) {
                        return d['labels'][t['index']]
                            + ': '
                            + d['datasets'][0]['data'][t['index']]
                            + '%';
                    }
                }
            }
        }
    },
    agePieChartConfig: {
        type: 'pie',
        data: null,
        options: {
            responsive: true,
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: function(t, d) {
                        return d['labels'][t['index']]
                            + ': '
                            + d['datasets'][0]['data'][t['index']]
                            + '%';
                    }
                }
            }
        }
    },
    drawLineChart: function (axis, e, p) {
        // draw double line chart
        salesTrendMain.lineChartConfig.data = {
            labels: axis,
            datasets: [{
                label: '상품 조회 수',
                borderColor: salesTrendMain.chartColors.orange,
                backgroundColor: salesTrendMain.chartColors.orange,
                fill: false,
                data: e,
                yAxisID: 'eventY',
            }, {
                label: '상품 결제건 수',
                borderColor: salesTrendMain.chartColors.blue,
                backgroundColor: salesTrendMain.chartColors.blue,
                fill: false,
                data: p,
                yAxisID: 'paymentY'
            }]
        };
        let lineChart = new Chart(
            salesTrendMain.lineChartCtx,
            salesTrendMain.lineChartConfig);
        lineChart.update();

        // draw single line chart
        salesTrendMain.eventChartConfig.data = {
            labels: axis,
            datasets: [{
                label: '상품 조회 수',
                borderColor: salesTrendMain.chartColors.orange,
                backgroundColor: salesTrendMain.chartColors.orange,
                fill: false,
                data: e,
                yAxisID: 'eventY',
            }]
        };
        let eventChart = new Chart(
            salesTrendMain.eventChartCtx,
            salesTrendMain.eventChartConfig);
        eventChart.update();

        salesTrendMain.paymentChartConfig.data = {
            labels: axis,
            datasets: [{
                label: '상품 결제건 수',
                borderColor: salesTrendMain.chartColors.blue,
                backgroundColor: salesTrendMain.chartColors.blue,
                fill: false,
                data: p,
                yAxisID: 'paymentY',
            }]
        };
        let paymentChart = new Chart(
            salesTrendMain.paymentChartCtx,
            salesTrendMain.paymentChartConfig);
        paymentChart.update();
    },
    drawGenderPieChart: function (lb, d) {
        salesTrendMain.genderPieChartConfig.data = {
            datasets: [{
                data: d,
                backgroundColor: [
                    salesTrendMain.chartColors.red,
                    salesTrendMain.chartColors.blue
                ],
                label: 'gender'
            }],
            labels: lb
        };
        let genderPieChart = new Chart(
            salesTrendMain.genderPieChartCtx,
            salesTrendMain.genderPieChartConfig);
        genderPieChart.update();
    },
    drawAgePieChart: function (lb, d) {
        salesTrendMain.agePieChartConfig.data = {
            datasets: [{
                data: d,
                backgroundColor: [
                    salesTrendMain.chartColors.red,
                    salesTrendMain.chartColors.green,
                    salesTrendMain.chartColors.blue,
                    salesTrendMain.chartColors.orange,
                    salesTrendMain.chartColors.yellow
                ],
                label: 'age'
            }],
            labels: lb
        };
        let agePieChart = new Chart(
            salesTrendMain.agePieChartCtx,
            salesTrendMain.agePieChartConfig);
        agePieChart.update();
    },
    init: function () {
        this.initDate('-7d');
        this.bind();
    },
    initDate: function (d) {
        salesTrendMain.setDate = Calendar.datePickerRange('startDate',
            'endDate');
        salesTrendMain.setDate.setEndDate(0);
        salesTrendMain.setDate.setStartDate(d);
        $('#endDate').datepicker('option', 'minDate', $('#startDate').val());
    },
    bind: function () {
        $('#set7dBtn').on('change', function () {
            salesTrendMain.initDate('-7d');
        });
        $('#set3mBtn').on('change', function () {
            salesTrendMain.initDate('-3m');
        });
    },
    search: function () {
        let s = $('#startDate').val();
        let e = $('#endDate').val();
        if (!salesTrendMain.validation(s, e)) {
            return;
        }

        let data = salesTrendMain.toJson($('#salesTrendSearchForm'));
        // TODO 데이터가 없어서 개발 당시에만 치환처리
        //data.sellerId = sellerId;
        data.sellerId = 'S111111';
        let req = JSON.stringify(data);

        CommonAjax.basic({
            url: "/settle/salesTrend/summary.json",
            method: "POST",
            data: req,
            contentType: "application/json",
            callbackFunc: function (res) {
                $('#eventCnt').html($.jUtil.comma(res.eventCnt));
                $('#paymentCnt').html($.jUtil.comma(res.paymentCnt));
                $('#saleAmt').html($.jUtil.comma(res.saleAmt));
                $('#conversionRate').html(res.conversionRate.toFixed(2));
            }
        });

        CommonAjax.basic({
            url: "/settle/salesTrend/daily.json",
            method: "POST",
            data: req,
            contentType: "application/json",
            callbackFunc: function (res) {
                let x = [];
                let evt = [];
                let pmt = [];

                $.each(res, function (idx, row) {
                    evt.push(row.eventCnt);
                    pmt.push(row.paymentCnt);
                    x.push(moment(row.regDt, 'YYYY-MM-DD').format(
                        'MM.DD' + salesTrendMain.getBracketDate(row.regDt)));
                });
                salesTrendMain.drawLineChart(x, evt, pmt);
            }
        });

        CommonAjax.basic({
            url: "/settle/salesTrend/gender.json",
            method: "POST",
            data: req,
            contentType: "application/json",
            callbackFunc: function (res) {
                let lb = [];
                let gdr = [];
                $.each(res, function (idx, d) {
                    gdr.push(d.rate.toFixed(2));
                    lb.push(d.gender);
                });
                salesTrendMain.drawGenderPieChart(lb, gdr);
            }
        });

        CommonAjax.basic({
            url: "/settle/salesTrend/age.json",
            method: "POST",
            data: req,
            contentType: "application/json",
            callbackFunc: function (res) {
                let lb = [];
                let age = [];
                $.each(res, function (idx, d) {
                    age.push(d.rate.toFixed(2));
                    lb.push(d.age);
                });
                salesTrendMain.drawAgePieChart(lb, age);
            }
        });
    },
    toJson: function (f) {
        let array = f.serializeArray();
        let param = {};

        $(array).each(function (i, v) {
            if (param[v.name]) {
                if (param[v.name].length < 2) {
                    let tmpArray = [];
                    tmpArray.push(param[v.name]);
                    tmpArray.push(v.value);
                    param[v.name] = tmpArray;
                } else {
                    param[v.name].push(v.value);
                }
            } else {
                param[v.name] = v.value;
            }
        });
        return param;
    },
    reset: function () {
        $('#salesTrendSearchForm').resetForm();
        salesTrendMain.initDate('-1m');
    },
    setData: function () {

    },
    validation: function (s, e) {
        if (moment(e).diff(moment(s), 'month') >= 6) {
            alert('통계는 최대 6개월까지 조회할 수 있습니다.');
            return false;
        }
        return true;
    },
    comma: function (d) {
        return d.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
};