/***
 * 세금계산서 조회 그리드 설정
 */

$(document).ready(function() {
    partnerTaxBillForm.init();
    partnerTaxBillForm.bindingEvent();
    partnerTaxBillGrid.init();
    CommonAjaxBlockUI.global();
});

var partnerTaxBillGrid = {
    gridView : new RealGridJS.GridView("partnerTaxBillGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        partnerTaxBillGrid.initGrid();
        partnerTaxBillGrid.initDataProvider();
    },
    initGrid : function () {
        partnerTaxBillGrid.gridView.setDataSource(partnerTaxBillGrid.dataProvider);
        partnerTaxBillGrid.gridView.setStyles(partnerTaxBillBaseInfo.realgrid.styles);
        partnerTaxBillGrid.gridView.setDisplayOptions(partnerTaxBillBaseInfo.realgrid.displayOptions);
        partnerTaxBillGrid.gridView.setOptions(partnerTaxBillBaseInfo.realgrid.options);
        partnerTaxBillGrid.gridView.setColumns(partnerTaxBillBaseInfo.realgrid.columns);
    },
    initDataProvider : function() {
        partnerTaxBillGrid.dataProvider.setFields(partnerTaxBillBaseInfo.dataProvider.fields);
        partnerTaxBillGrid.dataProvider.setOptions(partnerTaxBillBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        partnerTaxBillGrid.dataProvider.clearRows();
        partnerTaxBillGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(partnerTaxBillGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = partnerTaxBillForm.setExcelFileName("세금계산서");
        partnerTaxBillGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
/***
 * 세금계산서  조회 폼
 */
var partnerTaxBillForm = {
    init : function () {
        this.initSearchYM();
        $('#searchField').val('');
    },
    initSearchYM : function () {
        var today = new Date();
        var year = moment(today, 'YYYY-MM-DD', true).add(-1, "month").format("YYYY");
        var month = moment(today, 'YYYY-MM-DD', true).add(-1, "month").format("MM");

        $('#startYear, #endYear').val(year);
        SettleCommon.setMonth(year,'startMonth');
        SettleCommon.setMonth(year,'endMonth');
        $('#startMonth, #endMonth').val(month);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(partnerTaxBillForm.search);
        $('#resetBtn').bindClick(partnerTaxBillForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(partnerTaxBillGrid.excelDownload);

        $('#pageViewCnt').change(function () {
            sellUtil.onRealGridPaging(partnerTaxBillGrid.realGrid, "pagingArea", $('#pageViewCnt').val(), 10);
        });
    },
    valid : function () {
        var startDt = $("#startYear").val() + '-' + $("#startMonth").val() + '-01';
        var endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
        startDt = moment(startDt, 'YYYY-MM-DD', true);
        endDt = moment(endDt, 'YYYY-MM-DD', true).add(1, "day");

        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            var year = $("#startYear").val();
            var month = $("#startMonth").val();
            $("#startYear").val($("#endYear").val());
            $("#startMonth").val($("#endMonth").val());
            $("#endYear").val(year);
            SettleCommon.setMonth(year,'endMonth');
            $("#endMonth").val(month);
            return false;
        }
        if (endDt.diff(startDt, "month", true) > 3) {
            alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
            var year = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("YYYY");
            var month = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("MM");
            $("#endYear").val(year);
            SettleCommon.setMonth(year,'endMonth');
            $("#endMonth").val(month);
            return false;
        }

        return true;
    },
    search : function () {
        if(!partnerTaxBillForm.valid()) return false;

        var searchForm = $("form[name=searchForm]").serialize();
        var startDt = $("#startYear").val() +'-'+ $("#startMonth").val() + '-01';
        var endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
        endDt = moment(endDt, 'YYYY-MM-DD', true).add(1, "month").format("YYYY-MM-DD");
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        CommonAjax.basic(
        {
            url: '/settle/getTaxBillList.json?' + searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                partnerTaxBillGrid.setData(res);
                $('#searchCnt').html($.jUtil.comma(partnerTaxBillGrid.gridView.getItemCount()));

                //페이징 처리
                sellUtil.onRealGridPaging(partnerTaxBillGrid.gridView, "pagingArea", $('#pageViewCnt').val(), 100);
            }
        });
    },
    setExcelFileName : function (fileName) {
        var excelFileName = fileName +'_'+ $("#startYear").val() + $("#startMonth").val()+'_'+ $("#endYear").val() + $("#endMonth").val();
        return excelFileName;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function() {
            this.reset();
            partnerTaxBillForm.init();
        });
    }
};