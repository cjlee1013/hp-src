/***
 * 정산현황 그리드 설정
 */

$(document).ready(function() {
    partnerSettleListForm.init();
    partnerSettleListForm.bindingEvent();
    partnerSettleListGrid.init();
    CommonAjaxBlockUI.global();
});

var partnerSettleListGrid = {
    gridView : new RealGridJS.GridView("partnerSettleListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        partnerSettleListGrid.initGrid();
        partnerSettleListGrid.initDataProvider();
        partnerSettleListGrid.event();
    },
    initGrid : function () {
        partnerSettleListGrid.gridView.setDataSource(partnerSettleListGrid.dataProvider);
        partnerSettleListGrid.gridView.setStyles(partnerSettleBaseInfo.realgrid.styles);
        partnerSettleListGrid.gridView.setDisplayOptions(partnerSettleBaseInfo.realgrid.displayOptions);
        partnerSettleListGrid.gridView.setOptions(partnerSettleBaseInfo.realgrid.options);
        partnerSettleListGrid.gridView.setColumns(partnerSettleBaseInfo.realgrid.columns);
    },
    initDataProvider : function() {
        partnerSettleListGrid.dataProvider.setFields(partnerSettleBaseInfo.dataProvider.fields);
        partnerSettleListGrid.dataProvider.setOptions(partnerSettleBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        partnerSettleListGrid.dataProvider.clearRows();
        partnerSettleListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(partnerSettleListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = partnerSettleListForm.setExcelFileName("정산현황_검색결과");
        partnerSettleListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
/***
 * 매출조회 파트너 상세
 */
var partnerSettleListForm = {
    init : function () {
        this.initSearchDate('-1d');
        $('#searchField').val('');
    },
    initSearchDate : function (flag) {
        partnerSettleListForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        partnerSettleListForm.setDate.setEndDate(0);
        partnerSettleListForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(partnerSettleListForm.search);
        $('#resetBtn').bindClick(partnerSettleListForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(partnerSettleListGrid.excelDownload);

        $('#pageViewCnt').change(function () {
            sellUtil.onRealGridPaging(partnerSettleListGrid.realGrid, "pagingArea", $('#pageViewCnt').val(), 10);
        });
    },
    valid : function () {
        var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day");
        if(!startDt.isValid() || !endDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }
        if (endDt.diff(startDt, "month", true) > 3) {
            alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
            $("#schStartDt").val(endDt.add(-3, "month").format("YYYY-MM-DD"));
            return false;
        }

        return true;
    },
    search : function () {
        if(!partnerSettleListForm.valid()) return false;

        var searchForm = $("form[name=searchForm]").serialize();
        var startDt = $("#schStartDt").val().split('-').join('');
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        CommonAjax.basic({
            url: '/settle/getFinalSettle.json?' + searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                partnerSettleListGrid.setData(res);
                $('#searchCnt').html($.jUtil.comma(partnerSettleListGrid.gridView.getItemCount()));

                //페이징 처리
                sellUtil.onRealGridPaging(partnerSettleListGrid.gridView, "pagingArea", $('#pageViewCnt').val(), 100);
            }
        });
    },
    setExcelFileName : function (fileName) {
        var excelFileName = fileName +'_'+ $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        return excelFileName;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function() {
            this.reset();
            partnerSettleListForm.init();
        });
    }
};