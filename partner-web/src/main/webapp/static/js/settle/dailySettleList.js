/***
 * 매출현황 그리드 설정
 */

$(document).ready(function() {
    dailySettleListForm.init();
    dailySettleListForm.bindingEvent();
    dailySettleListGrid.init();
    dailySettleInfoGrid.init();
    CommonAjaxBlockUI.global();
});

var dailySettleListGrid = {
    gridView : new RealGridJS.GridView("dailySettleListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dailySettleListGrid.initGrid();
        dailySettleListGrid.initDataProvider();
        dailySettleListGrid.event();
    },
    initGrid : function () {
        dailySettleListGrid.gridView.setDataSource(dailySettleListGrid.dataProvider);
        dailySettleListGrid.gridView.setStyles(dailySettleListBaseInfo.realgrid.styles);
        dailySettleListGrid.gridView.setDisplayOptions(dailySettleListBaseInfo.realgrid.displayOptions);
        dailySettleListGrid.gridView.setOptions(dailySettleListBaseInfo.realgrid.options);
        dailySettleListGrid.gridView.setColumns(dailySettleListBaseInfo.realgrid.columns);
    },
    initDataProvider : function() {
        dailySettleListGrid.dataProvider.setFields(dailySettleListBaseInfo.dataProvider.fields);
        dailySettleListGrid.dataProvider.setOptions(dailySettleListBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        dailySettleListGrid.dataProvider.clearRows();
        dailySettleListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(dailySettleListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = dailySettleListForm.setExcelFileName("매출현황");
        dailySettleListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
/***
 * 매출관리 상세 그리드
 */
var dailySettleInfoGrid = {
    gridView : new RealGridJS.GridView("dailySettleInfoGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dailySettleInfoGrid.initGrid();
        dailySettleInfoGrid.initDataProvider();
    },
    initGrid : function () {
        dailySettleInfoGrid.gridView.setDataSource(dailySettleInfoGrid.dataProvider);
        dailySettleInfoGrid.gridView.setStyles(dailySettleInfoBaseInfo.realgrid.styles);
        dailySettleInfoGrid.gridView.setDisplayOptions(dailySettleInfoBaseInfo.realgrid.displayOptions);
        dailySettleInfoGrid.gridView.setOptions(dailySettleInfoBaseInfo.realgrid.options);
        dailySettleInfoGrid.gridView.setColumns(dailySettleInfoBaseInfo.realgrid.columns);
    },
    initDataProvider : function() {
        dailySettleInfoGrid.dataProvider.setFields(dailySettleInfoBaseInfo.dataProvider.fields);
        dailySettleInfoGrid.dataProvider.setOptions(dailySettleInfoBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        dailySettleInfoGrid.dataProvider.clearRows();
        dailySettleInfoGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        var searchForm = $("form[name=searchForm]").serialize();
        var startDt = $("#schStartDt").val().split('-').join('');
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        if ($('#searchType').val() == "ITEM_NO") {
            searchForm += "&itemNo="+$('#searchField').val();
        } else if ($('#searchType').val() == "ITEM_NM") {
            searchForm += "&itemNm="+ $('#searchField').val();
        }

        CommonAjax.basic(
            {
                url: '/settle/getSettleInfo.json?' + searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    dailySettleInfoGrid.setData(res);

                    if(dailySettleInfoGrid.gridView.getItemCount() == 0) {
                        alert("검색 결과가 없습니다.");
                        return false;
                    }

                    var fileName = dailySettleListForm.setExcelFileName("매출현황_주문상세내역");
                    dailySettleInfoGrid.gridView.exportGrid( {
                        type: "excel",
                        target: "local",
                        fileName: fileName+".xlsx",
                        showProgress: true,
                        progressMessage: " 엑셀 데이터 추줄 중입니다."
                    });
                }
            });
    }
};
/***
 * 매출조회 파트너 상세
 */
var dailySettleListForm = {
    init : function () {
        this.initSearchDate('-1d');
        $('#searchField').val('');
        SettleCommon.getInputTypeSetter('searchType', "ITEM_NO,ITEM_NM");
    },
    initSearchDate : function (flag) {
        dailySettleListForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        dailySettleListForm.setDate.setEndDate(0);
        dailySettleListForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(dailySettleListForm.search);
        $('#resetBtn').bindClick(dailySettleListForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(dailySettleListGrid.excelDownload);
        $('#detailExcelDownloadBtn').bindClick(dailySettleInfoGrid.excelDownload);

        $('#searchType').on('change', function() {
            if($(this).val() == "") {
                $('#searchField').attr('readonly', true).val('');
            } else {
                $('#searchField').removeAttr('readonly');
                $('#searchField').focus();
            }
        });

        $('#pageViewCnt').change(function () {
            sellUtil.onRealGridPaging(dailySettleListGrid.realGrid, "pagingArea", $('#pageViewCnt').val(), 10);
        });
    },
    valid : function () {
        var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day");
        if(!startDt.isValid() || !endDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }
        if (endDt.diff(startDt, "month", true) > 3) {
            alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
            $("#schStartDt").val(endDt.add(-3, "month").format("YYYY-MM-DD"));
            return false;
        }

        if($("#searchType").val() != '' && $.jUtil.isEmpty($('#searchField').val())) {
            alert('검색키워드를 입력해주세요.');
            $("#searchField").focus();
            return false;
        }
        if($("#searchType").val() == 'ITEM_NO' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM'])){
            alert('상품번호는 숫자 외의 문자는 입력할 수 없습니다.');
            $("#searchField").focus();
            return false;
        }

        return true;
    },
    search : function () {
        if(!dailySettleListForm.valid()) return;

        var searchForm = $("form[name=searchForm]").serialize();
        var startDt = $("#schStartDt").val().split('-').join('');
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        if ($('#searchType').val() == "ITEM_NO") {
            searchForm += "&itemNo="+$('#searchField').val();
        } else if ($('#searchType').val() == "ITEM_NM") {
            searchForm += "&itemNm="+ $('#searchField').val();
        }

        CommonAjax.basic(
            {
                url: '/settle/getDailySettle.json?' + searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    dailySettleListGrid.setData(res);
                    $('#searchCnt').html($.jUtil.comma(dailySettleListGrid.gridView.getItemCount()));

                    //페이징 처리
                    sellUtil.onRealGridPaging(dailySettleListGrid.gridView, "pagingArea", $('#pageViewCnt').val(), 100);
                }
            });
    },
    setExcelFileName : function (fileName) {
        var excelFileName = fileName +'_'+ $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        return excelFileName;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function() {
            this.reset();
            dailySettleListForm.init();
        });
    }
};