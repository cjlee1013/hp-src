var searchUserMain = {
    /**
     * init 이벤트
     */
    init: function(){
        this.buttonEvent();
    },

    /**
     * button 이벤트
     */
    buttonEvent: function(){

        // 조회 폼 초기화
        $("#initBtn").click(function(){
            searchUserMain.reset();
        });

        // 검색
        $("#searchBtn").click(function(){
            searchUserMain.search();
        });


        // 회원구분에 따라 검색어 셀릭트박스 구성 변경
        $('#searchUserClassify').on('change', function(){
          var type = $('#searchUserClassify option:selected').val();
            searchUserMain.makeSearchFrom(type);
        });
    },

    /**
     * 검색 폼 초기화
     * 일반/휴면 회원 초기화 (df 일반회원)
     * 회원구분 초기화 (df 개인)
     * 검색어구분 초기화 (df Mid)
     * 검색창 초기화
     */
    reset: function(){
        $("#searchUserStatus").val("1");
        $("#searchUserClassify").val("10");
        $("#searchType").val("userId");
        $("#keyword").val("");
        searchUserMain.makeSearchFrom("10");
    },
    /**
     * 검색 셀릭트박스 생성
     * 1. Id,이메일,휴대폰,이름
     * 2 사업자,판매자 = Min,Id,이메일,휴대폰,이름, 사업자번호, 상호명
     * @param type
     */
    makeSearchFrom : function (type) {
      var html = '<option value="userId">회원아이디</option> <option value="userEmail">이메일</option><option value="userPhone">휴대폰</option><option value="userName">회원이름</option>';

      if(type != "10"){
        html += '<option value="companyName">상호명</option>';
        html += '<option value="companyNo">사업자등록번호</option>';
      }

      $('#searchType').empty();
      $('#searchType').append(html);
    },

    /**
     * 데이터 조회
     * 1. 검색어가 없을 경우 조회 불가
     * 2. 검색어가 2자 미만일 경우 조회 불가
     * 3. 유료회원 grid list 조회
     * @param func
     * @returns {boolean}
     */
    search: function(func){
      if (isEmpty($("#keyword").val().trim())) {
          alert("검색어를 입력해주세요.");
          return false;
      }

      if ($("#keyword").val().trim().length < 2) {
        alert("검색어는 2글자 이상 입력해주세요.");
        return false;
      }
      //상세 초기화
      searchUserMain.clearDetail();

      var data = $("#searchForm").serialize();
      manageAjax('/user/searchUserMain/searchUserList.json?' + data, 'get', null, function(res){
        searchUserGrid.setData(res);
        $('#searchUserMainListCount').html($.jUtil.comma(searchUserGrid.gridView.getItemCount()));
      });

    },
    /**
    * 유료회원 상세정보 조회
    * grid에 있는 내용으로 그대로 조회
    * @param data
    */
    setDetail: function(data) {
        // 문의 상세
        $("#userId").html(data.userId);
        $("#userName").html(data.userName);
        $("#userClassifyNm").html(data.userClassifyNm);
        $("#companyNm").html(data.companyNm);
        $("#companyNo").html(companyNoFormatter(data.companyNo));
        $("#userEmail").html(data.userEmail);
        $("#userPhone").html(data.userPhone);
        $("#clubGrade").html(data.clubGrade);
    },

    /**
     * 유료회원 여부, 유료회원 이력 상세 조회
     *
     * @param data
     */
    setSpecialClubHistory: function(data) {
        var html;
        if (data.length > 0) {
          $(data).each(function(i, data){
            html += '<tr>';
            html += '   <td>' + data.historySeq + '</td>';
            html += '   <td>' + data.clubStartDate +'</td>';
            html += '   <td>' + data.clubEndDate +'</td>';
            html += '   <td>' + data.clubGrade + '</td>';
            html += '   <td>' + data.clubJoinType + '</td>';
            html += '   <td>' + data.dealId + '</td>';
            html += '   <td>' + data.paymentId + '</td>';
            html += '   <td>' + data.regId + '</td>';
            html += '</tr>';
          });

          $('#specialclubHistoryListTbody').html(html);
        } else {
          searchUserMain.initSpecialclubHistoryListTable();
        }

    },
    // 계약서 체결 현황 초기화
    initSpecialclubHistoryListTable : function () {
        $('#specialclubHistoryListTbody').html('<tr><td colspan="8" style="text-align:center;">검색결과가 없습니다.</td></tr>');
    },
    /**
     * Clear 데이터 상세 정보
     */
    clearDetail: function(){
        // 상세보기 데이터 초기화
        $("#userId, #userName, #userClassifyNm, #companyNm, #companyNo, #userEmail, #userPhone, #membershipStatus, #clubGrade").html("");

        searchUserMain.initSpecialclubHistoryListTable();
    }

};

var searchUserGrid = {
    gridView : new RealGridJS.GridView("searchUserGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function(){
        searchUserGrid.initGrid();
        searchUserGrid.initDataProvider();
        searchUserGrid.event();
    },
    initGrid : function(){
        searchUserGrid.gridView.setDataSource(searchUserGrid.dataProvider);
        searchUserGrid.gridView.setStyles(searchUserMainGridBaseInfo.realgrid.styles);
        searchUserGrid.gridView.setDisplayOptions(searchUserMainGridBaseInfo.realgrid.displayOptions);
        searchUserGrid.gridView.setColumns(searchUserMainGridBaseInfo.realgrid.columns);
        searchUserGrid.gridView.setOptions(searchUserMainGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        searchUserGrid.dataProvider.setFields(searchUserMainGridBaseInfo.dataProvider.fields);
        searchUserGrid.dataProvider.setOptions(searchUserMainGridBaseInfo.dataProvider.options);
    },
    event : function() {
        searchUserGrid.gridView.onDataCellClicked = function(gridView, index) {
            var data = searchUserGrid.dataProvider.getJsonRow(index.dataRow);
            manageAjax('/user/searchUserMain/searchUserDetail.json', 'get', data, function(res){
              searchUserMain.setDetail(res);
            });
            //상세정보 입력, 유료회원이력 호출
            manageAjax('/user/specialClubMain/searchSpecialClubHistoryList.json', 'get', data, function(response){
                searchUserMain.setSpecialClubHistory(response);
            });
        };
    },
    setData : function(dataList) {
        searchUserGrid.dataProvider.clearRows();
        searchUserGrid.dataProvider.setRows(dataList);
    }
};

function companyNoFormatter(num) {
  if (num.length != 10){
    return num
  }
  return num.substring(0,3)+"-"+num.substring(3,5)+"-"+num.substring(5,10);

}

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg){
    var params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;

    //content Type
    $.ajaxSetup({ contentType: "application/json; charset=utf-8", });
    CommonAjax.basic(params);
}

/**
 * Util: Null체크 함수
 *
 * @param value
 * @return Boolean
 */
var isEmpty = function (value) {
    if (value == "" || value == null || value == undefined ||
        ( value != null && typeof value == "object" && !Object.keys(value).length )) {
        return true
    } else {
        return false
    }
};

