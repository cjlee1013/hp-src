let partner = {
    deliveryInfo        : null,     // 반품택배사 정보
    timer       	: 0,
    smsCertToken	: "",		// sms 토큰
    smsSuccessToken	: "",		// sms 성공 토큰
    checkInfo		: "",
    smsCountdownTimer	: null,
    mobile			: "",
    smsCheck        : true,
    isApiKey        : false,
    init : function() {
        partner.event();
        $('input[name="sellerInfo.cultureDeductionYn"][value="N"]').trigger('click');
        partner.getDetail();
        commonCategory.setCategorySelectBox(1, '', '', $('#categoryCd'), function () {
            $('#categoryCd').val(partnerInfo.categoryCd);
        });
        partner.file.init();
    },
    event : function() {
        $('#copyAffi').on('change', function () {
            if ($(this).is(':checked')) {
                for (var i = 1; i < 4; i++) {
                    $('#mngNm' + i).val($('#mngNm0').val());
                    $.jUtil.valSplit($.jUtil.valNotSplit('mngMobile0', '-'), '-',
                            ('mngMobile' + i));
                    $.jUtil.valSplit($.jUtil.valNotSplit('mngPhone0', '-'), '-',
                            ('mngPhone' + i));
                    $('#mngEmail' + i ).val($('#mngEmail0').val());
                }
            } else {
                for (var i = 1; i < 4; i++) {
                    $('#managerDiv' + i).find('input').not('#mngCd'+i).val('');

                }
            }
        });

        $('#passwordPopBtn').bindClick(partner.getPasswordPop);
        $('#returnDeliveryPopBtn').bindClick(partner.getReturnDeliveryPop);
        $("input[name='sellerInfo.cultureDeductionYn']").bindClick(partner.setCultureDeductionYn);

        // 인증하기
        $("#certiCheck").on("click", function() {
            if (partner.affiManagerPhoneCheck()){
                // 인증번호 호출
                var mobileNum = $.jUtil.valNotSplit('mngMobile0', '-');
                partner.setCertified(mobileNum);
            }
        });

        // 재발송
        $("#reCertiCheck").on("click", function() {
            if (partner.affiManagerPhoneCheck()){
                // interval clear
                clearInterval(partner.timer);
                // 인증번호 호출
                var mobileNum = $.jUtil.valNotSplit('mngMobile0', '-');
                partner.setCertified(mobileNum);
            }
        });

        // 인증하기 - 완료
        $("#certiCheckWar").on("click", function() {
            if ($("#checkInfo").val() == "") {
                alert("인증번호를 입력해 주세요.");
                $("#checkInfo").focus();
                return;
            }

            if ($("#checkInfo").val().length != 6) {
                alert("6자리 인증번호를 입력해 주세요.");
                $("#checkInfo").focus();
                return;
            }

            var mobileNum = $.jUtil.valNotSplit('mngMobile0', '-');
            partner.checkCertified(mobileNum);
        });

        // 인증해제
        $("#releaseCerti").on("click", function () {
            if (!confirm("등록한 휴대폰 번호의 인증을 해제하고 새로운 번호로 등록하시겠습니까?")) {
                return;
            }

            // 영업담당자 인증 flag 변경
            partner.smsCheck = false;

            // 휴대폰 번호 입력 활성화
            $("#mngMobile0_1").attr("readonly", false);
            $("#mngMobile0_2").attr("readonly", false);
            $("#mngMobile0_3").attr("readonly", false);

            // button view
            $("#certiCheck").show();
            $("#reCertiCheck").hide();
            $("#releaseCerti").hide();
            $("#divCertiCheck").hide();
        });
    },
    getDetail : function() {
        $('#returnPopPartnerId').text(partnerInfo.partnerId);
        $('#partnerNo').text(partnerInfo.partnerNo);
        $('#communityNotiNo').text(partnerInfo.communityNotiNo);
        $('#partnerNm').text(partnerInfo.partnerNm);
        $('#businessNm').text(partnerInfo.businessNm);
        $('#partnerOwner').text(partnerInfo.partnerOwner);
        $('#businessConditions').text(partnerInfo.businessConditions);
        $('#bizCateCdNm').text(partnerInfo.bizCateCdNm);
        $('#addr').text('(' + partnerInfo.zipcode + ')' + partnerInfo.addr1 + ' ' + partnerInfo.addr2);
        $.jUtil.valSplit(partnerInfo.phone1, '-', 'phone1');
        $('#email').val(partnerInfo.email);
        $('input[name="sellerInfo.smsYn"][value="'+partnerInfo.smsYn+'"]').trigger('click');
        $('input[name="sellerInfo.emailYn"][value="'+partnerInfo.emailYn+'"]').trigger('click');
        $.jUtil.valSplit(partnerInfo.infoCenter, '-', 'infoCenter');
        $('input[name="sellerInfo.returnDeliveryYn"][value="'+partnerInfo.returnDeliveryYn+'"]').trigger('click');
        $('input[name="sellerInfo.cultureDeductionYn"][value="'+partnerInfo.cultureDeductionYn+'"]').trigger('click');
        $('#cultureDeductionNo').val(partnerInfo.cultureDeductionNo);
        $('#homepage').val(partnerInfo.homepage);
        $('#companyInfo').val(partnerInfo.companyInfo);
        $('#bankArea').text(partnerInfo.bankCdNm + ' ' + partnerInfo.bankAccountNo + '(예금주 : ' + partnerInfo.depositor + ')');
        $('#agencyCd').val(partnerInfo.agencyCd);
        $('#apiKey').val(partnerInfo.apiKey);
        if ($.jUtil.isNotEmpty(partnerInfo.apiKey)) {
            $('#apiKeyTxt').text('재발급');
            partner.isApiKey = true;
        }

        $('#apiKeyRegDt').text(partnerInfo.apiKeyRegDt);

        partnerInfo.managerList.forEach(function (item, index, array) {
            var tagId =  $('.managerDiv[data-mng-cd="'+item.mngCd+'"]');

            tagId.find('#seq'+index).val(item.seq);
            tagId.find('#mngNm'+index).val(item.mngNm);
            tagId.find('#mngEmail'+index).val(item.mngEmail);

            $.jUtil.valSplit(item.mngMobile, '-', ('mngMobile' + index));
            if (_V.chkCellPhoneNum(item.mngPhone, true)) {
                $.jUtil.valSplit(item.mngPhone, '-', ('mngPhone' + index));
            }
        });

        //첨부서류
        for (let i in partnerInfo.partnerFileList) {
            partner.file.appendPartnerFile(partnerInfo.partnerFileList[i]);
        }

        //로그인 인증 담당자 정보
        if( partnerInfo.certificationManagerList.length > 0 ){
            partnerInfo.certificationManagerList.forEach(function (item, index, array) {

                partner.addCertification(index, "U");

                var tarId = $('#certiDiv'+ index);

                tarId.find('#certificationSeq' + index).val(item.certificationSeq);
                tarId.find('#certificationMngNm' + index).val(item.certificationMngNm);

                $.jUtil.valSplit(item.certificationMngPhone, '-', ('certificationMngPhone' + index));

            });
        }else{
            partner.addCertification(0, "I");
        }


    },
    /**
     * 판매업체 저장
     */
    setSeller : function() {
        if (partner.valid.set()) {
            $('#phone1').val($.jUtil.valNotSplit('phone1', '-'));
            $('#infoCenter').val($.jUtil.valNotSplit('infoCenter', '-'));

            for(var i=0; i < 4; i++) {
                $('#mngMobile' + i).val($.jUtil.valNotSplit('mngMobile' + i, '-'));
                $('#mngPhone' + i).val($.jUtil.valNotSplit('mngPhone' + i, '-'));
            }

            $('#certificationTable .managerDiv').each(function(){
                var _num =  $(this).attr('id').substring(8);
                $('#certificationMngPhone' + _num ).val($.jUtil.valNotSplit('certificationMngPhone' + _num, '-'));
            });

            var sellerForm = $('#sellerForm').serializeObject();

            // 반품택배 추가
            if ($("input:radio[name='sellerInfo.returnDeliveryYn']:checked").val() == 'Y' && partner.deliveryInfo != null) {
                sellerForm.deliveryList = [];
                for (let i in partner.deliveryInfo) {
                    let data = partner.deliveryInfo[i];
                    sellerForm.deliveryList.push({dlvCd: data.dlvCd, creditCd: data.creditCd, useYn: "Y"});
                }
            }

            CommonAjax.basic({
                url: "/partner/setPartner.json",
                data: JSON.stringify(sellerForm),
                method: "POST",
                contentType: "application/json",
                callbackFunc: function (res) {
                    passwordPop.reset();
                    alert(res.returnMsg);
                    location.reload();
                }
            });
        }
    },
    getPasswordPop : function() {
        $ui.modalOpen('passwordPop');
    },
    /**
     * 반품택배사 계약정보 팝업창
     */
    getReturnDeliveryPop : function () {
        if ($("input:radio[name='sellerInfo.returnDeliveryYn']:checked").val() == 'Y') {
            $ui.modalOpen('returnDeliveryPop');
        } else {
            alert('반품택배사 있음으로 설정해주세요.');
        }
    },
    /**
     * 소득공제 제공여부
     */
    setCultureDeductionYn : function(cultureDeductionYn) {
        if (typeof cultureDeductionYn == 'object') {
            cultureDeductionYn = $(cultureDeductionYn).val();
        }

        if (cultureDeductionYn == 'Y') {
            $('#cultureDeductionNo').prop('readonly', false);
        } else {
            $('#cultureDeductionNo').val('').prop('readonly', true);
        }
    },
    /**
     * 영업담당자 휴대전화 체크
     * @returns {boolean}
     */
    affiManagerPhoneCheck : function(){
        if ($("#mngMobile0_1").val() == "" || $("#mngMobile0_2").val() == "" || $("#mngMobile0_3").val() == "") {
            alert("휴대폰 번호를 입력해주세요.");
            return false;
        }
        if($.jUtil.phoneValidate($("#mngMobile0_1").val() + '-' + $("#mngMobile0_2").val() + '-' + $("#mngMobile0_3").val(), true)) {
            return true;
        } else {
            alert('휴대폰 번호 형식이 잘못되었습니다. 다시 확인해 주세요.');
            return false;
        }
    },
    /**
     * 영업담당자 인증번호 생성
     * @param mobile
     */
    setCertified : function(mobile) {
        CommonAjax.basic({
            url:"/common/mobile/certno/send.json?mobile="+mobile
            , method:"GET"
            , callbackFunc:function(res) {
                if (res.hasOwnProperty("data")) {
                    partner.smsCertToken = res.data;

                    // 타이머 호출
                    partner.checkTimer();
                    alert("인증번호가 전송되었습니다.");

                    // 재발송 view
                    $("#certiCheck").hide();
                    $("#reCertiCheck").show();
                    $("#divCertiCheck").show();

                }
            }
            , errorCallbackFunc : function() {
                clearInterval(partner.timer);

                // 인증 view
                $("#certiCheck").show();
                $("#reCertiCheck").hide();
                $("#divCertiCheck").hide();
            }
        });
    },
    /**
     * 남은 시간 카운터
     */
    checkTimer : function() {
        var endTime = moment(new Date()).add(3, 'minutes');
        if(partner.smsCountdownTimer != null)
        {
            partner.smsCountdownTimer.endTimer();
        }
        partner.smsCountdownTimer = countdownTimer($('#spanTimeOut'),endTime,'ms','0:00',function(){
            if (partner.smsCheck == false) {
                alert('인증번호 입력시간이 지났습니다. \r\n다시 시도해 주세요.');
                partner.smsCheck = false;
            }
        });
    },
    /**
     * 인증번호 확인
     * @param mobile
     */
    checkCertified : function(mobile) {
        var param = {
            certToken: partner.smsCertToken
            ,   certNo: $("#checkInfo").val()
            ,   mobile: mobile
        };
        CommonAjax.basic({
            url: "/common/mobile/certno/valid.json"
            , data: JSON.stringify(param)
            , contentType: 'application/json'
            , method:"POST"
            , callbackFunc:function(res) {
                if (res.hasOwnProperty("data") && res.data == true) {
                    alert("휴대폰 인증에 성공하였습니다.");

                    partner.smsSuccessToken = res.data;
                    partner.checkInfo = $("#checkInfo").val();
                    partner.mobile = mobile;
                    partner.smsCheck = true;
                    clearInterval(partner.timer);

                    $("#checkInfo").val("");

                    // 휴대폰 번호 입력 활성화
                    $("#mngMobile0_1").attr("readonly", true);
                    $("#mngMobile0_2").attr("readonly", true);
                    $("#mngMobile0_3").attr("readonly", true);

                    // button view
                    $("#certiCheck").hide();
                    $("#reCertiCheck").hide();
                    $("#releaseCerti").show();
                    $("#divCertiCheck").hide();
                } else {
                    alert(res.returnMessage);
                }
            }
        });
    },
    setCancel : function () {
        $('.dhxtabbar_tab_text_close', parent.document).each(function(){
            if ($(this).text() == '회원정보 조회/수정') {
                $(this).siblings('.dhxtabbar_tab_close').trigger('click');
            }
        });
    },
    setApiKey : function () {
        if ($.jUtil.isEmpty($('#agencyCd').val())) {
            return $.jUtil.alert('발급업체를 선택해 주세요.', 'agencyCd');
        }

        if (partner.isApiKey == false || confirm('재발급 시, 기존에 발급된 키는 삭제되어 연동이 끊길 수 있습니다. 재발급을 진행하시겠습니까?')) {
            CommonAjax.basic({
                url: "/partner/setApiKey.json",
                data: JSON.stringify({agencyCd:$('#agencyCd').val()}),
                method: "POST",
                contentType: "application/json",
                callbackFunc: function (res) {
                    $('#apiKey').val(res.encKey);
                    $('#apiKeyRegDt').text(moment().format("YYYY-MM-DD"));
                    $('#apiKeyTxt').text('재발급');
                    partner.isApiKey = true;
                }
            });

        }
    },
    addCertification : function ( _num, type ){

        var certificateNum = 0;
        var certificateLength = $('#certificationTable [value=I]').length + $('#certificationTable [value=U]').length;
        let btnHtml = "";

        if( certificateLength != 0 ){
            certificateNum = Number($('#certificationTable tbody tr:last').attr('id').substring(8)) + 1;
        }

        $('#certiAddBtn' + ( _num - 1 ) ).remove();

        if( certificateLength == 4 ){
            btnHtml = '<button type="button" id="certiDelBtn' + certificateNum +'" class="btn-base mgl10" onclick="partner.delCertification(' + certificateNum + ');"><i>삭제</i></button>';
        }else if(certificateLength == 0){
            btnHtml = '<button type="button" id="certiAddBtn' + certificateNum +'" class="btn-base mgl10" onclick="partner.addCertification(' + ( certificateNum + 1 ) + ',\'I\');"><i>추가</i></button>';
        }else{
            btnHtml = '<button type="button" id="certiDelBtn' + certificateNum +'" class="btn-base mgl10" onclick="partner.delCertification(' + certificateNum + ');"><i>삭제</i></button>'
                 + '<button type="button" id="certiAddBtn' + certificateNum +'" class="btn-base mgl10" onclick="partner.addCertification(' + ( certificateNum + 1 ) + ',\'I\');"><i>추가</i></button>';
        }

        let html = '<tr id="certiDiv' + certificateNum + '" class="managerDiv" data-mng-cd="AFFI">'
                + '<th scope="row">담당자</th>'
                +   '<td>'
                +       '<dl class="inp-dl">'
                +           '<dt><span>담당자명</span></dt>'
                +           '<dd>'
                +               '<input type="hidden" name="certificationManagerList[].certificationSeq" id="certificationSeq' + certificateNum + '">'
                +               '<input type="hidden" name="certificationManagerList[].certificationState" id="certificationState' + certificateNum + '" value="' + type + '">'
                +               '<input type="text" name="certificationManagerList[].certificationMngNm" id="certificationMngNm' + certificateNum + '" class="inp-base" title="담당자명" maxlength="10">'
                +          '</dd>'
                +      '</dl>'
                +       '<dl class="inp-dl">'
                +           '<dt><span>휴대폰번호</span></dt>'
                +           '<dd>'
                +               '<input type="hidden" name="certificationManagerList[].certificationMngPhone" id="certificationMngPhone' + certificateNum + '" value="">'
                +               '<input type="text" name="certificationMngPhone' + certificateNum + '_1" id="certificationMngPhone' + certificateNum + '_1" class="inp-base w-80" title="휴대폰번호" maxlength="3"> - '
                +               '<input type="text" name="certificationMngPhone' + certificateNum + '_2" id="certificationMngPhone' + certificateNum + '_2" class="inp-base w-80" title="휴대폰번호" maxlength="4"> - '
                +               '<input type="text" name="certificationMngPhone' + certificateNum + '_3" id="certificationMngPhone' + certificateNum + '_3" class="inp-base w-80" title="휴대폰번호" maxlength="4"> '
                +               btnHtml
                +           '</dd>'
                +       '</dl>'
                +    '</td>'
                + '</tr>'

        $('#certificationTable').append(html);
    },
    delCertification : function ( _num ){

        var seq = $('#certificationSeq' + _num).val();

        if( !$.jUtil.isEmpty(seq) ){

            if( !confirm("정말 삭제하시겠습니까?") ){
                return false;
            }
            $('#certificationState' + _num).val('D');
            $('#certiAddBtn' + _num ).remove();
            $('#certiDiv' + _num ).hide();

        }else{
            $('#certiDiv' + _num ).remove();
        }

        var certificateLength = $('#certificationTable tbody tr [id^=certiAddBtn]').length;

        if( certificateLength < 1 ){
            var certificateNum = Number($('#certificationTable tbody tr:last').attr('id').substring(8));
            $('#certificationTable tbody tr:visible dd:last').append('<button type="button" id="certiAddBtn' + certificateNum +'" class="btn-base mgl10" onclick="partner.addCertification(' + ( certificateNum + 1 ) + ');"><i>추가</i></button>');
        }



    }
};


partner.valid = {
    set: function () {
        var isValid = true;

        if (!$('input[name="sellerInfo.smsYn"]').is(':checked')) {
            return $.jUtil.alert('필수입력항목을 모두 입력해주세요.', 'lbAgree01_01');
        } else if (!$('input[name="sellerInfo.emailYn"]').is(':checked')) {
            return $.jUtil.alert('필수입력항목을 모두 입력해주세요.', 'lbAgree02_01');
        }

        $('.checkVal').each(function () {
            if ($.jUtil.isEmpty($(this).val())) {
                isValid = false;
                return $.jUtil.alert('필수입력항목을 모두 입력해주세요.', $(this).prop('id'));
            }
        });

        $('#certificationTable .managerDiv').each(function(){
            var _num =  $(this).attr('id').substring(8);

            if( $('#certificationState' + _num).val() != 'D'  ){

                if( !( $.jUtil.isEmpty($('#certificationMngNm' + _num).val()) && $.jUtil.isEmpty($('#certificationMngPhone' + _num + '_1').val())
                    && $.jUtil.isEmpty($('#certificationMngPhone' + _num + '_2').val()) && $.jUtil.isEmpty($('#certificationMngPhone' + _num + '_3').val()) )  ){

                    // 로그인 인증 담당자명 한글,영문
                    var types = [ 'ENG' , 'KOR' ];
                    if( !$.jUtil.isAllowInput($('#certificationMngNm' + _num).val() , types )  ){
                        alert('로그인 인증담당자명 형식이 잘못되었습니다. 다시 확인해 주세요.');
                        isValid = false;
                        return false;
                    }

                    // 로그인 인증 담당자 핸드폰번호
                    var phoneNumber = $('#certificationMngPhone' + _num + '_1').val() + '-' + $('#certificationMngPhone' + _num + '_2').val() + '-' + $('#certificationMngPhone' + _num + '_3').val()
                    if(!$.jUtil.phoneValidate( phoneNumber, true)) {
                        alert('로그인 인증담당자 휴대폰 번호 형식이 잘못되었습니다. 다시 확인해 주세요.');
                        isValid = false;
                        return false;
                    }
                }else{

                    if( !$.jUtil.isEmpty( $('#certificationSeq' + _num).val()) ){
                        $('#certificationState' + _num).val('D');
                    }

                }
            }
        });

        if (isValid) {

            if (partner.smsCheck == false) {
                return $.jUtil.alert('영업담당자 휴대폰 인증을 진행해주세요.', 'certiCheck');
            }

            return isValid;
        } else {
            return isValid;
        }
    }
};


partner.file = {
    init : function() {
        partner.file.event();
    },
    event : function() {
        //파일선택시
        $('input[name="partnerFile1"], input[name="partnerFile2"], input[name="partnerFile3"], input[name="partnerFile4"], input[name="partnerFile5"]').change(function() {
            partner.file.addFile($(this).data('file-id'), $(this).prop('name'));
        });

        //판일삭제
        $('[id^=uploadPartnerFileSpan_]').on('click', '.btn-close', function() {
            if (confirm('등록된 파일을 삭제하시겠습니까?')) {
                $(this).closest('.add-file').remove();
            }
        });

        $('#partnerFileTable').on('click', '.partnerDownLoad', function() {
            $.jUtil.downloadFile($(this).data('url'), $(this).data('file-nm'), $(this).data('process-key'));
        });
    },
    clickFile : function(obj) {
        let limit = Number($('input[name="'+obj.data('field-name')+'"]').data('limit'));

        if ($('#uploadPartnerFileSpan_'+obj.data('file-type')).find('.btn-close').length >= limit) {
            return $.jUtil.alert("파일 추가는 최대 "+limit+"개까지 가능합니다.");
        }

        $('input[name="'+obj.data('field-name')+'"]').click();
    },
    /**
     * 파일업로드
     */
    addFile : function(id, fieldName) {
        let file = $('input[name="'+fieldName+'"]');

        let params = {
            processKey : "PartnerJoinBinary",
            fieldName : fieldName,
            mode : "FILE"
        };

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        partner.file.appendPartnerFile(f.fileStoreInfo, id);
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    appendPartnerFile : function(f, id) {
        if (id) { //등록
            let html = '<li class="add-file">'
                    + '<input type="hidden" class="fileSeq" name="partnerFileList[].fileSeq" value=""/>'
                    + '<input type="hidden" class="fileType" name="partnerFileList[].fileType" value="'+$('#'+id).data('file-type')+'"/>'
                    + '<input type="hidden" class="fileUrl" name="partnerFileList[].fileUrl" value="'+f.fileId+'"/>'
                    + '<input type="hidden" class="fileNm" name="partnerFileList[].fileNm" value="'+f.fileName+'"/>'
                    + '<a href="javascript:void(0);" class="partnerDownLoad file-link" data-url="'+ f.fileId + '" '
                    + 'data-process-key="PartnerJoinBinary" '
                    + 'data-file-nm="' + f.fileName + '" '
                    + '" >' + f.fileName
                    + '</a><button type="button" class="btn-close"><i class="hide">첨부파일삭제</i></button></li>';

            $('#'+id).append(html);
        } else { //조회
            let html = '<li class="add-file">'
                    + '<input type="hidden" class="fileSeq" name="partnerFileList[].fileSeq" value="'+f.fileSeq+'"/>'
                    + '<input type="hidden" class="fileType" name="partnerFileList[].fileType" value="'+f.fileType+'"/>'
                    + '<input type="hidden" class="fileUrl" name="partnerFileList[].fileUrl" value="'+f.fileUrl+'"/>'
                    + '<input type="hidden" class="fileNm" name="partnerFileList[].fileNm" value="'+f.fileNm+'"/>'
                    + '<a href="javascript:void(0);" class="partnerDownLoad file-link" data-url="'+ f.fileUrl + '" '
                    + 'data-process-key="PartnerJoinBinary" '
                    + 'data-file-nm="' + f.fileNm + '" '
                    + '" >' + f.fileNm
                    + '</a><button type="button" class="btn-close"><i class="hide">첨부파일삭제</i></button></li>';

            $('#uploadPartnerFileSpan_'+f.fileType).append(html);
        }
    }
};
