
$(document).ready(function() {
    $ui.init();

});

var $ui =  window.$ui || {},
    tabsSwiper,
    mainSlider = null;

;(function ($, win, doc, undefined) {
    $ui = {
        init : function(){
            $ui.lnbNav();
            // $ui.pageTab();
            // $ui.addTab();
            // $ui.removeTab();
            $ui.sticky();
            $ui.tblHeadFixed();
            //$ui.dropDown();
            $ui.inputLayer();

            //polifill closest
            if (!Element.prototype.matches) {
                Element.prototype.matches = Element.prototype.msMatchesSelector ||
                    Element.prototype.webkitMatchesSelector;
            }
            if (!Element.prototype.closest) {
                Element.prototype.closest = function(s) {
                    var el = this;
                    do {
                        if (el.matches(s)) return el;
                        el = el.parentElement || el.parentNode;
                    } while (el !== null && el.nodeType === 1);
                    return null;
                };
            }
        },

        pageTab : function(){
            function tabAction(){
                var currentTab = $('.swiper-slide.active').index();
                tabsSwiper = new Swiper('.swiper-container',{
                    slidesPerView: 'auto',
                    initialSlide : currentTab,
                    resizeReInit : true,
                    visibilityFullFit: true,
                    noSwiping: true,
                    noSwipingClass: 'no-swipe'
                });

                var tabWrapW = $('.swiper-container').outerWidth(),
                    tabsW = $('.swiper-wrapper').outerWidth();

                if( tabsW > tabWrapW ) {
                    $('.tab-arrows').show();
                    //$('.swiper-wrapper').removeClass('no-swipe');
                } else {
                    $('.tab-arrows').hide();
                    //$('.swiper-wrapper').addClass('no-swipe');
                }
            }

            $('.tab-arrows .btn-tab-prev').on('click', function(e){
                e.preventDefault();
                tabsSwiper.swipePrev();
            });
            $('.tab-arrows .btn-tab-next').on('click', function(e){
                e.preventDefault();
                tabsSwiper.swipeNext();
            });

            var docWidth = $(win).width() - 210;
            function tabResize(docW){
                if($('.wrap').hasClass('wide')) {
                    $('#menuTab').css({
                        'width': '100%'
                    });
                } else {
                    $('#menuTab').css({
                        'width': docW
                    });
                }
                tabAction();
            }

            tabResize(docWidth);

            $(win).on('resize', function(){
                var docWidth = $(win).width() - 210;
                tabResize(docWidth);
            });
        },

        addTab : function(e){
            $('#uiNav a').on('click', function(){
                var tabName = $(this).html();
                var tabNameGroup = [];
                for (var i = 0; i < $('.swiper-slide').length; i++ ){
                    tabNameGroup.push($('.swiper-slide').eq(i).find('a span').html());
                }

                var tabIndex = tabNameGroup.indexOf(tabName);
                if ( tabIndex == -1) {
                    var tabHtml = '';
                    tabHtml += '<a href="#none"><span>'+ tabName +'</span></a>';
                    tabHtml += '<button type="button" class="btn-pin"><i class="hide">탭 고정</i></button>';
                    tabHtml += '<button type="button" class="btn-tab-close"><i class="hide">닫기</i></button>'
                    tabsSwiper.appendSlide(tabHtml);
                    $ui.pageTab();
                    $('.swiper-slide').removeClass('active').eq(tabIndex).addClass('active');
                } else {
                    tabsSwiper.swipeTo(tabIndex);
                    $('.swiper-slide').removeClass('active').eq(tabIndex).addClass('active');
                }
            })
        },

        removeTab : function(e){
            $(doc).off('click.removeTabClick').on('click.removeTabClick', '#menuTab .btn-tab-close', function(){
                var t = $(this).parents('.swiper-slide'),
                    tabIndex = t.index();

                if( t.hasClass('active')) {
                    tabMovePrev(tabIndex);
                } else {
                    tabsSwiper.removeSlide(tabIndex);
                }
            })

            function tabMovePrev(idx) {
                tabsSwiper.removeSlide(idx);
                $ui.pageTab();
                tabsSwiper.swipeTo(idx-1);
                $('.swiper-slide').removeClass('active').eq(idx-1).addClass('active swiper-slide-active');
            }
        },

        sticky : function(){

            function stickyMenu(){
                var sct = $(doc).scrollTop(),
                    width = $('#menuTab').outerWidth();

                if(sct > 40){
                    $('#menuTab').css({
                        'width': width
                    }).addClass('fix');
                    $('.lnb-wrap , .sticky-wrap').addClass('fix');
                }else if(sct < 40){
                    $('#menuTab , .lnb-wrap , .sticky-wrap').removeClass('fix');
                }
            }

            //setTimeout(function(){
            stickyMenu();
            //},300)


            $(win).scroll(function(){
                stickyMenu()
            });
        },

        lnbNav : function(){
            var hide = false,
                curr = $('#uiNav .acco').find('.active').closest('.acco').parent('li').index() > 0 ?
                    $('#uiNav .acco').find('.active').closest('.acco').parent('li').index() : null;

            $('#uiNav').uiAcco({
                current : curr
            });

            $('.btn-lnb-toggle').off('click').on('click', function(){

                $('.wrap').toggleClass('wide').delay(300).queue(function () {
                    //dhtmlx tabbar, cell의 사이즈를 강제로 100% 처리
                    $('.dhxtabbar_cont').css({'width': '100%'});
                    $('div.dhxtabbar_tabs.dhxtabbar_tabs_top').css({'width': '100%'});
                    $('.dhxtabbar_tabs div.dhxtabbar_tabs_top div.dhxtabbar_tabs_base').css({'width': '100%'});
                    $('.dhx_cell_tabbar').css({'width': '100%'});
                    $('.dhx_cell_cont_tabbar').css({'width': '100%'});
                }).dequeue();

                hide = !hide;
                hide ? $(this).addClass('close').find('i').html("사이드 메뉴 열기")
                    : $(this).removeClass('close').find('i').html("사이드 메뉴 닫기");

                if( !(mainSlider == null) ) {
                    //setTimeout(function(){
                    mainSlider.reloadSlider();
                    //},100)
                }
                // $ui.pageTab();
            });
        },

        tblHeadFixed : function(){
            var $table = $('.tbl-fixed table'),
                $tbodyCell = $table.find('tbody tr:first').children(),
                colWidth;

            $(win).resize(function() {
                colWidth = $tbodyCell.map(function() {
                    return $(this).width();
                }).get();

                $table.find('thead tr').children().each(function(i, v) {
                    $(v).width(colWidth[i]);
                });
            }).resize();
        },

        popOpen : function(name, url, width, height, scroll, pos) {
            scroll = scroll || false;
            scroll = (eval(scroll)) ? "yes" : "no";

            pos = (!pos || (typeof pos != "object"))?{}:pos;
            pos.left = pos.left || "auto", pos.top = pos.top || "auto"

            if(scroll=="yes") width = eval(width)+17;
            window.open(url, name, ('width='+width+', height='+height+', toolbar=no, menubar=no, location=no, scrollbars='+scroll+', status=no, resizable=no, left='+pos.left+', top='+pos.top));
        },

        allNavOpen : function(v){
            var scrollPos = $(win).scrollTop();
            var $id = $('#' + v),
                navHtml = '';
            navHtml += $('.nav').html();
            navHtml += '<button type="button" class="btn-close-modal">닫기</button>';
            $id.append(navHtml);

            $('body').css('overflow', 'hidden');
            $('.modal-wrap').show().append($id);
            $id.before('<div class="dim"></div>').show().attr('tabindex', -1).focus();
            $('.wrap').attr('aria-hidden', true);
            $id.find('.acco-btn').removeAttr('aria-expended').off('click');
            $id.find('.acco-arr').remove();
            $id.find('.acco-pnl').removeAttr('style aira-labelledby role');

            scrollPos < 40 ? $id.css('top', '40px') : $id.css('top', '0')

            // $(doc).off('keydown').on('keydown', function(e){
            //     if ( e.which.toString() == 27) {
            //         allNavClose();
            //     }
            // });

            $(doc).off('click.allNavClose').on('click.allNavClose', '.btn-close-modal',  function(){
                allNavClose();
            });

            function allNavClose(){
                $('.wrap').attr('aria-hidden', false);
                $('body').css('overflow', '');
                $('.modal-wrap').hide();
                $id.html('').attr('tabindex','').hide();
                $('.dim').remove();
                $('.btn-all-nav').after($id).focus();
            }

        },

        dropDown : function(){
            $('.box-drop').hide();
            $('.btn-drop').on('click', function(){
                $(this).siblings('.box-drop').toggleClass('on').slideToggle();
            });
            $(doc).on('click',function(e){
                if( !($(e.target).is('.btn-drop, .btn-drop i')) ) {
                    if ( $('.box-drop').hasClass('on') ) {
                        $('.box-drop').slideUp().removeClass('on');
                    }
                }
            });
        },

        inputLayer : function(){
            $('.box-srh .inp-base').on('focus.inpLayer', function (){
                $('.layer-srh').removeClass('on');
                $(this).closest('.box-srh').find('.layer-srh').addClass('on');
            });
            $(doc).on('click',function(e){
                if( !($(e.target).is('.box-srh .inp-base')) ) {
                    if ( $('.layer-srh').hasClass('on') ) {
                        $('.layer-srh').removeClass('on');
                    }
                }
            });
            $('.layer-srh button:last').on('focusout.inpLayer', function(){
                if ( $('.layer-srh').hasClass('on') ) {
                    $('.layer-srh').removeClass('on');
                }
            });
        },

        modalOpen : function (id, callback){
            var $id = $('#' + id),
                btn = document.activeElement,
                $lastFocus = $id.find('.ui-modal-close:last'),
                winHeight = $(win).outerHeight(),
                modalHeight = '',
                contHeight = '',
                psfixL = $('.modal-noti').length,
                modalTimer;

            /* 201126 수정 */
            if (psfixL) {
                $('.modal-noti').parent().addClass('wrap-modal-noti');
                if ($id.hasClass('modal-noti')) {
                    for (var i = 0; i < psfixL; i++) {
                        $('.modal-noti').eq(i).css({
                            top: 10 + (20 * i) + 'px',
                            left: 10 + (20 * i) + 'px'
                        })
                    }
                }
            } else {
                $('body').css('overflow', 'hidden');
                $('.wrap').attr('aria-hidden', true);
                $('.dim').length ? '' : $('.modal-wrap').prepend(
                    '<div class="dim"></div>');
            }

            $('.modal-wrap').show();
            $('.modal').attr({'tabindex': '', 'aria-modal':'true'});
            $id.addClass('open').show().attr({
                'tabindex' : 0,
                'data-focus' : 'focus-' + id,
                'data-focus-prev' : 'focus-' + id +'-close'
            }).focus();

            lastFocus = $id.find('.ui-modal-close:last');
            $lastFocus.attr({
                'data-focus' : 'focus-' + id +'-close',
                'data-focus-next' : 'focus-' + id
            });
            $(btn).attr('data-focus-end', 'focus-'+ id +'-end');

            modalHeight = $id.outerHeight();
            contHeight = $id.find('.modal-cont').outerHeight();
            if ( winHeight < modalHeight) {
                $id.find('.modal-cont').css({
                    'height': winHeight - 200
                })
                modalHeight = $id.outerHeight();
                $id.css('margin-top', -(modalHeight/2)+'px');
            } else {
                $id.css('margin-top', -(modalHeight/2)+'px');
            }

            $(doc).off('keydown.tabMove').on('keydown.tabMove', '[data-focus-prev], [data-focus-next]', function (e) {
                var next = $(e.target).attr('data-focus-next'),
                    prev = $(e.target).attr('data-focus-prev'),
                    target = next || prev || false;

                if( e.keyCode == 9 && next == target && !e.shiftKey ) {
                    $('[data-focus="' + target + '"]').focus();
                }

                if( e.shiftKey && prev == target && e.keyCode == 9) {
                    var keyTimer = setTimeout(function () {
                        $('[data-focus="' + target + '"]').focus();
                    }, 1);
                }
            });

            callback ?  callback() : '';

            $ui.tblHeadFixed();
        },

        modalClose : function (id, callback){
            var $id = $('#' + id),
                modalTimer;
            if ($('.modal.open').length > 1 ){
                $id.hide().removeClass('open').attr('tabindex', '');
                $id.find('.modal-cont').css('height','auto');
                $('[data-focus-end="'+'focus-'+id+'-end'+'"]').focus().removeAttr('data-focus-end');
            } else {
                $('body').css('overflow', '');
                $('.wrap').attr('aria-hidden', false);
                $('.modal-wrap').hide();
                $('.dim').remove();
                $id.hide().removeClass('open').attr('tabindex', '');
                $id.find('.modal-cont').css('height','auto');
                $('[data-focus-end="'+'focus-'+id+'-end'+'"]').focus().removeAttr('data-focus-end');
            }

            callback ?  callback() : '';
        }
    }


})(jQuery, window, document);


// ui tab
;(function ($, win, doc, undefined) {

    $.fn.uiTab = function(options) {
        var opt = $.extend({}, $.fn.uiTab.defaults, options);

        return this.each(function() {
            var $id = $(this).attr('id'),
                $nav = $(this).find('tab-nav'),
                $btn = $(this).find('.tab-btn'),
                $pnl = $(this).find('.tab-pnl'),
                len =  $btn.length,
                current = opt.current;

            $nav.attr('role', 'tablist');
            $btn.attr({
                'role': 'tab',
                'aria-selected' : false,
                'tabindex' : -1
            })
            .eq(current).attr('aria-selected' , true).removeAttr('tabindex').addClass('active');
            $pnl.attr({
                'role' : 'tabpanel',
                'aria-hidden' : true
            }).hide()
            .eq(current).attr('aria-hidden', false).show().addClass('active');

            for( var i = 0; i < len; i++) {
                $btn.eq(i)
                .attr('id', $id + '-btn' + i)
                .attr('aria-controls', $pnl.eq(i).attr('id'));
                $pnl.eq(i)
                .attr('id', $id + '-pnl' + i)
                .attr('aria-labelledby', $btn.eq(i).attr('id'));
            }

            $btn.off('click.uiTab keydown.uiTab').on({
                'click.uiTab' : actClick,
                // 'keydown.uiTab' : actKeydown
            });

            function actClick(e){
                var current = $(this).index();
                tabAct(e, current);
            }

            function tabAct(e, curr) {
                e.preventDefault();

                $btn.attr({
                    'aria-selected' : false,
                    'tabindex' : -1
                }).removeClass('active')
                .eq(curr).attr('aria-selected', true).removeAttr('tabindex').addClass('active').focus();
                $pnl.attr('aria-hiddne', true).removeClass('active').hide()
                .eq(curr).attr('aria-hidden', false).addClass('active').show();
            }

            function actKeydown(e){
                var $t = $(this),
                    n = Number($t.index()),
                    m = len,
                    key = e.which.toString();

                switch(key){
                    case '38': upLeftKey(e);
                        break;

                    case '37': upLeftKey(e);
                        break;

                    case '40': downRightKey(e);
                        break;

                    case '39': downRightKey(e);
                        break;

                    case '35': endKey(e);
                        break;

                    case '36': homeKey(e);
                        break;
                }

                function upLeftKey(e) {
                    e.preventDefault();
                    !(n == 0) ? tabAct(e, n - 1 ): '';
                }
                function downRightKey(e) {
                    e.preventDefault();
                    !(n == m-1) ? tabAct(e, n + 1 ) : '';
                }
                function endKey(e) {
                    e.preventDefault();
                    tabAct(e, m -1 );
                }
                function homeKey(e) {
                    e.preventDefault();
                    tabAct(e, 0);
                }
            }

        });
    };

    $.fn.uiTab.defaults = {
        current : 0
    };

})(jQuery, window, document);


// ui accordion
;(function ($, win, doc, undefined) {

    $.fn.uiAcco = function(options) {
        var opt = $.extend({}, $.fn.uiAcco.defaults, options);

        return this.each(function() {
            var $id = $(this).attr('id'),
                $acco = $(this).find('.acco'),
                $btn = $(this).find('.acco-btn'),
                $pnl = $(this).find('.acco-pnl'),
                len =  $acco.length,
                current = opt.current,
                toggle = opt.toggle;

            $btn.attr('aria-expended', false).append('<i class="acco-arr hide">닫힘</i>');
            $pnl.attr('role','region').hide();

            for( var i = 0; i < len; i++) {
                $btn.eq(i)
                .attr('id', $id + '-btn' + i)
                .attr('aria-controls', $pnl.eq(i).attr('id'));
                $pnl.eq(i)
                .attr('id', $id + '-pnl' + i)
                .attr('aria-labelledby', $btn.eq(i).attr('id'));
            }

            if ( !(current == null) && !(current == 'all') ) {
                $acco.eq(current).addClass('active');
                $btn.eq(current).attr('aria-expended',true).find('.acco-arr').html('열림');
                $pnl.eq(current).show();
            } else if ( current == 'all' ) {
                $acco.addClass('active');
                $btn.attr('aria-expended',true).find('.acco-arr').html('열림');
                $pnl.show();
            }

            $btn.off('click.uiacco keydown.uiAcco').on({
                'click.uiacco' : actClick,
            });

            function actClick(e){
                e.preventDefault();
                var $t = $(this),
                    $tAcco = $t.closest('.acco');

                if ( $tAcco.hasClass('active') ) {
                    $tAcco.removeClass('active').find('.acco-pnl').slideUp();
                    $t.attr('aria-expended',false).find('.acco-arr').html('닫힘');
                } else  {
                    if ( toggle ) {
                        $acco.removeClass('active');
                        $btn.attr('aria-expended',false).find('.acco-arr').html('닫힘');
                        $pnl.slideUp();
                        $tAcco.addClass('active').find('.acco-pnl').slideDown();
                        $t.attr('aria-expended',true).find('.acco-arr').html('열림');
                    } else {
                        $tAcco.addClass('active').find('.acco-pnl').slideDown();
                        $t.attr('aria-expended',true).find('.acco-arr').html('열림');
                    }
                }
            }
        });
    };

    $.fn.uiAcco.defaults = {
        current : null,
        toggle : true
    };

})(jQuery, window, document);

//ui tooltip
;(function ($, win, doc, undefined) {

    $.fn.uiTooltip = function(options) {
        var opt = $.extend({}, $.fn.uiTooltip.defaults, options);

        return this.each(function() {
            var $tip = $(this).attr('id'),
                $btn = $('[aria-describedby="'+ $tip +'"]'),
                visible = opt.visible,
                clickable = opt.clickable,
                sp = 10,
                ps = opt.ps,
                off_t, off_l, w, h, bw, bh, st, sl, timer,
                class_ps = 'ps-ct ps-cb ps-lt ps-lb ps-rt ps-rb';

            if (visible !== null) {
                visible ? tooltipSet($tip) : tooltipHide();
            }

            if ( !clickable ) {
                $btn
                .off('mouseover.ui focus.ui').on('mouseover.ui focus.ui', function(e){
                    e.preventDefault();
                    tooltipSet($tip);
                })
                .off('mouseleave.ui focusout.ui').on('mouseleave.ui focusout.ui', function(){
                    tooltipHideDelay();

                    $('.ui-tooltip')
                    .on('mouseover.ui', function(){
                        clearTimeout(timer);
                    })
                    .on('mouseleave.ui', function(e){
                        tooltipHideDelay();
                    });
                })
            } else {
                $btn
                .off('click.ui focus.ui').on('click.ui focus.ui', function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    tooltipHide();
                    tooltipSet($tip);
                })

                $(doc).off('click.ui').on('click.ui', function(e){
                    tooltipHideDelay();
                })
            }

            $btn
            .off('touchstart.uitooltip').on('touchstart.uitooltip', function(e){
                e.preventDefault();
                if (!$(this).data('view')){
                    $(this).data('view', true);
                    tooltipHide();
                    tooltipSet($tip);
                } else {
                    $(this).data('view', false);
                    tooltipHide();
                }
            });

            function tooltipSet(v) {
                $('#' + v).removeClass(class_ps);
                id = v;
                off_t = $btn.offset().top;
                off_l =$btn.offset().left;
                w = $btn.outerWidth();
                h = $btn.outerHeight();
                bw = $(win).innerWidth();
                bh = $(win).innerHeight();
                st = $(doc).scrollTop();
                sl = $(doc).scrollLeft();

                tooltipShow(off_t, off_l, w, h, bw, bh, st, sl, id, false);
            }
            function tooltipHide() {
                $('.ui-tooltip').removeAttr('style').attr('aria-hidden', true).removeClass(class_ps);
            }
            function tooltipHideDelay(){
                timer = setTimeout(tooltipHide, 100);
            }

            function tooltipShow(off_t, off_l, w, h, bw, bh, st, sl, id) {
                var $id = $('#' + id),
                    pst = (bh / 2 > (off_t - st) + (h / 2)) ? true : false,
                    psl = (bw / 2 > (off_l - sl) + (w / 2)) ? true : false,
                    tw = $id.outerWidth(),
                    th = $id.outerHeight(),
                    ps_l, ps_r, cursorCls = 'ps-';

                if (psl) {
                    if (off_l - sl > tw / 2) {
                        cursorCls += 'c';
                        ps_l = off_l - (tw / 2) + (w / 2);
                    } else {
                        cursorCls += 'l';
                        ps_l = off_l;
                    }
                } else {
                    if (bw - (off_l - sl + w) > tw / 2) {
                        cursorCls += 'c';
                        ps_r = Math.ceil(off_l) - (tw / 2) + (w / 2);
                    } else {
                        cursorCls += 'r';
                        ps_r = off_l - tw + w;
                    }
                }

                ps ? cursorCls = 'ps-l' : '';
                ps ? ps_l = off_l : '';
                ps ? psl = true : '';
                pst ? cursorCls += 'b' : cursorCls += 't';

                $id.addClass(cursorCls).attr('aria-hidden', false).css({
                    display:'block'
                }).css({
                    top : pst ? off_t + h + sp : off_t - th - sp,
                    left : psl ? ps_l : ps_r
                });
            }
        });
    };

    $.fn.uiTooltip.defaults = {
        visible: false,
        clickable : false,
        ps: false
    };

})(jQuery, window, document);