/** Main Script */
var zipcodeManageMain = {
    /**
     * init 이벤트
     */
    init: function() {
        zipcodeManageMain.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        $("#schBtn").bindClick(zipcodeManageMain.search);                       // [검색] 버튼
        $("#schResetBtn").bindClick(zipcodeManageMain.reset);                   // [초기화] 버튼
        $("#schExcelDownloadBtn").bindClick(zipcodeManageMain.excelDownload);   // [엑셀다운] 버튼

        // 그리드 상단 [제주권변경] [도서산간변경] [해당없음변경] 버튼
        $("#chgJejuBtn, #chgIslandBtn, #chgNoneBtn").on("click", function() {
            var checkRowIds = zipcodeManageGrid.gridView.getCheckedRows();
            if (checkRowIds.length === 0) {
                alert("선택된 행이 없습니다.");
                return;
            }
            // 클릭한 버튼에 따라 도서산간유형 update 요청
            var clickBtnType = $(this).attr("id");
            if (clickBtnType === "chgJejuBtn") {
                zipcodeManageMain.updateIslandType(checkRowIds, "JEJU");
            } else if (clickBtnType === "chgIslandBtn") {
                zipcodeManageMain.updateIslandType(checkRowIds, "ISLAND");
            } else if (clickBtnType === "chgNoneBtn") {
                zipcodeManageMain.updateIslandType(checkRowIds, "ALL");
            }
        });
    },

    /**
     * 도로명 검색시, '시/도' 를 선택하면 시군구 리스트를 불러와야함
     */
    getSigunguList: function() {
        var sidoTypeVal = $("#schSidoType").val();
        // df box 면 조회하지 않음
        if (sidoTypeVal === "") {
            return;
        }
        // 부르기 전에 초기화 작업
        $("#schSigunguType option").remove();
        $("#schSigunguType").html('<option value>시군구 선택</option>');
        CommonAjax.basic({
            url         : '/escrow/standard/getZipcodeSigunguList.json?sidoNm=' + sidoTypeVal,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                zipcodeManageMain.drawSigunguList(res);
            }
        });
    },
    /**
     * 시군구 리스트 그리기
     */
    drawSigunguList: function(res) {
        // 시군구가 없는 경우 (ex.세종특별자치시) 전체 option 만 그리기
        if (res.length <= 1) {
            $("#schSigunguType").append("<option value='ALL'>전체</option>");
            return;
        }

        res.forEach(function(sigungu) {
            var appendVal = "<option value='" + sigungu + "'>" + sigungu + "</option>";
            $("#schSigunguType").append(appendVal);
        });
    },

    /**
     * 데이터 검색
     */
    search: function() {
        // Form 데이터 유효성 체크
        if (!zipcodeManageMain.valid()) {
            return;
        }

        var data = $("#zipcodeManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/standard/getZipcodeManageList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                zipcodeManageGrid.setData(res);
                $('#zipcodeManageSearchCnt').html($.jUtil.comma(zipcodeManageGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 데이터 검색 전 Form 데이터 유효성 체크
     */
    valid: function() {
        // 검색어 조건에 따른 필수 선택값 check
        var schKeywordTypeVal = $("#schKeywordType option:selected").val();
        if (schKeywordTypeVal === "ROADADDR") {
            var schSidoTypeVal = $("#schSidoType option:selected").val();
            if ($.jUtil.isEmpty(schSidoTypeVal)) {
                alert("시/도를 선택해주세요.");
                return false;
            }
            var schSigunguTypeVal = $("#schSigunguType option:selected").val();
            if ($.jUtil.isEmpty(schSigunguTypeVal)) {
                alert("시군구를 선택해주세요.");
                return false;
            }
        } else if (schKeywordTypeVal === "ZIPCODE") {
            var schKeywordVal = $("#schKeyword").val();
            if ($.jUtil.isEmpty(schKeywordVal)) {
                alert("우편번호를 입력해주세요.");
                return false;
            }
            if (!$.jUtil.isAllowInput(schKeywordVal, ['NUM'])) {
                alert("우편번호는 숫자만 입력해주세요.");
                return false;
            }
        }
        // 검색어 2글자 이상 입력 check
        var schKeywordVal = $("#schKeyword").val();
        if (schKeywordVal.length < 2) {
            if (schKeywordTypeVal === "ROADADDR") {
                alert("도로명/건물명을 2글자 이상 입력해주세요.");
            } else {
                alert("검색어를 2글자 이상 입력해주세요.");
            }
            return false;
        }
        return true;
    },
    /**
     * 검색영역 폼 초기화
     */
    reset: function() {
        $("#zipcodeManageSearchForm").resetForm();
    },
    /**
     * 데이터 엑셀 다운로드
     */
    excelDownload: function() {
        if(zipcodeManageGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "우편번호관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        zipcodeManageGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },
    /**
     * 검색어 타입 선택에 따른 Event
     */
    changeKeywordType: function() {
        var keywordType = $("#schKeywordType option:selected").val();
        if (keywordType === "ROADADDR") {
            $("#schSidoType").show();
            $("#schSigunguType").show();
            $("#schKeyword").val("").attr("placeholder", "도로명/건물명 입력");
            $("#schInputBox").removeAttr("style").addClass("mg-t-10");
            $("#schBuildNo").show();
        } else if (keywordType === "GIBUN") {
            $("#schSidoType").hide();
            $("#schSigunguType").hide();
            $("#schKeyword").val("").attr("placeholder", "읍/면/동 입력");
            $("#schInputBox").attr("style", "display: inline").removeClass("mg-t-10");
            $("#schBuildNo").hide();
        } else {
            $("#schSidoType").hide();
            $("#schSigunguType").hide();
            $("#schKeyword").val("").attr("placeholder", "");
            $("#schInputBox").attr("style", "display: inline").removeClass("mg-t-10");
            $("#schBuildNo").hide();
        }
    },
    /**
     * 도서산간여부 변경
     * @param checkRowIds
     * @param islandType
     */
    updateIslandType: function(checkRowIds, islandType) {
        var reqZipcodeArray = [];
        // 한 건 씩 처리 - 중복데이터 제거
        for (var checkRowId in checkRowIds) {
            var checkedRows = zipcodeManageGrid.dataProvider.getJsonRow(checkRowId);
            $.each(checkedRows, function(key, value) {
                // 중복요청을 방지하기 위해, 우편번호가 중복이라면 하나만 들고갈 수 있도록 Array 에 담기
                if (key === "zipcode") {
                    if ($.inArray(value, reqZipcodeArray) === -1 ) {
                        reqZipcodeArray.push(value);
                    }
                }
            });
        }

        // 한 건 씩 처리 - Ajax 요청
        console.log(reqZipcodeArray);
        $.each(reqZipcodeArray, function(index, value) {
            var formData = {};
            formData.zipcode = value;
            formData.islandType = islandType;
            zipcodeManageMain.updateIslandTypeAjax(formData, islandType);
        });
    },
    updateIslandTypeAjax: function(formData, islandType) {
        CommonAjax.basic({
            url         : '/escrow/standard/updateZipcodeIslandType.json',
            data        : JSON.stringify(formData),
            method      : "POST",
            contentType: 'application/json',
            callbackFunc: function(res) {
                // update 성공시 alert 메세지 노출 및 검색 새로고침
                if (res > 0) {
                    var resultIslandTypeTxt;
                    if (islandType === "JEJU") {
                        resultIslandTypeTxt = "제주";
                    } else if (islandType === "ISLAND") {
                        resultIslandTypeTxt = "도서산간";
                    } else if (islandType === "ALL") {
                        resultIslandTypeTxt = "해당없음";
                    }
                    alert("우편번호 [" + value + "] 의\n도서산간여부가 [" + resultIslandTypeTxt + "] 로 변경되었습니다.");
                    zipcodeManageMain.search();
                }
            }
        });
    }
};

/** Grid Script */
var zipcodeManageGrid = {
    gridView: new RealGridJS.GridView("zipcodeManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        zipcodeManageGrid.initGrid();
        zipcodeManageGrid.initDataProvider();
        zipcodeManageGrid.event();
    },
    initGrid: function () {
        zipcodeManageGrid.gridView.setDataSource(zipcodeManageGrid.dataProvider);
        zipcodeManageGrid.gridView.setStyles(zipcodeManageGridBaseInfo.realgrid.styles);
        zipcodeManageGrid.gridView.setDisplayOptions(zipcodeManageGridBaseInfo.realgrid.displayOptions);
        zipcodeManageGrid.gridView.setColumns(zipcodeManageGridBaseInfo.realgrid.columns);
        zipcodeManageGrid.gridView.setOptions(zipcodeManageGridBaseInfo.realgrid.options);
        // 같은 우편번호끼리 셀 합병하기 위해 아래 속성 추가
        zipcodeManageGrid.gridView.setColumnProperty("zipcode", "mergeRule", {criteria:"value"});
    },
    initDataProvider: function () {
        zipcodeManageGrid.dataProvider.setFields(zipcodeManageGridBaseInfo.dataProvider.fields);
        zipcodeManageGrid.dataProvider.setOptions(zipcodeManageGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 같은 우편번호 끼리 체크/체크해제 함께하는 스크립트
        zipcodeManageGrid.gridView.onItemChecked = function(gridView, index, checked) {
            // 선택한 행의 zipcode 값 가져오기
            var zipcode = zipcodeManageGrid.dataProvider.getJsonRow(gridView.getRowsOfItems([index])).zipcode;
            console.log("선택한 zipcode: " + zipcode + ", checked: " + checked);

            // 같은 zipcode 를 가진 행 찾아서 선택하게 하기
            var indexArray = [];
            for (var i=0; i<$("#zipcodeManageSearchCnt").text(); i++) {
                var currentRow = zipcodeManageGrid.dataProvider.getJsonRow(i);
                if (zipcode === currentRow.zipcode) {
                    indexArray.push(i);
                }
            }
            zipcodeManageGrid.gridView.checkRows(indexArray, checked);
        };
    },
    setData: function (dataList) {
        zipcodeManageGrid.dataProvider.clearRows();
        zipcodeManageGrid.dataProvider.setRows(dataList);
        // 도로명주소로 초기 Sorting 하기 위해 아래 속성 추가
        zipcodeManageGrid.gridView.orderBy(["roadAddrFullTxt"], ["ascending"]);
    }
};