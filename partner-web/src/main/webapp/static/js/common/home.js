/**
 * 파트너 홈
 */

$(document).ready(function() {
    home.init();
});

//홈 공통
var home = {
    generalSelect : {title:"제목", titleContents:"제목+내용"},
    harmSelect : {name:"제품명", nmrpttype:"유형", institution:"검사기관", manufacture:"제조업소", seller:"판매업소"},
    init : function() {
        tabDiv = 'GENERAL';
        home.searchTypeSetting(tabDiv);
        home.event();
        home.getProdStatusCount();
    },
    event : function() {
		// 상단 탭 클릭
		$('.tab_vicinity li').on('click', function (){
			$('.tab_vicinity li').removeClass('on');
			$(this).addClass("on");

			tabDiv = $(this).data("type");
			home.searchTypeSetting(tabDiv);
			switch (tabDiv) {
                case 'GENERAL' :
                    $('#harmGridTotalDiv').hide();
                    $('#harmDetail').hide();
                    $('#cnoticeGridTotalDiv').show();
                    if ($('#cnoticeNo').text()) {
                        $('#cnoticeDetail').show();
                    }
                    break;
                case 'HARM' :
                    $('#cnoticeGridTotalDiv').hide();
                    $('#cnoticeDetail').hide();
                    $('#harmGridTotalDiv').show();
                    if ($('#noDocument').text()) {
                        $('#harmDetail').show();
                    }
                    break;
            }
		});

		//MD영역 대시보드 클릭
        $('.prodStatusCnt').on('click', function(){
            home.parentAddTab($(this).data('sch-type'), $(this).attr('id'));
        });

        //검색 버튼
        $('#searchBtn').on('click', function(){
            home.search();
        });

        //초기화 버튼
        $('#searchInitBtn').on('click', function(){
            $('#schType').find('option:first').prop('selected', true);
            $('#schValue').val('');
        });

        //검색어 칸 엔터
        $('#schValue').on("keypress", function (e) {
            if (e.keyCode === 13) {
                home.search();
            }
        });
    },

    //검색
    search : function(){
        switch (tabDiv) {
            case 'GENERAL' :
                cnotice.search();
                break;
            case 'HARM' :
                harmManage.search();
                break;
        }
    },

    parentAddTab : function (schType, schVal) {
        parent.initialTab.addTab('menu111', '상품 등록/수정', '/product/prodMain?schCalDate=-1w&schMdEmpNo=' + loginEmpNo + '&' + schType + '=' + schVal);
    },

    getProdStatusCount : function () {
        if ($('#mdDashBoard').length > 0) {
            CommonAjax.basic({url: '/home/getProdStatusCount.json', method: 'GET', callbackFunc: function (res) {
                $('#SCH_P2_HMP').html(res.hmpApprWaitCnt);
                $('#SCH_P2_SELLER').html(res.sellerApprWaitCnt);
                $('#SCH_P4_HMP').html(res.hmpApprRejectCnt);
                $('#SCH_P4_SELLER').html(res.sellerApprRejectCnt);
                $('#SCH_P5').html(res.judgeReqCnt);
                $('#SCH_P6').html(res.judgeApprRejectCnt);

                $('#P10_1_SELLER').html(res.sellerAgreeWaitCnt);
                $('#P10_1_HMP').html(res.hmpAgreeWaitCnt);
                $('#P10_2_SELLER').html(res.sellerAgreeApprCnt);
                $('#P10_2_HMP').html(res.hmpAgreeApprCnt);
                $('#P10_3_SELLER').html(res.sellerAgreeRejectCnt);
                $('#P10_3_HMP').html(res.hmpAgreeRejectCnt);
            }});
        }
    },

    //검색어 타입 셋팅
    searchTypeSetting : function(tabDiv){
        var schType;
        switch(tabDiv) {
            case 'GENERAL' :
                schType = home.generalSelect;
                break;
            case 'HARM' :
                schType = home.harmSelect;
                break;
        }
        $('#schType').html("");
        $.each(schType, function (val, txt) {
            $('#schType').append($('<option>', {
                value: val,
                text : txt
            }));
        });
    },
};


//일반공지 관련
var cnotice = {

    //일반공지 상세 조회
    getCnoticeDetail : function (selectRowId) {
        var noticeNo = GridGetValueByRowId(cnoticeGrid, "NO", selectRowId);
        CommonAjax.basic({url: '/home/getNoticeDetail.json', data: {noticeNo:noticeNo}, method: 'GET', callbackFunc: function (res) {

            $('#cnoticeNo').text(res.noticeNo);
            $('#cnoticeTitle').text(res.noticeTitle);
            $('#cnoticeRegDt').text(res.regDt);
            $('#cnoticeDesc').html(res.noticeDesc);

            if(res.file.length == 0) {
                $('#cnoticeAttach').hide();
            } else {
                var file = '';
                file += '<th>첨부파일</th>';
                file += '<td colspan="2">';
                for(var i in res.file) {
                    var data = res.file[i];
                    var fileUrl = data.fileUrl;
                    file +=    '<ul>';
                    file +=    '<input type="hidden" fileNm="' + data.fileNm + '"id="' + data.fileNm + '_' + i + '" value="' + fileUrl + '">';
                    file +=    '<a href="javascript:void(0);" onclick="cnotice.fileDownload(this);"><div id="fileNm" type="text">' + data.fileNm + '</div></a>';
                    file +=    '</ul>';
                }
                file +=  '</td>';
                $('#cnoticeAttach').html(file);
                $('#cnoticeAttach').show();
            }
        }});
        $('#cnoticeDetail').show();
    },

    //첨부파일 다운로드
    fileDownload : function (obj) {
        var uri = $(obj).siblings("input").val();
        var filenm = $(obj).siblings("input").attr('fileNm');
        var link = document.createElement('a');
        if (typeof link.download === 'string') {
            document.body.appendChild(link); // Firefox requires the link to be in the body
            link.download = filenm;
            link.href = uri;
            link.target = '_blank';
            link.click();
            document.body.removeChild(link); // remove the link when done
        } else {
            location.replace(uri);
        }
    },

    //검색
    search : function(){
        var url = $('#searchForm').serialize();
        GridResponseFill(cnoticeGrid, '/home/getAdminNoticeList.json?'+url, true, function(res){
                $('#cnoticeSearchCnt').html($.jUtil.comma(res.length));
        });
    }
};

var harmManage = {
    harmGridSelectRow: null,
    
    init: function(){
        harmManage.search();
    },
    // 검색
    search: function(func){
        //상세정보 clear - 홈화면에서는 검색시 상세영역 초기화하지 않기(박민경 대리 요청)
        //harmManage.clearHarmProdDetail();

        var data = $('#searchForm').serialize();
        GridResponseFill(harmGrid, '/home/getHarmProductList.json?'+data, true, function() {
            $('#harmListCount').html(GridRowsNum(harmGrid));
        });
    },
    // 위해상품 상세정보 출력
    setHarmProdDetail: function(data) {
        //clear
        harmManage.clearHarmProdDetail();

        $("#noDocument").html(data.noDocument);
        $("#seq").html(data.seq);
        $("#nmProd").html(data.nmProd);
        $("#nmProdtype").html(data.nmProdtype);

        var imgsrc = setImgSrc(data.imgAttach01);
        imgsrc += setImgSrc(data.imgAttach02);
        imgsrc += setImgSrc(data.imgAttach03);
        imgsrc += setImgSrc(data.imgAttach04);
        imgsrc += setImgSrc(data.imgAttach05);

        $("#detailImage").html(imgsrc);
        $("#nmRpttype").html(data.nmRpttype);
        $("#nmCiInsptmachi").html(data.nmCiInsptmachi);
        $("#barcode").html(data.barcode);
        $("#dtMake").html(data.dtMake);
        $("#prdValid").html(data.prdValid);
        $("#unitPack").html(data.unitPack);
        $("#nmManufaccntr").html(data.nmManufaccntr);
        $("#nmManufactype").html(data.nmManufactype);
        $("#nmManufacupso").html(data.nmManufacupso);
        $("#telManufacupso").html(data.telManufacupso);
        $("#noManufacture").html(data.noManufacture);
        $("#addrManufac").html(data.addrManufac + " " + data.addrManufacdtl);
        $("#zipnoManufac").html(data.zipnoManufac);
        $("#biznoManufacupso").html(data.biznoManufacupso);
        $("#biznoSalerupso").html(data.biznoSalerupso);
        $("#nmSalertype").html(data.nmSalertype);
        $("#nmSalerupso").html(data.nmSalerupso);
        $("#telSalerupso").html(data.telSalerupso);
        $("#zipnoSaler").html(data.zipnoSaler);
        $("#addrSaler").html(data.addrSaler + " " + data.addrSalerdtl);
        $("#nmInspttype").html(data.nmInspttype);
        $("#nmTakemachi").html(data.nmTakemachi);
        $("#plcTake").html(data.plcTake);
        $("#dtTake").html(data.dtTake);

        //부적합 시험항목 multi row 추가
        $("#nmExm01abbr").html(data.nmExm01abbr);
        $("#valExm01base").html(data.valExm01base);
        $("#valExm01result").html(data.valExm01result);

        if(!$.jUtil.isEmpty(data.nmExm02abbr)) {
            var exam = '<tr class="exam"><th>부적합 시험항목</th><td colspan="3">'+ data.nmExm02abbr + '</td></tr>';
            exam += '<tr class="exam"><th>부적합 시험항목 규격</th><td colspan="3">'+ data.valExm02base + '</td></tr>';
            exam += '<tr class="exam"><th>부적합 시험항목 결과</th><td colspan="3">'+ data.valExm02result + '</td></tr>';

            $("#exam").before(exam);
        }

        if(!$.jUtil.isEmpty(data.nmExm03abbr)) {
            var exam = '<tr class="exam"><th>부적합 시험항목</th><td colspan="3">'+ data.nmExm03abbr + '</td></tr>';
            exam += '<tr class="exam"><th>부적합 시험항목 규격</th><td colspan="3">'+ data.valExm03base + '</td></tr>';
            exam += '<tr class="exam"><th>부적합 시험항목 결과</th><td colspan="3">'+ data.valExm03result + '</td></tr>';

            $("#exam").before(exam);
        }

        if(!$.jUtil.isEmpty(data.nmExm04abbr)) {
            var exam = '<tr class="exam"><th>부적합 시험항목</th><td colspan="3">'+ data.nmExm04abbr + '</td></tr>';
            exam += '<tr class="exam"><th>부적합 시험항목 규격</th><td colspan="3">'+ data.valExm04base + '</td></tr>';
            exam += '<tr class="exam"><th>부적합 시험항목 결과</th><td colspan="3">'+ data.valExm04result + '</td></tr>';

            $("#exam").before(exam);
        }

        if(!$.jUtil.isEmpty(data.nmExm05abbr)) {
            var exam = '<tr class="exam"><th>부적합 시험항목</th><td colspan="3">'+ data.nmExm05abbr + '</td></tr>';
            exam += '<tr class="exam"><th>부적합 시험항목 규격</th><td colspan="3">'+ data.valExm05base + '</td></tr>';
            exam += '<tr class="exam"><th>부적합 시험항목 결과</th><td colspan="3">'+ data.valExm05result + '</td></tr>';

            $("#exam").before(exam);
        }

        //발신일자 datatype 수정
        $("#dtReport").html(data.dtReport);
        $("#nmRptrmachi").html(data.nmRptrmachi);
        $("#nmReporter").html(data.nmReporter);
        $("#telReporter").html(data.telReporter);
        $("#dhRecv").html(data.dhRecv);
        $("#statusResult").html(data.statusResult);
        $("#dhSendresult").html(data.dhSendresult);

        //전송상태 : CP - 송수신완료, RS - 송신대기
        var statusAppresult = '';
        if (data.statusAppresult == 'CP'){
            statusAppresult = '송수신완료';
        } else if (data.statusAppresult == 'RS') {
            statusAppresult = '송신대기';
        }
        $("#statusAppresult").html(statusAppresult);

        $('#harmDetail').show();
    },
    //clear 위해상품 상세정보
    clearHarmProdDetail: function(){
        $("#noDocument").html('');
        $("#seq").html('');
        $("#nmProd").html('');
        $("#nmProdtype").html('');

        $("#detailImage").html('');
        $("#nmRpttype").html('');
        $("#nmCiInsptmachi").html('');
        $("#barcode").html('');
        $("#dtMake").html('');
        $("#prdValid").html('');
        $("#unitPack").html('');
        $("#nmManufaccntr").html('');
        $("#nmManufactype").html('');
        $("#nmManufacupso").html('');
        $("#telManufacupso").html('');
        $("#noManufacture").html('');
        $("#addrManufac").html('');
        $("#zipnoManufac").html('');
        $("#biznoManufacupso").html('');
        $("#biznoSalerupso").html('');
        $("#nmSalertype").html('');
        $("#nmSalerupso").html('');
        $("#telSalerupso").html('');
        $("#zipnoSaler").html('');
        $("#addrSaler").html('');
        $("#nmInspttype").html('');
        $("#nmTakemachi").html('');
        $("#plcTake").html('');
        $("#dtTake").html('');

        $("#nmExm01abbr").html('');
        $("#valExm01base").html('');
        $("#valExm01result").html('');
        //
        $(".exam").remove();

        $("#dtReport").html('');
        $("#nmRptrmachi").html('');
        $("#nmReporter").html('');
        $("#telReporter").html('');
        $("#dhRecv").html('');
        $("#statusResult").html('');
        $("#dhSendresult").html('');
        $("#statusAppresult").html('');

    },
};

/**
 * 위해상품 목록 Grid ROW 클릭(선택) 이벤트
 */
function harmGrid_onRowSelect(selectRowId){
    var data = {};
    data.cdInsptmachi = GridGetValueByRowId(harmGrid, "검사기관코드", selectRowId);
    data.noDocument = GridGetValueByRowId(harmGrid, "문서번호", selectRowId);
    data.seq = GridGetValueByRowId(harmGrid, "차수", selectRowId);
    data.changeStatusResult = GridGetValueByRowId(harmGrid, "처리상태코드", selectRowId);

    harmManage.harmGridSelectRow = selectRowId;

    //callback event
    var harmSuccessCallback = function(resData){
        harmManage.setHarmProdDetail(resData);
    };

    //상세정보
    harmAjax('/judge/getHarmProductDetail.json', 'get', data, harmSuccessCallback);

}

/**
 * 홈플러스 위해상품 목록 Grid ROW 클릭(선택) 이벤트
 */
function harmProdGrid_onRowSelect(selectRowId){
    var isSelected = harmProdGrid.cells(selectRowId, 0).getValue();

    if (isSelected == 0) {
        harmProdGrid.cells(selectRowId, 0).setChecked(1);
    } else {
        harmProdGrid.cells(selectRowId, 0).setChecked(0);
    }

}

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function harmAjax(url, method, data, successCallback, successMsg){
    var params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;

    //content Type
    $.ajaxSetup({ contentType: "application/json; charset=utf-8", });
    CommonAjax.basic(params);
}

/***
 * 이미지 태그 생성 임시
 *
 * @param url
 * @returns {string}
 */
function setImgSrc(url){
    if (!$.jUtil.isEmpty(url)) {
        var img = '<span style="border:1px solid black;vertical-align: middle; display: inline-block; max-width: 100px; margin: 5px; text-align: center;">'
                  + '<img src="' + url + '" width="100px" height="100px" />'
                  + '<a href="#" onclick="expand_image(this)">크게보기</a></span>';
        return img;
    }
    else {
        return '';
    }
}

function expand_image(obj) {
    var img_src = $(obj).parents('span').find('img').attr('src');

    $("#popup_img").attr('src', img_src);
    $('#popup_wrap').dialog({
                                closeText: '',
                                width: 550,
                                height: 550,
                                autoOpen: false,
                                modal: true,
                                resizable: true,
                                open: function(event, ui) { },
                                close: function(event, ui) {
                                    window.close();
                                }
                            });
    $('#popup_wrap').dialog('open');
}

