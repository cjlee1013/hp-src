/**
 * 폼에서 자주 사용되는 유틸성 함수
 * f : form (object)
 * n : element name (string)
 * d : delimiter (string)
 * v : value (string)
 **/
var _F = {

    /**
     * 라디오, 체크박스 에서 체크된 값이 있는지 여부 체크
     * req ex) _F.isChecked($('#formId'), 'elementName')
     * res ex) true
     **/
    isChecked : function (f, n) {
        return $("input[name=" + n + "]:checked", f).length > 0 ? true : false;
    },
    isChecked : function (n) {
        return $("input[name=" + n + "]:checked").length > 0 ? true : false;
    },

    /**
     * 체크박스에서 체크된 값이 몇 개 있는지 체크
     * req ex) _F.getCount($('#formId'), 'elementName')
     * res ex) 3
     **/
    getCount : function (f, n) {
        return $("input[name=" + n + "]:checked", f).length;
    },
    getCount : function (n) {
        return $("input[name=" + n + "]:checked").length;
    },

    /**
     * 동일 name을 가진 요소들의 value를 원하는 구분자(delimiter)로 이어붙임.
     * req ex) _F.getAllValue($('#formId'), 'elementName', '/')
     * res ex) 정상/일시정지/탈퇴/휴면
     **/
    getAllValue : function (f, n, d) {
        var result = "";
        if (d == undefined) { d = ''; }
        $("input[name=" + n + "]", f).each(function (i, elem) {
            result += $(this).val() + ( $("input[name=" + n + "]", f).length - 1 == i ? "" : d );
        });

        return result;
    },
    getAllValue : function (n, d) {
        var result = "";
        if (d == undefined) { d = ''; }
        $("input[name=" + n + "]").each(function (i, elem) {
            result += $(this).val() + ( $("input[name=" + n + "]").length - 1 == i ? "" : d );
        });

        return result;
    },

    /**
     * 체크박스에서 체크된 값(value)들을 원하는 구분자(delimiter)로 이어붙임.
     * req ex) _F.getCheckedValue($('#formId'), 'elementName', '/')
     * res ex) 정상/일시정지
     **/
    getCheckedValue : function (f, n, d) {
        var result = "";
        $("input[name=" + n + "]:checked", f).each(function (i, elem) {
            result += $(this).val() + ( $("input[name=" + n + "]:checked", f).length - 1 == i ? "" : d );
        });

        return result;
    },
    getCheckedValue : function (n, d) {
        var result = "";
        $("input[name=" + n + "]:checked").each(function (i, elem) {
            result += $(this).val() + ( $("input[name=" + n + "]:checked").length - 1 == i ? "" : d );
        });

        return result;
    },

    /**
     * 체크박스에서 체크되지 않은 값(value)들을 원하는 구분자(delimiter)로 이어붙임.
     * req ex) _F.getUnCheckedValue($('#formId'), 'elementName', '/')
     * res ex) 탈퇴/휴면
     **/
    getUnCheckedValue : function (f, n, d) {
        var result = "";
        $("input[name=" + n + "]:not(:checked)", f).each(function (i, elem) {
            result += $(this).val() + ( $("input[name=" + n + "]:not(:checked)", f).length - 1 == i ? "" : d );
        });

        return result;
    },
    getUnCheckedValue : function (n, d) {
        var result = "";
        $("input[name=" + n + "]:not(:checked)").each(function (i, elem) {
            result += $(this).val() + ( $("input[name=" + n + "]:not(:checked)").length - 1 == i ? "" : d );
        });

        return result;
    },

    /**
     * hidden 타입의 input 태그를 form에 추가한다. (name과 value 필요)
     * 이미 해당 name의 요소가 있는 경우, 기존 요소에 value값만 수정한다.
     * req ex) _F.addHiddenElement($('#formId'), 'newTagName', 'newValue')
     * result <input type="hidden" name="newTagName", value="newValue"> 가 form태그 제일 끝에 추가됨.
     **/
    addHiddenElement : function (f, n, v) {
        var ele;
        for (var i = 0; i < f[0].elements.length; i++) {
            if (n == f[0].elements[i].name) {
                ele = f[0].elements[i];
                break;
            }
        }

        if (!ele) {
            ele = document.createElement("input");
            ele.type = "hidden";
            //ele.id = n;
            ele.name = n;
            ele.value = v;
            f[0].appendChild(ele);
        } else {
            ele.value = v;
        }
    },

    /**
     * form 데이터를 json 타입으로 바꾼다.
     * req ex) _F.getFormDataByJson($('#formId'))
     * res ex) {"radioName":"A","checkboxName":["C","D"],"selectName":"G","textName":"GG"}
     **/
    getFormDataByJson : function (f){
        var unindexed_array = f.serializeArray();
        var param = {};

        $(unindexed_array).each(function(i, v) {
            if (param[v.name]) {
                if (param[v.name].length < 2) {
                    var tmpArray = [];
                    tmpArray.push(param[v.name]);
                    tmpArray.push(v.value);
                    param[v.name] = tmpArray;
                } else {
                    param[v.name].push(v.value);
                }
            } else {
                param[v.name] = v.value;
            }
        });
        return JSON.stringify(param);
    },

    /**
     * 전체 선택 체크박스를 체크/해지 하면 하위 체크박스들이 모두 체크/해지 되도록 한다.
     * use ex) <input type="checkbox" onchange="_F.chkAll('childChkBoxName', this);">전체선택
     *          <input type="checkbox" name="childChkBoxName" value="C">C
     *          <input type="checkbox" name="childChkBoxName" value="D">D
     **/
    chkAll : function(n, allChkObj) {
        $("input[name=" + n + "]").attr('checked', allChkObj.checked);
    },

    /**
     * 체크박스 선택된 값(value)들을 Array에 저장
     * req ex) _F.checkboxArrSetting('checkboxName')
     * res ex) ["C", "E"]
     **/
    checkboxArrSetting : function(n) {
        var valueArr = new Array();
        $('input:checkbox[name="'+n+'"]:checked').each(function(){
            valueArr.push($(this).val());
        });
        return valueArr;
    }

}