/**
 * 자주 사용하는 벨리데이션 모음
 * str : 유효성 검사를 할 대상 텍스트
 * return type : boolean (true/false)
 * 알럿 메시지, 포커싱 처리 등은 원래 페이지에서 각자 구현
 **/
var _V = {

    /**
     * 직접 작성한 정규식으로 체크
     * regExp : 정규식
     **/
    chkAny : function(str, regExp) {
        return regExp.test(str);
    },

    /**
     * 핸드폰 번호 체크
     * barAllowFlag (true/false) : '-' 허용 여부
     **/
    chkCellPhoneNum : function(str, barAllowFlag) {
        var regExp;

        if (barAllowFlag) {
            regExp = /^01([0|1|6|7|8|9])-[0-9]{3,4}-[0-9]{4}$/;
        } else {
            regExp = /^01([0|1|6|7|8|9])[0-9]{3,4}[0-9]{4}$/;
        }
        return regExp.test(str);
    },

    /**
     * 일반 전화번호 체크
     * barAllowFlag (true/false) : '-' 허용 여부
     **/
    chkTelNum : function(str, barAllowFlag) {
        var regExp;

        if (barAllowFlag) {
            regExp = /^[0-9]{2,3}-[0-9]{3,4}-[0-9]{4}$/;
        } else {
            regExp = /^[0-9]{2,3}[0-9]{3,4}[0-9]{4}$/;
        }
        return regExp.test(str);
    },

    /**
     * 숫자 체크
     * commaAllowFlag (true/false) : ',' 허용 여부
     **/
    chkNumber : function(str, commaAllowFlag) {
        var regExp;

        if (commaAllowFlag) {
            regExp = /^[0-9]+$/;
        } else {
            regExp = /[^0-9]/;
        }
        return regExp.test(str);
    },

    /**
     * 소수점 n째자리 체크
     * n : 소수점 아래 n자리
     **/
    chkDecimalPoint : function (str, n) {
        var regExp = new RegExp('^\\d*[.]\\d{' + n + '}$');
        return regExp.test(str);
    },

    /**
     * 날짜(yyyy-mm-dd)형식 체크
     **/
    chkDate : function(str) {
        var regExp = /^(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)$/;
        return regExp.test(str);
    },

    /**
     * 영어 체크
     **/
    chkEnglish : function(str) {
        var regExp = /^[a-zA-Z]+$/;
        return regExp.test(str);
    },

    /**
     * 한글 체크
     **/
    chkKorean : function(str) {
        var regExp = /^[가-힣]+$/;
        return regExp.test(str);
    },

    /**
     * 이메일 체크
     **/
    chkEmail : function(str) {
        var regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/;
        return regExp.test(str);

    },

    /**
     *  URL 체크
     */
    chkUrl : function(str){
        var regExp = /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
        return regExp.test(str);
    },

    /**
     * 사업자 등록번호 체크
     * str 에 '-'가 포함되어 있어도 상관 없다.
     **/
    chkBizNum : function(str) {
        var bizID = str.replace(/-/gi,'');
        var checkID = new Array(1, 3, 7, 1, 3, 7, 1, 3, 5, 1);
        var tmpBizID, i, chkSum=0, c2, remander;
        for (i=0; i<=7; i++) chkSum += checkID[i] * bizID.charAt(i);
        c2 = "0" + (checkID[8] * bizID.charAt(8));
        c2 = c2.substring(c2.length - 2, c2.length);
        chkSum += Math.floor(c2.charAt(0)) + Math.floor(c2.charAt(1));
        remander = (10 - (chkSum % 10)) % 10;
        if (Math.floor(bizID.charAt(9)) == remander) return true;
        return false;
    },

    /**
     * 주민등록번호 체크
     * str에 '-'가 포함되어 있어도 상관 없다.
     **/
    chkRRN : function(str){
        var rrn = str.replace(/-/gi, '');

        var regExp = /^[0-9]{6}[1234][0-9]{6}$/;
        if (!regExp.test(rrn)) return false;

        var birthYear = (rrn.charAt(6) <= "2") ? "19" : "20";
        birthYear += rrn.substr(0, 2);
        var birthMonth = rrn.substr(2, 2) - 1;
        var birthDate = rrn.substr(4, 2);
        var birth = new Date(birthYear, birthMonth, birthDate);
        if (birth.getYear() % 100 != rrn.substr(0, 2) || birth.getMonth() != birthMonth || birth.getDate() != birthDate) return false;

        var buf = new Array(13);
        for (var i = 0; i < 13; i++) buf[i] = parseInt(rrn.charAt(i));
        var multipliers = [2,3,4,5,6,7,8,9,2,3,4,5];
        for (var sum = 0, i = 0; i < 12; i++) sum += (buf[i] *= multipliers[i]);
        if ((11 - (sum % 11)) % 10 != buf[12]) return false;

        return true;
    }
}