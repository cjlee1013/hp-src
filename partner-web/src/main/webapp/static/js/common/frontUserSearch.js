/**
 * 회원검색 팝업 이벤트 JS
 * 회원검색 팝업과 관련 된 이벤트들을 처리한다.
 */
var __core_frontUserSearchPop = "/common/frontUserSearchPop"; //회원검색 팝업호출 url
var __api_frontUserSearch = "/common/frontUserSearchGrid.json"; //resourceClient를 통해 api 조회하는 url
var pattern_number = /^[0-9]*$/;

/**
 * 회원검색 팝업 open 이벤트
 *
 * @param callBackFuncName callback 부모 창의 함수 명
 * @param useMultiSelect 다중선택 여부, true면 다중선택, false면 단일선택, 기본은 false
 *
 * @return value callback Object
 * <pre>
 * 단일선택
 * {
 *   'userId': 회원아이디,
 *   'userName': 회원이름,
 *   'mobile': 핸드폰,
 *   'email': 이메일
 * }
 *
 * 다중선택 array[object]
 * </pre>
 */
function frontUserSearchPopup(callBackFuncName, useMultiSelect) {
    if (typeof callBackFuncName == 'undefined' || callBackFuncName == "") {
        alert('callBackFuncName 이 필요합니다.');
        return;
    }
    if (typeof callBackFuncName != 'string') {
        alert('callBackFuncName 은 문자열만 입력 가능합니다.');
        return;
    }

    //다중선택여부 : 값을 넣지 않으면 default는 false
    if (useMultiSelect == null) {
        useMultiSelect = false;
    }

    var url = __core_frontUserSearchPop + "?useMultiSelect=" + useMultiSelect + "&callBack=" + callBackFuncName;
    return windowPopupOpen(url, callBackFuncName, 822, 531);
}

/**
 * 회원검색 팝업 버튼 이벤트
 */
var buttonEvent = {

    //검색버튼 클릭이벤트
    frontUserSearchAjax: function () {
        var searchType = $("#searchType").val();
        var keyword = $("#keyword").val();

        if (!keyword) {
            alert('검색할 정보를 입력해주세요');
            $('#keyword').focus();
            return false;
        }

        if (searchType == "USERNAME" && keyword.length < 2) {
            alert('최소 2글자 이상 입력해주세요');
            $('#keyword').focus();
            return false;
        } else if (searchType == "MID" && !pattern_number.test(keyword)) {
            alert('회원번호는 숫자만 입력 가능합니다.');
            $('#keyword').focus();
            return false;
        }

        var _url = __api_frontUserSearch;
        _url += '?searchType=' + searchType + '&keyword=' + keyword;


        //그리드영역 클리어
        gridArea.clearAll();

        CommonAjax.basic({
                             url: _url,
                             method: 'get',
                             dataType: 'json',
                             callbackFunc: function (resData) {
                                 if (resData == null || resData.data === undefined) {
                                     alert('정상적으로 검색되지 않았습니다.');

                                 } else if (resData.data.length == 0) {
                                     alert('검색결과가 없습니다.');

                                 } else {
                                     gridArea.clearAll(); //'gridArea' : 그리드영역 ID

                                     var dataList = new Array();
                                     $.each(resData.data, function (i, row) {
                                         var data = new Object();
                                         data.userId = row.userId;
                                         data.userName = row.userName;
                                         data.mobile = row.mobile;
                                         data.email = row.email;

                                         dataList.push(data);
                                     });
                                     gridArea.parse(JSON.stringify(dataList), 'js');
                                 }
                             }
                         });
    },

    //선택버튼 클릭이벤트(callback)
    userSelect: function () {
        //다중선택 여부에 따라 이벤트 변경
        if (useMultiSelect == "true") {
            frontUserMultiSelect();
        } else {
            frontUserSingleSelect();
        }
    },

    //취소버튼 클릭이벤트(창닫기)
    userCancel: function () {
        buttonEvent.closeUserSearch();
    },

    //초기화이벤트
    searchInputReset: function () {
        $("#keyword").val('');
        gridArea.clearAll(); //gridArea : 그리드영역 ID
        $('#keyword').focus();
    },

    //창 닫기
    closeUserSearch: function () {
        window.close();
    }
};

/**
 * 단일 선택시 선택버튼 처리 Function
 * @returns {boolean}
 */
function frontUserSingleSelect() {
    if ($.jUtil.isEmpty(txtUserName) && $.jUtil.isEmpty(txtUserId) && $.jUtil.isEmpty(txtmId)
        && $.jUtil.isEmpty(txtMobile) && $.jUtil.isEmpty(txtEmail)) {
        alert("회원을 선택해주세요");
        return false;
    }
    var result = {
        'userId': txtUserId,
        'userName': txtUserName,
        'mobile': txtMobile,
        'email': txtEmail
    };

    var callBack = 'opener.' + callBackFunction;
    eval(callBack)(result);

    //close
    buttonEvent.closeUserSearch();
}

/**
 * 다중 선택시 처리 Function
 */
function frontUserMultiSelect() {
    var checkedRows = gridArea.getCheckedRows(0);

    if ($.jUtil.isEmpty(checkedRows)) {
        alert("회원을 선택해주세요");
        return false;
    }

    var rowArr = checkedRows.split(',');
    var resultList = new Array();

    for (var i in rowArr) {
        var rowData = new Object();
        rowData.userId = GridGetValueByRowId(gridArea, "회원아이디", rowArr[i]);
        rowData.userName = GridGetValueByRowId(gridArea, "회원이름", rowArr[i]);
        rowData.mobile = GridGetValueByRowId(gridArea, "핸드폰", rowArr[i]);
        rowData.email = GridGetValueByRowId(gridArea, "이메일", rowArr[i]);

        resultList.push(rowData);
    }

    var callBack = 'opener.' + callBackFunction;
    eval(callBack)(resultList);

    //close
    buttonEvent.closeUserSearch();
}

/**
 * 회원검색 팝업 function
 */
$(function () {

    $("#userSearch").on("click", function () {
        buttonEvent.frontUserSearchAjax();
    });

    $('#keyword').on("keypress", function (e) {
        if (e.keyCode === 13) {
            buttonEvent.frontUserSearchAjax();
        }
    });

    $("#userCancel").on("click", function () {
        buttonEvent.userCancel();
    });

    $("#userSelect").on("click", function () {
        buttonEvent.userSelect();
    });

    $("#initEventBtn").on("click", function () {
        buttonEvent.searchInputReset();
    });
});

/**
 * document ready
 */
$(document).ready(function () {
    $('#keyword').focus();
});