/**
 * 공통 - 카테고리
 */

var commonCategory = {
	init : function() {
		this.event();
	},
	event : function() {
	},
	/**
	 * 카테고리 셀렉트박스 생성
	 *
	 * @param depth
	 * @param parentCateCd
	 * @param cateCd
	 * @param selector
     */
	setCategorySelectBox : function(depth, parentCateCd, cateCd, selector, callBack, exceptHdnCate) {
		$(selector).html('<option value="">'+$(selector).data('default')+'</option>');
		if ((!parentCateCd && depth == 1) || (parentCateCd && depth > 1)) {
			CommonAjax.basic({
				 url: '/common/category/getCategoryForSelectBox.json',
				 data: {
					 depth: depth,
					 parentCateCd: parentCateCd,
                     exceptHdnCate : exceptHdnCate //히든프라이스 카테고리 제외 플래그(true:히든프라이스 카테고리 제외, false or 값 넘기지 않으면 : 히든프라이스 카테고리 포함)
				 },
				 method: 'get',
				 callbackFunc: function (resData) {
					 $.each(resData, function (idx, val) {
						 var selected = (cateCd == val.cateCd) ? 'selected="selected"' : '';
						 $(selector).append('<option value="' + val.cateCd + '" ' + selected  + '>' + val.cateNm + '</option>');
					 });
					 $(selector).css('-webkit-padding-end','30px');

                     if (typeof callBack == 'function') {
                         var callData = {
                             depth:depth,
                             selector : selector,
                             data:resData
                         };
                         callBack(callData);
                     }
				 }
			});
		}
	},
	/**
	 * 카테고리 select box change 이벤트
	 *
	 * @param depth 해당 카테고리 뎁스
	 * @param prefix 셀렉트박스 아이디 prefix
     */
	changeCategorySelectBox : function(depth, prefix, dispYnArr) {
		var cateCd = $('#'+prefix+depth).val();
		this.setCategorySelectBox(depth+1, cateCd, '', $('#'+prefix+(depth+1)), null, dispYnArr);
		$('#'+prefix+(depth+1)).change();
	},
    /**
	 * 카테고리 select box init
	 *
     * @param depth 로딩할 카테고리 뎁스
     * @param prefix 셀렉트박스 아이디 prefix
     */
    initCategorySelectBox : function(depth, prefix, dispYnArr , exceptHdnCate) {

		for(var i=1; i<depth+1; i++) {
            this.setCategorySelectBox(i,'','',$('#'+prefix+i), null, dispYnArr , exceptHdnCate);

		}
		switch(depth) {
			case 4 :
                $('#'+prefix+'3').unbind('change').on('change', function() {
                    commonCategory.changeCategorySelectBox(3, prefix, dispYnArr);
                });
            case 3 :
                $('#'+prefix+'2').unbind('change').on('change', function() {
                    commonCategory.changeCategorySelectBox(2, prefix, dispYnArr);
                });
            case 2 :
                $('#'+prefix+'1').unbind('change').on('change', function() {
                    commonCategory.changeCategorySelectBox(1, prefix, dispYnArr);
                });
		}
    }
};

$(document).ready(function(){
	commonCategory.init();
});