/**
 * 카테고리검색 공통 팝업 (트리)
 */
$(document).ready(function() {
    treeCatePop.init();
});

// 카테고리 조회
var treeCatePop = {
    /**
     * 초기화
     */
    init : function() {
        // dHtmlx 그리드 그리기
        treeCatePopGrid.load("/common/getTreeCategoryPop.json", treeCatePop.disableAllCheckBox,"json");
        treeCatePop.event();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $("#schBtn").bindClick(treeCatePop.searchCategoryList);
        $("#cancelBtn").bindClick(window.close);
    },
    /**
     * 전체 선택 체크박스 노출 여부
     */
    disableAllCheckBox : function () {
        treeCatePopGrid.showItemCheckbox("All", isAllCheck);
    },
    searchCategoryList : function () {
        var dataArray = new Array();
        var checkedTrees = treeCatePopGrid.getAllCheckedBranches().replace(",All", "");

        if(checkedTrees == "") {
            alert("카테고리를 선택해주세요.");
            return;
        }
        else {
            var rowArr = checkedTrees.split(",");

            // 상위ID만 남기고 삭제
            if(transDataParentNode && tsChecked) {
                rowArr = treeCatePop.setTreeData(rowArr, "4");
                rowArr = treeCatePop.setTreeData(rowArr, "3");
                rowArr = treeCatePop.setTreeData(rowArr, "2");
            }

            // 최하위ID만 남기고 삭제
            if(transDataChildNode && tsChecked) {
                rowArr = treeCatePop.spliceTreeData(rowArr, "3");
                rowArr = treeCatePop.spliceTreeData(rowArr, "2");
                rowArr = treeCatePop.spliceTreeData(rowArr, "1");
            }

            for(var i in rowArr){
                var dataObj = new Object();
                dataObj.categoryId  = treeCatePop.getParentCateId(rowArr[i], 4);
                dataObj.categoryNm  = treeCatePop.getParentCateNm(rowArr[i], 4);
                dataObj.categoryLId = treeCatePop.getParentCateId(rowArr[i], 1);
                dataObj.categoryLNm = treeCatePop.getParentCateNm(rowArr[i], 1);
                dataObj.categoryMId = treeCatePop.getParentCateId(rowArr[i], 2);
                dataObj.categoryMNm = treeCatePop.getParentCateNm(rowArr[i], 2);
                dataObj.categorySId = treeCatePop.getParentCateId(rowArr[i], 3);
                dataObj.categorySNm = treeCatePop.getParentCateNm(rowArr[i], 3);

                //전체카테고리 키는 제외
                if(dataObj.categoryId > 0) {
                    dataArray.push(dataObj);
                }
            }

            eval("opener." + callBackScript + "(dataArray);");
        }

        self.close();
    },
    setTreeData : function (rowArr, depth) {
        for (var i = 0; i < rowArr.length; i++) {
            if(rowArr[i].substr(0,1) == depth) {
                for (var z in rowArr) {
                    if (treeCatePopGrid.getParentId(rowArr[i]) == rowArr[z]) {
                        rowArr.splice(i, 1);
                        i--;
                    }
                }
            }
        }

        return rowArr;
    },
    spliceTreeData : function (rowArr, depth) {
        for (var i = 0; i < rowArr.length; i++) {
            if (rowArr[i].substr(0,1) == depth) {
                rowArr.splice(i, 1);
                i--;
            }
        }

        return rowArr;
    },
    getParentCateId : function (cateId, depth) {
        var parentCategoryId = 0;

        switch (depth) {
            case 1 :
                parentCategoryId = treeCatePopGrid.getParentId(treeCatePopGrid.getParentId(treeCatePopGrid.getParentId(cateId)));
                break;
            case 2 :
                parentCategoryId = treeCatePopGrid.getParentId(treeCatePopGrid.getParentId(cateId));
                break;
            case 3 :
                parentCategoryId = treeCatePopGrid.getParentId(cateId);
                break;
            case 4 :
                parentCategoryId = cateId;
                break;
            default :
                break;
        }

        if(parentCategoryId == "All") {
            parentCategoryId = 0;
        }

        return parentCategoryId;
    },
    getParentCateNm : function (cateId, depth) {
        var parentCategoryNm = "";

        switch (depth) {
            case 1 :
                parentCategoryNm = treeCatePopGrid.getItemText(treeCatePop.getParentCateId(cateId, 1));
                break;
            case 2 :
                parentCategoryNm = treeCatePopGrid.getItemText(treeCatePop.getParentCateId(cateId, 2));
                break;
            case 3 :
                parentCategoryNm = treeCatePopGrid.getItemText(treeCatePop.getParentCateId(cateId, 3));
                break;
            case 4 :
                parentCategoryNm = treeCatePopGrid.getItemText(treeCatePop.getParentCateId(cateId, 4));
                break;
            default :
                break;
        }

        if(parentCategoryNm == "전체 카테고리") {
            parentCategoryNm = "";
        }

        return parentCategoryNm;
    }
};