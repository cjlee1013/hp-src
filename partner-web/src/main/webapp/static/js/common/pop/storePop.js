/**
 * 판매업체조회 공통 팝업
 */
$(document).ready(function() {
    storePopGrid.init();
    storePop.init();
    CommonAjaxBlockUI.global();
});

// 판매업체조회 공통팝업 그리드
var storePopGrid = {
    gridView : new RealGridJS.GridView("storePopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        storePopGrid.initStorePopGrid();
        storePopGrid.initDataProvider();
        storePopGrid.event();
    },
    /**
     * 조회 그리드 설정
     */
    initStorePopGrid : function() {
        storePopGrid.gridView.setDataSource(storePopGrid.dataProvider);
        storePopGrid.gridView.setStyles(storePopGridBaseInfo.realgrid.styles);
        storePopGrid.gridView.setDisplayOptions(storePopGridBaseInfo.realgrid.displayOptions);
        storePopGrid.gridView.setColumns(storePopGridBaseInfo.realgrid.columns);
        storePopGrid.gridView.setOptions(storePopGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        storePopGrid.dataProvider.setFields(storePopGridBaseInfo.dataProvider.fields);
        storePopGrid.dataProvider.setOptions(storePopGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        storePopGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    },
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        storePopGrid.dataProvider.clearRows();
        storePopGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드에서 선택된 데이터 return
     * @returns {any[]|boolean}
     */
    checkData : function () {
        var checkedRows = storePopGrid.gridView.getCheckedRows(true);
        var storeList = new Array();

        checkedRows.forEach(function (_row) {
            var _data = this.dataProvider.getJsonRow(_row);
            var dataObj = new Object();

            dataObj.storeId =  _data.storeId;
            dataObj.storeNm = _data.storeNm;

            storeList.push(dataObj);
        }, this);

        return storeList;
    },
    /**
     * 엑셀다운로드
     * @returns {boolean}
     */
    excelDownload: function() {
        if(storePopGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "점포 리스트_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        storePopGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

// 점포조회
var storePop = {
    /**
     * 초기화
     */
    init : function() {
        storePop.event();
        storePop.storePopFormReset();
        storePop.initSearchDate('-1y');
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#storePopForm').resetForm();

        // 조회 일자 초기화
        storePop.initSearchDate('-1y');
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        storePop.setDate = Calendar.datePickerRange('schRegStartDate', 'schRegEndDate');

        storePop.setDate.setEndDate(0);
        storePop.setDate.setStartDate(flag);

        $("#schRegEndDate").datepicker("option", "minDate", $("#schRegStartDate").val());
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $("#schBtn").bindClick(storePop.searchStoreList);
        $("#selectBtn").bindClick(storePop.checkGridRow);
        $('#searchResetBtn').bindClick(storePop.searchFormReset);
        $('#excelDownloadBtn').bindClick(storePop.excelDownload);

        $("#searchKeyword").keyup(function (e) {
            if (e.keyCode == 13) {
                storePop.searchStoreList();
            }
        });
    },
    /**
     * 판매업체조회 공통팝업 form 초기화
     */
    storePopFormReset : function() {
        $("#storePopForm").resetForm();
    },
    /**
     * 조회 조건 내용검증
     * @returns {boolean}
     */
    validCheck : function() {
        // 날짜 체크
        var startDt = $("#schRegStartDate");
        var endDt = $("#schRegEndDate");

        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            seller.initSearchDate('-1y');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        // 검색어 체크
        var searchKeyword = $('#searchKeyword').val();

        if (searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            } else if (searchKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }
        return true;
    },
    /**
     * 판매업체 조회
     */
    searchStoreList : function () {
        // 조회 조건 validation check
        if (storePop.validCheck()) {
            var storePopForm = $("#storePopForm").serialize();

            CommonAjax.basic({
                url: '/item/store/getStoreList.json',
                data: storePopForm,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    storePopGrid.setData(res);
                    $("#storePopSearchCnt").html(storePopGrid.dataProvider.getRowCount());
                }
            });
        }
    },
    /**
     * 선택한 데이터 return
     * @returns {boolean}
     */
    checkGridRow : function () {
        var checkedRows = storePopGrid.gridView.getCheckedRows(true);

        if (checkedRows.length > 1) {
            alert("하나의 항목만 선택할 수 있습니다.");
            return false;
        }

        if (checkedRows.length == 0) {
            alert("점포를 선택해 주세요.");
            return false;
        }

        var checkStore= storePopGrid.checkData();
        eval("opener." + callBackScript + "(checkStore);");

        self.close();
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        storePopGrid.excelDownload();
    }
};