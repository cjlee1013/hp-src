/**
 * 파트너 홈메인 대시보드 관련
 */
$(document).ready(function() {
    home.init();
});

var home = {
    RETURN_CODE_SUCCESS: "SUCCESS",
    RETURN_STATUS_SUCCESS: 200,
    RETURN_CODE_SUCCESS_ESCROWMNG: "0000",

    init: function(){
        home.search.salesProgress();
        home.search.settleAmt();
        home.search.claimDashBoardCount();
    },
    parentAddTab: function(tabId, tabName, url){
      parent.initialTab.addTab(tabId, tabName, url);
    },
    /**
     * 공지사항 더보기
     */
    noticeMoreView: function(){
        home.parentAddTab('Tab_Notice', '공지사항', '/manage/notice/noticeMain');
    },
    /**
     * 공지사항 상세
     * @param dspNo
     * @param kind
     */
    noticeDetailOpenTab: function(dspNo, kind) {
        home.noticeMoreView();

        const param = "?dspNo="+dspNo+"&kind="+ kind;
        home.parentAddTab('Tab_noticeDetail', '공지사항 상세',
            '/manage/notice/noticeDetailMain' + param);
    },
    /**
     * 위해상품공지 상세
     * @param noDocument
     * @param seq
     * @param cdInsptmachi
     * @param itemNo
     */
    harmfulDetailOpenTab: function(noDocument, seq, cdInsptmachi){
        home.noticeMoreView();

        var param = "?noDocument="+noDocument;
        param += "&seq="+seq;
        param += "&cdInsptmachi="+cdInsptmachi;

        home.parentAddTab('Tab_harmfulDetail', '위해상품공지 상세',
            '/manage/notice/harmfulDetailMain' + param);
    },
    /**
     * 미 답변 상품Q&A 상세
     */
    goNoAnswerPage: function() {
        home.parentAddTab('Tab_qnaMain', '상품Q&A관리',
            '/voc/csQna/csQnaMain?qnaStatus=READY');
    },
    /**
     * 판매중인 상품
     */
    goSailingPage: function() {
        home.parentAddTab('Tab_itemMain',
            '상품조회/수정','/item/itemMain?itemStatus=A');
    },
    /**
     * 재고 10개이하 상품
     */
    goTenLessStockPage: function () {
        home.parentAddTab('Tab_itemMain', '상품조회/수정',
            '/item/itemMain?itemStatus=L');
    },
    /**
     * 판매종료 임박상품
     */
    goEndOfSalePage: function() {
        home.parentAddTab('Tab_itemMain','상품조회/수정',
            '/item/itemMain?itemStatus=E');
    },
    /**
     * 신규주문 건수
     */
    goNewCntPage: function() {
        home.parentAddTab('Tab_orderShipManageMain', '발주/발송관리',
            '/sell/orderShipManage/orderShipManageMain?schType=newCnt');
    },

    /**
     * 주문확인지연 건수
     */
    goNewDelayCntPage: function() {
        home.parentAddTab('Tab_orderShipManageMain', '발주/발송관리',
            '/sell/orderShipManage/orderShipManageMain?schType=newDelayCnt');
    },
    /**
     * 발송대기 건수
     */
    goReadyCntPage: function() {
        home.parentAddTab('Tab_orderShipManageMain', '발주/발송관리',
            '/sell/orderShipManage/orderShipManageMain?schType=readyCnt');
    },
    /**
     * 발송지연 건수
     */
    goShipDelayCntPage: function(){
        home.parentAddTab('Tab_orderShipManageMain', '발주/발송관리',
            '/sell/orderShipManage/orderShipManageMain?schType=shipDelayCnt');
    },
    /**
     * 배송중 건수
     */
    goShippingCntPage: function() {
        home.parentAddTab('Tab_shipStatusMain', '배송현황',
            '/sell/shipStatus/shipStatusMain?schType=shippingCnt');
    },

    refresh: function () {
        CommonAjaxBlockUI.global();

        home.search.salesProgress();
        home.search.settleAmt();
        home.search.claimDashBoardCount();
    },
    /**
     * 셰금계산서 상세
     */
    viewTaxBill: function () {
        home.parentAddTab('Tab_settleTaxInovice', '세금계산서',
            '/settle/taxBillList');
    },

    /**
     * 취소요청건수
     */
    goCancelRequestCnt: function () {
        home.parentAddTab('Tab_cancelMain','취소관리',
            'claim/claimMain?claimType=C&schStatus=request');
    },
    /**
     * 반품요청건수
     */
    goReturnRequestCnt: function () {
        home.parentAddTab('Tab_returnMain','반품관리',
            '/claim/claimMain?claimType=R&schStatus=request');
    },
    /**
     * 교환요청건수
     */
    goExchangeRequestCnt: function () {
        home.parentAddTab('Tab_exchangeMain','교환요청',
            '/claim/claimMain?claimType=X&schStatus=request');
    },
    fixDigitComma: function(data) {
        const regexp = /\B(?=(\d{3})+(?!\d))/g;
        return data.toString().replace(regexp, ',');
    },
    /**
     * 조회 관련
     */
    search: {
        salesProgress: function () {
            CommonAjax.basic({
                url         : '/home/salesProgress.json',
                data        : null,
                method      : "GET",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    $("#newCnt").html(home.fixDigitComma(res.newCnt));
                    $("#newDelayCnt").html(home.fixDigitComma(res.newDelayCnt));
                    $("#readyCnt").html(home.fixDigitComma(res.readyCnt));
                    $("#shipDelayCnt").html(home.fixDigitComma(res.shipDelayCnt));
                    $("#shippingCnt").html(home.fixDigitComma(res.shippingCnt));
                }
            });
        },
        settleAmt: function () {
            CommonAjax.basic({
                url         : '/home/partnerSettleAmt.json',
                data        : null,
                method      : "GET",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    $("#settleAmt").html(home.fixDigitComma(res.settleAmt));
                }
            });
        },
        claimDashBoardCount: function () {
            CommonAjax.basic({
                url         : '/home/claimDashBoardCount.json',
                data        : null,
                method      : "GET",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    $("#cancelRequestCnt").html(home.fixDigitComma(res.cancelRequestCnt));
                    $("#exchangeRequestCnt").html(home.fixDigitComma(res.exchangeRequestCnt));
                    $("#returnRequestCnt").html(home.fixDigitComma(res.returnRequestCnt));
                }
            });
        },
    },
};