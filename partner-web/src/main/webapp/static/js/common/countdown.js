function homeCountdown(view,endTime,format,endTxt,callback,defaultTxt)
{
	this.view 		= view					; 
	this.endTime 	= endTime				;
	this.format 	= format.toLowerCase()	;
	this.endTxt 	= endTxt				;
	this.callback 	= callback				;
	this.timer 		= null					;
	this.defaultTxt= typeof(defaultTxt)!='undefined'?defaultTxt:'';
}

homeCountdown.prototype = {
	countdown: function()
	{
		var remain_time = moment(this.endTime).diff(moment());

		if ( remain_time <= 0 )
		{
			$(this.view).html(this.endTxt);
			clearInterval(this.timer);
			if ( typeof(this.callback) == "function" )
			{
				this.callback();
			}
			return;
		}

		var rt;
		switch ( this.format )
		{
			case "hms":
				rt = moment(moment(this.endTime).diff(moment())).format('HH:mm:ss');
				break;
			case "ms":
				rt = moment(moment(this.endTime).diff(moment())).format('mm:ss');
				break;
			case "s":
				rt = moment(moment(this.endTime).diff(moment())).format('ss');
				break;
			default:
				clearInterval(this.timer);
				return;
		}

		$(this.view).html(this.defaultTxt+rt);
	},
	startTimer: function()
	{
		if ( !this.timer )
		{
			var c = this;
			c.countdown();
			this.timer = setInterval(function(){c.countdown();},1000);
		}
	},
	endTimer: function()
	{
		clearInterval(this.timer);
		this.timer = null;
	}
};

function countdownTimer(view,endTime,format,endTxt,callback,defaultTxt)
{
	var c = new homeCountdown(view,endTime,format,endTxt,callback,defaultTxt);
	c.startTimer();
	return c;
}