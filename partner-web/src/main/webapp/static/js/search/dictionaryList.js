



/***
 * 어휘 조회 리스트 그리드 설정
 * @type {{init: dictionaryListGrid.init, setData: dictionaryListGrid.setData, initDataProvider: dictionaryListGrid.initDataProvider, excelDownload: dictionaryListGrid.excelDownload, dataProvider: *, initGrid: dictionaryListGrid.initGrid, event: dictionaryListGrid.event, gridView: *}}
 */
var dictionaryListGrid = {
    gridView : new RealGridJS.GridView("dictionaryListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dictionaryListGrid.initGrid();
        dictionaryListGrid.initDataProvider();
        dictionaryListGrid.event();
    },
    initGrid : function () {
        dictionaryListGrid.gridView.setDataSource(dictionaryListGrid.dataProvider);

        dictionaryListGrid.gridView.setStyles(dictionaryListGridBaseInfo.realgrid.styles);
        dictionaryListGrid.gridView.setDisplayOptions(dictionaryListGridBaseInfo.realgrid.displayOptions);
        dictionaryListGrid.gridView.setColumns(dictionaryListGridBaseInfo.realgrid.columns);
        dictionaryListGrid.gridView.setOptions(dictionaryListGridBaseInfo.realgrid.options);
        dictionaryListGrid.gridView.setCheckBar({ visible: false });
        dictionaryListGrid.gridView.setSelectOptions({
            style: 'rows'
        });

        var flagMenu = [{
            label: "사용",
            enabled: true,
            tag : 1
        }, {
            label: "사용안함",
            enabled: true,
            tag : 0
        }];
        dictionaryListGrid.gridView.addPopupMenu("useFlagMenu", flagMenu);
        dictionaryListGrid.gridView.addPopupMenu("stopFlagMenu", flagMenu);

        dictionaryListGrid.gridView.onMenuItemClicked = function (grid, data, index) {
            var flag = 0;
            if(index.column == "useFlag") {
                flag = data.tag;
                var rowDataJson = dictionaryListGrid.dataProvider.getJsonRow(index.itemIndex);

                var requestData = {};
                requestData.flag = flag;
                requestData.wordId = rowDataJson.wordId;

                CommonAjax.basic({
                    url:'/search/dictionary/'+rowDataJson.wordId+'/updateUseFlag.json',
                    data: JSON.stringify(requestData),
                    type: 'post',
                    contentType: 'application/json',
                    method:"POST",
                    callbackFunc:function() {
                        dictionaryList.search(false);
                    }
                });
            } else if (index.column == "stopFlag") {
                flag = data.tag;
                var rowDataJson = dictionaryListGrid.dataProvider.getJsonRow(index.itemIndex);

                var requestData = {};
                requestData.flag = flag;
                requestData.wordId = rowDataJson.wordId;

                CommonAjax.basic({
                    url:'/search/dictionary/'+rowDataJson.wordId+'/updateStopFlag.json',
                    data: JSON.stringify(requestData),
                    type: 'post',
                    contentType: 'application/json',
                    method:"POST",
                    callbackFunc:function() {
                        dictionaryList.search(false);
                    }
                });
            }
        };

    },
    initDataProvider : function() {
        dictionaryListGrid.dataProvider.setFields(dictionaryListGridBaseInfo.dataProvider.fields);
        dictionaryListGrid.dataProvider.setOptions(dictionaryListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dictionaryListGrid.gridView.onDataCellClicked = function(gridView, index) {
            dictionaryListGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        dictionaryListGrid.dataProvider.clearRows();
        dictionaryListGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = dictionaryListGrid.dataProvider.getJsonRow(selectRowId);
        if($('#wordId').val() != rowDataJson.wordId)
        {
            dictionaryList.mindMap(rowDataJson.wordId);
        }
        $('#wordId').val(rowDataJson.wordId);
        $("#wordName").val(rowDataJson.wordName);
    },
    excelDownload: function() {
        if(dictionaryListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "사전_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        dictionaryListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};

var dictionaryList = {
    init : function () {
        this.initSearchDate('-5y');
    },
    initSearchDate : function (flag) {
        dictionaryList.setDate = Calendar.datePickerRange('sdate', 'edate');
        dictionaryList.setDate.setEndDate(0);
        dictionaryList.setDate.setStartDate(flag);
        $("#edate").datepicker("option", "minDate", $("#sdate").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(dictionaryList.search);
        $('#writeBtn').bindClick(dictionaryList.write);
        $('#modityBtn').bindClick(dictionaryList.modify);
    },
    hiddenFieldInit : function() {
        $('#wordId').val('');
        $('#wordName').val('');
        $("#wordMindMap").hide();
        $("#searchBtn").show();
        $("#writeBtn").hide();
    },
    ///어휘등록
    write : function() {
        location.href = '/search/dictionary/wordWrite/' + $('#searchKeyword').val();
    },
    ///관련어
    relation : function () {
        if($('#wordId').val() == '') {
            alert('관련어를 구성하려는 단어를 먼저 선택해주세요');
            return false;
        }
        var url = '/search/dictionary/relationInfo/' + $('#wordId').val();
        dictionaryList.windowOpen(url, 1132, 1040, 'relation');
    },
    ///기분석구성
    preAnalysis : function() {
        if($('#wordId').val() == '') {
            alert('기분석을 구성하려는 단어를 먼저 선택해주세요');
            return false;
        }
        var url = '/search/dictionary/preAnalysisInfo/' + $('#wordId').val();
        dictionaryList.windowOpen(url, 1132, 731,'preAnalysis');
    },
    ///복합어구성
    compounds : function() {
        if($('#wordId').val() == '') {
            alert('복합어를 구성하려는 단어를 먼저 선택해주세요');
            return false;
        }
        var url = '/search/dictionary/compoundsInfo/' + $('#wordId').val();
        dictionaryList.windowOpen(url, 1132, 731,'compounds');
    },
    ///어휘수정
    modify : function() {
        if(dictionaryListGrid.dataProvider.getRowCount() == 0) {
            alert('수정하려는 단어를 먼저 선택해주세요.');
            return;
        }
        location.href = '/search/dictionary/wordModify/' + $('#wordId').val();
    },
    search : function () {

        dictionaryList.hiddenFieldInit();
        var data = {};

        var search_keyword = $('#searchKeyword').val();
        if(!$.trim(search_keyword)) {
            alert('어휘를 입력해주세요.');
            return false;
        }

        /// 품사
        data.pos = $('#pos').val();
        /// 사전 형식
        var dicTypeList = new Array();
        $("input[name='dicType']:checked").each(function(){
            dicTypeList.push($(this).val());
        });
        data.dicType = dicTypeList;
        /// 관계
        data.relation = $("select[name=relation]").val();
        /// 시작일
        data.sdate = Date.parse($("input[name=sdate]").val() + " 00:00:00") / 1000;
        /// 종료일
        data.edate = Date.parse($("input[name=edate]").val() + " 23:59:59") / 1000;

        ///포함된 어휘 모두 찾기
        var wordInclude = $("input[name=wordInclude]:checked").val();
        if(wordInclude == 1) {
            data.wordName = '%' + search_keyword + '%';
        }
        else {
            data.wordName = search_keyword;
        }

        CommonAjax.basic(
            {
                url:'/search/dictionary/getSrchWordInfo.json',
                data: JSON.stringify(data),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                successMsg:null,
                callbackFunc:function(res)
                {
                    dictionaryListGrid.setData(res);
                    $('#dictionaryTotalCount').html($.jUtil.comma(dictionaryListGrid.gridView.getItemCount()));
                    if(dictionaryListGrid.dataProvider.getRowCount() > 0) {

                        dictionaryList.mindMap(dictionaryListGrid.dataProvider.getJsonRow(0).wordId);
                        $('#wordId').val(dictionaryListGrid.dataProvider.getJsonRow(0).wordId);
                        $('#wordName').val(dictionaryListGrid.dataProvider.getJsonRow(0).wordName);
                    }
                    else
                    {
                        $("#searchBtn").hide();
                        $("#writeBtn").show();
                    }
                }
            });

    },
    mindMap : function(wordId) {
        $("#wordMindMap").show();
        $("#wordMindMap").attr("src","/search/dictionary/wordMindMap/" + wordId);
    },
    windowOpen  : function (url, _width, _height, _target) {
        // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
        var _left = ($(window).width()/2)-(_width/2);
        var _top = ($(window).height()/2)-(_height/2);
        if(_target == 'undefined' || _target == null || _target == '') {
            _target ='popup-search';
        }
        void(window.open(url, _target, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+ _width +', height='+ _height +', left=' + _left + ', top='+ _top ));
    },
    history: function () {
        if(dictionaryListGrid.dataProvider.getRowCount() == 0) {
            alert('조회하려는 단어를 먼저 선택해주세요.');
            return;
        }
        dictionaryList.windowOpen('/search/dictionary/wordHistory/' + $('#wordId').val(), 860, 443,'history');
    },
    typoCorrect : function () {
        if(dictionaryListGrid.dataProvider.getRowCount() == 0) {
            alert('오타설정을 하려는 단어를 먼저 선택해주세요.');
            return;
        }
        dictionaryList.windowOpen('/search/dictionary/typoCorrect/' + $('#wordId').val(), 682, 337,'typo');
    },
    delete: function() {
        if(dictionaryListGrid.dataProvider.getRowCount() == 0) {
            alert('삭제하려는 단어를 먼저 선택해주세요.');
            return;
        }
        if(confirm("'" + $('#wordName').val() + "' 을(를) 삭제하시겠습니까?")) {
            CommonAjax.basic({url:'/search/dictionary/'+$('#wordId').val()+'/deleteWordInfo.json' , data:null, method:"POST",type:"post", contentType: 'application/json', successMsg:null, callbackFunc:function(res) {
                if(res != '1') {
                    alert('삭제에 실패하였습니다');
                } else {
                    alert('삭제되었습니다.');
                    dictionaryList.search();
                }
            }});
        }

    }
};