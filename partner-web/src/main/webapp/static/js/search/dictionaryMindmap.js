// load the mindmap
$(document).ready(function() {

    $("link").each(function() {
        var link = $(this).attr("href");
        if(link.indexOf('/static/css/common.css') != -1){
            $(this).remove();
        }
    });

    // enable the mindmap in the body
    $('#mindmapBody').mindmap();
    // add the data to the mindmap
    var root = $('#mindmapBody>ul>li').get(0).mynode = $('#mindmapBody').addRootNode($('#mindmapBody>ul>li>a').text(), {
        href:'/',
        url:'/',
        onclick:function(node) {
            $(node.obj.activeNode.content).each(function() {
                this.hide();
            });
        }
    });
    $('#mindmapBody>ul>li').hide();
    var addLI = function() {
        var parentnode = $(this).parents('li').get(0);
        if (typeof(parentnode)=='undefined') parentnode=root;
        else parentnode=parentnode.mynode;

        this.mynode = $('#mindmapBody').addNode(parentnode, $('a:eq(0)',this).text(), {
//          href:$('a:eq(0)',this).text().toLowerCase(),
            href:$('a:eq(0)',this).attr('href'),
            onclick:function(node) {
                $(node.obj.activeNode.content).each(function() {
                    this.hide();
                });
                $(node.content).each(function() {
                    this.show();
                });
            }
        });
        $(this).hide();
        $('>ul>li', this).each(addLI);
    };
    $('#mindmapBody>ul>li>ul').each(function() {
        $('>li', this).each(addLI);
    });

});