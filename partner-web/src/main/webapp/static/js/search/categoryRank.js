var categoryRankGrid = {
    gridView : new RealGridJS.GridView("categoryRankGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        categoryRankGrid.initGrid();
        categoryRankGrid.initDataProvider();
        categoryRankGrid.event();
    },
    page : 0,
    initGrid : function () {
        categoryRankGrid.gridView.setDataSource(categoryRankGrid.dataProvider);

        categoryRankGrid.gridView.setStyles(categoryRankGridBaseInfo.realgrid.styles);
        categoryRankGrid.gridView.setDisplayOptions(categoryRankGridBaseInfo.realgrid.displayOptions);
        categoryRankGrid.gridView.setColumns(categoryRankGridBaseInfo.realgrid.columns);
        categoryRankGrid.gridView.setOptions(categoryRankGridBaseInfo.realgrid.options);
        categoryRankGrid.gridView.setCheckBar({ visible: false });
        categoryRankGrid.gridView.setSelectOptions({
            style: 'singleRow'
        });
        categoryRankGrid.gridView.setEditOptions({
            appendable: true,
            insertable: true
        });
        var hoverMaskValue = $('#selHoverMask').val();
        categoryRankGrid.gridView.setDisplayOptions({
            rowHoverMask:{
                visible:true,
                styles:{
                    background:"#2065686b"
                },
                hoverMask: hoverMaskValue
            }
        });
        ///체크바
        categoryRankGrid.gridView.setCheckBar({
            visible: true,
            showAll: true
        });
        ///인디케이터바
        categoryRankGrid.gridView.setIndicator({
            visible: true
        });
        categoryRankGrid.gridView.onTopItemIndexChanged = function(grid, item) {
            if (item > categoryRankGrid.page) {
                categoryRankGrid.page += 50;
                categoryRankJs.searchList(categoryRankGrid.dataProvider.getRowCount(),categoryRankJs.limitPageSize());
            }
        };
    },
    initDataProvider : function() {
        categoryRankGrid.dataProvider.setFields(categoryRankGridBaseInfo.dataProvider.fields);
        categoryRankGrid.dataProvider.setOptions(categoryRankGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        categoryRankGrid.gridView.onDataCellClicked = function(gridView, index) {
            categoryRankGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        categoryRankGrid.dataProvider.clearRows();
        categoryRankGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = categoryRankGrid.dataProvider.getJsonRow(selectRowId);
        categoryRankJs.selectedCategoryRankGridIndex(rowDataJson);
    },
    excelDownload: function() {

        $("#searchKeyword").val('');

        //전체 다운로드를 위해 전체 데이터를 RealGrid에 바인딩 해야 한다.
        categoryRankJs.searchList(0,100000000, function() {
            var _date = new Date();
            var fileName = "카테고리랭킹_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

            categoryRankGrid.gridView.exportGrid( {
                type: "excel",
                target: "local",
                fileName: fileName+".xlsx",
                showProgress: true,
                progressMessage: " 엑셀 데이터 추줄 중입니다."
            });
        });
    }
};



var categoryRankRelationGrid = {
    gridView : new RealGridJS.GridView("categoryRankRelationGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        categoryRankRelationGrid.initGrid();
        categoryRankRelationGrid.initDataProvider();
        categoryRankRelationGrid.event();
    },
    page : 0,
    initGrid : function () {
        categoryRankRelationGrid.gridView.setDataSource(categoryRankRelationGrid.dataProvider);

        categoryRankRelationGrid.gridView.setStyles(categoryRankRelationGridBaseInfo.realgrid.styles);
        categoryRankRelationGrid.gridView.setDisplayOptions(categoryRankRelationGridBaseInfo.realgrid.displayOptions);
        categoryRankRelationGrid.gridView.setColumns(categoryRankRelationGridBaseInfo.realgrid.columns);
        categoryRankRelationGrid.gridView.setOptions(categoryRankRelationGridBaseInfo.realgrid.options);
        categoryRankRelationGrid.gridView.setCheckBar({ visible: false });
        categoryRankRelationGrid.gridView.setSelectOptions({
            style: 'singleRow'
        });
        categoryRankRelationGrid.gridView.setEditOptions({
            appendable: true,
            insertable: true
        });
        var hoverMaskValue = $('#selHoverMask').val();
        categoryRankRelationGrid.gridView.setDisplayOptions({
            rowHoverMask:{
                visible:true,
                styles:{
                    background:"#2065686b"
                },
                hoverMask: hoverMaskValue
            }
        });
        ///체크바
        categoryRankRelationGrid.gridView.setCheckBar({
            visible: true,
            showAll: true
        });
        ///인디케이터바
        categoryRankRelationGrid.gridView.setIndicator({
            visible: true
        });
        categoryRankRelationGrid.gridView.onTopItemIndexChanged = function(grid, item) {
            if (item > categoryRankRelationGrid.page) {
                categoryRankRelationGrid.page += 50;
                categoryRankJs.searchList(categoryRankRelationGrid.dataProvider.getRowCount(),categoryRankJs.limitPageSize());
            }
        };

        var menu = [{
            label: "1",
            enabled: true,
            tag : "1"
        }, {
            label: "2",
            enabled: true,
            tag : "2"
        }, {
            label: "3",
            enabled: true,
            tag : "3"
        }, {
            label: "4",
            enabled: true,
            tag : "4"
        }, {
            label: "5",
            enabled: true,
            tag : "5"
        }, {
            label: "-1",
            enabled: true,
            tag : "-1"
        }, {
            label: "-2",
            enabled: true,
            tag : "-2"
        }, {
            label: "-3",
            enabled: true,
            tag : "-3"
        }, {
            label: "-4",
            enabled: true,
            tag : "-4"
        }, {
            label: "-5",
            enabled: true,
            tag : "-5"
        }, {
            label: "제외",
            enabled: true,
            tag : "제외"
        }];
        categoryRankRelationGrid.gridView.addPopupMenu("boostTypeMenu", menu);
        /***
         * 매핑 정보 순위 변경 (Realtion)
         * @param grid
         * @param data
         * @param index
         */
        categoryRankRelationGrid.gridView.onMenuItemClicked = function (grid, info, index) {
            var data = {};
            data.rankingId = categoryRankGrid.dataProvider.getValue(categoryRankGrid.gridView.getCurrent().dataRow, "rankingId");
            data.relationId = categoryRankRelationGrid.dataProvider.getValue(categoryRankRelationGrid.gridView.getCurrent().dataRow, "relationId");
            data.boostType = info.tag;

            CommonAjax.basic(
                {
                    url:'/search/categoryRank/update.json',
                    data: JSON.stringify(data),
                    type: 'post',
                    method:'POST',
                    contentType: 'application/json',
                    successMsg:null,
                    callbackFunc:function(res)
                    {
                        if(res != 1) {
                            alert('수정에 실패하였습니다.');
                            return false;
                        } else {
                            var rankingId = categoryRankGrid.dataProvider.getValue(categoryRankGrid.gridView.getCurrent().dataRow, "rankingId");
                            categoryRankJs.searchRelation(rankingId);
                        }
                    }
                });
        };

    },
    initDataProvider : function() {
        categoryRankRelationGrid.dataProvider.setFields(categoryRankRelationGridBaseInfo.dataProvider.fields);
        categoryRankRelationGrid.dataProvider.setOptions(categoryRankRelationGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        categoryRankRelationGrid.gridView.onDataCellClicked = function(gridView, index) {
            categoryRankRelationGrid.gridRowSelect(index.dataRow)
        };
        // 가중치 수정
        categoryRankRelationGrid.dataProvider.onRowUpdated = function (provider, row) {
            var r = provider.getJsonRow(row);
            alert(JSON.stringify(r));
        }
    },
    setData : function(dataList) {
        categoryRankRelationGrid.dataProvider.clearRows();
        categoryRankRelationGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = categoryRankRelationGrid.dataProvider.getJsonRow(selectRowId);
        categoryRankJs.selectedCategoryRankRelationGridIndex(rowDataJson);
    }
};






var categoryRankJs = {
    init : function () {
        this.initCategorySelectBox();
    },
    limitPageSize : function() {
        return 100;
    },
    onload : function() {
        categoryRankJs.searchList(0,categoryRankJs.limitPageSize());
    },
    selectedCategoryRankRelationGridIndex : function(jsonRow) {
    },
    selectedCategoryRankGridIndex : function(jsonRow) {
        $("#keyword").val(jsonRow.keyword);
        categoryRankJs.searchRelation(jsonRow.rankingId);
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));
    },
    event : function() {
        $('#searchBtn').click(function(){
            categoryRankJs.searchList(0,categoryRankJs.limitPageSize());
        });
        $('#keyword').calcTextLength('keyup', '#textCountKr_keyword');
        $("#saveBtn").click(function(){
            categoryRankJs.write(false);
        });
        $("#excelDownloadBtn").click(function() {
            categoryRankGrid.excelDownload();
        });
    },
    resetCategoryRankRelationList : function() {
    },
    resetCategoryRankList : function() {
    },
    deleteRelation : function() {
        var rows = categoryRankRelationGrid.gridView.getCheckedRows(true);
        if(rows.length == 0) {
            alert('삭제하려는 랭킹 정보의 체크바를 선택해 주세요.');
            return false;
        }
        if(!confirm('체크하신 랭킹 매핑 정보를 삭제 하시겠습니까?')) {
            return false;
        }
        var rankingId = categoryRankGrid.dataProvider.getValue(categoryRankGrid.gridView.getCurrent().dataRow, "rankingId");
        for(var i = 0; i < rows.length; i++) {
            CommonAjax.basic({
                url: '/search/categoryRank/'+rankingId+'/'+categoryRankRelationGrid.dataProvider.getJsonRow(rows[i]).relationId+'/relation/delete.json',
                data: null,
                type: 'get',
                method:"GET",
                callbackFunc:function() {
                    categoryRankJs.searchRelation(rankingId);
                }
            });
        }
        return false;
    },
    deleteList : function() {
        var rows = categoryRankGrid.gridView.getCheckedRows(true);
        if(rows.length == 0) {
            alert('삭제하려는 랭킹 정보의 체크바를 선택해 주세요.');
            return false;
        }
        if(!confirm('체크하신 랭킹 매핑 정보를 삭제 하시겠습니까?')) {
            return false;
        }
        for(var i = 0; i < rows.length; i++) {
            CommonAjax.basic({
                url: '/search/categoryRank/'+categoryRankGrid.dataProvider.getJsonRow(rows[i]).rankingId+'/all/delete.json',
                data: null,
                type: 'get',
                method:"GET",
                callbackFunc:function() {
                    categoryRankJs.searchList(0,categoryRankJs.limitPageSize());
                }
            });
        }
        return false;
    },
    write : function(edit) {
        if(!$.trim($("#keyword").val())) {
            alert('신규로 등록 할 검색어를 입력해주세요.');
            $("#keyword").focus();
            return false;
        }

        var boostType = $("#boostType option:selected").val();
        var searchCateCd1 = $("#searchCateCd1 option:selected").val();
        var searchCateCd2 = $("#searchCateCd2 option:selected").val();
        var searchCateCd3 = $("#searchCateCd3 option:selected").val();
        var searchCateCd4 = $("#searchCateCd4 option:selected").val();

        var searchCateNm1 = $("#searchCateCd1 option:selected").text();
        var searchCateNm2 = $("#searchCateCd2 option:selected").text();
        var searchCateNm3 = $("#searchCateCd3 option:selected").text();
        var searchCateNm4 = $("#searchCateCd4 option:selected").text();

        var selectedCategoryId = (searchCateCd1 != "") ? (searchCateCd2 != "") ? (searchCateCd3 != "") ? (searchCateCd4 != "") ? searchCateCd4 : searchCateCd3 : searchCateCd2 : searchCateCd1 : "";
        var selectedCategoryNm = (searchCateCd1 != "") ? (searchCateCd2 != "") ? (searchCateCd3 != "") ? (searchCateCd4 != "") ? searchCateNm1 + ":" + searchCateNm2 + ":" + searchCateNm3 + ":" + searchCateNm4 : searchCateNm1 + ":" + searchCateNm2 + ":" + searchCateNm3 : searchCateNm1 + ":" + searchCateNm2 : searchCateNm1 : "";
        if(selectedCategoryId == "" || selectedCategoryNm == "") {
            alert('신규로 등록 할 카테고리를 선택해주세요.');
            $("#searchCateCd1").focus()
            return false;
        }

        var data = {};
        data.keyword = $("#keyword").val();
        data.boostType = boostType;
        data.categoryId = selectedCategoryId;
        data.categoryNm = selectedCategoryNm;

        CommonAjax.basic(
            {
                url:'/search/categoryRank/insert.json',
                data: JSON.stringify(data),
                type: 'post',
                method:'POST',
                contentType: 'application/json',
                successMsg:null,
                callbackFunc:function(res)
                {
                    if(res != 1) {
                        alert('등록에 실패하였습니다.');
                        return false;
                    } else {
                        //성공
                        var rankingId = categoryRankGrid.dataProvider.getValue(categoryRankGrid.gridView.getCurrent().dataRow, "rankingId");
                        $("#searchKeyword").val($("#keyword").val());
                        categoryRankJs.searchList(0,categoryRankJs.limitPageSize());
                        categoryRankJs.searchRelation(rankingId);
                        categoryRankJs.initCategorySelectBox();
                        $("#boostType").val("1").prop("selected", true);
                    }
                }
            });

    },
    searchRelation : function(rankingId) {
        CommonAjax.basic({
            url: '/search/categoryRank/'+rankingId+'/relation.json',
            data: null,
            type: 'get',
            method:"GET",
            callbackFunc:function(res) {
                categoryRankRelationGrid.setData(res.result);
                categoryRankRelationGrid.page = 0;
                if(categoryRankRelationGrid.dataProvider.getRowCount() > 0) {
                    categoryRankJs.selectedCategoryRankRelationGridIndex(categoryRankRelationGrid.dataProvider.getJsonRow(0));
                } else {
                    categoryRankJs.resetCategoryRankRelationList(); //검색 결과가 없을 시 초기화
                }
            }
        });
    },
    searchList : function (offset, limit, callback) {

        var data = {};

        data.keyword = $('#searchKeyword').val();
        data.offset = offset;
        data.limit = limit;

        CommonAjax.basic(
            {
                url:'/search/categoryRank/rankList.json',
                data: JSON.stringify(data),
                type: 'post',
                method:'POST',
                contentType: 'application/json',
                successMsg:null,
                callbackFunc:function(res)
                {
                    if(offset > 0) {
                        categoryRankGrid.dataProvider.fillJsonData(res.result,{fillMode: "append"});
                    }
                    else
                    {
                        categoryRankGrid.setData(res.result);
                        $('#dictionaryTotalCount').html($.jUtil.comma(res.totalCount));
                        categoryRankGrid.page = 0;
                        if(categoryRankGrid.dataProvider.getRowCount() > 0) {
                            categoryRankJs.selectedCategoryRankGridIndex(categoryRankGrid.dataProvider.getJsonRow(0));
                        } else {
                            categoryRankJs.resetCategoryRankList(); //검색 결과가 없을 시 초기화
                            categoryRankJs.resetCategoryRankRelationList();
                            categoryRankRelationGrid.setData([]);
                            categoryRankRelationGrid.page = 0;
                        }
                    }

                    if(callback != "undefined" && callback != null) {
                        callback();
                    }
                }
            });

    }
};