var keywordRankJs = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-30d');
        this.initSaveDate();
        this.initCategorySelectBox();
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        keywordRankJs.setDate = Calendar.datePickerRange('schStartDate', 'schEndDate');

        keywordRankJs.setDate.setEndDate(0);
        keywordRankJs.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate", $("#schStartDate").val());
    },
    initSaveDate : function (startDt,endDt) {
        keywordRankJs.setDate = Calendar.datePickerRange('startDt', 'endDt');

        keywordRankJs.setDate.setEndDate((endDt == null || endDt == undefined) ? "9999-12-31" : endDt);
        keywordRankJs.setDate.setStartDate((startDt == null || startDt == undefined) ? "0d" : startDt);

        $("#endDt").datepicker("option", "minDate", $("#startDt").val());
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#searchResetBtn').bindClick(keywordRankJs.searchFormReset);
        $('#searchBtn').bindClick(keywordRankJs.search);
        $('#excelDownloadBtn').bindClick(keywordRankJs.excelDownload);
        $("#searchKeyword").notAllowInput('keyup', ['SPC_SCH']);
        $("#saveBtn").bindClick(keywordRankJs.save);
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        itemListGrid.excelDownload();
    },
    /***
     * 저장
     */
    save : function() {

        CommonAjax.basic({
            url: '/search/keywordRank/save.json',
            data: JSON.stringify($('#itemSetForm').serializeObject(true)),
            type: 'post',
            contentType: 'application/json',
            method:"POST",
            successMsg:null,
            callbackFunc:function(res) {
                if(res == 1) {
                    alert('저장되었습니다.');
                    return false;
                } else {
                    alert('저장에 실패하였습니다. (관리자에게 문의해주세요)');
                    return false;
                }
            }
        });

    },
    /**
     * 상품 검색
     */
    search : function() {
        var searchKeyword = $('#searchKeyword').val();

        if(searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            }
            var searchType = $('#schType').val();
            switch(searchType) {
                case "itemCode" :
                    var pattern = /^\d{6}$/;
                    if(!pattern.test(searchKeyword) || Number(searchKeyword) < 500000) {
                        alert('검색하고자 하는 상품코드 전체를 입력하세요');
                        $('#searchKeyword').focus();
                        return false;
                    }
                    break;
            }
        }

        var mallType = $("input:radio[name=schStoreType]:checked").val() == "DS" ? "DS" : "TD";
        $("#mallType").val(mallType);
        var schStoreId = $("input:radio[name=schStoreType]:checked").val() == "DS" ? 0 : 37;
        $("#schStoreId").val(schStoreId);

        CommonAjax.basic({url:'/item/getItemList.json?' + $('#itemSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                itemListGrid.setData(res);
                $('#itemTotalCount').html($.jUtil.comma(itemListGrid.gridView.getItemCount()));
                if(itemListGrid.dataProvider.getRowCount() > 0) {
                    keywordRankJs.gridRowSelect(itemListGrid.dataProvider.getJsonRow(0));
                } else {
                    $("#itemNo").val('');
                    $("#itemNm").val('');
                    keywordRankJs.gridRowSelect();
                }
            }});
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $("#itemSearchForm").resetForm();

        keywordRankJs.initSearchDate('-30d');
    },
    saveFormRest : function() {
        $("#keywordList").val('');
        keywordRankJs.initSaveDate();
    },
    gridRowSelect: function (selectRow) {

        if(selectRow == null || selectRow == undefined) {
            keywordRankJs.saveFormRest();
            return;
        }

        var itemNo = selectRow.itemNo;

        $("#itemNo").val(selectRow.itemNo);
        $("#itemNm").val(selectRow.itemNm);

        CommonAjax.basic({url:'/search/keywordRank/'+itemNo+'.json', data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                if(res.result != null) {
                    keywordRankJs.initSaveDate(res.result.startDt,res.result.endDt);
                    $("#keywordList").val(res.result.keywordList);
                }
                else {
                    keywordRankJs.saveFormRest();
                }
            }});
    }
};



var itemListGrid = {
    gridView : new RealGridJS.GridView("itemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        itemListGrid.initGrid();
        itemListGrid.initDataProvider();
        itemListGrid.event();
    },
    initGrid : function() {
        itemListGrid.gridView.setDataSource(itemListGrid.dataProvider);

        itemListGrid.gridView.setStyles(itemListGridBaseInfo.realgrid.styles);
        itemListGrid.gridView.setDisplayOptions(itemListGridBaseInfo.realgrid.displayOptions);
        itemListGrid.gridView.setColumns(itemListGridBaseInfo.realgrid.columns);
        itemListGrid.gridView.setOptions(itemListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        itemListGrid.dataProvider.setFields(itemListGridBaseInfo.dataProvider.fields);
        itemListGrid.dataProvider.setOptions(itemListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        itemListGrid.gridView.onDataCellClicked = function(gridView, index) {
            var rowDataJson = itemListGrid.dataProvider.getJsonRow(index.dataRow);
            keywordRankJs.gridRowSelect(rowDataJson);
        };
    },
    setData : function(dataList) {
        itemListGrid.dataProvider.clearRows();
        itemListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(itemListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "상품관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        itemListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};