/***
 * 어휘 조회
 * @type {{init: relationSearchGrid.init, setData: relationSearchGrid.setData, initDataProvider: relationSearchGrid.initDataProvider, gridRowSelect: relationSearchGrid.gridRowSelect, dataProvider: *, initGrid: relationSearchGrid.initGrid, event: relationSearchGrid.event, gridView: *}}
 */
var relationSearchGrid = {
    gridView : new RealGridJS.GridView("relationSearchGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        relationSearchGrid.initGrid();
        relationSearchGrid.initDataProvider();
        relationSearchGrid.event();
    },
    initGrid : function () {
        relationSearchGrid.gridView.setDataSource(relationSearchGrid.dataProvider);

        relationSearchGrid.gridView.setStyles(dictionaryRelationSearchInfo.realgrid.styles);
        relationSearchGrid.gridView.setDisplayOptions(dictionaryRelationSearchInfo.realgrid.displayOptions);
        relationSearchGrid.gridView.setColumns(dictionaryRelationSearchInfo.realgrid.columns);
        relationSearchGrid.gridView.setOptions(dictionaryRelationSearchInfo.realgrid.options);
        relationSearchGrid.gridView.setCheckBar({ visible: false });
        relationSearchGrid.gridView.setSelectOptions({
            style: 'rows'
        });
    },
    initDataProvider : function() {
        relationSearchGrid.dataProvider.setFields(dictionaryRelationSearchInfo.dataProvider.fields);
        relationSearchGrid.dataProvider.setOptions(dictionaryRelationSearchInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        relationSearchGrid.gridView.onDataCellClicked = function(gridView, index) {
            relationSearchGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        relationSearchGrid.dataProvider.clearRows();
        relationSearchGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = relationSearchGrid.dataProvider.getJsonRow(selectRowId);
        $('#wordId').val(rowDataJson.wordId);
        $("#wordName").val(rowDataJson.wordName);
        $('#senseTag').val(rowDataJson.senseTag);
        $('#pos').val(rowDataJson.pos);
        $('#description').val(rowDataJson.description);
    }
};

/***
 * 기분석명사 : 검색용
 * @type {{init: relationSynonymGrid.init, setData: relationSynonymGrid.setData, initDataProvider: relationSynonymGrid.initDataProvider, gridRowSelect: relationSynonymGrid.gridRowSelect, dataProvider: *, initGrid: relationSynonymGrid.initGrid, event: relationSynonymGrid.event, gridView: *}}
 */
var relationSynonymGrid = {
    gridView : new RealGridJS.GridView("relationSynonymGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        relationSynonymGrid.initGrid();
        relationSynonymGrid.initDataProvider();
        relationSynonymGrid.event();
    },
    type : function() {
        return "SYNONYM";
    },
    initGrid : function () {
        relationSynonymGrid.gridView.setDataSource(relationSynonymGrid.dataProvider);

        relationSynonymGrid.gridView.setStyles(dictionaryRelationSynonymInfo.realgrid.styles);
        relationSynonymGrid.gridView.setDisplayOptions(dictionaryRelationSynonymInfo.realgrid.displayOptions);
        relationSynonymGrid.gridView.setColumns(dictionaryRelationSynonymInfo.realgrid.columns);
        relationSynonymGrid.gridView.setOptions(dictionaryRelationSynonymInfo.realgrid.options);
        relationSynonymGrid.gridView.setCheckBar({ visible: false });
        relationSynonymGrid.gridView.setSelectOptions({
            style: 'rows'
        });
        relationSynonymGrid.gridView.setEditOptions({
            editable : true,
            updatable : true
        });

        ///삭제 버튼
        relationSynonymGrid.gridView.onCellButtonClicked = function (grid, itemIndex, column) {
            relationSynonymGrid.dataProvider.removeRow(itemIndex);
        };

        ///행 편집 시도
        relationSynonymGrid.dataProvider.onRowUpdating = function (provider, row) {
            return true;
        };

        ///행 편집 완료
        relationSynonymGrid.dataProvider.onRowUpdated = function (provider, row) {
            var r = provider.getJsonRow(row);
        }
    },
    initDataProvider : function() {
        relationSynonymGrid.dataProvider.setFields(dictionaryRelationSynonymInfo.dataProvider.fields);
        relationSynonymGrid.dataProvider.setOptions(dictionaryRelationSynonymInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        relationSynonymGrid.dataProvider.clearRows();
        relationSynonymGrid.dataProvider.setRows(dataList);
    }
};

/***
 * 하위어
 * @type {{init: relationHyponymGrid.init, setData: relationHyponymGrid.setData, initDataProvider: relationHyponymGrid.initDataProvider, dataProvider: *, type: (function(): string), initGrid: relationHyponymGrid.initGrid, event: relationHyponymGrid.event, gridView: *}}
 */
var relationHyponymGrid = {
    gridView : new RealGridJS.GridView("relationHyponymGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        relationHyponymGrid.initGrid();
        relationHyponymGrid.initDataProvider();
        relationHyponymGrid.event();
    },
    type : function() {
        return "HYPONYM";
    },
    initGrid : function () {
        relationHyponymGrid.gridView.setDataSource(relationHyponymGrid.dataProvider);

        relationHyponymGrid.gridView.setStyles(dictionaryRelationHyponymInfo.realgrid.styles);
        relationHyponymGrid.gridView.setDisplayOptions(dictionaryRelationHyponymInfo.realgrid.displayOptions);
        relationHyponymGrid.gridView.setColumns(dictionaryRelationHyponymInfo.realgrid.columns);
        relationHyponymGrid.gridView.setOptions(dictionaryRelationHyponymInfo.realgrid.options);
        relationHyponymGrid.gridView.setCheckBar({ visible: false });
        relationHyponymGrid.gridView.setSelectOptions({
            style: 'rows'
        });
        relationHyponymGrid.gridView.setEditOptions({
            editable : true,
            updatable : true
        });

        ///삭제 버튼
        relationHyponymGrid.gridView.onCellButtonClicked = function (grid, itemIndex, column) {
            relationHyponymGrid.dataProvider.removeRow(itemIndex);
        };

        ///행 편집 시도
        relationHyponymGrid.dataProvider.onRowUpdating = function (provider, row) {
            return true;
        };

        ///행 편집 완료
        relationHyponymGrid.dataProvider.onRowUpdated = function (provider, row) {
            var r = provider.getJsonRow(row);
        }
    },
    initDataProvider : function() {
        relationHyponymGrid.dataProvider.setFields(dictionaryRelationHyponymInfo.dataProvider.fields);
        relationHyponymGrid.dataProvider.setOptions(dictionaryRelationHyponymInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        relationHyponymGrid.dataProvider.clearRows();
        relationHyponymGrid.dataProvider.setRows(dataList);
    }
};

/***
 * 연관어
 * @type {{init: relationRelatednymGrid.init, setData: relationRelatednymGrid.setData, initDataProvider: relationRelatednymGrid.initDataProvider, dataProvider: *, type: (function(): string), initGrid: relationRelatednymGrid.initGrid, event: relationRelatednymGrid.event, gridView: *}}
 */
var relationRelatednymGrid = {
    gridView : new RealGridJS.GridView("relationRelatednymGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        relationRelatednymGrid.initGrid();
        relationRelatednymGrid.initDataProvider();
        relationRelatednymGrid.event();
    },
    type : function() {
        return "RELATEDNYM";
    },
    initGrid : function () {
        relationRelatednymGrid.gridView.setDataSource(relationRelatednymGrid.dataProvider);

        relationRelatednymGrid.gridView.setStyles(dictionaryRelationRelatednymInfo.realgrid.styles);
        relationRelatednymGrid.gridView.setDisplayOptions(dictionaryRelationRelatednymInfo.realgrid.displayOptions);
        relationRelatednymGrid.gridView.setColumns(dictionaryRelationRelatednymInfo.realgrid.columns);
        relationRelatednymGrid.gridView.setOptions(dictionaryRelationRelatednymInfo.realgrid.options);
        relationRelatednymGrid.gridView.setCheckBar({ visible: false });
        relationRelatednymGrid.gridView.setSelectOptions({
            style: 'rows'
        });
        relationRelatednymGrid.gridView.setEditOptions({
            editable : true,
            updatable : true
        });

        ///삭제 버튼
        relationRelatednymGrid.gridView.onCellButtonClicked = function (grid, itemIndex, column) {
            relationRelatednymGrid.dataProvider.removeRow(itemIndex);
        };

        ///행 편집 시도
        relationRelatednymGrid.dataProvider.onRowUpdating = function (provider, row) {
            return true;
        };

        ///행 편집 완료
        relationRelatednymGrid.dataProvider.onRowUpdated = function (provider, row) {
            var r = provider.getJsonRow(row);
        }
    },
    initDataProvider : function() {
        relationRelatednymGrid.dataProvider.setFields(dictionaryRelationRelatednymInfo.dataProvider.fields);
        relationRelatednymGrid.dataProvider.setOptions(dictionaryRelationRelatednymInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        relationRelatednymGrid.dataProvider.clearRows();
        relationRelatednymGrid.dataProvider.setRows(dataList);
    }
};


/***
 * 관련어 : 공통스크립트
 * @type {{init: relationJs.init, search: relationJs.search, event: relationJs.event}}
 */
var relationJs = {
    init : function() {
        relationSearchGrid.init();
        relationSynonymGrid.init();
        relationHyponymGrid.init();
        relationRelatednymGrid.init();
        relationJs.onLoad();
    },
    onLoad : function() {
        CommonAjax.basic({url:'/search/dictionary/'+$('#requestWordId').val()+'/getRelationInfo.json', data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                if(res.synonyms != null)
                {
                    relationSynonymGrid.setData(res.synonyms);
                }
                if(res.hyponyms != null)
                {
                    relationHyponymGrid.setData(res.hyponyms);
                }
                if(res.relatednyms != null)
                {
                    relationRelatednymGrid.setData(res.relatednyms);
                }
            }});
    },
    event : function() {
        $('#searchBtn').click(function(){
            relationJs.search();
        });
        $('#writeBtn').click(function(){
            relationJs.windowOpen('/search/dictionary/wordWrite/' + $('#searchKeyword').val() + '?popup=1',972,1171, 'wordWrite');
        });
    },
    windowOpen  : function (url, _width, _height, _target) {
        // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
        var _left = ($(window).width()/2)-(_width/2);
        var _top = ($(window).height()/2)-(_height/2);
        if(_target == 'undefined' || _target == null || _target == '') {
            _target ='popup-search';
        }
        void(window.open(url, _target, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+ _width +', height='+ _height +', left=' + _left + ', top='+ _top ));
    },
    search : function() {

        $('#searchBtn').show();
        $('#writeBtn').hide();

        var searchKeyword = $('#searchKeyword').val().trim();
        if(searchKeyword == "") {
            return false;
        }

        var data = {};
        data.wordName = searchKeyword;
        CommonAjax.basic({url:'/search/dictionary/getSrchWordInfo.json', data:JSON.stringify(data),                     type: 'post',
            contentType: 'application/json',
            method:"POST", successMsg:null, callbackFunc:function(res) {
                relationSearchGrid.setData(res);
                if(relationSearchGrid.dataProvider.getRowCount() > 0) {
                    $('#wordId').val(relationSearchGrid.dataProvider.getJsonRow(0).wordId);
                    $('#wordName').val(relationSearchGrid.dataProvider.getJsonRow(0).wordName);
                    $('#senseTag').val(relationSearchGrid.dataProvider.getJsonRow(0).senseTag);
                    $('#pos').val(relationSearchGrid.dataProvider.getJsonRow(0).pos);
                    $('#description').val(relationSearchGrid.dataProvider.getJsonRow(0).description);
                }else{
                    $('#searchBtn').hide();
                    $('#writeBtn').show();
                }
            }});
    },
    /***
     * 어휘 이동
     * @param type
     * @returns {boolean}
     */
    move : function(grid) {
        if(relationSearchGrid.dataProvider.getRowCount() <= 0) {
            return false;
        }

        if($('#wordId').val() == $('#requestWordId').val()) ///할당하려는 단어와 "표제어"의 단어가 동일 할 경우 할당 못함
            return false;

        for(var i = 0; i < grid.dataProvider.getRowCount(); i++)
        {
            if(grid.dataProvider.getJsonRow(i).relId == $('#wordId').val())
                return false;
        }

        var values = [$('#wordId').val(), $("#wordName").val(), $('#senseTag').val(), $('#pos').val(), $('#description').val()];
        grid.dataProvider.addRow(values);
    },
    /***
     * 기분석 저장
     */
    save : function (grid) {
        var endpoint = "insertRelation.json";

        var data = {};
        data.list = new Array();
        data.list = grid.dataProvider.getJsonRows();
        data.relType = grid.type();

        CommonAjax.basic({ url:'/search/dictionary/'+$('#requestWordId').val()+'/' + endpoint,
            data: JSON.stringify(data),
            type: 'post',
            method : 'POST',
            contentType: 'application/json',
            successMsg : null,
            callbackFunc : function(res) {
                if(res != 1)
                {
                    alert('저장에 실패하였습니다.');
                }
                else
                {
                    alert('저장에 성공하였습니다.');
                }
            }});
    }
};

function reSearch() {
    relationJs.search();
}