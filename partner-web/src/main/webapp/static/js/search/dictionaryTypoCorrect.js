var typoCorrectJs = {
    init : function() {

    },
    event : function() {
        $("#setItemBtn").click(function() {
            var requestData = {};
            requestData.correctedTerms = $("#correctedTerms").val();
            CommonAjax.basic({
                url:'/search/dictionary/'+$("#wordId").val()+'/insertTypoCorrect.json',
                data: JSON.stringify(requestData),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                callbackFunc:function(res) {
                    if(res == 1)
                    {
                        alert('등록 되었습니다.');
                        opener.parent.dictionaryList.search();
                        self.close();
                    }
                    else
                    {
                        alert('등록에 실패했습니다.');
                    }
                }
            });


        });

        $("#resetBtn").click(function () {
            self.close();
        });
    }
};

$(function() {
    typoCorrectJs.init();
    typoCorrectJs.event();
});