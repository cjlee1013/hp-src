$(document).ready(function() {

    csQnaMain.init();
    CommonAjaxBlockUI.global();
});


/**
 * 고객관리 > 상품 QnA상세
 */
var csQnaMain = {
    /**
     * 초기화
     */
    templateList : null,
    templateDiv : null,
    init : function() {

        this.event();
        this.getQnaDetail();

    },
    /**
     * 이벤트 바인딩
     */
    event : function() {

        $(document).on('change','.template',function(){

            if($(this).val() == 'default'){

                $(this).parent().parent().find('.textarea').val('').prop('placeholder','신규로 작성하려는 내용 입력');

            } else{

                var contents = templateList[$(this).val()].templateContents;
                $(this).parent().parent().find('.textarea').val(contents);
            }


        });
        $(document).on('keyup','.textarea',function(){
            $(this).parent().find('.cnt').html($(this).val().length);
        });

    },
    getQnaDetail : function(){

        CommonAjax.basic({url:'/voc/csQna/getQnaDetail.json?qnaNo=' +$('#qnaNo').val(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {

                $('#replyForm').resetForm();
                $('#applyArea').html('');
                $('#qnaType').html(res.qnaType);
                $('#regDt').html(res.regDt);
                $('#compDt').html(res.compDt);
                $('#userNm').html(res.userNm);
                $('#userId').html(res.userId);
                $('#qnaContents').text(res.qnaContents);

                templateList  = res.qnaTemplateList;
                templateDiv = csQnaMain.appendTemplate();

                var replyDiv = csQnaMain.appendReplyList(res.qnaReplyList, templateDiv);

                if(res.displayStatus == 'DISP'){

                    replyDiv +='<div class="request-wrap" id="applyNew">\n'
                    +'<div class="mgt10"> <select class="slc-base w-325 template" title="템플릿선택">'
                    +  templateDiv
                    +'</select></div>'
                    + '<div class="words-wrap  w-max mgt10">\n'
                    + '<textarea class="textarea w-max" id="reply_new" style="height: 200px" title="답변등록" placeholder="신규로 작성하려는 내용 입력"></textarea>\n'
                    + '<span class="words"><i><span class="cnt">0</span></i>/1500자</span>\n'
                    + '</div>\n'
                    + '<button type="button" class="btn-base type4" onclick="csQnaMain.addReply();" ><i>등록</i></button>\n'
                    + '</div>'

                } else{

                    $("[class^='template']").prop('disabled', true);
                }

                $('#applyArea').append(replyDiv);

            }});

    },

    appendTemplate : function(){

        var templateSelectDiv ='<option value=default>기본답변</option>';
        for (var i in templateList) {
            templateSelectDiv+='<option value='+i+ '>'+ templateList[i].templateTitle +'</option>';
        }
        return templateSelectDiv;

    },
    appendReply : function(reply,templateDiv ){
        var replyDiv ='';

        replyDiv +=' <div class="request-wrap">'

        //답변자 영여
        if(reply.replyArea == 'PARTNER') {
            replyDiv += '<span class="mgr10">판매자(' + reply.partnerNm +')</span>';
        } else{
            replyDiv += '<span class="mgr10">홈플러스(관리자)</span>';
        }

        //답변 노출 상태
        if(reply.displayStatus == 'DISP') {
            replyDiv +='<em class="flag type3">노출중</em>';
        } else{
            replyDiv +='<em class="flag">노출중지</em>';
        }

        //텝플릿 선택 영역
        replyDiv +='<div class="mgt10"><select class="slc-base w-325 template" title="템플릿선택"';
        if (reply.displayStatus != 'DISP' || reply.replyArea == 'ADMIN' || reply.qnaDisplayStatus!='DISP' ){
            replyDiv += "disabled";

        }
        replyDiv +='>' + templateDiv;
        replyDiv += '</select></div>';


        //텍스트 입력 영역
        replyDiv += '<div class="words-wrap  w-max mgt10">';
        replyDiv += '<textarea id="reply_'+ reply.qnaReplyNo+'"class="textarea w-max" style="height: 200px" title="답변등록" ';
        if (reply.displayStatus != 'DISP' || reply.replyArea == 'ADMIN' || reply.qnaDisplayStatus!='DISP' ){
            replyDiv += "disabled";
        }
        replyDiv +='>' + reply.qnaReply + '</textarea>';
        replyDiv += '<span class="words"><i><span class="cnt">'+ reply.qnaReply.length +'</span></i>/1500자</span>';
        replyDiv += '</div>';
        $('#reply_'+reply.qnaReplyNo).calcTextLength('keyup', '#reply_txt_cnt_'+reply.qnaReplyNo);
        //버튼 영역
        if (reply.qnaDisplayStatus =='DISP' && reply.displayStatus == 'DISP') {
            if (reply.replyArea != 'ADMIN') {
                replyDiv += '<button type="button" class="btn-base type2" onclick="csQnaMain.modifyReply(' + reply.qnaReplyNo + ');"><i>수정</i></button>';
            }
        }
        replyDiv +='</div>'

        return replyDiv;

    },
    appendReplyList : function(replyList, templateDiv){

        var replyDiv ='';

        for(var i in replyList){
            replyDiv+=csQnaMain.appendReply(replyList[i], templateDiv)
        }

        return replyDiv

    }
    ,
    modifyReply : function (replyNo) {

        if(confirm("해당 문의에 대한 답변을 수정하시겠습니?")){
            csQnaMain.setQnaReply(replyNo, $('#reply_' +replyNo ).val());
        }

    }
    ,
    addReply : function () {

        csQnaMain.setQnaReply(null, $('#reply_new').val());

    }
    ,
    setQnaReply : function(qnaReplyNo, replyTxt){

        var setQnaReplay = new Object();
        setQnaReplay.qnaNo = $('#qnaNo').val();  //선택된 Q&A 번호
        setQnaReplay.replyArea = 'PARTNER';  //답변영역(ADMIN, PARTNER)
        setQnaReplay.qnaReplyNo = qnaReplyNo;  //선택된 Q&A 답변 번호
        setQnaReplay.displayStatus = 'DISP'; //노출 여부
        setQnaReplay.qnaReply = replyTxt; //답변 내용

        if($.jUtil.isEmpty(replyTxt) || replyTxt.length<10){
            alert("10자 이상으로 작성해주세요.");
            return false;
        }

        CommonAjax.basic({url:'/voc/csQna/setQnaReply.json',data: JSON.stringify(setQnaReplay), method:"POST", successMsg:null, contentType:"application/json",callbackFunc:function(res) {
                alert(res.returnMsg);
                csQnaMain.getQnaDetail();
            }});

    },
};

