$(document).ready(function() {
    itemReviewDetailMain.init();
    CommonAjaxBlockUI.global();
});

/**
 * 고객관리 > 상품 QnA상세
 */
var itemReviewDetailMain = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
        this.getItemReviewDetail();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {

    },
    getItemReviewDetail : function() {
        CommonAjax.basic({
            url :'/voc/itemReview/getItemReviewDetail.json?reviewNo=' + reviewNo
            , data : null
            , method : "GET"
            , successMsg : null
            , callbackFunc : function(res) {
                $('#itemNo').html(res.itemNo);
                $('#itemNm').html(res.itemNm);
                $('#userNm').html(res.userNm);
                $('#regDt').html(res.regDt);
                $('#grade').html(res.grade);
                $('#gradeStatusNm').html(res.gradeStatusNm);
                $('#gradeAccuracyNm').html(res.gradeAccuracyNm);
                $('#gradePriceNm').html(res.gradePriceNm);
                $('#contents').html(res.contents);
                var imgList = res.imgList.split(',');
                if(!$.jUtil.isEmpty(imgList)) {
                    var html = '';
                    imgList.forEach(function (data) {
                        var imgSrc = hmpImgUrl + "/provide/view?processKey=UserCommentImage&fileId=" + data;
                        html += '<img src="' + imgSrc + '" alt="임시">';
                    });

                    $('#imgDiv').append(html);
                }
            }});
    }
};

