// 상품조회
var csQnaTemplatePop = {
    /**
     * 초기화
     */
    init : function() {
        csQnaTemplatePop.event();
        csQnaTemplatePop.csQnaTemplatePopFormReset();

    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $("#resetBtn").bindClick(csQnaTemplatePop.csQnaTemplatePopFormReset);
        $("#addTemplate").bindClick(csQnaTemplatePop.addTemplate);
        $("#delTemplate").bindClick(csQnaTemplatePop.delTemplate);
        $('#templateContents').calcTextLength('keyup', '#textCountTemplateContents');

    },
    /**
     * Qna Template 등록
     */
    addTemplate : function(){

        if(!$.jUtil.isEmpty( $('#templateNo').val() )){
            if(!confirm("해당 탬플릿을 수정 하시겠습니까?")) {
                return false;
            }

        }

        var setTemplate = $('#csQnaTemplatePopForm').serializeObject(true);
        setTemplate.useYn = 'Y';
        csQnaTemplatePop.setTemplate(setTemplate);
    },

    /**
     * Qna Template 삭제
     */
    delTemplate : function(){


        if(confirm("해당 탬플릿을 삭제 하시겠습니까?")){
            var setTemplate = $('#csQnaTemplatePopForm').serializeObject(true);
            setTemplate.useYn ='N';
            csQnaTemplatePop.setTemplate(setTemplate);
        }

    },
    /**
     * Qna Template 저장
     */
    setTemplate : function(obj){

        CommonAjax.basic({url:'/voc/csQna/setQnaTemplate.json',data: JSON.stringify(obj), method:"POST", successMsg:null, contentType:"application/json",callbackFunc:function(res) {
                alert(res.returnMsg);
                csQnaTemplatePop.csQnaTemplatePopFormReset();
                csQnaTemplatePop.getTemplateList();
            }});

    },
    /**
     * Qna Template form 초기화
     */
    csQnaTemplatePopFormReset : function() {

        $("#csQnaTemplatePopForm").resetForm();
        $("#templateNo").val('');
        $("#textCountTemplateContents").html('0');

    },

    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('csQnaTemplatePop', csQnaTemplatePop.getTemplateList);
        csQnaTemplatePopGrid.gridView.resetSize();
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('csQnaTemplatePop');
    },
    /**
     * 템플릿 조회
     */
    getTemplateList : function () {

            CommonAjax.basic({
                url: "/voc/csQna/getTemplateList.json"
                , data: null
                , method: "GET"
                , successMsg: null
                , callbackFunc: function (res) {
                    csQnaTemplatePopGrid.setData(res);

                }
            });
    },
    gridRowSelect : function(selectRowId) {

        var rowDataJson = csQnaTemplatePopGrid.dataProvider.getJsonRow(selectRowId);
        $('#templateTitle').val(rowDataJson.templateTitle);
        $('#templateContents').val(rowDataJson.templateContents);
        $('#templateNo').val(rowDataJson.templateNo);
        $("#textCountTemplateContents").html(rowDataJson.templateContents.length);

    }
};

// 상품조회 공통팝업 그리드
var csQnaTemplatePopGrid = {
    gridView : new RealGridJS.GridView("csQnaTemplatePopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        csQnaTemplatePopGrid.initItemPopGrid();
        csQnaTemplatePopGrid.initDataProvider();
        csQnaTemplatePopGrid.event();
    },
    /**
     * 조회 그리드 설정
     */
    initItemPopGrid : function() {
        csQnaTemplatePopGrid.gridView.setDataSource(csQnaTemplatePopGrid.dataProvider);
        csQnaTemplatePopGrid.gridView.setStyles(csQnaTemplatePopGridBaseInfo.realgrid.styles);
        csQnaTemplatePopGrid.gridView.setDisplayOptions(csQnaTemplatePopGridBaseInfo.realgrid.displayOptions);
        csQnaTemplatePopGrid.gridView.setColumns(csQnaTemplatePopGridBaseInfo.realgrid.columns);
        csQnaTemplatePopGrid.gridView.setOptions(csQnaTemplatePopGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        csQnaTemplatePopGrid.dataProvider.setFields(csQnaTemplatePopGridBaseInfo.dataProvider.fields);
        csQnaTemplatePopGrid.dataProvider.setOptions(csQnaTemplatePopGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        csQnaTemplatePopGrid.gridView.onDataCellClicked = function(gridView, index) {
            csQnaTemplatePop.gridRowSelect(index.dataRow);
        };
    },
    clear : function () {
        csQnaTemplatePopGrid.dataProvider.clearRows();

    }
    ,
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        csQnaTemplatePopGrid.dataProvider.clearRows();
        csQnaTemplatePopGrid.dataProvider.setRows(dataList);
    }

};
