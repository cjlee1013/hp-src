$(document).ready(function() {
    itemReviewMain.init();
    itemReviewListGrid.init();
    CommonAjaxBlockUI.global();
});

var itemReviewListGrid = {

    gridView : new RealGridJS.GridView("itemReviewListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        itemReviewListGrid.initGrid();
        itemReviewListGrid.initDataProvider();
        itemReviewListGrid.event();
    },
    initGrid : function() {
        itemReviewListGrid.gridView.setDataSource(itemReviewListGrid.dataProvider);

        itemReviewListGrid.gridView.setStyles(itemReviewListGridBaseInfo.realgrid.styles);
        itemReviewListGrid.gridView.setDisplayOptions(itemReviewListGridBaseInfo.realgrid.displayOptions);
        itemReviewListGrid.gridView.setColumns(itemReviewListGridBaseInfo.realgrid.columns);
        itemReviewListGrid.gridView.setOptions(itemReviewListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        itemReviewListGrid.dataProvider.setFields(itemReviewListGridBaseInfo.dataProvider.fields);
        itemReviewListGrid.dataProvider.setOptions(itemReviewListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        itemReviewListGrid.gridView.onDataCellClicked = function(gridView, index) {
            itemReviewMain.modifyBannerOpenTab(index.dataRow);
        };
    },
    setData : function(dataList) {
        itemReviewListGrid.dataProvider.clearRows();
        itemReviewListGrid.dataProvider.setRows(dataList);
    }
};

/**
 * 고객관리 > 상품평 관리
 */
var itemReviewMain = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
        this.initSearchDate('-7d');
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        itemReviewMain.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');

        itemReviewMain.setDate.setEndDate(0);
        itemReviewMain.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "searchStartDt", $("#schStartDate").val());
    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    modifyBannerOpenTab : function(selectRowId) {
        var rowDataJson = itemReviewListGrid.dataProvider.getJsonRow(selectRowId);
        var param = "?reviewNo=" + rowDataJson.reviewNo;

        UIUtil.addTabWindow('/voc/itemReview/itemReviewDetailMain' + param,'Tab_itemReviewMainDetail', '상품평관리 상세', '140');
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {

        $('#searchResetBtn').bindClick(itemReviewMain.searchFormReset);
        $('#searchBtn').bindClick(itemReviewMain.searchBySchArea);
        $('input:radio[name="setSchDate"]').on('change', function() {
            itemReviewMain.searchDate();
        });
    },
    searchDate : function () {
        var flag = $('input[name="setSchDate"]:checked').val();

        //이번달
        if("M" == flag){
            var date = new Date();
            var this_month = date.getDate()-1;
            flag = '-'+this_month;
        }

        itemReviewMain.initSearchDate(flag);
    },
    /**
     * 상품 검색
     */
    searchBySchArea : function() {
        var searchKeyword = $('#searchKeyword').val();
        var schStartDt = moment($("#searchStartDt").val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($("#searchEndDt").val(), 'YYYY-MM-DD', true);

        if(searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            }
            if ($("#searchKeyword").val() != "" && $("#searchKeyword").val().length < 2) {
                alert("검색어는 최소 2자리 이상 입력하세요.");
                return false;
            }
        }

        if(!schStartDt.isValid() || !schEndDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if (schEndDt.diff(schStartDt, "day", true) > 7) {
            alert("조회기간은 최대 1주일 범위까지만 가능합니다. 조회기간을 다시 설정해주세요.");
            $("#searchStartDt").val(schEndDt.add(-7, "day").format("YYYY-MM-DD"));
            return false;
        }
        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#searchStartDt").val(schEndDt.format("YYYY-MM-DD"));
            return false;
        }

        itemReviewMain.search();
    },
    /**
     * qnaStatus Type 검색
     * @param val
     */

    search : function () {
        CommonAjax.basic({
            url : '/voc/itemReview/getItemReviewList.json?' + $('#itemReviewSearchForm').serialize()
            , data : null
            , method : "GET"
            , successMsg : null
            , callbackFunc : function(res) {
                itemReviewListGrid.setData(res);
                $('#itemReviewTotalCount').html($.jUtil.comma(itemReviewListGrid.gridView.getItemCount()));
            }});
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchType').val('ITEMNM');
        $('#searchKeyword').val('');
        itemReviewMain.initSearchDate('-7d');

    }
};

