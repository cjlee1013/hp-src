$(document).ready(function() {

    csQnaMain.init();
    csQnaListGrid.init();
    CommonAjaxBlockUI.global();
});

var csQnaListGrid = {

    gridView : new RealGridJS.GridView("csQnaListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        csQnaListGrid.initGrid();
        csQnaListGrid.initDataProvider();
        csQnaListGrid.event();

    },
    initGrid : function() {
        csQnaListGrid.gridView.setDataSource(csQnaListGrid.dataProvider);

        csQnaListGrid.gridView.setStyles(csQnaListGridBaseInfo.realgrid.styles);
        csQnaListGrid.gridView.setDisplayOptions(csQnaListGridBaseInfo.realgrid.displayOptions);
        csQnaListGrid.gridView.setColumns(csQnaListGridBaseInfo.realgrid.columns);
        csQnaListGrid.gridView.setOptions(csQnaListGridBaseInfo.realgrid.options);


    },
    initDataProvider : function() {
        csQnaListGrid.dataProvider.setFields(csQnaListGridBaseInfo.dataProvider.fields);
        csQnaListGrid.dataProvider.setOptions(csQnaListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        csQnaListGrid.gridView.onDataCellClicked = function(gridView, index) {
            csQnaMain.modifyBannerOpenTab(index.dataRow);
        };

    },
    setData : function(dataList) {
        csQnaListGrid.dataProvider.clearRows();
        csQnaListGrid.dataProvider.setRows(dataList);
    }

};

/**
 * 상품관리 > 셀러/점포상품관리 > 상품 등록/수정
 */
var csQnaMain = {
    /**
     * 초기화
     */
    init : function() {

        this.event();
        this.initSearchDate('-7d');
        this.setQnaMainBoardCnt();
        if($.jUtil.isNotEmpty(qnaStatus)){
            csQnaMain.searchByQnaStatus(qnaStatus);
        }
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        csQnaMain.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');

        csQnaMain.setDate.setEndDate(0);
        csQnaMain.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "searchStartDt", $("#schStartDate").val());
    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    modifyBannerOpenTab : function(selectRowId) {
        var rowDataJson = csQnaListGrid.dataProvider.getJsonRow(selectRowId);
        var param = "?qnaNo="+rowDataJson.qnaNo;

        UIUtil.addTabWindow('/voc/csQna/csQnaDetailMain' + param,'Tab_qnaMainDetail', '상품Q&A 상세', '140');
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {

        $('#searchResetBtn').bindClick(csQnaMain.searchFormReset);
        $('#searchBtn').bindClick(csQnaMain.searchBySchArea);
        $('#openTemplatePop').bindClick(csQnaMain.openTemplatePop);
        $('input:radio[name="setSchDate"]').on('change', function() {
            csQnaMain.searchDate();
        });

    },
    searchDate : function () {
        var flag = $('input[name="setSchDate"]:checked').val();

        //이번달
        if("M" == flag){
            var date = new Date();
            var this_month = date.getDate()-1;
            flag = '-'+this_month;
        }

        csQnaMain.initSearchDate(flag);

    },

    /**
     * 판매상태 별 상품수 요약
     */
    setQnaMainBoardCnt : function () {

        CommonAjax.basic({url:'/voc/csQna/getQnaBoardCount.json?'
            , data:null
            , method:"GET"
            , successMsg:null
            , callbackFunc:function(resArr) {
                var total = 0;
                $.each(resArr, function (idx, val) {
                    if(!$.jUtil.isEmpty(val)) {
                        $('#'+val.qnaStatus).text($.jUtil.comma(val.cnt));
                    }
                });

            }});

    },
    /**
     * 상품 검색
     */
    searchBySchArea : function() {

        $('#searchArea').val('SCH');
        var searchKeyword = $('#searchKeyword').val();
        var schStartDt = moment($("#searchStartDt").val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($("#searchEndDt").val(), 'YYYY-MM-DD', true);


        if(searchKeyword != "") {

            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            }
            if ($("#searchKeyword").val() != "" && $("#searchKeyword").val().length < 2) {
                alert("검색어는 최소 2자리 이상 입력하세요.");
                return false;
            }
        }

        if(!schStartDt.isValid() || !schEndDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if (schEndDt.diff(schStartDt, "day", true) > 7) {
            alert("조회기간은 최대 1주일 범위까지만 가능합니다. 조회기간을 다시 설정해주세요.");
            $("#searchStartDt").val(schEndDt.add(-7, "day").format("YYYY-MM-DD"));
            return false;
        }
        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#searchStartDt").val(schEndDt.format("YYYY-MM-DD"));
            return false;
        }

        csQnaMain.search();
    },
    /**
     * qnaStatus Type 검색
     * @param val
     */

    search : function () {

        CommonAjax.basic({url:'/voc/csQna/getQnaList.json?' + $('#csQnaSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                csQnaListGrid.setData(res);
                $('#csQnaTotalCount').html($.jUtil.comma(csQnaListGrid.gridView.getItemCount()));
            }});

    },
    /**
     * qnaStatus Type 검색
     * @param val
     */
    searchByQnaStatus : function (val) {

        $('#searchArea').val(val)
        csQnaMain.search();
        csQnaMain.searchFormReset();
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {

        $("#csQnaSearchForm").resetForm();
        $('#searchArea').val('SCH');
        csQnaMain.initSearchDate('-7d');

    },

};

