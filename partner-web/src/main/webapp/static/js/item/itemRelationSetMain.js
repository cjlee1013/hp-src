/**
 * 상품관리>점포상품관리>연관상품관리
 */
$(document).ready(function() {

    itemRelationDetailListGrid.init();
    itemRelationSet.init();
    CommonAjaxBlockUI.global();

    parentMain = itemRelationSet;

    //수정일 경우 공지 내용을 조회
    if('Y' == $('#isMod').val()) {
        itemRelationSet.getItemRelationDetail();
    }else{
        itemRelationSet.resetForm();
    }

});

var itemRelationDetailListGrid = {
    selectedRowId : null,
    gridView : new RealGridJS.GridView("itemRelationDetailListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        itemRelationDetailListGrid.initGrid();
        itemRelationDetailListGrid.initDataProvider();
        itemRelationDetailListGrid.event();
    },
    initGrid : function () {
        itemRelationDetailListGrid.gridView.setDataSource(itemRelationDetailListGrid.dataProvider);

        itemRelationDetailListGrid.gridView.setStyles(itemRelationDetailListGridBaseInfo.realgrid.styles);
        itemRelationDetailListGrid.gridView.setDisplayOptions(itemRelationDetailListGridBaseInfo.realgrid.displayOptions);
        itemRelationDetailListGrid.gridView.setColumns(itemRelationDetailListGridBaseInfo.realgrid.columns);
        itemRelationDetailListGrid.gridView.setOptions(itemRelationDetailListGridBaseInfo.realgrid.options);
        itemRelationDetailListGrid.gridView.setColumnProperty("representYn", "renderer", {
            "type": "check", "shape":"box", "editable": true, "startEditOnClick": true, "trueValues": "Y", "falseValues": "N", "labelPosition": "center"
        });

    },
    initDataProvider : function() {
        itemRelationDetailListGrid.dataProvider.setFields(itemRelationDetailListGridBaseInfo.dataProvider.fields);
        itemRelationDetailListGrid.dataProvider.setOptions(itemRelationDetailListGridBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        itemRelationDetailListGrid.dataProvider.clearRows();
        itemRelationDetailListGrid.dataProvider.setRows(dataList);
    },
    //그리드 초기화
    clear : function () {
        itemRelationDetailListGrid.dataProvider.clearRows();
        $('#itemRelationDetailTotalCount').html(0);
    },
    // 상품 그리드 row 이동
    itemGridMoveRow : function(obj) {
        let checkedRows = itemRelationDetailListGrid.gridView.getCheckedRows(false);

        if (checkedRows.length == 0) {
            alert('순서변경할 상품을 선택해 주세요.');
            return false;
        }

        realGridMoveRow(itemRelationDetailListGrid, $(obj).attr('key'), checkedRows);
    }
};
var itemRelationSet = {
    /**
     * 초기화
     */
    init: function () {
        this.event();
        this.initCategorySelectBox();
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        itemRelationSet.setDate = Calendar.datePickerRange('schStartDate', 'schEndDate');

        itemRelationSet.setDate.setEndDate(0);
        itemRelationSet.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate", $("#schStartDate").val());
    },
    /**
     * 이벤트 바인딩
     */
    event: function () {

        $('#setItemRelationBtn').bindClick(itemRelationSet.setItemRelation);

        $('#schItemPopBtn').bindClick(itemRelationSet.addItemPopUp);

        $('.itemMoveCtrl').on('click', function() {
            itemRelationDetailListGrid.itemGridMoveRow(this);
        });

        $('#relationNm').calcTextLength('keyup', '#relationNmCount');

        $('input:radio[name="mRelationType"]').on('change', function() {
            itemRelationSet.changeDispMobileType($(this).val());
        });

        $('input:radio[name="pcRelationType"]').on('change', function() {
            itemRelationSet.changeDispPcType($(this).val());
        });

        $('#resetBtn').on('click', function() {
            itemRelationSet.resetFormConfirm();
        });

        //파일선택시
        $('input[name="introImg"]').change(function() {
            itemCommon.img.addImg($(this).data('file-id'), $(this).prop('name'), 3145728, "3MB");
        });

        $('#deleteItem').bindClick(itemRelationSet.deleteItemRelationSelect);

        itemCommon.img.event();

    },
    changeDispMobileType : function(type) {

        $('input:radio[name="mRelationType"]:input[value="' + type + '"]').trigger('click');
        switch (type) {
            case "LIST" :
                $('.mListArea').show();
                $('.mCardArea').hide();
                $('.mListArea').find('input:radio').eq(0).prop("checked", true);
                break;

            case "CARD" :
                $('.mListArea').hide();
                $('.mCardArea').show();
                $('.mCardArea').find('input:radio').eq(0).prop("checked", true);
                break;
        }
    },
    changeDispPcType : function(type) {
        $('input:radio[name="pcRelationType"]:input[value="' + type + '"]').trigger('click');

        switch (type) {
            case "LIST" :
                $('.pcListArea').show();
                $('.pcCardArea').hide();
                $('.pcListArea').find('input:radio').eq(0).prop("checked", true);
                break;

            case "CARD" :
                $('.pcListArea').hide();
                $('.pcCardArea').show();
                $('.pcCardArea').find('input:radio').eq(0).prop("checked", true);
                break;
        }
    },
    /**
     * 연관 상품관리 그리드 선택 영역
     * @param selectRowId
     */
    getItemRelationDetail : function () {

        CommonAjax.basic( {
            url: '/item/getItemRelation.json?',
            data: {relationNo: ($('#relationNo').val())},
            method: "GET",
            callbackFunc: function (data) {
                $('#relationNo').val(data.relationNo);
                $('#relationNoText').text(data.relationNo);
                $('#relationNm').val(data.relationNm);
                $('#scateCd').val(data.scateCd);

                $('input:radio[name="dispYn"]:input[value="' + data.dispYn + '"]').trigger('click');

                itemRelationSet.changeDispPcType(data.dispPcType.substr(0,4));
                itemRelationSet.changeDispMobileType(data.dispMobileType.substr(0,4));

                $('input:radio[name="dispPcType"]:input[value="' + data.dispPcType + '"]').trigger('click');
                $('input:radio[name="dispMobileType"]:input[value="' + data.dispMobileType + '"]').trigger('click');

                $('#relationNm').setTextLength('#relationNmCount');

                if (!$.jUtil.isEmpty(data.imgUrl)) {
                    itemCommon.img.setImg($('#imgIntro'), {
                        'imgNm' : data.imgNm,
                        'imgUrl': data.imgUrl,
                        'src'   : hmpImgUrl + data.imgUrl,
                    });
                }

                CommonAjax.basic( {
                    url: '/item/getItemRelationDetailList.json?',
                    data: {relationNo: (data.relationNo)},
                    method: "GET",
                    callbackFunc: function (res) {
                        itemRelationDetailListGrid.setData(res);
                        $('#itemRelationDetailTotalCount').html($.jUtil.comma(itemRelationDetailListGrid.gridView.getItemCount()));
                    }
                });
            }
        });
    },
    /**
     * 연관 상품관리 등록/수정
     * @returns {boolean}
     */
    setItemRelation : function () {

        if(!itemRelationSet.valid.set()){
            return false;
        }

        if (itemRelationSet.valid.set()) {
            let form = $('#itemRelationSetForm').serializeObject();
            form.useYn = 'Y';
            form.relationItemList = [];

            for(let _idx = 0; _idx < itemRelationDetailListGrid.dataProvider.getRowCount(); _idx++){
                form.relationItemList.push(itemRelationDetailListGrid.dataProvider.getValue(_idx, "itemNo"));
            }

            CommonAjax.basic({
                url: '/item/setItemRelation.json',
                data: JSON.stringify(form),
                contentType: 'application/json',
                method:"POST",
                successMsg:null,
                callbackFunc:function(res) {
                    alert(res.returnMsg);
                    $('#relationNo').val(res.returnKey);
                    itemRelationSet.getItemRelationDetail();
                },
                error: function(e) {
                    if(e.responseJSON.errors[0].detail != null) {
                        alert(e.responseJSON.errors[0].detail);
                    } else {
                        alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                    }
                }
            });
        }
    },
    resetFormConfirm : function () {

        if( !confirm("작성을 취소하시겠습니까?")){
            return false;
        }

        if('Y' == $('#isMod').val()) {
            UIUtil.RemoveTabWindow('Tab_itemRelationMod');
        }else {
            UIUtil.RemoveTabWindow('Tab_ItemRelationSet');
        }

    },

    /**
     * 연관 상품관리 등록창 초기화
     */
    resetForm : function () {

        $('#itemRelationSetForm').resetForm();
        $('#relationNmCount').text(0);
        $('#relationNo, #scateCd').val('');
        $('#relationNoText').text('');

        itemRelationSet.changeDispMobileType("LIST");
        itemRelationSet.changeDispPcType("LIST");

        itemRelationDetailListGrid.clear();
        itemCommon.img.init();

    },
    /**
     * 연관 상품관리 상품조회 팝업
     * @returns {boolean}
     */
    addItemPopUp : function(){
        itemPop.openModal();
    },
    /**
     * 상품 팝업 Callback
     * @param res
     */
    callback :function (param) {
        let data = param.reverse();
        let checkCount = itemRelationDetailListGrid.gridView.getItemCount();
        let setData = [];
        let limitCnt = 50;

        for (let i in data) {
            if (checkCount < limitCnt) {
                if (itemRelationDetailListGrid.dataProvider.searchDataRow({fields : ["itemNo"], values : [data[i].itemNo]}) == -1) {
                    setData.push(data[i]);
                    checkCount++;
                }
            } else {
                checkCount++;
            }
        }

        for (let i in setData) {
            itemRelationSet.applyItemPopupCallbackAjax(setData[i]);
        }

        if (checkCount > limitCnt) {
            $.jUtil.alert('하나의 연관에 최대 ' + limitCnt + '개 상품을 설정 할 수 있습니다.');
        }
    },
    /**
     * 검색상품 관련 정보 조회
     * @param data
     */
    applyItemPopupCallbackAjax : function(data) {
        CommonAjax.basic( {
            url: '/item/getItemRelationDetail.json?',
            data: {itemNo: (data.itemNo)},
            method: "GET",
            callbackFunc: function (res) {
                if ($.jUtil.isEmpty($('#scateCd').val()) || itemRelationDetailListGrid.gridView.getItemCount() == 0) {
                    $('#scateCd').val(res.scateCd);
                }

                itemRelationSet.addItemRelationDetail(res);

                $('#itemRelationDetailTotalCount').html($.jUtil.comma(itemRelationDetailListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 연관 상품관리 상품 추가
     * @returns {boolean}
     */
    addItemRelationDetail : function(data) {

        var value = {};

        value.adultTypeNm   = data.adultTypeNm;
        value.adultType     = data.adultType;
        value.cartLimitYn   = data.cartLimitYn;
        value.cartLimitYnNm = data.cartLimitYnNm;
        value.optTxtUseYn   = data.optTxtUseYn;
        value.optTxtUseYnNm = data.optTxtUseYnNm;
        value.itemNm        = data.itemNm;
        value.itemNo        = data.itemNo;
        value.itemStatusNm  = data.itemStatusNm;
        value.lcateNm       = data.lcateNm;
        value.mcateNm       = data.mcateNm;
        value.saleEndDt     = data.saleEndDt;
        value.salePrice     = data.salePrice;
        value.saleStartDt   = data.saleStartDt;
        value.scateNm       = data.scateNm;
        value.scateCd       = data.scateCd;

        itemRelationDetailListGrid.dataProvider.addRow(value);
    },
    /**
     * 상품 선택삭제
     * @returns {boolean}
     */
    deleteItemRelationSelect : function() {

        let rowArr = itemRelationDetailListGrid.gridView.getCheckedRows(true);

        if (rowArr.length == 0) {
            alert('삭제하려는 상품을 선택해주세요.');
            return false;
        }

        if (confirm('선택한 상품을 모두 삭제하시겠습니까?')) {
            for (let i in rowArr) {
                itemRelationDetailListGrid.dataProvider.removeRow(rowArr[i] - parseInt(i));
            }

            $('#itemRelationDetailTotalCount').text($.jUtil.comma(itemRelationDetailListGrid.gridView.getItemCount()));
        }
    },
}

/**
 * itemRelation validation.
 * @type {{search: itemRelationSet.valid.search, set: itemRelationSet.valid.set}}
 */
itemRelationSet.valid = {

    set : function() {
        var checkSet = true;

        //기본정보
        if ($.jUtil.isEmpty($('#relationNm').val())) {
            return $.jUtil.alert("연관명을 입력해주세요.", 'relationNm');
        }

        if (itemRelationDetailListGrid.gridView.getItemCount() == 0) {
            return $.jUtil.alert("연관상품 정보를 등록해주세요.");
        }

        //상품정보
        var checkAdultTypeJson = itemRelationDetailListGrid.dataProvider.getJsonRow(0);
        var adultType = checkAdultTypeJson.adultType;

        for (let i = 0; i < itemRelationDetailListGrid.gridView.getItemCount(); i++) {
            var rowDataJson = itemRelationDetailListGrid.dataProvider.getJsonRow(i);
            if (rowDataJson.adultType != adultType || "Y" == rowDataJson.cartLimitYn || "Y" == rowDataJson.optTxtUseYn || $('#scateCd').val() != rowDataJson.scateCd ) {
                checkSet = false;
                return $.jUtil.alert('연관상품의 조건이 다른 상품이 있습니다.');
            }
        }

        return checkSet;
    }
};