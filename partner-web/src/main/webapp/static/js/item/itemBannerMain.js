/**
 * 상품관리  > 상품공지 조회
 */
$(document).ready(function() {
    itemBannerListGrid.init();
    itemBanner.init();

    commonCategory.init();
    CommonAjaxBlockUI.global();
});

//상품공지 그리드
var itemBannerListGrid = {

    gridView : new RealGridJS.GridView("itemBannerListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {

        itemBannerListGrid.initGrid();
        itemBannerListGrid.initDataProvider();
        itemBannerListGrid.event();
    },
    initGrid : function() {
        itemBannerListGrid.gridView.setDataSource(itemBannerListGrid.dataProvider);

        itemBannerListGrid.gridView.setStyles(itemBannerListGridBaseInfo.realgrid.styles);
        itemBannerListGrid.gridView.setDisplayOptions(itemBannerListGridBaseInfo.realgrid.displayOptions);
        itemBannerListGrid.gridView.setColumns(itemBannerListGridBaseInfo.realgrid.columns);
        itemBannerListGrid.gridView.setOptions(itemBannerListGridBaseInfo.realgrid.options);

        itemGridCommon.setRealGridImgCellBtn(itemBannerListGrid.gridView, "/static/images/btn_edit.png", "editBtn");
    },

    initDataProvider : function() {
        itemBannerListGrid.dataProvider.setFields(itemBannerListGridBaseInfo.dataProvider.fields);
        itemBannerListGrid.dataProvider.setOptions(itemBannerListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        //이미지 버튼 클릭시 Sample
        itemBannerListGrid.gridView.onImageButtonClicked = function (grid, itemIndex, column, buttonIndex, name) {
            itemBanner.modifyBannerOpenTab(itemIndex);
        };
    },
    setData : function(dataList) {
        itemBannerListGrid.dataProvider.clearRows();
        itemBannerListGrid.dataProvider.setRows(dataList);
    },
};

//상품관리 > 상품공지 조회 
var itemBanner = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate("-30d", "schStartDt", "schEndDt");
        this.initCategorySelectBox();
    },
    /**
     * 날짜 초기화
     * @param flag
     * @param startId
     * @param endId
     */
    initSearchDate : function (flag, startId, endId) {

        itemBanner.setDate = Calendar.datePickerRange(startId, endId);
        itemBanner.setDate.setEndDate(0);
        itemBanner.setDate.setStartDate(flag);

        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {

        commonCategory.setCategorySelectBox(1, '', '', $('#schCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#schCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#schCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#schCateCd4'));
},
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#searchBtn').bindClick(itemBanner.search);
        $('#searchResetBtn').bindClick(itemBanner.resetSearchForm);

        $('input:radio[name="setSchDate"]').on('change', function() {
            var flag = $('input[name="setSchDate"]:checked').val();
            itemBanner.initSearchDate(flag, 'schStartDt' , 'schEndDt');
        });
    },
    /**
     * 신규생성버튼
     */
    setBannerOpenTab : function () {
        var param = "?bannerNo=";
        UIUtil.addTabWindow('/item/notice/itemBannerSetMain' + param,'Tab_SellerItemBannerNew', '상품공지등록', '140');
    },
    /**
     * 상품상세공지검색
     * @returns {boolean}
     */
    search : function() {

        if(!itemBanner.valid.search()){
            return false;
        }

        var form = $("#itemBannerSearchForm").serializeObject();

        CommonAjax.basic({
            url: '/item/notice/getItemBannerList.json',
            data: JSON.stringify(form),
            method: "POST",
            contentType: 'application/json',
            successMsg: null,
            callbackFunc: function (res) {
                itemBannerListGrid.setData(res);
                $('#itemBannerTotalCount').html($.jUtil.comma(itemBannerListGrid.gridView.getItemCount()));
            }
        });

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    modifyBannerOpenTab : function(index) {
        let rowId = itemBannerListGrid.gridView.getDataRow(index);
        let rowDataJson = itemBannerListGrid.dataProvider.getJsonRow(rowId);

        UIUtil.addTabWindow('/item/notice/itemBannerSetMain?bannerNo=' + rowDataJson.bannerNo,'Tab_itemBannerSet', '상품공지수정', '140');
    },
    /**
     * 검색폼 초기화
     */
    resetSearchForm : function() {

        itemBanner.initSearchDate("-30d", "schStartDt", "schEndDt");

        $('#schBannerNm').val('');
        $('#schCateCd1').val('').change();
        $('#schDispType').val('');
        $('#schDispYn').val('');
        $('#schDateType').val("REGDT")
        $('input:radio[name="setSchDate"]:input[value="-30d"]').trigger('click');
    },
};

/**
 * validation
 * @type {{search: itemBanner.valid.search, set: itemBanner.valid.set}
 */
itemBanner.valid = {

    search : function () {

        var schBannerNm = $('#schBannerNm').val();

        if( !$.jUtil.isEmpty(schBannerNm)) {
            if (schBannerNm.length < 2) {
                alert("검색 키워드는 2자 이상 입력해주세요.");
                $('#schBannerNm').focus();
                return false;
            }
        }

        // 날짜 체크
        var startDt = $("#schStartDt");
        var endDt = $("#schEndDt");

        if ($.jUtil.isEmpty(startDt.val()) || ($.jUtil.isEmpty(endDt.val()))) {
            alert("검색기간을 입력해주세요.")
            $("#schStartDt").focus();
            return false;
        }
        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            $("#schStartDt").focus();
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("최대 1년 범위까지만 조회 가능합니다. 조회기간을 다시 설정해 주세요. ");
            startDt.val(moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("검색기간의 시작일은 종료일보다 클 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        return true;

    },
};