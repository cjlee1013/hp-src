$(document).ready(function() {

    itemMain.init();
    itemListGrid.init();
    commonCategory.init();
    CommonAjaxBlockUI.global();

    itemMain.setItemBoardCnt();
    parentGrid =  itemListGrid;

});

var itemListGrid = {

    gridView : new RealGridJS.GridView("itemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        itemListGrid.initGrid();
        itemListGrid.initDataProvider();
        itemListGrid.event();

    },
    initGrid : function() {
        itemListGrid.gridView.setDataSource(itemListGrid.dataProvider);

        itemListGrid.gridView.setStyles(itemListGridBaseInfo.realgrid.styles);
        itemListGrid.gridView.setDisplayOptions(itemListGridBaseInfo.realgrid.displayOptions);
        itemListGrid.gridView.setColumns(itemListGridBaseInfo.realgrid.columns);
        itemListGrid.gridView.setOptions(itemListGridBaseInfo.realgrid.options);

        itemGridCommon.setRealGridImgCellBtn(itemListGrid.gridView, "/static/images/btn_edit.png", "editBtn")
    },
    initDataProvider : function() {
        itemListGrid.dataProvider.setFields(itemListGridBaseInfo.dataProvider.fields);
        itemListGrid.dataProvider.setOptions(itemListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        itemListGrid.gridView.onImageButtonClicked = function (grid, itemIndex, column, buttonIndex, name) {
            itemMain.modifyOpenTab(itemIndex);
        };

    },
    setData : function(dataList) {
        itemListGrid.dataProvider.clearRows();
        itemListGrid.dataProvider.setRows(dataList);
        itemListGrid.gridView.orderBy(['regDt'],[RealGridJS.SortDirection.DESCENDING]);
    },
    excelDownload: function() {
        if(itemListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        let _date = new Date();
        let fileName =  "상품관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        itemListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

/**
 * 상품관리 > 셀러/점포상품관리 > 상품 등록/수정
 */
var itemMain = {
    /**
     * 초기화
     */
    init : function() {

        this.event();
        this.initSearchDate('-30d');
        this.initCategorySelectBox();

        if($.jUtil.isNotEmpty(itemStatus)){
            itemMain.searchByItemStatusMain(itemStatus);
        }
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        itemMain.setDate = Calendar.datePickerRange('schStartDate', 'schEndDate');

        itemMain.setDate.setEndDate(0);
        itemMain.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate", $("#schStartDate").val());
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {

        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    modifyOpenTab : function(index) {

        let rowId = itemListGrid.gridView.getDataRow(index);
        let rowDataJson = itemListGrid.dataProvider.getJsonRow(rowId);

        UIUtil.addTabWindow('/item/itemSetMain?itemNo='+rowDataJson.itemNo,'Tab_itemModMain', '상품수정', '140');
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {

        $('#searchResetBtn').bindClick(itemMain.searchFormReset);
        $('#searchBtn').bindClick(itemMain.search);
        $('#excelDownloadBtn').bindClick(itemMain.excelDownload);
        $('#refreshItemCnt').bindClick(itemMain.setItemBoardCnt)

        $('input:radio[name="setSchDate"]').on('change', function() {
            itemMain.searchDate();
        });

        $('#itemStatusTotal').on('click', function() {
            if ($(this).is(':checked')) {
                $('.itemStatus').each(function () {
                    $('.itemStatus').prop('checked', false);
                });
            }
        });

        $('.itemStatus').on('click', function() {
            if ($(this).is(':checked')) {
                $('#itemStatusTotal').prop('checked', false);
            }
        });
    },
    searchDate : function () {
        var flag = $('input[name="setSchDate"]:checked').val();
        itemMain.initSearchDate(flag, 'schStartDate' , 'schEndDate');
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        itemListGrid.excelDownload();
    },
    /**
     * 상품 검색
     */
    search : function() {

        if(!itemMain.valid()) {
            return false;
        }

        $('#schKeyword').val($('#schKeyword').val().replace(/(?:\r\n|\r|\n)/g, ','));

        var schItemStatus = new Array();
        $("input[name=schItemStatus]:checked").each(function () {
                var status = $(this).val();
                schItemStatus.push(status)
            });

        var form = $("#itemSearchForm").serializeObject();
        form.schItemStatus = schItemStatus;

        CommonAjax.basic({
            url: "/item/getItemList.json"
            , data:JSON.stringify(form)
            , contentType: 'application/json'
            , method:"POST"
            , successMsg:null
            , callbackFunc:function(res) {
                itemListGrid.setData(res);
                $('#itemTotalCount').html($.jUtil.comma(itemListGrid.gridView.getItemCount()));
                $('#schStockQty').val('');
            }});
    },
    /**
     * ItemStatus Type 검색
     * @param type
     */
    searchByItemStatus : function (type) {

        itemMain.searchFormReset();

        //타입 선택 후 조회
        if(!$.jUtil.isEmpty(type)){
            var id = 'schItemStatus' + type;
            $('input:checkbox[id="itemStatusTotal"]').prop("checked", false);
            $('input:checkbox[id="' +id + '"]').prop("checked", true);
        }

        $('input:radio[name="setSchDate"]:input[value="-90d"]').trigger('click');

        itemMain.search();
    },
    /**
     * 메인에서 ItemStatus Type 검색
     * @param type
     */
    searchByItemStatusMain : function(type) {
        itemMain.searchFormReset();

        $('input:radio[name="setSchDate"]:input[value="-90d"]').trigger('click');
        $('#schDate').val('regDt');

        $('input:checkbox[id="itemStatusTotal"]').prop("checked", false);

        if(!$.jUtil.isEmpty(type)) {
            switch (type) {
                case "A" :
                case "E" :
                    var id = 'schItemStatus' + type;
                    $('input:checkbox[id="' + id + '"]').prop("checked", true);
                    break;

                case "L" :
                    $('input:checkbox[id="schItemStatusA"]').prop("checked", true);
                    $('#schStockQty').val('10');
                    break;
            }
            itemMain.search();
        }
    },
    /**
     * 팝업
     * @param type
     * @returns {boolean}
     */
    openItemApplyPopup : function (type) {

        var checkedRows = itemListGrid.gridView.getCheckedRows(false);
        if (checkedRows.length == 0) {
            alert("변경할 상품을 먼저 선택해주세요.")
            return false;
        }
        switch (type) {
            case "stop" :
                itemStopPop.openModal();
                break;

            case "active" :
                itemActivePop.openModal();
                break;

            case "dispYn" :
                itemDispYnPop.openModal();
                break;

            case "salePrice" :
                itemSalePricePop.openModal();
                break;

            case "saleDate" :
                itemSaleDatePop.openModal();
                break;

            case "dcPrice" :
                itemDcPricePop.openModal();
                break;

            case "shipPolicy" :
                itemShipPolicyPop.openModal();
                break;
        }
    },
    /**
     * 판매상태 별 상품수 요약
     */
    setItemBoardCnt : function () {

        CommonAjax.basic({url:'/item/getItemBoardCount.json?'
            , data:null
            , method:"GET"
            , successMsg:null
            , callbackFunc:function(resArr) {
                var total = 0;
                $.each(resArr, function (idx, val) {
                    if(!$.jUtil.isEmpty(val)) {
                        total += val.count;
                        $('#count'+val.itemStatus).text($.jUtil.comma(val.count));
                    }
                });
                //전체
                $('#total').text(total);
        }});
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $("#itemSearchForm").resetForm();
        itemMain.initSearchDate('-30d');
    },

    /**
     * 조회 validation
     */
    valid : function () {

        var schKeyword = $('#schKeyword').val();
        if(schKeyword != "") {
            if ($.jUtil.isNotAllowInput(schKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#schKeyword').focus();
                return false;
            }
        }

        var startDt = $("#schStartDate");
        var endDt = $("#schEndDate");

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("최대 1년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(
                moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD"));
            return false;
        }

        var schItemNm = $('#schItemNm').val();

        if(!$.jUtil.isEmpty(schItemNm)){
            if (schItemNm.length < 2) {
                alert("상품명은 2자 이상 입력해주세요.");
                $('#schBannerNm').focus();
                return false;
            }
        }
        return true;
    }
};

