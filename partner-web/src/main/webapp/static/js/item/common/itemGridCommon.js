/**
 * 공통 - 리얼그리드
 */
var itemGridCommon = {

    /**
     * 컬럼명 찾기
     * @param _gridView
     * @param _findText
     * @returns {string}
     */
    getRealGridColumnName : function (_gridView, _findText) {
        var columnName = "";
        $.each(_gridView.getColumns(), function (idx, data) {
            if(data.type == "group"){
                $.each(data.columns, function (_gIdx, _dData) {
                    if(_gridView.getColumnProperty(_dData, "header").text == _findText){
                        columnName = _dData.name;
                        return false;
                    }
                });
            }else{
                if(_gridView.getColumnProperty(data, "header").text == _findText){
                    columnName = data.name;
                    return false;
                }
            }
        });
        return columnName;
    },
    /**
     * 셀버튼-이미지
     * @param _gridView
     * @param_img
     * @param _columnName
     */
    setRealGridImgCellBtn : function (_gridView, _img, _columnName) {

        _gridView.setColumnProperty( _columnName, "renderer" ,{
            "type": "imageButtons",
            "editable": false,
            "images": [{
                "name": "팝업버튼",
                "up": _img,
                "hover": _img,
                "down":  _img,
                "width":40
            }],
            "alignment": "center"
        })
    },

}