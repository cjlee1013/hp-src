/**
 * 상품관리 > 셀러상품관리/점포상품관리 > 상품 등록/수정 > 그리드
 */
// 추가구성 그리드
let optAddListGrid = {
    selectedRowId : null,
    gridView : new RealGridJS.GridView("optAddListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        optAddListGrid.initGrid();
        optAddListGrid.initDataProvider();
        optAddListGrid.event();
    },
    initGrid : function() {
        optAddListGrid.gridView.setDataSource(optAddListGrid.dataProvider);

        optAddListGrid.gridView.setStyles(optAddListGridBaseInfo.realgrid.styles);
        optAddListGrid.gridView.setDisplayOptions(optAddListGridBaseInfo.realgrid.displayOptions);
        optAddListGrid.gridView.setColumns(optAddListGridBaseInfo.realgrid.columns);
        optAddListGrid.gridView.setOptions(optAddListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        optAddListGrid.dataProvider.setFields(optAddListGridBaseInfo.dataProvider.fields);
        optAddListGrid.dataProvider.setOptions(optAddListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        optAddListGrid.gridView.onDataCellClicked = function(gridView, index) {
            optAddListGrid.selectedRowId = index.dataRow;
            item.optAdd.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        optAddListGrid.dataProvider.clearRows();
        optAddListGrid.dataProvider.setRows(dataList);
    },
    // 추가구성 그리드 row 이동
    optAddGridMoveRow : function(obj) {
        let checkedRows = optAddListGrid.gridView.getCheckedRows(false);

        if (checkedRows.length == 0) {
            alert('순서변경할 옵션을 선택해 주세요.');
            return false;
        }

        realGridMoveRow(optAddListGrid, $(obj).attr('key'), checkedRows);
    },
    excelDownload: function() {
        if(optAddListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        let _date = new Date();
        let fileName =  "추가구성_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        optAddListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

let prListGrid = {
    selectedRowId : null,
    gridView : new RealGridJS.GridView("prListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        prListGrid.initGrid();
        prListGrid.initDataProvider();
        prListGrid.event();

        if (mallType == 'TD') {
            prListGrid.gridView.setColumnProperty(prListGrid.gridView.columnByName("storeCount"), "visible", true);
        }
    },
    initGrid : function() {
        prListGrid.gridView.setDataSource(prListGrid.dataProvider);

        prListGrid.gridView.setStyles(prListGridBaseInfo.realgrid.styles);
        prListGrid.gridView.setDisplayOptions(prListGridBaseInfo.realgrid.displayOptions);
        prListGrid.gridView.setColumns(prListGridBaseInfo.realgrid.columns);
        prListGrid.gridView.setOptions(prListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        prListGrid.dataProvider.setFields(prListGridBaseInfo.dataProvider.fields);
        prListGrid.dataProvider.setOptions(prListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        prListGrid.gridView.onDataCellClicked = function(gridView, index) {
            prListGrid.selectedRowId = index.dataRow;
            item.pr.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        prListGrid.dataProvider.clearRows();
        prListGrid.dataProvider.setRows(dataList);
    }
};
