/**
 * 상품관리  > 상품공지
 */
$(document).ready(function() {

    bannerDetailListGrid.init();
    itemBannerSet.init();

    commonCategory.init();
    CommonAjaxBlockUI.global();

    parentMain = itemBannerSet;

    //수정일 경우 공지 내용을 조회
    if('Y' == $('#isMod').val()) {
        itemBannerSet.getItemBannerDetail($('#bannerNo').val());
    }else{
        itemBannerSet.resetForm();
    }
});

//상품공지 그리드 
var bannerDetailListGrid = {

    gridView : new RealGridJS.GridView("bannerDetailListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {

        bannerDetailListGrid.initGrid();
        bannerDetailListGrid.initDataProvider();
        bannerDetailListGrid.event();

        $('#itemRow').hide();
        $('#cateRow').hide();

    },
    initGrid : function() {
        bannerDetailListGrid.gridView.setDataSource(bannerDetailListGrid.dataProvider);

        bannerDetailListGrid.gridView.setStyles(bannerDetailListGridBaseInfo.realgrid.styles);
        bannerDetailListGrid.gridView.setDisplayOptions(bannerDetailListGridBaseInfo.realgrid.displayOptions);
        bannerDetailListGrid.gridView.setColumns(bannerDetailListGridBaseInfo.realgrid.columns);
        bannerDetailListGrid.gridView.setOptions(bannerDetailListGridBaseInfo.realgrid.options);

        itemGridCommon.setRealGridImgCellBtn(bannerDetailListGrid.gridView, "/static/images/btn_delete.png", "delBtn")

    },
    initDataProvider : function() {
        bannerDetailListGrid.dataProvider.setFields(bannerDetailListGridBaseInfo.dataProvider.fields);
        bannerDetailListGrid.dataProvider.setOptions(bannerDetailListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        ///버튼 클릭 시
        bannerDetailListGrid.gridView.onImageButtonClicked = function (grid, itemIndex, column, buttonIndex, name) {
            itemBannerSet.deleteSelectedItem(itemIndex);
        };

    },
    setData : function(dataList) {
        bannerDetailListGrid.dataProvider.clearRows();
        bannerDetailListGrid.dataProvider.setRows(dataList);
    },
};

var itemBannerSet = {
    /**
     * 초기화
     */
    init: function () {
        this.bindingEvent();
        this.initDispDate("7d", "dispStartDt", "dispEndDt");
        this.initCategorySelectBox();

    },
    /**
     * 날짜 초기화
     * @param flag
     * @param startId
     * @param endId
     */
    initDispDate: function (flag, startId, endId) {


        itemBannerSet.setDateTime = CalendarTime.datePickerRange(startId, endId, {timeFormat:"HH:00:00"}, true, {timeFormat:"HH:59:59"}, true);

        itemBannerSet.setDateTime.setEndDateByTimeStamp(flag);
        itemBannerSet.setDateTime.setStartDate(0);

        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {

        commonCategory.setCategorySelectBox(1, '', '', $('#selectCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#selectCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#selectCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#selectCateCd4'));

    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent: function () {

        //저장
        $('#resetFormBtn').bindClick(itemBannerSet.resetFormConfirm);
        $('#setBannerBtn').bindClick(itemBannerSet.setSellerItemBanner, $('isMod').val());
        
        $('input:radio[name="dispType"]').on('click', function() {
            itemBannerSet.getDispTypeHandler($(this).val());
        })

        $('#bannerDesc').calcTextLength('keyup', '#textCountKr_keyword');

    },
    /**
     * 상품상세공지 등록/수정
     */
    setSellerItemBanner : function (isMod) {

        if (!itemBannerSet.valid.set(isMod)) {
            return false;
        }

        var form = $('#itemBannerSetForm').serializeObject();
        //상품번호
        form.itemList = bannerDetailListGrid.dataProvider.getJsonRows();

        CommonAjax.basic({
            url: '/item/notice/setItemBanner.json',
            data: JSON.stringify(form),
            type: 'post',
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function (res) {
                alert(res.returnMsg);
                $('#bannerNo').val(res.returnKey);
            }
        });
    },


    /**
     * 상품 삭제
     */
    deleteSelectedItem : function (index) {

        let row = bannerDetailListGrid.gridView.getDataRow(index);
        let rowDataJson = bannerDetailListGrid.dataProvider.getJsonRow(row);

        if (!$.jUtil.isEmpty(rowDataJson.bannerNo)) {
            bannerDetailListGrid.dataProvider.setValue(row , "useYn", "N");
        }
        bannerDetailListGrid.dataProvider.removeRow(row);
    },
    
    /**
     * 구분값에 따른 노출 영역 활성/비활성화
     * @param dispType
     */
    getDispTypeHandler : function (dispType) {

        switch (dispType) {
            case "ALL" :
                $('#cateRow').hide();
                $('#itemRow').hide();
                break;

            case "CATE" :
                $('#cateRow').show();
                $('#itemRow').hide();
                break;

            case "ITEM" :
                $('#cateRow').hide();
                $('#itemRow').show();
                bannerDetailListGrid.gridView.resetSize();
                break;

            default:
                $('#cateRow').hide();
                $('#itemRow').hide();
                break;
        }
    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    getItemBannerDetail : function (bannerNo) {

        if ($.jUtil.isEmpty(bannerNo)) {
            alert("잘못된 경로 입니다. ");
            return false;
        }

        //초기화
        itemBannerSet.resetForm();

        CommonAjax.basic({
            url: '/item/notice/getItemBannerDetail.json?',
            data: {bannerNo: bannerNo},
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {

                $('#bannerNm').val(res.bannerNm);
                $('#bannerDesc').val(res.bannerDesc);
                $('#bannerDesc').setTextLength('#textCountKr_keyword');

                $('#dispStartDt').val(res.dispStartDt);
                $('#dispEndDt').val(res.dispEndDt);

                $('input:radio[name="dispType"]:input[value="' + res.dispType + '"]').trigger('click'); //구분
                $('input:radio[name="dispYn"]:input[value="' + res.dispYn + '"]').trigger('click'); //구분

                if ("CATE" == res.dispType) {
                    //구분: 카테고리
                    itemBannerSet.getCategoryDetail(res.dcateCd);

                }
                else if ("ITEM" == res.dispType) {
                    bannerDetailListGrid.setData(res.itemList);
                }
            }
        });

    },
    /**
     * 상세카테고리 조회
     */
    getCategoryDetail: function (dcateCd) {

        if (!$.jUtil.isEmpty(dcateCd)) {
            CommonAjax.basic({
                url: '/common/category/getCategoryDetail.json?depth=4&cateCd='
                    + dcateCd,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    commonCategory.setCategorySelectBox(1, '', res.lcateCd,
                        $('#selectCateCd1'));
                    commonCategory.setCategorySelectBox(2, res.lcateCd,
                        res.mcateCd, $('#selectCateCd2'));
                    commonCategory.setCategorySelectBox(3, res.mcateCd,
                        res.scateCd, $('#selectCateCd3'));
                    commonCategory.setCategorySelectBox(4, res.scateCd,
                        res.dcateCd, $('#selectCateCd4'));
                }
            });
        }
    },
    /**
     * 상품상세 입력 폼 초기화
     */
    resetForm : function () {

        if('N' == $('#isMod').val()) {
            $('#bannerNo').val('');
        }

        $('#itemBannerSetForm').resetForm();
        $('#dispYn').val("Y");

        itemBannerSet.initDispDate("7d", "dispStartDt", "dispEndDt");
        $('#dispStartDt').val('');
        $('#dispEndDt').val('');

        $('#textCountKr_keyword').text(0);

        $('input:radio[name="dispType"]:input[value="SELLER"]').trigger('click');

        $('#selectCateCd1').val('').change();

        bannerDetailListGrid.dataProvider.clearRows();
    },

    resetFormConfirm : function () {

        if( !confirm("작성을 취소하시겠습니까? ")){
            return false;
        }
        if($('#isMod').val() == 'Y' ){
            UIUtil.RemoveTabWindow ('Tab_itemBannerSet' );
        }else {
            itemBannerSet.resetForm();
        }
    },
    /**
     * 상품 팝업 callback
     * @param res
     */
    callback : function(resDataArr) {
        itemPop.closeModal();

        var resultCount = resDataArr.length;

        if( resultCount > 0 ) {

            for (var i=0; i<resultCount; i++) {
                var data  = {

                    itemNo : resDataArr[i].itemNo,
                    itemNm : resDataArr[i].itemNm,
                    lcateNm : resDataArr[i].lcateNm,
                    mcateNm : resDataArr[i].mcateNm,
                    scateNm : resDataArr[i].scateNm,
                    dcateNm : resDataArr[i].dcateNm,
                    useYn : 'Y'
                };

                var isExist = bannerDetailListGrid.gridView.searchItem({fields: ["itemNo"], values: [resDataArr[i].itemNo], wrap: true});
                if (isExist < 1) {
                    bannerDetailListGrid.dataProvider.addRow(data);
                }
            }
        }
    }
};

itemBannerSet.valid = {

    set : function(isMod){

        //수정, 등록버튼 체크
        var bannerNm = $('#bannerNm').val();
        var bannerNo = $('#bannerNo').val();

        if(isMod == 'Y') {
            if ($.jUtil.isEmpty(bannerNo)){
                alert("잘못된 경로로 접근하였습니다. ");
                return false;
            }
        }

        //제목체크
        if (bannerNm != "") {
            if ($.jUtil.isNotAllowInput(bannerNm, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#bannerNm').focus();
                return false;
            }
        }else {
            alert("모든 항목은 빠짐없이 입력해주세요 ")
            $('#bannerNm').focus();
            return false;
        }

        // 날짜 체크
        var startDt = $("#dispStartDt");
        var endDt = $("#dispEndDt");

        if ($.jUtil.isEmpty(startDt.val()) || ($.jUtil.isEmpty(endDt.val())))  {
            alert("노출기간을 입력해주세요.")
            $('#dispStartDt').focus();
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("노출기간은 최대 1년 범위까지만 가능합니다.");
            startDt.val(moment(endDt.val()).add(-364, "day").format("YYYY-MM-DD 00:00:00"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("노출기간의 시작일은 종료일보다 클 수 없습니다.");
            startDt.val(moment(endDt.val()).format("YYYY-MM-DD 00:00:00"));
            return false;
        }

        if ($('input[name="dispType"]:checked').val() == 'CATE') {
            if ($.jUtil.isEmpty($('#selectCateCd4').val())) {
                $.jUtil.alert("카테고리를 확인해주세요" , 'selectCateCd4');
                return false;
            }
        }

        //공지사항 체크
        var bannerDesc = $('#bannerDesc').val();
        var dispYn = $('input:radio[name="dispYn"]:checked').val();
        if(!$.jUtil.isEmpty(bannerDesc)) {
            if (bannerDesc.length >1000) {
                alert("공지사항은 최대 1000자까지만 입력 가능합니다. ")
                $("#bannerDesc").focus();
                return false;
            }
        } else {
            if(dispYn == "Y") {
                alert("모든 항목은 빠짐없이 입력해주세요 ")
                $('#bannerDesc').focus();
                return false;
            }
        }

        return true;
    }

};