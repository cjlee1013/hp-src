var itemDispYnPop = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $('#setDispYnBtn').bindClick(itemDispYnPop.setItemDispYn);
    },
    /**
     * 초기화
     */
    resetForm : function() {
        $('#itemDispYnPopForm').resetForm();
        $('#itemDispYnCnt').html(0);
    },
    /**
     */
    setDispYnItemCnt : function (){
        var rowArr = parentGrid.gridView.getCheckedRows();
        $('#itemDispYnCnt').html($.jUtil.comma(rowArr.length));
    },
    /**
     * 저장
     */
    setItemDispYn : function() {

        var rowArr = parentGrid.gridView.getCheckedRows();
        var itemList = new Array();

        rowArr.forEach(function (_row) {
            var _data = parentGrid.dataProvider.getJsonRow(_row);
            itemList.push(_data.itemNo);
        });

        var form = $('#itemDispYnPopForm').serializeObject();
        form.itemNo = itemList;

        CommonAjax.basic({
            url         : '/item/pop/setItemDispYn.json',
            data        : JSON.stringify(form),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                itemDispYnPop.closeModal();
                resultPop.openModal(res);

                if(res.returnCnt > 0  ){
                    itemMain.search();
                }
            }
        });

    },
    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('itemDispYnPop', itemDispYnPop.setDispYnItemCnt);
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('itemDispYnPop', itemDispYnPop.resetForm);
    },

    /**
     * modal root selector
     */
    getRoot: function() {
        return $('#itemDispYn');
    }
}