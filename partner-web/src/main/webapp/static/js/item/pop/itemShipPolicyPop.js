/**
 * 상품관리 > 셀러/점포상품관리 > 상품 등록/수정
 */
var itemShipPolicyPop = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {

        itemCommon.ship.event();

        $('#setItemShipPolicyBtn').bindClick(itemShipPolicyPop.setItemShipPolicy);

        //배송정보
        $('#shipPolicyNo').on('change', function() {
            if ($(this).val() != '') {
                itemCommon.ship.getSellerShipDetail(true, $(this).val());
            }
        });
    },
    /**
     * 초기화
     */
    resetForm : function() {
        $('#itemShipPolicyPopForm').resetForm();
    },
    /**
     * 선택된 배송정책 수 노출
     */
    setItemShipPolicyCnt : function (){
        var rowArr = parentGrid.gridView.getCheckedRows();
        $('#itemShipPolicyCnt').html($.jUtil.comma(rowArr.length));

        itemCommon.ship.getSellerShipList(true);
    },
    /**
     * 배송정책 일괄 저장
     */
    setItemShipPolicy : function() {

        if(!itemShipPolicyPop.valid()) {
            return false;
        }

        var rowArr = parentGrid.gridView.getCheckedRows();
        var itemList = new Array();

        rowArr.forEach(function (_row) {
            var _data = parentGrid.dataProvider.getJsonRow(_row);
            itemList.push(_data.itemNo);
        });

        var form = $('#itemShipPolicyPopForm').serializeObject();
        form.itemNo = itemList;

        CommonAjax.basic({
            url         : '/item/pop/setItemShipPolicy.json',
            data        : JSON.stringify(form),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                itemShipPolicyPop.closeModal();
                resultPop.openModal(res);

                if(res.returnCnt > 0  ){
                    itemMain.search();
                }
            }
        });
    },
    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('itemShipPolicyPop', itemShipPolicyPop.setItemShipPolicyCnt);
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('itemShipPolicyPop', itemShipPolicyPop.resetForm);
    },

    /**
     * 유효성 체크
     * @returns {boolean}
     */
    valid : function () {

        var arr = [$('#returnAddr1') ,$('#returnZipcode') ,$('#releaseAddr1'), $('#releaseZipcode'), $('#shipPolicyNo')];

        if (! itemShipPolicyPop.checkIsEmptyArr(arr)) {
            return false;
        }

        //반품배송비
        if ($.jUtil.isEmpty($("#claimShipFee").val())) {
            alert("반품/교환배송비를 입력해주세요. (금액은 0 ~ 999,999 까지 입력 가능합니다.)");
            return false;
        } else {
            if (!$.jUtil.isAllowInput($("#claimShipFee").val(), ['NUM'])) {
                alert("반품/교환배송비는 숫자만 입력가능합니다. ");
                return false;
            }
        }

        return true;
    },

    /**
     * Null array 체크
     * @param arr
     * @returns {boolean}
     */
    checkIsEmptyArr : function (arr) {
        for(var i =0; i < arr.length; i++) {

            if(typeof arr[i] == 'object') {
                var value = $(arr[i]).val();
                if($.jUtil.isEmpty(value)) {
                    alert("필수 항목을 입력해주세요.");
                    $(arr[i]).focus();
                    return false;
                }
            }
        }

        return true;
    },

}