var itemSaleDatePop = {
    /**
     * 초기화
     */
    init : function() {
        itemCommon.util.initDateTime('10d', 'saleStartDt', 'saleEndDt', "HH:00:00", "HH:59:59");

        this.event();
        this.resetForm()
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {

        $('#setItemSaleDateBtn').bindClick(itemSaleDatePop.setItemSaleDate);
        
        $('input:radio[name="setSaleDate"]').on('change', function() {

            var flag = $('input[name="setSaleDate"]:checked').val();

            if(flag == 'N') {
                itemSaleDatePop.resetForm();
            }else{
                $('.saleDateArea').show();
                itemCommon.util.initDateTime(flag, 'saleStartDt', 'saleEndDt', "HH:00:00", "HH:59:59");
                $('#salePeriodYn').val('Y');
            }

        });

    },
    /**
     * 초기화
     */
    resetForm : function() {

        itemCommon.util.initDateTime('0d', 'saleStartDt', 'saleEndDt', "HH:00:00", "HH:59:59");
        $('#salePeriodYn').val('N');
        $('.saleDateArea').hide();

        $('input:radio[name="setSaleDate"]:input[value="N"]').trigger('click');
    },
    /**
     * 선택된 상품 수 노출
     */
    setItemSaleDateCnt : function (){

        var rowArr = parentGrid.gridView.getCheckedRows();
        $('#itemSaleDateCnt').html($.jUtil.comma(rowArr.length));
    },

    /**
     * 저장
     */
    setItemSaleDate : function() {

        if( !itemSaleDatePop.valid()) {
            return false;
        }
        var rowArr = parentGrid.gridView.getCheckedRows();
        var itemList = new Array();

        rowArr.forEach(function (_row) {
            var _data = parentGrid.dataProvider.getJsonRow(_row);
            itemList.push(_data.itemNo);
        });

        var form = $('#itemSaleDateForm').serializeObject();
        form.itemNo = itemList;

       CommonAjax.basic({
            url         : '/item/pop/setItemSaleDate.json',
            data        : JSON.stringify(form),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {

                itemSaleDatePop.closeModal();
                resultPop.openModal(res);

                if(res.returnCnt > 0  ){
                    itemMain.search();
                }
            }
        });
    },
    valid : function () {

        //판매기간 검증
        if ($('#salePeriodYn').val() == 'Y') {
            if ($.jUtil.isEmpty($('#saleStartDt').val()) || $.jUtil.isEmpty($('#saleStartDt').val())) {
                return $.jUtil.alert('판매기간을 입력해주세요.', 'saleStartDt');
            }

            if (moment($('#saleEndDt').val()).isBefore($('#saleStartDt').val()) == true) {
                return $.jUtil.alert('판매종료일은 시작일 이전으로 입력 할 수 없습니다.', 'saleEndDt');
            }
        }
        return true;
    },
    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('itemSaleDatePop', itemSaleDatePop.setItemSaleDateCnt);
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('itemSaleDatePop', itemSaleDatePop.resetForm);
    },


}