// 상품조회
var itemPop = {
    /**
     * 초기화
     */
    init : function() {
        itemPop.event();
        itemPop.itemPopFormReset();

    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $("#schBtn").bindClick(itemPop.searchItemList);
        $("#schResetBtn").bindClick(itemPop.itemPopFormReset);
        $("#selectBtn").bindClick(itemPop.checkGridRow);

        $("#itemNo, #itemNm").keyup(function (e) {
            if (e.keyCode == 13) {
                itemPop.searchItemList();
            }
        });
    },
    /**
     * 카테고리 초기화
     */
    initSearchCategory : function () {
        // 입력값이 따른 화면 컨트롤
        commonCategory.setCategorySelectBox(1, "", "", $("#schCateCd1"));
        commonCategory.setCategorySelectBox(2, "", "", $("#schCateCd2"),true);
        commonCategory.setCategorySelectBox(3, "", "", $("#schCateCd3"),true);
        commonCategory.setCategorySelectBox(4, "", "", $("#schCateCd4"),true);
    },
    /**
     * 상품조회 공통팝업 form 초기화
     */
    itemPopFormReset : function() {

        $("#itemPopForm").resetForm();

        itemPop.initSearchCategory();
        itemPopGrid.clear();
    },

    /**
     * 조회 조건 내용검증
     * @returns {boolean}
     */
    validCheck : function() {

        if (!$.jUtil.isEmpty($("#schItemNo").val())) {
            if ($("#schItemNo").val().trim().length < 14) {
                alert("상품번호를 확인해주세요.");
                return false;
            }
        } else {
            if($("#schCateCd2").val() == "") {
                alert("중분류까지 선택해 주세요.");
                return false;
            }

            if (!$.jUtil.isEmpty($("#schItemNm").val()) && $("#schItemNm").val().trim().length < 2) {
                alert("상품명은 2자 이상 입력해주세요.");
                return false;
            }
        }
        return true;
    },
    /**
     * 상품 조회
     */
    searchItemList : function () {
        // 조회 조건 validation check
        if (itemPop.validCheck()) {
            var itemPopForm = $("#itemPopForm").serializeObject();

            CommonAjax.basic({
                url: "/common/getItemPopList.json"
                , data: itemPopForm
                , method: "POST"
                , successMsg: null
                , callbackFunc: function (res) {
                    itemPopGrid.setData(res);
                    $("#itemPopSearchCnt").html(itemPopGrid.dataProvider.getRowCount());
                }
            });
        }
    },
    /**
     * 선택한 데이터 return
     * @returns {boolean}
     */
    checkGridRow : function (index) {

        if(isMulti == 'N') {
            itemPopGrid.gridView.checkItem(index, true);
        }

        var checkedRows = itemPopGrid.gridView.getCheckedRows(true);

        if(checkedRows.length == 0 ) {
            alert("상품을 선택해 주세요.");
            return false;
        }

        parentMain.callback(itemPopGrid.checkData());
        itemPop.closeModal();

    },

    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('itemPop', itemPop.itemPopFormReset);
        itemPopGrid.gridView.resetSize();
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('itemPop');
    },

};

// 상품조회 공통팝업 그리드
var itemPopGrid = {
    gridView : new RealGridJS.GridView("itemPopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        itemPopGrid.initItemPopGrid();
        itemPopGrid.initDataProvider();
        itemPopGrid.event();
    },
    /**
     * 조회 그리드 설정
     */
    initItemPopGrid : function() {
        itemPopGrid.gridView.setDataSource(itemPopGrid.dataProvider);
        itemPopGrid.gridView.setStyles(itemPopGridBaseInfo.realgrid.styles);
        itemPopGrid.gridView.setDisplayOptions(itemPopGridBaseInfo.realgrid.displayOptions);
        itemPopGrid.gridView.setColumns(itemPopGridBaseInfo.realgrid.columns);
        itemPopGrid.gridView.setOptions(itemPopGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        itemPopGrid.dataProvider.setFields(itemPopGridBaseInfo.dataProvider.fields);
        itemPopGrid.dataProvider.setOptions(itemPopGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        itemPopGrid.gridView.onDataCellClicked = function(gridView, index) {
            itemPop.checkGridRow(index.dataRow);
        };
    },
    clear : function () {
        itemPopGrid.dataProvider.clearRows();
        $("#itemPopSearchCnt").html(0);
    }
    ,
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        itemPopGrid.dataProvider.clearRows();
        itemPopGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드에서 선택된 데이터 return
     * @returns {any[]|boolean}
     */
    checkData : function () {
        var itemList = new Array();
        var checkedRows = itemPopGrid.gridView.getCheckedRows(true);
        checkedRows.forEach(function (_row) {
            var _data = this.dataProvider.getJsonRow(_row);
            var dataObj = new Object();

            dataObj.itemNo = _data.itemNo;
            dataObj.itemNm = _data.itemNm1;
            dataObj.lcateNm = _data.lcateNm;
            dataObj.mcateNm = _data.mcateNm;
            dataObj.scateNm = _data.scateNm;
            dataObj.dcateNm = _data.dcateNm;

            itemList.push(dataObj);
        }, this);

        return itemList;
    },

};
