var resultPop = {
    /**
     * 초기화
     */
    init : function() {
        this.reset();
    },
    /**
     * 초기화
     */
    reset : function() {
        $("#resultRowArea").html('');
        $("#applyItemCnt").text(0);
    },
    /**
     * 결과
     * @param res
     */
    viewResult : function(res) {

        $('#resultRowArea').show();

        if(res) {
            $("#applyItemCnt").text(res.returnCnt);
            let list = res.resultList;

            for(let i in list) {
                let innerHtml ='';

                innerHtml = '<tr>'
                + '<td>'+list[i].itemNo+'</td>'
                + '<td>'+list[i].itemNm+'</td>'
                + '<td>'+list[i].successYn+'</td>'
                + '</tr>';

                $('#resultRowArea').append(innerHtml);
            }
        }else{
            $('#resultRowArea').hide();
        }

    },
    openModal: function(res) {
        $ui.modalOpen('resultPop', resultPop.viewResult(res));
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('resultPop', resultPop.reset);
    },
}