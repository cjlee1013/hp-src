
var itemDcPricePop = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
        itemDcPricePop.initDate('0d', 'dcStartDt', 'dcEndDt', "HH:00:00", "HH:59:59");
        itemDcPricePop.resetForm();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $('#setItemDcPriceBtn').bindClick(itemDcPricePop.setItemDcPrice);

        $('#dcPeriodYn').on('click', function() {
            itemDcPricePop.changeDcPeriodYn($(this));
        });

        $('input:radio[name="dcYn"]').on('change', function() {
            $(this).val() == 'Y' ? $('.dcArea').show() : $('.dcArea').hide();
        });
    },
    /**
     * 일자 초기화
     * @param flag
     * @param startId
     * @param endId
     */
    initDate : function (flag, startId, endId, _timeFormat, _endTimeFormat) {
        itemDcPricePop.setDateTime = CalendarTime.datePickerRange(startId, endId, {timeFormat:_timeFormat}, true, {timeFormat:_endTimeFormat}, true);

        itemDcPricePop.setDateTime.setEndDateByTimeStamp(flag);
        itemDcPricePop.setDateTime.setStartDate(0);

        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
    /**
     * 초기화
     */
    resetForm : function() {

        $('#itemDcPricePopForm').resetForm();

        itemDcPricePop.changeDcPeriodYn('#dcPeriodYn');
        $('input:radio[name="dcYn"]:input[value="N"]').trigger('click');
        $('.dcArea').hide();
    },
    /**
     * 특정기간 할인 셋팅
     * @param _this
     */
    changeDcPeriodYn : function (_this) {
        if( $(_this).is(':checked') == true ){
            $('#dcStartDt, #dcEndDt').prop('disabled', false);
        }else{
            $('#dcStartDt, #dcEndDt').prop('disabled', true);
        }
    },
    /**
     * 선택된 상품 수 노출
     */
    setItemDcPriceCnt : function (){

        var rowArr = parentGrid.gridView.getCheckedRows();
        $('#itemDcPriceCnt').html($.jUtil.comma(rowArr.length));
    },
    /**
     * 할인가격 일괄 저장
     */
    setItemDcPrice : function() {

        if( !itemDcPricePop.valid() ) {
            return false;
        }

        var rowArr = parentGrid.gridView.getCheckedRows();
        var itemList = new Array();

        rowArr.forEach(function (_row) {
            var _data = parentGrid.dataProvider.getJsonRow(_row);
            itemList.push(_data.itemNo);
        });

        var form = $('#itemDcPricePopForm').serializeObject();
        form.itemNo = itemList;

        CommonAjax.basic({
            url         : '/item/pop/setItemDcPrice.json',
            data        : JSON.stringify(form),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                itemDcPricePop.closeModal();
                resultPop.openModal(res);

                if( res.returnCnt > 0 ) {
                    itemMain.search();
                }
            }
        });
    },
    /**
     * 유효성 체크
     * @returns {*|boolean|boolean}
     */
    valid : function() {

        //할인가격 특정기간 할인 날짜 검증
        if ($('input[name="dcYn"]:checked').val() == 'Y' ) {

            if (!$.jUtil.isAllowInput($('#dcPrice').val(), ['NUM'])) {
                return $.jUtil.alert('숫자만 입력 가능합니다. ', 'dcPrice');
            } else {

                if ($('#dcPrice').val() < 100) {
                    return $.jUtil.alert('할인가격으 100원 이상 입력 가능합니다. ', 'dcPrice');
                }
            }

            if ($('#dcPeriodYn').is(':checked') == true) {

                if ($.jUtil.isEmpty($('#dcStartDt').val()) || $.jUtil.isEmpty(
                    $('#dcEndDt').val())) {
                    return $.jUtil.alert('할인기간을 입력해주세요.', 'dcStartDt');
                }

                if (moment($('#dcEndDt').val()).isBefore(
                    $('#dcStartDt').val()) == true) {
                    return $.jUtil.alert('할인기간 종료일은 시작일 이전으로 입력 할 수 없습니다.','dcEndDt');
                }
            }
        }
        return true;
    },
    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('itemDcPricePop', itemDcPricePop.setItemDcPriceCnt);
    },
    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('itemDcPricePop', itemDcPricePop.resetForm);
    },
}