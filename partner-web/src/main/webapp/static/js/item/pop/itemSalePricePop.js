var itemSalePricePop = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $('#setItemSalePriceBtn').bindClick(itemSalePricePop.setItemDispYn);
    },
    /**
     * 초기화
     */
    resetForm : function() {
        $('#itemSalePricePopForm').resetForm();
    },
    /**
     * 선택된 상품 수 노출
     */
    setItemSalePriceCnt : function (){

        var rowArr = parentGrid.gridView.getCheckedRows();
        $('#itemSalePriceCnt').html($.jUtil.comma(rowArr.length));
    },
    /**
     * 노출여부 저장
     */
    setItemDispYn : function() {

        if( !itemSalePricePop.valid() ){
            return false;
        }

        var rowArr = parentGrid.gridView.getCheckedRows();
        var itemList = new Array();

        rowArr.forEach(function (_row) {
            var _data = parentGrid.dataProvider.getJsonRow(_row);
            itemList.push(_data.itemNo);
        });

        var form = $('#itemSalePricePopForm').serializeObject();
        form.itemNo = itemList;

        CommonAjax.basic({
            url         : '/item/pop/setItemSalePrice.json',
            data        : JSON.stringify(form),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {

                itemSalePricePop.closeModal();
                resultPop.openModal(res);

                if ( res.returnCnt > 0 ){
                    itemMain.search();
                }
            }
        });
    },
    /**
     * 유효성 체크
     * @returns {boolean}
     */
    valid : function () {

        if (!$.jUtil.isAllowInput( $('#salePrice').val(), ['NUM'])) {
            return $.jUtil.alert('숫자만 입력 가능합니다. ', 'salePrice');
        }else{
            if (Number($('#salePrice').val()) < 100) {
                return $.jUtil.alert("판매가격은 100원 이상 등록 가능합니다.", 'salePrice');
            }
        }

        return true;
    },
    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('itemSalePricePop', itemSalePricePop.setItemSalePriceCnt);
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('itemSalePricePop', itemSalePricePop.resetForm);
    },
}