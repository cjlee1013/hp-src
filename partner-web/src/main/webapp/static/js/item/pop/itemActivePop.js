var itemActivePop = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $('#setItemActiveBtn').bindClick(itemActivePop.setItemStop);
    },
    /**
     * 초기화
     */
    resetForm : function() {
        $('#itemActivePopForm').resetForm();
        $('#itemStatusActiveCnt').html(0);
    },
    /**
     * 선택된 상품 수 노출
     */
    setItemStatusActiveCnt : function (){
        var rowArr = parentGrid.gridView.getCheckedRows();
        $('#itemStatusActiveCnt').html($.jUtil.comma(rowArr.length));
    },
    /**
     * 저장
     */
    setItemStop : function() {

        var rowArr = parentGrid.gridView.getCheckedRows();
        var itemList = new Array();

        rowArr.forEach(function (_row) {
            var _data = parentGrid.dataProvider.getJsonRow(_row);
            itemList.push(_data.itemNo);
        });

        var form = $('#itemActivePopForm').serializeObject();

        form.itemNo = itemList;

        CommonAjax.basic({
            url         : '/item/pop/setItemActive.json',
            data        : JSON.stringify(form),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {

                itemActivePop.closeModal();
                resultPop.openModal(res);

                if(res.returnCnt > 0  ){
                    itemMain.search();
                }
            }
        });

    },
    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('itemActivePop', itemActivePop.setItemStatusActiveCnt);
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('itemActivePop', itemActivePop.resetForm);
    },
}