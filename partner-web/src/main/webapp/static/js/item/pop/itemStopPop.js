/**
 * 상품관리 > 셀러/점포상품관리 > 상품 등록/수정
 */
var itemStopPop = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $('#setItemStopBtn').bindClick(itemStopPop.setItemStop);
    },
    /**
     * 초기화
     */
    resetForm : function() {
        $('#stopReason').val('');
        $('#itemStatusStopCnt').html(0);
    },
    /**
     * 상품중지
     */
    setItemStatusStopCnt : function (){

        var rowArr = parentGrid.gridView.getCheckedRows();
        $('#itemStatusStopCnt').html($.jUtil.comma(rowArr.length));
    },
    /**
     * 저장
     */
    setItemStop : function() {

        if ($.jUtil.isEmpty($('#stopReason').val())) {
            return $.jUtil.alert('사유를 입력해주세요.', 'stopReason');
        }

        var rowArr = parentGrid.gridView.getCheckedRows();
        var itemList = new Array();

        rowArr.forEach(function (_row) {
            var _data = parentGrid.dataProvider.getJsonRow(_row);
            itemList.push(_data.itemNo);
        });

        var form = $('#itemStopPopForm').serializeObject();

        form.itemNo = itemList;

        CommonAjax.basic({
            url         : '/item/pop/setItemStop.json',
            data        : JSON.stringify(form),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                itemStopPop.closeModal();
                resultPop.openModal(res);

                if(res.returnCnt > 0  ) {
                    itemMain.search();
                }
            }
        });

    },
    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('itemStopPop', itemStopPop.setItemStatusStopCnt);
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('itemStopPop', itemStopPop.resetForm);
    },
}