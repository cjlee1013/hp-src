$(document).ready(function() {

    categoryCommissionListGrid.init();
    categoryCommissionPop.init();
    CommonAjaxBlockUI.global();
});

//그리드
var categoryCommissionListGrid = {
    gridView : new RealGridJS.GridView("categoryCommissionListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        categoryCommissionListGrid.initGrid();
        categoryCommissionListGrid.initDataProvider();
    },
    initGrid : function () {
        categoryCommissionListGrid.gridView.setDataSource(categoryCommissionListGrid.dataProvider);

        categoryCommissionListGrid.gridView.setStyles(categoryCommissionListGridBaseInfo.realgrid.styles);
        categoryCommissionListGrid.gridView.setDisplayOptions(categoryCommissionListGridBaseInfo.realgrid.displayOptions);
        categoryCommissionListGrid.gridView.setColumns(categoryCommissionListGridBaseInfo.realgrid.columns);
        categoryCommissionListGrid.gridView.setOptions(categoryCommissionListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function () {
        categoryCommissionListGrid.dataProvider.setFields(categoryCommissionListGridBaseInfo.dataProvider.fields);
        categoryCommissionListGrid.dataProvider.setOptions(categoryCommissionListGridBaseInfo.dataProvider.options);
    },
    setData : function (dataList) {
        categoryCommissionListGrid.dataProvider.clearRows();
        categoryCommissionListGrid.dataProvider.setRows(dataList);
    }
};

var categoryCommissionPop = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
        this.resetForm();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {

        var $main = categoryCommissionPop.getRoot();

        $main.find('#resetShipPolicyMngBtn').bindClick(shipPolicyMngPop.resetForm);
        $main.find("#schBtn").bindClick(categoryCommissionPop.search);

        $main.find("#schCategoryNm").keydown(function(key) {
            if (key.keyCode == 13) {
                categoryCommissionPop.getCategoryNm();
            }
        });

        $main.find("#schCategoryNm").keyup(function() {
            if($.jUtil.isEmpty($main.find('#searchCategoryNm').val())) {
                $main.find('#schCateLayer').html('');
            }
        });

    },

    /**
     * modal root selector
     */
    getRoot : function() {
        return $('#categoryCommissionPop');
    },
    /**
     * 초기화
     */
    resetForm : function() {

        var $main = categoryCommissionPop.getRoot();

        $('#categoryCommissionPopForm').resetForm();
        categoryCommissionPop.initSearchCategory();
        $main.find('#schCateLayer').html('');
    },
    /**
     * 카테고리 초기화
     */
    initSearchCategory : function () {
        // 입력값이 따른 화면 컨트롤
        commonCategory.setCategorySelectBox(1, "", "", $("#schCateCd1"));
        commonCategory.setCategorySelectBox(2, "", "", $("#schCateCd2"),true);
        commonCategory.setCategorySelectBox(3, "", "", $("#schCateCd3"),true);
        commonCategory.setCategorySelectBox(4, "", "", $("#schCateCd4"),true);
    },
    /**
     * 카테고리 이름 조회
     */
    getCategoryNm : function () {
        var $main = categoryCommissionPop.getRoot();

        let cateNm = $main.find('#schCategoryNm').val();
        if ($.jUtil.isEmpty(cateNm) || cateNm.length < 2) {
            return $.jUtil.alert("카테고리를 입력해주세요.(2자이상)", 'schCategoryNm');
        }

        CommonAjax.basic({
            url: '/common/category/getCategoryNm.json',
            data: {cateSchNm: cateNm},
            method: 'GET',
            callbackFunc: function (res) {
                var resCnt = res.length;

                if(resCnt == 0) {
                    $main.find('#schCateLayer').html('');
                    $main.find('#schCateLayer').append('<li class="no-result">검색결과가 없습니다.</li>');
                } else {
                    $main.find('#schCateLayer').html('');

                    for (let i = 0; resCnt > i; i++) {

                        let id = res[i].lcateCd + '_' + res[i].mcateCd + '_' + res[i].scateCd + '_' + res[i].dcateCd;
                        let _name = res[i].lcateNm + ' > ' + res[i].mcateNm + ' > ' + res[i].scateNm + ' > ' + res[i].dcateNm;
                        let searchVal = itemCommon.util.replaceAll(_name, cateNm, '<i>'+cateNm+'</i>' );

                        $main.find('#schCateLayer').append('<li id="schCateLayer_'+id+'"><button type="button">'+searchVal+'</button></li>');
                    }
                    categoryCommissionPop.setCategory();
                }
                $main.find('.schCateLayer').show();
            }
        });
    },
    setCategory : function () {
        var $main = categoryCommissionPop.getRoot();
        $main.find('li[id^=schCateLayer_]').click(function() {

            let idArr = $(this).attr('id').split('_');
            let depth = idArr.length -1 ;

            switch (depth) {
                case 1 :
                    commonCategory.setCategorySelectBox(1, '', idArr[1], $main.find('#schCateCd1'));
                    break;
                case 2 :
                    commonCategory.setCategorySelectBox(1, '', idArr[1], $main.find('#schCateCd1'));
                    commonCategory.setCategorySelectBox(2, idArr[1], idArr[2], $main.find('#schCateCd2'));
                    break;
                case 3 :
                    commonCategory.setCategorySelectBox(1, '', idArr[1], $main.find('#schCateCd1'));
                    commonCategory.setCategorySelectBox(2, idArr[1], idArr[2], $main.find('#schCateCd2'));
                    commonCategory.setCategorySelectBox(3, idArr[2], idArr[3], $main.find('#schCateCd3'));
                    break;
                case 4 :
                    commonCategory.setCategorySelectBox(1, '', idArr[1], $main.find('#schCateCd1'), function () {
                        commonCategory.setCategorySelectBox(2, idArr[1], idArr[2], $main.find('#schCateCd2'), function () {
                            commonCategory.setCategorySelectBox(3, idArr[2], idArr[3], $main.find('#schCateCd3'), function () {
                                commonCategory.setCategorySelectBox(4,idArr[3], idArr[4], $main.find('#schCateCd4'), function () {
                                });
                            });
                        });
                    });
                    break;
            }
        });
    },
    /**
     * 조회
     */
    search : function () {

        if($("#schCateCd1").val() == "") {
            alert("수수료 확인이 필요한 카테고리를 선택해주세요.");
            return false;
        }

        let form = $("#categoryCommissionPopForm").serialize();

        CommonAjax.basic({
            url: "/common/getCategoryCommissionList.json"
            , data: form
            , method: "GET"
            , successMsg: null
            , callbackFunc: function (res) {
                categoryCommissionListGrid.setData(res);
            }
        });

    },
    /**
     * modal open
     */
    openModal: function() {
        $ui.modalOpen('categoryCommissionPop', categoryCommissionPop.resetForm);
        categoryCommissionListGrid.gridView.resetSize();
    },

    /**
     * modal close
     */
    closeModal: function() {
        $ui.modalClose('categoryCommissionPop', categoryCommissionPop.resetForm);
    },
}