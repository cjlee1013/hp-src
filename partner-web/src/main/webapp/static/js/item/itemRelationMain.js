$(document).ready(function() {

    itemRelationListGrid.init();
    itemRelation.init();
    commonCategory.init();
    CommonAjaxBlockUI.global();

});

var itemRelationListGrid = {
    gridView : new RealGridJS.GridView("itemRelationListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        itemRelationListGrid.initGrid();
        itemRelationListGrid.initDataProvider();
        itemRelationListGrid.event();
    },
    initGrid : function () {
        itemRelationListGrid.gridView.setDataSource(itemRelationListGrid.dataProvider);

        itemRelationListGrid.gridView.setStyles(itemRelationListGridBaseInfo.realgrid.styles);
        itemRelationListGrid.gridView.setDisplayOptions(itemRelationListGridBaseInfo.realgrid.displayOptions);
        itemRelationListGrid.gridView.setColumns(itemRelationListGridBaseInfo.realgrid.columns);
        itemRelationListGrid.gridView.setOptions(itemRelationListGridBaseInfo.realgrid.options);
        //링크적용
        itemRelationListGrid.gridView.setColumnProperty("relationNo", "renderer", {type: "link", url:"/item/itemRelationSetMain", showUrl: false});
        itemRelationListGrid.gridView.setColumnProperty("relationNm", "renderer", {type: "link", url:"/item/itemRelationSetMain", showUrl: false});
    },
    initDataProvider : function() {
        itemRelationListGrid.dataProvider.setFields(itemRelationListGridBaseInfo.dataProvider.fields);
        itemRelationListGrid.dataProvider.setOptions(itemRelationListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        //링크 클릭 시
        itemRelationListGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var rowDataJson = itemRelationListGrid.dataProvider.getJsonRow(index.dataRow);
            var param = "?relationNo="+rowDataJson.relationNo;

            UIUtil.addTabWindow(url + param,'Tab_itemRelationMod', '연관상품수정');
        };
    },
    setData : function(dataList) {
        itemRelationListGrid.dataProvider.clearRows();
        itemRelationListGrid.dataProvider.setRows(dataList);
        itemRelationListGrid.gridView.orderBy(['regDt'],[RealGridJS.SortDirection.DESCENDING]);
    },
};


/**
 * 상품관리 > 셀러/점포상품관리 > 상품 등록/수정
 */
var itemRelation = {
    /**
     * 초기화
     */
    init : function() {

        this.event();
        this.initSearchDate('-30d');
        this.initCategorySelectBox();

    },

    /**
     * 이벤트 바인딩
     */
    event : function() {
        $('#searchResetBtn').bindClick(itemRelation.resetSearchForm);
        $('#searchBtn').bindClick(itemRelation.search);
        $('#delItemRelationBtn').bindClick(itemRelation.delItemRelation);

        $('input:radio[name="setSchDate"]').on('change', function() {
            var flag = $('input[name="setSchDate"]:checked').val();
            itemRelation.initSearchDate(flag, 'schStartDt' , 'schEndDt');
        });

        $('#dispYnTotal').on('click', function() {
            if ($(this).is(':checked')) {
                $('.dispYn').each(function () {
                    $('.dispYn').prop('checked', false);
                });
            }
        });

        $('.dispYn').on('click', function() {

            if ($(this).is(':checked')) {
                $('input:checkbox[name=schDispYn]:checked').prop('checked' , false);
                $(this).prop('checked', true);
                $('#dispYnTotal').prop('checked', false);
            }
        });
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        itemRelation.setDate = Calendar.datePickerRange('schStartDate', 'schEndDate');

        itemRelation.setDate.setEndDate(0);
        itemRelation.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate", $("#schStartDate").val());
    },
    /**
     * 신규생성버튼
     */
    setItemRelationOpenTab : function () {
        var param = "?relationNo=&isMod=N";
        UIUtil.addTabWindow('/item/itemRelationSetMain' + param,'Tab_ItemRelationSet', '연관상품등록', '140');
    },

    /**
     * 검색 영역 초기화
     */
    resetSearchForm : function () {
        $('#itemRelationSearchForm select').each( function() {
            $(this).val( $(this).find("option[selected]").val() );
        });

        $("#itemRelationSearchForm").resetForm();

        itemRelation.initSearchDate('-30d');
    },
    /**
     * 연관 상품관리 조회
     * @returns {boolean}
     */
    search : function () {

        $('#schRelationNo').val($('#schRelationNo').val().replace(/(?:\r\n|\r|\n)/g, ','));
        var data = $('#itemRelationSearchForm').serializeObject();

        if(itemRelation.valid.search()){
            CommonAjax.basic({
                url: '/item/getItemRelationList.json',
                data: JSON.stringify(data),
                method: "POST",
                contentType : "application/json",
                successMsg: null,
                callbackFunc: function (res) {
                    itemRelationListGrid.setData(res);
                    $('#itemRelationTotalCount').html($.jUtil.comma(itemRelationListGrid.gridView.getItemCount()));
                }
            });
        }
    },
    delItemRelation : function () {

        let rowArr = itemRelationListGrid.gridView.getCheckedRows();

        if (rowArr.length == 0) {
            alert('삭제하려는 연관번호를 선택해주세요.');
            return false;
        }

        if (confirm('삭제 시 연관으로 등록된 상품들의 연관이 해지됩니다.')) {

            var relationNoList = new Array();

            rowArr.forEach(function (_row) {
                var _data = itemRelationListGrid.dataProvider.getJsonRow(_row);
                relationNoList.push(_data.relationNo);
            });

            var form = $('#delItemRelationForm').serializeObject();

            form.relationNo = relationNoList;

            CommonAjax.basic({
                url         : '/item/delItemRelation.json',
                data        : JSON.stringify(form),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    alert(res.returnMsg);
                    if(res.returnCnt > 0  ){
                        itemRelation.search();
                    }

                }
            });

        }
    },

};

itemRelation.valid = {

    search: function () {
        $('#schRelationNo').val(
            $('#schRelationNo').val().replace(/(?:\r\n|\r|\n)/g, ','));

        let startDt = $("#schStartDate");
        let endDt = $("#schEndDate");
        let schRelationNo = $('#schRelationNo').val();

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("최대 1년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(
                moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD"));
            return false;
        }

        if (schRelationNo != "") {

            var pattern = /[^0-9,]/g;
            if (pattern.test(schRelationNo)) {
                alert('숫자 또는 ","를 입력하세요');
                $('#schRelationNo').focus();
                return false;
            }
            if ( !$.jUtil.isEmpty($('#schRelationNm').val()) && $('#schRelationNm').val().length < 2) {
                return $.jUtil.alert('검색 키워드는 2자 이상 입력해주세요.', 'schRelationNm');
            }
        }

        return true;

    },
}


