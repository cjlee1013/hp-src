var itemCommon ={};

itemCommon.img = {

    imgDiv : null,
    beforeMainId : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        $('.uploadType').each(function(){ itemCommon.img.setImg($(this))});
        itemCommon.img.imgDiv = null;
        itemCommon.img.beforeMainId = null;
    },
    event : function() {
        $('.uploadType').bindClick(itemCommon.img.imagePreview);

        $('#proofFileTable').on('click', '.proofDownLoad', function() {
            $.jUtil.downloadFile($(this).data('url'), $(this).data('file-nm'), $(this).data('process-key'));
        });
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params, isDelete) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            if (!obj.find('.imgNo').val()) {
                obj.find('.imgNo').val(params.imgNo);
            }
            obj.find('.imgNm').val(params.imgNm);
            obj.find('.changeYn').val(params.changeYn);
            obj.find('.imgUrlTag').prop('src', params.src);

            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {
            obj.find('.mainYn').val('N');
            obj.find('.imgUrl').val('');
            if (!isDelete) {
                obj.find('.imgNo').val('');
            }
            obj.find('.imgNm').val('');
            obj.find('.changeYn').val('N');
            obj.find('.imgUrlTag').prop('src', '');

            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function(id, fieldName, size, sizeMsg) {
        if (!size) {
            size = 4194304;
            sizeMsg = '4MB';
        }

        // if ($('input[name="'+fieldName+'"]').get(0).files[0].size > size) {
        //     return $.jUtil.alert("용량을 초과하였습니다.\n" + sizeMsg + " 이하로 업로드해주세요.");

        if ($('input[name="'+fieldName+'"]').get(0).files[0].name.length > 45) {
             return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('input[name="'+fieldName+'"]');
        let params = {
            fieldName : fieldName,
            processKey : 'ItemMainImage',
            mode : 'IMG'
        };

        if (itemCommon.img.imgDiv.data('type') == 'LST') {
            params.processKey = 'ItemListImage';
        } else if(itemCommon.img.imgDiv.data('type') == 'INTRO') {
            params.processKey = 'ItemRelation';
        } else if(itemCommon.img.imgDiv.data('type') == 'LBL') {
            params.processKey = 'ItemLabelImage';
        }

        // let ext = $('input[name="'+fieldName+'"]').get(0).files[0].name.split('.');

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        itemCommon.img.setImg($('#'+id), {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'src'   : hmpImgUrl + f.fileStoreInfo.fileId,
                            'ext'   : "",
                            'changeYn' : 'Y'
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 미리보기 팝업
     * @param thisParam
     */
    imagePreview : function(obj) {

        var imgUrl = $(obj).find('img').prop('src');

        if(imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(obj) {
        if (confirm('삭제하시겠습니까?')) {
            itemCommon.img.setImg(obj, null, true);
        }

    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(obj) {
        itemCommon.uploadType = 'IMG';
        itemCommon.img.imgDiv = obj.parents('.preview').find('.uploadType');
        $('input[name="'+obj.data('field-name')+'"]').click();
    }
};

itemCommon.file = {
    fileObj : null,
    /**
     * 파일 업로드 클릭 이벤트
     * @param obj
     */
    clickFile : function(type) {

        var proofFile = $('#proofFile').val();

        if ($.jUtil.isEmpty(proofFile)){
            return $.jUtil.alert('첨부할 증명서류를 선택해주세요.', 'proofFile');
        }

        itemCommon.file.fileObj = $("#proofFile option:selected");

        $('input[name="'+$("#proofFile option:selected").data('field-name')+'"]').click();
    },
    /**
     * 파일업로드
     */
    addFile : function(id, fieldName) {

        var file = $('input[name="'+fieldName+'"]');
        var params = {
            processKey : "ItemFile",
            fieldName : fieldName,
            mode : "FILE"
        };

        if ($('input[name="'+fieldName+'"]').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i].fileStoreInfo;

                    if (f.hasOwnProperty("errors")) {
                        errorMsg += f.errors[0].detail + "\n";
                    } else {
                        itemCommon.file.appendProofFile(f, id);
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    appendProofFile : function(f, id) {
        if (id) { //등록
            let html = '<span class="add-file proofFile">'
                    + '<input type="hidden" class="fileSeq" name="proofFileList[].fileSeq" value=""/>'
                    + '<input type="hidden" class="fileType" name="proofFileList[].fileType" value="'+itemCommon.file.fileObj.data('file-type')+'"/>'
                    + '<input type="hidden" class="fileUrl" name="proofFileList[].fileUrl" value="'+f.fileId+'"/>'
                    + '<input type="hidden" class="fileNm" name="proofFileList[].fileNm" value="'+f.fileName+'"/>'
                    + '<span class="proofDownLoad" data-url="'+ f.fileId + '" '
                    + 'data-process-key="ItemFile" '
                    + 'data-file-nm="' + f.fileName + '" '
                    + '" target="_blank" class="file-link" style="text-decoration: underline; cursor: pointer;">'
                    + f.fileName
                    + '</span><button type="button" class="btn-close close"><i class="hide">첨부파일삭제</i></button></span>';

            $('#'+id).append(html);
        } else { //조회
            let html = '<span class="add-file proofFile">'
                    + '<input type="hidden" class="fileSeq" name="proofFileList[].fileSeq" value="'+f.fileSeq+'"/>'
                    + '<input type="hidden" class="fileType" name="proofFileList[].fileType" value="'+f.fileType+'"/>'
                    + '<input type="hidden" class="fileUrl" name="proofFileList[].fileUrl" value="'+f.fileUrl+'"/>'
                    + '<input type="hidden" class="fileNm" name="proofFileList[].fileNm" value="'+f.fileNm+'"/>'
                    + '<span class="proofDownLoad" data-url="'+ f.fileUrl + '" '
                    + 'data-process-key="ItemFile" '
                    + 'data-file-nm="' + f.fileNm + '" '
                    + '" target="_blank" class="file-link" style="text-decoration: underline; cursor: pointer;">'
                    + f.fileNm
                    + '</span><button type="button" class="btn-close close"><i class="hide">첨부파일삭제</i></button></span>';

            $('#uploadProofFileSpan_'+f.fileType).append(html);
        }
    }
};

itemCommon.util = {
    /**
     * set disabled
     * @param id
     * @param isBool
     */
    setDisabledAll : function(id, isBool) {
        $('#' + id).find('input,select,textarea,button').prop('disabled', isBool);
    },
    /**
     * select box 초기화 (공통)
     * @param obj
     * @param isInit
     * @param initMsg
     */
    initSelectBoxOptions : function(obj, isInit, initMsg) {
        let initOption = '';

        if (isInit) {
            initOption = '<option value="">' + initMsg + '</option>';
        }

        $(obj).find('option').remove().end().append(initOption).val();
    },
    /**
     * Object key 리턴
     * @param key
     * @returns {*}
     */
    getKey : function(key) {
        return key;
    },
    /**
     * 일자 초기화
     * @param flag
     * @param startId
     * @param endId
     */
    initDate : function (flag, startId, endId) {
        itemCommon.setDate = Calendar.datePickerRange(startId, endId);

        if ($.jUtil.isEmpty(flag)) {
            itemCommon.setDate.setEndDate(flag);
        } else {
            itemCommon.setDate.setEndDate(0);
        }

        itemCommon.setDate.setStartDate(flag);

        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
    /**
     * 일자시간 초기화
     * @param flag
     * @param startId
     * @param endId
     */
    initDateTime : function (flag, startId, endId, _timeFormat, _endTimeFormat) {
        itemCommon.setDateTime = CalendarTime.datePickerRange(startId, endId, {timeFormat:_timeFormat}, true, {timeFormat:_endTimeFormat}, true);

        itemCommon.setDateTime.setEndDateByTimeStamp(flag);
        itemCommon.setDateTime.setStartDate(0);
    },
    /**
     * set disabled
     * @param className
     * @param isBool
     */
    setClassDisabledAll : function(className, isBool) {
        $('.' + className).find('input,select,textarea,button').prop('disabled', isBool);
    },
    /**
     * set disabled
     * @param id
     * @param isBool
     */
    setDisabledAll : function(id, isBool) {
        $('#' + id).find('input,select,textarea,button').prop('disabled', isBool);
    },
    /**
     * select box 초기화 (공통)
     * @param obj
     * @param isInit
     * @param initMsg
     */
    initSelectBoxOptions : function(obj, isInit, initMsg) {
        let initOption = '';

        if (isInit) {
            initOption = '<option value="">' + initMsg + '</option>';
        }

        $(obj).find('option').remove().end().append(initOption).val();
    },
    /**
     * Object key 리턴
     * @param key
     * @returns {*}
     */
    getKey : function(key) {
        return key;
    },
    /**
     * 일자 초기화
     * @param flag
     * @param startId
     * @param endId
     */
    initDate : function (flag, startId, endId) {
        itemCommon.setDate = Calendar.datePickerRange(startId, endId);

        if ($.jUtil.isEmpty(flag)) {
            itemCommon.setDate.setEndDate(flag);
        } else {
            itemCommon.setDate.setEndDate(0);
        }

        itemCommon.setDate.setStartDate(flag);

        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
    /**
     * set disabled
     * @param className
     * @param isBool
     */
    setClassDisabledAll : function(className, isBool) {
        $('.' + className).find('input,select,textarea,button').prop('disabled', isBool);
    },
    /**
     * replaceAll
     * @param str
     * @param pattern
     * @param newStr
     * @returns {string}
     */
    replaceAll : function (str, pattern, newStr) {
        return str.split(pattern).join(newStr);
    }

};

itemCommon.ship = {
    event : function() {

        //사업자 주소 동일 적용
        $('#copyPartnerAddr').bindClick(itemCommon.ship.copyPartnerAddr);
        //출고지 주소 동일 적용
        $('#copyReleaseAddr').bindClick(itemCommon.ship.copyReleaseAddr);

        //도로명 주소2 입력
        $('#releaseRoadAddr2, #returnRoadAddr2').on('propertychange change keyup paste input', function() {
            let _td = $(this).closest('td');

            _td.find('span.roadAddr2').text($(this).val());
            _td.find('input.roadAddr2').val($(this).val());
        });

        //출고기한 변경 이벤트
        $('input:radio[name="shipReleaseDay"]').on('change', function(){
            itemCommon.ship.getShipReleaseDay($(this).val());
        });

        $('#releaseDayTemp').on('change', function(){
            $('#releaseDay').val($(this).val());
        });

    },
    copyPartnerAddr : function() {
        if ($("#copyPartnerAddr").prop('checked') == true) {

            CommonAjax.basic({
                url:'/partner/getSellerAddress.json',
                method:'get',
                callbackFunc:function(resData) {
                    $("#releaseZipcode").val(resData.zipCode);
                    $("#releaseAddr1").val(resData.addr1);
                    $("#releaseAddr2").val(resData.addr2);

                    if ($("#copyReleaseAddr").prop('checked') == true) {
                        $("#returnZipcode").val($("#releaseZipcode").val());
                        $("#returnAddr1").val($("#releaseAddr1").val());
                        $("#returnAddr2").val($("#releaseAddr2").val());
                    }
                },
                errorCallbackFunc : function() {
                    alert('등록된 사업자 주소가 없습니다!');
                    $("#copyPartnerAddr").prop("checked", false);
                }
            });
        } else {
            $("#releaseZipcode").val("");
            $("#releaseAddr1").val("");
            $("#releaseAddr2").val("");

            if ($("#copyReleaseAddr").prop('checked') == true) {
                $("#returnZipcode").val("");
                $("#returnAddr1").val("");
                $("#returnAddr2").val("");
            }
        }
    },
    copyReleaseAddr : function() {
        if ($("#copyReleaseAddr").prop('checked') == true) {
            $("#returnAddr2").prop('readonly', true);

            $("#returnZipcode").val($("#releaseZipcode").val());
            $("#returnAddr1").val($("#releaseAddr1").val());
            $("#returnAddr2").val($("#releaseAddr2").val());

        } else {
            $("#returnAddr2").prop('readonly', false);
            $("#returnZipcode").val("");
            $("#returnAddr1").val("");
            $("#returnAddr2").val("");
        }
    },
    /**
     * 배송정보 > 배송정보 선택 select box
     * @param isSelectShipPolicyNo
     * @param selectShipPolicyNo
     */
    getSellerShipList : function(isSelectShipPolicyNo, selectShipPolicyNo) {
        CommonAjax.basic({
            url: '/partner/getSellerShipList.json',
            data: { useYn: 'Y'},
            method: 'GET',
            callbackFunc: function (res) {
                if (res) {
                    itemCommon.ship.initShipInfoDetail();

                    if (res.length > 0) {
                        for (let idx in res) {
                            let data = res[idx];

                            if (data.defaultYn == 'Y') {
                                $('#shipPolicyNo').append('<option value="' + data.shipPolicyNo + '" >[기본] ' + data.shipPolicyNm + '</option>');

                                if( $.jUtil.isEmpty(selectShipPolicyNo)) {
                                    $('#shipPolicyNo').val(data.shipPolicyNo);
                                    itemCommon.ship.getSellerShipDetail(isSelectShipPolicyNo, data.shipPolicyNo);
                                }
                            } else {
                                $('#shipPolicyNo').append('<option value="' + data.shipPolicyNo + '" >' + data.shipPolicyNm + '</option>');
                            }
                        }

                        if( !$.jUtil.isEmpty(selectShipPolicyNo)) {
                            $('#shipPolicyNo').val(selectShipPolicyNo);
                            itemCommon.ship.getSellerShipDetail(isSelectShipPolicyNo, selectShipPolicyNo);
                        }

                    } else {
                        itemCommon.ship.initShipInfoRelease();
                        itemCommon.ship.initShipAddInfo();
                    }

                } else {
                    itemCommon.ship.initShipAddInfo();
                }
            }});
    },
    /**
     * 배송정보 > 배송상세정보출력
     * @param shipPolicyNo
     * @param isSelectShipPolicyNo
     */
    getSellerShipDetail : function( isSelectShipPolicyNo, shipPolicyNo) {

        $('.deliveryDetail').html('');
        itemCommon.ship.initShipCheckBox();

        CommonAjax.basic({
            url: '/partner/getShipPolicyList.json',
            data: { shipPolicyNo: shipPolicyNo},
            method: 'GET',
            callbackFunc: function (data) {
                let res = data[0];
                if (res) {
                    // 배송정보번호
                    $('#detailShipPolicyNo').html(res.shipPolicyNo);
                    // 배송정보명
                    $('#detailShipPolicyNm').html(res.shipPolicyNm);
                    // 배송주체
                    $('#detailShipMngNm').html('업체배송');

                    // 배송유형
                    $('#detailShipTypeNm').html(res.shipTypeNm);
                    // 배송방법
                    $('#detailShipMethodNm').html(res.shipMethodNm);
                    // 배송비종류
                    $('#detailShipKindNm').html(res.shipKindNm);

                    // 배송비
                    if(res.shipFee) {
                        $('#detailShipFee').html( $.jUtil.comma(res.shipFee) + '원');
                    }

                    if (res.freeCondition) {
                        $('#detailShipFee').append(' ( ' + $.jUtil.comma(res.freeCondition) + '원 이상 구매 시 무료 )');
                    }

                    if (res.diffYn == 'Y') {
                        $('#detailShipFee').append(' ( 수량별 차등 : ' + $.jUtil.comma(res.diffQty) + '개 초과 시 마다 )');
                    }

                    // 추가 배송비
                    if (res.prepaymentYn == 'Y') {
                        $('#detailPrepaymentYn').html('선결제');

                    } else {
                        $('#detailPrepaymentYn').html('착불');
                    }

                    // 배송비 노출여부
                    if (res.dispYn == 'Y'){
                        $('#detailShipFeeDispYn').html('노출');

                    } else {
                        $('#detailShipFeeDispYn').html('노출안함');
                    }

                    if (isSelectShipPolicyNo) {
                        // 반품/교환비(편도)
                        $('#claimShipFee').val(res.claimShipFee);
                        //출고지
                        $('#releaseZipcode').val(res.releaseZipcode);
                        $('#releaseAddr1').val(res.releaseAddr1);
                        $('#releaseAddr2').val(res.releaseAddr2);

                        //회수지
                        $('#returnZipcode').val(res.returnZipcode);
                        $('#returnAddr1').val(res.returnAddr1);
                        $('#returnAddr2').val(res.returnAddr2);

                        //출고기한
                        $('.shipBundleLayer').prop('disabled', false);

                        if(res.releaseDay == '1'){
                            $('input:radio[name="shipReleaseDay"][value="T"]').trigger('click');
                            $('#releaseTime').val(res.releaseTime);
                        }else{
                            $('input:radio[name="shipReleaseDay"][value="N"]').trigger('click');
                            $('#releaseDayTemp').val(res.releaseDay);
                        }

                        $('#shipHolidayExceptYn').prop('checked', res.holidayExceptYn == "Y" ? true : false);

                        //안심번호 서비스
                        $('input:radio[name="safeNumberUseYn"]:input[value="'+res.safeNumberUseYn+'"]').trigger('click');
                    }
                    // 배송유형
                    if (res.shipKind == 'BUNDLE') {
                        // 묶음
                        // $('.shipBundleLayer').prop('disabled', true);

                    } else {
                        // 상품별
                        $('.shipBundleLayer').prop('disabled', false);
                    }

                }
            }
        });

    },
    /**
     * 배송정책관리 팝업
     */
    openShipPolicyMngPop : function() {
        shipPolicyMngPop.openModal();
    },
    /**
     * 배송정보 선택/상세정보 초기화
     */
    initShipInfoDetail : function() {
        itemCommon.util.initSelectBoxOptions($('#shipPolicyNo'), true, '선택하세요');

        $('.deliveryDetail').html('');
        $('#deliveryDetailTr').hide();

        itemCommon.ship.initShipCheckBox();

    },
    /**
     * 배송정보 동일적용 체크박스 초기화
     */
    initShipCheckBox : function() {
        $('#copyPartnerAddr').prop('checked', false);
        $('#copyReleaseAddr').prop('checked', false);
        $("#returnAddr2").prop('readonly', false);
    },
    /**
     * 배송정보 추가정보 초기화
     */
    initShipAddInfo : function() {
        // 반품/교환비(편도)
        $('#claimShipFee').val('');
        //출고지
        $('#releaseZipcode').val('');
        $('#releaseAddr1').val('');
        $('#releaseAddr2').val('');
        //회수지
        $('#returnZipcode').val('');
        $('#returnAddr1').val('');
        $('#returnAddr2').val('');
    },
    /**
     * 배송정보 출고기한 초기화
     */
    initShipInfoRelease : function() {
        $('#shipReleaseDay').val('');
        itemCommon.ship.getShipReleaseDay("N");
        $("input:checkbox[id='shipHolidayExceptYn']").prop("checked", false);
    },
    /**
     * 배송정보 > 출고기한 날짜 선택
     * @param val
     */
    getShipReleaseDay : function(val) {
        switch(val) {
            case "N" :
                $('#releaseDay').val('2');
                $("#releaseTimeDetailInfo").hide();
                $("#releaseDayDetailInfo").show();
                break;

            case "T" :
                $('#releaseDay').val('1');
                $("#releaseTimeDetailInfo").show();
                $("#releaseDayDetailInfo").hide();
                break;
        }
    },
    /**
     * 출하지 주소 callback
     * @param res
     */
    releaseZipcode : function (res) {
        $('#copyPartnerAddr').prop('checked', false);
        $('#releaseZipcode').val(res.zipCode);
        $('#releaseAddr1').val(res.roadAddr);
    },
    /**
     * 회수지 주소 callback
     * @param res
     */
    returnZipcode : function (res) {
        $('#copyReleaseAddr').prop('checked', false);
        $('#returnZipcode').val(res.zipCode);
        $('#returnAddr1').val(res.roadAddr);
    },
};



