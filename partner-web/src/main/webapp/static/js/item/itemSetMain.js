/**
 * 상품관리 > 상품등록
 */
$(document).ready(function() {

    itemSetMain.init();
    commonCategory.init();
    CommonAjaxBlockUI.global();

    //tooltip
    $('.ui-tooltip').uiTooltip();

    //acco
    $('#uiAcco1').uiAcco({
        current: 'all',
        toggle: false
    });
});

var itemSetMain = {
    uploadType : 'IMG',
    relationInfo : {
        relationNo :'',
        scateCd : '',
        adultType : ''
    },
    /**
     * 초기화
     */
    init : function() {

        itemSetMain.event();
        itemSetMain.attr.getAttrList("TAG");
        itemSetMain.initCategorySelectBox();

        itemCommon.util.initDateTime('10d', 'saleStartDt', 'saleEndDt', "HH:00:00", "HH:59:59");
        itemCommon.util.initDateTime('0d', 'dcStartDt', 'dcEndDt', "HH:00:00", "HH:59:59");

        itemCommon.util.initDate('-30d', 'rsvStartDt', 'rsvEndDt');
        itemCommon.util.initDate('0d', 'shipStartDt', 'shipEndDt');

        Calendar.datePicker('makeDt');
        Calendar.datePicker('expDt');
        Calendar.datePicker('shipStartDt');

        commonEditor.initEditor($('#editor1'), 300, 'ItemDetail');
        commonEditor.hmpImgUrl = hmpImgUrl;
        commonEditor.setHtml($('#editor1'), "");

        //판매정보
        $('#originPrice, #salePrice, #purchasePrice, #dcPrice, #saleUnit, #purchaseMinQty, #purchaseLimitDay, #purchaseLimitQty, #stockQty').allowInput("keyup", ["NUM"]);
        //배송,속성정보
        $('#claimShipFee, #rentalFee, #rentalPeriod, #rentalRegFee').allowInput("keyup", ["NUM"]);

        this.initIsMod();
    },
    /**
     * 등록/수정 별 화면
     */
    initIsMod : function () {

        if($('#isMod').val() == 'Y') {
            //수정
            itemSetMain.resetForm(false);
            $('#setItemTempTopBtn, #setItemTempBtn').hide();
            $('#itemStatusReasonArea').show();
            itemSetMain.getDetail($('#itemNo').val());
        }else{
            //등록
            $('#setItemTempTopBtn, #setItemTempBtn').show();
            $('#itemStatusReasonArea').hide();

            itemSetMain.resetForm(true);

            //직매입
            if ( saleType == 'D') {
                //장바구니제한
                itemSetMain.sale.setCartLimitYn('Y');
                $('#purchasePriceArea').show();
            } else {
                $('#purchasePriceArea').hide();
            }
        }
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        commonCategory.setCategorySelectBox(1, '', '', $('#cateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#cateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#cateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#cateCd4'));
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $("#searchCategoryNm").keydown(function(key) {
            if (key.keyCode == 13) {
                itemSetMain.basic.getCategoryNm();
            }
        });
        $("#searchCategoryNm").keyup(function() {
            if($.jUtil.isEmpty($('#searchCategoryNm').val())) {
                $('#searchCateLayer').html('');
            }
        });

        $('#cateCd4').bindChange(itemSetMain.sub.changeCate4);
        $("#cateCd1, #cateCd2, #cateCd3").on('change', function() {
            $('#categoryFullNm').text('');
        });

        $('#itemType').bindChange(itemSetMain.sale.changeItemType);

        $('#itemNm').calcTextLength('keyup', '#itemNmCount');
        $('#itemDesc').calcTextLength('keyup', '#textCountDesc');

        $('#rentalFee, #rentalPeriod, #rentalRegFee').on('keyup', function() {
            itemSetMain.sale.changeRental();
        });
        $("#dcPrice").keyup(function() {
            $('#discountRate').text(itemSetMain.sale.getDiscountRate( $('#dcPrice').val(), $('#salePrice').val()));
        });
        $('input:radio[name="dcYn"]').on('change', function() {
            $(this).val() == 'Y' ? $('.dcArea').show() : $('.dcArea').hide();
        });

        $('#dcPeriodYn').on('click', function() {
            itemSetMain.sale.changeDcPeriodYn($(this));
        });

        $('input:radio[name="salePeriodYn"]').on('change', function() {
            $(this).val() == 'Y' ? $('.salePeriodArea').show() : $('.salePeriodArea').hide();
            itemSetMain.sale.changeSalePeriodYn($(this).val());
        });

        $('input:radio[name="setSaleDate"]').on('change', function() {
            var flag = $('input[name="setSaleDate"]:checked').val();
            itemCommon.util.initDateTime(flag, 'saleStartDt', 'saleEndDt', "HH:00:00", "HH:59:59");
        });

        $('input:radio[name="rsvYn"]').on('change', function() {
            itemSetMain.sale.changeRsvYn($(this).val());
        });

        $('input:radio[name="purchaseLimitYn"]').on('click', function() {
            itemSetMain.sale.changePurchaseLimitYn($(this));
        });

        $('input:radio[name="globalAgcyYn"]').on('change', function() {
            itemSetMain.sale.changeGiftYn($(this).val());
        });

        $('#changeStockQtyCheck').on('change', function() {
            if($('#changeStockQtyCheck').is(":checked")) {
                $('#stockQty').prop('readonly', false);
            }else {
                $('#stockQty').prop('readonly', true);
            }
        });

        $('#dcPeriodYn').on('change', function(){
            if ($(this).is(':checked')) {
                $('#dcStartDt, #dcEndDt').prop('disabled', false);
            } else {
                $('#dcStartDt, #dcEndDt').prop('disabled', true);
            }
        });

        $('#purchaseLimitDuration').bindChange(itemSetMain.sale.getPurchaseLimitDuration);

        //옵션정보
        $('input:radio[name="optTxtUseYn"]').on('change', function() {
            itemSetMain.opt.getOptTxtUseYnRadio($(this).val());
        });

        $('#optTxtDepth').on('change', function() {
            itemSetMain.opt.getOptTxtDepth($(this).val());
        });

        //배송정보
        $('#shipPolicyNo').on('change', function() {
            if ($(this).val() != '') {
                itemCommon.ship.getSellerShipDetail(true, $(this).val());
            }
        });

        $('#openShipPolicyPop').bindClick(itemCommon.ship.openShipPolicyMngPop);
        $('#openCommissionInfoPop').bindClick(itemSetMain.openCommissionInfoPop);

        //이미지선택시
        $('input[name="mainImg0"], input[name="mainImg1"], input[name="mainImg2"], input[name="listImg"]').change(function() {
                itemCommon.img.addImg($(this).data('file-id'), $(this).prop('name'));
        });

        //파일선택시
        $('input[name="proof1"], input[name="proof2"], input[name="proof3"], input[name="proof4"]').change(function() {
                itemCommon.file.addFile($(this).data('file-id'), $(this).prop('name'));
        });

        $('#itemSetForm').on('propertychange change keyup paste input', '#fr-image-by-url-layer-text-1', function(e){
            commonEditor.inertImgurl = $('#fr-image-by-url-layer-text-1').val();
        });

        //부가정보
        $('input:radio[name="adultType"]').bindChange(itemSetMain.sale.changeAdultType);

        $('#getBrandBtn').bindClick(itemSetMain.attr.getBrand);
        $('#getMakerBtn').bindClick(itemSetMain.attr.getMaker);

        //브랜드
        $("#brandNm").keydown(function(key) {
            if (key.keyCode == 13) {
                itemSetMain.attr.getBrand();
            }
        });

        $("#brandNm").keyup(function() {
            if($.jUtil.isEmpty($('#brandNm').val())) {
                $('#searchBrandLayer').html('');
            }
        });

        //제조사
        $("#makerNm").keydown(function(key) {
            if (key.keyCode == 13) {
                itemSetMain.attr.getMaker();
            }
        });

        $("#makerNm").keyup(function() {
            if($.jUtil.isEmpty($('#makerNm').val())) {
                $('#searchMakerLayer').html('');
            }
        });

        itemSetMain.attr.event();
        itemSetMain.ext.event();
        itemCommon.ship.event();
        itemCommon.img.event();

        //버튼
        $('#setItemBtn, #setItemTopBtn').on('click', function(){
            itemSetMain.setItem('N');
        });

        $('#setItemTempBtn, #setItemTempTopBtn').on('click', function(){
            itemSetMain.setItem('Y');
        });

        $('#resetBtn').bindClick(itemSetMain.resetConfirm);
        $('#itemSetNewBtn').bindClick(itemSetMain.openItemSetMainTab);
        $('#itemSearchNewBtn').bindClick(itemSetMain.openItemMainTab);
    },
    /**
     * 초기화
     * @returns {boolean}
     */
    resetConfirm : function () {
        if( !confirm("등록된 정보를 모두 삭제하시겠습니까? ")){
            return false;
        }

        if($('#isMod').val() == 'Y' ){
            UIUtil.RemoveTabWindow ('Tab_itemModMain' );
        }else {
            itemSetMain.resetForm(true);
        }
    },
    /**
     * 상품등록 탭 Open
     */
    openItemSetMainTab : function () {
        UIUtil.addTabWindow('/item/itemSetMain?itemNo=','Tab_itemSetMain', '상품등록', '140');
    },
    /**
     * 상품조회/수정 탭 Open
     */
    openItemMainTab : function() {
        UIUtil.addTabWindow('/item/itemMain','Tab_itemMain', '상품조회/수정', '140');
    },
    /**
     * 수수료안내 팝업
     */
    openCommissionInfoPop : function() {
        categoryCommissionPop.openModal();
    },
    /**
     * 상품 등록/수정
     * @param tempYn
     */
    setItem : function(tempYn) {

        if (tempYn =='N'){
            //등록, 수정
            if(!itemSetMain.valid()) {
                return false;
            }
        } else {
            //임시저장
            if(!itemSetMain.tempValid()){
                return false;
            }
        }

        let mainId = $('input[name="mainYnRadio"]:checked').val();
        if (itemCommon.img.beforeMainId != mainId) {
            $('[id^="MN"]').each(function (idx) {
                if (mainId == idx) {
                    $(this).find('.mainYn').val('Y');
                    $(this).find('.changeYn').val('Y');
                } else {
                    $(this).find('.mainYn').val('N');
                }
            });
        }

        let itemForm = $('#itemSetForm').serializeObject(true);
        itemForm.itemDesc = commonEditor.getHtmlData('#editor1');
        itemForm.attrNoList = itemSetMain.attr.getAttrNoList();
        itemForm.tempYn = tempYn;

        if ($('input[name="mainYnRadio"]').is(':checked')) {
            itemForm.imgList[$(
                'input[name="mainYnRadio"]:checked').val()].mainYn = "Y";
        }

        CommonAjax.basic({
            url: '/item/setItem.json',
            data: JSON.stringify(itemForm),
            type: 'post',
            contentType: 'application/json',
            method:"POST",
            successMsg:null,
            callbackFunc:function(res) {
                $.jUtil.alert(res.returnMsg);

                if($('#isMod').val() == 'N') {
                    $('.regBtnGroup').hide();
                    $('.regSuccessArea').show();
                    $('#returnItemNo').text(res.returnKey);
                }else {
                    itemSetMain.resetForm(false);
                    itemSetMain.getDetail(res.returnKey);
                }
            },
            errorCallbackFunc : function(resError) {
                let res = resError.responseJSON;

                if (res.returnCode == '1057') { //금칙어
                    $.jUtil.alert(res.returnMessage, 'itemNm');
                } else {
                    $.jUtil.alert(res.returnMessage);
                }

            }
        });
    },
    /**
     * Form 초기화
     */
    resetForm : function(isReg) {
        //error 영역 초기화
        itemSetMain.initErrStyle();

        $('#itemSetForm select').each( function() {
            $(this).val( $(this).find("option[selected]").val() );
        });

        $("#itemSetForm").resetForm();

        //기본
        $(".changeStockQtyArea").hide();

        //배송
        itemCommon.ship.getSellerShipList(isReg);
        $('input:radio[name="shipReleaseDay"]:input[value="N"]').trigger('click');
        $('input:radio[name="shipReleaseDay"][value="N"]').trigger('click');
        $('#releaseDay').val('2');

        itemCommon.ship.initShipInfoRelease();
        itemCommon.ship.initShipAddInfo();
        itemCommon.ship.initShipCheckBox();
        itemCommon.ship.initShipInfoDetail();

        $('#cateCd1 option:eq(0)').prop('selected', true).trigger('change');
        $('#itemNm, #itemDesc, #saleRsvNo, #totalRentalFee').val('');
        $('#itemNm').setTextLength('#itemNmCount');
        $('#statusDesc, #baseCommissionRate, .itemStatusNm').html('');
        $('#useYn, #itemDispYn').val('Y');

        $('#itemTitleArea, .rentalArea, #changeStockQtyLabel, .dcArea, #setStatusArea, #isbnArea').hide();
        $('#cateCd1 option:eq(0)').prop('selected', true).trigger('change');
        $('#itemType, input[name="giftYn"]').prop('disabled', false);

        //판매
        $('#purchaseLimitDuration').trigger('change');

        $('#dcStartDt, #dcEndDt').prop('disabled', true);
        $('#dcPeriodYn').prop('checked', false);

        $('input:radio[name="salePeriodYn"]:input[value="'+$('input:radio[name="salePeriodYn"]:checked').val()+'"]').trigger('change');
        $('input:radio[name="purchaseLimitYn"]:input[value="'+$('input:radio[name="purchaseLimitYn"]:checked').val()+'"]').trigger('click');
        $('input:radio[name="adultType"]:input[value="NORMAL"]').trigger('click');

        itemSetMain.sale.changeRsvYn('N');
        itemSetMain.sale.setCartLimitYn('N');
        itemSetMain.sale.setAdultLimitYn('N');

        $("#stockQty").prop('readonly', false);
        $('#saleUnit, #purchaseMinQty').val(1);

        //옵션
        itemSetMain.opt.getOptTxtUseYnRadio('N');

        //브랜드, 제조사
        itemSetMain.attr.init();
        itemSetMain.attr.addBrandHtml(0,'');
        itemSetMain.attr.addMakerHtml(0,'');
        itemSetMain.attr.getMngNoticeList();

        commonEditor.setHtml($('#editor1'), "");
        //이미지
        itemCommon.img.init();
        commonEditor.inertImgurl = null;

        $('[id^=uploadProofFileSpan_]').each(function(){
            $(this).html('');
        });

        itemSetMain.relationInfoStr = null;
        itemSetMain.sub.dcateCartYn = 'N';

        $('#categoryFullNm').text('');

        $('.certCheck').show();
        $('.energy').hide();
        $('.certNo').prop('placeholder','');

        $('#certTable').find('.certExemptType').prop('disabled', true).hide();
        $('#certTable').find('.certNo, .certType').prop('disabled', false).prop('readonly', false).val('').show();
        $('#certTable').find('.certNoSubArea').html('');
        $('#certTable').find('.addObj').remove();
        $('.isCertRadio:checked').trigger('click');
        $('#discountRate').text('00.00');
    },
    /**
     * 상품 검색
     */
    getDetail : function(itemNo) {
        if (itemNo) {
            CommonAjaxBlockUI.startBlockUI();
            CommonAjax.basic({
                url: '/item/getDetail.json',
                data: {itemNo: itemNo },
                method: 'GET',
                callbackFunc: function (data) {

                    if (!$.jUtil.isEmpty(data.relationNo)) {
                        itemSetMain.relationInfo.relationNo = data.relationNo;
                        itemSetMain.relationInfo.scateCd = data.scateCd;
                        itemSetMain.relationInfo.adultType = data.adultType;
                    }

                    //기본정보
                    if(data.itemStatus == 'T') {
                        $('#setItemTempTopBtn, #setItemTempBtn').show();
                    }

                    $('#itemNo').val(data.itemNo);
                    $('#itemNoTxt').text(data.itemNo);
                    $('#itemTypeNm').text(data.itemTypeNm)

                    if(data.itemStatus =='A' && data.remainStockQty <= 0 ) {
                        $('#itemStatusTotal').text("일시품절 ( " + data.chgDt + " )");
                    } else {
                        $('#itemStatusTotal').text(data.itemStatusNm + " ( " + data.chgDt + " )");
                    }
                    $("#itemStatusDesc").text(data.statusDesc);

                    commonCategory.setCategorySelectBox(1, '', data.lcateCd, $('#cateCd1'));
                    commonCategory.setCategorySelectBox(2, data.lcateCd, data.mcateCd, $('#cateCd2'));
                    commonCategory.setCategorySelectBox(3, data.mcateCd, data.scateCd, $('#cateCd3'));
                    commonCategory.setCategorySelectBox(4, data.scateCd, data.dcateCd, $('#cateCd4'));


                    if (!$.jUtil.isEmpty(data.dcateNm)) {
                        var cateNm = data.lcateNm + ' > ' + data.mcateNm + ' > '
                            + data.scateNm + ' > ' + data.dcateNm;
                    }

                    $('#categoryFullNm').text(cateNm);

                    itemSetMain.sub.dcateCartYn = data.cartYn;
                    itemSetMain.sale.setCartLimitYn(data.cartYn);

                    itemSetMain.sale.setAdultLimitYn(data.adultLimitYn);

                    $('#itemType').val(data.itemType).prop('disabled', true);
                    itemSetMain.sale.changeItemType($('#itemType'));

                    $('#itemNm').val(data.itemNm).setTextLength('#itemNmCount');
                    $('#itemStatus').val(data.itemStatus);

                    $('input:radio[name="dispYn"]:input[value="'+data.dispYn+'"]').trigger('click');

                    //판매정보
                    $('#originPrice').val(data.originPrice);

                    if(data.saleType == 'D') {
                        $('#purchasePriceArea').show();
                        $('#purchasePrice').val(data.purchasePrice);
                    }else {
                        $('#purchasePriceArea').hide();
                    }

                    $('#salePrice').val(data.salePrice);

                    $('#commissionType').val(data.commissionType);
                    $('#commissionTitle').text("수수료");

                    if (data.commissionType == 'R') {
                        $('#commissionRate').val(data.commissionRate);
                        $('#baseCommissionRate').text(data.commissionRate + "%");
                    } else {
                        $('#commissionPrice').val(data.commissionPrice);
                        $('#baseCommissionRate').text(data.commissionPrice + "원");
                    }
                    $('input:radio[name="dcYn"]:input[value="'+data.dcYn+'"]').trigger('click');

                    if (data.dcYn == 'Y') {
                        $('#dcStartDt').val(data.dcStartDt);
                        $('#dcEndDt').val(data.dcEndDt);
                        $('#dcPrice').val(data.dcPrice);

                        if (data.dcPeriodYn == 'Y') {
                            $('#dcPeriodYn').prop('checked', true);
                            itemSetMain.sale.changeDcPeriodYn($('#dcPeriodYn'));
                        }

                        $('#discountRate').text(itemSetMain.sale.getDiscountRate(data.dcPrice, data.salePrice));
                    }

                    $('input:radio[name="taxYn"]:input[value="'+data.taxYn+'"]').trigger('click');
                    $('input:radio[name="salePeriodYn"]:input[value="'+data.salePeriodYn+'"]').trigger('click');
                    itemSetMain.sale.changeSalePeriodYn(data.salePeriodYn, data.saleStartDt, data.saleEndDt);

                    $('#stockQty').val(data.remainStockQty).prop('readonly', true);
                    $("input:checkbox[name='chgStockQtyYn']").prop("checked", false);
                    $('.changeStockQtyArea').show();

                    $('#saleUnit').val(data.saleUnit);
                    $('#purchaseMinQty').val(data.purchaseMinQty);

                    $('input:radio[name="giftYn"]:input[value="'+data.giftYn+'"]').trigger('click');

                    if (!$.jUtil.isEmpty(data.isbn)) {
                        $('#isbn').val(data.isbn);
                    }
                    itemSetMain.sale.setIsbnYn(data.isbnYn);

                    $('input:radio[name="rsvYn"]:input[value="'+data.rsvYn+'"]').trigger('click');
                    itemSetMain.sale.changeRsvYn(data.rsvYn, data.saleRsvNo, data.rsvStartDt, data.rsvEndDt, data.shipStartDt);

                    $('input[name="purchaseLimitYn"][value="'+data.purchaseLimitYn+'"]').prop('checked', true).trigger('click');

                    if (data.purchaseLimitDuration) {
                        $('#purchaseLimitDuration').val(data.purchaseLimitDuration).trigger('change');
                    } else {
                        $('#purchaseLimitDuration').trigger('change');
                    }

                    $('#purchaseLimitDay').val(data.purchaseLimitDay == 0 ? 1: data.purchaseLimitDay);
                    $('#purchaseLimitQty').val(data.purchaseLimitQty == 0 ? 1: data.purchaseLimitQty);

                    $('#purchaseMinQty').val(data.purchaseMinQty);

                    $('input:radio[name="cartLimitYn"]:input[value="'+data.cartLimitYn+'"]').trigger('click');

                    //옵션정보
                    $('input:radio[name="optTxtUseYn"]:input[value="'+data.optTxtUseYn+'"]').trigger('click');
                    $('#optTxtDepth').val(data.optTxtDepth).trigger('change');
                    $('#opt1Text').val(data.opt1Text);
                    $('#opt2Text').val(data.opt2Text);

                    //이미지
                    if (!$.jUtil.isEmpty(data.imgList)) {
                        let changeYn = 'N';

                        data.imgList.forEach(obj => {

                            let priority = obj.priority;

                            if (obj.imgType == 'MN' && !$.jUtil.isEmpty($('#MN' + priority + ' .imgNo').val())) {
                                changeYn = 'Y';

                                $('.itemImg').each(function(idx1) {
                                    if ($.jUtil.isEmpty($('#MN' + (idx1) + ' .imgNo').val())) {
                                        priority = idx1;
                                        return false;
                                    }
                                });
                            }

                            let _this = $('#MN' + priority);

                            if (!$.jUtil.isEmpty(obj.mainYn) && obj.mainYn == 'Y') {
                                $('input[name="mainYnRadio"][value='+priority+']').trigger('click');
                                itemCommon.img.beforeMainId = priority;
                            }

                            if (obj.imgType == 'LST') {
                                _this = $('#imgLST');
                            }

                            itemCommon.img.setImg(_this, {
                                'imgNo' : obj.imgNo,
                                'imgNm' : obj.imgNm,
                                'imgUrl': obj.imgUrl,
                                'src'   : hmpImgUrl + obj.imgUrl,
                                'changeYn' : 'N'
                            });
                        });

                        if (changeYn == 'Y') {
                            $('#imgMNDiv').find('.itemImg').each(function() {
                                if (!$.jUtil.isEmpty($(this).find('.imgUrl').val())) {
                                    $(this).find('.changeYn').val('Y');
                                }
                            });
                        }
                    }

                    //Editor
                    commonEditor.setHtml($('#editor1'), data.itemDesc);

                    //속성정보
                    itemSetMain.attr.addBrandHtml(data.brandNo, data.brandNm);
                    itemSetMain.attr.addMakerHtml(data.makerNo, data.makerNm);

                    itemSetMain.attr.getAttrList("ATTR", data.itemNo, data.scateCd);

                    $('#makeDt').val(data.makeDt);
                    $('#expDt').val(data.expDt);

                    //부가정보
                    $('#sellerItemCd').val(data.sellerItemCd);

                    $('input[name="adultType"][value="'+data.adultType+'"]').trigger('click');
                    $('input[name="epYn"][value="'+data.epYn+'"]').trigger('click');

                    $('input[name="globalAgcyYn"][value="'+data.globalAgcyYn+'"]').trigger('click');
                    $('#srchKeyword').val(data.srchKeyword);

                    itemSetMain.attr.getAttrList("TAG", data.itemNo);

                    if (data.isCert) {
                        $('input[name="isCert"][value="'+data.isCert+'"]').trigger('click');
                    }

                    //안전인증
                    for (let i in data.certList) {
                        let cert = data.certList[i];
                        let certId = '#certTr' + cert.certGroup;

                        if (i > 1 && !$.jUtil.isEmpty($(certId).find('.certType').val())) {
                            $(certId).find('.certAdd').trigger('click');
                            certId = '#cert_' + ($('.addObj').length - 1);
                        } else {
                            $('#cert_' + cert.certGroup + '_isCert_' + cert.isCert).trigger('click');
                        }

                        $(certId).find('.itemCertSeq').val(cert.itemCertSeq);
                        $(certId).find('.isCert').val(cert.isCert);
                        $(certId).find('.certExemptType').val(cert.certExemptType);
                        $(certId).find('.certType').val(cert.certType).trigger('change');

                        if (cert.certType == 'ET_CER') {
                            $(certId).find('.energy').val(cert.certNo);
                        }

                        if (!$.jUtil.isEmpty(cert.certNo)) {
                            $(certId).find('.certNo').val(cert.certNo).prop('readonly', true);
                            //기타인증
                            if (cert.certType =='ET_CT') {
                                let certNoValArr = cert.certNo.split('|');

                                $(certId).find('.certNo').hide();
                                $(certId).find('.certNoSub').each(function(i) {
                                    $(this).val(certNoValArr[i]);
                                });
                            }
                        }
                    }

                    //증빙서류
                    for (let i in data.proofFileList) {
                        itemCommon.file.appendProofFile(data.proofFileList[i]);
                    }

                    if (data.itemType == 'T') {
                        $('#rentalFee').val(data.rentalFee);
                        $('#rentalRegFee').val(data.rentalRegFee);
                        $('#rentalPeriod').val(data.rentalPeriod);

                        itemSetMain.sale.changeRental();
                    }

                    //고시정보
                    itemSetMain.attr.getMngNoticeList(data.gnoticeNo, data.noticeList);
                    $('#gnoticeNo').val(data.gnoticeNo);

                    //배송
                    $('#shipPolicyNo').val(data.shipPolicyNo);
                    itemCommon.ship.getSellerShipDetail(false, data.shipPolicyNo);

                    if (data.itemType != 'E') {
                        $('#claimShipFee').val(data.claimShipFee);
                        $('#releaseZipcode').val(data.releaseZipcode);
                        $('#releaseAddr1').val(data.releaseAddr1);
                        $('#releaseAddr2').val(data.releaseAddr2);

                        $('#returnZipcode').val(data.returnZipcode);
                        $('#returnAddr1').val(data.returnAddr1);
                        $('#returnAddr2').val(data.returnAddr2);

                        $('#shipHolidayExceptYn').prop("checked", data.holidayExceptYn == "Y" ? true : false);

                        if(data.releaseDay == '1'){
                            $('input:radio[name="shipReleaseDay"][value="T"]').trigger('click');
                            $('#releaseTime').val(data.releaseTime);
                        }else{
                            $('input:radio[name="shipReleaseDay"][value="N"]').trigger('click');
                            $('#releaseDayTemp').val(data.releaseDay);
                        }
                        $('input[name="safeNumberUseYn"][value="'+data.safeNumberUseYn+'"]').trigger('click');
                    }

                    $('#imgDispYn').val(data.imgDispYn == null ? 'Y' : data.imgDispYn);

                    if (data.globalAgcyYn == 'Y') {
                        itemSetMain.sale.changeGiftYn("Y");
                    }

                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
        }
    },
    /**
     * 에러 표시영역 초기화
     */
    initErrStyle : function() {
        //필수 에러 노출 초기화
        $('.txt-err').remove();
        $('input').removeClass('err');
    },
    /**
     * Null array 체크
     * @param arr
     * @returns {boolean}
     */
    checkIsEmptyArr : function (arr) {

        for(var i =0; i < arr.length; i++) {
            var value = $('#'+arr[i]).val();
            if($.jUtil.isEmpty(value)) {
                return $.jUtil.boxAlert(arr[i]);
            }
        }
        return true;
    },
    /**
     * 임시저장 등록/수정
     * @returns {boolean}
     */
    tempValid : function() {

        itemSetMain.initErrStyle();

        var _arr1 = ['cateCd4', 'itemNm' ];
        if (! itemSetMain.checkIsEmptyArr(_arr1)) {
            return false;
        }

        return true;
    },
    /**
     * 상품 등록/수정 유효성 검사
     */
    valid : function() {

        itemSetMain.initErrStyle();

        //기본정보, 세일정보 필수체크
        var _arr1 = ['cateCd4', 'itemNm', 'originPrice', 'salePrice', 'stockQty', 'gnoticeNo' ];
        if (! itemSetMain.checkIsEmptyArr(_arr1)) {
            return false;
        }

        //배송정보
        if ($('#itemType').val() != 'E' && $('#itemType').val() != 'M' && $('#itemType').val() != 'T') {
            var _arr2 = ['shipPolicyNo', 'returnZipcode', 'returnAddr1', 'releaseZipcode', 'releaseAddr1' ];
            if (!itemSetMain.checkIsEmptyArr(_arr2)) {
                return false;
            }
        }

        //부가정보
        if ($.jUtil.isNotAllowInput($("#itemNm").val(), ['SYS_DIVISION'])) {
            return $.jUtil.alert($.jUtil.getNotAllowInputMessage(["SYS_DIVISION"]), 'itemNm');
        }

        //판매기간 검증
        if ($('input[name="salePeriodYn"]:checked').val() == 'Y') {
            if ($.jUtil.isEmpty($('#saleStartDt').val()) || $.jUtil.isEmpty($('#saleEndDt').val())) {
                return $.jUtil.alert('판매기간을 입력해주세요.', 'saleStartDt');
            }

            if (moment($('#saleEndDt').val()).isBefore($('#saleStartDt').val()) == true) {
                return $.jUtil.alert('판매종료일은 시작일 이전으로 입력 할 수 없습니다.', 'saleEndDt');
            }
        }

        //할인가격 특정기간 할인 날짜 검증
        if ($('input[name="dcYn"]:checked').val() == 'Y') {

            if ($('#dcPeriodYn').is(':checked') == true) {
                if ($.jUtil.isEmpty($('#dcStartDt').val()) || $.jUtil.isEmpty(
                    $('#dcEndDt').val())) {
                    return $.jUtil.alert('할인기간을 입력해주세요.', 'dcStartDt');
                }

                if (moment($('#dcEndDt').val()).isBefore($('#dcStartDt').val())
                    == true) {
                    return $.jUtil.alert('할인종료일은 시작일 이전으로 입력 할 수 없습니다.',
                        'dcEndDt');
                }

                if ($('input[name="salePeriodYn"]:checked').val() == 'Y') {
                    if (moment($('#dcEndDt').val()).isBefore(
                        $('#saleStartDt').val()) == true) {
                        return $.jUtil.alert('할인종료일은 시작일 이전으로 입력 할 수 없습니다.',
                            'dcEndDt');
                    }
                }

            }

            if ( $.jUtil.isEmpty($('#dcPrice').val()) || $.jUtil.isEmpty($('#salePrice').val()) ||
                Number($('#dcPrice').val()) > Number($('#salePrice').val())
            ) {
                return $.jUtil.alert('할인가격이 판매가격보다 클 수 없습니다', 'dcPrice');
            }
        }

        if ($('input[name="rsvYn"]:checked').val() == 'Y') {
            if ($('input[name="giftYn"]:checked').val() == 'Y') {
                return $.jUtil.alert('예약판매 상품은 선물하기 설정이 불가합니다', 'rsvYn');
            }
        }

        //판매가격, 할인가격 검증
        if ($('#itemType').val() == 'T' || $('#itemType').val() == 'E' || $('#itemType').val() == 'M') {
        } else {
            if (Number($('#salePrice').val()) < 100) {
                return $.jUtil.alert("판매가격은 100원 이상 등록 가능합니다.", 'salePrice');
            }

            if ($('input[name="dcYn"]:checked').val() == 'Y' &&  Number($('#dcPrice').val()) < 100) {
                return $.jUtil.alert("할인가격은 100원 이상 등록 가능합니다.", 'dcPrice');
            }
        }

        //상품유형 렌탈 검사
        if ($('#itemType').val() == 'T') {
            if ($.jUtil.isEmpty($('#rentalFee').val())) {
                return $.jUtil.alert("월렌탈료를 입력해 주세요.", 'rentalFee');
            }

            if ($.jUtil.isEmpty($('#rentalRegFee').val())) {
                return $.jUtil.alert("등록비를 입력해 주세요.", 'rentalRegFee');
            }

            if ($.jUtil.isEmpty($('#rentalPeriod').val())) {
                return $.jUtil.alert("의무사용기간을 입력해 주세요.", 'rentalPeriod');
            }
        }

        //판매단위수량 검증
        if ($.jUtil.isEmpty($('#saleUnit').val())) {
            return $.jUtil.alert("판매단위수량을 입력해주세요.", 'saleUnit');
        }

        //최소구매수량 검증
        if ($.jUtil.isEmpty($('#purchaseMinQty').val())) {
            return $.jUtil.alert("최소구매수량을 입력해주세요.", 'purchaseMinQty');
        }

        //구매수량제한 검증
        if ($('input[name="purchaseLimitYn"]:checked').val() == 'Y') {
            if (Number($('#saleUnit').val()) > Number($('#purchaseLimitQty').val())) {
                return $.jUtil.alert("구매수량제한 값은 판매단위수량 보다 적을 수 없습니다.", 'saleUnit');
            }

            if (Number($('#purchaseMinQty').val()) > Number($('#purchaseLimitQty').val())) {
                return $.jUtil.alert("구매수량제한 값은 최소구매수량 보다 적을 수 없습니다.", 'purchaseMinQty');
            }

            if ($('#purchaseLimitDuration').val() == 'P' && Number($('#purchaseLimitDay').val()) > 30) {
                return $.jUtil.alert("구매수량제한 기간은 30일을 초과할 수 없습니다.", 'purchaseLimitDay');
            }
        }

        if (!$('input:radio[name=taxYn]').is(':checked')) {
            return $.jUtil.alert("판매정보 과세여부를 설정해 주세요.", 'taxYnY');
        }

        if ($('input[name="imgList[].imgUrl"]:input[value!=""]').length == 0) {
            return $.jUtil.alert("이미지를 등록해 주세요.", 'productImgRegBtn0');
        }

        if ($.jUtil.isEmpty($('input[name="mainYnRadio"]:checked').val())
            || $.jUtil.isEmpty($('input[name="mainYnRadio"]:checked').closest('.imgMainYnRadioView').siblings('.preview').find('.imgUrl').val())
        ) {
            return $.jUtil.alert("대표이미지를 선택해 주세요.", 'mainYnRadio0');
        }

        //상품유형 렌탈 검사
        if ($('#itemType').val() == 'T') {
            if ($.jUtil.isEmpty($('#rentalFee').val())) {
                return $.jUtil.alert("월렌탈료를 입력해 주세요.", 'rentalFee');
            }

            if ($.jUtil.isEmpty($('#rentalRegFee').val())) {
                return $.jUtil.alert("등록비를 입력해 주세요.", 'rentalRegFee');
            }

            if ($.jUtil.isEmpty($('#rentalPeriod').val())) {
                return $.jUtil.alert("의무사용기간을 입력해 주세요.", 'rentalPeriod');
            }
        }

        //isbn 검증
        if ($('#isbnArea').is(':visible')) {
            if($.jUtil.isEmpty($('#isbn').val())) {
                return $.jUtil.alert("도서 카테고리는 ISBN을 입력해야 합니다.", 'isbn');
            }else{

                if ($('input[name="dcYn"]:checked').val() == 'Y') {
                    if($('#salePrice').val() * 0.9 >= $('#dcPrice').val() ) {
                        return $.jUtil.alert("도서 카테고리는 10%이상 할인 설정이 불가합니다.", 'dcPrice');
                    }
                }
            }
        }

        //에디터 검증
        if (commonEditor.checkIsEmpty('#editor1')) {
            $("#editor1").prop("tabindex", -1).focus();
            return $.jUtil.alert("상품상세를 입력해 주세요.", 'editor1');
        }

        //옵션
        if ($('input[name="optTxtUseYn"]:checked').val() == 'Y') {
            if ($.jUtil.isEmpty($('#opt1Text').val())) {
                return $.jUtil.alert("텍스트 옵션 타이틀을 입력해 주세요.", 'opt1Text');
            } else if ($('#optTxtDepth').val() == '2' && $.jUtil.isEmpty($('#opt2Text').val())) {
                return $.jUtil.alert("텍스트 옵션 타이틀을 입력해 주세요.", 'opt2Text');
            }
        }

        if (!$.jUtil.isEmpty(itemSetMain.relationInfo.relationNo)) {

            let preMsg = '';

            if (itemSetMain.relationInfo.scateCd != $('#cateCd3').val()) {
                preMsg = '카테고리 조건이';
            } else if (itemSetMain.relationInfo.adultType != $('input[name="adultType"]:checked').val()) {
                preMsg = '성인상품유형 조건이';
            } else if ($('input[name="cartLimitYn"]:checked').val() == 'Y') {
                preMsg = '장바구니제한 조건이';
            } else if ($('input[name="optTxtUseYn"]:checked').val() == 'Y') {
                preMsg = '텍스트옵션 조건이';
            }

            if (!$.jUtil.isEmpty(preMsg) && !confirm(preMsg + " 변경되면 기존 연관상품이 해지됩니다. 변경하시겠습니까?")) {
                return false;
            }
        }

        //해외배송 검증
        if ($('input[name="globalAgcyYn"]:checked').val() == 'Y' && $('input[name="safeNumberUseYn"]:checked').val() == 'Y') {
            return $.jUtil.alert("해외배송의 경우 안심번호를 사용할 수 없습니다.", 'globalAgcyYn');
        }

        // 검색키워드
        if ($('#srchKeyword').val().split(',').length > 20) {
            return $.jUtil.alert("검색키워드는 최대 20개 까지 등록 가능합니다.", 'srchKeyword');
        }

        //고시정보
        let checkNoticeDesc = true;
        $('.noticeDesc').each(function() {
            if($.jUtil.isEmpty($(this).val())) {
                checkNoticeDesc = false;
                $(this).focus();
                return $.jUtil.alert('상품 고시정보를 입력해주세요.');
            }
        });

        if (!checkNoticeDesc) {
            return checkNoticeDesc;
        }

        //안전인증 검증
        let checkCert = true;
            $('.certNo').each(function(){
            if( !$(this).prop('readonly') && $(this).css('display') != 'none') {
                if($.jUtil.isEmpty($(this).val())) {
                    checkCert = false;
                    $(this).focus();
                    return $.jUtil.alert('안전인증을 입력해주세요.');
                }else {

                    if($(this).closest('.certRow').find('.certCheck').css('display') != 'none' &&
                        $(this).closest('.certRow').find('.certYn').val() == 'N') {
                        checkCert = false;
                        $(this).focus();
                        return $.jUtil.alert('안전인증을 인증해주세요.');
                    }
                }
            }
            if (!$(this).prop('readonly') && $(this).css('display') != 'none' && $.jUtil.isEmpty($(this).val())) {
                checkCert = false;
                $(this).focus();
                return $.jUtil.alert('안전인증을 입력해주세요.');
            }
        });

        if (checkCert) {
            $('.energy').each(function(){
                if ($(this).css('display') != 'none' && $.jUtil.isEmpty($(this).val())) {
                    checkCert = false;
                    $(this).focus();
                    return $.jUtil.alert('안전인증을 입력해주세요.');
                }
            });
        }

        if (!checkCert) {
            return checkCert;
        }

        return true;
    },
};

itemSetMain.opt = {
    /**
     * 텍스트형 옵션 라디오 버튼 컨트롤
     * @param val
     */
    getOptTxtUseYnRadio : function(val) {

        itemSetMain.opt.getOptTxtDepth('1');
        $('#optTxtDepth').val('1').prop("selected", true);
        switch (val) {
            case 'Y' :
                itemCommon.util.setClassDisabledAll('setOptTxtFormAll', false);
                $('.setOptTxtFormAll').show();
                break;

            case 'N' :
                itemCommon.util.setClassDisabledAll('setOptTxtFormAll', true);
                $('.setOptTxtFormAll').hide();
                break;
        }
    },
    /**
     * 옵션정보 > 텍스트형 옵션 > 단계선택
     * @param val
     */
    getOptTxtDepth : function(val) {
        $('.optTxtDepthTr').css('display', 'none');

        for (let idx = 0; val > idx; idx++) {
            let idxNum = idx + 1;

            $('#optTxtDepthTr' + idxNum).css('display', '');
        }
    }
};

itemSetMain.basic = {
    /**
     * 카테고리 전체 명칭 조회
     */
    getFullCateNm : function () {
        var cateNm = $("select[name=lcateCd] option:selected").text() + ' > '
            + $("select[name=mcateCd] option:selected").text() + ' > '
            + $("select[name=scateCd] option:selected").text() +' > '
            + $("select[name=dcateCd] option:selected").text();

        $('#categoryFullNm').text(cateNm);
    },
    /**
     * 카테고리 이름 조회
     */
    getCategoryNm : function () {

        var cateNm = $('#searchCategoryNm').val();
        if ($.jUtil.isEmpty(cateNm) || cateNm.length < 2) {
            return $.jUtil.alert("카테고리를 입력해주세요.(2자이상)", 'searchCategoryNm');
        }

        CommonAjax.basic({
            url: '/common/category/getCategoryNm.json',
            data: {cateSchNm: cateNm},
            method: 'GET',
            callbackFunc: function (res) {
                var resCnt = res.length;

                if(resCnt == 0) {
                    $('#searchCateLayer').html('');
                    $('#searchCateLayer').append('<li class="no-result">검색결과가 없습니다.</li>');
                } else {
                    $('#searchCateLayer').html('');

                    for (let i = 0; resCnt > i; i++) {

                        let id = res[i].lcateCd + '_' + res[i].mcateCd + '_' + res[i].scateCd + '_' + res[i].dcateCd;
                        let _name = res[i].lcateNm + ' > ' + res[i].mcateNm + ' > ' + res[i].scateNm + ' > ' + res[i].dcateNm;
                        let searchVal = itemCommon.util.replaceAll(_name, cateNm, '<i>'+cateNm+'</i>' );

                        $('#searchCateLayer').append('<li id="cateLayer_'+id+'"><button type="button">'+searchVal+'</button></li>');
                    }
                    itemSetMain.basic.addCategory();
                }
                $('.searchCateLayer').show();
            }
        });
    },
    /**
     * 카테고리 리스트 선택
     */
    addCategory : function() {

        $('li[id^=cateLayer_]').click(function() {

            let idArr = $(this).attr('id').split('_');
            let depth = idArr.length -1 ;

            switch (depth) {
                case 1 :
                    commonCategory.setCategorySelectBox(1, '', idArr[1], $('#cateCd1'));
                    break;
                case 2 :
                    commonCategory.setCategorySelectBox(1, '', idArr[1], $('#cateCd1'));
                    commonCategory.setCategorySelectBox(2, idArr[1], idArr[2], $('#cateCd2'));
                    break;
                case 3 :
                    commonCategory.setCategorySelectBox(1, '', idArr[1], $('#cateCd1'));
                    commonCategory.setCategorySelectBox(2, idArr[1], idArr[2], $('#cateCd2'));
                    commonCategory.setCategorySelectBox(3, idArr[2], idArr[3], $('#cateCd3'));
                    itemSetMain.attr.getAttrList("ATTR", '', idArr[3]);
                    break;
                case 4 :
                    commonCategory.setCategorySelectBox(1, '', idArr[1], $('#cateCd1'), function () {
                        commonCategory.setCategorySelectBox(2, idArr[1], idArr[2], $('#cateCd2'), function () {
                            commonCategory.setCategorySelectBox(3, idArr[2], idArr[3], $('#cateCd3'), function () {
                                commonCategory.setCategorySelectBox(4,idArr[3], idArr[4], $('#cateCd4'), function () {
                                    itemSetMain.attr.getAttrList("ATTR", '', idArr[3]);
                                    itemSetMain.sub.changeCate4( $('#cateCd4'));
                                });
                            });
                        });
                    });
                    break;
            }
        });
    },
}

itemSetMain.sale = {
    commissionTypeObj : {R : 'R', A : 'A'}, // 수수료부과기준 (정률, 정액)
    /**
     * 판매정보 > 구매수량제한 > 구매제한타입
     * @param _this
     */
    getPurchaseLimitDuration : function(_this) {
        $('.purchaseLimitDuration').css('display', '');
        $('.purchaseLimitDuration' + $(_this).val()).css('display', 'none');

        if ($(_this).val() == 'P') {
            $('#purchaseLimitDay').prop('disabled', false);

        } else {
            $('#purchaseLimitDay').prop('disabled', true);
        }
    },
    /**
     * 상품유형 변경 이벤트
     * @param obj
     */
    changeItemType : function (obj) {

        $('input[name="dcYn"][value="Y"]').prop('disabled', false);

        let type = $(obj).val()
        switch (type) {
            case 'E':
                //e-티켓 : 배송정책, 렌탈 X
                $('.shipArea, .rentalArea').hide();

                itemSetMain.sale.changeGiftYn("Y");
                if (itemSetMain.sub.dcateCartYn == 'N') {
                    itemSetMain.sale.setCartLimitYn('N');
                }
                $('#originPrice, #salePrice, #purchasePrice').prop('readonly', false);
                $('input[name="purchaseLimitYn"][value="N"]').prop('checked', true).trigger('click');

                //해외배송 사용안함 강제처리
                $('input[name="globalAgcyYn"][value="Y"]').prop('disabled', true);
                $('input[name="globalAgcyYn"][value="N"]').prop('checked', true).trigger('change');
                break;
            case 'T': //렌탈
            case 'M': //모바일
                //렌탈영역
                type == 'T' ? $('.rentalArea').show() : $('.rentalArea').hide();

                //배송정책
                $('.shipArea').hide();

                //가격
                $('#salePrice, #originPrice, #purchasePrice').val(0).prop('readonly', true);

                //선물하기
                itemSetMain.sale.changeGiftYn("Y");
                //장바구니제한
                itemSetMain.sale.setCartLimitYn('Y');

                //할인가격 셋팅
                $('input[name="dcYn"][value="Y"]').prop('disabled', true);
                $('input[name="dcYn"][value="N"]').prop('checked', true).trigger('change');

                //구매수량 제한
                $('input[name="purchaseLimitYn"][value="Y"]').prop('checked', true).trigger('click');

                //해외배송 사용안함 강제처리
                $('input[name="globalAgcyYn"][value="Y"]').prop('disabled', true);
                $('input[name="globalAgcyYn"][value="N"]').prop('checked', true).trigger('change');
                break;
            default :
                //나머지
                $('.shipArea').show();
                $('.rentalArea').hide();
                itemSetMain.sale.changeGiftYn('N');
                if (itemSetMain.sub.dcateCartYn == 'N') {
                    itemSetMain.sale.setCartLimitYn('N');
                }
                $('#originPrice, #salePrice, #purchasePrice').prop('readonly', false);
                $('input[name="purchaseLimitYn"][value="N"]').prop('checked', true).trigger('click');

                //해외배송 사용안함 강제처리 해제
                $('input[name="globalAgcyYn"][value="Y"]').prop('disabled', false);
                break;
        }
    },
    /**
     * 판매기간 셋팅
     * @param salePeriodYn
     * @param saleStartDt
     * @param saleEndDt
     */
    changeSalePeriodYn : function(salePeriodYn, saleStartDt, saleEndDt) {
        if (salePeriodYn == 'Y') {
            $('#saleStartDt, #saleEndDt').prop('disabled', false);
            $('#saleStartDt').val(saleStartDt);
            $('#saleEndDt').val(saleEndDt);
        } else {
            $('#saleStartDt, #saleEndDt').val('').prop('disabled', true);
        }
    },
    /**
     * 특정기간 할인 셋팅
     * @param _this
     */
    changeDcPeriodYn : function (_this) {
        if( $(_this).is(':checked') == true ){
            $('#dcStartDt, #dcEndDt').prop('disabled', false);
        }else{
            $('#dcStartDt, #dcEndDt').prop('disabled', true);
        }
    },
    /**
     * 선물하기 여부 컨트롤
     * @param giftYn
     */
    changeGiftYn :function(giftYn) {

        if (giftYn == 'Y') {
            $('input[name="giftYn"][value="N"]').trigger('click');
            $('input[name="giftYn"][value="Y"]').prop('disabled', true);
        } else {
            if ($('input:radio[name="rsvYn"]:checked').val() == 'N'
                && $('input[name="adultType"]:checked').val() == 'NORMAL'
                && $('#itemType').val() != 'T' //렌탈
                && $('#itemType').val() != 'M' //모바일
                && $('#itemType').val() != 'E' //E티켓
            ) {
                $('input[name="giftYn"][value="Y"]').prop('disabled', false);
            }
        }
    },
    /**
     * 구매제한여부 컨트롤
     * @param obj
     */
    changePurchaseLimitYn : function (obj) {
        let purchaseLimitYn = $(obj).val();

        switch (purchaseLimitYn) {
            case 'Y' :
                $('#purchaseLimitArea').show();
                if ($('#itemType').val() != 'T' && $('#itemType').val() != 'M') {
                    $('#purchaseLimitQty').prop('readonly', false);
                    $('#purchaseLimitDuration option[value="P"]').show();
                    $('input[name="purchaseLimitYn"][value="N"]').prop('disabled', false);

                } else{
                    $('#purchaseLimitQty').val(1).prop('readonly', true);
                    $('#purchaseLimitDuration option[value="P"]').hide();
                    $('input[name="purchaseLimitYn"][value="N"]').prop('disabled', true);
                }
                break;
            case 'N' :
                $('#purchaseLimitArea').hide();
                $('#purchaseLimitQty').prop('readonly', false);
                $('#purchaseLimitDuration option[value="P"]').show();
                $('#purchaseLimitQty').val(1);
                break;
        }
    },
    /**
     * 예약판매설정여부 컨트롤
     * @param rsvYn
     * @param rsvStartDt
     * @param rsvEndDt
     * @param shipStartDt
     */
    changeRsvYn : function(rsvYn, saleRsvNo, rsvStartDt, rsvEndDt, shipStartDt) {
        if (rsvYn == 'Y') {
            if (saleRsvNo) {
                $('#saleRsvNo').val(saleRsvNo);
                $('#rsvStartDt').val(rsvStartDt);
                $('#rsvEndDt').val(rsvEndDt);
                $('#shipStartDt').val(shipStartDt);
            }
            $('.rsvArea').show();
            $('#rsvStartDt, #rsvEndDt, #shipStartDt').prop('disabled', false);
            itemSetMain.sale.changeGiftYn('Y');
        } else {
            $('.rsvArea').hide();
            $('#rsvStartDt, #rsvEndDt, #shipStartDt').prop('disabled', true);
            itemSetMain.sale.changeGiftYn('N');
        }
    },

    /**
     * 성인상품유형 변경 컨트롤
     * @param obj
     */
    changeAdultType : function(obj) {
        switch ($(obj).val()) {
            case 'NORMAL':
                $('#imgDispYn').val('Y');
                $('#adultTypeArea').hide();
                itemSetMain.sale.changeGiftYn('N');
                itemSetMain.sale.changeItemType($('#itemType'));
                break;
            case 'ADULT':
            case 'LOCAL_LIQUOR':
                $('#imgDispYn').val('N');
                $('#adultTypeArea').show();
                itemSetMain.sale.changeGiftYn('Y');
                break;
        }
    },
    /**
     * 성인 인증 제한 UI적용
     * @param adultLimitYn
     */
    setAdultLimitYn : function(adultLimitYn) {

        if (adultLimitYn == 'Y') {
            //성인카테고리일 경우,
            $('input[name="adultType"]').prop('disabled', true);
            $('input[name="adultType"][value="ADULT"]').prop('disabled', false).trigger('click');

            $('#imgDispYn option[value="Y"]').hide();

        } else {
            $('input[name="adultType"]').prop('disabled', false);
            $('input[name="adultType"][value="NORMAL"]').trigger('click');

            $('#imgDispYn option[value="Y"]').show();
        }
    },
    /**
     * isbn여부
     * @param isbnYn
     */
    setIsbnYn : function (isbnYn) {
        if (isbnYn == 'Y') {
            $('#isbnArea').show();
        } else {
            $('#isbnArea').hide();
            $('#isbn').val('');
        }
    },

    /**
     * 장바구니 제한 UI적용
     * @param cartLimitYn
     */
    setCartLimitYn : function(cartLimitYn) {
        if (cartLimitYn == 'Y') {
            $('input[name="cartLimitYn"][value="Y"]').trigger('click');
            $('input[name="cartLimitYn"][value="N"]').prop('disabled', true);
        } else {
            $('input[name="cartLimitYn"]').prop('disabled', false);
            $('input[name="cartLimitYn"][value="N"]').trigger('click');
        }
    },

    /**
     * 총렌탈료 계산
     */
    changeRental : function() {

        let rentalFee = $('#rentalFee').val();
        let rentalPeriod = $('#rentalPeriod').val();
        let rentalRegFee = $('#rentalRegFee').val();

        if (!$.jUtil.isEmpty(rentalFee) && !$.jUtil.isEmpty(rentalPeriod) && !$.jUtil.isEmpty(rentalRegFee)) {
            $('#totalRentalFee').text($.jUtil.comma((Number(rentalFee) * Number(rentalPeriod)) + Number(rentalRegFee)));
        }
    },
    /**
     * 기준 수수료 조회
     * @param dcateCd
     */
    getBaseCommissionRate : function(dcateCd) {
        if(dcateCd) {
            CommonAjax.basic({
                url: '/partner/getBaseCommissionRate.json',
                data: {dcateCd: dcateCd},
                method: 'GET',
                callbackFunc: function (res) {
                    $('#commissionTitle').text('기준수수료');
                    $('#baseCommissionRate').text(res + '%');
                    $('#commissionType').val('R');
                    $('#commissionRate').val(res);
                }
            });
        }
    },
    /**
     * 할인율 구하기(소수점 2자리)
     *
     */
    getDiscountRate : function (dcPrice, salePrice) {
        return ( 100 - (dcPrice / salePrice ) * 100 ).toFixed(2);
    }
};

itemSetMain.ext = {
    event : function() {
        $('.isCertRadio').on('change', function(){
            itemSetMain.ext.changeCert($(this));
        });

        $('.certNo').on('change keyup', function () {
            itemSetMain.ext.initCheckCert($(this));
        });

        $('#certTable').on('change', '.certType', function() {

            $(this).closest('.form').find('.certNo').val('');

            if ($.jUtil.isEmpty($(this).val())) {
                $(this).closest('.form').find('.certNo').val('').show().prop('placeholder', '');
                $(this).closest('.form').find('.energy').hide();
                $(this).closest('.form').find('.certCheck').show();
                $(this).closest('.form').find('.certNoSubArea').html('');
            } else {
                //select box | placeholder
                if ($(this).find('option:selected').data('ref1') == 'selectbox') {
                    $(this).closest('.form').find('.certNo').hide();
                    $(this).closest('.form').find('.energy').show();
                } else {
                    $(this).siblings('.certNo').prop('placeholder', $(this).find('option:selected').data('ref1'));
                    $(this).closest('.form').find('.certNo').show();
                    $(this).closest('.form').find('.energy').hide();
                }
                //인증번호
                var ref2 = $(this).find('option:selected').data('ref2');

                if ($.jUtil.isEmpty(ref2)) {
                    $(this).closest('.form').find('.certNo').hide();
                    $(this).closest('.form').find('.certNoSubArea').html('');

                } else if(ref2 == '1') {
                    $(this).closest('.form').find('.certNoSubArea').html('');
                    $(this).closest('.form').find('.certNo').show();

                } else if(ref2 == '2') {
                    var ref1Arr = $(this).find('option:selected').data('ref1').split('_');
                    $(this).closest('.form').find('.certNoSubArea').html(
                        '<input type="text" class="inp-base w-325 certNoSub" style="width:300px;" maxlength="25"  placeholder="'+ref1Arr[0]+'"/>'
                        + '<input type="text" class="inp-base mgl5 w-325 certNoSub" style="width:300px;" maxlength="25"  placeholder="'+ref1Arr[1]+'"/>');
                    $(this).closest('.form').find('.certNo').hide();
                } else {
                    $(this).closest('.form').find('.certNoSubArea').html('');
                }
                //인증체크 버튼
                var ref3 = $(this).find('option:selected').data('ref3');

                if (!$.jUtil.isEmpty(ref3)) {
                    $(this).closest('.form').find('.certCheck').hide();
                } else {
                    $(this).closest('.form').find('.certCheck').show();
                }

                $(this).closest('.form').find('.certNo').prop('readonly', false);
            }
            itemSetMain.ext.initCheckCert($(this));
        });

        $('#certTable').on('keyup', '.certNoSubArea', function() {
            let innerHtml = [];
            $('.certNoSubArea').find('.certNoSub').each(function(){
                innerHtml.push($(this).val());
            });

            $(this).siblings('.certNo').val(innerHtml.join('|'));
        });

        $('#certTable').on('change', '.energy', function() {
            if (!$.jUtil.isEmpty($(this).val())) {
                $(this).closest('.form').find('.certNo').val($(this).val());
            }
        });

        $('#certTable').on('click', '.certDelete', function() {
            $(this).closest('.addObj').remove();
        });

        $('.certAdd').on('click', function() {
            if ($(this).closest('td').find('.isCertRadio:checked').val() == 'Y' && $(this).closest('td').find('.certType').length < 20) {
                var addHtml = '<div id="cert_'+$('.addObj').length+'" class="certRow form mgt10 addObj">'
                    + '<input type="hidden" name="certList[].isCert" value="Y" >'
                    + '<input type="hidden" name="certList[].itemCertSeq" class="itemCertSeq">'
                    + '<input type="hidden" name="certList[].certGroup" value="'
                    + $(this).closest('td').find('.certGroup').val() + '">'
                    + $(this).siblings('.addArea').html() + ' <button type="button" class="btn-base type2 certDelete">삭제</button>'
                    + '<input type="hidden" class="certYn" id="cert_${codeDto.mcCd}_certYn" value="N">'
                    + '<span class="fc05 fs12 mgl10 certY" style="display: none;" >인증</span>'
                    + '<span class="fc02 fs12 mgl10 certN" style="display: none">인증실패</span></div>';
                $(this).closest('td').append(addHtml);
            } else{
                alert("인증대상으로 변경 후 추가 가능합니다.");
            }
        });
    },
    changeCert : function(obj) {

        let _tr = obj.closest('tr');
        _tr.find('.isCert').val(obj.val());

        switch (obj.val()) {
            case 'Y':
                _tr.find('.certExemptType').prop('disabled', true).hide();
                _tr.find('.certNo, .certType').prop('disabled', false).prop('readonly', false).val('').show();
                break;
            case 'E':
                _tr.find('.certExemptType, .certNo, .certType').prop('disabled', false).prop('readonly', false).val('').show();
                break;
            case 'N':
                _tr.find('.certExemptType').prop('disabled', true).hide();
                _tr.find('.certNo, .certType').prop('disabled', true).prop('readonly', true).val('');
                _tr.find('.addObj').remove();
                _tr.find('.certNo').prop('placeholder','');
                _tr.find('.certNoSubArea').html('');
                break;
        }
        _tr.find('.certYn').val('N');
        _tr.find('.certY').hide();
        _tr.find('.certN').hide();

    },
    initCheckCert : function (obj) {
        if( typeof obj == 'object') {
            let _row = obj.closest('.certRow');
            _row.find('.certYn').val('N');
            _row.find('.certY').hide();
            _row.find('.certN').hide();
        }
    },
    /**
     * 안전인증 조회
     * @param obj
     * @returns {boolean}
     */
    checkCert : function (obj) {

        let _row = obj.closest('.certRow');
        let certType = _row.find('.certType').val();

        if (!$.jUtil.isEmpty(certType)) {

            let certGroup = obj.data('cert-group');
            let certNo = _row.find('.certNo').val().replace(/^\s*/, "");

            if (!$.jUtil.isEmpty(certGroup) && !$.jUtil.isEmpty(certNo)) {

                CommonAjax.basic({
                    url: "/item/checkCert.json"
                    , data: {
                        certGroup: certGroup,
                        certNo: certNo
                    }
                    , method: "GET"
                    , callbackFunc: function (res) {
                        _row.find('.certYn').val(res.data.returnKey);

                        if (res.data.returnKey == 'Y') {
                            alert('정상조회 되었습니다.');
                            _row.find('.certNo').val(certNo);
                            _row.find('.certY').show();
                            _row.find('.certN').hide();
                        } else {
                            alert("조회에 실패하였습니다. ");
                            _row.find('.certY').hide();
                            _row.find('.certN').show();
                        }
                    },
                    error: function (e) {
                        if (e.responseJSON.errors[0].detail != null) {
                            alert(e.responseJSON.errors[0].detail);
                        } else {
                            alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                        }
                    }
                });
            } else {
                alert("인증번호를 확인해주세요. ");
            }

        } else {
            alert("인증유형을 선택해주세요");
            return false;
        }
    },

};

itemSetMain.sub = {
    dcateCartYn : 'N',
    changeCate4 : function(obj) {
        // 세카테고리 변경 시
        var dcateCd = '';
        //var imgDispYn = 'N';

        if (typeof obj == 'undefined') {
            dcateCd = $('#cateCd4').val();
        } else {
            dcateCd = $(obj).val();
        }

        if (dcateCd) {
            CommonAjax.basic({
                url: '/common/category/getCategoryDivide.json',
                data: {dcateCd: dcateCd},
                method: 'GET',
                callbackFunc: function (res) {
                    //장바구니 제한
                    itemSetMain.sub.dcateCartYn = res.cartYn;
                    if (res.cartYn == 'Y' && ($('#itemType').val() != 'T' && $('#itemType').val() != 'M')) {
                        //렌탈상품, 휴대폰-가입유형 장바구니제한 처리
                        itemSetMain.sale.setCartLimitYn(res.cartYn);
                    }
                    //성인인증 제한
                    itemSetMain.sale.setAdultLimitYn(res.adultLimitYn);

                    //도서
                    itemSetMain.sale.setIsbnYn(res.isbnYn);

                    //기준수수료 적용
                    itemSetMain.sale.getBaseCommissionRate(dcateCd);
                    itemSetMain.basic.getFullCateNm()
                }
            });
        }
    },
};

itemSetMain.attr  = {
    init : function() {
        itemSetMain.attr.initAttr();
        itemSetMain.attr.initAttrTag();
    },
    initAttr : function() {
        $('#attrArea').html('');
        $('#attrArea').closest('table').hide();
        $('#attrDiv').hide();
    },
    initAttrTag : function() {
        $('#selectedTags').html('');
    },
    event : function() {
        $('#schTagTitleArea').on('click', '.tag-list li', function() {
            itemSetMain.attr.setAttrTag($(this).closest('ul').prop('id'), $(this).data('attr-no'), $(this).text(), $(this).closest('ul').data('multi-yn'));
        });

        $('#selectedTags').on('click', '.btn-close', function() {
            $(this).closest('.tag').remove();
        });

        $('#brandDelBtn').on('click', function() {
            ;
        });

        $('#marketDelBtn').on('click', '.btn-close', function() {
            itemSetMain.attr.delMakerHtml();
        });

        $('[id^=uploadProofFileSpan_]').on('click', '.close', function() {
            if (confirm('등록된 파일을 삭제하시겠습니까?')) {
                $(this).closest('.proofFile').remove();
            }
        });

        $('#cateCd3').on('change', function() {
            itemSetMain.attr.getAttrList("ATTR", '', $(this).val());
        });

        $('#gnoticeNo').on('change', function() {
            itemSetMain.attr.getMngNoticeList($(this).val());
        });

        $('#addNoticeArea').on('keyup', 'textarea', function () {
            let idArr = $(this).attr('id').split('_');
            $(this).calcTextLength('keyup', "#noticeNmCnt"+idArr[1] );
        });

        $('#addNoticeArea').on('change', '.checkComment', function() {
            let idArr = $(this).attr('id').split('_');
            if ($(this).is(':checked')) {
                $('#noticeDesc_'+idArr[1]).val($(this).data('comment')).setTextLength("#noticeNmCnt"+idArr[1]);
            }else {
                $('#noticeDesc_'+idArr[1]).val('');
                $("#noticeNmCnt"+idArr[1]).z;
            }
        });

        $('#checkboxAllNoticeExp').on('change', function() {
            let checked = $(this).is(':checked');

            $('#addNoticeArea').find('input:checkbox').each(function(){
                if (checked == true && !$(this).is(':checked')) {
                    $(this).trigger('click');
                } else if (checked == false && $(this).is(':checked')) {
                    $(this).trigger('click');
                }
            });
        });
    },
    /**
     * 속성 리스트 조회 및 UI셋팅 (상품속성/검색태그)
     * @param gattrType
     * @param itemNo
     * @param scateCd
     * @returns {boolean}
     */
    getAttrList : function (gattrType, itemNo, scateCd) {

        if (gattrType == "ATTR" && $.jUtil.isEmpty(scateCd)) {
            itemSetMain.attr.initAttr();
            return false;
        }

        CommonAjax.basic({
            url: '/item/attribute/getAttributeItemList.json',
            data: {
                gattrType: gattrType,
                itemNo: itemNo,
                scateCd: scateCd
            },
            method: 'GET',
            callbackFunc: function (res) {

                let idx = 0;
                let selectId = 0;
                let gattrNo = 0;
                let checkLast = 0;
                let row = res.length;

                let innerHtml = "";
                let checkHtml = "";
                let optionHtml = "";

                let selected = '';
                let checked = '';

                if (gattrType == "ATTR") {

                    //속성
                    $('#attrArea').html('');
                    $('#attrDiv').show();
                    $('#attrArea').closest('table').show();

                    if (row == 0) {
                        itemSetMain.attr.initAttr();
                        return false;
                    }


                    for (let i in res) {

                        if (res[i].useYn == 'Y') {
                            selected = 'selected';
                            checked = 'checked';
                        } else {
                            selected = '';
                            checked = '';
                        }

                        checkLast++;
                        if (res[i].multiYn == 'Y') { //checkBox 생성

                            if (gattrNo != res[i].gattrNo) {
                                gattrNo = res[i].gattrNo;

                                innerHtml = '<tr id="attrTr'+idx+'">'
                                    + '<th scope="row">' + res[i].gattrNm + '</th>'
                                    + '<td id="attrSelect'+idx+'">'
                                    + '<div class="chk-group2 checkArea'+idx+'"></div>'
                                    + '</td></tr>';

                                $('#attrArea').append(innerHtml);
                                innerHtml ='';

                                if (idx > 0 ) {
                                    $('.checkArea'+(idx-1)).append(checkHtml);
                                    checkHtml ='';
                                }
                                idx++;
                            }

                            checkHtml += '<input type="checkbox"  id = "attr'+res[i].attrNo+'" name="attrNo"  value="'+res[i].attrNo+'" '+checked+'>'
                                + '<label for="attr'+res[i].attrNo+'" class="lb-check"> '+ res[i].attrNm+' </label>';

                            //마지막일 경우
                            if( row == checkLast ) {
                                $('.checkArea'+(idx-1)).append(checkHtml);
                                checkHtml ='';
                            }

                        } else {
                            //select box
                            if (gattrNo != res[i].gattrNo) {
                                gattrNo = res[i].gattrNo;

                                innerHtml =   '<tr id="attrTr'+idx+'">'
                                    +'<th scope="row">' + res[i].gattrNm + '</th>'
                                    + '<td>'
                                    + '<select name="attrNo" id="attrSelect'+selectId+'" class="slc-base">'
                                    + '<option value="">선택</option>'
                                    + '</select>'
                                    + '</td>';

                                $('#attrArea').append(innerHtml);

                                innerHtml ='';
                                optionHtml ='';
                                selectId++;
                                idx++;
                            }
                            optionHtml = '<option value="' + res[i].attrNo + '" '+selected+'>' + res[i].attrNm + '</option>';
                            $('#attrSelect'+(selectId-1)).append(optionHtml);
                        }

                    }

                } else {
                    //검색태그
                    $('#schTagTitleArea, #schTagDetailArea, #selectedTags').html('');

                    for (let i in res) {
                        if ($.jUtil.isEmpty(res[i].attrNm)) {
                            continue;
                        }

                        if (gattrNo != res[i].gattrNo) {
                            gattrNo = res[i].gattrNo;
                            checkLast++;

                            innerHtml = '<div>'
                                + '<p class="tag-tit">' + res[i].gattrNm + '</p>'
                                +'<ul id="gattrNo'+res[i].gattrNo+'" class="tag-list" data-multi-yn="'+res[i].multiYn+'">'
                                + '</ul>'
                                + '</div>' ;

                            $('#schTagTitleArea').append(innerHtml);
                        }

                        innerHtml = '<li data-attr-no="' + res[i].attrNo + '"> <button type="button"><i>' + res[i].attrNm + '</i></button></li>';
                        $('#gattrNo'+res[i].gattrNo).append(innerHtml);

                        if (res[i].useYn == 'Y') {
                            itemSetMain.attr.setAttrTag('gattrNo'+res[i].gattrNo, res[i].attrNo, res[i].attrNm, res[i].multiYn);
                        }

                    }

                    if (checkLast < 8) {
                        let tableWidth = 100 / checkLast;

                        $('#schTagArea table').css('width', tableWidth + '%');
                    }
                }
            }
        });
    },
    /**
     * 속성리스트 추출 (상품속성/검색태그)
     * @returns {[]}
     */
    getAttrNoList : function() {
        let attrNoList = [];

        $("input[name='attrNo']:checked, select[name='attrNo'], [id^='selectedgattrNo']").each(function() {
            if (!$.jUtil.isEmpty($(this).val())) {
                attrNoList.push($(this).val());
            } else if (!$.jUtil.isEmpty($(this).data('attr-no'))) {
                attrNoList.push($(this).data('attr-no'));
            }
        });

        return attrNoList;
    },
    /**
     * 검색태그 설정 (선택한 태그)
     * @param gattrNoId
     * @param attrNo
     * @param attrTxt
     */
    setAttrTag : function(gattrNoId, attrNo, attrTxt, multiYn) {
        let selectedId = 'selected'+gattrNoId;

        if (multiYn == 'Y') {
            //멀티로 설정되어 있는 경우만 2개 이상 선택 가능
            selectedId += attrNo;
        }

        $('#'+selectedId).remove();
        $('#selectedTags').append('<span class="tag mgl5" id="' + selectedId +'" data-attr-no="'+attrNo+'"> # '+attrTxt+'('+attrNo+') <button type="button" class="btn-close"><i class="hide">태그삭제</i></button></span>');
    },
    /**
     * 브랜드 조회
     * @returns {boolean}
     */
    getBrand : function() {

        let brandNm = $('#brandNm').val();

        if (0 == brandNm.length) {
            alert('검색 키워드는 1자 이상 입력해주세요.');
            $('#brandNm').focus();
            return false;

        } else if ($('#brandNm').val() && $.jUtil.isNotAllowInput($('#brandNm').val(), ['SPC_SCH'])) {
            alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
            $.jUtil.patternReplace($('#brandNm'), ['SPC_SCH'], "");
            $('#brandNm').focus();
            return false;
        }

        CommonAjax.basic({url:'/item/brand/getBrandList.json'
            , data:{searchType:'brandNm'
                , searchKeyword:brandNm
                , callType:"P"}
            , method:'GET'
            , callbackFunc:function(res) {
                var resultCount = res.length;

                $('#searchBrandLayer').html('');

                if (resultCount == 0) {
                    $('#searchBrandLayer').append('<li class="no-result">검색결과가 없습니다.</li>');
                } else {

                    for (let idx = 0; resultCount > idx; idx++) {

                        let data = res[idx];
                        let brandInfo = new Array();

                        brandInfo.push(data.brandNm);
                        if (data.brandNmEng) {
                            brandInfo.push(data.brandNmEng);
                        }

                        brandInfo.push(data.brandNo);

                        let value =  brandInfo.join(' | ');

                        let _searchVal = $.jUtil.isEmpty(data.brandNmEng) ? data.brandNm : data.brandNm + '( ' + data.brandNmEng +' )';
                        let searchVal = itemCommon.util.replaceAll(_searchVal, brandNm,'<i>'+brandNm+'</i>');

                        $('#searchBrandLayer').append(
                            '<li id="brandSearchList_'+data.brandNo+'" value="'+value+'"><button type="button">'+searchVal+'</button></li>');
                    }
                    itemSetMain.attr.addBrand();
                }
                $('.searchBrandLayer').show();
            }
        });
    },

    /**
     * 브랜드 추가
     */
    addBrand : function() {
        $('li[id^=brandSearchList_]').click(function() {
            let idArr = $(this).attr('id').split('_');
            let htmlArr = $(this).attr('value').split('|');
            let brandNo = idArr[1];
            let brandNm = htmlArr[0];

            itemSetMain.attr.addBrandHtml(brandNo, $.trim(brandNm));
            $('.searchBrandLayer').hide();
        });
    },
    /**
     * brand html 생성
     * @param brandNo
     * @param brandNm
     */
    addBrandHtml : function(brandNo, brandNm) {
        if( !$.jUtil.isEmpty(brandNo)){
            $('#brandNo').val(brandNo);
            $('#brandNm').val(brandNm);
            var brandHtml = brandNm + ' <button type="button" class="btn-close" onclick="itemSetMain.attr.delBrandHtml();"><i class="hide">삭제</i></button>' ;
            $('#spanBrandNm').html(brandHtml);
        }else{
            itemSetMain.attr.delBrandHtml();
        }
    },
    /**
     * 브랜드 삭제
     */
    delBrandHtml : function() {
        $('#brandNo').val('');
        $('#brandNm').val('');
        $('#spanBrandNm').html('');
    },
    /**
     * 제조사 조회
     * @returns {boolean}
     */
    getMaker : function() {
        let makerNm = $('#makerNm').val();

        if (0 == makerNm.length) {
            alert('검색 키워드는 1자 이상 입력해주세요.');
            $('#makerNm').focus();
            return false;
        } else if ($('#makerNm').val() && $.jUtil.isNotAllowInput($('#makerNm').val(), ['SPC_SCH'])) {
            alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
            $.jUtil.patternReplace($('#makerNm'), ['SPC_SCH'], "");
            $('#makerNm').focus();
            return false;
        }

        CommonAjax.basic({url:'/item/maker/getMakerList.json'
            , data:{searchType:'makerNm'
                , searchKeyword:makerNm
                , callType:"P"}
            , method:'GET'
            , callbackFunc:function(res) {
                let resultCount = res.length;

                $('#searchMakerLayer').html('');
                if (resultCount == 0) {
                    $('#searchMakerLayer').append('<li class="no-result">검색 결과가 없습니다.</li>');
                } else {

                    for (let idx = 0; resultCount > idx; idx++) {
                        let data = res[idx];
                        let makeInfo = new Array();
                        makeInfo.push(data.makerNm);
                        if (data.makerNmEng) {
                            makeInfo.push(data.makerNmEng);
                        }
                        makeInfo.push(data.makerNo);

                        var value = makeInfo.join(' | ');
                        var searchVal = itemCommon.util.replaceAll(data.makerNm, makerNm,'<i>'+makerNm+'</i>' );

                        $('#searchMakerLayer').append(
                            '<li id="makerSearchList_'+data.makerNo+'" value="'+value+'"><button type="button">'+searchVal+'</button></li>');
                    }
                    itemSetMain.attr.addMaker();
                }
                $('.searchMakerLayer').show();
            }
        });
    },
    /**
     * 제조사 추가
     */
    addMaker : function() {
        $('li[id^=makerSearchList_]').click(function() {
            let idArr = $(this).attr('id').split('_');
            let htmlArr = $(this).attr('value').split('|');
            let makerNo = idArr[1];
            let makerNm = htmlArr[0];
            itemSetMain.attr.addMakerHtml(makerNo, $.trim(makerNm));
            $('.searchMakerLayer').hide();
        });
    },
    /**
     * 제조사 html 생성
     * @param makerNo
     * @param makerNm
     */
    addMakerHtml : function(makerNo, makerNm) {
        if( !$.jUtil.isEmpty(makerNo)){
            $('#makerNo').val(makerNo);
            $('#makerNm').val(makerNm);
            var makerHtml = makerNm + ' <button type="button" class="btn-close" onclick="itemSetMain.attr.delMakerHtml();"><i class="hide">삭제</i></button>' ;
            $('#spanMakerNm').html(makerHtml);
        }else{
            itemSetMain.attr.delMakerHtml();
        }
    },
    /**
     * 선택한 제조사 삭제
     */
    delMakerHtml : function() {
        $('#makerNo').val('');
        $('#makerNm').val('');
        $('#spanMakerNm').html('');
    },
    /**
     * 고시 그룹 리스트 조회
     */
    getMngNoticeGroupList : function() {
        CommonAjax.basic({
            url: '/item/getMngNoticeGroupList.json',
            data: {
            },
            method: 'GET',
            callbackFunc: function (res) {
                for (let i in res) {
                    $('#gnoticeNo').append('<option value="'+res[i].gnoticeNo+'">' + res[i].gnoticeNm + '</option>');
                }
            }
        });
    },
    /**
     * 고시 항목 조회
     * @param gnoticeNo
     * @param noticeList
     */
    getMngNoticeList : function(gnoticeNo, noticeList) {
        $('#addNoticeArea').html('');

        if (gnoticeNo) {
            CommonAjax.basic({
                url: '/item/getMngNoticeList.json',
                data: {
                    gnoticeNo : gnoticeNo
                },
                method: 'GET',
                callbackFunc: function (res) {
                    $('#addNoticeArea').show();
                    let id = 0;

                    for (let i in res) {
                        let innerHtml = '';
                        id++;
                        innerHtml =
                            '<div class="mgt15 notice'+id+'">'
                            + '<span class="lb-block fc03">'+res[i].noticeNm+'<span class="txt fc04 mgl10">'+res[i].noticeDetail+'</span></span>'
                            + '<div class="words-wrap">'
                            + '<input type="hidden" name="noticeList[].noticeNo" value="'+res[i].noticeNo+'" >'
                            + '<textarea class="textarea noticeDesc" name="noticeList[].noticeDesc" id="noticeDesc_'+res[i].noticeNo+'" maxlength="500" placeholder="세부사항을 입력해주세요."></textarea>'
                            + '<span class="words"><i><span class="noticeNmCnt" id="noticeNmCnt'+res[i].noticeNo+'">0</span></i>/500자</span>';

                        if( !$.jUtil.isEmpty(res[i].commentTitle) && !$.jUtil.isEmpty(res[i].commentNm) ){
                                innerHtml += '<div class="mgt10"><input type="checkbox" id="Mng_'+res[i].noticeNo+'" data-comment="'+res[i].commentNm+'" name="Mng'+res[i].noticeNo+'" class="checkComment" value="Y">'
                            + '<label for="Mng_'+res[i].noticeNo+'"class="lb-check">'+res[i].commentTitle+'</label></div>'
                            }

                            innerHtml += '</div></div>'
                        ;

                        $('#addNoticeArea').append(innerHtml);
                    }

                    if (noticeList) {
                        for (let i in noticeList) {
                            $('#noticeDesc_' + noticeList[i].noticeNo).val(noticeList[i].noticeDesc);
                            if( !$.jUtil.isEmpty( $('#noticeDesc_' + noticeList[i].noticeNo).val())) {
                                $('#noticeDesc_' + noticeList[i].noticeNo).setTextLength('#noticeNmCnt' + noticeList[i].noticeNo);
                            }
                        }
                    }
                }
            });
        } else {
            $('#addNoticeArea').hide();
        }
    }
};

