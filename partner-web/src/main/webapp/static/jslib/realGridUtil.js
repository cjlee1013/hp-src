//그리드 row 이동 _ctrl (firstTop: 맨위로, top: 위로, bottom: 아래로, lastBottom: 맨아래로)
function realGridMoveRow(_gridObj, _ctrl, rowArr) {
    const lastRowId = _gridObj.gridView.getItemCount() - 1;

    switch (_ctrl) {
        case "firstTop" : //맨위로 이동
            for (let i in rowArr.reverse()) {
                let dfNum = 0;

                _gridObj.dataProvider.moveRow(rowArr[i] + parseInt(i) + dfNum, dfNum);

                if (_gridObj.gridView.isCheckedRow(i)) {
                    dfNum++;
                }
            }

            _gridObj.gridView.checkRows(rowArr, false);

            for (let i in rowArr) {
                _gridObj.gridView.checkRow(i);
            }
            break;
        case "top" : //위로 이동
            for (let i in rowArr) {
                if(rowArr[i] > 0 && !_gridObj.gridView.isCheckedRow(rowArr[i]-1)) {
                    _gridObj.dataProvider.moveRow(rowArr[i], rowArr[i]-1);
                    _gridObj.gridView.checkRow(rowArr[i]-1);
                    _gridObj.gridView.checkRow(rowArr[i], false);
                }
            }
            break;
        case "bottom" : //아래로 이동
            for (let i in rowArr.reverse()) {
                if(rowArr[i] < lastRowId && !_gridObj.gridView.isCheckedRow(rowArr[i]+1)) {
                    _gridObj.dataProvider.moveRow(rowArr[i], rowArr[i]+1);
                    _gridObj.gridView.checkRow(rowArr[i]+1);
                    _gridObj.gridView.checkRow(rowArr[i], false);
                }
            }
            break;
        case "lastBottom" : //맨아래로 이동
            for (let i in rowArr) {
                let dfNum = 0;

                _gridObj.dataProvider.moveRow(rowArr[i] - parseInt(i) - dfNum, lastRowId);

                if (_gridObj.gridView.isCheckedRow(i)) {
                    dfNum++;
                }
            }

            _gridObj.gridView.checkRows(rowArr, false);

            let checkIdx = lastRowId;
            for (let i in rowArr) {
                _gridObj.gridView.checkRow(checkIdx--);
            }

            break;
    }
}

/**
 * RealGrid 컬럼에 Lookup Option 추가 - select box version
 * - Lookup 이란? 컬럼에 연결된 데이터 필드의 실제 값 대신 그 값과 연관된 다른 값을 셀에 표시 하는 Option.
 * - 코드값으로 저장된 데이터를 그리드에 노출 시 코드명으로 표시 할 수 있음.
 * - but, 해당 코드값을 가지고 있는 select box가 존재해야 함.
 *
 * @param _gridObj : {} 그리드 객체
 * @param _columnNm : string
 * @param _codeObj : select box
 */
function setColumnLookupOption(_gridObj, _columnNm, _codeObj) {
    let values = new Array();
    let labels = new Array();

    _codeObj.find("option").each(function () {
        values.push(this.value);
        labels.push(this.text);
    });

    if (values.length > 0 && labels.length > 0) {
        let columnLookup = _gridObj.columnByName(_columnNm);
        columnLookup.values = values;
        columnLookup.labels = labels;
        columnLookup.lookupDisplay = true;

        _gridObj.setColumn(columnLookup);
    }
}

/**
 * RealGrid 컬럼에 Lookup Option 추가 2 - jsonObject Data version
 * @param _gridObj
 * @param _columnNm
 * @param _jsonData
 */
function setColumnLookupOptionForJson(_gridObj, _columnNm, _jsonData) {
    let values = new Array();
    let labels = new Array();

    $.each(_jsonData, function(key, code) {
        values.push(code.mcCd);
        labels.push(code.mcNm);
    });

    if (values.length > 0 && labels.length > 0) {
        let columnLookup = _gridObj.columnByName(_columnNm);
        columnLookup.values = values;
        columnLookup.labels = labels;
        columnLookup.lookupDisplay = true;

        _gridObj.setColumn(columnLookup);
    }
}