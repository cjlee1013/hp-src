<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<section class="contents">
    <div class="float">
        <h2 class="h2 float-l">부가세 신고내역</h2>
        <div class="float-r">
            <button type="button" class="btn-ico-re"><i>새로고침</i></button>
        </div>
    </div>

    <div class="srh-form">
        <form name="searchForm" onsubmit="return false;">
            <table>
                <caption>부가세 신고내역</caption>
                <colgroup>
                    <col style="width:80px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row"><label for="srhForm01">검색 기간</label></th>
                    <td>
                        <select class="slc-base w-110" id="startYear" name="startYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'startMonth')">
                            <c:forEach var="years" items="${getYear}" varStatus="status">
                                <option value="${years.dataValue}">${years.dataStr}</option>
                            </c:forEach>
                        </select>
                        <select class="slc-base w-110" id="startMonth" name="startMonth" style="width: 100px;">
                            <c:forEach var="months" items="${getMonth}" varStatus="status">
                                <option value="${months.dataValue}">${months.dataStr}</option>
                            </c:forEach>
                        </select>
                        <span class="text">~</span>
                        <select class="slc-base w-110"id="endYear" name="endYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'endMonth')">
                            <c:forEach var="years" items="${getYear}" varStatus="status">
                                <option value="${years.dataValue}">${years.dataStr}</option>
                            </c:forEach>
                        </select>
                        <select class="slc-base w-110" id="endMonth" name="endMonth" style="width: 100px;">
                            <c:forEach var="months" items="${getMonth}" varStatus="status">
                                <option value="${months.dataValue}">${months.dataStr}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="btn-wrap">
                <button type="button" id="searchBtn" class="btn-base-l type4">검색</button>
                <button type="button" id="resetBtn" class="btn-base-l type3">초기화</button>
            </div>
        </form>
    </div>

    <div class="result-wrap float mgt20">
        <p class="float-l result-p">* 부가세 신고 내역은 파트너님의 편의를 위해 제공되는 자료로, 작성하는 자료와 차이가 있을 수 있으니 참고자료로 활용 부탁드립니다.</p><br /><br/>
        <p class="float-l result-p">* 매출금액 = 상품금액 – 업체할인 + 배송비 + 배송비할인 = 신용카드 + 현금영수증 합계 + 기타 + 홈플러스할인</p><br /><br/>
        <p class="float-l result-p">검색 결과 <b><span id="searchCnt">0</span></b>건</p>
        <div class="float-r">
            <button type="button" class="btn-ico-exel" id="excelDownloadBtn"><i>엑셀 다운로드</i></button>
            <select class="slc-base w-160" title="정렬선택" id="pageViewCnt">
                <option value="100" selected>100개씩 보기</option>
                <option value="200">200개씩 보기</option>
                <option value="300">300개씩 보기</option>
                <option value="500">500개씩 보기</option>
                <option value="1000">1000개씩 보기</option>
            </select>
        </div>
    </div>

    <div class="data-wrap" style="height: 400px" id="partnerSalesVATGrid" style="height: 400px;"></div>
    <div id="pagingArea" class="paging"></div>

    <div class="data-wrap" style="height: 400px;display:none;" id="partnerSalesOrderGrid"></div>
    <div class="data-wrap" id="adjustListGrid" style="display: none"></div>
</section>
<!-- script -->
<script type="text/javascript" src="/static/js/settle/partnerSalesVAT.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/sell/sellUtil.js?${fileVersion}"></script>
<script>
    ${salesVATListBaseInfo}
    ${salesVATOrderBaseInfo}
    ${adjustListBaseInfo}
</script>