<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<section class="contents">
    <div class="float">
        <h2 class="h2 float-l">세금계산서</h2>
        <div class="float-r">
            <button type="button" class="btn-ico-re"><i>새로고침</i></button>
        </div>
    </div>

    <div class="srh-form">
        <form name="searchForm" onsubmit="return false;">
            <table>
                <caption>세금계산서</caption>
                <colgroup>
                    <col style="width:80px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row"><label for="srhForm01">검색 기간</label></th>
                    <td>
                        <select class="slc-base w-110" id="startYear" name="startYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'startMonth')">
                            <c:forEach var="years" items="${getYear}" varStatus="status">
                                <option value="${years.dataValue}">${years.dataStr}</option>
                            </c:forEach>
                        </select>
                        <select class="slc-base w-110" id="startMonth" name="startMonth" style="width: 100px;">
                            <c:forEach var="months" items="${getMonth}" varStatus="status">
                                <option value="${months.dataValue}">${months.dataStr}</option>
                            </c:forEach>
                        </select>
                        <span class="text">~</span>
                        <select class="slc-base w-110"id="endYear" name="endYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'endMonth')">
                            <c:forEach var="years" items="${getYear}" varStatus="status">
                                <option value="${years.dataValue}">${years.dataStr}</option>
                            </c:forEach>
                        </select>
                        <select class="slc-base w-110" id="endMonth" name="endMonth" style="width: 100px;">
                            <c:forEach var="months" items="${getMonth}" varStatus="status">
                                <option value="${months.dataValue}">${months.dataStr}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="btn-wrap">
                <button type="button" id="searchBtn" class="btn-base-l type4">검색</button>
                <button type="button" id="resetBtn" class="btn-base-l type3">초기화</button>
            </div>
        </form>
    </div>

    <div class="result-wrap float mgt20">
        <p class="float-l result-p">* 전자 세금계산서 상 상호명은 실제 상호명+ “__1” 이 포함되어 발행될 수 있습니다.</p><br /><br/>
        <p class="float-l result-p">검색 결과 <b><span id="searchCnt">0</span></b>건</p>
        <div class="float-r">
            <button type="button" class="btn-ico-exel" id="excelDownloadBtn"><i>엑셀 다운로드</i></button>
            <select class="slc-base w-160" title="정렬선택" id="pageViewCnt">
                <option value="100" selected>100개씩 보기</option>
                <option value="200">200개씩 보기</option>
                <option value="300">300개씩 보기</option>
                <option value="500">500개씩 보기</option>
                <option value="1000">1000개씩 보기</option>
            </select>
        </div>
    </div>

    <div class="data-wrap" style="height: 400px" id="partnerTaxBillGrid" style="height: 400px;"></div>
    <div id="pagingArea" class="paging"></div>
</section>
<!-- script -->
<script type="text/javascript" src="/static/js/settle/partnerTaxBillList.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/sell/sellUtil.js?${fileVersion}"></script>
<script>
    ${partnerTaxBillBaseInfo}
</script>