<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<section class="contents">
    <div class="float">
        <h2 class="h2 float-l">정산현황</h2>
        <div class="float-r">
            <button type="button" class="btn-ico-re"><i>새로고침</i></button>
        </div>
    </div>

    <div class="srh-form">
        <form name="searchForm" onsubmit="return false;">
            <table>
                <caption>정산현황</caption>
                <colgroup>
                    <col style="width:80px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row"><label for="srhForm01">검색 기간</label></th>
                    <td>
                        <span class="inp-date"><input type="text" class="inp-base" id="schStartDt" title="검색기간 시작일"></span> -
                        <span class="inp-date"><input type="text" class="inp-base" id="schEndDt" title="검색기간 종료일"></span>
                        <span class="radio-group mgl5">
                            <input type="radio" id="ex-radio3-1" name="ex-radio3" onclick="partnerSettleListForm.initSearchDate('-1d');" checked><label for="ex-radio3-1"  class="lb-radio">어제</label>
                            <input type="radio" id="ex-radio3-2" name="ex-radio3" onclick="partnerSettleListForm.initSearchDate('-7d');"><label for="ex-radio3-2"  class="lb-radio">1주일</label>
                            <input type="radio" id="ex-radio3-3" name="ex-radio3" onclick="partnerSettleListForm.initSearchDate('-30d');"><label for="ex-radio3-3"  class="lb-radio">1개월</label>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="srhForm02">지급상태</label></th>
                    <td>
                        <select id="settlePayState" name="settlePayState" class="slc-base w-160">
                            <option value="">전체</option>
                            <c:forEach var="codeDto" items="${settlePayState}" varStatus="status">
                                <option value="${codeDto.code}">${codeDto.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="srhForm02">정산상태</label></th>
                    <td>
                        <select id="settleState" name="settleState" class="slc-base w-160">
                            <option value="">전체</option>
                            <c:forEach var="codeDto" items="${settleState}" varStatus="status">
                                <option value="${codeDto.code}">${codeDto.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="btn-wrap">
                <button type="button" id="searchBtn" class="btn-base-l type4">검색</button>
                <button type="button" id="resetBtn" class="btn-base-l type3">초기화</button>
            </div>
        </form>
    </div>

    <div class="result-wrap float mgt20">
        <p class="float-l result-p">* 매월 1일 집계하여 지급예정일 기준으로 제공됩니다.</p><br /><br/>
        <p class="float-l result-p">검색 결과 <b><span id="searchCnt">0</span></b>건</p>
        <div class="float-r">
            <button type="button" class="btn-ico-exel" id="excelDownloadBtn"><i>엑셀 다운로드</i></button>
            <select class="slc-base w-160" title="정렬선택" id="pageViewCnt">
                <option value="100" selected>100개씩 보기</option>
                <option value="200">200개씩 보기</option>
                <option value="300">300개씩 보기</option>
                <option value="500">500개씩 보기</option>
                <option value="1000">1000개씩 보기</option>
            </select>
        </div>
    </div>

    <div class="data-wrap" id="partnerSettleListGrid" style="height: 400px;"></div>
    <div id="pagingArea" class="paging"></div>

    <div class="data-wrap" style="height: 400px;display:none" id="partnerSettleInfoGrid" style="height: 400px;"></div>
</section>
<!-- script -->
<script type="text/javascript" src="/static/js/settle/partnerSettleList.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/sell/sellUtil.js?${fileVersion}"></script>
<script>
    ${partnerSettleBaseInfo}
</script>