<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<jsp:include page="/WEB-INF/views/pages/core/includeDhtmlx.jsp"/>

<section class="contents" id="salesTrendMain">
    <div class="float">
        <h2 class="h2 float-l">판매트렌드</h2>
        <div class="float-r">
            <%--<button type="button" class="btn-ico-re" id="boardRefresh"><i>새로고침</i></button>--%>
        </div>
    </div>
    <div class="srh-form">
        <form id="salesTrendSearchForm" name="salesTrendSearchForm" onsubmit="return false;">
            <table>
                <caption>조회</caption>
                <colgroup>
                    <col style="width:75px">
                    <col style="width:392px">
                    <col style="width:80px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row"><label for="searchType">검색기간</label></th>
                    <td colspan="5">
                        <select id="searchType" name="searchType" class="slc-base w-120">
                            <option value="sales">판매기간</option>
                        </select>
                        <span>&nbsp;</span>
                        <span>&nbsp;</span>
                        <span class="inp-date">
                            <label for="startDate"></label>
                            <input id="startDate" name="startDate" type="text" class="inp-base" placeholder="시작일" maxlength="10">
                        </span> -
                        <span class="inp-date">
                            <label for="endDate"></label>
                            <input id="endDate" name="endDate" type="text" class="inp-base" placeholder="종료일" maxlength="10">
                        </span>
                        <span class="radio-group mgl5">
                            <input type="radio" id="set7dBtn" name="searchDateBtn" checked>
                            <label for="set7dBtn" class="lb-radio">최근 7일</label>
                            <input type="radio" id="set3mBtn" name="searchDateBtn">
                            <label for="set3mBtn" class="lb-radio mgl10">최근 3개월</label>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="btn-wrap">
                <button type="button" id="searchBtn" class="btn-base-l type4" onclick="salesTrendMain.search();">검색</button>
                <button type="button" id="resetBtn" class="btn-base-l type3" onclick="salesTrendMain.reset();">초기화</button>
            </div>
        </form>
    </div>
    <!-- summary board -->
    <div class="box-manage line2" id="summaryBoard">
        <div style="width: 24%!important;">
            <dl>
                <dt style="padding: inherit">상품 조회 수</dt>
                <dd><span id="eventCnt" style="font-size: 24px;font-weight: bold;">0</span> 건</dd>
            </dl>
        </div>
        <div style="width: 24%!important;">
            <dl>
                <dt style="padding: inherit">상품 결제건 수</dt>
                <dd><span id="paymentCnt" style="font-size: 24px;font-weight: bold;">0</span> 건</dd>
            </dl>
        </div>
        <div style="width: 24%!important;">
            <dl>
                <dt style="padding: inherit">전환율</dt>
                <dd><span id="conversionRate" style="font-size: 24px;font-weight: bold;">0</span> %</dd>
            </dl>
        </div>
        <div style="width: 27%!important;">
            <dl>
                <dt style="padding: inherit">상품 판매 금액</dt>
                <dd><span id="saleAmt" style="font-size: 24px;font-weight: bold;">0</span> 원</dd>
            </dl>
        </div>
    </div>

    <div style="margin-top: 50px">

    </div>

    <!-- line chart -->
    <div class="result-wrap float mgt20" style="margin-bottom: 100px;">
        <span style="font-size: 20px; font-weight: bold;"><strong>상품 조회 및 결제건 수 추세</strong></span>
        <div class="result-wrap float mgt20" style="width:100%; height:500px;">
            <canvas id="lineChart" style="width:inherit; height:inherit;"></canvas>
        </div>

        <div style="margin-top: 50px"></div>

        <span style="font-size: 20px; font-weight: bold;"><strong>조회 차트 단독</strong></span>
        <div class="result-wrap float mgt20" style="width:100%; height:250px;">
            <canvas id="eventChart" style="width:inherit; height:inherit;"></canvas>
        </div>

        <div style="margin-top: 50px"></div>

        <span style="font-size: 20px; font-weight: bold;"><strong>결제건 차트 단독</strong></span>
        <div class="result-wrap float mgt20" style="width:100%; height:250px;">
            <canvas id="paymentChart" style="width:inherit; height:inherit;"></canvas>
        </div>
    </div>

    <!-- pie chart -->
    <div class="result-wrap float mgt20" style="margin-bottom: 100px;">
        <div style="width:50%; height: fit-content; float: left; text-align: center">
            <span style="font-size: 20px; font-weight: bold;">
                <strong style="text-align: center;">연령별 판매비중</strong>
            </span>
            <canvas id="agePieChart" style="height: inherit; margin-top: 30px;"></canvas>
        </div>
        <div style="width:50%; height: fit-content; float: right; text-align: center">
            <span style="font-size: 20px; font-weight: bold;">
                <strong>성별 판매비중</strong>
            </span>
            <canvas id="genderPieChart" style="height: inherit; margin-top: 30px;"></canvas>
        </div>
    </div>
</section>

<div class="modal-wrap">
</div>

<script>
    const sellerId = '${sellerId}';
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/salesTrendMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>

<%--<script type="text/javascript" src="/static/jslib/chartjs-2.9.4/dist/Chart.bundle.js?v=${fileVersion}"></script>--%>
<script type="text/javascript" src="/static/jslib/chartjs-2.9.4/dist/Chart.bundle.min.js?v=${fileVersion}"></script>
