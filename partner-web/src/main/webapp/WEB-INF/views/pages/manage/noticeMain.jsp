<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<!-- script -->
<section class="contents">
    <h2 class="h2" id="headerTitle">공지사항</h2>

    <div id="tabNoti" class="tab-basic tab-wrap mgt20">
        <div class="tab-nav">
            <button type="button" class="btn-group tab-btn active" role="tab" aria-selected="true" id ="GENERAL" ><i>일반</i></button>
            <button type="button" class="btn-group tab-btn" role="tab" aria-selected="false" id ="MANUAL" ><i>매뉴얼</i></button>
            <button type="button" class="btn-group tab-btn" role="tab" aria-selected="false" id ="FAQ" ><i>FAQ</i></button>
            <button type="button" class="btn-group tab-btn" role="tab" aria-selected="false" id ="HARMFUL"><i>위해상품 공지</i></button>
        </div>

        <div class="tab-pnl" id="baseTab">
            <div class="info-noti-wrap">
                <p class="info-noti">
                    <span>전체 <em><span id="csNotiTotalCount">0</span></em>건</span>
                </p>
                <select id="faqType" class="slc-base w-110" title="카테고리선택" style="display: none">
                    <option value="">전체</option>
                    <c:forEach var="codeDto" items="${faqClassPartner}" varStatus="status">
                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="tbl-noti" >
                <div class="data-wrap" style="height: 500px" id="noticeListGrid">
                </div>
            </div>
        </div>

        <!-- 위해상품공지 tab -->
        <div class="tab-pnl" style="display: none" id="harmTab">
            <div class="info-noti-wrap">
                <p class="info-noti">
                    <span>전체 <em><span id="harmfulItemTotalCount">0</span></em>건</span>
                    <span>최근 6개월 내 전체 공지가 노출됩니다.</span>
                </p>
            </div>
            <div class="tbl-noti">
                <div class="data-wrap" style="height: 500px" id="harmfulItemListGrid">
                </div>
            </div>
        </div>
    </div>

</section>



<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/noticeMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>

<script>
    // qna리스트 그리드
    ${noticeListGridBaseInfo}
    ${harmfulItemListGridBaseInfo}

</script>

