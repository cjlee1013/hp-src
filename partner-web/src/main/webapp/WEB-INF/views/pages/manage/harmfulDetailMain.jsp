<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<!-- script -->
<section class="contents">
    <h2 class="h2">위해상품공지</h2>
    <input type="hidden" id="noDocument" name="noDocument" value="${noDocument}">
    <input type="hidden" id="seq" name="seq" value="${seq}">
    <input type="hidden" id="cdInsptmachi" name="cdInsptmachi" value="${cdInsptmachi}">
    <input type="hidden" id="itemNo" name="itemNo" value="${itemNo}">
    <div class="tbl-base">
        <table>
            <caption>위해상품공지 상세</caption>
            <colgroup>
                <col style="width: 160px">
                <col style="width: auto">
                <col style="width: 160px">
                <col style="width: auto">
                <col style="width: 160px">
                <col style="width: auto">
            </colgroup>
            <tbody>
            <tr>
                <th scope="row">검사기관</th>
                <td id ="instituteTxt"></td>
                <th scope="row">제품명</th>
                <td id ="itemNm"></td>
                <th scope="row">제조업소명</th>
                <td id ="nmManufacupso"></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="tbl-base type2">
        <table>
            <caption>위해상품공지 상세</caption>
            <colgroup>
                <col style="width: 160px">
                <col style="width: auto">
                <col style="width: 160px">
                <col style="width: auto">
            </colgroup>
            <tbody>
            <tr>
                <th scope="row">제품유형명</th>
                <td colspan="3" id="nmProdtype" name ="itemDesc" ></td>
            </tr>
            <tr>
                <th scope="row">포장단위</th>
                <td colspan="3" id="unitPack" name ="itemDesc"></td>
            </tr>
            <tr>
                <th scope="row">바코드</th>
                <td colspan="3" id="barcode" name ="itemDesc"></td>
            </tr>
            <tr>
                <th scope="row">유통기한</th>
                <td colspan="3" id="prdValid" name ="itemDesc"></td>
            </tr>
            <tr>
                <th scope="row">원산지</th>
                <td colspan="3" id="nmManufaccntr" name ="itemDesc"></td>
            </tr>
            <tr>
                <th scope="row">제조일자</th>
                <td colspan="3" id="dtMake" name ="itemDesc"></td>
            </tr>
            <tr>
                <th scope="row">유형</th>
                <td colspan="3" id="nmRpttype" name ="itemDesc"></td>
            </tr>
            <tr>
                <th scope="row">검사구분</th>
                <td colspan="3" id="nmInspttype" name ="itemDesc"></td>
            </tr>
            <tr>
                <th scope="row">판매업소</th>
                <td id="nmSalerupso" name ="itemDesc"></td>
                <th scope="row">수거일자</th>
                <td id="dtTake" name ="itemDesc"></td>
            </tr>
            <tr>
                <th scope="row">수거기관</th>
                <td colspan="3" id="nmTakemachi" name ="itemDesc"></td>
            </tr>
            <tr>
                <th scope="row">발신기관명</th>
                <td colspan="3" id="nmRptrmachi" name ="itemDesc"></td>
            </tr>
            <tr>
                <th scope="row">출처</th>
                <td colspan="3">
                    대한상공회의소 유통물류진흥원 위해상품차단시스템  (<a href="http://upss.gs1kr.org" target="_blank">http://upss.gs1kr.org</a>)<br>
                    <button type="button" id="searchUpssPop" class="ui button large white mg-t-5">위해상품차단시스템 위해상품검사결과 보기</button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

   <%-- <div class="btn-wrap mgt30">
        <button type="button" class="btn-base-l"><i>목록보기</i></button>
    </div>--%>
</section>



<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/harfulDetailMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>

<script>
    // qna리스트 그리드
    ${noticeListGridBaseInfo}
    ${harmfulItemListGridBaseInfo}
    var hmpImgUrl = '${hmpImgUrl}';
</script>

