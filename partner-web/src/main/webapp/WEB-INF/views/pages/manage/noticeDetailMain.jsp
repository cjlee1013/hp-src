<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<!-- script -->
<section class="contents">
    <input type="hidden" id="dspNo" name="dspNo" value="${dspNo}">
    <input type="hidden" id="kind" name="kind" value="${kind}">
    <h2 class="h2">공지사항 상세</h2>
    <div class="tbl-base">
        <table>
            <caption>공지사항 상세</caption>
            <colgroup>
                <col style="width: 160px">
                <col style="width: 50%">
                <col style="width: 160px">
                <col style="width: auto">
            </colgroup>
            <tbody>
            <tr>
                <th scope="row">제목</th>
                <td id ="title">
                   <%-- <span class="flag type2 valign-t">중요</span>
                    파트너센터 신규 오픈 안내--%>
                </td>
                <th scope="row">등록/수정일</th>
                <td id ="regDt"></td>
            </tr>
            <tr>
                <th scope="row">구분</th>
                <td id ="kindTxt"></td>
                <th scope="row">첨부파일</th>
                <td id ="file">
                  <%--  <a href="#none" class="btn-txt fc05">image01.jpg</a>
                    <a href="#none" class="btn-txt fc05">image01.jpg</a>--%>
                </td>
            </tr>
            <tr>
                <th scope="row">내용</th>
                <td colspan="3" id="contents">

                </td>
            </tr>
            </tbody>
        </table>
    </div>
  <%--  <div class="btn-wrap mgt30">
        <button type="button" class="btn-base-l"><i>목록보기</i></button>
    </div>--%>
</section>



<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/noticeDetailMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>

<script>
    // qna리스트 그리드
    ${noticeListGridBaseInfo}
    ${harmfulItemListGridBaseInfo}
    const hmpImgUrl = '${hmpImgUrl}';
</script>

