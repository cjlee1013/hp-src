<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/WEB-INF/views/pages/core/includeDhtmlx.jsp" />
<style>
    .tab_vicinity {height: 30px;padding: 0;border-bottom: 1px solid #ccc;margin-top: 20px;}
    .tab_vicinity li {padding: 0 10px;overflow: hidden;text-align: center;float: left;position: relative;z-index: 10;border: 1px solid #ccc;line-height: 28px;font-weight: bold;font-size: 12px;background-color: #dfdfdf;bottom: -1px;margin-right: 2px;}
    .tab_vicinity li:hover, .tab_vicinity li.on {background-color: #dc2626;}
    .tab_vicinity li:hover a, .tab_vicinity li.on a {color: #fff;text-decoration: none;}
    .num-box .link{color: #5886d1 !important; font-weight: bold !important;}
    .num-box td {border-left: 1px #eee solid !important;}
    .num-box tr {border-left: 1px #eee solid !important; border-right: 1px #eee solid !important;}
    .default-width {width:70%;}
</style>

<section class="main-contents">

<div class="main-cont1">
    <div class="part">
        <h2>공지사항</h2>
        <div class="box-line">
            <div id="tabNotice" class="tab-notice">
                <div class="tab-nav">
                    <button type="button" class="tab-btn"><i>일반</i></button>
                    <button type="button" class="tab-btn"><i>매뉴얼</i></button>
                    <button type="button" class="tab-btn"><i>FAQ</i></button>
                    <button type="button" class="tab-btn"><i>위해상품</i></button>
                </div>

                <div class="tab-pnl">
                    <ul class="notice-list">
                    <c:choose>
                        <c:when test="${empty generalNoticeList}">
                            <li><a href="#none" class="tit">검색결과가 없습니다.</a></li>
                        </c:when>

                        <c:otherwise>
                            <c:forEach var="notice" items="${generalNoticeList}" varStatus="status">
                                <li>
                                    <c:if test="${notice.topYn eq 'Y'}">
                                        <i class="tag type1">중요</i>
                                    </c:if>
                                        <a href="#none" onclick="home.noticeDetailOpenTab(${notice.dspNo} ,'NOTICE')" class="tit">${notice.title}</a>
                                    <c:if test="${notice.newYn eq 'Y'}">
                                        <b class="ico-new"><i class="hide">new</i></b>
                                    </c:if>
                                    <span class="date">${notice.chgDt}</span>
                                </li>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    </ul>
                </div>

                <div class="tab-pnl">
                    <ul class="notice-list">
                    <c:choose>
                        <c:when test="${empty menualNoticeList}">
                            <li><a href="#none" class="tit">검색결과가 없습니다.</a></li>
                        </c:when>

                        <c:otherwise>
                            <c:forEach var="manual" items="${menualNoticeList}" varStatus="status">
                                <li>
                                    <c:if test="${manual.topYn eq 'Y'}">
                                        <i class="tag type1">중요</i>
                                    </c:if>
                                    <a href="#none" onclick="home.noticeDetailOpenTab(${manual.dspNo} ,'NOTICE')" class="tit">${manual.title}</a>
                                    <c:if test="${manual.newYn eq 'Y'}">
                                        <b class="ico-new"><i class="hide">new</i></b>
                                    </c:if>
                                    <span class="date">${manual.chgDt}</span>
                                </li>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    </ul>
                </div>

                <div class="tab-pnl">
                    <ul class="notice-list">
                    <c:choose>
                        <c:when test="${empty faqList}">
                            <li><a href="#none" class="tit">검색결과가 없습니다.</a></li>
                        </c:when>

                        <c:otherwise>
                            <c:forEach var="faq" items="${faqList}" varStatus="status">
                                <li>
                                    <a href="#none" onclick="home.noticeDetailOpenTab(${faq.dspNo} ,'FAQ');" class="tit">${faq.title}</a>
                                    <c:if test="${faq.newYn eq 'Y'}">
                                        <b class="ico-new"><i class="hide">new</i></b>
                                    </c:if>
                                    <span class="date">${faq.chgDt}</span>
                                </li>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    </ul>
                </div>

                <div class="tab-pnl">
                    <ul class="notice-list">
                    <c:choose>
                        <c:when test="${empty harmfulList}">
                            <li><a href="#none" class="tit">검색결과가 없습니다.</a></li>
                        </c:when>

                        <c:otherwise>
                            <c:forEach var="harmful" items="${harmfulList}" varStatus="status">
                                <li>
                                    <a href="#none" onclick="home.harmfulDetailOpenTab('${harmful.noDocument}' ,'${harmful.seq}', '${harmful.cdInsptmachi}');" class="tit">[${harmful.nmRpttype}] ${harmful.itemNm}</a>
                                    <span class="date">${harmful.regDt}</span>
                                </li>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    </ul>
                </div>
            </div>
        </div>
        <button type="button" class="btn-more" onclick="home.noticeMoreView();"><i>더보기</i></button>
    </div>

    <div class="part">
        <h2>나의 판매 활동 <span>등록일 기준, 최근 3개월 대상</span></h2>
        <div class="box-line type2">
            <div class="user-notice">
                <a href="#none">
                    <dl>
                        <dt>미 답변 상품 Q&amp;A</dt>
                        <dd>
                            <b id="no-answer-product-count" onclick="home.goNoAnswerPage();">
                                <c:choose>
                                    <c:when test="${null != salesActInfo.notReplyQnaCount}">
                                        <fmt:formatNumber value="${salesActInfo.notReplyQnaCount}" pattern="#,##0" />
                                    </c:when>
                                    <c:otherwise> - </c:otherwise>
                                </c:choose>
                            </b>건
                            <%--<span id="no-answer-rate">%</span>--%>
                        </dd>
                    </dl>
                </a>
                <a href="#none">
                    <dl>
                        <dt>판매중인 상품</dt>
                        <dd><b id="sailing-product-count" onclick="home.goSailingPage();">
                            <c:choose>
                                <c:when test="${null != salesActInfo.saleItemQty}">
                                    <fmt:formatNumber value="${salesActInfo.saleItemQty}" pattern="#,##0" />
                                </c:when>
                                <c:otherwise> - </c:otherwise>
                            </c:choose>
                        </b>건</dd>
                    </dl>
                </a>
                <a href="#none">
                    <dl>
                        <dt>재고 10개 이하 상품</dt>
                        <dd><b id="ten-less-stock-count" onclick="home.goTenLessStockPage();">
                            <c:choose>
                                <c:when test="${null != salesActInfo.underStockQty}">
                                    <fmt:formatNumber value="${salesActInfo.underStockQty}" pattern="#,##0" />
                                </c:when>
                                <c:otherwise> - </c:otherwise>
                            </c:choose>
                        </b>건</dd>
                    </dl>
                </a>
                <a href="#none">
                    <dl>
                        <dt>판매종료 상품</dt>
                        <dd><b id="end-of-sale-count" onclick="home.goEndOfSalePage();">
                            <c:choose>
                                <c:when test="${null != salesActInfo.endImpnItemQty}">
                                    <fmt:formatNumber value="${salesActInfo.endImpnItemQty}" pattern="#,##0" />
                                </c:when>
                                <c:otherwise> - </c:otherwise>
                            </c:choose>
                        </b>건</dd>
                    </dl>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="main-cont2">
    <h2>판매 진행 현황 <span>최근 1개월 기준</span></h2>
    <button type="button" class="btn-ico-re" onclick="home.refresh();"><i>새로고침</i></button>
    <div class="box-state">
        <h3>발송 전</h3>
        <div class="user-notice">
            <dl>
                <dt>신규주문</dt>
                <dd><a href="#none"><b id="newCnt" onclick="home.goNewCntPage();">0</b>건</a></dd>
            </dl>
            <dl>
                <dt>주문확인지연</dt>
                <dd><a href="#none"><b id="newDelayCnt"
                                       onclick="home.goNewDelayCntPage();">0</b>건</a></dd>
            </dl>
        </div>
        <div class="user-notice">
            <dl>
                <dt>발송대기</dt>
                <dd><a href="#none"><b id="readyCnt" onclick="home.goReadyCntPage();">0</b>건</a></dd>
            </dl>
            <dl>
                <dt>발송지연</dt>
                <dd><a href="#none"><b id="shipDelayCnt"
                                       onclick="home.goShipDelayCntPage();">0</b>건</a></dd>
            </dl>
        </div>
        <div class="user-notice pdt15">
            <dl>
                <dt>주문취소</dt>
                <dd><a href="#none"><b id="cancelRequestCnt"
                                       onclick="home.goCancelRequestCnt();">0</b>건</a></dd>
            </dl>
        </div>
    </div>
    <div class="box-state type2">
        <h3>발송 후</h3>
        <div class="user-notice pdt15">
            <dl>
                <dt>배송중</dt>
                <dd><a href="#none"><b id="shippingCnt"
                                       onclick="home.goShippingCntPage();">0</b>건</a></dd>
            </dl>
        </div>
        <div class="user-notice">
            <dl>
                <dt>반품요청</dt>
                <dd><a href="#none"><b id="returnRequestCnt"
                                       onclick="home.goReturnRequestCnt();">0</b>건</a></dd>
            </dl>
            <dl>
                <dt>교환요청</dt>
                <dd><a href="#none"><b id="exchangeRequestCnt"
                                       onclick="home.goExchangeRequestCnt();">0</b>건</a></dd>
            </dl>
        </div>
        <div class="user-notice">
            <dl>
                <dt>정산예정</dt>
                <dd>
                    <a href="#none" style="cursor: text;">
                        <b id="settleAmt" style="cursor: text;">0</b>원</a>
                    <button type="button" class="btn-base-s" onclick="home.viewTaxBill();">
                        <i>세금계산서조회</i></button>
                </dd>
            </dl>
        </div>
    </div>
</div>
<%-- 메인 롤링배너 영역
<ul id="mainSlider" class="main-slider">
    <li class="item line">
        <a href="#none" class="line" style="background: url(/static/images/@main_slide01.png) 0 0 no-repeat">
            <i class="hide">TONG 고객감동을 실천한 자랑스러운 홈플러스인</i>
        </a>
    </li>
    <li class="item">
        <a href="#none" style="background: url(/static/images/@main_slide01.png) 0 0 no-repeat">
            <i class="hide">TONG 고객감동을 실천한 자랑스러운 홈플러스인</i>
        </a>
    </li>
</ul>
--%>

</section>


<div class="modal-wrap">
    <c:forEach var="partnerPopup" items="${partnerPopupList}">
        <div id="partnerPopup_${partnerPopup.popupNo}" class="modal modal-noti" role="dialog">
            <h2 class="h2">${partnerPopup.popupNm}</h2> <!-- 팝업명 -->

            <div class="modal-cont">
                <div class="noti-img"> <!-- 팝업 IMG -->
                    <c:choose>
                        <c:when test="${not empty partnerPopup.linkInfo}">
                            <c:if test="${partnerPopup.linkType eq 'NOTICE'}">
                                <a href="javascript:void(0);" onclick="home.noticeDetailOpenTab(${partnerPopup.linkInfo}, 'NOTICE');">
                                    <img src="${imgFrontUrl}${partnerPopup.imgUrl}" alt="${partnerPopup.imgNm}">
                                </a>
                            </c:if>
                            <c:if test="${partnerPopup.linkType eq 'FAQ'}">
                                <a href="javascript:void(0);" onclick="home.noticeDetailOpenTab(${partnerPopup.linkInfo}, 'FAQ');">
                                    <img src="${imgFrontUrl}${partnerPopup.imgUrl}" alt="${partnerPopup.imgNm}">
                                </a>
                            </c:if>
                            <c:if test="${partnerPopup.linkType eq 'URL'}">
                                <a href="${partnerPopup.linkInfo}" target="_blank"><img
                                        src="${imgFrontUrl}${partnerPopup.imgUrl}"
                                        alt="${partnerPopup.imgNm}"></a>
                            </c:if>
                        </c:when>
                        <c:otherwise>
                            <img src="${imgFrontUrl}${partnerPopup.imgUrl}"
                                 alt="${partnerPopup.imgNm}">
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <c:choose>
                <c:when test="${partnerPopup.closeBtnPeriod eq 0}">
                    <div class="noti-chk">
                        <input type="checkbox" name="lbChkClose"
                               id="partnerPopupClose_${partnerPopup.popupNo}"
                               onclick="partnerPopup.popupClose('partnerPopup_${partnerPopup.popupNo}', ${partnerPopup.closeBtnPeriod})">
                        <label for="partnerPopupClose_${partnerPopup.popupNo}" class="lb-check">다시
                            보지 않기</label>
                    </div>
                </c:when>
                <c:when test="${partnerPopup.closeBtnPeriod eq 1}">
                    <div class="noti-chk">
                        <input type="checkbox" name="lbChkClose"
                               id="partnerPopupClose_${partnerPopup.popupNo}"
                               onclick="partnerPopup.popupClose('partnerPopup_${partnerPopup.popupNo}', ${partnerPopup.closeBtnPeriod})">
                        <label for="partnerPopupClose_${partnerPopup.popupNo}" class="lb-check">하루동안
                            보지 않기</label>
                    </div>
                </c:when>
                <c:when test="${partnerPopup.closeBtnPeriod eq 7}">
                    <div class="noti-chk">
                        <input type="checkbox" name="lbChkClose"
                               id="partnerPopupClose_${partnerPopup.popupNo}"
                               onclick="partnerPopup.popupClose('partnerPopup_${partnerPopup.popupNo}', ${partnerPopup.closeBtnPeriod})">
                        <label for="partnerPopupClose_${partnerPopup.popupNo}" class="lb-check">7일동안
                            보지 않기</label>
                    </div>
                </c:when>
            </c:choose>

            <button type="button" class="btn-close2 ui-modal-close"
                    onclick="$ui.modalClose('partnerPopup_${partnerPopup.popupNo}')">
                <i class="hide">닫기</i></button>
        </div>

    </c:forEach>
</div>


<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/homeMain.js?${fileVersion}"></script>
<script>
    // body load 영역
    $(document).ready(function () {
        //공지사항 탭 이벤트
        $('#tabNotice').uiTab();

        mainSlider = $('#mainSlider').bxSlider({
            pagerType: 'short',
            slideWidth: '1080px',
            maxSlides: 1,
            shrinkItems: true,
            adaptiveHeight: true
        });

        //공지팝업 노출
        partnerPopup.init();

    });
</script>
<script>
    /**
     * 파트너 공지팝업
     * @type {{popupClose: partnerPopup.popupClose, init: partnerPopup.init}}
     */
    const partnerPopup = {

        init: function () {
            var cookieData = document.cookie;
            <c:forEach var="partnerPopup" items="${partnerPopupList}">
            if (cookieData.indexOf('partnerPopup_${partnerPopup.popupNo}') == -1) {
                $ui.modalOpen('partnerPopup_${partnerPopup.popupNo}');
            }
            </c:forEach>
            $('.dim').remove();

        },
        popupClose: function (popupId, periodType) {

            var periodDay = periodType;
            if (periodType == 0) {
                periodDay = 30;
            }

            var expireDate = new Date();
            expireDate.setDate(expireDate.getDate() + periodDay);
            console.log(expireDate.getDate());
            console.log(expireDate.toGMTString());
            var cookieValue = escape('Y') + "; secure sameSite=none; expires="
                + expireDate.toGMTString(); // secure sameSite=none;
            document.cookie = popupId + "=" + cookieValue;

            $ui.modalClose(popupId);
        }

    }
</script>