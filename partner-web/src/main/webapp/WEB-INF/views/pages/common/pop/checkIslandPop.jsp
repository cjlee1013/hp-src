<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel='stylesheet' type='text/css' href='/static/css/checkIsland.css'>
<body>

<div class="com wrap-popup small popup-zipcode">
    <div class="com popup-wrap-title">
        <h3 class="title pull-left">
            제주/도서산간지역 확인
        </h3>
    </div>
    <!-- tabui-wrap -->
    <div class="tabui-wrap">
        <!-- tab-cont -->
        <div class="tab-cont">
            <div class="disc">
                <%-- <p>도로명, 건물명 또는 지번 주소 중 하나를 검색해 주세요.</p>--%>
            </div>
            <!-- srch-wrap -->
            <div class="srch-wrap">
                <div class="inline">
                    <div class="input_area">
                        <label id="labelSearch" class="lbl_type">주소를 입력해주세요.</label>
                        <input type="text" id="zipSearch" name="zipSearch" class="input" value=""/>
                    </div>
                    <a href="#" id="clearSearch" class="ico btn_del_address">삭제</a>
                    <button type="button" onclick="checkIsland.searchZipCode()" class="button medium darkblue">검색</button>
                </div>
                <div id="errorLabel"></div>
            </div>

            <!-- 검색결과 표시 영역 -->
            <div id="searchResult">
                <div class="result-text">
                    <p class="result">TIP. 아래와 같은 방법으로 검색해주세요</p>
                </div>
            </div>

            <div id="searchAddr" class="bxScroll auto">
                <ul class="com-list-box em">
                    <li><strong>도로명 + 건물번호</strong></li>
                    <dd>예) 시흥대로 391</dd>
                    <li><strong>지역명(동/리) + 번지</strong></li>
                    <dd>예) 독산동 291-7</dd>
                    <li><strong>지역명(동/리) + 건물명(아파트명)</strong></li>
                    <dd>예) 독산동 홈플러스1</dd>
                    <li><strong>사서함명 + 번호</strong></li>
                    <dd>예) 08584</dd>
                </ul>
            </div>
            <div class="pagination">
                <div id="pagingNavi" class="inner"></div>
            </div>
            <!-- //bottom -->
        </div>
        <!-- //tab-cont -->
    </div>
    <!-- //tabui-wrap -->
</div>
<!-- //sys-pop -->

</body>
<script src="/static/js/common/pop/checkIslandPop.js?v=${fileVersion}"></script>
<script>
    $(document).ready(function() {
        checkIsland.init();
    });
</script>

