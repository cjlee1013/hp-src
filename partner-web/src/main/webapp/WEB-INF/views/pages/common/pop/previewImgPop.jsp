<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script>

    $(document).ready(function() {
        var url = new URL(window.location);
        var imgUrl = url.searchParams.get("imgUrl");
        var fileId = url.searchParams.get("fileId");

        if(imgUrl) {
            var imgUrlPath = imgUrl;

            if (!$.jUtil.isEmpty(fileId)) {
                imgUrlPath += "&fileId=" + fileId;
            }

            var tagString = '<img src="'+imgUrlPath+'" onclick="window.close();" style="cursor:hand;margin:0px;padding:0px;">';
            document.write(tagString);

        } else {
            alert('이미지 정보가 없습니다!');
            window.close();
        }
    });

</script>
