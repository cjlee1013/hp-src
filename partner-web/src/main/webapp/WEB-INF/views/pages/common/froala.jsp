<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--<style>--%>
<%--@import url(https://fonts.googleapis.com/earlyaccess/nanumgothic.css);--%>
<%--@import url(https://fonts.googleapis.com/earlyaccess/nanummyeongjo.css);--%>
<%--@import url(https://fonts.googleapis.com/earlyaccess/nanumpenscript.css);--%>
<%--</style>--%>
<%-- 기본 font 로딩 : 외부URL파일은 서버에 올려서 사용함 --%>
<link href="/static/jslib/froala/css/fonts/nanumgothic.css" rel="stylesheet"/>
<link href="/static/jslib/froala/css/fonts/nanummyeongjo.css" rel="stylesheet"/>
<link href="/static/jslib/froala/css/fonts/nanumpenscript.css" rel="stylesheet"/>

<%-- 기본CSS 로딩 : 외부URL파일은 서버에 올려서 사용함 --%>
<%--<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />--%>
<%--<link href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css" rel="stylesheet">--%>
<link href="/static/jslib/froala/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="/static/jslib/froala/css/codemirror.min.css" rel="stylesheet">
<link href="/static/jslib/froala/css/froala_editor.pkgd.css" rel="stylesheet" type="text/css" />
<link href="/static/jslib/froala/css/froala_style.min.css" rel="stylesheet" type="text/css" />
<link href="/static/jslib/froala/css/themes/gray.min.css" rel="stylesheet" type="text/css" />

<%-- 기본 스크립트 로딩 : 외부URL파일은 서버에 올려서 사용함 --%>
<%--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>--%>
<%--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>--%>
<%--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>--%>
<script type="text/javascript" src="/static/jslib/froala/js/codemirror.min.js"></script>
<script type="text/javascript" src="/static/jslib/froala/js/xml.min.js"></script>
<script type="text/javascript" src="/static/jslib/froala/js/froala_editor.pkgd.min.js"></script>
<script type="text/javascript" src="/static/jslib/froala/js/languages/ko.js"></script>

<%-- 추가 플러그인 CSS 로딩 --%>
<link href="/static/jslib/froala/css/plugins/image.min.css" rel="stylesheet" type="text/css" />
<%-- 추가 플러그인 스크립트 로딩 --%>
<script type="text/javascript" src="/static/jslib/froala/js/plugins/image.min.js"></script>
<script>
    froalaHmpImgUrl = '${homeImgUrl}';


    //기본 버튼
    var defaultToolbarBtnType = [
        'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript','|',
        'fontFamily', 'fontSize', 'color', '|',
        'align',
        'insertImage',
        'insertTable', '|',
        'insertHR',
        'clearFormatting',
        'html'
    ];

    //기본 플러그인
    var defaultPluginsType = [
        'fontFamily','fontSize','colors','align','image','table','codeView','charCounter'
    ];

    //Quick Insert 포함 플러그인
    var userPluginsType1 = [
        'fontFamily','fontSize','colors','align','image','table','codeView','charCounter','quickInsert'
    ];

    var commonEditor = {
        hmpImgUrl : typeof hmpImgFrontUrl == "undefined" ? 'https://image.homeplus.kr' : hmpImgFrontUrl,
        inertImgurl: null,
        initEditor: function(obj, heightSize, processKey, toolbarBtnType, pluginsType) {

            if(!heightSize) heightSize = 300;
            if(!toolbarBtnType && toolbarBtnType != undefined) defaultToolbarBtnType.push(toolbarBtnType);
            if(!pluginsType && pluginsType != undefined) defaultPluginsType.push(pluginsType);


            commonEditor.event(obj, processKey);

            $(obj).froalaEditor({
                key: 'bTYPASIBGMWC1YLMP==',
                language: 'ko',
                iframe: true,
                height: heightSize,
                enter: $.FroalaEditor.ENTER_P,
                theme: 'gray',
                fontSize: ['10', '11', '12', '14', '18', '24', '30', '36', '48', '60', '72', '96'],
                fontSizeDefaultSelection: '14',
                fontFamilySelection: true,
                fontFamily: {
                    "dotum, serif":"돋움",
                    "Gulim, serif":"굴림",
                    "Malgun Gothic, serif":"맑은고딕",
                    "Nanum Gothic, serif":"나눔고딕",
                    "Nanum Myeongjo, serif":"나눔명조",
                    "Nanum Pen Script, serif":"나눔손글씨 펜",
                    "KoPub Batang, serif":"바탕체"
                },
                toolbarBottom: false,
                toolbarButtons: defaultToolbarBtnType,
                imageCORSProxy:"/common/imageCORSProxy/upload.json?processKey="+processKey+"&mode=IMG&imgUrl=",
                // 이미지 설정
                imageAllowedTypes: ['jpeg', 'jpg', 'png', 'gif', 'bmp'],
                imageDefaultDisplay: 'inline',
                imageDefaultWidth: 0,
                imageEditButtons: ['imageAlign', 'imageCaption', 'imageRemove', 'imageDisplay', 'imageSize'],

                pluginsEnabled: defaultPluginsType
            });
        },
        event: function(obj, processKey) {
            $(obj).off('froalaEditor.image.inserted');
            $(obj).off('froalaEditor.file.inserted');

            $(obj).on('froalaEditor.image.beforeUpload', function (e, editor, img) {
                commonEditor.uploadImageAjax(obj, processKey, img);
                return false;
            });

            $(obj).on('froalaEditor.file.beforeUpload', function (e, editor, img) {
                console.log(obj, processKey, img);
                return false;
            });
        },
        clearEditor: function (obj) {
            commonEditor.setHtml(obj, "");
        },
        clearHtmlTag: function (obj) {
            $(obj).froalaEditor('html.cleanEmptyTags');
        },
        checkIsEmpty: function (obj) {
            return $(obj).froalaEditor('core.isEmpty');
        },
        getHtml: function(obj) {
            var isEmpty = $(obj).froalaEditor('core.isEmpty');
            if(isEmpty) {
                alert("내용을 입력하세요");
                return true;
            }

            var html = $(obj).froalaEditor('html.get');
            return html;
        },
        getHtmlData: function(obj) {
            var isEmpty = $(obj).froalaEditor('core.isEmpty');

            if (isEmpty) {
                return '';
            } else {
                return $(obj).froalaEditor('html.get');
            }
        },
        setHtml: function (obj, text) {
            $(obj).froalaEditor('html.set', text);
        },
        enableEditor: function (obj) {
            $(obj).froalaEditor('edit.on');
        },
        disableEditor: function (obj) {
            $(obj).froalaEditor('edit.off');
        },
        hideToolbar: function(obj) {
            $(obj).froalaEditor('toolbar.hide');
        },
        showToolbar: function(obj) {
            $(obj).froalaEditor('toolbar.show');
        },
        charCount: function(obj) {
            return $(obj).froalaEditor('charCounter.count');
        },
        uploadImageAjax : function(obj, processKey, files) {
            if (!processKey) {
                return $.jUtil.alert('이미지키를 등록해 주세요.');
            }

            let formData = new FormData();

            if (typeof files[0].name == 'undefined') { //todo: froala editor에서 제공하는거 적용
                formData.append("imgUrl", commonEditor.inertImgurl);
            } else {
                for (let i in files) {
                    formData.append("fileArr", files[i]);
                }
            }

            formData.append("mode", "IMG");
            formData.append("processKey", processKey);

            $.ajax ({
                data: formData,
                type: "POST",
                url: "/common/upload.json",
                cache: false,
                contentType: false,
                processData: false,
                success: function(resData) {
                    let errorMsg = "";

                    for (let i in resData.fileArr) {
                        let f = resData.fileArr[i];

                        if (f.hasOwnProperty("error")) {
                            errorMsg += f.error;
                        } else {
                            var imgUrl = commonEditor.hmpImgUrl;
                            if (!$.jUtil.isEmpty(hmpImgUrl)) {
                                imgUrl = hmpImgUrl;
                            }

                            if (typeof hmpImgFrontUrl != 'undefined') {
                                imgUrl = hmpImgFrontUrl;
                            }

                            imgUrl += f.fileStoreInfo.fileId;
                            commonEditor.insertImage(obj, imgUrl);
                        }
                    }

                    if (!$.jUtil.isEmpty(errorMsg)) {
                        alert(errorMsg);
                    }
                },
                error: function(data) {
                    // 실패시 에러메세지
                    alert("정상적으로 처리되지 않았습니다. 재시도 해주십시오.");
                }
            });
        },
        insertImage: function (obj, url, file) {
            $(obj).froalaEditor('image.insert', url);
        }
    };
</script>

