<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="cancelRejectPop" class="modal type-xl">
    <h2 class="h2">취소거부</h2>
    <div class="modal-cont">
        <p>
            · 선택한 클레임 건을 <span style="font-size: 15px; font-weight: bold">거부처리</span> 합니다.<br>
            · 취소거부로 처리할 경우 배송방법, 송장번호 등을 입력해야 합니다.<br>
            · 거부사유를 상세하게 작성해 주세요.
        </p>

        <h2 class="h3 mgt20" id="cancelRejectTitle"></h2>
        <div class="tbl-data" style="height: auto; max-height: 250px; overflow: auto">
            <table id="itemInfoTable" >
            <caption>상품정보 테이블</caption>
            <colgroup>
                <col width="40%">
                <col width="40%">
                <col width="20%">
            </colgroup>
            <thead>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">상품명</th>
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">옵션명</th>
                <th style="font-size: 15px; text-align: center">신청수량</th>
            </tr>
            </thead>
            <tbody id="itemInfoTableBody">
            </tbody>
            </table>
        </div>

        <h2 class="h3 mgt20"> > 배송설정</h2>
        <div class="tbl-data">
            <table id="deliveryInfo">
                <caption>배송설정 테이블</caption>
                <colgroup>
                    <col width="20%">
                    <col width="20%">
                    <col width="*%">
                </colgroup>
                <tr>
                    <th>배송설정</th>
                    <td>
                        <select id="selectDeliveryType" name="selectDeliveryType" class="ui input medium" style="width: 100px">
                            <c:forEach var="codeDto" items="${shipMethod}" varStatus="status">
                                <c:if test="${codeDto.ref1 eq 'DS'}">
                                    <c:if test="${codeDto.mcCd ne 'DS_QUICK' && codeDto.mcCd ne 'DS_PICK'}">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:if>
                                </c:if>
                            </c:forEach>
                            <option value="N">배송없음</option>
                        </select>
                    </td>
                    <td>
                        <div class="ui form inline"  id="parcelDiv" style="display: none">
                            <select id="selectDlvCd" name="selectDlvCd" class="ui input medium mg-r-5" style="width: 30%">
                                <option value="">택배사선택</option>
                                    <c:forEach var="codeDto" items="${dlvCd}" varStatus="status">
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:forEach>
                            </select>
                            <input type="text" id="invoice" name="invoice" class="ui input medium mg-r-5" maxlength="30" style="width: 60%" placeholder="송장번호 입력">
                        </div>
                        <div class="ui form inline"  id="pickDiv" style="display: none">
                            <span class="text mg-r-5" style="font-size: 15px; font-weight: bold" id="pickDateText">수거예정일 : </span>
                            <span class="inp-date"><input type="text" id="pickRequestDt" name="pickRequestDt" class="inp-base" title="수거예정일 변경"></span>
                        </div>
                        <div class="ui form inline"  id="postDiv" style="display: none">
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4"  onclick="cancelReject.save()"><i>저장</i></button>
            <button type="button" class="btn-base-l"  onclick="cancelReject.closeModal()"><i>취소</i></button>
            <button type="button" class="btn-close2 ui-modal-close" onclick="cancelReject.closeModal()"><i>취소</i></button>
        </div>
    </div>

</div>
<script type="text/javascript" src="/static/js/claim/pop/cancelRejectPop.js?${fileVersion}"></script>
