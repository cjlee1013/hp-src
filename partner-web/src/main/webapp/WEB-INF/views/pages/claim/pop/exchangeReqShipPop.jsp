<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="exchangeRegShipPop" class="modal type-xl">
    <h2 class="h2">교환배송</h2>
    <div class="modal-cont">
        <p>
            · 선택한 클레임 건을 <span style="font-size: 15px; font-weight: bold">교환배송</span> 합니다.<br>
            · 등록된 배송정보는 고객 마이페이지에서 노출됩니다.
        </p>

        <h4 class="h4 mgt20" id="title1" style="font-weight: bold"></h4>
        <div class="tbl-data" style="height: auto; max-height: 250px; overflow: auto">
            <table id="itemInfoTable" >
            <caption>상품정보 테이블</caption>
            <colgroup>
                <col width="40%">
                <col width="40%">
                <col width="20%">
            </colgroup>
            <thead>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">상품명</th>
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">옵션명</th>
                <th style="font-size: 15px; text-align: center">신청수량</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>

        <h4 class="h4 mgt20" id="title2" style="font-weight: bold"></h4>
        <div class="tbl-data" id="exchRequestForm">
            <form id="requestForm">
            <table>
                <caption>신청정보 테이블</caption>
                <colgroup>
                    <col width="20%">
                    <col width="20%">
                    <col width="*%">
                </colgroup>
                <tr>
                    <th>구매자</th>
                    <td colspan="2">
                        <span class="text" id="buyerNm"></span>
                    </td>
                </tr>
                <tr>
                    <th>교환사유</th>
                    <td colspan="2">
                        <span class="text" id="claimReasonType" style="font-weight: bold"></span><br>
                        <span class="text" id="claimReasonDetail"></span>
                    </td>
                </tr>
                <tr id="uploadRow">
                    <th>첨부파일</th>
                    <td colspan="2">
                        <a id="uploadFileName" target="_blank"><span>첨부파일1</span><input type="hidden" id="uploadFileNameUrl"/><input type="hidden" id="uploadFileNameType"/></a>&nbsp
                        <a id="uploadFileName2" target="_blank"><span>첨부파일2</span><input type="hidden" id="uploadFileNameUrl2"/><input type="hidden" id="uploadFileNameType2"/></a>&nbsp
                        <a id="uploadFileName3" target="_blank"><span>첨부파일3</span><input type="hidden" id="uploadFileNameUrl3"/><input type="hidden" id="uploadFileNameType3"/></a>
                    </td>
                </tr>
                <tr>
                    <th>교환 배송비</th>
                    <td colspan="2">
                        <div class="ui form inline" >
                            <span class="text" id="returnShipAmt" ></span>
                            <span class="text">&nbsp  &nbsp</span>
                            <span class="text" id="returnShipAmtText" ></span>
                            <span class="text">&nbsp  &nbsp</span>
                            <span class="text" id="whoReason" style="font-weight: bold"></span>
                            <span class="text">&nbsp  &nbsp</span>
                            <span class="text" id="shipFeeEnclose"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>교환 배송지</th>
                    <td colspan="2">
                        <span class="text" id="exchAddr"></span>
                    </td>
                </tr>
                <tr>
                    <th>교환 배송처리</th>
                    <td>
                        <select id="selectDeliveryType" name="exchShipType" class="ui input medium" style="width: 100px">
                            <option value="C">택배수거</option>
                            <option value="D">직접배송</option>
                            <option value="N">배송없음</option>
                            <option value="P">우편배송</option>
                        </select>
                    </td>
                    <td>
                        <div class="ui form inline"  id="parcelDiv" style="display: none">
                            <select id="exchDlvCd" name="exchDlvCd" class="ui input medium mg-r-5" style="width: 30%">
                                <option value="">택배사선택</option>
                                    <c:forEach var="codeDto" items="${dlvCd}" varStatus="status">
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:forEach>
                            </select>
                            <input type="text" id="exchInvoiceNo" name="exchInvoiceNo" class="ui input medium mg-r-5" maxlength="30" style="width: 60%" placeholder="송장번호 입력">
                        </div>
                        <div class="ui form inline"  id="pickDiv" style="display: none">
                            <span class="text mg-r-5" style="font-size: 15px; font-weight: bold" id="pickDateText">수거예정일 : </span>
                            <span class="inp-date"><input type="text" id="exchReqShipDt" name="shipDt" class="inp-base" title="수거예정일 변경"></span>
                        </div>
                        <div class="ui form inline"  id="postDiv" style="display: none">
                            <input type="text" id="exchMemo" name="exchMemo" class="ui input medium mg-r-5" style="width: 100%" placeholder="메모">
                        </div>
                    </td>
                </tr>
                <input type="hidden" id="claimBundleNo" name="claimBundleNo">
                <input type="hidden" id="claimNo" name="claimNo">
                <input type="hidden" id="exchRoadBaseAddr" name="exchRoadBaseAddr">

            </table>
            </form>
        </div>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4"  onclick="exchangeRegShip.save()"><i>저장</i></button>
            <button type="button" class="btn-base-l"  onclick="exchangeRegShip.closeModal()"><i>취소</i></button>
            <button type="button" class="btn-close2 ui-modal-close" onclick="exchangeRegShip.closeModal()"><i>취소</i></button>
        </div>
    </div>

</div>
<script>

</script>
<script type="text/javascript" src="/static/js/claim/pop/exchangeReqShipPop.js?${fileVersion}"></script>
