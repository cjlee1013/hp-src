<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="exchangeRejectPop" class="modal type-xl">
    <h2 class="h2">교환거부</h2>
    <div class="modal-cont">
        <p>
            · 선택한 클레임 건을 <span style="font-size: 15px; font-weight: bold">교환 거부</span>처리합니다.<br>
            · 반드시 거부 전 구매자와 협의를 진행해 주세요.<br>
            · 택배사 수거 취소 요청을 진행해주세요.<br>
            · 교환 거부 사유를 상세히 입력해 주세요.<br>
            · 교환 거부 처리시 철회가 불가능 합니다.<br>
        </p>

        <h4 class="h4 mgt20" id="title1" style="font-weight: bold"></h4>
        <div class="tbl-data" style="height: auto; max-height: 250px; overflow: auto">
            <table id="itemInfoTable" >
            <caption>상품정보 테이블</caption>
            <colgroup>
                <col width="40%">
                <col width="40%">
                <col width="20%">
            </colgroup>
            <thead>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">상품명</th>
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">옵션명</th>
                <th style="font-size: 15px; text-align: center">신청수량</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>

        <h4 class="h4 mgt20" id="title2" style="font-weight: bold"></h4>
        <div class="tbl-data" id="pickRequestForm">
            <table>
                <caption>신청정보 테이블</caption>
                <colgroup>
                    <col width="20%">
                    <col width="20%">
                    <col width="*%">
                </colgroup>
                <tr>
                    <th>구매자</th>
                    <td colspan="2">
                        <span class="text" id="buyerNm"></span>
                    </td>
                </tr>
                <tr>
                    <th>교환사유</th>
                    <td colspan="2">
                        <span class="text" id="claimReasonType" style="font-weight: bold"></span><br>
                        <span class="text" id="claimReasonDetail"></span>
                    </td>
                </tr>
                <tr id="uploadRow">
                    <th>첨부파일</th>
                    <td colspan="2">
                        <a id="uploadFileName" target="_blank"><span>첨부파일1</span><input type="hidden" id="uploadFileNameUrl"/><input type="hidden" id="uploadFileNameType"/></a>&nbsp
                        <a id="uploadFileName2" target="_blank"><span>첨부파일2</span><input type="hidden" id="uploadFileNameUrl2"/><input type="hidden" id="uploadFileNameType2"/></a>&nbsp
                        <a id="uploadFileName3" target="_blank"><span>첨부파일3</span><input type="hidden" id="uploadFileNameUrl3"/><input type="hidden" id="uploadFileNameType3"/></a>
                    </td>
                </tr>
                <tr>
                    <th>교환배송비</th>
                    <td colspan="2">
                        <div class="ui form inline" >
                            <span class="text" id="returnShipAmt" ></span>
                            <span class="text">&nbsp  &nbsp</span>
                            <span class="text" id="returnShipAmtText" ></span>
                            <span class="text">&nbsp  &nbsp</span>
                            <span class="text" id="whoReason" style="font-weight: bold"></span>
                            <span class="text">&nbsp  &nbsp</span>
                            <span class="text" id="shipFeeEnclose"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>보류사유</th>
                    <td colspan="2">
                        <div class="ui form inline" >

                        <select id="rejectReasonType" name="rejectReasonType" class="ui input medium mg-r-5" style="width: 30%">
                            <option value="">거부사유 선택</option>
                            <option value="X005">기타(구매자 책임 사유)</option>
                            <option value="X006">기타(판매자 책임 사유)</option>
                        </select>
                        <input type="text" id="rejectReasonDetail" name="rejectReasonDetail" class="ui input medium mg-r-5" style="width: 50%" placeholder="상세사유를 입력해 주세요. (최대 100자)">
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <input type="text" id="claimBundleNo" name="claimBundleNo" hidden>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4"  onclick="exchangeReject.save()"><i>저장</i></button>
            <button type="button" class="btn-base-l"  onclick="exchangeReject.closeModal()"><i>취소</i></button>
            <button type="button" class="btn-close2 ui-modal-close" onclick="exchangeReject.closeModal()"><i>취소</i></button>
        </div>
    </div>

</div>
<script>
  $(document).ready(function() {
      exchangeReject.init();
  });
</script>
<script type="text/javascript" src="/static/js/claim/pop/exchangeRejectPop.js?${fileVersion}"></script>

