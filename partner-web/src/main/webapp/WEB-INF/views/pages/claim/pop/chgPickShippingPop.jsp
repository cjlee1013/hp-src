<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="chgPickShippingPop" class="modal type-xl">
    <h2 class="h2">수거방법 변경</h2>
    <div class="modal-cont">
        <p>
            · 선택한 클레임 건의 수거방법을 변경합니다.<br>
        </p>

        <h4 class="h4 mgt20" id="title1" style="font-weight: bold"></h4>
        <div class="tbl-data" style="height: auto; max-height: 250px; overflow: auto">
            <table id="itemInfoTable" >
            <caption>상품정보 테이블</caption>
            <colgroup>
                <col width="40%">
                <col width="40%">
                <col width="20%">
            </colgroup>
            <thead>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">상품명</th>
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">옵션명</th>
                <th style="font-size: 15px; text-align: center">신청수량</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>

        <h4 class="h4 mgt20" id="title2" style="font-weight: bold"></h4>
        <div class="tbl-data" >
            <form id="requestForm">
            <table id="deliveryInfo">
                <caption>신청정보 테이블</caption>
                <colgroup>
                    <col width="20%">
                    <col width="20%">
                    <col width="*%">
                </colgroup>
                <tr>
                    <th>수거방법</th>
                    <td colspan="2">
                        <span class="text" id="pickShipType" name="pickShipType"></span>
                        <span class="text" id="pickShipTypeText" name="pickShipTypeText"></span>&nbsp;
                    </td>
                </tr>
                <tr>
                    <th>수거방법 변경</th>
                    <td>
                        <select id="selectDeliveryType" name="pickShipType" class="ui input medium" style="width: 100px">
                            <option value="NONE" ckecked>변경안함</option>
                            <option value="C">택배수거</option>
                            <option value="D">직접수거</option>
                            <option value="N">수거안함</option>
                            <option value="P">우편</option>
                        </select>
                        <input type="hidden" id="initDeliveryType" name="initDeliveryType">
                    </td>
                    <td>
                        <div class="ui form inline"  id="parcelDiv" style="display: none">
                            <select id="pickDlvCd" name="pickDlvCd" class="ui input medium mg-r-5" style="width: 30%">
                                <option value="">택배사선택</option>
                                    <c:forEach var="codeDto" items="${dlvCd}" varStatus="status">
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:forEach>
                            </select>
                            <input type="text" id="pickInvoiceNo" name="pickInvoiceNo" class="ui input medium mg-r-5" maxlength="30" style="width: 60%" placeholder="송장번호 입력">
                        </div>
                        <div class="ui form inline"  id="pickDiv" style="display: none">
                            <span class="text mg-r-5" style="font-size: 15px; font-weight: bold" id="pickDateText">수거예정일 : </span>
                            <span class="inp-date"><input type="text" id="chgPickShippingDt" name="shipDt" class="inp-base" title="수거예정일 변경"></span>
                        </div>
                        <div class="ui form inline"  id="postDiv" style="display: none">
                            <input type="text" id="pickMemo" name="pickMemo" class="ui input medium mg-r-5" maxlength="50" style="width: 100%" placeholder="메모">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th id="pickAddrTitle"></th>
                    <td style="text-align: left" colspan="2">
                        <table class="ui table">
                            <tr>
                                <th style="width: 100px;">*이름</th>
                                <td>
                                    <input type="text" id="pickReceiverNm" name="pickReceiverNm" class="ui input medium mg-r-5" minlength="2" maxlength="20" style="width: 40%;" placeholder="이름을 입력해주세요">
                                </td>
                            </tr>
                            <tr>
                                <th>*연락처</th>
                                <td style="text-align: left">
                                    <input type="text" name="pickMobileNo" id="pickMobileNo" hidden>
                                    <input type="text" name="pickMobileNo_1" id="pickMobileNo_1" class="inp-base w-80 checkVal" title="연락처" maxlength="3"> -
                                    <input type="text" name="pickMobileNo_2" id="pickMobileNo_2" class="inp-base w-80 checkVal" title="연락처" maxlength="4"> -
                                    <input type="text" name="pickMobileNo_3" id="pickMobileNo_3" class="inp-base w-80 checkVal" title="연락처" maxlength="4">
                                </td>
                            </tr>
                            <tr>
                                <th>*주소</th>
                                <td style="text-align: left">
                                    <div class="ui form inline" id="shippingDiv">
                                        <input type="text" name="pickZipcode" id="pickZipcode" class="ui input medium mg-r-5" style="width:100px;" readonly><button type="button" class="btn-base" onclick="zipCodePopup('claimMainCommon.chgPickShippingSetZipcode');"><i>우편번호 찾기</i></button>
                                        <br>
                                        <input type="text" name="pickRoadBaseAddr" id="pickAddr" class="ui input medium mg-t-5 mg-r-5" style="width:150px;" readonly>
                                        <input type="hidden" name="pickBaseAddr" id="pickBaseAddr" class="ui input medium mg-t-5 mg-r-5" style="width:150px;">
                                        <input type="text" name="pickDetailAddr" id="pickAddrDetail" class="ui input medium mg-t-5" maxlength="100" style="width:240px;" placeholder="상세주소를 입력해주세요">

                                        <input type="hidden" name="claimPickShippingNo" id="claimPickShippingNo">
                                        <input type="hidden" id="modifyType" name="modifyType" value="ADDR">
                                        <input type="hidden" name="pickStatus" value="">
                                        <input type="hidden" name="shipType" value="DS">
                                        <input type="hidden" id="claimBundleNo" name="claimBundleNo">
                                        <input type="hidden" id="claimNo" name="claimNo">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </div>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4"  onclick="chgPickShipping.save()"><i>저장</i></button>
            <button type="button" class="btn-base-l"  onclick="chgPickShipping.closeModal()"><i>취소</i></button>
            <button type="button" class="btn-close2 ui-modal-close" onclick="chgPickShipping.closeModal()"><i>취소</i></button>
        </div>
    </div>

</div>
<script>

</script>
<script type="text/javascript" src="/static/js/claim/pop/chgPickShippingPop.js?${fileVersion}"></script>
