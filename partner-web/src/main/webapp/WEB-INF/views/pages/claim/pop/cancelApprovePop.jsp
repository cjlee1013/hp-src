<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="cancelApprovePop" class="modal type-xl">
    <h2 class="h2">취소승인</h2>
    <div class="modal-cont">
        <p>
            · 선택한 클레임 건을 <span style="font-size: 15px; font-weight: bold">승인처리</span> 합니다.<br>
            · 승인된 건은 거부 및 철회가 불가능 합니다.
        </p>

        <input type="text" id="claimBundleNo" name="claimBundleNo" hidden>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4"  onclick="cancelApprove.save()"><i>승인</i></button>
            <button type="button" class="btn-base-l"  onclick="cancelApprove.closeModal()"><i>취소</i></button>
            <button type="button" class="btn-close2 ui-modal-close" onclick="cancelApprove.closeModal()"><i>취소</i></button>
        </div>
    </div>

</div>

<div id="cancelApproveResultPop" class="modal type-xl" role="dialog">
    <h2 class="h2">취소승인 결과</h2>

    <div class="modal-cont">
        <div class="box-group mgt25">
            <div class="box">
                <span>등록</span>
                <strong id="totalCnt">0</strong>
            </div>
            <div class="box">
                <span>성공</span>
                <strong id="successCnt">0</strong>
            </div>
            <div class="box">
                <span>실패</span>
                <strong class="fc02" id="failCnt">0</strong>
            </div>
        </div>
        <p class="bul-p mgt20">
            실패한 건은 확인 후 다시 진행 부탁드립니다.
        </p>
        <div class="btn-wrap mgt20">
            <button type="button" class="btn-base-l type4" onclick="cancelApprove.closeResultModal()"><i>확인</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="cancelApprove.closeResultModal()"><i class="hide" >닫기</i></button>
</div>

<script>
  $(document).ready(function() {
  });
</script>
<script type="text/javascript" src="/static/js/claim/pop/cancelApprovePop.js?${fileVersion}"></script>
