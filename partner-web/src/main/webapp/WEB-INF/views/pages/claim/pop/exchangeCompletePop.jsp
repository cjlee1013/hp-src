<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="exchangeCompletePop" class="modal type-xl">
    <h2 class="h2">교환완료</h2>
    <div class="modal-cont">
        <p>
            · 교환배송 중인 건을 강제로 교환완료 처리 합니다.<br>
            · 배송방법이 업체직배송이거나, 택배사 트래킹이 정상적으로 동작하지 않을 경우 사용해 주세요
        </p>

        <h4 class="h4 mgt20" id="title1" style="font-weight: bold"></h4>
        <div class="tbl-data" style="height: auto; max-height: 250px; overflow: auto">
            <table id="itemInfoTable" >
            <caption>상품정보 테이블</caption>
            <colgroup>
                <col width="40%">
                <col width="40%">
                <col width="20%">
            </colgroup>
            <thead>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">상품명</th>
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">옵션명</th>
                <th style="font-size: 15px; text-align: center">신청수량</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>

        <h4 class="h4 mgt20" id="title2" style="font-weight: bold"></h4>
        <div class="tbl-data" id="pickRequestForm">
            <form id="requestForm">
            <table>
                <caption>신청정보 테이블</caption>
                <colgroup>
                    <col width="20%">
                    <col width="25%">
                    <col width="*%">
                </colgroup>
                <tr>
                    <th>구매자</th>
                    <td colspan="2">
                        <span class="text" id="buyerNm"></span>
                    </td>
                </tr>
                <tr>
                    <th>교환사유</th>
                    <td colspan="2">
                        <span class="text" id="claimReasonType" style="font-weight: bold"></span><br>
                        <span class="text" id="claimReasonDetail"></span>
                    </td>
                </tr>
                <tr id="uploadRow">
                    <th>첨부파일</th>
                    <td colspan="2">
                        <a id="uploadFileName" target="_blank"><span>첨부파일1</span><input type="hidden" id="uploadFileNameUrl"/><input type="hidden" id="uploadFileNameType"/></a>&nbsp
                        <a id="uploadFileName2" target="_blank"><span>첨부파일2</span><input type="hidden" id="uploadFileNameUrl2"/><input type="hidden" id="uploadFileNameType2"/></a>&nbsp
                        <a id="uploadFileName3" target="_blank"><span>첨부파일3</span><input type="hidden" id="uploadFileNameUrl3"/><input type="hidden" id="uploadFileNameType3"/></a>
                    </td>
                </tr>
                <tr>
                    <th>교환 배송비</th>
                    <td colspan="2">
                        <div class="ui form inline" >
                            <span class="text" id="returnShipAmt" ></span>
                            <span class="text">&nbsp  &nbsp</span>
                            <span class="text" id="returnShipAmtText" ></span>
                            <span class="text">&nbsp  &nbsp</span>
                            <span class="text" id="whoReason" style="font-weight: bold"></span>
                            <span class="text">&nbsp  &nbsp</span>
                            <span class="text" id="shipFeeEnclose"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>교환수거상태</th>
                    <td style="border: 0; border-bottom: 1px solid #ddd; border-right: 1px solid #ddd; text-align: center">
                        <span class="text" id="pickStatus"></span>
                    </td>
                    <td>
                        <span class="text" id="pickShipTypeText" name="pickShipTypeText"></span>&nbsp;
                        <span class="text">&nbsp</span>
                    </td>
                </tr>
                <tr>
                    <th>교환배송상태</th>
                    <td style="border: 0; border-bottom: 1px solid #ddd; border-right: 1px solid #ddd; text-align: center">
                        <span class="text" id="exchStatus"></span>
                    </td>
                    <td>
                        <span class="text" id="exchShipTypeText" name="exchShipTypeText"></span>&nbsp;
                        <span class="text">&nbsp</span>
                    </td>
                </tr>
                <input type="hidden" name="claimExchShippingNo" id="claimExchShippingNo">
                <input type="hidden" id="claimNo" name="claimNo">
                <input type="hidden" id="claimBundleNo" name="claimBundleNo">
                <input type="hidden" id="claimReqType" name="claimReqType" value="CA">
                <input type="hidden" id="claimType" name="claimType" value="X">
                <input type="hidden" id="regId" name="regId" value="PARTNER">
            </table>
            </form>
        </div>


        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4"  onclick="exchangeComplete.save()"><i>완료</i></button>
            <button type="button" class="btn-base-l"  onclick="exchangeComplete.closeModal()"><i>취소</i></button>
            <button type="button" class="btn-close2 ui-modal-close" onclick="exchangeComplete.closeModal()"><i>취소</i></button>
        </div>
    </div>

</div>
<script>
  $(document).ready(function() {
  });
</script>
<script type="text/javascript" src="/static/js/claim/pop/exchangeCompletePop.js?${fileVersion}"></script>
