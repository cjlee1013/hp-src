<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="pickRequestPop" class="modal type-xl">
    <h2 class="h2">수거요청</h2>
    <div class="modal-cont">
        <p>
            · 선택한 클레임 건을 <span style="font-size: 15px; font-weight: bold">수거요청</span> 합니다.<br>
            · 수거요청 후, 수거지 및 수거방법 수정이 불가능합니다.
        </p>

        <h4 class="h4 mgt20" id="pickRequestTitle" style="font-weight: bold"></h4>
        <div class="tbl-data" style="height: auto; max-height: 250px; overflow: auto">
            <table id="itemInfoTable" >
            <caption>상품정보 테이블</caption>
            <colgroup>
                <col width="40%">
                <col width="40%">
                <col width="20%">
            </colgroup>
            <thead>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">상품명</th>
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">옵션명</th>
                <th style="font-size: 15px; text-align: center">신청수량</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>

        <h4 class="h4 mgt20" id="pickRequestTitle2" style="font-weight: bold"></h4>
        <div class="tbl-data" >
            <form id="pickRequestForm">
            <table>
                <caption>신청정보 테이블</caption>
                <colgroup>
                    <col width="20%">
                    <col width="20%">
                    <col width="*%">
                </colgroup>
                <tr>
                    <th>구매자</th>
                    <td colspan="2">
                        <span class="text" id="buyerNm"></span>
                    </td>
                </tr>
                <tr>
                    <th id="claimReasonTitle"></th>
                    <td colspan="2">
                        <span class="text" id="claimReasonType"></span>
                    </td>
                </tr>
                <tr id="uploadRow">
                    <th>첨부파일</th>
                    <td colspan="2">
                        <a id="uploadFileName" target="_blank"><span>첨부파일1</span><input type="hidden" id="uploadFileNameUrl"/><input type="hidden" id="uploadFileNameType"/></a>&nbsp
                        <a id="uploadFileName2" target="_blank"><span>첨부파일2</span><input type="hidden" id="uploadFileNameUrl2"/><input type="hidden" id="uploadFileNameType2"/></a>&nbsp
                        <a id="uploadFileName3" target="_blank"><span>첨부파일3</span><input type="hidden" id="uploadFileNameUrl3"/><input type="hidden" id="uploadFileNameType3"/></a>
                    </td>
                </tr>
                <tr>
                    <th id="claimPickShippingTitle"></th>
                    <td colspan="2">
                        <table class="ui table">
                            <tr>
                                <th style="width: 100px;">*이름</th>
                                <td>
                                    <input type="text" id="pickReceiverNm" name="pickReceiverNm" class="ui input medium mg-r-5" style="width: 40%;" placeholder="이름을 입력해주세요">
                                </td>
                            </tr>
                            <tr>
                                <th>*연락처</th>
                                <td style="text-align: left">
                                    <input type="text" name="pickRequestMobileNo" id="pickRequestMobileNo" hidden>
                                    <input type="text" name="pickMobileNo" id="pickMobileNo" hidden>
                                    <input type="text" name="pickRequestMobileNo_1" id="pickRequestMobileNo_1" class="inp-base w-80 checkVal" title="연락처" maxlength="3"> -
                                    <input type="text" name="pickRequestMobileNo_2" id="pickRequestMobileNo_2" class="inp-base w-80 checkVal" title="연락처" maxlength="4"> -
                                    <input type="text" name="pickRequestMobileNo_3" id="pickRequestMobileNo_3" class="inp-base w-80 checkVal" title="연락처" maxlength="4">
                                </td>
                            </tr>
                            <tr>
                                <th>*주소</th>
                                <td style="text-align: left">
                                    <div class="ui form inline" id="shippingDiv">
                                        <input type="text" name="pickZipcode" id="pickZipcode" class="ui input medium mg-r-5" style="width:100px;" readonly><button type="button" class="btn-base" onclick="zipCodePopup('claimMainCommon.pickRequestSetZipcode');"><i>우편번호 찾기</i></button>
                                        <br>
                                        <input type="text" name="pickRoadBaseAddr" id="pickAddr" class="ui input medium mg-t-5 mg-r-5" style="width:150px;" readonly>
                                        <input type="hidden" name="pickBaseAddr" id="pickBaseAddr" class="ui input medium mg-t-5 mg-r-5" style="width:150px;">
                                        <input type="text" name="pickDetailAddr" id="pickAddrDetail" class="ui input medium mg-t-5" style="width:240px;" placeholder="상세주소를 입력해주세요">
                                        <input type="hidden" name="pickRoadDetailAddr" id="pickRoadDetailAddr" class="ui input medium mg-t-5">
                                        <input type="hidden" name="claimPickShippingNo" id="claimPickShippingNo">
                                        <input type="hidden" id="modifyType" name="modifyType" value="ALL">
                                        <input type="hidden" name="pickStatus" value="P1">
                                        <input type="hidden" name="shipType" value="DS">
                                        <input type="hidden" id="claimBundleNo" name="claimBundleNo">
                                        <input type="hidden" id="claimNo" name="claimNo">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th>배송설정</th>
                    <td>
                        <select id="selectDeliveryType" name="pickShipType" class="ui input medium" style="width: 100px">
                            <option value="C">택배수거</option>
                            <option value="D">직접수거</option>
                            <option value="N">수거안함</option>
                            <option value="P">우편</option>
                        </select>
                    </td>
                    <td>
                        <div class="ui form inline"  id="parcelDiv" style="display: none">
                            <select id="pickDlvCd" name="pickDlvCd" class="ui input medium mg-r-5" style="width: 30%">
                                <option value="">택배사선택</option>
                                    <c:forEach var="codeDto" items="${dlvCd}" varStatus="status">
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:forEach>
                            </select>
                            <input type="text" id="pickInvoiceNo" name="pickInvoiceNo" class="ui input medium mg-r-5" maxlength="30" style="width: 60%" placeholder="송장번호 입력">
                        </div>
                        <div class="ui form inline"  id="pickDiv" style="display: none">
                            <span class="text mg-r-5" style="font-size: 15px; font-weight: bold">수거예정일 : </span>
                            <span class="inp-date"><input type="text" id="shipDt" name="shipDt" class="inp-base" title="수거예정일 변경"></span>
                        </div>
                        <div class="ui form inline"  id="postDiv" style="display: none">
                            <input type="text" id="pickMemo" name="pickMemo" class="ui input medium mg-r-5" style="width: 100%" placeholder="메모">
                        </div>
                    </td>
                </tr>
            </table>
            </form>
        </div>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4"  onclick="pickRequest.save()"><i>저장</i></button>
            <button type="button" class="btn-base-l"  onclick="pickRequest.closeModal()"><i>취소</i></button>
            <button type="button" class="btn-close2 ui-modal-close" onclick="pickRequest.closeModal()"><i>취소</i></button>
        </div>
    </div>

</div>
<script type="text/javascript" src="/static/js/claim/pop/pickRequestPop.js?${fileVersion}"></script>
