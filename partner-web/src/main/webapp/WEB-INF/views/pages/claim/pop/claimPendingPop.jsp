<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="claimPendingPop" class="modal type-xl">
    <h2 class="h2" id="mainTitle"></h2>
    <div class="modal-cont">
        <p>
            · 선택한 클레임 건을 <span style="font-size: 15px; font-weight: bold">보류처리</span> 합니다.<br>
            · 보류사유를 상세하게 작성해 주세요
        </p>

        <h4 class="h4 mgt20" id="title1" style="font-weight: bold"></h4>
        <div class="tbl-data" style="height: auto; max-height: 250px; overflow: auto">
            <table id="itemInfoTable" >
            <caption>상품정보 테이블</caption>
            <colgroup>
                <col width="40%">
                <col width="40%">
                <col width="20%">
            </colgroup>
            <thead>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">상품명</th>
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">옵션명</th>
                <th style="font-size: 15px; text-align: center">신청수량</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>

        <h4 class="h4 mgt20" id="title2" style="font-weight: bold"></h4>
        <div class="tbl-data" id="pickRequestForm">
            <table>
                <caption>신청정보 테이블</caption>
                <colgroup>
                    <col width="20%">
                    <col width="20%">
                    <col width="*%">
                </colgroup>
                <tr>
                    <th>구매자</th>
                    <td colspan="2">
                        <span class="text" id="buyerNm"></span>
                    </td>
                </tr>
                <tr>
                    <th>반품사유</th>
                    <td colspan="2">
                        <span class="text" id="claimReasonType" style="font-weight: bold"></span><br>
                        <span class="text" id="claimReasonDetail"></span>
                    </td>
                </tr>
                <tr id="uploadRow">
                    <th>첨부파일</th>
                    <td colspan="2">
                        <a id="uploadFileName" target="_blank"><span>첨부파일1</span><input type="hidden" id="uploadFileNameUrl"/><input type="hidden" id="uploadFileNameType"/></a>&nbsp
                        <a id="uploadFileName2" target="_blank"><span>첨부파일2</span><input type="hidden" id="uploadFileNameUrl2"/><input type="hidden" id="uploadFileNameType2"/></a>&nbsp
                        <a id="uploadFileName3" target="_blank"><span>첨부파일3</span><input type="hidden" id="uploadFileNameUrl3"/><input type="hidden" id="uploadFileNameType3"/></a>
                    </td>
                </tr>
                <tr>
                    <th>반송배송비</th>
                    <td colspan="2">
                        <div class="ui form inline" >
                            <span class="text" id="returnShipAmt" ></span>
                            <span class="text">&nbsp  &nbsp</span>
                            <span class="text" id="returnShipAmtText" ></span>
                            <span class="text">&nbsp  &nbsp</span>
                            <span class="text" id="whoReason" style="font-weight: bold"></span>
                            <span class="text">&nbsp  &nbsp</span>
                            <span class="text" id="shipFeeEnclose"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>보류사유</th>
                    <td colspan="2">
                        <div class="ui form inline" >

                        <select id="pendingReasonType" name="pendingReasonType" class="ui input medium mg-r-5" style="width: 30%">
                            <option value="">보류사유 선택</option>
                                <c:forEach var="codeDto" items="${claimReasonType}" varStatus="status">
                                    <c:if test="${codeDto.ref1 eq 'P'}">
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                        </select>
                        <input type="text" id="pendingReasonDetail" name="pendingReasonDetail" class="ui input medium mg-r-5" style="width: 50%" placeholder="상세사유를 입력해 주세요. (최대 100자)">
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <input type="text" id="claimBundleNo" name="claimBundleNo" hidden>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4"  onclick="claimPending.save()"><i>저장</i></button>
            <button type="button" class="btn-base-l"  onclick="claimPending.closeModal()"><i>취소</i></button>
            <button type="button" class="btn-close2 ui-modal-close" onclick="claimPending.closeModal()"><i>취소</i></button>
        </div>
    </div>

</div>
<script>
  $(document).ready(function() {
      claimPending.init();
  });
</script>
<script type="text/javascript" src="/static/js/claim/pop/claimPendingPop.js?${fileVersion}"></script>

