<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<!-- script -->
<section class="contents" id="itemMain">
    <div class="float">
        <h2 class="h2 float-l">교환관리</h2>
        <div class="float-r">
            <button type="button" class="btn-ico-re" id="refreshClaimCnt"><i>새로고침</i></button>
        </div>
    </div>

    <div class="box-manage line2" id="claimCntBoard">
        <div style="width: 25%!important;">
            <dl>
                <dt>교환신청</dt>
                <dd><a href="javascript:;" id="request"><span id="claimRequestCnt">0</span>건</a></dd>
            </dl>
        </div>
        <div style="width: 25%!important;">
            <dl>
                <dt>수거완료</dt>
                <dd><a href="javascript:;" id="pickComplete"><span id="claimPickupCnt">0</span>건</a></dd>
            </dl>
        </div>
        <div style="width: 25%!important;">
            <dl>
                <dt>교환보류</dt>
                <dd><a href="javascript:;" id="pending"><span id="claimPendingCnt">0</span>건</a></dd>
            </dl>
        </div>
        <div style="width: 25%!important;">
            <dl>
                <dt>교환완료</dt>
                <dd><a href="javascript:;" id="complete"><span id="claimCompleteCnt">0</span>건</a></dd>
            </dl>
        </div>
    </div>
    <div class="srh-form">
        <form id="claimSearchForm">
            <table>
                <caption>조회</caption>
                <colgroup>
                    <col style="width:75px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row"><label for="claimDateType">조회기간</label></th>
                    <td>
                        <select id="claimDateType" name="claimDateType" class="slc-base w-120">
                            <option value="REQUEST" selected>신청일</option>
                            <option value="COMPLETE">교환완료일</option>
                            <option value="PENDING">보류설정일</option>
                        </select>
                        <span class="inp-date"><input type="text" id="schStartDt" name ="schStartDt" class="inp-base" title="검색기간 시작일"></span> -
                        <span class="inp-date"><input type="text" id="schEndDt" name ="schEndDt" class="inp-base" title="검색기간 종료일"></span>
                        <span class="radio-group mgl5">
                            <input type="radio" name="setSchDt" id="setToday" value="0"><label for="setToday" class="lb-radio">오늘</label>
                            <input type="radio" name="setSchDt" id="setLastWeek" value="-1w"checked><label for="setLastWeek" class="lb-radio">1주일</label>
                            <input type="radio" name="setSchDt" id="setOneMonth" value="-1m"><label for="setOneMonth" class="lb-radio">1개월</label>
                            <input type="radio" name="setSchDt" id="setThreeMonth" value="-3m"><label for="setThreeMonth" class="lb-radio">3개월</label>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th scope="row">처리상태</th>
                    <td>
                        <select class="slc-base" title="처리상태 대분류" id="schClaimStatus" name="schClaimStatus"  data-default="대분류" style="width: 180px">
                            <option value="ALL">전체</option>
                            <c:forEach var="codeDto" items="${claimStatus}" varStatus="status">
                                <option value="${codeDto.mcCd}">교환${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                        <select style="width: 180px" id="schPickupStatus"  name="schPickupStatus" class="slc-base" style="width: 180px">
                            <option value="ALL">수거배송상태전체</option>
                            <option value="NN">수거대기</option>
                            <option value="P0">수거요청 완료</option>
                            <option value="P1">수거예정</option>
                            <option value="P2">수거 중</option>
                            <option value="P3">수거완료</option>
                            <option value="P8">수거실패</option>
                        </select>
                        <select style="width: 180px" id="schExchStatus"  name="schExchStatus" class="slc-base" style="width: 180px">
                            <option value="ALL">교환배송상태전체</option>
                            <option value="D0">상품출고대기</option>
                            <option value="D1">상품출고</option>
                            <option value="D2">배송중</option>
                            <option value="D3">배송완료</option>
                        </select>
                        <select class="slc-base" id="claimSearchType" name="claimSearchType"  style="width: 180px">
                            <option value="PURCHASEORDERNO">주문번호</option>
                            <option value="BUNDLENO">배송번호</option>
                            <option value="CLAIMBUNDLENO">클레임번호</option>
                            <option value="ITEMNO">상품번호</option>
                            <option value="MOBILENO">구매자연락처</option>
                            <option value="SHIPMOBILENO">수령인연락처</option>
                        </select>
                        <input type="text" id="schClaimSearch" name="schClaimSearch" class="inp-base" style="width: 300px;" >
                    </td>
                    <th>승인채널</th>
                    <td>
                        <div>
                            <select style="width: 180px" name="schClaimChannel" id="schClaimChannel" class="slc-base" >
                                <option value="ALL">전체</option>
                                <option value="mypage">구매자</option>
                                <option value="admin">ADMIN</option>
                                <option value="partner">PARTNER</option>
                            </select>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="btn-wrap">
                <button type="button" id="searchBtn" class="btn-base-l type4">검색</button>
                <button type="button" id="searchResetBtn" class="btn-base-l">초기화</button>
            </div>
            <input type="hidden" id="schStatus" name="schStatus">
            <input type="hidden" id="claimType" name="claimType" value="X">
        </form>
    </div>

    <div class="result-wrap float mgt20">
        <p class="float-l result-p">검색 결과 <b><span id="claimTotalCount">0</span></b>건</p>
        <div class="float-r">
            <button type="button" id="excelDownloadBtn" class="btn-ico-exel"><i>엑셀다운</i></button>
            <select class="slc-base w-110" title="정렬선택">
                <option value="">50개씩</option>
                <option value="" selected>100개씩</option>
                <option value="">200개씩</option>
                <option value="">300개씩</option>
                <option value="">400개씩</option>
                <option value="">500개씩</option>
            </select>
        </div>
    </div>

    <div class="btn-gruop mgt20">
        <button type="button" class="ui button medium" id="pickReqBtn" onclick="exchangeMain.openPickReqPop();" disabled><i>수거요청</i></button>
        <button type="button" class="ui button medium" id="pickCompleteBtn" onclick="exchangeMain.openPickCompletePop();" disabled><i>수거완료</i></button>
        <button type="button" class="ui button medium" id="exchangeReqShipBtn" onclick="exchangeMain.openExchangeReqShipPop();" disabled><i>교환배송</i></button>
        <button type="button" class="ui button medium" id="pendingBtn" onclick="exchangeMain.openClaimPendingPop();" disabled><i>교환보류</i></button>
        <button type="button" class="ui button medium" id="exchCompleteBtn" onclick="exchangeMain.openExchangeCompletePop();" disabled><i>교환완료</i></button>
        <button type="button" class="ui button medium" id="chgPickInfoBtn" onclick="exchangeMain.openChgPickShippingPop();" disabled><i>수거정보변경</i></button>
        <button type="button" class="ui button medium" id="chgExchInfoBtn" onclick="exchangeMain.openChgExchShippingPop();" disabled><i>배송정보변경</i></button>
        <button type="button" class="ui button medium" id="rejectBtn" onclick="exchangeMain.openExchangeRejectPop();" disabled><i>교환거부</i></button>
    </div>

    <div class="data-wrap" style="height: 450px" id="claimExchangeListGrid">
    </div>

</section>


<div class="modal-wrap">
    <jsp:include page="/WEB-INF/views/pages/sell/pop/itemClaimDetailPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/claim/pop/returnApprovePop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/claim/pop/claimPendingPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/claim/pop/pickRequestPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/claim/pop/pickCompletePop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/claim/pop/chgPickShippingPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/claim/pop/chgExchShippingPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/claim/pop/exchangeReqShipPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/claim/pop/exchangeCompletePop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/claim/pop/claimShipHistoryPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/claim/pop/exchangeRejectPop.jsp" />

</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/claimExchangeMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/claimMainCommon.js?${fileVersion}"></script>

<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?${fileVersion}"></script>

<script>
    // 상품공지 리얼 그리드
    ${claimExchangeListGridBaseInfo}
    var schStatus = '${schStatus}'; // 보드 카운트 클릭 타입
    var parentMain = null;
</script>

