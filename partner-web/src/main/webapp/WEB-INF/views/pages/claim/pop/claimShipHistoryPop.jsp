<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="claimShipHistoryPop" class="modal" role="dialog">
    <h2 class="h2">배송조회</h2>

    <div class="modal-cont">
        <div class="delivery-info">
            <span id="invoiceInfo"></span>
        </div>
        <div class="delivery-state">
            <ul id="ulList">
            </ul>
        </div>
    </div>
    <button type="button" class="btn-close2 ui-modal-close" onclick="claimShipHistoryPop.closeModal()"><i class="hide" >닫기</i></button>
</div>

<script>
  $(document).ready(function() {
    claimShipHistoryPop.init();
  });
</script>

<script type="text/javascript" src="/static/js/claim/pop/claimShipHistoryPop.js?v=${fileVersion}"></script>
