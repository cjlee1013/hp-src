<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="pickCompletePop" class="modal type-xl">
    <h2 class="h2">수거완료</h2>
    <div class="modal-cont">
        <p>
            · 선택한 클레임 건을 <span style="font-size: 15px; font-weight: bold">수거를 완료처리</span> 합니다.<br>
        </p>

        <h4 class="h4 mgt20" id="title1" style="font-weight: bold"></h4>
        <div class="tbl-data" style="height: auto; max-height: 250px; overflow: auto">
            <table id="itemInfoTable" >
            <caption>상품정보 테이블</caption>
            <colgroup>
                <col width="40%">
                <col width="40%">
                <col width="20%">
            </colgroup>
            <thead>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">상품명</th>
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">옵션명</th>
                <th style="font-size: 15px; text-align: center">신청수량</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>

        <h4 class="h4 mgt20" id="title2" style="font-weight: bold"></h4>
        <div class="tbl-data" id="pickRequestForm">
            <form id="requestForm">
            <table>
                <caption>신청정보 테이블</caption>
                <colgroup>
                    <col width="20%">
                    <col width="20%">
                    <col width="*%">
                </colgroup>
                <tr>
                    <th>구매자</th>
                    <td colspan="2">
                        <span class="text" id="buyerNm"></span>
                    </td>
                </tr>
                <tr>
                    <th id="claimReasonTitle"></th>
                    <td colspan="2">
                        <span class="text" id="claimReasonType" style="font-weight: bold"></span><br>
                        <span class="text" id="claimReasonDetail"></span>
                    </td>
                </tr>
                <tr id="uploadRow">
                    <th>첨부파일</th>
                    <td colspan="2">
                        <a id="uploadFileName" target="_blank"><span>첨부파일1</span><input type="hidden" id="uploadFileNameUrl"/><input type="hidden" id="uploadFileNameType"/></a>&nbsp
                        <a id="uploadFileName2" target="_blank"><span>첨부파일2</span><input type="hidden" id="uploadFileNameUrl2"/><input type="hidden" id="uploadFileNameType2"/></a>&nbsp
                        <a id="uploadFileName3" target="_blank"><span>첨부파일3</span><input type="hidden" id="uploadFileNameUrl3"/><input type="hidden" id="uploadFileNameType3"/></a>
                    </td>
                </tr>
                <tr>
                    <th id="claimPickShippingTitle"></th>
                    <td colspan="2">
                        <span class="text" id="pickAddr"></span>
                    </td>
                </tr>
                <tr>
                    <th>수거방법</th>
                    <td colspan="2">
                        <span class="text" id="pickShipType" name="pickShipType"></span>
                        <span class="text" id="pickShipTypeText" name="pickShipTypeText"></span>&nbsp;
                    </td>
                </tr>
            </table>
                <input type="hidden" id="claimPickShippingNo" name="claimPickShippingNo">
                <input type="hidden" id="claimBundleNo" name="claimBundleNo">
                <input type="hidden" id="claimNo" name="claimNo">
                <input type="hidden" id="pickStatus" name="pickStatus" value="P3">
                <input type="hidden" id="chgId" name="chgId" value="PARTNER">
            </form>
        </div>


        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4"  onclick="pickComplete.save()"><i>저장</i></button>
            <button type="button" class="btn-base-l"  onclick="pickComplete.closeModal()"><i>취소</i></button>
            <button type="button" class="btn-close2 ui-modal-close" onclick="pickComplete.closeModal()"><i>취소</i></button>
        </div>
    </div>

</div>
<script>
  $(document).ready(function() {
      pickComplete.init();
  });
</script>
<script type="text/javascript" src="/static/js/claim/pop/pickCompletePop.js?${fileVersion}"></script>
