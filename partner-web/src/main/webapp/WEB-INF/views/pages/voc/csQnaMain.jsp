<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<!-- script -->
<section class="contents">
    <h2 class="h2">상품Q&amp;A 관리</h2>

    <div class="box-manage line1">
        <div>
            <dl>
                <dt>답변대기</dt>
                <dd><a href="javascript:csQnaMain.searchByQnaStatus('READY')"><span id="READY">0</span></a>건</dd>
            </dl>
        </div>
        <div>
            <dl>
                <dt>답변완료</dt>
                <dd><a href="javascript:csQnaMain.searchByQnaStatus('COMP')"><span id="COMP">0</span></a>건</dd>
            </dl>
        </div>
        <div>
            <dl>
                <dt>답변지연</dt>
                <dd><a href="javascript:csQnaMain.searchByQnaStatus('DELAY')"><span id="DELAY">0</span></a>건</dd>
            </dl>
        </div>
    </div>

    <div class="srh-form">
        <form id="csQnaSearchForm">
            <input type="hidden" id="searchArea" name="searchArea" value="SCH">
            <table>
                <caption>조회</caption>
                <colgroup>
                    <col style="width:80px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row"><label for="srhForm01">등록일</label></th>
                    <td>
                        <span class="inp-date"><input type="text" id="searchStartDt" name ="searchStartDt" class="inp-base" title="검색기간 시작일"></span> -
                        <span class="inp-date"><input type="text"id="searchEndDt" name ="searchEndDt"  class="inp-base" title="검색기간 종료일"></span>
                        <span class="radio-group mgl5">
                            <input type="radio" id="ex-radio1-1" name="setSchDate" value="0"><label for="ex-radio1-1"  class="lb-radio">오늘</label>
                            <input type="radio" id="ex-radio1-2" name="setSchDate" value="-7" checked><label for="ex-radio1-2"  class="lb-radio">일주일</label>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th scope="row">답변여부</th>
                    <td>
                        <input type="radio" id="ex-radio2-01" name="searchQnaStatus" value="" checked>
                        <label for="ex-radio2-01" class="lb-radio mgr20">전체</label>
                        <input type="radio" id="ex-radio2-02" name="searchQnaStatus" value="READY">
                        <label for="ex-radio2-02" class="lb-radio mgr20">답변대기</label>
                        <input type="radio" id="ex-radio2-03" name="searchQnaStatus" value="COMP">
                        <label for="ex-radio2-03" class="lb-radio mgr20">답변완료</label>
                    </td>
                </tr>
                <tr>
                    <th scope="row">문의유형</th>
                    <td>
                        <input type="radio" id="ex-radio3-01" name="searchQnaType" value="" checked>
                        <label for="ex-radio3-01" class="lb-radio mgr20">전체</label>
                        <input type="radio" id="ex-radio3-02" name="searchQnaType" value="ITEM">
                        <label for="ex-radio3-02" class="lb-radio mgr20">상품</label>
                        <input type="radio" id="ex-radio3-03" name="searchQnaType" value="SHIP">
                        <label for="ex-radio3-03" class="lb-radio mgr20">배송</label>
                        <input type="radio" id="ex-radio3-04" name="searchQnaType" value="CLAIM">
                        <label for="ex-radio3-04" class="lb-radio mgr20">교환/반품</label>
                        <input type="radio" id="ex-radio3-05" name="searchQnaType" value="ETC">
                        <label for="ex-radio3-05" class="lb-radio mgr20">기타</label>
                    </td>
                </tr>
                <tr>
                    <th scope="row">검색어</th>
                    <td>
                        <select class="slc-base w-180" id="searchType" name="searchType" title="검색어">
                            <c:forEach var="codeDto" items="${qnaPoSearchType}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                        <input type="text" id ="searchKeyword" name="searchKeyword" class="inp-base w-257" title="검색어 입력">
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="btn-wrap">
                <button type="button" id ="searchBtn"  class="btn-base-l type4">검색</button>
                <button type="button" id ="searchResetBtn" class="btn-base-l type3">초기화</button>
            </div>
        </form>
    </div>

    <div class="result-wrap float mgt20">
        <p class="float-l result-p">검색 결과 <b><span id="csQnaTotalCount">0</span></b>건</p>
        <div class="float-r">
            <button type="button" class="btn-base" id="openTemplatePop" onclick="csQnaTemplatePop.openModal();"><i>템플릿관리</i></button>
        </div>
    </div>

    <div class="data-wrap" style="height: 500px" id="csQnaListGrid">
    </div>

    <div class="modal-wrap">
        <jsp:include page="/WEB-INF/views/pages/voc/pop/csQnaTemplatePop.jsp" />
    </div>

</section>



<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/csQnaMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>

<script>
    // qna리스트 그리드
    ${csQnaListGridBaseInfo}

    const qnaStatus = '${qnaStatus}';


    var parentGrid = null;

</script>

