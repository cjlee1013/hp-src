<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<!-- script -->
<section class="contents">
    <form id="replyForm">
        <input type="hidden" id="qnaNo" name="qnaNo" value="${qnaNo}">
        <h2 class="h2">상품Q&amp;A 상세 </h2>

        <div class="tbl-form mgt10">
            <table>
                <caption>상품Q&amp;A</caption>
                <colgroup>
                    <col style="width:180px">
                    <col style="width:auto">
                    <col style="width:180px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">문의유형</th>
                    <td colspan="3" id="qnaType"></td>
                </tr>
                <tr>
                    <th scope="row">등록일</th>
                    <td id="regDt"></td>
                    <th scope="row">처리일시</th>
                    <td id="compDt"></td>
                </tr>
                <tr>
                    <th scope="row">작성자</th>
                    <td id="userNm"></td>
                    <th scope="row">작성자 ID</th>
                    <td id="userId"></td>
                </tr>
                <tr>
                    <th scope="row">문의내용</th>
                    <td colspan="3">
                        <div class="qna-wrap" id="qnaContents">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">답변등록</th>
                    <td colspan="3" id ="applyArea">
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
</section>



<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/csQnaDetailMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>

<script>
    // qna리스트 그리드
    ${csQnaListGridBaseInfo}

    var parentGrid = null;

</script>

