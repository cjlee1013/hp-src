<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<!-- script -->
<section class="contents">
    <form>
        <h2 class="h2">상품평 상세내용</h2>

        <div class="tbl-form mgt25">
            <table>
                <caption>상품평 상세내용</caption>
                <colgroup>
                    <col style="width:160px">
                    <col style="width:34%">
                    <col style="width:160px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">상품번호</th>
                    <td id="itemNo"></td>
                    <th scope="row">상품명</th>
                    <td id="itemNm"></td>
                </tr>
                <tr>
                    <th scope="row">작성자</th>
                    <td id="userNm"></td>
                    <th scope="row">작성일</th>
                    <td id="regDt"></td>
                </tr>
                <tr>
                    <th scope="row">만족도</th>
                    <td id="grade"></td>
                    <th scope="row">제품상태 만족도</th>
                    <td id="gradeStatusNm"></td>
                </tr>
                <tr>
                    <th scope="row">상품일치 만족도</th>
                    <td id="gradeAccuracyNm"></td>
                    <th scope="row">가격 만족도</th>
                    <td id="gradePriceNm"></td>
                </tr>
                <tr>
                    <th scope="row">상품평내용</th>
                    <td colspan="3">
                        <div class="comment-wrap">
                            <div class="comment-txt">
                                <span id="contents"></span>
                            </div>
                            <div class="comment-img" id="imgDiv"> <!-- 이미지사이즈 가록 180으로 fix함 -->
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
</section>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/itemReviewDetailMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>

<script>
    var reviewNo = '${reviewNo}';
    var hmpImgUrl = '${hmpImgUrl}';
</script>