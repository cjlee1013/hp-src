<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<!-- script -->
<section class="contents">
    <h2 class="h2">상품평 관리</h2>

    <div class="srh-form">
        <form id="itemReviewSearchForm">
            <table>
                <caption>조회</caption>
                <colgroup>
                    <col style="width:80px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row"><label for="srhForm01">등록일</label></th>
                    <td>
                        <span class="inp-date"><input type="text" id="searchStartDt" name="searchStartDt" class="inp-base" title="검색기간 시작일"></span> -
                        <span class="inp-date"><input type="text" id="searchEndDt" name="searchEndDt" class="inp-base" title="검색기간 종료일"></span>
                        <span class="radio-group mgl5">
                            <input type="radio" id="ex-radio1-1" name="setSchDate" value="0"><label for="ex-radio1-1"  class="lb-radio">오늘</label>
                            <input type="radio" id="ex-radio1-2" name="setSchDate" value="-7" checked><label for="ex-radio1-2"  class="lb-radio">일주일</label>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th scope="row">검색어</th>
                    <td>
                        <select class="slc-base w-180" title="검색어" id="searchType" name="searchType">
                            <option value="ITEMNM">상품명</option>
                            <option value="ITEMNO">상품번호</option>
                        </select>
                        <input type="text" class="inp-base w-257" title="검색어 입력" id="searchKeyword" name="searchKeyword">
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="btn-wrap">
                <button type="button" class="btn-base-l type4" id="searchBtn">검색</button>
                <button type="button" class="btn-base-l type3" id="searchResetBtn">초기화</button>
            </div>
        </form>
    </div>

    <div class="result-wrap mgt20">
        <p class="result-p">검색 결과 <b><span id="itemReviewTotalCount">0</span></b>건</p>
    </div>

    <div class="data-wrap" style="height: 500px" id="itemReviewListGrid"></div>

</section>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/itemReviewMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>

<script>
    // itemReview 리스트 그리드
    ${itemReviewListGridBaseInfo}
</script>

