<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="csQnaTemplatePop" class="modal type-l" role="dialog">
    <h2 class="h2">탬플릿관리</h2>

    <div class="modal-cont">
        <div class="tbl-fixed">
            <div class="data-wrap" style="height: 160px" id="csQnaTemplatePopGrid" >
            </div>

        <div class="tbl-form mgt20">
            <form id="csQnaTemplatePopForm" onsubmit="return false;">
                <input type="hidden" id="templateNo" name="templateNo"/>
                <table>
                    <caption>템플릿내용변경</caption>
                    <colgroup>
                        <col style="width:20%">
                        <col style="width:auto">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row">제목</th>
                        <td>
                            <input type="text" class="inp-base w-max" title="제목" id="templateTitle" name="templateTitle" maxlength="10">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">내용</th>
                        <td>
                            <div class="words-wrap w-max mgt5">
                                <textarea class="textarea w-max" title="내용" id="templateContents" name="templateContents" maxlength="1500"></textarea>
                                <span class="words"><i><span class="text" id="textCountTemplateContents" >0</span></i>/1500자</span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="btn-wrap mgt20">
            <button type="button" class="btn-base-l type4" id="addTemplate"><i>등록</i></button>
            <%--<button type="button" class="btn-base-l type2"id="modTemplate" ><i>수정</i></button>--%>
            <button type="button" class="btn-base-l" id="delTemplate"><i>삭제</i></button>
            <button type="button" class="btn-base-l type3" id ="resetBtn"><i>초기화</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="csQnaTemplatePop.closeModal()"><i class="hide" >닫기</i></button>
</div>

<script>
    // 상품 조회 그리드 정보
    ${csQnaTemplatePopGridBaseInfo}

    $(document).ready(function() {
        csQnaTemplatePopGrid.init();
        csQnaTemplatePop.init();
    });


</script>

<script type="text/javascript" src="/static/js/voc/pop/csQnaTemplatePop.js?v=${fileVersion}"></script>