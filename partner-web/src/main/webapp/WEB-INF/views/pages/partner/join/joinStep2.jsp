<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/countdown.js?v=${fileVersion}"></script>
<form id="sellerForm">
    <div class="wrap2">
        <h1 onclick="location.href='/'" style="cursor: pointer;"><img src="/static/images/logo2.png" alt="Homeplus 파트너센터"></h1>
        <ul class="join-step">
            <li class="chk"><span>1</span>사업자 인증</li>
            <li class="curr"><span>2</span>회원정보 입력 <i class="hide">현재 단계</i></li>
            <li><span>3</span>회원가입 완료</li>
        </ul>

        <div class="wrap-box">
            <h2 class="hide">회원정보 입력</h2>
            <h3 class="h3 mgb0">기본정보</h3>
            <div class="tbl-form2">
                <table>
                    <caption>기본정보</caption>
                    <colgroup>
                        <col style="width:180px">
                        <col style="width:auto">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row">판매업체 아이디 <i class="bul-require">필수항목</i></th>
                        <td>
                            <input type="text" id="partnerId" name="partnerId"
                                   class="inp-base w-260" title="판매업체 아이디" maxlength="12">
                            <p id="partnerIdTxt" style="display: none">사용 가능 합니다.</p>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">비밀번호 입력<i class="bul-require">필수항목</i></th>
                        <td>
                            <input type="password" name="password" id="password"
                                   class="inp-base w-260" title="비밀번호 입력" maxlength="20" formmethod="post" autocomplete="off">
                            <!-- 기준 미준수시 클래스 err-->
                            <p id="passwordTxt" style="display: none">비밀번호 생성규칙을 확인해주세요.</p>

                            <ul class="bul-list mgt10">
                                <li>비밀번호는 10자~15자 이내로 입력가능하며, 아이디와 동일한 문자나 3자 연속 동일문자는 이용불가합니다.</li>
                                <li>특수문자는 <span class="txt-underline">!@#$%^&*</span>만 입력가능합니다.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">비밀번호 확인<i class="bul-require">필수항목</i></th>
                        <td>
                            <input type="password" id="passwordConfirm" class="inp-base w-260"
                                   title="비밀번호 확인" formmethod="post" autocomplete="off">
                            <p id="passwordConfirmTxt" style="display: none">비밀번호 생성규칙을 확인해주세요.</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h3 class="h3 mgb0">사업자정보</h3>
            <div class="tbl-form2">
                <table>
                    <caption>사업자정보</caption>
                    <colgroup>
                        <col style="width:180px">
                        <col style="width:auto">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row">사업자 등록번호 <i class="bul-require">필수항목</i></th>
                        <td>
                            <div class="inp-wrap">
                                <input type="hidden" id="partnerNo" name="partnerNo" value="">
                                <input type="text" name="partnerNo_1" id="partnerNo_1"
                                       class="inp-base w-80" title="사업자 등록번호 앞부분" disabled>
                                <input type="text" name="partnerNo_2" id="partnerNo_2"
                                       class="inp-base w-80" title="사업자 등록번호 중간부분" disabled>
                                <input type="text" name="partnerNo_3" id="partnerNo_3"
                                       class="inp-base w-80" title="사업자 등록번호 뒷부분" disabled>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">상호<i class="bul-require">필수항목</i></th>
                        <td>
                            <input type="text" name="partnerNm" id="partnerNm" class="inp-base"
                                   title="상호" value="" disabled>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">판매업체명<i class="bul-require">필수항목</i></th>
                        <td>
                            <input type="text" name="sellerInfo.businessNm" id="businessNm"
                                   class="inp-base w-260" title="판매업체명" maxlength="20">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">대표자명<i class="bul-require">필수항목</i></th>
                        <td>
                            <input type="text" name="partnerOwner" id="partnerOwner"
                                   class="inp-base" title="대표자명" readonly>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">업태<i class="bul-require">필수항목</i></th>
                        <td>
                            <input type="text" name="businessConditions" id="businessConditions"
                                   class="inp-base w-325" title="업태" maxlength="50">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">종목<i class="bul-require">필수항목</i></th>
                        <td>
                            <div class="box-srh w-325">
                                <div class="inp-srh">
                                    <input type="hidden" id="bizCateCd" name="bizCateCd">
                                    <input type="text" name="bizCateCdNm" id="bizCateCdNm"
                                           class="inp-base" title="종목">
                                    <button type="button" id="getBizCateCd" class="btn-srh"><i
                                            class="hide">검색</i></button>
                                </div>
                                <div class="layer-srh">
                                    <ul id="searchCategory"></ul>
                                </div>
                                <div id="bizCateCdListId" class="tag-result"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">통신판매업 신고번호<i class="bul-require">통신판매업 신고번호항목</i></th>
                        <td>
                            <div class="box-srh w-200">
                                <div class="inp-srh">
                                    <input type="text" name="communityNotiNo" id="communityNotiNo"
                                           class="inp-base checkVal" title="통신판매업 신고번호" readonly>
                                </div>
                                <div class="layer-srh">
                                    <ul id="searchCommunityNotiNo"></ul>
                                </div>
                            </div>
                            <button type="button" id="communityNotiNoBtn" class="btn-base">
                                <i>자동입력</i>
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">주소<i class="bul-require">필수항목</i></th>
                        <td>
                            <input type="text" name="zipcode" id="zipcode" class="inp-base w-80"
                                   title="우편번호" disabled>
                            <button type="button" class="btn-base mgl5"
                                    onclick="zipCodePopup('join.zipcode');"><i>우편번호</i></button>
                            <div class="inp-wrap mgt5">
                                <input type="text" name="addr1" id="addr1" class="inp-base"
                                       title="주소" disabled>
                                <input type="text" name="addr2" id="addr2"
                                       class="inp-base w-260 checkVal" title="주소 상세">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h3 class="h3 mgb0">파트너정보</h3>
            <div class="tbl-form2">
                <table>
                    <caption>파트너정보</caption>
                    <colgroup>
                        <col style="width:180px">
                        <col style="width:auto">
                    </colgroup>
                    <tr>
                        <th scope="row">대표연락처<i class="bul-require">필수항목</i></th>
                        <td>
                            <input type="hidden" name="sellerInfo.phone1" id="phone1" value="">
                            <input type="text" name="phone1_1" id="phone1_1" class="inp-base w-80 checkVal" title="대표연락처" maxlength="3"> -
                            <input type="text" name="phone1_2" id="phone1_2" class="inp-base w-80 checkVal" title="대표연락처" maxlength="4"> -
                            <input type="text" name="phone1_3" id="phone1_3" class="inp-base w-80 checkVal" title="대표연락처" maxlength="4">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">대표 이메일<i class="bul-require">필수항목</i></th>
                        <td>
                            <div class="inp-mail">
                                <input type="text" name="sellerInfo.email" id="email"
                                       class="inp-base w-260 checkVal" title="대표 이메일 주소" placeholder="예 : example@homeplus.co.kr">
                            </div>
                            <p class="bul-p mgt10">홈플러스 상품상세, 주문내역 등에 노출됩니다. <br> 고객응대가 가능한 정확한 정보를
                                입력해 주시기 바랍니다. </p>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">마케팅정보 수신동의<i class="bul-require">필수항목</i></th>
                        <td>
                            <div>
                                <span class="radio-tit">SMS 수신동의</span>
                                <span class="radio-group size2">
                                <input type="radio" id="lbAgree01_01" name="sellerInfo.smsYn"
                                       value="Y"><label for="lbAgree01_01"
                                                        class="lb-radio">동의</label>
                                <input type="radio" id="lbAgree01_02" name="sellerInfo.smsYn"
                                       value="N"><label for="lbAgree01_02"
                                                        class="lb-radio">동의안함</label>
                            </span>
                            </div>
                            <div class="mgt20">
                                <span class="radio-tit">이메일 수신동의</span>
                                <span class="radio-group size2">
                                <input type="radio" id="lbAgree02_01" name="sellerInfo.emailYn"
                                       value="Y"><label for="lbAgree02_01"
                                                        class="lb-radio">동의</label>
                                <input type="radio" id="lbAgree02_02" name="sellerInfo.emailYn"
                                       value="N"><label for="lbAgree02_02"
                                                        class="lb-radio">동의안함</label>
                            </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">고객문의 연락처<i class="bul-require">필수항목</i></th>
                        <td>
                            <input type="hidden" name="sellerInfo.infoCenter" id="infoCenter"
                                   value="">
                            <input type="text" name="infoCenter_1" id="infoCenter_1"
                                   class="inp-base w-80 checkVal" title="고객문의 연락처" maxlength="3"> -
                            <input type="text" name="infoCenter_2" id="infoCenter_2"
                                   class="inp-base w-80 checkVal" title="고객문의 연락처" maxlength="4"> -
                            <input type="text" name="infoCenter_3" id="infoCenter_3"
                                   class="inp-base w-80 checkVal" title="고객문의 연락처" maxlength="4">

                            <p class="bul-p mgt10">배송, 환불 등 고객문의 대응의 목적으로 고객에게 제공됩니다. 고객 응대가 가능한 정확한
                                정보를 입력해 주시기 바랍니다.</p>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">대표 카테고리</th>
                        <td>
                            <select title="대표 카테고리" name="sellerInfo.categoryCd" id="categoryCd" class="slc-base" data-default="대표 카테고리"></select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">도서/공연 소득공제</th>
                        <td>
                        <span class="radio-group size2">
                            <input type="radio" id="lbAgree03_01"
                                   name="sellerInfo.cultureDeductionYn" value="Y"><label
                                for="lbAgree03_01" class="lb-radio">제공</label>
                            <input type="radio" id="lbAgree03_02"
                                   name="sellerInfo.cultureDeductionYn" value="N" checked><label
                                for="lbAgree03_02" class="lb-radio">제공안함</label>
                        </span>

                            <label for="cultureDeductionNo" class="lb-block mgt15">사업자 식별번호</label>
                            <input type="text" name="sellerInfo.cultureDeductionNo"
                                   id="cultureDeductionNo" name="sellerInfo.cultureDeductionNo"
                                   class="inp-base" maxlength="30">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">홈페이지</th>
                        <td>
                            <input type="text" name="sellerInfo.homepage" id="homepage"
                                   class="inp-base w-max" title="홈페이지" value="http://" maxlength="100">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">회사 및 상품소개</th>
                        <td>
                            <input type="text" name="sellerInfo.companyInfo" id="companyInfo"
                                   class="inp-base w-max" title="회사 및 상품소개" maxlength="50">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="tit-wrap">
                <h3 class="h3 mgb0">담당자정보</h3>
                <div class="tit-chk">
                    <input type="checkbox" id="copyAffi"><label for="copyAffi" class="lb-check fs12">각 담당자는 영업 담당자와 동일합니다.</label>
                </div>
            </div>
            <div class="tbl-form2">
                <table>
                    <caption>담당자정보</caption>
                    <colgroup>
                        <col style="width:180px">
                        <col style="width:auto">
                    </colgroup>
                    <tbody>
                    <tr id="managerDiv0" class="managerDiv" data-mng-cd="AFFI">
                        <th scope="row">영업담당자</th>
                        <td>
                            <div>
                                <input type="hidden" name="managerList[].mngCd" id="mngCd0"
                                       value="AFFI">
                                <span class="lb-block">담당자명<i class="bul-require">필수항목</i></span>
                                <input type="text" name="managerList[].mngNm" id="mngNm0"
                                       class="inp-base checkVal" title="담당자명">
                            </div>
                            <div class="mgt15">
                                <span class="lb-block">이메일<i class="bul-require">필수항목</i></span>
                                <div class="inp-mail">
                                    <input type="text" name="managerList[].mngEmail"
                                           id="mngEmail0" class="inp-base w-260" title="대표 이메일 주소" placeholder="예 : example@homeplus.co.kr">
                                </div>
                            </div>
                            <div class="mgt15">
                                <span class="lb-block">휴대폰번호<i class="bul-require">필수항목</i></span>
                                <input type="hidden" name="managerList[].mngMobile" id="mngMobile0"
                                       value="">
                                <input type="text" name="mngMobile0_1" id="mngMobile0_1"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="3"> -
                                <input type="text" name="mngMobile0_2" id="mngMobile0_2"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="4"> -
                                <input type="text" name="mngMobile0_3" id="mngMobile0_3"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="4">

                                <button type="button" id="certiCheck" class="btn-base">인증</button>
                                <button type="button" id="reCertiCheck" class="btn-base" style="display: none;">재발송</button>
                                <button type="button" id="releaseCerti" class="btn-base" style="display: none;">변경</button>
                            </div>
                            <div class="mgt15" id="divCertiCheck" style="display: none;">
                                <div class="inp_wrap">
                                    <input type="text" id="checkInfo" name="" class="inp-base w-120" />
                                    <button type="button" id="certiCheckWar" class="btn-base">인증하기</button>
                                </div>
                                <p class="validation_txt mgt15">인증번호를 입력해주세요. 남은시간 <span id="spanTimeOut" class="fc02 fs12 mgl10">02:59</span></p>
                            </div>
                            <div class="mgt15">
                                <span class="lb-block">연락처</span>
                                <input type="hidden" name="managerList[].mngPhone" id="mngPhone0"
                                       value="">
                                <input type="text" name="mngPhone0_1" id="mngPhone0_1"
                                       class="inp-base w-80" title="연락처" maxlength="3"> -
                                <input type="text" name="mngPhone0_2" id="mngPhone0_2"
                                       class="inp-base w-80" title="연락처" maxlength="4"> -
                                <input type="text" name="mngPhone0_3" id="mngPhone0_3"
                                       class="inp-base w-80" title="연락처" maxlength="4">
                            </div>
                        </td>
                    </tr>
                    <tr id="managerDiv1" class="managerDiv" data-mng-cd="SETT">
                        <th scope="row">정산담당자</th>
                        <td>
                            <div>
                                <input type="hidden" name="managerList[].mngCd" id="mngCd1"
                                       value="SETT">
                                <span class="lb-block">담당자명<i class="bul-require">필수항목</i></span>
                                <input type="text" name="managerList[].mngNm" id="mngNm1"
                                       class="inp-base checkVal" title="담당자명">
                            </div>
                            <div class="mgt15">
                                <span class="lb-block">이메일<i class="bul-require">필수항목</i></span>
                                <div class="inp-mail">
                                    <input type="text" name="managerList[].mngEmail"
                                           id="mngEmail1" class="inp-base w-260" title="대표 이메일 주소" placeholder="예 : example@homeplus.co.kr">
                                </div>
                            </div>
                            <div class="mgt15">
                                <span class="lb-block">휴대폰번호<i class="bul-require">필수항목</i></span>
                                <input type="hidden" name="managerList[].mngMobile" id="mngMobile1"
                                       value="">
                                <input type="text" name="mngMobile1_1" id="mngMobile1_1"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="3"> -
                                <input type="text" name="mngMobile1_2" id="mngMobile1_2"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="4"> -
                                <input type="text" name="mngMobile1_3" id="mngMobile1_3"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="4">
                            </div>
                            <div class="mgt15">
                                <span class="lb-block">연락처</span>
                                <input type="hidden" name="managerList[].mngPhone" id="mngPhone1"
                                       value="">
                                <input type="text" name="mngPhone1_1" id="mngPhone1_1"
                                       class="inp-base w-80" title="연락처" maxlength="3"> -
                                <input type="text" name="mngPhone1_2" id="mngPhone1_2"
                                       class="inp-base w-80" title="연락처" maxlength="4"> -
                                <input type="text" name="mngPhone1_3" id="mngPhone1_3"
                                       class="inp-base w-80" title="연락처" maxlength="4">
                            </div>
                        </td>
                    </tr>
                    <tr id="managerDiv2" class="managerDiv" data-mng-cd="CS">
                        <th scope="row">CS담당자</th>
                        <td>
                            <div>
                                <input type="hidden" name="managerList[].mngCd" id="mngCd2"
                                       value="CS">
                                <span class="lb-block">담당자명<i class="bul-require">필수항목</i></span>
                                <input type="text" name="managerList[].mngNm" id="mngNm2"
                                       class="inp-base checkVal" title="담당자명">
                            </div>
                            <div class="mgt15">
                                <span class="lb-block">이메일<i class="bul-require">필수항목</i></span>
                                <div class="inp-mail">
                                    <input type="text" name="managerList[].mngEmail"
                                           id="mngEmail2" class="inp-base w-260 checkVal " title="대표 이메일 주소" placeholder="예 : example@homeplus.co.kr">
                                </div>
                            </div>
                            <div class="mgt15">
                                <span class="lb-block">휴대폰번호<i class="bul-require">필수항목</i></span>
                                <input type="hidden" name="managerList[].mngMobile" id="mngMobile2"
                                       value="">
                                <input type="text" name="mngMobile2_1" id="mngMobile2_1"
                                       class="inp-base w-80" title="휴대폰번호 checkVal" maxlength="3"> -
                                <input type="text" name="mngMobile2_2" id="mngMobile2_2"
                                       class="inp-base w-80" title="휴대폰번호 checkVal" maxlength="4"> -
                                <input type="text" name="mngMobile2_3" id="mngMobile2_3"
                                       class="inp-base w-80" title="휴대폰번호 checkVal" maxlength="4">
                            </div>
                            <div class="mgt15">
                                <span class="lb-block">연락처</span>
                                <input type="hidden" name="managerList[].mngPhone" id="mngPhone2"
                                       value="">
                                <input type="text" name="mngPhone2_1" id="mngPhone2_1"
                                       class="inp-base w-80" title="연락처" maxlength="3"> -
                                <input type="text" name="mngPhone2_2" id="mngPhone2_2"
                                       class="inp-base w-80" title="연락처" maxlength="4"> -
                                <input type="text" name="mngPhone2_3" id="mngPhone2_3"
                                       class="inp-base w-80" title="연락처" maxlength="4">
                            </div>
                        </td>
                    </tr>
                    <tr id="managerDiv3" class="managerDiv" data-mng-cd="LOGI">
                        <th scope="row">물류담당자</th>
                        <td>
                            <div>
                                <input type="hidden" name="managerList[].mngCd" id="mngCd3"
                                       value="LOGI">
                                <span class="lb-block">담당자명<i class="bul-require">필수항목</i></span>
                                <input type="text" name="managerList[].mngNm" id="mngNm3"
                                       class="inp-base checkVal" title="담당자명">
                            </div>
                            <div class="mgt15">
                                <span class="lb-block">이메일<i class="bul-require">필수항목</i></span>
                                <div class="inp-mail">
                                    <input type="text" name="managerList[].mngEmail"
                                           id="mngEmail3" class="inp-base w-260" title="대표 이메일 주소" placeholder="예 : example@homeplus.co.kr">
                                </div>
                            </div>
                            <div class="mgt15">
                                <span class="lb-block">휴대폰번호<i class="bul-require">필수항목</i></span>
                                <input type="hidden" name="managerList[].mngMobile" id="mngMobile3"
                                       value="">
                                <input type="text" name="mngMobile3_1" id="mngMobile3_1"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="3"> -
                                <input type="text" name="mngMobile3_2" id="mngMobile3_2"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="4"> -
                                <input type="text" name="mngMobile3_3" id="mngMobile3_3"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="4">
                            </div>
                            <div class="mgt15">
                                <span class="lb-block">연락처</span>
                                <input type="hidden" name="managerList[].mngPhone" id="mngPhone3"
                                       value="">
                                <input type="text" name="mngPhone3_1" id="mngPhone3_1"
                                       class="inp-base w-80" title="연락처" maxlength="3"> -
                                <input type="text" name="mngPhone3_2" id="mngPhone3_2"
                                       class="inp-base w-80" title="연락처" maxlength="4"> -
                                <input type="text" name="mngPhone3_3" id="mngPhone3_3"
                                       class="inp-base w-80" title="연락처" maxlength="4">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h3 class="h3 mgb0">정산정보</h3>
            <div class="tbl-form2">
                <table>
                    <caption>정산정보</caption>
                    <colgroup>
                        <col style="width:180px">
                        <col style="width:auto">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row">계좌정보<i class="bul-require">필수항목</i></th>
                        <td>
                            <select id="bankCd" name="settleInfo.bankCd" class="slc-base w-160" title="계좌은행">
                                <option value="">은행선택</option>
                                <c:forEach var="code" items="${bankCode}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>

                            <input type="text" name="settleInfo.bankAccountNo" id="bankAccountNo" class="inp-base w-200" maxlength="70" title="계좌번호" placeholder="계좌번호 입력 ('-'없이 입력)">
                            <button type="button" id="getAccAuthenticationBtn" class="btn-base"><i>계좌인증</i></button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">예금주<i class="bul-require">필수항목</i></th>
                        <td>
                            <input type="text" name="settleInfo.depositor" id="depositor" class="inp-base w-160" maxlength="70" title="예금주" readonly>
                            <span id="bankStatus" class="fc02 fs12 mgl10">인증대기</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="tit-wrap">
                <h3 class="h3 mgb0">첨부서류</h3>
                <p class="txt fc04">이미지파일(.jpg, .jpeg, .png, .ppt, .doc)만 등록가능하며, 파일 최대 크기는 1MB까지 가능합니다.</p>
            </div>
            <div id="partnerFileTable" class="tbl-form2">
                <table>
                    <caption>첨부서류</caption>
                    <colgroup>
                        <col style="width:300px">
                        <col style="width:auto">
                    </colgroup>
                    <tbody>
                    <c:forEach var="codeDto" items="${partnerFileType}" varStatus="status">
                        <tr>
                            <th scope="row">${codeDto.mcNm}<i class="${codeDto.ref2}"></i></th>
                            <td>
                                <div class="file-wrap">
                                    <label class="lb-file" data-file-type="${codeDto.mcCd}" data-field-name="partnerFile${status.count}" onclick="partner.file.clickFile($(this));">추가</label>
                                    <ul id="uploadPartnerFileSpan_${codeDto.mcCd}" data-file-type="${codeDto.mcCd}" class="file-result2">

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

            <div class="btn-wrap mgt30">
                <button type="button" class="btn-base-l type4" onclick="join.setJoin();"><i>회원가입
                    신청</i></button>
            </div>
        </div>
    </div>
</form>

<div class="footer-wrap">
    <footer id="footer" class="footer"></footer>
</div>

<c:forEach var="codeDto" items="${partnerFileType}" varStatus="status">
    <input type="file" name="partnerFile${status.count}" data-file-id="uploadPartnerFileSpan_${codeDto.mcCd}" data-limit="${codeDto.ref1}"  style="display: none;"/>
</c:forEach>

<script type="text/javascript">
    const hmpImgUrl = '${hmpImgUrl}';
    const partnerNo = '${partnerNo}';
    const partnerNm = '${partnerNm}';
    const partnerOwner = '${partnerOwner}';

    $(document).ready(function () {
        join.init();
        partner.file.init();
        CommonAjaxBlockUI.global();
    });
</script>
<script type="text/javascript" src="/static/js/common/validateUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/join/joinStep2.js?${fileVersion}"></script>