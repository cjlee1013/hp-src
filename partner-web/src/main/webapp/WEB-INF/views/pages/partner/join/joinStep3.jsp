<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="wrap2">
    <h1 onclick="location.href='/'" style="cursor: pointer;"><img src="/static/images/logo2.png" alt="Homeplus 파트너센터"></h1>
    <ul class="join-step">
        <li class="chk"><span>1</span>사업자 인증</li>
        <li class="chk"><span>2</span>회원정보 입력</li>
        <li class="curr"><span>3</span>회원가입 완료<i class="hide">현재 단계</i></li>
    </ul>

    <div class="wrap-box">
        <h2 class="hide">회원가입 완료</h2>

        <div class="complete-box">
            <h2 class="h1">파트너센터 회원가입이 완료되었습니다.</h2>
            <p>입력하신 정보를 토대로 심사중입니다. <br>심사완료 시 아래 제휴 담당자 이메일을 통해 안내드립니다.</p>
        </div>

        <div class="complete-info">
            <dl>
                <dt>담당자명</dt>
                <dd>${mngNm}</dd>
            </dl>
            <dl>
                <dt>휴대폰번호</dt>
                <dd>${mngMobile}</dd>
            </dl>
            <dl>
                <dt>연락처</dt>
                <dd>${mngPhone}</dd>
            </dl>
            <dl>
                <dt>이메일</dt>
                <dd>${mngEmail}</dd>
            </dl>
        </div>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l" onclick="window.location.href = '../../../../..';"><i>메인으로 이동</i></button>
        </div>
    </div>
</div>

<div class="footer-wrap">
    <footer id="footer" class="footer"></footer>
</div>
