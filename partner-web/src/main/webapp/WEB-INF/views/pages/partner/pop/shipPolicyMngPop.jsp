<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="shipPolicyMngPop" class="modal type-xl" role="dialog">
    <h2 class="h2">배송정책관리</h2>

    <div class="modal-cont">
        <form id="shipPolicySetForm" name="shipPolicySetForm" onsubmit="return false;">
            <h3 class="h4">배송정책 수 : <em><span id="shipPolicyTotalCnt">0</span></em>건</h3>

            <div class="data-wrap" style="height: 160px" id="shipPolicyListGrid">
            </div>

            <h3 class="h4 mgt15">배송상세정보</h3>

            <div class="tbl-form mgt10">
                <table>
                    <caption>배송상세정보표</caption>
                    <colgroup>
                        <col style="width:140px">
                        <col style="width:auto">
                        <col style="width:140px">
                        <col style="width:auto">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row">배송정책번호</th>
                        <input type="hidden" id="shipPolicyNoP" name="shipPolicyNo">
                        <td id="shipPolicyNoInfo"></td>
                        <th scope="row">배송정보명<i class="bul-require">필수항목</i></th>
                        <td><input type="text" name="shipPolicyNm" id="shipPolicyNm" class="inp-base" title="배송정보명" maxlength="25"></td>
                    </tr>
                    <tr>
                        <th scope="row">배송방법<i class="bul-require">필수항목</i></th>
                        <td colspan="3">
                            <input type="hidden" id="shipMethod" name="shipMethod" value="">
                            <select class="slc-base" title="배송방법 선택" id="shipMethodDS" name="shipMethodDS">
                                <option value="">선택</option>
                                <c:forEach var="code" items="${shipMethod}" varStatus="status">
                                    <c:if test="${code.ref1 eq 'DS'}">
                                        <option value="${code.mcCd}" >${code.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">배송유형<i class="bul-require">필수항목</i></th>
                        <td  colspan="3">
                            <span class="radio-group02">
                                 <c:forEach var="codeDto" items="${shipType}" varStatus="status">
                                     <c:set var="checked" value="" />
                                     <c:if test="${codeDto.ref1 eq 'df'}">
                                         <c:set var="checked" value="checked" />
                                     </c:if>
                                     <input type="radio" id="shipType${codeDto.mcCd}" name="shipType" value="${codeDto.mcCd}" ${checked}><label for="shipType${codeDto.mcCd}" class="lb-radio mgb0">${codeDto.mcNm}</label>
                                 </c:forEach>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">배송비종류<i class="bul-require">필수항목</i></th>
                        <td colspan="3">
                            <span class="radio-group02">
                                <c:forEach var="codeDto" items="${shipKind}" varStatus="status">
                                    <c:set var="checked" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="checked" value="checked" />
                                    </c:if>
                                    <input type="radio" id="shipKind${codeDto.mcCd}" name="shipKind" value="${codeDto.mcCd}" ${checked}><label for="shipKind${codeDto.mcCd}" class="lb-radio mgb0">${codeDto.mcNm}</label>
                                </c:forEach>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">배송비<i class="bul-require">필수항목</i></th>
                        <td colspan="3">
                            <input type="text" name="shipFee" id="shipFee" class="inp-base" title="배송비 입력" maxlength="6" readonly> 원 <span class="mgr10 mgl10">|</span> <input type="text"name="freeCondition" id="freeCondition" class="inp-base" title="배송비 입력" maxlength="7" readonly> 원 이상 구매 시 무료
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">수량별 차등<i class="bul-require">필수항목</i></th>
                        <td colspan="3">
                            <span class="radio-group02">
                                <input type="radio" id="diffYnN" name="diffYn" value="N" checked><label for="diffYnN" class="lb-radio mgb0">설정안함</label>
                                <input type="radio" id="diffYnY" name="diffYn" value="Y" disabled ><label for="diffYnY" class="lb-radio mgb0">설정함</label>
                            </span>

                            <div class="mgt10">
                                구매수량 <input type="text" name="diffQty" id="diffQty" class="inp-base" title="구매수량 입력" max="4" readonly> 개 초과 시 마다 배송비를 추가로 부과
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">배송비 결제방식</th>
                        <td colspan="3">
                            <span class="radio-group02">
                                 <c:forEach var="codeDto" items="${prepaymentYn}" varStatus="status">
                                     <c:set var="checked" value="" />
                                     <c:if test="${codeDto.ref1 eq 'df'}">
                                         <c:set var="checked" value="checked" />
                                     </c:if>
                                     <input type="radio" id="prepaymentYn${codeDto.mcCd}" name="prepaymentYn" value="${codeDto.mcCd}" ${checked}><label for="prepaymentYn${codeDto.mcCd}" class="lb-radio mgb0">${codeDto.mcNm}</label>
                                 </c:forEach>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">배송가능지역<i class="bul-require">필수항목</i></th>
                        <td colspan="3">
                            <select id="shipArea" name="shipArea" class="slc-base " title="배송가능지역 선택">
                                <c:forEach var="code" items="${shipArea}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">도서산간 배송비</th>
                        <td colspan="3">
                            제주 <input type="text" name="extraJejuFee" id="extraJejuFee" class="inp-base" title="제주 배송비 입력" maxlength="6" value="0"> 원 <span class="mgl10 mgr10">|</span> 도서산간 <input type="text"  name="extraIslandFee" id="extraIslandFee" class="inp-base" title="도서산간 배송비 입력" maxlength="6" value="0"> 원
                            <div class="mgt10">
                                <button type="button" class="btn-base-s" onclick="checkIslandPop(); return false;"><i>제주/도서산간 지역안내</i></button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">배송비 노출여부</th>
                        <td colspan="3">
                            <span class="radio-group02">
                                 <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                                     <c:set var="checked" value="" />
                                     <c:if test="${codeDto.ref1 eq 'df'}">
                                         <c:set var="checked" value="checked" />
                                     </c:if>
                                     <input type="radio" id="popDispYn${codeDto.mcCd}" name="dispYn" value="${codeDto.mcCd}" ${checked}><label for="popDispYn${codeDto.mcCd}" class="lb-radio mgb0">${codeDto.mcNm}</label>
                                 </c:forEach>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">사용여부</th>
                        <td colspan="3">
                            <select class="slc-base " title="사용여부 선택" id="useYn" name="useYn">
                                <c:forEach var="code" items="${useYn}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h3 class="h4 mgt15">배송추가정보 <b>( 이미 등록된 상품의 배송추가정보는 자동으로 변경되지 않습니다. )</b></h3>

            <div class="tbl-form mgt10">
                <table>
                    <caption>배송상세정보표</caption>
                    <colgroup>
                        <col style="width:17%">
                        <col style="width:32%">
                        <col style="width:17%">
                        <col style="width:auto">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row">반품/교환 배송비<i class="bul-require">필수항목</i></th>
                        <td colspan="3"><input type="text"  name="claimShipFee" id="claimShipFeeP" class="inp-base" title="반품/교환 배송비 입력" maxlength="6"> 원</td>
                    </tr>
                    <tr>
                        <th scope="row">출고지<i class="bul-require">필수항목</i></th>
                        <td colspan="3">
                            <input type="text" name="releaseZipcode" id="releaseZipcodeP" class="inp-base w-110" title="우편번호" readonly> <button type="button" class="btn-base" onclick="zipCodePopup('shipPolicyMngPop.releaseZipcode');"><i>우편번호</i></button>
                            <div class="mgt10">
                                <input type="text" name="releaseAddr1" id="releaseAddr1P" class="inp-base w-half " title="주소" readonly>
                                <input type="text"  name="releaseAddr2" id="releaseAddr2P" class="inp-base w-half " title="상세주소" autocomplete="off">
                            </div>
                            <div class="mgt15">
                                <input type="checkbox" id="copyPartnerAddrPop"><label for="copyPartnerAddrPop" class="lb-check">사업자주소 동일 적용</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">회수지<i class="bul-require">필수항목</i></th>
                        <td colspan="3">
                            <input type="text" name="returnZipcode" id="returnZipcodeP" class="inp-base w-110" title="우편번호" readonly> <button type="button" class="btn-base" onclick="zipCodePopup('shipPolicyMngPop.returnZipcode');"><i>우편번호</i></button>
                            <div class="mgt10">
                                <input type="text" name="returnAddr1" id="returnAddr1P" class="inp-base w-half " title="주소" readonly>
                                <input type="text" name="returnAddr2" id="returnAddr2P" class="inp-base w-half " title="상세주소">
                            </div>
                            <div class="mgt15">
                                <input type="checkbox" id="copyReleaseAddrPop"><label for="copyReleaseAddrPop" class="lb-check">출고지주소 동일 적용</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">출고기한<i class="bul-require">필수항목</i></th>
                        <td colspan="3">
                            <select class="slc-base"  name="releaseDay" id="releaseDayP" title="출고기한 일 선택">
                                <option value="">선택</option>
                                <option value="1">오늘발송</option>
                                <c:forEach var="item"  begin="2" end="15" step="1" varStatus="status">
                                    <option value="${item}">${item}</option>
                                </c:forEach>
                                <option value="-1">미정</option>
                            </select> 일 이내    <span class="releaseDetailInfo" style="display:none"> <span class="mgl10 mgr10">|</span>

                                <select class="slc-base" name="releaseTime" id="releaseTimeP" title="출고기한 시간 선택">
                                     <option value="">선택</option>
                                <c:forEach var="item"  begin="1" end="24" step="1" varStatus="status">
                                    <option value="${status.count}">${status.count}</option>
                                </c:forEach>
                                </select> 시 결제건까지
                            </span>
                            <div class="mgt15">
                                <input type="checkbox" id="holidayExceptYn" name="holidayExceptYn" value="Y"><label for="holidayExceptYn" class="lb-check">주말/공휴일 및 해당 전일 제외 </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">안심번호 사용여부</th>
                        <td colspan="3">
                            <select class="slc-base" id="safeNumberUseYn" name="safeNumberUseYn"  title="안심번호 사용여부 선택">
                                <c:forEach var="code" items="${safeNumberUseYn}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div class="float mgt15">
                    <div class="float-l">
                        <input type="checkbox" id="defaultYn" name="defaultYn" value="Y"><label for="defaultYn" class="lb-check">기본 배송방법으로 설정</label>
                    </div>
                </div>
            </div>

            <div class="btn-wrap mgt30">
                <button type="button" class="btn-base-l type4" id="setShipPolicyMngBtn"><i>저장</i></button>
                <button type="button" class="btn-base-l type3" id="resetShipPolicyMngBtn"><i>초기화</i></button>
            </div>
        </form>
    </div>

    <button type="button" class="btn-close2 ui-modal-close"  onclick="shipPolicyMngPop.closeModal();"><i class="hide" >닫기</i></button>
</div>

<script>
    // 배송정책 등록/수정 리얼 그리드
    ${shipPolicyListGridBaseInfo}

</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/partner/pop/shipPolicyMngPop.js?v=${fileVersion}"></script>