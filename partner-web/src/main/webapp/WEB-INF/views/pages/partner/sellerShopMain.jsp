<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!-- script -->
<section class="contents">
    <form id="sellerShopSetForm">
        <div class="float">
            <h2 class="h2 float-l">기본정보관리 <span class="fc02"><i class="bul-require">필수항목</i>표시는 필수입력 항목입니다.</span></h2>
        </div>

        <div id="uiAcco1" class="form-acco acco-wrap">
            <dl class="acco">
                <dt class="acco-tit">
                    <h3 class="h3">기본 정보</h3>
                </dt>
                <dd class="acco-pnl">
                    <div class="tbl-form">
                        <table>
                            <caption>기본정보표</caption>
                            <colgroup>
                                <col style="width:180px">
                                <col style="width:auto">
                            </colgroup>
                            <tbody>
                                <tr>
                                    <th scope="row">
                                        셀러샵 이름(닉네임)<i class="bul-require">필수항목</i>
                                    </th>
                                    <td>
                                        <input type="text" class="inp-base w-490" title="셀러샵 이름(닉네임)" id="shopNm" name="shopNm" maxlength="10">
                                        <button type="button" class="btn-base" onclick="sellerShop.validation('shopNm');" id="nameValidationBtn"><i>중복확인</i></button>
                                        <!-- span class="txt-err">필수항목은 반드시 입력해주세요.</span -->
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">셀러샵 프로필 이미지<i class="bul-require">필수항목</i></th>
                                    <td>
                                        <!--이미지 등록 전-->
                                        <div id="displayProfileImgDiv">
                                            <ul class="list-upload">
                                                <li>
                                                    <div class="upload-wrap">
                                                        <div class="thumb"></div>
                                                        <button type="button" class="btn-base-s" onclick="sellerShop.img.clickFile('profile');"><i>등록</i></button>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <!--이미지 등록 후-->
                                        <div id="displayNoneProfileImgDiv" style="display: none;">
                                            <ul class="list-upload">
                                                <li>
                                                    <div class="upload-wrap">
                                                        <div class="thumb">
                                                            <input type="hidden" id="shopProfileImg" name="shopProfileImg"/>
                                                            <img id="profileImgUrlTag" width="132" height="132" src="" alt="프로파일 이미지">
                                                        </div>
                                                        <button type="button" class="btn-clear" onclick="sellerShop.img.deleteImg('profile');"><i class="hide">삭제</i></button>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <p class="bul-p mgt15">사이즈: 1200 X 330 / 용량: 2MB 이하 / 파일 : JPG, JPEG, GIF</p>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">셀러샵 로고<i class="bul-require">필수항목</i></th>
                                    <td>
                                        <!-- span class="radio-group size2">
                                            <input type="radio" id="ex-radio1-1" name="shopLogoYn" value="Y"><label for="ex-radio1-1"  class="lb-radio">노출</label>
                                            <input type="radio" id="ex-radio1-2" name="shopLogoYn" value="N" checked><label for="ex-radio1-2"  class="lb-radio">노출안함</label>
                                        </span -->
                                        <div id="displayLogoDiv">
                                            <!--이미지 등록 전-->
                                            <div id="displayLogoImgDiv">
                                                <ul class="list-upload">
                                                    <li>
                                                        <div class="upload-wrap">
                                                            <div class="thumb"></div>
                                                            <button type="button" class="btn-base-s" onclick="sellerShop.img.clickFile('logo');"><i>등록</i></button>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!--이미지 등록 후-->
                                            <div id="displayNoneLogoImgDiv" style="display: none;">
                                                <ul class="list-upload">
                                                    <li>
                                                        <div class="upload-wrap">
                                                            <div class="thumb">
                                                                <input type="hidden" id="shopLogoImg" name="shopLogoImg"/>
                                                                <img id="logoImgUrlTag" width="132" height="132" src="" alt="로고 이미지">
                                                            </div>
                                                            <button type="button" class="btn-clear" onclick="sellerShop.img.deleteImg('logo');"><i class="hide">삭제</i></button>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <p class="bul-p mgt15">사이즈 : 320*320 / 용량 : 2MB이하 / 파일 : PNG</p>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">셀러샵 URL<i class="bul-require">필수항목</i></th>
                                    <td>
                                        <div class="mgb10" id="shopUrlDiv">
                                            <input type="text" class="inp-base w-374" title="셀러샵 URL 입력" id="shopUrl" name="shopUrl" onkeyup="sellerShop.setHttpUrl(this.value);" >
                                            <button type="button" class="btn-base" onclick="sellerShop.validation('shopUrl');" id="urlValidationBtn" ><i>중복확인</i></button>
                                        </div>
                                        <p>
                                            PC – https://front.homeplus.co.kr/sellerShop/<span id="sellerShopUrlPc"></span>
                                        </p>
                                        <p class="mgt10">
                                            MOBILE – https://mfront.homeplus.co.kr/sellerShop/<span id="sellerShopUrlMobile"></span>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">셀러샵 소개</th>
                                    <td>
                                        <div class="words-wrap">
                                            <input type="text" class="inp-base" title="셀러샵 소개" id="shopInfo" name="shopInfo" maxlength="20">
                                            <span class="words"><i><span id="shopInfoCnt">0</span></i>/20자</span>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">고객센터 정보</th>
                                    <td>
                                        <span class="radio-group size2">
                                            <input type="radio" id="ex-radio2-1" name="csUseYn" value="Y"><label for="ex-radio2-1" class="lb-radio">노출</label>
                                            <input type="radio" id="ex-radio2-2" name="csUseYn" value="N" checked><label for="ex-radio2-2" class="lb-radio">노출안함</label>
                                        </span>

                                        <!--고객센터 정보 노출 케이스-->
                                        <div id="csInfoDiv" class="mgt15" style="display: none;">
                                            <input type="text" class="inp-base w-max" title="고객센터 정보 입력" id="csInfo" name="csInfo" placeholder="예시 : 평일 9시부터 18시까지 ( 토/일 휴무 )">
                                            <p class="bul-p mgt5">대표번호, 이메일은 판매업체 관리에서 등록한 정보로 노출됩니다.</p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">셀러샵 사용여부</th>
                                    <td>
                                        <span class="radio-group size2">
                                            <input type="radio" id="ex-radio3-1" name="useYn" value="Y" checked><label for="ex-radio3-1" class="lb-radio">사용</label>
                                            <input type="radio" id="ex-radio3-2" name="useYn" value="N"><label for="ex-radio3-2" class="lb-radio">사용안함</label>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </dd>
            </dl>
        </div>

        <div class="btn-wrap mgt40">
            <button type="button" class="btn-base-l type4" id="setBtn"><i>저장</i></button>
            <button type="button" class="btn-base-l type3" id="resetBtn"><i>초기화</i></button>
        </div>

    </form>
</section>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="imageFile" multiple style="display: none;"/>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/partner/sellerShopMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>

<script>
    var hmpImgUrl = '${hmpImgUrl}';
</script>