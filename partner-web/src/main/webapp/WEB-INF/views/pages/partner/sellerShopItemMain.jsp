<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<!-- script -->
<section class="contents">
    <form>
        <div class="float">
            <h2 class="h2 float-l">추천상품 전시관리</h2>
        </div>

        <div class="result-wrap float mgt20">
            <p class="float-l result-p">전체 <b><span id="sellershopItemTotalCount">0</span></b>건</p>
            <div class="float-r">
                <button type="button" class="btn-base" id="addItemPopUp" onclick="itemPop.openModal();"><i>상품등록</i></button>
            </div>
        </div>
        <div class="data-wrap" style="height: 160px" id="sellerShopItemListGrid">
        </div>
        <div class="align-l mgt10">
            <button type="button" class="btn-data-up" key="top" onclick="sellerShopItemMain.moveRow(this);" ><i class="hide">데이터 위로 </i></button>
            <button type="button" class="btn-data-down" key="bottom" onclick="sellerShopItemMain.moveRow(this);" ><i class="hide">데이터 아래로 </i></button>
            <button type="button" class="btn-data-down"  onclick="sellerShopItemMain.removeCheckData();" ><i class="hide">선택 삭제 </i></button>
            <p class="inline-b mgl5 txt">우선순위 기준 노출됩니다.</p>
        </div>

        <div class="tbl-data02">
            <table>
                <caption>대표상품 노출 형식 표</caption>
                <colgroup>
                    <col style="width:180px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">대표상품 노출 형식<i class="bul-require">필수항목</i></th>
                    <td>
                        <span class="radio-group02 ">
                            <input type="radio" id="ex-radio43-1" name="dispType" value="TWO" checked><label for="ex-radio43-1" class="lb-radio mgb0">2개</label>
                            <input type="radio" id="ex-radio43-2" name="dispType" value="FIVE" ><label for="ex-radio43-2" class="lb-radio mgb0">5개</label>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="btn-wrap mgt40">
            <button type="button" class="btn-base-l type4" id="setSellerShopItmBtn"><i>등록</i></button>
            <button type="button" class="btn-base-l type3" id="resetBtn"><i>초기화</i></button>
        </div>
    </form>
</section>
<div class="modal-wrap">
    <jsp:include page="/WEB-INF/views/pages/item/pop/itemPop.jsp" />
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/partner/sellerShopItemMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/common/itemGridCommon.js?${fileVersion}"></script>
<script>

    ${sellerShopItemListGridBaseInfo}

</script>