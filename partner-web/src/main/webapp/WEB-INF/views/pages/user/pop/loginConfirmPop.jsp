<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="loginConfirmPop" class="modal" role="dialog">
    <h2 class="h2">계정 확인</h2>
    <div class="modal-cont" style="height: auto;">
        <p class="fs14 fc03 mgb15">계정확인을 위해 아래 항목을 정확히 입력해 주세요.</p>
        <div class="tbl-form type2">
            <table>
                <caption>영업담당자 계정 확인</caption>
                <colgroup>
                    <col style="width:29.5%">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">로그인ID<i class="bul-require">필수항목</i></th>
                    <td><input type="text" id="partnerIdPop" class="inp-base w-180" maxlength="12" title="로그인ID" readonly></td>
                </tr>
                <tr>
                    <th scope="row">영업담당자 성명<i class="bul-require">필수항목</i></th>
                    <td>
                        <input type="text" id="affiMngNm" class="inp-base" maxlength="10" title="영업담당자 성명">
                    </td>
                </tr>
                <tr>
                    <th scope="row">휴대폰 번호<i class="bul-require">필수항목</i></th>
                    <td class="cell-phone-input">
                        <input type="hidden" name="mobile" id="mobile0"
                               value="">
                        <input type="text" id="mobile0_1"class="inp-base w-50" maxlength="3" title="휴대폰번호"> -
                        <input type="text" id="mobile0_2"class="inp-base w-60" maxlength="4" title="휴대폰번호"> -
                        <input type="text" id="mobile0_3"class="inp-base w-60" maxlength="4" title="휴대폰번호">
                        <button type="button" id="certiCheck" class="btn-base ins-no-send border-type1 w-96"><i>인증번호 발송</i></button>
                        <button type="button" id="reCertiCheck" class="btn-base ins-no-send border-type1 w-96" style="display: none;"><i>재발송</i></button>
                        <button type="button" id="releaseCerti" class="btn-base ins-no-send border-type1 w-96" style="display: none;">변경</button>
                    </td>
                </tr>
                <tr>
                    <th scope="row">인증번호<i class="bul-require">필수항목</i></th>
                    <td>
                        <input type="password" id="certNo" class="inp-base w-114" maxlength="6" title="인증번호 입력">
                        <button type="button" id="certiCheckWar" class="btn-base border-type1"><i>확인</i></button>
                        <span id="spanCertiCheck" class="txt-err type2" style="display: none;">남은 시간 <span id="spanTimeOut" class="remain-ins-time">02:59</span></span>
                        <span id="spanCertiCheckTxt" class="txt-err type2" style="display: none;">인증완료</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="btn-wrap mgt30">
            <button type="button" onclick="loginConfirm.setPartnerPassword()" class="btn-base-l type4"><i>확인</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="loginConfirm.close()" data-focus="focus-loginConfirmPop-close" data-focus-next="focus-loginConfirmPop"><i class="hide">닫기</i></button>
</div>
<div style="display:none;" class="pwdPop" >
    <form id="pwdPopForm" name="pwdPopForm" method="post" action="/user/setPartnerPasswordPop">
        <input type="text" name="partnerId" />
        <input type="text" name="certToken" />
        <input type="text" name="affiMngNm" />
        <input type="text" name="certNo" />
        <input type="text" name="mobile" />
    </form>
</div>
<script>
    $(document).ready(function() {
        loginConfirm.init();
    });

    var loginConfirm = {
        timer       	: 0,
        certToken	: "",		// sms 토큰
        smsSuccessToken	: "",		// sms 성공 토큰
        certNo		: "",
        smsCountdownTimer	: null,
        mobile			: "",
        smsCheck        : false,
        init : function() {
            $('#affiMngNm').allowInput("keyup", ["KOR", "ENG"]);
            loginConfirm.event();
        },
        event : function() {
            // 인증하기
            $("#certiCheck").on("click", function() {
                loginConfirm.mobile = $.jUtil.valNotSplit('mobile0', '-');

                if (loginConfirm.mobileCheck()){
                    loginConfirm.validPartnerPassword(false);
                }
            });

            // 재발송
            $("#reCertiCheck").on("click", function() {
                if (loginConfirm.mobileCheck()) {
                    // interval clear
                    if (loginConfirm.smsCountdownTimer != null) {
                        loginConfirm.smsCountdownTimer.endTimer();
                    }

                    // 인증번호 호출
                    loginConfirm.mobile = $.jUtil.valNotSplit('mobile0', '-');
                    loginConfirm.validPartnerPassword(false);
                }
            });

            // 인증하기 - 완료
            $("#certiCheckWar").on("click", function() {
                if (loginConfirm.mobileCheck()) {
                    if ($("#certiCheck").is(':visible') == true) {
                        return $.jUtil.alert('인증번호 발송을 진행해 주세요.');
                    }

                    if ($("#certNo").val() == "") {
                        return $.jUtil.alert('인증번호를 입력해 주세요.', 'certNo');
                    }

                    if ($("#certNo").val().length != 6) {
                        return $.jUtil.alert('6자리 인증번호를 입력해 주세요.', 'certNo');
                        return;
                    }

                    let mobileNum = $.jUtil.valNotSplit('mobile0', '-');
                    loginConfirm.checkCertified(mobileNum);
                }
            });

            // 인증해제
            $("#releaseCerti").on("click", function () {
                if (!confirm("등록한 휴대폰 번호의 인증을 해제하고 새로운 번호로 등록하시겠습니까?")) {
                    return;
                }
                loginConfirm.resetForm(true);
            });

        },
        valid : function() {

            if ($.jUtil.isEmpty($('#affiMngNm').val())) {
                return $.jUtil.alert('영업담당자 성명을 입력해주세요.', 'affiMngNm');
            }

            if (loginConfirm.smsCheck == false) {
                return $.jUtil.alert('휴대폰 번호 인증을 진행해주세요.', 'mobile0_1');
            }

            return true;
        },
        mobileCheck : function(){
            if ($.jUtil.isEmpty($('#affiMngNm').val())) {
                return $.jUtil.alert('영업담당자 성명을 입력해주세요.');
            }

            if ($("#mobile0_1").val() == "" || $("#mobile0_2").val() == "" || $("#mobile0_3").val() == "") {
                return $.jUtil.alert('휴대폰 번호를 입력해주세요.');
            }
            if($.jUtil.phoneValidate($("#mobile0_1").val() + '-' + $("#mobile0_2").val() + '-' + $("#mobile0_3").val(), true)) {
                return true;
            } else {
                return $.jUtil.alert('휴대폰 번호 형식이 잘못되었습니다. 다시 확인해 주세요.');
            }
        },
        /**
         * 인증번호 생성
         * @param mobile
         */
        setCertified : function(mobile) {
            CommonAjax.basic({
                url:"/common/mobile/certno/send.json?mobile="+mobile
                , method:"GET"
                , callbackFunc:function(res) {
                    if (res.hasOwnProperty("data")) {
                        loginConfirm.certToken = res.data;

                        // 타이머 호출
                        loginConfirm.checkTimer();
                        alert("인증번호가 전송되었습니다.");

                        // 재발송 view
                        $("#certiCheck").hide();
                        $("#reCertiCheck").show();
                        $("#spanCertiCheck").show();
                    }
                }
                , errorCallbackFunc : function() {
                    loginConfirm.smsCountdownTimer.endTimer();

                    // 인증 view
                    $("#certiCheck").show();
                    $("#reCertiCheck").hide();
                    $("#spanCertiCheck").hide();
                }
            });
        },
        /**
         * 인증번호 확인
         * @param mobile
         */
        checkCertified : function(mobile) {
            var param = {
                    certToken: loginConfirm.certToken
                ,   certNo: $("#certNo").val()
                ,   mobile: mobile
            };
            CommonAjax.basic({
                url: "/common/mobile/certno/valid.json"
                , data: JSON.stringify(param)
                , contentType: 'application/json'
                , method:"POST"
                , callbackFunc:function(res) {
                    if (res.hasOwnProperty("data") && res.data == true) {
                        alert("휴대폰 인증에 성공하였습니다.");

                        loginConfirm.smsSuccessToken = res.data;
                        loginConfirm.certNo = $("#certNo").val();
                        loginConfirm.mobile = mobile;
                        loginConfirm.smsCheck = true;
                        loginConfirm.smsCountdownTimer.endTimer();

                        // 휴대폰 번호 입력 활성화
                        $("#mobile0_1").attr("readonly", true);
                        $("#mobile0_2").attr("readonly", true);
                        $("#mobile0_3").attr("readonly", true);
                        $("#certNo").attr("readonly", true);

                        // button view
                        $("#certiCheck").hide();
                        $("#reCertiCheck").hide();
                        $("#releaseCerti, #spanCertiCheckTxt").show();
                        $("#spanCertiCheck").hide();
                    } else {
                        alert(res.returnMessage);
                    }
                }
            });
        },
        /**
         * 남은 시간 카운터
         */
        checkTimer : function() {
            var endTime = moment(new Date()).add(3, 'minutes');
            if(loginConfirm.smsCountdownTimer != null)
            {
                loginConfirm.smsCountdownTimer.endTimer();
            }
            loginConfirm.smsCountdownTimer = countdownTimer($('#spanTimeOut'),endTime,'ms','0:00',function(){
                if (loginConfirm.smsCheck == false) {
                    alert('인증번호 입력시간이 지났습니다. \r\n다시 시도해 주세요.');
                    loginConfirm.smsCheck = false;
                }
            });
        },
        setPartnerPassword : function() {

            if (loginConfirm.valid()) {
                loginConfirm.validPartnerPassword(true);
            }
        },
        validPartnerPassword : function(isClose) {
            CommonAjax.basic({
                url         : "/user/validPartnerPassword.json",
                method      : "POST",
                data        : JSON.stringify({
                    "partnerId" : $('#partnerIdPop').val(),
                    "affiMngNm" : $('#affiMngNm').val(),
                    "affiMobile" : loginConfirm.mobile
                }),
                contentType : "application/json",
                callbackFunc: function (res) {
                    if (res.returnCode == "0") {
                        if (isClose) {
                            windowPopupOpen("", "pwdPopForm", 470, 375);

                            $("#pwdPopForm").find('input[name="partnerId"]').val($('#partnerIdPop').val());
                            $("#pwdPopForm").find('input[name="certToken"]').val(loginConfirm.certToken);
                            $("#pwdPopForm").find('input[name="affiMngNm"]').val($('#affiMngNm').val());
                            $("#pwdPopForm").find('input[name="certNo"]').val(loginConfirm.certNo);
                            $("#pwdPopForm").find('input[name="mobile"]').val(loginConfirm.mobile);

                            $("#pwdPopForm").attr("method", "post")
                            .attr("action", "/user/setPartnerPasswordPop")
                            .attr("target", "pwdPopForm")
                            .submit();

                            loginConfirm.close();
                        } else {
                            // 인증번호 호출
                            var mobileNum = $.jUtil.valNotSplit('mobile0', '-');
                            loginConfirm.setCertified(mobileNum);
                        }
                    } else {
                        return $.jUtil.alert("입력하신 정보를 찾을 수 없습니다.\n관리자에 문의하세요.");
                    }
                },
                errorCallbackFunc: function (res) {
                    return $.jUtil.alert(res.responseJSON.returnMessage);
                }
            });
        },
        resetForm : function(isCerti) {
            // 영업담당자 인증 flag 변경
            loginConfirm.smsCheck = false;
            loginConfirm.certNo = "";


            // 휴대폰 번호 입력 활성화
            $("#mobile0_1").attr("readonly", false);
            $("#mobile0_2").attr("readonly", false);
            $("#mobile0_3").attr("readonly", false);
            $("#certNo").val("").attr("readonly", false);

            // button view
            $("#certiCheck").show();
            $("#reCertiCheck").hide();
            $("#releaseCerti, #spanCertiCheckTxt").hide();
            $("#spanCertiCheck").hide();

            if (!isCerti) {
                $('#loginConfirmPop').find('input').val('');
                $("#partnerIdPop").val($('#partnerId').val());
            }

            if (loginConfirm.smsCountdownTimer != null) {
                loginConfirm.smsCountdownTimer.endTimer();
            }
        },
        close : function(){
            loginConfirm.resetForm();
            $ui.modalClose('loginConfirmPop');
        }
    }
</script>