<!-- 변경주기 팝업 --><%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <div id="passwordExceedPop" class="modal" role="dialog">
        <h2 class="h2">비밀번호 변경</h2>
        <form id="passwordForm" name="passwordForm" method="POST">
            <div class="modal-cont">
                <p class="fs14 fc03 mgb15">90일동안 동일한 비밀번호를 사용중입니다.<br />비밀번호 변경 후 이용하세요.</p>
                <div class="tbl-form">
                    <table>
                        <caption>비밀번호 변경</caption>
                        <colgroup>
                            <col style="width:30%">
                            <col style="width:auto">
                        </colgroup>
                        <tbody>
                        <tr>
                            <th scope="row">로그인ID</th>
                            <td>
                                <p id="passwordExceedId" class="fs14 fc03">${userInfo.partnerId}</p>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">신규 비밀번호<i class="bul-require">필수항목</i></th>
                            <td>
                                <input type="password" name="partnerPw" id="partnerPw" class="inp-base w-180" maxlength="15" title="신규 비밀번호">
                                <span id="passwordTxt" class="txt-err mgt5" style="display: none" >비밀번호 생성규칙을 확인해주세요.</span>
                                <ul class="bul-list">
                                    <li>비밀번호는 10자~15자 이내로 입력가능하며, 아이디와 동일한 문자나 3자 연속 동일문자는 이용불가합니다.</li>
                                    <li>특수문자는 !@#$%^&amp;*만 입력가능합니다.</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">비밀번호 확인<i class="bul-require">필수항목</i></th>
                            <td>
                                <input type="password" id="passwordConfirm" class="inp-base w-180" maxlength="15" title="비밀번호 확인">    <!-- 오류 밸리데이션 노출 시 err 클래스 추가 -->
                                <p id="passwordConfirmTxt" class="mgt5" style="display: none"></p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="btn-wrap mgt30">
                    <button type="button" class="btn-base-l type4" onclick="passwordPop.setPassword()"><i>확인</i></button>
                </div>
            </div>
        </form>
    </div>
<script>

    $(document).ready(function() {
        passwordPop.init();
    });

    var passwordPop = {
        init : function() {
            passwordPop.event();
            // passwordPop.checkMobile();
        },
        event : function() {

            $('#partnerPw').on('focusout', function () {
                passwordPop.checkPassword($(this).val(), null);
                $('#passwordConfirm').trigger('focusout');
            });

            $('#passwordConfirm').on('focusout', function () {
                passwordPop.checkPassword($('#partnerPw').val(), $(this).val());
            });
        },
        valid : function() {
            if ($.jUtil.isEmpty($('#partnerPw').val())
                    || $.jUtil.isEmpty($('#passwordConfirm').val())
            ) {
                return $.jUtil.alert('비밀번호를 입력해주세요.');
            }

            if ($('.txt-err').length > 0) {
                return $.jUtil.alert('비밀번호를 확인해주세요.');
            }

            return true;
        },
        reset : function() {
            $('#newPassword, #partnerPw, #passwordConfirm').val('');
            $('#partnerPw, #passwordConfirm').removeClass('err');
            $('#passwordTxt, #passwordConfirmTxt').removeClass('txt-err').removeClass('txt-success').hide();
        },
        checkOriginPassword : function(password, func) {
            if (password) {
                CommonAjax.basic({
                    url: "/partner/getCheckOriginPassword.json",
                    data: JSON.stringify({password: password}),
                    method: "POST",
                    contentType: "application/json",
                    callbackFunc: function (res) {
                        func(res);
                    }
                });
            }
        },
        checkPassword: function (password, passwordConfirm) {
            if (passwordConfirm != null) {
                if (passwordConfirm) {
                    if (password == passwordConfirm) {
                        $('#passwordConfirm').removeClass('err');
                        $('#passwordConfirmTxt').removeClass('txt-err').addClass('txt-success').text(
                                '일치합니다.').show();
                    } else {
                        $('#passwordConfirm').addClass('err');
                        $('#passwordConfirmTxt').removeClass('txt-success').addClass('txt-err').text(
                                '일치하지 않습니다.').show();
                    }
                }
            } else if (password) {
                let msgPassword = '';
                let num = password.search(/[0-9]/g);
                let eng = password.search(/[a-z]/ig);
                let spe = password.search(/[!@#%&\\*\\^\\$]/gi);

                if (!$.jUtil.isAllowInput(password, ['ENG_LOWER', 'NUM', 'PASSWORD'])
                        || /(.)\1\1/.test(password)
                        || password.length < 10 || password.length > 15) {
                    msgPassword = '비밀번호 생성규칙을 확인해주세요.';
                } else if( (num < 0 && eng < 0) || (eng < 0 && spe < 0) || (spe < 0 && num < 0) ) {
                    msgPassword = "영문,숫자, 특수문자 중 2가지 이상을 혼합하여 입력해주세요.";
                }

                if (msgPassword == '') {
                    passwordPop.checkOriginPassword(password, function (isOriginPw) {
                        if (isOriginPw) {
                            $('#partnerPw').addClass('err');
                            $('#passwordTxt').removeClass('txt-success').addClass('txt-err').text(
                                    '이전 비밀번호와 동일한 비밀번호로 설정할 수 없습니다. 다시 입력해주세요.').show();
                        } else {
                            $('#partnerPw').removeClass('err');
                            $('#passwordTxt').removeClass('txt-err').addClass('txt-success').text(
                                    '사용 가능합니다.').show();
                        }
                    });
                } else {
                    $('#partnerPw').addClass('err');
                    $('#passwordTxt').removeClass('txt-success').addClass('txt-err').text(msgPassword).show();
                }

            }
        },
        setPassword : function() {
            if (passwordPop.valid()) {
                let passwordForm = $('#passwordForm').serializeObject(true);

                CommonAjax.basic({
                    url: '/user/setPartnerPasswordExceed.json',
                    data: JSON.stringify(passwordForm),
                    type: 'post',
                    contentType: 'application/json',
                    method:"POST",
                    successMsg:null,
                    callbackFunc:function(res) {
                        alert(res.returnMsg);
                        passwordPop.modalClose();
                    }
                });
            }
        },
        modalClose : function () {
            passwordPop.reset();
            $ui.modalClose('passwordExceedPop');
        }
    }
</script>