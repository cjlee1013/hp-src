<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section class="contents">
    <h2 class="h2">회원정보 조회/수정</h2>
    <p class="mgt20">판매사의 정보를 안전하게 보호하기 위해 비밀번호를 다시 한 번 확인합니다.</p>
    <form onsubmit="return false;">
        <div class="tbl-form mgt10">
            <table>
                <caption>회원정보 확인</caption>
                <colgroup>
                    <col style="width:180px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">아이디</th>
                    <td>${partnerId}</td>
                </tr>
                <tr>
                    <th scope="row"><label for="userPw">비밀번호</label></th>
                    <td>
                        <input type="password" id="userPw" class="inp-base" formmethod="post" autocomplete="off">
                        <p id="userPwErr" class="txt-err" style="display: none;">비밀번호가 일치하지 않습니다.</p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="btn-wrap mgt40">
            <button type="submit" class="btn-base-l type4" onclick="user.checkPartnerPassword();"><i>확인</i></button>
            <button type="button" class="btn-base-l" onclick="user.setCancel();"><i>취소</i></button>
        </div>
    </form>
</section>
<script>
    let user = {
        checkPartnerPassword : function() {
            let userPw = $('#userPw').val();
            if (userPw.length < 6 || 15 < userPw.length) {
                return $.jUtil.alert('비밀번호 6에서 15자리 이하로 입력해주세요.');
            }

            CommonAjax.basic({
                url: "/partner/getCheckOriginPassword.json",
                data: JSON.stringify({password: $('#userPw').val()}),
                method: "POST",
                contentType: "application/json",
                callbackFunc: function (res) {
                    if (res) {
                        $.jUtil.redirectPost("/user/modify",  { partnerId: '${partnerId}', partnerPw:$('#userPw').val() });
                    } else {
                        $('#userPw').addClass('err');
                        $('#userPwErr').show();
                    }
                }
            });
        },
        setCancel : function () {
            $('.dhxtabbar_tab_text_close', parent.document).each(function(){
                if ($(this).text() == '회원정보 조회/수정') {
                    $(this).siblings('.dhxtabbar_tab_close').trigger('click');
                }
            });
        }
    }
</script>

