<%@ page import="kr.co.homeplus.partner.web.core.dto.UserInfo" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="passwordPop" class="modal" role="dialog">
    <h2 class="h2">비밀번호 변경</h2>

    <div class="modal-cont">
        <div class="tbl-form">
            <table>
                <caption>비밀번호 변경</caption>
                <colgroup>
                    <col style="width:30%">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">기존 비밀번호</th>
                    <td>
                        <input type="password" name="originPasswordPop" id="originPasswordPop" class="inp-base" title="기존 비밀번호">
                        <p id="originPasswordConfirmTxt" style="display: none">일치합니다.</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row">신규 비밀번호</th>
                    <td>
                        <input type="password" name="newPasswordPop" id="newPasswordPop"
                               class="inp-base w-260" title="신규비밀번호 입력" maxlength="20">
                        <!-- 기준 미준수시 클래스 err-->
                        <p id="passwordTxt" style="display: none">비밀번호 생성규칙을 확인해주세요.</p>

                        <ul class="bul-list mgt10">
                            <li>비밀번호는 10자~15자 이내로 입력가능하며, 아이디와 동일한 문자나 3자 연속 동일문자는 이용불가합니다.</li>
                            <li>특수문자는 <span class="txt-underline">!@#$%^&*</span>만 입력가능합니다.</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th scope="row">비밀번호 확인</th>
                    <td>
                        <input type="password" id="passwordConfirm" class="inp-base w-260"
                               title="비밀번호 확인">
                        <p id="passwordConfirmTxt" style="display: none">비밀번호 생성규칙을 확인해주세요.</p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4" onclick="passwordPop.setPassword()"><i>확인</i></button>
            <button type="button" class="btn-base-l" onclick="passwordPop.modalClose()"><i>취소</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="passwordPop.modalClose()"><i class="hide" >닫기</i></button>
</div>
<script>

    $(document).ready(function() {
        passwordPop.init();
    });

    var passwordPop = {
        init : function() {
            passwordPop.event();
        },
        event : function() {
            $('#originPasswordPop').on('focusout', function () {
                passwordPop.checkOriginPassword($(this).val());
            });

            $('#newPasswordPop').on('focusout', function () {
                passwordPop.checkPassword($(this).val(), null);
                $('#passwordConfirm').trigger('focusout');
            });

            $('#passwordConfirm').on('focusout', function () {
                passwordPop.checkPassword($('#newPasswordPop').val(), $(this).val());
            });
        },
        valid : function() {
            if ($.jUtil.isEmpty($('#originPasswordPop').val())
                    || $.jUtil.isEmpty($('#newPasswordPop').val())
                    || $.jUtil.isEmpty($('#passwordConfirm').val())
            ) {
                return $.jUtil.alert('비밀번호를 입력해주세요.');
            }

            if ($('.txt-err').length > 0) {
                return $.jUtil.alert('비밀번호를 확인해주세요.');
            }

            return true;
        },
        reset : function() {
            $('#originPassword, #newPassword, #originPasswordPop, #newPasswordPop, #passwordConfirm').val('');
            $('#originPasswordConfirm, #newPasswordPop, #passwordConfirm').removeClass('err');
            $('#originPasswordConfirmTxt, #passwordTxt, #passwordConfirmTxt').removeClass('txt-err').removeClass('txt-success').hide();
        },
        checkOriginPassword : function(password) {
            if (password) {
                CommonAjax.basic({
                    url: "/partner/getCheckOriginPassword.json",
                    data: JSON.stringify({password: password}),
                    method: "POST",
                    contentType: "application/json",
                    callbackFunc: function (res) {
                        if (res) {
                            $('#originPasswordConfirm').removeClass('err');
                            $('#originPasswordConfirmTxt').removeClass('txt-err').addClass('txt-success').text(
                                    '일치합니다.').show();
                            passwordPop.checkPassword($('#newPasswordPop').val(), null);
                        } else {
                            $('#originPasswordConfirm').addClass('err');
                            $('#originPasswordConfirmTxt').removeClass('txt-success').addClass('txt-err').text(
                                    '일치하지 않습니다.').show();
                        }
                    }
                });
            } else {
                $('#originPasswordConfirm').addClass('err');
                $('#originPasswordConfirmTxt').removeClass('txt-success').addClass('txt-err').text(
                        '일치하지 않습니다.').show();
            }
        },
        checkPassword: function (password, passwordConfirm) {
            if (passwordConfirm != null) {
                if (passwordConfirm) {
                    if (password == passwordConfirm) {
                        $('#passwordConfirm').removeClass('err');
                        $('#passwordConfirmTxt').removeClass('txt-err').addClass('txt-success').text(
                                '일치합니다.').show();
                    } else {
                        $('#passwordConfirm').addClass('err');
                        $('#passwordConfirmTxt').removeClass('txt-success').addClass('txt-err').text(
                                '일치하지 않습니다.').show();
                    }
                }
            } else if (password) {
                let msgPassword = '';
                let num = password.search(/[0-9]/g);
                let eng = password.search(/[a-z]/ig);
                let spe = password.search(/[!@#%&\\*\\^\\$]/gi);
                let checkPassword = false;

                CommonAjax.basic({
                    url: "/partner/getCheckPassword.json",
                    data: JSON.stringify({password: password}),
                    method: "POST",
                    contentType: "application/json",
                    callbackFunc: function (res) {
                        checkPassword = res;

                        if (!$.jUtil.isAllowInput(password, ['ENG_LOWER', 'NUM', 'PASSWORD'])
                                || checkPassword
                                || /(.)\1\1/.test(password)
                                || password.length < 10 || password.length > 15) {
                            msgPassword = '비밀번호 생성규칙을 확인해주세요.';
                        } else if ($('#originPasswordConfirmTxt').hasClass('txt-success') == true && $('#originPasswordPop').val() == $('#newPasswordPop').val()) {
                            msgPassword = '이전 비밀번호와 동일한 비밀번호로 설정할 수 없습니다. 다시 입력해주세요.';
                        } else if( (num < 0 && eng < 0) || (eng < 0 && spe < 0) || (spe < 0 && num < 0) ) {
                            msgPassword = "영문,숫자, 특수문자 중 2가지 이상을 혼합하여 입력해주세요.";
                        }

                        if (msgPassword) {
                            $('#newPasswordPop').addClass('err');
                            $('#passwordTxt').removeClass('txt-success').addClass('txt-err').text(
                                    msgPassword).show();
                        } else {
                            $('#newPasswordPop').removeClass('err');
                            $('#passwordTxt').removeClass('txt-err').addClass('txt-success').text(
                                    '사용 가능 합니다.').show();
                        }
                    }
                });
            } else {
              $('#passwordTxt').removeClass('txt-err').removeClass('txt-success').text('');
            }
        },
        setPassword : function() {
            if (passwordPop.valid()) {
                $('#originPassword').val($('#originPasswordPop').val());
                $('#newPassword').val($('#newPasswordPop').val());
                alert('비밀번호가 정상 변경되었습니다.');
                $ui.modalClose('passwordPop');
            }
        },
        modalClose : function () {
            passwordPop.reset();
            $ui.modalClose('passwordPop');
        }
    }
</script>