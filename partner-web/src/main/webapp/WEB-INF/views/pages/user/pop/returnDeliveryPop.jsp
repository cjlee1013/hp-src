<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="returnDeliveryPop" class="modal type-l" role="dialog">
    <h2 class="h2">반품 택배사 계약 정보</h2>

    <div class="modal-cont">
        <div class="tbl-data">
            <table>
                <caption>회원 정보</caption>
                <colgroup>
                    <col style="width:80px">
                    <col style="width:auto">
                    <col style="width:80px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">ID</th>
                    <td id="returnPopPartnerId"></td>
                    <th scope="row">상호</th>
                    <td>홈플러스㈜</td>
                </tr>
                </tbody>
            </table>
        </div>

        <h3 class="h4 mgt30">저장 시점에 바로 반영됩니다.</h3>
        <div class="tbl-data tbl-line">
            <table id="deliveryTb">
                <caption>회원 정보</caption>
                <colgroup>
                    <col style="width:104px">
                    <col style="width:auto">
                </colgroup>
                <thead>
                <tr>
                    <th scope="col">택배사</th>
                    <th scope="col">계약코드</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="btn-wrap mgt30">
            <button type="button" id="setDeliveryBtn" class="btn-base-l type4"><i>확인</i></button>
            <button type="button" id="initCreditCdAll" class="btn-base-l type3"><i>초기화</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="$ui.modalClose('returnDeliveryPop')"><i class="hide" >닫기</i></button>
</div>

<script>
    let deliveryList = ${deliveryList};
    let deliveryInfo = ${deliveryInfo};

    $(document).ready(function() {
        delivery.init();
    });

    let delivery = {
        init : function() {
            delivery.drawDeliveryList();
            delivery.event();
        },
        event : function() {

            $(document).on("click", "button[id^=getCreditCdBtn_]", function() {
                delivery.getCreditCd($(this).data("idx"));
            });

            // 초기화
            $(document).on("click", "button[id^=initCreditCdBtn_]", function() {
                delivery.initCreditCd($(this).data("idx"));
            });

            $('#initCreditCdAll').on("click", function() {
                delivery.initCreditCd(null);
            });
            // 적용 버튼
            $("#setDeliveryBtn").on("click", function() {
                delivery.setDelivery();
            });
        },
        /**
         * 택배사 정보 그리기
         */
        drawDeliveryList : function () {
            if (deliveryList.length > 0) {
                var html = "";
                for (var idx in deliveryList) {
                    var data = deliveryList[idx];

                    html += "<tr id='tr_" + data.dlvCd + "' data-index='" + idx + "'>";
                    html += "    <td>" + data.dlvNm + "</td>";
                    html += "    <td class='pdr0'>";
                    if (data.dlvCd == '004') { //우체국택배
                        html += "   <input type='text' id='creditCd_" + idx + "' class=\"inp-base\" readonly placeholder='우체국 점포코드'>";
                        html += "   <input type='hidden' id='dlvCd_" + idx + "' value='" + data.dlvCd + "'>";
                        html += "   <button type='button' class=\"btn-base type2\" id='postCdRequestBtn'><i>이용신청</i></button>";
                        html += "   <div class='mg-l-60 mg-t-10' style='display:none' id='postStatusDiv'></div>";
                    } else {
                        html += "   <input type='text' id='creditCd_" + idx + "' class=\"inp-base\" maxlength='12'>";
                        html += "   <input type='hidden' id='dlvCd_" + idx + "' value='" + data.dlvCd + "'>";
                        html += "   <input type='hidden' id='creditFlag_" + idx + "' value=false>";
                        html += "   <button type='button' class=\"btn-base type2\" id='getCreditCdBtn_" + idx + "' data-idx='" + idx + "'><i>인증</i></button>";
                        html += "   <button type='button' class=\"btn-base\" id='initCreditCdBtn_" + idx + "' data-idx='" + idx + "'><i>초기화</i></button>";
                    }
                    html += "    </td>";
                    html += "</tr>";
                }
                $("#deliveryTb > tbody:last").html(html);


                if (deliveryInfo != null && deliveryInfo.length > 0) {
                    for (var idx in deliveryInfo) {
                        var info = deliveryInfo[idx];
                        var targetTrIndex = $('#tr_'+info.dlvCd).data("index");
                        $("#creditCd_" + targetTrIndex).val(info.creditCd).prop("readonly", true);
                        $("#creditFlag_" + targetTrIndex).val("true");
                    }
                }
            }
        },
        /**
         * 택배사 인증
         * @param idx
         */
        getCreditCd : function (idx) {
            if ($("#creditCd_" + idx).val() == "") {
                alert("택배사 계약코드를 입력하세요.");
                return;
            }

            CommonAjax.basic({
                url:"/partner/pop/getCompanyCreditValidInfo.json"
                , data:{dlvCd:$("#dlvCd_" + idx).val(), creditCd:$("#creditCd_" + idx).val()}
                , method : "GET"
                , callbackFunc:function(res) {
                    if (res[0].success != "Y"){
                        $("#creditFlag_" + idx).val("false");
                        alert("유효하지 않은 코드 입니다.");
                        $("#creditCd_" + idx).attr("readOnly", false);
                    }
                    else {
                        $("#creditFlag_" + idx).val("true");
                        alert("유효한 계약 코드 입니다.");
                        $("#creditCd_" + idx).attr("readOnly", true);
                    }
                }
            });
        },
        // 초기화
        initCreditCd : function (idx) {
            if (idx) {
                $("#creditCd_" + idx).val("");
                $("#creditFlag_" + idx).val("false");
                $("#creditCd_" + idx).prop("readOnly", false);
            } else {
                $('[id^=creditCd_]').val('').prop("readOnly", false);
                $('[id^=creditFlag_]').val('');
                $("#creditCd_3").prop("readOnly", true);
            }

        },
        // 적용
        setDelivery : function () {
            var creditCheck = true;
            var dataArray = new Array();

            if (deliveryList.length > 0) {
                for (var idx in deliveryList) {
                    if ($("#creditCd_" + idx).val() != "" && $("#creditFlag_" + idx).val() == "false") {
                        creditCheck = false;
                        break;
                    }

                    var dataObj = new Object();

                    if ($("#creditCd_" + idx).val() != "") {
                        dataObj.dlvCd = $("#dlvCd_" + idx).val();
                        dataObj.creditCd = $("#creditCd_" + idx).val();

                        dataArray.push(dataObj);
                    }
                }

                if (!creditCheck) {
                    alert("유효성을 확인해 주세요.");
                    return;
                }

                if (!confirm("적용하시겠습니까?")) {
                    return;
                }

                partner.deliveryInfo = dataArray;
                $ui.modalClose('returnDeliveryPop');
            }
        },
    }
</script>