<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="bankAccountPop" class="modal" role="dialog">
    <h2 class="h2">계좌번호 변경</h2>

    <div class="modal-cont">
        <p class="fs14 fc03 mgb15">예금주명은 사업자번호의 대표명과 동일해야 합니다.</p>
        <div class="tbl-form">
            <table>
                <caption>계좌번호 변경</caption>
                <colgroup>
                    <col style="width:120px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">계좌정보</th>
                    <td>
                        <select id="bankCdPop" class="slc-base" title="계좌은행">
                            <option value="">은행선택</option>
                            <c:forEach var="code" items="${bankCode}" varStatus="status">
                                <option value="${code.mcCd}">${code.mcNm}</option>
                            </c:forEach>
                        </select>
                        <input type="text" id="bankAccountNoPop" class="inp-base w-150 mgt5" maxlength="70" title="계좌번호" placeholder="계좌번호 입력 ('-'없이 입력)">
                        <button type="button" id="getAccAuthenticationBtn" class="btn-base mgt5"><i id="getAccAuthenticationBtnTxt">계좌인증</i></button>
                    </td>
                </tr>
                <tr>
                    <th scope="row">예금주명</th>
                    <td>
                        <input type="text" id="depositorPop" class="inp-base w-120" maxlength="70" title="예금주" readonly>
                        <span id="bankStatus"  class="fc02 fs12 mgl10">인증대기</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="btn-wrap mgt30">
            <button type="button" id="setBankAccountBtn" class="btn-base-l type4"><i>확인</i></button>
            <button type="button" class="btn-base-l" onclick="$ui.modalClose('bankAccountPop')"><i>취소</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="$ui.modalClose('bankAccountPop')"><i class="hide" >닫기</i></button>
</div>

<script>

    $(document).ready(function() {
        bankAccount.init();
    });

    let bankAccount = {
        isCheckBankAccnt : false,
        init : function() {
            bankAccount.event();
        },
        event : function() {

            $("#bankAccountNoPop").allowInput("keyup", ["NUM"]);

            // 적용 버튼
            $("#setBankAccountBtn").on("click", function() {
                bankAccount.setBankAccount();
            });

            $('#getAccAuthenticationBtn').on('click', function() {
                if (bankAccount.isCheckBankAccnt) {
                    bankAccount.isCheckBankAccnt = false;
                    $('#getAccAuthenticationBtnTxt').text('계좌인증');
                    $('#bankStatus').text('인증대기');
                    $('#bankCdPop, #bankAccountNoPop').prop('disabled', false).val('');
                    $('#depositorPop').val('');
                } else {
                    bankAccount.getBankAccntNoCertify();
                }
            });
        },
        /**
         * 계좌실명 인증 조회
         * @returns {*|boolean}
         */
        getBankAccntNoCertify : function() {
            if ($.jUtil.isEmpty($("#bankCdPop").val())) {
                return $.jUtil.alert('은행을 선택해주세요.', 'bankCd');
            }

            if ($.jUtil.isEmpty($("#bankAccountNoPop").val())) {
                return $.jUtil.alert('계좌번호를 입력해주세요.', 'bankAccountNo');
            }

            CommonAjax.basic({
                url: "/partner/getBankAccntNoCertify.json",
                data: {
                    bankCd: $("#bankCdPop").val(),
                    bankAccntNo: $("#bankAccountNoPop").val(),
                    depositor: $("#depositorPop").val()
                },
                method: "POST",
                callbackFunc: function (res) {
                    if ($.jUtil.isNotEmpty(res.depositor)) {
                        $('#depositorPop').val(res.depositor);
                        alert('인증되었습니다.');
                        $('#bankStatus').text('인증완료');
                        $('#getAccAuthenticationBtnTxt').text('재설정');
                        $('#bankCdPop, #bankAccountNoPop').prop('disabled', true);
                    } else {
                        $('#depositorPop').val('');
                        alert('인증 실패하였습니다.');
                        $('#bankStatus').text('인증대기');
                    }

                    bankAccount.isCheckBankAccnt = res;
                }
            });
        },
        setBankAccount : function() {
            if (!bankAccount.isCheckBankAccnt) {
                return $.jUtil.alert('입금계좌의 계좌실명인증을 완료해주세요.', 'depositor');
            }

            $('#bankCd').val($("#bankCdPop").val());
            $('input[name="settleInfo.bankAccountNo"]').val($("#bankAccountNoPop").val());
            $('input[name="settleInfo.depositor"]').val($("#depositorPop").val());

            $('#bankArea').text($("#bankCdPop option:selected").text() + ' ' + $("#bankAccountNoPop").val() + '(예금주 : ' + $("#depositorPop").val() + ')');

            $('#bankCdPop, #bankAccountNoPop, #depositorPop').val("");
            $('#bankStatus').text('인증대기');
            bankAccount.isCheckBankAccnt = false;

            $.jUtil.alert('적용되었습니다.');
            $ui.modalClose('bankAccountPop');
        }
    }
</script>