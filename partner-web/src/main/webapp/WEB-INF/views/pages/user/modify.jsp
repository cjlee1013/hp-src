<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/countdown.js?v=${fileVersion}"></script>
<section class="contents">
    <h2 class="h2">회원정보 조회/수정</h2>
    <form id="sellerForm">
        <h3 class="h3 mgt30">기본정보</h3>
        <div class="tbl-form mgt10">
            <table>
                <caption>기본정보</caption>
                <colgroup>
                    <col style="width:200px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">판매업체 아이디<i class="bul-require">필수항목</i></th>
                    <td>${partnerId}</td>
                </tr>
                <tr>
                    <th scope="row">비밀번호 입력<i class="bul-require">필수항목</i></th>
                    <td>
                        <input type="hidden" name="originPassword" id="originPassword">
                        <input type="hidden" name="newPassword" id="newPassword">
                        <button type="button" id="passwordPopBtn" class="btn-base"><i>변경</i>
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <h3 class="h3 mgt30">사업자정보</h3>
        <div class="tbl-form mgt10">
            <table>
                <caption>사업자정보</caption>
                <colgroup>
                    <col style="width:200px">
                    <col style="width:450px">
                    <col style="width:200px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">사업자등록번호<i class="bul-require">필수항목</i></th>
                    <td id="partnerNo"></td>
                    <th scope="row">통신판매업신고번호<i class="bul-require">필수항목</i></th>
                    <td id="communityNotiNo"></td>
                </tr>
                <tr>
                    <th scope="row">상호<i class="bul-require">필수항목</i></th>
                    <td colspan="3" id="partnerNm"></td>
                </tr>
                <tr>
                    <th scope="row">판매업체명<i class="bul-require">필수항목</i></th>
                    <td colspan="3" id="businessNm"></td>
                </tr>
                <tr>
                    <th scope="row">대표자명<i class="bul-require">필수항목</i></th>
                    <td colspan="3" id="partnerOwner"></td>
                </tr>
                <tr>
                    <th scope="row">업태<i class="bul-require">필수항목</i></th>
                    <td id="businessConditions"></td>
                    <th scope="row">종목<i class="bul-require">필수항목</i></th>
                    <td id="bizCateCdNm"></td>
                </tr>
                <tr>
                    <th scope="row">주소<i class="bul-require">필수항목</i></th>
                    <td colspan="3" id="addr"></td>
                </tr>
                </tbody>
            </table>
        </div>

        <h3 class="h3 mgt30">파트너정보</h3>
        <div class="tbl-form mgt10">
            <table>
                <caption>파트너정보</caption>
                <colgroup>
                    <col style="width:180px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">대표연락처<i class="bul-require">필수항목</i></th>
                    <td>
                        <input type="hidden" name="sellerInfo.phone1" id="phone1" value="">
                        <input type="text" name="phone1_1" id="phone1_1" class="inp-base w-80 checkVal" title="대표연락처" maxlength="4">-
                        <input type="text" name="phone1_2" id="phone1_2" class="inp-base w-80 checkVal" title="대표연락처" maxlength="4">-
                        <input type="text" name="phone1_3" id="phone1_3" class="inp-base w-80 checkVal" title="대표연락처" maxlength="4">
                    </td>
                </tr>
                <tr>
                    <th scope="row">대표 이메일<i class="bul-require">필수항목</i></th>
                    <td>
                        <div class="inp-mail">
                            <input type="text" class="inp-base w-260 checkVal" name="sellerInfo.email" id="email">
                        </div>
                        <p class="bul-p mgt10">홈플러스 상품상세, 주문내역 등에 노출됩니다. 고객응대가 가능한 정확한 정보를 입력해 주시기 바랍니다.  </p>
                    </td>
                </tr>
                <tr>
                    <th scope="row">마케팅정보 수신동의<i class="bul-require">필수항목</i></th>
                    <td>
                        <div>
                            <span class="radio-tit">SMS 수신동의</span>
                            <span class="radio-group size2">
                                <input type="radio" id="lbAgree01_01" name="sellerInfo.smsYn" value="Y"><label for="lbAgree01_01" class="lb-radio">동의</label>
                                <input type="radio" id="lbAgree01_02" name="sellerInfo.smsYn" value="N"><label for="lbAgree01_02" class="lb-radio">동의안함</label>
                            </span>
                        </div>
                        <div class="mgt20">
                            <span class="radio-tit">이메일 수신동의</span>
                            <span class="radio-group size2">
                                <input type="radio" id="lbAgree02_01" name="sellerInfo.emailYn" value="Y"><label for="lbAgree02_01" class="lb-radio">동의</label>
                                <input type="radio" id="lbAgree02_02" name="sellerInfo.emailYn" value="N"><label for="lbAgree02_02" class="lb-radio">동의안함</label>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">고객문의 연락처<i class="bul-require">필수항목</i></th>
                    <td>
                        <input type="hidden" name="sellerInfo.infoCenter" id="infoCenter"
                               value="">
                        <input type="text" name="infoCenter_1" id="infoCenter_1"
                               class="inp-base w-80 checkVal" title="고객문의 연락처" maxlength="3"> -
                        <input type="text" name="infoCenter_2" id="infoCenter_2"
                               class="inp-base w-80 checkVal" title="고객문의 연락처" maxlength="4"> -
                        <input type="text" name="infoCenter_3" id="infoCenter_3"
                               class="inp-base w-80 checkVal" title="고객문의 연락처" maxlength="4">

                        <p class="bul-p mgt10">배송, 환불 등 고객문의 대응의 목적으로 고객에게 제공됩니다. 고객 응대가 가능한 정확한 정보를 입력해 주시기 바랍니다.</p>
                    </td>
                </tr>
                <c:if test="${saleType ne 'D'}">
                <tr>
                    <th scope="row">API Key 발급</th>
                    <td>
                        <select id="agencyCd" name="agencyCd" class="slc-base w-120">
                            <option value="">선택하세요</option>
                            <c:forEach var="code" items="${plusapiAgencyCd}" varStatus="status">
                                <option value="${code.mcCd}">${code.mcNm}</option>
                            </c:forEach>
                        </select>

                        <input type="text" id="apiKey" class="inp-base w-325" readonly>
                        <button type="button" class="btn-base mgl5" onclick="partner.setApiKey();"><i id="apiKeyTxt">발급</i></button>
                        <span class="inline-b mgl5 fs14">(발급일 : <span id="apiKeyRegDt"></span>)</span>
                    </td>
                </tr>
                </c:if>
                <tr>
                    <th scope="row">반품택배사</th>
                    <td>
                        <span class="radio-group size2">
                            <input type="radio" id="lbAgree11_01" name="sellerInfo.returnDeliveryYn" value="Y"><label for="lbAgree11_01" class="lb-radio">있음</label>
                            <input type="radio" id="lbAgree11_02" name="sellerInfo.returnDeliveryYn" value="N" checked><label for="lbAgree11_02" class="lb-radio">없음</label>
                        </span>
                        <button type="button" id="returnDeliveryPopBtn" class="btn-base mgl10"><i>반품택배사 계약정보</i></button>
                    </td>
                </tr>
                <tr>
                    <th scope="row">대표 카테고리</th>
                    <td>
                        <select title="대표 카테고리" name="sellerInfo.categoryCd" id="categoryCd" class="slc-base w-325" data-default="대표 카테고리">
                            <option value="">선택하세요</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row">도서/공연 소득공제</th>
                    <td>
                        <span class="radio-group size2 mgr20">
                            <input type="radio" id="lbAgree03_01" name="sellerInfo.cultureDeductionYn" value="Y"><label for="lbAgree03_01" class="lb-radio">제공</label>
                            <input type="radio" id="lbAgree03_02" name="sellerInfo.cultureDeductionYn" value="N"><label for="lbAgree03_02" class="lb-radio">제공안함</label>
                        </span>
                        <label for="cultureDeductionNo" class="mgr10">사업자 식별번호</label>
                        <input type="text" name="sellerInfo.cultureDeductionNo" id="cultureDeductionNo" name="sellerInfo.cultureDeductionNo" class="inp-base" maxlength="30">
                    </td>
                </tr>
                <tr>
                    <th scope="row">홈페이지</th>
                    <td>
                        <input type="text" name="sellerInfo.homepage" id="homepage" class="inp-base w-max" title="홈페이지" value="http://" maxlength="100">
                    </td>
                </tr>
                <tr>
                    <th scope="row">회사 및 상품소개</th>
                    <td>
                        <input type="text" name="sellerInfo.companyInfo" id="companyInfo" class="inp-base w-max" title="회사 및 상품소개" maxlength="50">
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="tit-wrap mgt30">
            <h3 class="h3">담당자정보 </h3>
            <div class="tit-chk">
                <input type="checkbox" id="copyAffi"><label for="copyAffi" class="lb-check fs12">각 담당자는 영업 담당자와 동일합니다.</label>
            </div>
        </div>
        <div class="tbl-form mgt10">
            <table>
                <caption>담당자정보</caption>
                <colgroup>
                    <col style="width:180px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr id="managerDiv0" class="managerDiv" data-mng-cd="AFFI">
                    <th scope="row">영업담당자</th>
                    <td>
                        <dl class="inp-dl">
                            <dt><span>담당자명<i class="bul-require">필수항목</i></span></dt>
                            <dd>
                                <input type="hidden" name="managerList[].seq" id="seq0">
                                <input type="hidden" name="managerList[].mngCd" id="mngCd0"
                                       value="AFFI">
                                <input type="text" name="managerList[].mngNm" id="mngNm0"
                                       class="inp-base checkVal" title="담당자명">
                            </dd>
                        </dl>
                        <dl class="inp-dl">
                            <dt><span>이메일<i class="bul-require">필수항목</i></span></dt>
                            <dd class="inp-mail">
                                <input type="text" name="managerList[].mngEmail" id="mngEmail0" class="inp-base w-260 checkVal" title="대표 이메일 주소">
                            </dd>
                        </dl>
                        <dl class="inp-dl">
                            <dt><span>휴대폰번호<i class="bul-require">필수항목</i></span></dt>
                            <dd>
                                <input type="hidden" name="managerList[].mngMobile" id="mngMobile0"
                                       value="">
                                <input type="text" name="mngMobile0_1" id="mngMobile0_1"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="3" readonly> -
                                <input type="text" name="mngMobile0_2" id="mngMobile0_2"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="4" readonly> -
                                <input type="text" name="mngMobile0_3" id="mngMobile0_3"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="4" readonly>

                                <button type="button" id="certiCheck" class="btn-base" style="display: none;">인증</button>
                                <button type="button" id="reCertiCheck" class="btn-base" style="display: none;">재발송</button>
                                <button type="button" id="releaseCerti" class="btn-base" >변경</button>
                            </dd>
                        </dl>
                        <dl class="mgt15" id="divCertiCheck" style="display: none;">
                            <div class="inp_wrap">
                                <input type="text" id="checkInfo" name="" class="inp-base w-120" />
                                <button type="button" id="certiCheckWar" class="btn-base">인증하기</button>
                            </div>
                            <p class="validation_txt mgt15">인증번호를 입력해주세요. 남은시간 <span id="spanTimeOut" class="fc02 fs12 mgl10">02:59</span></p>
                        </dl>
                        <dl class="inp-dl">
                            <dt><span>연락처</span></dt>
                            <dd>
                                <input type="hidden" name="managerList[].mngPhone" id="mngPhone0"
                                       value="">
                                <input type="text" name="mngPhone0_1" id="mngPhone0_1"
                                       class="inp-base w-80" title="연락처" maxlength="3"> -
                                <input type="text" name="mngPhone0_2" id="mngPhone0_2"
                                       class="inp-base w-80" title="연락처" maxlength="4"> -
                                <input type="text" name="mngPhone0_3" id="mngPhone0_3"
                                       class="inp-base w-80" title="연락처" maxlength="4">
                            </dd>
                        </dl>
                    </td>
                </tr>
                <tr id="managerDiv1" class="managerDiv" data-mng-cd="SETT">
                    <th scope="row">정산담당자</th>
                    <td>
                        <dl class="inp-dl">
                            <dt><span>담당자명<i class="bul-require">필수항목</i></span></dt>
                            <dd>
                                <input type="hidden" name="managerList[].seq" id="seq1">
                                <input type="hidden" name="managerList[].mngCd" id="mngCd1"
                                       value="SETT">
                                <input type="text" name="managerList[].mngNm" id="mngNm1"
                                       class="inp-base checkVal" title="담당자명">
                            </dd>
                        </dl>
                        <dl class="inp-dl">
                            <dt><span>이메일<i class="bul-require">필수항목</i></span></dt>
                            <dd class="inp-mail">
                                <input type="text" name="managerList[].mngEmail"
                                       id="mngEmail1" class="inp-base w-260 checkVal" title="대표 이메일 주소">
                            </dd>
                        </dl>
                        <dl class="inp-dl">
                            <dt><span>휴대폰번호<i class="bul-require">필수항목</i></span></dt>
                            <dd>
                                <input type="hidden" name="managerList[].mngMobile" id="mngMobile1"
                                       value="">
                                <input type="text" name="mngMobile1_1" id="mngMobile1_1"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="3"> -
                                <input type="text" name="mngMobile1_2" id="mngMobile1_2"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="4"> -
                                <input type="text" name="mngMobile1_3" id="mngMobile1_3"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="4">
                            </dd>
                        </dl>
                        <dl class="inp-dl">
                            <dt><span>연락처</span></dt>
                            <dd>
                                <input type="hidden" name="managerList[].mngPhone" id="mngPhone1"
                                       value="">
                                <input type="text" name="mngPhone1_1" id="mngPhone1_1"
                                       class="inp-base w-80" title="연락처" maxlength="3"> -
                                <input type="text" name="mngPhone1_2" id="mngPhone1_2"
                                       class="inp-base w-80" title="연락처" maxlength="4"> -
                                <input type="text" name="mngPhone1_3" id="mngPhone1_3"
                                       class="inp-base w-80" title="연락처" maxlength="4">
                            </dd>
                        </dl>
                    </td>
                </tr>
                <tr id="managerDiv2" class="managerDiv" data-mng-cd="CS">
                    <th scope="row">CS담당자</th>
                    <td>
                        <dl class="inp-dl">
                            <dt><span>담당자명<i class="bul-require">필수항목</i></span></dt>
                            <dd>
                                <input type="hidden" name="managerList[].seq" id="seq2">
                                <input type="hidden" name="managerList[].mngCd" id="mngCd2"
                                       value="CS">
                                <input type="text" name="managerList[].mngNm" id="mngNm2"
                                       class="inp-base checkVal" title="담당자명">
                            </dd>
                        </dl>
                        <dl class="inp-dl">
                            <dt><span>이메일<i class="bul-require">필수항목</i></span></dt>
                            <dd class="inp-mail">
                                <input type="text" name="managerList[].mngEmail"
                                       id="mngEmail2" class="inp-base w-260 checkVal" title="대표 이메일 주소">
                            </dd>
                        </dl>
                        <dl class="inp-dl">
                            <dt><span>휴대폰번호<i class="bul-require">필수항목</i></span></dt>
                            <dd>
                                <input type="hidden" name="managerList[].mngMobile" id="mngMobile2"
                                       value="">
                                <input type="text" name="mngMobile2_1" id="mngMobile2_1"
                                       class="inp-base w-80" title="휴대폰번호 checkVal" maxlength="3"> -
                                <input type="text" name="mngMobile2_2" id="mngMobile2_2"
                                       class="inp-base w-80" title="휴대폰번호 checkVal" maxlength="4"> -
                                <input type="text" name="mngMobile2_3" id="mngMobile2_3"
                                       class="inp-base w-80" title="휴대폰번호 checkVal" maxlength="4">
                            </dd>
                        </dl>
                        <dl class="inp-dl">
                            <dt><span>연락처</span></dt>
                            <dd>
                                <input type="hidden" name="managerList[].mngPhone" id="mngPhone2"
                                       value="">
                                <input type="text" name="mngPhone2_1" id="mngPhone2_1"
                                       class="inp-base w-80" title="연락처" maxlength="3"> -
                                <input type="text" name="mngPhone2_2" id="mngPhone2_2"
                                       class="inp-base w-80" title="연락처" maxlength="4"> -
                                <input type="text" name="mngPhone2_3" id="mngPhone2_3" class="inp-base w-80" title="연락처" maxlength="4">
                            </dd>
                        </dl>
                    </td>
                </tr>
                <tr id="managerDiv3" class="managerDiv" data-mng-cd="LOGI">
                    <th scope="row">물류담당자</th>
                    <td>
                        <dl class="inp-dl">
                            <dt><span>담당자명<i class="bul-require">필수항목</i></span></dt>
                            <dd>
                                <input type="hidden" name="managerList[].seq" id="seq3">
                                <input type="hidden" name="managerList[].mngCd" id="mngCd3"
                                       value="LOGI">
                                <input type="text" name="managerList[].mngNm" id="mngNm3"
                                       class="inp-base checkVal" title="담당자명">
                            </dd>
                        </dl>
                        <dl class="inp-dl">
                            <dt><span>이메일<i class="bul-require">필수항목</i></span></dt>
                            <dd class="inp-mail">
                                <input type="text" name="managerList[].mngEmail"
                                       id="mngEmail3" class="inp-base w-260 checkVal" title="대표 이메일 주소">
                            </dd>
                        </dl>
                        <dl class="inp-dl">
                            <dt><span>휴대폰번호<i class="bul-require">필수항목</i></span></dt>
                            <dd>
                                <input type="hidden" name="managerList[].mngMobile" id="mngMobile3"
                                       value="">
                                <input type="text" name="mngMobile3_1" id="mngMobile3_1"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="3"> -
                                <input type="text" name="mngMobile3_2" id="mngMobile3_2"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="4"> -
                                <input type="text" name="mngMobile3_3" id="mngMobile3_3"
                                       class="inp-base w-80 checkVal" title="휴대폰번호" maxlength="4">
                            </dd>
                        </dl>
                        <dl class="inp-dl">
                            <dt><span>연락처</span></dt>
                            <dd>
                                <input type="hidden" name="managerList[].mngPhone" id="mngPhone3"
                                       value="">
                                <input type="text" name="mngPhone3_1" id="mngPhone3_1"
                                       class="inp-base w-80" title="연락처" maxlength="3"> -
                                <input type="text" name="mngPhone3_2" id="mngPhone3_2"
                                       class="inp-base w-80" title="연락처" maxlength="4"> -
                                <input type="text" name="mngPhone3_3" id="mngPhone3_3"
                                       class="inp-base w-80" title="연락처" maxlength="4">
                            </dd>
                        </dl>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <h3 class="h3 mgt30">2단계 로그인 인증 담당자정보</h3>
        <div class="tbl-form mgt10">
            개인정보보호법에 의해 2단계 로그인 인증이 필요합니다. <br />
            영업담당자 외 인증 담당자 등록이 필요한 경우 등록하실 수 있습니다. ( 최대 5명 )
        </div>
        <div class="tbl-form mgt10">
            <table id="certificationTable">
                <caption>담당자정보</caption>
                <colgroup>
                    <col style="width:180px">
                    <col style="width:auto">
                </colgroup>
                <tbody>

                </tbody>
            </table>
        </div>

        <h3 class="h3 mgt30">정산정보</h3>
        <div class="tbl-form mgt10">
            <table>
                <caption>정산정보</caption>
                <colgroup>
                    <col style="width:200px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">계좌정보</th>
                    <td>
                        <span id="bankArea" class="fc01"></span>
                        <span id="bankInputArea">
                            <select id="bankCd" name="settleInfo.bankCd" class="slc-base w-180" title="계좌은행" style="display: none;">
                                <option value="">은행선택</option>
                                <c:forEach var="code" items="${bankCode}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                            <input type="hidden" name="settleInfo.bankAccountNo" id="bankAccountNo" class="inp-base w-260" maxlength="70" title="계좌번호" placeholder="계좌번호 입력 ('-'없이 입력)">
                            <input type="hidden" name="settleInfo.depositor" id="depositor" class="inp-base w-260" maxlength="70" title="예금주">
                        </span>
                        <button type="button" class="btn-base mgl10" onclick="$ui.modalOpen('bankAccountPop')"><i>변경</i></button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <h3 class="h3 mgt30">첨부서류</h3>
        <div id="partnerFileTable" class="tbl-form mgt10">
            <table>
                <caption>첨부서류</caption>
                <colgroup>
                    <col style="width:300px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <c:forEach var="codeDto" items="${partnerFileType}" varStatus="status">
                    <tr>
                        <th scope="row">${codeDto.mcNm}<i class="${codeDto.ref2}"></i></th>
                        <td>
                            <div class="file-wrap">
                                <label class="lb-file" data-file-type="${codeDto.mcCd}" data-field-name="partnerFile${status.count}" onclick="partner.file.clickFile($(this));">추가</label>
                                <ul id="uploadPartnerFileSpan_${codeDto.mcCd}" data-file-type="${codeDto.mcCd}" class="file-result2">

                                </ul>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <div class="btn-wrap mgt40">
            <button type="button" class="btn-base-l type4" onclick="partner.setSeller();"><i>확인</i></button>
            <button type="button" class="btn-base-l" onclick="partner.setCancel();"><i>취소</i></button>
        </div>
    </form>
</section>
<div class="modal-wrap" style="display: none;">
<jsp:include page="/WEB-INF/views/pages/user/pop/returnDeliveryPop.jsp" />
<jsp:include page="/WEB-INF/views/pages/user/pop/bankAccountPop.jsp" />
<jsp:include page="/WEB-INF/views/pages/user/password.jsp" />
</div>

<c:forEach var="codeDto" items="${partnerFileType}" varStatus="status">
    <input type="file" name="partnerFile${status.count}" data-file-id="uploadPartnerFileSpan_${codeDto.mcCd}" data-limit="${codeDto.ref1}"  style="display: none;"/>
</c:forEach>

<script>
    const partnerInfo = ${partnerInfo};
    const hmpImgUrl = '${hmpImgUrl}';

    $(document).ready(function() {
        partner.init();
        CommonAjaxBlockUI.global();
    });
</script>
<script type="text/javascript" src="/static/js/common/validateUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/user/modify.js?${fileVersion}"></script>