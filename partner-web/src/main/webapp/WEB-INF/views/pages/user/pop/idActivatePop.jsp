<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form id="statusPopForm" name="statusPopForm" method="post" action="/user/setIdActivatePop">
    <input type="hidden" name="partnerId" />
    <input type="hidden" name="certToken" />
    <input type="hidden" name="affiMngNm" />
    <input type="hidden" name="certNo" />
    <input type="hidden" name="mobile" />
<div id="idActivatePop" class="modal" role="dialog">
    <h2 class="h2">계정 사용처리</h2>
    <div class="modal-cont" style="height: auto;">
        <p class="fs14 fc03 mgb15">로그인 하신 계정이 장기간 미사용 되어 잠김처리 되었습니다. 계정 사용처리를 위해 휴대폰 인증을 완료해 주시기 바랍니다.</p>
        <div class="tbl-form type2">
            <table>
                <caption>영업담당자 계정 확인</caption>
                <colgroup>
                    <col style="width:29.5%">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">로그인ID<i class="bul-require">필수항목</i></th>
                    <td><input type="text" id="activateId" class="inp-base w-180" maxlength="12" title="로그인ID" readonly></td>
                </tr>
                <tr>
                    <th scope="row">영업담당자 성명<i class="bul-require">필수항목</i></th>
                    <td>
                        <input type="text" id="activateAffiMngNm" class="inp-base" maxlength="10" title="영업담당자 성명">
                    </td>
                </tr>
                <tr>
                    <th scope="row">휴대폰 번호<i class="bul-require">필수항목</i></th>
                    <td class="cell-phone-input">
                        <input type="hidden" name="mobile" id="aMobile0"
                               value="">
                        <input type="text" id="aMobile0_1"class="inp-base w-50" maxlength="3" title="휴대폰번호"> -
                        <input type="text" id="aMobile0_2"class="inp-base w-60" maxlength="4" title="휴대폰번호"> -
                        <input type="text" id="aMobile0_3"class="inp-base w-60" maxlength="4" title="휴대폰번호">
                        <button type="button" id="aCertiCheck" class="btn-base ins-no-send border-type1 w-96"><i>인증번호 발송</i></button>
                        <button type="button" id="aReCertiCheck" class="btn-base ins-no-send border-type1 w-96" style="display: none;"><i>재발송</i></button>
                        <button type="button" id="aReleaseCerti" class="btn-base ins-no-send border-type1 w-96" style="display: none;">변경</button>
                    </td>
                </tr>
                <tr>
                    <th scope="row">인증번호<i class="bul-require">필수항목</i></th>
                    <td>
                        <input type="password" id="aCertNo" class="inp-base w-114" maxlength="6" title="인증번호 입력">
                        <button type="button" id="aCertiCheckWar" class="btn-base border-type1"><i>확인</i></button>
                        <span id="aSpanCertiCheck" class="txt-err type2" style="display: none;">남은 시간 <span id="aSpanTimeOut" class="remain-ins-time">02:59</span></span>
                        <span id="aSpanCertiCheckTxt" class="txt-err type2" style="display: none;">인증완료</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="btn-wrap mgt30">
            <button type="button" onclick="idActivate.setPartnerPassword()" class="btn-base-l type4"><i>확인</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="idActivate.close()" data-focus="focus-idActivatePop-close" data-focus-next="focus-idActivatePop"><i class="hide">닫기</i></button>
</div>
</form>

<script>
    $(document).ready(function() {
        idActivate.init();
    });

    var idActivate = {
        timer       	: 0,
        certToken	: "",		// sms 토큰
        smsSuccessToken	: "",		// sms 성공 토큰
        aCertNo		: "",
        smsCountdownTimer	: null,
        mobile			: "",
        smsCheck        : false,
        init : function() {
            $('#activateAffiMngNm').allowInput("keyup", ["KOR", "ENG"]);
            idActivate.event();
        },
        event : function() {
            // 인증하기
            $("#aCertiCheck").on("click", function() {
                idActivate.mobile = $.jUtil.valNotSplit('aMobile0', '-');

                if (idActivate.mobileCheck()){
                    idActivate.validPartnerIdAffi(false);
                }
            });

            // 재발송
            $("#aReCertiCheck").on("click", function() {
                if (idActivate.mobileCheck()) {
                    // interval clear
                    if (idActivate.smsCountdownTimer != null) {
                        idActivate.smsCountdownTimer.endTimer();
                    }

                    // 인증번호 호출
                    idActivate.mobile = $.jUtil.valNotSplit('aMobile0', '-');
                    idActivate.validPartnerIdAffi(false);
                }
            });

            // 인증하기 - 완료
            $("#aCertiCheckWar").on("click", function() {
                if (idActivate.mobileCheck()) {
                    if ($("#aCertiCheck").is(':visible') == true) {
                        return $.jUtil.alert('인증번호 발송을 진행해 주세요.');
                    }

                    if ($("#aCertNo").val() == "") {
                        return $.jUtil.alert('인증번호를 입력해 주세요.', 'aCertNo');
                    }

                    if ($("#aCertNo").val().length != 6) {
                        return $.jUtil.alert('6자리 인증번호를 입력해 주세요.', 'aCertNo');
                        return;
                    }

                    let mobileNum = $.jUtil.valNotSplit('aMobile0', '-');
                    idActivate.checkCertified(mobileNum);
                }
            });

            // 인증해제
            $("#aReleaseCerti").on("click", function () {
                if (!confirm("등록한 휴대폰 번호의 인증을 해제하고 새로운 번호로 등록하시겠습니까?")) {
                    return;
                }
                idActivate.resetForm(true);
            });

            $("#partnerId").on("keyup", function () {
                $('#activateId').val($('#partnerId').val());
            });

            // 인증번호 엔터 키 이벤트
            $("#statusPopForm").find('input').keydown(function(key) {
                if (!$.jUtil.isEmpty($(this).val())) {
                    if (key.keyCode == 13) {
                        idActivate.setPartnerPassword();
                    }
                }
            });
        },
        valid : function() {

            if ($.jUtil.isEmpty($('#activateAffiMngNm').val())) {
                return $.jUtil.alert('영업담당자 성명을 입력해주세요.', 'activateAffiMngNm');
            }

            if (idActivate.smsCheck == false) {
                return $.jUtil.alert('휴대폰 번호 인증을 진행해주세요.', 'aMobile0_1');
            }

            return true;
        },
        mobileCheck : function(){
            if ($.jUtil.isEmpty($('#activateAffiMngNm').val())) {
                return $.jUtil.alert('영업담당자 성명을 입력해주세요.');
            }

            if ($("#aMobile0_1").val() == "" || $("#aMobile0_2").val() == "" || $("#aMobile0_3").val() == "") {
                return $.jUtil.alert('휴대폰 번호를 입력해주세요.');
            }
            if($.jUtil.phoneValidate($("#aMobile0_1").val() + '-' + $("#aMobile0_2").val() + '-' + $("#aMobile0_3").val(), true)) {
                return true;
            } else {
                return $.jUtil.alert('휴대폰 번호 형식이 잘못되었습니다. 다시 확인해 주세요.');
            }
        },
        /**
         * 인증번호 생성
         * @param mobile
         */
        setCertified : function(mobile) {
            CommonAjax.basic({
                url:"/common/mobile/certno/send.json?mobile="+mobile
                , method:"GET"
                , callbackFunc:function(res) {
                    if (res.hasOwnProperty("data")) {
                        idActivate.certToken = res.data;

                        // 타이머 호출
                        idActivate.checkTimer();
                        alert("인증번호가 전송되었습니다.");

                        // 재발송 view
                        $("#aCertiCheck").hide();
                        $("#aReCertiCheck").show();
                        $("#aSpanCertiCheck").show();
                    }
                }
                , errorCallbackFunc : function() {
                    idActivate.smsCountdownTimer.endTimer();

                    // 인증 view
                    $("#aCertiCheck").show();
                    $("#aReCertiCheck").hide();
                    $("#aSpanCertiCheck").hide();
                }
            });
        },
        /**
         * 인증번호 확인
         * @param mobile
         */
        checkCertified : function(mobile) {
            var param = {
                    certToken: idActivate.certToken
                ,   certNo: $("#aCertNo").val()
                ,   mobile: mobile
            };
            CommonAjax.basic({
                url: "/common/mobile/certno/valid.json"
                , data: JSON.stringify(param)
                , contentType: 'application/json'
                , method:"POST"
                , callbackFunc:function(res) {
                    if (res.hasOwnProperty("data") && res.data == true) {
                        alert("휴대폰 인증에 성공하였습니다.");

                        idActivate.smsSuccessToken = res.data;
                        idActivate.aCertNo = $("#aCertNo").val();
                        idActivate.mobile = mobile;
                        idActivate.smsCheck = true;
                        idActivate.smsCountdownTimer.endTimer();

                        // 휴대폰 번호 입력 활성화
                        $("#aMobile0_1").attr("readonly", true);
                        $("#aMobile0_2").attr("readonly", true);
                        $("#aMobile0_3").attr("readonly", true);
                        $("#aCertNo").attr("readonly", true);

                        // button view
                        $("#aCertiCheck").hide();
                        $("#aReCertiCheck").hide();
                        $("#aReleaseCerti, #aSpanCertiCheckTxt").show();
                        $("#aSpanCertiCheck").hide();
                    } else {
                        alert(res.returnMessage);
                    }
                }
            });
        },
        /**
         * 남은 시간 카운터
         */
        checkTimer : function() {
            var endTime = moment(new Date()).add(3, 'minutes');
            if(idActivate.smsCountdownTimer != null)
            {
                idActivate.smsCountdownTimer.endTimer();
            }
            idActivate.smsCountdownTimer = countdownTimer($('#aSpanTimeOut'),endTime,'ms','0:00',function(){
                if (idActivate.smsCheck == false) {
                    alert('인증번호 입력시간이 지났습니다. \r\n다시 시도해 주세요.');
                    idActivate.smsCheck = false;
                }
            });
        },
        setPartnerPassword : function() {

            if (idActivate.valid()) {
                idActivate.validPartnerIdAffi(true);
            }
        },
        validPartnerIdAffi : function(isClose) {
            CommonAjax.basic({
                url         : "/user/validPartnerIdAffi.json",
                method      : "POST",
                data        : JSON.stringify({
                    "partnerId" : $('#activateId').val(),
                    "affiMngNm" : $('#activateAffiMngNm').val(),
                    "affiMobile" : idActivate.mobile
                }),
                contentType : "application/json",
                callbackFunc: function (res) {
                    if (res.returnCode == "0") {
                        if (isClose) {

                            $("#statusPopForm").find('input[name="partnerId"]').val($('#activateId').val());
                            $("#statusPopForm").find('input[name="certToken"]').val(idActivate.certToken);
                            $("#statusPopForm").find('input[name="affiMngNm"]').val($('#activateAffiMngNm').val());
                            $("#statusPopForm").find('input[name="certNo"]').val(idActivate.aCertNo);
                            $("#statusPopForm").find('input[name="mobile"]').val(idActivate.mobile);

                            var statusPopForm = $('#statusPopForm').serializeObject();

                            CommonAjax.basic({
                                url: "/user/setIdActivatePop.json",
                                data: JSON.stringify(statusPopForm),
                                method: "POST",
                                contentType: "application/json",
                                callbackFunc: function (res) {
                                    alert(res.returnMsg);
                                    idActivate.close();
                                }
                            });


                            // idActivate.close();
                        } else {
                            // 인증번호 호출
                            var mobileNum = $.jUtil.valNotSplit('aMobile0', '-');
                            idActivate.setCertified(mobileNum);
                        }
                    } else {
                        return $.jUtil.alert("입력하신 정보를 찾을 수 없습니다.\n관리자에 문의하세요.");
                    }
                },
                errorCallbackFunc: function (res) {
                    return $.jUtil.alert(res.responseJSON.returnMessage);
                }
            });
        },
        resetForm : function(isCerti) {
            // 영업담당자 인증 flag 변경
            idActivate.smsCheck = false;
            idActivate.aCertNo = "";


            // 휴대폰 번호 입력 활성화
            $("#aMobile0_1").attr("readonly", false);
            $("#aMobile0_2").attr("readonly", false);
            $("#aMobile0_3").attr("readonly", false);
            $("#aCertNo").val("").attr("readonly", false);

            // button view
            $("#aCertiCheck").show();
            $("#aReCertiCheck").hide();
            $("#aReleaseCerti, #aSpanCertiCheckTxt").hide();
            $("#aSpanCertiCheck").hide();

            if (!isCerti) {
                $('#idActivatePop').find('input').val('');
                $("#activateId").val($('#partnerId').val());
            }

            if (idActivate.smsCountdownTimer != null) {
                idActivate.smsCountdownTimer.endTimer();
            }
        },
        close : function(){
            idActivate.resetForm();
            $ui.modalClose('idActivatePop');
        }
    }
</script>