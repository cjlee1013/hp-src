<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="noReceiveInfoPop" class="modal type-l">
    <h2 class="h2" id="noReceiveInfoPopTitle"></h2>
    <div class="modal-cont">
        <div class="tbl-data">
            <table>
                <caption>미수취 신고 내역</caption>
                <colgroup>
                    <col style="width:15%">
                    <col style="width:35%">
                    <col style="width:15%">
                    <col style="width:35%">
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row">신고일시</th>
                        <td id="noRcvDeclrDt"></td>
                        <th scope="row">등록자</th>
                        <td id="noRcvRegId"></td>
                    </tr>
                    <tr>
                        <th scope="row">신고사유</th>
                        <td id="noRcvDeclrType" colspan="3"></td>
                    </tr>
                    <tr>
                        <th scope="row">상세사유</th>
                        <td id="noRcvDetailReason" colspan="3"></td>
                    </tr>
                    <tr>
                        <th scope="row">처리일시</th>
                        <td id="noRcvProcessDt"></td>
                        <th scope="row">처리자</th>
                        <td id="noRcvChgId"></td>
                    </tr>
                    <tr>
                        <th scope="row">처리결과</th>
                        <td id="noRcvProcessResult" colspan="3"></td>
                    </tr>
                    <tr id="noRcvProcessCntntArea" style="display: none;">
                        <th scope="row">처리내용</th>
                        <td id="noRcvProcessCntnt" colspan="3"></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4"  onclick="noReceiveInfoPop.closeModal()"><i>확인</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="noReceiveInfoPop.closeModal()"><i class="hide" >닫기</i></button>
</div>
<script>
  $(document).ready(function() {
    noReceiveInfoPop.init();
  });
</script>

<script type="text/javascript" src="/static/js/sell/pop/noReceiveInfoPop.js?v=${fileVersion}"></script>