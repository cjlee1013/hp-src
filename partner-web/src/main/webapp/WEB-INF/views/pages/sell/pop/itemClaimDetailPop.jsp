<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="itemClaimDetailPop" class="modal type-xl" role="dialog">
    <h2 class="h2">상품/클레임 상세정보</h2>

    <div class="modal-cont">
        <div id="itemClaimDetailPopTab" class="tab-basic tab-wrap">
            <div class="tab-nav">
                <button type="button" class="tab-btn">주문정보</button>
                <button type="button" class="tab-btn">취소/반품/교환 정보</button>
            </div>
            <div class="tab-pnl">
                <h3 class="h4">주문기본정보</h3>
                <div class="tbl-data">
                    <table>
                        <caption>주문기본정보</caption>
                        <colgroup>
                            <col style="width:12%">
                            <col style="width:21%">
                            <col style="width:15%">
                            <col style="width:20%">
                            <col style="width:14%">
                            <col style="width:18%">
                        </colgroup>
                        <tbody>
                            <tr>
                                <th scope="row">주문번호</th>
                                <td colspan="2" id="purchaseOrderNo"></td>
                                <th scope="row">주문일시</th>
                                <td colspan="2" id="orderDt"></td>
                            </tr>
                            <tr>
                                <th scope="row">구매자</th>
                                <td colspan="2" id="buyerNm"></td>
                                <th scope="row">구매자연락처</th>
                                <td colspan="2" id="buyerMobileNo"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h3 class="h4 mgt20">주문상품정보</h3>
                <div class="tbl-data">
                    <table>
                        <caption>주문상품정보</caption>
                        <colgroup>
                            <col style="width:12%">
                            <col style="width:24%">
                            <col style="width:15%">
                            <col style="width:17%">
                            <col style="width:14%">
                            <col style="width:18%">
                        </colgroup>
                        <tbody id="tbodyItemList">
                        </tbody>
                    </table>
                </div>

                <h3 class="h4 mgt20">배송정보</h3>
                <div class="tbl-data">
                    <table>
                        <caption>배송정보</caption>
                        <colgroup>
                            <col style="width:12%">
                            <col style="width:19%">
                            <col style="width:14%">
                            <col style="width:21%">
                            <col style="width:15%">
                            <col style="width:19%">
                        </colgroup>
                        <tbody>
                            <tr>
                                <th scope="row">배송번호</th>
                                <td id="bundleNo"></td>
                                <th scope="row">배송비</th>
                                <td id="shipPriceType_totShipPrice"></td>
                                <th scope="row">도서산간배송비</th>
                                <td id="islandShipPrice"></td>
                            </tr>
                            <tr>
                                <th scope="row">발송기한</th>
                                <td id="orgShipDt"></td>
                                <th scope="row">2차발송기한</th>
                                <td colspan="3" id="delayShipDt_delayShipNm"></td>
                            </tr>
                            <tr>
                                <th scope="row">수령인</th>
                                <td id="receiverNm"></td>
                                <th scope="row">수령인연락처</th>
                                <td colspan="3" id="shipMobileNo"></td>
                            </tr>
                            <tr>
                                <th scope="row">배송지</th>
                                <td colspan="6" id="zipcode_addr"></td>
                            </tr>
                            <tr>
                                <th scope="row">배송메모</th>
                                <td colspan="6" id="dlvShipMsg"></td>
                            </tr>
                            <tr>
                                <th scope="row">미수취신고</th>
                                <td colspan="6" id="noRcvDeclrDt"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="tab-pnl">
                <!-- 클레임 정보가 없는 경우 -->
                <p id="nonClaimText">진행 된 클레임이 없습니다</p>

                <div class="tbl-data" id="claimDetailTap">
                    <table>
                        <caption>클레임 상세정보</caption>
                        <colgroup>
                            <col style="width:15%">
                            <col style="width:18%">
                            <col style="width:15%">
                            <col style="width:19%">
                            <col style="width:15%">
                            <col style="width:18%">
                        </colgroup>
                        <tbody id="tbodyClaimList">
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4"  onclick="itemClaimDetailPop.closeModal()"><i>확인</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="itemClaimDetailPop.closeModal()"><i class="hide" >닫기</i></button>
</div>
<script>
  $(document).ready(function() {
    itemClaimDetailPop.init();
  });
</script>

<script type="text/javascript" src="/static/js/sell/pop/itemClaimDetailPop.js?v=${fileVersion}"></script>