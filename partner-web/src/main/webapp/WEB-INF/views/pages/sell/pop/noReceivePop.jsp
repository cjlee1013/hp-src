<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="noReceivePop" class="modal type-l" role="dialog">
    <h2 class="h2">미수취신고 철회요청</h2>
    <div class="modal-cont">
        <ul class="bul-list">
            <li>입력하신 내용은 고객에게 안내됩니다.</li>
        </ul>
        <div id="noReceivePopGrid" class="data-wrap" style="height: 200px"></div>
        <div class="tbl-form mgt20">
            <table>
                <caption>주문기본정보</caption>
                <colgroup>
                    <col style="width:20%">
                    <col style="width:30%">
                    <col style="width:20%">
                    <col style="width:30%">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">신고일시</th>
                    <td id="noRcvDeclrDt"></td>
                    <th scope="row">등록자</th>
                    <td id="noRcvRegId"></td>
                </tr>
                <tr>
                    <th scope="row">신고사유</th>
                    <td colspan="3" id="noRcvDeclrType"></td>
                </tr>
                <tr>
                    <th scope="row">상세사유</th>
                    <td colspan="3" id="noRcvDetailReason"></td>
                </tr>
                <tr>
                    <th scope="row">전달 메시지</i></th>
                    <td colspan="3">
                        <div class="words-wrap mgt10">
                            <textarea class="textarea" id="noRcvProcessCntnt" placeholder="작성하신 내용은 고객에게 발송됩니다."></textarea>
                            <span class="words"><i id="msgCount">0</i>/1000자</span>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l" onclick="noReceivePop.closeModal()"><i>취소</i></button>
            <button type="button" id="saveBtn" class="btn-base-l type4" id="saveBtn"><i>등록</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="noReceivePop.closeModal()"><i class="hide" >닫기</i></button>
</div>

<script>
  ${noReceivePopGridBaseInfo}

  $(document).ready(function() {
    noReceivePop.init();
    noReceivePopGrid.init();
  });
</script>

<script type="text/javascript" src="/static/js/sell/pop/noReceivePop.js?v=${fileVersion}"></script>