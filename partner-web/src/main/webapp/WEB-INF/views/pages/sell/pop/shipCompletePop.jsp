<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="shipCompletePop" class="modal type-l" role="dialog">
    <h2 class="h2">배송완료</h2>
    <div class="modal-cont">
        <ul class="bul-list">
            <li>배송전 배송완료 처리시 판매자에게 불이익이 발생할 수 있습니다.</li>
        </ul>

        <div id="shipCompletePopGrid" class="data-wrap" style="height: 200px"></div>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l" onclick="shipCompletePop.closeModal()"><i>취소</i></button>
            <button type="button" id="saveBtn" class="btn-base-l type4" id="saveBtn"><i>배송완료</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="shipCompletePop.closeModal()"><i class="hide" >닫기</i></button>
</div>

<div id="shipCompleteResultPop" class="modal" role="dialog">
    <h2 class="h2">배송완료</h2>

    <div class="modal-cont">
        <div class="box-group mgt25">
            <div class="box">
                <span>등록</span>
                <strong id="totalCnt">0</strong>
            </div>
            <div class="box">
                <span>성공</span>
                <strong id="successCnt">0</strong>
            </div>
            <div class="box">
                <span>실패</span>
                <strong class="fc02" id="failCnt">0</strong>
            </div>
        </div>
        <p class="bul-p mgt20">
            실패한 건은 배송중인 건이 아니거나 배송완료 가능한 상태가 아닌 주문입니다.
        </p>
        <div class="btn-wrap mgt20">
            <button type="button" class="btn-base-l type4" onclick="shipCompletePop.closeResultModal()"><i>확인</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="shipCompletePop.closeResultModal()"><i class="hide" >닫기</i></button>
</div>

<script>
  ${shipCompletePopGridBaseInfo}

  $(document).ready(function() {
    shipCompletePop.init();
    shipCompletePopGrid.init();
  });
</script>

<script type="text/javascript" src="/static/js/sell/pop/shipCompletePop.js?v=${fileVersion}"></script>