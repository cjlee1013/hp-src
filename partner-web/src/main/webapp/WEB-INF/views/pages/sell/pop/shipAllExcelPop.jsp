<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="shipAllExcelPop" class="modal" role="dialog">
    <h2 class="h2">일괄 발송처리</h2>

    <form id="fileUploadForm">
        <div class="modal-cont">
            <!-- 등록 전 -->
            <div class="tbl-data">
                <table>
                    <colgroup>
                        <col style="width:30%">
                        <col style="width:auto">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row">배송방법</th>
                        <td>
                            <div>
                                <input type="radio" id="shipMethod_DLV" name="shipMethod" value="DLV"><label for="shipMethod_DLV" class="lb-radio">택배배송</label>
                                <select id="dlvCd" name="dlvCd" class="slc-base w-160 mgl5" title="택배사 선택" disabled>
                                    <option value="">택배사 선택</option>
                                    <c:forEach var="codeDto" items="${dlvCd}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="mgt10">
                                <input type="radio" id="shipMethod_DRCT" name="shipMethod" value="DRCT"><label for="shipMethod_DRCT" class="lb-radio">직접배송</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">파일등록</th>
                        <td>
                            <div class="file-wrap">
                                <ul class="file-result w-max">
                                    <li id="uploadFileNm" name="uploadFileNm"></li>
                                </ul>
                                <input type="file" id="uploadFile" name="uploadFile" accept=".xlsx" onchange="shipAllExcelPop.getFile($(this))" title="첨부파일" style="display: none">
                                <label id="searchUploadFile" class="lb-file mgt5">파일찾기</label>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <ul class="bul-list mgt10">
                <li>원하시는 배송방법을 선택하시고 택배배송을 선택한 경우 해당하는 택배사를 선택해주세요.</li>
                <li>만약, 퀵서비스나, 방문수령, 직접전달 로 발송하시는 경우에는 배송방법만 선택하시면 됩니다.</li>
            </ul>

            <div class="btn-wrap mgt20 align-l">
                <button type="button" id="templateDlv" class="btn-ico-exel"><i>택배배송 일괄처리 엑셀양식 다운로드</i></button>
            </div>

            <div class="btn-wrap mgt5 align-l">
                <button type="button" id="templateDrct" class="btn-ico-exel"><i>직접배송 일괄처리 엑셀양식 다운로드</i></button>
            </div>

            <div class="btn-wrap mgt20">
                <button type="button" id="closeBtn" class="btn-base-l" onclick="shipAllExcelPop.closeModal()" ><i>취소</i></button>
                <button type="button" id="saveBtn" class="btn-base-l type4"><i>등록</i></button>
            </div>
        </div>
    </form>

    <button type="button" class="btn-close2 ui-modal-close" onclick="shipAllExcelPop.closeModal()"><i class="hide" >닫기</i></button>
</div>

<div id="shipAllExcelResultPop" class="modal" role="dialog">
    <h2 class="h2">일괄 발송처리</h2>

    <div class="modal-cont">
        <!-- 등록 후 -->
        <div class="box-group mgt25">
            <div class="box">
                <span>등록</span>
                <strong id="totalCnt">0</strong>
            </div>
            <div class="box">
                <span>성공</span>
                <strong id="successCnt">0</strong>
            </div>
            <div class="box">
                <span>실패</span>
                <strong class="fc02" id="failCnt">0</strong>
            </div>
        </div>
        <p class="bul-p mgt20">
            엑셀을 다운로드하여 실패 사유를 확인해주세요.
        </p>
        <div class="btn-wrap mgt20">
            <button type="button" class="btn-base-l type4" onclick="shipAllExcelPop.excelDownload()" ><i>엑셀 다운</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="shipAllExcelPop.closeResultModal()"><i class="hide" >닫기</i></button>

    <div id="shipAllExcelPopGrid" style="display: none;"></div>
</div>
<script>
  ${shipAllExcelPopGridBaseInfo}

  $(document).ready(function() {
    shipAllExcelPop.init();
  });
</script>

<script type="text/javascript" src="/static/js/sell/pop/shipAllExcelPop.js?v=${fileVersion}"></script>