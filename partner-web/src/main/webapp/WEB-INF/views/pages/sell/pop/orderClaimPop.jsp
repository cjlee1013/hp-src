<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="orderClaimPop" class="modal type-xl" role="dialog">
    <h2 class="h2" id="claimPopTitle"></h2>

    <div class="modal-cont" style="height: 580px;">

        <jsp:include page="layout/orderItemInfoPopLayout.jsp"></jsp:include>

        <div class="mgt10">

            <div class="ui form inline mg-b-10">
                <select id="claimReasonType" name="claimReasonType" class="ui input medium mg-r-20" style="width: 25%;">
                </select>
                <input type="text" id="claimReasonDetail" name="claimReasonDetail" class="ui input medium mg-r-5" style="width: 70%;" maxlength="100" placeholder="상세사유를 입력 (최대 100자)">
            </div>
        </div>

        <h4 class="h4 mgt20" id="claimPopTitle2" style="font-weight: bold"></h4>
        <div class="tbl-data">
            <form id="claimRequestForm">
            <table>
                <caption>신청정보 테이블</caption>
                <colgroup>
                    <col width="20%">
                    <col width="20%">
                    <col width="*%">
                </colgroup>
                <tr>
                    <th id="pickRequestTitle"></th>
                    <td colspan="2">
                        <table class="ui table">
                            <tr>
                                <th style="width: 100px;">*이름</th>
                                <td>
                                    <input type="text" id="pickReceiverNm" name="pickReceiverNm" class="ui input medium mg-r-5" style="width: 40%;" maxlength="20" placeholder="이름을 입력해주세요">
                                </td>
                            </tr>
                            <tr>
                                <th>*연락처</th>
                                <td style="text-align: left">
                                    <input type="text" name="pickMobileNo" id="pickMobileNo" hidden>
                                    <input type="text" name="pickMobileNo_1" id="pickMobileNo_1" class="inp-base w-80 checkVal" title="연락처" maxlength="3"> -
                                    <input type="text" name="pickMobileNo_2" id="pickMobileNo_2" class="inp-base w-80 checkVal" title="연락처" maxlength="4"> -
                                    <input type="text" name="pickMobileNo_3" id="pickMobileNo_3" class="inp-base w-80 checkVal" title="연락처" maxlength="4">
                                </td>
                            </tr>
                            <tr>
                                <th>*주소</th>
                                <td style="text-align: left">
                                    <div class="ui form inline">
                                        <input type="text" name="pickZipCode" id="pickZipCode" class="ui input medium mg-r-5" style="width:100px;" readonly><button type="button" class="btn-base" onclick="zipCodePopup('shipStatusMain.orderReturnSetZipcode');"><i>우편번호 찾기</i></button>
                                        <br>
                                        <input type="text" name="pickRoadBaseAddr" id="pickRoadBaseAddr" class="ui input medium mg-t-5 mg-r-5" style="width:150px;" readonly>
                                        <input type="hidden" name="pickBaseAddr" id="pickBaseAddr" class="ui input medium mg-t-5 mg-r-5" style="width:150px;">
                                        <input type="text" name="pickBaseDetailAddr" id="pickBaseDetailAddr" class="ui input medium mg-t-5" style="width:240px;" placeholder="상세주소를 입력해주세요">
                                        <input type="hidden" name="pickRoadDetailAddr" id="pickRoadDetailAddr">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="exchangeTableRow" hidden>
                    <th>교환배송지</th>
                    <td colspan="2">
                        <table class="ui table">
                            <tr>
                                <th style="width: 100px;">*이름</th>
                                <td>
                                    <input type="text" id="exchReceiverNm" name="exchReceiverNm" class="ui input medium mg-r-5" style="width: 40%;" maxlength="20" placeholder="이름을 입력해주세요">
                                </td>
                            </tr>
                            <tr>
                                <th>*연락처</th>
                                <td style="text-align: left">
                                    <input type="text" name="exchMobileNo" id="exchMobileNo" hidden>
                                    <input type="text" name="exchMobileNo_1" id="exchMobileNo_1" class="inp-base w-80 checkVal" title="연락처" maxlength="3"> -
                                    <input type="text" name="exchMobileNo_2" id="exchMobileNo_2" class="inp-base w-80 checkVal" title="연락처" maxlength="4"> -
                                    <input type="text" name="exchMobileNo_3" id="exchMobileNo_3" class="inp-base w-80 checkVal" title="연락처" maxlength="4">
                                </td>
                            </tr>
                            <tr>
                                <th>*주소</th>
                                <td style="text-align: left">
                                    <div class="ui form inline">
                                        <input type="text" name="exchZipCode" id="exchZipCode" class="ui input medium mg-r-5" style="width:100px;" readonly><button type="button" class="btn-base" onclick="zipCodePopup('shipStatusMain.orderExchangeSetZipcode');"><i>우편번호 찾기</i></button>
                                        <br>
                                        <input type="text" name="exchRoadBaseAddr" id="exchRoadBaseAddr" class="ui input medium mg-t-5 mg-r-5" style="width:150px;" readonly>
                                        <input type="hidden" name="exchBaseAddr" id="exchBaseAddr" class="ui input medium mg-t-5 mg-r-5" style="width:150px;">
                                        <input type="text" name="exchBaseDetailAddr" id="exchBaseDetailAddr" class="ui input medium mg-t-5" style="width:240px;" placeholder="상세주소를 입력해주세요">
                                        <input type="hidden" name="exchRoadDetailAddr" id="exchRoadDetailAddr">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th>수거방법</th>
                    <td>
                        <select id="pickShipType" name="pickShipType" class="ui input medium" style="width: 100px">
                            <option value="C">택배수거</option>
                            <option value="N">수거안함</option>
                        </select>
                    </td>
                    <td>
                        <div class="ui form inline"  id="parcelDiv" style="display: none">
                            <select id="pickDlvCd" name="pickDlvCd" class="ui input medium mg-r-5" style="width: 30%">
                                <option value="">택배사선택</option>
                                    <c:forEach var="codeDto" items="${dlvCd}" varStatus="status">
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:forEach>
                            </select>
                            <input type="text" id="pickInvoiceNo" name="pickInvoiceNo" class="ui input medium mg-r-5" maxlength="30" style="width: 60%" placeholder="송장번호 입력">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th rowspan="3">사진첨부</th>
                    <td colspan="2">
                        <div class="com wrap-title" id="imgBtnView" hidden>
                            <span id="fileName" name="fileName"></span>
                            <input type="hidden" id="uploadFileName" name="uploadFileName" hidden></input>
                            <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm mg-r-60" style="float: right" onclick="orderClaimPop.deleteImg('');">삭제</button>
                        </div>
                        <div class="com wrap-title" id="imgBtnUpload">
                            <span class="pull-left mg-r-10">
                                <button type="button" class="ui button medium" id="imgBtn" name="imgBtn" onclick="orderClaimPop.clickFile('');">사진첨부</button>
                            </span>
                            <h2 class="mg-t-5">이미지 파일(GIF, PNG, JPG)을 기준으로 최대 5MB이하</h2>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="com wrap-title" id="imgBtnView2" hidden>
                            <span id="fileName2" name="fileName2"></span>
                            <input type="hidden" id="uploadFileName2" name="uploadFileName2" hidden></input>
                            <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm mg-r-60" style="float: right" onclick="orderClaimPop.deleteImg(2);">삭제</button>
                        </div>
                        <div class="com wrap-title" id="imgBtnUpload2">
                            <span class="pull-left mg-r-10">
                                <button type="button" class="ui button medium" id="imgBtn2" name="imgBtn2" onclick="orderClaimPop.clickFile(2);">사진첨부</button>
                            </span>
                            <h2 class="mg-t-5">이미지 파일(GIF, PNG, JPG)을 기준으로 최대 5MB이하</h2>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="com wrap-title" id="imgBtnView3" hidden>
                            <span id="fileName3" name="fileName3"></span>
                            <input type="hidden" id="uploadFileName3" name="uploadFileName3" hidden></span>
                            <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm mg-r-60" style="float: right" onclick="orderClaimPop.deleteImg(3);">삭제</button>
                        </div>
                        <div class="com wrap-title" id="imgBtnUpload3">
                            <span class="pull-left mg-r-10">
                                <button type="button" class="ui button medium" id="imgBtn3" name="imgBtn3" onclick="orderClaimPop.clickFile(3);">사진첨부</button>
                            </span>
                            <h2 class="mg-t-5">이미지 파일(GIF, PNG, JPG)을 기준으로 최대 5MB이하</h2>
                        </div>
                    </td>
                </tr>
            </table>
            <input type="hidden" name="isPick" id="isPick" value="Y">
            <input type="hidden" name="isPickReq" id="isPickReq" value="Y">
            <input type="hidden" name="isEnclose" id="isEnclose" value="Y">
            <input type="hidden" name="shipType" id="shipType" value="DS">
            </form>
        </div>
        <div class="btn-wrap mgt30">
            <button type="button" id="saveBtn" class="btn-base-l type4"><i id="reqBtnTitle"></i></button>
            <button type="button" class="btn-base-l" onclick="orderClaimPop.closeModal()"><i>취소</i></button>
        </div>
    </div>
    <input type="hidden" name="shipStatusNm" id="shipStatusNm">
    <input type="hidden" name="shipNo" id="shipNo">
    <input type="hidden" name="claimType" id="claimType">
    <button type="button" class="btn-close2 ui-modal-close" onclick="orderClaimPop.closeModal()"><i class="hide" >닫기</i></button>
</div>



<input type="file" id="claimImgFile" name="fileArr" multiple style="display: none;"/>
<input type="file" id="claimImgFile2" name="fileArr" multiple style="display: none;"/>
<input type="file" id="claimImgFile3" name="fileArr" multiple style="display: none;"/>

<script>
    var claimReasonList = new Array();

    <c:forEach var="codeDto" items="${claimReasonType}">
        claimReasonList.push({
            mcNm : "${codeDto.mcNm}",
            mcCd : "${codeDto.mcCd}",
            claimType : "${codeDto.ref1}",
            ref2 : "${codeDto.ref2}",
            cancelType : "${codeDto.ref3}",
            detailReasonRequired : "${codeDto.ref4}"
            }
        );
    </c:forEach>
</script>
<script type="text/javascript" src="/static/js/sell/pop/orderClaimPop.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/sell/pop/layout/orderClaimCommon.js?v=${fileVersion}"></script>