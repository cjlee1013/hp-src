<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<section class="contents" id="shipStatusMain">
    <div class="float">
        <h2 class="h2 float-l">배송현황 <span>최근 1개월 기준</span></h2>
        <div class="float-r">
            <button type="button" class="btn-ico-re" id="boardRefresh"><i>새로고침</i></button>
        </div>
    </div>

    <div class="box-manage line2" id="cntBoard">
        <div>
            <dl>
                <dt>배송중</dt>
                <dd><a href="javascript:;" id="shippingCnt">0</a>건</dd>
            </dl>
        </div>
        <div>
            <dl>
                <dt>배송완료</dt>
                <dd><a href="javascript:;" id="completeCnt">0</a>건</dd>
            </dl>
        </div>
        <div>
            <dl>
                <dt>미수취신고</dt>
                <dd><a href="javascript:;" id="noReceiveCnt">0</a>건</dd>
            </dl>
        </div>
    </div>
    <div class="srh-form">
        <form id="shipStatusSearchForm" name="shipStatusSearchForm" onsubmit="return false;">
            <table>
                <caption>조회</caption>
                <colgroup>
                    <col style="width:75px">
                    <col style="width:392px">
                    <col style="width:80px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row"><label for="schShipDtType">검색기간</label></th>
                    <td colspan="5">
                        <select id="schShipDtType" name="schShipDtType" class="slc-base w-120">
                            <option value="orderDt">주문일</option>
                            <option value="orgShipDt">발송기한</option>
                            <option value="delayShipDt">2차발송기한</option>
                            <option value="confirmDt">주문확인일</option>
                            <option value="paymentFshDt">결제완료일</option>
                        </select>
                        <span class="inp-date"><input id="schStartDt" name="schShipStartDt" type="text" class="inp-base" placeholder="시작일" maxlength="10"></span> -
                        <span class="inp-date"><input id="schEndDt" name="schShipEndDt" type="text" class="inp-base" placeholder="종료일" maxlength="10"></span>
                        <span class="radio-group mgl5">
                            <input type="radio" id="setTodayBtn" name="setCalBtn"><label for="setTodayBtn"  class="lb-radio">오늘</label>
                            <input type="radio" id="setOneWeekBtn" name="setCalBtn" checked><label for="setOneWeekBtn"  class="lb-radio">1주일</label>
                            <input type="radio" id="setOneMonthBtn" name="setCalBtn"><label for="setOneMonthBtn"  class="lb-radio">1개월</label>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="schKeywordType">검색조건</label></th>
                    <td>
                        <select id="schKeywordType" name="schKeywordType" class="slc-base w-160">
                            <option value="orderItemNo">상품주문번호</option>
                            <option value="bundleNo">배송번호</option>
                            <option value="purchaseOrderNo">주문번호</option>
                            <option value="itemNo">상품번호</option>
                            <option value="receiverNm">수령인</option>
                            <option value="itemNm1">상품명</option>
                            <option value="buyerNm">구매자</option>
                        </select>
                        <input type="text" class="inp-base w-160" id="schKeyword" name="schKeyword">
                    </td>
                    <th scope="row" class="algin-r"><label for="schShipStatus">주문상태</label></th>
                    <td>
                        <select id="schShipStatus" name="schShipStatus" class="slc-base w-160">
                            <option value="">전체</option>
                            <c:forEach var="codeDto" items="${shipStatus}" varStatus="status">
                                <c:if test="${codeDto.mcCd eq 'D3' or codeDto.mcCd eq 'D4' or codeDto.mcCd eq 'D5'}">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="btn-wrap">
                <button type="submit" id="schBtn" class="btn-base-l type4">검색</button>
                <button type="button" id="schResetBtn" class="btn-base-l type3">초기화</button>
            </div>
        </form>
    </div>

    <div class="result-wrap float mgt20">
        <p class="float-l result-p">검색 결과 <b id="shipStatusSearchCnt">0</b>건</p>
        <div class="float-r">
            <button type="button" id="schExcelDownloadBtn" class="btn-ico-exel"><i>엑셀다운</i></button>
            <select class="slc-base w-110" id="pageViewCnt" title="정렬선택">
                <option value="50">50개씩</option>
                <option value="100"selected>100개씩</option>
                <option value="200">200개씩</option>
                <option value="300">300개씩</option>
                <option value="400">400개씩</option>
                <option value="500">500개씩</option>
            </select>
        </div>
    </div>

    <div class="btn-gruop mgt20">
        <button type="button" id="editShipInfo" class="btn-base-s"><i>배송방법/송장수정</i></button>
        <button type="button" id="noReceiveCancel" class="btn-base-s"><i>미수취 신고 철회요청</i></button>
        <button type="button" id="shipComplete" class="btn-base-s"><i>배송완료</i></button>
        <button type="button" id="claimReturn" class="btn-base-s"><i>반품신청</i></button>
        <button type="button" id="claimExchange" class="btn-base-s"><i>교환신청</i></button>
    </div>

    <div class="data-wrap" style="height: 500px" id="shipStatusGrid" ></div>

    <div id="pagingArea" class="paging"></div>

</section>

<div class="modal-wrap">
    <jsp:include page="/WEB-INF/views/pages/sell/pop/shipAllPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/sell/pop/noReceivePop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/sell/pop/shipCompletePop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/sell/pop/itemClaimDetailPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/sell/pop/noReceiveInfoPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/sell/pop/shipHistoryPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/sell/pop/orderClaimPop.jsp" />
</div>

<script>
    ${shipStatusGridBaseInfo}

    var parentMain = null;
    var parentGrid = null;
    var theclubFrontUrl = '${theclubFrontUrl}'; // 더클럽 프론트 페이지 URL
    var homeplusFrontUrl = '${homeplusFrontUrl}'; // 홈플러스 프론트 페이지 URL
    var schType = '${schType}'; // 보드 카운트 클릭 타입
    var parnterId = '${parnterId}'; // 보드 카운트 클릭 타입

    $(document).ready(function() {
        shipStatusMain.init();
        shipStatusGrid.init();

        parentMain = shipStatusMain;
        parentGrid = shipStatusGrid;

        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/sell/shipStatusMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/sell/sellUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/claimMainCommon.js?${fileVersion}"></script>
