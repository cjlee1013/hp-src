<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<section class="contents" id="orderShipManageMain">
    <div class="float">
        <h2 class="h2 float-l">발주/발송관리 <span>최근 1개월 기준</span></h2>
        <div class="float-r">
            <button type="button" class="btn-ico-re" id="boardRefresh"><i>새로고침</i></button>
        </div>
    </div>

    <div class="box-manage line3" id="cntBoard">
        <div>
            <dl>
                <dt>신규주문</dt>
                <dd><a href="javascript:;" id="newCnt">0</a>건</dd>
            </dl>
            <dl>
                <dt>상품준비중</dt>
                <dd><a href="javascript:;" id="readyCnt">0</a>건</dd>
            </dl>
        </div>
        <div>
            <dl>
                <dt>주문확인 지연</dt>
                <dd><a href="javascript:;" id="newDelayCnt">0</a>건</dd>
            </dl>
            <dl>
                <dt>발송 지연</dt>
                <dd><a href="javascript:;" id="shipDelayCnt">0</a>건</dd>
            </dl>
            <dl>
                <dt>발송전 지연안내</dt>
                <dd><a href="javascript:;" id="shipDelayNotiCnt">0</a>건</dd>
            </dl>
        </div>
        <div>
            <dl>
                <dt>오늘 발송요청</dt>
                <dd><a href="javascript:;" id="shipTodayCnt">0</a>건</dd>
            </dl>
            <dl>
                <dt>발송전 취소요청</dt>
                <dd><a href="javascript:;" id="claimCnt">0</a>건</dd>
            </dl>
        </div>
    </div>
    <div class="srh-form">
        <form id="orderShipManageSearchForm" name="orderShipManageSearchForm" onsubmit="return false;">
            <table>
                <caption>조회</caption>
                <colgroup>
                    <col style="width:75px">
                    <col style="width:392px">
                    <col style="width:80px">
                    <col style="width:220px">
                    <col style="width:100px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row"><label for="schShipDtType">검색기간</label></th>
                        <td colspan="7">
                            <select id="schShipDtType" name="schShipDtType" class="slc-base w-120">
                                <option value="orderDt">주문일</option>
                                <option value="orgShipDt">발송기한</option>
                                <option value="delayShipDt">2차발송기한</option>
                                <option value="confirmDt">주문확인일</option>
                                <option value="paymentFshDt">결제일</option>
                            </select>
                            <span class="inp-date"><input id="schStartDt" name="schShipStartDt" type="text" class="inp-base" placeholder="시작일" maxlength="10"></span> -
                            <span class="inp-date"><input id="schEndDt" name="schShipEndDt" type="text" class="inp-base" placeholder="종료일" maxlength="10"></span>
                            <span class="radio-group mgl5">
                                <input type="radio" id="setTodayBtn" name="setCalBtn"><label for="setTodayBtn"  class="lb-radio">오늘</label>
                                <input type="radio" id="setOneWeekBtn" name="setCalBtn" checked><label for="setOneWeekBtn"  class="lb-radio">1주일</label>
                                <input type="radio" id="setOneMonthBtn" name="setCalBtn"><label for="setOneMonthBtn"  class="lb-radio">1개월</label>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="schKeywordType">검색조건</label></th>
                        <td>
                            <select id="schKeywordType" name="schKeywordType" class="slc-base w-160">
                                <option value="orderItemNo">상품주문번호</option>
                                <option value="bundleNo">배송번호</option>
                                <option value="purchaseOrderNo">주문번호</option>
                                <option value="itemNo">상품번호</option>
                                <option value="receiverNm">수령인</option>
                                <option value="itemNm1">상품명</option>
                                <option value="buyerNm">구매자</option>
                            </select>
                            <input type="text" class="inp-base w-160" id="schKeyword" name="schKeyword">
                        </td>
                        <th scope="row" class="align-r"><label for="schShipStatus">주문상태</label></th>
                        <td>
                            <select id="schShipStatus" name="schShipStatus" class="slc-base w-160">
                                <option value="">전체</option>
                                <c:forEach var="codeDto" items="${shipStatus}" varStatus="status">
                                    <c:if test="${codeDto.mcCd eq 'D1' or codeDto.mcCd eq 'D2'}">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </td>
                        <th scope="row" class="align-r"><label for="schShipDelay">발송지연안내</label></th>
                        <td>
                            <select id="schShipDelay" name="schShipDelay" class="slc-base w-160">
                                <option value="">전체</option>
                                <option value="Y">처리</option>
                                <option value="N">미처리</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="btn-wrap">
                <button type="submit" id="schBtn" class="btn-base-l type4">검색</button>
                <button type="button" id="schResetBtn" class="btn-base-l type3">초기화</button>
            </div>
        </form>
    </div>

    <div class="result-wrap float mgt20">
        <p class="float-l result-p">검색 결과 <b id="orderShipManageSearchCnt">0</b>건</p>
        <div class="float-r">
            <button type="button" id="schExcelDownloadBtn" class="btn-ico-exel"><i>엑셀다운</i></button>
            <select class="slc-base w-110" id="pageViewCnt" title="정렬선택">
                <option value="50">50개씩</option>
                <option value="100"selected>100개씩</option>
                <option value="200">200개씩</option>
                <option value="300">300개씩</option>
                <option value="400">400개씩</option>
                <option value="500">500개씩</option>
            </select>
        </div>
    </div>

    <div class="btn-gruop mgt20">
        <button type="button" id="confirmOrder" class="btn-base-s"><i>주문확인</i></button>
        <button type="button" id="notiDelay" class="btn-base-s"><i>발송지연안내</i></button>
        <button type="button" id="shipAll" class="btn-base-s"><i>선택건 발송처리</i></button>
        <button type="button" id="shipAllExcel" class="btn-base-s"><i>일괄발송처리</i></button>
        <button type="button" id="openShipAddr" class="btn-base-s"><i>배송지 수정</i></button>
        <button type="button" id="openClaim" class="btn-base-s"><i>품절 주문취소</i></button>
    </div>

    <div class="data-wrap" style="height: 500px" id="orderShipManageGrid" ></div>

    <div id="pagingArea" class="paging"></div>

</section>

<div class="modal-wrap">
    <jsp:include page="/WEB-INF/views/pages/sell/pop/notiDelayPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/sell/pop/shipAllExcelPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/sell/pop/shipAllPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/sell/pop/shipAddrPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/sell/pop/itemClaimDetailPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/sell/pop/orderCancelPop.jsp" />
    <jsp:include page="/WEB-INF/views/pages/sell/pop/multiCancelPop.jsp" />
</div>

<script>
    ${orderShipManageGridBaseInfo}

    var parentMain = null;
    var parentGrid = null;
    var theclubFrontUrl = '${theclubFrontUrl}'; // 더클럽 프론트 페이지 URL
    var homeplusFrontUrl = '${homeplusFrontUrl}'; // 홈플러스 프론트 페이지 URL
    var schType = '${schType}'; // 보드 카운트 클릭 타입

    $(document).ready(function() {
        orderShipManageMain.init();
        orderShipManageGrid.init();

        parentMain = orderShipManageMain;
        parentGrid = orderShipManageGrid;

        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/sell/orderShipManageMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/sell/sellUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>