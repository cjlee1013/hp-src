<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="notiDelayPop" class="modal" role="dialog">
    <h2 class="h2">발송 지연 안내</h2>

    <div class="modal-cont">
        <!-- 저장 전 -->
        <div class="tbl-form">
            <table>
                <caption>발송지연안내</caption>
                <colgroup>
                    <col style="width:30%">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">발송기한 변경</th>
                    <td>
                        <span class="inp-date"><input type="text" id="delayShipDt" name="delayShipDt" class="inp-base" title="발송기한 변경"></span>
                    </td>
                </tr>
                <tr>
                    <th scope="row">사유입력</th>
                    <td>
                        <select id="delayShipCd" class="slc-base w-max" title="사유선택">
                            <option value="">선택</option>
                            <c:forEach var="codeDto" items="${delayShipCd}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                        <!-- 기타 일때 -->
                        <div class="words-wrap w-max mgt5" style="display: none;">
                            <textarea id="delayShipMsg" class="textarea w-max" title="사유입력" maxlength="100"></textarea>
                            <span class="words align-l"><i id="msgCount">0</i>/100자</span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <ul class="bul-list mgt20">
            <li>발송기한안내는 최초 설정한 출고예정일 당일을 포함한 6영업일까지 등록가능하며, 발송기한 경과 시 주문취소 처리될 수 있습니다.</li>
            <li>발송기한은 입력 후 수정할 수 없으며, 최초 1회에 한해 안내 가능합니다.</li>
            <li>발송기한과 지연사유는 구매자에게 직접 안내되므로 신중히 작성해 주세요.(인사말과 사과문구는 자동으로 안내되므로 상세사유에 입력하지 않으셔도 됩니다.)</li>
        </ul>
        <div class="btn-wrap mgt20">
            <button type="button" class="btn-base-l" onclick="notiDealyPop.closeModal()"><i>취소</i></button>
            <button type="button" id="saveBtn" class="btn-base-l type4"><i>저장</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="notiDealyPop.closeModal()"><i class="hide" >닫기</i></button>
</div>

<div id="notiDelayResultPop" class="modal" role="dialog">
    <h2 class="h2">발송 지연 안내</h2>

    <div class="modal-cont">
        <div class="box-group mgt25">
            <div class="box">
                <span>등록</span>
                <strong id="totalCnt">0</strong>
            </div>
            <div class="box">
                <span>성공</span>
                <strong id="successCnt">0</strong>
            </div>
            <div class="box">
                <span>실패</span>
                <strong class="fc02" id="failCnt">0</strong>
            </div>
        </div>
        <p class="bul-p mgt20">
            실패한 건은 발송지연 안내를 한 경우, 발송기한 설정이 맞지 않는 경우 또는 배송중인 주문입니다.
        </p>
        <div class="btn-wrap mgt20">
            <button type="button" class="btn-base-l type4" onclick="notiDealyPop.closeResultModal()"><i>확인</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="notiDealyPop.closeResultModal()"><i class="hide" >닫기</i></button>
</div>

<script>
  $(document).ready(function() {
    notiDealyPop.init();
  });
</script>

<script type="text/javascript" src="/static/js/sell/pop/notiDelayPop.js?v=${fileVersion}"></script>