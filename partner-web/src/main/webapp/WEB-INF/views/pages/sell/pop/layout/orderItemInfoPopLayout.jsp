<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<style type="text/css">
    .itemNo {
    border-right: 1px solid #eee;
    text-align: center;
    font-size: 14px;
    }
    .itemName1 {
    font-weight: bold;
    font-size: 14px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    }
    .itemName2 {
    font-weight: bold;
    font-size: 14px;
    }
    .addItem {
    font-size: 12px;
    font-weight: bold;
    }
</style>

<!-- 상품 정보 테이블-->
<span class="text pull-right" id="titleOrderInfo"></span>
<div class="ui wrap-table horizontal" style="width: 100%; height: auto; max-height: 350px; overflow:auto; border-left: 1px solid #ddd;  border-right: 1px solid #ddd;">
    <input type="hidden" id="purchaseOrderNo" name="purchaseOrderNo">
    <input type="hidden" id="bundleNo" name="bundleNo">
    <input type="hidden" id="shipStatus" name="shipStatus">


    <table class="ui table" id="itemInfoTable" style="table-layout: fixed">
        <caption>상품정보 테이블</caption>
        <colgroup>
            <col width="23%">
            <col width="47%">
            <col width="15%">
            <col width="15%">
        </colgroup>
        <div class="ui form inline">
            <thead>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">상품번호</th>
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">상품명</th>
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">상품금액</th>
                <th style="font-size: 15px; text-align: center">신청수량</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </div>
    </table>
</div>
<!-- 상품 정보 테이블 end -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/sell/pop/layout/orderItemInfoPopLayout.js?v=${fileVersion}"></script>

