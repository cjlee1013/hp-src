<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="orderCancelPop" class="modal type-xl" role="dialog">
    <h2 class="h2">주문취소 신청</h2>

    <div class="modal-cont">

        <jsp:include page="layout/orderItemInfoPopLayout.jsp"></jsp:include>

        <div class="mgt10">
            <div class="ui form inline mgt30 mg-l-10 mg-b-10">
                <select id="claimReasonType" name="claimReasonType" class="ui input medium mg-r-20" style="width: 25%;">
                    <option value="PT01">주문상품의 품절/재고없음</option>
                </select>
                <input type="text" id="claimReasonDetail" name="claimReasonDetail" class="ui input medium mg-r-5" style="width: 70%;" maxlength="100" placeholder="취소 상세사유를 입력 (최대 100자)">
            </div>
        </div>

        <div class="btn-wrap mgt30">
            <button type="button" id="saveBtn" class="btn-base-l type4"><i>주문취소</i></button>
            <button type="button" class="btn-base-l" onclick="orderCancelPop.closeModal()"><i>취소</i></button>
        </div>
    </div>
    <input type="hidden" id="claimType" name="claimType" value="C">

    <button type="button" class="btn-close2 ui-modal-close" onclick="orderCancelPop.closeModal()"><i class="hide" >닫기</i></button>
</div>
<script>


</script>
<script type="text/javascript" src="/static/js/sell/pop/orderCancelPop.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/sell/pop/layout/orderClaimCommon.js?v=${fileVersion}"></script>