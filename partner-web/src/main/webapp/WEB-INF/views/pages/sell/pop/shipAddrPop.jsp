<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="shipAddrPop" class="modal type-l" role="dialog">
    <h2 class="h2">배송지 수정</h2>

    <div class="modal-cont">
        <div class="tbl-data">
            <form id="shipAddrPopForm">
                <input type="hidden" id="personalOverseaNo" name="personalOverseaNo">
                <input type="hidden" id="shipMsgCd" name="shipMsgCd">
                <input type="hidden" id="shipMsg" name="shipMsg">
                <input type="hidden" id="shipType" name="shipType">
                <table>
                    <colgroup>
                        <col style="width:20%">
                        <col style="width:auto">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row"><label for="receiverNm">수령인</label></th>
                        <td>
                            <input type="text" id="receiverNm" name="receiverNm" class="inp-base" maxlength="20">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="shipMobileNo">연락처</label></th>
                        <td>
                            <input type="hidden" id="shipMobileNo" name="shipMobileNo" value="">
                            <input type="text" id="shipMobileNo_1" class="inp-base w-80" maxlength="3" > -
                            <input type="text" id="shipMobileNo_2" class="inp-base w-80" maxlength="4"> -
                            <input type="text" id="shipMobileNo_3" class="inp-base w-80" maxlength="4">
                            <span class="text"> 안심번호 : <span id="safetyNm"></span></span>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="zipCode">배송지주소</label></th>
                        <td>
                            <input type="text" id="zipCode" name="zipCode" class="inp-base" readonly> <button id="searchAddr" type="button" class="btn-base">우편번호</button>
                            <input type="text" id="roadBaseAddr" name="roadBaseAddr" class="inp-base w-max mgt5" readonly>
                            <input type="text" id="roadDetailAddr" name="roadDetailAddr" class="inp-base w-max mgt5">
                            <input type="hidden" id="baseAddr" name="baseAddr">
                            <input type="hidden" id="detailAddr" name="detailAddr">
                            <p class="mgt10" id="addr"></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="btn-wrap mgt20">
            <button type="button" class="btn-base-l" onclick="shipAddrPop.closeModal()"><i>취소</i></button>
            <button type="button" id="saveBtn" class="btn-base-l type4"><i>수정</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="shipAddrPop.closeModal()"><i class="hide">닫기</i></button>
</div>
<script>
  $(document).ready(function() {
    shipAddrPop.init();
  });
</script>

<script type="text/javascript" src="/static/js/sell/pop/shipAddrPop.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/validateUtil.js?v=${fileVersion}"></script>