<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="multiCancelPop" class="modal type-xl">
    <h2 class="h2">일괄 취소</h2>
    <div class="modal-cont">
        <p>
            · 취소사유는 상품 품절로 처리됩니다.(변경불가)<br>
            · 취소처리 시 구매자에게 추가배송비가 청구되지 않습니다.
        </p>
        <h4 class="h4 mg-t-20">&#10095; 취소사유</h4>
        <table class="mg-t-5">
            <caption>일괄취소 테이블</caption>
            <colgroup>
                <col width="20%">
                <col width="*%">
            </colgroup>
            <tr>
                <td colspan="2">
                    <div class="ui form inline" >

                    <select id="claimReasonType" name="claimReasonType" class="ui input medium mg-r-5" style="width: 30%">
                        <option value="C007">주문상품의 품절/재고없음</option>
                    </select>
                    <input type="text" id="claimReasonDetail" name="claimReasonDetail" class="ui input medium mg-r-5" style="width: 50%" placeholder="상세사유를 입력해 주세요. (최대 100자)" maxlength="100">
                    </div>
                </td>
            </tr>
        </table>

        <input type="text" id="claimBundleNo" name="claimBundleNo" hidden>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4" onclick="multiCancelPop.save()"><i>일괄취소</i></button>
            <button type="button" class="btn-base-l"  onclick="multiCancelPop.closeModal()"><i>취소</i></button>
            <button type="button" class="btn-close2 ui-modal-close" onclick="multiCancelPop.closeModal()"><i>취소</i></button>
        </div>
    </div>

</div>

<div id="multiCancelResultPop" class="modal type-xl" role="dialog">
    <h2 class="h2">일괄 취소</h2>

    <div class="modal-cont">
        <div class="box-group mgt25">
            <div class="box">
                <span>등록</span>
                <strong id="multiCancelTotalCnt">0</strong>
            </div>
            <div class="box">
                <span>성공</span>
                <strong id="multiCancelSuccessCnt">0</strong>
            </div>
            <div class="box">
                <span>실패</span>
                <strong class="fc02" id="multiCancelFailCnt">0</strong>
            </div>
        </div>
        <p class="bul-p mgt20">
            성공내역은 취소관리에서 확인 가능합니다.
        </p>
        <div class="btn-wrap mgt20">
            <button type="button" class="btn-base-l type4" onclick="multiCancelPop.closeResultModal()"><i>닫기</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="multiCancelPop.closeResultModal()"><i class="hide" >닫기</i></button>
</div>

<script type="text/javascript" src="/static/js/sell/pop/multiCancelPop.js?${fileVersion}"></script>
