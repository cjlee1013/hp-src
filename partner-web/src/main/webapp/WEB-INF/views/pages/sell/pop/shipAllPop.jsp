<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="shipAllPop" class="modal type-l" role="dialog">
    <h2 class="h2">${shipAllPopTitle}</h2>

    <div class="modal-cont">
        <ul class="bul-list">
            <li>선택한 주문에 대해 택배사와 송장번호를 입력해 주세요.</li>
            <li>우편과 직접배송은 송장번호 없이 처리할 수 있으며, 배송중으로 상태값이 변경됩니다.</li>
        </ul>
        <div class="mgt10">
            <select id="shipMethod" name="shipMethod" class="slc-base w-160" title="배송방법">
                <option value="">배송방법 선택</option>
                <c:forEach var="codeDto" items="${shipMethod}" varStatus="status">
                    <c:if test="${codeDto.ref1 eq 'DS'}">
                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                    </c:if>
                </c:forEach>
            </select>
            <select id="dlvCd" name="dlvCd" class="slc-base w-120" title="택배사" style="display: none;">
                <option value="">택배사 선택</option>
                <c:forEach var="codeDto" items="${dlvCd}" varStatus="status">
                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                </c:forEach>
            </select>
            <input type="text" id="invoiceNo" name="invoiceNo" class="inp-base w-257" style="display: none;" placeholder="송장번호" maxlength="30">
            <input type="text" id="scheduleShipDt" name="scheduleShipDt" class="inp-base w-257" style="display: none;" placeholder="배송예정일">
        </div>

        <!-- 그리드 영역 -->
        <div id="shipAllPopGrid" class="data-wrap" style="height: 200px"></div>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l" onclick="shipAllPop.closeModal()"><i>취소</i></button>
            <button type="button" id="saveBtn" class="btn-base-l type4"><i>등록</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="shipAllPop.closeModal()"><i class="hide" >닫기</i></button>
</div>
<script>
  ${shipAllPopGridBaseInfo}

  $(document).ready(function() {
    shipAllPop.init();
    shipAllPopGrid.init();
  });
</script>

<script type="text/javascript" src="/static/js/sell/pop/shipAllPop.js?v=${fileVersion}"></script>