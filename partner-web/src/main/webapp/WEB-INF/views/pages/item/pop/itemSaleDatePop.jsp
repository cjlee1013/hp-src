<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="itemSaleDatePop" class="modal type-l" role="dialog">
    <h2 class="h2">판매기간 일괄변경</h2>

    <form id="itemSaleDateForm" onsubmit="return false;">
        <input type="hidden" id="salePeriodYn" name="salePeriodYn" value="N">
    <div class="modal-cont">
        <p>
            <span id="itemSaleDateCnt">0</span>  개 상품에 대한 판매기간 일괄변경
        </p>


            <div class="radio-group mgt20 " >
                <input type="radio" id="saleDate3d" name="setSaleDate" value="2d" ><label for="saleDate3d" class="lb-radio">3일</label>
                <input type="radio" id="saleDate5d" name="setSaleDate" value="4d" ><label for="saleDate5d" class="lb-radio">5일</label>
                <input type="radio" id="saleDate7d" name="setSaleDate" value="6d" ><label for="saleDate7d" class="lb-radio">7일</label>
                <input type="radio" id="saleDate15d" name="setSaleDate" value="14d" ><label for="saleDate15d" class="lb-radio">15일</label>
                <input type="radio" id="saleDate30d" name="setSaleDate" value="29d" ><label for="saleDate30d" class="lb-radio">30일</label>
                <input type="radio" id="saleDate60d" name="setSaleDate" value="59d" ><label for="saleDate60d" class="lb-radio">60일</label>
                <input type="radio" id="saleDate90d" name="setSaleDate" value="89d" ><label for="saleDate90d" class="lb-radio">90일</label>
                <input type="radio" id="saleDate120d" name="setSaleDate" value="119d" ><label for="saleDate120d" class="lb-radio">120일</label>
                <input type="radio" id="saleDateAlways" name="setSaleDate" value="N" checked><label for="saleDateAlways"  class="lb-radio">상시판매</label>
            </div>

        <div class="saleDateArea" style="display: none">
            <div class="mgt20">
                <span class="inp-date-time w-200"><input type="text" name="saleStartDt" id="saleStartDt" class="inp-base" title="판매기간 시작일"></span> -
                <span class="inp-date-time w-200"><input type="text" name="saleEndDt" id="saleEndDt" class="inp-base" title="판매기간 종료일"></span>
            </div>
        </div>

        <p class="bul-p mgt10">예약판매 상품의 경우 판매기간이 변경되지 않을 수 있습니다.</p>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l" onclick="itemSaleDatePop.closeModal();"><i>취소</i></button>
            <button type="button" class="btn-base-l type4" id="setItemSaleDateBtn"><i>저장</i></button>
        </div>
    </div>
    </form>

    <button type="button" class="btn-close2 ui-modal-close" onclick="itemSaleDatePop.closeModal();"><i class="hide" >닫기</i></button>
</div>

<script>
    var item = {};

    $(document).ready(function() {
        itemSaleDatePop.init();
    });
</script>

<script type="text/javascript" src="/static/js/item/pop/itemSaleDatePop.js?v=${fileVersion}"></script>