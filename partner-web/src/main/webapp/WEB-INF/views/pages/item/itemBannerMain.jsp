<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
    <section class="contents">
    <div class="float">
        <h2 class="h2 float-l">상품공지관리</h2>
        <div class="float-r">
            <button type="button" class="btn-base" id="newRegBtn" name="newRegBtn" onclick="itemBanner.setBannerOpenTab();"><i>신규등록</i></button>
        </div>
    </div>
    <div class="srh-form">
        <form id="itemBannerSearchForm" name="itemBannerSearchForm">
            <table>
                <caption>조회</caption>
                <colgroup>
                    <col style="width:75px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">제목</th>
                    <td>
                        <input type="text" class="inp-base w-490" title="제목" id ="schBannerNm" name="schBannerNm">
                    </td>
                </tr>
                <tr>
                    <th scope="row">구분</th>
                    <td>
                        <select  class="slc-base" title="구분" id="schDispType" name="schDispType">
                            <option value="">전체</option>
                            <c:forEach var="codeDto" items="${dispType}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row">노출여부</th>
                    <td>
                        <select  class="slc-base" title="노출여부" id="schDispYn" name="schDispYn">
                            <option value="">전체</option>
                            <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row">카테고리</th>
                    <td>
                        <select  class="slc-base w-180" title="카테고리 대분류" id="schCateCd1" name="schCateCd1"
                                 data-default="대분류"    style="width:150px;float:left;"
                                 onchange="javascript:commonCategory.changeCategorySelectBox(1,'schCateCd');">
                        </select>
                        <select  class="slc-base w-180" title="카테고리 중분류" id="schCateCd2" name="schCateCd2"
                                 style="width:150px;float:left;" data-default="중분류"
                                 onchange="javascript:commonCategory.changeCategorySelectBox(2,'schCateCd');">
                        </select>
                        <select  class="slc-base w-180" title="카테고리 소분류" id="schCateCd3" name="schCateCd3"
                                 style="width:150px;float:left;" data-default="소분류"
                                 onchange="javascript:commonCategory.changeCategorySelectBox(3,'schCateCd');">
                        </select>
                        <select  class="slc-base" title="카테고리 세분류" id="schCateCd4" name="schCateCd4"
                            style="width:150px;float:left;" data-default="세분류">
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="schDateType">기간</label></th>
                    <td>
                        <select id="schDateType" name="schDateType" class="slc-base w-120">
                            <c:forEach var="codeDto" items="${bannerPeriodType}" varStatus="status">
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected" />
                                </c:if>
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                        <span class="inp-date"><input type="text" class="inp-base" title="검색기간 시작일" id="schStartDt" name="schStartDt"></span> -
                        <span class="inp-date"><input type="text" class="inp-base" title="검색기간 종료일" id="schEndDt" name="schEndDt"></span>
                        <span class="radio-group mgl5">
                            <input type="radio" name="setSchDate" id="setToday" value="0"><label for="setToday" class="lb-radio">오늘</label>
                            <input type="radio" name="setSchDate" id="setYesterday" value="-1d"><label for="setYesterday" class="lb-radio">어제</label>
                            <input type="radio" name="setSchDate" id="setOneMonth" value="-30d" checked><label for="setOneMonth" class="lb-radio">1개월</label>
                            <input type="radio" name="setSchDate" id="setThreeMonth" value="-90d"><label for="setThreeMonth" class="lb-radio">3개월</label>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="btn-wrap">
                <button type="button" class="btn-base-l type4" id="searchBtn">검색</button>
                <button type="button" class="btn-base-l type3" id="searchResetBtn">초기화</button>
            </div>
        </form>
    </div>

    <div class="result-wrap float mgt20">
        <p class="float-l result-p">검색 결과 <b><span id="itemBannerTotalCount">0</span></b>건</p>
    </div>

    <div class="btn-gruop float mgt20">
        <p class="fc02 float-l mgt5">※ 동일한 상품에 구분이 다른 공지가 중복 등록된 경우, 상품별>카테고리별>판매자별 공지 순으로 우선 적용됩니다.</p>
    </div>

    <div class="data-wrap" style="height: 500px" id="itemBannerListGrid"></div>
</section>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemBannerMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/common/itemGridCommon.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>

<script>

    // 상품공지 리얼 그리드
    ${itemBannerListGridBaseInfo}


</script>
