<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="categoryCommissionPop" class="modal type-l" role="dialog">
    <h2 class="h2">홈플러스 판매 수수료 안내</h2>

    <div class="modal-cont">
        <div class="tbl-form type3 mgt10">
            <form id="categoryCommissionPopForm">
                <table>
                    <caption>조회</caption>
                    <colgroup>
                        <col style="width:100px">
                        <col style="width:auto">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row">카테고리</th>
                        <td>
                            <div class="box-srh mgb10">
                                <div class="inp-srh">
                                    <input type="text" class="inp-base" title="종목" placeholder="종목을 입력해주세요" id="schCategoryNm">
                                    <button type="button" class="btn-srh" onclick="categoryCommissionPop.getCategoryNm();"><i class="hide">검색</i></button>
                                </div>
                                <div class="layer-srh">
                                    <ul id="schCateLayer" class="schCateLayer"></ul>
                                </div>
                            </div>
                            <select  id="schCateCd1" name="schLcateCd" class="slc-base w-120" title="카테고리 대분류"
                                     data-default="대분류" onchange="javascript:commonCategory.changeCategorySelectBox(1,'schCateCd');">
                            </select>
                            <select  id="schCateCd2" name="schMcateCd" class="slc-base w-120" title="카테고리 중분류"
                                     data-default="중분류" onchange="javascript:commonCategory.changeCategorySelectBox(2,'schCateCd');">
                            </select>
                            <select  id="schCateCd3" name="schScateCd" class="slc-base w-120" title="카테고리 소분류"
                                     data-default="소분류" onchange="javascript:commonCategory.changeCategorySelectBox(3,'schCateCd');">
                            </select>
                            <select  id="schCateCd4" name="schDcateCd" class="slc-base w-120" title="카테고리 세분류"
                                     data-default="세분류">
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="btn-wrap2">
            <button type="button" class="btn-base-l type4" id="schBtn"><i>검색</i></button>
        </div>
        <div class="data-wrap" style="height: 205px" id="categoryCommissionListGrid">
        </div>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l " onclick="categoryCommissionPop.closeModal();"><i>닫기</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="categoryCommissionPop.closeModal();"><i class="hide" >닫기</i></button>
</div>

<script>
    //그리드
    ${categoryCommissionListGridBaseInfo}
</script>

<script type="text/javascript" src="/static/js/item/pop/categoryCommissionPop.js?v=${fileVersion}"></script>