<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<section class="contents">
    <form id="itemSetForm" name="itemSetForm">
        <input type="hidden" name="mallType" id="mallType" value="DS" />
        <input type="hidden" name="storeType" id="storeType" value="DS" />
        <input type="hidden" id="itemNo" name="itemNo" value="${itemNo}">
        <input type="hidden" id="isMod" name="isMod" value="${isMod}">

        <div class="float sticky-wrap-">
            <h2 class="h2 float-l mgt5" >${title}
                <span class="fc02"><i class="bul-require">필수항목</i>표시는 필수입력 항목입니다.</span></h2>
            <div class="float-r regBtnGroup">
                <button type="button" class="btn-base type2" id="setItemTempTopBtn"><i>임시저장</i></button>
                <button type="button" class="btn-base type4 w-80" id="setItemTopBtn"><i>등록</i></button>
            </div>
        </div>

        <div id="itemStatusReasonArea" class="tbl-form mgt30" style="display: none;">
            <table>
                <caption>상품정보</caption>
                <colgroup>
                    <col style="width:180px">
                    <col style="width:auto">
                    <col style="width:180px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">상품번호</th>
                    <td id="itemNoTxt"></td>
                    <th scope="row">상품유형</th>
                    <td id="itemTypeNm"></td>
                </tr>
                <tr>
                    <th scope="row">최종 상품상태</th>
                    <td colspan="3">
                        <span id="itemStatusTotal"></span>
                        <div>
                            <span id="itemStatusDesc"></span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div id="uiAcco1" class="form-acco acco-wrap">
            <dl class="acco">
                <dt class="acco-tit">
                    <h3 class="h3">기본 정보</h3>
                    <div class="float-r">
                        <button type="button" id="openCommissionInfoPop" class="btn-base-s"><i>판매수수료 안내</i></button>
                        <button type="button" class="acco-btn"></button>
                    </div>
                </dt>
                <dd class="acco-pnl">
                    <div class="tbl-form">
                        <table>
                            <caption>기본정보</caption>
                            <colgroup>
                                <col style="width:180px">
                                <col style="width:auto">
                            </colgroup>
                            <tbody>
                            <tr>
                                <th scope="row">
                                    카테고리<i class="bul-require">필수항목</i>
                                    <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipCategory"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <div class="box-srh">
                                        <div class="inp-srh">
                                            <input type="text" class="inp-base" id="searchCategoryNm" title="카테고리" placeholder="카테고리를 입력해주세요 (예: 운동화)" autocomplete="off">
                                            <button type="button" class="btn-srh"  onclick="itemSetMain.basic.getCategoryNm();"><i class="hide">검색</i></button>
                                        </div>
                                        <div class="layer-srh">
                                            <ul id="searchCateLayer" class="searchCateLayer"></ul>
                                        </div>
                                    </div>
                                    <div class="mgt10 cateCd4Div">

                                        <select class="slc-base w-180" title="카테고리 대분류" id="cateCd1" name="lcateCd"
                                                 data-default="대분류" onchange="javascript:commonCategory.changeCategorySelectBox(1,'cateCd');">
                                        </select>
                                        <select class="slc-base w-180" title="카테고리 중분류" id="cateCd2" name="mcateCd"
                                                 data-default="중분류"  onchange="javascript:commonCategory.changeCategorySelectBox(2,'cateCd');">
                                        </select>
                                        <select class="slc-base w-180" title="카테고리 소분류" id="cateCd3" name="scateCd"
                                                 data-default="소분류" onchange="javascript:commonCategory.changeCategorySelectBox(3,'cateCd');">
                                        </select>
                                        <select class="slc-base" title="카테고리 세분류 " id="cateCd4" name="dcateCd" data-default="세분류">
                                        </select>
                                    </div>
                                    <p class="txt mgt10">선택한 카테고리 : <span class="fc05" id="categoryFullNm"></span></p>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    상품명<i class="bul-require">필수항목</i>
                                    <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipItemNm"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <div class="words-wrap itemNmDiv">
                                        <input type="text" class="inp-base" title="상품명" id="itemNm" name="itemNm" maxlength="50">
                                        <span class="words"><i><span id="itemNmCount">0</span></i>/50자</span>
                                    </div>
                                    <p class="bul-p">판매 상품과 직접 관련이 없는 다른 상품명, 스팸성 키워드 입력 시 관리자에 의해 판매 금지될 수 있습니다.
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    상품유형
                                    <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipItemType"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <select  class="slc-base" title="상품유형" id="itemType" name="itemType">
                                        <c:forEach var="codeDto" items="${itemType}" varStatus="status">
                                            <c:set var="selected" value="" />
                                            <c:if test="${codeDto.ref1 eq 'df'}">
                                                <c:set var="selected" value="selected" />
                                            </c:if>
                                            <c:if test="${codeDto.ref2 eq ''}">
                                                <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                            </c:if>
                                            <c:if test="${codeDto.ref2 eq 'DS' && codeDto.mcCd eq codeDto.types}">
                                                <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">노출여부</th>
                                <td>
                                    <span class="radio-group size2">
                                         <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                                             <c:set var="checked" value="" />
                                             <c:if test="${codeDto.ref1 eq 'df'}">
                                                 <c:set var="checked" value="checked" />
                                             </c:if>
                                             <input type="radio" id="dispYn${codeDto.mcCd}" name="dispYn" value="${codeDto.mcCd}" ${checked}><label for="dispYn${codeDto.mcCd}"  class="lb-radio">${codeDto.mcNm}</label>
                                         </c:forEach>
                                    </span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </dd>
            </dl>

            <dl class="acco">
                <dt class="acco-tit">
                    <h3 class="h3">판매정보</h3>
                    <div class="float-r">
                        <button type="button" class="acco-btn"></button>
                    </div>
                </dt>
                <dd class="acco-pnl">
                    <div class="tbl-form">
                        <table>
                            <caption>판매정보</caption>
                            <colgroup>
                                <col style="width:180px">
                                <col style="width:auto">
                            </colgroup>
                            <tbody>
                            <tr>
                                <th scope="row">정상가격
                                    <i class="bul-require">필수항목</i><button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipOriginPrice"><i class="hide">팁열기</i></button>
                                </th>
                                <td class="originPriceDiv">
                                    <input type="text" id="originPrice" name="originPrice" maxlength="11" data-comma-count="2" class="inp-base" title="정상가격" placeholder="소비자권장가격 입력"> 원
                                </td>
                            </tr>
                            <tr id="purchasePriceArea" style="display: none">
                                <th scope="row">매입가<i class="bul-require">필수항목</i></th>
                                <td class="purchasePriceDiv">
                                    <input type="text" id="purchasePrice" name="purchasePrice" maxlength="11" data-comma-count="2" class="inp-base" title="매입가"> 원 (VAT 제외)
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">판매가격<i class="bul-require">필수항목</i><button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipSalePrice"><i class="hide">팁열기</i></button>
                                </th>
                                <td class="salePriceDiv">
                                    <input type="text" id="salePrice" name="salePrice" maxlength="11" data-comma-count="2" class="inp-base" title="판매가격"> 원
                                    <input type="hidden" id="commissionType" name="commissionType" value="R">
                                    <input type="hidden" id="commissionRate" name="commissionRate" value="">
                                    <input type="hidden" id="commissionPrice" name="commissionPrice" value="">

                                    <span>[ <span id="commissionTitle">수수료</span> : <i><span id="baseCommissionRate">00.00</span></i> ]</span>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    할인가격
                                    <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipDcPrice"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <span class="radio-group size2">
                                         <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                                             <c:set var="checked" value="" />
                                             <c:if test="${codeDto.ref1 eq 'df'}">
                                                 <c:set var="checked" value="checked" />
                                             </c:if>
                                             <input type="radio" id="dcYn${codeDto.mcCd}" name="dcYn" value="${codeDto.mcCd}" ${checked}><label for="dcYn${codeDto.mcCd}"  class="lb-radio">${codeDto.mcNm}</label>
                                         </c:forEach>
                                    </span>

                                    <!-- 할인 설정 시 -->
                                    <div class="dcArea" style="display: none;">
                                        <div class="mgt15">
                                            <input type="text" id="dcPrice" name="dcPrice" class="inp-base" title="할인" maxlength="9" placeholder="고정가격 입력" > 원
                                            <span>[ 할인율 : <i><span id="discountRate">00.00</span>%</i> ]</span>
                                            <p class="bul-p mg-t-10"> 입력된 할인가격으로 판매됩니다. 할인차액이 아닌 할인 판매가격을 입력해주세요.</p>
                                        </div>
                                        <div class="mgt15">
                                            <input type="checkbox" name="dcPeriodYn" id="dcPeriodYn" value="Y"><label for="dcPeriodYn" class="lb-check mgr10">특정기간만 할인</label>
                                                <span class="inp-date-time w-200"><input type="text" name="dcStartDt" id="dcStartDt" class="inp-base" title="할인기간 시작일" ></span> -
                                                <span class="inp-date-time w-200"><input type="text"  name="dcEndDt" id="dcEndDt" class="inp-base" title="할인기간 종료일" ></span>
                                        </div>

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">과세여부<i class="bul-require">필수항목</i></th>
                                <td>
                                    <span class="radio-group size2">
                                        <c:forEach var="codeDto" items="${taxYn}" varStatus="status">
                                            <c:if test="${codeDto.ref2 eq 'DS'}">
                                                <input type="radio" id="taxYn${codeDto.mcCd}" name="taxYn" value="${codeDto.mcCd}" ><label for="taxYn${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                                            </c:if>
                                        </c:forEach>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">판매기간<i class="bul-require">필수항목</i></th>
                                <td>
                                    <span class="radio-group size2">
                                        <c:forEach var="codeDto" items="${salePeriodYn}" varStatus="status">
                                            <c:set var="checked" value="" />
                                            <c:if test="${codeDto.ref1 eq 'df'}">
                                                <c:set var="checked" value="checked" />
                                            </c:if>
                                            <input type="radio" id="salePeriodYn${codeDto.mcCd}" name="salePeriodYn" value="${codeDto.mcCd}" ${checked} ><label for="salePeriodYn${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                                         </c:forEach>
                                    </span>
                                    <!-- 판매기간 설정시 -->
                                    <div class="mgt15 salePeriodArea" style="display: none;">
                                        <div class="radio-group">
                                            <input type="radio" id="saleDate3d" name="setSaleDate" value="2d" ><label for="saleDate3d" class="lb-radio">3일</label>
                                            <input type="radio" id="saleDate5d" name="setSaleDate" value="4d" ><label for="saleDate5d" class="lb-radio">5일</label>
                                            <input type="radio" id="saleDate7d" name="setSaleDate" value="6d" ><label for="saleDate7d" class="lb-radio">7일</label>
                                            <input type="radio" id="saleDate15d" name="setSaleDate" value="14d" ><label for="saleDate15d" class="lb-radio">15일</label>
                                            <input type="radio" id="saleDate30d" name="setSaleDate" value="29d" ><label for="saleDate30d" class="lb-radio">30일</label>
                                            <input type="radio" id="saleDate60d" name="setSaleDate" value="59d" ><label for="saleDate60d" class="lb-radio">60일</label>
                                            <input type="radio" id="saleDate90d" name="setSaleDate" value="89d" ><label for="saleDate90d" class="lb-radio">90일</label>
                                            <input type="radio" id="saleDate120d" name="setSaleDate" value="119d" ><label for="saleDate120d" class="lb-radio">120일</label>
                                        </div>
                                        <div class="mgt15">
                                            <span class="inp-date-time w-200"><input type="text" name="saleStartDt" id="saleStartDt" class="inp-base" title="판매기간 시작일"></span> -
                                            <span class="inp-date-time w-200"><input type="text" name="saleEndDt" id="saleEndDt" class="inp-base" title="판매기간 종료일"></span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    예약판매
                                    <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipRsv"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <span class="radio-group size2">
                                            <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                                                <c:set var="checked" value="" />
                                                <c:if test="${codeDto.ref1 eq 'df'}">
                                                    <c:set var="checked" value="checked" />
                                                </c:if>
                                                <input type="radio" id="rsvYn${codeDto.mcCd}" name="rsvYn" value="${codeDto.mcCd}" ${checked}><label for="rsvYn${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                                            </c:forEach>
                                    </span>

                                    <!-- 예약판매설정 시-->
                                    <div class="rsvArea" style="display: none">
                                        <input type="hidden" name="saleRsvNo" id="saleRsvNo">
                                        <div class="mgt15">
                                            <label for="rsvStartDt" class="mgr10">예약판매기간</label>
                                            <span class="inp-date-time"><input type="text" name="rsvStartDt" id="rsvStartDt" class="inp-base" title="예약기간 시작일"></span> -
                                            <span class="inp-date-time"><input type="text" name="rsvEndDt" id="rsvEndDt" disabled class="inp-base" title="예약ㄹ기간 종료일"></span>
                                        </div>
                                        <div class="mgt15">
                                            <label for="shipStartDt" class="mgr10">출하일</label>
                                            <span class="inp-date-time"><input type="text" name="shipStartDt" id="shipStartDt" class="inp-base" title="출하일"></span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">재고수량<i class="bul-require">필수항목</i><button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipStockQty"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <div class="stockQtyDiv">
                                        <input type="text" name="stockQty" id="stockQty" class="inp-base" title="재고수량" placeholder="숫자만 입력" maxlength="9" data-comma-count="1"> 개
                                        <input type="checkbox" class="changeStockQtyArea" id="changeStockQtyCheck" name="chgStockQtyYn" value="Y" checked> <label for="changeStockQtyCheck" class="lb-check mgl10 changeStockQtyArea" >수량변경</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">판매단위수량  <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipSaleUnit"><i class="hide">팁열기</i></button></th>
                                <td>
                                    <input type="text"  name="saleUnit" id="saleUnit" maxlength="3" class="inp-base" title="판매단위수량"> 개 씩 구매가능
                                    <p class="bul-p mgt10">판매단위수량은 2개 이상일 경우에 입력해 주세요. 입력하지 않아도 기본 1개로 적용됩니다.</p>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">최소구매수량  <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipPurchaseMinQty"><i class="hide">팁열기</i></button></th>
                                <td>
                                    <input type="text" class="inp-base" title="최소구매수량" name="purchaseMinQty" id="purchaseMinQty" maxlength="3"> 개 부터 구매가능
                                    <p class="bul-p mgt10">최소구매수량은 2개 이상일 경우에 입력해 주세요. 입력하지 않아도 기본 1개로 적용됩니다.</p>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">구매수량제한 <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipPurchaseLimitYn"><i class="hide">팁열기</i></button></th>
                                <td>
                                    <span class="radio-group size2">
                                        <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                                            <c:set var="checked" value="" />
                                            <c:if test="${codeDto.ref1 eq 'df'}">
                                                <c:set var="checked" value="checked" />
                                            </c:if>

                                            <input type="radio"  name="purchaseLimitYn" id="purchaseLimitYn${codeDto.mcCd}" value="${codeDto.mcCd}" ${checked}><label for="purchaseLimitYn${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                                        </c:forEach>
                                        </span>
                                    </span>
                                    <div class="mgt15" id="purchaseLimitArea" style="display: none;" >
                                        <select class="slc-base" name="purchaseLimitDuration" id="purchaseLimitDuration" title="제한 선택">
                                            <c:set var="dfPurchaseLimitDuration" value="" />
                                            <c:forEach var="codeDto" items="${limitDuration}" varStatus="status">
                                                <c:if test="${codeDto.ref1 eq 'df'}">
                                                    <c:set var="dfPurchaseLimitDuration" value="${codeDto.mcCd}" />
                                                </c:if>
                                                <option value="${codeDto.mcCd}" data-reference1="${codeDto.ref1}" data-reference2="${codeDto.ref2}" data-reference3="${codeDto.ref3}">${codeDto.mcNm}</option>
                                            </c:forEach>
                                        </select>
                                        <div class="mgt10">
                                            <span class="fc01">
                                                    구매자 1명이
                                               <input type="text" name="purchaseLimitDay" id="purchaseLimitDay" class="inp-base w-80 mgl5 purchaseLimitDuration purchaseLimitDurationO" title="최대 일수" value="1" style="display: none;">
                                                <span class="text purchaseLimitDuration purchaseLimitDurationO" style="display: none;">일 동안 </span> 최대
                                               <input type="text" name="purchaseLimitQty" id="purchaseLimitQty"  class="inp-base w-110 mgl5" title="최대 개수" maxlength="3">
                                                개 까지 구매가능
                                            </span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">장바구니제한</th>
                                <td>
                                    <span class="radio-group size2">
                                        <c:forEach var="codeDto" items="${limitYn}" varStatus="status">
                                            <c:set var="checked" value="" />
                                            <c:if test="${codeDto.ref1 eq 'df'}">
                                                <c:set var="checked" value="checked" />
                                            </c:if>
                                            <input type="radio" id="cartLimitYn${codeDto.mcCd}" name="cartLimitYn" value="${codeDto.mcCd}" ${checked}><label for="cartLimitYn${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                                        </c:forEach>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">선물하기</th>
                                <td>
                                    <span class="radio-group size2">
                                        <c:forEach var="codeDto" items="${giftYn}" varStatus="status">
                                            <c:set var="checked" value="" />
                                            <c:if test="${codeDto.ref1 eq 'df'}">
                                                <c:set var="checked" value="checked" />
                                            </c:if>
                                            <input type="radio" id="giftYn${codeDto.mcCd}" name="giftYn" value="${codeDto.mcCd}" ${checked}><label for="giftYn${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                                        </c:forEach>
                                    </span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </dd>
            </dl>

            <dl class="acco">
                <dt class="acco-tit">
                    <h3 class="h3">옵션정보</h3>
                    <div class="float-r">
                        <!--<button type="button" class="btn-base-s"><i>미리보기</i></button>-->
                        <button type="button" class="acco-btn"></button>
                    </div>
                </dt>
                <dd class="acco-pnl">
                    <div class="tbl-form">
                        <input type="hidden" name="optTitleDepth" id="optDepth" value="0"/>
                        <input type="hidden" name="opt1Title" id="opt1Title" value=""/>
                        <input type="hidden" name="opt2Title" id="opt2Title" value=""/>

                        <table>
                            <caption>옵션정보</caption>
                            <colgroup>
                                <col style="width:180px">
                                <col style="width:auto">
                            </colgroup>
                            <tbody>
                            <tr>
                                <th scope="row">텍스트형 옵션 <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipOptTxt"><i class="hide">팁열기</i></button></th>
                                <td>
                                    <span class="radio-group size2">
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <c:set var="checked" value="" />
                                        <c:if test="${codeDto.mcCd eq 'N'}">
                                            <c:set var="checked" value="checked" />
                                        </c:if>
                                        <input type="radio" id="optTxtUseYn${codeDto.mcCd}" name="optTxtUseYn" value="${codeDto.mcCd}" ${checked} ><label for="optTxtUseYn${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                                    </c:forEach>
                                    </span>
                                </td>
                            </tr>
                            <!-- 텍스트형 옵션 사용시 노출-->
                            <tr class="setOptTxtFormAll" style="display: none;">
                                <th scope="row">텍스트형 옵션 목록</th>
                                <td>
                                    <div class="setOptTxtFormAll">
                                        <label for="optTxtDepth" class="lb-block">옵션단계설정</label>
                                        <select class="slc-base" name="optTxtDepth" id="optTxtDepth" title="옵션단계설정">
                                            <c:forEach var="item"  begin="1" end="2" step="1" varStatus="status">
                                                <option value="${status.count}"  <c:if test="${status.count eq 1}"> selected </c:if> >${status.count}단계</option>
                                            </c:forEach>
                                    </select>
                                    </div>

                                    <div class="mgt15 optTxtDepthTr" id="optTxtDepthTr1" >
                                        <label for="opt1Text" class="mgr10">1단계 옵션명</label>
                                        <input type="text" id="opt1Text" name="opt1Text" class="inp-base w-490" title="1단계 옵션명" placeholder="예시 : 이니셜 문구" maxlength="25">
                                        <p class="bul-p mgt10">구매자가 주문 시 입력해야 하는 옵션의 타이틀을 입력해주세요.</p>
                                    </div>

                                    <div class="mgt15 optTxtDepthTr" id="optTxtDepthTr2" >
                                        <label for="opt2Text" class="mgr10">2단계 옵션명</label>
                                        <input type="text" id="opt2Text" name="opt2Text" class="inp-base w-490" title="2단계 옵션명" placeholder="예시 : 이니셜 문구" maxlength="25" >
                                        <p class="bul-p mgt10">구매자가 주문 시 입력해야 하는 옵션의 타이틀을 입력해주세요.</p>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </dd>
            </dl>

            <dl class="acco">
                <dt class="acco-tit">
                    <h3 class="h3">이미지 정보</h3>
                    <div class="float-r">
                        <!-- <button type="button" class="btn-base-s"><i>이미지 제작 가이드</i></button> -->
                        <button type="button" class="acco-btn"></button>
                    </div>
                </dt>
                <dd class="acco-pnl">
                    <div class="tbl-form" >
                        <table>
                            <caption>이미지 정보</caption>
                            <colgroup>
                                <col style="width:180px">
                                <col style="width:auto">
                            </colgroup>
                            <tbody>
                            <tr>
                                <th scope="row">상품 이미지 <i class="bul-require">필수항목</i> <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipImg"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <div id="imgMNDiv">
                                        <ul class="list-upload">
                                        <c:forEach var="codeDto" items="${itemImgType}" varStatus="status">
                                            <c:if test="${codeDto.mcCd eq 'MN'}">
                                                <c:forEach var="idx" begin="0" end="${codeDto.ref1-1}" step="1">
                                                <li>
                                                    <div class="upload-wrap preview">
                                                        <div class="imgDisplayView" style="display:none;">
                                                            <button type="button" id="productImgDelBtn${idx}" class="btn-clear deleteImgBtn" onclick="itemCommon.img.deleteImg($(${codeDto.mcCd}${idx}));"><i class="hide">삭제</i></button>
                                                            <div id="${codeDto.mcCd}${idx}" class="thumb uploadType itemImg" inputId="uploadMNIds" data-processkey="ItemMainImage" style="cursor:hand;">
                                                                <img class="imgUrlTag" src="" alt="상품이미지" >
                                                                <input type="hidden" class="mainYn" name="imgList[].mainYn" value="N"/>
                                                                <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                                                <input type="hidden" class="imgNo" name="imgList[].imgNo" value=""/>
                                                                <input type="hidden" class="imgNm" name="imgList[].imgNm" value=""/>
                                                                <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                                                <input type="hidden" class="imgType" name="imgList[].imgType" value="${codeDto.mcCd}"/>
                                                            </div>
                                                        </div>
                                                        <div class="imgDisplayReg">
                                                            <button type="button" id="productImgRegBtn${idx}" class="btn-base-s" onclick="itemCommon.img.clickFile($(this));" data-field-name="mainImg${idx}"><i>등록</i></button>
                                                        </div>
                                                    </div>
                                                    <div class="imgMainYnRadioView">
                                                        <input type="radio" id="mainYnRadio${idx}" name="mainYnRadio" value="${idx}"><label for="mainYnRadio${idx}" class="lb-radio">대표이미지</label>
                                                    </div>
                                                </li>

                                                </c:forEach>
                                            </c:if>
                                        </c:forEach>
                                        </ul>
                                        <p class="bul-p mgt15">사이즈: 1200 X 1200 / 용량: 4MB 이하 / 파일 : JPG, JPEG, PNG </p>
                                    </div>
                                </td>
                            </tr>
                            <!--리스팅 이미지 등록전-->
                            <tr>
                                <th scope="row">리스팅 이미지<i class="bul-require">필수항목</i> <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipListImg"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <div class="upload-wrap preview">
                                        <div class="imgDisplayView" style="display:none;">
                                            <button type="button" id="listImgDelBtn" class="btn-clear deleteImgBtn" onclick="itemCommon.img.deleteImg($('#imgLST'));"><i class="hide">삭제</i></button>
                                            <div id='imgLST' class="thumb uploadType itemImg" data-type="LST"  data-processkey="ItemListImage" >
                                                <img class="imgUrlTag" src="" alt="상품이미지">
                                                    <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value="" id="listImgUrl"/>
                                                    <input type="hidden" class="imgNo" name="imgList[].imgNo" value=""/>
                                                    <input type="hidden" class="imgNm" name="imgList[].imgNm" value="" id="listImgNm"/>
                                                    <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                                    <input type="hidden" class="imgType" name="imgList[].imgType" value="LST"/>
                                            </div>
                                        </div>
                                        <div class="imgDisplayReg">
                                            <button type="button" id="listImgRegBtn" class="btn-base-s" onclick="itemCommon.img.clickFile($(this));" data-field-name="listImg"><i>등록</i></button>
                                        </div>

                                    </div>
                                    <p class="bul-p mgt15">사이즈: 900 X 470 / 용량: 4MB 이하 / 파일 : JPG, JPEG, PNG</p>
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">상품 상세 설명<i class="bul-require">필수항목</i><button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipItemDetail"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <div id="detailHtml">
                                        <div id="editor1" class="editor-wrap" ></div>
                                        <div class="align-r mgt10">
                                            <!--<button type="button" class="btn-base-s"><i>미리보기</i></button>-->
                                            <button type="button" class="btn-base-s" onclick="if (confirm('등록된 상품상세설명을 모두 삭제하시겠습니까?')){commonEditor.setHtml($('#editor1'), '');}"><i>초기화</i></button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </dd>
            </dl>

            <dl class="acco shipArea">
                <dt class="acco-tit">
                    <h3 class="h3">배송정보</h3>
                    <div class="float-r">
                        <button type="button" class="acco-btn"></button>
                    </div>
                </dt>
                <dd class="acco-pnl">
                    <div class="tbl-form">
                        <table>
                            <caption>배송정보</caption>
                            <colgroup>
                                <col style="width:180px">
                                <col style="width:auto">
                            </colgroup>
                            <tbody>
                            <tr>
                                <th scope="row">배송정책<i class="bul-require">필수항목</i> <button type="button" class="btn-tip ui-tooltip-btn"  aria-describedby="tooltipShip"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <div class="shipPolicyNoDiv">
                                    <select class="slc-base w-325" title="배송정책" name="shipPolicyNo" id="shipPolicyNo" >
                                        <option value="">선택하세요</option>
                                    </select>
                                    <button type="button" class="btn-base" id="openShipPolicyPop" ><i>배송정보관리</i></button>
                                    <p class="bul-p mgt10">[배송정보관리] 버튼을 클릭하여 배송정보를 등록 후 선택해주세요</p>
                                    </div>
                                    <div class="tbl-data02">
                                        <table>
                                            <caption>배송정책표</caption>
                                            <colgroup>
                                                <col style="width:17%">
                                                <col style="width:auto" >
                                                <col style="width:17%">
                                                <col style="width:33%">
                                            </colgroup>
                                            <tbody>
                                            <tr>
                                                <th scope="row">배송정책번호</th>
                                                <td colspan="3"><span class="text deliveryDetail" id="detailShipPolicyNo"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">배송정책명</th>
                                                <td><span class="text deliveryDetail" id="detailShipPolicyNm"></span></td>
                                                <th scope="row">배송주체</th>
                                                <td> <span class="text deliveryDetail" id="detailShipMngNm"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">배송유형</th>
                                                <td><span class="text deliveryDetail" id="detailShipTypeNm"></span></td>
                                                <th scope="row">배송방법</th>
                                                <td><span class="text deliveryDetail" id="detailShipMethodNm"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">배송비종류</th>
                                                <td><span class="text deliveryDetail" id="detailShipKindNm"></span></td>
                                                <th scope="row">배송비</th>
                                                <td><span class="text deliveryDetail" id="detailShipFee"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">배송비 결제방식</th>
                                                <td><span class="text deliveryDetail" id="detailPrepaymentYn"></span></td>
                                                <th scope="row">배송비 노출여부</th>
                                                <td><span class="text deliveryDetail" id="detailShipFeeDispYn"></span></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">반품교환 배송비 <button type="button" class="btn-tip ui-tooltip-btn"  aria-describedby="tooltipClaimShipFee"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <input type="text"  name="claimShipFee" id="claimShipFee" maxlength="7" class="inp-base" title="반품교환 배송비"> 원 (편도)
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">출고지<i class="bul-require">필수항목</i></th>
                                <td class= "releaseZipcodeDiv releaseZipAddr1Div">
                                    <input type="text" name="releaseZipcode" id="releaseZipcode" class="inp-base" title="우편번호" readonly> <button type="button" class="btn-base" onclick="zipCodePopup('itemCommon.ship.releaseZipcode');"><i>우편번호</i></button>
                                    <div class="mgt10">
                                        <input type="text" name="releaseAddr1" id="releaseAddr1" class="inp-base w-325" title="주소" readonly>
                                        <input type="text"  name="releaseAddr2" id="releaseAddr2" class="inp-base w-490" title="상세주소" autocomplete="off">
                                    </div>
                                    <div class="mgt15">
                                        <input type="checkbox" id="copyPartnerAddr"><label for="copyPartnerAddr" class="lb-check">사업자주소 동일 적용</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">회수지<i class="bul-require">필수항목</i></th>
                                <td class="returnZipcodeDiv returnAddr1Div">
                                    <input type="text" name="returnZipcode" id="returnZipcode" class="inp-base" title="우편번호" readonly> <button type="button" class="btn-base" onclick="zipCodePopup('itemCommon.ship.returnZipcode');"><i>우편번호</i></button>
                                    <div class="mgt10">
                                        <input type="text" name="returnAddr1" id="returnAddr1" class="inp-base w-325" title="주소" readonly>
                                        <input type="text" name="returnAddr2" id="returnAddr2" class="inp-base w-490" title="상세주소">
                                    </div>
                                    <div class="mgt15">
                                        <input type="checkbox" id="copyReleaseAddr"><label for="copyReleaseAddr"  class="lb-check">출고지주소 동일 적용</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">출고기한<i class="bul-require">필수항목</i> <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipReleaseDay"><i class="hide">팁열기</i></button></th>
                                <td>
                                    <div class="radio-group size2">
                                        <input type="radio" id="shipReleaseDayT" name="shipReleaseDay" value="T" ><label for="shipReleaseDayT" class="lb-radio">오늘발송</label>
                                        <input type="radio" id="shipReleaseDayN" name="shipReleaseDay" value="N" checked><label for="shipReleaseDayN" class="lb-radio">일반발송</label>
                                        <input type="hidden" name="releaseDay" id="releaseDay" value="2" />
                                    </div>
                                    <!-- 오늘발송 -->
                                    <div class="mgt15" id="releaseTimeDetailInfo" style="display: none;">
                                        당일 <select class="slc-base w-110" name="releaseTime" id="releaseTime" title="출고기한 시간 선택">
                                            <option value="">선택</option>
                                            <c:forEach var="item"  begin="1" end="24" step="1" varStatus="status">
                                                <option value="${status.count}">${status.count}</option>
                                            </c:forEach>
                                            </select> 시 결제건까지 오늘발송처리
                                    </div>
                                    <!-- 일반발송 -->
                                    <div class="mgt15" id="releaseDayDetailInfo">
                                        <select class="slc-base w-110" title="일반발송 기간" name="releaseDayTemp" id="releaseDayTemp">
                                            <c:forEach var="item"  begin="2" end="15" step="1" varStatus="status">
                                                <c:set var="selected" value="" />
                                                <c:if test="${item eq '2'}">
                                                    <c:set var="selected" value="selected" />
                                                </c:if>
                                                <option value="${item}" ${selected}>${item}</option>
                                            </c:forEach>
                                            <option value="-1">미정</option>
                                        </select> 일 이내 발송처리
                                    </div>
                                    <div class="mgt15">
                                        <input type="checkbox" id='shipHolidayExceptYn' name='holidayExceptYn' value="Y"> <label for="shipHolidayExceptYn" class="lb-check">주말/공휴일 제외</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">안심번호 서비스</th>
                                <td>
                                    <div class="radio-group size2">
                                        <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                            <c:set var="checked" value="" />
                                            <c:if test="${codeDto.mcCd eq 'N'}">
                                                <c:set var="checked" value="checked" />
                                            </c:if>
                                            <input type="radio" id="safeNumberUseYn${codeDto.mcCd}" name="safeNumberUseYn" value="${codeDto.mcCd}" ><label for="safeNumberUseYn${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                                        </c:forEach>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </dd>
            </dl>

            <dl class="acco">
                <dt class="acco-tit">
                    <h3 class="h3">속성 정보</h3>
                    <div class="float-r">
                        <button type="button" class="acco-btn"></button>
                    </div>
                </dt>
                <dd class="acco-pnl">
                    <div class="tbl-form">
                        <table>
                            <caption>속성 정보</caption>
                            <colgroup>
                                <col style="width:180px">
                                <col style="width:auto">
                            </colgroup>
                            <tbody>
                            <tr>
                                <th scope="row">브랜드 <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipBrand"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <div class="box-srh mgr10 w-490">
                                        <div class="inp-srh">
                                            <input type="text" name="brandNm" id="brandNm" class="inp-base" title="브랜드" autocomplete="off">
                                            <input type="hidden" name="brandNo" id="brandNo" value="" />
                                            <button type="button" class="btn-srh" id="getBrandBtn"><i class="hide">검색</i></button>
                                        </div>

                                        <div class="layer-srh" id="brandArea">
                                            <ul id="searchBrandLayer" class="searchBrandLayer"></ul>
                                        </div>
                                    </div>
                                    <!--브랜드 선택시 노출-->
                                    <p class="txt mgt10">선택한 브랜드 : <span class="slcted-opt" id="spanBrandNm"></span></p>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">제조사</th>
                                <td>
                                    <div class="box-srh mgr10  w-490">
                                        <div class="inp-srh">
                                            <input type="text" name="makerNm" id="makerNm" class="inp-base" title="제조사" autocomplete="off">
                                            <input type="hidden" name="makerNo" id="makerNo" value="" />
                                            <button type="button" class="btn-srh makerInfo" id="getMakerBtn"><i class="hide">검색</i></button>
                                        </div>
                                        <div class="layer-srh" id="makerArea">
                                            <ul id="searchMakerLayer" class="searchMakerLayer"></ul>
                                        </div>
                                    </div>
                                    <!--제조사 선택시 노출-->
                                    <p class="txt mgt10">선택한 제조사 : <span class="slcted-opt" id="spanMakerNm"></span></p>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">상품속성 <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipAttr"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <div class="tbl-data02" id="attrDiv" >
                                        <table>
                                            <caption>상품속성표</caption>
                                            <colgroup>
                                                <col style="width:120px">
                                                <col style="width:auto">
                                            </colgroup>
                                            <tbody id="attrArea">
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">제조일자</th>
                                <td>
                                    <span class="inp-date-time"><input type="text" id="makeDt" name="makeDt" class="inp-base" title="제조일자"></span>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">유효일자</th>
                                <td>
                                    <span class="inp-date-time"><input type="text" name="expDt" id="expDt" class="inp-base" title="유효일자"></span>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">상품고시정보<i class="bul-require">필수항목</i> <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipNotice"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <div class="gnoticeNoDiv">
                                        <select class="slc-base w-490 mgr10" title="상품고시정보" id="gnoticeNo" name="gnoticeNo">
                                            <option value="0">상품군 선택</option>
                                            <c:forEach var="list" items="${gnoticeList}" varStatus="status">
                                                 <option value="${list.gnoticeNo}">${list.gnoticeNm}</option>
                                            </c:forEach>
                                        </select>
                                        <input type="checkbox" name="checkboxAllNoticeExp" id="checkboxAllNoticeExp" value="Y"><label for="checkboxAllNoticeExp" class="lb-check">전체 기본 문구 입력</label>
                                    </div>
                                    <div id="addNoticeArea">
                                    </div>
                                </td>
                            </tr>
                            <tr id="isbnArea" style="display: none;">
                                <th scope="row">ISBN<i class="bul-require">필수항목</i> </th>
                                <td>
                                    <input type="text"  name="isbn" id="isbn"  maxlength="13" class="inp-base" title="ISBN">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </dd>
            </dl>

            <dl class="acco">
                <dt class="acco-tit">
                    <h3 class="h3">부가 정보</h3>
                    <div class="float-r">
                        <button type="button" class="acco-btn"></button>
                    </div>
                </dt>
                <dd class="acco-pnl">
                    <div class="tbl-form">
                        <table>
                            <caption>부가정보</caption>
                            <colgroup>
                                <col style="width:180px">
                                <col style="width:auto">
                            </colgroup>
                            <tbody>

                            <tr>
                                <th scope="row">판매자 상품코드 <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipSellerItemCd"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <input type="text"  id="sellerItemCd" name="sellerItemCd" class="inp-base w-250" title="판매자 상품코드" maxlength="25">
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">성인상품유형<i class="bul-require">필수항목</i> <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipAdultType"><i class="hide">팁열기</i></button>
                                </th>
                                <td>
                                    <span class="radio-group size2">
                                          <c:forEach var="codeDto" items="${adultType}" varStatus="status">
                                              <c:set var="checked" value="" />
                                              <c:if test="${codeDto.ref1 eq 'df'}">
                                                  <c:set var="checked" value="checked" />
                                              </c:if>
                                              <input type="radio" id="adultType${codeDto.mcCd}" name="adultType" value="${codeDto.mcCd}" ${checked}><label for="adultType${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                                          </c:forEach>
                                    </span>

                                    <div id="adultTypeArea" style="display: none">
                                        <div class="mgt15">
                                            <select  class="slc-base" title="성인상품유형" id="imgDispYn" name="imgDispYn">
                                                <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                                                    <c:set var="selected" value="" />
                                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                                        <c:set var="selected" value="selected" />
                                                    </c:if>
                                                    <option value="${codeDto.mcCd}" ${selected}>이미지 ${codeDto.mcNm}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">가격비교 사이트 <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipEp"><i class="hide">팁열기</i></button></th>
                                <td>
                                    <span class="radio-group size2">
                                          <c:forEach var="codeDto" items="${regYn}" varStatus="status">
                                              <c:set var="checked" value="" />
                                              <c:if test="${codeDto.ref1 ne 'df'}">
                                                  <c:set var="checked" value="checked" />
                                              </c:if>
                                              <input type="radio" id="epYn${codeDto.mcCd}" name="epYn" value="${codeDto.mcCd}" ${checked}/><label for="epYn${codeDto.mcCd}"  class="lb-radio">${codeDto.mcNm}</label>
                                          </c:forEach>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">검색키워드</th>
                                <td>
                                    <input type="text" name="srchKeyword" id="srchKeyword"  maxlength="100" class="inp-base w-max" title="검색키워드" placeholder="최대 20개 키워드 입력가능  ( , 로 구분)">
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">검색태그 <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipSchTag"><i class="hide">팁열기</i></button></th>
                                <td>
                                    <div class="tag-wrap" id="schTagTitleArea"></div>
                                    <div class="tag-result">
                                        <span class="result-title">선택한 태그 </span>
                                        <span id="selectedTags"></span>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: none;">
                                <th scope="row">해외배송<i class="bul-require">필수항목</i> <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipGlobalAgcy"><i class="hide">팁열기</i></button></th>
                                <td>
                                    <span class="radio-group size2">
                                         <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                             <c:set var="checked" value="" />
                                             <c:if test="${codeDto.ref1 ne 'df'}">
                                                 <c:set var="checked" value="checked" />
                                             </c:if>
                                             <input type="radio" id="globalAgcyYn${codeDto.mcCd}" name="globalAgcyYn" value="${codeDto.mcCd}" ${checked}><label for="globalAgcyYn${codeDto.mcCd}"  class="lb-radio">${codeDto.mcNm}</label>
                                         </c:forEach>
                                    </span>
                                </td>
                            </tr>

                            <tr class="rentalArea" style="display: none">
                                <th scope="row">월렌탈료 </th>
                                <td>
                                    <input type="text" id="rentalFee" name="rentalFee" class="inp-base" title="월렌탈료" maxlength="9"> 원
                                    <p class="bul-p mgt5">상품유형이 렌탈 상품인 경우 월렌탈료, 의무사용기간, 등록비를 필수로 입력해주세요.</p>
                                </td>
                            </tr>
                            <tr class="rentalArea" style="display: none">
                                <th scope="row">의무사용기간</th>
                                <td>
                                    <input type="text" id="rentalPeriod" name="rentalPeriod" maxlength="9" class="inp-base" title="의무사용기간"> 개월
                                </td>
                            </tr>
                            <tr class="rentalArea" style="display: none">
                                <th scope="row">등록비</th>
                                <td>
                                    <input type="text" id="rentalRegFee" name="rentalRegFee" class="inp-base" title="등록비" maxlength="9">
                                </td>
                            </tr>
                            <tr class="rentalArea" style="display: none">
                                <th scope="row">총렌탈료</th>
                                <td>
                                    <span id="totalRentalFee"></span>원
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">안전인증<i class="bul-require">필수항목</i> <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltipCert"><i class="hide">팁열기</i></button></th>
                                <td>
                                    <div class="tbl-data02">
                                        <table id ="certTable">
                                            <caption>안전인증표</caption>
                                            <colgroup>
                                                <col style="width:120px">
                                                <col style="width:auto">
                                            </colgroup>
                                            <tbody>
                                            <c:forEach var="codeDto" items="${itemCertGroup}" varStatus="status">
                                                <tr id="certTr${codeDto.mcCd}">
                                                <th scope="row">${codeDto.mcNm}</th>
                                                <td>
                                                    <input type="hidden" name="certList[].itemCertSeq" class="itemCertSeq">
                                                    <input type="hidden" name="certList[].certGroup" class="certGroup" value="${codeDto.mcCd}">
                                                    <input type="hidden" name="certList[].isCert" class="isCert" value="Y">
                                                    <c:set var="dfProdIsCert" value="" />
                                                    <c:forEach var="isCertDto" items="${itemIsCert}" varStatus="status2">
                                                        <c:set var="checked" value="" />
                                                        <c:choose>
                                                            <c:when test="${codeDto.mcCd eq 'KD' and isCertDto.mcCd eq 'E'}">
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:if test="${isCertDto.ref1 eq 'df'}">
                                                                    <c:set var="dfProdIsCert" value="${isCertDto.mcCd}" />
                                                                    <c:set var="checked" value="checked" />
                                                                </c:if>
                                                                <span class="radio-group02 ">
                                                                    <input type="radio" class="isCertRadio" name="certList[${status.count}].isCert" id="cert_${codeDto.mcCd}_isCert_${isCertDto.mcCd}"
                                                                           data-cert-group="${codeDto.mcCd}" value="${isCertDto.mcCd}" ${checked}><label for="cert_${codeDto.mcCd}_isCert_${isCertDto.mcCd}" class="lb-radio mgb0">${isCertDto.mcNm}</label>
                                                                </span>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>

                                                    <div class="form mgt10 certRow">
                                                        <span class="addArea">
                                                            <select class="slc-base certType" id="cert_${codeDto.mcCd}_certType" name="certList[].certType" data-cert-group="${codeDto.mcCd}">
                                                            <option value="" >인증유형선택</option>
                                                                <c:forEach var="typeCodeDto" items="${itemCertType}" varStatus="status">
                                                                    <c:set var="typeCodePrefix" value="${fn:substring(typeCodeDto.mcCd, 0, 2)}"/>
                                                                    <c:if test="${typeCodePrefix eq codeDto.mcCd}">
                                                                        <option value="${typeCodeDto.mcCd}" data-ref1="${typeCodeDto.ref1}" data-ref2="${typeCodeDto.ref2}" data-ref3="${typeCodeDto.ref3}" >${typeCodeDto.mcNm}</option>
                                                                    </c:if>
                                                                </c:forEach>
                                                            </select>

                                                            <select class="slc-base energy" style="display: none">
                                                                <option value="" >선택해주세요</option>
                                                                    <c:forEach var="etCer" items="${itemCertEtCer}" varStatus="status">
                                                                        <option value="${etCer.mcCd}" >${etCer.mcNm}</option>
                                                                    </c:forEach>
                                                            </select>

                                                            <input type="text" name="certList[].certNo" id="cert_${codeDto.mcCd}_certNo" data-cert-group="${codeDto.mcCd}" class="inp-base w-325 certNo" maxlength="50" placeholder="'-'를 포함한 인증번호를 입력해주세요." autocomplete="off"/>
                                                            <span class="certNoSubArea"></span>
                                                            <button type="button" class="btn-base certCheck" data-cert-group="${codeDto.mcCd}" id="cert_${codeDto.mcCd}_certCheck" onclick="itemSetMain.ext.checkCert($(this))">조회</button>
                                                        </span>

                                                        <c:if test="${codeDto.mcCd eq 'ER' or codeDto.mcCd eq 'ET'}">
                                                            <button type="button" class="btn-base type2 certAdd">추가</button>
                                                        </c:if>
                                                        <input type="hidden" class="certYn" name="certList[].certYn" id="cert_${codeDto.mcCd}_certYn" value="N">
                                                        <span class="fc05 fs12 mgl10 certY" style="display: none;">인증</span>
                                                        <span class="fc02 fs12 mgl10 certN" style="display: none;">인증실패</span>
                                                        <span class="txt fc04 mgl10 certGroupDesc">${codeDto.ref1}</span>
                                                    </div>
                                                </td>
                                             </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">증빙 서류</th>
                                <td>
                                    <div class="mgb15">
                                        <div class="file-wrap">
                                            <select id="proofFile" class="slc-base" title="증빙서류구분 선택">
                                                <option value="">증빙서류구분</option>
                                                <c:forEach var="codeDto" items="${proofFile}" varStatus="status">
                                                    <option value="${codeDto.mcCd}"  data-file-type="${codeDto.mcCd}"
                                                            data-field-name="proof${status.count}">${codeDto.mcNm}</option>
                                                </c:forEach>
                                            </select>

                                            <button type="button" class="btn-base proofUpload" onclick="itemCommon.file.clickFile('PROOF');" >파일찾기
                                            </button>

                                        </div>
                                        <p class="bul-p mgt5">용량: 2MB 이하 / 파일 : PNG, JPG, JPEG, XLS, XLSX, PDF</p>

                                        <div class="tbl-data">
                                            <table id="proofFileTable" >
                                                <caption>주문기본정보</caption>
                                                <colgroup>
                                                    <col style="width:8%">
                                                    <col style="width:20%">
                                                    <col style="width:auto">
                                                </colgroup>
                                                <thead>
                                                <tr>
                                                    <th scope="col">No.</th>
                                                    <th scope="col">구분</th>
                                                    <th scope="col">서류 파일  PNG, JPG, JPEG, XLS, XLSX, PDF</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:forEach var="codeDto" items="${proofFile}" varStatus="status">
                                                <tr>
                                                    <td>${status.count}</td>
                                                    <td>${codeDto.mcNm}</td>
                                                    <td>
                                                        <div class="add-file">
                                                            <span class="add-file-item" id="uploadProofFileSpan_${codeDto.mcCd}" data-file-type="${codeDto.mcCd}">
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </dd>
            </dl>
        </div>
        <div class="btn-wrap regBtnGroup mgt40">
            <button type="button" class="btn-base-l type2" id="setItemTempBtn"><i>임시저장</i></button>
            <button type="button" class="btn-base-l type4" id="setItemBtn"><i>저장</i></button>
            <button type="button" id="resetBtn" class="btn-base-l type3" id="resetBtn"><i>취소</i></button>
            <!--<button type="button" class="btn-base-l"><i>미리보기</i></button> --> 
        </div>

        <div class="cmp-regist mgt40 regSuccessArea" style="display: none;">
            <p>상품번호 : <b><span id="returnItemNo"></span> </b></p>
            <p class="mgt15 fc03">상품등록이 완료 되었습니다.</p>
        </div>
        <div class="btn-wrap mgt40 regSuccessArea" style="display: none;">
            <button type="button" class="btn-base-l type2" id="itemSetNewBtn"><i>다른 상품등록</i></button>
            <button type="button" class="btn-base-l" id="itemSearchNewBtn"><i>상품조회/수정</i></button>
        </div>
    </form>

    <div class="modal-wrap">
        <jsp:include page="/WEB-INF/views/pages/partner/pop/shipPolicyMngPop.jsp" />
        <jsp:include page="/WEB-INF/views/pages/item/pop/categoryCommissionPop.jsp" />
    </div>

</section>

<div class="tooltip-wrap">
    <jsp:include page="/WEB-INF/views/pages/item/itemTooltip.jsp" />
</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="mainImg0" data-file-id="MN0"  style="display: none;"/>
<input type="file" name="mainImg1" data-file-id="MN1"  style="display: none;"/>
<input type="file" name="mainImg2" data-file-id="MN2"  style="display: none;"/>
<input type="file" name="listImg" data-file-id="imgLST"  style="display: none;"/>

<input type="file" name="proof1" data-file-id="uploadProofFileSpan_EXM"  style="display: none;"/>
<input type="file" name="proof2" data-file-id="uploadProofFileSpan_MAK"  style="display: none;"/>
<input type="file" name="proof3" data-file-id="uploadProofFileSpan_AWD"  style="display: none;"/>
<input type="file" name="proof4" data-file-id="uploadProofFileSpan_ORG"  style="display: none;"/>
<!-- 이미지정보 업로드 폼 -->

<script>
    const hmpImgUrl      = '${hmpImgUrl}';
    const saleType       = '${saleType}';
</script>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemSetMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemCommon.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>
