<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="resultPop" class="modal" role="dialog">
    <h2 class="h2">일괄변경 결과</h2>
    <div class="modal-cont">
        <h3 class="h4"><span id="applyItemCnt">0</span> 개의 상품정보가 변경되었습니다. </h3>

        <div class="tbl-fixed mgt20">
            <div class="tbl-data align-c" style="max-height: 420px">
                <table>
                    <colgroup>
                        <col style="width:36%">
                        <col style="width:auto">
                        <col style="width:25%">
                    </colgroup>
                    <thead>
                    <tr>
                        <th scope="col">상품번호</th>
                        <th scope="col">상품명</th>
                        <th scope="col">결과</th>
                    </tr>
                    </thead>
                    <tbody id="resultRowArea">
                    </tbody>
                </table>
            </div>
        </div>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4" onclick="resultPop.closeModal()"><i>확인</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="resultPop.closeModal();"><i class="hide" >닫기</i></button>
</div>

<script>

    $(document).ready(function() {
        resultPop.init();
    });
</script>

<script type="text/javascript" src="/static/js/item/pop/resultPop.js?v=${fileVersion}"></script>