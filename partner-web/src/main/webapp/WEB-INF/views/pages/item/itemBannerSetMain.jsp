<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<section class="contents">
<div class="float">
    <h2 class="h2 float-l"><c:if test="${isMod eq 'Y'}">상품공지수정</c:if>
        <c:if test="${isMod eq 'N'}">상품공지등록</c:if>
        <span class="fc02"><i class="bul-require">필수항목</i>표시는 필수입력 항목입니다.</span></h2>
</div>

<div class="tbl-form mgt20">
    <form id="itemBannerSetForm">
        <input type="hidden" id="bannerNo" name="bannerNo" value="${bannerNo}">
        <input type="hidden" id="isMod" name="isMod" value="${isMod}">
        <table>
            <caption>기본정보</caption>
            <colgroup>
                <col style="width:180px">
                <col style="width:auto">
            </colgroup>
            <tbody>
            <tr>
                <th scope="row">제목<i class="bul-require">필수항목</i></th>
                <td>
                    <input type="text" id="bannerNm" name="bannerNm" class="inp-base w-490" title="제목">
                </td>
            </tr>
            <tr>
                <th scope="row">구분<i class="bul-require">필수항목</i></th>
                <td>
                    <span class="radio-group size2">
                      <c:forEach var="codeDto" items="${dispType}" varStatus="status">
                          <c:set var="checked" value="" />
                          <c:if test="${codeDto.ref1 eq 'df'}">
                              <c:set var="checked" value="checked" />
                          </c:if>
                          <input type="radio" id="dispType${codeDto.mcCd}" name="dispType" value="${codeDto.mcCd}" ${checked}><label for="dispType${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                      </c:forEach>
                    </span>

                    <!-- 카테고리별 -->
                    <div id="cateRow" class="mgt10" style="display: none">
                        <select  class="slc-base" title="카테고리 대분류" id="selectCateCd1" name="selectCateCd1"
                                 style="width:150px;float:left;" data-default="대분류"
                                 onchange="javascript:commonCategory.changeCategorySelectBox(1,'selectCateCd');">
                        </select>
                        <select  class="slc-base" title="카테고리 중분류" id="selectCateCd2" name="selectCateCd2"
                                 style="width:150px;float:left;" data-default="중분류"
                                 onchange="javascript:commonCategory.changeCategorySelectBox(2,'selectCateCd');">
                        </select>
                        <select  class="slc-base" title="카테고리 소분류" id="selectCateCd3" name="selectCateCd3"
                                 style="width:150px;float:left;" data-default="소분류"
                                 onchange="javascript:commonCategory.changeCategorySelectBox(3,'selectCateCd');">
                        </select>
                        <select  class="slc-base" title="카테고리 세분류" id="selectCateCd4" name="selectCateCd4"
                                 style="width:150px;float:left;" data-default="세분류">
                        </select>
                    </div>

                    <!-- 상품별 -->
                    <div id="itemRow" style="display: none;">
                        <div class="btn-wrap align-l mgt10">
                            <button type="button" id="schItemPopBtn" class="btn-base" onclick="itemPop.openModal();"><i>+ 상품추가</i></button>
                        </div>
                        <div class="data-wrap" style="width: 100%; height: 500px"; id="bannerDetailListGrid">
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th scope="row">공지내용<i class="bul-require">필수항목</i></th>
                <td>
                    <div class="words-wrap" style="width: 100%">
                        <textarea id="bannerDesc" name="bannerDesc" class="textarea" style="width: 100%;" title="공지내용" maxlength="1000" ></textarea>
                        <span class="words"> (<i><span id="textCountKr_keyword">0</span></i>/1,000자)</span>
                    </div>
                </td>
            </tr>
            <tr>
                <th scope="row">노출여부<i class="bul-require">필수항목</i></th>
                <td>
                    <span class="radio-group size2">
                          <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                              <c:set var="checked" value="" />
                              <c:if test="${codeDto.ref1 eq 'df'}">
                                  <c:set var="checked" value="checked" />
                              </c:if>
                              <input type="radio" id="dispYn${codeDto.mcCd}" name="dispYn" value="${codeDto.mcCd}" ${checked}><label for="dispYn${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                          </c:forEach>
                    </span>

                </td>
            </tr>
            <tr>
                <th scope="row">노출기간<i class="bul-require">필수항목</i></th>
               <td>
                   <span class="inp-date-time w-200"><input type="text" id="dispStartDt" name="dispStartDt" class="inp-base" title="노출기간 시작일"></span> -
                   <span class="inp-date-time w-200"><input type="text" id="dispEndDt" name="dispEndDt" class="inp-base" title="노출기간 종료일"></span>
               </td>
            </tr>
            </tbody>
        </table>
        </form>
    </div>

    <div class="btn-wrap mgt40">
        <button type="button" id="setBannerBtn" class="btn-base-l type4"><i>저장</i></button>
        <button type="button" id="resetFormBtn" class="btn-base-l type3"><i>취소</i></button>
    </div>

    <div class="modal-wrap">
        <jsp:include page="/WEB-INF/views/pages/item/pop/itemPop.jsp" />
    </div>

</section>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/common/itemGridCommon.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemBannerSetMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>

<script>
    var parentMain = null;

    // 상품공지 디테일 리얼 그리드
    ${bannerDetailListGridBaseInfo}

</script>


