<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="itemSalePricePop" class="modal" role="dialog">
    <h2 class="h2">판매가격 일괄변경</h2>
    <form id="itemSalePricePopForm">
        <div class="modal-cont">
            <p>
               <span id="itemSalePriceCnt">0</span> 개 상품에 대한 판매가격 일괄변경
            </p>
            <div class="mgt20">
                <input type="text" class="inp-base w-160" title="판매가격" id="salePrice" name="salePrice" maxlength="11" > 원
            </div>
            <p class="bul-p mgt10">예약판매 상품의 경우 판매기간이 변경되지 않을 수 있습니다.</p>

            <div class="btn-wrap mgt30">
                <button type="button" class="btn-base-l" onclick="itemSalePricePop.closeModal();"><i>취소</i></button>
                <button type="button" class="btn-base-l type4" id="setItemSalePriceBtn"><i>저장</i></button>
            </div>
        </div>
    </form>
    <button type="button" class="btn-close2 ui-modal-close" onclick="itemSalePricePop.closeModal();"> <i class="hide" >닫기</i></button>
</div>
<script>

    $(document).ready(function() {
        itemSalePricePop.init();
    });
</script>

<script type="text/javascript" src="/static/js/item/pop/itemSalePricePop.js?v=${fileVersion}"></script>