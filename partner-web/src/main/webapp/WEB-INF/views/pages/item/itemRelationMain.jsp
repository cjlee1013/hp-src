<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<section class="contents">
    <div class="float">
        <h2 class="h2 float-l">연관상품관리</h2>
        <div class="float-r">
            <button type="button" class="btn-base-s type2" onclick="itemRelation.setItemRelationOpenTab();"><i>신규등록</i></button>
        </div>
    </div>

    <div class="srh-form">
        <form id="itemRelationSearchForm" onsubmit="return false;">
            <table>
                <caption>조회</caption>
                <colgroup>
                    <col style="width:75px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">연관명</th>
                    <td>
                        <input type="text" id="schRelationNm" name="schRelationNm" class="inp-base w-max" title="연관명">
                    </td>
                </tr>
                <tr>
                    <th scope="row">연관번호</th>
                    <td>
                        <textarea class="textarea mgt10" id="schRelationNo" name="schRelationNo" title="상품번호" placeholder="복수검색 시 Enter 또는 ,로 구분"></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row">노출여부</th>
                    <td>
                        <div class="chk-group2">

                            <input type="checkbox" id="dispYnTotal" checked ><label for="dispYnTotal" class="lb-check">전체</label>
                            <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                                <input type="checkbox" class="dispYn" id="schDispYn${codeDto.mcCd}" name="schDispYn" value="${codeDto.mcCd}"><label for="schDispYn${codeDto.mcCd}" class="lb-check">${codeDto.mcNm}</label>
                            </c:forEach>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">카테고리</th>
                    <td>
                        <select class="slc-base w-180" title="카테고리 대분류" id="searchCateCd1" name="schLcateCd"
                                data-default="대분류" onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                        </select>
                        <select class="slc-base w-180" title="카테고리 중분류" id="searchCateCd2" name="schMcateCd"
                                data-default="중분류" onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                        </select>
                        <select class="slc-base w-180" title="카테고리 소분류" id="searchCateCd3" name="schScateCd"
                                data-default="소분류" onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="schStartDate">등록일</label></th>
                    <td>
                        <span class="inp-date"><input type="text" id="schStartDate" name="schStartDate" class="inp-base" title="검색기간 시작일"></span> -
                        <span class="inp-date"><input type="text" id="schEndDate" name="schEndDate" class="inp-base" title="검색기간 종료일"></span>
                        <span class="radio-group mgl5">
                            <input type="radio" name="setSchDate" id="setToday" value="0"><label for="setToday" class="lb-radio">오늘</label>
                            <input type="radio" name="setSchDate" id="setYesterday" value="-1d"><label for="setYesterday" class="lb-radio">어제</label>
                            <input type="radio" name="setSchDate" id="setOneMonth" value="-30d" checked><label for="setOneMonth" class="lb-radio">1개월</label>
                            <input type="radio" name="setSchDate" id="setThreeMonth" value="-90d"><label for="setThreeMonth" class="lb-radio">3개월</label>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="btn-wrap">
                <button type="button" class="btn-base-l type4" id="searchBtn">검색</button>
                <button type="button" class="btn-base-l type3" id="searchResetBtn">초기화</button>
            </div>
        </form>
    </div>

    <div class="result-wrap float mgt20">
        <p class="float-l result-p">검색 결과 <b><span id="itemRelationTotalCount">0</span></b>건</p>
        <div class="float-r">
            <button type="button" class="btn-base-s" id="delItemRelationBtn"><i>삭제</i></button>
            <form id="delItemRelationForm"  onsubmit="return false;" />
        </div>
    </div>

    <div class="data-wrap" style="height: 500px" id="itemRelationListGrid">
    </div>

</section>

<script>
    //연관 상품관리 그리드
    ${itemRelationListGridBaseInfo}
</script>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemRelationMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>
