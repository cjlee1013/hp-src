<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<section class="contents">
    <form id="itemRelationSetForm" name="itemRelationSetForm" onsubmit="return false;">
        <div class="float">
            <h2 class="h2 float-l">연관상품등록 <span class="fc02"><i class="bul-require">필수항목</i>표시는 필수입력 항목입니다.</span></h2>
        </div>

        <div id="uiAcco1" class="form-acco acco-wrap">
            <dl class="acco">
                <dt class="acco-tit">
                    <h3 class="h3">기본 정보</h3>
                </dt>
                <dd class="acco-pnl">
                    <div class="tbl-form">
                        <table>
                            <caption>기본정보</caption>
                            <colgroup>
                                <col style="width:180px">
                                <col style="width:auto">
                            </colgroup>
                            <tbody>
                            <input type="hidden" id="relationNo" name="relationNo" value="${relationNo}">
                            <input type="hidden" id="isMod" name="isMod" value="${isMod}">
                            <input type="hidden" id="scateCd" name="scateCd" value="">
                            <tr>
                                <th scope="row">
                                    연관상품명<i class="bul-require">필수항목</i>
                                </th>
                                <td>
                                    <div class="words-wrap">
                                        <input type="text"  id="relationNm" name="relationNm" class="inp-base" title="연관상품명">
                                        <span class="words"><i><span id="relationNmCount">0</span></i>/50자</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">노출여부<i class="bul-require">필수항목</i></th>
                                <td>
                                    <span class="radio-group size2">
                                         <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                                             <c:set var="checked" value="" />
                                             <c:if test="${codeDto.ref1 eq 'df'}">
                                                 <c:set var="checked" value="checked" />
                                             </c:if>
                                             <input type="radio" id="dispYn${codeDto.mcCd}" name="dispYn" value="${codeDto.mcCd}" ${checked}><label for="dispYn${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                                         </c:forEach>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" rowspan="2">연관 노출타입<i class="bul-require">필수항목</i></th>
                                <td>
                                    <div>
                                        <span class="radio-tit w60">모바일</span>
                                        <span class="radio-group size2">
                                                <input type="radio" id="mRelationTypeL"name="mRelationType" value="LIST" checked><label for="mRelationTypeL" class="lb-radio">리스트형</label>
                                                <input type="radio" id="mRelationTypeC"name="mRelationType" value="CARD"><label for="mRelationTypeC" class="lb-radio">카드형</label>
                                            </span>
                                        <span class="mgl10 mListArea">
                                                <c:forEach var="codeDto" items="${poDispMobileType}" varStatus="status">
                                                    <c:set var="checked" value="" />
                                                    <c:if test="${codeDto.ref2 eq 'LIST'}">
                                                        <c:if test="${codeDto.ref1 eq 'df'}">
                                                            <c:set var="checked" value="checked" />
                                                        </c:if>
                                                        <input type="radio" id="dispMobileType${codeDto.mcCd}"name="dispMobileType" value="${codeDto.mcCd}" >
                                                        <label for="dispMobileType${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                                                    </c:if>
                                                </c:forEach>
                                        </span>
                                        <span class="mgl10 mCardArea" style="display: none;">
                                                 <c:forEach var="codeDto" items="${poDispMobileType}" varStatus="status">
                                                     <c:set var="checked" value="" />
                                                     <c:if test="${codeDto.ref2 eq 'CARD'}">
                                                         <c:if test="${codeDto.ref1 eq 'df'}">
                                                             <c:set var="checked" value="checked" />
                                                         </c:if>
                                                         <input type="radio" id="dispMobileType${codeDto.mcCd}"name="dispMobileType" value="${codeDto.mcCd}" >
                                                         <label for="dispMobileType${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                                                     </c:if>
                                                 </c:forEach>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="mgt15">
                                        <span class="radio-tit w60">PC</span>
                                        <span class="radio-group size2">
                                            <input type="radio" id="pcRelationTypeL" name="pcRelationType" value="LIST" checked><label for="pcRelationTypeL" class="lb-radio">리스트형</label>
                                            <input type="radio" id="pcRelationTypeC" name="pcRelationType" value="CARD"><label for="pcRelationTypeC" class="lb-radio">카드형</label>
                                        </span>
                                        <span class="mgl10 pcListArea">
                                              <c:forEach var="codeDto" items="${poDispPcType}" varStatus="status">
                                                  <c:set var="checked" value="" />
                                                  <c:if test="${codeDto.ref2 eq 'LIST'}">
                                                      <c:if test="${codeDto.ref1 eq 'df'}">
                                                          <c:set var="checked" value="checked" />
                                                      </c:if>
                                                      <input type="radio" id="dispPcType${codeDto.mcCd}" name="dispPcType" value="${codeDto.mcCd}" >
                                                      <label for="dispPcType${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                                                  </c:if>
                                              </c:forEach>
                                        </span>
                                        <span class="mgl10 pcCardArea" style="display: none;">
                                              <c:forEach var="codeDto" items="${poDispPcType}" varStatus="status">
                                                  <c:set var="checked" value="" />
                                                  <c:if test="${codeDto.ref2 eq 'CARD'}">
                                                      <c:if test="${codeDto.ref1 eq 'df'}">
                                                          <c:set var="checked" value="checked" />
                                                      </c:if>
                                                      <input type="radio" id="dispPcType${codeDto.mcCd}" name="dispPcType" value="${codeDto.mcCd}" >
                                                      <label for="dispPcType${codeDto.mcCd}" class="lb-radio">${codeDto.mcNm}</label>
                                                  </c:if>
                                              </c:forEach>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">인트로 이미지</th>
                                <td>
                                    <div class="upload-wrap preview">
                                        <div class="imgDisplayView" style="display:none;">
                                            <button type="button" id="introImgDelBtn" class="btn-clear deleteImgBtn" onclick="itemCommon.img.deleteImg($(imgIntro));"><i class="hide">삭제</i></button>
                                            <div id="imgIntro" class="thumb uploadType" data-type="INTRO" data-processkey="ItemRelation" style="cursor:hand;">
                                                <img class="imgUrlTag" width="132px;" height="132px;" src="">
                                                <input type="hidden" class="imgUrl" name="imgUrl" value="" id="introImgUrl"/>
                                                <input type="hidden" class="imgNm" name="imgNm" value=""/>
                                            </div>
                                        </div>
                                        <div class="imgDisplayReg" style="padding-top: 50px;">
                                            <button type="button" id="introImgRegBtn" class="btn-base-s" onclick="itemCommon.img.clickFile($(this));" data-field-name="introImg"><i>등록</i></button>
                                        </div>
                                    </div>
                                    <p class="bul-p mgt15">사이즈: 840 X 세로제한없음 / 용량: 3MB 이하 / 파일 : JPG, JPEG, PNG</p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </dd>
            </dl>
            <dl class="acco active">
                <dd class="acco-pnl">
                    <div class="result-wrap mgt20">
                        <h3 class="h3">연관상품 상품목록 (<span class="fs18" id="itemRelationDetailTotalCount">0</span>/50개)</h3>
                        <div class="float">
                            <div class="float-l fc03">
                                <p>- 동일 성인상품유형, 장바구니제한안함, 텍스트옵션 미사용, 동일 소분류카테고리의 상품들을 하나의 연관으로 설정가능합니다.</p>
                                <p>- 상품목록 리스트의 순서대로 프론트에 전시됩니다.</p>
                            </div>
                            <div class="float-r mgt10">
                                <button type="button" class="btn-base-s type2" onclick="itemPop.openModal();"><i>상품추가</i></button>
                            </div>
                        </div>
                    </div>

                    <div id="itemRelationDetailListGrid" class="data-wrap" style="height: 500px">
                    </div>

                    <div class="float-r align-l mgt10">
                        <button type="button" key="firstTop" class="btn-data-top itemMoveCtrl"><i class="hide">데이터 최상단으로</i></button>
                        <button type="button" key="top" class="btn-data-up itemMoveCtrl"><i class="hide">데이터 위로 </i></button>
                        <button type="button" key="bottom" class="btn-data-down itemMoveCtrl"><i class="hide">데이터 아래로 </i></button>
                        <button type="button" key="lastBottom" class="btn-data-bottom itemMoveCtrl"><i class="hide">데이터 최하단으로 </i></button>
                        <button type="button" class="btn-base-s ;" id="deleteItem"><i>선택삭제</i></button>
                    </div>

                </dd>
            </dl>
        </div>
        <div class="btn-wrap mgt40">
            <button type="button" class="btn-base-l type4" id="setItemRelationBtn"><i>저장</i></button>
            <button type="button" class="btn-base-l type3" id="resetBtn"><i>취소</i></button>
        </div>
    </form>

    <div class="modal-wrap">
        <jsp:include page="/WEB-INF/views/pages/item/pop/itemPop.jsp" />
    </div>

</section>
<input type="file" name="introImg" data-file-id="imgIntro"  style="display: none;"/>

<!-- script -->
<script>
    var parentMain = null;

    const hmpImgUrl      = '${hmpImgUrl}';

    // 연관 상품관리 그리드
    ${itemRelationDetailListGridBaseInfo}

</script>

<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemRelationSetMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemCommon.js?${fileVersion}"></script>
