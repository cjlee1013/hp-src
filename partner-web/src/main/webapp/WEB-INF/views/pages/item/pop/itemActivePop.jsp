<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="itemActivePop" class="modal" role="dialog">
    <h2 class="h2">판매상태 일괄변경</h2>

    <form id="itemActivePopForm">
        <div class="modal-cont">
            <p>
                <span id="itemStatusActiveCnt">0</span> 개 상품에 대해 일시중지해지 처리하시겠습니까? <br>
                각 상품의 상태에 따라 판매대기, 판매중, 판매종료 상태로 변경됩니다.
            </p>

            <div class="btn-wrap mgt30">
                <button type="button" class="btn-base-l" onclick="itemActivePop.closeModal();"><i>취소</i></button>
                <button type="button" class="btn-base-l type4" id="setItemActiveBtn"><i>저장</i></button>
            </div>
        </div>
    </form>
    <button type="button" class="btn-close2 ui-modal-close" onclick="itemActivePop.closeModal();"><i class="hide" >닫기</i></button>
</div>

<script>

    $(document).ready(function() {
        itemActivePop.init();
    });
</script>

<script type="text/javascript" src="/static/js/item/pop/itemActivePop.js?v=${fileVersion}"></script>