<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="itemShipPolicyPop" class="modal type-xl" role="dialog">
    <h2 class="h2">배송정책변경 일괄변경</h2>
    <form id="itemShipPolicyPopForm" onsubmit="return false;">
        <div class="modal-cont">
            <h3 class="h4"><span id="itemShipPolicyCnt">0</span>개 상품에 대한 배송정책변경</h3>

            <div class="tbl-form mgt10">
                <table>
                    <caption>배송상세정보표</caption>
                    <colgroup>
                        <col style="width:150px">
                        <col style="width:auto">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row">
                            배송정책번호
                            <i class="bul-require">필수항목</i> <button type="button" class="btn-tip ui-tooltip-btn" aria-describedby="tooltip30"><i class="hide">팁열기</i></button>
                        </th>
                        <td>
                            <select class="slc-base w-325" title="배송정책" name="shipPolicyNo" id="shipPolicyNo" >
                                <option value="">선택하세요</option>
                            </select>
                            <div class="tbl-data02">
                                <table>
                                    <caption>배송정책표</caption>
                                    <colgroup>
                                        <col style="width:18%">
                                        <col style="width:auto" >
                                        <col style="width:17%">
                                        <col style="width:33%">
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th scope="row">배송정책번호</th>
                                        <td colspan="3"><span class="text deliveryDetail" id="detailShipPolicyNo"></span></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">배송정책명</th>
                                        <td><span class="text deliveryDetail" id="detailShipPolicyNm"></span></td>
                                        <th scope="row">배송주체</th>
                                        <td> <span class="text deliveryDetail" id="detailShipMngNm"></span></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">배송유형</th>
                                        <td><span class="text deliveryDetail" id="detailShipTypeNm"></span></td>
                                        <th scope="row">배송방법</th>
                                        <td><span class="text deliveryDetail" id="detailShipMethodNm"></span></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">배송비종류</th>
                                        <td><span class="text deliveryDetail" id="detailShipKindNm"></span></td>
                                        <th scope="row">배송비</th>
                                        <td><span class="text deliveryDetail" id="detailShipFee"></span></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">배송비 결제방식</th>
                                        <td><span class="text deliveryDetail" id="detailPrepaymentYn"></span></td>
                                        <th scope="row">배송비 노출여부</th>
                                        <td><span class="text deliveryDetail" id="detailShipFeeDispYn"></span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">반품/교환 배송비<i class="bul-require">필수항목</i></th>
                        <td> <input type="text"  name="claimShipFee" id="claimShipFee" maxlength="7" class="inp-base" title="반품교환 배송비"> 원 (편도)</td>
                    </tr>
                    <tr>
                        <th scope="row">출고지<i class="bul-require">필수항목</i></th>
                        <td>

                            <input type="text" class="inp-base w-110" title="우편번호" name="releaseZipcode" id="releaseZipcode" readonly> <button type="button" onclick="zipCodePopup('item.callback.releaseZipcode');"  class="btn-base"><i>우편번호</i></button>
                            <div class="mgt10">
                                <input type="text" class="inp-base w-half " name="releaseAddr1" id="releaseAddr1" title="주소" readonly>
                                <input type="text" class="inp-base w-half " name="releaseAddr2" id="releaseAddr2" title="상세주소">
                            </div>
                            <div class="mgt15">
                                <input type="checkbox" id="copyPartnerAddr"><label for="copyPartnerAddr" class="lb-check">사업자주소 동일 적용</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">회수지<i class="bul-require">필수항목</i></th>
                        <td>
                            <input type="text" class="inp-base w-110" name="returnZipcode" id="returnZipcode"  title="우편번호"readonly> <button type="button" class="btn-base" onclick="zipCodePopup('item.callback.returnZipcode');"><i>우편번호</i></button>
                            <div class="mgt10">
                                <input type="text" name="returnAddr1" id="returnAddr1" class="inp-base w-half " title="주소" readonly>
                                <input type="text" name="returnAddr2" id="returnAddr2" class="inp-base w-half " title="상세주소">
                            </div>
                            <div class="mgt15">
                                <input type="checkbox" id="copyReleaseAddr"><label for="copyReleaseAddr"  class="lb-check">사업자주소 동일 적용</label>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="btn-wrap mgt30">
                <button type="button" class="btn-base-l" onclick="itemShipPolicyPop.closeModal();"><i>취소</i></button>
                <button type="button" class="btn-base-l type4" id="setItemShipPolicyBtn"><i>저장</i></button>
            </div>
        </div>
    </form>
    <button type="button" class="btn-close2 ui-modal-close" onclick="itemShipPolicyPop.closeModal();"><i class="hide" >닫기</i></button>
</div>

<script>
    var item = {};
    $(document).ready(function() {
        itemShipPolicyPop.init();
    });
</script>

<script type="text/javascript" src="/static/js/item/itemCommon.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/pop/itemShipPolicyPop.js?v=${fileVersion}"></script>
