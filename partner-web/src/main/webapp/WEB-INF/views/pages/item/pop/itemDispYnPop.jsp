<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div id="itemDispYnPop" class="modal" role="dialog">
    <h2 class="h2">노출여부 일괄변경</h2>

    <div class="modal-cont">
        <form id="itemDispYnPopForm">
            <p>
                <span id="itemDispYnCnt">0</span>개 상품에 대한 노출여부 일괄변경
            </p>
            <div class="radio-group size2 mgt20">
                <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.ref1 eq 'df'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <input type="radio" id="dispYn${codeDto.mcCd}" name="dispYn" value="${codeDto.mcCd}" ${checked}><label for="dispYn${codeDto.mcCd}"  class="lb-radio">${codeDto.mcNm}</label>
                </c:forEach>
            </div>

            <div class="btn-wrap mgt30">
                <button type="button" class="btn-base-l" onclick="itemDispYnPop.closeModal();"><i>취소</i></button>
                <button type="button" class="btn-base-l type4" id="setDispYnBtn"><i>저장</i></button>
            </div>
        </form>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="itemDispYnPop.closeModal()"><i class="hide" >닫기</i></button>
</div>

<script>
    $(document).ready(function() {
        itemDispYnPop.init();
    });
</script>

<script type="text/javascript" src="/static/js/item/pop/itemDispYnPop.js?v=${fileVersion}"></script>