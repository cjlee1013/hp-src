<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="itemPop" class="modal type-l" role="dialog">
    <h2 class="h2">상품조회</h2>
    <div class="modal-cont">
        <div class="tbl-form mgt10">
            <form id="itemPopForm" onsubmit="return false;">
                <table>
                    <caption>조회</caption>
                    <colgroup>
                        <col style="width:100px">
                        <col style="width:auto">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row">상품명</th>
                        <td>
                            <input type="text" id="schItemNm" name="schItemNm" class="inp-base w-230" placeholder="" title="상품명">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">상품번호</th>
                        <td>
                            <input type="text" id="schItemNo" name="schItemNo" class="inp-base w-230" placeholder="" title="상품번호">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">카테고리</th>
                        <td>
                            <select  id="schCateCd1" name="schLcateCd" class="slc-base w-120" title="카테고리 대분류" data-default="대분류" onchange="javascript:commonCategory.changeCategorySelectBox(1,'schCateCd');"></select>
                            <select  id="schCateCd2" name="schMcateCd" class="slc-base w-120" title="카테고리 중분류" data-default="중분류" onchange="javascript:commonCategory.changeCategorySelectBox(2,'schCateCd');"></select>
                            <select  id="schCateCd3" name="schScateCd" class="slc-base w-120" title="카테고리 소분류" data-default="소분류" onchange="javascript:commonCategory.changeCategorySelectBox(3,'schCateCd');"></select>
                            <select  id="schCateCd4" name="schDcateCd" class="slc-base w-120" title="카테고리 세분류"  data-default="세분류"></select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="btn-wrap mgt20">
            <button type="button" class="btn-base-l type4" id="schBtn"><i>검색</i></button>
            <button type="button" class="btn-base-l type3" id="schResetBtn"><i>초기화</i></button>
        </div>

        <div class="result-wrap float mgt20">
            <p class="float-l result-p">검색 결과 <b><span id="itemPopSearchCnt">0</span></b>건</p>
        </div>
        <div class="data-wrap" style="height: 160px" id="itemPopGrid" >
        </div>

        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l type4" id="selectBtn"><i>선택</i></button>
            <button type="button" class="btn-base-l " id="closeBtn" onclick="itemPop.closeModal();"><i>취소</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="itemPop.closeModal();"><i class="hide" >닫기</i></button>
</div>

<script>
    // 상품 조회 그리드 정보
    ${itemPopGridBaseInfo}

    $(document).ready(function() {
        itemPop.init();
        itemPopGrid.init();
    });

    var isMulti = "${isMulti}";

</script>

<script type="text/javascript" src="/static/js/item/pop/itemPop.js?v=${fileVersion}"></script>