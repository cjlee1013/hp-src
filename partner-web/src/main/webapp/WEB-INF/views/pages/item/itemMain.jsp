<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<!-- script -->
<section class="contents" id="itemMain">
    <div class="float">
        <h2 class="h2 float-l">상품조회/수정 <span>최근 3개월 등록된 상품 기준</span>
        </h2>
        <div class="float-r">
            <button type="button" class="btn-ico-re" id="refreshItemCnt"><i>새로고침</i></button>
        </div>
    </div>

    <div class="box-manage line2" id="itemCntBoard">
        <div>
            <dl>
                <dt>전체</dt>
                <dd><a href="javascript:itemMain.searchByItemStatus()"><span id="total">0</span></a>건</dd>
            </dl>
        </div>
        <div>
            <dl>
                <dt>임시저장</dt>
                <dd><a href="javascript:itemMain.searchByItemStatus('T')"><span id="countT">0</span></a>건</dd>
            </dl>
            <dl>
                <dt>판매중</dt>
                <dd><a href="javascript:itemMain.searchByItemStatus('A')"><span id="countA">0</span></a>건</dd>
            </dl>
        </div>
        <div>
            <dl>
                <dt>품절</dt>
                <dd><a href="javascript:itemMain.searchByItemStatus('O')"><span id="countO">0</span></a>건</dd>
            </dl>
            <dl>
                <dt>판매금지</dt>
                <dd><a href="javascript:itemMain.searchByItemStatus('S')"><span id="countS">0</span></a>건</dd>
            </dl>
        </div>
    </div>
    <div class="srh-form">
        <form id="itemSearchForm">
            <input type="hidden" name="schStockQty" id="schStockQty" value="" />
            <table>
                <caption>조회</caption>
                <colgroup>
                    <col style="width:75px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">상품명</th>
                    <td>
                        <input type="text" id="schItemNm" name="schItemNm" class="inp-base w-490" title="상품명">
                    </td>
                </tr>
                <tr>
                    <th scope="row">상품번호</th>
                    <td>
                        <span class="radio-group size2">
                            <input type="radio" id="itemNo" name="schType" value="itemNo" checked><label for="itemNo"  class="lb-radio">상품번호</label>
                            <input type="radio" id="relationNo" name="schType" value="relationNo"><label for="relationNo"  class="lb-radio">상품연관번호</label>
                            <input type="radio" id="sellerItemCd" name="schType" value="sellerItemCd"><label for="sellerItemCd"  class="lb-radio">판매자 상품코드</label>
                        </span>
                        <div class="mg-t-10">
                            <textarea class="textarea mgt5" id="schKeyword" name="schKeyword" title="상품번호" placeholder="복수검색 시 Enter 또는 ,로 구분"></textarea>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">판매상태</th>
                    <td>
                        <div class="chk-group2">
                            <input type="checkbox" id="itemStatusTotal" value="Y" checked><label for="itemStatusTotal" class="lb-check">전체</label>
                            <c:forEach var="codeDto" items="${partnerItemStatus}" varStatus="status">
                                    <input type="checkbox" class="itemStatus" id="schItemStatus${codeDto.mcCd}" name="schItemStatus" value="${codeDto.mcCd}"><label for="schItemStatus${codeDto.mcCd}" class="lb-check">${codeDto.mcNm}</label>
                            </c:forEach>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">카테고리</th>
                    <td>
                        <select  class="slc-base w-180" title="카테고리 대분류" id="searchCateCd1" name="schLcateCd"  data-default="대분류"
                                 onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                        </select>
                        <select  class="slc-base w-180" title="카테고리 중분류" id="searchCateCd2" name="schMcateCd"  data-default="중분류"
                                 onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                        </select>
                        <select  class="slc-base w-180" title="카테고리 소분류" id="searchCateCd3" name="schScateCd" data-default="소분류"
                                 onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                        </select>
                        <select  class="slc-base" title="카테고리 세분류" id="searchCateCd4" name="schDcateCd" data-default="세분류">
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="schDate">기간</label></th>
                    <td>
                        <select id="schDate" name="schDate" class="slc-base w-120">
                            <option value="regDt" selected>등록일</option>
                            <option value="saleDt">판매일</option>
                        </select>
                        <span class="inp-date"><input type="text" id="schStartDate" name ="schStartDate" class="inp-base" title="검색기간 시작일"></span> -
                        <span class="inp-date"><input type="text" id="schEndDate" name ="schEndDate" class="inp-base" title="검색기간 종료일"></span>
                        <span class="radio-group mgl5">
                            <input type="radio" name="setSchDate" id="setToday" value="0"><label for="setToday" class="lb-radio">오늘</label>
                            <input type="radio" name="setSchDate" id="setYesterday" value="-1d"><label for="setYesterday" class="lb-radio">어제</label>
                            <input type="radio" name="setSchDate" id="setOneMonth" value="-30d" checked><label for="setOneMonth" class="lb-radio">1개월</label>
                            <input type="radio" name="setSchDate" id="setThreeMonth" value="-90d"><label for="setThreeMonth" class="lb-radio">3개월</label>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="btn-wrap">
                <button type="button" id="searchBtn" class="btn-base-l type4">검색</button>
                <button type="button" id="searchResetBtn" class="btn-base-l">초기화</button>
            </div>
        </form>
    </div>

    <div class="result-wrap float mgt20">
        <p class="float-l result-p">검색 결과 <b><span id="itemTotalCount">0</span></b>건</p>
        <div class="float-r">
            <button type="button" id="excelDownloadBtn" class="btn-ico-exel"><i>엑셀다운</i></button>
        </div>
    </div>

    <div class="btn-gruop mgt20">
        <button type="button" class="btn-base-s" onclick="itemMain.openItemApplyPopup('stop');"><i>일시중지</i></button>
        <button type="button" class="btn-base-s" onclick="itemMain.openItemApplyPopup('active');"><i>일시중지해지</i></button>
        <button type="button" class="btn-base-s" onclick="itemMain.openItemApplyPopup('dispYn');"><i></i>노출여부변경</button>
        <button type="button" class="btn-base-s" onclick="itemMain.openItemApplyPopup('salePrice');"><i>판매가변경</i></button>
        <button type="button" class="btn-base-s" onclick="itemMain.openItemApplyPopup('dcPrice');"><i>즉시할인변경</i></button>
        <button type="button" class="btn-base-s" onclick="itemMain.openItemApplyPopup('saleDate');"><i>판매기간변경</i></button>
        <button type="button" class="btn-base-s" onclick="itemMain.openItemApplyPopup('shipPolicy');"><i>배송정책변경</i></button>
    </div>

    <div class="data-wrap" style="height: 500px" id="itemListGrid">
    </div>

    <div class="modal-wrap">
        <jsp:include page="/WEB-INF/views/pages/item/pop/itemStopPop.jsp" />
        <jsp:include page="/WEB-INF/views/pages/item/pop/itemActivePop.jsp" />
        <jsp:include page="/WEB-INF/views/pages/item/pop/itemSalePricePop.jsp" />
        <jsp:include page="/WEB-INF/views/pages/item/pop/itemSaleDatePop.jsp" />
        <jsp:include page="/WEB-INF/views/pages/item/pop/itemDcPricePop.jsp" />
        <jsp:include page="/WEB-INF/views/pages/item/pop/itemDispYnPop.jsp" />
        <jsp:include page="/WEB-INF/views/pages/item/pop/itemShipPolicyPop.jsp" />
        <jsp:include page="/WEB-INF/views/pages/item/pop/resultPop.jsp" />
    </div>

</section>

<script>
    // 상품 리얼그리드
    ${itemListGridBaseInfo}

    const itemStatus = '${itemStatus}';

    var parentGrid = null;
    var item = {};

</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/common/itemGridCommon.js?${fileVersion}"></script>

<script type="text/javascript" src="/static/js/item/itemMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemCommon.js?${fileVersion}"></script>
