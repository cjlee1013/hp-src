<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div id="itemDcPricePop" class="modal" role="dialog">
    <h2 class="h2">즉시할인가격 일괄변경</h2>
    <form id="itemDcPricePopForm">
    <div class="modal-cont">
        <p>
            <span id="itemDcPriceCnt">0</span> 개 상품에 대한 즉시할인 일괄변경
        </p>
        <div class="radio-group size2 mgt20">
            <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                <c:set var="checked" value="" />
                <c:if test="${codeDto.ref1 eq 'df'}">
                    <c:set var="checked" value="checked" />
                </c:if>
                <input type="radio" id="dcYn${codeDto.mcCd}" name="dcYn" value="${codeDto.mcCd}" ${checked}><label for="dcYn${codeDto.mcCd}"  class="lb-radio">${codeDto.mcNm}</label>
            </c:forEach>
        </div>

        <div class="dcArea" style="display: none;">
            <div class="mgt20">
                <input type="text" id="dcPrice" name="dcPrice" class="inp-base" title="할인" maxlength="9" placeholder="고정가격 입력"> 원
            </div>
            <p class="bul-p mg-t-10"> 입력된 할인가격으로 판매됩니다. 할인차액이 아닌 할인 판매가격을 입력해주세요.</p>

            <div class="mgt20">
                <input type="checkbox" name="dcPeriodYn" id="dcPeriodYn" value="Y"><label for="dcPeriodYn" class="lb-check mgr10">특정기간만 할인</label>
            </div>

            <div class="mgt10">
                <span class="inp-date-time w-200"><input type="text" name="dcStartDt" id="dcStartDt" class="inp-base" title="할인기간 시작일" ></span> -
                <span class="inp-date-time w-200"><input type="text"  name="dcEndDt" id="dcEndDt" class="inp-base" title="할인기간 종료일" ></span>
            </div>


        </div>


        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l" onclick="itemDcPricePop.closeModal()"><i>취소</i></button>
            <button type="button" class="btn-base-l type4" id="setItemDcPriceBtn"><i>저장</i></button>
        </div>
    </div>
    </form>

    <button type="button" class="btn-close2 ui-modal-close" onclick="itemDcPricePop.closeModal()"><i class="hide" >닫기</i></button>
</div>

<script>
    $(document).ready(function() {
        itemDcPricePop.init();
    });
</script>

<script type="text/javascript" src="/static/js/item/pop/itemDcPricePop.js?v=${fileVersion}"></script>