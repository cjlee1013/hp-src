<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="itemStopPop" class="modal" role="dialog">
    <h2 class="h2">판매상태 일괄변경</h2>

    <form id="itemStopPopForm">
        <div class="modal-cont">
            <p>
                <span id= "itemStatusStopCnt">0</span> 개 상품에 대해 일시중지 처리하시겠습니까?  <br>
                판매대기, 판매중, 판매종료 상태인 상품만 일시중지 처리됩니다.
            </p>

            <div class="mgt20">
                <textarea class="textarea" id="stopReason" name="stopReason" maxlength="30" title="일시중지 사유" placeholder="일시중지 사유를 입력해주세요."></textarea>
            </div>

            <div class="btn-wrap mgt30">
                <button type="button" class="btn-base-l" onclick="itemStopPop.closeModal()"><i>취소</i></button>
                <button type="button" class="btn-base-l type4" id="setItemStopBtn"><i>저장</i></button>
            </div>
        </div>
    </form>

    <button type="button" class="btn-close2 ui-modal-close" onclick="itemStopPop.closeModal()"><i class="hide" >닫기</i></button>
</div>

<script>

    $(document).ready(function() {
        itemStopPop.init();
    });
</script>

<script type="text/javascript" src="/static/js/item/pop/itemStopPop.js?v=${fileVersion}"></script>