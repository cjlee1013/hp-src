<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <!-- 기본정보 -->
    <div class="ui-tooltip" id="tooltipCategory" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 카테고리는 대분류 > 중분류 > 소분류 > 세분류 순으로 선택해주세요.<br>
        · 카테고리별 판매수수료가 다르며, 판매가 입력 시 자동으로 계산된 판매수수료를 확인하실 수 있습니다.<br>
        · 카테고리는 통합되거나 분리될 수 있으며, 변경 시 '공지사항'에 별도로 안내됩니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipItemNm" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 고객이 상품을 잘 찾고 구매를 결정할 수 있도록 상품명에 명확한 정보만을 입력해 주세요.<br>
        · 상품명은 최대 한글, 영문 포함하여 50자까지 입력가능합니다.<br>
        · 브랜드를 등록하면, 상품명에 브랜드가 자동으로 노출됩니다.(일부 브랜드 제외)<br>
        · 잘못된 상품명을 입력 시 검색에서 불이익을 받게 되며 판매 상품과 직접 관련이 없는 상품명, 유명 상품 유사문구, 스팸성 키워드 입력 시 관리자에 의해 판매활동이 제재될 수 있습니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipItemType" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 상품의 상태에 따라 새상품/중고상품/리퍼상품/반품상품/주문제작상품을 선택해주세요.<br>
        · 검색, 전시에서 다양한 서비스로 활용 될 예정입니다.<br>
    </div>
    <!--판매정보-->
    <div class="ui-tooltip" id="tooltipOriginPrice" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 상품의 최초 출시가격을 입력해주세요.<br>
        · 상품의 노출, 결제에는 영향이 없는 정보입니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipSalePrice" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 고객에게 노출, 결제되는 가격으로 최소 100원 부터 입력가능합니다.<br>
        · 판매가를 기준으로 홈플러스와 판매수수료 정산이 됩니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipDcPrice" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 고객에게 할인된 가격으로 상품을 판매할 수 있습니다.<br>
        · 도서 카테고리의 경우 판매가격 기준 10% 이상 할인 설정이 제한됩니다.(도서정가제)<br>
        · 입력된 할인가격으로 판매됩니다. 할인 차액이 아닌 할인 판매가격을 입력해주세요.<br>
        · 판매가격보다 할인된 가격을 입력해주세요.<br>
    </div>

    <div class="ui-tooltip" id="tooltipRsv" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 예약판매기간에 주문을 받아 출하일 이후 상품을 발송해 판매하는 방식입니다.<br>
        · 예약판매기간 종료 후에는 남은 판매기간동안 일반판매로 자동 전환됩니다.<br>
        · 출하일은 판매종료일과 같은 날 혹은 그 이후로 설정해주세요.<br>
        · 입력하신 출하일은 상품상세 페이지에 안내되며, 출하일 지연 시 관리자에 의해 판매활동이 제재될 수 있습니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipStockQty" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 판매 가능한 재고수량을 입력해주세요.<br>
        · 재고가 0이 되면 상품이 품절로 변경되며, 전시대상에서 제외됩니다.<br>
        · 상품등록 후 재고수정 시 '수량변경'을 체크하여 재고를 수정할 수 있습니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipSaleUnit" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 1회 구매시 구매할 수 있는 단위수량을 제한합니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipPurchaseMinQty" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 1회 구매시 최소 구매할 수 있는 수량을 제한합니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipPurchaseLimitYn" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 1회 구매시 최대 구매할 수 있는 수량을 제한합니다.<br>
        · 기간 제한시 최대 30일까지 이며, 비회원은 구매가 제한됩니다.<br>
    </div>
    <!--옵션정보 -->
    <div class="ui-tooltip" id="tooltipOptTxt" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 구매시 추가정보의 입력이 필요한 경우 구매자가 직접 입력 할 수 있도록 텍스트형 옵션을 제공합니다.<br>
        · 옵션 개수는 최대 2단계 설정가능합니다.<br>
        · 옵션명은 최대 한글, 영문 포함하여 25자까지 입력가능합니다.<br>
    </div>

    <!--이미지-->
    <div class="ui-tooltip" id="tooltipImg" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 최대 3개의 이미지를 등록가능합니다.<br>
        · 이미지 사이즈: 최소 1200 X 1200 / 용량: 최대 4MB 이하 / 파일 : JPG, JPEG, PNG
        · 1개의 대표이미지를 필수로 선택해주세요.<br>
        · 권장 크기에 맞지 않는 이미지를 등록하실 경우 이미지가 깨져 보일 수 있으니 유의해주세요.<br>
        · 권리자의 허락을 받지 않은 저작물 및 연예인 사진, 이름 등록 시 관리자에 의해 판매활동이 제재될 수 있습니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipListImg" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 최대 1개의 이미지를 등록가능합니다.<br>
        · 이미지 사이즈: 최소 900 X 470 / 용량: 최대 4MB 이하 / 파일 : JPG, JPEG, PNG
        · 리스팅 이미지를 필수로 등록해주세요.<br>
        · 권장 크기에 맞지 않는 이미지를 등록하실 경우 이미지가 깨져 보일 수 있으니 유의해주세요.<br>
        · 리스팅 이미지는 모바일 환경에서 메인, 프로모션 등 주요 페이지에 상품 노출 시 사용됩니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipItemDetail" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 상세설명 권장 크기는 가로 840px입니다.<br>
        · 홈플러스 이외의 외부링크, 일부 스크립트 및 태그는 자동 삭제될 수 있습니다.<br>
        · 직거래 가능 문구, 현금결제 시 할인 문구를 기재하는 경우 관리자에 의해 판매활동이 제재될 수 있습니다.<br>
        · 권리자의 허락을 받지 않은 저작물, 연예인 사진, 이름 등록 시 관리자에 의해 판매활동이 제재될 수 있습니다.<br>
        · 구매자에게 정확한 상품정보를 제공하지 않아 발생하는 모든 분쟁 및 손해에 대해서는 판매자가 책임을 지고 해결해야 합니다.<br>
    </div>


    <div class="ui-tooltip" id="tooltipShip" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · [배송정보관리] 버튼을 클릭하여 판매자의 배송정책 리스트 중 1개를 선택해야 배송비 입력이 가능합니다.<br>
        · 배송정책을 수정하면 배송정책이 선택된 상품에 자동으로 변경됩니다.<br>
        · 배송정책의 '배송추가정보'는 자동으로 변경되지 않습니다.<br><br>

        1. 배송방법<br>
        · 택배배송 : 택배회사를 통해 상품을 배송<br>
        · 직접배송 : 가구 등 판매자가 직접 배송을 진행<br>
        · 방문수령(픽업) : 구매자가 직접 판매자에게 방문하여 상품을 수령<br>
        · 우편배송 : 우편을 통한 상품을 배송<br>
        · 퀵배송 : 퀵서비스 업체를 통해 배송<br><br>

        2. 배송비 유형<br>
        · 상품별배송 : 상품단위로 배송비 부과<br>
        · 묶음배송(판매자별) : 판매자의 다른 상품과 동시 구매할 경우 배송비를 묶어서 한번만 부과<br><br>


        3. 배송비종류<br>
        · 무료 : 주문 조건에 상관없이 무료배송<br>
        · 유료 : 주문 조건에 상관없이 고정적인 배송비를 부과<br>
        · 조건부 무료 : 상품가격이 일정금액 이상일 때 배송비를 부과<br><br>

        4. 수량별 차등<br>
        · 중량이 무거운 상품에 주로 적용되는 배송비로 상품의 구매수량에 따라 배송비를 추가로 부과<br><br>

        5. 배송비 결제방식<br>
        · 선결제 : 상품 결제시 배송비 결제<br>
        · 착불 : 상품 수령시 배송비 결제<br><br>

        6. 배송가능지역<br>
        · 전국 또는 제주/도서산간 제외 선택<br><br>

        7.도서산간 배송비<br>
        · 배송가능 지역이 전국일 경우 제주/도서산간 추가배송비 부과가능<br>
    </div>

    <div class="ui-tooltip" id="tooltipClaimShipFee" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 반품/교환 배송비는 반품이나 교환으로 인해 추가적으로 발생되는 배송비입니다.<br>
        · 반품/교환 배송비는 편도 금액으로 설정할수 있습니다.<br>
        · 초기배송비가 무료였던 경우에는 설정하신 금액의 두 배에 해당하는 금액이 반품배송비로 부과됩니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipReleaseDay" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 출고기한은 상품 주문 시 언제 출고가 가능한지에 대하여 미리 지정하는 정보입니다.<br>
        · 상품상세화면에 입력된 출고기한이 노출되며, 주문이 완료된 상품은 출고기한이 지나기 전에 발송해야 합니다.<br>
        · 오늘발송 선택 시 입력한 시간 이전의 주문은 당일 발송해야 합니다.<br>
    </div>

    <!--속성정보-->
    <div class="ui-tooltip" id="tooltipBrand" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 브랜드명을 등록하면 검색결과, 카테고리 등 다양한 서비스에 내 상품이 노출됩니다.<br>
        · 브랜드명을 등록하면 상품명에 브랜드가 자동으로 노출됩니다.(일부 브랜드 제외) <br>
    </div>

    <div class="ui-tooltip" id="tooltipAttr" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 상품속성을 등록하면 검색결과, 카테고리 필터에 서비스되어 내 상품을 고객이 더 쉽고 빠르게 찾을 수 있습니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipNotice" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 상품별 유형에 맞는 정보 항목을 등록해야 하며, 정확한 상품 정보를 제공하지 않아 발생하는 분쟁 및 손해에 대해서는 판매자가 책임을 지고 해결해야 합니다.<br>
        · 허위정보를 기재한 경우 관리자에 의해 판매활동이 제재될 수 있으며, 관련법에 따라 처벌될 수도 있습니다.<br>
    </div>

    <!--부가정보-->

    <div class="ui-tooltip" id="tooltipSellerItemCd" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 판매자가 별도 관리가 필요한 코드를 입력할 수 있습니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipAdultType" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 청소년 구매불가 카테고리인 경우 상품 이미지가 노출되지 않으며 '19금'으로 표시됩니다.<br>
        · 미성년자가 구매할 수 없는 성인용품일 경우에는 반드시 '성인'으로 선택해 주시기 바랍니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipEp" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 가격비교 사이트 등록을 체크하면, 해당 상품은 홈플러스 이외의 다양한 가격비교에 노출될 수 있습니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipSchTag" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 검색태그 등록 시 더 많은 검색결과로 노출되어 내 상품을 고객이 더 쉽고 빠르게 찾을 수 있습니다.<br>
        · 상품과 관련이 없는 검색태그 입력 시 관리자에 의해 판매활동이 제재될 수 있습니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipGlobalAgcy" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 해외배송상품 사용 선택 시 세무/법률적 책임은 판매자에게 있습니다.<br>
        · 구매자는 주문 시 통관번호를 입력해야 상품을 구매할 수 있습니다.<br>
    </div>

    <div class="ui-tooltip" id="tooltipCert" role="tooltip" aria-hidden="true">
        <span class="tt-arrow"></span>
        · 판매상품의 KC인증 유무와 종류에 따라서 등록해 주세요.<br>
        · 입력한 인증정보 '조회'시 제품안전정보센터에서 제공된 정보를 기준으로 처리되며, 정상적인 정보가 아닐 경우 등록이 제한됩니다.<br>
        · 정상적인 인증정보를 입력했으나 조회 결과 실패한 경우, '상세설명에 별도표기'로 설정이 가능하며, 상품 상세 페이지에 인증번호와 모델명, KC마크를 꼭 표기해주셔야 합니다.<br>
        · KC인증이 필요한 상품을 인증 없이 판매하는 경우 3년 이하의 징역 또는 3천만원 이하의 벌금형에 처해질 수 있습니다.<br>
    </div>
