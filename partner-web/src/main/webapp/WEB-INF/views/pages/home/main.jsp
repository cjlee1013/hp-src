<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
<!-- 메인시작 -->
<!-- 기본 프레임 -->
<div class="content min-w1280">
    <div class="">
        <div class="com wrap-title has-border">
            <h2 class="title font-malgun">
                <i class="fa fa-bullhorn"></i> 알려드립니다 NO IFRAME
            </h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <tbody>
                <tr>
                    <th class="table-notice">
                        <div class="notice-title">
                            해당 메뉴에 대한 이용권한이 없습니다. 이용권한이 필요하신 경우, 전자결재를 통해 신청하여 주십시오.
                        </div>
                        <p class="mg-t-20">권한부여 담당자: 홍길동</p>
                        <p class="mg-t-10">연락처: 010-123-4567</p>
                        <p class="mg-t-10">이메일: hongs@homeplus.co.kr</p>
                    </th>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <tbody>
                <tr>
                    <th class="table-notice">
                        <div class="notice-title">권한관리 하위 메뉴 권한 관련 문의사항은 아래 문의처로 연락 주십시오.</div>
                        <p class="mg-t-20">담당자: 홍길동</p>
                        <p class="mg-t-10">연락처: 010-123-4567</p>
                        <p class="mg-t-10">이메일: hongs@homeplus.co.kr</p>
                    </th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
--%>
<!-- 메인종료 -->

<div class="modal-wrap" style="display: none;">
<!-- 약관 동의여부 팝업 -->
<jsp:include page="/WEB-INF/views/pages/home/sellerAgree.jsp" />
<!-- 변경주기 팝업(동일 비밀번호 90일 이상 사용) -->
<jsp:include page="/WEB-INF/views/pages/user/pop/passwordExceedPop.jsp" />
</div>

<script>
    <%-- 약관동의여부 --%>
    const acceptYn = "${acceptYn}";
    <%-- 동일 비밀번호 90일 미만으로 사용여부 --%>
    const isUnder90DaysSamePassword = ${isUnder90DaysSamePassword};

    $( document ).ready(function() {
        if (acceptYn === "N"){
            <%-- 파트너 약관동의 레이어 팝업 노출 --%>
            sellerAgreeModalOpen();
        } else if (acceptYn === "Y" && isUnder90DaysSamePassword === false) {
            <%-- 파트너 동의완료 상태에서 동일 비밀번호를 90일 이상(false) 사용할 경우 '비밀번호 변경팝업' 노출 --%>
            passwordPopModalOpen();
        }
    });

    // 파트너 동의 레이어 팝업 노출
    function sellerAgreeModalOpen() {
        $ui.modalOpen('P00-02-03');
    }

    // 파트너 동의 레이어 팝업 닫기
    function sellerAgreeModalClose() {
        $ui.modalClose('P00-02-03');
    }

    // 동일 비밀번호 90일 이상 사용시 비밀번호 변경팝업 노출
    function passwordPopModalOpen() {
        $ui.modalOpen('passwordExceedPop');
    }

    //파트너 동의 레이어팝업 이벤트
    $(function () {
        const chkList = $("input[id^='joinChk']"),
            allChk = $("input[id='allChk']"),
            btnClose = $('#sellerAgreePopCloseBtn'),
            btnAccept = $('#sellerAgreePopAcceptBtn');

        btnClose.on("click", function() {
            window.location.replace('/logout')
        });

        chkList.each(function () {
            $(this).change(function () {
                var chkedLngt = $("input[id^='joinChk']:checked").length;
                if (chkList.length === chkedLngt) {
                    allChk.prop('checked', true);
                } else {
                    allChk.prop('checked', false);
                }
            });
        });

        allChk.change(function () {
            if ($(this).prop('checked')) {
                chkList.prop('checked', true);
            } else {
                chkList.prop('checked', false);
            }
        });

        btnAccept.on("click", function() {
            var checkedCnt = $("input[id^='joinChk']:checked").length;
            if (checkedCnt < 3) {
                alert('이용약관에 전체 동의해주세요.');
                return false;
            }
            CommonAjax.basic({
                url: "/partner/setAccept.json",
                method: "GET",
                contentType: "application/json",
                callbackFunc: function (res) {
                    if (res.data === true) {
                        sellerAgreeModalClose();
                        <%-- 약관동의 이후 '비밀번호 변경 레이어 팝업' 노출처리 --%>
                        if (isUnder90DaysSamePassword === false) {
                            passwordPopModalOpen();
                        }
                    } else {
                        alert("약관에 동의해주세요.");
                    }
                }
            });
        });
    });
</script>


