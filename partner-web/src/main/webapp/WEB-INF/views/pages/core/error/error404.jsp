<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div style="padding: 60px;margin: 20px;border: 10px solid #eee;text-align: center;">
    <i class="fa fa-frown-o" style="font-size: 60px; color: #99A4B8;"></i>
    <h1 style="margin: 12px 0px; font-size: 26px; color: #626d83;">요청하신 페이지를 찾을 수 없습니다</h1>
    <div style="font-size: 14px; color: #999;">
        페이지가 삭제되었거나 일시적으로 사용할 수 없는 상태입니다.
    </div>
</div>