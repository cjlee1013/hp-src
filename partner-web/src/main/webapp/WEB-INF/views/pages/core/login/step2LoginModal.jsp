<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript" src="/static/js/common/countdown.js?v=${fileVersion}"></script>

<div id="step2LoginModal" class="modal" role="dialog">
    <h2 class="h2">2단계 로그인 인증</h2>
    <div class="modal-cont" style="height: auto;">
        <p>
            <span class="fc03">
                개인정보보호법 제 29조 및 개인정보의 안정성 확보 조치 기준 제6조 2항에 따라 2단계 로그인 인증 진행이 필요합니다.
                등록된 담당자를 선택하여 2단계 로그인 인증을 완료해 주시기 바랍니다. <br />
                영업 담당자 외 인증 담당자 등록이 추가로 필요한 경우 <strong>[회원정보수정]</strong> 페이지에서 추가 등록하실 수 있습니다.
            </span>
        </p>

        <div class="tbl-data mgt20 align-c">
            <table id="managerInfo">
                <caption>담당자 정보</caption>
                <colgroup>
                    <col style="width:80px">
                    <col style="width:120px">
                    <col style="width:auto">
                </colgroup>
                <thead>
                <tr class="">
                    <th scope="col" class="tit">선택</th>
                    <th scope="col" class="tit">성명</th>
                    <th scope="col" class="tit">휴대폰번호</th>
                </tr>
                </thead>
                <tbody>
                <%-- tbody --%>
                <%-- //tbody --%>
                </tbody>
            </table>
            <div class="btn-wrap mgt20" id ="resendBtnArea" style="display: none;">
                <!--  발송전 텍스트 : 인증번호 발송 / 발송후 텍스트 : 재발송 -->
                <button type="button" id="reSendBtn" class="btn-base-m type5"><i>재발송</i></button>
            </div>
            <div class="btn-wrap mgt20" id="sendBtnArea">
                <!--  발송전 텍스트 : 인증번호 발송 / 발송후 텍스트 : 재발송 -->
                <button type="button" id="sendMobileCertNoBtn" class="btn-base-m type5"><i>인증번호 발송</i></button>
            </div>
        </div>
        <div class="tbl-form3 mgt20">
            <table>
                <caption>휴대폰번호 인증</caption>
                <colgroup>
                    <col style="width:22%">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">인증번호</th>
                    <td>
                        <input type="text" id="mobileCertNo" name="mobileCertNo" class="inp-base" title="인증번호 입력" maxlength="6">
                        <button type="button" id="verifyMobileCertNoBtn" class="btn-base type5 mgl10">확인</button>
                        <span class="fc02 txt mgl10" id="step2SpanTimeOutCount" style="display: none;">(남은시간 : <span id="step2SpanTimeOut" class="remain-ins-time">02:59</span>)</span>
                        <span class="fc02 txt mgl10" id="step2SpanTimeOutComplete" style="display: none;">인증완료</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="btn-wrap mgt20">
            <button type="button" id="step2LoginBtn" class="btn-base-l type4"><i>로그인</i></button>
        </div>
    </div>

    <button type="button" id="step2CloseBtn" class="btn-close2 ui-modal-close" onclick=""><i class="hide">닫기</i></button>
</div>
<script>
    $( document ).ready(function() {
        step2LoginModal.init();
    });

    var step2LoginModal = {
        timer       	: 0,
        mobileCertToken	: "",		// 휴대폰 인증번호 토큰
        mobile			: "",
        CERT_NO_EXPIRE_TIME_3_MIN : 3,
        mobileCountdownTimer	: null,
        mobileVerifiedToken	: "",		// 휴대폰 번호인증 완료토큰(2단계 로그인시 사용)
        mobileCheck: false,
        init: function () {
            step2LoginModal.event();
        },
        getRoot: function () {
            return $('#step2LoginModal');
        },
        openModal: function (res) {
            if ($.jUtil.isNotEmpty(res.data)) {
                $ui.modalOpen('step2LoginModal',
                    step2LoginModal.setStep1LoginModal(res.data.managerList));
            } else {
                alert("통신 에러 : 내부 오류가 발생하였습니다.");
            }
        },
        closeModal: function() {
            $ui.modalClose('step2LoginModal');
        },
        event: function(){
            //인증번호 발송
            $("#sendMobileCertNoBtn").on("click", function() {
                step2LoginModal.sendMobileCertNo();
            });

            //인증번호 검증
            $("#verifyMobileCertNoBtn").on("click", function() {
                step2LoginModal.verifyMobileCertNo();
            });

            //2단계 로그인 버튼
            $("#step2LoginBtn").on("click", function() {
                loginProcess();
            });

            //재발송 버튼 클릭시 라디오버튼 해제, 버튼 변경
            $("#reSendBtn").on("click", function() {
                $("#resendBtnArea").hide();
                $("#sendBtnArea").show();

                //인증번호 입력창 readonly
                $("#mobileCertNo").attr("readonly", false)

                step2LoginModal.setRadioButtonDisabledFalse();
            });

            //2단계 로그인 닫기 버튼
            $("#step2CloseBtn").on("click", function() {

                step2LoginModal.mobileVerifiedToken = "";
                step2LoginModal.mobileCheck = false;
                step2LoginModal.mobile = "";

                clearInterval(step2LoginModal.timer);
                $("#step2SpanTimeOutCount").hide();
                $("#step2SpanTimeOutComplete").hide();

                //인증번호 입력창 포커스
                $("#mobileCertNo").val("");
                $("#mobileCertNo").attr("readonly", false)
                // 인증 view
                $("#resendBtnArea").hide();
                $("#sendBtnArea").show();

                step2LoginModal.setRadioButtonDefault();

                step2LoginModal.closeModal();
            });

        },
        /**
         * 2단계 인증 담당자 리스트 화면 출력
         * @param managerList
         */
        setStep1LoginModal: function (managerList) {
            const $modal = step2LoginModal.getRoot();

            if ($.jUtil.isEmpty(managerList)) {
                let tr = $('<tr>');
                tr.append($('<td colspan="3">').text("등록 된 담당자가 없습니다."));
                $modal.find('#managerInfo > tbody:last').empty();
                $modal.find('#managerInfo > tbody:last').append(tr);
            } else {
                let tr = "";
                for (let i = 0; i < managerList.length; i++) {
                    tr += '<tr>';
                    tr += '    <td>';
                    tr += '        <input type="radio" id="selectRadio' + i+ '" name="selectRadio"' +
                        ' value="' + managerList[i].mngMobile + '">';
                    tr += '            <label for="selectRadio' + i + '" class="lb-radio2 only">선택</label>';
                    tr += '    </td>';
                    tr += '    <td>'+ managerList[i].mngNm +'</td>';
                    tr += '    <td>'+ managerList[i].mskMngMobile + '</td>';
                    tr += '</tr>';
                }

                $modal.find('#managerInfo > tbody:last').empty();
                $modal.find('#managerInfo > tbody:last').append(tr);
            }
        },
        /**
         * 인증담당자 인증번호 문자 발송
         */
        sendMobileCertNo: function () {
            const mobile = $('input[name="selectRadio"]:checked').val();

            if ($.jUtil.isEmpty(mobile)) {
                alert("2단계 로그인 인증 담당자명을 선택해주세요.");
                return false;
            }

            CommonAjax.basic({
                url:"/common/mobile/certno/send.json?mobile=" + mobile
                , method:"GET"
                , callbackFunc:function(res) {
                    if (res.hasOwnProperty("data")) {
                        step2LoginModal.mobileCertToken = res.data;
                        // 타이머 호출
                        step2LoginModal.checkTimer();

                        alert("인증번호가 전송되었습니다.");

                        //라디오 버튼 및 버튼 비활성화
                        step2LoginModal.setRadioButtonDisabled();
                        //인증 view 변경
                        $("#resendBtnArea").show();
                        $("#sendBtnArea").hide();
                        //타이머 노출
                        $("#step2SpanTimeOutCount").show();
                        $("#step2SpanTimeOutComplete").hide();
                        //인증번호 입력창 포커스
                        $("#mobileCertNo").focus();
                    }
                }
                , errorCallbackFunc : function() {
                    clearInterval(step2LoginModal.timer);

                    // 인증 view
                    $("#resendBtnArea").hide();
                    $("#sendBtnArea").show();
                }
            });
        },
        /**
         * 인증번호 확인 타이머
         */
        checkTimer : function() {
            const endTime = moment(new Date()).add(step2LoginModal.CERT_NO_EXPIRE_TIME_3_MIN, 'minutes');

            if (step2LoginModal.mobileCountdownTimer != null) {
                step2LoginModal.mobileCountdownTimer.endTimer();
            }

            step2LoginModal.mobileCountdownTimer = countdownTimer($('#step2SpanTimeOut'),endTime,'ms','0:00',function(){
                if (step2LoginModal.mobileCheck === false) {
                    alert('인증시간이 초과 되었습니다. 다시 진행하세요.');
                    step2LoginModal.mobileCheck = false;
                }
            });
        },
        /**
         * 인증번호 발송 시 체크되지 않은 라디오버튼 disabled 처리
         */
        setRadioButtonDisabled: function() {
            $(':radio:not(:checked)').attr('disabled', true);
        },
        /**
         * 재발송 버튼 클릭시 disabled 된 라디오버튼 해제
         **/
        setRadioButtonDisabledFalse: function() {
            $(':radio:not(:checked)').attr('disabled', false);
        },
        /**
         * 라디오버튼 초기화
         */
        setRadioButtonDefault: function() {
            $(':radio:checked').attr('cheked', false);
        },
        /**
         * 로그인 인증번호 검증요청
         */
        verifyMobileCertNo: function () {
            const mobile = $('input[name="selectRadio"]:checked').val();
            const certNo = $("#mobileCertNo").val();

            if (step2LoginModal.invalidMobileCertNo(mobile, certNo)) {
                return false;
            }

            const param = {
                certToken: step2LoginModal.mobileCertToken
                , certNo: certNo
                , mobile: mobile
            };

            CommonAjax.basic({
                url:"/verify/mobile/certNo/send.json"
                , data: JSON.stringify(param)
                , contentType: 'application/json'
                , method:"POST"
                , callbackFunc:function(res) {
                    if (res.hasOwnProperty("data")) {
                        step2LoginModal.mobileVerifiedToken = res.data;
                        step2LoginModal.mobileCheck = true;
                        step2LoginModal.mobile = mobile;

                        clearInterval(step2LoginModal.timer);

                        //타이머 노출 숨김
                        $("#step2SpanTimeOutCount").hide();
                        $("#step2SpanTimeOutComplete").show();

                        //인증번호 입력창 readonly
                        $("#mobileCertNo").attr("readonly", true)
                    } else {
                        alert(res.returnMessage);
                    }
                }
            });
        },
        invalidMobileCertNo: function (mobile, certNo) {
            if ($.jUtil.isEmpty(mobile)) {
                alert("2단계 로그인 인증 담당자명을 선택해주세요.");
                return true;
            }

            if ($.jUtil.isEmpty(certNo)) {
                alert("인증번호를 입력하세요.");
                return true;
            }
        }
    }
</script>