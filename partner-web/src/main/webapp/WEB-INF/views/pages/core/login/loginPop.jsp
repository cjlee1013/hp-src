<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript" src="/static/js/jquery-3.1.1.min.js"></script>

<script type="text/javascript">
    jQuery.fn.center = function () {
        this.css("position","absolute");
        this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
        this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
        return this;
    }

    $( document ).ready(function() {
        if( $("#loginGatewayFrameResource", parent.document).length > 0 ){
            $("#loginGatewayFrameResource", parent.document).attr("src", "${returnUrl}");
        } else {
            $("#loginGatewayFrameResource").attr("src", "${returnUrl}");
        }
        $("#loginLayer").center();
    });

</script>

<div id="loginLayer" style="display:block;width:100%; height:100%;z-index:99;">
    <iframe id="loginGatewayFrameResource" frameborder="0" allowTransparency="true"  width="100%" height="100%" src=""></iframe>
</div>