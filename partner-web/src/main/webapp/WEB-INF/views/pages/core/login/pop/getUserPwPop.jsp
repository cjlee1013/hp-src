<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="getUserPwPop" class="modal" role="dialog">
    <h2 class="h2">임시 비밀번호 발급</h2>

    <div class="modal-cont">
        <div class="tbl-data">
            <table>
                <caption>회원 정보</caption>
                <colgroup>
                    <col style="width:80px">
                    <col style="width:auto">
                    <col style="width:80px">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">ID</th>
                    <td id="partnerIdPop"></td>
                    <th scope="row">상호</th>
                    <td id="partnerNmPop"></td>
                </tr>
                </tbody>
            </table>
        </div>

        <h3 class="h4 mgt30">영업담당자에게 임시 비밀번호를 발송합니다.</h3>
        <div class="tbl-data tbl-line">
            <table>
                <caption>회원 정보</caption>
                <colgroup>
                    <col style="width:50px">
                    <col style="width:94px">
                    <col style="width:auto">
                </colgroup>
                <thead>
                <tr>
                    <th scope="col">선택</th>
                    <th scope="col">구분</th>
                    <th scope="col">내용</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="align-c">
                        <input type="checkbox" id="checkEmail" name="checkEmail"><label for="checkEmail"  class="lb-check only"></label>
                    </td>
                    <td>이메일</td>
                    <td id="mngEmailPop"></td>
                </tr>
                <tr>
                    <td class="align-c">
                        <input type="checkbox" id="checkMobile" name="checkMobile"><label for="checkMobile"  class="lb-check only"></label>
                    </td>
                    <td>휴대폰</td>
                    <td id="mngMobilePop"></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="btn-wrap mgt30">
            <button type="button" id="sendPW" class="btn-base-l type4"><i>임시 비밀번호 발급</i></button>
        </div>
    </div>

    <button type="button" id="closeBtn" class="btn-close2 ui-modal-close" onclick="$ui.modalClose('getUserPwPop')"><i class="hide" >닫기</i></button>
</div>
<script>
    $( document ).ready(function() {
        userPwPop.event();
    });

    let userPwPop = {
        event : function() {
            $("#sendPW").on('click', function() {

                if (!$("input:checkbox[id='checkEmail']").is(":checked") && !$("input:checkbox[id='checkMobile']").is(":checked")) {
                    alert("임시 패스워드를 발송할 대상을 선택하세요.");
                    return;
                }

                if(confirm("임시 패스워드를 발급하시겠습니까?")) {

                    CommonAjax.basic({
                        url: "/login/setPartnerTempPw.json",
                        data: {
                            partnerId   : $('#partnerId').val(),
                            businessNm  : $('#partnerNmPop').text(),
                            emailCheck  : $("input:checkbox[id='checkEmail']").is(":checked") ? "Y" : "N",
                            mobileCheck : $("input:checkbox[id='checkMobile']").is(":checked") ? "Y" : "N",
                        },
                        method: "POST",
                        callbackFunc: function (res) {
                            alert(res.returnMsg);

                            if (res.returnCode == "0") {
                                userPwPop.resetForm();
                                $ui.modalClose('getUserPwPop');
                                location.href = '/';
                            }
                        }
                    });
                }
            });

            $('#closeBtn').on('click', function() {
                userPwPop.resetForm();
            });
        },
        resetForm : function() {
            $('#partnerId, #businessNm, #partnerNo, #partnerNm').val('');
        }
    }
</script>