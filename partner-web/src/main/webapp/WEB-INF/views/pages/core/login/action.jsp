<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript">
    $( document ).ready(function() {
        var goUrl = parent.document.location.protocol + "//" + parent.document.location.host + '${goUrl}';
        $(parent.document.location).attr('href', goUrl);
    });

</script>
