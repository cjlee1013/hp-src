<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="login-wrap">
    <h1><img src="/static/images/logo2.png" alt="Homeplus 파트너센터" onclick="location.href='/'" style="cursor: pointer;"></h1>
    <div class="login-box">
        <h2>아이디 찾기</h2>

        <div id="loginTab" class="tab-wrap">
            <div class="tab-nav">
                <button type="button" class="tab-btn" >휴대폰번호로 찾기</button>
                <button type="button" class="tab-btn" >이메일로 찾기</button>
            </div>
            <div class="tab-pnl">
                <h3 class="hide">휴대폰번호로 찾기</h3>
                <p class="txt-base align-c">판매자 가입 시 등록한 사업자번호와 휴대폰번호를 입력해주세요.</p>
                <form>
                    <div class="mgt25">
                        <label for="partnerNoMobile" class="lb-block">사업자 번호</label>
                        <input type="text" id="partnerNoMobile" class="inp-base-l" placeholder="-없이 입력해주세요">
                        <label for="affiManagerMobile" class="lb-block">영업담당자 휴대폰 번호</label>
                        <input type="text" id="affiManagerMobile" class="inp-base-l" placeholder="-없이 입력해주세요">
                    </div>
                    <p class="txt-err" style="display: none">일치하는 정보를 찾을 수 없습니다.</p>
                    <div class="btn-wrap mgt30">
                        <button type="button" id="getUserIdMobile" class="btn-base-l type4"><i>확인</i></button>
                    </div>
                </form>
            </div>
            <div class="tab-pnl">
                <h3 class="hide">이메일로 찾기</h3>
                <p class="txt-base align-c">판매자 가입 시 등록한 사업자번호와 이메일주소를 입력해주세요.</p>
                <form>
                    <div class="mgt25">
                        <label for="partnerNoEmail" class="lb-block">사업자 번호</label>
                        <input type="text" id="partnerNoEmail" class="inp-base-l" placeholder="-없이 입력해주세요">
                        <label for="affiManagerEmail" class="lb-block">영업담당자 이메일주소</label>
                        <div class="inp-mail">
                            <input type="text" name="affiManagerEmail" id="affiManagerEmail" class="inp-base w-260" title="대표 이메일 주소" placeholder="예 : example@homeplus.co.kr">
                        </div>
                        <p class="txt-err" style="display: none">일치하는 정보를 찾을 수 없습니다.</p>
                    </div>

                    <div class="btn-wrap mgt30">
                        <button type="button" id="getUserIdEmail" class="btn-base-l type4"><i>확인</i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<%--footer--%>
<div class="footer-wrap">
    <jsp:include page="/WEB-INF/views/pages/core/includeFooter.jsp" />
</div>

<div class="modal-wrap" style="display: none;">
    <jsp:include page="/WEB-INF/views/pages/core/login/pop/getUserIdPop.jsp" />
</div>

<script type="text/javascript">
    $( document ).ready(function() {
        userFind.init();
    });

    let userFind = {
        init : function() {
            CommonAjaxBlockUI.targetId('getUserIdMobile');
            CommonAjaxBlockUI.targetId('getUserIdEmail');
            userFind.event();
        },
        event : function() {
            $('#loginTab').uiTab();

            $('#loginTab').on('click', '[id^=loginTab-btn]', function(){
                $('#partnerNoEmail, #affiManagerEmail, #partnerNoMobile, #affiManagerMobile').val('');
            });

            $('#getUserIdMobile').on('click', function(){
                userFind.getUserId($('#partnerNoMobile').val(), $('#affiManagerMobile').val(), null);
            });

            $('#getUserIdEmail').on('click', function(){
                userFind.getUserId($('#partnerNoEmail').val(), null, $('#affiManagerEmail').val());
            });
        },
        getUserId : function(partnerNo, affiManagerMobile, affiManagerEmail) {

            if ($.jUtil.isEmpty(partnerNo)) {
                return $.jUtil.alert('사업자 등록번호를 입력해주세요.');
            }

            if (affiManagerMobile != null && $.jUtil.isEmpty(affiManagerMobile)) {
                return $.jUtil.alert('영업담당자 휴대폰 번호를 입력해주세요.');
            } else if (affiManagerMobile === null) {
                affiManagerMobile = '';
            }

            if (affiManagerEmail != null && $.jUtil.isEmpty(affiManagerEmail)) {
                return $.jUtil.alert('영업담당자 이메일 주소를 입력해주세요.');
            } else if (affiManagerEmail === null) {
                affiManagerEmail = '';
            }

            CommonAjax.basic({
                url: "/login/getUserId.json?partnerNo=" + partnerNo + '&affiManagerMobile=' + affiManagerMobile + '&affiManagerEmail=' + affiManagerEmail,
                method: "GET",
                contentType: "application/json",
                callbackFunc: function (resList) {
                    $('#partnerIdPopArea').html('');
                    if (resList.length > 0) {
                        for (var i in resList) {
                            let innerHtml = '';
                            innerHtml = '<tr><td>'+resList[i].partnerId+'</td><td>'+resList[i].regDt+'</td></tr>'
                            $('#partnerIdPopArea').append(innerHtml);
                        }

                        $ui.modalOpen('getUserIdPop');
                    } else {
                        $('.txt-err').show();
                    }

                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
        }
    }
</script>