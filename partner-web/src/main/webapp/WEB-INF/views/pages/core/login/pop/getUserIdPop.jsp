<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="getUserIdPop" class="modal" role="dialog" tabindex="" aria-modal="true" data-focus="focus-getUserIdPop" data-focus-prev="focus-getUserIdPop-close" style="display: none; margin-top: -146px;">
    <h2 class="h2">아이디 안내</h2>

    <div class="modal-cont" style="height: auto;">
        <p class="fs14 fc03 mgb15">입력하신 정보와 일치하는 아이디 정보입니다.</p>
        <div class="tbl-data align-c">
            <table>
                <caption>아이디 안내</caption>
                <colgroup>
                    <col style="width:50%" span="2">
                </colgroup>
                <thead>
                <tr>
                    <th scope="col" class="align-c">아이디</th>
                    <th scope="col" class="align-c">가입일</th>
                </tr>
                </thead>
                <tbody id="partnerIdPopArea">
                </tbody>
            </table>
        </div>
        <div class="btn-wrap mgt30">
            <button type="button" class="btn-base-l" onclick="location.href='/login/getUserPw'"><i>비밀번호 찾기</i></button>
            <button type="button" class="btn-base-l type4" onclick="location.href='/login'"><i>로그인하기</i></button>
        </div>
    </div>

    <button type="button" class="btn-close2 ui-modal-close" onclick="$('#partnerNoEmail, #affiManagerEmail, #partnerNoMobile, #affiManagerMobile').val(''); $ui.modalClose('getUserIdPop');" data-focus="focus-getUserIdPop-close" data-focus-next="focus-getUserIdPop"><i class="hide">닫기</i></button>
</div>