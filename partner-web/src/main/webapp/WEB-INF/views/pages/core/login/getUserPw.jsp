<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="login-wrap">
    <h1><img src="/static/images/logo2.png" alt="Homeplus 파트너센터" onclick="location.href='/'" style="cursor: pointer;"></h1>
    <div class="login-box">
        <h2>비밀번호 찾기</h2>
        <p class="txt-base">아이디와 판매자 가입 시 등록한 상호명 및 <br>사업자번호를 입력해주세요</p>
        <form>
            <div class="mgt25">
                <label for="partnerId" class="lb-block">아이디</label>
                <input type="text" id="partnerId" class="inp-base-l" placeholder="아이디를 정확히 입력해주세요">
                <input type="hidden" name="businessNm" id="businessNm">
                <label for="partnerNm" class="lb-block">사업자 등록증 상호</label>
                <input type="text" id="partnerNm" class="inp-base-l" placeholder="사업자 등록증 상호를 입력해주세요">
                <label for="partnerNo" class="lb-block">사업자번호</label>
                <input type="text" id="partnerNo" class="inp-base-l" placeholder="-없이 입력해주세요">
            </div>
            <p class="txt-err" style="display: none">일치하는 정보를 찾을 수 없습니다.</p>
            <div class="btn-wrap mgt30">
                <button type="button" id="getUserPw" class="btn-base-l type4"><i>확인</i></button>
            </div>
        </form>
    </div>
</div>


<%--footer--%>
<div class="footer-wrap">
    <jsp:include page="/WEB-INF/views/pages/core/includeFooter.jsp" />
</div>
<div class="modal-wrap" style="display: none;">
    <jsp:include page="/WEB-INF/views/pages/core/login/pop/getUserPwPop.jsp" />
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        userPwFind.init();
    });

    let userPwFind = {
        init : function() {
            userPwFind.event();
            CommonAjaxBlockUI.targetId('getUserPw');
        },
        event : function() {
            $('#loginTab').uiTab();

            $('#getUserPw').on('click', function(){
                userPwFind.getUserPw($('#partnerId').val(), $('#partnerNm').val(), $('#partnerNo').val());
            });
        },
        getUserPw : function(partnerId, partnerNm, partnerNo) {

            if ($.jUtil.isEmpty(partnerId)) {
                return $.jUtil.alert('아이디를 입력해주세요.', 'partnerId');
            }

            if ($.jUtil.isEmpty(partnerNm)) {
                return $.jUtil.alert('사업자 등록증 상호를 입력해주세요.', 'partnerNm');
            }

            if ($.jUtil.isEmpty(partnerNo)) {
                return $.jUtil.alert('사업자 등록번호를 입력해주세요.', 'partnerNo');
            }

            CommonAjax.basic({
                url: "/login/getUserPw.json",
                method: "POST",
                data: JSON.stringify({partnerId: partnerId, partnerNm: partnerNm, partnerNo: partnerNo}),
                contentType: "application/json",
                callbackFunc: function (res) {
                    if (res.hasOwnProperty('partnerId')) {
                        $('#partnerIdPop').text(res.partnerId);
                        $('#partnerNmPop').text(res.partnerNm);
                        $('#mngEmailPop').text(res.mngEmail);
                        $('#mngMobilePop').text(res.mngMobile);
                        $('#partnerId').val($('#partnerId').val().toLowerCase());
                        $ui.modalOpen('getUserPwPop');
                    } else {
                        $('.txt-err').show();
                    }

                    CommonAjaxBlockUI.stopBlockUI();
                },
                errorCallbackFunc: function (res) {
                    $.jUtil.alert(res.responseJSON.returnMessage);

                    if (res.responseJSON.returnCode == "1014") {
                        window.location.href = '/';
                    }
                }
            });
        }
    }
</script>