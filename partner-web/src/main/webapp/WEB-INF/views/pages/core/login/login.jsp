<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="/static/js/common/countdown.js?v=${fileVersion}"></script>
<%
    String savePartnerId = "";
    if (request.getAttribute("savePartnerId") != null) {
        savePartnerId = (String) request.getAttribute("savePartnerId");
    }

    boolean chkSavePartnerId = false;
    if (request.getAttribute("chkSavePartnerId") != null) {
        chkSavePartnerId = (boolean) request.getAttribute("chkSavePartnerId");
    }
%>
<script type="text/javascript">
    const RETURN_CODE_SUCCESS = "SUCCESS";

    $( document ).ready(function() {
        CommonAjaxBlockUI.global();

        documentReadyInputFocus();

        resetInputErrorClass();

        displayReturnMessage();
    });

    $(function () {
        $("#loginForm").find('input').keydown(function(key) {

            resetInputErrorClass();

            if (key.keyCode === 13) {
                $("#loginBtn").trigger("click");
            } else {
                hideReturnMessage();
            }
        });
    });

    /**
     * 최초 진입시 아이디 저장여부에 따라 input focus 설정
     */
    function documentReadyInputFocus() {
        let chkSavePartnerId = ${chkSavePartnerId};
        if (chkSavePartnerId) {
            $("#partnerPw").focus();
        } else {
            $("#partnerId").focus();
        }
    }

    /**
     * 에러 메세지 노출처리 이벤트
     */
    function displayReturnMessage(partnerId, partnerPw, returnMessage) {
        const valid_txt = $("#valid_txt");

        if ($.jUtil.isNotEmpty(returnMessage)) {
            setErrorAddClass(returnMessage);

            valid_txt.text(returnMessage);
            valid_txt.show();

            setUserIdByFailed(partnerId, partnerPw);
            return false;
        }
    }

    /**
     * 입력 시도 시 에러메세지 제거처리
     */
    function hideReturnMessage() {
        const valid_txt = $("#valid_txt");
        if (valid_txt.text().length > 0) {
            valid_txt.text("");
            valid_txt.hide();
        }
    }

    /**
     * 1단계 로그인 처리
     **/
    function step1Login() {
        const partnerId = $("#partnerId");
        const partnerPw = $("#partnerPw");

        if (invalid(partnerId, partnerPw)) {
            return false;
        }

        const step2LoginParam = {
            partnerId : partnerId.val(),
            partnerPw : partnerPw.val()
        };

        CommonAjax.basic({
            url: "/step1/login.json",
            data : step2LoginParam,
            method: "POST",
            callbackFunc: function (res) {
                if (res.returnCode !== RETURN_CODE_SUCCESS) {
                    alert(res.returnMessage);
                    return false;
                } else {
                    //2단계 로그인 인증 레이어 팝업 노출
                    step2LoginModal.openModal(res);
                }
            },
            errorCallbackFunc: function (resError) {
                displayReturnMessage($.jUtil.nvl(resError.responseJSON.data.partnerId, "")
                    , $.jUtil.nvl(resError.responseJSON.data.partnerPw, "")
                    , resError.responseJSON.returnMessage);
                //계정확인 레이어 팝업 노출
                if (resError.responseJSON.returnCode == "1014") {
                    $('#partnerIdPop').val($('#partnerId').val());
                    $ui.modalOpen('loginConfirmPop');
                } else if (resError.responseJSON.returnCode == "1019") {
                    $ui.modalOpen('idActivatePop');
                }

            }

        });
    }

    /**
     * 2단계 로그인 처리
     **/
    function loginProcess() {
        const partnerId = $("#partnerId");
        const partnerPw = $("#partnerPw");
        const chkSavePartnerId = $("input:checkbox[id='chkSavePartnerId']").is(":checked");
        const mobile = step2LoginModal.mobile;
        const verifiedToken = step2LoginModal.mobileVerifiedToken;

        if($.jUtil.isEmpty(mobile)) {
            alert("2단계 로그인 인증 진행 후 로그인이 가능합니다.");
            return false;
        }

        if($.jUtil.isEmpty(verifiedToken)) {
            alert("2단계 로그인 인증 진행 후 로그인이 가능합니다.");
            return false;
        }

        const step2LoginParam = {
            partnerId : partnerId.val(),
            partnerPw : partnerPw.val(),
            chkSavePartnerId : chkSavePartnerId,
            mobile: mobile,
            verifiedToken: verifiedToken
        };

        CommonAjax.basic({
            url: "/step2/login.json",
            data : step2LoginParam,
            method: "POST",
            callbackFunc: function (res) {
                if (res.returnCode !== RETURN_CODE_SUCCESS) {
                    alert(res.returnMessage);
                    return false;
                } else {
                    //로그인 팝업이 노출될 경우 로그인 처리 후 팝업창 닫기
                    if (self !== top && $("#loginGatewayFrameResource", parent.document).length > 0) {
                        $(location).attr('href', res.data);
                        window.open('about:blank','_parent').parent.close();

                        //탭바 내 iframe 에서 로그인 페이지가 노출될 경우, 로그인 처리 후 기존에 접근 하려던 메뉴로 이동
                    } else if (self !== top){
                        const currentTabId = parent.initialTab.currentTabId;
                        const parentUrl = parent.Tabbar.tabs(currentTabId).getFrame().getAttribute('src');
                        $(location).attr('href', parentUrl);
                        //기본 로그인 페이지
                    } else {
                        $(location).attr('href', res.data);
                    }
                }
            },
            errorCallbackFunc: function (resError) {
                displayReturnMessage($.jUtil.nvl(resError.responseJSON.data.partnerId, "")
                    , $.jUtil.nvl(resError.responseJSON.data.partnerPw, "")
                    , resError.responseJSON.returnMessage);
                if (resError.responseJSON.returnCode == "1014") {
                    $ui.modalOpen('loginConfirmPop', function(){$('#partnerIdPop').val($('#partnerId').val())});
                } else if (resError.responseJSON.returnCode == "1019") {
                    $ui.modalOpen('idActivatePop');
                }

            }

        });
    }

    /**
     * 입력창 에러 클래스 적용 리셋처리
     */
    function resetInputErrorClass() {
        $("#partnerId").removeClass("err");
        $("#partnerPw").removeClass("err");
    }

    /**
     * 로그인 유효성 체크
     */
    function invalid(partnerId, partnerPw) {
        const valid_txt = $("#valid_txt");

        if ($.jUtil.isEmpty(partnerId.val()) && $.jUtil.isEmpty(partnerPw.val())) {
            partnerId.addClass("err");
            partnerId.focus();

            valid_txt.text("아이디, 비밀번호를 입력하세요.");
            valid_txt.show();
            return true;
        }

        if ($.jUtil.isEmpty(partnerId.val())) {
            partnerId.addClass("err");
            partnerId.focus();

            valid_txt.text("아이디를 입력하세요.");
            valid_txt.show();
            return true;
        }
        if (partnerId.val().length < 5 || partnerId.val().length > 12) {
            partnerId.addClass("err");
            partnerId.focus();

            valid_txt.text("아이디는 5에서 12자리 이내로 입력해주세요.");
            valid_txt.show();
            return true;
        }

        if ($.jUtil.isEmpty(partnerPw.val())) {
            partnerPw.addClass("err");
            partnerPw.focus();

            valid_txt.text("비밀번호를 입력하세요.");
            valid_txt.show();
            return true;
        }
        return false;
    }

    /**
     * 예외 발생시 기존 입력한 로그인 아이디 설정
     */
    function setUserIdByFailed(partnerId, partnerPw) {
        if ($.jUtil.isNotEmpty(partnerId)) {
            $("#partnerId").val(partnerId);
        }

        if ($.jUtil.isNotEmpty(partnerPw)) {
            $("#partnerPw").val(partnerPw);
        }
    }

    function setErrorAddClass(returnMessage) {
        const partnerId = $("#partnerId");
        const partnerPw = $("#partnerPw");

        if (returnMessage ==='아이디가 존재하지 않습니다.') {
            partnerId.addClass("err");
            partnerId.focus();
        } else if (returnMessage === '비밀번호를 확인해주세요.') {
            partnerPw.addClass("err");
            partnerPw.focus();
        } else {
            partnerId.addClass("err");
            partnerId.focus();
        }
    }

</script>

<div class="login-wrap">
    <h1><img src="/static/images/logo2.png" alt="Homeplus 파트너센터"></h1>
    <div class="login-box">
        <h2>로그인</h2>

        <form id="loginForm" name="loginForm" method="POST" onsubmit="return false;">
            <div class="inp-login">
                <div class="user-id">
                    <input type="text" id="partnerId" name="partnerId" class="inp-base-l"
                           placeholder="아이디를 입력하세요" title="아이디" maxlength="12"
                           value="${savePartnerId}">
                </div>
                <div class="user-pw">
                    <input type="password" id="partnerPw" name="partnerPw"
                           class="inp-base-l" title="비밀번호" maxlength="16"
                           placeholder="비밀번호를 입력하세요" value="" autocomplete="off">
                </div>
            </div>
            <p class="login-chk">
                <input type="checkbox" id="chkSavePartnerId" name="chkSavePartnerId"
                    <c:if test="${chkSavePartnerId eq true}">
                           checked="checked"
                    </c:if>
                >
                <label for="chkSavePartnerId"  class="lb-check">아이디 저장</label>
            </p>
            <div class="btn-wrap">
                <button type="button" class="btn-base-l type4" name="loginBtn" id="loginBtn" onclick="step1Login();"><i>로그인</i></button>
            </div>
            <p class="txt-err mgt10" id="valid_txt" style="display: none;">입력하신 아이디, 비밀번호를 다시 확인해 주세요.</p>
        </form>
        <div class="login-link">
            <span><a href="/login/getUserId">아이디 찾기</a></span>
            <span><a href="/login/getUserPw">비밀번호 찾기</a></span>
            <%--<span><a href="/join/step1" class="fc01">회원가입</a></span>--%>
        </div>
    </div>
</div>

<div class="modal-wrap" style="display: none;">
    <jsp:include page="/WEB-INF/views/pages/user/pop/loginConfirmPop.jsp" />

    <%--2단계 로그인 레이어팝업--%>
    <jsp:include page="/WEB-INF/views/pages/core/login/step2LoginModal.jsp" />

    <jsp:include page="/WEB-INF/views/pages/user/pop/idActivatePop.jsp" />

</div>

<%--footer--%>
<div class="footer-wrap">
    <jsp:include page="/WEB-INF/views/pages/core/includeFooter.jsp" />
</div>