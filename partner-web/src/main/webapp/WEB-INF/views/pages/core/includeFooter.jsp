<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<footer id="footer" class="footer">
    <div class="link-group">
        <div>
            <div class="footer-link">
                <a href="http://corporate.homeplus.co.kr/index.aspx" target="_blank">회사소개</a>
                <a href="javascript:$ui.popOpen('basic','/terms?type=basic','700','800')" >이용약관</a>
                <a href="https://front.homeplus.co.kr/terms?type=personal" target="_blank" style="font-weight: bold; color: red;">개인정보처리방침</a>
                <a href="javascript:$ui.popOpen('contact','/terms?type=contact','470','370')">입점&middot;제휴문의</a>
            </div>
        </div>
    </div>
    <div class="footer-info">
        <div>
            <h2>홈플러스 주식회사</h2>
            <ul>
                <li>대표이사 : 이제훈</li> <!-- 210202 수정 -->
                <li>주소 : 서울특별시 강서구 화곡로 398(등촌동)</li>
                <li>사업자등록번호 : 220-81-60348 <a
                        href="javascript:$ui.popOpen('bizCommPop',
                        'https://www.ftc.go.kr/bizCommPop.do?wrkr_no=2208160348&apv_perm_no=','840','860');"
                        class="btn-txt">사업자정보확인</a></li>
                <li>통신판매업신고번호 : 2016-서울강서-0451호</li>
                <li>개인정보 및 청소년 보호책임자 : 김영상</li>
            </ul>
        </div>
    </div>
</footer>