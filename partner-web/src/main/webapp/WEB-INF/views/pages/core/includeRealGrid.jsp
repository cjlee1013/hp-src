<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- 리얼 그리드 사용시에 필요한 js 파일을 로드합니다 (호출 순서 중요) -->
<script type="text/javascript" src="${realGridLicensePath}?v=20200130"></script>
<script type="text/javascript" src="${realGridMainJsPath}"></script>
<script type="text/javascript" src="${realGridApiJsPath}"></script>
<script type="text/javascript" src="${realGridJsZipPath}"></script>

<script>
    RealGridJS.setRootContext("${realGridRootPath}");
</script>
