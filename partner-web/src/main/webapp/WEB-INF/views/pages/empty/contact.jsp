<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<link rel="stylesheet" href="/static/css/ui.base.css">
<link rel="stylesheet" href="/static/css/jquery-ui.min.css">
<script src="/static/js/ui.common.js"></script>

<div class="wrap2">
    <div class="wrap-box2">
        <h2 class="h2">입점&middot;제휴 문의</h2>
        <p class="mgt25">
            <span class="fc03">홈플러스 온라인 쇼핑과 함께 성공하시는데 도움이 되고자 합니다.</span> <br>
            <strong class="fc01">광고/제휴에 대해 궁금하신 점은 아래 연락처로 문의 부탁드립니다.</strong>
        </p>
        <div class="tbl-data mgt30">
            <table>
                <caption>입점.제휴문의</caption>
                <colgroup>
                    <col style="width: 180px">
                    <col style="width: auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">제휴문의</th>
                    <td><a href="mailto:homepl@homeplus.co.kr" class="link">homepl@homeplus.co.kr</a></td>
                </tr>
                <tr>
                    <th scope="row">온라인 외부광고 제안</th>
                    <td><a href="mailto:homepl@homeplus.co.kr" class="link">homepl@homeplus.co.kr</a></td>
                </tr>
                <tr>
                    <th scope="row">입점문의</th>
                    <td>
                        <a href="mailto:helppartner@homeplus.co.kr" class="link">helppartner@homeplus.co.kr</a> <br>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>