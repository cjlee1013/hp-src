<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<link rel="stylesheet" href="/static/css/ui.base.css">
<link rel="stylesheet" href="/static/css/jquery-ui.min.css">
<script src="/static/js/ui.common.js"></script>

<div class="wrap2">
    <h1><img src="/static/images/logo2.png" alt="Homeplus 파트너센터"></h1>
    <div class="wrap-box mgt40">
        <div class="term-tit">
            <h2 class="h2">개인정보 처리방침</h2>
            <!-- <div class="term-slc">
                <span>이전 개인정보처리방침 보기</span>
                <select class="slc-base" title="이전 개인정보처리방침 보기">
                    <option value="">시행일 2019-12-01 v1.0</option>
                    <option value="">시행일 2019-09-05 v0.1</option>
                </select>
            </div> -->
        </div>

        <div class="term-box mgt20">
            <!-- 210111 약관 업데이트 -->
            <h3>판매자 개인정보 수집•이용 동의</h3>
            <div class="term-section">
                <p class="term-txt">홈플러스 주식회사(이하 “회사”라 합니다)는 최소한의 정보만을 필요한 시점에 수집하며, 수집하는 정보는 고지한 범위 내에서만 사용되며, 사전 동의 없이 그 범위를 초과하여 이용하거나 외부에 공개하지 않습니다.</p>
                <div class="term-tbl mgt15">
                    <table>
                        <caption>판매자 개인정보 수집•이용 동의</caption>
                        <colgroup>
                            <col width="42%">
                            <col width="42%">
                            <col width="16%">
                        </colgroup>
                        <thead>
                        <tr>
                            <th scope="col">수집/활용목적</th>
                            <th scope="col">수집/활용항목</th>
                            <th scope="col">보유/이용기간</th>
                        </tr>
                        </thead>
                        <tbody>
                        <!-- <tr>
                            <th scope="row" rowspan="8">구매회원<br>/판매업체</th>
                            <td>본인여부 확인,SSO연동</td>
                            <td>이름, 아이디, 비밀번호, 휴대폰번호, 이메일주소,<br>비밀번호 찾기 힌트(질문/답변),  외국인등록번호(외국인인 경우)</td>
                        </tr> -->
                        <tr>
                            <td>이용자식별, 계약이행을 위한 연락, 서비스 이용에 따른 정보제공(고지사항 전달, 본인의사 확인, 서비스 관련 상담, 민원사항 처리, 유의사항 등),</td>
                            <td>성명, 주소, 휴대폰번호, 비밀번호, 이메일주소</td>
                            <td rowspan="3">회원탈퇴 후 15일 이내 또는 법령에 따른 보존기간 (단, 부정거래 확인 시 회원탈퇴 후 6개월)</td>
                        </tr>
                        <tr>
                            <td>입점 상담/신청 및 입점 관련 서비스, 상품 세금계산서 발행, 중복가입방지, 부정방지</td>
                            <td>성명, 아이디(ID), 이메일주소, 주소, 휴대전화번호</td>
                        </tr>
                        <tr>
                            <td>정산, 대금결제서비스의 제공</td>
                            <td>계좌정보 (은행선택, 계좌번호입력)
                                예금주 (계좌인증), 업태, 종목, 통신판매신고번호
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <p class="inline">귀하는 회사의 개인정보 수집•이용에 대한 동의를 거부할 권리가 있습니다. 그러나 동의를 거부할 경우 원활한 입점 및 판매 서비스 이용 제한 등의 불이익이 발생하게 됩니다.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer2"> <!-- 210111 수정 -->
    Copyright ⓒ Homeplus Co., LTD all rights reserved.
</footer>
