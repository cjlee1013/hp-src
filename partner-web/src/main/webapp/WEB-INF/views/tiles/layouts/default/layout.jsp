<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html lang="ko">
<head>
	<title><tiles:insertAttribute name="title" /></title>
	<tiles:insertAttribute name="header" />
</head>
<body style="overflow-y:hidden;overflow-x:hidden;">


<!-- 전체 레이아웃  -->
<div class="wrap">
	<!-- 사이드 메뉴 시작 -->
		<tiles:insertAttribute name="menu" />
	<!-- 사이드 메뉴 끝 -->

	<!-- 우측 컨텐츠 시작 -->
	<div class="wrap-in">

		<tiles:insertAttribute name="bodytab" />
		<tiles:insertAttribute name="body" />

	</div>
	<%--footer--%>
	<tiles:insertAttribute name="footer" />
	<!-- 우측 컨텐츠 끝  -->
</div>
<!-- 전체 레이아웃 끝 -->
</body>
</html>