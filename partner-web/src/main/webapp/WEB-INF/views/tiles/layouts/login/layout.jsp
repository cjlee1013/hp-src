<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html lang="ko">
<head>
	<title><tiles:insertAttribute name="title" /></title>
	<tiles:insertAttribute name="header" />
</head>
<body>
	<tiles:insertAttribute name="body" />

	<%--<tiles:insertAttribute name="footer" />--%>
</body>
</html>
