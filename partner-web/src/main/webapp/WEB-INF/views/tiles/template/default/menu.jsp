<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ page import="kr.co.homeplus.partner.web.core.dto.UserInfo" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<jsp:include page="/WEB-INF/views/pages/core/includeDhtmlx.jsp"/>

<%
	UserInfo userInfo = new UserInfo();
	if (request.getAttribute("userInfo") != null) {
		userInfo = (UserInfo) request.getAttribute("userInfo");
	}
%>

<!-- 팝업 제어 스크립트 -->
<script type="text/javascript">
	function dEI(elementID){
		return document.getElementById(elementID);
	}
	function openLayer(IdName , tpos, lpos){
		var pop = dEI(IdName);
		pop.style.display = "block";
		pop.style.top =  tpos + "px";
		pop.style.left = lpos + "px";

		var wrap = dEI("dimmed");
		var reservation = document.createElement("div");
		reservation.setAttribute("id", "outage");
		wrap.style.zIndex = "2";
		wrap.appendChild(reservation);
	}
	function closeLayer( IdName ){
		var pop = dEI(IdName);
		pop.style.display = "none";
		var clearEl = parent.dEI("outage");
		var momEl = parent.dEI("dimmed");
		momEl.style.zIndex = "0";
		momEl.removeChild(clearEl);
	}
</script>

<header class="header">
	<h1>
		<a href="/"><img src="/static/images/logo.png" alt="Homeplus 파트너센터"></a>
	</h1>
	<div class="user-info">
		<span><img src="/static/images/ico_user.png" alt="<%=userInfo.getBusinessNm()%>님"><i><%=userInfo.getBusinessNm()%> 님 안녕하세요.</i></span>
		<a href="#" onclick="initialTab.addTab('Tab_userVerify', '회원정보 조회/수정', '/user/verify');">회원정보수정</a>
		<a href="/logout">로그아웃</a>
	</div>
</header>

<%-- lnb --%>
<div id="lnb" class="lnb-wrap">
	<%--홈--%>
	<section id="navModal" class="nav-modal" role="dialog" aria-modal="true"></section>
	<nav class="nav">
		<ul id="uiNav">
		<%--1depth--%>
		<c:forEach var="mainMenus" items="${menus.data}" varStatus="mainStatus">
		<c:if test="${mainMenus.menuView}">
			<li>
				<dl class="acco">
					<dt class="acco-tit depth1 ico0${mainStatus.count}"><button type="button" class="acco-btn">${mainMenus.mainMenuNm}</button></dt>
					<dd class="acco-pnl">
						<ul class="depth2">
						<%--depth2--%>
						<c:forEach var="subMenus" items="${mainMenus.subMenus}" varStatus="subStatus">
							<c:if test="${subMenus.menuView}">
								<li id="${subMenus.tabId}">
								<a href="javascript:void(0);"
									   onclick="initialTab.addTab('${subMenus.tabId}', '${subMenus.menuNm}', '${subMenus.link}');">${subMenus.menuNm}</a>
								</li>
							</c:if>
						</c:forEach>
						</ul>
					</dd>
				</dl>
			</li>
		</c:if>
		</c:forEach>
		</ul>
	</nav>
</div>