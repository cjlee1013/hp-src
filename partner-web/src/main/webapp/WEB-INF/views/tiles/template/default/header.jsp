<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String fileVersion = (String)request.getAttribute("fileVersion");
%>
<meta http-equiv="Expires" content="-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="No-Cache">
<link rel='stylesheet' type='text/css' href='/static/css/font-awesome.min.css?v=${fileVersion}'>
<link rel='stylesheet' type='text/css' href='/static/css/global.css?v=${fileVersion}'>
<link rel='stylesheet' type='text/css' href='/static/css/common.css?v=${fileVersion}'>
<link rel='stylesheet' type='text/css' href='/static/css/ui-kit.css?v=${fileVersion}'>
<link rel='stylesheet' type='text/css' href='/static/css/ui.base.css?v=${fileVersion}'>

<link rel='stylesheet' type='text/css' href='/static/jquery-ui/jquery-ui.min.css'>

<link rel='stylesheet' type='text/css' href='/static/css/jquery-ui-timepicker-addon.css'>

<link rel="shortcut icon" href="/static/images/favicon.ico" />

<!-- js -->
<script type="text/javascript" src="/static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="/static/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/static/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/static/js/core/jquery.blockUI.js"></script>
<script type="text/javascript" src="/static/js/jquery.homeplus.jutil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.homeplus.accordion.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/core/common.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/core/commonAjax.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.bxslider.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/idangerous.swiper.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/ui.common.js?v=${fileVersion}"></script>




<!-- date common util -->
<script type="text/javascript" src="/static/js/core/moment.min.js"></script>

<!-- js calendar -->
<script type="text/javascript" src="/static/js/core/calendar.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/core/calendarTime.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery-ui.datepicker.min.js"></script>

<!-- datetimepicker addon -->
<script type="text/javascript" src="/static/js/core/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/static/js/core/jquery-ui-timepicker-addon-i18n.min.js"></script>

<!-- browser check-->
<script type="text/javascript" src="/static/js/core/checkBrowser.js?v=${fileVersion}"></script>

