package kr.co.homeplus.shippingbatch.tracking.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class ShippingRequestSetDto {
    @ApiModelProperty(notes = "배송번호")
    private String shipNo;
}
