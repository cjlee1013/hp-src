package kr.co.homeplus.shippingbatch.point.model.mileage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "외부연동 > 회원등급 마일리지 배송완료")
public class ShippingAccumulateDto extends AccumulateDto{

  @ApiModelProperty(value = "배송완료시 적립대상금액", position = 17)
  private long shippingAccTargetAmt;

  @ApiModelProperty(value = "배송완료시 총주문금액", position = 18)
  private long shippingTotOrderAmt;

}
