package kr.co.homeplus.shippingbatch.tracking.mapper;

import java.util.List;
import kr.co.homeplus.shippingbatch.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shippingbatch.tracking.model.BsdInvoiceGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.BsdRequestSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.ExchRequestSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.MultiShippingRequestSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.PickRequestSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.ShippingRequestSetDto;

@SlaveConnection
public interface TrackingSlaveMapper {
  List<ShippingRequestSetDto> getShippingTracking();
  List<PickRequestSetDto> getPickTracking();
  List<ExchRequestSetDto> getExchTracking();
  List<PickRequestSetDto> getPickRequest();
  List<MultiShippingRequestSetDto> getMultiShippingTracking();
  List<BsdRequestSetDto> getBsdTracking();
  List<BsdInvoiceGetDto> getShippingBsdInvoice();
  List<BsdInvoiceGetDto> getMultiOrderBsdInvoice();
}
