package kr.co.homeplus.shippingbatch.tracking.mapper;

import java.util.List;
import kr.co.homeplus.shippingbatch.core.db.annotation.MasterConnection;
import kr.co.homeplus.shippingbatch.linkcompany.model.LinkCompanyDto;
import org.apache.ibatis.annotations.Param;

@MasterConnection
public interface TrackingMasterMapper {
    int setShippingCompleteDLV();
    int setShippingCompleteDRCT();
    int setPurchaseCompleteDS();
    int setPurchaseCompleteTD();
    int setBundleComplete();
    int setPurchaseOrderFinish();
    int setMultiComplete();
    int setMultiPurchaseComplete();
    int setShippingCompleteMulti();
    int setBsdUpload();
    int setBsdDownload();
    int setShippingCompleteByHistory();
    int setMultiCompleteByHistory();
}
