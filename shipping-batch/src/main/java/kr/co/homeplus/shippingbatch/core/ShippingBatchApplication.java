package kr.co.homeplus.shippingbatch.core;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ExitCodeEvent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;

@Slf4j
@SpringBootApplication
@ComponentScan({"kr.co.homeplus", "kr.co.homeplus.plus"})
public class ShippingBatchApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(
            ShippingBatchApplication.class, args);

        log.debug("Closing Application Context");
        int exitCode = SpringApplication.exit(applicationContext);
        if(exitCode != 0) {
            log.error("error exit code: " +  exitCode);
        }
        System.exit(exitCode);
    }

    @Bean
    public ExitCodeEventModel exitCodeEventModelIntance() {
        return new ExitCodeEventModel();
    }

    private static class ExitCodeEventModel {

        public ExitCodeEventModel() {
            log.debug("Instantiating ExitCodeEventModel object");
        }

        @EventListener
        public void exitCodeEvent(ExitCodeEvent event) {
            log.debug("*************************************");
            log.debug("exit code: " + event.getExitCode());
            log.debug("*************************************");
        }
    }
}
