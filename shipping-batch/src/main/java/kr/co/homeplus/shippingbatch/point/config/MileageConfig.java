package kr.co.homeplus.shippingbatch.point.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
@Setter
public class MileageConfig {
  private String hyper;
  private String club;
  private String express;
  private String ds;

  @Bean
  @ConfigurationProperties("mileagecode.promo")
  public MileageConfig promoProperties() {
    return new MileageConfig();
  }
  @Bean
  @ConfigurationProperties("mileagecode.payback")
  public MileageConfig paybackProperties() {
    return new MileageConfig();
  }
  @Bean
  @ConfigurationProperties("mileagecode.accumulate")
  public MileageConfig accumulateProperties() {
    return new MileageConfig();
  }
}
