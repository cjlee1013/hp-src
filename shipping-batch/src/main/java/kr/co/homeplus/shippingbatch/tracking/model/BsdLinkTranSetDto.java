package kr.co.homeplus.shippingbatch.tracking.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > BSD연동조회 리스트 요청 DTO")
public class BsdLinkTranSetDto {
    @ApiModelProperty(notes = "BSD일련번호리스트")
    private List<String> bsdLinkNoList;

    public BsdLinkTranSetDto() {
        this.bsdLinkNoList = new ArrayList<>();
    }
}
