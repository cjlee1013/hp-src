package kr.co.homeplus.shippingbatch.safetyissue.mapper;

import java.util.List;
import kr.co.homeplus.shippingbatch.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shippingbatch.safetyissue.model.MultiSafetyCancelRequest;
import kr.co.homeplus.shippingbatch.safetyissue.model.SafetyCancelRequest;

@SlaveConnection
public interface SafetyissueSlaveMapper {
    List<SafetyCancelRequest> getSafetyCancelList(String basicDt);
    List<MultiSafetyCancelRequest> getSafetyMultiCancelList(String basicDt);
}
