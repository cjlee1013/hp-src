package kr.co.homeplus.shippingbatch.linkcompany.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.shippingbatch.history.model.BatchHistorySetDto;
import kr.co.homeplus.shippingbatch.history.service.HistoryService;
import kr.co.homeplus.shippingbatch.linkcompany.mapper.LinkCompanyMasterMapper;
import kr.co.homeplus.shippingbatch.linkcompany.mapper.LinkCompanySlaveMapper;
import kr.co.homeplus.shippingbatch.linkcompany.model.LinkCompanyDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class LinkCompanyService {

    @Value("${sweettracker.url.company-code}")
    private String SWEETTRACKER_URL_COMPANY_CODE;

    private final LinkCompanyMasterMapper linkCompanyMasterMapper;
    private final LinkCompanySlaveMapper linkCompanySlaveMapper;

    private final HistoryService historyService;

    /**
     * 연동업체코드 Batch
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateLinkCode(BatchHistorySetDto batchHistorySetDto) throws Exception {
        String company = "";

        // 스윗트래커 갱신 시작
        company = "SWT";

        // 데이터 삭제
        linkCompanyMasterMapper.deleteLinkCode(company);

        // 데이터 조회
        List<LinkCompanyDto> linkCompanyDtoList = this.getSweetTrackerCode();
        if(linkCompanyDtoList.isEmpty()) {
            throw new Exception("getSweetTrackerCode function Error");
        }

        // 데이터 저장
        linkCompanyMasterMapper.insertLinkCode(linkCompanyDtoList, company);

        batchHistorySetDto.setCount(linkCompanyDtoList.size(),0,linkCompanyDtoList.size());
        historyService.setBatchHistory(batchHistorySetDto);
    }

    /**
     * 스윗트래커 연동코드 리스트 조회
     */
    private List<LinkCompanyDto> getSweetTrackerCode() {

        List<LinkCompanyDto> codeList = new ArrayList<>();

        try {
            URL url = new URL(SWEETTRACKER_URL_COMPANY_CODE);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            // 응답 읽기
            BufferedReader br = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8"));

            String result = "";
            String line;
            while ((line = br.readLine()) != null) {
                result = result + line.trim();
            }

            // Json -> List<LinkCompanyDto>
            Gson gson = new Gson();
            Map<String, Object> jsonObject = gson.fromJson(result, new TypeToken<Map<String, Object>>(){}.getType());
            codeList = (List<LinkCompanyDto>) jsonObject.get("Company");

            return codeList;
        } catch (Exception e) {
            log.error(e.getMessage());
            return codeList;
        }
    }
}
