package kr.co.homeplus.shippingbatch.market.mapper;

import kr.co.homeplus.shippingbatch.core.db.annotation.MasterConnection;
import kr.co.homeplus.shippingbatch.market.model.NaverInquiryGetDto;

@MasterConnection
public interface NaverInquiryMasterMapper {
  int insertNaverInquiry(NaverInquiryGetDto naverInquiryGetDto);
}
