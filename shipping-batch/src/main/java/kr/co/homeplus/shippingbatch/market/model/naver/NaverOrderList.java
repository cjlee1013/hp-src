package kr.co.homeplus.shippingbatch.market.model.naver;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NaverOrderList {
    private boolean hasMoreData;
    private String fromDt;
    private List<Order> orderList;
}
