package kr.co.homeplus.shippingbatch.market.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마켓연동 주문번호")
public class MarketOrderItemGetDto {
  @JsonInclude(JsonInclude.Include.NON_NULL)
  @ApiModelProperty(notes = "마켓 종류", required = true)
  private String marketType;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @ApiModelProperty(notes = "마켓주문번호")
  private String marketOrderNo;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @ApiModelProperty(notes = "마켓상품주문번호")
  private String marketOrderItemNo;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @ApiModelProperty(notes = "마켓배송번호")
  private String dlvNo;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @ApiModelProperty(notes = "리핏주문번호")
  private String purchaseOrderNo;

}
