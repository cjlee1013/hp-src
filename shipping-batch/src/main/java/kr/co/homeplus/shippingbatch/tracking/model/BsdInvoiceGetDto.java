package kr.co.homeplus.shippingbatch.tracking.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "BSD 송장 정보")
public class BsdInvoiceGetDto {
  @ApiModelProperty(notes = "배송번호")
  private String bundleNo;
}