package kr.co.homeplus.shippingbatch.history.mapper;

import kr.co.homeplus.shippingbatch.core.db.annotation.MasterConnection;
import kr.co.homeplus.shippingbatch.history.model.BatchHistorySetDto;

@MasterConnection
public interface HistoryMasterMapper {
    int setBatchHistory(BatchHistorySetDto batchHistorySetDto);
}
