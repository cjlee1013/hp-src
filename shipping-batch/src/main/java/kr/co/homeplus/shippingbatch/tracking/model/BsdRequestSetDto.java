package kr.co.homeplus.shippingbatch.tracking.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "BSD 배송추적 파라미터")
public class BsdRequestSetDto {
    @ApiModelProperty(notes = "BSD 일련번호")
    private String bsdLinkNo;
}
