package kr.co.homeplus.shippingbatch.tracking.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "BSD 배송추적 파라미터")
public class BsdRequestGetDto {
    @ApiModelProperty(notes = "BSD 일련번호")
    private String bsdLinkNo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "성공여부")
    private String success;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "결과값")
    private String detail;

    public String getBsdLinkNo() {
        return bsdLinkNo;
    }

    public String getSuccess() {
        return success;
    }

    public String getDetail() {
        return detail;
    }
}
