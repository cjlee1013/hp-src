package kr.co.homeplus.shippingbatch.market.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@ApiModel(description = "구매확정")
public class ShipDecisionMarketSetDto {
    @ApiModelProperty(notes = "상품주문번호")
    private String orderItemNo;
}
