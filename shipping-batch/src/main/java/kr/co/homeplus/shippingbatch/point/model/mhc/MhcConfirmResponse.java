package kr.co.homeplus.shippingbatch.point.model.mhc;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value="mhcConfirmResponse", description = "결제정보(MHC 포인트 가용화 결과)")
@Getter
@Setter
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class MhcConfirmResponse {
    @ApiModelProperty(notes = "결과코드", required = true, position = 1)
    private String returnCode = "0000";

    @ApiModelProperty(notes = "결과메시지", required = true, position = 2)
    private String returnMessage = "정상";

    @ApiModelProperty(notes = "처리실패데이터목록(실패건만 리턴됨)", position = 3)
    private ArrayList<MhcApiConfirmResponse> failDataList;

    public ArrayList<MhcApiConfirmResponse> getFailDataList() {
        return failDataList;
    }
}
