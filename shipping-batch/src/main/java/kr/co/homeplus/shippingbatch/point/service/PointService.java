package kr.co.homeplus.shippingbatch.point.service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shippingbatch.history.model.BatchHistorySetDto;
import kr.co.homeplus.shippingbatch.history.service.HistoryService;
import kr.co.homeplus.shippingbatch.point.config.MileageConfig;
import kr.co.homeplus.shippingbatch.point.mapper.PointMasterMapper;
import kr.co.homeplus.shippingbatch.point.mapper.PointSlaveMapper;
import kr.co.homeplus.shippingbatch.point.model.mhc.MhcApiConfirmResponse;
import kr.co.homeplus.shippingbatch.point.model.mhc.MhcConfirm;
import kr.co.homeplus.shippingbatch.point.model.mhc.MhcConfirmResponse;
import kr.co.homeplus.shippingbatch.point.model.mileage.ClaimAccumulateDto;
import kr.co.homeplus.shippingbatch.point.model.mileage.FrontUserMyGradeInfoDto;
import kr.co.homeplus.shippingbatch.point.model.mileage.MileageDto;
import kr.co.homeplus.shippingbatch.point.model.mileage.MileagePaybackDto;
import kr.co.homeplus.shippingbatch.point.model.mileage.ShippingAccumulateDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PointService {
  private final PointMasterMapper pointMasterMapper;
  private final PointSlaveMapper pointSlaveMapper;
  private final HistoryService historyService;

  private final MileageConfig mileageConfig;

  @Autowired
  ResourceClient resourceClient;

  public void setPromoMileageSave(BatchHistorySetDto batchHistorySetDto, String basicDt) {
    if (basicDt == null) {
        basicDt = getMinusDay(8);
    }
    List<MileageDto> mileageDtoList = pointSlaveMapper.getPromoMileageSave(basicDt);
    StringBuilder errorMsg = new StringBuilder();
    errorMsg.append(basicDt).append("\n");
    int successCnt=0, failCnt=0;
    ResponseObject<String> responseObject;
    for (MileageDto mileageDto : mileageDtoList) {
      errorMsg.append(mileageDto.getTradeNo()).append(":");
      try {
        responseObject = resourceClient.postForResponseObject("mileage"
            , mileageDto, "/external/external/mileageSave"
            , new ParameterizedTypeReference<>() {
            });
        if ("0000".equals(responseObject.getReturnCode())) {
          successCnt++;
        } else {
          failCnt++;
          errorMsg.append(responseObject.getData()).append("\n");
        }
      } catch (Exception e) {
        errorMsg.append("API 연결 오류");
      }
    }
    batchHistorySetDto.setCount(successCnt,failCnt, mileageDtoList.size());
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setPaybackMileageSave(BatchHistorySetDto batchHistorySetDto, String basicDt) {
    if (basicDt == null) {
      basicDt = getMinusDay(8);
    }
    String mileageCode;
    List<MileagePaybackDto> mileageDtoList = pointSlaveMapper.getPaybackMileageSave(basicDt);
    StringBuilder errorMsg = new StringBuilder();
    errorMsg.append(basicDt).append("\n");
    int successCnt = 0, failCnt = 0;
    ResponseObject<String> responseObject;
    for (MileagePaybackDto mileageDto : mileageDtoList) {
      errorMsg.append(mileageDto.getTradeNo()).append(":");
      try {
        mileageCode = getMileageCode(mileageConfig.paybackProperties(),mileageDto.getStoreType());
        if (mileageCode != null) {
          mileageDto.setMileageCode(mileageCode);
          responseObject = resourceClient.postForResponseObject("mileage"
              , ((MileageDto) mileageDto), "/external/external/mileageSave"
              , new ParameterizedTypeReference<>() {
              });
          if ("0000".equals(responseObject.getReturnCode())) {
            successCnt++;
            pointMasterMapper.setPaybackResult(mileageDto.getPaybackNo());
          } else {
            failCnt++;
            errorMsg.append(responseObject.getData()).append("\n");
          }
        }
      } catch (Exception e) {
        errorMsg.append("API 연결 오류");
      }
    }
    batchHistorySetDto.setCount(successCnt, failCnt, mileageDtoList.size());
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setConfirmMhcPoint(BatchHistorySetDto batchHistorySetDto, String basicDt) {
    if (basicDt == null) {
      basicDt = getMinusDay(1);
    }
    List<MhcConfirm> mhcConfirmList = pointSlaveMapper.setConfirmMhcPoint(basicDt);
    StringBuilder errorMsg = new StringBuilder();
    errorMsg.append(basicDt).append("\n");
    String resultCode;
    int successCnt=0, failCnt=0, totalCnt = 0;
    MhcConfirmResponse mhcConfirmResponse;
    if (mhcConfirmList != null && mhcConfirmList.size() > 0) {
      totalCnt = mhcConfirmList.size();
      int requestCnt = 100;
      for (int i=0;i<totalCnt;i=i+100){
        if ((i+100) > totalCnt) {
          requestCnt = totalCnt - i;
        }
        try {
          mhcConfirmResponse = (resourceClient.postForResponseObject("escrow"
              , mhcConfirmList.subList(i,i+requestCnt), "/mhc/external/confirmMhcPoint"
              , new ParameterizedTypeReference<ResponseObject<MhcConfirmResponse>>() {
              })).getData();
          if (mhcConfirmResponse != null) {
            resultCode = mhcConfirmResponse.getReturnCode();
            if ("0000".equals(resultCode)) {
              successCnt += requestCnt;
            } else {
              errorMsg.append(resultCode).append(":");
              errorMsg.append(mhcConfirmResponse.getReturnMessage()).append("\n");

              List<MhcApiConfirmResponse> failDataList = mhcConfirmResponse.getFailDataList();
              for (MhcApiConfirmResponse failData : failDataList) {
                errorMsg.append(failData.getOrdNo()).append("\n");
              }
              failCnt += failDataList.size();
              successCnt += requestCnt - failDataList.size();
            }
          } else {
            errorMsg.append("API 응답 없음");
          }
        } catch (Exception e) {
          errorMsg.append("API 연결 오류");
        }
      }
    }
    batchHistorySetDto.setCount(successCnt,failCnt, totalCnt);
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setAccumulate(BatchHistorySetDto batchHistorySetDto,String basicDt) {
    if (basicDt == null) {
      basicDt = getMinusDay(1);
    }
    String mileageCode,userNo,storeType;
    StringBuilder errorMsg = new StringBuilder();
    errorMsg.append(basicDt).append("\n");
    int successCnt=0, failCnt=0, totalCnt = 0;
    long accTargetAmt,totOrderAmt,mileageAccAmt, claimAccTargetAmt,totClaimAmt;
    double mileageAccRate, orderAmtRate;
    FrontUserMyGradeInfoDto frontUserMyGradeInfoDto;
    List <ShippingAccumulateDto> accumulateDtoList = pointSlaveMapper.getPurchaseAccumulate(basicDt);
    ClaimAccumulateDto claimAccumulateDto;
    MileageDto mileageDto;
    ResponseObject<String> responseObject;
    for (ShippingAccumulateDto accumulateDto : accumulateDtoList) {
      try {
        userNo = accumulateDto.getUserNo();
        claimAccumulateDto = pointSlaveMapper.getClaimAccumulate(accumulateDto.getPurchaseOrderNo());
        accTargetAmt = accumulateDto.getAccTargetAmt();
        totOrderAmt = accumulateDto.getTotOrderAmt();
        if (claimAccumulateDto != null) {
          totClaimAmt = claimAccumulateDto.getTotClaimAmt();
          if (totOrderAmt == 0) {
            errorMsg.append(accumulateDto.getPurchaseOrderNo()).append(":totOrderAmt=0").append("\n");
            continue;
          }
          orderAmtRate = (double)accTargetAmt / (double)totOrderAmt;
          claimAccTargetAmt = (long)(orderAmtRate * (double)totClaimAmt);
          accTargetAmt = accTargetAmt - claimAccTargetAmt;
          totOrderAmt = totOrderAmt - totClaimAmt;
        }
        storeType = accumulateDto.getStoreType();
        if ("DS".equals(storeType)) {
          storeType = "HYPER";
        }
        mileageAccRate = accumulateDto.getMileageAccRate();
        mileageAccAmt = accumulateDto.getMileageAccAmt();
        if (mileageAccRate > 0) {
          mileageCode = getMileageCode(mileageConfig.accumulateProperties(),storeType) + (int)(mileageAccRate * 10);
          mileageAccAmt = (long) Math.floor((accTargetAmt * mileageAccRate) / 100);
          if (mileageAccAmt >= 10) {
            mileageDto = new MileageDto(
                mileageCode
                , storeType
                , "주문적립"
                , userNo
                , mileageAccAmt
                , accumulateDto.getPurchaseOrderNo()
                , "shipping"
                , "180"
            );

            responseObject = resourceClient.postForResponseObject("mileage"
                , (mileageDto), "/external/external/mileageSave"
                , new ParameterizedTypeReference<>() {
                });
            if ("0000".equals(responseObject.getReturnCode())) {
              successCnt++;
              accumulateDto.setMileageAccRate(mileageAccRate);
              accumulateDto.setMileageAccAmt(mileageAccAmt);
              pointMasterMapper.setShippingAccumulate(accumulateDto);
            } else {
              failCnt++;
              errorMsg.append(accumulateDto.getPurchaseOrderNo())
                  .append(":")
                  .append(responseObject.getReturnCode())
                  .append("\n");
            }
          }
        }
      } catch (Exception e) {
        errorMsg.append(accumulateDto.getPurchaseOrderNo()).append(":").append("API오류").append("\n");
      }
    }
    batchHistorySetDto.setCount(successCnt, failCnt, accumulateDtoList.size());
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  private String getMileageCode(MileageConfig mileageConfig,String storeType) {
    switch (storeType) {
      case "HYPER":
        return mileageConfig.getHyper();
      case "CLUB":
        return mileageConfig.getClub();
      case "EXP":
        return mileageConfig.getExpress();
    }
    return null;
  }

  private String getMinusDay(int days) {
    return LocalDateTime.ofInstant(Instant.now().minus(days, ChronoUnit.DAYS), ZoneId.systemDefault())
        .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
  }

  public void setAnytimeMileageSave(BatchHistorySetDto batchHistorySetDto, String basicDt) {
    if (basicDt == null) {
      basicDt = getMinusDay(1);
    }
    List<MileageDto> mileageDtoList = pointSlaveMapper.getAnytimeMileageSave(basicDt);
    int mileageAmt = pointSlaveMapper.getMileageSaveAmount("PAYBACK_ANYTIME");
    StringBuilder errorMsg = new StringBuilder();
    errorMsg.append(basicDt).append("\n");
    int successCnt=0, failCnt=0;
    ResponseObject<String> responseObject;
    for (MileageDto mileageDto : mileageDtoList) {
      mileageDto.setMileageAmt(mileageAmt);
      errorMsg.append(mileageDto.getTradeNo()).append(":");
      try {
        responseObject = resourceClient.postForResponseObject("mileage"
            , mileageDto, "/external/external/mileageSave"
            , new ParameterizedTypeReference<>() {
            });
        if ("0000".equals(responseObject.getReturnCode())) {
          successCnt++;
        } else {
          failCnt++;
          errorMsg.append(responseObject.getData()).append("\n");
        }
      } catch (Exception e) {
        errorMsg.append("API 연결 오류");
      }
    }
    batchHistorySetDto.setCount(successCnt,failCnt, mileageDtoList.size());
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }
}
