package kr.co.homeplus.shippingbatch.point.mapper;

import kr.co.homeplus.shippingbatch.core.db.annotation.MasterConnection;
import kr.co.homeplus.shippingbatch.point.model.mileage.AccumulateDto;

@MasterConnection
public interface PointMasterMapper {
    int setPaybackResult(String paybackNo);
    int setShippingAccumulate(AccumulateDto accumulateDto);
}
