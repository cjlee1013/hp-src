package kr.co.homeplus.shippingbatch.linkcompany.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배치 - 연동업체코드")
public class LinkCompanyDto {
    @ApiModelProperty(notes = "코드")
    private String Code;

    @ApiModelProperty(notes = "코드명")
    private String Name;
}
