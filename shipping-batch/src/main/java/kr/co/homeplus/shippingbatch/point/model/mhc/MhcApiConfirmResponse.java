package kr.co.homeplus.shippingbatch.point.model.mhc;

import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value="mhcApiConfirmResponse", description = "MHC API 포인트 확정 응답")
@Getter
@Setter
@EqualsAndHashCode
public class MhcApiConfirmResponse {
    private String avblMthdCd;
    private String aprvDt;
    private String aprvNo;
    private String ordNo;
}
