package kr.co.homeplus.shippingbatch.point.model.mileage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "외부연동 > 마일리지 적립")
public class MileageDto {
    @ApiModelProperty(value = "마일리지적립코드", position = 1)
    private String mileageCode;

    @ApiModelProperty(value = "상점타입", position = 2)
    private String storeType;

    @ApiModelProperty(value = "요청사유", position = 3)
    private String requestReason;

    @ApiModelProperty(value = "고객번호", position = 4)
    private String userNo;

    @ApiModelProperty(value = "마일리지금액", position = 5)
    private long mileageAmt;

    @ApiModelProperty(value = "거래번호", position = 6)
    private String tradeNo;

    @ApiModelProperty(value = "시스템명", position = 7)
    private String regId;

    @ApiModelProperty(value = "유효기간", position = 8)
    private String expireDay;

    public MileageDto(String mileageCode, String storeType, String requestReason,
        String userNo, long mileageAmt, String tradeNo, String regId, String expireDay) {
        this.mileageCode = mileageCode;
        this.storeType = storeType;
        this.requestReason = requestReason;
        this.userNo = userNo;
        this.mileageAmt = mileageAmt;
        this.tradeNo = tradeNo;
        this.regId = regId;
        this.expireDay = expireDay;
    }
}
