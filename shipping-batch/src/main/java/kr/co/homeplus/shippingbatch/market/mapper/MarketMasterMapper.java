package kr.co.homeplus.shippingbatch.market.mapper;

import java.util.List;
import kr.co.homeplus.shippingbatch.core.db.annotation.MasterConnection;
import kr.co.homeplus.shippingbatch.market.model.MarketShipSetDto;
import kr.co.homeplus.shippingbatch.market.model.ShipDecisionMarketSetDto;

@MasterConnection
public interface MarketMasterMapper {
    int setMarketShipDecision(MarketShipSetDto marketShipSetDto);
    int setShipDecisionMarket(ShipDecisionMarketSetDto shipDecisionMarketSetDto);
    List<ShipDecisionMarketSetDto> getShipDecisionMarket(MarketShipSetDto marketShipSetDto);
    int setMarketShipStart(MarketShipSetDto marketShipSetDto);
}
