package kr.co.homeplus.shippingbatch.market.mapper;

import kr.co.homeplus.shippingbatch.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shippingbatch.market.model.MarketOrderItemGetDto;
import kr.co.homeplus.shippingbatch.market.model.MarketShipSetDto;
import kr.co.homeplus.shippingbatch.market.model.ShipDecisionMarketSetDto;

import java.util.List;

@SlaveConnection
public interface MarketSlaveMapper {
    List<MarketOrderItemGetDto> getShippingMarket();
}
