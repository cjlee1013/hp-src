package kr.co.homeplus.shippingbatch.market.mapper;

import kr.co.homeplus.shippingbatch.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shippingbatch.market.model.NaverInquiryGetDto;

@SlaveConnection
public interface NaverInquirySlaveMapper {
  NaverInquiryGetDto selectSellerQnaOrderInfo(NaverInquiryGetDto naverInquiryGetDto);
}
