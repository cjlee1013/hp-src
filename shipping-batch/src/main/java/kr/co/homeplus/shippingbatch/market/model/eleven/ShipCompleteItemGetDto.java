package kr.co.homeplus.shippingbatch.market.model.eleven;

import lombok.Data;

@Data
public class ShipCompleteItemGetDto {
    //자동구매 확정여부
	//Y : 시스템 자동구매 확정(배송완료 후 7일, 미 배송완료 데이터 중 발송처리일 21일 후)
	//N : 사용자 자동구매 확정
    private String atmtBuyCnfrmYN;

    //배송비
    private String dlvCst;

    //회원번호
    private String memNo;

    // 주문총액 판매단가*수량(주문 -취소 -반품)+옵션가
	private String ordAmt;

    //11번가 주문번호
    private String ordNo;

    //주문순번
    private String ordPrdSeq;

    //수량
    private String ordQty;

    //구매확정일
    private String pocnfrmDt;

    //상품명
    private String prdNm;

    //판매자상품번호
    private String sellerPrdCd;

    //주문상품옵션명
    private String slctPrdOptNm;

    //전세계배송여부
    private String gblDlvYn;

    //주문일시
    private String ordDt;

    //11번가상품번호
    private String prdNo;

    //결제금액
    private String ordPayAmt;

    //원클릭체크아웃 주문코드
    private String referSeq;

    //판매자 재고번호
    private String sellerStockCd;

	//선물하기 여부
	private String sendGiftYn;
}
