package kr.co.homeplus.shippingbatch.shipping;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shippingbatch.history.model.BatchHistorySetDto;
import kr.co.homeplus.shippingbatch.history.service.HistoryService;
import kr.co.homeplus.shippingbatch.tracking.mapper.TrackingMasterMapper;
import kr.co.homeplus.shippingbatch.tracking.mapper.TrackingSlaveMapper;
import kr.co.homeplus.shippingbatch.tracking.model.BsdInvoiceGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.BsdLinkTranSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.BsdRequestGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.BsdRequestSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.ExchRequestGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.ExchRequestSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.MultiShippingRequestGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.MultiShippingRequestSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.PickRequestGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.PickRequestSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.ShippingRequestGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.ShippingRequestSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShippingMessageService {
  private final HistoryService historyService;

  @Autowired
  ResourceClient resourceClient;

  /**
   * 수취확인 요청 알림톡 발송
   */

  public void sendComfirmReceiveTalk(BatchHistorySetDto batchHistorySetDto){
    ResponseObject<Object> responseObject = new ResponseObject<>();
    String errorMsg ="";
    try {
      responseObject = resourceClient.postForResponseObject(
          "shipping",
          null,
          "/batch/shippingMessage/sendComfirmReceiveTalk",
          new ParameterizedTypeReference<ResponseObject<Object>>(){});

      errorMsg = responseObject.getReturnMessage();
    } catch (Exception e) {
      errorMsg = "API 연결 오류";
    }

    batchHistorySetDto.setCount(0,0,0);
    batchHistorySetDto.setErrorMessage(errorMsg);
    batchHistorySetDto.setMetaField1(responseObject.getData().toString());
    historyService.setBatchHistory(batchHistorySetDto);
  }

}
