package kr.co.homeplus.shippingbatch.point.model.mhc;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value="mhcConfirm", description = "결제정보(MHC 포인트 가용화)")
@Getter
@Setter
@EqualsAndHashCode
public class MhcConfirm {
    @ApiModelProperty(notes = "고객번호", required = true, position = 1)
    private long userNo;

    @ApiModelProperty(notes = "사이트유형(HOME:홈플러스, CLUB:더클럽)", required = true, position = 2)
    private String siteType;

    @ApiModelProperty(notes = "원거래구매주문번호", required = true, position = 3)
    private long orgPurchaseOrderNo;

    @ApiModelProperty(notes = "원거래승인일자", required = true, position = 4)
    private String orgMhcAprDate;

    @ApiModelProperty(notes = "원거래승인번호", required = true, position = 5)
    private String orgMhcAprNumber;
}
