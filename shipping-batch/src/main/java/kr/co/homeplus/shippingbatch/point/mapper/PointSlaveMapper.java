package kr.co.homeplus.shippingbatch.point.mapper;

import java.util.List;
import kr.co.homeplus.shippingbatch.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shippingbatch.point.model.mhc.MhcConfirm;
import kr.co.homeplus.shippingbatch.point.model.mileage.ClaimAccumulateDto;
import kr.co.homeplus.shippingbatch.point.model.mileage.MileageDto;
import kr.co.homeplus.shippingbatch.point.model.mileage.MileagePaybackDto;
import kr.co.homeplus.shippingbatch.point.model.mileage.ShippingAccumulateDto;

@SlaveConnection
public interface PointSlaveMapper {
    List<MileageDto> getPromoMileageSave(String basicDt);
    List<MhcConfirm> setConfirmMhcPoint(String basicDt);
    List<MileagePaybackDto> getPaybackMileageSave(String basicDt);
    List<ShippingAccumulateDto> getPurchaseAccumulate(String basicDt);
    ClaimAccumulateDto getClaimAccumulate(String purchaseOrderNo);
    List<MileageDto> getAnytimeMileageSave(String basicDt);
    int getMileageSaveAmount(String mileageType);
}
