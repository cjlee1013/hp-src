package kr.co.homeplus.shippingbatch.safetyissue.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@ApiModel(description = "안심번호 해지 요청 정보")
@Getter
@Setter
@EqualsAndHashCode
@RequiredArgsConstructor
public class SafetyCancelRequest {
    @ApiModelProperty(value = "배송주소번호", position = 1)
    private long shipAddrNo;
}
