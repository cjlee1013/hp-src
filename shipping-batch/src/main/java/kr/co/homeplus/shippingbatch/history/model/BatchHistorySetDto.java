package kr.co.homeplus.shippingbatch.history.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@ApiModel(description = "배치 - 연동업체코드")
public class BatchHistorySetDto {
    @ApiModelProperty(notes = "배치타입")
    private String batchType;

    @ApiModelProperty(notes = "배치시작일")
    private String batchStartDt;

    @ApiModelProperty(notes = "배치종료일시")
    private String batchEndDt;

    @ApiModelProperty(notes = "실패건수")
    private String failCount;

    @ApiModelProperty(notes = "성공건수")
    private String successCount;

    @ApiModelProperty(notes = "전체건수")
    private String totalCount;

    @ApiModelProperty(notes = "오류메시지")
    private String errorMessage;

    @ApiModelProperty(notes = "메타필드1")
    private String metaField1;

    @ApiModelProperty(notes = "메타필드2")
    private String metaField2;

    @ApiModelProperty(notes = "메타필드3")
    private String metaField3;

    @ApiModelProperty(notes = "메타필드4")
    private String metaField4;

    @ApiModelProperty(notes = "메타필드5")
    private String metaField5;

    public BatchHistorySetDto(String batchType) {
        this.batchType = batchType;
        init();
    }

    public void init() {
        this.batchStartDt = LocalDateTime.now().toString();
        this.successCount = "0";
        this.failCount = "0";
        this.totalCount = "0";
        this.errorMessage = null;
        metaField5 = null;
        metaField4 = null;
        metaField3 = null;
        metaField2 = null;
        metaField1 = null;
    }

    public void setBatchEndDt() {
        this.batchEndDt = LocalDateTime.now().toString();
    }

    public void setCount(int successCount, int failCount, int totalCount) {
        this.successCount = String.valueOf(successCount);
        this.failCount = String.valueOf(failCount);
        this.totalCount = String.valueOf(totalCount);
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void setMetaField1(String metaField1) {
        this.metaField1 = metaField1;
    }

    public void setMetaField2(String metaField2) {
        this.metaField2 = metaField2;
    }

    public void setMetaField3(String metaField3) {
        this.metaField3 = metaField3;
    }

    public void setMetaField4(String metaField4) {
        this.metaField4 = metaField4;
    }

    public void setMetaField5(String metaField5) {
        this.metaField5 = metaField5;
    }

    public void setErrorMsgSplit(StringBuilder errorMessage) {
        log.info(errorMessage.toString());
        if (errorMessage.length() > 4500) {
            metaField5 = errorMessage.substring(4000,4500);
            metaField4 = errorMessage.substring(3500,4000);
            metaField3 = errorMessage.substring(3000,3500);
            metaField2 = errorMessage.substring(2500,3000);
            metaField1 = errorMessage.substring(2000,2500);
            this.errorMessage = errorMessage.substring(0,2000);
        } else if (errorMessage.length() > 4000) {
            metaField5 = errorMessage.substring(4000,errorMessage.length());
            metaField4 = errorMessage.substring(3500,4000);
            metaField3 = errorMessage.substring(3000,3500);
            metaField2 = errorMessage.substring(2500,3000);
            metaField1 = errorMessage.substring(2000,2500);
            this.errorMessage = errorMessage.substring(0,2000);
        } else if (errorMessage.length() > 3500) {
            metaField5 = null;
            metaField4 = errorMessage.substring(3500,errorMessage.length());
            metaField3 = errorMessage.substring(3000,3500);
            metaField2 = errorMessage.substring(2500,3000);
            metaField1 = errorMessage.substring(2000,2500);
            this.errorMessage = errorMessage.substring(0,2000);
        } else if (errorMessage.length() > 3000) {
            metaField5 = null;
            metaField4 = null;
            metaField3 = errorMessage.substring(3000,errorMessage.length());
            metaField2 = errorMessage.substring(2500,3000);
            metaField1 = errorMessage.substring(2000,2500);
            this.errorMessage = errorMessage.substring(0,2000);
        } else if (errorMessage.length() > 2500) {
            metaField5 = null;
            metaField4 = null;
            metaField3 = null;
            metaField2 = errorMessage.substring(2500,errorMessage.length());
            metaField1 = errorMessage.substring(2000,2500);
            this.errorMessage = errorMessage.substring(0,2000);
        } else if (errorMessage.length() > 2000){
            metaField5 = null;
            metaField4 = null;
            metaField3 = null;
            metaField2 = null;
            metaField1 = errorMessage.substring(2000,errorMessage.length());
            this.errorMessage = errorMessage.substring(0,2000);
        } else {
            metaField5 = null;
            metaField4 = null;
            metaField3 = null;
            metaField2 = null;
            metaField1 = null;
            this.errorMessage = errorMessage.toString();
        }
    }
}
