package kr.co.homeplus.shippingbatch.market.service;

import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shippingbatch.history.model.BatchHistorySetDto;
import kr.co.homeplus.shippingbatch.history.service.HistoryService;
import kr.co.homeplus.shippingbatch.market.mapper.MarketMasterMapper;
import kr.co.homeplus.shippingbatch.market.mapper.MarketSlaveMapper;
import kr.co.homeplus.shippingbatch.market.model.MarketOrderItemGetDto;
import kr.co.homeplus.shippingbatch.market.model.MarketShipSetDto;
import kr.co.homeplus.shippingbatch.market.model.ShipDecisionMarketSetDto;
import kr.co.homeplus.shippingbatch.market.model.eleven.ShipCompleteItemGetDto;
import kr.co.homeplus.shippingbatch.market.model.naver.NaverOrderList;
import kr.co.homeplus.shippingbatch.market.model.naver.Order;
import kr.co.homeplus.shippingbatch.market.model.naver.ProductOrder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class MarketService {
  private final MarketMasterMapper marketMasterMapper;
  private final MarketSlaveMapper marketSlaveMapper;
  private final HistoryService historyService;

  @Autowired
  ResourceClient resourceClient;

  public void setNaverShipDecision(String fromDt, String toDt, BatchHistorySetDto batchHistorySetDto){
    int successCnt=0, failCnt=0,totalCnt = 0;
    boolean hasMoreData = true;
    StringBuilder errorMsg = new StringBuilder();
    errorMsg.append("Naver구매확정:");

    String lastChangedStatusCode = "PURCHASE_DECIDED";
    String marketOrderNo;
    String marketOrderItemNo;
    NaverOrderList naverOrderList;
    List<Order> orderList = null;
    List<ProductOrder> productOrderList = null;
    MarketShipSetDto marketShipSetDto;
    List<ShipDecisionMarketSetDto> shipDecisionMarketSetDtoList;
    int ret;
    ResourceClientRequest<NaverOrderList> request;
    TimeoutConfig timeoutConfig = new TimeoutConfig(1000 * 2, 1000 * 2, 1000 * 300);
    while (hasMoreData) {
      try {
        request = ResourceClientRequest.<NaverOrderList>getBuilder()
            .apiId("outbound")
            .uri("/market/naver/getShippingList?fromDt="+fromDt+"&toDt="+toDt+"&lastChangedStatusCode="+lastChangedStatusCode)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        naverOrderList = resourceClient.get(request, timeoutConfig).getBody().getData();
        orderList = naverOrderList.getOrderList();
        hasMoreData = naverOrderList.isHasMoreData();
        fromDt = naverOrderList.getFromDt();
      } catch (Exception e) {
        errorMsg.append("API 연결 오류");
        orderList = null;
        hasMoreData = false;
      }
      try {
        if (orderList != null) {
          totalCnt += orderList.size();
          for (Order order : orderList) {
            marketOrderNo = order.getOrderID();
            if (marketOrderNo != null) {
              marketShipSetDto = new MarketShipSetDto(marketOrderNo, null, "naver");
              productOrderList = order.getProductOrderList();
              if (productOrderList != null) {
                for (ProductOrder productOrder : productOrderList) {
                  marketOrderItemNo = productOrder.getProductOrderID();
                  if (marketOrderItemNo != null) {
                    marketShipSetDto.setMarketOrderItemNo(marketOrderItemNo);
                    try {
                      ret = marketMasterMapper.setMarketShipDecision(marketShipSetDto);
                      if (ret > 0) {
                        successCnt++;
                      } else {
                        failCnt++;
                        errorMsg.append(
                            " " + marketOrderNo + "|" + marketOrderItemNo + "업데이트오류.");
                      }
                    } catch (Exception e) {
                      failCnt++;
                      errorMsg.append(
                          " " + marketOrderNo + "|" + marketOrderItemNo + "|" + e.getMessage()
                              + "업데이트오류.");
                    }
                  } else {
                    errorMsg.append(" " + marketOrderNo + "상품주문번호없음.");
                  }
                }
              } else {
                errorMsg.append(" " + marketOrderNo + "상품정보없음.");
              }
              // 전체 구매확정 여부 확인
              shipDecisionMarketSetDtoList = marketMasterMapper.getShipDecisionMarket(marketShipSetDto);
              for (ShipDecisionMarketSetDto shipDecisionMarketSetDto : shipDecisionMarketSetDtoList) {
                marketMasterMapper.setShipDecisionMarket(shipDecisionMarketSetDto);
              }
            } else {
              errorMsg.append(" 상품번호없음.");
            }
          }
        }
      } catch (Exception exception) {
        errorMsg.append(" "+exception.getMessage()+"파싱오류.");
      }
    }
    batchHistorySetDto.setCount(successCnt, failCnt, totalCnt);
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setElevenShipDecision(BatchHistorySetDto batchHistorySetDto){
    int successCnt=0, failCnt=0, totalCnt = 0;
    StringBuilder errorMsg = new StringBuilder();

    ZoneId zoneid = ZoneId.systemDefault();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
    Instant now = Instant.now();
    String endTime = LocalDateTime.ofInstant(now,zoneid).format(formatter);
    String startTime = LocalDateTime.ofInstant(now.minus(30, ChronoUnit.MINUTES),zoneid).format(formatter);
    String marketOrderNo;
    String marketOrderItemNo;
    List<ShipCompleteItemGetDto> shipCompleteItemGetDtoList = null;
    MarketShipSetDto marketShipSetDto;
    List<ShipDecisionMarketSetDto> shipDecisionMarketSetDtoList;
    int ret;
    try {
      shipCompleteItemGetDtoList = (resourceClient.getForResponseObject(
              "outbound", "/market/eleven/getShipComplete?startTime="+startTime+"&endTime="+endTime,
              new ParameterizedTypeReference<ResponseObject<List<ShipCompleteItemGetDto>>>() {
              })).getData();
    } catch (Exception e) {
      errorMsg.append("API 연결 오류");
    }
    try {
      if (shipCompleteItemGetDtoList != null) {
        errorMsg.append("Eleven구매확정:");
        totalCnt = shipCompleteItemGetDtoList.size();
        for (ShipCompleteItemGetDto shipCompleteItemGetDto : shipCompleteItemGetDtoList) {
          marketOrderNo = shipCompleteItemGetDto.getOrdNo();
          marketOrderItemNo = shipCompleteItemGetDto.getOrdPrdSeq();
          marketShipSetDto = new MarketShipSetDto(marketOrderNo, marketOrderItemNo, "eleven");
          try {
            ret = marketMasterMapper.setMarketShipDecision(marketShipSetDto);
            if (ret > 0) {
              successCnt++;
            } else {
              failCnt++;
              errorMsg.append(" " + marketOrderNo + "|" + marketOrderItemNo + "업데이트오류.");
            }
          } catch (Exception e) {
            failCnt++;
            errorMsg.append(" " + marketOrderNo + "|" + marketOrderItemNo +"|" +e.getMessage() +"업데이트오류.");
          }
          // 전체 구매확정 여부 확인
          shipDecisionMarketSetDtoList = marketMasterMapper.getShipDecisionMarket(marketShipSetDto);
          for (ShipDecisionMarketSetDto shipDecisionMarketSetDto : shipDecisionMarketSetDtoList) {
            marketMasterMapper.setShipDecisionMarket(shipDecisionMarketSetDto);
          }
        }
      }
    } catch (Exception exception) {
      errorMsg.append(" "+exception.getMessage()+"파싱오류.");
    }
    batchHistorySetDto.setCount(successCnt, failCnt, totalCnt);
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setMarketShipStart(BatchHistorySetDto batchHistorySetDto) {
    int successCnt=0, failCnt=0;
    String marketUrl, marketType, marketOrderNo,marketOrderItemNo;
    boolean ret;
    ResourceClientRequest<Boolean> request;
    StringBuilder errorMsg = new StringBuilder();
    List<MarketOrderItemGetDto> marketOrderItemGetDtoList = marketSlaveMapper.getShippingMarket();
    errorMsg.append("배송출발:");
    for (MarketOrderItemGetDto marketOrderItemGetDto : marketOrderItemGetDtoList) {
      marketOrderNo = marketOrderItemGetDto.getMarketOrderNo();
      marketOrderItemNo = marketOrderItemGetDto.getMarketOrderItemNo();
      marketType = marketOrderItemGetDto.getMarketType();
      switch (marketType.toLowerCase()) {
        case "naver":
          marketUrl = "/market/naver/setShipStart?productOrderId=" + marketOrderItemNo + "&trackingNo=" + marketOrderItemGetDto.getPurchaseOrderNo();
          break;
        case "eleven":
          marketUrl = "/market/eleven/setShipStartPart?dlvNo=" + marketOrderItemGetDto.getDlvNo() + "&ordNo=" + marketOrderNo + "&ordPrdSeq=" + marketOrderItemNo;
          break;
        default:
          marketUrl = null;
          break;
      }
      if (marketUrl != null) {
        try {
          request = ResourceClientRequest.<Boolean>getBuilder()
                  .apiId("outbound")
                  .uri(marketUrl)
                  .typeReference(new ParameterizedTypeReference<>() {
                  })
                  .build();
          ret = resourceClient.get(request, new TimeoutConfig()).getBody().getData();
          if (ret) {
            marketMasterMapper.setMarketShipStart(new MarketShipSetDto(marketOrderNo, marketOrderItemNo, marketType));
            successCnt++;
          } else {
            failCnt++;
            errorMsg.append(marketType+"|" + marketOrderNo + "|" + marketOrderItemNo + "업데이트오류.");
          }
        } catch (Exception e) {
          failCnt++;
          errorMsg.append(marketType+"|" + marketOrderNo + "|" + marketOrderItemNo +"|"+e.getMessage()+ "연결오류.");
        }
      }
    }
    batchHistorySetDto.setCount(successCnt,failCnt,marketOrderItemGetDtoList.size());
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

}
