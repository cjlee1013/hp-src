package kr.co.homeplus.shippingbatch.point.model.mileage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "외부연동 > 회원등급 마일리지 적립")
public class AccumulateDto {
  @ApiModelProperty(value = "구매번호", position = 1)
  private String purchaseOrderNo;

  @ApiModelProperty(value = "고객번호", position = 2)
  private String userNo;

  @ApiModelProperty(value = "포인트구분", position = 3)
  private String pointKind;

  @ApiModelProperty(value = "사이트유형", position = 4)
  private String siteType;

  @ApiModelProperty(value = "점포유형", position = 4)
  private String storeType;

  @ApiModelProperty(value = "주문일자", position = 5)
  private String orderDt;

  @ApiModelProperty(value = "배송완료일", position = 6)
  private String shipFshDt;

  @ApiModelProperty(value = "매출용 실제점포ID", position = 7)
  private String originStoreId;

  @ApiModelProperty(value = "마일리지적립율", position = 8)
  private double mileageAccRate;

  @ApiModelProperty(value = "마일리지적립금액", position = 9)
  private long mileageAccAmt;

  @ApiModelProperty(value = "적립대상금액", position = 10)
  private long accTargetAmt;

  @ApiModelProperty(value = "총주문금액", position = 11)
  private long totOrderAmt;

}
