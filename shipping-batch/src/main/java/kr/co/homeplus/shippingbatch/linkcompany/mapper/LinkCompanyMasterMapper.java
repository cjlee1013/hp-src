package kr.co.homeplus.shippingbatch.linkcompany.mapper;

import java.util.List;
import kr.co.homeplus.shippingbatch.core.db.annotation.MasterConnection;
import kr.co.homeplus.shippingbatch.linkcompany.model.LinkCompanyDto;
import org.apache.ibatis.annotations.Param;

@MasterConnection
public interface LinkCompanyMasterMapper {
    void deleteLinkCode(@Param("company") String company);
    int insertLinkCode(@Param("linkCompanyDtoList") List<LinkCompanyDto> linkCompanyDtoList, @Param("company") String company);
    int insertTest();
}
