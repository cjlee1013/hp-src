package kr.co.homeplus.shippingbatch.point.model.mileage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("FrontUserMyGradeInfoDto")
public class FrontUserMyGradeInfoDto {

    @ApiModelProperty(value = "등급번호")
    private Integer gradeSeq;
    @ApiModelProperty(value = "등급명")
    private String gradeNm;
    @ApiModelProperty(value = "주문집계 산정기준월")
    private String orderPrevMonth;
    @ApiModelProperty(value = "주문건수")
    private Integer orderCnt;
    @ApiModelProperty(value = "주문금액")
    private Integer orderAmount;

}
