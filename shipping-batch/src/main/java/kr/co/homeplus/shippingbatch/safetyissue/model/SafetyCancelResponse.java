package kr.co.homeplus.shippingbatch.safetyissue.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@ApiModel(description = "안심번호 해지 응답 정보")
@Getter
@Setter
@EqualsAndHashCode
@RequiredArgsConstructor
public class SafetyCancelResponse {
    @ApiModelProperty(value = "전체건수", position = 1)
    private int totalCount;

    @ApiModelProperty(value = "성공건수", position = 2)
    private int successCount;

    @ApiModelProperty(value = "실패건수", position = 3)
    private int failCount;
}