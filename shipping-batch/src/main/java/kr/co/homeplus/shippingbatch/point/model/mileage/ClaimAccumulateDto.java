package kr.co.homeplus.shippingbatch.point.model.mileage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "외부연동 > 회원등급 마일리지 클레임")
public class ClaimAccumulateDto {

  @ApiModelProperty(value = "적립취소대상금액", position = 1)
  private long claimAccTargetAmt;

  @ApiModelProperty(value = "총클레임금액", position = 2)
  private long totClaimAmt;

}
