package kr.co.homeplus.shippingbatch.market.service;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shippingbatch.history.model.BatchHistorySetDto;
import kr.co.homeplus.shippingbatch.history.service.HistoryService;
import kr.co.homeplus.shippingbatch.market.mapper.NaverInquiryMasterMapper;
import kr.co.homeplus.shippingbatch.market.mapper.NaverInquirySlaveMapper;
import kr.co.homeplus.shippingbatch.market.model.NaverInquiryGetDto;
import kr.co.homeplus.shippingbatch.market.model.NaverInquirySetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Slf4j
@Service
@RequiredArgsConstructor
public class NaverInquiryService {

  private final NaverInquiryMasterMapper naverInquiryMasterMapper;
  private final NaverInquirySlaveMapper naverInquirySlaveMapper;
  private final HistoryService historyService;

  @Autowired
  ResourceClient resourceClient;

  public void getCustomerInquiryList(BatchHistorySetDto batchHistorySetDto){
    this.getCustomerInquiryList("","", batchHistorySetDto);
  }

  public void getCustomerInquiryList(String StartDate, String EndDate, BatchHistorySetDto batchHistorySetDto){
    String errorMsg = "";
    NaverInquirySetDto naverInquirySetDto = new NaverInquirySetDto();
    if(!ObjectUtils.isEmpty(StartDate) && !ObjectUtils.isEmpty(EndDate)) {
      naverInquirySetDto.setInquiryTimeFrom(StartDate);
      naverInquirySetDto.setInquiryTimeTo(EndDate);
    }

    try {
      List<NaverInquiryGetDto> naverList = resourceClient.postForResponseObject(
          "outbound",
          naverInquirySetDto,
          "/market/naver/getCustomerInquiryList",
          new ParameterizedTypeReference<ResponseObject<List<NaverInquiryGetDto>>>(){}).getData();

      if(naverList.size() > 0) {
        for (NaverInquiryGetDto dto : naverList) {
            if (!ObjectUtils.isEmpty(dto.getCopOrdNo())) {
              NaverInquiryGetDto order = naverInquirySlaveMapper.selectSellerQnaOrderInfo(dto);
              if (!ObjectUtils.isEmpty(order)) {
                dto.setOrdNo(order.getOrdNo());
                dto.setStoreId(order.getStoreId());
                dto.setInquirerPhone(order.getInquirerPhone());
              }
            }
          naverInquiryMasterMapper.insertNaverInquiry(dto);
        }
      }
    } catch (Exception e) {
      errorMsg = e.getMessage();
    }

    batchHistorySetDto.setCount(0,0,0);
    batchHistorySetDto.setErrorMessage(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void getProductInquiryList(BatchHistorySetDto batchHistorySetDto){
    this.getProductInquiryList("","", batchHistorySetDto);
  }

  public void getProductInquiryList(String StartDate, String EndDate, BatchHistorySetDto batchHistorySetDto){
    String errorMsg = "";
    NaverInquirySetDto naverInquirySetDto = new NaverInquirySetDto();
    if(!ObjectUtils.isEmpty(StartDate) && !ObjectUtils.isEmpty(EndDate)) {
      naverInquirySetDto.setInquiryTimeFrom(StartDate);
      naverInquirySetDto.setInquiryTimeTo(EndDate);
    }

    try {
      List<NaverInquiryGetDto> naverList = resourceClient.postForResponseObject(
          "outbound",
          naverInquirySetDto,
          "/market/naver/getProductInquiryList",
          new ParameterizedTypeReference<ResponseObject<List<NaverInquiryGetDto>>>(){}).getData();

      if(naverList.size() > 0) {
        for (NaverInquiryGetDto dto : naverList) {
          naverInquiryMasterMapper.insertNaverInquiry(dto);
        }
      }
    } catch (Exception e) {
      errorMsg = e.getMessage();
    }

    batchHistorySetDto.setCount(0,0,0);
    batchHistorySetDto.setErrorMessage(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

}
