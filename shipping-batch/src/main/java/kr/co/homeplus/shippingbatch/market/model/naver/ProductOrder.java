package kr.co.homeplus.shippingbatch.market.model.naver;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProductOrder {
    private String productOrderID;
    private String productOrderStatus;
    private String claimType;                                   //클레임타입코드(CANCEL,RETURN,EXCHANGE,ADMIN_CANCEL)
    private String claimStatus;                                 //클레임처리상태코드
    private String productID;
    private String productName;
    private String productClass;
    private String productOption;
    private int quantity;
    private long unitPrice;
    private long optionPrice;
    private String optionCode;
    private long totalProductAmount;
    private long productDiscountAmount;
    private long productImediateDiscountAmount;
    private long productProductDiscountAmount;
    private long productMultiplePurchaseDiscountAmount;
    private long totalPaymentAmount;
    private String sellerProductCode;
    private String mallID;
    private Address shippingAddress;
    private String expectedDeliveryMethod;                      //배송방법코드(DELIVERY:택배등기소포,VISIT_RECEIPT:방문수령,DIRECT_DELIVERY:직접전달,...)
    private String packageNumber;
    private String shippingFeeType;
    private String deliveryPolicyType;
    private long deliveryFeeAmount;
    private long sectionDeliveryFee;
    private long deliveryDiscountAmount;
    private String shippingMemo;
    private Address takingAddress;
    private String shippingDueDate;
    private String decisionDate;
    private String freeGift;
    private String placeOrderStatus;                            //발주상태코드(NOT_YET,OK,CANCEL)
    private String placeOrderDate;
    private String delayedDispatchReason;                       //발송지연사유코드(PRODUCT_PREPARE:상품준비중,CUSTOMER_REQUEST:고객요청,CUSTOM_BUILD:주문제작,RESERVED_DISPATCH:예약발송,ETC:기타)
    private String delayedDispatchDetailedReason;
    private long sellerBurdenDiscountAmount;
    private long sellerBurdenImediateDiscountAmount;
    private long sellerBurdenProductDiscountAmount;
    private long sellerBurdenMultiplePurchaseDiscountAmount;
    private String commissionRatingType;
    private String comissionPrePayStatus;                       //수수료결제방식코드(GENERAL_PRD:일반상품,PRE_PAY_PRD_NO_PAY:선차감(차감전),PRE_PAY_PRD_PAYED:선차감(차감후))
    private long paymentCommission;
    private long sellerCommission;
    private long channelCommission;
    private long knowledgeShoppingSellingInterlockCommission;
    private long expectedSettlementAmount;
    private String inflowPath;
    private String itemNo;
    private String optionManageCode;
    private String purchaserSocialSecurityNo;
    private String sellerCustomCode1;
    private String sellerCustomCode2;
    private String claimID;
    private String individualCustomUniqueCode;
    private String giftReceivingStatus;                         //선물수신상태코드(WAIT_FOR_RECEIVING:수락대기(배송지입력대기),RECEIVED:수락완료)
    private long sellerBurdenStoreDiscountAmount;
    private String sellerBurdenMultiplePurchaseDiscountType;    //판매자부담복수구매할인타입(QUANTITY:수량별할인,IGNORE_QUANTITY:수량무시할인)
    private String branchID;
    private boolean isAcceptAlternativeProduct;
    private String deliveryAttributeType;                       //배송속성타입코드(NORMAL,TODAY,HOPE,TODAY_ARRIVAL,DAWN_ARRIVAL)
    private String deliveryOperationDate;
    private String deliveryStartTime;
    private String deliveryEndTime;
    private String slotID;
    private String branchBenefitType;                           //지점혜택타입코드(PLUS_1:N+1혜택)
    private long branchBenefitValue;

    private String orderID;
}
