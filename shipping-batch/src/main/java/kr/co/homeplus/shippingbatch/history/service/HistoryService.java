package kr.co.homeplus.shippingbatch.history.service;


import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shippingbatch.history.mapper.HistoryMasterMapper;
import kr.co.homeplus.shippingbatch.history.model.BatchHistorySetDto;
import kr.co.homeplus.shippingbatch.tracking.mapper.TrackingMasterMapper;
import kr.co.homeplus.shippingbatch.tracking.mapper.TrackingSlaveMapper;
import kr.co.homeplus.shippingbatch.tracking.model.ExchRequestGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.ExchRequestSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.PickRequestGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.PickRequestSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.ShippingRequestGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.ShippingRequestSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class HistoryService {
  private final HistoryMasterMapper historyMasterMapper;

  public void setBatchHistory(BatchHistorySetDto batchHistorySetDto){
    batchHistorySetDto.setBatchEndDt();
    historyMasterMapper.setBatchHistory(batchHistorySetDto);
    batchHistorySetDto.init();
  }

}
