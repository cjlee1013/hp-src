package kr.co.homeplus.shippingbatch.point.model.mileage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "외부연동 > 마일리지 적립 > 배송비 페이백")
public class MileagePaybackDto extends MileageDto {
    @ApiModelProperty(value = "일련번호", position = 10)
    private String paybackNo;

    public MileagePaybackDto(String mileageCode, String storeType, String requestReason,
        String userNo, long mileageAmt, String tradeNo, String regId, String expireDay,
        String paybackNo) {
        super(mileageCode, storeType, requestReason, userNo, mileageAmt, tradeNo, regId, expireDay);
        this.paybackNo = paybackNo;
    }
}
