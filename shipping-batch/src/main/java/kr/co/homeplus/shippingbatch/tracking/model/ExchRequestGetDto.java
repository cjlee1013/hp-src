package kr.co.homeplus.shippingbatch.tracking.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class ExchRequestGetDto {
    @ApiModelProperty(notes = "배송번호")
    private String claimExchShippingNo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "성공여부")
    private String success;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "결과값")
    private String detail;

    public String getClaimExchShippingNo() {
        return claimExchShippingNo;
    }

    public String getSuccess() {
        return success;
    }

    public String getDetail() {
        return detail;
    }
}
