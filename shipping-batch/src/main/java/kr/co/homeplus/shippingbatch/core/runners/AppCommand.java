package kr.co.homeplus.shippingbatch.core.runners;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Callable;
import kr.co.homeplus.shippingbatch.history.model.BatchHistorySetDto;
import kr.co.homeplus.shippingbatch.linkcompany.service.LinkCompanyService;
import kr.co.homeplus.shippingbatch.market.service.NaverInquiryService;
import kr.co.homeplus.shippingbatch.market.service.MarketService;
import kr.co.homeplus.shippingbatch.point.service.PointService;
import kr.co.homeplus.shippingbatch.safetyissue.service.SafetyissueService;
import kr.co.homeplus.shippingbatch.shipping.ShippingMessageService;
import kr.co.homeplus.shippingbatch.tracking.service.TrackingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.ExitCode;
import picocli.CommandLine.IExitCodeExceptionMapper;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Component
@RequiredArgsConstructor
@Slf4j
@Command(name = "java -jar shipping-batch.jar", mixinStandardHelpOptions = true,
        version = "0.0.1",
        description = "shipping-batch command")
public class AppCommand implements Callable<Integer>, IExitCodeExceptionMapper {

    @ArgGroup(exclusive = true, multiplicity = "1")
    private Exclusive exclusive;

    static class Exclusive {
        @Option(names = {"-n", "--name"}, description = "service name")
        private boolean isName;
    }

    @Parameters(paramLabel = "service name", description = "positional params")
    private String serviceName;

    @Option(names = {"-d", "--date"}, description = "Date")
    private String basicDt;

    private final LinkCompanyService linkCompanyService;

    private final TrackingService trackingService;

    private final PointService pointService;

    private final ShippingMessageService shippingMessageService;

    private final SafetyissueService safetyissueService;

    private final NaverInquiryService naverInquiryService;

    private final MarketService marketService;

    @Override
    public Integer call() throws Exception {
        log.info("exclusive.isName :: "+ exclusive.isName);
        log.info("exclusive.isName :: "+ serviceName);

        // 실행 시간 확인.
        //Date executionTime = new Date();
        //log.info("shipping-batch 수행 시작 :: 시간 [{}] :: 실행 유형 :: [{}]", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(executionTime), serviceName);

        if(exclusive.isName) {
            BatchHistorySetDto batchHistorySetDto = new BatchHistorySetDto(serviceName);
            ZoneId zoneid = ZoneId.systemDefault();
            Instant now = Instant.now();
            switch (serviceName) {
                case "updateLinkCode":
                    linkCompanyService.updateLinkCode(batchHistorySetDto);
                    break;
                case "setShippingInvoice":
                    trackingService.setShippingTracking(batchHistorySetDto);
                    trackingService.setPickTracking(batchHistorySetDto);
                    trackingService.setExchTracking(batchHistorySetDto);
                    trackingService.setMultiShippingTracking(batchHistorySetDto);
                    trackingService.setBsdTracking(batchHistorySetDto);
                    break;
                case "setPickRequest":
                    trackingService.setPickRequest(batchHistorySetDto);
                    break;
                case "setShippingComplete":
                    trackingService.setShippingCompleteDLV(batchHistorySetDto);
                    trackingService.setShippingCompleteDRCT(batchHistorySetDto);
                    trackingService.setMultiComplete(batchHistorySetDto);
                    trackingService.setMultiPurchaseComplete(batchHistorySetDto);
                    trackingService.setShippingCompleteMulti(batchHistorySetDto);
                    trackingService.setPurchaseCompleteDS(batchHistorySetDto);
                    trackingService.setBundleComplete(batchHistorySetDto);
                    trackingService.setPurchaseOrderFinish(batchHistorySetDto);
                    trackingService.setShippingCompleteByHistory(batchHistorySetDto);
                    trackingService.setMultiCompleteByHistory(batchHistorySetDto);
                    break;
                case "setPurchaseCompleteTD":
                    trackingService.setPurchaseCompleteTD(batchHistorySetDto);
                    break;
                case "setBsdLink":
                    trackingService.setBsdLink(batchHistorySetDto);
                    trackingService.setShippingBsdInvoice(batchHistorySetDto);
                    trackingService.setMultiOrderBsdInvoice(batchHistorySetDto);
                    break;
                case "setPromoMileageSave":
                    pointService.setPromoMileageSave(batchHistorySetDto, basicDt);
                    break;
                case "setPaybackMileageSave":
                    pointService.setPaybackMileageSave(batchHistorySetDto, basicDt);
                    pointService.setAnytimeMileageSave(batchHistorySetDto, basicDt);
                    break;
                case "setConfirmMhcPoint":
                    pointService.setConfirmMhcPoint(batchHistorySetDto, basicDt);
                    break;
                case "setAccumulate":
                    pointService.setAccumulate(batchHistorySetDto, basicDt);
                    break;
                case "sendComfirmReceiveTalk":
                    shippingMessageService.sendComfirmReceiveTalk(batchHistorySetDto);
                    break;
                case "cancelSafetyNo":
                    safetyissueService.cancelSafetyNo(batchHistorySetDto,basicDt);
                    safetyissueService.cancelMultiSafetyNo(batchHistorySetDto,basicDt);
                    break;
                case "getNaverInquiry":
                    naverInquiryService.getCustomerInquiryList(batchHistorySetDto);
                    naverInquiryService.getProductInquiryList(batchHistorySetDto);
                    break;
                case "setMarketShippingStart":
                    marketService.setMarketShipStart(batchHistorySetDto);
                    break;
                case "setMarketShippingDecision":
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
                    String toDt = LocalDateTime.ofInstant(now,zoneid).format(formatter);
                    String fromDt = LocalDateTime.ofInstant(now.minus(30, ChronoUnit.MINUTES),zoneid).format(formatter);
                    marketService.setNaverShipDecision(fromDt,toDt,batchHistorySetDto);
                    //marketService.setElevenShipDecision(batchHistorySetDto);
                    break;
                default:
                    batchHistorySetDto.setErrorMessage("서비스 없음");
                    break;
            }
        }

        return ExitCode.OK;
    }

    @Override
    public int getExitCode(Throwable exception) {
        Throwable cause = exception.getCause();
        if (cause instanceof ArrayIndexOutOfBoundsException) {
            return 12;
        } else if (cause instanceof NumberFormatException) {
            return 13;
        }
        return 11;
    }

}