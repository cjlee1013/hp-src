package kr.co.homeplus.shippingbatch.market.model.naver;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class Order {
    private String orderID;
    private String orderDate;
    private String ordererID;
    private String ordererName;
    private String ordererTel1;
    private String ordererTel2;
    private String paymentDate;
    private String paymentMeans;
    private String paymentDueDate;
    private long orderDiscountAmount;
    private long generalPaymentAmount;
    private long naverMileagePaymentAmount;
    private long chargeAmountPaymentAmount;
    private long checkoutAccumlationPaymentAmount;
    private long payLaterPaymentAmount;
    private String isDeliveryMemoParticularInput;
    private String payLocationType;
    private String ordererNo;

    private List<ProductOrder> productOrderList = new ArrayList<ProductOrder>();
}
