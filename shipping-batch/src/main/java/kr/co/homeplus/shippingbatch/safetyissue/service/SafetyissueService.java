package kr.co.homeplus.shippingbatch.safetyissue.service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shippingbatch.history.model.BatchHistorySetDto;
import kr.co.homeplus.shippingbatch.history.service.HistoryService;
import kr.co.homeplus.shippingbatch.safetyissue.mapper.SafetyissueSlaveMapper;
import kr.co.homeplus.shippingbatch.safetyissue.model.MultiSafetyCancelRequest;
import kr.co.homeplus.shippingbatch.safetyissue.model.SafetyCancelRequest;
import kr.co.homeplus.shippingbatch.safetyissue.model.SafetyCancelResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SafetyissueService {
  private final SafetyissueSlaveMapper safetyissueSlaveMapper;
  private final HistoryService historyService;

  @Autowired
  ResourceClient resourceClient;

  public void cancelSafetyNo(BatchHistorySetDto batchHistorySetDto, String basicDt) {
    if (basicDt == null) {
      basicDt = LocalDateTime.ofInstant(Instant.now().minus(8, ChronoUnit.DAYS),
          ZoneId.systemDefault())
          .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }
    List<SafetyCancelRequest> safetyCancelRequestList = safetyissueSlaveMapper.getSafetyCancelList(basicDt);
    StringBuilder errorMsg = new StringBuilder();
    errorMsg.append(basicDt).append(": ");
    int successCnt=0, failCnt=0, totalCnt = 0;
    SafetyCancelResponse safetyCancelResponse;
    if (safetyCancelRequestList != null && safetyCancelRequestList.size() > 0) {
      totalCnt = safetyCancelRequestList.size();
      // 1회 요청 건수 최대 50건
      int requestCnt = 50;
      for (int i=0;i<totalCnt;i=i+50){
        if ((i+50) > totalCnt) {
          requestCnt = totalCnt - i;
        }
        try {
          safetyCancelResponse = (resourceClient.postForResponseObject("escrow"
            , safetyCancelRequestList.subList(i,i+requestCnt), "/safetyissue/cancelSafetyNo"
            , new ParameterizedTypeReference<ResponseObject<SafetyCancelResponse>>() {
            })).getData();
          if (safetyCancelResponse != null) {
            successCnt = safetyCancelResponse.getSuccessCount();
            failCnt = safetyCancelResponse.getFailCount();
            if (totalCnt != safetyCancelResponse.getTotalCount()) {
              errorMsg.append("요청건수:응답건수 불일치 ").append(totalCnt).append(":").append(safetyCancelResponse.getTotalCount());
            } else {
              errorMsg.append("성공");
            }
          } else {
            errorMsg.append("API 응답 없음");
          }
        } catch (Exception e) {
          errorMsg.append("API 연결 오류");
        }
      }
    } else {
      errorMsg.append("건없음");
    }
    batchHistorySetDto.setCount(successCnt,failCnt, totalCnt);
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void cancelMultiSafetyNo(BatchHistorySetDto batchHistorySetDto, String basicDt) {
    if (basicDt == null) {
      basicDt = LocalDateTime.ofInstant(Instant.now().minus(7, ChronoUnit.DAYS),
          ZoneId.systemDefault())
          .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }
    List<MultiSafetyCancelRequest> safetyMultiCancelList = safetyissueSlaveMapper.getSafetyMultiCancelList(basicDt);
    StringBuilder errorMsg = new StringBuilder();
    errorMsg.append(basicDt).append(": ");
    int successCnt=0, failCnt=0, totalCnt = 0;
    SafetyCancelResponse safetyCancelResponse;
    if (safetyMultiCancelList != null && safetyMultiCancelList.size() > 0) {
      totalCnt = safetyMultiCancelList.size();
      // 1회 요청 건수 최대 50건
      int requestCnt = 50;
      for (int i=0;i<totalCnt;i=i+50){
        if ((i+50) > totalCnt) {
          requestCnt = totalCnt - i;
        }
        try {
          safetyCancelResponse = (resourceClient.postForResponseObject("escrow"
              , safetyMultiCancelList.subList(i,i+requestCnt), "/safetyissue/cancelMultiSafetyNo"
              , new ParameterizedTypeReference<ResponseObject<SafetyCancelResponse>>() {
              })).getData();
          if (safetyCancelResponse != null) {
            successCnt = safetyCancelResponse.getSuccessCount();
            failCnt = safetyCancelResponse.getFailCount();
            if (totalCnt != safetyCancelResponse.getTotalCount()) {
              errorMsg.append("요청건수:응답건수 불일치 ").append(totalCnt).append(":").append(safetyCancelResponse.getTotalCount());
            } else {
              errorMsg.append("성공");
            }
          } else {
            errorMsg.append("API 응답 없음");
          }
        } catch (Exception e) {
          errorMsg.append("API 연결 오류");
        }
      }
    } else {
      errorMsg.append("건없음");
    }
    batchHistorySetDto.setCount(successCnt,failCnt, totalCnt);
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

}
