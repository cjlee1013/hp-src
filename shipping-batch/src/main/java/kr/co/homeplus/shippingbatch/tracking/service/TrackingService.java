package kr.co.homeplus.shippingbatch.tracking.service;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shippingbatch.history.model.BatchHistorySetDto;
import kr.co.homeplus.shippingbatch.history.service.HistoryService;
import kr.co.homeplus.shippingbatch.tracking.model.BsdInvoiceGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.BsdLinkTranSetDto;
import kr.co.homeplus.shippingbatch.tracking.mapper.TrackingMasterMapper;
import kr.co.homeplus.shippingbatch.tracking.mapper.TrackingSlaveMapper;
import kr.co.homeplus.shippingbatch.tracking.model.BsdRequestGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.BsdRequestSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.ExchRequestGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.ExchRequestSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.MultiShippingRequestGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.MultiShippingRequestSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.PickRequestGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.PickRequestSetDto;
import kr.co.homeplus.shippingbatch.tracking.model.ShippingRequestGetDto;
import kr.co.homeplus.shippingbatch.tracking.model.ShippingRequestSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TrackingService {
  private final TrackingMasterMapper trackingMasterMapper;
  private final TrackingSlaveMapper trackingSlaveMapper;
  private final HistoryService historyService;

  @Autowired
  ResourceClient resourceClient;

  public void setShippingTracking(BatchHistorySetDto batchHistorySetDto){
    int successCnt=0, failCnt=0;
    StringBuilder errorMsg = new StringBuilder();
    List<ShippingRequestSetDto> requestSetDtoList = trackingSlaveMapper.getShippingTracking();
    try {
      List<ShippingRequestGetDto> requestGetDtoList = (resourceClient.postForResponseObject(
          "shipping", requestSetDtoList, "/tracking/shipping/requestTracking",
          new ParameterizedTypeReference<ResponseObject<List<ShippingRequestGetDto>>>() {
          })).getData();
      for (ShippingRequestGetDto requestGetDto : requestGetDtoList) {
        if ("true".equals(requestGetDto.getSuccess())) {
          successCnt++;
        } else {
          failCnt++;
          errorMsg.append(requestGetDto.getShipNo() + " : " + requestGetDto.getDetail() + "\n");
        }
      }
    } catch (Exception e) {
      errorMsg.append("API 연결 오류");
    }
    batchHistorySetDto.setCount(successCnt,failCnt,requestSetDtoList.size());
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setPickTracking(BatchHistorySetDto batchHistorySetDto){
    int successCnt=0, failCnt=0;
    StringBuilder errorMsg = new StringBuilder();
    List<PickRequestSetDto> requestSetDtoList = trackingSlaveMapper.getPickTracking();
    try {
      List<PickRequestGetDto> requestGetDtoList = (resourceClient.postForResponseObject(
          "shipping", requestSetDtoList, "/tracking/pick/requestTracking",
          new ParameterizedTypeReference<ResponseObject<List<PickRequestGetDto>>>() {
          })).getData();
      for (PickRequestGetDto requestGetDto : requestGetDtoList) {
        if ("true".equals(requestGetDto.getSuccess())) {
          successCnt++;
        } else {
          failCnt++;
          errorMsg.append(requestGetDto.getClaimPickShippingNo() + " : " + requestGetDto.getDetail() + "\n");
        }
      }
    } catch (Exception e) {
      errorMsg.append("API 연결 오류");
    }
    batchHistorySetDto.setCount(successCnt,failCnt,requestSetDtoList.size());
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setExchTracking(BatchHistorySetDto batchHistorySetDto){
    int successCnt=0, failCnt=0;
    StringBuilder errorMsg = new StringBuilder();
    List<ExchRequestSetDto> requestSetDtoList = trackingSlaveMapper.getExchTracking();
    try {
      List<ExchRequestGetDto> requestGetDtoList = (resourceClient.postForResponseObject(
          "shipping", requestSetDtoList, "/tracking/exch/requestTracking",
          new ParameterizedTypeReference<ResponseObject<List<ExchRequestGetDto>>>() {
          })).getData();
      for (ExchRequestGetDto requestGetDto : requestGetDtoList) {
        if ("true".equals(requestGetDto.getSuccess())) {
          successCnt++;
        } else {
          failCnt++;
          errorMsg.append(requestGetDto.getClaimExchShippingNo() + " : " + requestGetDto.getDetail() + "\n");
        }
      }
    } catch (Exception e) {
      errorMsg.append("API 연결 오류");
    }
    batchHistorySetDto.setCount(successCnt,failCnt,requestSetDtoList.size());
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setPickRequest(BatchHistorySetDto batchHistorySetDto){
    int successCnt=0, failCnt=0;
    StringBuilder errorMsg = new StringBuilder();
    List<PickRequestSetDto> requestSetDtoList = trackingSlaveMapper.getPickRequest();
    try {
      List<PickRequestGetDto> requestGetDtoList = (resourceClient.postForResponseObject(
          "shipping", requestSetDtoList, "/tracking/pick/requestPick",
          new ParameterizedTypeReference<ResponseObject<List<PickRequestGetDto>>>() {
          })).getData();
      for (PickRequestGetDto requestGetDto : requestGetDtoList) {
        if ("Y".equals(requestGetDto.getSuccess())) {
          successCnt++;
        } else {
          failCnt++;
          errorMsg.append(requestGetDto.getClaimPickShippingNo() + " : " + requestGetDto.getDetail() + "\n");
        }
      }
    } catch (Exception e) {
      errorMsg.append("API 연결 오류");
    }
    batchHistorySetDto.setCount(successCnt,failCnt,requestSetDtoList.size());
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setShippingCompleteDLV(BatchHistorySetDto batchHistorySetDto){
    int successCnt;
    String errorMsg = null;
    try {
      successCnt = trackingMasterMapper.setShippingCompleteDLV();
    } catch (Exception e) {
      errorMsg = e.getMessage();
      successCnt = 0;
    }
    batchHistorySetDto.setCount(successCnt,0,successCnt);
    batchHistorySetDto.setErrorMessage(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setShippingCompleteDRCT(BatchHistorySetDto batchHistorySetDto){
    int successCnt;
    String errorMsg = null;
    try {
      successCnt = trackingMasterMapper.setShippingCompleteDRCT();
    } catch (Exception e) {
      errorMsg = e.getMessage();
      successCnt = 0;
    }
    batchHistorySetDto.setCount(successCnt,0,successCnt);
    batchHistorySetDto.setErrorMessage(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setPurchaseCompleteDS(BatchHistorySetDto batchHistorySetDto){
    int successCnt;
    String errorMsg = null;
    try {
      successCnt = trackingMasterMapper.setPurchaseCompleteDS();
    } catch (Exception e) {
      errorMsg = e.getMessage();
      successCnt = 0;
    }
    batchHistorySetDto.setCount(successCnt,0,successCnt);
    batchHistorySetDto.setErrorMessage(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setPurchaseCompleteTD(BatchHistorySetDto batchHistorySetDto){
    int successCnt;
    String errorMsg = null;
    try {
      successCnt = trackingMasterMapper.setPurchaseCompleteTD();
    } catch (Exception e) {
      errorMsg = e.getMessage();
      successCnt = 0;
    }
    batchHistorySetDto.setCount(successCnt,0,successCnt);
    batchHistorySetDto.setErrorMessage(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setBundleComplete(BatchHistorySetDto batchHistorySetDto){
    int successCnt;
    String errorMsg = null;
    try {
      successCnt = trackingMasterMapper.setBundleComplete();
    } catch (Exception e) {
      errorMsg = e.getMessage();
      successCnt = 0;
    }
    batchHistorySetDto.setCount(successCnt,0,successCnt);
    batchHistorySetDto.setErrorMessage(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }
  public void setPurchaseOrderFinish(BatchHistorySetDto batchHistorySetDto){
    int successCnt;
    String errorMsg = null;
    try {
      successCnt = trackingMasterMapper.setPurchaseOrderFinish();
    } catch (Exception e) {
      errorMsg = e.getMessage();
      successCnt = 0;
    }
    batchHistorySetDto.setCount(successCnt,0,successCnt);
    batchHistorySetDto.setErrorMessage(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setMultiShippingTracking(BatchHistorySetDto batchHistorySetDto){
    int successCnt=0, failCnt=0;
    StringBuilder errorMsg = new StringBuilder();
    List<MultiShippingRequestSetDto> requestSetDtoList = trackingSlaveMapper.getMultiShippingTracking();
    try {
      List<MultiShippingRequestGetDto> requestGetDtoList = (resourceClient.postForResponseObject(
          "shipping", requestSetDtoList, "/tracking/multi/requestTracking",
          new ParameterizedTypeReference<ResponseObject<List<MultiShippingRequestGetDto>>>() {
          })).getData();
      for (MultiShippingRequestGetDto requestGetDto : requestGetDtoList) {
        if ("true".equals(requestGetDto.getSuccess())) {
          successCnt++;
        } else {
          failCnt++;
          errorMsg.append(requestGetDto.getMultiOrderItemNo() + " : " + requestGetDto.getDetail() + "\n");
        }
      }
    } catch (Exception e) {
      errorMsg.append("API 연결 오류");
    }
    batchHistorySetDto.setCount(successCnt,failCnt,requestSetDtoList.size());
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setMultiComplete(BatchHistorySetDto batchHistorySetDto){
    int successCnt;
    String errorMsg = null;
    try {
      successCnt = trackingMasterMapper.setMultiComplete();
    } catch (Exception e) {
      errorMsg = e.getMessage();
      successCnt = 0;
    }
    batchHistorySetDto.setCount(successCnt,0,successCnt);
    batchHistorySetDto.setErrorMessage(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }
  public void setMultiPurchaseComplete(BatchHistorySetDto batchHistorySetDto){
    int successCnt;
    String errorMsg = null;
    try {
      successCnt = trackingMasterMapper.setMultiPurchaseComplete();
    } catch (Exception e) {
      errorMsg = e.getMessage();
      successCnt = 0;
    }
    batchHistorySetDto.setCount(successCnt,0,successCnt);
    batchHistorySetDto.setErrorMessage(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }
  public void setShippingCompleteMulti(BatchHistorySetDto batchHistorySetDto){
    int successCnt;
    String errorMsg = null;
    try {
      successCnt = trackingMasterMapper.setShippingCompleteMulti();
    } catch (Exception e) {
      errorMsg = e.getMessage();
      successCnt = 0;
    }
    batchHistorySetDto.setCount(successCnt,0,successCnt);
    batchHistorySetDto.setErrorMessage(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setBsdLink(BatchHistorySetDto batchHistorySetDto){
    int uploadCnt, downloadCnt,successCnt=0;
    StringBuilder metafield = new StringBuilder();
    String errorMsg = null;
    // 포장배분 데이터 생성
    uploadCnt = trackingMasterMapper.setBsdUpload();
    BsdLinkTranSetDto bsdLinkTranSetDto = new BsdLinkTranSetDto();
    try {
      // BSD(LGW) 데이터 전송
      String transCode = (resourceClient.postForResponseObject(
          "shipping", bsdLinkTranSetDto, "/admin/shipManage/setBsdLinkTran",
          new ParameterizedTypeReference<ResponseObject<String>>() {
          })).getData();
      successCnt = Integer.parseInt(transCode);
    } catch (Exception e) {
      errorMsg = "API 연결 오류";
    }
    // 송장번호 업데이트
    downloadCnt = trackingMasterMapper.setBsdDownload();
    metafield.append("Upload:").append(uploadCnt).append(", Down:").append(downloadCnt);
    batchHistorySetDto.setCount(successCnt,0,0);
    batchHistorySetDto.setErrorMessage(errorMsg);
    batchHistorySetDto.setMetaField1(metafield.toString());
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setShippingBsdInvoice(BatchHistorySetDto batchHistorySetDto){
    int successCnt=0, failCnt=0;
    StringBuilder metafield = new StringBuilder();
    String errorMsg = null;
    List<BsdInvoiceGetDto> requestSetDtoList = trackingSlaveMapper.getShippingBsdInvoice();
    try {
      List<String> requestGetDtoList = (resourceClient.postForResponseObject(
          "shipping", requestSetDtoList, "/admin/shipManage/setShippingBsdInvoice",
          new ParameterizedTypeReference<ResponseObject<List<String>>>() {
          })).getData();
      successCnt = requestGetDtoList.size();
    } catch (Exception e) {
      errorMsg = "API 연결 오류";
    }
    batchHistorySetDto.setCount(successCnt,failCnt,requestSetDtoList.size());
    batchHistorySetDto.setErrorMessage(errorMsg);
    batchHistorySetDto.setMetaField1(metafield.toString());
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setMultiOrderBsdInvoice(BatchHistorySetDto batchHistorySetDto){
    int successCnt=0, failCnt=0;
    StringBuilder metafield = new StringBuilder();
    String errorMsg = null;
    List<BsdInvoiceGetDto> requestSetDtoList = trackingSlaveMapper.getMultiOrderBsdInvoice();
    try {
      List<String> requestGetDtoList = (resourceClient.postForResponseObject(
          "shipping", requestSetDtoList, "/admin/shipManage/setMultiOrderBsdInvoice",
          new ParameterizedTypeReference<ResponseObject<List<String>>>() {
          })).getData();
      successCnt = requestGetDtoList.size();
    } catch (Exception e) {
      errorMsg = "API 연결 오류";
    }
    batchHistorySetDto.setCount(successCnt,failCnt,requestSetDtoList.size());
    batchHistorySetDto.setErrorMessage(errorMsg);
    batchHistorySetDto.setMetaField1(metafield.toString());
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setBsdTracking(BatchHistorySetDto batchHistorySetDto){
    int successCnt=0, failCnt=0;
    StringBuilder errorMsg = new StringBuilder();
    List<BsdRequestSetDto> requestSetDtoList = trackingSlaveMapper.getBsdTracking();
    try {
      List<BsdRequestGetDto> requestGetDtoList = (resourceClient.postForResponseObject(
          "shipping", requestSetDtoList, "/tracking/bsd/requestTracking",
          new ParameterizedTypeReference<ResponseObject<List<BsdRequestGetDto>>>() {
          })).getData();
      for (BsdRequestGetDto requestGetDto : requestGetDtoList) {
        if ("true".equals(requestGetDto.getSuccess())) {
          successCnt++;
        } else {
          failCnt++;
          errorMsg.append(requestGetDto.getBsdLinkNo() + " : " + requestGetDto.getDetail() + "\n");
        }
      }
    } catch (Exception e) {
      errorMsg.append("API 연결 오류");
    }
    batchHistorySetDto.setCount(successCnt,failCnt,requestSetDtoList.size());
    batchHistorySetDto.setErrorMsgSplit(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setShippingCompleteByHistory(BatchHistorySetDto batchHistorySetDto){
    int successCnt;
    String errorMsg = null;
    try {
      successCnt = trackingMasterMapper.setShippingCompleteByHistory();
    } catch (Exception e) {
      errorMsg = e.getMessage();
      successCnt = 0;
    }
    batchHistorySetDto.setCount(successCnt,0,successCnt);
    batchHistorySetDto.setErrorMessage(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

  public void setMultiCompleteByHistory(BatchHistorySetDto batchHistorySetDto){
    int successCnt;
    String errorMsg = null;
    try {
      successCnt = trackingMasterMapper.setMultiCompleteByHistory();
    } catch (Exception e) {
      errorMsg = e.getMessage();
      successCnt = 0;
    }
    batchHistorySetDto.setCount(successCnt,0,successCnt);
    batchHistorySetDto.setErrorMessage(errorMsg);
    historyService.setBatchHistory(batchHistorySetDto);
  }

}
