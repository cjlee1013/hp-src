package kr.co.homeplus.inbound.item.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("상품 기본정보")
public class ItemBasicSetDto {

	@ApiModelProperty(value = "상품번호 (미입력: 등록, 입력:수정)", position = 1)
	private String itemNo;

	@ApiModelProperty(value = "상품명 (최대 50자)", required = true, position = 2)
	@JsonProperty(value = "itemName")
	private String itemNm;

	@ApiModelProperty(value = "상품유형 (N:새상품, U:중고, R:리퍼, B:반품(리세일), C:주문제작) - 수정불가", required = true, position = 3)
	private String itemType;

	public void setItemType(String itemType) {
		this.itemType = itemType.toUpperCase();
	}

	@ApiModelProperty(value = "업체상품코드 (최대 25자)", position = 4)
	@JsonProperty(value = "sellerItemCode")
	private String sellerItemCd;

	@ApiModelProperty(value = "세카테고리코드", required = true, position = 5)
	@JsonProperty(value = "divideCategoryCode")
	private int dcateCd;

	@ApiModelProperty(value = "판매업체ID", position = 6, hidden = true)
	private String partnerId;

	@ApiModelProperty(value = "노출여부 기준:노출 (Y:노출, N:노출안함)", position = 7)
	@JsonProperty(value = "displayYn")
	@Builder.Default
	private String dispYn = "Y";

	public void setDispYn(String dispYn) {
		this.dispYn = dispYn.toUpperCase();
	}

}