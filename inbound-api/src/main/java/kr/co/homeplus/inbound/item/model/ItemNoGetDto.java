package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품번호 조회")
public class ItemNoGetDto {

	@ApiModelProperty(value = "상품번호", required = true, position = 1)
	private String itemNo;

	@ApiModelProperty(value = "업체상품코드", position = 2)
	private String sellerItemCode;

}