package kr.co.homeplus.inbound.claim.model.partner;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimDetailListPartnerGetDto {

    @ApiModelProperty(value = "클레임타입코드", example = "C")
    private String claimTypeCode;

    @ApiModelProperty(value = "클레임타입", example = "취소")
    private String claimType;

    @ApiModelProperty(value = "처리상태", example = "신청")
    private String claimStatus;

    @ApiModelProperty(value = "상품 주문번호", example = "3000012917")
    private String orderItemNo;

    @ApiModelProperty(value = "배송번호", example = "3000000004")
    private String bundleNo;

    @ApiModelProperty(value = "클레임번호", example = "1500000001")
    private String claimBundleNo;

    @ApiModelProperty(value = "클레임 신청수량", example = "2")
    private String claimBundleQty;

    @ApiModelProperty(value = "신청일", example = "2020-12-23 13:20:54")
    private String requestDt;

    @ApiModelProperty(value = "접수채널", example = "MYPAGE")
    private String requestId;

    @ApiModelProperty(value = "신청사유", example = "다른 상품으로 재주문")
    private String claimReasonType;

    @ApiModelProperty(value = "신청사유상세", example = "주문 취소 합니다.")
    private String claimReasonDetail;

    @ApiModelProperty(value = "보류채널", example = "MYPAGE")
    private String pendingId;

    @ApiModelProperty(value = "보류사유", example = "상품 미수거")
    private String pendingReasonType;

    @ApiModelProperty(value = "보류사유상세", example = "보류합니다.")
    private String pendingReasonDetail;

    @ApiModelProperty(value = "보류일시", example = "2020-12-23 13:20:54")
    private String pendingDt;

    @ApiModelProperty(value = "거부채널", example = "PARTNER")
    private String rejectId;

    @ApiModelProperty(value = "거부사유", example = "상품 발송처리")
    private String rejectReasonType;

    @ApiModelProperty(value = "거부사유상세", example = "이미 발송하였습니다.")
    private String rejectReasonDetail;

    @ApiModelProperty(value = "거부일시", example = "2020-12-23 13:20:54")
    private String rejectDt;

    @ApiModelProperty(value = "추가배송비", example = "3000")
    private String addShipPrice;

    @ApiModelProperty(value = "책임사유", example = "구매자")
    private String whoReason;

    @ApiModelProperty(value = "클레임 상품 리스트")
    private List<ClaimDetailItemPartner> claimDetailItemList;
}
