package kr.co.homeplus.inbound.item.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.inbound.constants.PatternConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("상품 판매정보")
public class ItemSaleSetDto {

	@ApiModelProperty(value = "과세여부 (Y:과세, N:비과세)", required = true, position = 1)
	@NotEmpty(message = "과세여부")
	private String taxYn;

	public void setTaxYn(String taxYn) {
		this.taxYn = taxYn.toUpperCase();
	}

	@ApiModelProperty(value = "정상가격 (최대 9자리)", required = true, position = 2)
	private Integer originPrice;

	@ApiModelProperty(value = "판매가격 (최대 9자리)", required = true, position = 3)
	private int salePrice;

	@ApiModelProperty(value = "할인판매여부 기준:할인안함 (Y:할인, N:할인안함)", position = 4)
	@JsonProperty(value = "discountYn")
	@Builder.Default
	private String dcYn = "N";

	public void setDcYn(String dcYn) {
		this.dcYn = dcYn.toUpperCase();
	}

	@ApiModelProperty(value = "할인판매가 (최대 9자리)", position = 5)
	@JsonProperty(value = "discountPrice")
	private Integer dcPrice;

	@ApiModelProperty(value = "특정할인기간여부 기준:할인안함 (Y:특정기간할인, N:특정기간 할인안함)", position = 6)
	@JsonProperty(value = "discountPeriodYn")
	@Builder.Default
	private String dcPeriodYn = "N";

	public void setDcPeriodYn(String dcPeriodYn) {
		this.dcPeriodYn = dcPeriodYn.toUpperCase();
	}

	@ApiModelProperty(value = "할인시작일(YYYY-MM-DD HH:00)", position = 7)
	@Pattern(regexp = PatternConstants.DATE_DF_FORMAT_PATTERN + "||" + PatternConstants.DATE_TIME_PATTERN, message = "할인시작일(YYYY-MM-DD HH:00)")
	@JsonProperty(value = "discountStartDate")
	private String dcStartDt;

	@ApiModelProperty(value = "할인종료일(YYYY-MM-DD HH:59)", position = 8)
	@Pattern(regexp = PatternConstants.DATE_DF_FORMAT_PATTERN + "||" + PatternConstants.DATE_END_TIME_PATTERN, message = "할인종료일(YYYY-MM-DD HH:59)")
	@JsonProperty(value = "discountEndDate")
	private String dcEndDt;

	@ApiModelProperty(value = "판매기간 (Y:설정함, N:설정안함)", required = true, position = 9)
	@NotEmpty(message = "판매기간")
	private String salePeriodYn;

	public void setSalePeriodYn(String salePeriodYn) {
		this.salePeriodYn = salePeriodYn.toUpperCase();
	}

	@ApiModelProperty(value = "판매기간시작일(YYYY-MM-DD HH:00:00)", position = 10)
	@JsonProperty(value = "saleStartDate")
	private String saleStartDt;

	@ApiModelProperty(value = "판매기간종료일(YYYY-MM-DD HH:59:59)", position = 11)
	@JsonProperty(value = "saleEndDate")
	private String saleEndDt;

	@ApiModelProperty(value = "예약판매사용여부 기준:설정안함 (Y:설정함, N:설정안함)", position = 12)
	@JsonProperty(value = "reserveYn")
	@Builder.Default
	private String rsvYn = "N";

	public void setRsvYn(String rsvYn) {
		this.rsvYn = rsvYn.toUpperCase();
	}

	@ApiModelProperty(value = "예약판매시작일(YYYY-MM-DD)", position = 13)
	@JsonProperty(value = "reserveStartDate")
	private String rsvStartDt;

	@ApiModelProperty(value = "예약판매종료일(YYYY-MM-DD)", position = 14)
	@JsonProperty(value = "reserveEndDate")
	private String rsvEndDt;

	@ApiModelProperty(value = "예약상품 출하 시작일(YYYY-MM-DD)", position = 15)
	@JsonProperty(value = "reserveShipStartDate")
	private String shipStartDt;

	@ApiModelProperty(value = "재고수량 (최대 9자리)", required = true, position = 16)
	private Integer stockQty;

	@ApiModelProperty(value = "판매단위수량 (최대 3자리)", required = true, position = 17)
	@JsonProperty(value = "saleUnitQty")
	private Integer saleUnit;

	@ApiModelProperty(value = "최소구매수량 (최대 3자리)", required = true, position = 18)
	private Integer purchaseMinQty;

	@ApiModelProperty(value = "1인당 구매제한 사용여부 기준:제한안함 (Y:제한, N:제한안함)", position = 19)
	@Builder.Default
	private String purchaseLimitYn = "N";

	public void setPurchaseLimitYn(String purchaseLimitYn) {
		this.purchaseLimitYn = purchaseLimitYn.toUpperCase();
	}

	@ApiModelProperty(value = "구매제한 타입 기준:1회 - 1회 : O / 기간제한 : P", position = 20)
	@Builder.Default
	private String purchaseLimitDuration = "O";

	public void setPurchaseLimitDuration(String purchaseLimitDuration) {
		this.purchaseLimitDuration = purchaseLimitDuration.toUpperCase();
	}

	@ApiModelProperty(value = "구매제한 일자 - ?일 (최대 3자리)", position = 21)
	private Integer purchaseLimitDay;

	@ApiModelProperty(value = "구매제한 개수 - ?개 (최대 3자리)", position = 22)
	private Integer purchaseLimitQty;

	@ApiModelProperty(value = "장바구니 제한여부 기준:제한안함 (Y:제한, N:제한안함)", position = 23)
	@Builder.Default
	private String cartLimitYn = "N";

	public void setCartLimitYn(String cartLimitYn) {
		this.cartLimitYn = cartLimitYn.toUpperCase();
	}

	@ApiModelProperty(value = "상품상세설명", required = true, position = 24)
	@JsonProperty(value = "itemDescription")
	@NotEmpty(message = "상품상세설명")
	private String itemDesc;

}