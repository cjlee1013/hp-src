package kr.co.homeplus.inbound.item.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.inbound.constants.ExceptionCode;
import kr.co.homeplus.inbound.constants.PatternConstants;
import kr.co.homeplus.inbound.core.constants.ResourceRouteName;
import kr.co.homeplus.inbound.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.inbound.item.enums.AdultType;
import kr.co.homeplus.inbound.item.model.ItemCertParamSetDto;
import kr.co.homeplus.inbound.item.model.ItemCertSetDto;
import kr.co.homeplus.inbound.item.model.ItemImgParamSetDto;
import kr.co.homeplus.inbound.item.model.ItemImgSetDto;
import kr.co.homeplus.inbound.item.model.ItemNoGetDto;
import kr.co.homeplus.inbound.item.model.ItemNoListParamDto;
import kr.co.homeplus.inbound.item.model.ItemInboundGetDto;
import kr.co.homeplus.inbound.item.model.ItemNoParamDto;
import kr.co.homeplus.inbound.item.model.ItemNoticeParamSetDto;
import kr.co.homeplus.inbound.item.model.ItemNoticeSetDto;
import kr.co.homeplus.inbound.item.model.ItemSetDto;
import kr.co.homeplus.inbound.item.model.ItemSetParamDto;
import kr.co.homeplus.inbound.item.model.ItemStatusSetDto;
import kr.co.homeplus.inbound.response.ResponseResult;
import kr.co.homeplus.inbound.utils.StreamUtil;
import kr.co.homeplus.inbound.utils.StringUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.exception.ResourceClientException;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.ObjectUtils;
import org.codehaus.plexus.util.ExceptionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;

@Slf4j
@Service
@RequiredArgsConstructor
public class ItemService {

    final private ResourceClient resourceClient;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    /**
     * 상품 등록/수정 핸들러
     * @param itemSetDto
     * @param partnerId
     * @return
     * @throws Exception
     */
    public ResponseObject setItemHandler(ItemSetDto itemSetDto, String partnerId, final HttpServletRequest req) throws Exception {

        itemSetDto.getBasic().setPartnerId(partnerId);

        return this.setItem(itemSetDto, req);
    }

    /**
     * 상품 상태변경
     * @param itemStatusSetDto
     * @return
     */
    public ResponseObject setItemStatus(ItemStatusSetDto itemStatusSetDto) {
        String apiUri = "/inbound/in/setItemStatus";

        ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri(apiUri)
                .postObject(itemStatusSetDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.post(request, new TimeoutConfig()).getBody();

    }

    /**
     * 상품번호 리스트 조회 (업체상품코드 기준)
     * @param sellerItemCdList
     * @param partnerId
     * @return
     */
    public List<String> getItemNoList(List<String> sellerItemCdList, String partnerId) {
        ItemNoParamDto itemNoParamDto  = ItemNoParamDto.builder().sellerItemCdList(sellerItemCdList).partnerId(partnerId).build();

        ResourceClientRequest<List<String>> request = ResourceClientRequest.<List<String>>postBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri("/inbound/out/getItemNoList")
                .postObject(itemNoParamDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 상품조회 (상세)
     * @param itemNo
     * @return
     */
    @SuppressWarnings("unchecked")
    public ResponseObject getItem(String itemNo, String partnerId) {

        String apiUri = "/inbound/out/getItem?itemNo="+itemNo+"&partnerId="+partnerId;

        ResourceClientRequest<ItemInboundGetDto> request = ResourceClientRequest.<ItemInboundGetDto>getBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri(apiUri)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();


        ResponseObject<ItemInboundGetDto> item = resourceClient.get(request, new TimeoutConfig()).getBody();

        if (ObjectUtils.isEmpty(item.getData())) {
            item.setReturnMessage("조회된 상품이 없습니다.");
        } else {
            item.getData().getImage().getMainImageList().forEach(o->o.setUrl(hmpImgFrontUrl + o.getUrl()));
            
            if (ObjectUtils.isNotEmpty(item.getData().getImage().getListImage())) {
                item.getData().getImage().getListImage().setUrl(hmpImgFrontUrl + item.getData().getImage().getListImage().getUrl());
            }
        }

        return item;
    }

    /**
     * 상품번호 리스트 조회 (기간)
     * @param itemNoListParamDto
     * @return
     */
    public List<ItemNoGetDto> getItemNoListByDate(ItemNoListParamDto itemNoListParamDto) {

        String apiUri = "/inbound/out/getItemNoListByDate";

        ResourceClientRequest<List<ItemNoGetDto>> request = ResourceClientRequest.<List<ItemNoGetDto>>getBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri(StringUtil.getRequestString(apiUri, ItemNoListParamDto.class, itemNoListParamDto))
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * DS상품 등록/수정
     * @param itemSetDto
     * @return
     * @throws Exception
     */
    public ResponseObject setItem(ItemSetDto itemSetDto, final HttpServletRequest req)
            throws Exception {
        this.setItemDefaultData(itemSetDto);
        ResponseObject responseObject;

        //등록/수정
        try {
            ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                    .apiId(ResourceRouteName.ITEM)
                    .uri("/inbound/in/setItem")
                    .postObject(this.setItemParamData(itemSetDto))
                    .typeReference(new ParameterizedTypeReference<>() {})
                    .build();

            responseObject = resourceClient.post(request, new TimeoutConfig()).getBody();

        } catch (ResourceClientException e) {
            final Throwable cause = e.getCause();
            Map<String, String> setInfo = new HashMap<>(){{
                put("partnerId", itemSetDto.getBasic().getPartnerId());
                put("shipPolicyNo", String.valueOf(itemSetDto.getShip().getShipPolicyNo()));
            }};

            req.setAttribute("setInfo", new JSONObject(setInfo));

            if (cause instanceof RestClientResponseException) {
                throw new ResourceClientException(new URI(req.getRequestURI()), cause);
            } else {
                throw new ResourceClientException("partnerId : " + itemSetDto.getBasic().getPartnerId() + ExceptionUtils.getStackTrace(e));
            }

        }

        return responseObject;
    }

    /**
     * 상품 기준값 셋팅
     * @param itemSetDto
     */
    private void setItemDefaultData(ItemSetDto itemSetDto) {
        //성인상품 이미지 노출여부
        if (itemSetDto.getEtc().getAdultType().equals(AdultType.ADULT.getType()) && ObjectUtils.isEmpty(itemSetDto.getEtc().getImgDispYn())) {
            itemSetDto.getEtc().setImgDispYn("N");
        }
    }

    /**
     * 상품 데이터 셋팅
     * @param itemSetDto
     * @return
     */
    private ItemSetParamDto setItemParamData(ItemSetDto itemSetDto) throws BusinessLogicException {
        ItemSetParamDto itemSetParamDto = ItemSetParamDto.builder().build();

        if (ObjectUtils.isEmpty(itemSetDto.getBasic())) {
            throw new BusinessLogicException(ExceptionCode.ERROR_CODE_1003, "기본정보");
        } else if (ObjectUtils.isEmpty(itemSetDto.getSale())) {
            throw new BusinessLogicException(ExceptionCode.ERROR_CODE_1003, "판매정보");
        } else if (ObjectUtils.isEmpty(itemSetDto.getOption())) {
            throw new BusinessLogicException(ExceptionCode.ERROR_CODE_1003, "옵션정보");
        } else if (ObjectUtils.isEmpty(itemSetDto.getProperty())) {
            throw new BusinessLogicException(ExceptionCode.ERROR_CODE_1003, "속성정보");
        } else if (ObjectUtils.isEmpty(itemSetDto.getShip())) {
            throw new BusinessLogicException(ExceptionCode.ERROR_CODE_1003, "배송정보");
        } else if (ObjectUtils.isEmpty(itemSetDto.getEtc())) {
            throw new BusinessLogicException(ExceptionCode.ERROR_CODE_1003, "부가정보");
        }

        //데이터 셋팅
        BeanUtils.copyProperties(itemSetDto.getBasic(), itemSetParamDto);
        BeanUtils.copyProperties(itemSetDto.getSale(), itemSetParamDto);
        BeanUtils.copyProperties(itemSetDto.getOption(), itemSetParamDto);
        BeanUtils.copyProperties(itemSetDto.getProperty(), itemSetParamDto);
        BeanUtils.copyProperties(itemSetDto.getShip(), itemSetParamDto);
        BeanUtils.copyProperties(itemSetDto.getEtc(), itemSetParamDto);

        //할인기간 데이터 형식 셋팅
        if (itemSetParamDto.getDcPeriodYn().equals("Y")) {
            if (ObjectUtils.isEmpty(itemSetParamDto.getDcStartDt())) {
                throw new BusinessLogicException(ExceptionCode.ERROR_CODE_1019, "할인시작일(YYYY-MM-DD HH:00)");
            }

            if (ObjectUtils.isEmpty(itemSetParamDto.getDcEndDt())) {
                throw new BusinessLogicException(ExceptionCode.ERROR_CODE_1019, "할인종료일(YYYY-MM-DD HH:59)");
            }

            Pattern dateDfFormatPattern = Pattern.compile(PatternConstants.DATE_DF_FORMAT_PATTERN);

            if (dateDfFormatPattern.matcher(itemSetParamDto.getDcStartDt()).matches()) {
                itemSetParamDto.setDcStartDt(itemSetParamDto.getDcStartDt() + " 00:00:00");
            } else {
                itemSetParamDto.setDcStartDt(itemSetParamDto.getDcStartDt() + ":00");
            }

            if (dateDfFormatPattern.matcher(itemSetParamDto.getDcEndDt()).matches()) {
                itemSetParamDto.setDcEndDt(itemSetParamDto.getDcEndDt() + " 23:59:59");
            } else {
                itemSetParamDto.setDcEndDt(itemSetParamDto.getDcEndDt() + ":59");
            }

        } else {
            itemSetParamDto.setDcCommissionType(null);
            itemSetParamDto.setDcStartDt(null);
            itemSetParamDto.setDcEndDt(null);
        }

        //고시정보 셋팅
        List<ItemNoticeParamSetDto> noticeList = new ArrayList<>();

        itemSetParamDto.setNoticeList(new ArrayList<>());
        for (ItemNoticeSetDto dto : itemSetDto.getProperty().getNoticeList()) {
            ItemNoticeParamSetDto noticeDto = new ItemNoticeParamSetDto();
            BeanUtils.copyProperties(dto, noticeDto);

            noticeList.add(noticeDto);
        }

        itemSetParamDto.setNoticeList(noticeList);

        //이미지 셋팅
        List<ItemImgParamSetDto> imgList = new ArrayList<>();

        itemSetParamDto.setImgList(new ArrayList<>());
        if (ObjectUtils.isNotEmpty(itemSetDto.getImage())) {
            for (ItemImgSetDto dto : itemSetDto.getImage()) {
                ItemImgParamSetDto imgDto = new ItemImgParamSetDto();
                BeanUtils.copyProperties(dto, imgDto);

                imgList.add(imgDto);
            }

            imgList.get(0).setMainYn("Y");
        }

        itemSetParamDto.setImgList(imgList);

        //안전인증 셋팅
        List<ItemCertParamSetDto> certList = new ArrayList<>();
        ItemCertParamSetDto itemCertSetDto;

        if (itemSetDto.getProperty().getKcItemIsCertification().equals("Y")) {
            itemCertSetDto = ItemCertParamSetDto.builder().isCert("Y")
                    .certGroup("IT")
                    .certType(itemSetDto.getProperty().getKcItemCertificationType())
                    .certNo(itemSetDto.getProperty().getKcItemCertificationNo()).build();
        } else {
            itemCertSetDto = ItemCertParamSetDto.builder().isCert("N").certGroup("IT").build();
        }

        certList.add(itemCertSetDto);

        if (itemSetDto.getProperty().getKcKidIsCertification().equals("Y")) {
            itemCertSetDto = ItemCertParamSetDto.builder().isCert("Y")
                    .certGroup("KD")
                    .certType(itemSetDto.getProperty().getKcKidCertificationType())
                    .certNo(itemSetDto.getProperty().getKcKidCertificationNo()).build();
        } else {
            itemCertSetDto = ItemCertParamSetDto.builder().isCert("N").certGroup("KD").build();
        }

        certList.add(itemCertSetDto);

        if (itemSetDto.getProperty().getKcElectricIsCertification().equals("Y")) {
            List<String> kcElectricTypeList = new ArrayList<>(Arrays.asList("ER_SPL", "ER_D"));
            List<ItemCertSetDto> kcElectricList = new ArrayList<>();

            if (ObjectUtils.isNotEmpty(itemSetDto.getProperty().getKcElectricCertificationList())) {
                if (itemSetDto.getProperty().getKcElectricCertificationList().size() > 10) {
                    throw new BusinessLogicException(ExceptionCode.ERROR_CODE_1009, "전파인증 인증정보", 10);
                }

                for (String str : kcElectricTypeList) {
                    List<ItemCertSetDto> kcElectricTmpList = itemSetDto.getProperty().getKcElectricCertificationList().stream().filter(o-> o.getCertType().equals(str)).collect(Collectors.toList());
                    kcElectricList.addAll(kcElectricTmpList.stream().filter(StreamUtil.distinctByKey(ItemCertSetDto::getCertNo)).collect(Collectors.toList()));
                }
            }

            if (kcElectricList.size() == 0) {
                throw new BusinessLogicException(ExceptionCode.ERROR_CODE_1019, "전파인증 인증정보");
            }

            for (ItemCertSetDto certDto : kcElectricList) {
                itemCertSetDto = ItemCertParamSetDto.builder().isCert("Y")
                        .certGroup("ER")
                        .certType(certDto.getCertType())
                        .certNo(certDto.getCertNo()).build();

                certList.add(itemCertSetDto);
            }

        } else {
            certList.add(ItemCertParamSetDto.builder().isCert("N").certGroup("ER").build());
        }

        if (itemSetDto.getProperty().getKcEtcIsCertification().equals("Y")) {
            List<String> kcEtcTypeList = new ArrayList<>(Arrays.asList("ET_LF_CH_ITEM", "ET_MEDI", "ET_D"));
            List<ItemCertSetDto> kcEtcList = new ArrayList<>();

            if (ObjectUtils.isNotEmpty(itemSetDto.getProperty().getKcEtcCertificationList())) {
                if (itemSetDto.getProperty().getKcEtcCertificationList().size() > 10) {
                    throw new BusinessLogicException(ExceptionCode.ERROR_CODE_1009, "기타인증 인증정보", 10);
                }

                for (String str : kcEtcTypeList) {
                    List<ItemCertSetDto> kcEtcTmpList = itemSetDto.getProperty().getKcEtcCertificationList().stream().filter(o-> o.getCertType().equals(str)).collect(Collectors.toList());
                    kcEtcList.addAll(kcEtcTmpList.stream().filter(StreamUtil.distinctByKey(ItemCertSetDto::getCertNo)).collect(Collectors.toList()));
                }
            }

            if (kcEtcList.size() == 0) {
                throw new BusinessLogicException(ExceptionCode.ERROR_CODE_1019, "기타인증 인증정보");
            }


            for (ItemCertSetDto certDto : kcEtcList) {
                itemCertSetDto = ItemCertParamSetDto.builder().isCert("Y")
                        .certGroup("ET")
                        .certType(certDto.getCertType())
                        .certNo(certDto.getCertNo()).build();

                certList.add(itemCertSetDto);
            }

        } else {
            certList.add(ItemCertParamSetDto.builder().isCert("N").certGroup("ET").build());
        }

        itemSetParamDto.setCertList(certList);

        return itemSetParamDto;
    }

}
