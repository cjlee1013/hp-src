package kr.co.homeplus.inbound.item.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("이미지 정보")
public class ItemImgSetDto {

	@ApiModelProperty(value = "메인(MN) 대표이미지 여부 - 한개의 이미지만 설정 (Y:대표이미지 설정, N:대표이미지 설정안함)", required = true, position = 1, hidden = true)
	@Builder.Default
	private String	mainYn = "N";

	@ApiModelProperty(value = "이미지 URL (최대 200자) ( 메인 이미지 사이즈: 1200 x 1200 / 용량: 4MB 이하 / 파일 : JPG, JPEG, PNG ) ( 리스트 이미지 사이즈: 900 x 470 / 용량: 4MB 이하 / 파일 : JPG, JPEG, PNG )", required = true, position = 2)
	@JsonProperty(value = "imageUrl")
	@NotEmpty(message = "이미지 URL")
	private String 	imgUrl;

	@ApiModelProperty(value = "이미지 유형 (MN: 메인(최대:3개), LST: 리스트(최대:1개)) - 메인 이미지의 경우 첫번째 이미지가 대표이미지가 됩니다.", required = true, position = 3)
	@JsonProperty(value = "imageType")
	@NotEmpty(message = "이미지 유형")
	@Pattern(regexp = "(MN|LST)", message="이미지 유형")
	private String 	imgType;

	public void setImgType(String imgType) {
		this.imgType = imgType.toUpperCase();
	}

}
