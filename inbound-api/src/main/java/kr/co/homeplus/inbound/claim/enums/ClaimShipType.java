package kr.co.homeplus.inbound.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimShipType {

        DELIVERY("C", "DELIVERY")
    ,   POST("P", "POST")
    ,   DIRECT("D", "DIRECT")
    ,   NOSHIP("N", "NOSHIP")
    ,   NONE("NONE", "NONE")
    ,   C("C", "DELIVERY")
    ,   P("P", "POST")
    ,   D("D", "DIRECT")
    ,   N("N", "NOSHIP")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;


}
