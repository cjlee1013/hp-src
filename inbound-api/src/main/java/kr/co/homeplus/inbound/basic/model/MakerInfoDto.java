package kr.co.homeplus.inbound.basic.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("제조사 정보")
public class MakerInfoDto {
	@ApiModelProperty(value = "제조사번호", position = 1)
	private long makerNo;

	@ApiModelProperty(value = "제조사명(최대 15자)", position = 2)
	private String makerName;
}
