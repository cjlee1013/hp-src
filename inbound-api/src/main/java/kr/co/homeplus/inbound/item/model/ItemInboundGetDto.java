package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 상세정보 Get Entry
 */
@Getter
@Setter
@ApiModel("상품 상세 조회")
public class ItemInboundGetDto {

	@ApiModelProperty(value = "기본정보", position = 1)
	private ItemBasicGetDto basic;

	@ApiModelProperty(value = "판매정보", position = 2)
	private ItemSaleInboundGetDto sale;

	@ApiModelProperty(value = "이미지정보", position = 3)
	private ItemImgInboundGetDto image;

	@ApiModelProperty(value = "옵션정보", position = 4)
	private ItemOptInboundGetDto option;

	@ApiModelProperty(value = "배송정보", position = 5)
	private ItemShipInboundGetDto ship;

	@ApiModelProperty(value = "속성정보", position = 6)
	private ItemPropInboundGetDto property;

	@ApiModelProperty(value = "부가정보", position = 7)
	private ItemEtcInboundGetDto etc;

}