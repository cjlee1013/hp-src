package kr.co.homeplus.inbound.core.config.swagger;


import static springfox.documentation.builders.PathSelectors.ant;

import com.google.common.base.Predicates;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class EtcSwaggerConfig {

    @Value("${api.swagger-config-host}")
    private String swaggerConfigHost;

    @Value("${api.swagger-config-protocol}")
    private String swaggerConfigProtocol;

    final private String docVersion = "1.0.0";


    @Bean
    @SuppressWarnings("unchecked")
    public Docket etcApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .host(swaggerConfigHost)
                .protocols(Sets.newHashSet(swaggerConfigProtocol))
                .groupName("etc")
                .tags(new Tag("QNA 등록 관리", ""), new Tag("공지사항조회", ""))
                .select()
                .paths(Predicates.or(ant("/qna/*/**"), ant("/notice/*/**")))
                .build()
                .apiInfo(this.etcApiInfo());
    }


    private ApiInfo etcApiInfo() {
        return new ApiInfoBuilder()
                .title(SwaggerConfig.SWAGGER_CONFIG_TITLE + " (기타)")
                .description(SwaggerConfig.SWAGGER_CONFIG_DESCRIPTION)
                .version(docVersion)
                .contact(new Contact(SwaggerConfig.SWAGGER_CONFIG_CONTACT_NAME, null, SwaggerConfig.SWAGGER_CONFIG_CONTACT_EMAIL))
                .build();
    }
}
