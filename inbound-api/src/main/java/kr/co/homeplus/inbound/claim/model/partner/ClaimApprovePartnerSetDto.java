package kr.co.homeplus.inbound.claim.model.partner;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import kr.co.homeplus.inbound.claim.enums.ClaimType;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimApproveItemSet;
import lombok.Data;


@Data
public class ClaimApprovePartnerSetDto {
    @NotNull(message = "클레임번호")
    @ApiModelProperty(value = "클레임번호", position = 1, example = "1500000022")
    private long claimNo;

    @NotNull(message = "클레임번들번호")
    @ApiModelProperty(value = "클레임번들번호", position = 2, example = "1500000022")
    private long claimBundleNo;

    @NotNull(message = "클레임 타입")
    @ApiModelProperty(value = "클레임 타입", position = 3, example = "R")
    private String claimType;

    public ClaimApprovePartnerSetDto(ClaimApproveItemSet claimApproveItem) {
        this.claimNo = claimApproveItem.getClaimNo();
        this.claimBundleNo = claimApproveItem.getClaimBundleNo();
        String claimType = claimApproveItem.getClaimType();
        if (claimType != null) {
            this.claimType = ClaimType.valueOf(claimType).getType();
        }
    }
}
