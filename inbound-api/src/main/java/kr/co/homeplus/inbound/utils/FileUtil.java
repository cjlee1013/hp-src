package kr.co.homeplus.inbound.utils;

import java.net.HttpURLConnection;
import java.net.URL;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;

/**
 * File Util
 */
@Slf4j
public class FileUtil {

    // 파일 존재유무
    public static boolean isUrlFileExists(final String fileUrl) {
        try {
            HttpURLConnection.setFollowRedirects(false);

            HttpURLConnection con = (HttpURLConnection) new URL(fileUrl).openConnection();
            con.setRequestMethod(HttpMethod.HEAD.name());
            con.setConnectTimeout(5000); // 5초
            con.setReadTimeout(10000); // 10초

            int responseCode = con.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK
                    || responseCode == HttpURLConnection.HTTP_MOVED_TEMP
                    || responseCode == HttpURLConnection.HTTP_NOT_MODIFIED) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
}
