package kr.co.homeplus.inbound.itemRelation.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@ApiModel("연관상품관리 조회")
@Getter
@Setter
public class ItemRelationListGetDto {

    @ApiModelProperty(value = "연관번호", position = 5)
    private long relationNo;

    @ApiModelProperty(value = "연관상품명", position = 15)
    private String relationName;

    @ApiModelProperty(value = "노출여부명", position = 25)
    private String displayYnName;

    @ApiModelProperty(value = "PC노출기준", position = 55)
    private String displayPcType;

    @ApiModelProperty(value = "모바일노출기준", position = 60)
    private String displayMobileType;

    @ApiModelProperty(value = "인트로이미지URL", position = 65)
    private String introImageUrl;

    @ApiModelProperty(value = "사용여부명", position = 80)
    private String useYnName;

    @ApiModelProperty(value = "상품번호", position = 85)
    List<String> relationItemList;

}