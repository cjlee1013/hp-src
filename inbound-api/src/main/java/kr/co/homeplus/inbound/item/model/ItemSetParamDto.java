package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.List;
import kr.co.homeplus.inbound.constants.ChannelConstants;
import kr.co.homeplus.inbound.constants.CodeConstants;
import kr.co.homeplus.inbound.item.enums.MallType;
import kr.co.homeplus.inbound.item.enums.StoreType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("상품 등록 Set Entry")
public class ItemSetParamDto {
	@ApiModelProperty(value = "유입채널 (등록/수정) : (ADMIN: 어드민, PARTNER: 파트너, INBOUND: 인바운드)")
	@Builder.Default
	private String channel = ChannelConstants.INBOUND;

	/**
	 * 기본정보
	 */
	@ApiModelProperty(value = "상품번호")
	private String itemNo;

	@ApiModelProperty(value = "상품명")
	private String itemNm;

	@ApiModelProperty(value = "상품상태(A:판매중,P:일시중지,S:영구중지)")
	private String itemStatus;

	@ApiModelProperty(value = "상품유형 (N:새상품, U:중고, R:리퍼, B:반품(리세일), C:주문제작)")
	private String itemType;

	@ApiModelProperty(value = "업체상품코드")
	private String sellerItemCd;

	@ApiModelProperty(value = "세카테고리")
	private int dcateCd;

	@ApiModelProperty(value = "판매업체ID")
	private String partnerId;

	@ApiModelProperty(value = "거래유형 (DS : 업체상품 , TD : 매장상품)")
	@Builder.Default
	private String mallType = MallType.DS.getType();

	@ApiModelProperty(value = "성인상품유형 (NORMAL: 일반, ADULT: 성인, LOCAL_LIQUOR: 전통주)")
	private String adultType;

	@ApiModelProperty(value = "이미지노출 여부 (Y:노출, N:노출안함)")
	private String imgDispYn;

	@ApiModelProperty(value = "노출여부")
	private String dispYn;

	@ApiModelProperty(value = "브랜드번호")
	private Long   brandNo;

	@ApiModelProperty(value = "제조사번호")
	private Long   makerNo;

	@ApiModelProperty(value = "텍스트옵션사용여부 (사용:Y,미사용:N)")
	private String optTxtUseYn;

	@ApiModelProperty(value = "등록/수정자")
	@Builder.Default
	private String userId = CodeConstants.SYSTEM_USER_ID;

	@ApiModelProperty(value = "제조일자")
	private String makeDt;

	@ApiModelProperty(value = "유효일자")
	private String expDt;

	/**
	 * 판매정보
	 */
	@ApiModelProperty(value = "과세여부 (Y:과세,N:비과세)")
	private String taxYn;

	@ApiModelProperty(value = "정상가격")
	private Integer originPrice;

	@ApiModelProperty(value = "판매가격")
	private int salePrice;

	@ApiModelProperty(value = "판매기간시작일")
	private String saleStartDt;

	@ApiModelProperty(value = "판매기간종료일")
	private String saleEndDt;

	@ApiModelProperty(value = "수수료 부과 기준(정률:Rate,정액:Amount)")
	private String commissionType;

	@ApiModelProperty(value = "수수료율(%)")
	private BigDecimal commissionRate;

	@ApiModelProperty(value = "수수료금액")
	private Integer commissionPrice;

	@ApiModelProperty(value = "할인판매가")
	private Integer dcPrice;

	@ApiModelProperty(value = "할인기간여부")
	private String dcYn;

	@ApiModelProperty(value = "특정할인기간여부")
	private String dcPeriodYn;

	@ApiModelProperty(value = "할인시작일")
	private String dcStartDt;

	@ApiModelProperty(value = "할인종료일")
	private String dcEndDt;

	@ApiModelProperty(value = "할인수수료타입")
	private String dcCommissionType;

	@ApiModelProperty(value = "할인수수료율")
	private BigDecimal dcCommissionRate;

	@ApiModelProperty(value = "할인수수료액")
	private Integer dcCommissionPrice;

	@ApiModelProperty(value = "판매기간 기준-설정함/설정안함(Y/N)")
	private String salePeriodYn;

	@ApiModelProperty(value = "예약판매사용여부")
	private String rsvYn;

	@ApiModelProperty(value = "예약판매번호")
	private Long saleRsvNo;

	@ApiModelProperty(value = "예약판매시작일")
	private String rsvStartDt;

	@ApiModelProperty(value = "예약판매종료일")
	private String rsvEndDt;

	@ApiModelProperty(value = "예약상품출하시작일")
	private String shipStartDt;

	@ApiModelProperty(value = "재고수량")
	private Integer stockQty;

	@ApiModelProperty(value = "판매단위")
	private Integer saleUnit;

	@ApiModelProperty(value = "구매제한 - 최소구매수량")
	private Integer purchaseMinQty;

	@ApiModelProperty(value = "1인당 구매제한 사용여부 - 미사용:Y / 사용:N")
	private String purchaseLimitYn;

	@ApiModelProperty(value = "구매제한 타입 - 1회 : O / 기간제한 : P")
	private String purchaseLimitDuration;

	@ApiModelProperty(value = "구매제한 일자 - ?일")
	private Integer purchaseLimitDay;

	@ApiModelProperty(value = "구매제한 개수 - ?개")
	private Integer purchaseLimitQty;

	@ApiModelProperty(value = "장바구니 제한여부")
	private String cartLimitYn;

	@ApiModelProperty(value = "점포ID")
	private Integer storeId;

	@ApiModelProperty(value = "점포유형")
	@Builder.Default
	private String storeType = StoreType.DS.getType();

	/**
	 * 이미지정보 
	 */
	@ApiModelProperty(value = "이미지정보")
	private List<ItemImgParamSetDto> imgList;

	/**
	 * 배송정보
	 */
	@ApiModelProperty(value = "배송정책번호")
	private Long shipPolicyNo;

	@ApiModelProperty(value = "반품/교환 배송비")
	private int claimShipFee;

	@ApiModelProperty(value = "출고지 우편번호")
	private String  releaseZipcode;

	@ApiModelProperty(value = "출고지 주소1(우편번호제공)")
	private String  releaseAddr1;

	@ApiModelProperty(value = "출고지 주소2(직접입력)")
	private String  releaseAddr2;

	@ApiModelProperty(value = "회수지 우편번호")
	private String  returnZipcode;

	@ApiModelProperty(value = "회수지 주소1(우편번호제공)")
	private String  returnAddr1;

	@ApiModelProperty(value = "회수지 주소2(직접입력)")
	private String  returnAddr2;

	@ApiModelProperty(value = "출고기한(-1:미정)")
	private Integer releaseDay;

	@ApiModelProperty(value = "출고기한 1일 인 경우 출고시간(1~24)")
	private Integer releaseTime;

	@ApiModelProperty(value = "휴일제외여부")
	private String  holidayExceptYn;

	@ApiModelProperty(value = "안심번호 서비스 여부")
	private String  safeNumberUseYn;


	@ApiModelProperty(value = "배송 정책명")
	private String 	shipPolicyNm;

	@ApiModelProperty(value = "배송유형명(ITEM:상품별/BUNDLE:묶음배송)")
	private String 	shipKindNm;

	@ApiModelProperty(value = "배송방법명 (DS/TD_DLV/DRCT/POST/PICK)")
	private String 	shipMethodNm;

	@ApiModelProperty(value = "배송비 유형명 ( FREE : 무료 / FIXED : 유료 / COND : 조건부 무료 )")
	private String 	shipTypeNm;

	@ApiModelProperty(value = "배송비")
	private String 	shipFee;

	@ApiModelProperty(value = "선결제여부명")
	private String 	prepaymentYnNm;

	@ApiModelProperty(value = "배송비노출여부명")
	private String 	shipDispYnNm;

	@ApiModelProperty(value = "차등설정")
	private String 	diffYn;

	@ApiModelProperty(value = "차등개수")
	private String 	diffQty;

	/**
	 * 상품 상세정보
	 */
	@ApiModelProperty(value = "상품상세정보")
	private String itemDesc;

	/**
	 * 텍스트옵션 정보
	 */
	@ApiModelProperty(value = "텍스트형 옵션단계")
	private int    optTxtDepth;

	@ApiModelProperty(value = "텍스트형 1단계 타이틀")
	private String opt1Text;

	@ApiModelProperty(value = "텍스트형 2단계 타이틀")
	private String opt2Text;

	/**
	 * 부가정보
	 */
	@ApiModelProperty(value = "속성번호 리스트")
	private List<Integer> attrNoList;

	@ApiModelProperty(value = "상품고시정보 정책코드")
	private int gnoticeNo;

	@ApiModelProperty(value = "상품고시정보 리스트")
	private List<ItemNoticeParamSetDto> noticeList;

	@ApiModelProperty(value = "가격비교 등록여부")
	private String epYn;

	@ApiModelProperty(value = "외부연동 사이트")
	private String linkageYn;

	@ApiModelProperty(value = "검색 키워드")
	private String srchKeyword;

	@ApiModelProperty(value = "해외배송 여부")
	private String globalAgcyYn;

	@ApiModelProperty(value = "월렌탈료")
	private int rentalFee;

	@ApiModelProperty(value = "렌탈 의무사용기간")
	private int rentalPeriod;

	@ApiModelProperty(value = "렌탈 등록비")
	private int rentalRegFee;

	@ApiModelProperty(value = "선물하기사용여부 (Y: 사용, N: 사용안함)")
	private String giftYn;

	@ApiModelProperty(value = "증빙서류 리스트")
	private List<ItemProofListSetDto> proofFileList;

	@ApiModelProperty(value = "안전인증 리스트")
	private List<ItemCertParamSetDto> certList;

	@ApiModelProperty(value = "ISBN")
	private String isbn;

	@ApiModelProperty(value = "제휴사, 마켓연동 리스트", hidden = true)
	private List<ItemProviderSetDto> providerList;
}
