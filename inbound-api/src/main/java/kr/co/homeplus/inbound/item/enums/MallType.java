package kr.co.homeplus.inbound.item.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum MallType {

        DS( "DS", "업체상품")
    ,   TD("TD", "매장상품");

    @Getter
    private final String type;

    @Getter
    private final String description;


}
