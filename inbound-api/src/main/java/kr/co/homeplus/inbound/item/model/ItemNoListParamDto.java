package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품번호 리스트 조회")
public class ItemNoListParamDto {

    @ApiModelProperty(value = "검색 날짜유형(R: 생성일 기준, M: 수정일 기준)", required = true)
    @NotEmpty(message = "검색 날짜유형")
    private String searchDateType;

    @ApiModelProperty(value = "검색 시작일 날짜(YYYY-MM-DD)", required = true)
    @NotEmpty(message = "검색 시작일")
    private String searchStartDate;

    @ApiModelProperty(value = "검색 종료일 날짜(YYYY-MM-DD)", required = true)
    @NotEmpty(message = "검색 종료일")
    private String searchEndDate;

    @ApiModelProperty(value = "파트너ID", required = true, hidden = true)
    private String partnerId;
}
