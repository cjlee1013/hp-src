package kr.co.homeplus.inbound.basic.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("카테고리 정보")
public class CategoryDspDto {
	@ApiModelProperty(value = "세분류 카테고리 코드 (상품 등록 시 카테고리 입력을 위한 Refit 카테고리 번호)", position = 5)
	private String divideCategoryCode;

	@ApiModelProperty(value = "세분류 카테고리 명(최대 50자)", position = 10)
	private String divideCategoryName;

	@ApiModelProperty(value = "소분류 카테고리 코드", position = 15)
	private String smallCategoryCode;

	@ApiModelProperty(value = "소분류 카테고리 명(최대 50자)", position = 20)
	private String smallCategoryName;

	@ApiModelProperty(value = "중분류 카테고리 코드", position = 22)
	private String middleCategoryCode;

	@ApiModelProperty(value = "중분류 카테고리 명(최대 50자)", position = 25)
	private String middleCategoryName;

	@ApiModelProperty(value = "대분류 카테고리 코드", position = 27)
	private String largeCategoryCode;

	@ApiModelProperty(value = "대분류 카테고리 명(최대 50자)", position = 30)
	private String largeCategoryName;

	@ApiModelProperty(value = "도서카테고리여부 (Y:사용, N:사용안함)", position = 35)
	private String bookCategoryUseYn;
}
