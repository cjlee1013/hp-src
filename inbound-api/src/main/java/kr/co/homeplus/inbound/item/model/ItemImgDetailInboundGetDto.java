package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("상품 이미지 상세정보")
public class ItemImgDetailInboundGetDto {
	@ApiModelProperty(value = "이미지경로", required = true)
	private String url;
}
