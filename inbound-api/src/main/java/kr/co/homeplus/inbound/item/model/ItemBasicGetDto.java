package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 기본정보 Get Entry
 */
@Getter
@Setter
@ApiModel("상품 기본정보 조회")
public class ItemBasicGetDto {

	/**
	 * 기본정보
	 */
	@ApiModelProperty(value = "상품번호", required = true, position = 1)
	private String itemNo;

	@ApiModelProperty(value = "상품상태명", position = 2)
	private String itemStatusName;

	@ApiModelProperty(value = "상품명", required = true, position = 3)
	private String itemName;

	@ApiModelProperty(value = "상품유형 (N:새상품, U:중고, R:리퍼, B:반품(리세일), C:주문제작)", required = true, position = 4)
	private String itemTypeName;

	/**
	 * 카테고리정보
	 */
	@ApiModelProperty(value = "세카테고리", required = true, position = 5)
	private Integer divideCategoryCode;

	@ApiModelProperty(value = "노출여부명", position = 6)
	private String displayYnName;

	@ApiModelProperty(value = "업체상품코드", position = 7)
	private String sellerItemCode;

}