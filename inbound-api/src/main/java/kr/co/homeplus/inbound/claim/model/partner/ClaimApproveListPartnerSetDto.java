package kr.co.homeplus.inbound.claim.model.partner;

import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimApproveSet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimApproveItemSet;
import lombok.Data;


@Data
public class ClaimApproveListPartnerSetDto {

    @ApiModelProperty(notes = "취소 승인 요청 dto")
    private List<ClaimApprovePartnerSetDto> claimRequestSetList;

    public ClaimApproveListPartnerSetDto(ClaimApproveSet claimApproveSet) {
        this.claimRequestSetList = new ArrayList<>();
        for (ClaimApproveItemSet claimApproveItem : claimApproveSet.getApproveList()){
            claimRequestSetList.add(new ClaimApprovePartnerSetDto(claimApproveItem));
        }
    }
}
