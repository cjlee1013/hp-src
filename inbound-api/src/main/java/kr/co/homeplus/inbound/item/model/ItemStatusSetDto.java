package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품 상태수정")
public class ItemStatusSetDto {

	@ApiModelProperty(value = "상품번호", position = 1)
	private String itemNo;

	@ApiModelProperty(value = "상품상태(A:판매중,P:일시중지)", position = 2)
	private String itemStatus;

	@ApiModelProperty(value = "파트너 아이디" , hidden = true)
	private String partnerId;

}

