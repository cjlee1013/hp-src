package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("주문,배송 요청")
public class OrderBundleSet {
    @ApiModelProperty(notes = "[Optional]배송상태(NEWORDER:신규주문, CONFIRM:상품준비중, SHIPPING:배송중, COMPLETE:배송완료, DECISION:구매확정)", example = "NEWORDER", position = 1)
    private String shipStatus;

    @ApiModelProperty(notes = "검색기간 타입(ORDERDATE:결제일 ,CONFIRMDATE: 주문확인일)", example = "ORDERDATE", position = 2)
    private String dateType;

    @ApiModelProperty(notes = "시작일", example = "2020-01-01", position = 3)
    private String startDate;

    @ApiModelProperty(notes = "종료일", example = "2020-01-01", position = 4)
    private String endDate;

    @ApiModelProperty(notes = "배송번호", example = "3000000001", position = 4)
    private String bundleNo;
}
