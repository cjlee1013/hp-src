package kr.co.homeplus.inbound.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimModifyType {

        ADDR("ADDR", "ADDRESS")
    ,   STATUS("STATUS", "STATUS")
    ,   TYPE("TYPE", "SHIPPINGTYPE")
    ,   ALL("ALL", "ALL")
    ,   ADDRESS("ADDR", "ADDRESS")
    ,   SHIPPINGTYPE("TYPE", "SHIPPINGTYPE")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;


}
