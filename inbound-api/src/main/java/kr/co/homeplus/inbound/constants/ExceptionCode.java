package kr.co.homeplus.inbound.constants;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * [공통] 에러코드 정의
 * 1001 ~ 8999 사용자 에러코드
 * 9000 ~ 9999 시스템에러 SP에러(9999) 등등
 */
@RequiredArgsConstructor
public enum ExceptionCode {

		SYS_ERROR_CODE_9001("-9001", "시스템 에러 : 관리자에게 문의해 주세요.")
	,   SYS_ERROR_CODE_9002("-9002", "시스템 에러 : 첨부파일 크기가 허용한도를 초과합니다.")
	,   SYS_ERROR_CODE_9003("-9003", "시스템 에러 : 첨부파일 등록 처리중 문제가 발생했습니다.")

	,	SYS_ERROR_CODE_9101("-9101", "통신 에러 : 잘못된 URL을 호출 하였습니다.")
	,   SYS_ERROR_CODE_9102("-9102", "통신 에러 : 내부 오류가 발생하였습니다.")
	,   SYS_ERROR_CODE_9103("-9103", "통신 에러 : API 연결에 실패하였습니다.")
	,   SYS_ERROR_CODE_9104("-9104", "통신 에러 : API 요청 대기시간이 만료되었습니다.")
	,   SYS_ERROR_CODE_9105("-9105", "통신 에러 : 잘못된 URL을 호출 하였습니다.")
	,   SYS_ERROR_CODE_9106("-9106", "통신 에러 : 요청이 허용되지 않는 메소드입니다.")

	,   SYS_ERROR_CODE_9200("-9200", "인증 에러 : 로그인 인증에 실패하였습니다.")
	,   SYS_ERROR_CODE_9201("-9201", "인증 에러 : 로그인 쿠키에서 오류가 발생하였습니다.")
	,   SYS_ERROR_CODE_9203("-9203", "인가 에러 : 메뉴 인가여부를 체크하는 도중 오류가 발생하였습니다.")
	,   SYS_ERROR_CODE_9204("-9204", "인가 에러 : 해당 메뉴에 접근권한이 없습니다.")
	,   SYS_ERROR_CODE_9205("-9205", "인가 에러 : 권한 체크하는 도중 오류가 발생하였습니다..")

	,   SYS_ERROR_CODE_9301("-9301", "필수 파라미터가 누락되었습니다.")
	,   SYS_ERROR_CODE_9302("-9302", "파라미터 유효성 검사 오류입니다.")
	,   SYS_ERROR_CODE_9303("-9303", "입력값이 형식에 맞지 않습니다.")
	,   SYS_ERROR_CODE_9304("-9304", "입력값이 지원하지 않는 타입입니다.")

	,   ERROR_CODE_1001("1001", " 파라미터 유효성 검사 오류입니다.")
	,   ERROR_CODE_1002("1002", " 필수값을 입력해주세요.")
	,   ERROR_CODE_1003("1003", "(%d) 필수값을 입력해주세요.")
	,   ERROR_CODE_1004("1004", " 파라미터 타입 오류입니다.")
	,   ERROR_CODE_1005("1005", " 자 사이로 입력해주세요.")
	,   ERROR_CODE_1006("1006", " %d 파라미터 유효성 검사 오류입니다.")
	,   ERROR_CODE_1007("1007", " 자 이하로 입력해주세요.")
	,   ERROR_CODE_1008("1008", "%d 개 이상 등록해주세요.")
	,   ERROR_CODE_1009("1009", "(%d) %s개 이하 등록해주세요.")
	,   ERROR_CODE_1014("1014", "API KEY가 유효기간이 지났습니다. (%d)")
	,   ERROR_CODE_1015("1015", "요청한 서비스를 호출할 수 없는 API KEY입니다.")
	,   ERROR_CODE_1016("1016", "API KEY 오류입니다. (%d)")
	,   ERROR_CODE_1017("1017", "데이터가 없습니다.")
	,   ERROR_CODE_1018("1018", "API KEY가 유효하지 않습니다.")
	,   ERROR_CODE_1019("1019", "(%d)를 확인해주세요.")
	,   ERROR_CODE_1020("1020", "이미 사용중인 아이디입니다. 다른 아이디를 입력해주세요.")
	,   ERROR_CODE_1021("1021", " 형식에 맞지 않습니다.")
	,   ERROR_CODE_1022("1022", " 초과의 값으로 입력해주세요.")
	,   ERROR_CODE_1023("1023", " 미만의 값으로 입력해주세요.")
	,   ERROR_CODE_1024("1024", "동일한 카테고리명이 존재합니다. 다른 카테고리명으로 변경해주세요.")
	,   ERROR_CODE_1025("1025", "이미 등록된 %d입니다.")
	,   ERROR_CODE_1028("1028", " 이상으로 입력해주세요.")
	,   ERROR_CODE_1029("1029", " 이하로 입력해주세요.")
	,   ERROR_CODE_1090("1090", "(%d) 줄바꿈 공백없이 입력해주세요.")


	,   ERROR_CODE_5000("5000", "브랜드 정보가 없습니다.")
	,   ERROR_CODE_5001("5001", "카테고리 정보가 없습니다.")
	,   ERROR_CODE_5002("5002", "검색태그 정보가 없습니다.")
	,   ERROR_CODE_5003("5003", "제조사 정보가 없습니다.")
	,   ERROR_CODE_5004("5004", "고시정보 정보가 없습니다.")
	,   ERROR_CODE_5005("5005", "판매자의 배송정책 정보가 없습니다.")

	,   ERROR_CODE_6000("6000", "연관상품 정보가 없습니다.")
	,   ERROR_CODE_6001("6001", "연관상품은 50개 까지 등록 가능합니다.")

	,   ERROR_CODE_7001("7001", "요청하신 데이터가 없습니다.")
	,   ERROR_CODE_7002("7002", "조회기간은 최대 5일까지 가능합니다.")
	;

	@Getter
	private final String code;
	@Getter
	private final String description;

	@Override
	public String toString() {
		return code + ": " + description;
	}
}
