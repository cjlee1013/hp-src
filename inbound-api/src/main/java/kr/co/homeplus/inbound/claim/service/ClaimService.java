package kr.co.homeplus.inbound.claim.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import kr.co.homeplus.inbound.claim.enums.ClaimType;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimDetailGet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimDetailListGet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimListGet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimListSet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimRegisterSet;
import kr.co.homeplus.inbound.claim.model.partner.ClaimDetailListPartnerGetDto;
import kr.co.homeplus.inbound.claim.model.partner.ClaimDetailPartnerGetDto;
import kr.co.homeplus.inbound.claim.model.partner.ClaimListPartnerSetDto;
import kr.co.homeplus.inbound.claim.model.partner.ClaimListPartnerGetDto;
import kr.co.homeplus.inbound.claim.model.partner.ClaimRegPartnerSetDto;
import kr.co.homeplus.inbound.constants.ExceptionCode;
import kr.co.homeplus.inbound.core.constants.ResourceRouteName;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimService {

    @Autowired
    private final ResourceClient resourceClient;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    /**
     * PO 클레임 리스트 조회
     * **/
    public List<ClaimListGet> getClaimList(ClaimListSet claimListSet,String partnerId) {
        String apiUri = "/po/claim/getPOClaimList";
        ClaimListPartnerSetDto claimListPartnerSetDto = new ClaimListPartnerSetDto(claimListSet, partnerId);

        List<ClaimListGet> claimListGetDtoList = new ArrayList<>();
        try {
            List<ClaimListPartnerGetDto> claimListGetDtoListPartner = resourceClient.postForResponseObject(
                ResourceRouteName.ESCROWMNG, claimListPartnerSetDto, apiUri,
                new ParameterizedTypeReference<ResponseObject<List<ClaimListPartnerGetDto>>>() {
                })
                .getData();
            for (ClaimListPartnerGetDto claimListPartnerGetDto : claimListGetDtoListPartner) {
                claimListGetDtoList.add(new ClaimListGet(claimListPartnerGetDto));
            }
            return claimListGetDtoList;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * PO 클레임 상세 조회
     * **/
    public List<ClaimDetailListGet> getClaimDetailList(long purchaseOrderNo, long bundleNo,String partnerId) {
        String apiUri = "/po/claim/getClaimDetailList";
        try {
        List<ClaimDetailListPartnerGetDto> claimDetailListPartnerGetDtoList = resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri + "?purchaseOrderNo=" + purchaseOrderNo + "&bundleNo=" + bundleNo,
            new ParameterizedTypeReference<ResponseObject<List<ClaimDetailListPartnerGetDto>>>() {}).getData();
        List<ClaimDetailListGet> claimDetailListGetDtoList = new ArrayList<>();
        for (ClaimDetailListPartnerGetDto claimDetailListPartnerGetDto : claimDetailListPartnerGetDtoList) {
            claimDetailListGetDtoList.add(new ClaimDetailListGet(claimDetailListPartnerGetDto));
        }
        return claimDetailListGetDtoList;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 클레임 단건 상세 조회
     * **/
    public ClaimDetailGet getClaimDetail(long claimBundleNo, String claimType,String partnerId) {
        String apiUri = "/po/claim/getClaimDetail";
        String claimTypeEnum;
        if (claimType != null) {
            claimTypeEnum = ClaimType.valueOf(claimType).getType();
        } else {
            return null;
        }
        try {
            ClaimDetailPartnerGetDto claimDetailPartnerGetDto = resourceClient.getForResponseObject(
                ResourceRouteName.ESCROWMNG,
                apiUri + "?claimBundleNo=" + claimBundleNo + "&claimType=" + claimTypeEnum,
                new ParameterizedTypeReference<ResponseObject<ClaimDetailPartnerGetDto>>() {
                })
                .getData();
            return new ClaimDetailGet(claimDetailPartnerGetDto, hmpImgFrontUrl);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 클레임 신청
     * **/
    public ResponseObject<String> claimRegister(ClaimRegisterSet claimRegisterSet,String partnerId){
        ClaimRegPartnerSetDto claimRegPartnerSetDto = new ClaimRegPartnerSetDto(claimRegisterSet,partnerId);
        try {
            return resourceClient.postForResponseObject(
                ResourceRouteName.ESCROWMNG,
                claimRegPartnerSetDto,
                "/claim/claimCancelReg",
                new ParameterizedTypeReference<ResponseObject<String>>() {
                }
            );
        } catch (Exception e) {
            return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_7001.getCode(), ExceptionCode.ERROR_CODE_7001.getDescription());
        }
    }

    private boolean validDate(String startDt,String endDt) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREAN);
        Date startDate, endDate;
        try {
            startDate = df.parse(startDt);
            endDate = df.parse(endDt);
            long between = Math.abs(startDate.getTime() - endDate.getTime());
            if (TimeUnit.DAYS.convert(between, TimeUnit.MILLISECONDS) > 5) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
