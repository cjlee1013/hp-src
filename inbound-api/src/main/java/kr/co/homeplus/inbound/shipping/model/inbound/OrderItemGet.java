package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.inbound.shipping.enums.ShipMethod;
import kr.co.homeplus.inbound.shipping.enums.ShipStatus;
import kr.co.homeplus.inbound.shipping.model.partner.OrderShipManageListGetDto;
import lombok.Data;

@Data
@ApiModel("주문내역")
public class OrderItemGet {
    @ApiModelProperty(notes = "배송SEQ", example = "3000000001", position = 1)
    private String shipNo;

    @ApiModelProperty(notes = "상품주문번호", example = "3000000001", position = 2)
    private String orderItemNo;

    @ApiModelProperty(notes = "배송상태", example = "NEWORDER", position = 3)
    private String shipStatus;

    @ApiModelProperty(notes = "상품번호", example = "10000000000001", position = 4)
    private String itemNo;

    @ApiModelProperty(notes = "상품명", example = "홈플러스 시그니처", position = 5)
    private String itemName;

    @ApiModelProperty(notes = "옵션", example = "화이트", position = 6)
    private String textOption;

    @ApiModelProperty(notes = "수량", example = "3", position = 7)
    private String itemQty;

    @ApiModelProperty(notes = "상품금액", example = "1000", position = 8)
    private String itemPrice;

    @ApiModelProperty(notes = "총상품금액", example = "3000", position = 9)
    private String orderPrice;

    @ApiModelProperty(notes = "배송방법", example = "DELIVERY", position = 12)
    private String shipMethod;

    @ApiModelProperty(notes = "택배사", example = "대한통운", position = 13)
    private String deliveryName;

    @ApiModelProperty(notes = "송장번호", example = "1234567890", position = 14)
    private String invoiceNo;

    @ApiModelProperty(notes = "배송요청일", example = "2021-01-15", position = 15)
    private String reserveShipDate;

    @ApiModelProperty(notes = "배송예정일", example = "2021-01-15", position = 16)
    private String scheduleShipDate;

    @ApiModelProperty(notes = "배송메세지", example = "부재시 경비실에 맡겨주세요", position = 17)
    private String deliveryShipMessage;

    @ApiModelProperty(notes = "주문확인일", example = "2021-01-01 12:00:00", position = 20)
    private String confirmDate;

    @ApiModelProperty(notes = "발송기한", example = "2021-01-10", position = 21)
    private String originShipDate;

    @ApiModelProperty(notes = "2차발송기한", example = "2021-01-20", position = 22)
    private String delayShipDate;

    @ApiModelProperty(notes = "배송완료일", example = "2021-01-15", position = 23)
    private String completeDate;

    @ApiModelProperty(notes = "업체상품코드", example = "1234567890", position = 25)
    private String sellerItemCode;

    public OrderItemGet(
        OrderShipManageListGetDto orderShipManageListGetDto) {
        this.shipNo = orderShipManageListGetDto.getShipNo();
        this.orderItemNo = orderShipManageListGetDto.getOrderItemNo();
        this.shipStatus = ShipStatus.valueOf(orderShipManageListGetDto.getShipStatus()).getDescription();
        this.itemNo = orderShipManageListGetDto.getItemNo();
        this.itemName = orderShipManageListGetDto.getItemNm1();
        this.textOption = orderShipManageListGetDto.getTxtOptVal();
        this.itemQty = orderShipManageListGetDto.getItemQty();
        this.itemPrice = orderShipManageListGetDto.getItemPrice();
        this.orderPrice = orderShipManageListGetDto.getOrderPrice();
        this.shipMethod = ShipMethod.valueOf(orderShipManageListGetDto.getShipMethod()).getDescription();
        this.deliveryName = orderShipManageListGetDto.getDlvCdNm();
        this.invoiceNo = orderShipManageListGetDto.getInvoiceNo();
        this.reserveShipDate = orderShipManageListGetDto.getReserveShipDt();
        this.scheduleShipDate = orderShipManageListGetDto.getScheduleShipDt();
        this.deliveryShipMessage = orderShipManageListGetDto.getDlvShipMsg();
        this.confirmDate = orderShipManageListGetDto.getConfirmDt();
        this.originShipDate = orderShipManageListGetDto.getOrgShipDt();
        this.delayShipDate = orderShipManageListGetDto.getDelayShipDt();
        this.completeDate = orderShipManageListGetDto.getCompleteDt();
        this.sellerItemCode = orderShipManageListGetDto.getSellerItemCd();
    }
}