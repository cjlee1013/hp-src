package kr.co.homeplus.inbound.applog.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("EscrowOpenLog")
public class EscrowOpenLogSet {
    @ApiModelProperty(value = "현재페이지", position = 1)
    private String url;
    @ApiModelProperty(value = "현재페이지 타이틀", position = 2)
    private String title;
    @ApiModelProperty(value = "불러올 페이지 URL", position = 3)
    private String loadUrl;
    @ApiModelProperty(value = "Hplus 값", position = 4)
    private String hplus;
    @ApiModelProperty(value = "Ahplus 값", position = 5)
    private String ahplus;
}
