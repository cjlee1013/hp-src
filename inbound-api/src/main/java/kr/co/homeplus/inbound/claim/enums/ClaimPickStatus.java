package kr.co.homeplus.inbound.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimPickStatus {

        NN("NN", "READY")
    ,   N0("N0", "REQUEST")
    ,   P0("P0", "CONFIRM")
    ,   P1("P1", "SCHEDULE")
    ,   P2("P2", "SHIPPING")
    ,   P3("P3", "COMPLETE")
    ,   P8("P8", "FAIL")
    ,   READY("NN", "READY")
    ,   REQUEST("N0", "REQUEST")
    ,   CONFIRM("P0", "CONFIRM")
    ,   SCHEDULE("P1", "SCHEDULE")
    ,   SHIPPING("P2", "SHIPPING")
    ,   COMPLETE("P3", "COMPLETE")
    ,   FAIL("P8", "FAIL")
    ,   ALL("ALL","ALL")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;


}
