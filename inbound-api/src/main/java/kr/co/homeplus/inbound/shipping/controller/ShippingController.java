package kr.co.homeplus.inbound.shipping.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.inbound.claim.service.ClaimService;
import kr.co.homeplus.inbound.common.service.CommonValidService;
import kr.co.homeplus.inbound.constants.ExceptionCode;
import kr.co.homeplus.inbound.shipping.model.inbound.DeliveryCodeGet;
import kr.co.homeplus.inbound.shipping.model.inbound.OrderClaimPossibleListGet;
import kr.co.homeplus.inbound.shipping.model.inbound.OrderConfirmSet;
import kr.co.homeplus.inbound.shipping.model.inbound.OrderBundleGet;
import kr.co.homeplus.inbound.shipping.model.inbound.OrderBundleSet;
import kr.co.homeplus.inbound.shipping.model.inbound.OrderDetailGet;
import kr.co.homeplus.inbound.shipping.model.inbound.ShipDelaySet;
import kr.co.homeplus.inbound.shipping.model.inbound.ShipCompleteSet;
import kr.co.homeplus.inbound.shipping.model.inbound.ShipStartGet;
import kr.co.homeplus.inbound.shipping.model.inbound.ShipStartSet;
import kr.co.homeplus.inbound.shipping.service.ShippingService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 판매관리 > 발주/발송관리
 */
@Api(value = "test1" ,tags = "주문/배송")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found")
    ,@ApiResponse(code = 405, message = "Method Not Allowed")
    ,@ApiResponse(code = 422, message = "Unprocessable Entity")
    ,@ApiResponse(code = 500, message = "Internal Server Error")
})
@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/shipping")
public class ShippingController {

    @Autowired
    private ShippingService shippingService;
    @Autowired
    private CommonValidService commonValidService;
    @Autowired
    private final ClaimService claimService;

    /**
     * 주문 리스트 조회
     */
    @ResponseBody
    @PostMapping(value = "/getOrderList")
    @ApiOperation(value = "주문 리스트 조회", tags = "test2")
    public ResponseObject<List<OrderBundleGet>> getOrderShipManageList(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "orderBundleSet", value = "주문,배송 요청", required = true) @RequestBody OrderBundleSet orderBundleSet) throws Exception {
        List<OrderBundleGet> order = shippingService.getOrderShipManageList(orderBundleSet,"getOrderShipManageList",commonValidService.validApiKeyHandler(apiKey));
        if (order == null) {
            return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_7002.getCode(), ExceptionCode.ERROR_CODE_7002.getDescription());
        } else if (order.size() == 0) {
            return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_7001.getCode(), ExceptionCode.ERROR_CODE_7001.getDescription());
        }
        return ResourceConverter.toResponseObject(order);
    }

    /**
     * 배송 리스트 조회
     */
    @ResponseBody
    @PostMapping(value = "/getShipList")
    @ApiOperation(value = "배송 리스트 조회")
    public ResponseObject<List<OrderBundleGet>> getShipStatusList(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "orderBundleSet", value = "주문,배송 요청", required = true) @RequestBody OrderBundleSet orderBundleSet) throws Exception {
        List<OrderBundleGet> order = shippingService.getOrderShipManageList(orderBundleSet,"getShipStatusList",commonValidService.validApiKeyHandler(apiKey));
        if (order == null) {
            return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_7002.getCode(), ExceptionCode.ERROR_CODE_7002.getDescription());
        } else if (order.size() == 0) {
            return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_7001.getCode(), ExceptionCode.ERROR_CODE_7001.getDescription());
        }
        return ResourceConverter.toResponseObject(order);
    }

    /**
     * 주문 정보 상세
     */
    @ResponseBody
    @GetMapping(value = "/getOrderDetail")
    @ApiOperation(value = "주문 정보 상세")
    public ResponseObject<OrderDetailGet> getOrderDetail(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "bundleNo", value = "배송번호", required = true) @RequestParam String bundleNo) throws Exception {
        OrderDetailGet orderDetailGet = shippingService.getOrderDetail(bundleNo,commonValidService.validApiKeyHandler(apiKey));
        if (orderDetailGet == null) {
            return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_7001.getCode(), ExceptionCode.ERROR_CODE_7001.getDescription());
        }
        return ResourceConverter.toResponseObject(orderDetailGet);
    }

    /**
     * 주문취소 팝업 조회
     */
    @ResponseBody
    @GetMapping(value = "/getClaimPossibleOrderList")
    @ApiOperation(value = "클레임가능주문리스트")
    public ResponseObject<List<OrderClaimPossibleListGet>> getClaimPossibleOrderList(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "purchaseOrderNo", value = "주문번호", required = true) @RequestParam String purchaseOrderNo,
        @ApiParam(name = "bundleNo", value = "배송번호", required = true) @RequestParam String bundleNo
    ) throws Exception {
        List<OrderClaimPossibleListGet> orderClaimPossibleList = shippingService.getClaimPossibleOrderList(purchaseOrderNo, bundleNo, "", commonValidService.validApiKeyHandler(apiKey));
        if (orderClaimPossibleList == null) {
            return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_7001.getCode(), ExceptionCode.ERROR_CODE_7001.getDescription());
        }
        return ResourceConverter.toResponseObject(orderClaimPossibleList);
    }

    /**
     * 주문 확인
     */
    @ResponseBody
    @PostMapping(value = "/setConfirmOrder")
    @ApiOperation(value = "주문 확인")
    public ResponseObject<Integer> setConfirmOrder(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "orderConfirmSet", value = "주문확인 요청", required = true) @RequestBody OrderConfirmSet orderConfirmSet) throws Exception {
        return shippingService.setConfirmOrder(orderConfirmSet,commonValidService.validApiKeyHandler(apiKey));
    }

    /**
     * 발송지연안내
     */
    @ResponseBody
    @PostMapping(value = "/setShipDelay")
    @ApiOperation(value = "발송지연안내")
    public ResponseObject<Integer> setNotiDelay(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "shipDelaySet", value = "발송지연 요청", required = true) @RequestBody ShipDelaySet shipDelaySet) throws Exception {
        return shippingService.setNotiDelay(shipDelaySet,commonValidService.validApiKeyHandler(apiKey));
    }

    /**
     * 발송 처리
     */
    @ResponseBody
    @PostMapping("/setInvoice")
    @ApiOperation(value = "발송 처리")
    public ResponseObject<ShipStartGet> setInvoice(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "shipStartList", value = "송장 수정 요청", required = true) @RequestBody List<ShipStartSet> shipStartList) throws Exception {
        return shippingService.setShipAllExcel(shipStartList,commonValidService.validApiKeyHandler(apiKey));
    }

    /**
     * 배송완료
     */
    @ResponseBody
    @PostMapping(value = "/setShipComplete")
    @ApiOperation(value = "배송완료")
    public ResponseObject<Integer> setShipComplete(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "shipCompleteSet", value = "배송번호 요청", required = true) @RequestBody ShipCompleteSet shipCompleteSet) throws Exception {
        return shippingService.setShipComplete(shipCompleteSet,commonValidService.validApiKeyHandler(apiKey));
    }

    /**
     * 택배사 코드 조회
     */
    @ResponseBody
    @PostMapping(value = "/getDeliveryCode")
    @ApiOperation(value = "택배사 코드 조회")
    public ResponseObject<List<DeliveryCodeGet>> getDeliveryCode() throws Exception {
        List<DeliveryCodeGet> deliveryCodeGetList = shippingService.getDeliveryCode();
        return ResourceConverter.toResponseObject(deliveryCodeGetList);
    }
}
