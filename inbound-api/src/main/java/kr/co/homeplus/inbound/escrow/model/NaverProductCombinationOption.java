package kr.co.homeplus.inbound.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ApiModel(value="네이버 옵션상품")
@Getter
@Setter
@ToString
public class NaverProductCombinationOption {

    @ApiModelProperty(value = "옵션ID", position = 1)
    private String id;
    @ApiModelProperty(value = "옵션판매자관리코드", position = 2)
    private String sellerManagementCode;
    @ApiModelProperty(value = "상품가격", position = 3)
    private Long price;
    @ApiModelProperty(value = "옵션가격", position = 4)
    private Long optionPrice;
    @ApiModelProperty(value = "옵션수량", position = 5)
    private Integer quantity;
    @ApiModelProperty(value = "옵션구매상태", position = 6)
    private String resultCode = "FAIL_QUANTITY";

}
