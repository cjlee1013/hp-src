package kr.co.homeplus.inbound.claim.model.inbound;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.inbound.claim.enums.ClaimExchangeStatus;
import kr.co.homeplus.inbound.claim.enums.ClaimPickStatus;
import kr.co.homeplus.inbound.claim.enums.ClaimShipType;
import kr.co.homeplus.inbound.claim.enums.ClaimStatus;
import kr.co.homeplus.inbound.claim.enums.ClaimType;
import kr.co.homeplus.inbound.claim.model.partner.ClaimListPartnerGetDto;
import kr.co.homeplus.inbound.shipping.enums.ShipMethod;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("클레임 목록 응답")
public class ClaimListGet {

    @ApiModelProperty(value = "클레임번호", example = "1500000001")
    private String claimBundleNo;

    @ApiModelProperty(value = "클레임그룹번호", example = "1500000011")
    private String claimNo;

    @ApiModelProperty(value = "클레임요청번호", example = "1500000011")
    private String claimRequestNo;

    @ApiModelProperty(value = "주문번호", example = "3000004971")
    private String purchaseOrderNo;

    @ApiModelProperty(value = "배송번호", example = "3000000004")
    private String bundleNo;

    @ApiModelProperty(value = "상품 주문번호", example = "3000012917")
    private String orderItemNo;

    @ApiModelProperty(value = "처리상태", example = "REQUEST")
    private String claimStatus;

    @ApiModelProperty(value = "클레임타입", example = "CANCEL")
    private String claimType;

    @ApiModelProperty(value = "신청일", example = "2020-12-23 13:20:54")
    private String requestDate;

    @ApiModelProperty(value = "신청사유", example = "다른 상품으로 재주문")
    private String claimReasonType;

    @ApiModelProperty(value = "처리일", example = "2020-12-23 13:20:54")
    private String processDate;

    @ApiModelProperty(value = "승인일", example = "2020-12-23 13:20:54")
    private String approveDate;

    @ApiModelProperty(value = "상품번호", example = "10000300000265")
    private String itemNo;

    @ApiModelProperty(value = "상품명", example = "[상품] 테스트_사라_알라크림치즈")
    private String itemName;

    @ApiModelProperty(value = "옵션명", example = "1번옵션 : 옵션입니다. | 2번옵션 : 옵션입니다.")
    private String optionItemName;

    @ApiModelProperty(value = "신청수량", example = "1")
    private String claimItemQty;

    @ApiModelProperty(value = "상품금액", example = "20000")
    private String itemPrice;

    @ApiModelProperty(value = "총 상품금액", example = "480000")
    private String totalItemPrice;

    @ApiModelProperty(value = "거부사유", example = "상품 발송처리")
    private String rejectReasonType;

    @ApiModelProperty(value = "구매자", example = "홍길동")
    private String buyerName;

    @ApiModelProperty(value = "구매자연락처", example = "010-1111-1111")
    private String buyerMobileNo;

    @ApiModelProperty(value = "수령인", example = "홍길동")
    private String receiverName;

    @ApiModelProperty(value = "수령인연락처", example = "010-1111-1111")
    private String receiverMobileNo;

    @ApiModelProperty(value = "우편번호", example = "07297")
    private String zipcode;

    @ApiModelProperty(value = "주소", example = "서울특별시 영등포구 당산로 42 (문래동3가)")
    private String address;

    @ApiModelProperty(value = "배송메시지", example = "직접 받고 부재 시 문 앞에 놓아주세요")
    private String deliveryShipMessage;

    @ApiModelProperty(value = "배송비지급", example = "선불")
    private String shipPriceType;

    @ApiModelProperty(value = "수거상태", example = "CONFIRM")
    private String pickStatus;

    @ApiModelProperty(value = "수거완료일", example = "2020-12-23 13:20:54")
    private String pickCompleteDate;

    @ApiModelProperty(value = "반품배송비", example = "3000")
    private String returnShipAmt;

    @ApiModelProperty(value = "배송비부담주체", example = "구매자")
    private String whoReason;

    @ApiModelProperty(value = "클레임비용결제 동봉/차감", example = "동봉")
    private String claimShipFeeEnclose;

    @ApiModelProperty(value = "배송방법", example = "DELIVERY")
    private String shipMethod;

    @ApiModelProperty(value = "택배사", example = "002")
    private String deliveryCode;

    @ApiModelProperty(value = "송장번호", example = "12312312313")
    private String invoiceNo;

    @ApiModelProperty(value = "수거방법", example = "업체직배송")
    private String pickShipType;

    @ApiModelProperty(value = "수거택배사", example = "002")
    private String pickDeliveryCode;

    @ApiModelProperty(value = "수거송장번호", example = "12312312313")
    private String pickInvoiceNo;

    @ApiModelProperty(value = "보류설정일", example = "2020-12-29 14:20:05")
    private String pendingDate;

    @ApiModelProperty(value = "수거지 우편번호", example = "07297")
    private String pickZipCode;

    @ApiModelProperty(value = "수거지주소", example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    private String pickAddress;

    @ApiModelProperty(value = "교환배송상태", example = "CONFIRM")
    private String exchangeStatus;

    @ApiModelProperty(value = "교환배송 방법", example = "택배배송")
    private String exchangeShipType;

    @ApiModelProperty(value = "교환배송 택배사", example = "한진택배")
    private String exchangeDeliveryCode;

    @ApiModelProperty(value = "교환배송 송장번호", example = "123123123")
    private String exchangeInvoiceNo;

    @ApiModelProperty(value = "교환배송 처리일", example = "2020-12-29 00:00:00")
    private String exchangeRegisterDate;

    @ApiModelProperty(value = "교환배송지 우편번호", example = "08584")
    private String exchangeZipcode;

    @ApiModelProperty(value = "교환배송지주소", example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    private String exchangeAddress;

    @ApiModelProperty(value = "업체상품코드", example = "12343423")
    private String sellerItemCode;

    @ApiModelProperty(value = "업체옵션코드", example = "12343423")
    private String sellerOptionCode;

    @ApiModelProperty(value = "업체관리코드", example = "12343423")
    private String sellerManageCode;

    public ClaimListGet(ClaimListPartnerGetDto claimListPartnerGetDto) {
        this.claimBundleNo = claimListPartnerGetDto.getClaimBundleNo();
        this.claimNo = claimListPartnerGetDto.getClaimNo();
        this.claimRequestNo = claimListPartnerGetDto.getClaimReqNo();
        this.purchaseOrderNo = claimListPartnerGetDto.getPurchaseOrderNo();
        this.bundleNo = claimListPartnerGetDto.getBundleNo();
        this.orderItemNo = claimListPartnerGetDto.getOrderItemNo();
        String claimStatusCode = claimListPartnerGetDto.getClaimStatusCode();
        if (claimStatusCode != null) {
            this.claimStatus = ClaimStatus.valueOf(claimStatusCode).getDescription();
        }
        String claimType = claimListPartnerGetDto.getClaimType();
        if (claimType != null) {
            this.claimType = ClaimType.valueOf(claimType).getDescription();
        }
        this.requestDate = claimListPartnerGetDto.getRequestDt();
        this.claimReasonType = claimListPartnerGetDto.getClaimReasonType();
        this.processDate = claimListPartnerGetDto.getProcessDt();
        this.itemNo = claimListPartnerGetDto.getItemNo();
        this.itemName = claimListPartnerGetDto.getItemName();
        this.optionItemName = claimListPartnerGetDto.getOptItemNm();
        this.claimItemQty = claimListPartnerGetDto.getClaimItemQty();
        this.itemPrice = claimListPartnerGetDto.getItemPrice();
        this.totalItemPrice = claimListPartnerGetDto.getTotItemPrice();
        this.rejectReasonType = claimListPartnerGetDto.getRejectReasonType();
        this.buyerName = claimListPartnerGetDto.getBuyerNm();
        this.buyerMobileNo = claimListPartnerGetDto.getBuyerMobileNo();
        this.receiverName = claimListPartnerGetDto.getReceiverNm();
        this.receiverMobileNo = claimListPartnerGetDto.getShipMobileNo();
        this.zipcode = claimListPartnerGetDto.getZipcode();
        this.address = claimListPartnerGetDto.getAddr();
        this.deliveryShipMessage = claimListPartnerGetDto.getDlvShipMsg();
        this.shipPriceType = claimListPartnerGetDto.getShipPriceType();
        this.sellerItemCode = claimListPartnerGetDto.getSellerItemCd();
        this.sellerOptionCode = claimListPartnerGetDto.getSellerOptCd();
        this.sellerManageCode = claimListPartnerGetDto.getSellerManageCd();
        String pickStatusCode = claimListPartnerGetDto.getPickStatusCode();
        if (pickStatusCode != null){
            this.pickStatus = ClaimPickStatus.valueOf(pickStatusCode).getDescription();
        }
        this.pickCompleteDate = claimListPartnerGetDto.getPickP3Dt();
        this.returnShipAmt = claimListPartnerGetDto.getReturnShipAmt();
        this.processDate = claimListPartnerGetDto.getProcessDt();
        this.claimShipFeeEnclose = claimListPartnerGetDto.getClaimShipFeeEnclose();
        String exchStatusCode = claimListPartnerGetDto.getExchStatusCode();
        if (exchStatusCode != null) {
            this.exchangeStatus = ClaimExchangeStatus.valueOf(exchStatusCode).getDescription();
        }
        this.whoReason = claimListPartnerGetDto.getWhoReason();
        this.shipMethod = claimListPartnerGetDto.getShipMethod();
        this.deliveryCode = claimListPartnerGetDto.getDlvCd();
        this.invoiceNo = claimListPartnerGetDto.getInvoiceNo();
        this.pickShipType = claimListPartnerGetDto.getPickShipType();
        this.pickDeliveryCode = claimListPartnerGetDto.getPickDlvCd();
        this.pickInvoiceNo = claimListPartnerGetDto.getPickInvoiceNo();
        this.pendingDate = claimListPartnerGetDto.getPendingDt();
        this.pickZipCode = claimListPartnerGetDto.getPickZipCode();
        this.pickAddress = claimListPartnerGetDto.getPickAddr();
        String exchangeShipType = claimListPartnerGetDto.getExchShipType();
        if (exchangeShipType != null) {
            this.exchangeShipType = ClaimShipType.valueOf(exchangeShipType).getDescription();
        }
        this.exchangeDeliveryCode = claimListPartnerGetDto.getExchDlvCd();
        this.exchangeInvoiceNo = claimListPartnerGetDto.getExchInvoiceNo();
        this.exchangeRegisterDate = claimListPartnerGetDto.getExchD0Dt();
        this.exchangeZipcode = claimListPartnerGetDto.getExchZipcode();
        this.exchangeAddress = claimListPartnerGetDto.getExchAddr();
        this.approveDate = claimListPartnerGetDto.getApproveDt();
    }
}
