package kr.co.homeplus.inbound.core.config.swagger;

import static springfox.documentation.builders.RequestHandlerSelectors.basePackage;

import com.google.common.collect.Sets;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    protected static final String SWAGGER_CONFIG_TITLE = "홈플러스 외부연동 API 가이드";
    protected static final String SWAGGER_CONFIG_DESCRIPTION = "";
    protected static final String SWAGGER_CONFIG_CONTACT_NAME = "홈플러스 담당자";
    protected static final String SWAGGER_CONFIG_CONTACT_EMAIL = "hmp-api@homeplus.co.kr";


    @Value("${api.swagger-config-host}")
    private String swaggerConfigHost;

    @Value("${api.swagger-config-protocol}")
    private String swaggerConfigProtocol;

    final private String docVersion = "1.0.0";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .host(swaggerConfigHost)
                .protocols(Sets.newHashSet(swaggerConfigProtocol))
                .select()
                .apis(basePackage("kr.co.homeplus.inbound"))
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(this.apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(SwaggerConfig.SWAGGER_CONFIG_TITLE)
                .description(SwaggerConfig.SWAGGER_CONFIG_DESCRIPTION)
                .version(docVersion)
                .contact(new Contact(SwaggerConfig.SWAGGER_CONFIG_CONTACT_NAME, null,
                        SwaggerConfig.SWAGGER_CONFIG_CONTACT_EMAIL))
                .build();
    }
}
