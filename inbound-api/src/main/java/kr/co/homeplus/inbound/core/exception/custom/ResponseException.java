package kr.co.homeplus.inbound.core.exception.custom;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

/**
 * 예외 발생시 예외내용을 response하기 위한 클래스<p>
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ResponseException extends Exception {
    private HttpStatus status;
    private String code;
    private String title;
    private String detail;

    public ResponseException(HttpStatus status, String code, String title, String detail) {
        this.status = status;
        this.code = code;
        this.title = title;
        this.detail = detail;
    }

    public ResponseException(HttpStatus status, String title, String detail) {
        this.status = status;
        this.title = title;
        this.detail = detail;
    }

}
