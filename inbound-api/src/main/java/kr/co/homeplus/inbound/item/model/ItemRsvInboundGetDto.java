package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 예약 판매정보
 */
@Getter
@Setter
@ApiModel("상품 예약 판매정보")
public class ItemRsvInboundGetDto {

	@ApiModelProperty(value = "예약판매번호")
	private long saleRsvNo;

	@ApiModelProperty(value = "예약판매시작일")
	private String rsvStartDt;

	@ApiModelProperty(value = "예약판매종료일")
	private String rsvEndDt;

	@ApiModelProperty(value = "예약상품출하시작일")
	private String rsvShipStartDt;

	@ApiModelProperty(value = "예약상품출하종료일시")
	private String rsvShipEndDt;

}
