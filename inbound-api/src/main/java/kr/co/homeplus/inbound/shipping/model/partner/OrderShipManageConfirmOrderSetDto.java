package kr.co.homeplus.inbound.shipping.model.partner;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.inbound.shipping.model.inbound.OrderConfirmSet;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 발주/발송관리 주문확인 요청 DTO")
public class OrderShipManageConfirmOrderSetDto {
    @ApiModelProperty(notes = "배송번호 리스트")
    private List<String> bundleNoList;

    @ApiModelProperty(notes = "수정자")
    private String chgId;

    public OrderShipManageConfirmOrderSetDto(
        OrderConfirmSet orderConfirmSet, String chgId) {
        this.bundleNoList = orderConfirmSet.getBundleNoList();
        this.chgId = chgId;
    }
}