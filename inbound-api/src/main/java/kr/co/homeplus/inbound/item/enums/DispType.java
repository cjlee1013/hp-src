package kr.co.homeplus.inbound.item.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum DispType {

    ALL( "ALL", "전체")
    ,   CATE("CATE", "카테고리")
    ,   ITEM( "ITEM", "상품별")
    ,   SELLER("SELLER", "판매자별");

    @Getter
    private final String type;

    @Getter
    private final String description;


}
