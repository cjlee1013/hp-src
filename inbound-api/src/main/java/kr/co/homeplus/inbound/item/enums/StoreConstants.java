package kr.co.homeplus.inbound.item.enums;

/**
 * 점포코드 관련 공통 상수
 */
public class StoreConstants {
    // 기준점포 번호 금천점 37
    public static final String DEFAULT_STORE_ID = "37";

}
