package kr.co.homeplus.inbound.basic.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kr.co.homeplus.inbound.basic.model.BrandInfoDto;
import kr.co.homeplus.inbound.basic.model.CategoryDspDto;
import kr.co.homeplus.inbound.basic.model.ItemNoticeDto;
import kr.co.homeplus.inbound.basic.model.MakerInfoDto;
import kr.co.homeplus.inbound.basic.model.SearchTagDto;
import kr.co.homeplus.inbound.basic.model.SellerShipDto;
import kr.co.homeplus.inbound.basic.service.BasicService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "상품 기초데이터 조회")
@ApiResponses(value = {
        @ApiResponse(code = 422, message = "Unprocessable Entity")
})
@RequestMapping("/basic")
@RestController
@RequiredArgsConstructor
public class BasicController {
    final private BasicService basicService;

    @ApiOperation(value = "브랜드 조회", response = BrandInfoDto.class)
    @GetMapping(value = "/brand/getBrandList")
    public ResponseObject getBrandList(
        @ApiParam(value = "apiKey", required = true) @RequestHeader(value = "apiKey") String apiKey
    ) throws Exception {
        return ResourceConverter.toResponseObject(basicService.getBrandList(apiKey));
    }

    @ApiOperation(value = "카테고리 조회", response = CategoryDspDto.class)
    @GetMapping(value = "/category/getCategoryList")
    public ResponseObject getCategoryList(@ApiParam(value = "apiKey", required = true) @RequestHeader(value = "apiKey") String apiKey) throws Exception {
        return ResourceConverter.toResponseObject(basicService.getCategoryList(apiKey));
    }

    @ApiOperation(value = "검색태그 조회", response = SearchTagDto.class)
    @GetMapping(value = "/search/getSearchTagList")
    public ResponseObject getSearchTagList(@ApiParam(value = "apiKey", required = true) @RequestHeader(value = "apiKey") String apiKey) throws Exception {
        return ResourceConverter.toResponseObject(basicService.getSearchTagList(apiKey));
    }

    @ApiOperation(value = "제조사 조회", response = MakerInfoDto.class)
    @GetMapping(value = "/maker/getMakerList")
    public ResponseObject getMakerList(@ApiParam(value = "apiKey", required = true) @RequestHeader(value = "apiKey") String apiKey) throws Exception {
        return ResourceConverter.toResponseObject(basicService.getMakerList(apiKey));
    }

    @ApiOperation(value = "고시정보 조회", response = ItemNoticeDto.class)
    @GetMapping(value = "/maker/getItemNoticeList")
    public ResponseObject getItemNoticeList(@ApiParam(value = "apiKey", required = true) @RequestHeader(value = "apiKey") String apiKey) throws Exception {
        return ResourceConverter.toResponseObject(basicService.getItemNoticeList(apiKey));
    }


    @ApiOperation(value = "판매자 배송정책 조회", response = SellerShipDto.class)
    @GetMapping(value = "/seller/getSellerShipList")
    public ResponseObject getSellerShipList(@ApiParam(value = "apiKey", required = true) @RequestHeader(value = "apiKey") String apiKey) throws Exception {
        return ResourceConverter.toResponseObject(basicService.getSellerShipList(apiKey));
    }
}
