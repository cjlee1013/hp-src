package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("상품 연관상품 상태관련 정보")
@Data
public class ItemRelationStatusSetDto {

	@ApiModelProperty(value = "상품유형 (N:새상품, U:중고, R:리퍼, B:반품(리세일), C:주문제작) ")
	private String partnerId;

	@ApiModelProperty(value = "소카테고리 DB")
	private int scateCdDb;

	@ApiModelProperty(value = "소카테고리")
	private int scateCd;

	@ApiModelProperty(value = "성인상품유형")
	private String adultType;

	@ApiModelProperty(value = "장바구니 제한여부")
	private String cartLimitYn;

}
