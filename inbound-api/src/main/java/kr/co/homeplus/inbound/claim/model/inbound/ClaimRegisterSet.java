package kr.co.homeplus.inbound.claim.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("주문 취소 요청")
public class ClaimRegisterSet {

    @NotNull(message = "주문번호가 없습니다.")
    @ApiModelProperty(value = "주문번호", position = 1, example = "3000004971")
    private String purchaseOrderNo;

    @NotNull(message = "배송번호가 없습니다.")
    @ApiModelProperty(value = "배송번호", position = 2, example = "3000000004")
    private String bundleNo;

    @ApiModelProperty(value = "상품번호", position = 3)
    private List<ClaimItem> claimItemList;

    @NotNull(message = "클레임사유코드가 없습니다.")
    @ApiModelProperty(value = "클레임사유코드(코드 별첨)", position = 8, example = "C002")
    private String claimReasonType;

    @ApiModelProperty(value = "클레임사유상세", position = 9, example = "클레임 상세사유를 입력해주세요")
    private String claimReasonDetail;

    @ApiModelProperty(value = "주문취소 여부 Y:주문취소/N:클레임신청", position = 12, example = "N")
    private String orderCancelYn;

    @Getter
    @Setter
    public static class ClaimItem {
        @ApiModelProperty(value = "상품번호", position = 1, example = "10000300000265")
        private String itemNo;

        @ApiModelProperty(value = "옵션번호", position = 2, example = "3000000005")
        private String orderOptionNo;

        @ApiModelProperty(value = "클레임수량", position = 3, example = "1")
        private String claimQty;

        @ApiModelProperty(value = "상품주문번호", position = 4, example = "3000000001")
        private String orderItemNo;
    }
}
