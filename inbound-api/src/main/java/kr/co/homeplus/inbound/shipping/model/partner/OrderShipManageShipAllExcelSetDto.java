package kr.co.homeplus.inbound.shipping.model.partner;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.inbound.shipping.enums.ShipMethod;
import kr.co.homeplus.inbound.shipping.model.inbound.ShipStartSet;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 발주/발송관리 일괄발송처리(EXCEL) 요청 DTO")
public class OrderShipManageShipAllExcelSetDto {
    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;

    @ApiModelProperty(notes = "배송방법")
    private String shipMethod;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "배송예정일")
    private String scheduleShipDt;

    @ApiModelProperty(notes = "수정자")
    private String chgId;

    @ApiModelProperty(notes = "실패메세지")
    private String failMsg;

    public OrderShipManageShipAllExcelSetDto(
        ShipStartSet shipStartSet, String chgId) {
        this.bundleNo = shipStartSet.getBundleNo();
        String shipMethod = shipStartSet.getShipMethod();
        if (shipMethod != null) {
            this.shipMethod = ShipMethod.valueOf(shipMethod).getType();
        }
        this.dlvCd = shipStartSet.getDeliveryCode();
        this.invoiceNo = shipStartSet.getInvoiceNo();
        this.scheduleShipDt = shipStartSet.getScheduleShipDate();
        this.chgId = chgId;
    }
}
