package kr.co.homeplus.inbound.etc.service;

import java.util.List;
import kr.co.homeplus.inbound.constants.ExceptionCode;
import kr.co.homeplus.inbound.core.constants.ResourceRouteName;
import kr.co.homeplus.inbound.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.inbound.etc.model.NoticeListSelectDto;
import kr.co.homeplus.inbound.common.service.CommonValidService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NoticeService {


    final private ResourceClient resourceClient;

    private final CommonValidService commonValidService;
    /**
     * 공지사항  리스트 조회
     * @param apiKey
     * @param searchDate
     * @throws Exception
     * @return
     */
    public List<NoticeListSelectDto> getNoticeList(String apiKey, String searchDate) throws Exception {

        //API KEY validation check
        final String partnerId = commonValidService.validApiKeyHandler(apiKey);

        List<NoticeListSelectDto> noticeList;
        String apiUri = "/inbound/out/getNoticeList?partnerId="+partnerId+"&searchDate=";
        try {

            if(StringUtils.isNotEmpty(searchDate)){

                apiUri += searchDate;
            }
            noticeList = resourceClient.getForResponseObject(
                ResourceRouteName.MANAGE, apiUri, new ParameterizedTypeReference<ResponseObject<List<NoticeListSelectDto>>>() {}).getData();

        } catch (Exception e) {
            throw new BusinessLogicException(ExceptionCode.SYS_ERROR_CODE_9001);
        }


        return noticeList;
    }

}
