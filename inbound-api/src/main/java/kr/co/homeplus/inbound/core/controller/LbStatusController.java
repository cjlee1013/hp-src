package kr.co.homeplus.inbound.core.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LbStatusController {
    private static final Logger log = LoggerFactory.getLogger(LbStatusController.class);

    @GetMapping("/lbStatusCheck")
    public ResponseEntity<String> getLbStatusCheck() {
        String result = "OK";

        return new ResponseEntity<>(result.trim(), HttpStatus.OK);
    }
}