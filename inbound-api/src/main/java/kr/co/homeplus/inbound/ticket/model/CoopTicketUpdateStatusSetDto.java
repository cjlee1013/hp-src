package kr.co.homeplus.inbound.ticket.model;


import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "쿠프티켓 상태 변경 요청 파라미터")
public class CoopTicketUpdateStatusSetDto {
    private String ticketCd;

    private String ticketStatus;

    private String chgId;
}
