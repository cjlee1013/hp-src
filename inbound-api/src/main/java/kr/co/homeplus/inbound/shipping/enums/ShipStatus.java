package kr.co.homeplus.inbound.shipping.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ShipStatus {

        D1("D1", "NEWORDER")
    ,   D2("D2", "CONFIRM")
    ,   D3("D3", "SHIPPING")
    ,   D4("D4", "COMPLETE")
    ,   D5("D5", "DECISION")
    ,   NEWORDER("D1", "NEWORDER")
    ,   CONFIRM("D2", "CONFIRM")
    ,   SHIPPING("D3", "SHIPPING")
    ,   COMPLETE("D4", "COMPLETE")
    ,   DECISION("D5", "DECISION")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;


}
