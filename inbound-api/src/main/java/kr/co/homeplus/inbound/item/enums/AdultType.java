package kr.co.homeplus.inbound.item.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum AdultType {

        NORMAL( "NORMAL", "일반")
    ,   ADULT("ADULT", "성인")
    ,   LOCAL_LIQUOR("LOCAL_LIQUOR", "전통주");

    @Getter
    private final String type;

    @Getter
    private final String description;


}
