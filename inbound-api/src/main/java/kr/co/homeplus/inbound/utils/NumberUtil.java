package kr.co.homeplus.inbound.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.NumberUtils;

/**
 * Number Util
 */
@Slf4j
public class NumberUtil {

    // 문자열 특정숫자 타입으로 치환
    public static <T extends Number> T getStringToNumber(String str, Class<T> type) {
        if (!StringUtils.isBlank(str)) {
            return NumberUtils.parseNumber(str, type);
        } else {
            return null;
        }
    }
}
