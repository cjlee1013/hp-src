package kr.co.homeplus.inbound.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimShippingReqType {

        PICK("P", "PICK")
    ,   EXCHANGE("E", "EXCHANGE")
    ,   P("P", "PICK")
    ,   E("E", "EXCHANGE")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;


}
