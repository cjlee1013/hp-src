package kr.co.homeplus.inbound.basic.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("검색태그 정보")
public class SearchTagDto {
	@ApiModelProperty(value = "검색태그분류번호", position = 5)
	private long groupSearchTagNo;

	@ApiModelProperty(value = "검색태그분류명(최대 12자)", position = 10)
	private String groupSearchTagName;

	@ApiModelProperty(value = "검색태그번호", position = 15)
	private long searchTagNo;

	@ApiModelProperty(value = "검색태그명(최대 12자)", position = 20)
	private String searchTagName;
}
