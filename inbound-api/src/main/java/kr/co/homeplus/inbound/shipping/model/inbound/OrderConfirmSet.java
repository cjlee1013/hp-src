package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel("주문확인 요청")
public class OrderConfirmSet {
    @ApiModelProperty(notes = "배송번호 리스트", example = "3000000001", position = 1)
    private List<String> bundleNoList;
}