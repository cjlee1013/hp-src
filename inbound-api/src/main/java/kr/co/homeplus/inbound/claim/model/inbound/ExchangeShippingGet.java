package kr.co.homeplus.inbound.claim.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.inbound.claim.enums.ClaimExchangeStatus;
import kr.co.homeplus.inbound.claim.enums.ClaimShipType;
import kr.co.homeplus.inbound.claim.model.partner.ClaimExchShippingPartnerDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("클레임 교환배송 응답")
public class ExchangeShippingGet {
    @ApiModelProperty(value = "교환-이름", position = 2, example = "홍길동")
    public String exchangeReceiverName;

    @ApiModelProperty(value = "교환-휴대폰", position = 3, example = "010-1111-1111")
    public String exchangeMobileNo;

    @ApiModelProperty(value = "교환 우편번호", position = 4, example = "07297")
    public String exchangeZipcode;

    @ApiModelProperty(value = "교환 전체주소", position = 5, example = "서울특별시 금천구 독산동 291-7")
    public String exchangeAddress;

    @ApiModelProperty(value = "교환 상세주소", position = 6, example = "홈플러스 금천점 1층")
    public String exchangeAddressDetail;

    @ApiModelProperty(value = "교환 송장번호", position = 7, example = "123123124122")
    public String exchangeInvoiceNo;

    @ApiModelProperty(value = "교환배송수단(DELIVERY:택배/DIRECT:직접배송/POST:우편)", position = 9, example = "DELIVERY")
    public String exchangeShipType;

    @ApiModelProperty(value = "교환 배송_상태(상품출고대기:CONFIRM/상품출고:SCHEDULE/배송중:SHIPPING/배송완료:COMPLETE)", position = 10, example = "CONFIRM")
    public String exchangeStatus;

    @ApiModelProperty(value = "교환배송 택배사", position = 12, example = "002")
    public String exchangeDelivery;

    @ApiModelProperty(value = "교환배송 메모", position = 14, example = "우편배송 메모")
    public String exchangeMemo;

    @ApiModelProperty(value = "업체직배송의 배송예정일 최초등록일", position = 15, example = "2020-12-29 00:00:00")
    public String exchangeConfirmDate;

    @ApiModelProperty(value = "교환배송 송장등록일(업체직배송의 경우 배송예정일)", position = 16, example = "2020-12-29 00:00:00")
    public String exchangeScheduleDate;

    @ApiModelProperty(value = "교환배송 시작일", position = 17, example = "2020-12-29 00:00:00")
    public String exchangeShippingDate;

    @ApiModelProperty(value = "교환배송 완료일", position = 18, example = "2020-12-29 00:00:00")
    public String exchangeCompleteDate;

    @ApiModelProperty(value="교환 지번주소", position = 19, example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점 1층")
    public String exchangeBaseAddress;

    public ExchangeShippingGet(ClaimExchShippingPartnerDto claimExchShippingPartnerDto) {
        this.exchangeReceiverName = claimExchShippingPartnerDto.getExchReceiverNm();
        this.exchangeMobileNo = claimExchShippingPartnerDto.getExchMobileNo();
        this.exchangeZipcode = claimExchShippingPartnerDto.getExchZipcode();
        this.exchangeAddress = claimExchShippingPartnerDto.getExchAddr();
        this.exchangeAddressDetail = claimExchShippingPartnerDto.getExchAddrDetail();
        this.exchangeInvoiceNo = claimExchShippingPartnerDto.getExchInvoiceNo();
        String exchShipType = claimExchShippingPartnerDto.getExchShipTypeCode();
        if (exchShipType != null) {
            this.exchangeShipType = ClaimShipType.valueOf(exchShipType).getDescription();
        }
        String exchStatus = claimExchShippingPartnerDto.getExchStatusCode();
        if (exchStatus != null) {
            this.exchangeStatus = ClaimExchangeStatus.valueOf(exchStatus).getDescription();
        }
        this.exchangeDelivery = claimExchShippingPartnerDto.getExchDlv();
        this.exchangeMemo = claimExchShippingPartnerDto.getExchMemo();
        this.exchangeConfirmDate = claimExchShippingPartnerDto.getExchD0Dt();
        this.exchangeScheduleDate = claimExchShippingPartnerDto.getExchD1Dt();
        this.exchangeShippingDate = claimExchShippingPartnerDto.getExchD2Dt();
        this.exchangeCompleteDate = claimExchShippingPartnerDto.getExchD3Dt();
        this.exchangeBaseAddress = claimExchShippingPartnerDto.getExchBaseAddr();
    }
}
