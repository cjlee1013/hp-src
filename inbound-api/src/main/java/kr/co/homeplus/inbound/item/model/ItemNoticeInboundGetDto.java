package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("상품정보고시정보")
public class ItemNoticeInboundGetDto {

	@ApiModelProperty(value = "고시항목번호", position = 1)
	private Integer noticeNo;

	@ApiModelProperty(value = "고시항목명", position = 2)
	private String noticeNm;

	@ApiModelProperty(value = "정보고시항목설명", position = 3)
	private String 	noticeDesc;

}
