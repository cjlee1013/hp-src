package kr.co.homeplus.inbound.core.config.swagger;


import static springfox.documentation.builders.PathSelectors.ant;

import com.google.common.base.Predicates;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class ItemSwaggerConfig {

    @Value("${api.swagger-config-host}")
    private String swaggerConfigHost;

    @Value("${api.swagger-config-protocol}")
    private String swaggerConfigProtocol;

    final private String docVersion = "1.0.0";


    @Bean
    @SuppressWarnings("unchecked")
    public Docket itemApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .host(swaggerConfigHost)
                .protocols(Sets.newHashSet(swaggerConfigProtocol))
                .groupName("item")
                .tags(new Tag("상품 등록/수정", ""))
                .select()
                .paths(Predicates.or(ant("/item/*/**")))
                .build()
                .apiInfo(this.itemApiInfo());
    }


    private ApiInfo itemApiInfo() {
        return new ApiInfoBuilder()
                .title(SwaggerConfig.SWAGGER_CONFIG_TITLE + " (상품)")
                .description(SwaggerConfig.SWAGGER_CONFIG_DESCRIPTION)
                .version(docVersion)
                .contact(new Contact(SwaggerConfig.SWAGGER_CONFIG_CONTACT_NAME, null, SwaggerConfig.SWAGGER_CONFIG_CONTACT_EMAIL))
                .build();
    }
}
