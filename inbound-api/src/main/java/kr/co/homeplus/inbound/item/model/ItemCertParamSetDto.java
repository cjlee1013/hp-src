package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 인증 정보 param
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("상품 안전인증 정보 param")
public class ItemCertParamSetDto {

	@ApiModelProperty(value = "상품인증 시퀀스")
	private Integer itemCertSeq;

	@ApiModelProperty(value = "인증여부 (Y:인증대상, N:인증대상아님, E:면제대상)")
	@NotEmpty(message = "인증여부")
	@Pattern(regexp = "(Y|N)", message="구매제한 타입")
	private String isCert;

	@ApiModelProperty(value = "인증그룹 (공통코드 : item_cert_group) - (제품안전인증:IT,어린이제품:KD,전파인증:ER,전파인증:ET)")
	private String	certGroup;

	@ApiModelProperty(value = "인증유형")
	private String 	certType;

	@ApiModelProperty(value = "인증번호")
	@Max(value = 50, message = "인증번호")
	private String 	certNo;

}
