package kr.co.homeplus.inbound.item.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("상품 옵션정보")
public class ItemOptSetDto {

	@ApiModelProperty(value = "텍스트옵션사용여부 기준:미사용 (사용:Y,미사용:N)", position = 1)
	@JsonProperty(value = "optionTextUseYn")
	@Builder.Default
	private String optTxtUseYn = "N";

	public void setOptTxtUseYn(String optTxtUseYn) {
		this.optTxtUseYn = optTxtUseYn.toUpperCase();
	}

	@ApiModelProperty(value = "텍스트형 옵션단계 (최대: 2단계)", position = 2)
	@JsonProperty(value = "optionTextDepth")
	@Builder.Default
	private Integer optTxtDepth = 1;

	@ApiModelProperty(value = "텍스트형 1단계 타이틀 (최대: 25자)", position = 3)
	@JsonProperty(value = "option1Text")
	private String opt1Text;

	@ApiModelProperty(value = "텍스트형 2단계 타이틀 (최대: 25자)", position = 4)
	@JsonProperty(value = "option2Text")
	private String opt2Text;

}