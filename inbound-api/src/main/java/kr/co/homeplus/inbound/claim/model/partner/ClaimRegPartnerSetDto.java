package kr.co.homeplus.inbound.claim.model.partner;

import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.inbound.claim.enums.ClaimCancelType;
import kr.co.homeplus.inbound.claim.enums.ClaimType;
import kr.co.homeplus.inbound.claim.enums.ClaimWhoReason;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimRegisterSet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimRegisterSet.ClaimItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClaimRegPartnerSetDto {

    @NotNull(message = "주문번호가 없습니다.")
    @ApiModelProperty(value = "주문번호", position = 1, example = "3000004971")
    private String purchaseOrderNo;

    @NotNull(message = "배송번호가 없습니다.")
    @ApiModelProperty(value = "배송번호", position = 2, example = "3000000004")
    private String bundleNo;

    @ApiModelProperty(value = "상품번호", position = 3)
    private List<ClaimPreRefundItemListPartner> claimItemList;

    @NotNull(message = "귀책사유가 없습니다.")
    @ApiModelProperty(value = "귀책사유(B:바이어(고객)/S:판매자/H:홈플러스)", position = 4, example = "S")
    @Pattern(regexp = "B|S|H", message = "귀책사유")
    private String whoReason;

    @NotNull(message = "취소타입이 없습니다.")
    @ApiModelProperty(value = "취소타입(O:주문취소/R:반품/X:교환)", position = 5, example = "O")
    @Pattern(regexp = "O|R", message = "취소타입")
    private String cancelType;

    @NotNull(message = "클레임타입이 없습니다.")
    @ApiModelProperty(value = "클레임타입(C:취소/R:반품/X:교환)", position = 6, example = "X")
    private String claimType;

    @NotNull(message = "클레임사유코드가 없습니다.")
    @ApiModelProperty(value = "클레임사유코드", position = 8, example = "C002")
    @Length(max = 4, message = "클레임사유코드")
    private String claimReasonType;

    @ApiModelProperty(value = "클레임사유상세", position = 9, example = "클레임 상세사유를 입력해주세요")
    private String claimReasonDetail;

    @NotNull(message = "등록아이디가 없습니다.")
    @ApiModelProperty(value = "등록아이디", position = 10, example = "PARTNER")
    private String regId;

    @ApiModelProperty(value = "부분취소 여부 Y:부분취소/N:전체취소", position = 11, example = "Y")
    private String claimPartYn;

    @ApiModelProperty(value = "주문취소 여부 Y:주문취소/N:클레임신청", position = 12, example = "N")
    private String orderCancelYn;

    @ApiModelProperty(value = "행사미차감 여부(Y:미차감,N:차감)", position = 14, example = "Y")
    private String piDeductPromoYn;

    @ApiModelProperty(value = "할인 미차감 여부(Y:미차감,N:차감)", position = 15, example = "Y")
    private String piDeductDiscountYn;

    @ApiModelProperty(value = "배송비 미차감 여부(Y:미차감,N:차감)", position = 16, example = "Y")
    private String piDeductShipYn;

    @Getter
    @Setter
    public static class ClaimPreRefundItemListPartner {
        @ApiModelProperty(value = "상품번호", position = 1)
        private String itemNo;

        @ApiModelProperty(value = "옵션번호", position = 2)
        private String orderOptNo;

        @ApiModelProperty(value = "클레임수량", position = 3)
        private String claimQty;

        @ApiModelProperty(value = "상품주문번호", position = 4)
        private String orderItemNo;

        public ClaimPreRefundItemListPartner(ClaimItem claimPreRefundItemList) {
            this.itemNo = claimPreRefundItemList.getItemNo();
            this.orderOptNo = claimPreRefundItemList.getOrderOptionNo();
            this.claimQty = claimPreRefundItemList.getClaimQty();
            this.orderItemNo = claimPreRefundItemList.getOrderItemNo();
        }
    }

    public ClaimRegPartnerSetDto(ClaimRegisterSet claimRegisterSet, String regId) {
        this.purchaseOrderNo = claimRegisterSet.getPurchaseOrderNo();
        this.bundleNo = claimRegisterSet.getBundleNo();
        this.claimItemList = new ArrayList<>();
        for (ClaimItem itemList : claimRegisterSet.getClaimItemList()) {
            claimItemList.add(new ClaimPreRefundItemListPartner(itemList));
        }
        this.whoReason = "S";
        this.cancelType = "O";
        this.claimType = "C";
        this.claimReasonType = claimRegisterSet.getClaimReasonType();
        this.claimReasonDetail = claimRegisterSet.getClaimReasonDetail();
        this.regId = regId;
        this.claimPartYn = "Y";
        this.orderCancelYn = claimRegisterSet.getOrderCancelYn();
        this.piDeductPromoYn = "N";
        this.piDeductDiscountYn = "N";
        this.piDeductShipYn = "N";
    }
}
