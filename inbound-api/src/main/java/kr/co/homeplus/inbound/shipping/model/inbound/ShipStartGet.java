package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel("발송처리 응답")
public class ShipStartGet {
    @ApiModelProperty(notes = "성공개수", example = "10", position = 1)
    private String successCnt;

    @ApiModelProperty(notes = "발송처리 요청 결과", position = 2)
    private List<ShipStartResult> resultList;

    public ShipStartGet(String successCnt,
        List<ShipStartResult> resultList) {
        this.successCnt = successCnt;
        this.resultList = resultList;
    }
}
