package kr.co.homeplus.inbound.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "마켓 배송 slot 연동 결과")
@Getter
@Setter
@NoArgsConstructor @AllArgsConstructor
@Builder
public class MarketSlotResponse {

    @ApiModelProperty(value = "return code", example = "0000")
    private String code;

    @ApiModelProperty(value = "return message")
    private String message;


}
