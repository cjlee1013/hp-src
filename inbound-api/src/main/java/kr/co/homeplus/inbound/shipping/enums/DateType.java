package kr.co.homeplus.inbound.shipping.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum DateType {

        orderDt("orderDt", "ORDERDATE")
    ,   confirmDt("confirmDt", "CONFIRMDATE")

    ,   ORDERDATE("orderDt", "ORDERDATE")
    ,   CONFIRMDATE("confirmDt", "CONFIRMDATE")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;


}
