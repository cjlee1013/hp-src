package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.inbound.constants.PatternConstants;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 예약 판매정보
 */
@Getter
@Setter
@ApiModel("상품 예약 판매정보")
public class ItemRsvDetailSetDto {

	//예약판매번호
	@ApiModelProperty(value = "예약판매번호", hidden = true)
	private long saleRsvNo;

	//예약판매시작일
	@ApiModelProperty(value = "예약판매시작일")
	@Pattern(regexp = PatternConstants.DATE_DF_FORMAT_PATTERN, message = "예약판매시작일")
	private String rsvStartDt;

	//예약판매종료일
	@ApiModelProperty(value = "예약판매종료일")
	@Pattern(regexp = PatternConstants.DATE_DF_FORMAT_PATTERN, message = "예약판매종료일")
	private String rsvEndDt;

	//예약상품출하시작일
	@ApiModelProperty(value = "예약상품출하시작일")
	@Pattern(regexp = PatternConstants.DATE_DF_FORMAT_PATTERN, message = "예약상품출하시작일")
	private String shipStartDt;

}
