package kr.co.homeplus.inbound.claim.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("클레임 수거완료 요청")
public class PickCompleteSet {

    @NotNull(message = "그룹클레임번호")
    @ApiModelProperty(value = "그룹클레임번호", position = 1, example = "1500000001")
    private long claimNo;

    @ApiModelProperty(value = "클레임번호", position = 2, example = "1500000011")
    public long claimBundleNo;
}
