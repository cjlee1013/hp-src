package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import kr.co.homeplus.inbound.constants.PatternConstants;
import kr.co.homeplus.inbound.core.valid.NotAllowInput;
import lombok.Data;

@Data
@ApiModel("상품관리 조회")
public class ItemListParamDto {

    @ApiModelProperty(value = "검색 날짜유형", required = true)
    private String schDate;

    @ApiModelProperty(value = "검색 시작일", required = true)
    @Pattern(regexp = PatternConstants.DATE_DF_FORMAT_PATTERN, message = "검색 시작일")
    private String schStartDate;

    @ApiModelProperty(value = "검색 종료일", required = true)
    @Pattern(regexp = PatternConstants.DATE_DF_FORMAT_PATTERN, message = "검색 종료일")
    private String schEndDate;

    @ApiModelProperty(value = "대분류 카테고리")
    private String schLcateCd;

    @ApiModelProperty(value = "증분류 카테고리")
    private String schMcateCd;

    @ApiModelProperty(value = "소분류 카테고리")
    private String schScateCd;

    @ApiModelProperty(value = "세분류 카테고리")
    private String schDcateCd;

    @ApiModelProperty(value = "판매업체ID")
    private String schPartnerId;

    @ApiModelProperty(value = "싱상품유형 (B:반품(리세일), N:새상품, R:리퍼, U:중고)")
    @Pattern(regexp = "(B|N|R|U)", message="상품유형")
    private String schItemType;

    @ApiModelProperty(value = "검색 조건")
    @NotEmpty(message = "검색 조건")
    private String schType;

    @ApiModelProperty(value = "검색 키워드")
    @NotAllowInput(types = {PatternConstants.SPC_SCH}, message = PatternConstants.SPC_SCH_MSG, onlyMessage = true)
    @Size(min = 2, max = 100, message = "검색 키워드")
    private String schKeyword;

    @ApiModelProperty(value = "상품상태(T: 임시저장, A:판매중, S:판매중지)")
    private String schItemStatus;

    @ApiModelProperty(value = "거래유형 (DS : 업체상품 , TD : 매장상품)")
    @Pattern(regexp = "(DS|TD)", message="거래유형")
    private String mallType;

    @ApiModelProperty(value = "점포구분 (Hyper,Club,Exp,DS)")
    @Pattern(regexp = "(HYPER|EXP|CLUB|DS)", message="점포구분")
    private String schStoreType;

    @ApiModelProperty(value = "점포ID")
    private String schStoreId;

    private Integer limitSize;
}
