package kr.co.homeplus.inbound.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ApiModel(value="네이버 상품 재고 조회 결과")
@Getter
@Setter
@ToString
public class NaverProductStockResponse {

    @ApiModelProperty(value = "상태", position = 1)
    private String status;

    @ApiModelProperty(value = "상품정보", position = 2)
    private List<NaverProduct> products;
}
