package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel("상품 이미지정보 조회")
public class ItemImgInboundGetDto {
	@ApiModelProperty(value = "메인 이미지 리스트", required = true)
	private List<ItemImgDetailInboundGetDto> mainImageList;

	@ApiModelProperty(value = "리스트 이미지", required = true)
	private ItemImgDetailInboundGetDto listImage;

}
