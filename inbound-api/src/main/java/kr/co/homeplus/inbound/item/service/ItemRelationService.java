package kr.co.homeplus.inbound.item.service;

import kr.co.homeplus.inbound.common.service.CommonValidService;
import kr.co.homeplus.inbound.constants.ChannelConstants;
import kr.co.homeplus.inbound.constants.CodeConstants;
import kr.co.homeplus.inbound.constants.ExceptionCode;
import kr.co.homeplus.inbound.core.constants.ResourceRouteName;
import kr.co.homeplus.inbound.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.inbound.itemRelation.model.ItemRelationInboundSetDto;
import kr.co.homeplus.inbound.itemRelation.model.ItemRelationListGetDto;
import kr.co.homeplus.inbound.itemRelation.model.ItemRelationSetDto;
import kr.co.homeplus.inbound.response.ResponseResult;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

@Service
@RequiredArgsConstructor
public class ItemRelationService {

    private final ResourceClient resourceClient;
    private final CommonValidService commonValidService;


    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    /**
     * 연관상품 등록/수정
     * @param itemRelationInboundSetDto
     * @return
     */
    public ResponseResult setItemRelationHandler(String apiKey, ItemRelationInboundSetDto itemRelationInboundSetDto) throws Exception {

        //API KEY validation check
        final String partnerId = commonValidService.validApiKeyHandler(apiKey);

        String apiUri = "/inbound/in/setItemRelation";

        if(ObjectUtils.isEmpty(itemRelationInboundSetDto.getRelationItemList())) {
            throw new BusinessLogicException(ExceptionCode.ERROR_CODE_6000);
        } else {
            if (itemRelationInboundSetDto.getRelationItemList().size() > 50) {
                throw new BusinessLogicException(ExceptionCode.ERROR_CODE_6001);
            }
        }

        return resourceClient
            .postForResponseObject(ResourceRouteName.ITEM,
                ItemRelationSetDto.builder()
                    .partnerId(partnerId)
                    .userId(CodeConstants.SYSTEM_USER_ID)
                    .relationNo(itemRelationInboundSetDto.getRelationNo())
                    .relationNm(itemRelationInboundSetDto.getRelationName())
                    .dispYn(itemRelationInboundSetDto.getDisplayYnName())
                    .dispPcType(itemRelationInboundSetDto.getDisplayPcType())
                    .dispMobileType(itemRelationInboundSetDto.getDisplayMobileType())
                    .imgUrl(itemRelationInboundSetDto.getIntroImageUrl())
                    .scateCd(itemRelationInboundSetDto.getSmallCategoryCode())
                    .useYn(itemRelationInboundSetDto.getUseYnName())
                    .relationItemList(itemRelationInboundSetDto.getRelationItemList())
                    .channel(ChannelConstants.INBOUND)
                    .build()
                , apiUri,
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }
    /**
     * 연관상품 조회
     * @param apiKey
     * @param relationNo
     * @return
     */
    public ItemRelationListGetDto getItemRelation(String apiKey, Long relationNo) throws Exception {

        //API KEY validation check
        final String partnerId = commonValidService.validApiKeyHandler(apiKey);
        String apiUri =
            "/inbound/out/getItemRelationList?partnerId=" + partnerId + "&relationNo=" + relationNo;

        ItemRelationListGetDto itemRelationListGetDto = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri,
            new ParameterizedTypeReference<ResponseObject<ItemRelationListGetDto>>() {
            }).getData();

        if( !ObjectUtils.isEmpty(itemRelationListGetDto)) {
            itemRelationListGetDto.setIntroImageUrl( hmpImgFrontUrl + itemRelationListGetDto.getIntroImageUrl());
        }
        return itemRelationListGetDto;
    }
}
