package kr.co.homeplus.inbound.etc.service;

import java.util.List;
import kr.co.homeplus.inbound.constants.ExceptionCode;
import kr.co.homeplus.inbound.core.constants.ResourceRouteName;
import kr.co.homeplus.inbound.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.inbound.response.ResponseResult;
import kr.co.homeplus.inbound.etc.model.CsQnaListSelectDto;
import kr.co.homeplus.inbound.etc.model.CsQnaReplySetParamDto;
import kr.co.homeplus.inbound.common.service.CommonValidService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CsQnaService {


    final private ResourceClient resourceClient;

    private final CommonValidService commonValidService;

    /**
     * ADMIN Qna 리스트 조회
     * @param apiKey
     * @param searchDate
     * @throws Exception
     * @return
     */
    public List<CsQnaListSelectDto> getQnaList(String apiKey, String searchDate) throws Exception {
        //API KEY validation check
        final String partnerId = commonValidService.validApiKeyHandler(apiKey);


        List<CsQnaListSelectDto> qnaList;
        String apiUri = "/inbound/out/getQnaList?partnerId="+partnerId+"&searchDate=";
        try {

            if(StringUtils.isNotEmpty(searchDate)){
                apiUri += searchDate;
            }
            qnaList = resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri, new ParameterizedTypeReference<ResponseObject<List<CsQnaListSelectDto>>>() {}).getData();

        } catch (Exception e) {
            throw new BusinessLogicException(ExceptionCode.SYS_ERROR_CODE_9001);
        }


        return qnaList;

    }

    /**
     *  Qna 답변 등록/수정
     * @param csQnaReplySetParamDto
     * @return
     */
    public ResponseObject setQnaReply(String apiKey, CsQnaReplySetParamDto csQnaReplySetParamDto) throws Exception {
        //API KEY validation check
        final String partnerId = commonValidService.validApiKeyHandler(apiKey);

        csQnaReplySetParamDto.setPartnerId(partnerId);

        ResponseObject responseObject = resourceClient
            .postForResponseObject(ResourceRouteName.MANAGE, csQnaReplySetParamDto, "/inbound/out/setQnaReply",
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                });

        return responseObject;
    }


}