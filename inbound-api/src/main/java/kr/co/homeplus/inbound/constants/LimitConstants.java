package kr.co.homeplus.inbound.constants;

/**
 * 제한 관련 공통 상수
 */
public class LimitConstants {
    // 사용자 ID 길이 제한
    public static final int USER_ID_LIMIT = 20;

    // 조회개수 제한
    public static final int LIST_LIMIT = 5000;

    // 조회개수 제한 1만건 버전
    public static final int LIST_LIMIT_10000 = 10000;

    // 조회개수 제한 3만건 버전
    public static final int LIST_LIMIT_30000 = 30000;

    // 1000개씩 짤라서 Query 날림 (IN쿼리)
    public static final int QUERY_OFFSET = 1000;

    // 100개씩 짤라서 Async 날림
    public static final int BATCH_ASYNC_OFFSET = 100;
}
