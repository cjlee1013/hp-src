package kr.co.homeplus.inbound.item.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.inbound.item.enums.AdultType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("상품 부가정보")
public class ItemEtcSetDto {

	@ApiModelProperty(value = "성인상품유형 기준:일반 (NORMAL: 일반, ADULT: 성인, LOCAL_LIQUOR: 전통주)", position = 1)
	@Builder.Default
	private String adultType = AdultType.NORMAL.getType();

	public void setAdultType(String adultType) {
		this.adultType = adultType.toUpperCase();
	}

	@ApiModelProperty(value = "성인상품 이미지 노출여부 기준:노출안함 (Y:노출, N:노출안함)", position = 2)
	@JsonProperty("imageDisplayYn")
	private String imgDispYn;

	@ApiModelProperty(value = "선물하기사용여부 기준:사용안함 (Y: 사용, N: 사용안함) (성인유형 설정시 사용안함 강제처리 대상)", position = 3)
	@Builder.Default
	private String giftYn = "N";

	public void setGiftYn(String giftYn) {
		this.giftYn = giftYn.toUpperCase();
	}

	@ApiModelProperty(value = "가격비교 사이트 등록여부 기준:등록안함 (Y: 등록, N: 등록안함)", position = 4)
	@Builder.Default
	private String epYn = "N";

	public void setEpYn(String epYn) {
		this.epYn = epYn.toUpperCase();
	}

	@ApiModelProperty(value = "제휴사, 마켓연동 리스트", position = 5, hidden = true)
	private List<ItemProviderSetDto> providerList;

	@ApiModelProperty(value = "검색 키워드 (최대: 100자, 20개까지 등록가능) 예) 식품,라면,뽀글이,매운맛", position = 6)
	@JsonProperty("searchKeyword")
	private String srchKeyword;

	@ApiModelProperty(value = "해외배송 여부 (Y: 사용, N: 사용안함)", required = true, position = 7)
	@JsonProperty("globalAgencyYn")
	private String globalAgcyYn;

	public void setGlobalAgcyYn(String globalAgcyYn) {
		this.globalAgcyYn = globalAgcyYn.toUpperCase();
	}

	@ApiModelProperty(value = "ISBN (최대: 13자) - 도서카테고리 경우 필수값", position = 12)
	private String isbn;

}