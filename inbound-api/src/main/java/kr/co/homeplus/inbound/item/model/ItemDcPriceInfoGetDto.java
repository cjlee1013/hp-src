package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 상품 할인가격 정보 Get Entry
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("상품 할인가격 정보")
public class ItemDcPriceInfoGetDto {

	@ApiModelProperty(value = "가격유형 (상품즉시할인: ITEM, 즉시할인쿠폰: COUPON, 카드즉시할인: CARD)", required = true, position = 1)
	private String priceType;

	@ApiModelProperty(value = "문자열 앞 텍스트", required = true, position = 2)
	private String preTxt;

	@ApiModelProperty(value = "기간 텍스트", required = true, position = 3)
	private String periodTxt;

	@ApiModelProperty(value = "할인가격", position = 4)
	private Long dcPrice;

}