package kr.co.homeplus.inbound.claim.model.partner;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임 배송정보변경 요청 파라미터")
public class ClaimShippingPartnerSetDto {

    @NotNull(message = "클레임번들번호")
    @ApiModelProperty(value = "클레임번들번호", position = 1)
    private long claimBundleNo;

    @NotNull(message = "클레임번들번호")
    @ApiModelProperty(value = "클레임번들번호", position = 2)
    private long claimNo;

    @ApiModelProperty(value = "반품/교환 요청타입, [P:반품, E:교환, null:반품/교환]", position = 3)
    private String claimShippingReqType;

    @ApiModelProperty(value = "반품 배송 정보", position = 4)
    private ClaimPickShippingModifyPartnerDto claimPickShippingModifyDto;

    @ApiModelProperty(value = "교환 발송 정보", position = 5)
    private ClaimExchShippingModifyPartnerDto claimExchShippingModifyDto;

    @ApiModelProperty(value = "변경자ID", position = 6)
    private String chgId;
}
