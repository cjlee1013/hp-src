package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("발송처리 요청")
public class ShipStartSet {
    @ApiModelProperty(notes = "배송번호", example = "3000000001", position = 1)
    private String bundleNo;

    @ApiModelProperty(notes = "배송방법(DELIVERY:택배배송, DIRECT:직접배송, PICK:방문수령, POST:우편배송, QUICK:퀵, NONE:배송없음)", example = "DELIVERY", position = 2)
    private String shipMethod;

    @ApiModelProperty(notes = "택배사코드(002:CJ택배, 004:우체국택배 그외 별첨)", example = "002", position = 3)
    private String deliveryCode;

    @ApiModelProperty(notes = "송장번호", example = "1234567890", position = 4)
    private String invoiceNo;

    @ApiModelProperty(notes = "[직접배송시]배송예정일", example = "2021-01-20", position = 5)
    private String scheduleShipDate;
}
