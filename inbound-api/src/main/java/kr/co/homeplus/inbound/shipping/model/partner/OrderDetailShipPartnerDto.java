package kr.co.homeplus.inbound.shipping.model.partner;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송정보 DTO")
public class OrderDetailShipPartnerDto {
    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;

    @ApiModelProperty(notes = "배송비지급")
    private String shipPriceType;

    @ApiModelProperty(notes = "배송비")
    private String totShipPrice;

    @ApiModelProperty(notes = "도서산간배송비")
    private String islandShipPrice;

    @ApiModelProperty(notes = "발송기한")
    private String orgShipDt;

    @ApiModelProperty(notes = "2차발송기한")
    private String delayShipDt;

    @ApiModelProperty(notes = "발송지연사유")
    private String delayShipNm;

    @ApiModelProperty(notes = "수령인")
    private String receiverNm;

    @ApiModelProperty(notes = "수령인 연락처")
    private String shipMobileNo;

    @ApiModelProperty(notes = "우편번호")
    private String zipcode;

    @ApiModelProperty(notes = "주소")
    private String addr;

    @ApiModelProperty(notes = "배송메세지")
    private String dlvShipMsg;

    @ApiModelProperty(notes = "미수취신고일")
    private String noRcvDeclrDt;
}
