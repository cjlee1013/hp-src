package kr.co.homeplus.inbound.core.constants;

/**
 * resource api 명을 관리
 * apiId의 url은 application.yml의 plus.resource-routes 에 정의 됨
 */
public class ResourceRouteName {

    /**
     * item api
     */
    public static final String ITEM = "item";
    /**
     * manage api
     */
    public static final String MANAGE = "manage";

    /**
     * cryptokey
     */
    public static final String CRYPTOKEY = "cryptokey";

    /**
     * shipping api
     */
    public static final String SHIPPING = "shipping";

    /**
     * escrowmng api
     */
    public static final String ESCROWMNG = "escrowmng";

    /**
     * claim api
     */
    public static final String CLAIM = "claim";

    /**
     * escrow
     */
    public static final String ESCROW = "escrow";

}
