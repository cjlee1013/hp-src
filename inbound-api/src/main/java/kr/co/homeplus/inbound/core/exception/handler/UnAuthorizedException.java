package kr.co.homeplus.inbound.core.exception.handler;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class UnAuthorizedException extends CommonRunTimeException {
    public UnAuthorizedException(String version, String msg) {
        super(version, msg);
    }
}
