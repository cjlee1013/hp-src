package kr.co.homeplus.inbound.constants;

public class EscrowConstants {

    public static final String MARKET_TYPE_NAVER = "naver";
    public static final String MARKET_TYPE_ELEVEN= "eleven";

    public static final String MARKET_RETURN_ERROR_CODE = "9999";
    public static final String MARKET_RETURN_ERROR_MSG = "처리중 오류가 발생하였습니다.";

    public static final String MARKET_RETURN_SUCCESS_CODE = "0000";

}
