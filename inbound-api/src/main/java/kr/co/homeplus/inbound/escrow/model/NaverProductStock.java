package kr.co.homeplus.inbound.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ApiModel(value="네이버 상품 재고 조회")
@Getter
@Setter
@ToString
public class NaverProductStock {

    @ApiModelProperty(value = "요청유형", position = 1)
    private String type;

    @ApiModelProperty(value = "상품리스트", position = 2)
    private List<NaverProduct> products;
}
