package kr.co.homeplus.inbound.common.service;

import kr.co.homeplus.inbound.common.model.PartnerApiKeyGetDto;
import kr.co.homeplus.inbound.constants.ExceptionCode;
import kr.co.homeplus.inbound.core.constants.ResourceRouteName;
import kr.co.homeplus.inbound.core.exception.custom.UnauthorizedException;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommonValidService {
    private final ResourceClient resourceClient;

    /**
     * API KEY Validation
     *
     * timeStampMillis 뒤에 6자리 + 랜덤 4자리 (총 10자리)_파트너ID_서버유형_API서비스명
     *      [0] timeStampMillis 뒤에 6자리 + 랜덤 4자리 (총 10자리)
     *      [1] 파트너ID: 생성된 업체ID
     *      [2] 서버유형: local, dev
     */
    public String validApiKeyHandler(final String encPartnerApiKey) throws Exception {
        String partnerId;

        try {
            // 복호화
            PartnerApiKeyGetDto partnerApiKeyGetDto = new PartnerApiKeyGetDto();
            partnerApiKeyGetDto.setData(encPartnerApiKey);

            String response = resourceClient.postForResponseObject(
                    ResourceRouteName.CRYPTOKEY
                    , partnerApiKeyGetDto
                    , "/own/direct/dec"
                    , new ParameterizedTypeReference<ResponseObject<PartnerApiKeyGetDto>>() {}
            ).getData().getData();

            final String decPartnerApiKey = response;

            final String[] arrApiKey = decPartnerApiKey.split("_");

            partnerId = arrApiKey[1];

        } catch(Exception e) {
            throw new UnauthorizedException(ExceptionCode.ERROR_CODE_1016, "암호화");
        }

        this.checkEqualsApiKey(partnerId, encPartnerApiKey);

        return partnerId;
    }

    /**
     * 발급된 API KEY와 전송받은 API KEY가 동일한지 체크
     * @param partnerId
     * @param encPartnerApiKey
     * @throws Exception
     */
    private void checkEqualsApiKey(final String partnerId, final String encPartnerApiKey) throws Exception {
        PartnerApiKeyGetDto partnerApiKeyGetDto = new PartnerApiKeyGetDto();
        partnerApiKeyGetDto.setPartnerId(partnerId);
        partnerApiKeyGetDto.setEncKey(encPartnerApiKey);

        Boolean response = resourceClient.postForResponseObject(
                ResourceRouteName.ITEM
                , partnerApiKeyGetDto
                , "/inbound/out/getValidApiKey"
                , new ParameterizedTypeReference<ResponseObject<Boolean>>() {}
        ).getData();

        if (response) {
            throw new UnauthorizedException(ExceptionCode.ERROR_CODE_1018);
        }
    }
}
