package kr.co.homeplus.inbound.claim.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("클레임 승인 요청")
public class ClaimApproveSet {
    @ApiModelProperty(notes = "승인 항목")
    private List<ClaimApproveItemSet> approveList;
}
