package kr.co.homeplus.inbound.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import kr.co.homeplus.inbound.constants.PatternConstants;
import org.apache.commons.lang3.time.DateUtils;

public class DateUtil {

	public static boolean isDateBefore(final String dt1, final String dt2) {
		final DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern(PatternConstants.DATE_TIME_DF_FORMAT);
		final DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern(PatternConstants.DATE_TIME_DF_FORMAT);
		final LocalDateTime dateTime1 = LocalDateTime.parse(dt1, formatter1);
		final LocalDateTime dateTime2 = LocalDateTime.parse(dt2, formatter2);
		return dateTime2.isBefore(dateTime1);
	}

	public static boolean isDateTimeBefore(final String dt1, final String dt2) {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PatternConstants.DATE_TIME_FORMAT);
		final LocalDateTime dateTime1 = LocalDateTime.parse(dt1, formatter);
		final LocalDateTime dateTime2 = LocalDateTime.parse(dt2, formatter);
		return dateTime2.isBefore(dateTime1);
	}

	public static boolean isDateTimeBefore(final String dt1, final String format1, final String dt2, final String format2) {
		final DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern(format1);
		final DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern(format2);
		final LocalDateTime dateTime1 = LocalDateTime.parse(dt1, formatter1);
		final LocalDateTime dateTime2 = LocalDateTime.parse(dt2, formatter2);
		return dateTime2.isBefore(dateTime1);
	}

	public static boolean isCurrentDateBefore(String dt) {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PatternConstants.DATE_TIME_FORMAT);
		final LocalDateTime currentDate = LocalDateTime.now();
		final LocalDateTime dateTime = LocalDateTime.parse(dt, formatter);
		return currentDate.toLocalDate().isAfter(dateTime.toLocalDate());
	}

	public static boolean isCurrentDateBefore(final String dt1, final String format1) {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format1);
		final LocalDateTime currentDate = LocalDateTime.now();
		final LocalDateTime dateTime = LocalDateTime.parse(dt1, formatter);
		return currentDate.toLocalDate().isAfter(dateTime.toLocalDate());
	}

	public static boolean isCurrentDateTimeBefore(final String dt1, final String format1) {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format1);
		final LocalDateTime currentDate = LocalDateTime.now();
		final LocalDateTime dateTime = LocalDateTime.parse(dt1, formatter);
		return currentDate.isAfter(dateTime);
	}

	public static boolean isCurrentTimeAfter(String dt) {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PatternConstants.DATE_TIME_FORMAT);
		final LocalDateTime currentDate = LocalDateTime.parse(LocalDateTime.now().format(formatter), formatter);
		final LocalDateTime dateTime = LocalDateTime.parse(dt, formatter);
		return dateTime.isAfter(currentDate);
	}

	public static String setCurrentDateBefore(String dt) {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PatternConstants.DATE_TIME_FORMAT);
		final LocalDateTime currentDate = LocalDateTime.now();
		final LocalDateTime dateTime = LocalDateTime.parse(dt, formatter);
		if (currentDate.toLocalDate().isAfter(dateTime.toLocalDate())) {
			return currentDate.format(formatter);
		} else {
			return dt;
		}
	}

	// 제한 month(월) 여부 EX) plusMonth:3 일경우 3개월 초과시 true
	public static Boolean isBeforeMonth(String startDt, String endDt, int plusMonth) {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PatternConstants.DATE_DF_FORMAT);
		final LocalDate endDtLocal = LocalDate.parse(endDt, formatter);
		final LocalDate startDtLocal = LocalDate.parse(startDt, formatter);
		final LocalDate startDtPlus = startDtLocal.plusMonths(plusMonth);

		return startDtPlus.isBefore(endDtLocal);
	}

	/**
	 * 날짜 문자열이 지정한 날짜 패턴과 일치하는지 체크(엄격모드)
	 * @param dateStr 날짜문자열
	 * @param parsePattern 문자열 패턴
	 * @return true: 패턴일치, false: 패턴일치하지 않음
	 */
	public static boolean isValidParseDateStrictly(String dateStr, String parsePattern) {
		try {
			DateUtils.parseDateStrictly(dateStr, parsePattern);
			return true;
		} catch (Exception e) {}
		return false;
	}

	/**
	 * 날짜 문자열이 지정한 패턴과 일치하는 경우 LocalDate타입으로 반환, 일치하지 않는 경우에는 NULL반환.
	 * @param dateStr 날짜문자열
	 * @param parsePattern 문자열 패턴
	 * @return 날짜문자열 Parsing성공 : LocalDate, Parsing실패 : NULL
	 */
	public static LocalDate getParseLocalDateStrictly(String dateStr, String parsePattern) {
		try {
			DateUtils.parseDateStrictly(dateStr, parsePattern);
            return LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(parsePattern));
		} catch (Exception e) {}
		return null;
	}

	/**
	 * 날짜 문자열이 지정한 패턴과 일치하는 경우 LocalDateTime타입으로 반환, 일치하지 않는 경우에는 NULL반환.
	 * @param dateStr 날짜문자열
	 * @param parsePattern 문자열 패턴
	 * @return 날짜문자열 Parsing성공 : LocalDate, Parsing실패 : NULL
	 */
	public static LocalDateTime getParseLocalDateTimeStrictly(String dateStr, String parsePattern) {
		try {
			DateUtils.parseDateStrictly(dateStr, parsePattern);
			return LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(parsePattern));
		} catch (Exception e) {}
		return null;
	}

}

