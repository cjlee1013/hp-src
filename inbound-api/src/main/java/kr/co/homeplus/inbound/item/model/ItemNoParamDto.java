package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@ApiModel("상품번호 리스트 조회 (업체상품코드 기준) 검색")
public class ItemNoParamDto {

	@ApiModelProperty(value = "업체상품코드리스트", required = true, position = 1)
	private List<String> sellerItemCdList;

	@ApiModelProperty(value = "판매업체ID", position = 2)
	private String partnerId;

}