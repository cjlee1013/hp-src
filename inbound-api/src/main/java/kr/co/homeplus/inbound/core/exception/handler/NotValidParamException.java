package kr.co.homeplus.inbound.core.exception.handler;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class NotValidParamException extends CommonException {
    public NotValidParamException(String version, String msg) {
        super(version, msg);
    }
}
