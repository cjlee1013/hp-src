package kr.co.homeplus.inbound.item.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 인증 정보
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("상품 안전인증 정보")
public class ItemCertSetDto {

	@ApiModelProperty(value = "인증유형", position = 1)
	@JsonProperty(value = "kcCertificationType")
	private String 	certType;

	public void setCertType(String certType) {
		this.certType = certType.toUpperCase();
	}

	@ApiModelProperty(value = "인증번호 (최대 50자)", position = 2)
	@JsonProperty(value = "kcCertificationNo")
	private String 	certNo;

}
