package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel("발송지연안내 요청")
public class ShipDelaySet {
    @ApiModelProperty(notes = "배송SEQ 리스트", example = "3000000001", position = 1)
    private String bundleNo;

    @ApiModelProperty(notes = "발송지연사유코드(SHORTSTOCK:재고부족,PRESSUREOFORDER:주문폭주,CUSTOMERREQUEST:고객요청,ETC:기타)", example = "SHORTSTOCK", position = 2)
    private String delayShipCode;

    @ApiModelProperty(notes = "[Optional]발송지연사유메시지", example = "재고가 부족하여 2021년 1월 30일까지 배송될 예정입니다", position = 3)
    private String delayShipMessage;

    @ApiModelProperty(notes = "2차발송기한", example = "2021-01-30", position = 4)
    private String delayShipDate;
}
