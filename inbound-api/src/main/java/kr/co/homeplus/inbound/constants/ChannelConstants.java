package kr.co.homeplus.inbound.constants;

/**
 * 채널 관련 공통 상수
 */
public class ChannelConstants {
    public static final String PARTNER = "PARTNER";

	public static final String ADMIN = "ADMIN";

    public static final String INBOUND = "INBOUND";

}
