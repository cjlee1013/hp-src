package kr.co.homeplus.inbound.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value="마켓 상품 재고")
@Getter
@Setter
public class MarketOrderItem {

    @ApiModelProperty(value = "상품번호", position = 1)
    private String itemNo;

    @ApiModelProperty(value = "마켓상품번호", position = 2)
    private String marketItemNo;

    @ApiModelProperty(value = "주문수량", position = 3)
    private int orderQty;

    @ApiModelProperty(value = "마켓상품가격", position = 4)
    private int marketItemPrice;

}
