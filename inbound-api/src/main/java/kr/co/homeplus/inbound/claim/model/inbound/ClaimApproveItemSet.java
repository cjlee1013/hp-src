package kr.co.homeplus.inbound.claim.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("클레임 승인 항목")
public class ClaimApproveItemSet {
    @NotNull(message = "클레임번호")
    @ApiModelProperty(value = "클레임번호", position = 1, example = "1500000022")
    private long claimNo;

    @NotNull(message = "클레임번들번호")
    @ApiModelProperty(value = "클레임번들번호", position = 2, example = "1500000022")
    private long claimBundleNo;

    @NotNull(message = "클레임타입")
    @ApiModelProperty(value = "클레임타입(취소:CANCEL,반품:PICK,교환:EXCHANGE", position = 3, example = "CANCEL")
    private String claimType;
}
