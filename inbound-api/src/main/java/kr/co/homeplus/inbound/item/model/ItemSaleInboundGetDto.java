package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 판매정보 Get Entry
 */
@Getter
@Setter
@ApiModel("상품 판매정보 조회")
public class ItemSaleInboundGetDto {
	/**
	 * 판매정보
	 */
	@ApiModelProperty(value = "정상가격", required = true, position = 1)
	private Integer originPrice;

	@ApiModelProperty(value = "판매가격", required = true, position = 2)
	private Integer salePrice;

	@ApiModelProperty(value = "할인판매여부 기준", position = 3)
	private String discountYn;

	@ApiModelProperty(value = "할인가격", position = 4)
	private Integer discountPrice;

	@ApiModelProperty(value = "할인기간여부", position = 5)
	private String discountPeriodYn;

	@ApiModelProperty(value = "할인시작일", position = 6)
	private String discountStartDate;

	@ApiModelProperty(value = "할인종료일", position = 7)
	private String discountEndDate;

	@ApiModelProperty(value = "과세여부명(과세,면세)", required = true, position = 8)
	private String taxYnName;

	@ApiModelProperty(value = "판매기간 (Y:설정함, N:설정안함)", position = 9)
	private String salePeriodYn;

	@ApiModelProperty(value = "판매기간시작일(YYYY-MM-DD HH:00:00)", required = true, position = 10)
	private String saleStartDate;

	@ApiModelProperty(value = "판매기간종료일(YYYY-MM-DD HH:59:59)", required = true, position = 11)
	private String saleEndDate;

	@ApiModelProperty(value = "예약판매사용여부 (Y: 예약, N: 예약안함)", required = true, position = 12)
	private String reserveYn;

	@ApiModelProperty(value = "예약판매시작일(YYYY-MM-DD)", position = 13)
	private String reserveStartDate;

	@ApiModelProperty(value = "예약판매종료일(YYYY-MM-DD)" , position = 14)
	private String reserveEndDate;

	@ApiModelProperty(value = "예약상품 출하 시작일(YYYY-MM-DD)", position = 15)
	private String reserveShipStartDate;

	@ApiModelProperty(value = "판매수량", required = true, position = 16)
	private int stockQty;

	@ApiModelProperty(value = "판매단위수량", required = true, position = 17)
	private int saleUnitQty;

	@ApiModelProperty(value = "구매제한 - 최소구매수량", required = true, position = 18)
	private int purchaseMinQty;

	@ApiModelProperty(value = "1인당 구매제한 사용여부 (미사용:Y, 사용:N)", required = true, position = 19)
	private String purchaseLimitYn;

	@ApiModelProperty(value = "구매제한 타입 - 1회 : O / 기간제한 : P", position = 20)
	private String purchaseLimitDuration;

	@ApiModelProperty(value = "구매제한 일자 - ?일", position = 21)
	private int purchaseLimitDay;

	@ApiModelProperty(value = "구매제한 개수 - ?개", position = 22)
	private int purchaseLimitQty;

	@ApiModelProperty(value = "장바구니 제한여부(Y:제한, N:제한안함)", required = true, position = 23)
	private String cartLimitYn;

	@ApiModelProperty(value = "선물하기사용여부 (Y: 사용, N: 사용안함)", required = true, position = 24)
	private String giftYn;

	@ApiModelProperty(value = "판매된 수량", required = true, position = 25)
	private int salesCount;

	/**
	 * 상품 상세정보
	 */
	@ApiModelProperty(value = "상품상세정보", required = true, position = 26)
	private String itemDescription;

}