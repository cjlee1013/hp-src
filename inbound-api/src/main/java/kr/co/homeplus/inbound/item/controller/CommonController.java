package kr.co.homeplus.inbound.item.controller;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 공통 Controller
 */
@ApiIgnore
@Api(tags = "공통")
@Controller
@RequiredArgsConstructor
public class CommonController {

    @RequestMapping("/index")
    public String getIndex() {
        return "index";
    }

    @RequestMapping("/apiSwaggerUI")
    public String getApiSwaggerUI() {
        return "swagger-ui";
    }

    @RequestMapping("/apiSwaggerNewUI")
    public String getApiSwaggerNewUI() {
        return "swagger-ui-new/index";
    }

    @RequestMapping("/apiSwaggerDocs")
    public String getApiDocs() {
        return "forward:/v2/api-docs";
    }

}


