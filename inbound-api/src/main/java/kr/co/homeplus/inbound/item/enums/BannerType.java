package kr.co.homeplus.inbound.item.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum BannerType {

    HMP( "HMP", "홈플러스")
    ,   SELLER("SELLER", "판매자별")
    ,   EVENT( "EVENT", "마케팅/이벤트");

    @Getter
    private final String type;

    @Getter
    private final String description;


}
