package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품 속성정보 조회")
public class ItemPropInboundGetDto {

	@ApiModelProperty(value = "브랜드번호", position = 1)
	private String brandNo;

	@ApiModelProperty(value = "브랜드명", position = 2)
	private String brandName;

	@ApiModelProperty(value = "제조사번호", position = 3)
	private String makerNo;

	@ApiModelProperty(value = "제조사명", position = 4)
	private String makerName;

	@ApiModelProperty(value = "제조일자(YYYY-MM-DD)", position = 5)
	private String makeDate;

	@ApiModelProperty(value = "유효일자(YYYY-MM-DD)", position = 6)
	private String expirationDate;

	@ApiModelProperty(value = "상품고시정보", position = 7)
	private List<ItemNoticeInboundGetDto> noticeList;

	@ApiModelProperty(value = "상품고시정보 정책코드", position = 8)
	private Long groupNoticeNo;

}