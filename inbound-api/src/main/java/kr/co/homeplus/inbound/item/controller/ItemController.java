package kr.co.homeplus.inbound.item.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import kr.co.homeplus.inbound.common.service.CommonValidService;
import kr.co.homeplus.inbound.item.model.ItemNoGetDto;
import kr.co.homeplus.inbound.item.model.ItemNoListParamDto;
import kr.co.homeplus.inbound.item.model.ItemInboundGetDto;
import kr.co.homeplus.inbound.item.model.ItemSetDto;
import kr.co.homeplus.inbound.item.model.ItemStatusSetDto;
import kr.co.homeplus.inbound.item.service.ItemRelationService;
import kr.co.homeplus.inbound.item.service.ItemService;
import kr.co.homeplus.inbound.itemRelation.model.ItemRelationInboundSetDto;
import kr.co.homeplus.inbound.itemRelation.model.ItemRelationListGetDto;
import kr.co.homeplus.inbound.response.ResponseResult;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "상품 등록/수정")
@ApiResponses(value = {
    @ApiResponse(code = 422, message = "Unprocessable Entity")
})
@RequestMapping("/item")
@RestController
@RequiredArgsConstructor
public class ItemController {
    private final CommonValidService commonValidService;
    private final ItemService itemService;
    private final ItemRelationService itemRelationService;

    @ApiOperation(value = "상품번호 리스트 조회 (업체상품코드 기준)", response = String.class)
    @GetMapping(value = "/getItemNoList")
    public ResponseObject getItemNoList(
            @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
            @ApiParam(name = "sellerItemCodeList", value = "업체상품코드", required = true) @RequestParam List<String> sellerItemCodeList) throws Exception {

        return ResourceConverter.toResponseObject(itemService.getItemNoList(sellerItemCodeList, commonValidService.validApiKeyHandler(apiKey)));
    }

    @ApiOperation(value = "상품번호 리스트 조회 (기간)", response = ItemNoGetDto.class)
    @GetMapping(value = "/getItemNoListByDate")
    public ResponseObject getItemNoListByDate(
            @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
            @Valid ItemNoListParamDto itemNoListParamDto) throws Exception {
        itemNoListParamDto.setPartnerId(commonValidService.validApiKeyHandler(apiKey));
        return ResourceConverter.toResponseObject(itemService.getItemNoListByDate(itemNoListParamDto));
    }

    @ApiOperation(value = "상품조회", response = ItemInboundGetDto.class)
    @GetMapping(value = "/getItem")
    public ResponseObject getItem(
            @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
            @ApiParam(name = "itemNo", value = "상품번호", required = true) @RequestParam String itemNo) throws Exception {

        return itemService.getItem(itemNo, commonValidService.validApiKeyHandler(apiKey));
    }

    @ApiOperation(value = "연관상품 조회", response = ItemRelationListGetDto.class)
    @GetMapping(value = "/getItemRelation")
    public ResponseObject getCategoryList(
            @ApiParam(value = "apiKey", required = true) @RequestHeader(value = "apiKey") String apiKey,
            @RequestParam @ApiParam(value = "연관상품번호(그룹번호)", required = true) Long relationNo
    ) throws Exception {
        return ResourceConverter.toResponseObject(itemRelationService.getItemRelation( apiKey, relationNo));
    }

    @ApiOperation(value = "연관 상품 등록/수정" , response = ResponseResult.class)
    @PostMapping(value = "/setItemRelation" )
    public ResponseObject setItemRelation (
        @ApiParam(value = "apiKey", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @RequestBody ItemRelationInboundSetDto itemRelationInboundSetDto
    ) throws Exception {
        ResponseResult responseResult = itemRelationService.setItemRelationHandler(apiKey,
            itemRelationInboundSetDto);
        return ResourceConverter.toResponseObject(responseResult);
    }

    @ApiOperation(value = "상품 등록/수정", response = ResponseResult.class)
    @PostMapping(value = "/setItem")
    public ResponseObject setItem(
            final HttpServletRequest req,
            @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
            @RequestBody @Valid ItemSetDto itemSetDto
    ) throws Exception {

        return itemService.setItemHandler(itemSetDto,  commonValidService.validApiKeyHandler(apiKey), req);
    }

    @ApiOperation(value = "상품 상태변경", response = ResponseResult.class)
    @PostMapping(value = "/setItemStatus")
    public ResponseObject setItemStatus(
            @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
            @RequestBody @Valid ItemStatusSetDto itemStatusSetDto
    ) throws Exception {
        itemStatusSetDto.setPartnerId(commonValidService.validApiKeyHandler(apiKey));
        return itemService.setItemStatus(itemStatusSetDto);
    }

}
