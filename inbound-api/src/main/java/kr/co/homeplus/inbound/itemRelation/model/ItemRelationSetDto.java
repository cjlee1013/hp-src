package kr.co.homeplus.inbound.itemRelation.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ItemRelationSetDto {

    @ApiModelProperty(value = "연관상품관리번호")
    private Long relationNo;

    @ApiModelProperty(value = "연관명")
    @NotEmpty(message = "연관명")
    private String relationNm;

    @ApiModelProperty(value = "노출여부")
    @NotEmpty(message = "노출여부")
    private String dispYn;

    @ApiModelProperty(value = "PC노출기준")
    private String dispPcType;

    @ApiModelProperty(value = "모바일노출기준")
    private String dispMobileType;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "소카테고리")
    private String scateCd;

    @ApiModelProperty(value = "인트로이미지URL")
    private String imgUrl;

    @ApiModelProperty(value = "인트로이미지 사용여부")
    private String imgUseYn;

    @ApiModelProperty(value = "사용여부")
    private String useYn;

    @ApiModelProperty(value = "등록/수정자")
    private String userId;

    @ApiModelProperty(value = "연관상품 리스트")
    List<String> relationItemList;

    @ApiModelProperty(value = "경로", hidden = true)
    private String channel;
}
