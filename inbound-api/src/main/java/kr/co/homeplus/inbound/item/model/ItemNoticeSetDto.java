package kr.co.homeplus.inbound.item.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품정보고시 정보")
public class ItemNoticeSetDto {

    @ApiModelProperty(value = "고시항목번호", required = true, position = 1)
    @NotNull(message = "고시항목번호")
    private int noticeNo;

    @JsonProperty(value = "description")
    @ApiModelProperty(value = "정보고시항목명 (최대: 500자)", required = true, position = 2)
    @Size(min = 1, max = 500, message = "정보고시항목명")
    @NotEmpty(message = "정보고시항목명")
    private String noticeDesc;

}
