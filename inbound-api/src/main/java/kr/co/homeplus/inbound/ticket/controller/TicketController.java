package kr.co.homeplus.inbound.ticket.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import kr.co.homeplus.inbound.response.ResponseResult;
import kr.co.homeplus.inbound.ticket.model.CoopTicketUpdateStatusGetDto;
import kr.co.homeplus.inbound.ticket.model.CoopUpdateRequestDto;
import kr.co.homeplus.inbound.ticket.service.TicketService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "티켓관리")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found")
    ,@ApiResponse(code = 405, message = "Method Not Allowed")
    ,@ApiResponse(code = 422, message = "Unprocessable Entity")
    ,@ApiResponse(code = 500, message = "Internal Server Error")
})

@RequestMapping("/ticket")
@RestController
@RequiredArgsConstructor
public class TicketController {

    private final TicketService ticketService;

    @ApiOperation(value = "쿠프 티켓상태 변경", response = ResponseResult.class)
    @PostMapping(value = "/coop/setTicketStatus")
    public CoopTicketUpdateStatusGetDto setTicketStatus(@RequestBody @Valid CoopUpdateRequestDto coopUpdateRequestDto) throws Exception {
        return ticketService.setTicketStatus(coopUpdateRequestDto);
    }


}
