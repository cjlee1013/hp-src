package kr.co.homeplus.inbound.basic.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품정보고시 정보")
public class ItemNoticeDto {
	@ApiModelProperty(value = "고시정보 항목코드", position = 5)
	private String groupNoticeNo;

	@ApiModelProperty(value = "고시정보 항목명", position = 10)
	private String groupNoticeName;

	@ApiModelProperty(value = "고시유형 코드", position = 15)
	private String noticeNo;

	@ApiModelProperty(value = "고시유형 명", position = 20)
	private String noticeName;


}
