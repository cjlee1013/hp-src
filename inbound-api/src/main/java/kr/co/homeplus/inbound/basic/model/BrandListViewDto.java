package kr.co.homeplus.inbound.basic.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.beanutils.BeanUtils;

@Data
@ApiModel("브랜드 리스트")
public class BrandListViewDto {

    @ApiModelProperty(value = "브랜드 코드")
    private long brandNo;

    @ApiModelProperty(value = "한글 브랜드명")
    private String brandName;

    @ApiModelProperty(value = "영문 브랜드명")
    private String brandNameEng;

    @ApiModelProperty(value = "브랜드 이미지")
    private String imageUrl;

    @ApiModelProperty(value = "브랜드 소개")
    private String brandDescription;

    @ApiModelProperty(value = "홈페이지 URL")
    private String siteUrl;

    @ApiModelProperty(value = "상품명 노출여부 코드")
    private String itemDisplayYn;

    @ApiModelProperty(value = "전시명 코드")
    private String displayName;

    @ApiModelProperty(value = "초성")
    private String initial;

}
