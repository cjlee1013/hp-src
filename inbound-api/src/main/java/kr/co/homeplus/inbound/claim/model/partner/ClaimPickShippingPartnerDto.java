package kr.co.homeplus.inbound.claim.model.partner;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimPickShippingPartnerDto {

    @ApiModelProperty(value = "클레임 반품수거 번호", position = 1, example = "23")
    public String claimPickShippingNo;

    @ApiModelProperty(value = "수거지-이름", position = 2, example = "홍길동")
    public String pickReceiverNm;

    @ApiModelProperty(value = "수거지-휴대폰", position = 3, example = "010-1111-1111")
    public String pickMobileNo;

    @ApiModelProperty(value = "수거지 우편번호", position = 4, example = "07297")
    public String pickZipcode;

    @ApiModelProperty(value = "수거지 기본주소", position = 5, example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    public String pickAddr;

    @ApiModelProperty(value = "수거지 샹세주소", position = 6, example = "금천전 홈플러스 1층")
    public String pickAddrDetail;

    @ApiModelProperty(value = "수거배송수단 (C:택배배송 P:우편배송  D:업체직배송 N:배송없음)", position = 7, example = "택배배송")
    public String pickShipType;

    @ApiModelProperty(value = "수거배송수단코드 (C:택배배송 P:우편배송  D:업체직배송 N:배송없음)", position = 7, example = "C")
    public String pickShipTypeCode;

    @ApiModelProperty(value = "수거택배사코드", position = 8, example = "002")
    public String pickDlvCd;

    @ApiModelProperty(value = "수거택배사명", position = 9, example = "한진택배")
    public String pickDlv;

    @ApiModelProperty(value = "수거송장번호", position = 10, example = "123123123")
    public String pickInvoiceNo;

    @ApiModelProperty(value = "수거접수타입", position = 11, example = "일반접수")
    public String pickRegisterType;

    @ApiModelProperty(value = "수거메모", position = 12, example = "우편수거 메모입니다.")
    public String pickMemo;

    @ApiModelProperty(value="수거상태", position = 13, example = "수거요청")
    public String pickStatus;

    @ApiModelProperty(value="수거상태코드", position = 14, example = "P1")
    public String pickStatusCode;

    @ApiModelProperty(value="수거일자", position = 15, example = "2020-12-29 00:00:00")
    public String shipDt;

    @ApiModelProperty(value="수거shiftID", position = 16, example = "14242")
    public String shiftId;

    @ApiModelProperty(value="수거slotID", position = 17, example = "12324")
    public String slotId;

    @ApiModelProperty(value="수거 예약접수자", position = 18, example = "2020-12-29 00:00:00")
    public String pickP0Id;

    @ApiModelProperty(value="수거 예약일(업체직배송인 경우 수거예정일 최초등록일)", position = 19, example = "2020-12-29 00:00:00")
    public String pickP0Dt;

    @ApiModelProperty(value="수거 송장등록일(업체직배송의 경우 수거예정일)", position = 20, example = "2020-12-29 00:00:00")
    public String pickP1Dt;

    @ApiModelProperty(value="수거 시작일", position = 21, example = "2020-12-29 00:00:00")
    public String pickP2Dt;

    @ApiModelProperty(value="수거 완료일", position = 22, example = "2020-12-29 00:00:00")
    public String pickP3Dt;

    @ApiModelProperty(value="점포명", position = 23, example = "금천점")
    public String storeNm;

    @ApiModelProperty(value="수거지 지번주소", position = 24, example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    public String pickBaseAddr;

    @ApiModelProperty(value="배송타입", position = 25, example = "DS")
    public String shipType;

    @ApiModelProperty(value="수거 장소", position = 26, example = "수거장소 상세")
    public String placeAddrExtnd;

    @ApiModelProperty(value="수거 시작시간", position = 27, example = "10:00")
    public String startTime;

    @ApiModelProperty(value="수거 종료시간", position = 28, example = "12:00")
    public String endTime;

    @ApiModelProperty(value="수거방법", position = 29, example = "DS_DLV")
    public String shipMethod;
}
