package kr.co.homeplus.inbound.claim.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.inbound.claim.enums.ClaimStatus;
import kr.co.homeplus.inbound.claim.enums.ClaimType;
import kr.co.homeplus.inbound.claim.model.partner.ClaimDetailItemPartner;
import kr.co.homeplus.inbound.claim.model.partner.ClaimDetailPartnerGetDto;
import kr.co.homeplus.inbound.claim.model.partner.ClaimExchShippingPartnerDto;
import kr.co.homeplus.inbound.claim.model.partner.ClaimPickShippingPartnerDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("클레임 상세 응답")
public class ClaimDetailGet {
    @ApiModelProperty(value = "클레임타입", example = "CANCEL", position = 2)
    private String claimType;

    @ApiModelProperty(value = "처리상태", example = "WITHDRAW", position = 3)
    private String claimStatus;

    @ApiModelProperty(value = "배송번호", example = "3000000004", position = 7)
    private String bundleNo;

    @ApiModelProperty(value = "클레임번호", example = "1500000001", position = 9)
    private long claimBundleNo;

    @ApiModelProperty(value = "클레임 사유", example = "결품(체크아웃누락)", position = 10)
    private String claimReasonType;

    @ApiModelProperty(value = "클레임 상세사유", example = "구매취소합니다.", position = 11)
    private String claimReasonDetail;

    @ApiModelProperty(value = "첨부파일1", example = "https://image.homeplus.kr/cl/f103d615-8150-4a57-945d-c4ee4ba053d4", position = 12)
    private String uploadFileName;

    @ApiModelProperty(value = "첨부파일2", example = "https://image.homeplus.kr/cl/f103d615-8150-4a57-945d-c4ee4ba053d4", position = 13)
    private String uploadFileName2;

    @ApiModelProperty(value = "첨부파일3", example = "https://image.homeplus.kr/cl/f103d615-8150-4a57-945d-c4ee4ba053d4", position = 14)
    private String uploadFileName3;

    @ApiModelProperty(value = "배송비 동봉 여부 Y:택배동봉, N:환불금액 차감", example = "Y", position = 15)
    private String claimShipFeeEnclose;

    @ApiModelProperty(value = "클레임 귀책자", example = "판매자", position = 16)
    private String whoReason;

    @ApiModelProperty(value = "추가 배송비", example = "3000", position = 17)
    private long totalAddShipPrice;

    @ApiModelProperty(value = "추가 도서산간 배송비", example = "3000", position = 18)
    private long totalAddIslandShipPrice;

    @ApiModelProperty(value="반품 수거지", position = 19)
    private PickShippingGet pickShipping;

    @ApiModelProperty(value="교환 수거지", position = 20)
    private ExchangeShippingGet exchangeShipping;

    @ApiModelProperty(value = "클레임 상품 리스트", position = 21)
    private List<ClaimDetailItemGet> itemList;

    public ClaimDetailGet(ClaimDetailPartnerGetDto claimDetailPartnerGetDto,String imageUrl) {
        String claimType = claimDetailPartnerGetDto.getClaimTypeCode();
        if (claimType != null) {
            this.claimType = ClaimType.valueOf(claimType).getDescription();
        }
        String claimStatus = claimDetailPartnerGetDto.getClaimStatusCode();
        if (claimStatus != null) {
            this.claimStatus = ClaimStatus.valueOf(claimStatus).getDescription();
        }
        this.bundleNo = claimDetailPartnerGetDto.getBundleNo();
        this.claimBundleNo = claimDetailPartnerGetDto.getClaimBundleNo();
        this.claimReasonType = claimDetailPartnerGetDto.getClaimReasonType();
        this.claimReasonDetail = claimDetailPartnerGetDto.getClaimReasonDetail();
        this.uploadFileName = imageUrl + claimDetailPartnerGetDto.getUploadFileName();
        this.uploadFileName2 = imageUrl + claimDetailPartnerGetDto.getUploadFileName2();
        this.uploadFileName3 = imageUrl + claimDetailPartnerGetDto.getUploadFileName3();
        this.claimShipFeeEnclose = claimDetailPartnerGetDto.getClaimShipFeeEnclose();
        this.whoReason = claimDetailPartnerGetDto.getWhoReason();
        this.totalAddShipPrice = claimDetailPartnerGetDto.getTotAddShipPrice();
        this.totalAddIslandShipPrice = claimDetailPartnerGetDto.getTotAddIslandShipPrice();
        ClaimPickShippingPartnerDto claimPickShippingPartnerDto = claimDetailPartnerGetDto.getClaimPickShippingDto();
        if (claimPickShippingPartnerDto != null) {
            this.pickShipping = new PickShippingGet(claimPickShippingPartnerDto);
        }
        ClaimExchShippingPartnerDto claimExchShippingPartnerDto = claimDetailPartnerGetDto.getClaimExchShippingDto();
        if (claimExchShippingPartnerDto != null) {
            this.exchangeShipping = new ExchangeShippingGet(claimExchShippingPartnerDto);
        }
        this.itemList = new ArrayList<>();
        for (ClaimDetailItemPartner claimDetailItemPartner : claimDetailPartnerGetDto.getClaimDetailItemList()){
            this.itemList.add(new ClaimDetailItemGet(claimDetailItemPartner));
        }
    }
}
