package kr.co.homeplus.inbound.shipping.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ShipMethod {

        DS_DLV("DS_DLV", "DELIVERY")
    ,   DS_DRCT("DS_DRCT", "DIRECT")
    ,   DS_PICK("DS_PICK", "PICK")
    ,   DS_POST("DS_POST", "POST")
    ,   DS_QUICK("DS_QUICK", "QUICK")
    ,   N("N", "NONE")
    ,   DELIVERY("DS_DLV", "DELIVERY")
    ,   DIRECT("DS_DRCT", "DIRECT")
    ,   PICK("DS_PICK", "PICK")
    ,   POST("DS_POST", "POST")
    ,   QUICK("DS_QUICK", "QUICK")
    ,   NONE("N", "NONE")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;


}
