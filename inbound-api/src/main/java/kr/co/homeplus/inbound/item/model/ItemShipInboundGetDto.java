package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("상품 배송정보 조회")
public class ItemShipInboundGetDto {

	@ApiModelProperty(value = "배송정책번호", position = 1)
	private Long shipPolicyNo;

	@ApiModelProperty(value = "출고 기한(-1:미정)", position = 2)
	private Integer releaseDay;

	@ApiModelProperty(value = "출고기한 1일 인 경우 출고시간(1~24)", position = 3)
	private Integer releaseTime;

	@ApiModelProperty(value = "휴일제외여부 (Y: 주말, 공휴일 제외, N: 주말, 공휴일 포함)", position = 4)
	private String holidayExceptYn;

	@ApiModelProperty(value = "반품/교환 배송비", position = 5)
	private Integer claimShipFee;

	@ApiModelProperty(value = "배송비 종류 ( FREE : 무료 / FIXED : 유료 / COND : 조건부 무료 )", required = true, position = 6)
	private String shipKind;

	@ApiModelProperty(value = "배송비 종류명 ( FREE : 무료 / FIXED : 유료 / COND : 조건부 무료 )", required = true, position = 7)
	private String shipKindName;

	@ApiModelProperty(value = "배송방법 (DS_DLV:택배배송, DS_DRCT:직접배송, DS_POST:우편배송, DS_QUICK:퀵배송)", required = true, position = 8)
	private String shipMethod;

	@ApiModelProperty(value = "배송방법명 (DS_DLV:택배배송, DS_DRCT:직접배송, DS_POST:우편배송, DS_QUICK:퀵배송)", required = true, position = 9)
	private String shipMethodName;

	@ApiModelProperty(value = "배송유형(ITEM:상품별/BUNDLE:묶음배송)", required = true, position = 10)
	private String shipTypeName;

	@ApiModelProperty(value = "배송유형(ITEM:상품별/BUNDLE:묶음배송)", position = 11)
	private String shipType;

	@ApiModelProperty(value = "배송비", required = true, position = 12)
	private int shipFee;

	@ApiModelProperty(value = "무료조건비", required = true, position = 13)
	private int freeCondition;

	@ApiModelProperty(value = "선결제여부(Y:선결제, N:착불)", required = true, position = 14)
	private String prepaymentYn;

	@ApiModelProperty(value = "배송비노출여부", required = true, position = 15)
	private String displayYn;

	@ApiModelProperty(value = "회수지 우편번호", position = 16)
	private String returnZipcode;

	@ApiModelProperty(value = "회수지 주소1(우편번호제공)", position = 17)
	private String returnAddress1;

	@ApiModelProperty(value = "회수지 주소2(직접입력)", position = 18)
	private String returnAddress2;

	@ApiModelProperty(value = "차등설정", required = true, position = 19)
	private String differenceYn;

	@ApiModelProperty(value = "차등개수", position = 20)
	private int differenceQty;
}