package kr.co.homeplus.inbound.claim.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("클레임 보류 요청")
public class ClaimPendingSet {

    @NotNull(message = "클레임번호")
    @ApiModelProperty(value = "클레임그룹번호", position = 1, example = "1500000011")
    private long claimNo;

    @NotNull(message = "클레임번들번호")
    @ApiModelProperty(value = "클레임번호", position = 2, example = "1500000001")
    private long claimBundleNo;

    @ApiModelProperty(value = "클레임사유코드 (코드 별첨)", position = 3, example = "P001")
    private String reasonType;

    @ApiModelProperty(value = "클레임상세사유", position = 4, example = "보류상세사유")
    private String reasonTypeDetail;

    @NotNull(message = "클레임타입")
    @ApiModelProperty(value = "클레임타입(취소:CANCEL,반품:PICK,교환:EXCHANGE", position = 3, example = "CANCEL")
    private String claimType;
}
