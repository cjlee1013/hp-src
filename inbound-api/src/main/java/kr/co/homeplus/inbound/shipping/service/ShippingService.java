package kr.co.homeplus.inbound.shipping.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.validation.Valid;
import kr.co.homeplus.inbound.core.constants.ResourceRouteName;
import kr.co.homeplus.inbound.shipping.model.inbound.DeliveryCodeGet;
import kr.co.homeplus.inbound.shipping.model.inbound.OrderClaimPossibleListGet;
import kr.co.homeplus.inbound.shipping.model.inbound.OrderConfirmSet;
import kr.co.homeplus.inbound.shipping.model.inbound.OrderBundleGet;
import kr.co.homeplus.inbound.shipping.model.inbound.OrderBundleSet;
import kr.co.homeplus.inbound.shipping.model.inbound.OrderDetailGet;
import kr.co.homeplus.inbound.shipping.model.inbound.ShipDelaySet;
import kr.co.homeplus.inbound.shipping.model.inbound.ShipCompleteSet;
import kr.co.homeplus.inbound.shipping.model.inbound.ShipStartGet;
import kr.co.homeplus.inbound.shipping.model.inbound.ShipStartSet;
import kr.co.homeplus.inbound.shipping.model.inbound.ShipStartResult;
import kr.co.homeplus.inbound.shipping.model.partner.ClaimOrderDetailInfoPartnerDto;
import kr.co.homeplus.inbound.shipping.model.partner.OrderDetailGetPartnerDto;
import kr.co.homeplus.inbound.shipping.model.partner.OrderShipManageConfirmOrderSetDto;
import kr.co.homeplus.inbound.shipping.model.partner.OrderShipManageListGetDto;
import kr.co.homeplus.inbound.shipping.model.partner.OrderShipManageListSetDto;
import kr.co.homeplus.inbound.shipping.model.partner.OrderShipManageNotiDelaySetDto;
import kr.co.homeplus.inbound.shipping.model.partner.OrderShipManageShipAllSetDto;
import kr.co.homeplus.inbound.shipping.model.partner.ShipCompanyManageListGetDto;
import kr.co.homeplus.inbound.shipping.model.partner.ShipStatusShipCompleteSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShippingService {

  private final ResourceClient resourceClient;

  /**
   * 발주/발송관리 리스트 조회
   */
  public List<OrderBundleGet> getOrderShipManageList(
      OrderBundleSet orderBundleSet,String Url, String partnerId) {
    if (orderBundleSet.getBundleNo() == null) {
      SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREAN);
      Date startDate, endDate;
      try {
        startDate = df.parse(orderBundleSet.getStartDate());
        endDate = df.parse(orderBundleSet.getEndDate());
        long between = Math.abs(startDate.getTime() - endDate.getTime());
        if (TimeUnit.DAYS.convert(between, TimeUnit.MILLISECONDS) > 5) {
          return null;
        }
      } catch (Exception e) {
        return null;
      }
    }

    OrderShipManageListSetDto orderShipManageListSetDto = new OrderShipManageListSetDto(
        orderBundleSet,partnerId);

    List<OrderShipManageListGetDto> orderShipManageListGetDtoList = resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
        orderShipManageListSetDto,
            "/partner/sell/" + Url,
            new ParameterizedTypeReference<ResponseObject<List<OrderShipManageListGetDto>>>(){}).getData();

    Map<String, OrderBundleGet> orderShipManageListGetDtoMap = new HashMap<>();
    OrderBundleGet orderBundleGet;
    String bundleNo;
    for (OrderShipManageListGetDto orderShipManageListGetDto : orderShipManageListGetDtoList) {
      bundleNo = orderShipManageListGetDto.getBundleNo();
      orderBundleGet = orderShipManageListGetDtoMap.get(bundleNo);
      if (orderBundleGet == null) {
        orderBundleGet = new OrderBundleGet(orderShipManageListGetDto);
        orderShipManageListGetDtoMap.put(bundleNo, orderBundleGet);
      } else {
        orderBundleGet.addOrderList(orderShipManageListGetDto);
      }
    }
    List<OrderBundleGet> order = new ArrayList<>();
    order.addAll(orderShipManageListGetDtoMap.values());
    return order;
  }

  /**
   * 주문확인
   */
  public ResponseObject<Integer> setConfirmOrder(
      OrderConfirmSet orderConfirmSet, String partnerId) throws Exception {
    OrderShipManageConfirmOrderSetDto orderShipManageConfirmOrderSetDto = new OrderShipManageConfirmOrderSetDto(
        orderConfirmSet,partnerId);

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        orderShipManageConfirmOrderSetDto,
        "/partner/sell/setConfirmOrder",
        new ParameterizedTypeReference<ResponseObject<Integer>>(){});
  }

  public OrderDetailGet getOrderDetail(String bundleNo, String partnerId) {
    ResponseObject<OrderDetailGetPartnerDto> orderDetailGetPartnerDto = resourceClient.getForResponseObject(
        ResourceRouteName.SHIPPING,
        "/partner/sell/getOrderDetailPop?bundleNo="+bundleNo+"&partnerId="+partnerId,
        new ParameterizedTypeReference<ResponseObject<OrderDetailGetPartnerDto>>() {});

    if (orderDetailGetPartnerDto != null){
      OrderDetailGet orderDetailGet = new OrderDetailGet(orderDetailGetPartnerDto.getData());
      return orderDetailGet;
    }
    return null;
  }

  public List<OrderClaimPossibleListGet> getClaimPossibleOrderList(String purchaseOrderNo, String bundleNo, String claimType, String partnerId) {
    ResponseObject<List<ClaimOrderDetailInfoPartnerDto>> claimOrderDetailPartnerDto = resourceClient.postForResponseObject(
        ResourceRouteName.ESCROWMNG,
        null,
        "/claim/orderCancel?purchaseOrderNo="+purchaseOrderNo+"&bundleNo="+bundleNo+"&claimType="+claimType,
        new ParameterizedTypeReference<ResponseObject<List<ClaimOrderDetailInfoPartnerDto>>>() {});

    if (claimOrderDetailPartnerDto != null){
      List<OrderClaimPossibleListGet> responseList = new ArrayList<>();
      for(ClaimOrderDetailInfoPartnerDto partnerDto : claimOrderDetailPartnerDto.getData()) {
        OrderClaimPossibleListGet getDto = new OrderClaimPossibleListGet(partnerDto);
        responseList.add(getDto);
      }
      return responseList;
    }
    return null;
  }


  /**
   * 발송지연안내
   */
  public ResponseObject<Integer> setNotiDelay(
      ShipDelaySet shipDelaySet, String partnerId) throws Exception {
    OrderShipManageNotiDelaySetDto orderShipManageNotiDelaySetDto = new OrderShipManageNotiDelaySetDto(
        shipDelaySet,partnerId);

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        orderShipManageNotiDelaySetDto,
        "/partner/sell/setNotiDelay",
        new ParameterizedTypeReference<ResponseObject<Integer>>(){});
  }

  /**
   * 발송처리
   */
  public ResponseObject<ShipStartGet> setShipAllExcel(@RequestBody @Valid List<ShipStartSet> shipStartSet, String partnerId) {
    List<ShipStartResult> shipStartResult = new ArrayList<>();
    ResponseObject<String> responseObject;
    int result;
    int successCnt = 0;
    for (ShipStartSet dto : shipStartSet) {
       responseObject = resourceClient.postForResponseObject(
          ResourceRouteName.SHIPPING,
          new OrderShipManageShipAllSetDto(dto,partnerId),
          "/partner/sell/setShipAll",
          new ParameterizedTypeReference<ResponseObject<String>>(){});
      try {
        result = Integer.parseInt(responseObject.getData());
        if (result > 0) {
          successCnt++;
          shipStartResult.add(new ShipStartResult(dto,null));
        } else {
          shipStartResult.add(new ShipStartResult(dto,responseObject.getReturnMessage()));
        }
      } catch (Exception e){
        shipStartResult.add(new ShipStartResult(dto,responseObject.getReturnMessage()));
      }
    }

    return ResourceConverter.toResponseObject(new ShipStartGet(String.valueOf(successCnt),shipStartResult));
  }

  /**
   * 선택발송처리
   */
  public ResponseObject<Object> setShipAll(ShipStartSet shipStartSet, String partnerId) throws Exception {
    OrderShipManageShipAllSetDto orderShipManageShipAllSetDto = new OrderShipManageShipAllSetDto(
        shipStartSet,partnerId);

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        orderShipManageShipAllSetDto,
        "/partner/sell/setShipAll",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

  /**
   * 배송완료
   */
  public ResponseObject<Integer> setShipComplete(
      ShipCompleteSet shipCompleteSet, String partnerId) throws Exception {
    ShipStatusShipCompleteSetDto shipStatusShipCompleteSetDto = new ShipStatusShipCompleteSetDto(
        shipCompleteSet,partnerId);

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        shipStatusShipCompleteSetDto,
        "/partner/sell/setShipComplete",
        new ParameterizedTypeReference<ResponseObject<Integer>>(){});
  }


  public List<DeliveryCodeGet> getDeliveryCode() {
    List<ShipCompanyManageListGetDto> shipCompanyManageListGetDtoList = resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        new ShipCompanyManageListGetDto(),
        "/admin/shipManage/getShipCompanyManageList",
        new ParameterizedTypeReference<ResponseObject<List<ShipCompanyManageListGetDto>>>(){}).getData();

    List<DeliveryCodeGet> codeGetList = new ArrayList<>();
    for (ShipCompanyManageListGetDto shipCompanyManageListGetDto : shipCompanyManageListGetDtoList) {
      codeGetList.add(new DeliveryCodeGet(shipCompanyManageListGetDto));
    }
    return codeGetList;
  }
}