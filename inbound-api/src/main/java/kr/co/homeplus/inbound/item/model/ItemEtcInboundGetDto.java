package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품 부가정보 조회")
public class ItemEtcInboundGetDto {

	@ApiModelProperty(value = "성인상품유형 (NORMAL: 일반, ADULT: 성인, LOCAL_LIQUOR: 전통주)", required = true, position = 1)
	private String adultType;

	@ApiModelProperty(value = "이미지노출여부 (Y: 노출, N: 노출안함)", required = true, position = 2)
	private String imageDisplayYn;

	@ApiModelProperty(value = "해외배송 여부 (Y: 사용, N: 사용안함)", required = true, position = 3)
	private String globalAgencyYn;

	@ApiModelProperty(value = "선물하기사용여부 (Y: 사용, N: 사용안함)", position = 4)
	private String giftYn;

	@ApiModelProperty(value = "가격비교 사이트 등록여부 기준:등록안함 (Y: 등록, N: 등록안함)", position = 5)
	private String epYn;

	@ApiModelProperty(value = "검색 키워드 (최대: 100자, 20개까지 등록가능) 예) 식품,라면,뽀글이,매운맛", position = 6)
	private String searchKeyword;

	@ApiModelProperty(value = "ISBN", position = 7)
	private String isbn;

	@ApiModelProperty(value = "안전인증 리스트", position=8)
	private List<ItemCertInboundGetDto> certList;
}