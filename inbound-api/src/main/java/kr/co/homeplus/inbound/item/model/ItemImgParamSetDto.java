package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("이미지 정보 param")
public class ItemImgParamSetDto {

	@ApiModelProperty(value = "대표이미지 여부", position = 2)
	private String	mainYn;

	@ApiModelProperty(value = "이미지 URL", required = true, position = 3)
	private String 	imgUrl;

	@ApiModelProperty(value = "이미지 유형 (MN: 메인, LST: 리스트)", required = true, position = 6)
	private String 	imgType;

}
