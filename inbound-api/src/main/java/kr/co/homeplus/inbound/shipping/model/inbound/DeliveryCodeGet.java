package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.inbound.shipping.model.partner.ShipCompanyManageListGetDto;
import lombok.Data;

@Data
@ApiModel("택배사 코드 응답")
public class DeliveryCodeGet {
    @ApiModelProperty(notes = "택배사코드", example = "002", position = 1)
    private String deliveryCode;

    @ApiModelProperty(notes = "택배사명", example = "CJ대한통운", position = 2)
    private String deliveryName;

    public DeliveryCodeGet(ShipCompanyManageListGetDto shipCompanyManageListGetDto) {
        this.deliveryCode = shipCompanyManageListGetDto.getDlvCd();
        this.deliveryName = shipCompanyManageListGetDto.getDlvNm();
    }
}
