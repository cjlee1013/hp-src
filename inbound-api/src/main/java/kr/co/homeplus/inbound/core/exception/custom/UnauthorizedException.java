package kr.co.homeplus.inbound.core.exception.custom;

import kr.co.homeplus.inbound.constants.ExceptionCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 인증 Exception
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class UnauthorizedException extends Exception {

	private String errorCode;
	private String description  = "";
	private String errorMsg     = "";

	public UnauthorizedException(ExceptionCode exceptionCode) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDescription();
	}

	public UnauthorizedException(ExceptionCode exceptionCode, String errorMsg) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDescription();
		this.errorMsg       = errorMsg;
	}

	public <T extends Number> UnauthorizedException(ExceptionCode exceptionCode, T errorMsg) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDescription();
		this.errorMsg       = String.valueOf(errorMsg);
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}
}
