package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("상품 등록 수정")
public class ItemSetDto {

	@Valid
	@ApiModelProperty(value = "기본정보", position = 1)
	private ItemBasicSetDto basic;

	@Valid
	@ApiModelProperty(value = "판매정보", position = 2)
	private ItemSaleSetDto sale;

	@Valid
	@ApiModelProperty(value = "이미지정보", position = 3)
	private List<ItemImgSetDto> image;

	@Valid
	@ApiModelProperty(value = "옵션정보", position = 4)
	private ItemOptSetDto option;

	@Valid
	@ApiModelProperty(value = "배송정보", position = 5)
	private ItemShipSetDto ship;

	@Valid
	@ApiModelProperty(value = "속성정보", position = 6)
	private ItemPropSetDto property;

	@Valid
	@ApiModelProperty(value = "부가정보", position = 7)
	private ItemEtcSetDto etc;

}