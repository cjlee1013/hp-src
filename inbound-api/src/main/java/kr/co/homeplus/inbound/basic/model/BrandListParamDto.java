package kr.co.homeplus.inbound.basic.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
@ApiModel("브랜드 조회")
public class BrandListParamDto {

    @ApiModelProperty(value = "브랜드 번호")
    private Long brandNo;

    @ApiModelProperty(value = "브랜드 이름")
    private String brandName;

    @ApiModelProperty(value = "브랜드 이름", hidden = true)
    private String brandNm;

    public void setBrandName(String brandName) {
        this.brandName  = brandName;
        this.brandNm    = brandName;
    }

    @ApiModelProperty(value = "제한갯수", hidden = true)
    private int limitSize;
}
