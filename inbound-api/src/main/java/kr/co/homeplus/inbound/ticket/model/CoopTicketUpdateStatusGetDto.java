package kr.co.homeplus.inbound.ticket.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "쿠프티켓 상태 변경 응답 파라미터")
public class CoopTicketUpdateStatusGetDto {
    @ApiModelProperty(value = "결과코드", position = 1)
    @JsonProperty("ResultCode")
    private String resultCode = "0000";

    @ApiModelProperty(value = "처리메시지", position = 2)
    @JsonProperty("ResultMessage")
    private String resultMessage;

}
