package kr.co.homeplus.inbound.ticket.service;

import kr.co.homeplus.inbound.core.constants.ResourceRouteName;
import kr.co.homeplus.inbound.ticket.model.CoopTicketUpdateStatusGetDto;
import kr.co.homeplus.inbound.ticket.model.CoopTicketUpdateStatusSetDto;
import kr.co.homeplus.inbound.ticket.model.CoopUpdateRequestDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class TicketService {

    final private ResourceClient resourceClient;

    public CoopTicketUpdateStatusGetDto setTicketStatus(CoopUpdateRequestDto coopUpdateRequestDto){
        log.info("[setTicketStatus] 쿠프 티켓상태 변경 요청 : {}", coopUpdateRequestDto);
        CoopTicketUpdateStatusGetDto response = new CoopTicketUpdateStatusGetDto();
        CoopTicketUpdateStatusSetDto requestDto = CoopTicketUpdateStatusSetDto.builder()
                                                  .ticketCd(coopUpdateRequestDto.getCouponNumber())
                                                  .chgId("coop")
                                                  .ticketStatus(this.getTicketStatus(coopUpdateRequestDto.getExchangeType()))
                                                  .build();

        ResponseObject<String> result = resourceClient.postForResponseObject(ResourceRouteName.CLAIM, requestDto,"/ticket/coop/setTicketStatus", new ParameterizedTypeReference<ResponseObject<String>>(){});
        log.info("[setTicketStatus] 요청결과 : " + result.getReturnCode() + " | " + result.getReturnMessage());
        if(!result.getReturnCode().equalsIgnoreCase("0000")){
            response.setResultCode("9999");
        }
        response.setResultMessage(result.getReturnMessage());
        return response;
    }

    private String getTicketStatus(String couponType){

        String ticketStatus = "";

        switch (couponType){
            case "Y" :
                ticketStatus = "T2";
                break;
            case "C" :
                ticketStatus = "T3";
                break;
        }
        return ticketStatus;
    }


}
