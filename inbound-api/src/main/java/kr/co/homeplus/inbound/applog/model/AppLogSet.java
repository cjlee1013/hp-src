package kr.co.homeplus.inbound.applog.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("applog")
public class AppLogSet {
    @ApiModelProperty(value = "Hplus", position = 1)
    private String hplus;

    @ApiModelProperty(value = "Splus", position = 2)
    private String splus;

    @ApiModelProperty(value = "user-agent", position = 3)
    private String userAgent;

    @ApiModelProperty(value = "client ip", position = 4)
    private String clientIp;
}
