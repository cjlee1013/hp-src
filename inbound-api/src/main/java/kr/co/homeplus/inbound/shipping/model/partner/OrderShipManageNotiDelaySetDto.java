package kr.co.homeplus.inbound.shipping.model.partner;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.inbound.shipping.enums.DelayShipCode;
import kr.co.homeplus.inbound.shipping.model.inbound.ShipDelaySet;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 발주/발송관리 발송지연안내 요청 DTO")
public class OrderShipManageNotiDelaySetDto {
    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;

    @ApiModelProperty(notes = "수정자")
    private String chgId;

    @ApiModelProperty(notes = "발송지연사유코드")
    private String delayShipCd;

    @ApiModelProperty(notes = "발송지연사유메시지")
    private String delayShipMsg;

    @ApiModelProperty(notes = "2차발송기한")
    private String delayShipDt;

    public OrderShipManageNotiDelaySetDto(
        ShipDelaySet shipDelaySet, String chgId) {
        this.bundleNo = shipDelaySet.getBundleNo();
        this.chgId = chgId;
        String delayShipCode = shipDelaySet.getDelayShipCode();
        if (delayShipCode != null) {
            this.delayShipCd = DelayShipCode.valueOf(delayShipCode).getType();
        }
        this.delayShipMsg = shipDelaySet.getDelayShipMessage();
        this.delayShipDt = shipDelaySet.getDelayShipDate();
    }
}
