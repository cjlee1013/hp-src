package kr.co.homeplus.inbound.etc.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@ApiModel("공지사항 첨부파일")
public class NoticeFileListGetDto {

    @ApiModelProperty(value = "파일명")
    private String fileName;

    @ApiModelProperty(value = "파일URL")
    private String fileUrl;

}
