package kr.co.homeplus.inbound.claim.model.inbound;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("클레임 목록 요청")
public class ClaimListSet {
    @ApiModelProperty(value = "처리상태 (전체:ALL/신청:REQUEST/승인:CONFIRM/완료:COMPLETE/실패:FAIL/철회:WITHDRAW/보류:PENDING/거부:REJECT)", position = 1, example = "REQUEST")
    private String claimStatus;

    @ApiModelProperty(value = "클레임 검색기간 (신청일:REQUEST/완료일:COMPLETE/주문일:ORDER/보류일:PENDING)", position = 2, example = "COMPLETE")
    private String claimDateType;

    @ApiModelProperty(value = "조회시작일", position = 3, example = "2020-12-29")
    private String startDate;

    @ApiModelProperty(value = "조회종료일", position = 4, example = "2020-12-29")
    private String endDate;

    @ApiModelProperty(value = "수거 상태 (전체:ALL/수거대기:READY/수거요청완료:REQUEST/수거예정:SCHEDULE/수거중:SHIPPING/수거완료:COMPLETE/수거실패:FAIL)", position = 10, example = "REQUEST")
    private String pickupStatus;

    @ApiModelProperty(value = "교환 상태 (전체:ALL/상품출고대기:CONFIRM/상품출고:SCHEDULE/배송중:SHIPPING/배송완료:COMPLETE)", position = 11, example = "CONFIRM")
    private String exchangeStatus;

    @ApiModelProperty(value = "클레임타입(취소:CANCEL,반품:PICK,교환:EXCHANGE", position = 12, example = "CANCEL")
    private String claimType;
}
