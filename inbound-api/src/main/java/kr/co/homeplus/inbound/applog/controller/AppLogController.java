package kr.co.homeplus.inbound.applog.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kr.co.homeplus.inbound.applog.model.AppLogSet;
import kr.co.homeplus.inbound.applog.model.EscrowOpenLogSet;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "app log 저장")
@ApiResponses(value = {
        @ApiResponse(code = 422, message = "Unprocessable Entity")
})
@RequestMapping("/applog")
@RestController
@RequiredArgsConstructor
@Slf4j
public class AppLogController {

    @ApiOperation(value = "자동 로그인 실패로그(app log) 저장", response = AppLogSet.class)
    @PostMapping(value = "/applog/setAppLog")
    public String setAppLog(
            @ApiParam(name = "AppLogSet", value = "app log info", required = true) @RequestBody AppLogSet appLogSet
    ) throws Exception {
        log.info ("app log :: {}", appLogSet);
        return "success";
    }

    @ApiOperation(value = "escrow webview open 로그 저장", response = String.class)
    @PostMapping(value = "/applog/setEscrowOpenLog")
    public String setEscrowWebviewOpenLog(
        @ApiParam(name = "EscrowLogSet", value = "escrow webview open log info", required = true) @RequestBody EscrowOpenLogSet escrowOpenLogSet) {
        log.info ("escrow webview open :: {}", escrowOpenLogSet);
        return "success";
    }
}
