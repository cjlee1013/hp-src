package kr.co.homeplus.inbound.claim.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import kr.co.homeplus.inbound.shipping.model.inbound.ShipStartSet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("클레임 거절 요청")
public class ClaimRejectSet {

    @NotNull(message = "클레임번호")
    @ApiModelProperty(value = "클레임번호", position = 1, example = "1500000001")
    private long claimNo;

    @NotNull(message = "클레임번들번호")
    @ApiModelProperty(value = "클레임번들번호", position = 2, example = "1500000011")
    private long claimBundleNo;

    @ApiModelProperty(value = "클레임상세사유", position = 3, example = "보류상세 사유를 입력해주세요")
    private String reasonTypeDetail;

    @NotNull(message = "클레임타입")
    @ApiModelProperty(value = "클레임타입(취소:CANCEL,반품:PICK,교환:EXCHANGE", position = 4, example = "CANCEL")
    private String claimType;

    @ApiModelProperty(value = "클레임사유(취소거부(상품기발송):CB,반품거부(구매자책임):PB,반품거부(판매자책임):PS,교환거부(구매자책임):XB,교환거부(판매자책임):XS", position = 5, example = "CB")
    private String reasonType;

    @ApiModelProperty(value = "배송정보", position = 6)
    private ShipStartSet shipInfo;
}
