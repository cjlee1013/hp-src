package kr.co.homeplus.inbound.etc.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kr.co.homeplus.inbound.etc.model.NoticeListSelectDto;
import kr.co.homeplus.inbound.etc.service.NoticeService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "공지사항조회")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found")
    ,   @ApiResponse(code = 405, message = "Method Not Allowed")
    ,   @ApiResponse(code = 422, message = "Unprocessable Entity")
    ,   @ApiResponse(code = 500, message = "Internal Server Error")
})
@RequestMapping("/notice")
@RestController
@RequiredArgsConstructor
public class NoticeController {

    final private NoticeService noticeService;

    @ApiOperation(value = "공지사항 리스트", response = NoticeListSelectDto.class)
    @RequestMapping(value = "/getNoticeList", method = {RequestMethod.GET})
    public ResponseObject getNoticeList(
            @ApiParam(value = "apiKey", required = true) @RequestHeader(value = "apiKey") String apiKey,
            @RequestParam(name ="searchDate", required = false) @ApiParam(name = "searchDate", value = "조회기준일") String searchDate
    ) throws Exception {
        return ResourceConverter.toResponseObject(noticeService.getNoticeList(apiKey, searchDate));
    }

}