package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.inbound.shipping.model.partner.OrderDetailBasePartnerDto;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@ApiModel("주문기본정보")
public class OrderDetailBasicGet {
    @ApiModelProperty(notes = "주문번호", example = "3000000001", position = 1)
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "주문일", example = "2020-10-14 13:44:07", position = 2)
    private String orderDate;

    @ApiModelProperty(notes = "구매자", example = "김홈플", position = 3)
    private String buyerName;

    @ApiModelProperty(notes = "구매자 연락처", example = "010-1234-5678", position = 4)
    private String buyerMobileNo;

    public OrderDetailBasicGet(OrderDetailBasePartnerDto orderDetailBasePartnerDto) {
        this.purchaseOrderNo = orderDetailBasePartnerDto.getPurchaseOrderNo();
        this.orderDate = orderDetailBasePartnerDto.getOrderDt();
        this.buyerName = orderDetailBasePartnerDto.getBuyerNm();
        this.buyerMobileNo = orderDetailBasePartnerDto.getBuyerMobileNo();
    }
}
