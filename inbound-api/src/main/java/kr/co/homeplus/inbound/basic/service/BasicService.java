package kr.co.homeplus.inbound.basic.service;

import java.util.List;
import kr.co.homeplus.inbound.basic.model.BrandInfoDto;
import kr.co.homeplus.inbound.basic.model.CategoryDspDto;
import kr.co.homeplus.inbound.basic.model.ItemNoticeDto;
import kr.co.homeplus.inbound.basic.model.MakerInfoDto;
import kr.co.homeplus.inbound.basic.model.SearchTagDto;
import kr.co.homeplus.inbound.basic.model.SellerShipDto;
import kr.co.homeplus.inbound.common.service.CommonValidService;
import kr.co.homeplus.inbound.constants.ExceptionCode;
import kr.co.homeplus.inbound.core.constants.ResourceRouteName;
import kr.co.homeplus.inbound.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
@RequiredArgsConstructor
public class BasicService {

    private final ResourceClient resourceClient;
    private final CommonValidService commonValidService;

    /**
     * 브랜드 리스트 조회
     * @return
     * @throws BusinessLogicException
     */
    @Cacheable(value = "getBrandListCache")
    public List<BrandInfoDto> getBrandList(final String apiKey) throws Exception {

        //API KEY validation check
        final String partnerId = commonValidService.validApiKeyHandler(apiKey);

        try {
            String apiUri = "/inbound/out/getBrand";

            List<BrandInfoDto> brandList = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<BrandInfoDto>>>() {}).getData();

            if (ObjectUtils.isEmpty(brandList)) {
                throw new BusinessLogicException(ExceptionCode.ERROR_CODE_5000);
            } else {
                return brandList;
            }
        } catch (Exception e) {
            throw new BusinessLogicException(ExceptionCode.SYS_ERROR_CODE_9001);
        }
    }


    /**
     * 카테고리 정보 조회
     * @return
     * @throws Exception
     */
    public List<CategoryDspDto> getCategoryList(final String apiKey) throws Exception {

        //API KEY validation check
        final String partnerId = commonValidService.validApiKeyHandler(apiKey);

        try {
            String apiUri = "/inbound/out/getCategory";

            List<CategoryDspDto> cateList = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<CategoryDspDto>>>() {}).getData();

            if (ObjectUtils.isEmpty(cateList)) {
                throw new BusinessLogicException(ExceptionCode.ERROR_CODE_5001);
            } else {
                return cateList;
            }
        } catch (Exception e) {
            throw new BusinessLogicException(ExceptionCode.SYS_ERROR_CODE_9001);
        }
    }

    /**
     * 검색태그 조회
     * @return
     * @throws Exception
     */
    public List<SearchTagDto> getSearchTagList(final String apiKey) throws Exception {

        //API KEY validation check
        final String partnerId = commonValidService.validApiKeyHandler(apiKey);

        try {
            String apiUri = "/inbound/out/getSearchTag";

            List<SearchTagDto> cateList = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<SearchTagDto>>>() {}).getData();

            if (ObjectUtils.isEmpty(cateList)) {
                throw new BusinessLogicException(ExceptionCode.ERROR_CODE_5002);
            } else {
                return cateList;
            }
        } catch (Exception e) {
            throw new BusinessLogicException(ExceptionCode.SYS_ERROR_CODE_9001);
        }
    }

    /**
     * 제조사 조회
     * @return
     * @throws Exception
     */
    @Cacheable(value = "getMakerListCache")
    public List<MakerInfoDto> getMakerList(final String apiKey) throws Exception {

        //API KEY validation check
        final String partnerId = commonValidService.validApiKeyHandler(apiKey);

        try {
            String apiUri = "/inbound/out/getMaker";

            List<MakerInfoDto> makerList = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<MakerInfoDto>>>() {}).getData();

            if (ObjectUtils.isEmpty(makerList)) {
                throw new BusinessLogicException(ExceptionCode.ERROR_CODE_5003);
            } else {
                return makerList;
            }
        } catch (Exception e) {
            throw new BusinessLogicException(ExceptionCode.SYS_ERROR_CODE_9001);
        }
    }

    /**
     * 고시정보 조회
     * @return
     * @throws Exception
     */
    public List<ItemNoticeDto> getItemNoticeList(final String apiKey) throws Exception {

        //API KEY validation check
        final String partnerId = commonValidService.validApiKeyHandler(apiKey);

        try {
            String apiUri = "/inbound/out/getItemNotice";

            List<ItemNoticeDto> noticeList = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<ItemNoticeDto>>>() {}).getData();

            if (ObjectUtils.isEmpty(noticeList)) {
                throw new BusinessLogicException(ExceptionCode.ERROR_CODE_5004);
            } else {
                return noticeList;
            }
        } catch (Exception e) {
            throw new BusinessLogicException(ExceptionCode.SYS_ERROR_CODE_9001);
        }
    }

    /**
     * 판매자 배송정책 조회
     * @return
     * @throws Exception
     */
    public List<SellerShipDto> getSellerShipList(final String apiKey) throws Exception {

        //API KEY validation check
        final String partnerId = commonValidService.validApiKeyHandler(apiKey);

        try {
            String apiUri = "/inbound/out/getSellerShipInfo?partnerId=" + partnerId;

            List<SellerShipDto> noticeList = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<SellerShipDto>>>() {}).getData();

            if (ObjectUtils.isEmpty(noticeList)) {
                throw new BusinessLogicException(ExceptionCode.ERROR_CODE_5005);
            } else {
                return noticeList;
            }
        } catch (Exception e) {
            throw new BusinessLogicException(ExceptionCode.SYS_ERROR_CODE_9001);
        }
    }
}
