package kr.co.homeplus.inbound.etc.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("QNA 리스트")
public class CsQnaListSelectDto {

    @ApiModelProperty(value = "QNA 일련번호")
    private long qnaNo;

    @ApiModelProperty(value = "문의내용", position = 1)
    private String qnaContents;

    @ApiModelProperty(value = "등록일시", position = 2)
    @JsonProperty(value = "qnaDate")
    private String registrationDate;

    @ApiModelProperty(value = "작성자ID", position = 3)
    private String userId;

    @ApiModelProperty(value = "작성자", position = 4)
    private String userNm;

    @ApiModelProperty(value = "문의유형(상품, 배송, 교환/반품, 기타)", position = 5)
    private String qnaType;

    @ApiModelProperty(value = "상품번호", position = 6)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 7)
    private String itemName;

    @ApiModelProperty(value = "비밀글 여부(Y:비밀글,N:공개글)", position = 8)
    private String secretYn;

}
