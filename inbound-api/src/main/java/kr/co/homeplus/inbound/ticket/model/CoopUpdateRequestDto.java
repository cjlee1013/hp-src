package kr.co.homeplus.inbound.ticket.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ApiModel(description = "쿠프티켓 상태 변경 요청 파라미터")
public class CoopUpdateRequestDto {
    @ApiModelProperty(value = "구분코드", position = 1)
    @Pattern(regexp = "KKO|CP", message = "구분코드")
    @JsonProperty("CompanyCode")
    private String companyCode;

    @ApiModelProperty(value = "교환/취소일자", position = 2)
    @JsonProperty("ExchangeDate")
    private String exchangeDate;

    @ApiModelProperty(value = "쿠폰번호", position = 3)
    @JsonProperty("CouponNumber")
    private String couponNumber;

    @ApiModelProperty(value = "쿠폰교환 타입 (Y:사용, C:사용취소, R:환불)", position = 4)
    @JsonProperty("ExchangeType")
    private String exchangeType;

    @ApiModelProperty(value = "교환금액", position = 5)
    @JsonProperty("ExchangePrice")
    private String exchangePrice;

    @ApiModelProperty(value = "티켓잔액", position = 6)
    @JsonProperty("BalancePrice")
    private String balancePrice;

    @ApiModelProperty(value = "쿠폰타입", position = 7)
    @JsonProperty("CouponType")
    private String couponType;

    @ApiModelProperty(value = "교환거래번호", position = 8)
    @JsonProperty("ExchangeTransactionNumber")
    private String exchangeTransactionNumber;

    @ApiModelProperty(value = "가맹점코드", position = 9)
    @JsonProperty("BranchCode")
    private String branchCode;

    @ApiModelProperty(value = "가맹점명", position = 10)
    @JsonProperty("BranchName")
    private String branchName;

    @ApiModelProperty(value = "브랜드코드", position = 11)
    @JsonProperty("BrandCode")
    private String brandCode;

    @ApiModelProperty(value = "연동코드", position = 12)
    @JsonProperty("CouponCode")
    private String couponCode;

    @ApiModelProperty(value = "발행사 브랜드 코드", position = 13)
    @JsonProperty("CompanyBrandCode")
    private String companyBrandCode;
}
