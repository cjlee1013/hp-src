package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품정보고시 정보")
public class ItemNoticeParamSetDto {

    @ApiModelProperty(value = "고시항목번호")
    private int noticeNo;

    @ApiModelProperty(value = "정보고시항목명")
    private String noticeDesc;

}
