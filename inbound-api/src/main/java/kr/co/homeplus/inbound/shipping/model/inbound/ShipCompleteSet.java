package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel("배송완료 요청")
public class ShipCompleteSet {
    @ApiModelProperty(notes = "배송번호 리스트")
    private List<String> bundleNoList;
}
