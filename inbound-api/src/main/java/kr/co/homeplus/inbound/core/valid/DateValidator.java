package kr.co.homeplus.inbound.core.valid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import kr.co.homeplus.inbound.utils.DateUtil;

public class DateValidator implements ConstraintValidator<Date, String>{

    private String format;

    @Override
    public void initialize(Date constraintAnnotation) {
        this.format = constraintAnnotation.format();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return DateUtil.isValidParseDateStrictly(s, format);
    }
}
