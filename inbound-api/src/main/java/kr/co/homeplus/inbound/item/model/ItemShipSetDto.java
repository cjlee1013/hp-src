package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품 배송정보")
public class ItemShipSetDto {

	@ApiModelProperty(value = "배송정책번호 (최대: 9자리)", required = true, position = 1)
	private Long shipPolicyNo;
}