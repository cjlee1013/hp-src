package kr.co.homeplus.inbound.claim.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("클레임 수거 요청")
public class PickShippingSet {

    @NotNull(message = "그룹클레임번호")
    @ApiModelProperty(value = "그룹클레임번호", position = 1, example = "1500000011")
    private long claimNo;

    @ApiModelProperty(value = "클레임 번들 번호", position = 2, example = "1500000001")
    public long claimBundleNo;

    @ApiModelProperty(value = "수거지-이름", position = 3, example = "홍길동")
    public String pickReceiverName;

    @ApiModelProperty(value = "수거지-휴대폰", position = 4, example = "010-1111-1111")
    public String pickMobileNo;

    @ApiModelProperty(value = "수거지 우편번호", position = 5, example = "07297")
    public String pickZipcode;

    @ApiModelProperty(value = "수거지 기본주소", position = 6, example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    public String pickRoadBaseAddress;

    @ApiModelProperty(value = "수거지 샹세주소", position = 7, example = "금천전 홈플러스 1층")
    public String pickRoadAddrDetail;

    @ApiModelProperty(value = "수거지 기본주소", position = 8, example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    public String pickBaseAddress;

    @ApiModelProperty(value = "수거지 샹세주소", position = 9, example = "금천전 홈플러스 1층")
    public String pickDetailAddress;

    @ApiModelProperty(value = "수거배송수단코드 (DELIVERY:택배/DIRECT:직접배송/POST:우편/NOSHIP:배송안함)", position = 10, example = "DELIVERY")
    public String pickShipType;

    @ApiModelProperty(value = "수거택배사", position = 11, example = "002")
    public String pickDeliveryCode;

    @ApiModelProperty(value = "수거송장번호", position = 12, example = "123123123")
    public String pickInvoiceNo;

    @ApiModelProperty(value = "수거메모", position = 13, example = "우편수거 메모입니다.")
    public String pickMemo;

    @ApiModelProperty(value="수거일자", position = 14, example = "2020-12-29 00:00:00")
    public String pickDate;

}
