package kr.co.homeplus.inbound.constants;

/**
 * 코드 관련 공통 상수
 */
public class CodeConstants {
    // 파트너 사용자 아이디
    public static final String PARTNER_USER_ID = "PARTNER";

	// 시스템 사용자 아이디 (BATCH 용)
	public static final String SYSTEM_USER_ID = "PLUS";

    // 상품 > 상품 수수료 map key
    public static final String PROD_SALE_COMMISSION = "commission";

    // 공통 > 시스템 구분자
    public static final String SYS_DIVISION = "∑";

    // 상품 > 금칙어 유효성체크 시 구분자
    public static final String BANNED_WORD_STRING_DIVISION = SYS_DIVISION;

    // 상품 > 옵션구분 시 구분자
    public static final String PROD_OPTION_STRING_DIVISION = SYS_DIVISION;

    // 상품 > 히든프라이스 > 대 카테고리코드
    public static final String HIDDEN_PRICE_LCATE_CD = "1100033";

}
