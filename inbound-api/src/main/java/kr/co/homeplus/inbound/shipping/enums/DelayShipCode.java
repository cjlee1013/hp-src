package kr.co.homeplus.inbound.shipping.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum DelayShipCode {

        SHORTSTOCK("SS", "SHORTSTOCK")
    ,   PRESSUREOFORDER("PO", "PRESSUREOFORDER")
    ,   CUSTOMERREQUEST("CR", "CUSTOMERREQUEST")
    ,   ETC("ET", "ETC")
    ,   SS("SS", "SHORTSTOCK")
    ,   PO("PO", "PRESSUREOFORDER")
    ,   CR("CR", "CUSTOMERREQUEST")
    ,   ET("ET", "ETC")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;
}
