package kr.co.homeplus.inbound.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimRequestType {

        CA("CA", "CONFIRM")
    ,   CW("CW", "WITHDRAW")
    ,   CH("CH", "PENDING")
    ,   CD("CD", "REJECT")
    ,   CC("CD", "COMPLETE")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;


}
