package kr.co.homeplus.inbound.core.exception.handler;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.co.homeplus.inbound.constants.ExceptionCode;
import kr.co.homeplus.inbound.constants.PatternConstants;
import kr.co.homeplus.inbound.core.exception.custom.UnauthorizedException;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.exception.ResourceClientException;
import kr.co.homeplus.plus.api.support.client.exception.ResourceTimeoutException;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.tomcat.util.http.fileupload.FileUploadBase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * 어드민 내 글로벌로 예외 헨들러를 관리하는 클래스
 */
@Slf4j
@ControllerAdvice
@RequiredArgsConstructor
public class ExceptionControllerAdvice {

    @Value("${spring.profiles.active}")
    private String profilesActive;

    /**
     * 어드민 내 {@link Throwable}로 발생되는 예외를 처리하는 메소드로 명시적이지 않는 모든 예외를 처리한다.<p>
     * <p>
     * 응답 타입은 요청한 url의 suffix가 '.json'이면 json 타입으로 리턴, 아닐 경우 error Page로 이동한다.<br> 응답 내용은 에러내용을 담은
     * {@link ResponseObject}와 {@link HttpStatus}를 전달한다.<p>
     * <p>
     *
     * @param req HttpServletRequest
     * @param ex  Exception
     * @return Url의 suffix가 .json이면 'httpStatus'와 json 타입으로 return, 아닐 경우 error Page로 이동
     * @see ResponseObject
     */
    @ExceptionHandler(Throwable.class)
    public Object handleServerError(final HttpServletRequest req, final Exception ex) {

        //응답할 ResponseObject 설정
        ResponseObject<String> responseObject = setResponseObjectForException(ex,
                HttpStatus.INTERNAL_SERVER_ERROR);
        log.error("Throwable Error!!! inboundUri: {}, apiKey: {}, itemNo: {}, trace: {}", req.getRequestURI(), req.getHeader("apiKey"), req.getParameter("itemNo"),
                ExceptionUtils.getStackTrace(ex));
        responseObject.setData("스크립트를 확인해주세요.");
        responseObject.setReturnMessage("");

        HttpHeaders httpHeaders = createHttpHeaders();
        return new ResponseEntity<>(responseObject, httpHeaders,
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Resttemplate 사용 시 resource api 호출 이거나, gateway 를 통한 호출 시 발생하는 예외를 처리하는 핸들러<p> <p/> 전달되는
     * {@link Exception}의 type에 따라 리턴되는 값이 변경된다. 세부내용은 하단의 내용을 참조한다. <p/>
     * <pre>
     * 1. {@link RestClientResponseException} 이 발생 할 경우
     *  - 리턴 : status, ResponseBody 그대로 전달
     *
     * 2. 기타 그외의 exception
     *  - 리턴 : status만 변경(500), ResponseObject를 전달
     *   * unknownHostException, {@link java.io.IOException} 등의 기타 {@link Exception} 처리
     *   * 에러 내용을 {@link ResponseObject}로 담아 전달
     * </pre>
     *
     * @param req HttpServletRequest
     * @param e  Exception
     * @return responseEntity
     * @see RestClientResponseException
     * @see ResourceClientException
     */
    @ExceptionHandler(ResourceClientException.class)
    public Object handleResourceClientException(final HttpServletRequest req,
            final HttpServletResponse res, final ResourceClientException e) {
        final Throwable cause = e.getCause();

        String setInfo = String.valueOf(req.getAttribute("setInfo"));

        /*
         * 1. RestClientResponseException 예외가 발생 할 경우
         *  1.1 url이 .json 일 경우 : status, ResponseBody 그대로 전달
         *  1.2 url이 .json이 아닐 경우 : 에러 페이지로 이동
         */
        if (cause instanceof RestClientResponseException) {
            res.setStatus(e.getHttpStatus());
            final String responseBodyAsString = ((RestClientResponseException) cause)
                    .getResponseBodyAsString();

            log.error(
                    "ResourceClientException Error! <uri:{}>,<resourceUrl:{}>,<httpStatus:{}>,<responseBody:{}>,<setInfo:{}>",
                    req.getRequestURI(), e.getResourceUrl(), e.getHttpStatus(), responseBodyAsString, setInfo);

            return returnResponse(req.getRequestURI(), e, e.getResponseBody(), HttpStatus.valueOf(e.getHttpStatus()));
        }
        /*
         * 2. 기타 그외의 exception
         *  2.1 url이 .json 일 경우 : status 지정(500), 에러 내용은 {@link ResponseObject}로 담아 전달
         *  2.2 url이 .json이 아닐 경우 : 에러 페이지로 이동
         */
        else {
            res.setStatus(500);
            ResponseObject<String> responseObject = setResponseForNotRestClientResponseException(e);

            log.error(
                    "ResourceClientException Error! <uri:{}>,<resourceUrl:{}>,<httpStatus:{}>,<responseBody:{}>,<setInfo:{}>,<trace:{}>",
                    req.getRequestURI(), e.getResourceUrl(), e.getHttpStatus(), responseObject, setInfo,
                    ExceptionUtils.getStackTrace(e));
            return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private <T> Object returnResponse(final String requestUri, final Exception e,
            final T responseBody, final HttpStatus status) {
        return returnResponse(requestUri, e, responseBody, status, "/core/error/error");
    }

    /**
     * 예외 처리후 requestURI 유형에 따라 {@link HttpEntity} 또는 {@link ModelAndView} 로 반환합니다.
     * @param uri request URI
     * @param e {@link Exception}
     * @param responseBody ResponseBody
     * @param status {@link HttpStatus}
     * @param viewName viewName
     * @return URI의 suffix 가 .json 일 경우 {@link HttpEntity}로 아닐경우 {@link ModelAndView} 반환합니다.
     */
    private <T> Object returnResponse(final String uri, final Exception e, final T responseBody,
            final HttpStatus status, final String viewName) {
        final HttpHeaders httpHeaders = createHttpHeaders();
        return new ResponseEntity<>(responseBody, httpHeaders, status);
    }

    /**
     * RestClientResponseException 이 아닌 Exception의 유형에 따라 에러코드와 문구가 담긴 ResponseObject 를 반환합니다.
     * @param e {@link ResourceClientException}
     * @return 에러코드, 문구가 담긴 ResponseObject 를 반환합니다.
     */
    private ResponseObject<String> setResponseForNotRestClientResponseException(
            final ResourceClientException e) {

        String returnCode;
        String returnMessage;

        if ( e.getCause() instanceof ResourceAccessException) {
            returnCode = ExceptionCode.SYS_ERROR_CODE_9103.getCode();
            returnMessage = ExceptionCode.SYS_ERROR_CODE_9103.getDescription();
        } else {
            returnCode = ExceptionCode.SYS_ERROR_CODE_9102.getCode();
            returnMessage = ExceptionCode.SYS_ERROR_CODE_9102.getDescription();
        }
        return ResourceConverter
                .toResponseObject(null, HttpStatus.INTERNAL_SERVER_ERROR, returnCode, returnMessage);
    }

    /**
     * {@link ResourceTimeoutException} 발생 시 예외를 처리하는 핸들러 입니다.<p>
     * <p>
     * {@link ResourceTimeoutException}은 SocketTimeoutException, ConnectTimeoutException 발생 할 경우
     * throw 하는 {@link Exception} 입니다.<br>
     * <pre>
     *  1. url이 .json 일 경우 : status 지정(408), 에러 내용은 {@link ResponseObject}로 담아 전달
     *  2. url이 .json이 아닐 경우 : 에러 페이지로 이동
     * </pre>
     *
     * @param req HttpServletRequest
     * @param ex  ResourceTimeoutException
     * @return responseEntity
     */
    @ExceptionHandler(ResourceTimeoutException.class)
    public Object handleResourceTimeoutException(final HttpServletRequest req,
            final ResourceTimeoutException ex) {
        // 응답할 ResponseObject 설정
        ResponseObject<String> responseObject = setResponseObjectForException(ex,
                HttpStatus.REQUEST_TIMEOUT);
        log.error("ResourceTimeoutException Error!!! adminUri:{}, httpStatus:{}, trace:{}",
                req.getRequestURI(), HttpStatus.REQUEST_TIMEOUT, ExceptionUtils.getStackTrace(ex));

        if (isRestUri(req.getRequestURI())) {
            HttpHeaders httpHeaders = createHttpHeaders();
            return new ResponseEntity<>(responseObject, httpHeaders, HttpStatus.REQUEST_TIMEOUT);
        } else {
            ModelAndView mav = new ModelAndView();
            setErrorModelAndView(mav, HttpStatus.REQUEST_TIMEOUT, ExceptionUtils.getStackTrace(ex),
                    responseObject);
            mav.setViewName("/core/error/error");
            return mav;
        }
    }

    /**
     * true 이면 .json url 로 호출, false 이면 page 호출여부 체크
     *
     * @param requestUrl request url
     * @return boolean
     */
    private boolean isRestUri(String requestUrl) {
        return StringUtils.endsWith(requestUrl, ".json");
    }

    /**
     * 404 error not found 예외처리<p>
     *
     * @param req {@link HttpServletRequest}
     * @param ex  NoHandlerFoundException
     * @return Url의 suffix가 .json이면 'httpStatus'와 json 타입으로 return, 아닐 경우 error Page로 이동
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Object noHandlerFoundException(final HttpServletRequest req,
            final NoHandlerFoundException ex) {
        log.error("404 Not Found Error!!! <adminUri:{}>, <RequestMethod:{}>, <HttpStatus:{}>",
                req.getRequestURI(), req.getMethod(), HttpStatus.NOT_FOUND.value());

        ResponseObject<String> responseObject = setResponseObjectForException(ex,
                HttpStatus.NOT_FOUND);
        //Url의 suffix가 .json이면 json, 아닐 경우 page로 이동
        if (isRestUri(req.getRequestURI())) {
            HttpHeaders httpHeaders = createHttpHeaders();
            return new ResponseEntity<>(responseObject, httpHeaders, HttpStatus.NOT_FOUND);
        } else {
            ModelAndView mav = new ModelAndView();
            mav.setStatus(HttpStatus.NOT_FOUND);
            mav.setViewName("/core/error/error404");
            return mav;
        }
    }

    /**
     * multipart 관련 에러를 처리하는 핸들러입니다.<p> 전달되는 {@link Exception}의 type에 따라 리턴되는 값이 변경됩니다. 분류내용은 하단의
     * 내용을 참조하시길 바랍니다. <p/>
     * <pre>
     * 1. 업로드 한 multipart의 사이즈가 application.yml에 설정 된 multipart 사이즈 크기를 초과할 경우
     *   - 리턴 : status : 413(PAYLOAD_TOO_LARGE), ResponseObject : 에러 정보
     *
     * 2. 그 외의 {@link Exception}
     *   - 리턴 : status : 500(INTERNAL_SERVER_ERROR), ResponseObject : 에러 정보
     * </pre>
     *
     * @param req {@link HttpServletRequest}
     * @param ex  {@link MultipartException}
     * @return Url의 suffix가 .json이면 'httpStatus'와 json 타입으로 return, 아닐 경우 error Page로 이동
     */
    @ExceptionHandler(MultipartException.class)
    public Object multipartException(final HttpServletRequest req, final Exception ex) {
        Throwable cause = ex.getCause();
        HttpStatus httpStatus;
        ResponseObject<String> responseObject;

        if (cause.getCause() instanceof FileUploadBase.SizeLimitExceededException) {
            httpStatus = HttpStatus.PAYLOAD_TOO_LARGE;
            log.error(
                    "MultipartException ERROR - Payload Too Large!!! <adminUri:{}>, <RequestMethod:{}>, <HttpStatus:{}>, <requestSize:{}>, <PermittedSize:{}>",
                    req.getRequestURI(), req.getMethod(), httpStatus.value(),
                    ((FileUploadBase.SizeLimitExceededException) cause.getCause()).getActualSize(),
                    ((FileUploadBase.SizeLimitExceededException) cause.getCause()).getPermittedSize());

            responseObject = setResponseObjectForException(ex, httpStatus);
        } else {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            log.error(
                    "MultipartException ERROR - Internal Server Error!!! <adminUri:{}>, <RequestMethod:{}>, <HttpStatus:{}>",
                    req.getRequestURI(), req.getMethod(), httpStatus.value());

            responseObject = setResponseObjectForException(ex, httpStatus);
        }

        //Url의 suffix가 .json이면 json, 아닐 경우 page로 이동
        if (isRestUri(req.getRequestURI())) {
            HttpHeaders httpHeaders = createHttpHeaders();
            return new ResponseEntity<>(responseObject, httpHeaders, httpStatus);
        } else {
            ModelAndView mav = new ModelAndView();
            mav.setStatus(httpStatus);
            mav.addObject("profilesActive", profilesActive);
            mav.addObject("responseObject", responseObject);
            mav.setViewName("/core/error/error");
            return mav;
        }
    }

    /**
     * {@link org.springframework.web.bind.annotation.RequestParam}과 바인딩되는 파라미터가 null일 경우 예외처리
     * @param req HttpServletRequest
     * @param e MissingServletRequestParameterException
     * @see org.springframework.web.bind.annotation.RequestParam
     * @return
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Object handleMissingServletRequestParameterException(final HttpServletRequest req,
            final MissingServletRequestParameterException e) {
        log.error("MissingServletRequestParameterException!<adminUri:{}>,<HttpStatus:{}>",
                req.getRequestURI(), ExceptionUtils.getStackTrace(e));

        //TODO system code enum 생성 및 관리
        ResponseObject<String> responseObject = setResponseObjectForException(e,
                HttpStatus.INTERNAL_SERVER_ERROR);

        // json 요청일 경우
        HttpHeaders httpHeaders = createHttpHeaders();
        return new ResponseEntity<>(responseObject, httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ResponseObject<String>> handleMethodArgumentNotValidException(MethodArgumentNotValidException e, HttpServletRequest request) {
        return this.getHandleException(e.getBindingResult(), request);
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<ResponseObject<String>> handleBindException(BindException e, HttpServletRequest request) {
        return this.getHandleException(e.getBindingResult(), request);
    }

    private ResponseEntity<ResponseObject<String>> getHandleException(BindingResult br, HttpServletRequest request) {
        return this.getHandleResponseEntity(br.getFieldError(), request);
    }

    private ResponseEntity<ResponseObject<String>> getHandleResponseEntity(final FieldError fieldError, HttpServletRequest request) {
        ResponseObject<String> responseObject;

        if (fieldError != null) {
            StringBuilder errorMessage = new StringBuilder();
            String errorCode;
            String errorField = fieldError.getField();
            String codeType = StringUtils.defaultString(fieldError.getCode());
            String errorDefMsg = fieldError.getDefaultMessage();
            Object[] errorArguments = org.apache.commons.lang3.ObjectUtils.defaultIfNull(fieldError.getArguments(), new Object[]{});

            Object errorArguments1  = errorArguments.length > 1 ? errorArguments[1] : "";
            Object errorArguments2  = errorArguments.length > 2 ? errorArguments[2] : "";

            switch (codeType) {
                case "typeMismatch":
                    errorCode = ExceptionCode.ERROR_CODE_1004.getCode();
                    errorMessage.append("(").append(errorField).append(")").append(ExceptionCode.ERROR_CODE_1004.getDescription());
                    break;
                case "NotNull":
                case "NotEmpty":
                case "NotBlank":
                    errorCode = ExceptionCode.ERROR_CODE_1002.getCode();
                    errorMessage.append("(").append(errorDefMsg).append(")").append(ExceptionCode.ERROR_CODE_1002.getDescription());
                    break;
                case "Range":
                case "Length":
                case "Size":
                    if (!ObjectUtils.isEmpty(errorArguments2) && (int) errorArguments2 > 0) {
                        errorCode = ExceptionCode.ERROR_CODE_1005.getCode();

                        errorMessage.append("(").append(errorDefMsg).append(")");
                        errorMessage.append(errorArguments2).append("~").append(errorArguments1).append(ExceptionCode.ERROR_CODE_1005.getDescription());

                    } else {
                        errorCode = ExceptionCode.ERROR_CODE_1007.getCode();

                        errorMessage.append("(").append(errorDefMsg).append(")");
                        errorMessage.append(errorArguments1).append(ExceptionCode.ERROR_CODE_1007.getDescription());
                    }
                    break;
                case "Date":
                case "Pattern":
                case "AllowInput":
                case "NotAllowInput":
                    errorCode = ExceptionCode.ERROR_CODE_1021.getCode();
                    errorMessage.append("(").append(errorDefMsg);
                    if (errorArguments1 instanceof Boolean && errorArguments2 instanceof String[]) {
                        if (!(boolean) errorArguments1) {
                            switch (codeType) {
                                case "NotAllowInput":
                                    errorMessage.append(" ※불가 문자: ");
                                    break;
                                default:
                                    errorMessage.append(" ※허용 문자: ");
                                    break;
                            }
                            errorMessage.append(PatternConstants.getConstantsMessage((String[])errorArguments2));
                        }
                    }
                    errorMessage.append(")").append(ExceptionCode.ERROR_CODE_1021.getDescription());
                    break;
                case "EnterNotEmpty":
                case "NotEnter":
                    errorCode = ExceptionCode.ERROR_CODE_1090.getCode();
                    errorMessage.append("(").append(errorDefMsg).append(")").append(ExceptionCode.ERROR_CODE_1090.getDescription().replace("(%d)", ""));
                    break;
                case "Min":
                    errorCode = ExceptionCode.ERROR_CODE_1028.getCode();
                    errorMessage.append("(").append(errorDefMsg).append(")").append(errorArguments1).append(ExceptionCode.ERROR_CODE_1028.getDescription());
                    break;
                case "Max":
                    errorCode = ExceptionCode.ERROR_CODE_1029.getCode();
                    errorMessage.append("(").append(errorDefMsg).append(")").append(errorArguments1).append(ExceptionCode.ERROR_CODE_1029.getDescription());
                    break;
                case "DecimalMin":
                    errorCode = ExceptionCode.ERROR_CODE_1022.getCode();
                    errorMessage.append("(").append(errorDefMsg).append(")").append(fieldError.getRejectedValue()).append(ExceptionCode.ERROR_CODE_1022.getDescription());
                    break;
                case "DecimalMax":
                    errorCode = ExceptionCode.ERROR_CODE_1023.getCode();
                    errorMessage.append("(").append(errorDefMsg).append(")").append(fieldError.getRejectedValue()).append(ExceptionCode.ERROR_CODE_1023.getDescription());
                    break;
                case "Digits":
                    errorCode = ExceptionCode.ERROR_CODE_1001.getCode();
                    errorMessage
                            .append("(")
                            .append(errorDefMsg)
                            .append(") 숫자:")
                            .append(errorArguments1)
                            .append("자리, 소수점:").append(errorArguments2)
                            .append("자리 까지")
                            .append(ExceptionCode.ERROR_CODE_1001.getDescription());
                    break;
                default:
                    errorCode = ExceptionCode.ERROR_CODE_1001.getCode();
                    errorMessage.append("(").append(errorDefMsg).append(")").append(ExceptionCode.ERROR_CODE_1001.getDescription());
                    break;
            }

            responseObject = ResponseObject.Builder.<String>builder()
                    .status(HttpStatus.UNPROCESSABLE_ENTITY)
                    .returnCode(errorCode)
                    .returnMessage(errorMessage.toString()).build();

        } else {

            responseObject = ResponseObject.Builder.<String>builder()
                    .status(HttpStatus.UNPROCESSABLE_ENTITY)
                    .returnCode(ExceptionCode.SYS_ERROR_CODE_9001.getCode())
                    .returnMessage(ExceptionCode.SYS_ERROR_CODE_9001.getDescription()).build();
        }

        return ResourceConverter.toResponseEntity(responseObject, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * 에러 페이지에 전달 할 {@link ModelAndView}를 설정 합니다.
     *
     * @param mav            {@link ModelAndView}
     * @param status         {@link HttpStatus}
     * @param stackTrace     {@link Exception} StackTrace
     * @param responseObject {@link ResponseObject}
     */
    private <T> void setErrorModelAndView(ModelAndView mav, HttpStatus status, String stackTrace,
            T responseObject) {
        mav.setStatus(status);
        mav.addObject("profilesActive", profilesActive);
        mav.addObject("responseObject", responseObject);
        mav.addObject("stackTrace", stackTrace);
    }

    /**
     * 에러 발생 시 리턴 할 {@link ResponseObject}를 spec에 맞춰 생성해주는 메소드
     *
     * @param ex         Exception
     * @param httpStatus ResponseObject 내에 입력할 HttpStatus
     * @return ResponseObject
     */
    private ResponseObject<String> setResponseObjectForException(final Exception ex,
            final HttpStatus httpStatus) {
        Exception processedException = new Exception(ex.getMessage());
        processedException.setStackTrace(Arrays.copyOf(ex.getStackTrace(), 3));

        return ResponseObject.Builder.<String>builder().status(httpStatus)
                .data(ExceptionUtils.getStackTrace(processedException)).returnCode("01")
                .returnMessage(ExceptionUtils.getMessage(processedException)).build();
    }

    /**
     * 에러 전달시 사용할 HttpHeaders 생성<p>
     * <p>
     * char : UTF-8<br> MediaType : application/json
     *
     * @return HttpHeaders
     */
    private HttpHeaders createHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        Charset utf8 = StandardCharsets.UTF_8;
        MediaType mediaType = new MediaType(MediaType.APPLICATION_JSON_UTF8, utf8);
        httpHeaders.setContentType(mediaType);

        return httpHeaders;
    }

    @ExceptionHandler(BusinessLogicException.class)
    public ResponseEntity<ResponseObject<String>> handleBusinessLogicException(
            BusinessLogicException ex, HttpServletRequest request) {
        ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
                .status(HttpStatus.UNPROCESSABLE_ENTITY)
                .returnCode(ex.getErrorCode())
                .returnMessage(ExceptionControllerAdvice
                        .getErrorMsgReplace(ex.getDescription(), ex.getErrorMsg(), ex.getErrorMsg2())).build();

        return ResourceConverter.toResponseEntity(responseObject, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    private static String getErrorMsgReplace(String description, String msg, String msg2) {
        try {
            String retMsg;

            if (StringUtils.isBlank(msg)) {
                retMsg = description;
            } else {
                retMsg = description.replace("%d", msg);
            }

            if (!StringUtils.isBlank(msg2)) {
                retMsg = retMsg.replace("%s", msg2);
            }

            return retMsg;
        } catch(Exception e) {
            return StringUtils.isBlank(msg) ? description : "(" + msg + ")" + description;
        }
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<ResponseObject<String>> handleBusinessForAPiException(UnauthorizedException ex, HttpServletRequest request) {

        ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
                .status(HttpStatus.UNPROCESSABLE_ENTITY)
                .returnCode(ex.getErrorCode())
                .returnMessage(ExceptionControllerAdvice.getErrorMsgReplace(ex.getDescription(), ex.getErrorMsg(), "")).build();

        return ResourceConverter.toResponseEntity(responseObject, HttpStatus.OK);
    }
}