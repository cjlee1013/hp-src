package kr.co.homeplus.inbound.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimWhoReason {

        B("B", "BUYER")
    ,   S("S", "SELLER")
    ,   H("H", "HOMEPLUS")
    ,   BUYER("B", "BUYER")
    ,   SELLER("S", "SELLER")
    ,   HOMEPLUS("H", "HOMEPLUS")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;


}
