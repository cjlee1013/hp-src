package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("상품리스트 Get Entry")
public class ItemListSetDto {

    @ApiModelProperty(value = "상품번호", position = 1)
    private String itemNo;

    @ApiModelProperty(value = "상품명" , position = 2)
    private String itemNm;

    @ApiModelProperty(value = "판매가격" , position = 3)
    private String salePrice;

    @ApiModelProperty(value = "업체명" , position = 4)
    private String businessNm;

    @ApiModelProperty(value = "판매업체ID", position = 5)
    private String partnerId;

    @ApiModelProperty(value = "업체관리코드", position = 6)
    private String sellerItemCd;

    @ApiModelProperty(value = "판매시작일", position = 7)
    private String saleStartDt;

    @ApiModelProperty(value = "판매종료일", position = 8)
    private String saleEndDt;

    @ApiModelProperty(value = "상품구분", position = 10)
    private String itemDivision;

    @ApiModelProperty(value = "상품유형명", position = 11)
    private String itemTypeNm;

    @ApiModelProperty(value = "상품상태명", position = 12)
    private String itemStatusNm;

    @ApiModelProperty(value = "노출여부", position = 13)
    private String dispYnNm;

    @ApiModelProperty(value = "온라인 취급상태", position = 14)
    private String onlineDealYnNm;

    @ApiModelProperty(value = "취급중지", position = 15)
    private String stopDealYnNm;

    @ApiModelProperty(value = "대분류 카테고리명", position = 16)
    private String lcateNm;

    @ApiModelProperty(value = "중분류 카테고리명", position = 17)
    private String mcateNm;

    @ApiModelProperty(value = "소분류 카테고리명", position = 18)
    private String scateNm;

    @ApiModelProperty(value = "세분류 카테고리명", position = 19)
    private String dcateNm;

    @ApiModelProperty(value = "등록자", position = 96)
    private String regNm;

    @ApiModelProperty(value = "등록일", position = 97)
    private String regDt;

    @ApiModelProperty(value = "수정자", position = 98)
    private String chgNm;

    @ApiModelProperty(value = "수정일", position = 99)
    private String chgDt;
}
