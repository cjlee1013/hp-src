package kr.co.homeplus.inbound.core.exception.custom;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

/**
 * 인증 및 권한 관련 예외 발생시 예외내용을 response하기 위한 클래스<p>
 *
 * @see ResponseException
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CustomResponseException extends ResponseException {
    public CustomResponseException(HttpStatus status, String title, String detail) {
        super(status, title, detail);
    }

    public CustomResponseException(HttpStatus status, String code, String title, String detail) {
        super(status, code, title, detail);
    }
}
