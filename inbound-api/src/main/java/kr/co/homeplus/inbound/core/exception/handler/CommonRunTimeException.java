package kr.co.homeplus.inbound.core.exception.handler;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class CommonRunTimeException extends RuntimeException {
    private String version;
    private String msg;

    public CommonRunTimeException(String version, String msg) {
        this.version = version;
        this.msg = msg;
    }
}
