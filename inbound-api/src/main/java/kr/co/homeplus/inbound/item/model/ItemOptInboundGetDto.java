package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("상품 옵션정보 조회")
public class ItemOptInboundGetDto {

	/**
	 * 텍스트형 옵션정보
	 */
	@ApiModelProperty(value = "텍스트옵션사용여부 (사용:Y,미사용:N)", position = 1)
	private String optionTextUseYn;

	@ApiModelProperty(value = "텍스트형 옵션단계", position = 2)
	private Integer optionTextDepth;

	@ApiModelProperty(value = "텍스트형 1단계 타이틀", position = 3)
	private String option1Text;

	@ApiModelProperty(value = "텍스트형 2단계 타이틀", position = 4)
	private String option2Text;

}