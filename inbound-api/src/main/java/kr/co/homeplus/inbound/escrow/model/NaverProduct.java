package kr.co.homeplus.inbound.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ApiModel(value="네이버 상품")
@Getter
@Setter
@ToString
public class NaverProduct {

    @ApiModelProperty(value = "상품ID", position = 1)
    private String id;

    @ApiModelProperty(value = "판매자관리코드", position = 2)
    private String sellerManagementCode;

    @ApiModelProperty(value = "판매자내부코드 1", position = 3)
    private String sellerCustomCode1;

    @ApiModelProperty(value = "판매자내부코드 2", position = 4)
    private String sellerCustomCode2;

    @ApiModelProperty(value = "단일상품", position = 11)
    private NaverProductSingle single;

    @ApiModelProperty(value = "옵션정보", position = 21)
    private List<NaverProductCombinationOption> combinationOptions;
}
