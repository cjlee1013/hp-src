package kr.co.homeplus.inbound.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimCancelType {

        O("O", "CANCEL")
    ,   R("R", "PICK")
    ,   CANCEL("O", "CANCEL")
    ,   PICK("R", "PICK")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;


}
