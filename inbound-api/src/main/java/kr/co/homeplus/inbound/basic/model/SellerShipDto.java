package kr.co.homeplus.inbound.basic.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("판매자 배송정책 정보")
public class SellerShipDto {
	@ApiModelProperty(value = "배송정책 번호", position = 1)
	private long shipPolicyNo;

	@ApiModelProperty(value = "배송정책 명(최대 50자)", position = 2)
	private String	shipPolicyName;

	@ApiModelProperty(value = "배송방법 코드 (DS_DLV:택배배송, DS_DRCT:직접배송, DS_PICK:방문수령(픽업), DS_QUICK:퀵배송)", position = 3)
	private String	shipMethodCode;

	@ApiModelProperty(value = "배송방법 명", position = 4)
	private String	shipMethodName;

	@ApiModelProperty(value = "배송유형 코드 (ITEM:상품별, BUNDLE:묶음배송)", position = 5)
	private String	shipTypeCode;

	@ApiModelProperty(value = "배송유형 명", position = 6)
	private String	shipTypeName;

	@ApiModelProperty(value = "배송비종류 코드 (FREE:무료, FIXED:유료, COND:조건부 무료 )", position = 7)
	private String	shipKindCode;

	@ApiModelProperty(value = "배송비종류 명", position = 8)
	private String	shipKindName;

	@ApiModelProperty(value = "배송비", position = 9)
	private Integer	shipFee;

	@ApiModelProperty(value = "수량별 차등 여부 (Y:차등설정, N:차등 설정 안함)", position = 10)
	private String	differentYn;

	@ApiModelProperty(value = "차등 수량", position = 11)
	private Integer	differentQty;

	@ApiModelProperty(value = "배송비 결제 방식 코드", position = 12)
	private String	prePaymentCode;

	@ApiModelProperty(value = "배송비 결제 방식 명", position = 13)
	private String	prePaymentName;

	@ApiModelProperty(value = "배송가능지역 코드 (ALL:전국, EXCLUDE:제주/도서산간 제외)", position = 14)
	private String	shipAreaCode;

	@ApiModelProperty(value = "배송가능지역 명", position = 15)
	private String	shipAreaName;

	@ApiModelProperty(value = "제주 배송비 - 오타. 삭제예정", position = 16)
	private Integer	extraJejeFee;

	@ApiModelProperty(value = "제주 배송비", position = 16)
	private Integer	extraJejuFee;

	@ApiModelProperty(value = "도서산간 배송비", position = 17)
	private Integer	extraIslandFee;

	@ApiModelProperty(value = "반품/교환 배송비", position = 18)
	private Integer	claimShipFee;

	@ApiModelProperty(value = "출고지", position = 19)
	private String	releaseZipcode;

	@ApiModelProperty(value = "출고지", position = 20)
	private String	releaseAddress1;

	@ApiModelProperty(value = "출고지", position = 21)
	private String	releaseAddress2;

	@ApiModelProperty(value = "회수지", position = 22)
	private String	returnZipcode;

	@ApiModelProperty(value = "회수지", position = 23)
	private String	returnAddress1;

	@ApiModelProperty(value = "회수지", position = 24)
	private String	returnAddress2;

	@ApiModelProperty(value = "출고기한 - 일 단위 (-1:미정)", position = 25)
	private Integer	releaseDay;

	@ApiModelProperty(value = "출고기한 5일 인 경우 결제시간(1~24)", position = 26)
	private Integer	releaseTime;

	@ApiModelProperty(value = "주말공휴일 제외 여부 (Y:제외, N:제외안함)", position = 27)
	private String	holidayExceptYn;

	@ApiModelProperty(value = "안심번호 사용 여부 (Y:사용, N:미사용)", position = 28)
	private String	safeNumberUseYn;
}

