package kr.co.homeplus.inbound.shipping.model.partner;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 택배사코드관리 리스트 요청 DTO")
public class ShipCompanyManageListSetDto {
    @ApiModelProperty(notes = "연동업체")
    private String schDlvLinkCompany;

    @ApiModelProperty(notes = "검색조건 타입")
    private String schKeywordType;

    @ApiModelProperty(notes = "검색어")
    private String schKeyword;
}
