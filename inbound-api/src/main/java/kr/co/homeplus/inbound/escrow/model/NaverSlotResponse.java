package kr.co.homeplus.inbound.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ApiModel(description = "네이 배송 slot 연동 결과")
@Getter
@Setter
@ToString
public class NaverSlotResponse {

    @ApiModelProperty(value = "상태", example = "NORMAL,FAIL_INSPECT")
    private String status;

    @ApiModelProperty(value = "배송마감여부")
    private boolean deliveryEndYn;

    @ApiModelProperty(value = "오류코드")
    private String code;

    @ApiModelProperty(value = "오류메세지")
    private String message;

}
