package kr.co.homeplus.inbound.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimExchangeStatus {

        D0("D0", "CONFIRM")
    ,   D1("D1", "SCHEDULE")
    ,   D2("D2", "SHIPPING")
    ,   D3("D3", "COMPLETE")
    ,   CONFIRM("D0", "CONFIRM")
    ,   SCHEDULE("D1", "SCHEDULE")
    ,   SHIPPING("D2", "SHIPPING")
    ,   COMPLETE("D3", "COMPLETE")
    ,   ALL("ALL","ALL")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;


}
