package kr.co.homeplus.inbound.itemRelation.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("연관 상품관리 조회 파라미터 Get Entry")
public class ItemRelationListParamDto {

    @ApiModelProperty(value = "검색 조건")
    @NotEmpty(message = "검색 조건")
    private String schType;

    @ApiModelProperty(value = "검색 키워드")
    @Size(min = 2, max = 100, message = "검색 키워드")
    private String schKeyword;

    @ApiModelProperty(value = "판매업체ID")
    private String schPartnerId;

    @ApiModelProperty(value = "연관번호 리스트")
    private List<String> relationNoList;
}
