package kr.co.homeplus.inbound.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ApiModel(value="네이버 단일상품")
@Getter
@Setter
@ToString
public class NaverProductSingle {

    @ApiModelProperty(value = "상품가격", position = 1)
    private Long price;

    @ApiModelProperty(value = "상품수량", position = 2)
    private Integer quantity;

    @ApiModelProperty(value = "구매상태", position = 3)
    private String resultCode = "FAIL_QUANTITY";

}
