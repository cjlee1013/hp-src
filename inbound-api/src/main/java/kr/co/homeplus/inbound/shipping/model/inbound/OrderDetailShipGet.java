package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.inbound.shipping.model.partner.OrderDetailShipPartnerDto;
import lombok.Data;

@Data
@ApiModel("배송정보")
public class OrderDetailShipGet {
    @ApiModelProperty(notes = "배송번호", example = "3000000001", position = 1)
    private String bundleNo;

    @ApiModelProperty(notes = "배송비지급", example = "선불", position = 2)
    private String shipPriceType;

    @ApiModelProperty(notes = "배송비", example = "2500", position = 3)
    private String ShipPrice;

    @ApiModelProperty(notes = "발송기한", example = "2021-01-10", position = 4)
    private String originShipDate;

    @ApiModelProperty(notes = "2차발송기한", example = "2021-01-30", position = 5)
    private String delayShipDate;

    @ApiModelProperty(notes = "발송지연사유메시지", example = "재고가 부족하여 2021년 1월 30일까지 배송될 예정입니다", position = 6)
    private String delayShipMessage;

    @ApiModelProperty(notes = "수령인", example = "이홈플", position = 7)
    private String receiverName;

    @ApiModelProperty(notes = "수령인 연락처", example = "010-9876-5432", position = 8)
    private String receiverMobileNo;

    @ApiModelProperty(notes = "우편번호", example = "07567", position = 9)
    private String zipcode;

    @ApiModelProperty(notes = "주소", example = "서울시 강서구 홈플러스 강서점", position = 10)
    private String address;

    @ApiModelProperty(notes = "배송메세지", example = "부재시 경비실에 맡겨주세요", position = 11)
    private String deliveryShipMessage;

    @ApiModelProperty(notes = "도서산간배송비", example = "2500", position = 12)
    private String islandShipPrice;

    public OrderDetailShipGet(OrderDetailShipPartnerDto orderDetailShipPartnerDto) {
        this.bundleNo = orderDetailShipPartnerDto.getBundleNo();
        this.shipPriceType = orderDetailShipPartnerDto.getShipPriceType();
        this.ShipPrice = orderDetailShipPartnerDto.getTotShipPrice();
        this.originShipDate = orderDetailShipPartnerDto.getOrgShipDt();
        this.delayShipDate = orderDetailShipPartnerDto.getDelayShipDt();
        this.delayShipMessage = orderDetailShipPartnerDto.getDelayShipNm();
        this.receiverName = orderDetailShipPartnerDto.getReceiverNm();
        this.receiverMobileNo = orderDetailShipPartnerDto.getShipMobileNo();
        this.zipcode = orderDetailShipPartnerDto.getZipcode();
        this.address = orderDetailShipPartnerDto.getAddr();
        this.deliveryShipMessage = orderDetailShipPartnerDto.getDlvShipMsg();
        this.islandShipPrice = orderDetailShipPartnerDto.getIslandShipPrice();
    }
}
