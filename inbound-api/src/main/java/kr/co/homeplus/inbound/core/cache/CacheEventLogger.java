package kr.co.homeplus.inbound.core.cache;

import lombok.extern.slf4j.Slf4j;
import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;

/**
 * EhCache event를 추적하기 위한 Listener 입니다.
 */
@Slf4j
public class CacheEventLogger implements CacheEventListener<Object, Object> {
    public void onEvent(CacheEvent<? extends Object, ? extends Object> cacheEvent) {
        log.debug("cache event logger message. getType:<{}>, getKey:<{}> / getOldValue:<{}> / getNewValue:<{}>",
            cacheEvent.getType(), cacheEvent.getKey(), cacheEvent.getOldValue(), cacheEvent.getNewValue());
    }
}