package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.inbound.shipping.model.partner.OrderDetailItemPartnerDto;
import lombok.Data;

@Data
@ApiModel("주문상품정보")
public class OrderDetailItemGet {
    @ApiModelProperty(notes = "상품번호", example = "10000000000001", position = 1)
    private String itemNo;

    @ApiModelProperty(notes = "상품주문번호", example = "3000000001", position = 2)
    private String orderItemNo;

    @ApiModelProperty(notes = "배송상태", example = "NEWORDER", position = 3)
    private String shipStatus;

    @ApiModelProperty(notes = "상품명", example = "홈플러스 시그니처", position = 4)
    private String itemName;

    @ApiModelProperty(notes = "옵션", example = "화이트", position = 5)
    private String textOption;

    @ApiModelProperty(notes = "상품금액", example = "1000", position = 6)
    private String itemPrice;

    @ApiModelProperty(notes = "최초주문수량", example = "1", position = 7)
    private String orgItemQty;

    @ApiModelProperty(notes = "수량", example = "3", position = 8)
    private String itemQty;

    @ApiModelProperty(notes = "총상품금액", example = "3000", position = 9)
    private String orderPrice;

    @ApiModelProperty(notes = "배송방법", example = "DELIVERY", position = 10)
    private String shipMethod;

    @ApiModelProperty(notes = "택배사명", example = "CJ대한통운", position = 11)
    private String deliveryName;

    @ApiModelProperty(notes = "송장번호", example = "1234567890", position = 12)
    private String invoiceNo;

    @ApiModelProperty(notes = "배송완료일", example = "2021-01-15", position = 13)
    private String completeDate;

    public OrderDetailItemGet(OrderDetailItemPartnerDto orderDetailItemPartnerDto) {
        this.itemNo = orderDetailItemPartnerDto.getItemNo();
        this.orderItemNo = orderDetailItemPartnerDto.getOrderItemNo();
        this.shipStatus = orderDetailItemPartnerDto.getShipStatusNm();
        this.itemName = orderDetailItemPartnerDto.getItemNm1();
        this.textOption = orderDetailItemPartnerDto.getTxtOptVal();
        this.itemPrice = orderDetailItemPartnerDto.getItemPrice();
        this.orgItemQty = orderDetailItemPartnerDto.getOrgItemQty();
        this.itemQty = orderDetailItemPartnerDto.getItemQty();
        this.orderPrice = orderDetailItemPartnerDto.getOrderPrice();
        this.shipMethod = orderDetailItemPartnerDto.getShipMethodNm();
        this.deliveryName = orderDetailItemPartnerDto.getDlvCd();
        this.invoiceNo = orderDetailItemPartnerDto.getInvoiceNo();
        this.completeDate = orderDetailItemPartnerDto.getCompleteDt();
    }
}
