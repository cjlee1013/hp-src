package kr.co.homeplus.inbound.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;
import lombok.RequiredArgsConstructor;


@Data
@RequiredArgsConstructor
@ApiModel("ResponseResult")
public class ResponseResult {

  @ApiModelProperty(value = "결과코드", required = true, position = 1)
  private String returnCode = "0";

  @ApiModelProperty(value = "결과메세지", required = true, position = 5)
  private String returnMsg = "처리 되었습니다.";

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @ApiModelProperty(value = "결과Key", position = 10)
  private String returnKey;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @ApiModelProperty(value = "결과개수", position = 15)
  private int returnCnt;

  public <T extends Number> ResponseResult(int returnCnt, T returnKey, String returnMsg) {
    this.returnCnt = returnCnt;
    this.returnKey = String.valueOf(returnKey);
    this.returnMsg = returnMsg;
  }

  public <T extends String> ResponseResult(int returnCnt, T returnKey, String returnMsg) {
    this.returnCnt = returnCnt;
    this.returnKey = returnKey;
    this.returnMsg = returnMsg;
  }

  public static <T extends String> ResponseResult getResponseResult(int returnCnt, T returnKey, String returnMsg) {
    return new ResponseResult(returnCnt, returnKey, returnMsg);
  }

  public static <T extends Number> ResponseResult getResponseResult(int returnCnt, T returnKey, String returnMsg) {
    return new ResponseResult(returnCnt, returnKey, returnMsg);
  }

  public static ResponseResult getEmpty() {
    return new ResponseResult(0, 0, "");
  }
}

