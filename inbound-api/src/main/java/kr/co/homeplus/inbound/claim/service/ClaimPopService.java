package kr.co.homeplus.inbound.claim.service;


import kr.co.homeplus.inbound.claim.enums.ClaimReasonType;
import kr.co.homeplus.inbound.claim.enums.ClaimType;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimApproveSet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimDeliverySet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimPendingSet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimRejectSet;
import kr.co.homeplus.inbound.claim.model.inbound.ExchangeCompleteSet;
import kr.co.homeplus.inbound.claim.model.inbound.ExchangeShippingSet;
import kr.co.homeplus.inbound.claim.model.inbound.PickCompleteSet;
import kr.co.homeplus.inbound.claim.model.inbound.PickRequestSet;
import kr.co.homeplus.inbound.claim.model.inbound.PickShippingSet;
import kr.co.homeplus.inbound.claim.model.partner.ClaimApproveListPartnerSetDto;
import kr.co.homeplus.inbound.claim.model.partner.ClaimExchShippingModifyPartnerDto;
import kr.co.homeplus.inbound.claim.model.partner.ClaimPickShippingModifyPartnerDto;
import kr.co.homeplus.inbound.claim.model.partner.ClaimRequestPartnerSetDto;
import kr.co.homeplus.inbound.claim.model.partner.ClaimShippingPartnerSetDto;
import kr.co.homeplus.inbound.claim.model.partner.ClaimShippingStatusPartnerSetDto;
import kr.co.homeplus.inbound.constants.ExceptionCode;
import kr.co.homeplus.inbound.core.constants.ResourceRouteName;
import kr.co.homeplus.inbound.shipping.model.partner.OrderShipManageShipAllSetDto;
import kr.co.homeplus.inbound.shipping.service.ShippingService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ClaimPopService {

    private final ResourceClient resourceClient;
    private final ShippingService shippingService;


    /**
     * 클레임 취소 승인 요청
     * parameter가 List 형이라 별도 method로 개발
     * **/
    public ResponseObject<String> requestClaimApprove(ClaimApproveSet claimApproveSet){
        ClaimApproveListPartnerSetDto claimApproveListPartnerSetDto = new ClaimApproveListPartnerSetDto(
            claimApproveSet);
        try {
            return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG,
                claimApproveListPartnerSetDto, "/po/claim/requestClaimApprove",
                new ParameterizedTypeReference<>() {
                });
        } catch (Exception e) {
            return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_7001.getCode(), ExceptionCode.ERROR_CODE_7001.getDescription());
        }
    }


    /**
     * 클레임 보류 요청
     * **/
    public ResponseObject<String> setClaimPending(ClaimPendingSet claimPendingSet) throws Exception {
        ClaimRequestPartnerSetDto claimRequestSetDto
            = ClaimRequestPartnerSetDto.builder()
            .claimNo(claimPendingSet.getClaimNo())
            .claimBundleNo(claimPendingSet.getClaimBundleNo())
            .reasonType(claimPendingSet.getReasonType())
            .reasonTypeDetail(claimPendingSet.getReasonTypeDetail())
            .claimReqType("CH")
            .regId("PARTNER")
            .claimType(ClaimType.valueOf(claimPendingSet.getClaimType()).getType())
            .build();

        try {
            return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG,
                claimRequestSetDto, "/claim/reqChange",
                new ParameterizedTypeReference<ResponseObject<String>>() {
                });
        } catch (Exception e) {
            return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_7001.getCode(), ExceptionCode.ERROR_CODE_7001.getDescription());
        }
    }

    /**
     * 클레임 거절 요청
     * **/
    public ResponseObject<String> setClaimReject(ClaimRejectSet claimRejectSetDto,String partnerId) throws Exception {
        ResponseObject<String> result = new ResponseObject<>();
        String claimReasonType = claimRejectSetDto.getReasonType();
        if (claimReasonType != null) {
            claimReasonType = ClaimReasonType.valueOf(claimReasonType).getDescription();
        }
        /* 1.클레임 상태변경 요청 */
        ClaimRequestPartnerSetDto claimRequestSetDto
                = ClaimRequestPartnerSetDto.builder()
                .claimNo(claimRejectSetDto.getClaimNo())
                .claimBundleNo(claimRejectSetDto.getClaimBundleNo())
                .reasonType(claimReasonType)
                .reasonTypeDetail(claimRejectSetDto.getReasonTypeDetail())
                .claimReqType("CD")
                .regId("PARTNER")
                .claimType(claimRejectSetDto.getClaimType())
                .build();

        ResponseObject<Object> claimStatusResult = resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimRequestSetDto, "/claim/reqChange", new ParameterizedTypeReference<ResponseObject<Object>>() {});

        if(!claimStatusResult.getReturnCode().equalsIgnoreCase("0000")){
            result.setReturnCode(claimStatusResult.getReturnCode());
            result.setReturnMessage(claimStatusResult.getReturnMessage());
            return result;
        }

        /* 1-1. 반품인경우 발송요청 없이 return */
        if(claimRejectSetDto.getClaimType().equalsIgnoreCase("R") || claimRejectSetDto.getClaimType().equalsIgnoreCase("X")){
            result.setReturnCode("0000");
            result.setReturnMessage("반품거부 성공");
            return result;
        }

        /* 2.발송요청
         *  claimReqType = CD / 상태 변경 타입 = 거부
         *  shipMethod != N / 배송방법 != 배송없음
         * */
        OrderShipManageShipAllSetDto orderShipManageShipAllSetDto = new OrderShipManageShipAllSetDto(claimRejectSetDto.getShipInfo(),partnerId);
        if(!orderShipManageShipAllSetDto.getShipMethod().equalsIgnoreCase("N")){
            ResponseObject<Object> setShipResult = resourceClient.postForResponseObject(ResourceRouteName.SHIPPING, orderShipManageShipAllSetDto, "/partner/sell/setShipAllExClaim", new ParameterizedTypeReference<ResponseObject<Object>>() {});

            if(!setShipResult.getReturnCode().equalsIgnoreCase("SUCCESS")){
                result.setReturnCode(setShipResult.getReturnCode());
                result.setReturnMessage("클레임 거부상태 변경성공\n 배송요청 실패 : " + setShipResult.getReturnMessage() + "\n 관리자에게 문의 바랍니다.");
                return result;
            }
        }

        /* return */
        result.setReturnCode("0000");
        result.setReturnMessage("클레임 상태변경 성공");
        return result;
    }

    /**
     * 클레임 수거요청
     * **/
    public ResponseObject<String> setPickRequest(PickRequestSet pickRequestSet){
        ClaimShippingPartnerSetDto claimShippingSetDto = new ClaimShippingPartnerSetDto();
        claimShippingSetDto.setClaimNo(pickRequestSet.getClaimNo());
        claimShippingSetDto.setClaimBundleNo(pickRequestSet.getClaimBundleNo());
        claimShippingSetDto.setClaimShippingReqType("P");
        claimShippingSetDto.setChgId("PARTNER");
        ClaimPickShippingModifyPartnerDto claimPickShippingModifyDto = new ClaimPickShippingModifyPartnerDto(
            pickRequestSet);
        claimShippingSetDto.setClaimPickShippingModifyDto(claimPickShippingModifyDto);
        try {
            return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG,
                claimShippingSetDto, "/claim/reqClaimShippingChange",
                new ParameterizedTypeReference<ResponseObject<String>>() {
                });
        } catch (Exception e) {
            return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_7001.getCode(), ExceptionCode.ERROR_CODE_7001.getDescription());
        }
    }

    /**
     * 클레임 수거완료요청
     * **/
    public ResponseObject<Boolean> setPickComplete(PickCompleteSet pickCompleteSet){

        ClaimShippingStatusPartnerSetDto claimShippingStatusSetDto = new ClaimShippingStatusPartnerSetDto();

        claimShippingStatusSetDto.setClaimNo(pickCompleteSet.getClaimNo());
        claimShippingStatusSetDto.setClaimBundleNo(pickCompleteSet.getClaimBundleNo());
        claimShippingStatusSetDto.setPickStatus("P3");
        claimShippingStatusSetDto.setChgId("PARTNER");
        try {
            return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG,
                claimShippingStatusSetDto, "/claim/reqClaimShippingStatus",
                new ParameterizedTypeReference<ResponseObject<Boolean>>() {
                });
        } catch (Exception e) {
            return ResourceConverter.toResponseObject(false, HttpStatus.OK, ExceptionCode.ERROR_CODE_7001.getCode(), ExceptionCode.ERROR_CODE_7001.getDescription());
        }
    }
    /**
     * 클레임 수거정보변경
     * **/
    public ResponseObject<String> setPickShipping(PickShippingSet pickShippingSet){
        ClaimShippingPartnerSetDto claimShippingSetDto = new ClaimShippingPartnerSetDto();
        claimShippingSetDto.setClaimNo(pickShippingSet.getClaimNo());
        claimShippingSetDto.setClaimBundleNo(pickShippingSet.getClaimBundleNo());
        claimShippingSetDto.setClaimShippingReqType("P");
        claimShippingSetDto.setChgId("PARTNER");
        ClaimPickShippingModifyPartnerDto claimPickShippingModifyDto = new ClaimPickShippingModifyPartnerDto(
            pickShippingSet);
        claimShippingSetDto.setClaimPickShippingModifyDto(claimPickShippingModifyDto);
        try {
            return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG,
                claimShippingSetDto, "/claim/reqClaimShippingChange",
                new ParameterizedTypeReference<ResponseObject<String>>() {
                });
        } catch (Exception e) {
            return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_7001.getCode(), ExceptionCode.ERROR_CODE_7001.getDescription());
        }
    }

    /**
     * 클레임 교환배송정보 변경
     * **/
    public ResponseObject<String> setClaimDelivery(ClaimDeliverySet claimDeliverySet){
        ClaimShippingPartnerSetDto claimShippingSetDto = new ClaimShippingPartnerSetDto();
        claimShippingSetDto.setClaimNo(claimDeliverySet.getClaimNo());
        claimShippingSetDto.setClaimBundleNo(claimDeliverySet.getClaimBundleNo());
        claimShippingSetDto.setClaimShippingReqType("E");
        claimShippingSetDto.setChgId("PARTNER");
        ClaimExchShippingModifyPartnerDto claimExchShippingModifyDto = new ClaimExchShippingModifyPartnerDto(
            claimDeliverySet);
        claimShippingSetDto.setClaimExchShippingModifyDto(claimExchShippingModifyDto);
        try {
            return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG,
                claimShippingSetDto, "/claim/reqClaimShippingChange",
                new ParameterizedTypeReference<>() {
                });
        } catch (Exception e) {
            return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_7001.getCode(), ExceptionCode.ERROR_CODE_7001.getDescription());
        }
    }


    /**
     * 클레임 교환배송정보 변경
     * **/
    public ResponseObject<String> setExchShipping(ExchangeShippingSet exchangeShippingSet){
        ClaimShippingPartnerSetDto claimShippingSetDto = new ClaimShippingPartnerSetDto();
        claimShippingSetDto.setClaimNo(exchangeShippingSet.getClaimNo());
        claimShippingSetDto.setClaimBundleNo(exchangeShippingSet.getClaimBundleNo());
        claimShippingSetDto.setClaimShippingReqType("E");
        claimShippingSetDto.setChgId("PARTNER");
        ClaimExchShippingModifyPartnerDto claimExchShippingModifyDto = new ClaimExchShippingModifyPartnerDto(
            exchangeShippingSet);
        claimShippingSetDto.setClaimExchShippingModifyDto(claimExchShippingModifyDto);
        try {
            return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG,
                claimShippingSetDto, "/claim/reqClaimShippingChange",
                new ParameterizedTypeReference<ResponseObject<String>>() {
                });
        } catch (Exception e) {
            return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_7001.getCode(), ExceptionCode.ERROR_CODE_7001.getDescription());
        }
    }

    /**
     * 클레임 교환배송정보 변경
     * **/
    public ResponseObject<String> setExchComplete(ExchangeCompleteSet exchangeCompleteSet){
        ResponseObject<String> result = new ResponseObject<>();
        ClaimRequestPartnerSetDto claimRequestSetDto
            = ClaimRequestPartnerSetDto.builder()
            .claimNo(exchangeCompleteSet.getClaimNo())
            .claimBundleNo(exchangeCompleteSet.getClaimBundleNo())
            .claimReqType("CA")
            .regId("PARTNER")
            .claimType("X")
            .build();

        /* 1.교환완료 요청 */
        try {
            ResponseObject<Object> claimStatusResult = resourceClient.postForResponseObject(
                ResourceRouteName.ESCROWMNG, claimRequestSetDto, "/claim/reqChange",
                new ParameterizedTypeReference<ResponseObject<Object>>() {
                });

            if (!claimStatusResult.getReturnCode()
                .equalsIgnoreCase("0000")) {
                result.setReturnCode(claimStatusResult.getReturnCode());
                result.setReturnMessage(claimStatusResult.getReturnMessage());
                return result;
            }
            ClaimShippingPartnerSetDto claimShippingSetDto = new ClaimShippingPartnerSetDto();
            claimShippingSetDto.setClaimNo(exchangeCompleteSet.getClaimNo());
            claimShippingSetDto.setClaimBundleNo(exchangeCompleteSet.getClaimBundleNo());
            claimShippingSetDto.setClaimShippingReqType("E");
            claimShippingSetDto.setChgId("PARTNER");
            ClaimExchShippingModifyPartnerDto claimExchShippingModifyDto = new ClaimExchShippingModifyPartnerDto(
                exchangeCompleteSet);
            claimShippingSetDto.setClaimExchShippingModifyDto(claimExchShippingModifyDto);

            return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG,
                claimShippingSetDto, "/claim/reqClaimShippingChange",
                new ParameterizedTypeReference<ResponseObject<String>>() {
                });
        } catch (Exception e) {
            return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_7001.getCode(), ExceptionCode.ERROR_CODE_7001.getDescription());
        }
    }
}
