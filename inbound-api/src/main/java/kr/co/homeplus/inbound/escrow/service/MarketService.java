package kr.co.homeplus.inbound.escrow.service;

import static kr.co.homeplus.inbound.constants.EscrowConstants.MARKET_RETURN_ERROR_CODE;
import static kr.co.homeplus.inbound.constants.EscrowConstants.MARKET_RETURN_ERROR_MSG;
import static kr.co.homeplus.inbound.constants.EscrowConstants.MARKET_RETURN_SUCCESS_CODE;
import static kr.co.homeplus.inbound.constants.EscrowConstants.MARKET_TYPE_NAVER;
import static kr.co.homeplus.inbound.core.constants.ResourceRouteName.ESCROW;

import kr.co.homeplus.inbound.escrow.model.MarketSlotResponse;
import kr.co.homeplus.inbound.escrow.model.MarketStockResponse;
import kr.co.homeplus.inbound.escrow.model.MarketStockSelect;
import kr.co.homeplus.inbound.escrow.model.NaverProductStock;
import kr.co.homeplus.inbound.escrow.model.NaverProductStockResponse;
import kr.co.homeplus.inbound.escrow.model.NaverSlotResponse;
import kr.co.homeplus.inbound.utils.StringUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Slf4j
@Service
@RequiredArgsConstructor
public class MarketService {

    private final ResourceClient resourceClient;

    /**
     * 마켓 슬롯 주문 가능여부 체크
     * @param marketType
     * @param storeId
     * @param deliveryType
     * @param shipDt
     * @param slotId
     * @param zipcode
     * @return
     */
    public MarketSlotResponse getMarketSlotCapa(String marketType, String storeId, String deliveryType, String shipDt, String slotId, String zipcode) {

        try {
            StringBuilder apiUri = new StringBuilder();
            apiUri.append("/")
                .append(marketType)
                .append("/store/checkStoresSlotClose?")
                .append("storeId=")
                .append(storeId)
                .append("&deliveryType=")
                .append(deliveryType)
                .append("&shipDt=")
                .append(shipDt)
                .append("&slotId=")
                .append(slotId)
                .append("&zipcode=")
                .append(zipcode);

            ResourceClientRequest<MarketSlotResponse> request = ResourceClientRequest.<MarketSlotResponse>getBuilder()
                .apiId(ESCROW)
                .uri(apiUri.toString())
                .typeReference(new ParameterizedTypeReference<>() {
                })
                .build();

            MarketSlotResponse marketSlotResponse = resourceClient.get(request, new TimeoutConfig())
                .getBody()
                .getData();

            return marketSlotResponse;
        } catch (Exception e) {
            return new MarketSlotResponse().builder().code(MARKET_RETURN_ERROR_CODE).message(MARKET_RETURN_ERROR_MSG).build();
        }
    }

    public NaverSlotResponse checkNaverSlotCapa(String storeId, long areaNo, String deliveryType, String shipDt, String slotId, String zipcode) {

        NaverSlotResponse naverSlotResponse = new NaverSlotResponse();

        if (StringUtils.isEmpty(slotId) || slotId.length() != 12) {
            naverSlotResponse.setStatus("NORMAL");
            naverSlotResponse.setDeliveryEndYn(true);
            naverSlotResponse.setCode("5002");
            naverSlotResponse.setMessage("slotId 길이가 상이합니다.");
            log.info("[NAVER_SLOT_CHECK] response:{}", naverSlotResponse.toString());
            return naverSlotResponse;
        }

        MarketSlotResponse marketSlotResponse = this.getMarketSlotCapa(MARKET_TYPE_NAVER, storeId, deliveryType, shipDt, slotId.substring(8), zipcode);

        switch (marketSlotResponse.getCode()) {
            case MARKET_RETURN_SUCCESS_CODE:
                naverSlotResponse.setStatus("NORMAL");
                naverSlotResponse.setDeliveryEndYn(false);
                naverSlotResponse.setCode(MARKET_RETURN_SUCCESS_CODE);
                break;
            case MARKET_RETURN_ERROR_CODE:
                naverSlotResponse.setStatus("FAIL_INSPECT");
                naverSlotResponse.setDeliveryEndYn(true);
                naverSlotResponse.setCode(marketSlotResponse.getCode());
                naverSlotResponse.setMessage(marketSlotResponse.getMessage());
                break;
            default:
                naverSlotResponse.setStatus("NORMAL");
                naverSlotResponse.setDeliveryEndYn(true);
                naverSlotResponse.setCode(marketSlotResponse.getCode());
                naverSlotResponse.setMessage(marketSlotResponse.getMessage());
        }
        log.info("[NAVER_SLOT_CHECK] response:{}", naverSlotResponse.toString());
        return naverSlotResponse;
    }

    /**
     * 마켓 상품 재고 체크 & 주문 가능 시간 체크
     * @param marketStockSelect
     * @param marketType
     * @return
     */
    public MarketStockResponse getMarketItemStock(MarketStockSelect marketStockSelect, String marketType) {

        try {
            String apiUri = "/" + marketType + "/store/checkItemStock";

            ResourceClientRequest<MarketStockResponse> request = ResourceClientRequest.<MarketStockResponse>postBuilder()
                .apiId(ESCROW)
                .uri(apiUri)
                .postObject(marketStockSelect)
                .typeReference(new ParameterizedTypeReference<>() {
                })
                .build();

            MarketStockResponse marketStockResponse = resourceClient.post(request, new TimeoutConfig())
                .getBody()
                .getData();

            return marketStockResponse;
        } catch (Exception e) {
            return MarketStockResponse.builder().code(MARKET_RETURN_ERROR_CODE).message(MARKET_RETURN_ERROR_MSG).build();
        }
    }

    /**
     * 네이버 아이템 재고 체크
     * @param naverProductStock
     * @return
     */
    public NaverProductStockResponse getNaverItemStock(NaverProductStock naverProductStock) {
        log.info("[NAVER_ITEM_STOCK_CHECK] req naverProductStock:" + naverProductStock);
        try {
            String apiUri = "/naver/store/checkItemStock";

            ResourceClientRequest<NaverProductStockResponse> request = ResourceClientRequest.<NaverProductStockResponse>postBuilder()
                .apiId(ESCROW)
                .uri(apiUri)
                .postObject(naverProductStock)
                .typeReference(new ParameterizedTypeReference<>() {
                })
                .build();

            NaverProductStockResponse naverProductStockResponse = resourceClient.post(request, new TimeoutConfig())
                .getBody()
                .getData();

            log.info("[NAVER_ITEM_STOCK_CHECK] response:{}", naverProductStockResponse.toString());
            
//            if (naverProductStockResponse.getProducts().get(0).getCombinationOptions() != null) {
//                naverProductStockResponse.getProducts()
//                    .stream()
//                    .flatMap(p -> p.getCombinationOptions()
//                        .stream())
//                    .forEach(c -> c.setResultCode("PASS"));
//            }
//            if (naverProductStockResponse.getProducts().get(0).getSingle() != null) {
//                naverProductStockResponse.getProducts()
//                    .stream()
//                    .map(p -> p.getSingle())
//                    .forEach(s -> s.setResultCode("PASS"));
//            }
//            log.info("[NAVER_ITEM_STOCK_CHECK] 임시 정상으로 return response:{}", naverProductStockResponse.toString());
            return naverProductStockResponse;
        } catch (Exception e) {
            NaverProductStockResponse naverProductStockResponse = new NaverProductStockResponse();
            naverProductStockResponse.setProducts(naverProductStock.getProducts());
            naverProductStockResponse.setStatus("FAIL_INSPECT");
            log.info("[NAVER_ITEM_STOCK_CHECK] response:{}", naverProductStockResponse.toString());
            return naverProductStockResponse;
        }
    }
}
