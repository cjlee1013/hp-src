package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.inbound.shipping.model.partner.ClaimOrderDetailInfoPartnerDto;
import lombok.Data;

@Data
@ApiModel("주문기본정보")
public class OrderClaimPossibleListGet {
    @ApiModelProperty(value = "주문번호", example = "3000000001", position = 1)
    private String purchaseOrderNo;
    @ApiModelProperty(value = "상품주문번호", example = "3000000001", position = 2)
    private String orderItemNo;
    @ApiModelProperty(value = "상품번호", example = "10000000000001", position = 3)
    private String itemNo;
    @ApiModelProperty(value = "상품명", example = "홈플러스 시그니처", position = 4)
    private String itemName;
    @ApiModelProperty(value = "옵션번호", example = "3000000001", position = 6)
    private String orderOptionNo;
    @ApiModelProperty(value = "주문수량", example = "1", position = 7)
    private String itemQty;
    @ApiModelProperty(value = "주문금액", example = "3000", position = 8)
    private String orderPrice;
    @ApiModelProperty(value = "상품별최소구매수량", example = "1", position = 9)
    private String purchaseMinimumQty;
    @ApiModelProperty(value = "주문타입", example = "ORD_NOR", hidden = true, position = 10)
    private String orderType;
    @ApiModelProperty(value = "주문일시", example = "2020-10-14 13:44:07", position = 11)
    private String orderDate;
    @ApiModelProperty(value = "클레임건수", example = "0", position = 12)
    private String claimItemQty;
    @ApiModelProperty(value = "행사번호", example = "1001015147", position = 13)
    private String promoNo;
    @ApiModelProperty(value = "행사명", example = "3월 한달행사-0227", position = 14)
    private String promoName;
    @ApiModelProperty(value = "수량별 배송비 차등 정보", example = "1", position = 15)
    private String bundleFeeDiffQty;

    public OrderClaimPossibleListGet(ClaimOrderDetailInfoPartnerDto partnerDto) {
        this.purchaseOrderNo = partnerDto.getPurchaseOrderNo();
        this.orderItemNo = partnerDto.getOrderItemNo();
        this.itemNo = partnerDto.getItemNo();
        this.itemName = partnerDto.getItemNm1();
        this.orderOptionNo = partnerDto.getOrderOptNo();
        this.itemQty = partnerDto.getOrderQty();
        this.orderPrice = partnerDto.getOrderPrice();
        this.purchaseMinimumQty = partnerDto.getPurchaseMinQty();
        this.orderType = partnerDto.getOrderType();
        this.orderDate = partnerDto.getOrderDt();
        this.claimItemQty = partnerDto.getClaimQty();
        this.promoNo = partnerDto.getPromoNo();
        this.promoName = partnerDto.getPromoNm1();
        this.bundleFeeDiffQty = partnerDto.getDiffQty();
    }
}
