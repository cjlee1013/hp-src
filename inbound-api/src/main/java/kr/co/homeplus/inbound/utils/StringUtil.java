package kr.co.homeplus.inbound.utils;

import java.lang.reflect.Field;
import kr.co.homeplus.inbound.constants.PatternConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

/**
 * String Util
 */
@Slf4j
public class StringUtil {

    /**
     * 상품 메인 이미지 URL
     * ex) this.imgUrl = CommonMainImg.getMainImgUrl(host, firstUrl, itemNo, "medium");
     *
     * @param host
     * @param firstUrl
     * @param no
     * @param type
     * @return String
     */
    public static String getMainImgUrl(final String host,
                                       final String firstUrl,
                                       final long no,
                                       final String type) {
        Assert.notNull(host, "host not be null. Please check StringUtil getMainImgUrl() param");
        Assert.notNull(firstUrl, "firstUrl not be null. Please check StringUtil getMainImgUrl() param");
        Assert.notNull(no, "no not be null. Please check StringUtil getMainImgUrl() param");
        Assert.notNull(type, "type not be null. Please check StringUtil getMainImgUrl() param");


        String noStr = String.valueOf(no);
        StringBuffer url = new StringBuffer();
        url.append(host);
        url.append(firstUrl);
        url.append(StringUtil.getMainImgPath1(noStr));
        url.append("/");
        url.append(StringUtil.getMainImgPath2(noStr));
        url.append("/");
        url.append(StringUtil.getMainImgPath3(noStr));
        url.append("/");
        url.append(noStr);
        if ( type.length() > 0 ) {
            url.append("_");
            url.append(type);
        }
        url.append(".jpg");

        return url.toString();
    }

    // 123456789 에서 오른쪽 끝 1자리 - 9
    private static String getMainImgPath1(String no) {
        return no.substring(no.length() -1);
    }

    // 123456789 에서 오른쪽 끝 1자리 제외하고 3자리 - 678
    private static String getMainImgPath2(String no) {
        final int PATH_LENGTH_CNT = 4;
        final String DUMMY_STR = "0";
        int noLength = no.length();
        String retPath;
        StringBuffer tempStr = new StringBuffer();

        if (noLength < PATH_LENGTH_CNT) {
            final int diffCnt = PATH_LENGTH_CNT - noLength;
            for (int idx = 0; idx < diffCnt; idx++) {
                tempStr.append(DUMMY_STR);
            }
            no = tempStr.toString() + no;
            noLength = no.length();
        }
        retPath = no.substring(noLength -4, noLength -1);

        return retPath;
    }

    // 123456789 전체. 0을 채워서 4자리로 만들기
    private static String getMainImgPath3(String no) {
        final int PATH_LENGTH_CNT = 4;
        final int NO_LENGTH = no.length();
        final String DUMMY_STR = "0";
        String retPath;
        StringBuffer tempStr = new StringBuffer();

        if (NO_LENGTH < PATH_LENGTH_CNT) {
            final int diffCnt = PATH_LENGTH_CNT - NO_LENGTH;
            for (int idx = 0; idx < diffCnt; idx++) {
                tempStr.append(DUMMY_STR);
            }
            retPath = tempStr.toString() + no;
        } else {
            retPath = no.toString();
        }

        return retPath;
    }

    // 업로드 서버에 URL전송시, 프로토콜(https:)이 지정안되어 있는경우 프로토콜 앞에 붙이기
    public static String concatUrlProtocol(String urlStr) {
        if (StringUtils.isEmpty(urlStr)) {
            return urlStr;
        }
        return !StringUtils.startsWith(urlStr, "https:") && !StringUtils.startsWith(urlStr, "http:") ? "https:" + urlStr : urlStr;
    }

    // 메인이미지 썸네일
    public static String getMainImgThumbnailUrl(String imgUrl) {
        String ext = imgUrl.substring(imgUrl.lastIndexOf( '.' ));
        return imgUrl.replace(ext, "_thumbnail") + ext;
    }

    // 날짜 string basic 포멧에 맞게 변환 (yyyy-MM-dd -> yyyy-MM-dd HH:00:00)
    public static String getDateFormatBasic(String str) {
        if (!StringUtils.isBlank(str)) {
            if (str.length() == 10) {
                return str + " 00:00:00";
            } else {
                return str;
            }
        } else {
            return "";
        }
    }

    // 날짜 string basic 포멧에 맞게 변환 (yyyy-MM-dd HH:00 -> yyyy-MM-dd HH:00:00)
    public static String getDateTimeFormatBasic(String str) {
        if (!StringUtils.isBlank(str)) {
            if (str.length() == 16) {
                return str + ":00";
            } else {
                return str;
            }
        } else {
            return "";
        }
    }

    // 엑셀 개행문자(줄바꿈) split
    public static String[] getExcelEnterSplit(final String str) {
        return str.split(PatternConstants.ENTER_STR);
    }

    // 엑셀 Row split
    public static String[] getExcelRowSplit(final String str) {
        if (!StringUtils.isBlank(str)) {
            return str.split(",");
        } else {
            return new String[0];
        }
    }

    // URL 이미지명 추출
    public static String getUrlImgName(final String url) {
        if (!StringUtils.isBlank(url)) {
            String name = url.substring(url.lastIndexOf('/') + 1, url.length());
            return name.substring(0, name.indexOf('?') > 0 ? name.indexOf('?') : name.length());
        } else {
            return null;
        }
    }

    // URL 이미지 확장자 추출
    public static String getUrlImgExtension(String url) {
        if (!StringUtils.isBlank(url)) {
            int pos = url.lastIndexOf(".");
            return url.substring(pos + 1);
        } else {
            return null;
        }
    }


    // 문자길이 제한(문자열 길이로 자름)
    public static String getStringLengthLimit(final String str, final int limit) {
        return str.length() > limit ? str.substring(0, limit) : str;
    }

    // 문자길이 제한(바이트로 자름)
    public static String getStringByteLengthLimit(final String str, final int limit) {
        return str.getBytes().length > limit ? new String(str.getBytes(), 0, limit) : str;
    }

    // 문자길이 제한(바이트로 자름)
    public static String getStringByteLengthLimit(final String str, final int limit, final String limitStr, final int limitByte) {
        if (str.length() > limit) {
            try {
                return ParseStringUtil.cut(str, limitByte) + limitStr;
            } catch (StringIndexOutOfBoundsException e) {
                return str;
            }
        } else {
            return str;
        }
    }

    // 공백제거 trim
    public static String getStringTrim(String str) {
        return StringUtils.trim(str).replaceAll(PatternConstants.NO_BREAK_SPACE, "");
    }

    // nvl
    public static String nvl(String str, String defaultStr) {
        return ( null == str || "" == str ) ? defaultStr : str ;
    }


    public static void main(String[] args) {
        System.out.println(
                StringUtil.getStringByteLengthLimit("[2.0 딜]모찌모찌 시바 쿠션쿠션쿠션", 17, "...", 30));
        System.out.println(ParseStringUtil.cut("[2.0 딜]모찌모찌 시바 쿠션쿠션쿠션", 30));

        System.out.println("[효신] 테스트 텍스트옵션 단계 변경".length());
        System.out.println(ParseStringUtil.cut("[효신] 테스트 텍스트옵션 단계 변경", 30));

        System.out.println(StringUtil.getStringByteLengthLimit("[2.0] 강아지 사료 - 딜테스트", 17, "...", 30));
    }

    /**
     * make a parameter url
     * @param urlString
     * @param clazz
     * @param o
     * @return
     */
    public static String getRequestString(String urlString, Class clazz, Object o) {
        String queryString = "?";

        try {
            for (Field f : clazz.getDeclaredFields()) {
                f.setAccessible(true);
                if (!ObjectUtils.isEmpty(f.get(o))) {
                    queryString += f.getName() + "=" + String.valueOf(f.get(o)) + "&";
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return urlString + queryString.substring(0,queryString.length()-1);
    }
}
