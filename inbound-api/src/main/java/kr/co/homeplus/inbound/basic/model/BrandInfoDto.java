package kr.co.homeplus.inbound.basic.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("브랜드 정보")
public class BrandInfoDto {
	@ApiModelProperty(value = "브랜드 코드", position = 1)
	private long brandNo;

	@ApiModelProperty(value = "한글 브랜드명(최대 15자)", position = 2)
	private String	brandName;

	@ApiModelProperty(value = "영문 브랜드명(최대 25자)", position = 3)
	private String 	brandEnglishName;
}
