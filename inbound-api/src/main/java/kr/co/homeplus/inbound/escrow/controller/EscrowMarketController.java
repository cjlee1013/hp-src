package kr.co.homeplus.inbound.escrow.controller;

import static kr.co.homeplus.inbound.constants.EscrowConstants.MARKET_TYPE_ELEVEN;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.inbound.escrow.model.MarketSlotResponse;
import kr.co.homeplus.inbound.escrow.model.MarketStockResponse;
import kr.co.homeplus.inbound.escrow.model.MarketStockSelect;
import kr.co.homeplus.inbound.escrow.model.NaverProductStock;
import kr.co.homeplus.inbound.escrow.model.NaverProductStockResponse;
import kr.co.homeplus.inbound.escrow.model.NaverSlotResponse;
import kr.co.homeplus.inbound.escrow.service.MarketService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "주문>마켓")
@ApiResponses(value = {
    @ApiResponse(code = 422, message = "Unprocessable Entity")
})
@RequestMapping("/escrow/market")
@RestController
@RequiredArgsConstructor
@Slf4j
public class EscrowMarketController {

    private final MarketService marketService;

    @GetMapping("/naver/store/checkStoresSlotClose")
    @ApiOperation(value = "[NAVER] 실시간 슬롯 마감여부 조회")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = NaverSlotResponse.class)
        , @ApiResponse(code = 404, message = "Not Found")
        , @ApiResponse(code = 405, message = "Method Not Allowed")
        , @ApiResponse(code = 422, message = "Unprocessable Entity")
        , @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public NaverSlotResponse checkStoreSlotCapaNaver(
        @ApiParam(value = "점포코드", required = true, example = "0024") @RequestParam String branchId,
        @ApiParam(value = "권역번호", required = true) @RequestParam long areaNo,
        @ApiParam(value = "배송유형", required = true, example = "TODAY_DELIVERY, DAWN_DELIVERY") @RequestParam String deliveryType,
        @ApiParam(value = "배송운영일 YYYY-MM-DD", required = true, example = "2021-08-02") @RequestParam String deliveryOperationYmd,
        @ApiParam(value = "slotId", required = true, example = "202108022301") @RequestParam(name="slotId") String slotId,
        @ApiParam(value = "우편번호", required = true) @RequestParam String zipCd
    )
    {
        log.info("[NAVER_SLOT_CHECK] storeId:{} areaNo:{} deliveryType:{} shipDt:{} slotId:{} zipcode:{}", branchId, areaNo, deliveryType, deliveryOperationYmd, slotId, zipCd);
        return marketService.checkNaverSlotCapa(branchId, areaNo, deliveryType, deliveryOperationYmd, slotId, zipCd);
    }

    @GetMapping("/eleven/store/checkStoresSlotClose")
    @ApiOperation(value = "[11번가] 실시간 슬롯 마감여부 조회")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = MarketSlotResponse.class)
        , @ApiResponse(code = 404, message = "Not Found")
        , @ApiResponse(code = 405, message = "Method Not Allowed")
        , @ApiResponse(code = 422, message = "Unprocessable Entity")
        , @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseObject<MarketSlotResponse> checkStoreSlotCapaEleven(
        @ApiParam(value = "점포코드", required = true, example = "0024") @RequestParam String storeId,
        @ApiParam(value = "배송유형", required = true, example = "TODAY_DELIVERY, DAWN_DELIVERY") @RequestParam String deliveryType,
        @ApiParam(value = "배송운영일 YYYY-MM-DD", required = true, example = "2021-08-02") @RequestParam String shipDt,
        @ApiParam(value = "slotId", required = true, example = "2301") @RequestParam(name="slotId") String slotId,
        @ApiParam(value = "우편번호", required = true) @RequestParam String zipcode
    )
    {
        log.info("[ELEVEN_SLOT_CHECK] storeId:{} deliveryType:{} shipDt:{} slotId:{} zipcode:{}", storeId, deliveryType, shipDt, slotId, zipcode);
        MarketSlotResponse marketSlotResponse = marketService.getMarketSlotCapa(MARKET_TYPE_ELEVEN, storeId, deliveryType, shipDt, slotId, zipcode);
        return ResourceConverter.toResponseObject(marketSlotResponse);
    }

    @PostMapping("/naver/store/checkItemStock")
    @ApiOperation(value = "[NAVER] 실시간 상품 재고 조회")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = NaverProductStockResponse.class)
        , @ApiResponse(code = 404, message = "Not Found")
        , @ApiResponse(code = 405, message = "Method Not Allowed")
        , @ApiResponse(code = 422, message = "Unprocessable Entity")
        , @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public NaverProductStockResponse checkStoreItemStockNaver(@RequestBody NaverProductStock naverProductStock) {
        return marketService.getNaverItemStock(naverProductStock);
    }

    @PostMapping("/eleven/store/checkItemStock")
    @ApiOperation(value = "[11번가] 실시간 상품 재고 조회")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = MarketStockResponse.class)
        , @ApiResponse(code = 404, message = "Not Found")
        , @ApiResponse(code = 405, message = "Method Not Allowed")
        , @ApiResponse(code = 422, message = "Unprocessable Entity")
        , @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseObject<MarketStockResponse> checkStoreItemStockEleven(@RequestBody MarketStockSelect marketStockSelect) {
        return ResourceConverter.toResponseObject(marketService.getMarketItemStock(marketStockSelect, MARKET_TYPE_ELEVEN));
    }

}
