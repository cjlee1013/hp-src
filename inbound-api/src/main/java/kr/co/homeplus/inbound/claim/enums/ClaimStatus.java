package kr.co.homeplus.inbound.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimStatus {

        C1("C1", "REQUEST")
    ,   C2("C2", "CONFIRM")
    ,   C3("C3", "COMPLETE")
    ,   C4("C4", "WITHDRAW")
    ,   C8("C8", "PENDING")
    ,   C9("C9", "REJECT")
    ,   P4("P4", "FAIL")
    ,   REQUEST("C1", "REQUEST")
    ,   CONFIRM("C2", "CONFIRM")
    ,   COMPLETE("C3", "COMPLETE")
    ,   WITHDRAW("C4", "WITHDRAW")
    ,   PENDING("C8", "PENDING")
    ,   REJECT("C9", "REJECT")
    ,   FAIL("P4", "FAIL")
    ,   ALL("ALL","ALL")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;


}
