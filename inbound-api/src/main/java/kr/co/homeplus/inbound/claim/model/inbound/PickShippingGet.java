package kr.co.homeplus.inbound.claim.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.inbound.claim.enums.ClaimPickStatus;
import kr.co.homeplus.inbound.claim.enums.ClaimShipType;
import kr.co.homeplus.inbound.claim.model.partner.ClaimPickShippingPartnerDto;
import kr.co.homeplus.inbound.shipping.enums.ShipMethod;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("클레임 수거 응답")
public class PickShippingGet {
    @ApiModelProperty(value = "수거지-이름", position = 2, example = "홍길동")
    public String pickReceiverName;

    @ApiModelProperty(value = "수거지-휴대폰", position = 3, example = "010-1111-1111")
    public String pickMobileNo;

    @ApiModelProperty(value = "수거지 우편번호", position = 4, example = "07297")
    public String pickZipcode;

    @ApiModelProperty(value = "수거지 기본주소", position = 5, example = "서울특별시 금천구 독산동 291-7")
    public String pickAddress;

    @ApiModelProperty(value = "수거지 샹세주소", position = 6, example = "홈플러스 금천점 1층")
    public String pickAddressDetail;

    @ApiModelProperty(value = "수거배송수단 (DELIVERY:택배배송 POST:우편배송  DIRECT:업체직배송 NOSHIP:배송없음)", position = 7, example = "DELIVERY")
    public String pickShipType;

    @ApiModelProperty(value = "수거택배사", position = 8, example = "002")
    public String pickDeliveryCode;

    @ApiModelProperty(value = "수거송장번호", position = 10, example = "123123123")
    public String pickInvoiceNo;

    @ApiModelProperty(value = "수거접수타입", position = 11, example = "일반접수")
    public String pickRegisterType;

    @ApiModelProperty(value = "수거메모", position = 12, example = "우편수거 메모입니다.")
    public String pickMemo;

    @ApiModelProperty(value="수거상태", position = 13, example = "REQUEST")
    public String pickStatus;

    @ApiModelProperty(value="수거일자", position = 15, example = "2020-12-29 00:00:00")
    public String shipDate;

    @ApiModelProperty(value="수거 예약일(업체직배송인 경우 수거예정일 최초등록일)", position = 19, example = "2020-12-29 00:00:00")
    public String pickConfirmDate;

    @ApiModelProperty(value="수거 송장등록일(업체직배송의 경우 수거예정일)", position = 20, example = "2020-12-29 00:00:00")
    public String pickScheduleDate;

    @ApiModelProperty(value="수거 시작일", position = 21, example = "2020-12-29 00:00:00")
    public String pickShippingDate;

    @ApiModelProperty(value="수거 완료일", position = 22, example = "2020-12-29 00:00:00")
    public String pickCompleteDate;

    @ApiModelProperty(value="수거 지번주소", position = 24, example = "서울특별시 금천구 독산동 291-7 홈플러스 금천점")
    public String pickBaseAddress;

    @ApiModelProperty(value="수거 장소", position = 26, example = "수거장소 상세")
    public String placeAddressPick;

    @ApiModelProperty(value="수거 시작시간", position = 27, example = "10:00")
    public String startTime;

    @ApiModelProperty(value="수거 종료시간", position = 28, example = "12:00")
    public String endTime;

    @ApiModelProperty(value="수거방법", position = 29, example = "DELIVERY")
    public String shipMethod;

    public PickShippingGet(ClaimPickShippingPartnerDto claimPickShippingPartnerDto) {
        this.pickReceiverName = claimPickShippingPartnerDto.getPickReceiverNm();
        this.pickMobileNo = claimPickShippingPartnerDto.getPickMobileNo();
        this.pickZipcode = claimPickShippingPartnerDto.getPickZipcode();
        this.pickAddress = claimPickShippingPartnerDto.getPickAddr();
        this.pickAddressDetail = claimPickShippingPartnerDto.getPickAddrDetail();
        String pickShipType = claimPickShippingPartnerDto.getPickShipTypeCode();
        if (pickShipType != null) {
            this.pickShipType = ClaimShipType.valueOf(pickShipType).getDescription();
        }
        this.pickDeliveryCode = claimPickShippingPartnerDto.getPickDlvCd();
        this.pickInvoiceNo = claimPickShippingPartnerDto.getPickInvoiceNo();
        this.pickRegisterType = claimPickShippingPartnerDto.getPickRegisterType();
        this.pickMemo = claimPickShippingPartnerDto.getPickMemo();
        String pickStatus = claimPickShippingPartnerDto.getPickStatus();
        if (pickStatus != null) {
            this.pickStatus = ClaimPickStatus.valueOf(pickStatus).getDescription();
        }
        this.shipDate = claimPickShippingPartnerDto.getShipDt();
        this.pickConfirmDate = claimPickShippingPartnerDto.getPickP0Dt();
        this.pickScheduleDate = claimPickShippingPartnerDto.getPickP1Dt();
        this.pickShippingDate = claimPickShippingPartnerDto.getPickP2Dt();
        this.pickCompleteDate = claimPickShippingPartnerDto.getPickP3Dt();
        this.pickBaseAddress = claimPickShippingPartnerDto.getPickBaseAddr();
        this.placeAddressPick = claimPickShippingPartnerDto.getPlaceAddrExtnd();
        this.startTime = claimPickShippingPartnerDto.getStartTime();
        this.endTime = claimPickShippingPartnerDto.getEndTime();
        String shipMethod = claimPickShippingPartnerDto.getShipMethod();
        if (shipMethod != null) {
            this.shipMethod = ShipMethod.valueOf(shipMethod).getDescription();
        }
    }
}
