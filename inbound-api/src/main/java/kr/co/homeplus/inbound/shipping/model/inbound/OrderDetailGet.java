package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.inbound.shipping.model.partner.OrderDetailGetPartnerDto;
import kr.co.homeplus.inbound.shipping.model.partner.OrderDetailItemPartnerDto;
import lombok.Data;

@Data
@ApiModel("주문정보 상세")
public class OrderDetailGet {
    @ApiModelProperty(notes = "주문기본정보")
    private OrderDetailBasicGet orderDetailBase;

    @ApiModelProperty(notes = "주문상품정보")
    private List<OrderDetailItemGet> orderDetailItemList;

    @ApiModelProperty(notes = "배송정보")
    private OrderDetailShipGet orderDetailShip;

    public OrderDetailGet(OrderDetailGetPartnerDto orderDetailGetPartnerDto) {
        orderDetailBase = new OrderDetailBasicGet(orderDetailGetPartnerDto.getOrderDetailBase());
        orderDetailItemList = new ArrayList<>();
        for (OrderDetailItemPartnerDto orderDetailItemPartnerDto : orderDetailGetPartnerDto.getOrderDetailItemList()) {
            orderDetailItemList.add(new OrderDetailItemGet(orderDetailItemPartnerDto));
        }
        orderDetailShip = new OrderDetailShipGet(orderDetailGetPartnerDto.getOrderDetailShip());
    }
}
