package kr.co.homeplus.inbound.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value="마켓 상품 재고 조회")
@Getter
@Setter
public class MarketStockSelect {

    @ApiModelProperty(value = "점포Id", position = 1)
    private String storeId;

    @ApiModelProperty(value = "배송유형", example = "TODAY_DELIVERY", position = 3)
    private String shipType;

    @ApiModelProperty(value = "배송일자(YYYY-MM-DD", position = 4)
    private String shipDt;

    @ApiModelProperty(value = "slotId", position = 5)
    private String slotId;

    @ApiModelProperty(value = "우편번호", position = 6)
    private String zipcode;

    @ApiModelProperty(value = "상품리스트", position = 7)
    private List<MarketOrderItem> marketOrderItemList;
}
