package kr.co.homeplus.inbound.itemRelation.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("연관상품 관리")
public class ItemRelationInboundSetDto {

    @ApiModelProperty(value = "연관상품번호(그룹번호 : 등록 시 미입력 | 수정 시 입력 )", position = 5)
    private Long relationNo;

    @ApiModelProperty(value = "연관상품명(50자이내)", required = true, position = 10)
    @NotEmpty(message = "연관상품명")
    @Size(max=50, message = "연관상품")
    private String relationName;

    @ApiModelProperty(value = "노출여부", required = true, position = 15)
    @NotEmpty(message = "노출여부")
    @Pattern(regexp = "(Y|N)", message="노출여부(노출:Y,노출안함:N)")
    private String displayYnName;

    @ApiModelProperty(value = "연관노출타입-PC (LIST1 : 리스트형1단, LIST2 : 리스트형2단 | CARD2 : 카드형2단, CARD3 : 카드형3단, CARD4 : 카드형4단)", required = true, position = 20)
    @NotEmpty(message = "연관노출타입-PC")
    @Pattern(regexp = "(LIST1|LIST2|CARD2|CARD3|CARD4)", message = "연관노출타입-PC")
    private String displayPcType;

    @ApiModelProperty(value = "연관노출타입-모바일 ((LIST1 : 리스트형1단 | CARD2 : 카드형2단)", required = true, position = 25)
    @Pattern(regexp = "(LIST1|CARD2)", message = "연관노출타입-모바일")
    @NotEmpty(message = "연관노출타입-모바일")
    private String displayMobileType;

    @ApiModelProperty(value = "소분류 카테고리", required = true, position = 30)
    private String smallCategoryCode;

    @ApiModelProperty(value = "인트로이미지URL(250자 이내)", position = 35)
    private String introImageUrl;

    @ApiModelProperty(value = "사용여부(사용:Y,미사용:N)", required = true, position = 50)
    @Pattern(regexp = "(Y|N)", message="사용여부")
    private String useYnName;

    @ApiModelProperty(value = "연관상품 상품 리스트(상품번호 50개이하 등록 가능)", required = true, position = 55)
    List<String> relationItemList;

}
