package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.Data;

@Data
@ApiModel("상품상세 조회")
public class ItemParamInboundDto {

    @ApiModelProperty(value = "상품번호", required = true)
    private String itemNo;

    @ApiModelProperty(value = "상품명", hidden = true)
    private String itemNm;

    @ApiModelProperty(value = "점포ID", required = true)
    private Integer storeId;

    @ApiModelProperty(value = "점포ID", required = true, hidden = true)
    private Integer originStoreId;

    @ApiModelProperty(value = "점포구분 - HYPER,CLUB,EXP,DS,AURORA", required = true)
    @NotEmpty(message = "점포구분")
    @Pattern(regexp = "((?i)HYPER|CLUB|EXP|DS|AURORA)", message="거래유형")
    private String storeType;

    @ApiModelProperty(value = "점포구분 - HYPER,CLUB,EXP,DS,AURORA", hidden = true)
    private String originStoreType;

    @ApiModelProperty(value = "점포종류", hidden = true)
    private String storeKind;
}
