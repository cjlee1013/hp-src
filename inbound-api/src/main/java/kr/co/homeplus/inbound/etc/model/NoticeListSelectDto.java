package kr.co.homeplus.inbound.etc.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(value = "공지사항 리스트")
public class NoticeListSelectDto {

    @ApiModelProperty(value = "공지사항 구분")
    private String noticKind;

    @ApiModelProperty(value = "공지사항 제목")
    private String title;

    @ApiModelProperty(value = "공지사항 내용")
    private String contents;

    @ApiModelProperty(value = "등록일시")
    private String noticeDate;

    @ApiModelProperty(value = "공지사항 첨부파일")
    private List<NoticeFileListGetDto> noticefileList;

}
