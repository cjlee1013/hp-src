package kr.co.homeplus.inbound.core.exception.handler;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class NotFoundException extends CommonException {
    public NotFoundException(String version, String msg) {
        super(version, msg);
    }
}
