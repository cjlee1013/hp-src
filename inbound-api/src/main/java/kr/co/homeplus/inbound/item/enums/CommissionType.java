package kr.co.homeplus.inbound.item.enums;

import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.collect.ImmutableBiMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum CommissionType {

        R("R", "정률(%)")
    ,   A("A", "정액(원)")
    ,   N("N", "없음");

    @Getter
    private final String type;

    @Getter
    private final String description;

    @Getter
    private static final ImmutableBiMap<CommissionType, String> biMap;

    static {
        BiMap<CommissionType, String> map = EnumHashBiMap.create(CommissionType.class);

        for (CommissionType policy : values()) {
            map.put(policy, policy.name());
        }

        biMap = ImmutableBiMap.copyOf(map);
    }

    public static CommissionType valueFor(final String type) {
        final CommissionType policy =  biMap.inverse().get(type);

        return policy;
    }

}
