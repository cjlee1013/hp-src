package kr.co.homeplus.inbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel("상품 제휴사,마켓연동 관련 정보")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ItemProviderSetDto {

	@ApiModelProperty(value = "업체아이디,제휴채널 ID")
	private String providerId;

	@ApiModelProperty(value = "업체아이디, 제휴채널 명")
	private String providerNm;

	@ApiModelProperty(value = "단독:S, 기타:E")
	private String providerType;

	@ApiModelProperty(value = "파트너 구분(AFFI:제휴사, COOP:마켓연동)-공통코드:partner_type")
	private String partnerType;

	@NotNull(message = "사용여부 (Y, N)")
	private String useYn;
}
