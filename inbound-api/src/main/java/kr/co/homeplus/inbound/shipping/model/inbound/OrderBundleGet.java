package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.inbound.shipping.model.partner.OrderShipManageListGetDto;
import lombok.Data;

@Data
@ApiModel("주문,배송 리스트 응답")
public class OrderBundleGet {
    @ApiModelProperty(notes = "배송번호", example = "3000000001", position = 1)
    private String bundleNo;

    @ApiModelProperty(notes = "주문일", example = "2020-10-14 13:44:07", position = 2)
    private String orderDate;

    @ApiModelProperty(notes = "결제일", example = "2021-01-01 08:30:00", position = 3)
    private String paymentDate;

    @ApiModelProperty(notes = "주문번호", example = "3000000001", position = 4)
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "구매자", example = "김홈플", position = 5)
    private String buyerName;

    @ApiModelProperty(notes = "구매자 연락처", example = "010-1234-5678", position = 6)
    private String buyerMobileNo;

    @ApiModelProperty(notes = "수령인", example = "이홈플", position = 7)
    private String receiverName;

    @ApiModelProperty(notes = "수령인 연락처", example = "010-9876-5432", position = 8)
    private String receiverMobileNo;

    @ApiModelProperty(notes = "우편번호", example = "07567", position = 9)
    private String zipcode;

    @ApiModelProperty(notes = "도로명기본주소", example = "서울시 강서구 화곡로 398", position = 10)
    private String baseAddress;

    @ApiModelProperty(notes = "도로명상세주소", example = "홈플러스 강서점", position = 11)
    private String detailAddress;

    @ApiModelProperty(notes = "개인통관고유번호", example = "P123456789012", position = 12)
    private String personalOverseaNo;

    @ApiModelProperty(notes = "배송비지급", example = "선불", position = 13)
    private String shipPriceType;

    @ApiModelProperty(notes = "배송비", example = "2500", position = 14)
    private String shipPrice;

    @ApiModelProperty(notes = "주문내역", position = 15)
    private List<OrderItemGet> orderList;

    public OrderBundleGet(OrderShipManageListGetDto orderShipManageListGetDto) {
        this.bundleNo = orderShipManageListGetDto.getBundleNo();
        this.orderDate = orderShipManageListGetDto.getOrderDt();
        this.paymentDate = orderShipManageListGetDto.getPaymentFshDt();
        this.purchaseOrderNo = orderShipManageListGetDto.getPurchaseOrderNo();
        this.buyerName = orderShipManageListGetDto.getBuyerNm();
        this.buyerMobileNo = orderShipManageListGetDto.getBuyerMobileNo();
        this.receiverName = orderShipManageListGetDto.getReceiverNm();
        this.receiverMobileNo = orderShipManageListGetDto.getShipMobileNo();
        this.zipcode = orderShipManageListGetDto.getZipcode();
        this.baseAddress = orderShipManageListGetDto.getRoadBaseAddr();
        this.detailAddress = orderShipManageListGetDto.getRoadDetailAddr();
        this.personalOverseaNo = orderShipManageListGetDto.getPersonalOverseaNo();
        this.shipPriceType = orderShipManageListGetDto.getShipPriceType();
        this.shipPrice = orderShipManageListGetDto.getShipPrice();
        orderList = new ArrayList<>();
        addOrderList(orderShipManageListGetDto);
    }

    public void addOrderList(OrderShipManageListGetDto orderShipManageListGetDto) {
        orderList.add(new OrderItemGet(orderShipManageListGetDto));
    }
}