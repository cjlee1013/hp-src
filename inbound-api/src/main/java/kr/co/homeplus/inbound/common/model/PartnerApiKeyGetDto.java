package kr.co.homeplus.inbound.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("외부연동 > 업체등록 Get Entry")
public class PartnerApiKeyGetDto {

    @ApiModelProperty(value = "파트너ID", required = true)
    private String partnerId;

    @ApiModelProperty(value = "외부연동 KEY", required = false)
    private String encKey;

    private String data;
}
