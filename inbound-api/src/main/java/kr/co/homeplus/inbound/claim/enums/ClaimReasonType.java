package kr.co.homeplus.inbound.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimReasonType {

        D001("CB", "D001") //상품 발송처리
    ,   R009("PB", "R009") //기타(구매자 책임 사유)
    ,   R010("PS", "R010") //기타(판매자 책임 사유)
    ,   X005("XB", "X005") //기타(구매자 책임 사유)
    ,   X006("XS", "X006") //기타(판매자 책임 사유)
    ,   CB("CB", "D001") //상품 발송처리
    ,   PB("PB", "R009") //기타(구매자 책임 사유)
    ,   PS("PS", "R010") //기타(판매자 책임 사유)
    ,   XB("XB", "X005") //기타(구매자 책임 사유)
    ,   XS("XS", "X006") //기타(판매자 책임 사유)
    ;
    @Getter
    private final String type;

    @Getter
    private final String description;
}
