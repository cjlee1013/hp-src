package kr.co.homeplus.inbound.claim.model.partner;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.inbound.claim.enums.ClaimShipType;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimDeliverySet;
import kr.co.homeplus.inbound.claim.model.inbound.ExchangeCompleteSet;
import kr.co.homeplus.inbound.claim.model.inbound.ExchangeShippingSet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "반품교환 상태 수정.")
public class ClaimExchShippingModifyPartnerDto {

    @ApiModelProperty(value = "클레임 번들 번호")
    public long claimBundleNo;

    @ApiModelProperty(value = "반품교환 수정 타입 [주소:ADDR, 상태:STATUS, 배송타입:TYPE, 전부:ALL]")
    public String modifyType;

    @ApiModelProperty(value = "교환지-이름")
    public String exchReceiverNm;

    @ApiModelProperty(value = "교환지-휴대폰")
    public String exchMobileNo;

    @ApiModelProperty(value = "교환지 우편번호")
    public String exchZipcode;

    @ApiModelProperty(value = "교환지 기본주소")
    public String exchRoadBaseAddr;

    @ApiModelProperty(value = "교환지 샹세주소")
    public String exchRoadAddrDetail;

    @ApiModelProperty(value = "교환지 기본주소")
    public String exchBaseAddr;

    @ApiModelProperty(value = "교환지 샹세주소")
    public String exchDetailAddr;

    @ApiModelProperty(value = "교환배송수단")
    public String exchShipType;

    @ApiModelProperty(value = "교환배송수단코드")
    public String exchShipTypeCode;

    @ApiModelProperty(value = "교환택배사코드")
    public String exchDlvCd;

    @ApiModelProperty(value = "교환택배사명")
    public String exchDlv;

    @ApiModelProperty(value = "교환송장번호")
    public String exchInvoiceNo;

    @ApiModelProperty(value = "교환메모")
    public String exchMemo;

    @ApiModelProperty(value="교환상태")
    public String exchStatus;

    @ApiModelProperty(value="배송타입")
    public String shipType;

    @ApiModelProperty(value="교환일자")
    public String shipDt;

    @ApiModelProperty(value="교환 예약접수자")
    public String exchP0Id;

    @ApiModelProperty(value="교환 예약일(업체직배송인 경우 교환예정일 최초등록일)")
    public String exchP0Dt;

    @ApiModelProperty(value="교환 송장등록일(업체직배송의 경우 교환예정일)")
    public String exchP1Dt;

    @ApiModelProperty(value="교환 시작일")
    public String exchP2Dt;

    @ApiModelProperty(value="교환 완료일")
    public String exchP3Dt;

    @ApiModelProperty(value = "수정ID")
    public String chgId;


    public ClaimExchShippingModifyPartnerDto(ExchangeCompleteSet exchangeCompleteSet){
        this.claimBundleNo = exchangeCompleteSet.getClaimBundleNo();
        this.exchShipType = "";
        this.exchStatus = "D3";
        this.modifyType = "STATUS";
    }


    public ClaimExchShippingModifyPartnerDto(ClaimDeliverySet claimDeliverySet){
        this.claimBundleNo = claimDeliverySet.getClaimBundleNo();
        String exchShipType = claimDeliverySet.getExchangeShipType();
        if (exchShipType != null) {
            this.exchShipType = ClaimShipType.valueOf(exchShipType).getType();
        }
        this.exchDlvCd = claimDeliverySet.getExchangeDeliveryCode();
        this.exchInvoiceNo = claimDeliverySet.getExchangeInvoiceNo();
        this.exchMemo = claimDeliverySet.getExchangeMemo();
        this.exchRoadBaseAddr = "";
        this.exchStatus = "D1";
        this.modifyType = "ALL";
        this.shipDt = claimDeliverySet.getExchangeDate();
        this.shipType = "DS";
    }

    public ClaimExchShippingModifyPartnerDto(ExchangeShippingSet exchangeShippingSet){
        this.claimBundleNo = exchangeShippingSet.getClaimBundleNo();
        this.exchReceiverNm = exchangeShippingSet.getExchangeReceiverName();
        this.exchMobileNo = exchangeShippingSet.getExchangeMobileNo();
        this.exchZipcode = exchangeShippingSet.getExchangeZipcode();
        this.exchRoadBaseAddr = exchangeShippingSet.getExchangeRoadBaseAddress();
        this.exchRoadAddrDetail = exchangeShippingSet.getExchangeRoadAddrDetail();
        this.exchBaseAddr = exchangeShippingSet.getExchangeBaseAddress();
        this.exchDetailAddr = exchangeShippingSet.getExchangeDetailAddress();
        String exchShipType = exchangeShippingSet.getExchangeShipType();
        if (exchShipType != null) {
            this.exchShipType = ClaimShipType.valueOf(exchShipType).getType();
        }
        this.exchDlvCd = exchangeShippingSet.getExchangeDeliveryCode();
        this.exchInvoiceNo = exchangeShippingSet.getExchangeInvoiceNo();
        this.exchMemo = exchangeShippingSet.getExchangeMemo();
        this.exchStatus = "";
        if(exchangeShippingSet.getExchangeShipType().equalsIgnoreCase("NONE")){
            this.modifyType = "ADDR";
        }else{
            this.modifyType = "ALL";
        }
        this.shipDt = exchangeShippingSet.getExchangeDate();
        this.shipType = "DS";
    }

}