package kr.co.homeplus.inbound.item.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("상품 속성정보")
public class ItemPropSetDto {

	@ApiModelProperty(value = "브랜드번호", position = 1)
	private Long   brandNo;

	@ApiModelProperty(value = "제조사번호", position = 2)
	private Long   makerNo;

	@ApiModelProperty(value = "제조일자(YYYY-MM-DD)", position = 3)
	@JsonProperty("makeDate")
	private String makeDt;

	@ApiModelProperty(value = "유효일자(YYYY-MM-DD)", position = 4)
	@JsonProperty("expirationDate")
	private String expDt;

	@ApiModelProperty(value = "상품고시정보 정책코드", required = true, position = 5)
	@JsonProperty("groupNoticeNo")
	private int gnoticeNo;

	@ApiModelProperty(value = "상품고시정보 리스트", position = 6)
	@Valid
	private List<ItemNoticeSetDto> noticeList;

	@ApiModelProperty(value = "제품안전인증 인증대상여부 (Y:인증대상(인증유형 및 인증번호 등록), N:인증대상 아님-기본값)", position = 7)
	@Builder.Default
	private String kcItemIsCertification = "N";

	public void setKcItemIsCertification(String kcItemIsCertification) {
		this.kcItemIsCertification = kcItemIsCertification.toUpperCase();
	}

	@ApiModelProperty(value = "제품안전인증 인증정보 - 인증유형 코드 (IT_SAFETY_CT:안전인증, IT_SAFETY_CK:안전확인신고, IT_SPL:공급자적합성확인, IT_D:상품상세 설명 참고)", position = 8)
	private String kcItemCertificationType;

	public void setKcItemCertificationType(String kcItemCertificationType) {
		this.kcItemCertificationType = kcItemCertificationType.toUpperCase();
	}

	@ApiModelProperty(value = "제품안전인증 인증번호 (최대 50자)", position = 9)
	private String kcItemCertificationNo;

	@ApiModelProperty(value = "어린이제품 인증대상여부 (Y:인증대상(인증유형 및 인증번호 등록), N:인증대상 아님-기본값)", position = 10)
	@Builder.Default
	private String kcKidIsCertification = "N";

	public void setKcKidIsCertification(String kcKidIsCertification) {
		this.kcKidIsCertification = kcKidIsCertification.toUpperCase();
	}

	@ApiModelProperty(value = "어린이제품 인증정보 - 인증유형 코드 (KD_SAFETY_CT:안전인증, KD_SAFETY_CK:안전확인신고, KD_SPL:공급자적합성확인, KD_D:상품상세 설명 참고)", position = 11)
	private String kcKidCertificationType;

	public void setKcKidCertificationType(String kcKidCertificationType) {
		this.kcKidCertificationType = kcKidCertificationType.toUpperCase();
	}

	@ApiModelProperty(value = "어린이제품 인증번호 (최대 50자)", position = 12)
	private String kcKidCertificationNo;

	@ApiModelProperty(value = "전파인증 인증대상여부 (Y:인증대상(인증유형 및 인증번호 등록), N:인증대상 아님-기본값)", position = 13)
	@Builder.Default
	private String kcElectricIsCertification = "N";

	public void setKcElectricIsCertification(String kcElectricIsCertification) {
		this.kcElectricIsCertification = kcElectricIsCertification.toUpperCase();
	}

	@ApiModelProperty(value = "전파인증 인증정보 (최대 10개) - 인증유형 코드 (ER_SPL:전자파적합성, ER_D:상품상세 설명 참고)", position = 14)
	private List<ItemCertSetDto> kcElectricCertificationList;

	@ApiModelProperty(value = "기타인증 인증대상여부 (Y:인증대상(인증유형 및 인증번호 등록), N:인증대상 아님-기본값)", position = 15)
	@Builder.Default
	private String kcEtcIsCertification = "N";

	public void setKcEtcIsCertification(String kcEtcIsCertification) {
		this.kcEtcIsCertification = kcEtcIsCertification.toUpperCase();
	}

	@ApiModelProperty(value = "기타인증 인증정보 (최대 10개) - 인증유형 코드 (ET_LF_CH_ITEM:안전확인대상 생활화학제품, ET_MEDI:의료기기 ET_D:상품상세 설명 참고)", position = 16)
	@Valid
	private List<ItemCertSetDto> kcEtcCertificationList;

}