package kr.co.homeplus.inbound.etc.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@ApiModel("QNA 답변 등록")
public class CsQnaReplySetParamDto {


    @ApiModelProperty(value = "Qna 일련번호")
    @NotEmpty(message = "Qna 일련번호")
    private String qnaNo;

    @ApiModelProperty(value = "답변내용")
    @NotEmpty(message = "답변내용")
    private String qnaReply;

    @ApiModelProperty(value = "파트너 아이디" , hidden = true)
    private String partnerId;

}
