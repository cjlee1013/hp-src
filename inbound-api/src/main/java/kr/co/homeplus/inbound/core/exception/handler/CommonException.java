package kr.co.homeplus.inbound.core.exception.handler;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class CommonException extends Exception {
    private String version;
    private String msg;

    public CommonException(String version, String msg) {
        this.version = version;
        this.msg = msg;
    }
}
