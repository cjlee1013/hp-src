package kr.co.homeplus.inbound.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kr.co.homeplus.inbound.constants.PatternConstants;
import org.apache.commons.lang3.StringUtils;

/**
 * 유효성검사 Util
 */
public class ValidUtil {

    public static boolean isPattern(String format, String s, String... types) {
        if(StringUtils.isBlank(s)) return true;

        StringBuffer regx = new StringBuffer();
        if (StringUtils.isNotBlank(s) && StringUtils.isNoneEmpty(types)) {
            for (String p : types) {
                regx.append(PatternConstants.getConstantsFormat(p));

                if(PatternConstants.NOT_SPACE.equals(p)){
                    format = format.replace("\\s", PatternConstants.getConstantsFormat(p));
                }
            }
        }
        return Pattern.matches(String.format(format, regx.toString()), s);
    }
    public static boolean isAllowInput(String s, String... types) {
        return ValidUtil.isPattern("^[\\s%s]*$", s, types);
    }

    // 허용하지 않는 문자 여부 (존재: false)
    public static boolean isNotAllowInput(String s, String... types) {
        return ValidUtil.isPattern("[^%s]+", s, types);
    }

    // 줄바꿈 존재여부 (존재 : true)
    public static boolean isEnterStr(final String str) {
        boolean isBool = false;

        if (!StringUtils.isBlank(str)) {
            final Pattern p = Pattern.compile(PatternConstants.ENTER_STR);
            final Matcher m = p.matcher(str);

            isBool = m.find();
        }

        return isBool;
    }

    // 줄바꿈 허용 - 빈거 있는지  (있음 : true)
    public static boolean isEnterNotEmptyStr(final String str) {
        boolean isBool = false;

        if (StringUtils.isNotBlank(str)) {
            final String[] strs = StringUtil.getExcelEnterSplit(str);

            for (final String s : strs) {
                if (StringUtils.isBlank(s)) {
                    isBool = true;
                }
            }
        }

        return isBool;
    }
}
