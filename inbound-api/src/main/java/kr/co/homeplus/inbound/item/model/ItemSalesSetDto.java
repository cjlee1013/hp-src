package kr.co.homeplus.inbound.item.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("FRONT 주문발생 시 > [ESCROW-API] > 상품 판매된수량 연동 Set Entry")
public class ItemSalesSetDto {
    @ApiModelProperty(value = "상품번호", required = true, position = 1)
    @NotNull(message = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "점포유형", required = true, position = 2)
    private String storeType;

    @ApiModelProperty(value = "점포ID", required = true, position = 3)
    private int storeId;

    @ApiModelProperty(value = "상품 판매된수량", required = true, position = 4)
    @NotNull(message = "상품 남은수량")
    @Min(value = 0, message = "상품 판매된수량")
    @Max(value = Long.MAX_VALUE, message = "상품 판매된수량")
    private long remainCnt;

    @ApiModelProperty(value = "상품 총 판매된수량", required = true, position = 5)
    @NotNull(message = "상품 총 판매된수량")
    @Min(value = 0, message = "상품 총 판매된수량")
    @Max(value = Long.MAX_VALUE, message = "상품 총 판매된수량")
    private Long salesCnt;
}
