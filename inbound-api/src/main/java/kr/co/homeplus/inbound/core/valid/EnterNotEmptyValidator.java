package kr.co.homeplus.inbound.core.valid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import kr.co.homeplus.inbound.utils.ValidUtil;

public class EnterNotEmptyValidator implements ConstraintValidator<EnterNotEmpty, String> {
    @Override
    public void initialize(EnterNotEmpty constraintAnnotation) {}

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return !ValidUtil.isEnterNotEmptyStr(s);
    }
}
