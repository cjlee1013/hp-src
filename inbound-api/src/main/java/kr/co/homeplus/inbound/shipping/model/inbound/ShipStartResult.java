package kr.co.homeplus.inbound.shipping.model.inbound;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.beans.ConstructorProperties;
import kr.co.homeplus.inbound.shipping.enums.ShipMethod;
import lombok.Data;

@Data
@ApiModel("발송처리 요청 결과")
public class ShipStartResult {
  @ApiModelProperty(notes = "배송번호", example = "3000000001", position = 1)
  private String bundleNo;

  @ApiModelProperty(notes = "배송방법(DELIVERY:택배배송, DIRECT:직접배송, PICK:방문수령, POST:우편배송, QUICK:퀵)", example = "DELIVERY", position = 2)
  private String shipMethod;

  @ApiModelProperty(notes = "택배사코드(002:CJ택배, 004:우체국택배 그외 별첨)", example = "002", position = 3)
  private String deliveryCode;

  @ApiModelProperty(notes = "송장번호", example = "1234567890", position = 4)
  private String invoiceNo;

  @ApiModelProperty(notes = "배송예정일", example = "2021-01-20", position = 5)
  private String scheduleShipDate;

  @ApiModelProperty(notes = "실패메세지", example = "상품 주문번호를 확인하세요[상품주문번호:3000000001]", position = 6)
  private String failMsg;

  public ShipStartResult(ShipStartSet shipStartSet,String failMsg) {
    this.bundleNo = shipStartSet.getBundleNo();
    this.shipMethod = shipStartSet.getShipMethod();
    this.deliveryCode = shipStartSet.getDeliveryCode();
    this.invoiceNo = shipStartSet.getInvoiceNo();
    this.scheduleShipDate = shipStartSet.getScheduleShipDate();
    this.failMsg = failMsg;
  }
}
