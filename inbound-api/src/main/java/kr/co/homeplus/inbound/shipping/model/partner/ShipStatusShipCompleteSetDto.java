package kr.co.homeplus.inbound.shipping.model.partner;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.inbound.shipping.model.inbound.ShipCompleteSet;
import lombok.Data;

@Data
@ApiModel(description = "배송완료 요청 DTO")
public class ShipStatusShipCompleteSetDto {
    @ApiModelProperty(notes = "배송번호 리스트")
    private List<String> bundleNoList;

    @ApiModelProperty(notes = "수정자")
    private String chgId;

    public ShipStatusShipCompleteSetDto(ShipCompleteSet shipCompleteSet, String chgId) {
        this.bundleNoList = new ArrayList<>();
        for (String bundleNo : shipCompleteSet.getBundleNoList()) {
            this.bundleNoList.add(bundleNo);
        }
        this.chgId = chgId;
    }
}
