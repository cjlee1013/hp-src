package kr.co.homeplus.inbound.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimType {

        C("C", "CANCEL")
    ,   R("R", "PICK")
    ,   X("X", "EXCHANGE")
    ,   CANCEL("C", "CANCEL")
    ,   PICK("R", "PICK")
    ,   EXCHANGE("X", "EXCHANGE")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;


}
