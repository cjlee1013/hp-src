package kr.co.homeplus.inbound.etc.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import kr.co.homeplus.inbound.etc.model.CsQnaListSelectDto;
import kr.co.homeplus.inbound.etc.model.CsQnaReplySetParamDto;
import kr.co.homeplus.inbound.etc.service.CsQnaService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "QNA 등록 관리")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found")
    ,   @ApiResponse(code = 405, message = "Method Not Allowed")
    ,   @ApiResponse(code = 422, message = "Unprocessable Entity")
    ,   @ApiResponse(code = 500, message = "Internal Server Error")
})
@RequestMapping("/qna")
@RestController
@RequiredArgsConstructor
public class CsQnaController {
    final private CsQnaService csQnaService;


    @ApiOperation(value = "QNA 리스트", response = CsQnaListSelectDto.class)
    @RequestMapping(value = "/getQnaList", method = {RequestMethod.GET})
    public ResponseObject getQnaList(
        @ApiParam(value = "apiKey", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @RequestParam(name ="searchDate", required = false) @ApiParam(name = "searchDate", value = "조회기준일") String searchDate
    ) throws Exception {
        return ResourceConverter.toResponseObject(csQnaService.getQnaList(apiKey, searchDate));
    }


    @ApiOperation(value = "QNA 답변 등록/수정", response = ResponseObject.class)
    @RequestMapping(value = "/setQnaReply", method = RequestMethod.POST)
    public ResponseObject setQnaReply(
        @ApiParam(value = "apiKey", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @Valid @RequestBody CsQnaReplySetParamDto csQnaReplySetParamDto
    ) throws Exception {
        return csQnaService.setQnaReply(apiKey, csQnaReplySetParamDto);
    }

}
