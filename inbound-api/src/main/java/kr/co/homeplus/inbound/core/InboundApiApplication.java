package kr.co.homeplus.inbound.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"kr.co.homeplus.inbound", "kr.co.homeplus.plus"})
public class InboundApiApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(InboundApiApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        setRegisterErrorPageFilter(false);
        return application.sources(InboundApiApplication.class);
    }
}
