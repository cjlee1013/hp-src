package kr.co.homeplus.inbound.claim.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimApproveSet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimDeliverySet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimDetailGet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimDetailListGet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimListGet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimListSet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimPendingSet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimRegisterSet;
import kr.co.homeplus.inbound.claim.model.inbound.ClaimRejectSet;
import kr.co.homeplus.inbound.claim.model.inbound.ExchangeCompleteSet;
import kr.co.homeplus.inbound.claim.model.inbound.ExchangeShippingSet;
import kr.co.homeplus.inbound.claim.model.inbound.PickCompleteSet;
import kr.co.homeplus.inbound.claim.model.inbound.PickRequestSet;
import kr.co.homeplus.inbound.claim.model.inbound.PickShippingSet;
import kr.co.homeplus.inbound.claim.service.ClaimPopService;
import kr.co.homeplus.inbound.claim.service.ClaimService;
import kr.co.homeplus.inbound.common.service.CommonValidService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "취소/교환/반품")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found")
    ,@ApiResponse(code = 405, message = "Method Not Allowed")
    ,@ApiResponse(code = 422, message = "Unprocessable Entity")
    ,@ApiResponse(code = 500, message = "Internal Server Error")
})
@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/claim")
public class ClaimController {

    @Autowired
    private final ClaimService claimService;
    @Autowired
    private final ClaimPopService claimPopService;
    @Autowired
    private final CommonValidService commonValidService;

    @ResponseBody
    @PostMapping("/getClaimList")
    @ApiOperation(value = "클레임 리스트 조회")
    public ResponseObject<List<ClaimListGet>> getCancelClaimList(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "ClaimListSet", value = "클레임 리스트 요청", required = true) @RequestBody ClaimListSet claimListSet) throws Exception {
        return ResourceConverter.toResponseObject(claimService.getClaimList(claimListSet,commonValidService.validApiKeyHandler(apiKey)));
    }

    @ResponseBody
    @GetMapping("getClaimDetailList")
    @ApiOperation(value = "클레임 정보 조회")
    public ResponseObject<List<ClaimDetailListGet>> getClaimDetailList(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "purchaseOrderNo", value = "주문번호", required = true, example = "3000004971") @RequestParam(name="purchaseOrderNo")long purchaseOrderNo,
        @ApiParam(name = "bundleNo", value = "배송번호", required = true , example = "3000000004") @RequestParam(name="bundleNo")long bundleNo) throws Exception{
        return ResourceConverter.toResponseObject(claimService.getClaimDetailList(purchaseOrderNo, bundleNo,commonValidService.validApiKeyHandler(apiKey)));
    }

    @ResponseBody
    @GetMapping("getClaimDetail")
    @ApiOperation(value = "클레임 상세 조회")
    public ResponseObject<ClaimDetailGet> getClaimDetail(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "claimBundleNo", value = "클레임번호", required = true) @RequestParam(name="claimBundleNo")long claimBundleNo,
        @ApiParam(name = "claimType", value = "클레임 종류(취소:CANCEL,반품:PICK,교환:EXCHANGE)", required = true) @RequestParam(name="claimType")String claimType) throws Exception{
        return ResourceConverter.toResponseObject(claimService.getClaimDetail(claimBundleNo, claimType,commonValidService.validApiKeyHandler(apiKey)));
    }

    // 클레임 승인
    @ResponseBody
    @PostMapping("/setClaimApprove")
    @ApiOperation(value = "클레임 승인")
    public ResponseObject<String> requestClaimApprove(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "ClaimApproveSet", value = "클레임 승인 요청", required = true) @RequestBody ClaimApproveSet claimApproveSet) throws Exception {
        return claimPopService.requestClaimApprove(claimApproveSet);
    }

    @ResponseBody
    @PostMapping("/setClaimPending.json")
    @ApiOperation(value = "클레임 보류 요청")
    public ResponseObject<String> setClaimPending(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "ClaimPendingSet", value = "클레임 보류 요청", required = true) @RequestBody ClaimPendingSet claimPendingSet) throws Exception {
        return claimPopService.setClaimPending(claimPendingSet);
    }

    @ResponseBody
    @PostMapping("/setClaimReject.json")
    @ApiOperation(value = "클레임 거절 요청")
    public ResponseObject<String> setClaimReject(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "ClaimRejectSet", value = "클레임 거절 요청", required = true) @RequestBody ClaimRejectSet claimRejectSet) throws Exception {
        return claimPopService.setClaimReject(claimRejectSet,commonValidService.validApiKeyHandler(apiKey));
    }

    /**
     * 클레임 수거요청
     * @param pickRequestSet
     */
    @ResponseBody
    @PostMapping("/setPickRequest.json")
    @ApiOperation(value = "클레임 수거 요청")
    public ResponseObject<String> setPickRequest(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "PickRequestSet", value = "클레임 수거 요청", required = true) @RequestBody PickRequestSet pickRequestSet) {
        return claimPopService.setPickRequest(pickRequestSet);
    }

    /**
     * 클레임 수거정보변경
     * @param pickShippingSet
     */
    @ResponseBody
    @PostMapping("/setPickShipping.json")
    @ApiOperation(value = "클레임 수거정보 변경")
    public ResponseObject<String> setPickShipping(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "PickShippingSet", value = "클레임 수거정보 변경", required = true) @RequestBody PickShippingSet pickShippingSet) {
        return claimPopService.setPickShipping(pickShippingSet);
    }

    /**
     * 클레임 수거완료요청
     * @param pickCompleteSet
     */
    @PostMapping("/setPickComplete.json")
    @ResponseBody
    @ApiOperation(value = "클레임 수거완료요청")
    public ResponseObject<Boolean> setPickComplete(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "PickCompleteSet", value = "클레임 수거완료 요청", required = true) @RequestBody PickCompleteSet pickCompleteSet) throws Exception {
        return claimPopService.setPickComplete(pickCompleteSet);
    }

    /**
     * 교환 배송 요청
     * @param claimDeliverySet
     */
    @ResponseBody
    @PostMapping("/setClaimDelivery.json")
    @ApiOperation(value = "교환 배송 요청")
    public ResponseObject<String> setClaimDelivery(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "ClaimDeliverySet", value = "교환 배송 요청", required = true) @RequestBody ClaimDeliverySet claimDeliverySet) {
        return claimPopService.setClaimDelivery(claimDeliverySet);
    }


    /**
     * 클레임 교환배송 정보변경
     * @param exchangeShippingSet
     */
    @ResponseBody
    @PostMapping("/setExchShipping.json")
    @ApiOperation(value = "클레임 교환배송 정보변경")
    public ResponseObject<String> setExchShipping(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "ExchShippingSet", value = "클레임 교환정보 변경", required = true) @RequestBody ExchangeShippingSet exchangeShippingSet) {
        return claimPopService.setExchShipping(exchangeShippingSet);
    }

    /**
     * 클레임 교환배송 완료
     * @param exchangeCompleteSet
     */
    @ResponseBody
    @PostMapping("/setExchComplete.json")
    @ApiOperation(value = "클레임 교환배송 완료")
    public ResponseObject<String> setExchComplete(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "ExchCompleteSet", value = "클레임 교환배송 완료", required = true) @RequestBody ExchangeCompleteSet exchangeCompleteSet) {
        return claimPopService.setExchComplete(exchangeCompleteSet);
    }

    /**
     * 주문 취소
     */
    @ResponseBody
    @PostMapping("/setOrderCancel")
    @ApiOperation(value = "주문 취소")
    public ResponseObject<String> setOrderCancel(
        @ApiParam(value = "API KEY", required = true) @RequestHeader(value = "apiKey") String apiKey,
        @ApiParam(name = "claimRegisterSet", value = "주문취소 요청", required = true) @RequestBody ClaimRegisterSet claimRegisterSet) throws Exception {
        return claimService.claimRegister(claimRegisterSet,commonValidService.validApiKeyHandler(apiKey));
    }
}
