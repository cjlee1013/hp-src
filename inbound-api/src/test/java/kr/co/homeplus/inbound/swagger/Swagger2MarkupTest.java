package kr.co.homeplus.inbound.swagger;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import io.github.swagger2markup.GroupBy;
import io.github.swagger2markup.Language;
import io.github.swagger2markup.OrderBy;
import io.github.swagger2markup.Swagger2MarkupConfig;
import io.github.swagger2markup.Swagger2MarkupConverter;
import io.github.swagger2markup.builder.Swagger2MarkupConfigBuilder;
import io.github.swagger2markup.markup.builder.MarkupLanguage;
import java.io.BufferedWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import kr.co.homeplus.inbound.core.InboundApiApplication;
import kr.co.homeplus.inbound.core.config.swagger.SwaggerConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureRestDocs(outputDir = "build/asciidoc/snippets")
@SpringBootTest(classes = {InboundApiApplication.class, SwaggerConfig.class})
@AutoConfigureMockMvc
public class Swagger2MarkupTest {

    @Autowired
    private MockMvc mockMvc;

    private static final String SWAGGER_OUTPUT_DIR = System.getProperty("io.springfox.staticdocs.outputDir");
    private static final String ASCILDOC_OUTPUT_DIR = "build/asciidoc/";
    private static final String GENERATED_OUTPUT_DIR = ASCILDOC_OUTPUT_DIR + "generated/";


    @Test
    public void createDoc() throws Exception {
        this.createSpringfoxSwaggerJson("item");
        this.createSpringfoxSwaggerJson("etc");
        this.createSpringfoxSwaggerJson("order");
        this.createSpringfoxSwaggerJson("claim");
        this.createSpringfoxSwaggerJson("basic");
        this.createSpringfoxSwaggerJson("escrow");
    }

    private void createSpringfoxSwaggerJson(final String group) throws Exception {
        final MvcResult mvcResult = this.mockMvc.perform(get("/v2/api-docs?group=" + group)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        final MockHttpServletResponse response = mvcResult.getResponse();
        String swaggerJson = response.getContentAsString();

        swaggerJson = swaggerJson.replace("\"host\":\"localhost:8080\",\"basePath\":\"/\",", "");
        swaggerJson = swaggerJson.replace("\"schemes\":[\"http\"],", "");

        Files.createDirectories(Paths.get(Swagger2MarkupTest.SWAGGER_OUTPUT_DIR));

        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(Swagger2MarkupTest.SWAGGER_OUTPUT_DIR, group + ".json"), StandardCharsets.UTF_8)) {
            writer.write(swaggerJson);
        }


        final String GENERATED_OUTPUT_DIR_GROUP = GENERATED_OUTPUT_DIR + group;

        final Swagger2MarkupConfig config = new Swagger2MarkupConfigBuilder()
                .withMarkupLanguage(MarkupLanguage.ASCIIDOC)
                .withOutputLanguage(Language.EN)
                .withPathsGroupedBy(GroupBy.TAGS)
                .withSwaggerMarkupLanguage(MarkupLanguage.ASCIIDOC)
                .withTagOrdering(OrderBy.NATURAL)
                .withOperationOrdering(OrderBy.NATURAL)
                .withParameterOrdering(OrderBy.AS_IS)
                .withPropertyOrdering(OrderBy.AS_IS)
                .withGeneratedExamples()
                .build();

        Swagger2MarkupConverter converter = Swagger2MarkupConverter.from(swaggerJson)
                .withConfig(config)
                .build();

        converter.toFolder(Paths.get(GENERATED_OUTPUT_DIR_GROUP));
    }
}
