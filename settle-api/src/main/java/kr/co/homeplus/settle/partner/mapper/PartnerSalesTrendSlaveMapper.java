package kr.co.homeplus.settle.partner.mapper;

import java.util.List;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;
import kr.co.homeplus.settle.partner.model.salesTrend.SalesByAgeInfo;
import kr.co.homeplus.settle.partner.model.salesTrend.SalesByDailyInfo;
import kr.co.homeplus.settle.partner.model.salesTrend.SalesByGenderInfo;
import kr.co.homeplus.settle.partner.model.salesTrend.SalesTrendSummaryInfo;
import kr.co.homeplus.settle.partner.model.salesTrend.SearchConditionDto;

@SlaveConnection
public interface PartnerSalesTrendSlaveMapper {

    SalesTrendSummaryInfo selectSalesTrendSummaryInfo(final SearchConditionDto searchConditionDto);

    List<SalesByDailyInfo> selectSalesByDailyInfo(final SearchConditionDto searchConditionDto);

    List<SalesByAgeInfo> selectSalesByAgeInfo(final SearchConditionDto searchConditionDto);

    List<SalesByGenderInfo> selectSalesByGenderInfo(final SearchConditionDto searchConditionDto);
}
