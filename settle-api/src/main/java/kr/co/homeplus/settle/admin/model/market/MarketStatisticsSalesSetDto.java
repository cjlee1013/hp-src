package kr.co.homeplus.settle.admin.model.market;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마켓별 판매현황")
public class MarketStatisticsSalesSetDto {
  @ApiModelProperty(notes = "시작일자")
  private String startDt;

  @ApiModelProperty(notes = "종료일자")
  private String endDt;
}
