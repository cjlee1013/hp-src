package kr.co.homeplus.settle.admin.model.affiliate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "전표조회 상세")
public class AffiliateDetailGetDto {
  @ApiModelProperty(notes = "구분")
  private String gubun;

  @ApiModelProperty(notes = "배송/구매완료시간")
  private String basicDt;

  @ApiModelProperty(notes = "취소완료시간")
  private String claimDt;

  @ApiModelProperty(notes = "주문시간")
  private String paymentCompleteDt;

  @ApiModelProperty(notes = "디바이스구분")
  private String platform;

  @ApiModelProperty(notes = "주문번호")
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "클레임번호")
  private String claimNo;

  @ApiModelProperty(notes = "상품번호")
  private String itemNo;

  @ApiModelProperty(notes = "상품명")
  private String itemName;

  @ApiModelProperty(notes = "결제금액")
  private String orderPrice;

  @ApiModelProperty(notes = "배송비")
  private String shipPrice;

  @ApiModelProperty(notes = "부가세")
  private String vatPrice;

  @ApiModelProperty(notes = "실적대상금액")
  private String supplyAmt;

  @ApiModelProperty(notes = "수수료율")
  private String commissionRate;

  @ApiModelProperty(notes = "수수료액")
  private String commissionAmt;

  @ApiModelProperty(notes = "제휴연동정보1")
  private String affiliateExtraCd;

  @ApiModelProperty(notes = "제휴연동정보2")
  private String ocbSaveCardNo;
}
