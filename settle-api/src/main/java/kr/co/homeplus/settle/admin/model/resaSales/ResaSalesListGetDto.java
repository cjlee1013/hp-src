package kr.co.homeplus.settle.admin.model.resaSales;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "SALES 자료조회 목록")
public class ResaSalesListGetDto {
  @ApiModelProperty(notes = "일자")
  private String basicDt;

  @ApiModelProperty(notes = "점포코드")
  private String storeId;

  @ApiModelProperty(notes = "점포명")
  private String storeNm;

  @ApiModelProperty(notes = "코스트센터")
  private String costCenter;

  @ApiModelProperty(notes = "ReSA_SALES")
  private String resaSales;

  @ApiModelProperty(notes = "ReSA_15191")
  private String resa15191;

  @ApiModelProperty(notes = "ReSA_20720")
  private String resa20720;

  @ApiModelProperty(notes = "ReSA_64800")
  private String resa64800;

  @ApiModelProperty(notes = "ReSA_72905")
  private String resa72905;

  @ApiModelProperty(notes = "ReSA_20744")
  private String resa20744;

  @ApiModelProperty(notes = "정산_Sales")
  private String settleSales;

  @ApiModelProperty(notes = "정산_15191")
  private String settle15191;

  @ApiModelProperty(notes = "정산_20720")
  private String settle20720;

  @ApiModelProperty(notes = "정산_20720_DS_장바구니")
  private String settle20720DsCart;

  @ApiModelProperty(notes = "정산_64800")
  private String settle64800;

  @ApiModelProperty(notes = "정산_72905")
  private String settle72905;

  @ApiModelProperty(notes = "정산_20744")
  private String settle20744;

  @ApiModelProperty(notes = "차이내역_Sales")
  private String diffSales;

  @ApiModelProperty(notes = "차이내역_15191")
  private String diff15191;

  @ApiModelProperty(notes = "차이내역_20720")
  private String diff20720;

  @ApiModelProperty(notes = "차이내역_64800")
  private String diff64800;

  @ApiModelProperty(notes = "차이내역_72905")
  private String diff72905;

  @ApiModelProperty(notes = "차이내역_20744")
  private String diff20744;
}
