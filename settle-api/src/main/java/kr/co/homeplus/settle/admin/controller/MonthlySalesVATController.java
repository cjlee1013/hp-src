package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlyAdjustListGetDto;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlySalesVATListGetDto;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlySalesVATListSetDto;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlySalesVATOrderGetDto;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlySalesVATOrderSetDto;
import kr.co.homeplus.settle.admin.service.MonthlySalesVATService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/monthlySalesVAT")
@Api(tags = "BackOffice > 부가세신고내역 조회")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class MonthlySalesVATController {
    private final MonthlySalesVATService MonthlySalesVATService;

    @ApiOperation(value = "세금계산서 관리 > 부가세신고내역 조회", response = ResponseObject.class)
    @PostMapping("/getMonthlySalesVATList")
    public ResponseObject<List<MonthlySalesVATListGetDto>> getMonthlySalesVATList(@RequestBody MonthlySalesVATListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(MonthlySalesVATService.getMonthlySalesVATList(listParamDto));
    }

    @ApiOperation(value = "부가세신고내역 > 주문 상세내역", response = ResponseObject.class)
    @PostMapping("/getMonthlySalesVATOrder")
    public ResponseObject<List<MonthlySalesVATOrderGetDto>> getMonthlySalesVATOrder(@RequestBody MonthlySalesVATOrderSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(MonthlySalesVATService.getMonthlySalesVATOrder(listParamDto));
    }

    //SETTLE-632 정산조정
    @ApiOperation(value = "부가세신고내역 > 정산조정 상세내역", response = ResponseObject.class)
    @PostMapping("/getMonthlyAdjustList")
    public ResponseObject<List<MonthlyAdjustListGetDto>> getMonthlyAdjustList(@RequestBody MonthlySalesVATOrderSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(MonthlySalesVATService.getMonthlyAdjustList(listParamDto));
    }
}