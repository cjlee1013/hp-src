package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.dailyPayment.DailyPaymentGetDto;
import kr.co.homeplus.settle.admin.model.dailyPayment.DailyPaymentSetDto;
import kr.co.homeplus.settle.admin.model.dailyPayment.DailyPaymentSumGetDto;
import kr.co.homeplus.settle.admin.model.dailyPayment.PaymentInfoGetDto;
import kr.co.homeplus.settle.admin.model.dailyPayment.PaymentInfoSetDto;
import kr.co.homeplus.settle.admin.service.DailyPaymentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/dailyPayment")
@Api(tags = "BackOffice > 판매현황")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class DailyPaymentController {
    private final DailyPaymentService dailyPaymentService;

    @ApiOperation(value = "정산관리 > 판매현황 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getDailyPaymentList")
    public ResponseObject<List<DailyPaymentGetDto>> getDailyPaymentList(@RequestBody DailyPaymentSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(dailyPaymentService.getDailyPaymentList(listParamDto));
    }

    @ApiOperation(value = "정산관리 > 판매현황 > 상세 주문조회", response = ResponseObject.class)
    @PostMapping("/getDailyPaymentInfo")
    public ResponseObject<List<PaymentInfoGetDto>> getDailyPaymentInfo(@RequestBody PaymentInfoSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(dailyPaymentService.getDailyPaymentInfo(listParamDto));
    }

    @ApiOperation(value = "정산관리 > 판매현황 > 점별 판매금액 합계", response = ResponseObject.class)
    @PostMapping("/getDailyPaymentSum")
    public ResponseObject<List<DailyPaymentSumGetDto>> getDailyPaymentSum(@RequestBody DailyPaymentSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(dailyPaymentService.getDailyPaymentSum(listParamDto));
    }
}