package kr.co.homeplus.settle.admin.model.taxBillInvoice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "세금계산서 수취 관리 > PG 공급처 정보")
public class TaxBillReceiveSupplierGetDto {
    @ApiModelProperty(value = "공급처")
    private String supplier;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "상호")
    private String partnerNm;

    @ApiModelProperty(value = "사업자번호")
    private String partnerNo;

    @ApiModelProperty(value = "대표자명")
    private String partnerOwner;

}
    
    