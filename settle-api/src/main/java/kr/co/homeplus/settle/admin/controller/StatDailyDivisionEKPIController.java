package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.statDailyDivisionEKPI.DailyDivisionEKPIListGetDto;
import kr.co.homeplus.settle.admin.model.statDailyDivisionEKPI.DailyDivisionEKPIListSetDto;
import kr.co.homeplus.settle.admin.model.statDailyDivisionEKPI.DailySettleMarketItemGetDto;
import kr.co.homeplus.settle.admin.service.StatDailyDivisionEKPIService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/dailyDivisionEKPI")
@Api(tags = "BackOffice > 상품주문통계 > 카테고리별 매출")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class StatDailyDivisionEKPIController {
    private final StatDailyDivisionEKPIService dailyDivisionEKPIService;

    @ApiOperation(value = "통계 > 상품주문통계 > 카테고리별 매출 (E-KPI)", response = ResponseObject.class)
    @PostMapping("/getDailyDivisionEKPIList")
    public ResponseObject<List<DailyDivisionEKPIListGetDto>> getDailyDivisionEKPIList(@RequestBody DailyDivisionEKPIListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(dailyDivisionEKPIService.getDailyDivisionEKPIList(listParamDto));
    }

    //통계 > 마켓통계 > 상품별 판매 통계 (SETTLE-699 SETTLE-904)
    @ApiOperation(value = "통계 > 마켓통계 > 상품별 판매 통계", response = ResponseObject.class)
    @PostMapping("/getDailySettleMarketItemList")
    public ResponseObject<List<DailySettleMarketItemGetDto>> getDailySettleMarketItemList(@RequestBody DailyDivisionEKPIListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(dailyDivisionEKPIService.getDailySettleMarketItemList(listParamDto));
    }

}