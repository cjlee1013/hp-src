package kr.co.homeplus.settle.admin.model.accounting;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AccountingSubjectListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;
}
