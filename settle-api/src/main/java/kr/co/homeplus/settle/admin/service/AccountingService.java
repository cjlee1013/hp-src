package kr.co.homeplus.settle.admin.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.settle.admin.mapper.AccountingMasterMapper;
import kr.co.homeplus.settle.admin.mapper.AccountingSlaveMapper;
import kr.co.homeplus.settle.admin.model.accounting.APBalanceSheetGetDto;
import kr.co.homeplus.settle.admin.model.accounting.APInvoiceConfirmGetDto;
import kr.co.homeplus.settle.admin.model.accounting.APInvoiceConfirmSetDto;
import kr.co.homeplus.settle.admin.model.accounting.APInvoiceListGetDto;
import kr.co.homeplus.settle.admin.model.accounting.APInvoiceListSetDto;
import kr.co.homeplus.settle.admin.model.accounting.AccountingSubjectListGetDto;
import kr.co.homeplus.settle.admin.model.accounting.AccountingSubjectListSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetSlipTypeGetDto;
import kr.co.homeplus.settle.core.exception.handler.ExceptionCode;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountingService {
    private final AccountingSlaveMapper accountingSlaveMapper;
    private final AccountingMasterMapper accountingMasterMapper;
    private final RefitCryptoService cryptoService;

    // 이메일
    public static final String EMAIL_PATTERN = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$";

    /**
     * 정산관리 > 판매관리 > 메인 리스트 조회
     */
    public List<AccountingSubjectListGetDto> getAccountSubjectList(AccountingSubjectListSetDto listParamDto) throws Exception {
        return accountingSlaveMapper.getAccountSubjectList(listParamDto);
    }

    /**
     * AP 전표조회 (rf_ap_invoices_interface)
     */
    public List<APInvoiceListGetDto> getAPInvoiceList(APInvoiceListSetDto listParamDto) throws Exception {
        List<APInvoiceListGetDto> returnDto = accountingSlaveMapper.getAPInvoiceList(listParamDto);
        for (APInvoiceListGetDto dto : returnDto) {

            try {
                Pattern emailPattern = Pattern.compile(AccountingService.EMAIL_PATTERN); //이메일 패턴 체크

                if (!emailPattern.matcher(dto.getMngEmail()).matches()) {
                    dto.setMngEmail(cryptoService.decrypt(dto.getMngEmail()));
                }
            } catch (Exception e) {
                dto.setMngEmail("");
            }

            dto.setPartnerId(PrivacyMaskingUtils.maskingUserId(dto.getPartnerId()));
        }
        return returnDto;
    }

    /**
     * AP 전표조회 (rf_ap_invoice_lines_interface)
     */
    public List<APBalanceSheetGetDto> getAPBalanceSheet(APInvoiceListSetDto listParamDto) throws Exception {
        return accountingSlaveMapper.getAPBalanceSheet(listParamDto);
    }

    /**
     * AP 전표조회 (rf_ap_invoice_lines_interface)
     */
    public ResponseObject<String> setAPInvoiceConfirm(List<APInvoiceConfirmSetDto> listParamDto) throws Exception {
        String resultMsg = "", returnCode = "";
        try {
            for (APInvoiceConfirmSetDto dto : listParamDto) {
                APInvoiceConfirmGetDto returnDto = accountingMasterMapper.setAPInvoiceConfirm(dto);

                resultMsg =  returnDto.getResultMsg();
                returnCode = returnDto.getResultCd();
            }

        }
        catch (Exception e) {
            resultMsg =  e.getLocalizedMessage();
            returnCode = ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode();
        }

        return ResourceConverter.toResponseObject(resultMsg, HttpStatus.OK, returnCode, resultMsg);
    }

    /**
     * AP 전표 목록 조회
     */
    public List<BalanceSheetSlipTypeGetDto> getAPSlipTypeList() throws Exception {
        return accountingSlaveMapper.getAPSlipTypeList();
    }
}
