package kr.co.homeplus.settle.admin.model.partnerPayment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PartnerSettlePreDtSetDto {
    @ApiModelProperty(value = "지급SRL")
    private long settleSrl;

    @ApiModelProperty(value = "정산예정일")
    private String settlePreDt;

    @ApiModelProperty(value = "정산예정일 변경 상세")
    private String changeMemo;

    @ApiModelProperty(value = "처리자")
    private String chgId;
}
