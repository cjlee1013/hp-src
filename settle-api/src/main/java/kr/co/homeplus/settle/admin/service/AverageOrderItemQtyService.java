package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.settle.admin.mapper.AverageOrderItemQtySlaveMapper;
import kr.co.homeplus.settle.admin.model.averageOrderItemQty.AverageOrderItemQtyListGetDto;
import kr.co.homeplus.settle.admin.model.averageOrderItemQty.AverageOrderItemQtyListSetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AverageOrderItemQtyService {

    private final AverageOrderItemQtySlaveMapper averageOrderItemQtySlaveMapper;

    /**
     * 통계 > 주문결제통계 > 주문당 평균 주문 상품수
     */
    public List<AverageOrderItemQtyListGetDto> getAverageOrderItemQtyList(
        AverageOrderItemQtyListSetDto averageOrderItemQtyListSetDto) throws Exception {
        return averageOrderItemQtySlaveMapper.getAverageOrderItemQtyList(averageOrderItemQtyListSetDto);
    }
}
