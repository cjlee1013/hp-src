package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.accounting.APBalanceSheetGetDto;
import kr.co.homeplus.settle.admin.model.accounting.APInvoiceListGetDto;
import kr.co.homeplus.settle.admin.model.accounting.APInvoiceListSetDto;
import kr.co.homeplus.settle.admin.model.accounting.AccountingSubjectListGetDto;
import kr.co.homeplus.settle.admin.model.accounting.AccountingSubjectListSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetSlipTypeGetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface AccountingSlaveMapper {
    List<AccountingSubjectListGetDto> getAccountSubjectList(AccountingSubjectListSetDto listParamDto);

    List<APInvoiceListGetDto> getAPInvoiceList(APInvoiceListSetDto listParamDto);

    List<APBalanceSheetGetDto> getAPBalanceSheet(APInvoiceListSetDto listParamDto);

    List<BalanceSheetSlipTypeGetDto> getAPSlipTypeList();
}
