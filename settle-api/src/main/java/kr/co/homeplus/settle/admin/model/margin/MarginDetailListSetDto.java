package kr.co.homeplus.settle.admin.model.margin;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MarginDetailListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String schStartDt;

    @ApiModelProperty(value = "검색종료일")
    private String schEndDt;

    @ApiModelProperty(value = "상품구분")
    private String mallType;

    @ApiModelProperty(value = "카테고리 division")
    private String division;

    @ApiModelProperty(value = "카테고리 groupNo")
    private String groupNo;

    @ApiModelProperty(value = "카테고리 deptNo")
    private String dept;

    @ApiModelProperty(value = "카테고리 classNo")
    private String classCd;

    @ApiModelProperty(value = "카테고리 subClassNo")
    private String subclass;
}
