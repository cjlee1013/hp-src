package kr.co.homeplus.settle.admin.model.statDailyDivisionEKPI;

import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import lombok.Data;

@Data
public class DailyDivisionEKPIListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(value = "거래유형")
    private String mallType;

    @ApiModelProperty(value = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "대분류")
    private String divisionNo;

    @ApiModelProperty(value = "중분류")
    private String groupNo;

    @ApiModelProperty(value = "소분류")
    private String deptNo;

    @ApiModelProperty(value = "세분류")
    private String classNo;

    @ApiModelProperty(value = "사이트타입")
    private String siteType;

    @ApiModelProperty(value = "검색날짜")
    private ArrayList<String> basicDt;

}
