package kr.co.homeplus.settle.admin.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제일마감")
public class PgPaymentInfoListSetDto {
  @ApiModelProperty(notes = "시작일자")
  private String startDt;

  @ApiModelProperty(notes = "종료일자")
  private String endDt;

  @ApiModelProperty(notes = "검색조건")
  private String gubun;

  @ApiModelProperty(notes = "상품구분")
  private String mallType;

  @ApiModelProperty(notes = "주문번호")
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "클레임번호")
  private String claimNo;

  @ApiModelProperty(notes = "점포유형")
  private String storeType;

  @ApiModelProperty(notes = "PG사")
  private String pgKind;

  @ApiModelProperty(notes = "연동 주문번호")
  private String pgOid;
}
