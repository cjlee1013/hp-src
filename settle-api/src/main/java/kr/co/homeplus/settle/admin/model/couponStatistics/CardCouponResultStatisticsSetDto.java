package kr.co.homeplus.settle.admin.model.couponStatistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardCouponResultStatisticsSetDto {
    @ApiModelProperty(value = "조회기간(시작일)")
    private String schStartDt;

    @ApiModelProperty(value = "조회기간(종료일)")
    private String schEndDt;

    @ApiModelProperty(value = "점포유형")
    private String schStoreType;

    @ApiModelProperty(value = "검색타입")
    private String schKeywordType;

    @ApiModelProperty(value = "검색어")
    private String schKeyword;

    @ApiModelProperty(value = "쿠폰번호")
    private String schCouponNo;
}
