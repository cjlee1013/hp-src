package kr.co.homeplus.settle.admin.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제일마감")
public class PgPaymentInfoListGetDto {
  @ApiModelProperty(notes = "기준일")
  private String basicDt;

  @ApiModelProperty(notes = "PG사")
  private String pgKind;

  @ApiModelProperty(notes = "점포코드")
  private String originStoreId;

  @ApiModelProperty(notes = "점포명")
  private String originStoreNm;

  @ApiModelProperty(notes = "주문번호")
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "연동 주문번호")
  private String pgOid;

  @ApiModelProperty(notes = "클레임번호")
  private String claimNo;

  @ApiModelProperty(notes = "상품구분")
  private String mallType;

  @ApiModelProperty(notes = "집계구분")
  private String gubun;

  @ApiModelProperty(notes = "총 상품금액")
  private String completeAmt;

  @ApiModelProperty(notes = "배송비")
  private String shipAmt;

  @ApiModelProperty(notes = "반품배송비")
  private String claimReturnShipAmt;

  @ApiModelProperty(notes = "상품할인(업체)")
  private String couponSellerChargeAmt;

  @ApiModelProperty(notes = "상품할인(자사)")
  private String couponHomeChargeAmt;

  @ApiModelProperty(notes = "상품할인(카드)")
  private String couponCardChargeAmt;

  @ApiModelProperty(notes = "카드할인(자사)")
  private String cardCouponHomeAmt;

  @ApiModelProperty(notes = "카드할인(카드)")
  private String cardCouponCardAmt;

  @ApiModelProperty(notes = "배송비할인")
  private String shipDiscountAmt;

  @ApiModelProperty(notes = "임직원할인")
  private String empDiscountAmt;

  @ApiModelProperty(notes = "행사할인")
  private String promoDiscountAmt;

  @ApiModelProperty(notes = "장바구니할인")
  private String cartCouponHomeAmt;

  @ApiModelProperty(notes = "매출(E-KPI)")
  private String ekpiAmt;

  @ApiModelProperty(notes = "결제수단/환불수단")
  private String paymentTool;

  @ApiModelProperty(notes = "합계 결제금액")
  private String orderPrice;

  @ApiModelProperty(notes = "PG 결제금액")
  private String pgAmt;

  @ApiModelProperty(notes = "DGV 결제금액")
  private String dgvAmt;

  @ApiModelProperty(notes = "MHP 결제금액")
  private String mhcAmt;

  @ApiModelProperty(notes = "OCB 결제금액")
  private String ocbAmt;

  @ApiModelProperty(notes = "마일리지 결제금액")
  private String mileageAmt;

  @ApiModelProperty(notes = "판매수수료")
  private String saleAgencyFee;

  @ApiModelProperty(notes = "정산대상금액")
  private String settleAmt;

  @ApiModelProperty(notes = "부가세조건")
  private String vatCondition;

  @ApiModelProperty(notes = "PG수수료율")
  private String commissionRate;

  @ApiModelProperty(notes = "PG수수료")
  private String commissionAmt;

  @ApiModelProperty(notes = "PG예치금")
  private String depositAmt;

  @ApiModelProperty(notes = "지급완료금액")
  private String payCompleteAmt;

  @ApiModelProperty(notes = "예치금잔고")
  private String depositBalance;

  @ApiModelProperty(notes = "입금주기")
  private String costPeriod;

  @ApiModelProperty(notes = "입금일")
  private String payScheduleDt;

  @ApiModelProperty(notes = "입금예정금액")
  private String payScheduleAmt;
}
