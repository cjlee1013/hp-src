package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.dailyPayment.DailyPaymentGetDto;
import kr.co.homeplus.settle.admin.model.dailyPayment.DailyPaymentSetDto;
import kr.co.homeplus.settle.admin.model.dailyPayment.DailyPaymentSumGetDto;
import kr.co.homeplus.settle.admin.model.dailyPayment.PaymentInfoGetDto;
import kr.co.homeplus.settle.admin.model.dailyPayment.PaymentInfoSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface DailyPaymentSlaveMapper {
    List<DailyPaymentGetDto> getDailyPaymentList(DailyPaymentSetDto listParamDto);

    List<PaymentInfoGetDto> getDailyPaymentInfo(PaymentInfoSetDto listParamDto);

    List<DailyPaymentSumGetDto> getDailyPaymentSum(DailyPaymentSetDto listParamDto);

    List<DailyPaymentSumGetDto> getDailyPaymentStore(DailyPaymentSetDto listParamDto);
}
