package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.pgReceivable.PgReceivableGetDto;
import kr.co.homeplus.settle.admin.model.pgSettle.PgSettleDetailGetDto;
import kr.co.homeplus.settle.admin.model.pgSettle.PgSettleSetDto;
import kr.co.homeplus.settle.admin.model.pgSettle.PgSettleSumGetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface PgSettleSlaveMapper {
  List<PgSettleSumGetDto> getPgSettleSum(PgSettleSetDto pgSettleSetDto);
  List<PgSettleDetailGetDto> getPgSettleDetail(PgSettleSetDto pgSettleSetDto);
  List<PgReceivableGetDto> getPgReceivable(PgSettleSetDto pgSettleSetDto);
}
