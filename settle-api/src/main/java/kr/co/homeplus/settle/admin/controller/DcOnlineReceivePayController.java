package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.dcOnlineReceivePay.DcOnlineOrderDetailListGetDto;
import kr.co.homeplus.settle.admin.model.dcOnlineReceivePay.DcOnlineOrderDetailListSetDto;
import kr.co.homeplus.settle.admin.model.dcOnlineReceivePay.DcOnlineReceivePayListGetDto;
import kr.co.homeplus.settle.admin.model.dcOnlineReceivePay.DcOnlineReceivePayListSetDto;
import kr.co.homeplus.settle.admin.service.DcOnlineReceivePayService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/dcOnlineReceivePay")
@Api(tags = "BackOffice > DC온라인수불 조회")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class DcOnlineReceivePayController {
    private final DcOnlineReceivePayService dcOnlineReceivePayService;

    @ApiOperation(value = "DC온라인수불 조회 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getDcOnlineReceivePayList")
    public ResponseObject<List<DcOnlineReceivePayListGetDto>> getDcOnlineReceivePayList(@RequestBody DcOnlineReceivePayListSetDto dcOnlineReceivePayListSetDto) throws Exception {
        return ResourceConverter.toResponseObject(dcOnlineReceivePayService.getDcOnlineReceivePayList(dcOnlineReceivePayListSetDto));
    }

    @ApiOperation(value = "DC온라인수불 조회 > 주문상세 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getDcOnlineOrderDetailList")
    public ResponseObject<List<DcOnlineOrderDetailListGetDto>> getDcOnlineOrderDetailList(@RequestBody DcOnlineOrderDetailListSetDto dcOnlineOrderDetailListSetDto) throws Exception {
        return ResourceConverter.toResponseObject(dcOnlineReceivePayService.getDcOnlineOrderDetailList(dcOnlineOrderDetailListSetDto));
    }
}