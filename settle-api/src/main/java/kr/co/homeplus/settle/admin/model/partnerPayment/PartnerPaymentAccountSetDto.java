package kr.co.homeplus.settle.admin.model.partnerPayment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PartnerPaymentAccountSetDto {
    @ApiModelProperty(value = "지급SRL")
    private String settleSrl;

    @ApiModelProperty(value = "파트너ID")
    private String partnerId;

    @ApiModelProperty(value = "처리자")
    private String chgId;
}
