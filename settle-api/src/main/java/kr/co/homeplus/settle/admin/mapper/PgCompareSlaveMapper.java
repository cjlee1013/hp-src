package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.pgCompare.PgCompareCostCommissionSetDto;
import kr.co.homeplus.settle.admin.model.pgCompare.PgCompareCostDiffGetDto;
import kr.co.homeplus.settle.admin.model.pgCompare.PgCompareCostSumGetDto;
import kr.co.homeplus.settle.admin.model.pgCompare.PgCompareCostSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface PgCompareSlaveMapper {
  List<PgCompareCostSumGetDto> getPgCompareCostSum(PgCompareCostSetDto pgCompareCostSetDto);
  List<PgCompareCostDiffGetDto> getPgCompareCostDiff(PgCompareCostSetDto pgCompareCostSetDto);
  List<PgCompareCostDiffGetDto> getPgCompareCostCommissionAmt(
      PgCompareCostCommissionSetDto pgCompareCostCommissionSetDto);
  List<PgCompareCostDiffGetDto> getPgCompareCostCommissionBalance(
      PgCompareCostCommissionSetDto pgCompareCostCommissionSetDto);
}
