package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.partnerSettle.PartnerSettleListGetDto;
import kr.co.homeplus.settle.admin.model.partnerSettle.PartnerSettleListSetDto;
import kr.co.homeplus.settle.admin.model.partnerSettleDetail.PartnerSettleDetailGetDto;
import kr.co.homeplus.settle.admin.model.partnerSettleDetail.PartnerSettleItemGetDto;
import kr.co.homeplus.settle.admin.model.partnerSettleDetail.PartnerSettleItemSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface PartnerSettleSlaveMapper {
    List<PartnerSettleListGetDto> getPartnerSettleList(PartnerSettleListSetDto listParamDto);

    List<PartnerSettleItemGetDto> getPartnerSettleItem(PartnerSettleItemSetDto listParamDto);

    List<PartnerSettleDetailGetDto> getPartnerSettleDetail(PartnerSettleListSetDto listParamDto);

}
