package kr.co.homeplus.settle.admin.model.pgCompare;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제일마감")
public class PgCompareCostSetDto {
    @ApiModelProperty(notes = "시작일자")
    private String startDt;
    @ApiModelProperty(notes = "종료일자")
    private String endDt;
    @ApiModelProperty(notes = "검색기간종류")
    private String dateType;
    @ApiModelProperty(notes = "PG사")
    private String pgKind;
}
