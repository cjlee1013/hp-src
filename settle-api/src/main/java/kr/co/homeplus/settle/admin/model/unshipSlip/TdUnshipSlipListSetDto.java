package kr.co.homeplus.settle.admin.model.unshipSlip;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TdUnshipSlipListSetDto {
    @ApiModelProperty(value = "검색기간(년)")
    private String schYear;

    @ApiModelProperty(value = "검색기간(월)")
    private String schMonth;

    @ApiModelProperty(value = "점포유형")
    private String schStoreType;
}
