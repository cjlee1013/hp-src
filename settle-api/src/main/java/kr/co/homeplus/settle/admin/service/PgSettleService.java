package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.settle.admin.mapper.PgSettleSlaveMapper;
import kr.co.homeplus.settle.admin.model.pgReceivable.PgReceivableGetDto;
import kr.co.homeplus.settle.admin.model.pgSettle.PgSettleDetailGetDto;
import kr.co.homeplus.settle.admin.model.pgSettle.PgSettleSetDto;
import kr.co.homeplus.settle.admin.model.pgSettle.PgSettleSumGetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
@Configuration
public class PgSettleService {
  private final PgSettleSlaveMapper pgSettleSlaveMapper;

  public List<PgSettleSumGetDto> getPgSettleSum(PgSettleSetDto pgSettleSetDto) {
    return pgSettleSlaveMapper.getPgSettleSum(pgSettleSetDto);
  }

  public List<PgSettleDetailGetDto> getPgSettleDetail(PgSettleSetDto pgSettleSetDto) {
    return pgSettleSlaveMapper.getPgSettleDetail(pgSettleSetDto);
  }

  public List<PgReceivableGetDto> getPgReceivable(PgSettleSetDto pgSettleSetDto) {
    return pgSettleSlaveMapper.getPgReceivable(pgSettleSetDto);
  }
}
