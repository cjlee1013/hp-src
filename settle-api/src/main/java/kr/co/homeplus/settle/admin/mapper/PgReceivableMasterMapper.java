package kr.co.homeplus.settle.admin.mapper;

import kr.co.homeplus.settle.admin.model.pgReceivable.PgReceivableSetDto;
import kr.co.homeplus.settle.core.db.annotation.MasterConnection;

@MasterConnection
public interface PgReceivableMasterMapper {
  int setPgReceivable(PgReceivableSetDto pgReceivableSetDto);
}
