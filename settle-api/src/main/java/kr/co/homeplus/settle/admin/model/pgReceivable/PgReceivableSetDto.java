package kr.co.homeplus.settle.admin.model.pgReceivable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PgReceivableSetDto {
    private String basicDt;
    private String paymentDt;
    private String pgKind;
    private String paymentTool;
    private String mallType;
    private String code;
    private String settleSrl;
    private long transAmt;
    private long transBalance;
    private long depositAmt;
    private long depositBalance;
    private long receivableAmt;
}
