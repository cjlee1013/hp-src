package kr.co.homeplus.settle.admin.model.pgReceivable;

import lombok.Data;

@Data
public class PgReceivableGetDto {
    private String basicDt;
    private String paymentDt;
    private String pgKind;
    private String mallType;
    private String code;
    private String description;
    private String settleSrl;
    private String gubun;
    private long transAmt;
    private long transBalance;
    private long depositAmt;
    private long depositBalance;
    private long receivableAmt;
    private String regDt;
}
