package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.settle.admin.model.minusSettle.MinusSettleListGetDto;
import kr.co.homeplus.settle.admin.model.minusSettle.MinusSettleListSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;
import org.apache.ibatis.annotations.Param;

@SlaveConnection
public interface MinusSettleSlaveMapper {
    List<MinusSettleListGetDto> selectMinusSettleList(MinusSettleListSetDto listParamDto);
}
