package kr.co.homeplus.settle.batch.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OpmallRegistSetDto {
  private String partnerId;
  private String partnerNo;
  private String partnerNm;
  private String partnerOwner;
  private String bankAccountHolder;
  private String bankCode;
  private String bankAccount;
  private String phoneNo;

}