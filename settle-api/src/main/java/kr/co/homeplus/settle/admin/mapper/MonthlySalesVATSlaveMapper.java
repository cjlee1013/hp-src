package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlyAdjustListGetDto;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlySalesVATOrderGetDto;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlySalesVATOrderSetDto;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlySalesVATListGetDto;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlySalesVATListSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface MonthlySalesVATSlaveMapper {
    List<MonthlySalesVATListGetDto> getMonthlySalesVATList(MonthlySalesVATListSetDto listParamDto);

    List<MonthlySalesVATOrderGetDto> getMonthlySalesVATOrder(MonthlySalesVATOrderSetDto listParamDto);

    List<MonthlyAdjustListGetDto> getMonthlyAdjustList(MonthlySalesVATOrderSetDto listParamDto);
}
