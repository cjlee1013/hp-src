package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.couponStatistics.CardCouponResultStatisticsDetailGetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CardCouponResultStatisticsGetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CardCouponResultStatisticsSetDto;
import kr.co.homeplus.settle.admin.service.CardCouponResultStatisticsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/statistics")
@Api(tags = "BackOffice > 카드사별쿠폰실적조회")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class CardCouponResultStatisticsController {
    private final CardCouponResultStatisticsService cardCouponResultStatisticsService;

    @ApiOperation(value = "카드사별쿠폰실적조회 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getCardCouponResultStatisticsList")
    public ResponseObject<List<CardCouponResultStatisticsGetDto>> getCardCouponResultStatisticsList(@RequestBody CardCouponResultStatisticsSetDto cardCouponResultStatisticsSetDto) {
        return ResourceConverter.toResponseObject(cardCouponResultStatisticsService.getCardCouponResultStatisticsList(cardCouponResultStatisticsSetDto));
    }

    @ApiOperation(value = "카드사별쿠폰실적조회 > 상세 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getCardCouponResultStatisticsDetailList")
    public ResponseObject<List<CardCouponResultStatisticsDetailGetDto>> getCardCouponResultStatisticsDetailList(@RequestBody CardCouponResultStatisticsSetDto cardCouponResultStatisticsSetDto) {
        return ResourceConverter.toResponseObject(cardCouponResultStatisticsService.getCardCouponResultStatisticsDetailList(cardCouponResultStatisticsSetDto));
    }

}