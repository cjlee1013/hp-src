package kr.co.homeplus.settle.admin.model.pgReceivable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PgCommissionSetDto {
    private String basicDt;
    private String pgKind;
}
