package kr.co.homeplus.settle.admin.model.dcOnlineReceivePay;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DcOnlineOrderDetailListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String schStartDt;

    @ApiModelProperty(value = "검색종료일")
    private String schEndDt;

    @ApiModelProperty(value = "검색조건 타입")
    private String schKeywordType;

    @ApiModelProperty(value = "검색어")
    private String schKeyword;
}
