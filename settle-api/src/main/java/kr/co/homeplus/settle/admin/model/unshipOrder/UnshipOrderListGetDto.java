package kr.co.homeplus.settle.admin.model.unshipOrder;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UnshipOrderListGetDto {
    @ApiModelProperty(value = "마감일")
    private String basicDt;

    @ApiModelProperty(value = "이월구분")
    private String carryDownType;

    @ApiModelProperty(value = "결제기준일")
    private String paymentBasicDt;

    @ApiModelProperty(value = "배송완료일")
    private String completeDt;

    @ApiModelProperty(value = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "점포코드")
    private String originStoreId;

    @ApiModelProperty(value = "점포명")
    private String storeNm;

    @ApiModelProperty(value = "상품구분")
    private String mallType;

    @ApiModelProperty(value = "주문구분")
    private String gubun;

    @ApiModelProperty(value = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(value = "배송번호")
    private String bundleNo;

    @ApiModelProperty(value = "클레임번호")
    private String claimNo;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "업체코드")
    private String vendorCd;

    @ApiModelProperty(value = "판매업체명")
    private String partnerName;

    @ApiModelProperty(value = "구분")
    private String orderType;

    @ApiModelProperty(value = "상품주문번호")
    private String orderItemNo;

    @ApiModelProperty(value = "카테고리그룹")
    private String division;

    @ApiModelProperty(value = "카테고리ID")
    private String deptNo;

    @ApiModelProperty(value = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    private String itemNm;

    @ApiModelProperty(value = "과면세")
    private String taxYn;

    @ApiModelProperty(value = "수수료율")
    private String commissionRate;

    @ApiModelProperty(value = "매입원가")
    private String orgPrice;

    @ApiModelProperty(value = "상품금액")
    private String itemSaleAmt;

    @ApiModelProperty(value = "수량")
    private String completeQty;

    @ApiModelProperty(value = "매출(IN VAT)")
    private String completeAmt;

    @ApiModelProperty(value = "매출(EX VAT)")
    private String completeAmtExVat;

    @ApiModelProperty(value = "정산(IN VAT)")
    private String settleAmt;

    @ApiModelProperty(value = "정산(EX VAT)")
    private String settleAmtExVat;

    @ApiModelProperty(value = "판매수수료")
    private String saleAgencyFee;

    @ApiModelProperty(value = "배송비")
    private String shipAmt;

    @ApiModelProperty(value = "반품배송비")
    private String claimReturnShipAmt;

    @ApiModelProperty(value = "할인")
    private String discountAmt;

    @ApiModelProperty(value = "결제금액합계")
    private String orderAmt;

    @ApiModelProperty(value = "PG")
    private String pgAmt;

    @ApiModelProperty(value = "DGV")
    private String dgvAmt;

    @ApiModelProperty(value = "MHC")
    private String mhcAmt;

    @ApiModelProperty(value = "OCB")
    private String ocbAmt;

    @ApiModelProperty(value = "마일리지")
    private String mileageAmt;

    @ApiModelProperty(value = "배송상태")
    private String shipStatus;

    @ApiModelProperty(value = "미수취신고상태")
    private String nonRcvType;
}
