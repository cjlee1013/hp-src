package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.settle.admin.mapper.PartnerPaymentMasterMapper;
import kr.co.homeplus.settle.admin.mapper.PartnerPaymentSlaveMapper;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerPaymentAccountSetDto;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerPaymentListGetDto;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerPaymentListSetDto;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerPaymentStateSetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PartnerPaymentService {
    private final PartnerPaymentMasterMapper partnerPaymentMasterMapper;
    private final PartnerPaymentSlaveMapper partnerPaymentSlaveMapper;

    private final RefitCryptoService refitCryptoService;
    /**
     * 정산관리 > 매출관리 > 메인 리스트 조회
     */
    public List<PartnerPaymentListGetDto> getPartnerPaymentList(PartnerPaymentListSetDto partnerPaymentListSetDto) throws Exception {
        String bankAccountNo = partnerPaymentListSetDto.getBankAccountNo();
        String bankAccountHolder;
        if (bankAccountNo != null && !"".equals(bankAccountNo)) {
            partnerPaymentListSetDto.setBankAccountNo(refitCryptoService.encrypt(bankAccountNo));
        }
        List<PartnerPaymentListGetDto> partnerPaymentListGetDtoList = partnerPaymentSlaveMapper.getPartnerPaymentList(partnerPaymentListSetDto);
        for (PartnerPaymentListGetDto partnerPaymentListGetDto : partnerPaymentListGetDtoList) {
            bankAccountNo = partnerPaymentListGetDto.getBankAccountNo();
            if (!"".equals(bankAccountNo)) {
                try {
                    bankAccountNo = refitCryptoService.decrypt(bankAccountNo);
                    partnerPaymentListGetDto.setBankAccountNo(PrivacyMaskingUtils.maskingBankAccount(bankAccountNo));
                } catch (Exception e) {
                    partnerPaymentListGetDto.setBankAccountNo("계좌번호 복호화 실패");
                }
            }
            bankAccountHolder = partnerPaymentListGetDto.getBankAccountHolder();
            partnerPaymentListGetDto.setBankAccountHolder(PrivacyMaskingUtils.maskingUserName(bankAccountHolder));
        }
        return partnerPaymentListGetDtoList;
    }

    public int setPartnerPaymentState(PartnerPaymentStateSetDto partnerPaymentStateSetDto) throws Exception {
        return partnerPaymentMasterMapper.setPartnerPaymentState(partnerPaymentStateSetDto);
    }

    public int setPartnerPaymentAccount(PartnerPaymentAccountSetDto partnerPaymentAccountSetDto) throws Exception {
        return partnerPaymentMasterMapper.setPartnerPaymentAccount(partnerPaymentAccountSetDto);
    }
}
