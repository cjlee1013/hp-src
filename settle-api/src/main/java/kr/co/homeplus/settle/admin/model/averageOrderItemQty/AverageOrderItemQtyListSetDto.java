package kr.co.homeplus.settle.admin.model.averageOrderItemQty;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AverageOrderItemQtyListSetDto {

    @ApiModelProperty(value = "조회시작일", position = 1)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", position = 2)
    private String schEndDt;

    @ApiModelProperty(value = "점포유형", position = 3)
    private String schStoreType;

    @ApiModelProperty(value = "상품구분", position = 4)
    private String schMallType;
}
