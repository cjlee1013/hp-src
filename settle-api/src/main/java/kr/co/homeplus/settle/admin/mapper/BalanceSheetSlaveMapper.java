package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetDetailGetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetDetailSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetListGetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetListSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetSlipTypeGetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface BalanceSheetSlaveMapper {
  List<BalanceSheetListGetDto> getBalanceSheetList(BalanceSheetListSetDto balanceSheetListSetDto);
  List<BalanceSheetDetailGetDto> getBalanceSheetDetail(BalanceSheetDetailSetDto balanceSheetDetailSetDto);
  List<BalanceSheetSlipTypeGetDto> getSlipTypeList();
}
