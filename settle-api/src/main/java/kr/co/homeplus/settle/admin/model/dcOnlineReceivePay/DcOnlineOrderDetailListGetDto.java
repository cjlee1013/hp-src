package kr.co.homeplus.settle.admin.model.dcOnlineReceivePay;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DcOnlineOrderDetailListGetDto {
    @ApiModelProperty(value = "구분")
    private String gubun;

    @ApiModelProperty(value = "배송완료일")
    private String shipCompleteDt;

    @ApiModelProperty(value = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(value = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "RMS상품번호")
    private String rmsItemNo;

    @ApiModelProperty(value = "상품명")
    private String itemName;

    @ApiModelProperty(value = "배송수량")
    private String completeQty;

    @ApiModelProperty(value = "매입원가")
    private String orgPrice;

    @ApiModelProperty(value = "판매가")
    private String itemSaleAmt;

    @ApiModelProperty(value = "할인금액")
    private String discountAmt;

    @ApiModelProperty(value = "발주서/반품 번호")
    private String poNo;

    @ApiModelProperty(value = "RMS처리일자")
    private String rmsDt;
}
