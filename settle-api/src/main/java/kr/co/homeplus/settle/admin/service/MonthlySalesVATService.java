package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.settle.admin.mapper.MonthlySalesVATSlaveMapper;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlyAdjustListGetDto;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlySalesVATListGetDto;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlySalesVATListSetDto;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlySalesVATOrderGetDto;
import kr.co.homeplus.settle.admin.model.monthlySalesVAT.MonthlySalesVATOrderSetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MonthlySalesVATService {
    private final MonthlySalesVATSlaveMapper MonthlySalesVATSlaveMapper;

    /**
     * 세금계산서 관리 > 부가세신고내역 조회 > 메인 리스트 조회
     */
    public List<MonthlySalesVATListGetDto> getMonthlySalesVATList(MonthlySalesVATListSetDto listParamDto) throws Exception {
        List<MonthlySalesVATListGetDto> returnDto = MonthlySalesVATSlaveMapper.getMonthlySalesVATList(listParamDto);
        for (MonthlySalesVATListGetDto dto : returnDto) {
            dto.setPartnerId(PrivacyMaskingUtils.maskingUserId(dto.getPartnerId()));
        }

        return returnDto;
    }

    /**
     * 부가세신고내역 > 주문 상세내역
     */
    public List<MonthlySalesVATOrderGetDto> getMonthlySalesVATOrder(MonthlySalesVATOrderSetDto listParamDto) throws Exception {
        List<MonthlySalesVATOrderGetDto> returnDto = MonthlySalesVATSlaveMapper.getMonthlySalesVATOrder(listParamDto);
        for (MonthlySalesVATOrderGetDto dto : returnDto) {
            dto.setUserNm(PrivacyMaskingUtils.maskingUserName(dto.getUserNm()));
        }

        return returnDto;
    }

    /**
     * 세금계산서 관리 > 정산조정 상세 내역
     */
    public List<MonthlyAdjustListGetDto> getMonthlyAdjustList(MonthlySalesVATOrderSetDto listParamDto) throws Exception {
        List<MonthlyAdjustListGetDto> returnDto = MonthlySalesVATSlaveMapper.getMonthlyAdjustList(listParamDto);
        for (MonthlyAdjustListGetDto dto : returnDto) {
            dto.setPartnerId(PrivacyMaskingUtils.maskingUserId(dto.getPartnerId()));
        }

        return returnDto;
    }

}
