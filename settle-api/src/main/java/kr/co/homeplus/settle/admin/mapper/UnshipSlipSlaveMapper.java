package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipCreateSetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipListGetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipListSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface UnshipSlipSlaveMapper {
    List<TdUnshipSlipListGetDto> selectTdUnshipSlipList(TdUnshipSlipListSetDto tdUnshipSlipListSetDto);
    int selectTdUnshipSlipCnt(TdUnshipSlipCreateSetDto tdUnshipSlipCreateSetDto);
}
