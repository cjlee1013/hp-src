package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipCreateSetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipListGetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipListSetDto;
import kr.co.homeplus.settle.admin.service.UnshipSlipService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/unshipSlip")
@Api(tags = "BackOffice > 미배송 전표조회")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class UnshipSlipController {
    private final UnshipSlipService unshipSlipService;

    @ApiOperation(value = "TD미배송 전표조회 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getTdUnshipSlipList")
    public ResponseObject<List<TdUnshipSlipListGetDto>> getTdUnshipSlipList(@RequestBody TdUnshipSlipListSetDto tdUnshipSlipListSetDto) throws Exception {
        return ResourceConverter.toResponseObject(unshipSlipService.getTdUnshipSlipList(tdUnshipSlipListSetDto));
    }

    @ApiOperation(value = "TD미배송 전표조회 > 전표 생성 가능 확인", response = ResponseObject.class)
    @PostMapping("/getTdUnshipSlipCnt")
    public ResponseObject<Integer> getTdUnshipSlipCnt(@RequestBody TdUnshipSlipCreateSetDto tdUnshipSlipCreateSetDto) throws Exception {
        return ResourceConverter.toResponseObject(unshipSlipService.getTdUnshipSlipCnt(tdUnshipSlipCreateSetDto));
    }

    @ApiOperation(value = "TD미배송 전표조회 > 전표 생성", response = ResponseObject.class)
    @PostMapping("/setTdUnshipSlip")
    public ResponseObject<String> setTdUnshipSlip(@RequestBody TdUnshipSlipCreateSetDto tdUnshipSlipCreateSetDto){
        unshipSlipService.setTdUnshipSlip(tdUnshipSlipCreateSetDto);
        return ResourceConverter.toResponseObject("");
    }
}