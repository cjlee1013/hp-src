package kr.co.homeplus.settle.admin.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "MHC 실적조회 > 건별 상세내역")
public class MhcPaymentListGetDto {
  @ApiModelProperty(notes = "기준일")
  private String basicDt;

  @ApiModelProperty(notes = "채널코드")
  private String chnlCd;

  @ApiModelProperty(notes = "POS번호")
  private String posNo;

  @ApiModelProperty(notes = "점포코드")
  private String storeId;

  @ApiModelProperty(notes = "결제번호")
  private String paymentNo;

  @ApiModelProperty(notes = "주문번호")
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "클레임번호")
  private String claimNo;

  @ApiModelProperty(notes = "승인번호")
  private String mhcAprNumber;

  @ApiModelProperty(notes = "취소구분")
  private String gubun;

  @ApiModelProperty(notes = "가용화여부")
  private String shipFshYn;

  @ApiModelProperty(notes = "매출액")
  private String completeAmt;

  @ApiModelProperty(notes = "결제금액")
  private String orderPrice;

  @ApiModelProperty(notes = "사용 포인트")
  private String mhcUsePoint;

  @ApiModelProperty(notes = "승인 포인트")
  private String mhcAprPoint;
}
