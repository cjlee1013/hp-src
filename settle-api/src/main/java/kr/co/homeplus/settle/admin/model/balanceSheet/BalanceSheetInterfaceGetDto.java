package kr.co.homeplus.settle.admin.model.balanceSheet;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
@ApiModel(description = "전표조회 IF")
public class BalanceSheetInterfaceGetDto {
  @ApiModelProperty(notes = "결과 코드")
  private String resultCd;

  @ApiModelProperty(notes = "결과 상세 메시지")
  private String resultMsg;
}
