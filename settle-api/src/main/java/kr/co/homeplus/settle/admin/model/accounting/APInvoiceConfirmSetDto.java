package kr.co.homeplus.settle.admin.model.accounting;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class APInvoiceConfirmSetDto {
    @ApiModelProperty(notes = "Invoice번호")
    private String invoiceNum;

    @ApiModelProperty(notes = "인터페이스번호")
    private long rfInvoiceId;

    @ApiModelProperty(notes = "담당자이메일")
    private String mngEmail;

    @ApiModelProperty(notes = "등록자")
    private String regId;
}
