package kr.co.homeplus.settle.admin.model.couponStatistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardCouponResultStatisticsDetailGetDto {
    @ApiModelProperty(value = "쿠폰번호")
    private String couponNo;

    @ApiModelProperty(value = "쿠폰명")
    private String couponNm;

    @ApiModelProperty(value = "결제일자")
    private String paymentFshDt;

    @ApiModelProperty(value = "승인번호")
    private String approvalNo;

    @ApiModelProperty(value = "카드결제금액")
    private String paymentAmt;

    @ApiModelProperty(value = "원매출금액")
    private String totItemPrice;

    @ApiModelProperty(value = "중복쿠폰할인금액")
    private String discountAmt;

    @ApiModelProperty(value = "장바구니쿠폰금액")
    private String cartDiscountAmt;

    @ApiModelProperty(value = "점포명")
    private String storeNm;

    @ApiModelProperty(value = "PG사")
    private String pgKind;

    @ApiModelProperty(value = "MID")
    private String pgMid;

    @ApiModelProperty(value = "카드사명")
    private String methodNm;

    @ApiModelProperty(value = "카드프리픽스")
    private String cardPrefixNo;

    @ApiModelProperty(value = "주문번호")
    private String purchaseOrderNo;
}
