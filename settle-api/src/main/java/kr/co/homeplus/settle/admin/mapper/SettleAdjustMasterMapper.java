package kr.co.homeplus.settle.admin.mapper;

import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.adjust.AdjustListGetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustRegisterGetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustRegisterSetDto;
import kr.co.homeplus.settle.core.db.annotation.MasterConnection;

@MasterConnection
public interface SettleAdjustMasterMapper {
    AdjustRegisterGetDto insertAdjust(AdjustRegisterSetDto listParamDto);

    int deleteAdjust(AdjustListGetDto listParamDto);
}
