package kr.co.homeplus.settle.admin.model.unshipSlip;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TdUnshipSlipCreateGetDto {
    @ApiModelProperty(value = "응답값")
    private String returnValue;

    @ApiModelProperty(value = "응답메세지")
    private String returnMsg;
}
