package kr.co.homeplus.settle.admin.mapper;

import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillReceiveConfirmGetDto;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillReceiveListGetDto;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillReceiveSetDto;
import kr.co.homeplus.settle.core.db.annotation.MasterConnection;
import org.springframework.web.bind.annotation.RequestParam;

@MasterConnection
public interface TaxBillInvoiceMasterMapper {

    TaxBillReceiveConfirmGetDto setTaxBillReceiveList(TaxBillReceiveSetDto listParamDto);

    TaxBillReceiveConfirmGetDto updTaxBillReceive(TaxBillReceiveSetDto listParamDto);

    TaxBillReceiveConfirmGetDto setTaxBillReceiveConfirm(@RequestParam(name ="taxReceiveNo") String taxReceiveNo, @RequestParam(name ="regId") String regId);

    int deleteTaxBillReceive(TaxBillReceiveSetDto listParamDto);

    void insertPGFeeBalanceSheet(@RequestParam(name ="basicDt") String basicDt);

    TaxBillReceiveListGetDto getTaxBillReceiveDetail(TaxBillReceiveSetDto listParamDto);

    //제휴수수료 새금계산서 수취
    TaxBillReceiveConfirmGetDto setSlipTaxReceiveMarket(@RequestParam(name ="taxReceiveNo") String taxReceiveNo, @RequestParam(name ="regId") String regId);
}
