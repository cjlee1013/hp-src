package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.advance.AdvanceListGetDto;
import kr.co.homeplus.settle.admin.model.advance.AdvanceListSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetDetailGetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetDetailSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetInterfaceGetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetInterfaceSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetListGetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetListSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetSlipTypeGetDto;
import kr.co.homeplus.settle.admin.service.AdvanceService;
import kr.co.homeplus.settle.admin.service.BalanceSheetService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/Advance")
@Api(tags = "BackOffice > 선수금조회")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class AdvanceController {

  private final AdvanceService advanceService;

  @ApiOperation(value = "선수금조회 목록", response = ResponseObject.class, notes = "선수금조회 목록")
  @RequestMapping(value = "/getList", method = {RequestMethod.POST})
  public ResponseObject<List<AdvanceListGetDto>> getAdvanceList (@RequestBody @Valid
      AdvanceListSetDto advanceListSetDto) {
    List<AdvanceListGetDto> advanceList = advanceService.getAdvanceList(advanceListSetDto);
    return ResourceConverter.toResponseObject(advanceList);
  }

  @ApiOperation(value = "선수금조회 0원주문 목록", response = ResponseObject.class, notes = "선수금조회 0원주문 목록")
  @RequestMapping(value = "/getZeroList", method = {RequestMethod.POST})
  public ResponseObject<List<AdvanceListGetDto>> getAdvanceZeroList (@RequestBody @Valid
      AdvanceListSetDto advanceListSetDto) {
    List<AdvanceListGetDto> advanceList = advanceService.getAdvanceZeroList(advanceListSetDto);
    return ResourceConverter.toResponseObject(advanceList);
  }
}
