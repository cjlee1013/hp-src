package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.minusSettle.MinusSettleCompleteSetDto;
import kr.co.homeplus.settle.admin.model.minusSettle.MinusSettleListGetDto;
import kr.co.homeplus.settle.admin.model.minusSettle.MinusSettleListSetDto;
import kr.co.homeplus.settle.admin.service.MinusSettleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/minusSettle")
@Api(tags = "BackOffice > DS역정산관리")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class MinusSettleController {
    private final MinusSettleService minusSettleService;

    @ApiOperation(value = "DS역정산관리 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getMinusSettleList")
    public ResponseObject<List<MinusSettleListGetDto>> getMinusSettleList(@RequestBody MinusSettleListSetDto minusSettleListSetDto) throws Exception {
        return ResourceConverter.toResponseObject(minusSettleService.getMinusSettleList(minusSettleListSetDto));
    }

    @ApiOperation(value = "DS역정산관리 > 수기마감", response = ResponseObject.class)
    @PostMapping("/setMinusSettleComplete")
    public ResponseObject<Integer> setMinusSettleComplete(@RequestBody MinusSettleCompleteSetDto minusSettleCompleteSetDto){
        return minusSettleService.setMinusSettleComplete(minusSettleCompleteSetDto);
    }
}