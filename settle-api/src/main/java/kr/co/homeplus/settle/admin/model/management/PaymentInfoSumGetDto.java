package kr.co.homeplus.settle.admin.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제일마감")
public class PaymentInfoSumGetDto {
  @ApiModelProperty(notes = "상품구분")
  private String mallType;

  @ApiModelProperty(notes = "점포코드")
  private String originStoreId;

  @ApiModelProperty(notes = "점포명")
  private String originStoreNm;

  @ApiModelProperty(notes = "매출(IN VAT)")
  private String completeAmt;

  @ApiModelProperty(notes = "정산(IN VAT)")
  private String settleAmt;

  @ApiModelProperty(notes = "판매수수료")
  private String saleAgencyFee;

  @ApiModelProperty(notes = "배송비")
  private String shipAmt;

  @ApiModelProperty(notes = "반품배송비")
  private String claimReturnShipAmt;

  @ApiModelProperty(notes = "상품할인(업체)")
  private String couponSellerChargeAmt;

  @ApiModelProperty(notes = "상품할인(자사)")
  private String couponHomeChargeAmt;

  @ApiModelProperty(notes = "상품할인(카드)")
  private String couponCardChargeAmt;

  @ApiModelProperty(notes = "카드할인(자사)")
  private String cardCouponHomeAmt;

  @ApiModelProperty(notes = "카드할인(카드)")
  private String cardCouponCardAmt;

  @ApiModelProperty(notes = "배송비할인")
  private String shipDiscountAmt;

  @ApiModelProperty(notes = "임직원할인")
  private String empDiscountAmt;

  @ApiModelProperty(notes = "행사할인")
  private String promoDiscountAmt;

  @ApiModelProperty(notes = "장바구니할인")
  private String cartCouponAmt;

  @ApiModelProperty(notes = "매출(E-KPI)")
  private String ekpiAmt;

  @ApiModelProperty(notes = "결제금액합계")
  private String orderPrice;

  @ApiModelProperty(notes = "PG")
  private String pgAmt;

  @ApiModelProperty(notes = "DGV")
  private String dgvAmt;

  @ApiModelProperty(notes = "MHP")
  private String mhcAmt;

  @ApiModelProperty(notes = "OCB")
  private String ocbAmt;

  @ApiModelProperty(notes = "마일리지")
  private String mileageAmt;
}
