package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.resaSales.ResaSalesListGetDto;
import kr.co.homeplus.settle.admin.model.resaSales.ResaSalesListSetDto;
import kr.co.homeplus.settle.admin.model.resaSales.ResaSalesUploadSetDto;
import kr.co.homeplus.settle.admin.service.ResaSalesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/ResaSales")
@Api(tags = "BackOffice > 회계자료조회 > SALES 자료조회")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class ResaSalesController {

  private final ResaSalesService resaSalesService;

  @ApiOperation(value = "SALES 자료조회 목록", response = ResponseObject.class, notes = "SALES 자료조회 목록")
  @RequestMapping(value = "/getList", method = {RequestMethod.POST})
  public ResponseObject<List<ResaSalesListGetDto>> getList (@RequestBody @Valid
      ResaSalesListSetDto resaSalesListSetDto) {
    List<ResaSalesListGetDto> resaSalesListGetDto = resaSalesService.getResaSalesList(resaSalesListSetDto);
    return ResourceConverter.toResponseObject(resaSalesListGetDto);
  }

  @ApiOperation(value = "SALES 존재 여부", response = ResponseObject.class, notes = "SALES 존재 여부 체크")
  @RequestMapping(value = "/getExist", method = {RequestMethod.POST})
  public ResponseObject<Integer> getResaSalesExist (@RequestBody @Valid String basicDt) {
    int salesCount = resaSalesService.getResaSalesExist(basicDt);
    return ResourceConverter.toResponseObject(salesCount);
  }

  @ApiOperation(value = "SALES 업로드", response = ResponseObject.class, notes = "SALES 업로드")
  @RequestMapping(value = "/setUpload", method = {RequestMethod.POST})
  public ResponseObject<Integer> setResaSalesUpload (@RequestBody @Valid List<ResaSalesUploadSetDto> resaSalesUploadSetDtoList) {
    int isSuccess = resaSalesService.setResaSalesUpload(resaSalesUploadSetDtoList);
    return ResourceConverter.toResponseObject(isSuccess);
  }
}
