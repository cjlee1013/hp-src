package kr.co.homeplus.settle.partner.model.salesTrend;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("셀러 판매 트렌드 요약 정보")
@Data
public class SalesTrendSummaryInfo {

    @ApiModelProperty("상품 조회 수")
    private long eventCnt;

    @ApiModelProperty("상품 결제 건 수")
    private long paymentCnt;

    @ApiModelProperty("상품 판매 금액")
    private long saleAmt;

    @ApiModelProperty("구매 전환율")
    private float conversionRate;
}
