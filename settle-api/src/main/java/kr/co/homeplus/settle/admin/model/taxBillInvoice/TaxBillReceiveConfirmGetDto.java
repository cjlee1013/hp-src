package kr.co.homeplus.settle.admin.model.taxBillInvoice;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TaxBillReceiveConfirmGetDto {
    @ApiModelProperty(value = "수취ID")
    private String taxReceiveNo;

    @ApiModelProperty(notes = "결과 코드")
    private String resultCd;

    @ApiModelProperty(notes = "결과 상세 메시지")
    private String resultMsg;
}
