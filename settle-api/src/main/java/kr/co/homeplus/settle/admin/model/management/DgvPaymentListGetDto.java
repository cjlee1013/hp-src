package kr.co.homeplus.settle.admin.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "DGV 실적조회 > 건별 상세내역")
public class DgvPaymentListGetDto {
  @ApiModelProperty(notes = "기준일")
  private String basicDt;

  @ApiModelProperty(notes = "가맹점번호")
  private String branchCode;

  @ApiModelProperty(notes = "POS번호")
  private String posNo;

  @ApiModelProperty(notes = "영수증번호")
  private String receiptNo;

  @ApiModelProperty(notes = "결제번호")
  private String paymentNo;

  @ApiModelProperty(notes = "주문번호")
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "클레임번호")
  private String claimNo;

  @ApiModelProperty(notes = "승인번호")
  private String appNo;

  @ApiModelProperty(notes = "취소구분")
  private String gubun;

  @ApiModelProperty(notes = "매출액")
  private String completeAmt;

  @ApiModelProperty(notes = "결제금액")
  private String orderPrice;

  @ApiModelProperty(notes = "DGV 사용금액")
  private String dgvAmt;

  @ApiModelProperty(notes = "GVS 전송금액")
  private String collectAmt;
}
