package kr.co.homeplus.settle.admin.model.accounting;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountingSubjectListGetDto {
    @ApiModelProperty(value = "계정코드")
    @RealGridColumnInfo(headText = "계정코드", width = 150)
    private String accountCd;

    @ApiModelProperty(value = "계정과목명")
    @RealGridColumnInfo(headText = "계정과목명", width = 200)
    private String accountNm;

    @ApiModelProperty(value = "차변")
    @RealGridColumnInfo(headText = "차변", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long accountDr;

    @ApiModelProperty(value = "대변")
    @RealGridColumnInfo(headText = "대변", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long accountCr;

    @ApiModelProperty(value = "합계 (차변-대변)")
    @RealGridColumnInfo(headText = "합계 (차변-대변)", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long accountAmt;

}
