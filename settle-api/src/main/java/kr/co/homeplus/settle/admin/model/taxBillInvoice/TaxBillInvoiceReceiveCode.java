package kr.co.homeplus.settle.admin.model.taxBillInvoice;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum TaxBillInvoiceReceiveCode {
    TAX_BILL_RECEIVE_INICIS("INICIS", "INICIS"),
    TAX_BILL_RECEIVE_TOSS("TOSS", "TOSSPG"),
    TAX_BILL_RECEIVE_NAVER("NAVER", "NAVER"),
    TAX_BILL_RECEIVE_11ST("11ST", "11ST");

    private String code;
    private String name;
}
