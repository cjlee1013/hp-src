package kr.co.homeplus.settle.admin.model.accounting;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class APInvoiceListGetDto {
    @ApiModelProperty(value = "기준일")
    @RealGridColumnInfo(headText = "기준일", width = 100, sortable = true, fieldType = RealGridFieldType.TEXT)
    private String basicDt;

    @ApiModelProperty(notes = "전표유형")
    @RealGridColumnInfo(headText = "전표유형", width = 110, sortable = true)
    private String slipName;

    @ApiModelProperty(notes = "Invoice유형")
    @RealGridColumnInfo(headText = "Invoice유형", width = 100, sortable = true)
    private String invoiceTypeLookupCode;

    @ApiModelProperty(notes = "Invoice번호")
    @RealGridColumnInfo(headText = "Invoice번호", width = 130, sortable = true)
    private String invoiceNum;

    @ApiModelProperty(value = "판매업체ID")
    @RealGridColumnInfo(headText = "판매업체ID", width = 100, sortable = true)
    private String partnerId;

    @ApiModelProperty(value = "업체코드")
    @RealGridColumnInfo(headText = "업체코드", width = 100, sortable = true)
    private String vendorCd;

    @ApiModelProperty(value = "판매업체명")
    @RealGridColumnInfo(headText = "판매업체명", width = 120, sortable = true)
    private String partnerNm;

    @ApiModelProperty(value = "상호")
    @RealGridColumnInfo(headText = "상호명", sortable = true, width = 100)
    private String businessNm;

    @ApiModelProperty(value = "금액")
    @RealGridColumnInfo(headText = "금액", width = 100, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long invoiceAmount;

    @ApiModelProperty(notes = "Invoice 일자")
    @RealGridColumnInfo(headText = "Invoice 일자", width = 110, sortable = true)
    private String invoiceDate;

    @ApiModelProperty(notes = "OF Invoice 일자")
    @RealGridColumnInfo(headText = "OF Invoice 일자", width = 100, sortable = true)
    private String creationDate;

    @ApiModelProperty(notes = "OF 지불일자")
    @RealGridColumnInfo(headText = "OF 지불일자", width = 110, sortable = true)
    private String ofInvoiceDate;

    @ApiModelProperty(value = "지불주기")
    @RealGridColumnInfo(headText = "지불주기", width = 100, sortable = true)
    private String termsName;

    @ApiModelProperty(value = "설명")
    @RealGridColumnInfo(headText = "설명", width = 240, sortable = true)
    private String description;

    @ApiModelProperty(notes = "상태")
    @RealGridColumnInfo(headText = "상태", width = 100, sortable = true)
    private String statusNm;

    @ApiModelProperty(notes = "변경일시")
    @RealGridColumnInfo(headText = "변경일시", width = 160, sortable = true)
    private String chgDt;

    @ApiModelProperty(notes = "변경자")
    @RealGridColumnInfo(headText = "변경자", width = 100, sortable = true)
    private String chgId;

    @ApiModelProperty(notes = "상태")
    @RealGridColumnInfo(headText = "상태", width = 100, hidden = true)
    private String status;

    @ApiModelProperty(notes = "인터페이스번호")
    @RealGridColumnInfo(headText = "인터페이스번호", width = 100, hidden = true)
    private long rfInvoiceId;

    @ApiModelProperty(notes = "담당자이메일")
    @RealGridColumnInfo(headText = "담당자이메일", width = 100, hidden = true)
    private String mngEmail;

}
