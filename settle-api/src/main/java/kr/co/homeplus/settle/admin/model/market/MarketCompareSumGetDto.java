package kr.co.homeplus.settle.admin.model.market;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제일마감")
public class MarketCompareSumGetDto {
  @ApiModelProperty(notes = "일자")
  private String basicDt;

  @ApiModelProperty(notes = "제휴사")
  private String siteType;

  @ApiModelProperty(notes = "홈플러스 결제금액")
  private String homeShipOrderPrice;

  @ApiModelProperty(notes = "홈플러스 취소금액")
  private String homeShipOrderRefPrice;

  @ApiModelProperty(notes = "홈플러스 합계금액")
  private String homeSumAmt;

  @ApiModelProperty(notes = "제휴사 결제금액")
  private String marketShipOrderPrice;

  @ApiModelProperty(notes = "제휴사 취소금액")
  private String marketShipOrderRefPrice;

  @ApiModelProperty(notes = "제휴사 합계금액")
  private String marketSumAmt;

  @ApiModelProperty(notes = "차액 결제금액")
  private String diffShipOrderPrice;

  @ApiModelProperty(notes = "차액 취소금액")
  private String diffShipOrderRefPrice;

  @ApiModelProperty(notes = "차액 합계금액")
  private String diffSumAmt;
}
