package kr.co.homeplus.settle.partner.model.finalSettle;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "정산관리 > 파트너메인 > 대시보드 > 정산예정 금액")
public class PartnerSettleAmtSumGetDto {
    @ApiModelProperty(value = "지급예정금액")
    private long settleAmt;

    @ApiModelProperty(value = "정산관리URL")
    private String partnerSettleMngUrl = "/settle/taxBillList";

}
    
    