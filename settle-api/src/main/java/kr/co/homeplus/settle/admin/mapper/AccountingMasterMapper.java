package kr.co.homeplus.settle.admin.mapper;

import kr.co.homeplus.settle.admin.model.accounting.APInvoiceConfirmGetDto;
import kr.co.homeplus.settle.admin.model.accounting.APInvoiceConfirmSetDto;
import kr.co.homeplus.settle.core.db.annotation.MasterConnection;

@MasterConnection
public interface AccountingMasterMapper {
    APInvoiceConfirmGetDto setAPInvoiceConfirm(APInvoiceConfirmSetDto listParamDto);
}
