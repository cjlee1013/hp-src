package kr.co.homeplus.settle.admin.model.dailyPayment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PaymentInfoSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "매입원가")
    private String orgPrice;

    @ApiModelProperty(value = "상품금액")
    private String itemSaleAmt;

    @ApiModelProperty(value = "점포코드")
    private String originStoreId;

    @ApiModelProperty(value = "거래유형")
    private String mallType;

    @ApiModelProperty(value = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "사이트타입")
    private String siteType;
}
