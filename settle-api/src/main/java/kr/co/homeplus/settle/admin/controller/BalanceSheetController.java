package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetDetailGetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetDetailSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetInterfaceGetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetInterfaceSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetListGetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetListSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetSlipTypeGetDto;
import kr.co.homeplus.settle.admin.service.BalanceSheetService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/balanceSheet")
@Api(tags = "BackOffice > 전표조회")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class BalanceSheetController {

  private final BalanceSheetService balanceSheetService;

  @ApiOperation(value = "전표조회 목록", response = ResponseObject.class, notes = "전표조회 목록")
  @RequestMapping(value = "/getList", method = {RequestMethod.POST})
  public ResponseObject<List<BalanceSheetListGetDto>> getList (@RequestBody @Valid
      BalanceSheetListSetDto balanceSheetListSetDto) {
    List<BalanceSheetListGetDto> balanceSheetListGetDto = balanceSheetService.getBalanceSheetList(balanceSheetListSetDto);
    return ResourceConverter.toResponseObject(balanceSheetListGetDto);
  }

  @ApiOperation(value = "전표조회 상세", response = ResponseObject.class, notes = "전표조회 상세")
  @RequestMapping(value = "/getDetail", method = {RequestMethod.POST})
  public ResponseObject<List<BalanceSheetDetailGetDto>> getDetail (@RequestBody @Valid BalanceSheetDetailSetDto balanceSheetDetailSetDto) {
    List<BalanceSheetDetailGetDto> balanceSheetDetailGetDto = balanceSheetService.getBalanceSheetDetail(balanceSheetDetailSetDto);
    return ResourceConverter.toResponseObject(balanceSheetDetailGetDto);
  }

  @ApiOperation(value = "전표조회 유형", response = ResponseObject.class, notes = "전표조회 유형")
  @RequestMapping(value = "/getSlipTypeList", method = {RequestMethod.GET})
  public ResponseObject<List<BalanceSheetSlipTypeGetDto>> getSlipTypeList () {
    List<BalanceSheetSlipTypeGetDto> balanceSheetListGetDto = balanceSheetService.getSlipTypeList();
    return ResourceConverter.toResponseObject(balanceSheetListGetDto);
  }

  @ApiOperation(value = "전표조회 IF", response = ResponseObject.class, notes = "전표조회 IF")
  @RequestMapping(value = "/setInterface", method = {RequestMethod.POST})
  public ResponseObject<BalanceSheetInterfaceGetDto> setInterface (@RequestBody @Valid BalanceSheetInterfaceSetDto balanceSheetInterfaceSetDto) {
    BalanceSheetInterfaceGetDto balanceSheetInterfaceGetDto = balanceSheetService.setBalanceSheetInterface(balanceSheetInterfaceSetDto);
    return ResourceConverter.toResponseObject(balanceSheetInterfaceGetDto);
  }
}
