package kr.co.homeplus.settle.admin.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.settle.admin.mapper.TaxBillInvoiceSlaveMapper;
import kr.co.homeplus.settle.admin.mapper.TaxBillInvoiceMasterMapper;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillInvoiceListGetDto;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillInvoiceListSetDto;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillInvoiceReceiveCode;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillReceiveConfirmGetDto;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillReceiveListGetDto;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillReceiveSetDto;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillReceiveSupplierGetDto;
import kr.co.homeplus.settle.core.exception.handler.ExceptionCode;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@RequiredArgsConstructor
public class TaxBillInvoiceService {
    private final TaxBillInvoiceSlaveMapper TaxBillInvoiceSlaveMapper;
    private final TaxBillInvoiceMasterMapper TaxBillInvoiceMasterMapper;
    private final PgReceivableService pgReceivableService;
    private final RefitCryptoService cryptoService;

    /**
     * 세금계산서 관리 > 세금계산서 조회 > 메인 리스트 조회
     */
    public List<TaxBillInvoiceListGetDto> getTaxBillInvoiceList(TaxBillInvoiceListSetDto listParamDto) throws Exception {
        if (listParamDto.getMngEmail() != null && listParamDto.getMngEmail() != "") {
            listParamDto.setOrgMngEmail(listParamDto.getMngEmail());
            listParamDto.setMngEmail(cryptoService.encrypt(listParamDto.getMngEmail()));
        }

        List<TaxBillInvoiceListGetDto> returnDto = TaxBillInvoiceSlaveMapper.getTaxBillInvoiceList(listParamDto);
        for (TaxBillInvoiceListGetDto dto : returnDto) {
            dto.setPartnerId(PrivacyMaskingUtils.maskingUserId(dto.getPartnerId()));
            if (dto.getMngId() != null && dto.getMngId().length() > 0) {
                dto.setMngId(PrivacyMaskingUtils.maskingUserName(dto.getMngId()));
            }

            try {
                Pattern emailPattern = Pattern.compile(AccountingService.EMAIL_PATTERN); //이메일 패턴 체크

                if (!emailPattern.matcher(dto.getMngEmail()).matches()) {
                    dto.setMngEmail(cryptoService.decrypt(dto.getMngEmail()));
                }
            } catch (Exception e) {
                dto.setMngEmail("");
            }

            if (dto.getMngEmail() != null && dto.getMngEmail().length() > 0) {
                dto.setMngEmail(PrivacyMaskingUtils.maskingEmail(dto.getMngEmail()));
            }

        }
        return returnDto;
    }


    /**
     * 세금계산서관리 > 수취내역 관리 리스트 조회
     */
    public List<TaxBillReceiveListGetDto> getTaxBillReceiveList(TaxBillInvoiceListSetDto listParamDto) throws Exception {
        return TaxBillInvoiceSlaveMapper.getTaxBillReceiveList(listParamDto);
    }


    /**
     * 세금계산서관리 > 수취내역 등록
     */
    public ResponseObject<String> setTaxBillReceiveList(TaxBillReceiveSetDto listParamDto) throws Exception {
        String resultMsg = "", returnCode = "";
        try {
            TaxBillReceiveConfirmGetDto returnDto = TaxBillInvoiceMasterMapper.setTaxBillReceiveList(listParamDto);

            resultMsg =  returnDto.getResultMsg();
            returnCode = returnDto.getResultCd();
        }
        catch (Exception e) {
            resultMsg +=  ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage() + e.toString();
            returnCode = ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode();
        }

        return ResourceConverter.toResponseObject(resultMsg, HttpStatus.OK, returnCode, resultMsg);
    }


    /**
     * 세금계산서관리 > 수취내역 수정
     */
    public ResponseObject<String> updTaxBillReceive(TaxBillReceiveSetDto listParamDto) throws Exception {
        String resultMsg = "", returnCode = "";
        try {
            TaxBillReceiveConfirmGetDto returnDto = TaxBillInvoiceMasterMapper.updTaxBillReceive(listParamDto);

            resultMsg =  returnDto.getResultMsg();
            returnCode = returnDto.getResultCd();
        }
        catch (Exception e) {
            resultMsg =  ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage();
            returnCode = ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode();
        }

        return ResourceConverter.toResponseObject(resultMsg, HttpStatus.OK, returnCode, resultMsg);
    }


    /**
     * 세금계산서관리 > 수취내역 확정
     */
    public ResponseObject<String> setTaxBillReceiveConfirm(TaxBillReceiveSetDto listParamDto) throws Exception {
        String resultMsg = "", returnCode = "";
        try {
            String[] _taxReceiveNo = listParamDto.getTaxReceiveNo().split(",");
            boolean isLastPGReceive = false;

            for (int i=0; i < _taxReceiveNo.length; i++) {
                listParamDto.setTaxReceiveNo(_taxReceiveNo[i]);
                //상세 정보 조회
                TaxBillReceiveListGetDto taxBillInfo = getTaxBillReceiveDetail(listParamDto);
                listParamDto.setIssueDt(taxBillInfo.getIssueDt());

                if (taxBillInfo.getTaxBillReceiveType().equals("PG")) { //PG수수료
                    TaxBillReceiveConfirmGetDto returnDto = TaxBillInvoiceMasterMapper.setTaxBillReceiveConfirm(listParamDto.getTaxReceiveNo(), listParamDto.getRegId());

                    resultMsg = returnDto.getResultMsg();
                    returnCode = returnDto.getResultCd();
                    isLastPGReceive = true;

                    if (taxBillInfo.getIssueDt() != null) {
                        pgReceivableService.setPgFeeAllocation(taxBillInfo.getIssueDt().replace("-",""), taxBillInfo.getPartnerId(), taxBillInfo.getSumAmt());
                    }

                } else if (taxBillInfo.getTaxBillReceiveType().equals("MK") || taxBillInfo.getTaxBillReceiveType().equals("MR") || taxBillInfo.getTaxBillReceiveType().equals("MD") ) { //마켓 제휴수수료
                    TaxBillReceiveConfirmGetDto returnDto = TaxBillInvoiceMasterMapper.setSlipTaxReceiveMarket(listParamDto.getTaxReceiveNo(), listParamDto.getRegId());

                    resultMsg = returnDto.getResultMsg();
                    returnCode = returnDto.getResultCd();
                } else {
                    resultMsg = taxBillInfo.getTaxBillReceiveType() + ExceptionCode.ERROR_CODE_1001.getResponseMessage();
                    returnCode = ExceptionCode.ERROR_CODE_1001.getResponseCode();
                }
            }

            if (isLastPGReceive) { //PG수수료
                //GL 호출
                insertPGFeeBalanceSheet(listParamDto.getIssueDt());
            }
        }
        catch (Exception e) {
            resultMsg +=  ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage() + e.toString();
            returnCode = ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode();
        }

        return ResourceConverter.toResponseObject(listParamDto.getTaxReceiveNo(), HttpStatus.OK, returnCode, resultMsg);
    }


    /**
     * 세금계산서관리 > 수취내역 상세조회
     */
    public TaxBillReceiveListGetDto getTaxBillReceiveDetail(TaxBillReceiveSetDto listParamDto) throws Exception {
        TaxBillReceiveListGetDto returnDto = TaxBillInvoiceMasterMapper.getTaxBillReceiveDetail(listParamDto);

        //TOSSPG
        if(returnDto.getPartnerId().equals(TaxBillInvoiceReceiveCode.TAX_BILL_RECEIVE_TOSS.getCode())) {
            returnDto.setPartnerId(TaxBillInvoiceReceiveCode.TAX_BILL_RECEIVE_TOSS.getName());
        }

        return returnDto;
    }


    /**
     * 세금계산서 수취내역 > 공급처 정보
     *
     ## 세금계산서 수취 업체를 판매업체를 등록하지않고 시스템관리 > 공통코드에 서버별 세금계산서 수취 정보 저장 (taxbill_receive_type)
     ## PG코드에 _VENDOR 붙여서 코드 분류 / 등록 순서 반드시 지켜야함
     ## 예) 이니시스 - 'INICIS' 파트너정보, 'INICIS_VENDOR' AP INVOICE 판매업체코드정보
     */
    public List<TaxBillReceiveSupplierGetDto> getTaxBillSupplierList(String taxBillReceiveType, String partnerId) throws Exception {
        return TaxBillInvoiceSlaveMapper.getTaxBillSupplierList(taxBillReceiveType, partnerId);
    }

    /**
     * 세금계산서관리 > 수취내역 삭제
     */
    public ResponseObject<String> deleteTaxBillReceive(TaxBillReceiveSetDto listParamDto) throws Exception {
        String resultMsg = "", returnCode = "";
        try {
            TaxBillInvoiceMasterMapper.deleteTaxBillReceive(listParamDto);
        }
        catch (Exception e) {
            resultMsg =  ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage();
            returnCode = ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode();
        }

        return ResourceConverter.toResponseObject(listParamDto.getTaxReceiveNo(), HttpStatus.OK, returnCode, resultMsg);
    }

    /**
     * GL_PG_FEE_ALLOCATION 호출
     */
    public void insertPGFeeBalanceSheet(@RequestParam(name ="basicDt") String basicDt) throws Exception {
        TaxBillInvoiceMasterMapper.insertPGFeeBalanceSheet(basicDt);
    }
}
