package kr.co.homeplus.settle.admin.model.adjust;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AdjustTypeInfo {
    @ApiModelProperty(notes = "유형", position = 1)
    private String adjustType;

    @ApiModelProperty(notes = "유형명", position = 2)
    private String adjustTypeNm;

    @ApiModelProperty(notes = "구분", position = 3)
    private String gubun;

    @ApiModelProperty(notes = "조정유형 금액", position = 4) //adjustAmt, adjustFee
    private String adjustInput;

    @ApiModelProperty(notes = "세금계산서 대상여부", position = 5)
    private String taxType;
}
