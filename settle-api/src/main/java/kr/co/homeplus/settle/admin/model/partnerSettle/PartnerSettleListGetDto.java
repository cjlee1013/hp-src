package kr.co.homeplus.settle.admin.model.partnerSettle;
    
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridButtonType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "정산관리 > 정산현황")
public class PartnerSettleListGetDto {
    @ApiModelProperty(value = "처리")
    @RealGridColumnInfo(headText = "처리", sortable = true, width = 150, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BUTTON, alwaysShowButton = true, buttonType = RealGridButtonType.ACTION)
    private String actionType;

    @ApiModelProperty(value = "정산번호")
    @RealGridColumnInfo(headText = "정산번호", sortable = true, width = 100, hidden = true)
    private String settleNo;

    @ApiModelProperty(value = "정산상태")
    @RealGridColumnInfo(headText = "정산상태", sortable = true, width = 100)
    private String settleState;

    @ApiModelProperty(value = "지급SRL")
    @RealGridColumnInfo(headText = "지급SRL", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER)
    private String settleSrl;

    @ApiModelProperty(value = "지급예정일")
    @RealGridColumnInfo(headText = "지급예정일", sortable = true, width = 150)
    private String settlePreDt;

    @ApiModelProperty(value = "지급방법")
    @RealGridColumnInfo(headText = "지급방법", sortable = true, width = 100)
    private String payMethodNm;

    @ApiModelProperty(value = "정산주기")
    @RealGridColumnInfo(headText = "정산주기", sortable = true, width = 100)
    private String settleCycleType;

    @ApiModelProperty(value = "집계기간")
    @RealGridColumnInfo(headText = "집계기간", sortable = true, width = 150)
    private String salesPeriod;

    @ApiModelProperty(value = "판매업체ID")
    @RealGridColumnInfo(headText = "판매업체ID", sortable = true, width = 100)
    private String partnerId;

    @ApiModelProperty(value = "업체코드")
    @RealGridColumnInfo(headText = "업체코드", sortable = true, width = 100)
    private String vendorCd;

    @ApiModelProperty(value = "판매업체명")
    @RealGridColumnInfo(headText = "판매업체명", sortable = true, width = 150)
    private String partnerNm;

    @ApiModelProperty(value = "상호")
    @RealGridColumnInfo(headText = "상호명", sortable = true, width = 100)
    private String businessNm;

    @ApiModelProperty(value = "매출집계금액")
    @RealGridColumnInfo(headText = "매출집계금액", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipCompleteAmt;

    @ApiModelProperty(value = "배송비")
    @RealGridColumnInfo(headText = "배송비", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipAmt;

    @ApiModelProperty(value = "반품배송비")
    @RealGridColumnInfo(headText = "반품배송비", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimReturnShipAmt;

    @ApiModelProperty(value = "판매수수료")
    @RealGridColumnInfo(headText = "판매수수료 (EX VAT)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long saleAgencyFeeExVat;

    @ApiModelProperty(value = "판매수수료")
    @RealGridColumnInfo(headText = "판매수수료 (VAT)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long saleAgencyFeeVat;

    @ApiModelProperty(value = "판매수수료")
    @RealGridColumnInfo(headText = "판매수수료 (IN VAT)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long saleAgencyFee;

    @ApiModelProperty(value = "정산금액")
    @RealGridColumnInfo(headText = "정산금액", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long settleSalesAmt;

    @ApiModelProperty(value = "자사할인")
    @RealGridColumnInfo(headText = "자사할인", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponHomeChargeAmt;

    @ApiModelProperty(value = "상품할인 (업체)")
    @RealGridColumnInfo(headText = "상품할인 (업체)", width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponSellerChargeAmt;

    @ApiModelProperty(value = "배송비할인 (업체)")
    @RealGridColumnInfo(headText = "배송비할인 (업체)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipDiscountAmt;

    @ApiModelProperty(value = "조정금액")
    @RealGridColumnInfo(headText = "조정금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long adjustAmt;

    @ApiModelProperty(value = "조정금액(선수금)")
    @RealGridColumnInfo(headText = "조정금액(선수금)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long adjustAdvAmt;

    @ApiModelProperty(value = "광고수수료")
    @RealGridColumnInfo(headText = "광고수수료", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long adjustFee;

    @ApiModelProperty(value = "광고수수료(선수금)")
    @RealGridColumnInfo(headText = "광고수수료(선수금)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long adjustAdvFee;

    @ApiModelProperty(value = "정산대상금액")
    @RealGridColumnInfo(headText = "정산대상금액", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long settlePreAmt;

    @ApiModelProperty(value = "정산예정금액")
    @RealGridColumnInfo(headText = "정산예정금액", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long finalSettleAmt;

    @ApiModelProperty(value = "이전보류금액")
    @RealGridColumnInfo(headText = "이전보류금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long holdAmt;

    @ApiModelProperty(value = "지급예정금액")
    @RealGridColumnInfo(headText = "지급예정금액", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long settleAmt;

    @ApiModelProperty(value = "보류발생금액")
    @RealGridColumnInfo(headText = "보류발생금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long settleHoldAmt;

    @ApiModelProperty(value = "실지급액")
    @RealGridColumnInfo(headText = "실지급액", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String settleCompleteAmt;

    @ApiModelProperty(value = "수정일시")
    @RealGridColumnInfo(headText = "수정일시", sortable = true, width = 160, fieldType = RealGridFieldType.TEXT)
    private String chgDt;

    @ApiModelProperty(value = "수정자")
    @RealGridColumnInfo(headText = "수정자", sortable = true, width = 150)
    private String chgId;

}
    
    