package kr.co.homeplus.settle.batch.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
@ConfigurationProperties(prefix = "opmall")
public class OpmallConfig {
    private String iniRegisterUrl;
    private String iniRequestUrl;
    private String iniRefundUrl;
    private String iniFundsUrl;
    private String iniResultUrl;
    private String iniMid;
}
