package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetDetailGetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetDetailSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetListGetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetListSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetSlipTypeGetDto;
import kr.co.homeplus.settle.admin.model.pgReceivable.PgCommissionSetDto;
import kr.co.homeplus.settle.admin.model.pgReceivable.PgReceivableGetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface PgReceivableSlaveMapper {
  long getPgCommission(PgCommissionSetDto pgCommissionSetDto);
  PgReceivableGetDto getPgBalanceAmt(String pgKind);
}
