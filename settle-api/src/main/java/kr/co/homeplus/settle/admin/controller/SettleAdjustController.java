package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.adjust.AdjustItemGetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustListSetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustListGetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustRegisterGetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustRegisterSetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustTypeInfo;
import kr.co.homeplus.settle.admin.service.SettleAdjustService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/adjust")
@Api(tags = "BackOffice > 정산조정")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class SettleAdjustController {
    private final SettleAdjustService settleService;

    @ApiOperation(value = "정산조정 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getAdjustList")
    public ResponseObject<List<AdjustListGetDto>> getAdjustList(@RequestBody AdjustListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(settleService.getAdjustList(listParamDto));
    }

    @ApiOperation(value = "정산조정 > 셀러상품명 조회", response = ResponseObject.class)
    @PostMapping("/getAdjustItem")
    public ResponseObject<AdjustItemGetDto> getAdjustItem(@RequestParam(name ="itemNo") String itemNo) throws Exception {
        return settleService.getAdjustItem(itemNo);
    }

    @ApiOperation(value = "정산조정 > 정산조정 입력", response = ResponseObject.class)
    @PostMapping("/insertAdjust")
    public ResponseObject<String> insertAdjust(@RequestBody AdjustRegisterSetDto listParamDto) throws Exception {
        return settleService.insertAdjust(listParamDto);
    }

    @ApiOperation(value = "정산조정 > 정산조정 유형 조회", response = ResponseObject.class)
    @GetMapping("/getAdjustType")
    public ResponseObject<List<AdjustTypeInfo>> getAdjustType(@RequestParam(name = "adjustType", required = false, defaultValue = "") String adjustType) throws Exception {
        return ResourceConverter.toResponseObject(settleService.getAdjustType(adjustType));
    }

    @ApiOperation(value = "정산조정 > 정산조정 입력", response = ResponseObject.class)
    @PostMapping("/deleteAdjust")
    public ResponseObject<String> deleteAdjust(@RequestBody AdjustListGetDto listParamDto) throws Exception {
        return settleService.deleteAdjust(listParamDto);
    }
}