package kr.co.homeplus.settle.admin.model.partnerPayment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class PartnerSettlePreDtGetDto {
    @ApiModelProperty(notes = "지급SRL")
    private long settleSrl;

    @ApiModelProperty(notes = "결과 코드")
    private String returnCode;

    @ApiModelProperty(notes = "결과 상세 메시지")
    private String returnMsg;
}
