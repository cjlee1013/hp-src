package kr.co.homeplus.settle.admin.mapper;

import kr.co.homeplus.settle.admin.model.partnerSettle.PartnerSettleCommonSetDto;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerSettlePreDtGetDto;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerSettlePreDtSetDto;
import kr.co.homeplus.settle.admin.model.partnerSettle.PartnerSettleStateGetDto;
import kr.co.homeplus.settle.admin.model.partnerSettle.PartnerSettleStateSetDto;
import kr.co.homeplus.settle.core.db.annotation.MasterConnection;
import org.apache.ibatis.annotations.Param;

@MasterConnection
public interface PartnerSettleMasterMapper {
    PartnerSettleStateGetDto getPartnerSettleState(PartnerSettleCommonSetDto listParamDto);

    int setPartnerSettleState(PartnerSettleStateSetDto listParamDto);

    int insertPartnerSettleHist(PartnerSettleStateSetDto listParamDto);

    int updatePartnerSettleHold(PartnerSettleStateSetDto listParamDto);

    int updateMinusBank(@Param("settleSrl") String settleSrl, @Param("addType") String addType);

    int insertMinusBankHistory(@Param("settleSrl") String settleSrl, @Param("chgId") String chgId);

    PartnerSettlePreDtGetDto setSettlePreDt(PartnerSettlePreDtSetDto listParamDto);
}
