package kr.co.homeplus.settle.admin.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "MHC 실적조회 > 일별 상세내역")
public class MhcPaymentDailyGetDto {
  @ApiModelProperty(notes = "기준일")
  private String basicDt;

  @ApiModelProperty(notes = "점포유형")
  private String storeType;

  @ApiModelProperty(notes = "점포코드")
  private String storeId;

  @ApiModelProperty(notes = "점포명")
  private String storeNm;

  @ApiModelProperty(notes = "코스트센터")
  private String onlineCostCenter;

  @ApiModelProperty(notes = "매출액")
  private String completeAmt;

  @ApiModelProperty(notes = "결제금액")
  private String orderPrice;

  @ApiModelProperty(notes = "사용 포인트")
  private String mhcUsePoint;

  @ApiModelProperty(notes = "승인 포인트")
  private String mhcAprPoint;

  @ApiModelProperty(notes = "적립 포인트")
  private String mhcSavePoint;
}
