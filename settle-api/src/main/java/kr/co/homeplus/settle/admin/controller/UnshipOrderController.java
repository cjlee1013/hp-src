package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.unshipOrder.TdUnshipOrderListGetDto;
import kr.co.homeplus.settle.admin.model.unshipOrder.TdUnshipOrderListSetDto;
import kr.co.homeplus.settle.admin.model.unshipOrder.UnshipOrderListGetDto;
import kr.co.homeplus.settle.admin.model.unshipOrder.UnshipOrderListSetDto;
import kr.co.homeplus.settle.admin.service.UnshipOrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/unshipOrder")
@Api(tags = "BackOffice > 미배송 주문조회")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class UnshipOrderController {
    private final UnshipOrderService unshipOrderService;

    @ApiOperation(value = "미배송 주문조회 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getUnshipOrderList")
    public ResponseObject<List<UnshipOrderListGetDto>> getUnshipOrderList(@RequestBody UnshipOrderListSetDto unshipOrderListSetDto) throws Exception {
        return ResourceConverter.toResponseObject(unshipOrderService.getUnshipOrderList(unshipOrderListSetDto));
    }

    @ApiOperation(value = "TD미배송 주문조회 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getTdUnshipOrderList")
    public ResponseObject<List<TdUnshipOrderListGetDto>> getTdUnshipOrderList(@RequestBody TdUnshipOrderListSetDto tdUnshipOrderListSetDto) throws Exception {
        return ResourceConverter.toResponseObject(unshipOrderService.getTdUnshipOrderList(tdUnshipOrderListSetDto));
    }
}