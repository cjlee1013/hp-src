package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.pgCompare.PgCompareCostCommissionSetDto;
import kr.co.homeplus.settle.admin.model.pgCompare.PgCompareCostDiffGetDto;
import kr.co.homeplus.settle.admin.model.pgCompare.PgCompareCostSumGetDto;
import kr.co.homeplus.settle.admin.model.pgCompare.PgCompareCostSetDto;
import kr.co.homeplus.settle.admin.service.PgCompareService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/pgCompare")
@Api(tags = "BackOffice > 결제관리 > PG대사")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class PgCompareController {

  private final PgCompareService pgCompareService;

  @ApiOperation(value = "PG정산대사 PG사별 합계내역", response = ResponseObject.class, notes = "PG사별 합계내역")
  @RequestMapping(value = "/getPgCompareCostSum", method = {RequestMethod.POST})
  public ResponseObject<List<PgCompareCostSumGetDto>> getPgCompareCostSum (@RequestBody @Valid
      PgCompareCostSetDto pgCompareCostSetDto) {
    List<PgCompareCostSumGetDto> pgCompareCostSumGetDtoList = pgCompareService.getPgCompareCostSum(
        pgCompareCostSetDto);
    return ResourceConverter.toResponseObject(pgCompareCostSumGetDtoList);
  }
  @ApiOperation(value = "PG정산대사 차이 상세내역", response = ResponseObject.class, notes = "차이 상세내역")
  @RequestMapping(value = "/getPgCompareCostDiff", method = {RequestMethod.POST})
  public ResponseObject<List<PgCompareCostDiffGetDto>> getPgCompareCostDiff (@RequestBody @Valid
      PgCompareCostSetDto pgCompareCostSetDto) {
    List<PgCompareCostDiffGetDto> pgCompareCostDiffGetDtoList = pgCompareService.getPgCompareCostDiff(
        pgCompareCostSetDto);
    return ResourceConverter.toResponseObject(pgCompareCostDiffGetDtoList);
  }

  @ApiOperation(value = "PG수수료대사 차이 상세내역", response = ResponseObject.class, notes = "차이 상세내역")
  @RequestMapping(value = "/getPgCompareCostCommission", method = {RequestMethod.POST})
  public ResponseObject<List<PgCompareCostDiffGetDto>> getPgCompareCostCommission (@RequestBody @Valid
      PgCompareCostCommissionSetDto pgCompareCostCommissionSetDto) {
    List<PgCompareCostDiffGetDto> pgCompareCostDiffGetDtoList = pgCompareService.getPgCompareCostCommission(
        pgCompareCostCommissionSetDto);
    return ResourceConverter.toResponseObject(pgCompareCostDiffGetDtoList);
  }
}
