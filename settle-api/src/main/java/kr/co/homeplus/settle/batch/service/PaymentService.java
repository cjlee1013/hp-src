package kr.co.homeplus.settle.batch.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import kr.co.homeplus.settle.admin.model.pgReceivable.PgReceivableGetDto;
import kr.co.homeplus.settle.batch.config.OpmallConfig;
import kr.co.homeplus.settle.batch.enums.OpmallType;
import kr.co.homeplus.settle.batch.mapper.PaymentMasterMapper;
import kr.co.homeplus.settle.batch.mapper.PaymentSlaveMapper;
import kr.co.homeplus.settle.batch.model.BankCodeDto;
import kr.co.homeplus.settle.batch.model.OpmallHistorySetDto;
import kr.co.homeplus.settle.batch.model.OpmallRegistSetDto;
import kr.co.homeplus.settle.batch.model.OpmallRequestSetDto;
import kr.co.homeplus.settle.batch.model.OpmallResultSetDto;
import kr.co.homeplus.settle.batch.model.PartnerInfoDto;
import kr.co.homeplus.settle.batch.model.PaymentRequestDto;
import kr.co.homeplus.settle.batch.model.PaymentResultDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

/**
 * settle service interface
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class PaymentService {
    private final PaymentMasterMapper paymentMasterMapper;
    private final PaymentSlaveMapper paymentSlaveMapper;
    private final RefitCryptoService cryptoService;
    private static Map<String, BankCodeDto> bankCodeMap;
    private final OpmallConfig opmallConfig;

    @Autowired
    ResourceClient resourceClient;

    // 파트너 변경사항 지급대행 등록
    public int setChangePartner() {
        List<PartnerInfoDto> partnerList = paymentSlaveMapper.getChangePartnerList();
        ResponseObject<String> result;
        String resultCode,resultMsg,bankCode,bankAccount,partnerNo,phoneNo;
        OpmallRegistSetDto opmallRegistSetDto;
        OpmallHistorySetDto opmallHistorySetDto;
        int success = 0;
        for (PartnerInfoDto partner : partnerList) {
            try {
                partnerNo = partner.getPartnerNo().replaceAll("-","");
                phoneNo = partner.getPhoneNo().replaceAll("-","");
                bankCode = getBankCodeMap().get(partner.getBankCode()).getInicisBankCd();
                bankAccount = cryptoService.decrypt(partner.getBankAccount());
                opmallRegistSetDto = new OpmallRegistSetDto(partner.getPartnerId(), partnerNo
                    , partner.getPartnerNm() , partner.getPartnerOwner()
                    , partner.getBankAccountHolder(), bankCode, bankAccount, phoneNo);
                opmallHistorySetDto = new OpmallHistorySetDto(OpmallType.UPDATE,opmallConfig,opmallRegistSetDto);
                result = resourceClient.postForResponseObject("payment"
                    , opmallRegistSetDto, "/opmall/updatePartner"
                    , new ParameterizedTypeReference<ResponseObject<String>>() {});
                if (result != null) {
                    resultCode = result.getData();
                    resultMsg = result.getReturnMessage();
                    setPaymentHistory(resultCode,resultMsg,opmallHistorySetDto);
                    if (!"00".equals(resultCode)) {
                        log.info(resultMsg);
                    }
                    // 미등록된 파트너의 경우 신규등록처리
                    if ("04".equals(resultCode)) {
                        opmallHistorySetDto = new OpmallHistorySetDto(OpmallType.NEW,opmallConfig,opmallRegistSetDto);
                        result = resourceClient.postForResponseObject("payment"
                            , opmallRegistSetDto, "/opmall/registPartner"
                            , new ParameterizedTypeReference<ResponseObject<String>>() {
                            });
                        if (result != null) {
                            resultCode = result.getData();
                            resultMsg = result.getReturnMessage();
                            if (!"00".equals(resultCode)) {
                                log.info(resultMsg);
                            }
                        } else {
                            resultCode = "-1";
                            resultMsg = "연결오류";
                        }
                        setPaymentHistory(resultCode,resultMsg,opmallHistorySetDto);
                    }
                } else {
                    resultCode = "-1";
                    resultMsg = "연결오류";
                    setPaymentHistory(resultCode,resultMsg,opmallHistorySetDto);
                }
            } catch (Exception e) {
                resultCode = "99";
            }
            partner.setResultCode(resultCode);
            if ("00".equals(resultCode)) {
                success++;
            }
            paymentMasterMapper.setChangePartner(partner);
        }
        return success;
    }

    // 신규파트너 지급대행 등록
    public int setNewPartner() {
        List<PartnerInfoDto> partnerList = paymentSlaveMapper.getNewPartnerList();
        ResponseObject<String> result;
        String resultCode,resultMsg,bankCode,bankAccount,partnerNo,phoneNo;
        OpmallRegistSetDto opmallRegistSetDto;
        OpmallHistorySetDto opmallHistorySetDto;
        int success = 0;
        for (PartnerInfoDto partner : partnerList) {
            try {
                partnerNo = partner.getPartnerNo().replaceAll("-","");
                phoneNo = partner.getPhoneNo().replaceAll("-","");
                bankCode = getBankCodeMap().get(partner.getBankCode()).getInicisBankCd();
                bankAccount = cryptoService.decrypt(partner.getBankAccount());
                opmallRegistSetDto = new OpmallRegistSetDto(partner.getPartnerId(), partnerNo
                    , partner.getPartnerNm(), partner.getPartnerOwner()
                    , partner.getBankAccountHolder(), bankCode, bankAccount, phoneNo);
                opmallHistorySetDto = new OpmallHistorySetDto(OpmallType.NEW,opmallConfig,opmallRegistSetDto);
                result = resourceClient.postForResponseObject("payment"
                    , opmallRegistSetDto, "/opmall/registPartner"
                    , new ParameterizedTypeReference<ResponseObject<String>>() {});
                if (result != null) {
                    resultCode = result.getData();
                    resultMsg = result.getReturnMessage();
                    setPaymentHistory(resultCode,resultMsg,opmallHistorySetDto);
                    if (!"00".equals(resultCode)) {
                        log.info(resultMsg);
                    }
                    // 기등록된 파트너의 경우 업데이트처리
                    if ("04".equals(resultCode)) {
                        opmallHistorySetDto = new OpmallHistorySetDto(OpmallType.UPDATE,opmallConfig,opmallRegistSetDto);
                        result = resourceClient.postForResponseObject("payment"
                            , opmallRegistSetDto, "/opmall/updatePartner"
                            , new ParameterizedTypeReference<ResponseObject<String>>() {
                            });
                        if (result != null) {
                            resultCode = result.getData();
                            resultMsg = result.getReturnMessage();
                            if (!"00".equals(resultCode)) {
                                log.info(resultMsg);
                            }
                        } else {
                            resultCode = "-1";
                            resultMsg = "연결오류";
                        }
                        setPaymentHistory(resultCode,resultMsg,opmallHistorySetDto);
                    }
                } else {
                    resultCode = "-1";
                    resultMsg = "연결오류";
                    setPaymentHistory(resultCode,resultMsg,opmallHistorySetDto);
                }
            } catch (Exception e) {
                resultCode = "99";
            }
            partner.setResultCode(resultCode);
            if ("00".equals(resultCode)){
                partner.setRegistYn("Y");
                success++;
            } else {
                partner.setRegistYn("N");
            }
            paymentMasterMapper.setNewPartner(partner);
        }
        return success;
    }

    // 지급대행 호출
    public int setPaymentRequest() {
        int success = 0;
        List<PaymentRequestDto> partnerPaymentList = paymentSlaveMapper.getPartnerPaymentRequestList();
        success = setPaymentRequest(partnerPaymentList);

        // 홈플러스 입금 금액은 남은 잔액만 요청
        String pgKind = "INICIS";
        long transAmt;
        PgReceivableGetDto pgBalanceGetDto = paymentMasterMapper.getPgBalanceAmt(pgKind);
        if (pgBalanceGetDto != null) {
            transAmt = (-1) * pgBalanceGetDto.getTransAmt();
            if (transAmt > 0) {
                PaymentRequestDto paymentRequestDto = paymentSlaveMapper.getHomeplusPaymentRequestList();
                paymentRequestDto.setPaymentAmt(String.valueOf(transAmt));

                List<PaymentRequestDto> homeplusPaymentList = new ArrayList<>();
                homeplusPaymentList.add(paymentRequestDto);
                success = success + setPaymentRequest(homeplusPaymentList);
            }
        }
        return success;
    }

    // 지급대행 API 호출
    private int setPaymentRequest(List<PaymentRequestDto> partnerPaymentList) {
        String resultCode,resultMsg,bankCode,bankAccount,partnerNo,phoneNo;
        OpmallRequestSetDto opmallRequestSetDto;
        OpmallRegistSetDto opmallRegistSetDto;
        OpmallHistorySetDto opmallHistorySetDto;
        ResponseObject<String> result;
        int success = 0;
        for (PaymentRequestDto payment : partnerPaymentList) {
            if (!"00".equals(payment.getResultCode())) {
                paymentMasterMapper.setPartnerPaymentFail(payment);
                continue;
            }
            try {
                partnerNo = payment.getPartnerNo().replaceAll("-","");
                phoneNo = payment.getPhoneNo().replaceAll("-","");
                bankCode = getBankCodeMap().get(payment.getBankCode()).getInicisBankCd();
                bankAccount = cryptoService.decrypt(payment.getBankAccount());

                opmallRegistSetDto = new OpmallRegistSetDto(payment.getPartnerId(), partnerNo
                    , payment.getPartnerNm() , payment.getPartnerOwner()
                    , payment.getBankAccountHolder(), bankCode, bankAccount, phoneNo);
                opmallHistorySetDto = new OpmallHistorySetDto(OpmallType.UPDATE,opmallConfig,opmallRegistSetDto);
                result = resourceClient.postForResponseObject("payment"
                    , opmallRegistSetDto, "/opmall/updatePartner"
                    , new ParameterizedTypeReference<ResponseObject<String>>() {});
                if (result != null) {
                    resultCode = result.getData();
                    resultMsg = result.getReturnMessage();
                    setPaymentHistory(resultCode,resultMsg,opmallHistorySetDto);
                    // 미등록된 파트너의 경우 신규등록처리
                    if ("04".equals(resultCode)) {
                        opmallHistorySetDto = new OpmallHistorySetDto(OpmallType.NEW,opmallConfig,opmallRegistSetDto);
                        result = resourceClient.postForResponseObject("payment"
                            , opmallRegistSetDto, "/opmall/registPartner"
                            , new ParameterizedTypeReference<ResponseObject<String>>() {
                            });
                        if (result!=null) {
                            resultCode = result.getData();
                            resultMsg = result.getReturnMessage();
                        } else {
                            resultCode = "-1";
                            resultMsg = "연결오류";
                        }
                        setPaymentHistory(resultCode,resultMsg,opmallHistorySetDto);
                    }
                    if ("00".equals(resultCode)) {
                        opmallRequestSetDto = new OpmallRequestSetDto(payment.getPartnerId()
                            , payment.getPaymentDt(), partnerNo, payment.getBankAccountHolder(),
                            bankCode,
                            bankAccount, payment.getPaymentAmt());
                        opmallHistorySetDto = new OpmallHistorySetDto(OpmallType.REQUEST,opmallConfig,opmallRequestSetDto);
                        result = resourceClient.postForResponseObject("payment",
                            opmallRequestSetDto
                            , "/opmall/requestPayment"
                            , new ParameterizedTypeReference<ResponseObject<String>>() {
                            });
                        if (result != null) {
                            resultCode = result.getData();
                            resultMsg = result.getReturnMessage();
                        } else {
                            resultCode = "-1";
                            resultMsg = "연결오류";
                        }
                        setPaymentHistory(resultCode,resultMsg,opmallHistorySetDto);
                    }
                } else {
                    resultCode = "-1";
                    resultMsg = "연결오류";
                    setPaymentHistory(resultCode,resultMsg,opmallHistorySetDto);
                }
            } catch (Exception e) {
                resultCode = "99";
            }
            payment.setResultCode(resultCode);
            paymentMasterMapper.setPaymentRequest(payment);
            if (!"00".equals(resultCode)) {
                paymentMasterMapper.setPartnerPaymentFail(payment);
            } else {
                success++;
            }
        }
        return success;
    }

    // STEP.0 지급결과 확인
    public int setPaymentResult(String basicDt) {
        List<PaymentResultDto> paymentList = paymentSlaveMapper.getPaymentResultList(basicDt);
        String resultCode,resultMsg;
        ResponseObject<String> result;
        OpmallHistorySetDto opmallHistorySetDto;
        int success = 0;
        OpmallResultSetDto opmallResultSetDto;
        for (PaymentResultDto payment : paymentList) {
            try {
                opmallResultSetDto = new OpmallResultSetDto(payment.getPartnerId(),payment.getPaymentDt(),payment.getPaymentDt());
                opmallHistorySetDto = new OpmallHistorySetDto(OpmallType.RESULT,opmallConfig,opmallResultSetDto);
                result = resourceClient.postForResponseObject("payment", opmallResultSetDto, "/opmall/resultPayment"
                    , new ParameterizedTypeReference<ResponseObject<String>>() {});
                if (result != null) {
                    resultCode = result.getData();
                    resultMsg = result.getReturnMessage();
                } else {
                    resultCode = "-1";
                    resultMsg = "연결오류";
                }
                setPaymentHistory(resultCode,resultMsg,opmallHistorySetDto);
            } catch (Exception e) {
                resultCode = "99";
            }
            payment.setResultCode(resultCode);
            paymentMasterMapper.setPartnerPaymentComplete(payment);
            if ("01".equals(resultCode)) {
                success++;
                paymentMasterMapper.setPartnerPaymentHold(payment);
            }
        }
        paymentMasterMapper.setHomeplusPaymentComplete();
        return success;
    }

    private Map<String, BankCodeDto> getBankCodeMap() {
        if (bankCodeMap == null) {
            bankCodeMap = new HashMap<>();
            List<BankCodeDto> bankCodeList = paymentSlaveMapper.getBankCodeList();
            for (BankCodeDto dto : bankCodeList) {
                bankCodeMap.put(dto.getHomeBankCd(),dto);
            }
        }
        return bankCodeMap;
    }
    private void setPaymentHistory(String resultCode,String resultMsg, OpmallHistorySetDto opmallHistorySetDto){
        opmallHistorySetDto.setResultCode(resultCode);
        opmallHistorySetDto.setResultMsg(resultMsg);
        paymentMasterMapper.setPaymentHistory(opmallHistorySetDto);
    }
}
