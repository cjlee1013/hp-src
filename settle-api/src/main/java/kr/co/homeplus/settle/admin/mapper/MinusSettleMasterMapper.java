package kr.co.homeplus.settle.admin.mapper;

import java.util.Map;
import kr.co.homeplus.settle.core.db.annotation.MasterConnection;
import org.apache.ibatis.annotations.Param;

@MasterConnection
public interface MinusSettleMasterMapper {
  int updateFinalSettleState(@Param("settleSrl") String settleSrl, @Param("settleState") String settleState, @Param("chgId") String chgId);
  int updateMinusBank(@Param("settleSrl") String settleSrl);
  int insertMinusBankHistory(@Param("settleSrl") String settleSrl, @Param("chgId") String chgId);
  Map<String,Object> selectMinusSettleInfo(@Param("settleSrl") String settleSrl);
  int setApMinusDs(String settleSrl);
}