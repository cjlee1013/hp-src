package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.settle.admin.mapper.PgOcbPaySlaveMapper;
import kr.co.homeplus.settle.admin.model.ocb.PgOcbListSetDto;
import kr.co.homeplus.settle.admin.model.ocb.PgOcbStoreListGetDto;
import kr.co.homeplus.settle.admin.model.ocb.PgOcbDailyListGetDto;
import kr.co.homeplus.settle.admin.model.ocb.PgOcbTidListGetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PgOcbPayService {
    private final PgOcbPaySlaveMapper pgOcbPaySlaveMapper;

    /**
     * 정산관리 > 운영관리 > OCB실적조회 > 지점별 실적조회
     */
    public List<PgOcbStoreListGetDto> getStorePgOcbPay(PgOcbListSetDto listParamDto) throws Exception {
        return pgOcbPaySlaveMapper.getStorePgOcbPay(listParamDto);
    }

    /**
     * 정산관리 > 운영관리 > OCB실적조회 > 일별 실적조회
     */
    public List<PgOcbDailyListGetDto> getDailyPgOcbPay(PgOcbListSetDto listParamDto) throws Exception {
        return pgOcbPaySlaveMapper.getDailyPgOcbPay(listParamDto);
    }

    /**
     * 정산관리 > 운영관리 > OCB실적조회 > 건별 실적조회
     */
    public List<PgOcbTidListGetDto> getTidPgOcbPay(PgOcbListSetDto listParamDto) throws Exception {
        return pgOcbPaySlaveMapper.getTidPgOcbPay(listParamDto);
    }

}
