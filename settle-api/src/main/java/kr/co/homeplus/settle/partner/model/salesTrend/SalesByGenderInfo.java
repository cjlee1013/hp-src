package kr.co.homeplus.settle.partner.model.salesTrend;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("성별 판매 비중")
@Data
public class SalesByGenderInfo {

    @ApiModelProperty("성별")
    private String gender;

    @ApiModelProperty("비율")
    private float rate;
}
