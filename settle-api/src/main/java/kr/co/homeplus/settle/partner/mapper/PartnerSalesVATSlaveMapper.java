package kr.co.homeplus.settle.partner.mapper;

import java.util.List;
import kr.co.homeplus.settle.partner.model.salesVAT.PartnerSalesVATListGetDto;
import kr.co.homeplus.settle.partner.model.salesVAT.PartnerSalesVATListSetDto;
import kr.co.homeplus.settle.partner.model.salesVAT.PartnerSalesVATOrderGetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface PartnerSalesVATSlaveMapper {
    List<PartnerSalesVATListGetDto> getSalesVATList(PartnerSalesVATListSetDto listParamDto);

    List<PartnerSalesVATOrderGetDto> getSalesVATOrder(PartnerSalesVATListSetDto listParamDto);
}
