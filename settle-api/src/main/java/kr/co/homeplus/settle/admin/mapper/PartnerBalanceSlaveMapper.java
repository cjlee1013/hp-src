package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.partnerBalance.PartnerBalanceListGetDto;
import kr.co.homeplus.settle.admin.model.partnerBalance.PartnerBalanceListSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface PartnerBalanceSlaveMapper {
    List<PartnerBalanceListGetDto> selectPartnerBalanceList(PartnerBalanceListSetDto partnerBalanceListSetDto);
}
