package kr.co.homeplus.settle.admin.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "DGV 실적조회 > 점별 합계")
public class DgvPaymentStoreGetDto {
  @ApiModelProperty(notes = "점포유형")
  private String storeType;

  @ApiModelProperty(notes = "점포코드")
  private String storeId;

  @ApiModelProperty(notes = "점포명")
  private String storeNm;

  @ApiModelProperty(notes = "코스트센터")
  private String onlineCostCenter;

  @ApiModelProperty(notes = "매출금액")
  private String completeAmt;

  @ApiModelProperty(notes = "결제금액")
  private String orderPrice;

  @ApiModelProperty(notes = "DGV 사용금액")
  private String dgvAmt;

  @ApiModelProperty(notes = "GVS 전송금액")
  private String collectAmt;
}
