package kr.co.homeplus.settle.admin.model.partnerSettle;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PartnerSettleCommonSetDto {
    @ApiModelProperty(value = "지급SRL")
    private long settleSrl;

    @ApiModelProperty(value = "변경 메모")
    private String changeMemo;

    @ApiModelProperty(value = "처리자")
    private String chgId;
}
