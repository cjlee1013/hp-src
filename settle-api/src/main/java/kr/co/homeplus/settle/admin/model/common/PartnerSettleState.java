package kr.co.homeplus.settle.admin.model.common;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum PartnerSettleState {
    //정신관리 > 파트너 정산상태
    SETTLE_STATUS_WAIT("1", "정산대기"),
    SETTLE_STATUS_COMPLETE("2", "정산완료(수동)"),
    SETTLE_STATUS_COMPLETE_AUTO("3", "정산완료(자동)"),
    SETTLE_STATUS_POSTPONE_MANUAL("4", "정산보류(수동)");

    private String code;
    private String name;
}
