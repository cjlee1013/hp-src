package kr.co.homeplus.settle.admin.mapper;

import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipCreateGetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipCreateSetDto;
import kr.co.homeplus.settle.core.db.annotation.MasterConnection;

@MasterConnection
public interface UnshipSlipMasterMapper {
  TdUnshipSlipCreateGetDto insertTdUnshipSlip(TdUnshipSlipCreateSetDto tdUnshipSlipCreateSetDto);
}