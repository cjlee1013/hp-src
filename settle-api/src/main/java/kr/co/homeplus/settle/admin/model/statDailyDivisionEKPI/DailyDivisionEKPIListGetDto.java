package kr.co.homeplus.settle.admin.model.statDailyDivisionEKPI;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "통계 > 상품주문통계 > 카테고리별 매출 (E-KPI)")
public class DailyDivisionEKPIListGetDto {
    @ApiModelProperty(value = "상품구분")
    @RealGridColumnInfo(headText = "상품구분", sortable = true, width = 80)
    private String mallType;

    @ApiModelProperty(value = "대분류")
    @RealGridColumnInfo(headText = "대분류", sortable = true, width = 150)
    private String divisionNm;

    @ApiModelProperty(value = "중분류")
    @RealGridColumnInfo(headText = "중분류", sortable = true, width = 200)
    private String groupNm;

    @ApiModelProperty(value = "소분류")
    @RealGridColumnInfo(headText = "소분류", sortable = true, width = 200)
    private String deptNm;

    @ApiModelProperty(value = "세분류")
    @RealGridColumnInfo(headText = "세분류", sortable = true, width = 200)
    private String classNm;

    @ApiModelProperty(value = "전체 매출(E-KPI)")
    @RealGridColumnInfo(headText = "전체 매출(E-KPI)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long ekpiAmt;

    @ApiModelProperty(value = "PC 매출(E-KPI)")
    @RealGridColumnInfo(headText = "PC 매출(E-KPI)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long ekpiPcAmt;

    @ApiModelProperty(value = "Mobile 매출(E-KPI)")
    @RealGridColumnInfo(headText = "Mobile 매출(E-KPI)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long ekpiMobileAmt;
}
