package kr.co.homeplus.settle.admin.model.adjust;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AdjustListSetDto {
    @ApiModelProperty(notes = "임의조정 타입")
    private String adjustType;

    @ApiModelProperty(notes = "검색시작일")
    private String startDt;

    @ApiModelProperty(notes = "검색종료일")
    private String endDt;

    @ApiModelProperty(notes = "상품번호")
    private String itemNo;

    @ApiModelProperty(notes = "상품명")
    private String itemNm;

    @ApiModelProperty(notes = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(notes = "판매업체명")
    private String partnerNm;

    @ApiModelProperty(notes = "업체코드")
    private String vendorCd;
}
