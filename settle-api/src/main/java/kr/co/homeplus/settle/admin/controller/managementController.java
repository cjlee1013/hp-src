package kr.co.homeplus.settle.admin.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.management.DgvPaymentDailyGetDto;
import kr.co.homeplus.settle.admin.model.management.DgvPaymentListGetDto;
import kr.co.homeplus.settle.admin.model.management.DgvPaymentSetDto;
import kr.co.homeplus.settle.admin.model.management.DgvPaymentStoreGetDto;
import kr.co.homeplus.settle.admin.model.management.MhcPaymentDailyGetDto;
import kr.co.homeplus.settle.admin.model.management.MhcPaymentListGetDto;
import kr.co.homeplus.settle.admin.model.management.MhcPaymentSetDto;
import kr.co.homeplus.settle.admin.model.management.MhcPaymentStoreGetDto;
import kr.co.homeplus.settle.admin.model.management.PaymentInfoListGetDto;
import kr.co.homeplus.settle.admin.model.management.PaymentInfoListSetDto;
import kr.co.homeplus.settle.admin.model.management.PaymentInfoSumGetDto;
import kr.co.homeplus.settle.admin.model.management.PgPaymentInfoListGetDto;
import kr.co.homeplus.settle.admin.model.management.PgPaymentInfoListSetDto;
import kr.co.homeplus.settle.admin.service.ManagementService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/management")
@Api(tags = "BackOffice > 운영관리")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class managementController {

  private final ManagementService managementService;

  @ApiOperation(value = "결제일마감합계", response = ResponseObject.class, notes = "결제일마감합계")
  @RequestMapping(value = "/getPaymentInfoSum", method = {RequestMethod.POST})
  public ResponseObject<List<PaymentInfoSumGetDto>> getPaymentInfoSum (@RequestBody @Valid
      PaymentInfoListSetDto paymentInfoListSetDto) {
    List<PaymentInfoSumGetDto> paymentInfoSumGetDtoList = managementService.getPaymentInfoSum(paymentInfoListSetDto);
    return ResourceConverter.toResponseObject(paymentInfoSumGetDtoList);
  }

  @ApiOperation(value = "결제일마감조회", response = ResponseObject.class, notes = "결제일마감조회")
  @RequestMapping(value = "/getPaymentInfoList", method = {RequestMethod.POST})
  public ResponseObject<List<PaymentInfoListGetDto>> getPaymentInfoList (@RequestBody @Valid
      PaymentInfoListSetDto paymentInfoListSetDto) {
    List<PaymentInfoListGetDto> paymentInfoListGetDtoList = managementService.getPaymentInfoList(paymentInfoListSetDto);
    return ResourceConverter.toResponseObject(paymentInfoListGetDtoList);
  }

  @ApiOperation(value = "정산일마감합계", response = ResponseObject.class, notes = "정산일마감합계")
  @RequestMapping(value = "/getSettleInfoSum", method = {RequestMethod.POST})
  public ResponseObject<List<PaymentInfoSumGetDto>> getSettleInfoSum (@RequestBody @Valid
      PaymentInfoListSetDto paymentInfoListSetDto) {
    List<PaymentInfoSumGetDto> paymentInfoListGetDtoList = managementService.getSettleInfoSum(paymentInfoListSetDto);
    return ResourceConverter.toResponseObject(paymentInfoListGetDtoList);
  }

  @ApiOperation(value = "정산일마감조회", response = ResponseObject.class, notes = "정산일마감조회")
  @RequestMapping(value = "/getSettleInfoList", method = {RequestMethod.POST})
  public ResponseObject<List<PaymentInfoListGetDto>> getSettleInfoList (@RequestBody @Valid
      PaymentInfoListSetDto paymentInfoListSetDto) {
    List<PaymentInfoListGetDto> paymentInfoListGetDtoList = managementService.getSettleInfoList(paymentInfoListSetDto);
    return ResourceConverter.toResponseObject(paymentInfoListGetDtoList);
  }

  @ApiOperation(value = "PG일마감조회", response = ResponseObject.class, notes = "PG일마감조회")
  @RequestMapping(value = "/getPgPaymentInfoList", method = {RequestMethod.POST})
  public ResponseObject<List<PgPaymentInfoListGetDto>> getPgPaymentInfoList (@RequestBody @Valid
      PgPaymentInfoListSetDto pgPaymentInfoListSetDto) {
    List<PgPaymentInfoListGetDto> pgPaymentInfoListGetDtoList = managementService.getPgPaymentInfoList(pgPaymentInfoListSetDto);
    return ResourceConverter.toResponseObject(pgPaymentInfoListGetDtoList);
  }

  @ApiOperation(value = "DGV 실적조회 점별 합계", response = ResponseObject.class, notes = "점별 합계")
  @RequestMapping(value = "/getDgvPaymentStore", method = {RequestMethod.POST})
  public ResponseObject<List<DgvPaymentStoreGetDto>> getDgvPaymentStore (@RequestBody @Valid
      DgvPaymentSetDto dgvPaymentSetDto) {
    List<DgvPaymentStoreGetDto> dgvPaymentList = managementService.getDgvPaymentStore(dgvPaymentSetDto);
    return ResourceConverter.toResponseObject(dgvPaymentList);
  }

  @ApiOperation(value = "DGV 실적조회 일별 상세내역", response = ResponseObject.class, notes = "일별 상세내역")
  @RequestMapping(value = "/getDgvPaymentDaily", method = {RequestMethod.POST})
  public ResponseObject<List<DgvPaymentDailyGetDto>> getDgvPaymentDaily (@RequestBody @Valid
      DgvPaymentSetDto dgvPaymentSetDto) {
    List<DgvPaymentDailyGetDto> dgvPaymentList = managementService.getDgvPaymentDaily(dgvPaymentSetDto);
    return ResourceConverter.toResponseObject(dgvPaymentList);
  }

  @ApiOperation(value = "DGV 실적조회 건별 상세내역", response = ResponseObject.class, notes = "건별 상세내역")
  @RequestMapping(value = "/getDgvPaymentList", method = {RequestMethod.POST})
  public ResponseObject<List<DgvPaymentListGetDto>> getDgvPaymentList (@RequestBody @Valid
      DgvPaymentSetDto dgvPaymentSetDto) {
    List<DgvPaymentListGetDto> dgvPaymentList = managementService.getDgvPaymentList(dgvPaymentSetDto);
    return ResourceConverter.toResponseObject(dgvPaymentList);
  }

  @ApiOperation(value = "MHC 실적조회 점별 합계", response = ResponseObject.class, notes = "점별 합계")
  @RequestMapping(value = "/getMhcPaymentStore", method = {RequestMethod.POST})
  public ResponseObject<List<MhcPaymentStoreGetDto>> getMhcPaymentStore (@RequestBody @Valid
      MhcPaymentSetDto mhcPaymentSetDto) {
    List<MhcPaymentStoreGetDto> mhcPaymentList = managementService.getMhcPaymentStore(mhcPaymentSetDto);
    return ResourceConverter.toResponseObject(mhcPaymentList);
  }
  @ApiOperation(value = "MHC 실적조회 일별 상세내역", response = ResponseObject.class, notes = "일별 상세내역")
  @RequestMapping(value = "/getMhcPaymentDaily", method = {RequestMethod.POST})
  public ResponseObject<List<MhcPaymentDailyGetDto>> getMhcPaymentDaily (@RequestBody @Valid
      MhcPaymentSetDto mhcPaymentSetDto) {
    List<MhcPaymentDailyGetDto> mhcPaymentList = managementService.getMhcPaymentDaily(mhcPaymentSetDto);
    return ResourceConverter.toResponseObject(mhcPaymentList);
  }
  @ApiOperation(value = "MHC 실적조회 건별 상세내역", response = ResponseObject.class, notes = "건별 상세내역")
  @RequestMapping(value = "/getMhcPaymentList", method = {RequestMethod.POST})
  public ResponseObject<List<MhcPaymentListGetDto>> getMhcPaymentList (@RequestBody @Valid
      MhcPaymentSetDto mhcPaymentSetDto) {
    List<MhcPaymentListGetDto> mhcPaymentList = managementService.getMhcPaymentList(mhcPaymentSetDto);
    return ResourceConverter.toResponseObject(mhcPaymentList);
  }
}
