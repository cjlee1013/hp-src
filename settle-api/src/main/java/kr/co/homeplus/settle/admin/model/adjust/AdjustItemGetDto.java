package kr.co.homeplus.settle.admin.model.adjust;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AdjustItemGetDto {
    @ApiModelProperty(notes = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(notes = "판매업체명")
    private String partnerNm;

    @ApiModelProperty(notes = "업체코드")
    private String vendorCd;

    @ApiModelProperty(notes = "상품명")
    private String itemNm;
}
