package kr.co.homeplus.settle.admin.model.adjust;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AdjustRegisterSetDto {
    @ApiModelProperty(notes = "계산서 대상여부")
    private String taxType;

    @ApiModelProperty(notes = "상품번호")
    private String itemNo;

    @ApiModelProperty(notes = "업체코드")
    private String vendorCd;

    @ApiModelProperty(notes = "유형")
    private String adjustType;

    @ApiModelProperty(notes = "구분")
    private String gubun;

    @ApiModelProperty(notes = "조정금액")
    private long adjustAmt;

    @ApiModelProperty(notes = "조정수수료")
    private long adjustFee;

    @ApiModelProperty(notes = "사유")
    private String comment;

    @ApiModelProperty(notes = "별도정산")
    private String separateYn;

    @ApiModelProperty(notes = "등록자")
    private String regId;

    @ApiModelProperty(notes = "사유")
    private String reason;
}
