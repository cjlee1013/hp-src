package kr.co.homeplus.settle.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"kr.co.homeplus.settle", "kr.co.homeplus.plus"})
public class SettleApiApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(SettleApiApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SettleApiApplication.class);
    }
}
