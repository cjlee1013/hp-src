package kr.co.homeplus.settle.admin.mapper;


import java.util.List;
import kr.co.homeplus.settle.admin.model.averageOrderItemQty.AverageOrderItemQtyListGetDto;
import kr.co.homeplus.settle.admin.model.averageOrderItemQty.AverageOrderItemQtyListSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface AverageOrderItemQtySlaveMapper {
    List<AverageOrderItemQtyListGetDto> getAverageOrderItemQtyList(AverageOrderItemQtyListSetDto averageOrderItemQtyListGetDto);
}
