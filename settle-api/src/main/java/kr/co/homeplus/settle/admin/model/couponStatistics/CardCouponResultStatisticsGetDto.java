package kr.co.homeplus.settle.admin.model.couponStatistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardCouponResultStatisticsGetDto {
    @ApiModelProperty(value = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "쿠폰번호")
    private String couponNo;

    @ApiModelProperty(value = "쿠폰명")
    private String couponNm;
}
