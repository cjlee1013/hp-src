package kr.co.homeplus.settle.admin.model.partnerSettleDetail;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PartnerSettleItemSetDto {
    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "지급SRL")
    private long settleSrl;

    @ApiModelProperty(value = "정산번호")
    private long settleNo;

    @ApiModelProperty(value = "정산주기")
    private String settleCycleType;
}
