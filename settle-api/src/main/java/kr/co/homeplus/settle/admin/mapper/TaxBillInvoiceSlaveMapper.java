package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillInvoiceListGetDto;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillInvoiceListSetDto;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillReceiveListGetDto;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillReceiveSupplierGetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface TaxBillInvoiceSlaveMapper {
    List<TaxBillInvoiceListGetDto> getTaxBillInvoiceList(TaxBillInvoiceListSetDto listParamDto);

    List<TaxBillReceiveListGetDto> getTaxBillReceiveList(TaxBillInvoiceListSetDto listParamDto);

    List<TaxBillReceiveSupplierGetDto> getTaxBillSupplierList(String taxBillReceiveType, String partnerId);
}
