package kr.co.homeplus.settle.batch.model;

import lombok.Data;

@Data
public class PaymentRequestDto {
  private String partnerId;
  private String paymentDt;
  private String partnerNo;
  private String bankAccountHolder;
  private String bankCode;
  private String bankAccount;
  private String paymentAmt;
  private String resultCode;
  private String phoneNo;
  private String partnerNm;
  private String partnerOwner;

  public void setPaymentAmt(String paymentAmt) {
    this.paymentAmt = paymentAmt;
  }
}