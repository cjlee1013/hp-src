package kr.co.homeplus.settle.admin.model.balanceSheet;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "전표조회 목록")
public class BalanceSheetListGetDto {
  @ApiModelProperty(notes = "기준일")
  private String basicDt;

  @ApiModelProperty(notes = "전표유형코드")
  private String slipType;

  @ApiModelProperty(notes = "전표유형")
  private String slipName;

  @ApiModelProperty(notes = "차변")
  private String drAmt;

  @ApiModelProperty(notes = "대변")
  private String crAmt;

  @ApiModelProperty(notes = "상태")
  private String status;

  @ApiModelProperty(notes = "변경일시")
  private String chgDt;

  @ApiModelProperty(notes = "변경자")
  private String chgId;

}
