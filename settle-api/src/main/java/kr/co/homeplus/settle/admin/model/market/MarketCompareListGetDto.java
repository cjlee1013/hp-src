package kr.co.homeplus.settle.admin.model.market;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제일마감")
public class MarketCompareListGetDto {
  @ApiModelProperty(notes = "매출일")
  private String basicDt;

  @ApiModelProperty(notes = "입금일")
  private String payScheduleDt;

  @ApiModelProperty(notes = "제휴사")
  private String siteType;

  @ApiModelProperty(notes = "주문번호")
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "제휴사 주문번호")
  private String marketOrderNo;

  @ApiModelProperty(notes = "상품번호")
  private String itemNo;

  @ApiModelProperty(notes = "주문구분")
  private String gubun;

  @ApiModelProperty(notes = "차이구분")
  private String diffType;

  @ApiModelProperty(notes = "홈플러스")
  private String homeAmt;

  @ApiModelProperty(notes = "제휴사")
  private String marketAmt;

  @ApiModelProperty(notes = "차액")
  private String diffAmt;
}
