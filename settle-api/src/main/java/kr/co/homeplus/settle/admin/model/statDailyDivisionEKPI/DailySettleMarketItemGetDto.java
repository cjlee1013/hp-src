package kr.co.homeplus.settle.admin.model.statDailyDivisionEKPI;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "통계 > 마켓통계 > 상품별 판매 통계")
public class DailySettleMarketItemGetDto {
    @ApiModelProperty(value = "No")
    @RealGridColumnInfo(headText = "No", sortable = true, width = 30, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long seqNo;

    @ApiModelProperty(value = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", sortable = true, width = 80)
    private String storeType;

    @ApiModelProperty(value = "마켓")
    @RealGridColumnInfo(headText = "마켓", sortable = true, width = 80)
    private String marketTypeNm;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", sortable = true, width = 100)
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", sortable = true, width = 200)
    private String itemNm;

    @ApiModelProperty(value = "수량")
    @RealGridColumnInfo(headText = "수량", sortable = true, width = 80, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long completeCnt;

    @ApiModelProperty(value = "매출금액")
    @RealGridColumnInfo(headText = "매출금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long completeAmt;

    @ApiModelProperty(value = "대분류")
    @RealGridColumnInfo(headText = "대분류", sortable = true, width = 100)
    private String divisionNm;

    @ApiModelProperty(value = "중분류")
    @RealGridColumnInfo(headText = "중분류", sortable = true, width = 100)
    private String groupNm;

    @ApiModelProperty(value = "Section")
    @RealGridColumnInfo(headText = "Section", sortable = true, width = 80)
    private String deptNo;

    @ApiModelProperty(value = "소분류")
    @RealGridColumnInfo(headText = "소분류", sortable = true, width = 100)
    private String deptNm;

    @ApiModelProperty(value = "Class")
    @RealGridColumnInfo(headText = "Class", sortable = true, width = 80)
    private String classNo;

    @ApiModelProperty(value = "세분류")
    @RealGridColumnInfo(headText = "세분류", sortable = true, width = 100)
    private String classNm;

    @ApiModelProperty(value = "Subclass")
    @RealGridColumnInfo(headText = "Subclass", sortable = true, width = 80)
    private String subClassNo;

    @ApiModelProperty(value = "미분류")
    @RealGridColumnInfo(headText = "미분류", sortable = true, width = 100)
    private String subClassNm;

    @ApiModelProperty(value = "사이트유형")
    @RealGridColumnInfo(headText = "사이트유형", sortable = true, width = 100, hidden = true)
    private String siteType;
}
