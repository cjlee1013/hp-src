package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillInvoiceListGetDto;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillInvoiceListSetDto;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillReceiveListGetDto;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillReceiveSetDto;
import kr.co.homeplus.settle.admin.model.taxBillInvoice.TaxBillReceiveSupplierGetDto;
import kr.co.homeplus.settle.admin.service.TaxBillInvoiceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/TaxBillInvoice")
@Api(tags = "BackOffice > 세금계산서 관리")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class TaxBillInvoiceController {
    private final TaxBillInvoiceService settleService;

    @ApiOperation(value = "세금계산서 조회 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getTaxBillInvoiceList")
    public ResponseObject<List<TaxBillInvoiceListGetDto>> getTaxBillInvoiceList(@RequestBody TaxBillInvoiceListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(settleService.getTaxBillInvoiceList(listParamDto));
    }

    @ApiOperation(value = "세금계산서관리 > 수취내역 관리 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getTaxBillReceiveList")
    public ResponseObject<List<TaxBillReceiveListGetDto>> getTaxBillReceiveList(@RequestBody TaxBillInvoiceListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(settleService.getTaxBillReceiveList(listParamDto));
    }

    @ApiOperation(value = "세금계산서관리 > 수취내역 등록", response = ResponseObject.class)
    @PostMapping("/setTaxBillReceiveList")
    public ResponseObject<String> setTaxBillReceiveList(@RequestBody TaxBillReceiveSetDto listParamDto) throws Exception {
        return settleService.setTaxBillReceiveList(listParamDto);
    }

    @ApiOperation(value = "세금계산서관리 > 수취내역 수정", response = ResponseObject.class)
    @PostMapping("/updTaxBillReceive")
    public ResponseObject<String> updTaxBillReceive(@RequestBody TaxBillReceiveSetDto listParamDto) throws Exception {
        return settleService.updTaxBillReceive(listParamDto);
    }

    @ApiOperation(value = "세금계산서관리 > 수취내역 확정", response = ResponseObject.class)
    @PostMapping("/setTaxBillReceiveConfirm")
    public ResponseObject<String> setTaxBillReceiveConfirm(@RequestBody TaxBillReceiveSetDto listParamDto) throws Exception {
        return settleService.setTaxBillReceiveConfirm(listParamDto);
    }

    @ApiOperation(value = "세금계산서 수취내역 > 공급처 정보", response = ResponseObject.class)
    @GetMapping("/getTaxBillSupplierList")
    public ResponseObject<List<TaxBillReceiveSupplierGetDto>> getTaxBillSupplierList(String taxBillReceiveType, String partnerId) throws Exception {
        return ResourceConverter.toResponseObject(settleService.getTaxBillSupplierList(taxBillReceiveType, partnerId));
    }

    @ApiOperation(value = "세금계산서관리 > 수취내역 삭제", response = ResponseObject.class)
    @PostMapping("/deleteTaxBillReceive")
    public ResponseObject<String> deleteTaxBillReceive(@RequestBody TaxBillReceiveSetDto listParamDto) throws Exception {
        return settleService.deleteTaxBillReceive(listParamDto);
    }
}