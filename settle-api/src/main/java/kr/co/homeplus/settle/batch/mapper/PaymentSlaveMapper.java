package kr.co.homeplus.settle.batch.mapper;

import java.util.List;
import kr.co.homeplus.settle.batch.model.BankCodeDto;
import kr.co.homeplus.settle.batch.model.PartnerInfoDto;
import kr.co.homeplus.settle.batch.model.PaymentRequestDto;
import kr.co.homeplus.settle.batch.model.PaymentResultDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface PaymentSlaveMapper {
    List<PartnerInfoDto> getChangePartnerList();
    List<PartnerInfoDto> getNewPartnerList();
    List<PaymentRequestDto> getPartnerPaymentRequestList();
    PaymentRequestDto getHomeplusPaymentRequestList();
    List<PaymentResultDto> getPaymentResultList(String basicDt);
    List<BankCodeDto> getBankCodeList();
}
