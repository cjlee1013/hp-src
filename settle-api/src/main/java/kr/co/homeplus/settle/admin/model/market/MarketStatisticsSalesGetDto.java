package kr.co.homeplus.settle.admin.model.market;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마켓별 판매현황")
public class MarketStatisticsSalesGetDto {
  @ApiModelProperty(notes = "결제일")
  private String basicDt;

  @ApiModelProperty(notes = "매출건수_Total")
  private String totOrderCnt;

  @ApiModelProperty(notes = "매출건수_Naver")
  private String naverOrderCnt;

  @ApiModelProperty(notes = "매출건수_11번가")
  private String elevenOrderCnt;

  @ApiModelProperty(notes = "매출금액_Total")
  private String totOrderAmt;

  @ApiModelProperty(notes = "매출금액_Naver")
  private String naverOrderAmt;

  @ApiModelProperty(notes = "매출금액_11번가")
  private String elevenOrderAmt;

  @ApiModelProperty(notes = "Basket_Size_Total")
  private String totBasketSize;

  @ApiModelProperty(notes = "Basket_Size_Naver")
  private String naverBasketSize;

  @ApiModelProperty(notes = "Basket_Size_11번가")
  private String elevenBasketSize;

  @ApiModelProperty(notes = "상품수량_Total")
  private String totItemCnt;

  @ApiModelProperty(notes = "상품수량_Naver")
  private String naverItemCnt;

  @ApiModelProperty(notes = "상품수량_11번가")
  private String elevenItemCnt;
}
