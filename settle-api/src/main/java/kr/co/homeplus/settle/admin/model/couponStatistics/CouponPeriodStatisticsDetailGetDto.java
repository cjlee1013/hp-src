package kr.co.homeplus.settle.admin.model.couponStatistics;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CouponPeriodStatisticsDetailGetDto {
    @ApiModelProperty(value = "점포코드")
    private String storeId;

    @ApiModelProperty(value = "점포명")
    private String storeNm;

    @ApiModelProperty(value = "주문건수")
    private String purchaseCnt;

    @ApiModelProperty(value = "주문금액")
    private String completeAmt;

    @ApiModelProperty(value = "사용금액")
    private String discountAmt;
}
