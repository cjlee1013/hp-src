package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.settle.admin.mapper.SettleAdjustMasterMapper;
import kr.co.homeplus.settle.admin.mapper.SettleAdjustSlaveMapper;
import kr.co.homeplus.settle.admin.model.adjust.AdjustItemGetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustListSetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustListGetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustRegisterGetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustRegisterSetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustType;
import kr.co.homeplus.settle.admin.model.adjust.AdjustTypeInfo;
import kr.co.homeplus.settle.core.exception.handler.ExceptionCode;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@RequiredArgsConstructor
public class SettleAdjustService {
    private final SettleAdjustSlaveMapper settleSlaveMapper;
    private final SettleAdjustMasterMapper settleMasterMapper;

    /**
     * 정산조정 관리 조회
     */
    public List<AdjustListGetDto> getAdjustList(AdjustListSetDto listParamDto) throws Exception {
        List<AdjustListGetDto> returnDto = settleSlaveMapper.getAdjustList(listParamDto);
        for (AdjustListGetDto dto : returnDto) {
            dto.setPartnerId(PrivacyMaskingUtils.maskingUserId(dto.getPartnerId()));
        }
        return returnDto;
    }

    /**
     * 정산조정 셀러상품 조회
     */
    public ResponseObject<AdjustItemGetDto> getAdjustItem(@RequestParam(name ="itemNo") String itemNo) throws Exception {
        String resultMsg = "", returnCode = "";
        AdjustItemGetDto returnDto = new AdjustItemGetDto();
        try {
            returnDto = settleSlaveMapper.getAdjustItem(itemNo);
            returnDto.setPartnerId(PrivacyMaskingUtils.maskingUserId(returnDto.getPartnerId()));
            if (returnDto.getItemNm() == "") {
                resultMsg = ExceptionCode.ERROR_CODE_2004.getResponseMessage();
                returnCode = ExceptionCode.ERROR_CODE_2004.getResponseCode();
            }
        }
        catch (Exception e) {
            resultMsg = ExceptionCode.ERROR_CODE_2004.getResponseMessage();
            returnCode = ExceptionCode.ERROR_CODE_2004.getResponseCode();
        }

        return ResourceConverter.toResponseObject(returnDto, HttpStatus.OK, returnCode, resultMsg);
    }

    /**
     * 정산조정 입력
     */
    public ResponseObject<String> insertAdjust(AdjustRegisterSetDto listParamDto) throws Exception {
        String resultMsg = "", returnCode = "";
        try {
            //TaxType Gubun
            if (listParamDto.getAdjustType().equals(AdjustType.SETTLE_ADJUST_TYPE_03.getCode())) {
                listParamDto.setTaxType(AdjustType.SETTLE_ADJUST_TYPE_03.getTaxType());
                listParamDto.setGubun(AdjustType.SETTLE_ADJUST_TYPE_03.getGubun());
                listParamDto.setAdjustAmt(0); //조정금액
                listParamDto.setAdjustFee(Math.abs(listParamDto.getAdjustFee())); //조정수수료
            } else if (listParamDto.getAdjustType().equals(AdjustType.SETTLE_ADJUST_TYPE_02.getCode())) {
                listParamDto.setTaxType(AdjustType.SETTLE_ADJUST_TYPE_02.getTaxType());
                listParamDto.setGubun(AdjustType.SETTLE_ADJUST_TYPE_02.getGubun());
                listParamDto.setAdjustAmt(Math.abs(listParamDto.getAdjustAmt())); //조정금액
                listParamDto.setAdjustFee(0); //조정수수료
            } else if (listParamDto.getAdjustType().equals(AdjustType.SETTLE_ADJUST_TYPE_01.getCode())) {
                listParamDto.setTaxType(AdjustType.SETTLE_ADJUST_TYPE_01.getTaxType());
                listParamDto.setGubun(AdjustType.SETTLE_ADJUST_TYPE_01.getGubun());
                listParamDto.setAdjustAmt(Math.abs(listParamDto.getAdjustAmt())); //조정금액
                listParamDto.setAdjustFee(0); //조정수수료
            } else if (listParamDto.getAdjustType().equals(AdjustType.SETTLE_ADJUST_TYPE_04.getCode())) { //판매 수수료 환금(가산)
                long adjustFee = listParamDto.getAdjustFee();
                listParamDto.setTaxType(AdjustType.SETTLE_ADJUST_TYPE_04.getTaxType());
                listParamDto.setGubun(AdjustType.SETTLE_ADJUST_TYPE_04.getGubun());
                listParamDto.setAdjustAmt(0); //조정금액
                listParamDto.setAdjustFee(Math.abs(adjustFee)); //조정수수료
            } else if (listParamDto.getAdjustType().equals(AdjustType.SETTLE_ADJUST_TYPE_05.getCode())) { //판매 수수료 청구(차감)
                long adjustFee = listParamDto.getAdjustFee();
                listParamDto.setTaxType(AdjustType.SETTLE_ADJUST_TYPE_05.getTaxType());
                listParamDto.setGubun(AdjustType.SETTLE_ADJUST_TYPE_05.getGubun());
                listParamDto.setAdjustAmt(0); //조정금액
                listParamDto.setAdjustFee(Math.abs(adjustFee)); //조정수수료
                listParamDto.setAdjustType(AdjustType.SETTLE_ADJUST_TYPE_04.getCode());
            } else {
                resultMsg =  "유효하지 않은 정산조정 유형입니다.";
                returnCode = ExceptionCode.ERROR_CODE_1021.getResponseCode();
                return ResourceConverter.toResponseObject(listParamDto.getItemNo(), HttpStatus.OK, returnCode, resultMsg);
            }

            AdjustRegisterGetDto returnDto = settleMasterMapper.insertAdjust(listParamDto);

            resultMsg =  returnDto.getResultMsg();
            returnCode = returnDto.getResultCd();
        }
        catch (Exception e) {
            resultMsg =  ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage();
            returnCode = ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode();
        }

        return ResourceConverter.toResponseObject(listParamDto.getItemNo(), HttpStatus.OK, returnCode, resultMsg);
    }

    /**
     * 정산조정타입 조회
     */
    public List<AdjustTypeInfo> getAdjustType(@RequestParam(name ="adjustType", required = false, defaultValue = "") String adjustType) throws Exception {
        return settleSlaveMapper.getAdjustType(adjustType);
    }

    /**
     * 정산조정 삭제
     */
    public ResponseObject<String> deleteAdjust(AdjustListGetDto listParamDto) throws Exception {
        String resultMsg = "", returnCode = "0";
        try {
            settleMasterMapper.deleteAdjust(listParamDto);
        }
        catch (Exception e) {
            resultMsg =  ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage();
            returnCode = ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode();
        }

        return ResourceConverter.toResponseObject(listParamDto.getAdjustId(), HttpStatus.OK, returnCode, resultMsg);
    }
}