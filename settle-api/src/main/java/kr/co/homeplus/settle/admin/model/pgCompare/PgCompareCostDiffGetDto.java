package kr.co.homeplus.settle.admin.model.pgCompare;

import lombok.Data;

@Data
public class PgCompareCostDiffGetDto {
    private String pgKind;
    private String pgMid;
    private String basicDt;
    private String payScheduleDt;
    private String pgOid;
    private String pgTid;
    private String flag;
    private long hmpAmt;
    private long pgAmt;
    private long diffAmt;
}
