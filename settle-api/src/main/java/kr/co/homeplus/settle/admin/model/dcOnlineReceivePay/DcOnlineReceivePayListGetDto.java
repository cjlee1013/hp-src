package kr.co.homeplus.settle.admin.model.dcOnlineReceivePay;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DcOnlineReceivePayListGetDto {
    @ApiModelProperty(value = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "RMS상품코드")
    private String rmsItemNo;

    @ApiModelProperty(value = "상품명")
    private String itemName;

    @ApiModelProperty(value = "과면세구분")
    private String taxYn;

    @ApiModelProperty(value = "카테고리")
    private String deptNo;

    @ApiModelProperty(value = "점포코드")
    private String storeId;

    @ApiModelProperty(value = "점포카테고리")
    private String deptStoreCd;

    @ApiModelProperty(value = "업체코드")
    private String vendorCd;

    @ApiModelProperty(value = "기초잔고수량")
    private String basicQty;

    @ApiModelProperty(value = "기초잔고금액")
    private String basicAmt;

    @ApiModelProperty(value = "매입수량")
    private String purchaseQty;

    @ApiModelProperty(value = "매입금액")
    private String purchaseAmt;

    @ApiModelProperty(value = "재고이동수량")
    private String tranQty;

    @ApiModelProperty(value = "재고이동금액")
    private String tranAmt;

    @ApiModelProperty(value = "손실수량")
    private String lossQty;

    @ApiModelProperty(value = "손실금액")
    private String lossAmt;

    @ApiModelProperty(value = "미확인손실수량")
    private String unknownLossQty;

    @ApiModelProperty(value = "미확인손실금액")
    private String unknownLossAmt;

    @ApiModelProperty(value = "반품수량")
    private String refundQty;

    @ApiModelProperty(value = "반품금액")
    private String refundAmt;

    @ApiModelProperty(value = "매출수량")
    private String salesQty;

    @ApiModelProperty(value = "매출금액")
    private String salesAmt;

    @ApiModelProperty(value = "기말잔고수량")
    private String termEndQty;

    @ApiModelProperty(value = "기말잔고금액")
    private String termEndAmt;

    @ApiModelProperty(value = "매입원가")
    private String orgPrice;

    @ApiModelProperty(value = "주문수량")
    private String orderQty;

    @ApiModelProperty(value = "주문금액")
    private String orderAmt;

    @ApiModelProperty(value = "이동평균원가")
    private String tranAvgAmt;

    @ApiModelProperty(value = "할인금액")
    private String discountAmt;

    @ApiModelProperty(value = "판매금액")
    private String sellAmt;

    @ApiModelProperty(value = "순매출금액")
    private String realSaleAmt;

    @ApiModelProperty(value = "공급가")
    private String supplyAmt;

    @ApiModelProperty(value = "마진")
    private String margin;
}
