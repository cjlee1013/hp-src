package kr.co.homeplus.settle.partner.model.salesVAT;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridButtonType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "정산관리 > 부가세 신고내역 조회")
public class PartnerSalesVATListGetDto {
    @ApiModelProperty(value = "기간")
    @RealGridColumnInfo(headText = "기간", sortable = true, width = 100)
    private String basicDt;

    @ApiModelProperty(value = "매출금액")
    @RealGridColumnInfo(headText = "매출금액", groupName = "과세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long taxSalesAmt;

    @ApiModelProperty(value = "신용카드")
    @RealGridColumnInfo(headText = "신용카드", groupName = "과세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long taxPgCardAmt;

    @ApiModelProperty(value = "현금영수증(소득공제)")
    @RealGridColumnInfo(headText = "현금영수증\\n(소득공제)", groupName = "과세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long incomeDeductAmt;

    @ApiModelProperty(value = "현금영수증(지출증빙)")
    @RealGridColumnInfo(headText = "현금영수증\\n(지출증빙)", groupName = "과세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long expenditureAmt;

    @ApiModelProperty(value = "기타")
    @RealGridColumnInfo(headText = "기타", groupName = "과세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long taxPgEtcAmt;

    @ApiModelProperty(value = "홈플러스할인")
    @RealGridColumnInfo(headText = "홈플러스할인", groupName = "과세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long taxHomeDiscountAmt;

    @ApiModelProperty(value = "면세매출금액")
    @RealGridColumnInfo(headText = "면세매출금액", groupName = "면세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long freeTaxSalesAmt;

    @ApiModelProperty(value = "신용카드")
    @RealGridColumnInfo(headText = "신용카드", groupName = "면세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long freeTaxPgCardAmt;

    @ApiModelProperty(value = "현금영수증(소득공제)")
    @RealGridColumnInfo(headText = "현금영수증\\n(소득공제)", groupName = "면세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long freeTaxIncomeDeductAmt;

    @ApiModelProperty(value = "현금영수증(지출증빙)")
    @RealGridColumnInfo(headText = "현금영수증\\n(지출증빙)", groupName = "면세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long freeTaxExpenditureAmt;

    @ApiModelProperty(value = "기타")
    @RealGridColumnInfo(headText = "기타", groupName = "면세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long freeTaxPgEtcAmt;

    @ApiModelProperty(value = "홈플러스할인")
    @RealGridColumnInfo(headText = "홈플러스할인", groupName = "면세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long freeTaxHomeDiscountAmt;

    @ApiModelProperty(value = "서비스이용료")
    @RealGridColumnInfo(headText = "서비스이용료", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long saleAgencyFee;

    @ApiModelProperty(value = "주문상세내역")
    @RealGridColumnInfo(headText = "주문상세내역", sortable = true, width = 120, columnType = RealGridColumnType.BUTTON, alwaysShowButton = true, buttonType = RealGridButtonType.ACTION)
    private String actionType;
    
}
