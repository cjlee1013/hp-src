package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.settle.admin.mapper.DcOnlineReceivePaySlaveMapper;
import kr.co.homeplus.settle.admin.model.dcOnlineReceivePay.DcOnlineOrderDetailListGetDto;
import kr.co.homeplus.settle.admin.model.dcOnlineReceivePay.DcOnlineOrderDetailListSetDto;
import kr.co.homeplus.settle.admin.model.dcOnlineReceivePay.DcOnlineReceivePayListGetDto;
import kr.co.homeplus.settle.admin.model.dcOnlineReceivePay.DcOnlineReceivePayListSetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DcOnlineReceivePayService {
    private final DcOnlineReceivePaySlaveMapper dcOnlineReceivePaySlaveMapper;

    /**
     * DC온라인수불 조회 > 메인 리스트 조회
     */
    public List<DcOnlineReceivePayListGetDto> getDcOnlineReceivePayList(DcOnlineReceivePayListSetDto dcOnlineReceivePayListSetDto) throws Exception {
        return dcOnlineReceivePaySlaveMapper.selectDcOnlineReceivePayList(dcOnlineReceivePayListSetDto);
    }

    /**
     * DC온라인수불 조회 > 주문상세 리스트 조회
     */
    public List<DcOnlineOrderDetailListGetDto> getDcOnlineOrderDetailList(DcOnlineOrderDetailListSetDto dcOnlineOrderDetailListSetDto) throws Exception {
        return dcOnlineReceivePaySlaveMapper.selectDcOnlineOrderDetailList(dcOnlineOrderDetailListSetDto);
    }

}
