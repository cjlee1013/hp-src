package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.settle.admin.mapper.PgCompareSlaveMapper;
import kr.co.homeplus.settle.admin.model.pgCompare.PgCompareCostCommissionSetDto;
import kr.co.homeplus.settle.admin.model.pgCompare.PgCompareCostDiffGetDto;
import kr.co.homeplus.settle.admin.model.pgCompare.PgCompareCostSumGetDto;
import kr.co.homeplus.settle.admin.model.pgCompare.PgCompareCostSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class PgCompareService {
  private final PgCompareSlaveMapper pgCompareSlaveMapper;
  public List<PgCompareCostSumGetDto> getPgCompareCostSum(PgCompareCostSetDto pgCompareCostSetDto){
    return pgCompareSlaveMapper.getPgCompareCostSum(pgCompareCostSetDto);

  }
  public List<PgCompareCostDiffGetDto> getPgCompareCostDiff(PgCompareCostSetDto pgCompareCostSetDto){
    return pgCompareSlaveMapper.getPgCompareCostDiff(pgCompareCostSetDto);

  }
  public List<PgCompareCostDiffGetDto> getPgCompareCostCommission(
      PgCompareCostCommissionSetDto pgCompareCostCommissionSetDto){
    if ("amount".equals(pgCompareCostCommissionSetDto.getFlagType())) {
      return pgCompareSlaveMapper.getPgCompareCostCommissionAmt(pgCompareCostCommissionSetDto);
    } else {
      return pgCompareSlaveMapper.getPgCompareCostCommissionBalance(pgCompareCostCommissionSetDto);
    }
  }
}
