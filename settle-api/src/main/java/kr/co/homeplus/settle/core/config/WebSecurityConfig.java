package kr.co.homeplus.settle.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${spring.security.user.name}")
    private String securityUserName;

    @Value("${spring.security.user.password}")
    private String securityUserPassword;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("user")
                .password("{noop}password")
                .roles("USER")
                .and()
                .withUser(securityUserName)
                .password("{noop}"+securityUserPassword)
                .roles("USER", "ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // For example: Use only Http Basic and not form login.
        http.httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/")
                .hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/swagger-ui.html")
                .hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/v2/api-docs")
                .hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/configuration/ui")
                .hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/swagger-resources")
                .hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/configuration/security")
                .hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/webjars/**")
                .hasRole("ADMIN")
                .and()
                .csrf()
                .disable()
                .formLogin()
                .disable();
    }
}
