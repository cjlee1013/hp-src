package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.ocb.PgOcbListSetDto;
import kr.co.homeplus.settle.admin.model.ocb.PgOcbStoreListGetDto;
import kr.co.homeplus.settle.admin.model.ocb.PgOcbDailyListGetDto;
import kr.co.homeplus.settle.admin.model.ocb.PgOcbTidListGetDto;
import kr.co.homeplus.settle.admin.service.PgOcbPayService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/ocb")
@Api(tags = "BackOffice > OCB 실적조회")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class PgOcbPayController {
    private final PgOcbPayService pgOcbPayService;

    @ApiOperation(value = "정산관리 > 운영관리 > OCB실적조회 > 지점별 실적조회", response = ResponseObject.class)
    @PostMapping("/getStorePgOcbPay")
    public ResponseObject<List<PgOcbStoreListGetDto>> getStorePgOcbPay(@RequestBody PgOcbListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(pgOcbPayService.getStorePgOcbPay(listParamDto));
    }

    @ApiOperation(value = "정산관리 > 운영관리 > OCB실적조회 > 일별 실적조회", response = ResponseObject.class)
    @PostMapping("/getDailyPgOcbPay")
    public ResponseObject<List<PgOcbDailyListGetDto>> getDailyPgOcbPay(@RequestBody PgOcbListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(pgOcbPayService.getDailyPgOcbPay(listParamDto));
    }

    @ApiOperation(value = "정산관리 > 운영관리 > OCB실적조회 > 건별 실적조회", response = ResponseObject.class)
    @PostMapping("/getTidPgOcbPay")
    public ResponseObject<List<PgOcbTidListGetDto>> getTidPgOcbPay(@RequestBody PgOcbListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(pgOcbPayService.getTidPgOcbPay(listParamDto));
    }
}