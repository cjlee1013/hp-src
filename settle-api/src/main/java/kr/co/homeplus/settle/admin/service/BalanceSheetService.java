package kr.co.homeplus.settle.admin.service;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.settle.admin.mapper.BalanceSheetMasterMapper;
import kr.co.homeplus.settle.admin.mapper.BalanceSheetSlaveMapper;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetDetailGetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetDetailSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetInterfaceGetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetInterfaceSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetListGetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetListSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetSlipTypeGetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
@Configuration
public class BalanceSheetService {
  @Value("${spring.profiles}")
  private String profile;

  private final BalanceSheetMasterMapper balanceSheetMasterMapper;
  private final BalanceSheetSlaveMapper balanceSheetSlaveMapper;

  public List<BalanceSheetListGetDto> getBalanceSheetList(BalanceSheetListSetDto balanceSheetListSetDto) {
    return balanceSheetSlaveMapper.getBalanceSheetList(balanceSheetListSetDto);
  }

  public List<BalanceSheetDetailGetDto> getBalanceSheetDetail(BalanceSheetDetailSetDto balanceSheetDetailSetDto) {
    return balanceSheetSlaveMapper.getBalanceSheetDetail(balanceSheetDetailSetDto);
  }

  public List<BalanceSheetSlipTypeGetDto> getSlipTypeList() {
    List<BalanceSheetSlipTypeGetDto> slipTypeList = new ArrayList<>();
    if ("local".equals(profile)||"dev".equals(profile)||"qa".equals(profile)) {
      slipTypeList.add(new BalanceSheetSlipTypeGetDto("","전체"));
    }
    slipTypeList.addAll(balanceSheetSlaveMapper.getSlipTypeList());
    return slipTypeList;
  }

  public BalanceSheetInterfaceGetDto setBalanceSheetInterface(BalanceSheetInterfaceSetDto balanceSheetInterfaceSetDto) {
    return balanceSheetMasterMapper.setBalanceSheetInterface(balanceSheetInterfaceSetDto);
  }
}
