package kr.co.homeplus.settle.admin.model.statStorePeriodEKPI;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StorePeriodListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(value = "거래유형")
    private String mallType;

    @ApiModelProperty(value = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "날짜타입") //D:일별, M:월별
    private String dateType;

    @ApiModelProperty(value = "검색타입") //A:전체합계, D:날짜별합계
    private String searchType;

    @ApiModelProperty(value = "점포코드")
    private String originStoreId;

    @ApiModelProperty(value = "마켓유형")
    private String marketType;

}
