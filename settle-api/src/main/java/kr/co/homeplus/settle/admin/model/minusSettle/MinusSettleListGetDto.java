package kr.co.homeplus.settle.admin.model.minusSettle;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "정산관리 > 역정산 관리")
public class MinusSettleListGetDto {
    @ApiModelProperty(value = "정산상태")
    private String settleState;

    @ApiModelProperty(value = "지급SRL")
    private String settleSrl;

    @ApiModelProperty(value = "지급예정일")
    private String settlePreDt;

    @ApiModelProperty(value = "지급방법")
    private String settlePeriodCd;

    @ApiModelProperty(value = "정산주기")
    private String settleCycleType;

    @ApiModelProperty(value = "집계기간")
    private String settleStartEndDt;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "업체코드")
    private String vendorCd;

    @ApiModelProperty(value = "판매업체명")
    private String partnerNm;

    @ApiModelProperty(value = "상호명")
    private String businessNm;

    @ApiModelProperty(value = "매출집계금액")
    private String saleSumAmt;

    @ApiModelProperty(value = "배송비")
    private String shipAmt;

    @ApiModelProperty(value = "반품배송비")
    private String claimReturnShipAmt;

    @ApiModelProperty(value = "판매수수료(EX VAT)")
    private String saleFeeExVat;

    @ApiModelProperty(value = "판매수수료(VAT)")
    private String saleFeeVat;

    @ApiModelProperty(value = "판매수수료(IN VAT)")
    private String saleFeeInVat;

    @ApiModelProperty(value = "정산(IN VAT)")
    private String settleInVatAmt;

    @ApiModelProperty(value = "자사할인")
    private String homeChargeAmt;

    @ApiModelProperty(value = "상품할인(업체)")
    private String couponSellerChargeAmt;

    @ApiModelProperty(value = "배송비할인(업체)")
    private String shipDiscountAmt;

    @ApiModelProperty(value = "조정금액")
    private String taxAdjustAmt;

    @ApiModelProperty(value = "조정수수료")
    private String adjustFee;

    @ApiModelProperty(value = "정산대상금액")
    private String settleAmt;

    @ApiModelProperty(value = "정산예정금액")
    private String settlePreAmt;

    @ApiModelProperty(value = "이전보류금액")
    private String holdAmt;

    @ApiModelProperty(value = "지급예정금액")
    private String settleGivePreAmt;

    @ApiModelProperty(value = "보류발생금액")
    private String holdOccurAmt;

    @ApiModelProperty(value = "실지급액")
    private String realAmt;

    @ApiModelProperty(value = "변경일시")
    private String chgDt;

    @ApiModelProperty(value = "변경자")
    private String chgId;
}
    
    