package kr.co.homeplus.settle.partner.model.settleInfo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DailySettleSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    private String itemNm;
}
