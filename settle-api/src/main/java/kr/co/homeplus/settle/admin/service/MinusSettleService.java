package kr.co.homeplus.settle.admin.service;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.settle.admin.mapper.MinusSettleMasterMapper;
import kr.co.homeplus.settle.admin.mapper.MinusSettleSlaveMapper;
import kr.co.homeplus.settle.admin.model.minusSettle.MinusSettleCompleteSetDto;
import kr.co.homeplus.settle.admin.model.minusSettle.MinusSettleListGetDto;
import kr.co.homeplus.settle.admin.model.minusSettle.MinusSettleListSetDto;
import kr.co.homeplus.settle.core.exception.handler.ExceptionCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class MinusSettleService {
    private final MinusSettleSlaveMapper minusSettleSlaveMapper;
    private final MinusSettleMasterMapper minusSettleMasterMapper;
    private final PgReceivableService pgReceivableService;

    /**
     * DS역정산관리 > 메인 리스트 조회
     */
    public List<MinusSettleListGetDto> getMinusSettleList(MinusSettleListSetDto minusSettleListSetDto) throws Exception {
        List<MinusSettleListGetDto> minusSettleListGetDtoList = minusSettleSlaveMapper.selectMinusSettleList(minusSettleListSetDto);

        for (MinusSettleListGetDto dto : minusSettleListGetDtoList) {
            dto.setPartnerId(PrivacyMaskingUtils.maskingUserId(dto.getPartnerId()));
        }

        return minusSettleListGetDtoList;
    }

    /**
     * DS역정산관리 > 수기마감
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseObject<Integer> setMinusSettleComplete(MinusSettleCompleteSetDto minusSettleCompleteSetDto) {
        int returnVal = 0;

        try {
            // 정산 정보 조회
            Map<String,Object> minusSettleInfo = minusSettleMasterMapper.selectMinusSettleInfo(minusSettleCompleteSetDto.getSettleSrl());

            // 정산상태 업데이트
            returnVal = minusSettleMasterMapper.updateFinalSettleState(minusSettleCompleteSetDto.getSettleSrl(), "5", minusSettleCompleteSetDto.getChgId());

            if (returnVal != 1) {
                return ResourceConverter.toResponseObject(returnVal, HttpStatus.OK, ExceptionCode.ERROR_CODE_2005.getResponseCode(), ExceptionCode.ERROR_CODE_2005.getResponseMessage());
            }

            // 업체 보류금액 업데이트
            returnVal = minusSettleMasterMapper.updateMinusBank(minusSettleCompleteSetDto.getSettleSrl());

            if (returnVal != 1) {
                return ResourceConverter.toResponseObject(returnVal, HttpStatus.OK, ExceptionCode.ERROR_CODE_2006.getResponseCode(), ExceptionCode.ERROR_CODE_2006.getResponseMessage());
            }

            // 업체 보류금액 히스토리 저장
            returnVal = minusSettleMasterMapper.insertMinusBankHistory(minusSettleCompleteSetDto.getSettleSrl(), minusSettleCompleteSetDto.getChgId());

            // AP 역정산 환입 전표 생성
            minusSettleMasterMapper.setApMinusDs(minusSettleCompleteSetDto.getSettleSrl());

            pgReceivableService.setMinusSettleComplete(minusSettleCompleteSetDto.getSettleSrl(), Long.parseLong(minusSettleInfo.get("settle_amt").toString()));

            return ResourceConverter.toResponseObject(returnVal, HttpStatus.OK, ResponseObject.RETURN_CODE_SUCCESS, "수기마감 완료");
        } catch (Exception e) {
            log.error("수기마감오류:"+e.getMessage());
            return ResourceConverter.toResponseObject(returnVal, HttpStatus.OK, ExceptionCode.ERROR_CODE_2007.getResponseCode(), ExceptionCode.ERROR_CODE_2007.getResponseMessage());
        }
    }

}
