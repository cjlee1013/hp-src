package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.pgReceivable.PgReceivableGetDto;
import kr.co.homeplus.settle.admin.model.pgSettle.PgSettleDetailGetDto;
import kr.co.homeplus.settle.admin.model.pgSettle.PgSettleSetDto;
import kr.co.homeplus.settle.admin.model.pgSettle.PgSettleSumGetDto;
import kr.co.homeplus.settle.admin.service.PgSettleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/pgSettle")
@Api(tags = "BackOffice > PG정산관리")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class PgSettleController {

  private final PgSettleService pgSettleService;

  @ApiOperation(value = "합계 내역", response = ResponseObject.class, notes = "합계 내역")
  @RequestMapping(value = "/getPgSettleSum", method = {RequestMethod.POST})
  public ResponseObject<List<PgSettleSumGetDto>> getPgSettleSum (@RequestBody @Valid PgSettleSetDto pgSettleSetDto) {
    List<PgSettleSumGetDto> pgSettleSumGetDto = pgSettleService.getPgSettleSum(pgSettleSetDto);
    return ResourceConverter.toResponseObject(pgSettleSumGetDto);
  }

  @ApiOperation(value = "일별 상세 내역", response = ResponseObject.class, notes = "일별 상세 내역")
  @RequestMapping(value = "/getPgSettleDetail", method = {RequestMethod.POST})
  public ResponseObject<List<PgSettleDetailGetDto>> getPgSettleDetail (@RequestBody @Valid PgSettleSetDto pgSettleSetDto) {
    List<PgSettleDetailGetDto> pgSettleDetailGetDto = pgSettleService.getPgSettleDetail(pgSettleSetDto);
    return ResourceConverter.toResponseObject(pgSettleDetailGetDto);
  }

  @ApiOperation(value = "히스토리 상세 내역", response = ResponseObject.class, notes = "히스토리 상세 내역")
  @RequestMapping(value = "/getPgReceivable", method = {RequestMethod.POST})
  public ResponseObject<List<PgReceivableGetDto>> getPgReceivable (@RequestBody @Valid PgSettleSetDto pgSettleSetDto) {
    List<PgReceivableGetDto> pgSettleDetailGetDto = pgSettleService.getPgReceivable(pgSettleSetDto);
    return ResourceConverter.toResponseObject(pgSettleDetailGetDto);
  }
}
