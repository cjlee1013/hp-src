package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.statDailyDivisionEKPI.DailyDivisionEKPIListGetDto;
import kr.co.homeplus.settle.admin.model.statDailyDivisionEKPI.DailyDivisionEKPIListSetDto;
import kr.co.homeplus.settle.admin.model.statDailyDivisionEKPI.DailySettleMarketItemGetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface StatDailyDivisionEKPISlaveMapper {
    List<DailyDivisionEKPIListGetDto> getDailyDivisionEKPIList(DailyDivisionEKPIListSetDto listParamDto);

    List<DailySettleMarketItemGetDto> getDailySettleMarketItemList(DailyDivisionEKPIListSetDto listParamDto);
}
