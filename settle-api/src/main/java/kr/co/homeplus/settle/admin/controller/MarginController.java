package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.margin.MarginDetailListGetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginDetailListSetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginHeadListGetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginPartListGetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginTeamListGetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginListSetDto;
import kr.co.homeplus.settle.admin.service.MarginService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/margin")
@Api(tags = "BackOffice > DC/DS 마진조회")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class MarginController {
    private final MarginService marginService;

    @ApiOperation(value = "DC/DS 마진조회 > 팀별 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getMarginTeamList")
    public ResponseObject<List<MarginTeamListGetDto>> getMarginTeamList(@RequestBody MarginListSetDto marginListSetDto) throws Exception {
        return ResourceConverter.toResponseObject(marginService.getMarginTeamList(marginListSetDto));
    }

    @ApiOperation(value = "DC/DS 마진조회 > 파트별 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getMarginPartList")
    public ResponseObject<List<MarginPartListGetDto>> getMarginPartList(@RequestBody MarginListSetDto marginListSetDto) throws Exception {
        return ResourceConverter.toResponseObject(marginService.getMarginPartList(marginListSetDto));
    }

    @ApiOperation(value = "DC/DS 마진조회 > 유형별 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getMarginHeadList")
    public ResponseObject<List<MarginHeadListGetDto>> getMarginHeadList(@RequestBody MarginListSetDto marginListSetDto) throws Exception {
        return ResourceConverter.toResponseObject(marginService.getMarginHeadList(marginListSetDto));
    }

    @ApiOperation(value = "DC/DS 마진상세조회 > 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getMarginDetailList")
    public ResponseObject<List<MarginDetailListGetDto>> getMarginDetailList(@RequestBody MarginDetailListSetDto marginDetailListSetDto) throws Exception {
        return ResourceConverter.toResponseObject(marginService.getMarginDetailList(marginDetailListSetDto));
    }
}