package kr.co.homeplus.settle.partner.model.salesTrend;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import lombok.Data;

@ApiModel("일자 별 조회 및 결제 정보")
@Data
public class SalesByDailyInfo {

    @ApiModelProperty("조회 수")
    private long eventCnt;

    @ApiModelProperty("결제 건 수")
    private long paymentCnt;

    @ApiModelProperty("일자")
    private LocalDate regDt;
}
