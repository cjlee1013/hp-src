package kr.co.homeplus.settle.partner.mapper;

import java.util.List;
import kr.co.homeplus.settle.partner.model.taxBill.PartnerTaxBillListGetDto;
import kr.co.homeplus.settle.partner.model.taxBill.PartnerTaxBillListSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface PartnerTaxBillSlaveMapper {
    List<PartnerTaxBillListGetDto> getTaxBillList(PartnerTaxBillListSetDto listParamDto);
}
