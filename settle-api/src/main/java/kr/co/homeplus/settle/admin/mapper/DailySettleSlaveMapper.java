package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleListGetDto;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleListSetDto;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleInfoGetDto;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleInfoSetDto;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleSumGetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface DailySettleSlaveMapper {
    List<DailySettleListGetDto> getDailySettleList(DailySettleListSetDto listParamDto);

    List<DailySettleInfoGetDto> getDailySettleInfo(DailySettleInfoSetDto listParamDto);

    List<DailySettleSumGetDto> getDailySettleSum(DailySettleListSetDto listParamDto);

    List<DailySettleSumGetDto> getDailySettleStore(DailySettleListSetDto listParamDto);
}
