package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.settle.admin.mapper.StatStorePeriodSlaveMapper;
import kr.co.homeplus.settle.admin.model.statStorePeriodEKPI.StorePeriodListGetDto;
import kr.co.homeplus.settle.admin.model.statStorePeriodEKPI.StorePeriodListSetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StatStorePeriodEKPIService {
    private final StatStorePeriodSlaveMapper settleSlaveMapper;

    /**
     * 통계 > 상품주문통계 > 기간별 점포 판매현황 (E-KPI)
     */
    public List<StorePeriodListGetDto> getStoreList(StorePeriodListSetDto listParamDto) throws Exception {
        return settleSlaveMapper.getStoreList(listParamDto);
    }
}
