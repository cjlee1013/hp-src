package kr.co.homeplus.settle.admin.mapper;

import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerPaymentAccountSetDto;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerPaymentStateSetDto;
import kr.co.homeplus.settle.core.db.annotation.MasterConnection;

@MasterConnection
public interface PartnerPaymentMasterMapper {
    int setPartnerPaymentState(PartnerPaymentStateSetDto partnerPaymentStateSetDto);
    int setPartnerPaymentAccount(PartnerPaymentAccountSetDto partnerPaymentAccountSetDto);
}
