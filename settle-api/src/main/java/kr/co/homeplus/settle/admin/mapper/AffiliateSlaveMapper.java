package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.affiliate.AffiliateDetailGetDto;
import kr.co.homeplus.settle.admin.model.affiliate.AffiliateDetailSetDto;
import kr.co.homeplus.settle.admin.model.affiliate.AffiliateSalesGetDto;
import kr.co.homeplus.settle.admin.model.affiliate.AffiliateSalesSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface AffiliateSlaveMapper {
  List<AffiliateSalesGetDto> getAffiliateSales(AffiliateSalesSetDto affiliateSalesSetDto);
  List<AffiliateDetailGetDto> getAffiliateDetail(AffiliateDetailSetDto affiliateDetailSetDto);
}
