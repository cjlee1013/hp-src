package kr.co.homeplus.settle.admin.model.pgSettle;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "전표조회 목록")
public class PgSettleSumGetDto {
  @ApiModelProperty(notes = "PG사")
  private String pgKind;

  @ApiModelProperty(notes = "결제수단")
  private String paymentTool;

  @ApiModelProperty(notes = "결제금액-결제기준")
  private String basicPg;

  @ApiModelProperty(notes = "결제수수료-결제기준")
  private String basicCommission;

  @ApiModelProperty(notes = "DS 정산대상금액-결제기준")
  private String basicSettle;

  @ApiModelProperty(notes = "입금예정액-결제기준")
  private String basicSum;

  @ApiModelProperty(notes = "결제금액-입금기준")
  private String payPg;

  @ApiModelProperty(notes = "결제수수료-입금기준")
  private String payCommission;

  @ApiModelProperty(notes = "DS 정산대상금액-입금기준")
  private String paySettle;

  @ApiModelProperty(notes = "입금예정액-입금기준")
  private String paySum;

  @ApiModelProperty(notes = "이전예치금")
  private String depositBalance;

  @ApiModelProperty(notes = "예치금")
  private String depositAmt;

  @ApiModelProperty(notes = "지급완료")
  private String settleAmt;

  @ApiModelProperty(notes = "예치금 잔액")
  private String depositLeft;

  @ApiModelProperty(notes = "PG미수금")
  private String receivableAmt;
}
