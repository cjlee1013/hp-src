package kr.co.homeplus.settle.admin.model.monthlySalesVAT;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MonthlySalesVATListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "판매업체명")
    private String partnerNm;

    @ApiModelProperty(value = "사업자번호")
    private String partnerNo;

    @ApiModelProperty(value = "업체코드")
    private String vendorCd;
}
