package kr.co.homeplus.settle.partner.model.finalSettle;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PartnerFinalSettleSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "지급상태")
    private String settlePayState;

    @ApiModelProperty(value = "정산상태")
    private String settleState;
}
