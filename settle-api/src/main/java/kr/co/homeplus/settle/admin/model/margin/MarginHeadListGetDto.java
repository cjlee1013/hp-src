package kr.co.homeplus.settle.admin.model.margin;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MarginHeadListGetDto {
    @ApiModelProperty(value = "유형명")
    private String name;

    @ApiModelProperty(value = "매출(IN VAT)")
    private String completeInAmt;

    @ApiModelProperty(value = "매출(IN VAT) 비율")
    private String completeInAmtRate;

    @ApiModelProperty(value = "매출(EXT VAT)")
    private String completeExAmt;

    @ApiModelProperty(value = "매출(EXT VAT) 비율")
    private String completeExAmtRate;

    @ApiModelProperty(value = "마진")
    private String marginAmt;

    @ApiModelProperty(value = "마진율")
    private String marginAmtRate;
}
