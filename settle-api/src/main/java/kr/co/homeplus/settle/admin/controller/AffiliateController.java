package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.affiliate.AffiliateDetailGetDto;
import kr.co.homeplus.settle.admin.model.affiliate.AffiliateDetailSetDto;
import kr.co.homeplus.settle.admin.model.affiliate.AffiliateSalesGetDto;
import kr.co.homeplus.settle.admin.model.affiliate.AffiliateSalesSetDto;
import kr.co.homeplus.settle.admin.service.AffiliateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/affiliate")
@Api(tags = "BackOffice > 제휴채널실적")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class AffiliateController {
  private final AffiliateService affiliateService;

  @ApiOperation(value = "제휴채널실적 목록", response = ResponseObject.class, notes = "제휴채널실적 목록")
  @RequestMapping(value = "/getAffiliateSales", method = {RequestMethod.POST})
  public ResponseObject<List<AffiliateSalesGetDto>> getAffiliateSales (@RequestBody @Valid
      AffiliateSalesSetDto affiliateSalesSetDto) {
    return ResourceConverter.toResponseObject(affiliateService.getAffiliateSales(
        affiliateSalesSetDto));
  }

  @ApiOperation(value = "제휴채널실적 상세", response = ResponseObject.class, notes = "제휴채널실적 상세")
  @RequestMapping(value = "/getAffiliateDetail", method = {RequestMethod.POST})
  public ResponseObject<List<AffiliateDetailGetDto>> getAffiliateDetail (@RequestBody @Valid
      AffiliateDetailSetDto affiliateDetailSetDto) {
    return ResourceConverter.toResponseObject(affiliateService.getAffiliateDetail(affiliateDetailSetDto));
  }
}
