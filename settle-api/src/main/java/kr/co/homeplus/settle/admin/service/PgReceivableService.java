package kr.co.homeplus.settle.admin.service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import kr.co.homeplus.settle.admin.mapper.PgReceivableMasterMapper;
import kr.co.homeplus.settle.admin.mapper.PgReceivableSlaveMapper;
import kr.co.homeplus.settle.admin.model.pgReceivable.PgCommissionSetDto;
import kr.co.homeplus.settle.admin.model.pgReceivable.PgReceivableGetDto;
import kr.co.homeplus.settle.admin.model.pgReceivable.PgReceivableSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
@Configuration
public class PgReceivableService {
  private final PgReceivableMasterMapper pgReceivableMasterMapper;
  private final PgReceivableSlaveMapper pgReceivableSlaveMapper;

  /*
    PG수수료 차액분
    issueDt : 발행일
    pgKind : PG사 (INICIS,TOSSPG)
    pgFeeAmt : 차액
   */
  public int setPgFeeAllocation(String issueDt, String pgKind, long pgFeeAmt) {
    String basicDt = LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault())
        .format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    String mallType = "공통";

    long transBalance, transAmt = 0;
    long depositBalance, depositAmt = 0;
    long pgReceivableAmt;
    long pgCommissionAmt = pgReceivableSlaveMapper.getPgCommission(new PgCommissionSetDto(issueDt,pgKind));
    PgReceivableGetDto pgBalanceGetDto = pgReceivableSlaveMapper.getPgBalanceAmt(pgKind);
    if (pgBalanceGetDto != null) {
      transBalance = pgBalanceGetDto.getTransBalance();
      depositBalance = pgBalanceGetDto.getDepositBalance();
      pgReceivableAmt = pgBalanceGetDto.getReceivableAmt();
    } else {
      transBalance = 0;
      depositBalance = 0;
      pgReceivableAmt = 0;
    }
    transAmt = pgCommissionAmt - pgFeeAmt;
    depositAmt = 0;
    transBalance = transBalance + transAmt;
    depositBalance = depositBalance + depositAmt;
    pgReceivableAmt = pgReceivableAmt + transAmt + depositAmt;

    PgReceivableSetDto pgReceivableSetDto = new PgReceivableSetDto(basicDt, issueDt, pgKind,null, mallType, "7"
        , null, (transAmt), (transBalance), (depositAmt), (depositBalance),
        (pgReceivableAmt));
    return pgReceivableMasterMapper.setPgReceivable(pgReceivableSetDto);
  }

  /*
    역정산 수기마감
    settleSrl : 지급SRL
    minusAmt : 지급예정금액
   */
  public int setMinusSettleComplete(String settleSrl, long minusAmt) {
    String pgKind = "INICIS";
    String basicDt = LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault())
        .format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    String mallType = "DS";

    long transBalance, transAmt = 0;
    long depositBalance, depositAmt = 0;
    long pgReceivableAmt;
    PgReceivableGetDto pgBalanceGetDto = pgReceivableSlaveMapper.getPgBalanceAmt(pgKind);
    if (pgBalanceGetDto != null) {
      transBalance = pgBalanceGetDto.getTransBalance();
      depositBalance = pgBalanceGetDto.getDepositBalance();
      pgReceivableAmt = pgBalanceGetDto.getReceivableAmt();
    } else {
      transBalance = 0;
      depositBalance = 0;
      pgReceivableAmt = 0;
    }

    transAmt = 0;
    depositAmt = (-1) * minusAmt;
    transBalance = transBalance + transAmt;
    depositBalance = depositBalance + depositAmt;
    pgReceivableAmt = pgReceivableAmt + transAmt + depositAmt;

    PgReceivableSetDto pgReceivableSetDto = new PgReceivableSetDto(basicDt, basicDt, pgKind,null, mallType, "8"
        , settleSrl, (transAmt), (transBalance), (depositAmt), (depositBalance),
        (pgReceivableAmt));
    return pgReceivableMasterMapper.setPgReceivable(pgReceivableSetDto);
  }
}
