package kr.co.homeplus.settle.batch.model;

import lombok.Data;
import lombok.Setter;

@Data
@Setter
public class PartnerInfoDto {
    private String partnerId;
    private String partnerNo;
    private String partnerNm;
    private String partnerOwner;
    private String bankAccountHolder;
    private String bankCode;
    private String bankAccount;
    private String phoneNo;
    private String resultCode;
    private String registYn;
}
