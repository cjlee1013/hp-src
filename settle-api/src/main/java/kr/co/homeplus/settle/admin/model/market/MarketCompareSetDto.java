package kr.co.homeplus.settle.admin.model.market;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제일마감")
public class MarketCompareSetDto {
  @ApiModelProperty(notes = "시작일자")
  private String startDt;

  @ApiModelProperty(notes = "종료일자")
  private String endDt;

  @ApiModelProperty(notes = "검색기간")
  private String dateType;

  @ApiModelProperty(notes = "제휴사")
  private String siteType;
}
