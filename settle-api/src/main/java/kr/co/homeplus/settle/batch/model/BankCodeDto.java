package kr.co.homeplus.settle.batch.model;

import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class BankCodeDto {
    private String homeBankCd;
    private String inicisBankCd;
    private String tossBankCd;
}
