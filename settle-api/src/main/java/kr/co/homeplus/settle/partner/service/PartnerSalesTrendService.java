package kr.co.homeplus.settle.partner.service;

import java.util.List;
import kr.co.homeplus.settle.partner.mapper.PartnerSalesTrendSlaveMapper;
import kr.co.homeplus.settle.partner.model.salesTrend.SalesByAgeInfo;
import kr.co.homeplus.settle.partner.model.salesTrend.SalesByDailyInfo;
import kr.co.homeplus.settle.partner.model.salesTrend.SalesByGenderInfo;
import kr.co.homeplus.settle.partner.model.salesTrend.SalesTrendSummaryInfo;
import kr.co.homeplus.settle.partner.model.salesTrend.SearchConditionDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class PartnerSalesTrendService {

    private final PartnerSalesTrendSlaveMapper partnerSalesTrendSlaveMapper;

    public SalesTrendSummaryInfo searchSummary(final SearchConditionDto conditionDto) {
        return partnerSalesTrendSlaveMapper.selectSalesTrendSummaryInfo(conditionDto);
    }

    public List<SalesByDailyInfo> searchSalesByDaily(final SearchConditionDto conditionDto) {
        return partnerSalesTrendSlaveMapper.selectSalesByDailyInfo(conditionDto);
    }

    public List<SalesByGenderInfo> searchSalesByGender(final SearchConditionDto conditionDto) {
        return partnerSalesTrendSlaveMapper.selectSalesByGenderInfo(conditionDto);
    }

    public List<SalesByAgeInfo> searchSalesByAge(final SearchConditionDto conditionDto) {
        return partnerSalesTrendSlaveMapper.selectSalesByAgeInfo(conditionDto);
    }
}
