package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.settle.admin.mapper.AdvanceSlaveMapper;
import kr.co.homeplus.settle.admin.model.advance.AdvanceListGetDto;
import kr.co.homeplus.settle.admin.model.advance.AdvanceListSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
@Configuration
public class AdvanceService {
  private final AdvanceSlaveMapper advanceSlaveMapper;

  public List<AdvanceListGetDto> getAdvanceList(AdvanceListSetDto advanceListSetDto) {
    List<AdvanceListGetDto> advanceListGetDtoList = advanceSlaveMapper.getAdvanceUnshipList(advanceListSetDto);
    advanceListGetDtoList.addAll(advanceSlaveMapper.getAdvanceSettleList(advanceListSetDto));
    if ("Y".equals(advanceListSetDto.getIncludeZero())) {
      advanceListGetDtoList.addAll(advanceSlaveMapper.getAdvanceZeroList(advanceListSetDto));
    }
    return advanceListGetDtoList;
  }
  public List<AdvanceListGetDto> getAdvanceZeroList(AdvanceListSetDto advanceListSetDto) {
    return advanceSlaveMapper.getAdvanceZeroList(advanceListSetDto);
  }
}
