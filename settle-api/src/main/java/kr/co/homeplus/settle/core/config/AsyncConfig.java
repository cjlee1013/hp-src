package kr.co.homeplus.settle.core.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class AsyncConfig {

    @Bean
    @Primary
    @Qualifier("asyncCreateTdUnshipSlipTask")
    public TaskExecutor asyncCreateTdUnshipSlip() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(10);                         //최초에 생성할 Pool개수
        threadPoolTaskExecutor.setMaxPoolSize(30);                          //몇개까지 Pool을 생성할 것인지
        threadPoolTaskExecutor.setQueueCapacity(100);                       //Max Thread가 동작하는 경우 대기하는 Queue 사이
        threadPoolTaskExecutor.setThreadNamePrefix("asyncCreateTdUnshipSlip-");  //Thread 생성시 가장 앞에 붙는 이름
        threadPoolTaskExecutor.initialize();
        return threadPoolTaskExecutor;
    }

}
