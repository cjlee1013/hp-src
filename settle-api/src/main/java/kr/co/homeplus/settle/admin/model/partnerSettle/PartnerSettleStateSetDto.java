package kr.co.homeplus.settle.admin.model.partnerSettle;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PartnerSettleStateSetDto {
    @ApiModelProperty(value = "지급SRL")
    private long settleSrl;

    @ApiModelProperty(value = "정산상태")
    private String settleState;

    @ApiModelProperty(value = "정산상태 변경 상세")
    private String changeMemo;

    @ApiModelProperty(value = "처리자")
    private String chgId;
}
