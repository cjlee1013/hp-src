package kr.co.homeplus.settle.admin.model.accounting;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class APBalanceSheetGetDto {
    @ApiModelProperty(value = "기준일")
    @RealGridColumnInfo(headText = "기준일", width = 100, sortable = true, fieldType = RealGridFieldType.TEXT)
    private String basicDt;

    @ApiModelProperty(notes = "전표유형")
    @RealGridColumnInfo(headText = "전표유형", width = 110, sortable = true)
    private String slipName;

    @ApiModelProperty(notes = "Invoice번호")
    @RealGridColumnInfo(headText = "Invoice번호", width = 130, sortable = true)
    private String invoiceNum;

    @ApiModelProperty(notes = "라인번호")
    @RealGridColumnInfo(headText = "라인번호", width = 100, sortable = true)
    private String lineNumber;

    @ApiModelProperty(notes = "라인구분")
    @RealGridColumnInfo(headText = "라인구분", width = 100, sortable = true)
    private String lineTypeLookupCode;

    @ApiModelProperty(notes = "과세구분")
    @RealGridColumnInfo(headText = "과세구분", width = 100, sortable = true)
    private String taxCode;

    @ApiModelProperty(value = "금액")
    @RealGridColumnInfo(headText = "금액", width = 100, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long invoiceAmount;

    @ApiModelProperty(value = "계정코드")
    @RealGridColumnInfo(headText = "계정코드", width = 100, sortable = true)
    private String globalAttribute14;

    @ApiModelProperty(value = "계정과목명")
    @RealGridColumnInfo(headText = "계정과목명", width = 100, sortable = true)
    private String accountName;

    @ApiModelProperty(notes = "코스트센터")
    @RealGridColumnInfo(headText = "코스트센터", width = 100, sortable = true)
    private String globalAttribute15;

    @ApiModelProperty(notes = "코스트센터명")
    @RealGridColumnInfo(headText = "코스트센터명", width = 120, sortable = true)
    private String costCenterName;

    @ApiModelProperty(notes = "설명")
    @RealGridColumnInfo(headText = "설명", width = 240, sortable = true)
    private String description;

}
