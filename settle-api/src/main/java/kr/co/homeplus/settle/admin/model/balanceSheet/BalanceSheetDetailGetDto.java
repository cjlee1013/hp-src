package kr.co.homeplus.settle.admin.model.balanceSheet;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "전표조회 상세")
public class BalanceSheetDetailGetDto {
  @ApiModelProperty(notes = "기준일")
  private String basicDt;

  @ApiModelProperty(notes = "전표유형코드")
  private String slipType;

  @ApiModelProperty(notes = "전표유형")
  private String slipName;

  @ApiModelProperty(notes = "코스트센터")
  private String costCenter;

  @ApiModelProperty(notes = "코스트센터명")
  private String costCenterName;

  @ApiModelProperty(notes = "카테고리구분")
  private String category;

  @ApiModelProperty(notes = "계정코드")
  private String accountCode;

  @ApiModelProperty(notes = "계정과목명")
  private String accountName;

  @ApiModelProperty(notes = "적요")
  private String description;

  @ApiModelProperty(notes = "차변")
  private String drAmt;

  @ApiModelProperty(notes = "대변")
  private String crAmt;

}
