package kr.co.homeplus.settle.admin.model.adjust;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AdjustRegisterGetDto {
    @ApiModelProperty(notes = "결과 코드")
    private String resultCd;

    @ApiModelProperty(notes = "결과 상세 메시지")
    private String resultMsg;
}
