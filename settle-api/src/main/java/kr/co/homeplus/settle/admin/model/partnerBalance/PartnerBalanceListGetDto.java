package kr.co.homeplus.settle.admin.model.partnerBalance;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PartnerBalanceListGetDto {
    @ApiModelProperty(value = "기간")
    private String basicDt;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "업체코드")
    private String vendorCd;

    @ApiModelProperty(value = "판매업체명")
    private String partnerNm;

    @ApiModelProperty(value = "상호명")
    private String businessNm;

    @ApiModelProperty(value = "발생내역(결제완료)")
    private String paymentSettleAmt;

    @ApiModelProperty(value = "발생내역(매출집계)")
    private String settleSettleAmt;

    @ApiModelProperty(value = "발생내역(지급완료)")
    private String finalSettleAmt;

    @ApiModelProperty(value = "발생내역(역정산환입)")
    private String minusSettleAmt;

    @ApiModelProperty(value = "누적합계(결제완료)")
    private String paymentSettleSum;

    @ApiModelProperty(value = "누적합계(매출집계)")
    private String settleSettleSum;

    @ApiModelProperty(value = "누적합계(지급완료)")
    private String finalSettleSum;

    @ApiModelProperty(value = "누적합계(역정산환입)")
    private String minusSettleSum;

    @ApiModelProperty(value = "선수금잔액")
    private String advanceAmt;

    @ApiModelProperty(value = "예수금잔액")
    private String balanceAmt;
}
