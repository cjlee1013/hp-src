package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.settle.admin.mapper.UnshipSlipMasterMapper;
import kr.co.homeplus.settle.admin.mapper.UnshipSlipSlaveMapper;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipCreateGetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipCreateSetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipListGetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipListSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class UnshipSlipService {
    private final UnshipSlipSlaveMapper unshipSlipSlaveMapper;
    private final UnshipSlipMasterMapper unshipSlipMasterMapper;

    /**
     * TD미배송 전표조회 > 메인 리스트 조회
     */
    public List<TdUnshipSlipListGetDto> getTdUnshipSlipList(TdUnshipSlipListSetDto tdUnshipSlipListSetDto) throws Exception {
        return unshipSlipSlaveMapper.selectTdUnshipSlipList(tdUnshipSlipListSetDto);
    }

    /**
     * TD미배송 전표조회 > 전표생성 가능 확인
     */
    public int getTdUnshipSlipCnt(TdUnshipSlipCreateSetDto tdUnshipSlipCreateSetDto) throws Exception {
        return unshipSlipSlaveMapper.selectTdUnshipSlipCnt(tdUnshipSlipCreateSetDto);
    }

    /**
     * TD미배송 전표조회 > 전표생성
     */
    @Async("asyncCreateTdUnshipSlipTask")
    public void setTdUnshipSlip(TdUnshipSlipCreateSetDto tdUnshipSlipCreateSetDto) {
        try {
            // 전표생성
            TdUnshipSlipCreateGetDto dto = unshipSlipMasterMapper.insertTdUnshipSlip(tdUnshipSlipCreateSetDto);
        } catch (Exception e) {
            log.error("setTdUnshipSlip Error {} " , e.getMessage());
        }
    }
}
