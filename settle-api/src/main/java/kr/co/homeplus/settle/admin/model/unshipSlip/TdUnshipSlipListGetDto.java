package kr.co.homeplus.settle.admin.model.unshipSlip;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TdUnshipSlipListGetDto {
    @ApiModelProperty(value = "마감월")
    private String basicDt;

    @ApiModelProperty(value = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "점포명")
    private String costCenterName;

    @ApiModelProperty(value = "코스트센터")
    private String costCenter;

    @ApiModelProperty(value = "회사구분")
    private String company;

    @ApiModelProperty(value = "카테고리구분")
    private String category;

    @ApiModelProperty(value = "선수금일반")
    private String amt20800;

    @ApiModelProperty(value = "매출액차감_미배송")
    private String amt30376;

    @ApiModelProperty(value = "매출부가세")
    private String amt24290;

    @ApiModelProperty(value = "점출부가세")
    private String amt30200;

    @ApiModelProperty(value = "상품원가_직영/특정")
    private String amt35000;

    @ApiModelProperty(value = "매장상품_직매입")
    private String amt14160;
}
