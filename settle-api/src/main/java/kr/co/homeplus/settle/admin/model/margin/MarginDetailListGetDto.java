package kr.co.homeplus.settle.admin.model.margin;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MarginDetailListGetDto {
    @ApiModelProperty(value = "매출일")
    private String basicDt;

    @ApiModelProperty(value = "과면세")
    private String taxYn;

    @ApiModelProperty(value = "카테고리ID")
    private String deptNo;

    @ApiModelProperty(value = "CLASS")
    private String classNo;

    @ApiModelProperty(value = "SUB CLASS")
    private String subClassNo;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "업체코드")
    private String vendorCd;

    @ApiModelProperty(value = "판매업체명")
    private String partnerName;

    @ApiModelProperty(value = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    private String itemName;

    @ApiModelProperty(value = "상품금액")
    private String itemSaleAmt;

    @ApiModelProperty(value = "수량")
    private String completeQty;

    @ApiModelProperty(value = "할인금액")
    private String discountAmt;

    @ApiModelProperty(value = "매출(IN VAT)")
    private String completeInAmt;

    @ApiModelProperty(value = "매출(EX VAT)")
    private String completeExAmt;

    @ApiModelProperty(value = "원가(IN VAT)")
    private String orgInAmt;

    @ApiModelProperty(value = "원가(EX VAT)")
    private String orgExAmt;

    @ApiModelProperty(value = "마진")
    private String margin;

    @ApiModelProperty(value = "마진율")
    private String marginRate;
}
