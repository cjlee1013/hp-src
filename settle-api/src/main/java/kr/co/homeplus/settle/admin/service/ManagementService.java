package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.settle.admin.mapper.ManagementSlaveMapper;
import kr.co.homeplus.settle.admin.model.management.DgvPaymentDailyGetDto;
import kr.co.homeplus.settle.admin.model.management.DgvPaymentListGetDto;
import kr.co.homeplus.settle.admin.model.management.DgvPaymentSetDto;
import kr.co.homeplus.settle.admin.model.management.DgvPaymentStoreGetDto;
import kr.co.homeplus.settle.admin.model.management.MhcPaymentDailyGetDto;
import kr.co.homeplus.settle.admin.model.management.MhcPaymentListGetDto;
import kr.co.homeplus.settle.admin.model.management.MhcPaymentSetDto;
import kr.co.homeplus.settle.admin.model.management.MhcPaymentStoreGetDto;
import kr.co.homeplus.settle.admin.model.management.PaymentInfoListGetDto;
import kr.co.homeplus.settle.admin.model.management.PaymentInfoListSetDto;
import kr.co.homeplus.settle.admin.model.management.PaymentInfoSumGetDto;
import kr.co.homeplus.settle.admin.model.management.PgPaymentInfoListGetDto;
import kr.co.homeplus.settle.admin.model.management.PgPaymentInfoListSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ManagementService {
  private final ManagementSlaveMapper managementSlaveMapper;

  public List<PaymentInfoSumGetDto> getPaymentInfoSum(PaymentInfoListSetDto paymentInfoListSetDto) {
    return managementSlaveMapper.getPaymentInfoSum(paymentInfoListSetDto);
  }

  public List<PaymentInfoListGetDto> getPaymentInfoList(PaymentInfoListSetDto paymentInfoListSetDto) {
    return managementSlaveMapper.getPaymentInfoList(paymentInfoListSetDto);
  }

  public List<PaymentInfoSumGetDto> getSettleInfoSum(PaymentInfoListSetDto paymentInfoListSetDto) {
    return managementSlaveMapper.getSettleInfoSum(paymentInfoListSetDto);
  }

  public List<PaymentInfoListGetDto> getSettleInfoList(PaymentInfoListSetDto paymentInfoListSetDto) {
    return managementSlaveMapper.getSettleInfoList(paymentInfoListSetDto);
  }

  public List<PgPaymentInfoListGetDto> getPgPaymentInfoList(PgPaymentInfoListSetDto pgPaymentInfoListSetDto) {
    return managementSlaveMapper.getPgPaymentInfoList(pgPaymentInfoListSetDto);
  }

  public List<DgvPaymentStoreGetDto> getDgvPaymentStore(DgvPaymentSetDto dgvPaymentSetDto){
    return managementSlaveMapper.getDgvPaymentStore(dgvPaymentSetDto);
  }
  public List<DgvPaymentDailyGetDto> getDgvPaymentDaily(DgvPaymentSetDto dgvPaymentSetDto){
    return managementSlaveMapper.getDgvPaymentDaily(dgvPaymentSetDto);
  }
  public List<DgvPaymentListGetDto> getDgvPaymentList(DgvPaymentSetDto dgvPaymentSetDto){
    return managementSlaveMapper.getDgvPaymentList(dgvPaymentSetDto);
  }
  public List<MhcPaymentStoreGetDto> getMhcPaymentStore(MhcPaymentSetDto mhcPaymentSetDto){
    return managementSlaveMapper.getMhcPaymentStore(mhcPaymentSetDto);
  }
  public List<MhcPaymentDailyGetDto> getMhcPaymentDaily(MhcPaymentSetDto mhcPaymentSetDto){
    return managementSlaveMapper.getMhcPaymentDaily(mhcPaymentSetDto);
  }
  public List<MhcPaymentListGetDto> getMhcPaymentList(MhcPaymentSetDto mhcPaymentSetDto){
    return managementSlaveMapper.getMhcPaymentList(mhcPaymentSetDto);
  }
}
