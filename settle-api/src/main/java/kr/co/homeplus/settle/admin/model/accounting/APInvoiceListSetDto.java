package kr.co.homeplus.settle.admin.model.accounting;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class APInvoiceListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(notes = "전표유형")
    private String slipType;

    @ApiModelProperty(notes = "Invoice번호")
    private String invoiceNum;

    @ApiModelProperty(notes = "인터페이스번호")
    private long rfInvoiceId;
}
