package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.statStorePeriodEKPI.StorePeriodListGetDto;
import kr.co.homeplus.settle.admin.model.statStorePeriodEKPI.StorePeriodListSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface StatStorePeriodSlaveMapper {
    List<StorePeriodListGetDto> getStoreList(StorePeriodListSetDto listParamDto);
}
