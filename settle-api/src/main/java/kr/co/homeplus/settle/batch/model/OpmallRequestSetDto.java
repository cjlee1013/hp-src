package kr.co.homeplus.settle.batch.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OpmallRequestSetDto {
  private String partnerId;
  private String paymentDt;
  private String partnerNo;
  private String bankAccountHolder;
  private String bankCode;
  private String bankAccount;
  private String paymentAmt;
}