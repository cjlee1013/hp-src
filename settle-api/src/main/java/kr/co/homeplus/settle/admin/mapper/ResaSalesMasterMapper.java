package kr.co.homeplus.settle.admin.mapper;

import kr.co.homeplus.settle.admin.model.resaSales.ResaSalesUploadSetDto;
import kr.co.homeplus.settle.core.db.annotation.MasterConnection;

@MasterConnection
public interface ResaSalesMasterMapper {
  int setResaSalesUpload(ResaSalesUploadSetDto resaSalesUploadSetDto);
  int setResaSalesDelete(ResaSalesUploadSetDto resaSalesUploadSetDto);
}
