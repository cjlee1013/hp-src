package kr.co.homeplus.settle.partner.model.taxBill;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PartnerTaxBillListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;
}
