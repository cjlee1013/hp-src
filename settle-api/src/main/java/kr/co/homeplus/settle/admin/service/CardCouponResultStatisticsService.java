package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.settle.admin.mapper.CardCouponResultStatisticsSlaveMapper;
import kr.co.homeplus.settle.admin.mapper.CouponPeriodStatisticsSlaveMapper;
import kr.co.homeplus.settle.admin.model.couponStatistics.CardCouponResultStatisticsDetailGetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CardCouponResultStatisticsGetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CardCouponResultStatisticsSetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CouponPeriodStatisticsDetailGetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CouponPeriodStatisticsGetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CouponPeriodStatisticsSetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CardCouponResultStatisticsService {
    private final CardCouponResultStatisticsSlaveMapper cardCouponResultStatisticsSlaveMapper;

    /**
     * 카드사별쿠폰실적조회 > 메인 리스트 조회
     */
    public List<CardCouponResultStatisticsGetDto> getCardCouponResultStatisticsList(CardCouponResultStatisticsSetDto cardCouponResultStatisticsSetDto) {
        return cardCouponResultStatisticsSlaveMapper.selectCardCouponResultStatisticsList(cardCouponResultStatisticsSetDto);
    }

    /**
     * 카드사별쿠폰실적조회 > 상세 리스트 조회
     */
    public List<CardCouponResultStatisticsDetailGetDto> getCardCouponResultStatisticsDetailList(CardCouponResultStatisticsSetDto cardCouponResultStatisticsSetDto) {
        return cardCouponResultStatisticsSlaveMapper.selectCardCouponResultStatisticsDetailList(cardCouponResultStatisticsSetDto);
    }

}
