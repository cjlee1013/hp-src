package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.settle.admin.mapper.PartnerSettleSlaveMapper;
import kr.co.homeplus.settle.admin.mapper.PartnerSettleMasterMapper;
import kr.co.homeplus.settle.admin.model.partnerSettle.PartnerSettleCommonSetDto;
import kr.co.homeplus.settle.admin.model.partnerSettle.PartnerSettleListGetDto;
import kr.co.homeplus.settle.admin.model.partnerSettle.PartnerSettleListSetDto;
import kr.co.homeplus.settle.admin.model.partnerSettleDetail.PartnerSettleDetailGetDto;
import kr.co.homeplus.settle.admin.model.partnerSettleDetail.PartnerSettleItemGetDto;
import kr.co.homeplus.settle.admin.model.partnerSettleDetail.PartnerSettleItemSetDto;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerSettlePreDtGetDto;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerSettlePreDtSetDto;
import kr.co.homeplus.settle.admin.model.partnerSettle.PartnerSettleStateGetDto;
import kr.co.homeplus.settle.admin.model.partnerSettle.PartnerSettleStateSetDto;
import kr.co.homeplus.settle.admin.model.common.PartnerSettleState;
import kr.co.homeplus.settle.core.exception.handler.ExceptionCode;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class PartnerSettleService {
    private final PartnerSettleSlaveMapper partnerSettleSlaveMapper;
    private final PartnerSettleMasterMapper partnerSettleMasterMapper;

    private final RefitCryptoService refitCryptoService;

    private final String ADD_TYPE_A = "A";
    private final String ADD_TYPE_D = "D";

    /**
     * 정산관리 > 메인 리스트 조회
     */
    public List<PartnerSettleListGetDto> getPartnerSettleList(PartnerSettleListSetDto listParamDto) throws Exception {
        List<PartnerSettleListGetDto> returnDto = partnerSettleSlaveMapper.getPartnerSettleList(listParamDto);
        for (PartnerSettleListGetDto dto : returnDto) {
            dto.setPartnerId(PrivacyMaskingUtils.maskingUserId(dto.getPartnerId()));
        }
        return returnDto;
    }

    /**
     * 정산관리 > 파트너 상세 매출 조회
     */
    public List<PartnerSettleItemGetDto> getPartnerSettleItem(PartnerSettleItemSetDto listParamDto) throws Exception {
        List<PartnerSettleItemGetDto> returnDto = partnerSettleSlaveMapper.getPartnerSettleItem(listParamDto);
        for (PartnerSettleItemGetDto dto : returnDto) {
            dto.setPartnerId(PrivacyMaskingUtils.maskingUserId(dto.getPartnerId()));
        }
        return returnDto;
    }

    /**
     * 정산관리 > 파트너 정산상태 조회
     */
    public PartnerSettleStateGetDto getPartnerSettleState(PartnerSettleCommonSetDto listParamDto) throws Exception {
        PartnerSettleStateGetDto results = partnerSettleMasterMapper.getPartnerSettleState(listParamDto);
        return results;
    }

    /**
     * 정산관리 > 파트너 정산완료
     */
    public ResponseObject<String> setPartnerSettleComplete(List<PartnerSettleCommonSetDto> listParamDto) throws Exception {
        String resultMsg = "", errMsg = "";
        int successCnt = 0, failCnt = 0;
        try {
            for (PartnerSettleCommonSetDto dto : listParamDto) {
                PartnerSettleStateGetDto settleInfo = getPartnerSettleState(dto);
                PartnerSettleStateSetDto settleState = new PartnerSettleStateSetDto();

                if (settleInfo.getSettleState().equals(PartnerSettleState.SETTLE_STATUS_COMPLETE.getCode())) {
                    errMsg = "이미 처리되었습니다.";
                    successCnt++;
                } else if (settleInfo.getSettleState().equals(PartnerSettleState.SETTLE_STATUS_WAIT.getCode())) {
                    settleState.setSettleSrl(dto.getSettleSrl());
                    settleState.setChgId(dto.getChgId());
                    settleState.setSettleState(PartnerSettleState.SETTLE_STATUS_COMPLETE.getCode());
                    partnerSettleMasterMapper.setPartnerSettleState(settleState);
                    partnerSettleMasterMapper.insertPartnerSettleHist(settleState);
                    successCnt++;
                } else {
                    failCnt++;
                }

            }

        }
        catch (Exception e) {
            failCnt++;
            errMsg = ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage();
        }

        if (failCnt > 0)
        {
            resultMsg = PartnerSettleState.SETTLE_STATUS_COMPLETE.getName() + "처리되었습니다. (총: ".concat(Integer.toString(successCnt+failCnt))
                .concat("건, 성공:").concat(Integer.toString(successCnt)).concat("건, 실패:").concat(Integer.toString(failCnt)).concat("건)").concat(errMsg);
        }
        else
        {
            resultMsg = PartnerSettleState.SETTLE_STATUS_COMPLETE.getName() + "처리되었습니다.";
        }
        return ResourceConverter.toResponseObject("0", HttpStatus.OK, "0", resultMsg);
    }

    /**
     * 정산관리 > 파트너 정산완료 (정산대기->정산완료수동)
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseObject<String> setSettleComplete(PartnerSettleCommonSetDto listParamDto) throws Exception {
        String resultMsg = "", returnCode = "0";
        try {
            PartnerSettleStateGetDto settleInfo = getPartnerSettleState(listParamDto);
            PartnerSettleStateSetDto settleState = new PartnerSettleStateSetDto();

            if (settleInfo.getSettleState().equals(PartnerSettleState.SETTLE_STATUS_COMPLETE.getCode())) { //이미 정산완료
                resultMsg = ExceptionCode.ERROR_CODE_2002.getResponseMessage();
            } else if (settleInfo.getSettleState().equals(PartnerSettleState.SETTLE_STATUS_WAIT.getCode())) { //정산대기 체크
                settleState.setSettleSrl(listParamDto.getSettleSrl());
                settleState.setChgId(listParamDto.getChgId());

                settleState.setSettleState(PartnerSettleState.SETTLE_STATUS_COMPLETE.getCode()); //정산완료
                partnerSettleMasterMapper.setPartnerSettleState(settleState);
                partnerSettleMasterMapper.insertPartnerSettleHist(settleState);
                resultMsg = PartnerSettleState.SETTLE_STATUS_COMPLETE.getName() + "처리되었습니다.";
            }
        }
        catch (Exception e) {
            resultMsg = ExceptionCode.ERROR_CODE_2001.getResponseMessage();
            returnCode = ExceptionCode.ERROR_CODE_2001.getResponseCode();
        }

        return ResourceConverter.toResponseObject(Long.toString(listParamDto.getSettleSrl()), HttpStatus.OK, returnCode, resultMsg);
    }

    /**
     * 정산관리 > 정산보류 (정산대기->정산보류수동) +역정산 테이블에 데이터 합산
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseObject<String> setSettlePostpone(PartnerSettleCommonSetDto listParamDto) throws Exception {
        String resultMsg = "", returnCode = "0";
        try {
            PartnerSettleStateGetDto settleInfo = getPartnerSettleState(listParamDto);
            PartnerSettleStateSetDto settleState = new PartnerSettleStateSetDto();

            if (settleInfo.getSettleState().equals(PartnerSettleState.SETTLE_STATUS_POSTPONE_MANUAL.getCode())) { //이미 정산보류
                resultMsg = ExceptionCode.ERROR_CODE_2002.getResponseMessage();
            } else if (settleInfo.getSettleState().equals(PartnerSettleState.SETTLE_STATUS_WAIT.getCode())) { //정산대기 체크
                settleState.setSettleSrl(listParamDto.getSettleSrl());
                settleState.setChgId(listParamDto.getChgId());

                settleState.setSettleState(PartnerSettleState.SETTLE_STATUS_POSTPONE_MANUAL.getCode()); //정산보류 수동으로 변경
                partnerSettleMasterMapper.updatePartnerSettleHold(settleState);
                partnerSettleMasterMapper.insertPartnerSettleHist(settleState);

                //역정산 금액 업데이트 & 히스토리 입력 - 플러스로 합산
                String settleSrl = Long.toString(listParamDto.getSettleSrl());
                partnerSettleMasterMapper.updateMinusBank(settleSrl, ADD_TYPE_A);
                partnerSettleMasterMapper.insertMinusBankHistory(settleSrl, listParamDto.getChgId());

                resultMsg = PartnerSettleState.SETTLE_STATUS_POSTPONE_MANUAL.getName() + "처리되었습니다.";
            } else {
                resultMsg = ExceptionCode.ERROR_CODE_2001.getResponseMessage();
                returnCode = ExceptionCode.ERROR_CODE_2001.getResponseCode();
            }
        }
        catch (Exception e) {
            resultMsg =  ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage();
            returnCode = ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode();;
        }

        return ResourceConverter.toResponseObject(Long.toString(listParamDto.getSettleSrl()), HttpStatus.OK, returnCode, resultMsg);
    }

    /**
     * 정산관리 > 정산보류 해제 (정산보류수동->정산대기)
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseObject<String> setPostponeCancel(PartnerSettleCommonSetDto listParamDto) throws Exception {
        String resultMsg = "", returnCode = "0";
        try {
            PartnerSettleStateGetDto settleInfo = getPartnerSettleState(listParamDto);
            PartnerSettleStateSetDto settleState = new PartnerSettleStateSetDto();

            if (settleInfo.getSettleState().equals(PartnerSettleState.SETTLE_STATUS_WAIT.getCode())) { //이미 정산대기
                resultMsg = ExceptionCode.ERROR_CODE_2002.getResponseMessage();
            } else if (settleInfo.getSettleState().equals(PartnerSettleState.SETTLE_STATUS_POSTPONE_MANUAL.getCode())) { //정산보류수동 체크
                settleState.setSettleSrl(listParamDto.getSettleSrl());
                settleState.setChgId(listParamDto.getChgId());

                settleState.setSettleState(PartnerSettleState.SETTLE_STATUS_WAIT.getCode()); //정산보류 해제
                partnerSettleMasterMapper.setPartnerSettleState(settleState);
                partnerSettleMasterMapper.insertPartnerSettleHist(settleState);

                //역정산 금액 업데이트 & 히스토리 입력 - 마이너스로 합산
                String settleSrl = Long.toString(listParamDto.getSettleSrl());
                partnerSettleMasterMapper.updateMinusBank(settleSrl, ADD_TYPE_D);
                partnerSettleMasterMapper.insertMinusBankHistory(settleSrl, listParamDto.getChgId());

                resultMsg = PartnerSettleState.SETTLE_STATUS_POSTPONE_MANUAL.getName() + "해제 처리되었습니다.";
            } else {
                resultMsg = ExceptionCode.ERROR_CODE_2001.getResponseMessage();
                returnCode = ExceptionCode.ERROR_CODE_2001.getResponseCode();
            }
        }
        catch (Exception e) {
            resultMsg =  ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage();
            returnCode = ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode();;
        }

        return ResourceConverter.toResponseObject(Long.toString(listParamDto.getSettleSrl()), HttpStatus.OK, returnCode, resultMsg);
    }

    /**
     * 정산관리 > 지급예정일 변경
     */
    public ResponseObject<String> setSettlePreDt(PartnerSettlePreDtSetDto listParamDto) throws Exception {
        String resultMsg = "", returnCode = "0";
        try {
            PartnerSettlePreDtGetDto returnDto = partnerSettleMasterMapper.setSettlePreDt(listParamDto);
            resultMsg = returnDto.getReturnMsg();
            returnCode = returnDto.getReturnCode();
        }
        catch (Exception e) {
            resultMsg =  ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage();
            returnCode = ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode();;
        }

        return ResourceConverter.toResponseObject(Long.toString(listParamDto.getSettleSrl()), HttpStatus.OK, returnCode, resultMsg);
    }

    /**
     * 정산관리 > 파트너 정보
     */
    public List<PartnerSettleDetailGetDto> getPartnerSettleDetail(PartnerSettleListSetDto listParamDto) throws Exception {
        List<PartnerSettleDetailGetDto> returnDto = partnerSettleSlaveMapper.getPartnerSettleDetail(listParamDto);

        for (PartnerSettleDetailGetDto dto : returnDto) {
            try {
                dto.setPartnerId(PrivacyMaskingUtils.maskingUserId(dto.getPartnerId()));
                if (dto.getBankAccountHolder() != null && !StringUtils.isBlank(dto.getBankAccountHolder())) {
                    dto.setBankAccountHolder(" / " + PrivacyMaskingUtils.maskingUserName(dto.getBankAccountHolder()));
                }
            } catch (Exception e) {
                dto.setBankAccountHolder("");
            }

            if (dto.getBankAccountNo() != null && !StringUtils.isBlank(dto.getBankAccountNo())) {
                String bankAccountNo  = "";
                try {
                    bankAccountNo = PrivacyMaskingUtils.maskingBankAccount(refitCryptoService.decrypt(dto.getBankAccountNo()));
                } catch (Exception e) {
                    bankAccountNo  = "-";
                }

                dto.setBankAccountNo(bankAccountNo);
                dto.setBankInfo(dto.getBankNm().concat(" / ").concat(bankAccountNo).concat(dto.getBankAccountHolder()));

            } else {
                dto.setBankInfo(dto.getBankNm().concat(dto.getBankAccountHolder()));
            }
        }
        return returnDto;
    }
}
