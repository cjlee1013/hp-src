package kr.co.homeplus.settle.admin.model.partnerSettle;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PartnerSettleStateGetDto {
    @ApiModelProperty(notes = "지급SRL", required = true)
    private long settleSrl;

    @ApiModelProperty(notes = "정산상태")
    private String settleState;

    @ApiModelProperty(notes = "결과 코드")
    private String resultCd;

    @ApiModelProperty(notes = "결과 상세 메시지")
    private String resultMsg;
}
