package kr.co.homeplus.settle.batch.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OpmallType {

    NEW("N", "신규등록")
    ,UPDATE("U", "정보변경")
    ,REQUEST("R", "지급요청")
    ,CANCEL("C","지급취소")
    ,FUNDS("F","잔액확인")
    ,RESULT("T","지급결과")
    ;

    private String code;
    private String name;
}
