package kr.co.homeplus.settle.admin.model.adjust;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdjustListGetDto {
    @RealGridColumnInfo(headText = "조정ID", width = 100)
    @ApiModelProperty(notes = "조정ID")
    private String adjustId;

    @RealGridColumnInfo(headText = "판매업체ID", width = 100)
    @ApiModelProperty(notes = "판매업체ID")
    private String partnerId;

    @RealGridColumnInfo(headText = "업체코드", width = 100)
    @ApiModelProperty(notes = "업체코드")
    private String vendorCd;

    @RealGridColumnInfo(headText = "판매업체명", width = 120)
    @ApiModelProperty(notes = "판매업체명")
    private String partnerNm;

    @ApiModelProperty(value = "상호")
    @RealGridColumnInfo(headText = "상호명", sortable = true, width = 100)
    private String businessNm;

    @RealGridColumnInfo(headText = "상품번호", width = 100)
    @ApiModelProperty(notes = "상품번호")
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 250)
    @ApiModelProperty(notes = "상품명")
    private String itemNm;

    @RealGridColumnInfo(headText = "유형", width = 100)
    @ApiModelProperty(notes = "유형")
    private String adjustType;

    @RealGridColumnInfo(headText = "구분", width = 80)
    @ApiModelProperty(notes = "구분")
    private String gubun;

    @RealGridColumnInfo(headText = "조정금액", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    @ApiModelProperty(notes = "조정금액")
    private String adjustAmt;

    @RealGridColumnInfo(headText = "조정수수료", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    @ApiModelProperty(notes = "조정수수료")
    private String adjustFee;

    @RealGridColumnInfo(headText = "설명", width = 200)
    @ApiModelProperty(notes = "설명")
    private String comment;

    @RealGridColumnInfo(headText = "별도정산", width = 80)
    @ApiModelProperty(notes = "별도정산")
    private String separateYn;

    @RealGridColumnInfo(headText = "등록일", width = 150)
    @ApiModelProperty(notes = "등록일")
    private String regDt;

    @RealGridColumnInfo(headText = "등록자", width = 100)
    @ApiModelProperty(notes = "등록자")
    private String regId;

    @RealGridColumnInfo(headText = "사용여부", width = 100, hidden = true)
    @ApiModelProperty(notes = "사용여부")
    private String useYn;
}
