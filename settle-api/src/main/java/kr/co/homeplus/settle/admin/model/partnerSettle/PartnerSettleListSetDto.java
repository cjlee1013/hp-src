package kr.co.homeplus.settle.admin.model.partnerSettle;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.Data;

@Data
public class PartnerSettleListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(value = "정산상태")
    private String settleState;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "판매업체명")
    private String partnerNm;

    @ApiModelProperty(value = "지급SRL")
    private long settleSrl;

    @ApiModelProperty(value = "정산주기")
    private String settleCycleType;

    @ApiModelProperty(value = "지급방법")
    @RealGridColumnInfo(headText = "지급방법")
    private String payMethodNm;
}
