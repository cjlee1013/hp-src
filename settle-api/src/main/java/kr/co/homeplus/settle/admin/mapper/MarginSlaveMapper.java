package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.margin.MarginDetailListGetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginDetailListSetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginHeadListGetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginPartListGetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginTeamListGetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginListSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface MarginSlaveMapper {
    List<MarginTeamListGetDto> selectMarginTeamList(MarginListSetDto marginListSetDto);
    List<MarginPartListGetDto> selectMarginPartList(MarginListSetDto marginListSetDto);
    List<MarginPartListGetDto> selectMarginPartElecList(MarginListSetDto marginListSetDto);
    List<MarginHeadListGetDto> selectMarginHeadList(MarginListSetDto marginListSetDto);
    List<MarginDetailListGetDto> selectMarginDetailList(MarginDetailListSetDto marginDetailListSetDto);
}
