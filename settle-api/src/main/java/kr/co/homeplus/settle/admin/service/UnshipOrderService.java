package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.settle.admin.mapper.UnshipOrderSlaveMapper;
import kr.co.homeplus.settle.admin.model.minusSettle.MinusSettleListGetDto;
import kr.co.homeplus.settle.admin.model.unshipOrder.TdUnshipOrderListGetDto;
import kr.co.homeplus.settle.admin.model.unshipOrder.TdUnshipOrderListSetDto;
import kr.co.homeplus.settle.admin.model.unshipOrder.UnshipOrderListGetDto;
import kr.co.homeplus.settle.admin.model.unshipOrder.UnshipOrderListSetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UnshipOrderService {
    private final UnshipOrderSlaveMapper unshipOrderSlaveMapper;

    /**
     * 미배송 주문조회 > 메인 리스트 조회
     */
    public List<UnshipOrderListGetDto> getUnshipOrderList(UnshipOrderListSetDto unshipOrderListSetDto) throws Exception {
        List<UnshipOrderListGetDto> unshipOrderListGetDtoList = unshipOrderSlaveMapper.selectUnshipOrderList(unshipOrderListSetDto);

        for (UnshipOrderListGetDto dto : unshipOrderListGetDtoList) {
            dto.setPartnerId(PrivacyMaskingUtils.maskingUserId(dto.getPartnerId()));
        }

        return unshipOrderListGetDtoList;
    }

    /**
     * TD미배송 주문조회 > 메인 리스트 조회
     */
    public List<TdUnshipOrderListGetDto> getTdUnshipOrderList(TdUnshipOrderListSetDto tdUnshipOrderListSetDto) throws Exception {
        return unshipOrderSlaveMapper.selectTdUnshipOrderList(tdUnshipOrderListSetDto);
    }
}
