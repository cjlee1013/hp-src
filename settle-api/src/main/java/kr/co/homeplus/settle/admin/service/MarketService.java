package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.settle.admin.mapper.MarketSlaveMapper;
import kr.co.homeplus.settle.admin.model.market.MarketCompareListGetDto;
import kr.co.homeplus.settle.admin.model.market.MarketCompareSetDto;
import kr.co.homeplus.settle.admin.model.market.MarketCompareSumGetDto;
import kr.co.homeplus.settle.admin.model.market.MarketSettleListGetDto;
import kr.co.homeplus.settle.admin.model.market.MarketSettleListSetDto;
import kr.co.homeplus.settle.admin.model.market.MarketStatisticsSalesGetDto;
import kr.co.homeplus.settle.admin.model.market.MarketStatisticsSalesSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class MarketService {
  private final MarketSlaveMapper marketSlaveMapper;

  public List<MarketSettleListGetDto> getMarketSettleList(
      MarketSettleListSetDto marketSettleListSetDto) {
    return marketSlaveMapper.getMarketSettleList(marketSettleListSetDto);
  }

  public List<MarketCompareSumGetDto> getMarketCompareSum(
      MarketCompareSetDto marketCompareSetDto) {
    return marketSlaveMapper.getMarketCompareSum(marketCompareSetDto);
  }

  public List<MarketCompareListGetDto> getMarketCompareList(
      MarketCompareSetDto marketCompareSetDto) {
    return marketSlaveMapper.getMarketCompareList(marketCompareSetDto);
  }

  public List<MarketStatisticsSalesGetDto> getMarketStatisticsSales(MarketStatisticsSalesSetDto marketStatisticsSalesSetDto){
    return marketSlaveMapper.getMarketStatisticsSales(marketStatisticsSalesSetDto);
  }
}
