package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.advance.AdvanceListGetDto;
import kr.co.homeplus.settle.admin.model.advance.AdvanceListSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface AdvanceSlaveMapper {
  List<AdvanceListGetDto> getAdvanceUnshipList(AdvanceListSetDto advanceListSetDto);
  List<AdvanceListGetDto> getAdvanceSettleList(AdvanceListSetDto advanceListSetDto);
  List<AdvanceListGetDto> getAdvanceZeroList(AdvanceListSetDto advanceListSetDto);
}
