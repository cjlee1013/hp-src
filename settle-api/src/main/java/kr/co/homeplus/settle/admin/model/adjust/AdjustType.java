package kr.co.homeplus.settle.admin.model.adjust;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum AdjustType {
    //정산조정 타입
    SETTLE_ADJUST_TYPE_01("1", "업체 보상비", "2", "1"),
    SETTLE_ADJUST_TYPE_02("2", "업체 패널티", "2", "2"),
    SETTLE_ADJUST_TYPE_03("3", "광고 수수료", "1", "1"),
    SETTLE_ADJUST_TYPE_04("4", "판매 수수료(환급)", "1", "1"), //가산
    SETTLE_ADJUST_TYPE_05("5", "판매 수수료(청구)", "1", "2")  //차감

    ;

    private final String code;
    private final String name;
    private final String taxType;
    private final String gubun;
}
