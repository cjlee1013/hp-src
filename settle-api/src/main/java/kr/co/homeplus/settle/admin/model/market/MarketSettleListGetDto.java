package kr.co.homeplus.settle.admin.model.market;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제일마감")
public class MarketSettleListGetDto {
  @ApiModelProperty(notes = "결제일")
  private String paymentCompleteDt;

  @ApiModelProperty(notes = "매출일")
  private String basicDt;

  @ApiModelProperty(notes = "입금일")
  private String payScheduleDt;

  @ApiModelProperty(notes = "제휴사")
  private String siteType;

  @ApiModelProperty(notes = "주문번호")
  private String marketOrderNo;

  @ApiModelProperty(notes = "상품주문번호")
  private String marketOrderItemNo;

  @ApiModelProperty(notes = "정산구분")
  private String orderType;

  @ApiModelProperty(notes = "정산상태")
  private String gubun;

  @ApiModelProperty(notes = "상품번호")
  private String itemNo;

  @ApiModelProperty(notes = "정산대상금액")
  private String settleAmt;

  @ApiModelProperty(notes = "매출")
  private String completeAmt;

  @ApiModelProperty(notes = "결제금액")
  private String orderPrice;

  @ApiModelProperty(notes = "자사할인")
  private String marketDiscountHomeAmt;

  @ApiModelProperty(notes = "제휴사할인")
  private String marketDiscountMarketAmt;

  @ApiModelProperty(notes = "수수료합계")
  private String commissionSumAmt;

  @ApiModelProperty(notes = "판매수수료")
  private String saleAgencyFee;

  @ApiModelProperty(notes = "결제수수료")
  private String commissionAmt;

  @ApiModelProperty(notes = "기타수수료")
  private String etcAmt;

  @ApiModelProperty(notes = "공제금액")
  private String deductAmt;
}
