package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.couponStatistics.CouponPeriodStatisticsDetailGetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CouponPeriodStatisticsGetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CouponPeriodStatisticsSetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipCreateSetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipListGetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipListSetDto;
import kr.co.homeplus.settle.admin.service.CouponPeriodStatisticsService;
import kr.co.homeplus.settle.admin.service.UnshipSlipService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/statistics")
@Api(tags = "BackOffice > 기간별 쿠폰통계")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class CouponPeriodStatisticsController {
    private final CouponPeriodStatisticsService couponPeriodStatisticsService;

    @ApiOperation(value = "기간별 쿠폰통계 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getCouponPeriodStatisticsList")
    public ResponseObject<List<CouponPeriodStatisticsGetDto>> getCouponPeriodStatisticsList(@RequestBody CouponPeriodStatisticsSetDto couponPeriodStatisticsSetDto) {
        return ResourceConverter.toResponseObject(couponPeriodStatisticsService.getCouponPeriodStatisticsList(couponPeriodStatisticsSetDto));
    }

    @ApiOperation(value = "기간별 쿠폰통계 > 상세 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getCouponPeriodStatisticsDetailList")
    public ResponseObject<List<CouponPeriodStatisticsDetailGetDto>> getCouponPeriodStatisticsDetailList(@RequestBody CouponPeriodStatisticsSetDto couponPeriodStatisticsSetDto) {
        return ResourceConverter.toResponseObject(couponPeriodStatisticsService.getCouponPeriodStatisticsDetailList(couponPeriodStatisticsSetDto));
    }

}