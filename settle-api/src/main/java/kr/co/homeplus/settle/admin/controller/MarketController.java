package kr.co.homeplus.settle.admin.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.market.MarketCompareListGetDto;
import kr.co.homeplus.settle.admin.model.market.MarketCompareSetDto;
import kr.co.homeplus.settle.admin.model.market.MarketCompareSumGetDto;
import kr.co.homeplus.settle.admin.model.market.MarketSettleListGetDto;
import kr.co.homeplus.settle.admin.model.market.MarketSettleListSetDto;
import kr.co.homeplus.settle.admin.model.market.MarketStatisticsSalesGetDto;
import kr.co.homeplus.settle.admin.model.market.MarketStatisticsSalesSetDto;
import kr.co.homeplus.settle.admin.service.MarketService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/market")
@Api(tags = "BackOffice > 정산관리 > 제휴정산관리")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class MarketController {

  private final MarketService marketService;

  @ApiOperation(value = "제휴 주문조회", response = ResponseObject.class, notes = "제휴 주문조회")
  @RequestMapping(value = "/getMarketSettleList", method = {RequestMethod.POST})
  public ResponseObject<List<MarketSettleListGetDto>> getMarketSettleList (@RequestBody @Valid
      MarketSettleListSetDto marketSettleListSetDto) {
    List<MarketSettleListGetDto> marketSettleListGetDto = marketService.getMarketSettleList(marketSettleListSetDto);
    return ResourceConverter.toResponseObject(marketSettleListGetDto);
  }

  @ApiOperation(value = "제휴 정산대사 일별상세내역", response = ResponseObject.class, notes = "제휴 정산대사 일별상세내역")
  @RequestMapping(value = "/getMarketCompareSum", method = {RequestMethod.POST})
  public ResponseObject<List<MarketCompareSumGetDto>> getMarketCompareSum (@RequestBody @Valid
      MarketCompareSetDto marketCompareSetDto) {
    List<MarketCompareSumGetDto> marketCompareSumGetDto = marketService.getMarketCompareSum(marketCompareSetDto);
    return ResourceConverter.toResponseObject(marketCompareSumGetDto);
  }

  @ApiOperation(value = "제휴 정산대사 차이내역", response = ResponseObject.class, notes = "제휴 정산대사 차이내역")
  @RequestMapping(value = "/getMarketCompareList", method = {RequestMethod.POST})
  public ResponseObject<List<MarketCompareListGetDto>> getMarketCompareList (@RequestBody @Valid
      MarketCompareSetDto marketCompareSetDto) {
    List<MarketCompareListGetDto> marketCompareListGetDto = marketService.getMarketCompareList(marketCompareSetDto);
    return ResourceConverter.toResponseObject(marketCompareListGetDto);
  }

  @ApiOperation(value = "마켓별 판매현황", response = ResponseObject.class, notes = "마켓별 판매현황")
  @RequestMapping(value = "/getMarketStatisticsSales", method = {RequestMethod.POST})
  public ResponseObject<List<MarketStatisticsSalesGetDto>> getMarketStatisticsSales (@RequestBody @Valid
      MarketStatisticsSalesSetDto marketStatisticsSalesSetDto) {
    List<MarketStatisticsSalesGetDto> marketStatisticsSalesGetDto = marketService.getMarketStatisticsSales(marketStatisticsSalesSetDto);
    return ResourceConverter.toResponseObject(marketStatisticsSalesGetDto);
  }
}
