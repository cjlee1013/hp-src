package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.couponStatistics.CouponPeriodStatisticsDetailGetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CouponPeriodStatisticsGetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CouponPeriodStatisticsSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface CouponPeriodStatisticsSlaveMapper {
    List<CouponPeriodStatisticsGetDto> selectCouponPeriodStatisticsList(CouponPeriodStatisticsSetDto couponPeriodStatisticsSetDto);
    List<CouponPeriodStatisticsDetailGetDto> selectCouponPeriodStatisticsDetailList(CouponPeriodStatisticsSetDto couponPeriodStatisticsSetDto);
}
