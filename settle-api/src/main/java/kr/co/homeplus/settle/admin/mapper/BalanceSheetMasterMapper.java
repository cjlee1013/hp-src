package kr.co.homeplus.settle.admin.mapper;

import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetInterfaceGetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetInterfaceSetDto;
import kr.co.homeplus.settle.core.db.annotation.MasterConnection;

@MasterConnection
public interface BalanceSheetMasterMapper {
  BalanceSheetInterfaceGetDto setBalanceSheetInterface(
      BalanceSheetInterfaceSetDto balanceSheetInterfaceSetDto);
}
