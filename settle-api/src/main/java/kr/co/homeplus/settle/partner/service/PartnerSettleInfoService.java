package kr.co.homeplus.settle.partner.service;

import java.util.List;
import java.util.regex.Pattern;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.settle.admin.service.AccountingService;
import kr.co.homeplus.settle.partner.mapper.PartnerDailySettleSlaveMapper;
import kr.co.homeplus.settle.partner.mapper.PartnerFinalSettleSlaveMapper;
import kr.co.homeplus.settle.partner.mapper.PartnerSalesVATSlaveMapper;
import kr.co.homeplus.settle.partner.mapper.PartnerTaxBillSlaveMapper;
import kr.co.homeplus.settle.partner.model.finalSettle.PartnerFinalSettleGetDto;
import kr.co.homeplus.settle.partner.model.finalSettle.PartnerFinalSettleSetDto;
import kr.co.homeplus.settle.partner.model.finalSettle.PartnerSettleAmtSumGetDto;
import kr.co.homeplus.settle.partner.model.salesVAT.PartnerSalesVATListGetDto;
import kr.co.homeplus.settle.partner.model.salesVAT.PartnerSalesVATListSetDto;
import kr.co.homeplus.settle.partner.model.salesVAT.PartnerSalesVATOrderGetDto;
import kr.co.homeplus.settle.partner.model.settleInfo.SettleInfoGetDto;
import kr.co.homeplus.settle.partner.model.settleInfo.DailySettleGetDto;
import kr.co.homeplus.settle.partner.model.settleInfo.DailySettleSetDto;
import kr.co.homeplus.settle.partner.model.taxBill.PartnerTaxBillListGetDto;
import kr.co.homeplus.settle.partner.model.taxBill.PartnerTaxBillListSetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@RequiredArgsConstructor
public class PartnerSettleInfoService {
    private final PartnerDailySettleSlaveMapper dailySettleSlaveMapper;
    private final PartnerFinalSettleSlaveMapper partnerSettleSlaveMapper;
    private final PartnerSalesVATSlaveMapper MonthlySalesVATSlaveMapper;
    private final PartnerTaxBillSlaveMapper TaxBillInvoiceSlaveMapper;
    private final RefitCryptoService cryptoService;

    /**
     * 정산관리 > 매출관리 > 메인 리스트 조회
     */
    public List<DailySettleGetDto> getDailySettle(DailySettleSetDto listParamDto) throws Exception {
        return dailySettleSlaveMapper.getDailySettle(listParamDto);
    }

    /**
     * 정산관리 > 매출관리 > 파트너 상세 매출 조회
     */
    public List<SettleInfoGetDto> getSettleInfo(DailySettleSetDto listParamDto) throws Exception {
        List<SettleInfoGetDto> returnDto = dailySettleSlaveMapper.getSettleInfo(listParamDto);
        for (SettleInfoGetDto dto : returnDto) {
            dto.setUserNm(PrivacyMaskingUtils.maskingUserName(dto.getUserNm()));
        }

        return returnDto;
    }

    /**
     * 정산관리 > 메인 리스트 조회
     */
    public List<PartnerFinalSettleGetDto> getFinalSettle(PartnerFinalSettleSetDto listParamDto) throws Exception {
        return partnerSettleSlaveMapper.getFinalSettle(listParamDto);
    }

    /**
     * 세금계산서 관리 > 부가세신고내역 조회 > 메인 리스트 조회
     */
    public List<PartnerSalesVATListGetDto> getSalesVATList(PartnerSalesVATListSetDto listParamDto) throws Exception {
        return MonthlySalesVATSlaveMapper.getSalesVATList(listParamDto);
    }

    /**
     * 부가세신고내역 > 주문 상세내역
     */
    public List<PartnerSalesVATOrderGetDto> getSalesVATOrder(PartnerSalesVATListSetDto listParamDto) throws Exception {
        List<PartnerSalesVATOrderGetDto> returnDto = MonthlySalesVATSlaveMapper.getSalesVATOrder(listParamDto);;
        for (PartnerSalesVATOrderGetDto dto : returnDto) {
            dto.setUserNm(PrivacyMaskingUtils.maskingUserName(dto.getUserNm()));
        }

        return returnDto;
    }

    /**
     * 세금계산서 관리 > 세금계산서 조회 > 메인 리스트 조회
     */
    public List<PartnerTaxBillListGetDto> getTaxBillList(PartnerTaxBillListSetDto listParamDto) throws Exception {
        List<PartnerTaxBillListGetDto> returnDto = TaxBillInvoiceSlaveMapper.getTaxBillList(listParamDto);

        for (PartnerTaxBillListGetDto dto : returnDto) {
            Pattern emailPattern = Pattern.compile(AccountingService.EMAIL_PATTERN); //이메일 패턴 체크

            if (!emailPattern.matcher(dto.getMngEmail()).matches()) {
                dto.setMngEmail(cryptoService.decrypt(dto.getMngEmail()));
            }
        }

        return returnDto;
    }

    /**
     * 파트너메인 > 정산예정금액 조회
     */
    public PartnerSettleAmtSumGetDto getPartnerSettleAmt(@RequestParam(name ="partnerId") String partnerId) throws Exception {
        return partnerSettleSlaveMapper.getPartnerSettleAmt(partnerId);
    }
}
