package kr.co.homeplus.settle.partner.model.salesVAT;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PartnerSalesVATListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "페이지번호")
    private long pageNo;

    @ApiModelProperty(value = "페이징크기")
    private long pageSize;
}
