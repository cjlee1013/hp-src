package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleListGetDto;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleListSetDto;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleInfoGetDto;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleInfoSetDto;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleSumGetDto;
import kr.co.homeplus.settle.admin.service.DailySettleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/dailySettle")
@Api(tags = "BackOffice > 매출관리")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class DailySettleController {
    private final DailySettleService dailySettleService;

    @ApiOperation(value = "정산관리 > 매출관리 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getDailySettleList")
    public ResponseObject<List<DailySettleListGetDto>> getDailySettleList(@RequestBody DailySettleListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(dailySettleService.getDailySettleList(listParamDto));
    }

    @ApiOperation(value = "정산관리 > 매출관리 > 파트너 상세 매출 조회", response = ResponseObject.class)
    @PostMapping("/getDailySettleInfo")
    public ResponseObject<List<DailySettleInfoGetDto>> getDailySettleInfo(@RequestBody DailySettleInfoSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(dailySettleService.getDailySettleInfo(listParamDto));
    }

    @ApiOperation(value = "정산관리 > 매출관리 > 점별 매출금액 합계", response = ResponseObject.class)
    @PostMapping("/getDailySettleSum")
    public ResponseObject<List<DailySettleSumGetDto>> getDailySettleSum(@RequestBody DailySettleListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(dailySettleService.getDailySettleSum(listParamDto));
    }
}