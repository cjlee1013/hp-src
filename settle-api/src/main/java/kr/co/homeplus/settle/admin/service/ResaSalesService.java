package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.settle.admin.mapper.ResaSalesMasterMapper;
import kr.co.homeplus.settle.admin.mapper.ResaSalesSlaveMapper;
import kr.co.homeplus.settle.admin.model.resaSales.ResaSalesListGetDto;
import kr.co.homeplus.settle.admin.model.resaSales.ResaSalesListSetDto;
import kr.co.homeplus.settle.admin.model.resaSales.ResaSalesUploadSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
@Configuration
public class ResaSalesService {

  private final ResaSalesMasterMapper resaSalesMasterMapper;
  private final ResaSalesSlaveMapper resaSalesSlaveMapper;

  public List<ResaSalesListGetDto> getResaSalesList(ResaSalesListSetDto resaSalesListSetDto) {
    if ("D".equals(resaSalesListSetDto.getDateType())) {
      return resaSalesSlaveMapper.getResaSalesListDay(resaSalesListSetDto);
    } else {
      return resaSalesSlaveMapper.getResaSalesListMonth(resaSalesListSetDto);
    }
  }

  public int getResaSalesExist(String basicDt){
    return resaSalesSlaveMapper.getResaSalesExist(basicDt);
  }

  public int setResaSalesUpload(List<ResaSalesUploadSetDto> resaSalesUploadSetDtoList){
    int ret = 0;
    if (resaSalesUploadSetDtoList.size() > 0) {
      resaSalesMasterMapper.setResaSalesDelete(resaSalesUploadSetDtoList.get(0));
      for (ResaSalesUploadSetDto resaSalesUploadSetDto : resaSalesUploadSetDtoList) {
        if (resaSalesUploadSetDto.getBusiness_date() != null) {
          ret += resaSalesMasterMapper.setResaSalesUpload(resaSalesUploadSetDto);
        }
      }
    }
    return ret;
  }
}
