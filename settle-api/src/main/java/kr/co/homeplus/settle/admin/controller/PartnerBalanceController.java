package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.partnerBalance.PartnerBalanceListGetDto;
import kr.co.homeplus.settle.admin.model.partnerBalance.PartnerBalanceListSetDto;
import kr.co.homeplus.settle.admin.service.PartnerBalanceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/partnerBalance")
@Api(tags = "BackOffice > 파트너 잔액 조회")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class PartnerBalanceController {
    private final PartnerBalanceService partnerBalanceService;

    @ApiOperation(value = "파트너 잔액 조회 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getPartnerBalanceList")
    public ResponseObject<List<PartnerBalanceListGetDto>> getPartnerBalanceList(@RequestBody PartnerBalanceListSetDto partnerBalanceListSetDto) throws Exception {
        return ResourceConverter.toResponseObject(partnerBalanceService.getPartnerBalanceList(partnerBalanceListSetDto));
    }
}