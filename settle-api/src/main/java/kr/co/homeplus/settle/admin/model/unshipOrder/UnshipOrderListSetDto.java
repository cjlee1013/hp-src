package kr.co.homeplus.settle.admin.model.unshipOrder;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UnshipOrderListSetDto {
    @ApiModelProperty(value = "검색기간(년)")
    private String schYear;

    @ApiModelProperty(value = "검색기간(월)")
    private String schMonth;

    @ApiModelProperty(value = "상품구분")
    private String schMallType;

    @ApiModelProperty(value = "점포유형")
    private String schStoreType;

    @ApiModelProperty(value = "검색조건 타입")
    private String schKeywordType;

    @ApiModelProperty(value = "검색어")
    private String schKeyword;

    @ApiModelProperty(notes = "배송상태")
    private String schShipStatus;
}
