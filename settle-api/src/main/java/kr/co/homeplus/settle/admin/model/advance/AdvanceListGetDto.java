package kr.co.homeplus.settle.admin.model.advance;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "선수금조회 목록")
public class AdvanceListGetDto {
  @ApiModelProperty(notes = "기준월")
  private String basicDt;

  @ApiModelProperty(notes = "결제일")
  private String paymentBasicDt;

  @ApiModelProperty(notes = "입금일")
  private String payScheduleDt;

  @ApiModelProperty(notes = "매출일")
  private String settleDt;

  @ApiModelProperty(notes = "상품구분")
  private String mallType;

  @ApiModelProperty(notes = "주문번호")
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "배송번호")
  private String bundleNo;

  @ApiModelProperty(notes = "클레임번호")
  private String claimNo;

  @ApiModelProperty(notes = "상품번호")
  private String itemNo;

  @ApiModelProperty(notes = "구분")
  private String gubun;

  @ApiModelProperty(notes = "주문금액")
  private String completeAmt;

  @ApiModelProperty(notes = "자사할인")
  private String homeAmt;

  @ApiModelProperty(notes = "업체할인")
  private String sellerAmt;

  @ApiModelProperty(notes = "판매수익")
  private String profitAmt;

  @ApiModelProperty(notes = "PG예치금")
  private String pgBalance;

  @ApiModelProperty(notes = "선수금")
  private String orderPrice;

  @ApiModelProperty(notes = "PG결제금액")
  private String pgAmt;

  @ApiModelProperty(notes = "이외수단")
  private String etcAmt;

  @ApiModelProperty(notes = "이월 선수금")
  private String receivedLast;

  @ApiModelProperty(notes = "당월 결제금액")
  private String monthOrder;

  @ApiModelProperty(notes = "당월 PG결제금액")
  private String monthPg;

  @ApiModelProperty(notes = "당월 이외수단")
  private String monthEtc;

  @ApiModelProperty(notes = "당월 PG입금액")
  private String monthPgPay;

  @ApiModelProperty(notes = "구매확정")
  private String decisionAmt;

  @ApiModelProperty(notes = "선수금 잔액")
  private String receivedLeft;

  @ApiModelProperty(notes = "DS매출")
  private String dsSales;

  @ApiModelProperty(notes = "세금계산서")
  private String taxInvoice;

  @ApiModelProperty(notes = "선수수익잔액")
  private String revenueLeft;

}
