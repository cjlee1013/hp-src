package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.unshipOrder.TdUnshipOrderListGetDto;
import kr.co.homeplus.settle.admin.model.unshipOrder.TdUnshipOrderListSetDto;
import kr.co.homeplus.settle.admin.model.unshipOrder.UnshipOrderListGetDto;
import kr.co.homeplus.settle.admin.model.unshipOrder.UnshipOrderListSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface UnshipOrderSlaveMapper {
    List<UnshipOrderListGetDto> selectUnshipOrderList(UnshipOrderListSetDto unshipOrderListSetDto);
    List<TdUnshipOrderListGetDto> selectTdUnshipOrderList(TdUnshipOrderListSetDto tdUnshipOrderListSetDto);
}
