package kr.co.homeplus.settle.admin.model.affiliate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "전표조회 목록")
public class AffiliateSalesSetDto {
  @ApiModelProperty(notes = "시작일")
  private String startDt;

  @ApiModelProperty(notes = "종료일")
  private String endDt;

  @ApiModelProperty(notes = "사이트구분")
  private String siteType;

  @ApiModelProperty(notes = "집계기준")
  private String gubun;

  @ApiModelProperty(notes = "제휴채널ID")
  private String affiliateCd;

  @ApiModelProperty(notes = "제휴업체ID")
  private String partnerId;

  @ApiModelProperty(notes = "제휴채널명")
  private String channelNm;

  @ApiModelProperty(notes = "제휴업체명")
  private String affiliateNm;
}
