package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.settle.admin.mapper.DailyPaymentSlaveMapper;
import kr.co.homeplus.settle.admin.model.dailyPayment.DailyPaymentGetDto;
import kr.co.homeplus.settle.admin.model.dailyPayment.DailyPaymentSetDto;
import kr.co.homeplus.settle.admin.model.dailyPayment.DailyPaymentSumGetDto;
import kr.co.homeplus.settle.admin.model.dailyPayment.PaymentInfoGetDto;
import kr.co.homeplus.settle.admin.model.dailyPayment.PaymentInfoSetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DailyPaymentService {
    private final DailyPaymentSlaveMapper dailyPaymentSlaveMapper;

    /**
     * 정산관리 > 판매관리 > 메인 리스트 조회
     */
    public List<DailyPaymentGetDto> getDailyPaymentList(DailyPaymentSetDto listParamDto) throws Exception {
        List<DailyPaymentGetDto> returnDto = dailyPaymentSlaveMapper.getDailyPaymentList(listParamDto);
        for (DailyPaymentGetDto dto : returnDto) {
            dto.setPartnerId(PrivacyMaskingUtils.maskingUserId(dto.getPartnerId()));
        }
        return returnDto;
    }

    /**
     * 정산관리 > 판매관리 > 상세 매출 조회
     */
    public List<PaymentInfoGetDto> getDailyPaymentInfo(PaymentInfoSetDto listParamDto) throws Exception {
        List<PaymentInfoGetDto> returnDto = dailyPaymentSlaveMapper.getDailyPaymentInfo(listParamDto);
        for (PaymentInfoGetDto dto : returnDto) {
            dto.setUserNm(PrivacyMaskingUtils.maskingUserName(dto.getUserNm()));
        }

        return returnDto;
    }

    /**
     * 정산관리 > 판매관리 > 점별 판매금액 합계
     */
    public List<DailyPaymentSumGetDto> getDailyPaymentSum(DailyPaymentSetDto listParamDto) throws Exception {
        List<DailyPaymentSumGetDto> returnDto;
        if ((listParamDto.getItemNo() != null && listParamDto.getItemNo() != "") || (listParamDto.getItemNm() != null && listParamDto.getItemNm() != "")) {
            returnDto = dailyPaymentSlaveMapper.getDailyPaymentSum(listParamDto);
        } else {
            returnDto = dailyPaymentSlaveMapper.getDailyPaymentStore(listParamDto); //SETTLE-437 점별합계테이블 추가
        }
        return returnDto;
    }
}
