package kr.co.homeplus.settle.batch.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class OpmallResultSetDto {
  private String partnerId;
  private String startDt;
  private String endDt;
}