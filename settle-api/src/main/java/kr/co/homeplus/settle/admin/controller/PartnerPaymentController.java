package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerPaymentAccountSetDto;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerPaymentListGetDto;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerPaymentListSetDto;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerPaymentStateSetDto;
import kr.co.homeplus.settle.admin.service.PartnerPaymentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/partnerPayment")
@Api(tags = "BackOffice > 지급관리")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class PartnerPaymentController {
    private final PartnerPaymentService partnerPaymentService;

    @ApiOperation(value = "지급관리 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getPartnerPaymentList")
    public ResponseObject<List<PartnerPaymentListGetDto>> getPartnerPaymentList(@RequestBody PartnerPaymentListSetDto partnerPaymentListSetDto) throws Exception {
        return ResourceConverter.toResponseObject(partnerPaymentService.getPartnerPaymentList(partnerPaymentListSetDto));
    }

    @ApiOperation(value = "지급관리 > 지급상태 수정", response = ResponseObject.class)
    @PostMapping("/setPartnerPaymentState")
    public ResponseObject<Integer> setPartnerPaymentState(@RequestBody PartnerPaymentStateSetDto partnerPaymentStateSetDto) throws Exception {
        return ResourceConverter.toResponseObject(partnerPaymentService.setPartnerPaymentState(partnerPaymentStateSetDto));
    }

    @ApiOperation(value = "지급관리 > 지급계좌 변경", response = ResponseObject.class)
    @PostMapping("/setPartnerPaymentAccount")
    public ResponseObject<Integer> setPartnerPaymentAccount(@RequestBody PartnerPaymentAccountSetDto partnerPaymentAccountSetDto) throws Exception {
        return ResourceConverter.toResponseObject(partnerPaymentService.setPartnerPaymentAccount(partnerPaymentAccountSetDto));
    }
}