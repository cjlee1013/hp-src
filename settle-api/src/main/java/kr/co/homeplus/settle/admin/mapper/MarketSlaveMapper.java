package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.market.MarketCompareListGetDto;
import kr.co.homeplus.settle.admin.model.market.MarketCompareSetDto;
import kr.co.homeplus.settle.admin.model.market.MarketCompareSumGetDto;
import kr.co.homeplus.settle.admin.model.market.MarketSettleListGetDto;
import kr.co.homeplus.settle.admin.model.market.MarketSettleListSetDto;
import kr.co.homeplus.settle.admin.model.market.MarketStatisticsSalesGetDto;
import kr.co.homeplus.settle.admin.model.market.MarketStatisticsSalesSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface MarketSlaveMapper {
  List<MarketSettleListGetDto> getMarketSettleList(MarketSettleListSetDto marketSettleListSetDto);
  List<MarketCompareSumGetDto> getMarketCompareSum(MarketCompareSetDto marketCompareSetDto);
  List<MarketCompareListGetDto> getMarketCompareList(MarketCompareSetDto marketCompareSetDto);
  List<MarketStatisticsSalesGetDto> getMarketStatisticsSales(MarketStatisticsSalesSetDto marketStatisticsSalesSetDto);
}
