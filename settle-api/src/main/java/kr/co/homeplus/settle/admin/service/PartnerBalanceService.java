package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.settle.admin.mapper.PartnerBalanceSlaveMapper;
import kr.co.homeplus.settle.admin.model.partnerBalance.PartnerBalanceListGetDto;
import kr.co.homeplus.settle.admin.model.partnerBalance.PartnerBalanceListSetDto;
import kr.co.homeplus.settle.admin.model.unshipOrder.UnshipOrderListGetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PartnerBalanceService {
    private final PartnerBalanceSlaveMapper partnerBalanceSlaveMapper;

    /**
     * 파트너 잔액 조회 > 메인 리스트 조회
     */
    public List<PartnerBalanceListGetDto> getPartnerBalanceList(PartnerBalanceListSetDto partnerBalanceListSetDto) throws Exception {
        List<PartnerBalanceListGetDto> partnerBalanceListGetDtoList =  partnerBalanceSlaveMapper.selectPartnerBalanceList(partnerBalanceListSetDto);

        for (PartnerBalanceListGetDto dto : partnerBalanceListGetDtoList) {
            dto.setPartnerId(PrivacyMaskingUtils.maskingUserId(dto.getPartnerId()));
        }

        return partnerBalanceListGetDtoList;
    }
}
