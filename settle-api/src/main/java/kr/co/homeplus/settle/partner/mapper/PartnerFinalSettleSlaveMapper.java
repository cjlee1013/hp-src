package kr.co.homeplus.settle.partner.mapper;

import java.util.List;
import kr.co.homeplus.settle.partner.model.finalSettle.PartnerFinalSettleGetDto;
import kr.co.homeplus.settle.partner.model.finalSettle.PartnerFinalSettleSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;
import kr.co.homeplus.settle.partner.model.finalSettle.PartnerSettleAmtSumGetDto;
import org.springframework.web.bind.annotation.RequestParam;

@SlaveConnection
public interface PartnerFinalSettleSlaveMapper {
    List<PartnerFinalSettleGetDto> getFinalSettle(PartnerFinalSettleSetDto listParamDto);

    PartnerSettleAmtSumGetDto getPartnerSettleAmt(@RequestParam(name ="partnerId") String partnerId);
}
