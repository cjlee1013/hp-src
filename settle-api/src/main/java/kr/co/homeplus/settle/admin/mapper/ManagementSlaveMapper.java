package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.management.DgvPaymentDailyGetDto;
import kr.co.homeplus.settle.admin.model.management.DgvPaymentListGetDto;
import kr.co.homeplus.settle.admin.model.management.DgvPaymentSetDto;
import kr.co.homeplus.settle.admin.model.management.DgvPaymentStoreGetDto;
import kr.co.homeplus.settle.admin.model.management.MhcPaymentDailyGetDto;
import kr.co.homeplus.settle.admin.model.management.MhcPaymentListGetDto;
import kr.co.homeplus.settle.admin.model.management.MhcPaymentSetDto;
import kr.co.homeplus.settle.admin.model.management.MhcPaymentStoreGetDto;
import kr.co.homeplus.settle.admin.model.management.PaymentInfoListGetDto;
import kr.co.homeplus.settle.admin.model.management.PaymentInfoListSetDto;
import kr.co.homeplus.settle.admin.model.management.PaymentInfoSumGetDto;
import kr.co.homeplus.settle.admin.model.management.PgPaymentInfoListGetDto;
import kr.co.homeplus.settle.admin.model.management.PgPaymentInfoListSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface ManagementSlaveMapper {
  List<PaymentInfoSumGetDto> getPaymentInfoSum(PaymentInfoListSetDto paymentInfoListSetDto);
  List<PaymentInfoListGetDto> getPaymentInfoList(PaymentInfoListSetDto paymentInfoListSetDto);
  List<PaymentInfoSumGetDto> getSettleInfoSum(PaymentInfoListSetDto paymentInfoListSetDto);
  List<PaymentInfoListGetDto> getSettleInfoList(PaymentInfoListSetDto paymentInfoListSetDto);
  List<PgPaymentInfoListGetDto> getPgPaymentInfoList(PgPaymentInfoListSetDto pgPaymentInfoListSetDto);
  List<DgvPaymentStoreGetDto> getDgvPaymentStore(DgvPaymentSetDto dgvPaymentSetDto);
  List<DgvPaymentDailyGetDto> getDgvPaymentDaily(DgvPaymentSetDto dgvPaymentSetDto);
  List<DgvPaymentListGetDto> getDgvPaymentList(DgvPaymentSetDto dgvPaymentSetDto);
  List<MhcPaymentStoreGetDto> getMhcPaymentStore(MhcPaymentSetDto mhcPaymentSetDto);
  List<MhcPaymentDailyGetDto> getMhcPaymentDaily(MhcPaymentSetDto mhcPaymentSetDto);
  List<MhcPaymentListGetDto> getMhcPaymentList(MhcPaymentSetDto mhcPaymentSetDto);
}
