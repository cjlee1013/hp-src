package kr.co.homeplus.settle.admin.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import kr.co.homeplus.settle.admin.mapper.MarginSlaveMapper;
import kr.co.homeplus.settle.admin.model.margin.MarginDetailListGetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginDetailListSetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginHeadListGetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginListSetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginPartListGetDto;
import kr.co.homeplus.settle.admin.model.margin.MarginTeamListGetDto;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MarginService {
    private final MarginSlaveMapper marginSlaveMapper;

    /**
     * DC/DS 마진조회 > 팀별 리스트 조회
     */
    public List<MarginTeamListGetDto> getMarginTeamList(MarginListSetDto marginListSetDto) throws Exception {
        List<MarginTeamListGetDto> marginTeamList = marginSlaveMapper.selectMarginTeamList(marginListSetDto);

        BigDecimal completeInRateTotal = BigDecimal.valueOf(0.0);
        BigDecimal completeExRateTotal = BigDecimal.valueOf(0.0);
        BigDecimal marginRateTotal = BigDecimal.valueOf(0.0);

        BigDecimal completeInMax = BigDecimal.valueOf(0);
        BigDecimal completeExMax = BigDecimal.valueOf(0);
        BigDecimal marginMax = BigDecimal.valueOf(0);

        String nameInMax = "";
        String nameExMax = "";
        String nameMarginMax = "";

        for(MarginTeamListGetDto dto : marginTeamList) {
            BigDecimal completeInRate = BigDecimal.valueOf(Double.parseDouble(dto.getCompleteInAmtRate().replace("%","")));
            BigDecimal completeExRate = BigDecimal.valueOf(Double.parseDouble(dto.getCompleteExAmtRate().replace("%","")));
            BigDecimal marginRate = BigDecimal.valueOf(Double.parseDouble(dto.getMarginAmtRate().replace("%","")));

            completeInRateTotal = completeInRateTotal.add(completeInRate);
            completeExRateTotal = completeExRateTotal.add(completeExRate);
            marginRateTotal = marginRateTotal.add(marginRate);

            BigDecimal completeIn = BigDecimal.valueOf(Integer.parseInt(dto.getCompleteInAmt().replace("%","")));
            BigDecimal completeEx = BigDecimal.valueOf(Integer.parseInt(dto.getCompleteExAmt().replace("%","")));
            BigDecimal margin = BigDecimal.valueOf(Integer.parseInt(dto.getMarginAmt().replace("%","")));

            String name = dto.getName();

            if(completeIn.compareTo(completeInMax) >= 0 && !completeIn.equals(BigDecimal.ZERO)) {
                nameInMax = name;
                completeInMax = completeIn;
            }
            if(completeEx.compareTo(completeExMax) >= 0 && !completeEx.equals(BigDecimal.ZERO)) {
                nameExMax = name;
                completeExMax = completeEx;
            }
            if(margin.compareTo(marginMax) >= 0 && !margin.equals(BigDecimal.ZERO)) {
                nameMarginMax = name;
                marginMax = margin;
            }
        }

        int compareCompleteIn = completeInRateTotal.equals(BigDecimal.ZERO) ? 0 : completeInRateTotal.compareTo(BigDecimal.valueOf(100));
        int compareCompleteEx = completeExRateTotal.equals(BigDecimal.ZERO) ? 0 : completeExRateTotal.compareTo(BigDecimal.valueOf(100));
        int compareMargin = marginRateTotal.equals(BigDecimal.ZERO) ? 0 : marginRateTotal.compareTo(BigDecimal.valueOf(100));

        if (compareCompleteIn != 0 || compareCompleteEx != 0 || compareMargin != 0) {
            for(MarginTeamListGetDto dto : marginTeamList) {
                if(compareCompleteIn != 0 && dto.getName().equals(nameInMax)) {
                    BigDecimal completeInRate = BigDecimal.valueOf(Double.parseDouble(dto.getCompleteInAmtRate().replace("%","")));
                    dto.setCompleteInAmtRate((completeInRate.subtract(completeInRateTotal.subtract(BigDecimal.valueOf(100)))) + "%");
                }
                if(compareCompleteEx != 0 && dto.getName().equals(nameExMax)) {
                    BigDecimal completeExRate = BigDecimal.valueOf(Double.parseDouble(dto.getCompleteExAmtRate().replace("%","")));
                    dto.setCompleteExAmtRate((completeExRate.subtract(completeExRateTotal.subtract(BigDecimal.valueOf(100)))) + "%");
                }
                if(compareMargin != 0 && dto.getName().equals(nameMarginMax)) {
                    BigDecimal marginRate = BigDecimal.valueOf(Double.parseDouble(dto.getMarginAmtRate().replace("%","")));
                    dto.setMarginAmtRate((marginRate.subtract(marginRateTotal.subtract(BigDecimal.valueOf(100)))) + "%");
                }
            }
        }

        return marginTeamList;
    }

    /**
     * DC/DS 마진조회 > 파트별 리스트 조회
     */
    public List<MarginPartListGetDto> getMarginPartList(MarginListSetDto marginListSetDto) throws Exception {
        List<MarginPartListGetDto> marginPartList = marginSlaveMapper.selectMarginPartList(marginListSetDto);
        List<MarginPartListGetDto> marginPartElecList = marginSlaveMapper.selectMarginPartElecList(marginListSetDto);

        BigDecimal completeInRateTotal = BigDecimal.valueOf(0.0);
        BigDecimal completeExRateTotal = BigDecimal.valueOf(0.0);
        BigDecimal marginRateTotal = BigDecimal.valueOf(0.0);

        BigDecimal completeInMax = BigDecimal.valueOf(0);
        BigDecimal completeExMax = BigDecimal.valueOf(0);
        BigDecimal marginMax = BigDecimal.valueOf(0);

        String nameInMax = "";
        String nameExMax = "";
        String nameMarginMax = "";

        for(MarginPartListGetDto dto : marginPartList) {
            BigDecimal completeInRate = BigDecimal.valueOf(Double.parseDouble(dto.getCompleteInAmtRate().replace("%","")));
            BigDecimal completeExRate = BigDecimal.valueOf(Double.parseDouble(dto.getCompleteExAmtRate().replace("%","")));
            BigDecimal marginRate = BigDecimal.valueOf(Double.parseDouble(dto.getMarginAmtRate().replace("%","")));

            completeInRateTotal = completeInRateTotal.add(completeInRate);
            completeExRateTotal = completeExRateTotal.add(completeExRate);
            marginRateTotal = marginRateTotal.add(marginRate);

            BigDecimal completeIn = BigDecimal.valueOf(Integer.parseInt(dto.getCompleteInAmt().replace("%","")));
            BigDecimal completeEx = BigDecimal.valueOf(Integer.parseInt(dto.getCompleteExAmt().replace("%","")));
            BigDecimal margin = BigDecimal.valueOf(Integer.parseInt(dto.getMarginAmt().replace("%","")));

            String name = dto.getName();

            if(completeIn.compareTo(completeInMax) >= 0 && !completeIn.equals(BigDecimal.ZERO)) {
                nameInMax = name;
                completeInMax = completeIn;
            }
            if(completeEx.compareTo(completeExMax) >= 0 && !completeEx.equals(BigDecimal.ZERO)) {
                nameExMax = name;
                completeExMax = completeEx;
            }
            if(margin.compareTo(marginMax) >= 0 && !margin.equals(BigDecimal.ZERO)) {
                nameMarginMax = name;
                marginMax = margin;
            }
        }

        int compareCompleteIn = completeInRateTotal.equals(BigDecimal.ZERO) ? 0 : completeInRateTotal.compareTo(BigDecimal.valueOf(100));
        int compareCompleteEx = completeExRateTotal.equals(BigDecimal.ZERO) ? 0 : completeExRateTotal.compareTo(BigDecimal.valueOf(100));
        int compareMargin = marginRateTotal.equals(BigDecimal.ZERO) ? 0 : marginRateTotal.compareTo(BigDecimal.valueOf(100));

        if (compareCompleteIn != 0 || compareCompleteEx != 0 || compareMargin != 0) {
            for(MarginPartListGetDto dto : marginPartList) {
                if(compareCompleteIn != 0 && dto.getName().equals(nameInMax)) {
                    BigDecimal completeInRate = BigDecimal.valueOf(Double.parseDouble(dto.getCompleteInAmtRate().replace("%","")));
                    dto.setCompleteInAmtRate((completeInRate.subtract(completeInRateTotal.subtract(BigDecimal.valueOf(100)))) + "%");
                }
                if(compareCompleteEx != 0 && dto.getName().equals(nameExMax)) {
                    BigDecimal completeExRate = BigDecimal.valueOf(Double.parseDouble(dto.getCompleteExAmtRate().replace("%","")));
                    dto.setCompleteExAmtRate((completeExRate.subtract(completeExRateTotal.subtract(BigDecimal.valueOf(100)))) + "%");
                }
                if(compareMargin != 0 && dto.getName().equals(nameMarginMax)) {
                    BigDecimal marginRate = BigDecimal.valueOf(Double.parseDouble(dto.getMarginAmtRate().replace("%","")));
                    dto.setMarginAmtRate((marginRate.subtract(marginRateTotal.subtract(BigDecimal.valueOf(100)))) + "%");
                }
            }
        }

        int index = 0;
        for(MarginPartListGetDto dto : marginPartList) {
            if (dto.getName().equals("General Merchandise")) {
                completeInRateTotal = BigDecimal.valueOf(0.0);
                completeExRateTotal = BigDecimal.valueOf(0.0);
                marginRateTotal = BigDecimal.valueOf(0.0);

                completeInMax = BigDecimal.valueOf(0);
                completeExMax = BigDecimal.valueOf(0);
                marginMax = BigDecimal.valueOf(0);

                nameInMax = "";
                nameExMax = "";
                nameMarginMax = "";

                for(MarginPartListGetDto dtoElec : marginPartElecList) {
                    String name = dto.getName();

                    dtoElec.setName(" - " + dtoElec.getName());

                    BigDecimal completeIn = BigDecimal.valueOf(Long.parseLong(dto.getCompleteInAmt()));
                    BigDecimal completeInRate = BigDecimal.valueOf(Double.parseDouble(dto.getCompleteInAmtRate().replace("%","")));
                    BigDecimal completeInElec = BigDecimal.valueOf(Long.parseLong(dtoElec.getCompleteInAmt()));

                    if (!completeIn.equals(BigDecimal.ZERO) && !completeInRate.equals(BigDecimal.ZERO) && !completeInElec.equals(BigDecimal.ZERO)) {
                        BigDecimal completeInElecRate = completeInElec
                            .divide(completeIn,2, RoundingMode.HALF_EVEN)
                            .multiply(completeInRate)
                            .setScale(2,RoundingMode.HALF_EVEN);

                        dtoElec.setCompleteInAmtRate(completeInElecRate.toString()+"%");
                        completeInRateTotal = completeInRateTotal.add(completeInElecRate);
                    }

                    if(completeInElec.compareTo(completeInMax) >= 0 && !completeInElec.equals(BigDecimal.ZERO)) {
                        nameInMax = name;
                        completeInMax = completeInElec;
                    }

                    BigDecimal completeEx = BigDecimal.valueOf(Long.parseLong(dto.getCompleteExAmt()));
                    BigDecimal completeExRate = BigDecimal.valueOf(Double.parseDouble(dto.getCompleteExAmtRate().replace("%","")));
                    BigDecimal completeExElec = BigDecimal.valueOf(Long.parseLong(dtoElec.getCompleteExAmt()));

                    if (completeEx.compareTo(BigDecimal.ZERO) != 0
                        && completeExRate.compareTo(BigDecimal.ZERO) != 0
                        && completeExElec.compareTo(BigDecimal.ZERO) != 0) {
                        BigDecimal completeExElecRate = completeExElec
                            .divide(completeEx,2, RoundingMode.HALF_EVEN)
                            .multiply(completeExRate)
                            .setScale(2,RoundingMode.HALF_EVEN);

                        dtoElec.setCompleteExAmtRate(completeExElecRate.toString()+"%");
                        completeExRateTotal = completeExRateTotal.add(completeExElecRate);
                    }

                    if(completeExElec.compareTo(completeExMax) >= 0 && !completeExElec.equals(BigDecimal.ZERO)) {
                        nameExMax = name;
                        completeExMax = completeExElec;
                    }

                    BigDecimal margin = BigDecimal.valueOf(Long.parseLong(dto.getMarginAmt()));
                    BigDecimal marginRate = BigDecimal.valueOf(Double.parseDouble(dto.getMarginAmtRate().replace("%","")));
                    BigDecimal marginElec = BigDecimal.valueOf(Long.parseLong(dtoElec.getMarginAmt()));

                    if (margin.compareTo(BigDecimal.ZERO) != 0
                        && marginRate.compareTo(BigDecimal.ZERO) != 0
                        && marginElec.compareTo(BigDecimal.ZERO) != 0) {
                        BigDecimal marginElecRate = marginElec
                            .divide(margin,2, RoundingMode.HALF_EVEN)
                            .multiply(marginRate)
                            .setScale(2,RoundingMode.HALF_EVEN);

                        dtoElec.setMarginAmtRate(marginElecRate.toString()+"%");
                        marginRateTotal = marginRateTotal.add(marginElecRate);
                    }

                    if(marginElec.compareTo(marginMax) >= 0 && !marginElec.equals(BigDecimal.ZERO)) {
                        nameMarginMax = name;
                        marginMax = marginElec;
                    }
                }

                compareCompleteIn = completeInRateTotal.equals(BigDecimal.ZERO) ? 0 : completeInRateTotal.compareTo(
                    BigDecimal.valueOf(Double.parseDouble(dto.getCompleteInAmtRate().replace("%",""))));

                compareCompleteEx = completeExRateTotal.equals(BigDecimal.ZERO) ? 0 : completeExRateTotal.compareTo(
                    BigDecimal.valueOf(Double.parseDouble(dto.getCompleteExAmtRate().replace("%",""))));

                compareMargin = marginRateTotal.equals(BigDecimal.ZERO) ? 0 : marginRateTotal.compareTo(
                    BigDecimal.valueOf(Double.parseDouble(dto.getMarginAmtRate().replace("%",""))));

                for(MarginPartListGetDto dtoElec : marginPartElecList) {
                    if(compareCompleteIn != 0 && dto.getName().equals(nameInMax)) {
                        BigDecimal completeInRate = BigDecimal.valueOf(Double.parseDouble(dtoElec.getCompleteInAmtRate().replace("%","")));
                        dtoElec.setCompleteInAmtRate((completeInRate.subtract(completeInRateTotal.subtract(BigDecimal.valueOf(100)))) + "%");
                    }

                    if(compareCompleteEx != 0 && dto.getName().equals(nameExMax)) {
                        BigDecimal completeExRate = BigDecimal.valueOf(Double.parseDouble(dtoElec.getCompleteExAmtRate().replace("%","")));
                        dtoElec.setCompleteExAmtRate((completeExRate.subtract(completeExRateTotal.subtract(BigDecimal.valueOf(100)))) + "%");
                    }

                    if(compareMargin != 0 && dto.getName().equals(nameMarginMax)) {
                        BigDecimal marginRate = BigDecimal.valueOf(Double.parseDouble(dtoElec.getMarginAmtRate().replace("%","")));
                        dtoElec.setMarginAmtRate((marginRate.subtract(marginRateTotal.subtract(BigDecimal.valueOf(100)))) + "%");
                    }

                    marginPartList.add(index+1, dtoElec);
                }

                break;
            }

            index++;
        }

        return marginPartList;
    }

    /**
     * DC/DS 마진조회 > 유형별 리스트 조회
     */
    public List<MarginHeadListGetDto> getMarginHeadList(MarginListSetDto marginListSetDto) throws Exception {
        List<MarginHeadListGetDto> marginHeadList = marginSlaveMapper.selectMarginHeadList(marginListSetDto);

        BigDecimal completeInRateTotal = BigDecimal.valueOf(0.0);
        BigDecimal completeExRateTotal = BigDecimal.valueOf(0.0);
        BigDecimal marginRateTotal = BigDecimal.valueOf(0.0);

        BigDecimal completeInMax = BigDecimal.valueOf(0);
        BigDecimal completeExMax = BigDecimal.valueOf(0);
        BigDecimal marginMax = BigDecimal.valueOf(0);

        String nameInMax = "";
        String nameExMax = "";
        String nameMarginMax = "";

        for(MarginHeadListGetDto dto : marginHeadList) {
            BigDecimal completeInRate = BigDecimal.valueOf(Double.parseDouble(dto.getCompleteInAmtRate().replace("%","")));
            BigDecimal completeExRate = BigDecimal.valueOf(Double.parseDouble(dto.getCompleteExAmtRate().replace("%","")));
            BigDecimal marginRate = BigDecimal.valueOf(Double.parseDouble(dto.getMarginAmtRate().replace("%","")));

            completeInRateTotal = completeInRateTotal.add(completeInRate);
            completeExRateTotal = completeExRateTotal.add(completeExRate);
            marginRateTotal = marginRateTotal.add(marginRate);

            BigDecimal completeIn = BigDecimal.valueOf(Integer.parseInt(dto.getCompleteInAmt().replace("%","")));
            BigDecimal completeEx = BigDecimal.valueOf(Integer.parseInt(dto.getCompleteExAmt().replace("%","")));
            BigDecimal margin = BigDecimal.valueOf(Integer.parseInt(dto.getMarginAmt().replace("%","")));

            String name = dto.getName();

            if(completeIn.compareTo(completeInMax) >= 0 && !completeIn.equals(BigDecimal.ZERO)) {
                nameInMax = name;
                completeInMax = completeIn;
            }
            if(completeEx.compareTo(completeExMax) >= 0 && !completeEx.equals(BigDecimal.ZERO)) {
                nameExMax = name;
                completeExMax = completeEx;
            }
            if(margin.compareTo(marginMax) >= 0 && !margin.equals(BigDecimal.ZERO)) {
                nameMarginMax = name;
                marginMax = margin;
            }
        }

        int compareCompleteIn = completeInRateTotal.equals(BigDecimal.ZERO) ? 0 : completeInRateTotal.compareTo(BigDecimal.valueOf(100));
        int compareCompleteEx = completeExRateTotal.equals(BigDecimal.ZERO) ? 0 : completeExRateTotal.compareTo(BigDecimal.valueOf(100));
        int compareMargin = marginRateTotal.equals(BigDecimal.ZERO) ? 0 : marginRateTotal.compareTo(BigDecimal.valueOf(100));

        if (compareCompleteIn != 0 || compareCompleteEx != 0 || compareMargin != 0) {
            for(MarginHeadListGetDto dto : marginHeadList) {
                if(compareCompleteIn != 0 && dto.getName().equals(nameInMax)) {
                    BigDecimal completeInRate = BigDecimal.valueOf(Double.parseDouble(dto.getCompleteInAmtRate().replace("%","")));
                    dto.setCompleteInAmtRate((completeInRate.subtract(completeInRateTotal.subtract(BigDecimal.valueOf(100)))) + "%");
                }
                if(compareCompleteEx != 0 && dto.getName().equals(nameExMax)) {
                    BigDecimal completeExRate = BigDecimal.valueOf(Double.parseDouble(dto.getCompleteExAmtRate().replace("%","")));
                    dto.setCompleteExAmtRate((completeExRate.subtract(completeExRateTotal.subtract(BigDecimal.valueOf(100)))) + "%");
                }
                if(compareMargin != 0 && dto.getName().equals(nameMarginMax)) {
                    BigDecimal marginRate = BigDecimal.valueOf(Double.parseDouble(dto.getMarginAmtRate().replace("%","")));
                    dto.setMarginAmtRate((marginRate.subtract(marginRateTotal.subtract(BigDecimal.valueOf(100)))) + "%");
                }
            }
        }

        return marginHeadList;
    }

    /**
     * DC/DS 마진조회 > 유형별 리스트 조회
     */
    public List<MarginDetailListGetDto> getMarginDetailList(MarginDetailListSetDto marginDetailListSetDto) throws Exception {
        return marginSlaveMapper.selectMarginDetailList(marginDetailListSetDto);
    }
}
