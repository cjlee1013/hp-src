package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.dcOnlineReceivePay.DcOnlineOrderDetailListGetDto;
import kr.co.homeplus.settle.admin.model.dcOnlineReceivePay.DcOnlineOrderDetailListSetDto;
import kr.co.homeplus.settle.admin.model.dcOnlineReceivePay.DcOnlineReceivePayListGetDto;
import kr.co.homeplus.settle.admin.model.dcOnlineReceivePay.DcOnlineReceivePayListSetDto;
import kr.co.homeplus.settle.admin.model.unshipOrder.UnshipOrderListGetDto;
import kr.co.homeplus.settle.admin.model.unshipOrder.UnshipOrderListSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface DcOnlineReceivePaySlaveMapper {
    List<DcOnlineReceivePayListGetDto> selectDcOnlineReceivePayList(DcOnlineReceivePayListSetDto dcOnlineReceivePayListSetDto);
    List<DcOnlineOrderDetailListGetDto> selectDcOnlineOrderDetailList(DcOnlineOrderDetailListSetDto dcOnlineOrderDetailListSetDto);
}
