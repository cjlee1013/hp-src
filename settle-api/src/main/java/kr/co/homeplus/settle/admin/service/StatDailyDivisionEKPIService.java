package kr.co.homeplus.settle.admin.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import kr.co.homeplus.settle.admin.mapper.StatDailyDivisionEKPISlaveMapper;
import kr.co.homeplus.settle.admin.model.statDailyDivisionEKPI.DailyDivisionEKPIListGetDto;
import kr.co.homeplus.settle.admin.model.statDailyDivisionEKPI.DailyDivisionEKPIListSetDto;
import kr.co.homeplus.settle.admin.model.statDailyDivisionEKPI.DailySettleMarketItemGetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StatDailyDivisionEKPIService {
    private final StatDailyDivisionEKPISlaveMapper dailyDivisionEKPISlaveMapper;

    /**
     * 통계 > 상품주문통계 > 카테고리별 매출 (E-KPI)
     */
    public List<DailyDivisionEKPIListGetDto> getDailyDivisionEKPIList(DailyDivisionEKPIListSetDto listParamDto) throws Exception {
        return dailyDivisionEKPISlaveMapper.getDailyDivisionEKPIList(listParamDto);
    }

    /**
     * 통계 > 마켓통계 > 상품별 판매 통계 (SETTLE-699 SETTLE-904)
     */
    public List<DailySettleMarketItemGetDto> getDailySettleMarketItemList(DailyDivisionEKPIListSetDto listParamDto) throws Exception {
        List<DailySettleMarketItemGetDto> returnDto =  new ArrayList<DailySettleMarketItemGetDto>();

        SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyyMMdd");
        Date startDt = dateFormat.parse(listParamDto.getStartDt());
        Date endDt = dateFormat.parse(listParamDto.getEndDt());

        Calendar cal = Calendar.getInstance();
        Date tempDt = dateFormat.parse(listParamDto.getStartDt());
        cal.setTime(tempDt);
        dateFormat.format(cal.getTime());

        double diffDays = (endDt.getTime() - startDt.getTime()) / (24*60*60*1000); //일자수 차이

        ArrayList<String> dList = new ArrayList<String>();

        if (diffDays > 0) {
            for (int i = 1; i < diffDays; i++) {
                cal.add(Calendar.DATE, 1);
                dateFormat.format(cal.getTime());
                dList.add(dateFormat.format(cal.getTime()));
            }
            listParamDto.setBasicDt(dList);
        }

        return dailyDivisionEKPISlaveMapper.getDailySettleMarketItemList(listParamDto);
    }
}
