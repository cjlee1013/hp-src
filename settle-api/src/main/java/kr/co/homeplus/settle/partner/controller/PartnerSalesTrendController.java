package kr.co.homeplus.settle.partner.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.partner.model.salesTrend.SalesByAgeInfo;
import kr.co.homeplus.settle.partner.model.salesTrend.SalesByDailyInfo;
import kr.co.homeplus.settle.partner.model.salesTrend.SalesByGenderInfo;
import kr.co.homeplus.settle.partner.model.salesTrend.SalesTrendSummaryInfo;
import kr.co.homeplus.settle.partner.model.salesTrend.SearchConditionDto;
import kr.co.homeplus.settle.partner.service.PartnerSalesTrendService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Partner > 판매트렌드")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
@RestController
@RequestMapping(value = "/partner/salesTrend", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class PartnerSalesTrendController {

    private final PartnerSalesTrendService partnerSalesTrendService;

    @ApiOperation(value = "판매 트렌드 요약 정보")
    @ApiResponse(code = 200, message = "성공", response = SalesTrendSummaryInfo.class)
    @PostMapping("/summary")
    public ResponseObject<SalesTrendSummaryInfo> searchSummary(
        @ApiParam(value = "검색 조건", required = true) @RequestBody @Valid final SearchConditionDto searchConditionDto) {
        return ResourceConverter.toResponseObject(
            partnerSalesTrendService.searchSummary(searchConditionDto));
    }

    @ApiOperation(value = "일자별 조회, 결제 수")
    @ApiResponse(code = 200, message = "성공", response = SalesByDailyInfo.class)
    @PostMapping("/daily")
    public ResponseObject<List<SalesByDailyInfo>> searchSalesByDaily(
        @ApiParam(value = "검색 조건", required = true) @RequestBody @Valid final SearchConditionDto searchConditionDto) {
        return ResourceConverter.toResponseObject(
            partnerSalesTrendService.searchSalesByDaily(searchConditionDto));
    }

    @ApiOperation(value = "성별 판매비중")
    @ApiResponse(code = 200, message = "성공", response = SalesByGenderInfo.class)
    @PostMapping("/gender")
    public ResponseObject<List<SalesByGenderInfo>> searchSalesByGender(
        @ApiParam(value = "검색 조건", required = true) @RequestBody @Valid final SearchConditionDto searchConditionDto) {
        return ResourceConverter.toResponseObject(
            partnerSalesTrendService.searchSalesByGender(searchConditionDto));
    }

    @ApiOperation(value = "연령별 판매비중")
    @ApiResponse(code = 200, message = "성공", response = SalesByAgeInfo.class)
    @PostMapping("/age")
    public ResponseObject<List<SalesByAgeInfo>> searchSalesByAge(
        @ApiParam(value = "검색 조건", required = true) @RequestBody @Valid final SearchConditionDto searchConditionDto) {
        return ResourceConverter.toResponseObject(
            partnerSalesTrendService.searchSalesByAge(searchConditionDto));
    }
}
