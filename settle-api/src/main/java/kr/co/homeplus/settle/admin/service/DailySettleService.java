package kr.co.homeplus.settle.admin.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.settle.admin.mapper.DailySettleSlaveMapper;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleListGetDto;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleListSetDto;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleInfoGetDto;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleInfoSetDto;
import kr.co.homeplus.settle.admin.model.dailySettle.DailySettleSumGetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DailySettleService {
    private final DailySettleSlaveMapper dailySettleSlaveMapper;

    /**
     * 정산관리 > 매출관리 > 메인 리스트 조회
     */
    public List<DailySettleListGetDto> getDailySettleList(DailySettleListSetDto listParamDto) throws Exception {
        List<DailySettleListGetDto> returnDto = new ArrayList<DailySettleListGetDto>();

        //SETTLE-915 인덱스 날짜범위 검색
        SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyyMMdd");
        Date startDt = dateFormat.parse(listParamDto.getStartDt());
        Date endDt = dateFormat.parse(listParamDto.getEndDt());

        Calendar cal = Calendar.getInstance();
        Date tempDt = dateFormat.parse(listParamDto.getStartDt());
        cal.setTime(tempDt);
        dateFormat.format(cal.getTime());

        double diffDays = (endDt.getTime() - startDt.getTime()) / (24*60*60*1000); //일자수 차이

        if (diffDays >= 14) {
            int interval = (listParamDto.getMallType().equals("TD")? 10 : 7);
            String tempEndDt = "";
            List<DailySettleListGetDto> _tempList  = new ArrayList<DailySettleListGetDto>();

            switch (((int) Math.floor(diffDays / interval)) + 1) {
                case 2:
                    tempEndDt = listParamDto.getEndDt();
                    cal.add(Calendar.DATE, interval);
                    dateFormat.format(cal.getTime());
                    listParamDto.setEndDt(dateFormat.format(cal.getTime())); //END
                    returnDto = dailySettleSlaveMapper.getDailySettleList(listParamDto);

                    listParamDto.setStartDt(dateFormat.format(cal.getTime())); //START
                    listParamDto.setEndDt(tempEndDt); //END
                    _tempList = dailySettleSlaveMapper.getDailySettleList(listParamDto);
                    if (_tempList != null) {
                        returnDto.addAll(_tempList);
                    }
                    break;
                case 3:
                case 4:
                    tempEndDt = listParamDto.getEndDt();
                    cal.add(Calendar.DATE, interval);
                    dateFormat.format(cal.getTime());
                    listParamDto.setEndDt(dateFormat.format(cal.getTime())); //END
                    returnDto = dailySettleSlaveMapper.getDailySettleList(listParamDto);

                    listParamDto.setStartDt(dateFormat.format(cal.getTime())); //START
                    cal.add(Calendar.DATE, interval);
                    dateFormat.format(cal.getTime());
                    listParamDto.setEndDt(dateFormat.format(cal.getTime())); //END
                    _tempList = dailySettleSlaveMapper.getDailySettleList(listParamDto);
                    if (_tempList != null) {
                        returnDto.addAll(_tempList);
                    }

                    listParamDto.setStartDt(dateFormat.format(cal.getTime())); //START
                    listParamDto.setEndDt(tempEndDt); //END
                    _tempList = dailySettleSlaveMapper.getDailySettleList(listParamDto);
                    if (_tempList != null) {
                        returnDto.addAll(_tempList);
                    }
                    break;
                default:
                    returnDto = dailySettleSlaveMapper.getDailySettleList(listParamDto);
                    break;
            }
        }
        else {
            returnDto = dailySettleSlaveMapper.getDailySettleList(listParamDto);
        }


        for (DailySettleListGetDto dto : returnDto) {
            dto.setPartnerId(PrivacyMaskingUtils.maskingUserId(dto.getPartnerId()));
        }

        return returnDto;
    }

    /**
     * 정산관리 > 매출관리 > 파트너 상세 매출 조회
     */
    public List<DailySettleInfoGetDto> getDailySettleInfo(DailySettleInfoSetDto listParamDto) throws Exception {
        List<DailySettleInfoGetDto> returnDto = dailySettleSlaveMapper.getDailySettleInfo(listParamDto);
        for (DailySettleInfoGetDto dto : returnDto) {
            dto.setUserNm(PrivacyMaskingUtils.maskingUserName(dto.getUserNm()));
        }

        return returnDto;
    }

    /**
     * 정산관리 > 매출관리 > 점별 매출금액 합계
     */
    public List<DailySettleSumGetDto> getDailySettleSum(DailySettleListSetDto listParamDto) throws Exception {
        List<DailySettleSumGetDto> returnDto;
        if ((listParamDto.getItemNo() != null && listParamDto.getItemNo() != "") || (listParamDto.getItemNm() != null && listParamDto.getItemNm() != "")) {
            returnDto = dailySettleSlaveMapper.getDailySettleSum(listParamDto);
        } else {
            returnDto = dailySettleSlaveMapper.getDailySettleStore(listParamDto);  //SETTLE-437 점별합계테이블 추가
        }
        return returnDto;
    }

}
