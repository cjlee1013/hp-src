package kr.co.homeplus.settle.admin.model.couponStatistics;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CouponPeriodStatisticsGetDto {
    @ApiModelProperty(value = "점포유형")
    private String siteType;

    @ApiModelProperty(value = "쿠폰번호")
    private String couponNo;

    @ApiModelProperty(value = "쿠폰종류")
    private String discountKindNm;

    @ApiModelProperty(value = "쿠폰종류코드")
    private String discountKind;

    @ApiModelProperty(value = "마켓유형")
    private String marketType;

    @ApiModelProperty(value = "쿠폰명")
    private String couponNm;

    @ApiModelProperty(value = "쿠폰바코드")
    private String barcode;

    @ApiModelProperty(value = "주문건수")
    private String purchaseCnt;

    @ApiModelProperty(value = "주문금액")
    private String completeAmt;

    @ApiModelProperty(value = "사용금액")
    private String discountAmt;
}
