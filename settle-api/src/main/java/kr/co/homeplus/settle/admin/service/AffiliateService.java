package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.settle.admin.mapper.AffiliateSlaveMapper;
import kr.co.homeplus.settle.admin.model.affiliate.AffiliateDetailGetDto;
import kr.co.homeplus.settle.admin.model.affiliate.AffiliateDetailSetDto;
import kr.co.homeplus.settle.admin.model.affiliate.AffiliateSalesGetDto;
import kr.co.homeplus.settle.admin.model.affiliate.AffiliateSalesSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
@Configuration
public class AffiliateService {
  private final AffiliateSlaveMapper affiliateSlaveMapper;

  public List<AffiliateSalesGetDto> getAffiliateSales(AffiliateSalesSetDto affiliateSalesSetDto){
    return affiliateSlaveMapper.getAffiliateSales(affiliateSalesSetDto);
  }
  public List<AffiliateDetailGetDto> getAffiliateDetail(AffiliateDetailSetDto affiliateDetailSetDto){
    List<AffiliateDetailGetDto> affiliateDetailGetDtoList = affiliateSlaveMapper.getAffiliateDetail(affiliateDetailSetDto);
    for (AffiliateDetailGetDto affiliateDetailGetDto : affiliateDetailGetDtoList) {
      affiliateDetailGetDto.setOcbSaveCardNo(PrivacyMaskingUtils.maskingCard(affiliateDetailGetDto.getOcbSaveCardNo()));
    }
    return affiliateDetailGetDtoList;
  }
}
