package kr.co.homeplus.settle.admin.service;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.mapper.CouponPeriodStatisticsSlaveMapper;
import kr.co.homeplus.settle.admin.mapper.UnshipSlipMasterMapper;
import kr.co.homeplus.settle.admin.mapper.UnshipSlipSlaveMapper;
import kr.co.homeplus.settle.admin.model.couponStatistics.CouponPeriodStatisticsDetailGetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CouponPeriodStatisticsGetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CouponPeriodStatisticsSetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipCreateGetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipCreateSetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipListGetDto;
import kr.co.homeplus.settle.admin.model.unshipSlip.TdUnshipSlipListSetDto;
import kr.co.homeplus.settle.core.exception.handler.ExceptionCode;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CouponPeriodStatisticsService {
    private final CouponPeriodStatisticsSlaveMapper couponPeriodStatisticsSlaveMapper;

    /**
     * 기간별 쿠폰통계 > 메인 리스트 조회
     */
    public List<CouponPeriodStatisticsGetDto> getCouponPeriodStatisticsList(CouponPeriodStatisticsSetDto couponPeriodStatisticsSetDto) {
        couponPeriodStatisticsSetDto.setSchStartDt(couponPeriodStatisticsSetDto.getSchStartDt().replace("-",""));
        couponPeriodStatisticsSetDto.setSchEndDt(couponPeriodStatisticsSetDto.getSchEndDt().replace("-",""));
        return couponPeriodStatisticsSlaveMapper.selectCouponPeriodStatisticsList(couponPeriodStatisticsSetDto);
    }

    /**
     * 기간별 쿠폰통계 > 상세 리스트 조회
     */
    public List<CouponPeriodStatisticsDetailGetDto> getCouponPeriodStatisticsDetailList(CouponPeriodStatisticsSetDto couponPeriodStatisticsSetDto) {
        return couponPeriodStatisticsSlaveMapper.selectCouponPeriodStatisticsDetailList(couponPeriodStatisticsSetDto);
    }

}
