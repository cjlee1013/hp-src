package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.partnerSettle.PartnerSettleCommonSetDto;
import kr.co.homeplus.settle.admin.model.partnerSettle.PartnerSettleListGetDto;
import kr.co.homeplus.settle.admin.model.partnerSettle.PartnerSettleListSetDto;
import kr.co.homeplus.settle.admin.model.partnerSettleDetail.PartnerSettleDetailGetDto;
import kr.co.homeplus.settle.admin.model.partnerSettleDetail.PartnerSettleItemGetDto;
import kr.co.homeplus.settle.admin.model.partnerSettleDetail.PartnerSettleItemSetDto;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerSettlePreDtSetDto;
import kr.co.homeplus.settle.admin.service.PartnerSettleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/partnerSettle")
@Api(tags = "BackOffice > 정산관리")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class PartnerSettleController {
    private final PartnerSettleService settleService;

    @ApiOperation(value = "정산관리 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getPartnerSettleList")
    public ResponseObject<List<PartnerSettleListGetDto>> getPartnerSettleList(@RequestBody PartnerSettleListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(settleService.getPartnerSettleList(listParamDto));
    }

    @ApiOperation(value = "정산관리 > 파트너 정산 > 상품 상세 내역 조회", response = ResponseObject.class)
    @PostMapping("/getPartnerSettleItem")
    public ResponseObject<List<PartnerSettleItemGetDto>> getPartnerSettleItem(@RequestBody PartnerSettleItemSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(settleService.getPartnerSettleItem(listParamDto));
    }

    @ApiOperation(value = "정산관리 > 파트너 정산완료", response = ResponseObject.class)
    @PostMapping("/setSettleComplete")
    public ResponseObject<String> setSettleComplete(@RequestBody PartnerSettleCommonSetDto listParamDto) throws Exception {
        return settleService.setSettleComplete(listParamDto);
    }

    @ApiOperation(value = "정산관리 > 파트너 정산보류", response = ResponseObject.class)
    @PostMapping("/setSettlePostpone")
    public ResponseObject<String> setSettlePostpone(@RequestBody PartnerSettleCommonSetDto listParamDto) throws Exception {
        return settleService.setSettlePostpone(listParamDto);
    }

    @ApiOperation(value = "정산관리 > 파트너 정산보류해제", response = ResponseObject.class)
    @PostMapping("/setPostponeCancel")
    public ResponseObject<String> setPostponeCancel(@RequestBody PartnerSettleCommonSetDto listParamDto) throws Exception {
        return settleService.setPostponeCancel(listParamDto);
    }

    @ApiOperation(value = "정산관리 > 지급예정일 변경", response = ResponseObject.class)
    @PostMapping("/setSettlePreDt")
    public ResponseObject<String> setSettlePreDt(@RequestBody PartnerSettlePreDtSetDto listParamDto) throws Exception {
        return settleService.setSettlePreDt(listParamDto);
    }

    @ApiOperation(value = "정산관리 > 판매자 정산정보", response = ResponseObject.class)
    @PostMapping("/getPartnerSettleDetail")
    public ResponseObject<List<PartnerSettleDetailGetDto>> getPartnerSettleDetail(@RequestBody PartnerSettleListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(settleService.getPartnerSettleDetail(listParamDto));
    }
}