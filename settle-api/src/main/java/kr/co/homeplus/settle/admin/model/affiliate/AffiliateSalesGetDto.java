package kr.co.homeplus.settle.admin.model.affiliate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "전표조회 목록")
public class AffiliateSalesGetDto {
  @ApiModelProperty(notes = "집계기간")
  private String basicDt;

  @ApiModelProperty(notes = "제휴채널ID")
  private String affiliateCd;

  @ApiModelProperty(notes = "제휴채널명")
  private String channelNm;

  @ApiModelProperty(notes = "제휴업체명")
  private String affiliateNm;

  @ApiModelProperty(notes = "사이트구분코드")
  private String siteType;

  @ApiModelProperty(notes = "사이트구분")
  private String siteName;

  @ApiModelProperty(notes = "주문건 수")
  private String orderCnt;

  @ApiModelProperty(notes = "취소건 수")
  private String cancelCnt;

  @ApiModelProperty(notes = "결제금액")
  private String orderPrice;

  @ApiModelProperty(notes = "배송비")
  private String shipPrice;

  @ApiModelProperty(notes = "부가세")
  private String vatPrice;

  @ApiModelProperty(notes = "실적대상금액")
  private String supplyAmt;

  @ApiModelProperty(notes = "수수료율")
  private String commissionRate;

  @ApiModelProperty(notes = "수수료액")
  private String commissionAmt;
}
