package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.statStorePeriodEKPI.StorePeriodListGetDto;
import kr.co.homeplus.settle.admin.model.statStorePeriodEKPI.StorePeriodListSetDto;
import kr.co.homeplus.settle.admin.service.StatStorePeriodEKPIService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/statStorePeriodEKPI")
@Api(tags = "BackOffice > 상품주문통계 > 기간별 점포/판매업체 판매현황 (E-KPI)")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class StatStorePeriodEKPIController {
    private final StatStorePeriodEKPIService settleService;

    @ApiOperation(value = "통계 > 상품주문통계 > 기간별 점포 판매현황 (E-KPI)", response = ResponseObject.class)
    @PostMapping("/getStoreList")
    public ResponseObject<List<StorePeriodListGetDto>> getStoreList(@RequestBody StorePeriodListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(settleService.getStoreList(listParamDto));
    }

}