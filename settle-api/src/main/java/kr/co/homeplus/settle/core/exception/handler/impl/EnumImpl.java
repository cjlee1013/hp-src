package kr.co.homeplus.settle.core.exception.handler.impl;

public interface EnumImpl {
    String getResponseCode();
    String getResponseMessage();
}