package kr.co.homeplus.settle.partner.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.partner.model.finalSettle.PartnerFinalSettleGetDto;
import kr.co.homeplus.settle.partner.model.finalSettle.PartnerFinalSettleSetDto;
import kr.co.homeplus.settle.partner.model.finalSettle.PartnerSettleAmtSumGetDto;
import kr.co.homeplus.settle.partner.model.salesVAT.PartnerSalesVATListGetDto;
import kr.co.homeplus.settle.partner.model.salesVAT.PartnerSalesVATListSetDto;
import kr.co.homeplus.settle.partner.model.salesVAT.PartnerSalesVATOrderGetDto;
import kr.co.homeplus.settle.partner.model.settleInfo.SettleInfoGetDto;
import kr.co.homeplus.settle.partner.model.settleInfo.DailySettleGetDto;
import kr.co.homeplus.settle.partner.model.settleInfo.DailySettleSetDto;
import kr.co.homeplus.settle.partner.model.taxBill.PartnerTaxBillListGetDto;
import kr.co.homeplus.settle.partner.model.taxBill.PartnerTaxBillListSetDto;
import kr.co.homeplus.settle.partner.service.PartnerSettleInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/partner/settleInfo")
@Api(tags = "Partner > 정산관리")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class PartnerSettleInfoController {
    private final PartnerSettleInfoService partnerSettleService;

    @ApiOperation(value = "정산관리 > 매출관리 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getDailySettle")
    public ResponseObject<List<DailySettleGetDto>> getDailySettle(@RequestBody DailySettleSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(partnerSettleService.getDailySettle(listParamDto));
    }

    @ApiOperation(value = "정산관리 > 매출관리 > 상세 매출 조회", response = ResponseObject.class)
    @PostMapping("/getSettleInfo")
    public ResponseObject<List<SettleInfoGetDto>> getSettleInfo(@RequestBody DailySettleSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(partnerSettleService.getSettleInfo(listParamDto));
    }

    @ApiOperation(value = "정산관리 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getFinalSettle")
    public ResponseObject<List<PartnerFinalSettleGetDto>> getFinalSettle(@RequestBody PartnerFinalSettleSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(partnerSettleService.getFinalSettle(listParamDto));
    }

    @ApiOperation(value = "부가세신고내역 > 부가세신고내역 조회", response = ResponseObject.class)
    @PostMapping("/getSalesVATList")
    public ResponseObject<List<PartnerSalesVATListGetDto>> getSalesVATList(@RequestBody PartnerSalesVATListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(partnerSettleService.getSalesVATList(listParamDto));
    }

    @ApiOperation(value = "부가세신고내역 > 주문 상세내역", response = ResponseObject.class)
    @PostMapping("/getSalesVATOrder")
    public ResponseObject<List<PartnerSalesVATOrderGetDto>> getSalesVATOrder(@RequestBody PartnerSalesVATListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(partnerSettleService.getSalesVATOrder(listParamDto));
    }

    @ApiOperation(value = "세금계산서 조회 > 메인 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getTaxBillList")
    public ResponseObject<List<PartnerTaxBillListGetDto>> getTaxBillList(@RequestBody PartnerTaxBillListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(partnerSettleService.getTaxBillList(listParamDto));
    }

    @ApiOperation(value = "파트너메인 > 정산예정금액 조회", response = ResponseObject.class)
    @GetMapping("/getPartnerSettleAmt")
    public ResponseObject<PartnerSettleAmtSumGetDto> getPartnerSettleAmt(@ApiParam(name = "partnerId", value = "파트너ID", required = true) @RequestParam(name ="partnerId") String partnerId) throws Exception {
        return ResourceConverter.toResponseObject(partnerSettleService.getPartnerSettleAmt(partnerId));
    }
}