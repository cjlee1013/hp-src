package kr.co.homeplus.settle.partner.mapper;

import java.util.List;
import kr.co.homeplus.settle.partner.model.settleInfo.SettleInfoGetDto;
import kr.co.homeplus.settle.partner.model.settleInfo.DailySettleGetDto;
import kr.co.homeplus.settle.partner.model.settleInfo.DailySettleSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface PartnerDailySettleSlaveMapper {
    List<DailySettleGetDto> getDailySettle(DailySettleSetDto listParamDto);

    List<SettleInfoGetDto> getSettleInfo(DailySettleSetDto listParamDto);
}
