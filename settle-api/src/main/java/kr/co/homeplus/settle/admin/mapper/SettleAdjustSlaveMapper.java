package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.adjust.AdjustItemGetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustListGetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustListSetDto;
import kr.co.homeplus.settle.admin.model.adjust.AdjustType;
import kr.co.homeplus.settle.admin.model.adjust.AdjustTypeInfo;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;
import org.springframework.web.bind.annotation.RequestParam;

@SlaveConnection
public interface SettleAdjustSlaveMapper {
    List<AdjustListGetDto> getAdjustList(AdjustListSetDto listParamDto);

    AdjustItemGetDto getAdjustItem(@RequestParam(name ="itemNo") String itemNo);

    List<AdjustTypeInfo> getAdjustType(@RequestParam(name ="adjustType", required = false, defaultValue = "") String adjustType);
}
