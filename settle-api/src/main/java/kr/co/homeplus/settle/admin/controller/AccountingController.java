package kr.co.homeplus.settle.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.accounting.APBalanceSheetGetDto;
import kr.co.homeplus.settle.admin.model.accounting.APInvoiceConfirmGetDto;
import kr.co.homeplus.settle.admin.model.accounting.APInvoiceConfirmSetDto;
import kr.co.homeplus.settle.admin.model.accounting.APInvoiceListGetDto;
import kr.co.homeplus.settle.admin.model.accounting.APInvoiceListSetDto;
import kr.co.homeplus.settle.admin.model.accounting.AccountingSubjectListGetDto;
import kr.co.homeplus.settle.admin.model.accounting.AccountingSubjectListSetDto;
import kr.co.homeplus.settle.admin.model.balanceSheet.BalanceSheetSlipTypeGetDto;
import kr.co.homeplus.settle.admin.service.AccountingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/accounting")
@Api(tags = "BackOffice > 계정과목별 전표조회 / AP전표 조회")
@Slf4j
@ApiResponses(value = {
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Failure")
    }
)
public class AccountingController {
    private final AccountingService accountingService;

    @ApiOperation(value = "정산관리 > 회계자료 조회 > 계정과목별 데이터 조회", response = ResponseObject.class)
    @PostMapping("/getAccountSubjectList")
    public ResponseObject<List<AccountingSubjectListGetDto>> getAccountSubjectList(@RequestBody AccountingSubjectListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(accountingService.getAccountSubjectList(listParamDto));
    }

    @ApiOperation(value = "정산관리 > 회계자료 조회 > AP전표 조회", response = ResponseObject.class)
    @PostMapping("/getAPInvoiceList")
    public ResponseObject<List<APInvoiceListGetDto>> getAPInvoiceList(@RequestBody APInvoiceListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(accountingService.getAPInvoiceList(listParamDto));
    }

    @ApiOperation(value = "정산관리 > 회계자료 조회 > AP전표 상세데이터 조회", response = ResponseObject.class)
    @PostMapping("/getAPBalanceSheet")
    public ResponseObject<List<APBalanceSheetGetDto>> getAPBalanceSheet(@RequestBody APInvoiceListSetDto listParamDto) throws Exception {
        return ResourceConverter.toResponseObject(accountingService.getAPBalanceSheet(listParamDto));
    }

    @ApiOperation(value = "정산관리 > 회계자료 조회 > AP전표 확정 OF로 전송", response = ResponseObject.class)
    @PostMapping("/setAPInvoiceConfirm")
    public ResponseObject<String> setAPInvoiceConfirm(@RequestBody List<APInvoiceConfirmSetDto> listParamDto) throws Exception {
        return accountingService.setAPInvoiceConfirm(listParamDto);
    }

    @ApiOperation(value = "정산관리 > 회계자료 조회 > AP 전표 목록 조회", response = ResponseObject.class)
    @GetMapping("/getAPSlipTypeList")
    public ResponseObject<List<BalanceSheetSlipTypeGetDto>> getAPSlipTypeList() throws Exception {
        return ResourceConverter.toResponseObject(accountingService.getAPSlipTypeList());
    }
}