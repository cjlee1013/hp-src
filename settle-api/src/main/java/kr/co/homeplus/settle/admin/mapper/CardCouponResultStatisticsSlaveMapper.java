package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.couponStatistics.CardCouponResultStatisticsDetailGetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CardCouponResultStatisticsGetDto;
import kr.co.homeplus.settle.admin.model.couponStatistics.CardCouponResultStatisticsSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface CardCouponResultStatisticsSlaveMapper {
    List<CardCouponResultStatisticsGetDto> selectCardCouponResultStatisticsList(CardCouponResultStatisticsSetDto cardCouponResultStatisticsSetDto);
    List<CardCouponResultStatisticsDetailGetDto> selectCardCouponResultStatisticsDetailList(CardCouponResultStatisticsSetDto cardCouponResultStatisticsSetDto);
}
