package kr.co.homeplus.settle.batch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
@AllArgsConstructor
public class PaymentResultDto {
  private String partnerId;
  private String paymentDt;
  private String resultCode;

  public void setResultCode(String resultCode) {
    this.resultCode = resultCode;
  }
}