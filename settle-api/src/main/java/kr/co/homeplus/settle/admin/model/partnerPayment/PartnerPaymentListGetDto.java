package kr.co.homeplus.settle.admin.model.partnerPayment;
    
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "정산관리 > 지급 관리")
public class PartnerPaymentListGetDto {

    @ApiModelProperty(value = "지급SRL")
    @RealGridColumnInfo(headText = "지급SRL", width = 100)
    private String settleSrl;

    @ApiModelProperty(value = "지급상태")
    @RealGridColumnInfo(headText = "지급상태", width = 100)
    private String settlePayState;

    @ApiModelProperty(value = "판매업체ID")
    @RealGridColumnInfo(headText = "판매업체ID", width = 150)
    private String partnerId;

    @ApiModelProperty(value = "업체코드")
    @RealGridColumnInfo(headText = "업체코드", width = 150)
    private String ofVendorCd;

    @ApiModelProperty(value = "판매업체명")
    @RealGridColumnInfo(headText = "판매업체명", width = 150)
    private String partnerName;

    @ApiModelProperty(value = "상호명")
    @RealGridColumnInfo(headText = "상호명", width = 150)
    private String partnerNm;

    @ApiModelProperty(value = "지급예정일")
    @RealGridColumnInfo(headText = "지급예정일", width = 150)
    private String settlePreDt;

    @ApiModelProperty(value = "지급완료일")
    @RealGridColumnInfo(headText = "지급완료일", width = 150)
    private String settleDt;

    @ApiModelProperty(value = "정산주기")
    @RealGridColumnInfo(headText = "정산주기", width = 100)
    private String settleCycleType;

    @ApiModelProperty(value = "실지급액")
    @RealGridColumnInfo(headText = "실지급액", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String settleAmt;

    @ApiModelProperty(value = "사업자번호")
    @RealGridColumnInfo(headText = "사업자번호", width = 150)
    private String partnerNo;

    @ApiModelProperty(value = "은행명")
    @RealGridColumnInfo(headText = "은행명", width = 100, hidden = true)
    private String bankNm;

    @ApiModelProperty(value = "계좌번호")
    @RealGridColumnInfo(headText = "계좌번호", width = 100, hidden = true)
    private String bankAccountNo;

    @ApiModelProperty(value = "예금주")
    @RealGridColumnInfo(headText = "예금주", width = 100, hidden = true)
    private String bankAccountHolder;

    @ApiModelProperty(value = "지급대행ID")
    @RealGridColumnInfo(headText = "계좌변경", width = 100)
    private String payPartnerId;

    @ApiModelProperty(value = "지급방법")
    @RealGridColumnInfo(headText = "지급방법", width = 100)
    private String payMethod;

    @ApiModelProperty(value = "지급주체")
    @RealGridColumnInfo(headText = "지급주체", width = 100)
    private String payCompany;

    @ApiModelProperty(value = "수정일")
    @RealGridColumnInfo(headText = "수정일", width = 150)
    private String chgDt;

    @ApiModelProperty(value = "수정자")
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgId;

    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(String bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

}
    
    