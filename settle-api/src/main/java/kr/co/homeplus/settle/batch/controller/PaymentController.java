package kr.co.homeplus.settle.batch.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.batch.service.PaymentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/batch/Payment")
@Api(tags = "지급대행 요청")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class PaymentController {

  private final PaymentService paymentService;

  @ApiOperation(value = "업체 등록", response = ResponseObject.class, notes = "업체 등록")
  @RequestMapping(value = "/setNewPartner", method = {RequestMethod.POST})
  public ResponseObject<Integer> setNewPartner () {
    return ResourceConverter.toResponseObject(paymentService.setNewPartner());
  }
  @ApiOperation(value = "업체 수정", response = ResponseObject.class, notes = "업체 수정")
  @RequestMapping(value = "/setChangePartner", method = {RequestMethod.POST})
  public ResponseObject<Integer> setChangePartner () {
    return ResourceConverter.toResponseObject(paymentService.setChangePartner());
  }
  @ApiOperation(value = "지급 요청", response = ResponseObject.class, notes = "지급 요청")
  @RequestMapping(value = "/setPaymentRequest", method = {RequestMethod.POST})
  public ResponseObject<Integer> setPaymentRequest () {
    return ResourceConverter.toResponseObject(paymentService.setPaymentRequest());
  }
  @ApiOperation(value = "지급 결과", response = ResponseObject.class, notes = "지급 결과")
  @RequestMapping(value = "/setPaymentResult", method = {RequestMethod.POST})
  public ResponseObject<Integer> setPaymentResult (String basicDt) {
    if (basicDt == null) {
      basicDt = LocalDateTime.ofInstant(Instant.now(),ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    }
    return ResourceConverter.toResponseObject(paymentService.setPaymentResult(basicDt));
  }
}
