package kr.co.homeplus.settle.admin.model.margin;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MarginListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String schStartDt;

    @ApiModelProperty(value = "검색종료일")
    private String schEndDt;

    @ApiModelProperty(value = "상품구분")
    private String mallType;
}
