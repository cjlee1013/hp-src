package kr.co.homeplus.settle.batch.model;

import java.nio.charset.Charset;
import kr.co.homeplus.settle.batch.config.OpmallConfig;
import kr.co.homeplus.settle.batch.enums.OpmallType;
import lombok.Data;
import org.springframework.web.util.UriComponentsBuilder;

@Data
public class OpmallHistorySetDto {
  private String type;
  private String url;
  private String resultCode;
  private String resultMsg;

  public OpmallHistorySetDto(OpmallType opmallType, OpmallConfig opmallConfig,OpmallRegistSetDto opmallRegistSetDto) {
    String gubun;
    if (opmallType.equals(OpmallType.NEW)) {
      gubun = "1";
    } else if (opmallType.equals(OpmallType.UPDATE)){
      gubun = "2";
    } else {
      return;
    }
    this.type = opmallType.getCode();
    this.url = UriComponentsBuilder.fromHttpUrl(opmallConfig.getIniRequestUrl())
        .queryParam("id_merchant",opmallConfig.getIniMid())
        .queryParam("id_mall", opmallRegistSetDto.getPartnerId())
        .queryParam("cl_id","2")
        .queryParam("no_comp", opmallRegistSetDto.getPartnerNo())
        .queryParam("nm_comp", opmallRegistSetDto.getPartnerNm())
        .queryParam("nm_boss", opmallRegistSetDto.getPartnerOwner())
        .queryParam("nm_regist", opmallRegistSetDto.getBankAccountHolder())
        .queryParam("cd_bank", opmallRegistSetDto.getBankCode())
        .queryParam("no_acct", opmallRegistSetDto.getBankAccount().replaceAll("[^\\d]", ""))
        .queryParam("no_tel", opmallRegistSetDto.getPhoneNo())
        .queryParam("cl_gubun",gubun)
        .build().encode(Charset.forName("EUC-KR")).toString();
  }

  public OpmallHistorySetDto(OpmallType opmallType, OpmallConfig opmallConfig,OpmallRequestSetDto opmallRequestSetDto) {
    String status;
    if (opmallType.equals(OpmallType.REQUEST)) {
      status = "I";
    } else if (opmallType.equals(OpmallType.CANCEL)){
      status = "D";
    } else {
      return;
    }
    this.type = opmallType.getCode();
    this.url = UriComponentsBuilder.fromHttpUrl(opmallConfig.getIniRequestUrl())
        .queryParam("id_merchant",opmallConfig.getIniMid())
        .queryParam("id_mall", opmallRequestSetDto.getPartnerId())
        .queryParam("dt_pay", opmallRequestSetDto.getPaymentDt())
        .queryParam("no_comp", opmallRequestSetDto.getPartnerNo())
        .queryParam("nm_regist", opmallRequestSetDto.getBankAccountHolder())
        .queryParam("cd_bank", opmallRequestSetDto.getBankCode())
        .queryParam("no_acct", opmallRequestSetDto.getBankAccount().replaceAll("[^\\d]", ""))
        .queryParam("amt_supply", opmallRequestSetDto.getPaymentAmt())
        .queryParam("cl_service","2")
        .queryParam("cl_status",status)
        .build().encode(Charset.forName("EUC-KR")).toString();
  }

  public OpmallHistorySetDto(OpmallType opmallType, OpmallConfig opmallConfig,OpmallResultSetDto opmallResultSetDto){
    String gubun;
    if (opmallType.equals(OpmallType.RESULT)) {
      gubun = "1";
    } else {
      return;
    }
    this.type = opmallType.getCode();
    this.url = UriComponentsBuilder.fromHttpUrl(opmallConfig.getIniResultUrl())
        .queryParam("strReqNo",gubun)
        .queryParam("strIdMerchant",opmallConfig.getIniMid())
        .queryParam("startDate", opmallResultSetDto.getStartDt())
        .queryParam("endDate", opmallResultSetDto.getEndDt())
        .queryParam("subMerchant", opmallResultSetDto.getPartnerId())
        .queryParam("clService","2")
        .build().encode(Charset.forName("EUC-KR")).toString();
  }
}