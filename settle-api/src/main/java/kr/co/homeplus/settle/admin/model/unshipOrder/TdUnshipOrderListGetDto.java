package kr.co.homeplus.settle.admin.model.unshipOrder;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TdUnshipOrderListGetDto {
    @ApiModelProperty(value = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "상품구분")
    private String mallType;

    @ApiModelProperty(value = "회사코드")
    private String companyCd;

    @ApiModelProperty(value = "명칭")
    private String companyNm;

    @ApiModelProperty(value = "점포코드")
    private String originStoreId;

    @ApiModelProperty(value = "점포명")
    private String storeNm;

    @ApiModelProperty(value = "카테고리그룹")
    private String division;

    @ApiModelProperty(value = "카테고리ID")
    private String deptNo;

    @ApiModelProperty(value = "카테고리명")
    private String categoryNm;

    @ApiModelProperty(value = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    private String itemNm;

    @ApiModelProperty(value = "수량")
    private String completeQty;

    @ApiModelProperty(value = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(value = "상품주문번호")
    private String orderItemNo;

    @ApiModelProperty(value = "주문금액")
    private String completeAmt;

    @ApiModelProperty(value = "임직원할인")
    private String empDiscountAmt;

    @ApiModelProperty(value = "상품할인(합계)")
    private String couponChargeSumAmt;

    @ApiModelProperty(value = "상품할인(업체)")
    private String couponSellerChargeAmt;

    @ApiModelProperty(value = "상품할인(카드사)")
    private String couponCardChargeAmt;

    @ApiModelProperty(value = "상품할인(자사)")
    private String couponHomeChargeAmt;

    @ApiModelProperty(value = "카드할인(합계)")
    private String cardCouponSumAmt;

    @ApiModelProperty(value = "카드할인(카드사)")
    private String cardCouponCardAmt;

    @ApiModelProperty(value = "카드할인(자사)")
    private String cardCouponHomeAmt;

    @ApiModelProperty(value = "매출(합계)")
    private String salesAmt;

    @ApiModelProperty(value = "매출(공급가액)")
    private String supplyAmt;

    @ApiModelProperty(value = "매출(부가세)")
    private String vatAmt;

    @ApiModelProperty(value = "원가")
    private String originPrice;

    @ApiModelProperty(value = "결제일")
    private String paymentBasicDt;

    @ApiModelProperty(value = "배송상태")
    private String shipStatus;

    @ApiModelProperty(value = "배송요청일")
    private String shipDt;

    @ApiModelProperty(value = "배송번호")
    private String bundleNo;

    @ApiModelProperty(value = "배송방법")
    private String shipMethodNm;

    @ApiModelProperty(value = "발송처리일시")
    private String shippingDt;

    @ApiModelProperty(value = "현배송상태")
    private String currentShipStatus;
}
