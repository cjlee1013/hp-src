package kr.co.homeplus.settle.admin.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settle.admin.model.averageOrderItemQty.AverageOrderItemQtyListGetDto;
import kr.co.homeplus.settle.admin.model.averageOrderItemQty.AverageOrderItemQtyListSetDto;
import kr.co.homeplus.settle.admin.service.AverageOrderItemQtyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/statistics/order")
@Api(tags = "BackOffice > 주문당 평균 주문 상품수 리스트 조회")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class AverageOrderItemQtyController {

    private final AverageOrderItemQtyService averageOrderItemQtyService;

    @ApiOperation(value = "주문당 평균 주문 상품수 리스트 조회", response = ResponseObject.class)
    @PostMapping("/getAverageOrderItemQtyList")
    public ResponseObject<List<AverageOrderItemQtyListGetDto>>  getAverageOrderItemQtyList(@RequestBody AverageOrderItemQtyListSetDto averageOrderItemQtyListSetDto) throws Exception {
        return ResourceConverter.toResponseObject(averageOrderItemQtyService.getAverageOrderItemQtyList(averageOrderItemQtyListSetDto));
    }
}
