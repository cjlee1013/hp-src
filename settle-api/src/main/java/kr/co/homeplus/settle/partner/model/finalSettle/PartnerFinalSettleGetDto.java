package kr.co.homeplus.settle.partner.model.finalSettle;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "정산관리 > 정산현황")
public class PartnerFinalSettleGetDto {
    @ApiModelProperty(value = "지급SRL")
    @RealGridColumnInfo(headText = "지급SRL", width = 100)
    private String settleSrl;

    @ApiModelProperty(value = "지급상태")
    @RealGridColumnInfo(headText = "지급상태", width = 100)
    private String settlePayState;

    @ApiModelProperty(value = "정산상태")
    @RealGridColumnInfo(headText = "정산상태", width = 100)
    private String settleState;

    @ApiModelProperty(value = "지급예정일")
    @RealGridColumnInfo(headText = "지급예정일", width = 150)
    private String settlePreDt;

    @ApiModelProperty(value = "정산주기")
    @RealGridColumnInfo(headText = "정산주기", width = 100)
    private String settleCycleType;

    @ApiModelProperty(value = "집계기간")
    @RealGridColumnInfo(headText = "집계기간", width = 150)
    private String salesPeriod;

    @ApiModelProperty(value = "지급예정금액")
    @RealGridColumnInfo(headText = "지급예정금액", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long settlePreAmt;

    @ApiModelProperty(value = "이전보류금액")
    @RealGridColumnInfo(headText = "이전보류금액", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long holdAmt;

    @ApiModelProperty(value = "정산대상금액")
    @RealGridColumnInfo(headText = "정산대상금액", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long settleAmt;

    @ApiModelProperty(value = "매출집계금액")
    @RealGridColumnInfo(headText = "매출집계금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long itemSalesAmt;

    @ApiModelProperty(value = "판매수수료")
    @RealGridColumnInfo(headText = "판매수수료", width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long saleAgencyFee;

    @ApiModelProperty(value = "배송비")
    @RealGridColumnInfo(headText = "배송비", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipAmt;

    @ApiModelProperty(value = "반품배송비")
    @RealGridColumnInfo(headText = "반품배송비", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimShipAmt;

    @ApiModelProperty(value = "상품할인")
    @RealGridColumnInfo(headText = "상품할인", width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponSellerChargeAmt;

    @ApiModelProperty(value = "배송비할인")
    @RealGridColumnInfo(headText = "배송비할인", width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long sellerChargeShipAmt;

    @ApiModelProperty(value = "정산 조정금액")
    @RealGridColumnInfo(headText = "정산 조정금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long adjustAmt;

   @ApiModelProperty(value = "정산조정금액(선납)")
    @RealGridColumnInfo(headText = "정산조정금액(선납)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long adjustAdvAmt;

    @ApiModelProperty(value = "광고수수료")
    @RealGridColumnInfo(headText = "광고수수료", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long adjustFee;

    @ApiModelProperty(value = "광고수수료(선납)")
    @RealGridColumnInfo(headText = "광고수수료(선납)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long adjustAdvFee;
}
    
    