package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.resaSales.ResaSalesListGetDto;
import kr.co.homeplus.settle.admin.model.resaSales.ResaSalesListSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface ResaSalesSlaveMapper {
  List<ResaSalesListGetDto> getResaSalesListDay(ResaSalesListSetDto resaSalesListSetDto);
  List<ResaSalesListGetDto> getResaSalesListMonth(ResaSalesListSetDto resaSalesListSetDto);
  int getResaSalesExist(String basicDt);
}
