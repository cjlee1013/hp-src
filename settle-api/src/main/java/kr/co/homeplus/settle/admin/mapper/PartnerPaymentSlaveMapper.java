package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerPaymentListGetDto;
import kr.co.homeplus.settle.admin.model.partnerPayment.PartnerPaymentListSetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface PartnerPaymentSlaveMapper {
    List<PartnerPaymentListGetDto> getPartnerPaymentList(PartnerPaymentListSetDto listParamDto);
}
