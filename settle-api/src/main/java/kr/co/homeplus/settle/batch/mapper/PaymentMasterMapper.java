package kr.co.homeplus.settle.batch.mapper;

import kr.co.homeplus.settle.admin.model.pgReceivable.PgReceivableGetDto;
import kr.co.homeplus.settle.batch.model.OpmallHistorySetDto;
import kr.co.homeplus.settle.batch.model.PartnerInfoDto;
import kr.co.homeplus.settle.batch.model.PaymentRequestDto;
import kr.co.homeplus.settle.batch.model.PaymentResultDto;
import kr.co.homeplus.settle.core.db.annotation.MasterConnection;

@MasterConnection
public interface PaymentMasterMapper {
    int setChangePartner(PartnerInfoDto partnerInfoDto);
    int setNewPartner(PartnerInfoDto partnerInfoDto);
    int setPaymentRequest(PaymentRequestDto paymentRequestDto);
    int setHomeplusPaymentComplete();
    int setPartnerPaymentFail(PaymentRequestDto paymentResultDto);
    int setPartnerPaymentComplete(PaymentResultDto paymentResultDto);
    int setPartnerPaymentHold(PaymentResultDto paymentResultDto);
    PgReceivableGetDto getPgBalanceAmt(String pgKind);
    int setPaymentHistory(OpmallHistorySetDto opmallHistorySetDto);
}
