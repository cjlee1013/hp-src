package kr.co.homeplus.settle.admin.model.pgCompare;

import lombok.Data;

@Data
public class PgCompareCostSumGetDto {
    private String pgKind;
    private long hmpPaymentAmt;
    private long hmpCommissionAmt;
    private long hmpSettleAmt;
    private long pgPaymentAmt;
    private long pgCommissionAmt;
    private long pgSettleAmt;
    private long diffPaymentAmt;
    private long diffCommissionAmt;
    private long diffSettleAmt;
}
