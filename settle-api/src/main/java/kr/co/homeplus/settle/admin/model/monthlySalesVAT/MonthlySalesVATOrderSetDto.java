package kr.co.homeplus.settle.admin.model.monthlySalesVAT;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MonthlySalesVATOrderSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(value = "파트너ID")
    private String partnerId;
}
