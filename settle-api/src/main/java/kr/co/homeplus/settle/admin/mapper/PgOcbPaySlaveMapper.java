package kr.co.homeplus.settle.admin.mapper;

import java.util.List;
import kr.co.homeplus.settle.admin.model.ocb.PgOcbListSetDto;
import kr.co.homeplus.settle.admin.model.ocb.PgOcbStoreListGetDto;
import kr.co.homeplus.settle.admin.model.ocb.PgOcbDailyListGetDto;
import kr.co.homeplus.settle.admin.model.ocb.PgOcbTidListGetDto;
import kr.co.homeplus.settle.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface PgOcbPaySlaveMapper {
    List<PgOcbStoreListGetDto> getStorePgOcbPay(PgOcbListSetDto listParamDto);

    List<PgOcbDailyListGetDto> getDailyPgOcbPay(PgOcbListSetDto listParamDto);

    List<PgOcbTidListGetDto> getTidPgOcbPay(PgOcbListSetDto listParamDto);
}
