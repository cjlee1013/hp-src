/**
 * BranchServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.9  Built on : Nov 16, 2018 (12:05:37 GMT)
 */
package com.nhncorp.platform.shopn;


/**
 *  BranchServiceCallbackHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class BranchServiceCallbackHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public BranchServiceCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public BranchServiceCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for modifyBranchHoliday method
     * override this method for handling normal response from modifyBranchHoliday operation
     */
    public void receiveResultmodifyBranchHoliday(
        com.nhncorp.platform.shopn.BranchServiceStub.ModifyBranchHolidayResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from modifyBranchHoliday operation
     */
    public void receiveErrormodifyBranchHoliday(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for retrieveBranch method
     * override this method for handling normal response from retrieveBranch operation
     */
    public void receiveResultretrieveBranch(
        com.nhncorp.platform.shopn.BranchServiceStub.RetrieveBranchResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from retrieveBranch operation
     */
    public void receiveErrorretrieveBranch(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for modifyBranchAreaZipCd method
     * override this method for handling normal response from modifyBranchAreaZipCd operation
     */
    public void receiveResultmodifyBranchAreaZipCd(
        com.nhncorp.platform.shopn.BranchServiceStub.ModifyBranchAreaZipCdResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from modifyBranchAreaZipCd operation
     */
    public void receiveErrormodifyBranchAreaZipCd(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for retrieveBranchAreaDeliverySlot method
     * override this method for handling normal response from retrieveBranchAreaDeliverySlot operation
     */
    public void receiveResultretrieveBranchAreaDeliverySlot(
        com.nhncorp.platform.shopn.BranchServiceStub.RetrieveBranchAreaDeliverySlotResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from retrieveBranchAreaDeliverySlot operation
     */
    public void receiveErrorretrieveBranchAreaDeliverySlot(
        java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for addBranchAreaDeliverySlot method
     * override this method for handling normal response from addBranchAreaDeliverySlot operation
     */
    public void receiveResultaddBranchAreaDeliverySlot(
        com.nhncorp.platform.shopn.BranchServiceStub.AddBranchAreaDeliverySlotResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from addBranchAreaDeliverySlot operation
     */
    public void receiveErroraddBranchAreaDeliverySlot(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for createBranch method
     * override this method for handling normal response from createBranch operation
     */
    public void receiveResultcreateBranch(
        com.nhncorp.platform.shopn.BranchServiceStub.CreateBranchResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from createBranch operation
     */
    public void receiveErrorcreateBranch(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for modifyBranchAreaDeliverySlot method
     * override this method for handling normal response from modifyBranchAreaDeliverySlot operation
     */
    public void receiveResultmodifyBranchAreaDeliverySlot(
        com.nhncorp.platform.shopn.BranchServiceStub.ModifyBranchAreaDeliverySlotResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from modifyBranchAreaDeliverySlot operation
     */
    public void receiveErrormodifyBranchAreaDeliverySlot(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for addBranchAreaZipCd method
     * override this method for handling normal response from addBranchAreaZipCd operation
     */
    public void receiveResultaddBranchAreaZipCd(
        com.nhncorp.platform.shopn.BranchServiceStub.AddBranchAreaZipCdResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from addBranchAreaZipCd operation
     */
    public void receiveErroraddBranchAreaZipCd(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for retrieveBranchAreaZipCd method
     * override this method for handling normal response from retrieveBranchAreaZipCd operation
     */
    public void receiveResultretrieveBranchAreaZipCd(
        com.nhncorp.platform.shopn.BranchServiceStub.RetrieveBranchAreaZipCdResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from retrieveBranchAreaZipCd operation
     */
    public void receiveErrorretrieveBranchAreaZipCd(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for addBranchHoliday method
     * override this method for handling normal response from addBranchHoliday operation
     */
    public void receiveResultaddBranchHoliday(
        com.nhncorp.platform.shopn.BranchServiceStub.AddBranchHolidayResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from addBranchHoliday operation
     */
    public void receiveErroraddBranchHoliday(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for retrieveBranchHoliday method
     * override this method for handling normal response from retrieveBranchHoliday operation
     */
    public void receiveResultretrieveBranchHoliday(
        com.nhncorp.platform.shopn.BranchServiceStub.RetrieveBranchHolidayResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from retrieveBranchHoliday operation
     */
    public void receiveErrorretrieveBranchHoliday(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for modifyBranchArea method
     * override this method for handling normal response from modifyBranchArea operation
     */
    public void receiveResultmodifyBranchArea(
        com.nhncorp.platform.shopn.BranchServiceStub.ModifyBranchAreaResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from modifyBranchArea operation
     */
    public void receiveErrormodifyBranchArea(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for addBranchArea method
     * override this method for handling normal response from addBranchArea operation
     */
    public void receiveResultaddBranchArea(
        com.nhncorp.platform.shopn.BranchServiceStub.AddBranchAreaResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from addBranchArea operation
     */
    public void receiveErroraddBranchArea(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for modifyBranch method
     * override this method for handling normal response from modifyBranch operation
     */
    public void receiveResultmodifyBranch(
        com.nhncorp.platform.shopn.BranchServiceStub.ModifyBranchResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from modifyBranch operation
     */
    public void receiveErrormodifyBranch(java.lang.Exception e) {
    }
}
