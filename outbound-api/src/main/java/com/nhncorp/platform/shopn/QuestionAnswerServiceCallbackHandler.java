/**
 * QuestionAnswerServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.9  Built on : Nov 16, 2018 (12:05:37 GMT)
 */
package com.nhncorp.platform.shopn;


/**
 *  QuestionAnswerServiceCallbackHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class QuestionAnswerServiceCallbackHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public QuestionAnswerServiceCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public QuestionAnswerServiceCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for manageQuestionAnswer method
     * override this method for handling normal response from manageQuestionAnswer operation
     */
    public void receiveResultmanageQuestionAnswer(
        com.nhncorp.platform.shopn.QuestionAnswerServiceStub.ManageQuestionAnswerResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from manageQuestionAnswer operation
     */
    public void receiveErrormanageQuestionAnswer(Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getQuestionAnswerList method
     * override this method for handling normal response from getQuestionAnswerList operation
     */
    public void receiveResultgetQuestionAnswerList(
        com.nhncorp.platform.shopn.QuestionAnswerServiceStub.GetQuestionAnswerListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getQuestionAnswerList operation
     */
    public void receiveErrorgetQuestionAnswerList(Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getQuestionAnswerTemplateList method
     * override this method for handling normal response from getQuestionAnswerTemplateList operation
     */
    public void receiveResultgetQuestionAnswerTemplateList(
        com.nhncorp.platform.shopn.QuestionAnswerServiceStub.GetQuestionAnswerTemplateListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getQuestionAnswerTemplateList operation
     */
    public void receiveErrorgetQuestionAnswerTemplateList(Exception e) {
    }
}
