/**
 * CustomerInquiryServiceStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.9  Built on : Nov 16, 2018 (12:05:37 GMT)
 */
package com.nhncorp.platform.shopn.customerinquiry;


/*
 *  CustomerInquiryServiceStub java implementation
 */
public class CustomerInquiryServiceStub extends org.apache.axis2.client.Stub {
    private static int counter = 0;
    protected org.apache.axis2.description.AxisOperation[] _operations;

    //hashmaps to keep the fault mapping
    private java.util.HashMap faultExceptionNameMap = new java.util.HashMap();
    private java.util.HashMap faultExceptionClassNameMap = new java.util.HashMap();
    private java.util.HashMap faultMessageMap = new java.util.HashMap();
    private javax.xml.namespace.QName[] opNameArray = null;

    /**
     *Constructor that takes in a configContext
     */
    public CustomerInquiryServiceStub(
        org.apache.axis2.context.ConfigurationContext configurationContext,
        java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
        this(configurationContext, targetEndpoint, false);
    }

    /**
     * Constructor that takes in a configContext  and useseperate listner
     */
    public CustomerInquiryServiceStub(
        org.apache.axis2.context.ConfigurationContext configurationContext,
        java.lang.String targetEndpoint, boolean useSeparateListener)
        throws org.apache.axis2.AxisFault {
        //To populate AxisService
        populateAxisService();
        populateFaults();

        _serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext,
            _service);

        _serviceClient.getOptions()
            .setTo(new org.apache.axis2.addressing.EndpointReference(
                targetEndpoint));
        _serviceClient.getOptions().setUseSeparateListener(useSeparateListener);

        //Set the soap version
        _serviceClient.getOptions()
            .setSoapVersionURI(org.apache.axiom.soap.SOAP12Constants.SOAP_ENVELOPE_NAMESPACE_URI);
    }

    /**
     * Default Constructor
     */
    public CustomerInquiryServiceStub(
        org.apache.axis2.context.ConfigurationContext configurationContext)
        throws org.apache.axis2.AxisFault {
        this(configurationContext,
            "http://sandbox.api.naver.com/ShopN/CustomerInquiryService");
    }

    /**
     * Default Constructor
     */
    public CustomerInquiryServiceStub() throws org.apache.axis2.AxisFault {
        this("http://sandbox.api.naver.com/ShopN/CustomerInquiryService");
    }

    /**
     * Constructor taking the target endpoint
     */
    public CustomerInquiryServiceStub(java.lang.String targetEndpoint)
        throws org.apache.axis2.AxisFault {
        this(null, targetEndpoint);
    }

    private static synchronized java.lang.String getUniqueSuffix() {
        // reset the counter if it is greater than 99999
        if (counter > 99999) {
            counter = 0;
        }

        counter = counter + 1;

        return java.lang.Long.toString(java.lang.System.currentTimeMillis()) +
            "_" + counter;
    }

    private void populateAxisService() throws org.apache.axis2.AxisFault {
        //creating the Service with a unique name
        _service = new org.apache.axis2.description.AxisService(
            "CustomerInquiryService" + getUniqueSuffix());
        addAnonymousOperations();

        //creating the operations
        org.apache.axis2.description.AxisOperation __operation;

        _operations = new org.apache.axis2.description.AxisOperation[2];

        __operation = new org.apache.axis2.description.OutInAxisOperation();

        __operation.setName(new javax.xml.namespace.QName(
            "http://customerinquiry.shopn.platform.nhncorp.com/",
            "getCustomerInquiryList"));
        _service.addOperation(__operation);

        _operations[0] = __operation;

        __operation = new org.apache.axis2.description.OutInAxisOperation();

        __operation.setName(new javax.xml.namespace.QName(
            "http://customerinquiry.shopn.platform.nhncorp.com/",
            "answerCustomerInquiry"));
        _service.addOperation(__operation);

        _operations[1] = __operation;
    }

    //populates the faults
    private void populateFaults() {
    }

    /**
     * Auto generated method signature
     *
     * @see com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryService#getCustomerInquiryList
     * @param getCustomerInquiryListRequest0
     */
    public com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListResponse getCustomerInquiryList(
        com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListRequest getCustomerInquiryListRequest0)
        throws java.rmi.RemoteException {
        org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

        try {
            org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
            _operationClient.getOptions()
                .setAction("CustomerInquiryService#GetCustomerInquiryList");
            _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

            addPropertyToOperationClient(_operationClient,
                org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
                "&");

            // create SOAP envelope with that payload
            org.apache.axiom.soap.SOAPEnvelope env = null;

            env = toEnvelope(getFactory(_operationClient.getOptions()
                    .getSoapVersionURI()),
                getCustomerInquiryListRequest0,
                optimizeContent(
                    new javax.xml.namespace.QName(
                        "http://customerinquiry.shopn.platform.nhncorp.com/",
                        "getCustomerInquiryList")),
                new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "GetCustomerInquiryListRequest"));

            //adding SOAP soap_headers
            _serviceClient.addHeadersToEnvelope(env);
            // set the message context with that soap envelope
            _messageContext.setEnvelope(env);

            // add the message contxt to the operation client
            _operationClient.addMessageContext(_messageContext);

            //execute the operation client
            _operationClient.execute(true);

            org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
            org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

            java.lang.Object object = fromOM(_returnEnv.getBody()
                    .getFirstElement(),
                com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListResponse.class);

            return (com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListResponse) object;
        } catch (org.apache.axis2.AxisFault f) {
            org.apache.axiom.om.OMElement faultElt = f.getDetail();

            if (faultElt != null) {
                if (faultExceptionNameMap.containsKey(
                    new org.apache.axis2.client.FaultMapKey(
                        faultElt.getQName(), "GetCustomerInquiryList"))) {
                    //make the fault by reflection
                    try {
                        java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(
                            faultElt.getQName(),
                            "GetCustomerInquiryList"));
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
                        java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());

                        //message class
                        java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(
                            faultElt.getQName(),
                            "GetCustomerInquiryList"));
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,
                            messageClass);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                            new java.lang.Class[] { messageClass });
                        m.invoke(ex, new java.lang.Object[] { messageObject });

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    } catch (java.lang.ClassCastException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                } else {
                    throw f;
                }
            } else {
                throw f;
            }
        } finally {
            if (_messageContext.getTransportOut() != null) {
                _messageContext.getTransportOut().getSender()
                    .cleanup(_messageContext);
            }
        }
    }

    /**
     * Auto generated method signature for Asynchronous Invocations
     *
     * @see com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryService#startgetCustomerInquiryList
     * @param getCustomerInquiryListRequest0
     */
    public void startgetCustomerInquiryList(
        com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListRequest getCustomerInquiryListRequest0,
        final com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceCallbackHandler callback)
        throws java.rmi.RemoteException {
        org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
        _operationClient.getOptions()
            .setAction("CustomerInquiryService#GetCustomerInquiryList");
        _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

        addPropertyToOperationClient(_operationClient,
            org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
            "&");

        // create SOAP envelope with that payload
        org.apache.axiom.soap.SOAPEnvelope env = null;
        final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

        //Style is Doc.
        env = toEnvelope(getFactory(_operationClient.getOptions()
                .getSoapVersionURI()),
            getCustomerInquiryListRequest0,
            optimizeContent(
                new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "getCustomerInquiryList")),
            new javax.xml.namespace.QName(
                "http://customerinquiry.shopn.platform.nhncorp.com/",
                "GetCustomerInquiryListRequest"));

        // adding SOAP soap_headers
        _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);

        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
            public void onMessage(
                org.apache.axis2.context.MessageContext resultContext) {
                try {
                    org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

                    java.lang.Object object = fromOM(resultEnv.getBody()
                            .getFirstElement(),
                        com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListResponse.class);
                    callback.receiveResultgetCustomerInquiryList((com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListResponse) object);
                } catch (org.apache.axis2.AxisFault e) {
                    callback.receiveErrorgetCustomerInquiryList(e);
                }
            }

            public void onError(java.lang.Exception error) {
                if (error instanceof org.apache.axis2.AxisFault) {
                    org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
                    org.apache.axiom.om.OMElement faultElt = f.getDetail();

                    if (faultElt != null) {
                        if (faultExceptionNameMap.containsKey(
                            new org.apache.axis2.client.FaultMapKey(
                                faultElt.getQName(),
                                "GetCustomerInquiryList"))) {
                            //make the fault by reflection
                            try {
                                java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(
                                    faultElt.getQName(),
                                    "GetCustomerInquiryList"));
                                java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                                java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
                                java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());

                                //message class
                                java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(
                                    faultElt.getQName(),
                                    "GetCustomerInquiryList"));
                                java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                                java.lang.Object messageObject = fromOM(faultElt,
                                    messageClass);
                                java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                    new java.lang.Class[] { messageClass });
                                m.invoke(ex,
                                    new java.lang.Object[] { messageObject });

                                callback.receiveErrorgetCustomerInquiryList(new java.rmi.RemoteException(
                                    ex.getMessage(), ex));
                            } catch (java.lang.ClassCastException e) {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErrorgetCustomerInquiryList(f);
                            } catch (java.lang.ClassNotFoundException e) {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErrorgetCustomerInquiryList(f);
                            } catch (java.lang.NoSuchMethodException e) {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErrorgetCustomerInquiryList(f);
                            } catch (java.lang.reflect.InvocationTargetException e) {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErrorgetCustomerInquiryList(f);
                            } catch (java.lang.IllegalAccessException e) {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErrorgetCustomerInquiryList(f);
                            } catch (java.lang.InstantiationException e) {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErrorgetCustomerInquiryList(f);
                            } catch (org.apache.axis2.AxisFault e) {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErrorgetCustomerInquiryList(f);
                            }
                        } else {
                            callback.receiveErrorgetCustomerInquiryList(f);
                        }
                    } else {
                        callback.receiveErrorgetCustomerInquiryList(f);
                    }
                } else {
                    callback.receiveErrorgetCustomerInquiryList(error);
                }
            }

            public void onFault(
                org.apache.axis2.context.MessageContext faultContext) {
                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                onError(fault);
            }

            public void onComplete() {
                try {
                    _messageContext.getTransportOut().getSender()
                        .cleanup(_messageContext);
                } catch (org.apache.axis2.AxisFault axisFault) {
                    callback.receiveErrorgetCustomerInquiryList(axisFault);
                }
            }
        });

        org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;

        if ((_operations[0].getMessageReceiver() == null) &&
            _operationClient.getOptions().isUseSeparateListener()) {
            _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
            _operations[0].setMessageReceiver(_callbackReceiver);
        }

        //execute the operation client
        _operationClient.execute(false);
    }

    /**
     * Auto generated method signature
     *
     * @see com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryService#answerCustomerInquiry
     * @param answerCustomerInquiryRequest2
     */
    public com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryResponse answerCustomerInquiry(
        com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryRequest answerCustomerInquiryRequest2)
        throws java.rmi.RemoteException {
        org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

        try {
            org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[1].getName());
            _operationClient.getOptions()
                .setAction("CustomerInquiryService#AnswerCustomerInquiry");
            _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

            addPropertyToOperationClient(_operationClient,
                org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
                "&");

            // create SOAP envelope with that payload
            org.apache.axiom.soap.SOAPEnvelope env = null;

            env = toEnvelope(getFactory(_operationClient.getOptions()
                    .getSoapVersionURI()),
                answerCustomerInquiryRequest2,
                optimizeContent(
                    new javax.xml.namespace.QName(
                        "http://customerinquiry.shopn.platform.nhncorp.com/",
                        "answerCustomerInquiry")),
                new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "AnswerCustomerInquiryRequest"));

            //adding SOAP soap_headers
            _serviceClient.addHeadersToEnvelope(env);
            // set the message context with that soap envelope
            _messageContext.setEnvelope(env);

            // add the message contxt to the operation client
            _operationClient.addMessageContext(_messageContext);

            //execute the operation client
            _operationClient.execute(true);

            org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
            org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

            java.lang.Object object = fromOM(_returnEnv.getBody()
                    .getFirstElement(),
                com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryResponse.class);

            return (com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryResponse) object;
        } catch (org.apache.axis2.AxisFault f) {
            org.apache.axiom.om.OMElement faultElt = f.getDetail();

            if (faultElt != null) {
                if (faultExceptionNameMap.containsKey(
                    new org.apache.axis2.client.FaultMapKey(
                        faultElt.getQName(), "AnswerCustomerInquiry"))) {
                    //make the fault by reflection
                    try {
                        java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(
                            faultElt.getQName(), "AnswerCustomerInquiry"));
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
                        java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());

                        //message class
                        java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(
                            faultElt.getQName(), "AnswerCustomerInquiry"));
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,
                            messageClass);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                            new java.lang.Class[] { messageClass });
                        m.invoke(ex, new java.lang.Object[] { messageObject });

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    } catch (java.lang.ClassCastException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                } else {
                    throw f;
                }
            } else {
                throw f;
            }
        } finally {
            if (_messageContext.getTransportOut() != null) {
                _messageContext.getTransportOut().getSender()
                    .cleanup(_messageContext);
            }
        }
    }

    /**
     * Auto generated method signature for Asynchronous Invocations
     *
     * @see com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryService#startanswerCustomerInquiry
     * @param answerCustomerInquiryRequest2
     */
    public void startanswerCustomerInquiry(
        com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryRequest answerCustomerInquiryRequest2,
        final com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceCallbackHandler callback)
        throws java.rmi.RemoteException {
        org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[1].getName());
        _operationClient.getOptions()
            .setAction("CustomerInquiryService#AnswerCustomerInquiry");
        _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

        addPropertyToOperationClient(_operationClient,
            org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
            "&");

        // create SOAP envelope with that payload
        org.apache.axiom.soap.SOAPEnvelope env = null;
        final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

        //Style is Doc.
        env = toEnvelope(getFactory(_operationClient.getOptions()
                .getSoapVersionURI()),
            answerCustomerInquiryRequest2,
            optimizeContent(
                new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "answerCustomerInquiry")),
            new javax.xml.namespace.QName(
                "http://customerinquiry.shopn.platform.nhncorp.com/",
                "AnswerCustomerInquiryRequest"));

        // adding SOAP soap_headers
        _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);

        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
            public void onMessage(
                org.apache.axis2.context.MessageContext resultContext) {
                try {
                    org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

                    java.lang.Object object = fromOM(resultEnv.getBody()
                            .getFirstElement(),
                        com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryResponse.class);
                    callback.receiveResultanswerCustomerInquiry((com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryResponse) object);
                } catch (org.apache.axis2.AxisFault e) {
                    callback.receiveErroranswerCustomerInquiry(e);
                }
            }

            public void onError(java.lang.Exception error) {
                if (error instanceof org.apache.axis2.AxisFault) {
                    org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
                    org.apache.axiom.om.OMElement faultElt = f.getDetail();

                    if (faultElt != null) {
                        if (faultExceptionNameMap.containsKey(
                            new org.apache.axis2.client.FaultMapKey(
                                faultElt.getQName(),
                                "AnswerCustomerInquiry"))) {
                            //make the fault by reflection
                            try {
                                java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(
                                    faultElt.getQName(),
                                    "AnswerCustomerInquiry"));
                                java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                                java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
                                java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());

                                //message class
                                java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(
                                    faultElt.getQName(),
                                    "AnswerCustomerInquiry"));
                                java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                                java.lang.Object messageObject = fromOM(faultElt,
                                    messageClass);
                                java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                    new java.lang.Class[] { messageClass });
                                m.invoke(ex,
                                    new java.lang.Object[] { messageObject });

                                callback.receiveErroranswerCustomerInquiry(new java.rmi.RemoteException(
                                    ex.getMessage(), ex));
                            } catch (java.lang.ClassCastException e) {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErroranswerCustomerInquiry(f);
                            } catch (java.lang.ClassNotFoundException e) {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErroranswerCustomerInquiry(f);
                            } catch (java.lang.NoSuchMethodException e) {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErroranswerCustomerInquiry(f);
                            } catch (java.lang.reflect.InvocationTargetException e) {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErroranswerCustomerInquiry(f);
                            } catch (java.lang.IllegalAccessException e) {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErroranswerCustomerInquiry(f);
                            } catch (java.lang.InstantiationException e) {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErroranswerCustomerInquiry(f);
                            } catch (org.apache.axis2.AxisFault e) {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErroranswerCustomerInquiry(f);
                            }
                        } else {
                            callback.receiveErroranswerCustomerInquiry(f);
                        }
                    } else {
                        callback.receiveErroranswerCustomerInquiry(f);
                    }
                } else {
                    callback.receiveErroranswerCustomerInquiry(error);
                }
            }

            public void onFault(
                org.apache.axis2.context.MessageContext faultContext) {
                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                onError(fault);
            }

            public void onComplete() {
                try {
                    _messageContext.getTransportOut().getSender()
                        .cleanup(_messageContext);
                } catch (org.apache.axis2.AxisFault axisFault) {
                    callback.receiveErroranswerCustomerInquiry(axisFault);
                }
            }
        });

        org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;

        if ((_operations[1].getMessageReceiver() == null) &&
            _operationClient.getOptions().isUseSeparateListener()) {
            _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
            _operations[1].setMessageReceiver(_callbackReceiver);
        }

        //execute the operation client
        _operationClient.execute(false);
    }

    private boolean optimizeContent(javax.xml.namespace.QName opName) {
        if (opNameArray == null) {
            return false;
        }

        for (int i = 0; i < opNameArray.length; i++) {
            if (opName.equals(opNameArray[i])) {
                return true;
            }
        }

        return false;
    }

    private org.apache.axiom.om.OMElement toOM(
        com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListRequest param,
        boolean optimizeContent) throws org.apache.axis2.AxisFault {
        try {
            return param.getOMElement(com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListRequest.MY_QNAME,
                org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private org.apache.axiom.om.OMElement toOM(
        com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListResponse param,
        boolean optimizeContent) throws org.apache.axis2.AxisFault {
        try {
            return param.getOMElement(com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListResponse.MY_QNAME,
                org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private org.apache.axiom.om.OMElement toOM(
        com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryRequest param,
        boolean optimizeContent) throws org.apache.axis2.AxisFault {
        try {
            return param.getOMElement(com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryRequest.MY_QNAME,
                org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private org.apache.axiom.om.OMElement toOM(
        com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryResponse param,
        boolean optimizeContent) throws org.apache.axis2.AxisFault {
        try {
            return param.getOMElement(com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryResponse.MY_QNAME,
                org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
        org.apache.axiom.soap.SOAPFactory factory,
        com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListRequest param,
        boolean optimizeContent, javax.xml.namespace.QName elementQName)
        throws org.apache.axis2.AxisFault {
        try {
            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
            emptyEnvelope.getBody()
                .addChild(param.getOMElement(
                    com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListRequest.MY_QNAME,
                    factory));

            return emptyEnvelope;
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    /* methods to provide back word compatibility */
    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
        org.apache.axiom.soap.SOAPFactory factory,
        com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryRequest param,
        boolean optimizeContent, javax.xml.namespace.QName elementQName)
        throws org.apache.axis2.AxisFault {
        try {
            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
            emptyEnvelope.getBody()
                .addChild(param.getOMElement(
                    com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryRequest.MY_QNAME,
                    factory));

            return emptyEnvelope;
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    /* methods to provide back word compatibility */

    /**
     *  get the default envelope
     */
    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
        org.apache.axiom.soap.SOAPFactory factory) {
        return factory.getDefaultEnvelope();
    }

    private java.lang.Object fromOM(org.apache.axiom.om.OMElement param,
        java.lang.Class type) throws org.apache.axis2.AxisFault {
        try {
            if (com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryRequest.class.equals(
                type)) {
                return com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
            }

            if (com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryResponse.class.equals(
                type)) {
                return com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
            }

            if (com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListRequest.class.equals(
                type)) {
                return com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
            }

            if (com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListResponse.class.equals(
                type)) {
                return com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
            }
        } catch (java.lang.Exception e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

        return null;
    }

    //http://sandbox.api.naver.com/ShopN/CustomerInquiryService
    public static class GetCustomerInquiryListResponseType
        extends BaseCheckoutResponseType implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = GetCustomerInquiryListResponseType
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         * field for CustomerInquiryList
         */
        protected CustomerInquiryList_type0 localCustomerInquiryList;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localCustomerInquiryListTracker = false;

        /**
         * field for ReturnedDataCount
         */
        protected int localReturnedDataCount;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localReturnedDataCountTracker = false;

        /**
         * field for HasMoreData
         */
        protected boolean localHasMoreData;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localHasMoreDataTracker = false;

        /**
         * field for InquiryExtraData
         */
        protected java.lang.String localInquiryExtraData;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localInquiryExtraDataTracker = false;

        public boolean isCustomerInquiryListSpecified() {
            return localCustomerInquiryListTracker;
        }

        /**
         * Auto generated getter method
         * @return CustomerInquiryList_type0
         */
        public CustomerInquiryList_type0 getCustomerInquiryList() {
            return localCustomerInquiryList;
        }

        /**
         * Auto generated setter method
         * @param param CustomerInquiryList
         */
        public void setCustomerInquiryList(CustomerInquiryList_type0 param) {
            localCustomerInquiryListTracker = param != null;

            this.localCustomerInquiryList = param;
        }

        public boolean isReturnedDataCountSpecified() {
            return localReturnedDataCountTracker;
        }

        /**
         * Auto generated getter method
         * @return int
         */
        public int getReturnedDataCount() {
            return localReturnedDataCount;
        }

        /**
         * Auto generated setter method
         * @param param ReturnedDataCount
         */
        public void setReturnedDataCount(int param) {
            // setting primitive attribute tracker to true
            localReturnedDataCountTracker = param != java.lang.Integer.MIN_VALUE;

            this.localReturnedDataCount = param;
        }

        public boolean isHasMoreDataSpecified() {
            return localHasMoreDataTracker;
        }

        /**
         * Auto generated getter method
         * @return boolean
         */
        public boolean getHasMoreData() {
            return localHasMoreData;
        }

        /**
         * Auto generated setter method
         * @param param HasMoreData
         */
        public void setHasMoreData(boolean param) {
            // setting primitive attribute tracker to true
            localHasMoreDataTracker = true;

            this.localHasMoreData = param;
        }

        public boolean isInquiryExtraDataSpecified() {
            return localInquiryExtraDataTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getInquiryExtraData() {
            return localInquiryExtraData;
        }

        /**
         * Auto generated setter method
         * @param param InquiryExtraData
         */
        public void setInquiryExtraData(java.lang.String param) {
            localInquiryExtraDataTracker = param != null;

            this.localInquiryExtraData = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                "http://customerinquiry.shopn.platform.nhncorp.com/");

            if ((namespacePrefix != null) &&
                (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":GetCustomerInquiryListResponseType",
                    xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "GetCustomerInquiryListResponseType", xmlWriter);
            }

            if (localRequestIDTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "RequestID", xmlWriter);

                if (localRequestID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "RequestID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRequestID);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "ResponseType", xmlWriter);

            if (localResponseType == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ResponseType cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localResponseType);
            }

            xmlWriter.writeEndElement();

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "ResponseTime", xmlWriter);

            if (localResponseTime == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "ResponseTime cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localResponseTime));
            }

            xmlWriter.writeEndElement();

            if (localErrorTracker) {
                if (localError == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "Error cannot be null!!");
                }

                localError.serialize(new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "Error"), xmlWriter);
            }

            if (localWarningListTracker) {
                if (localWarningList == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "WarningList cannot be null!!");
                }

                localWarningList.serialize(new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "WarningList"), xmlWriter);
            }

            if (localQuotaStatusTracker) {
                if (localQuotaStatus == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "QuotaStatus cannot be null!!");
                }

                localQuotaStatus.serialize(new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "QuotaStatus"), xmlWriter);
            }

            if (localDetailLevelTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "DetailLevel", xmlWriter);

                if (localDetailLevel == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "DetailLevel cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localDetailLevel);
                }

                xmlWriter.writeEndElement();
            }

            if (localVersionTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "Version", xmlWriter);

                if (localVersion == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "Version cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localVersion);
                }

                xmlWriter.writeEndElement();
            }

            if (localReleaseTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "Release", xmlWriter);

                if (localRelease == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "Release cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRelease);
                }

                xmlWriter.writeEndElement();
            }

            if (localTimestampTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "Timestamp", xmlWriter);

                if (localTimestamp == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "Timestamp cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localTimestamp));
                }

                xmlWriter.writeEndElement();
            }

            if (localMessageIDTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "MessageID", xmlWriter);

                if (localMessageID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "MessageID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localMessageID);
                }

                xmlWriter.writeEndElement();
            }

            if (localCustomerInquiryListTracker) {
                if (localCustomerInquiryList == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "CustomerInquiryList cannot be null!!");
                }

                localCustomerInquiryList.serialize(new javax.xml.namespace.QName(
                    "", "CustomerInquiryList"), xmlWriter);
            }

            if (localReturnedDataCountTracker) {
                namespace = "";
                writeStartElement(null, namespace, "ReturnedDataCount",
                    xmlWriter);

                if (localReturnedDataCount == java.lang.Integer.MIN_VALUE) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "ReturnedDataCount cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localReturnedDataCount));
                }

                xmlWriter.writeEndElement();
            }

            if (localHasMoreDataTracker) {
                namespace = "";
                writeStartElement(null, namespace, "HasMoreData", xmlWriter);

                if (false) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "HasMoreData cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localHasMoreData));
                }

                xmlWriter.writeEndElement();
            }

            if (localInquiryExtraDataTracker) {
                namespace = "";
                writeStartElement(null, namespace, "InquiryExtraData", xmlWriter);

                if (localInquiryExtraData == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "InquiryExtraData cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localInquiryExtraData);
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static GetCustomerInquiryListResponseType parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                GetCustomerInquiryListResponseType object = new GetCustomerInquiryListResponseType();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"GetCustomerInquiryListResponseType".equals(
                                type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (GetCustomerInquiryListResponseType) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "RequestID").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "RequestID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRequestID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "ResponseType").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ResponseType" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setResponseType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "ResponseTime").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ResponseTime" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setResponseTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Error").equals(reader.getName())) {
                        object.setError(ErrorType.Factory.parse(reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "WarningList").equals(reader.getName())) {
                        object.setWarningList(WarningListType.Factory.parse(
                            reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "QuotaStatus").equals(reader.getName())) {
                        object.setQuotaStatus(QuotaStatusType.Factory.parse(
                            reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "DetailLevel").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "DetailLevel" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setDetailLevel(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Version").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Version" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setVersion(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Release").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Release" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRelease(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Timestamp").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Timestamp" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setTimestamp(org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "MessageID").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "MessageID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setMessageID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CustomerInquiryList").equals(reader.getName())) {
                        object.setCustomerInquiryList(CustomerInquiryList_type0.Factory.parse(
                            reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ReturnedDataCount").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ReturnedDataCount" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setReturnedDataCount(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        object.setReturnedDataCount(java.lang.Integer.MIN_VALUE);
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "HasMoreData").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "HasMoreData" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setHasMoreData(org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "InquiryExtraData").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "InquiryExtraData" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setInquiryExtraData(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class WarningListType implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = WarningListType
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         * field for Warning
         * This was an Array!
         */
        protected WarningType[] localWarning;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localWarningTracker = false;

        public boolean isWarningSpecified() {
            return localWarningTracker;
        }

        /**
         * Auto generated getter method
         * @return WarningType[]
         */
        public WarningType[] getWarning() {
            return localWarning;
        }

        /**
         * validate the array for Warning
         */
        protected void validateWarning(WarningType[] param) {
        }

        /**
         * Auto generated setter method
         * @param param Warning
         */
        public void setWarning(WarningType[] param) {
            validateWarning(param);

            localWarningTracker = param != null;

            this.localWarning = param;
        }

        /**
         * Auto generated add method for the array for convenience
         * @param param WarningType
         */
        public void addWarning(WarningType param) {
            if (localWarning == null) {
                localWarning = new WarningType[] {  };
            }

            //update the setting tracker
            localWarningTracker = true;

            java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localWarning);
            list.add(param);
            this.localWarning = (WarningType[]) list.toArray(new WarningType[list.size()]);
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://customerinquiry.shopn.platform.nhncorp.com/");

                if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":WarningListType", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "WarningListType", xmlWriter);
                }
            }

            if (localWarningTracker) {
                if (localWarning != null) {
                    for (int i = 0; i < localWarning.length; i++) {
                        if (localWarning[i] != null) {
                            localWarning[i].serialize(new javax.xml.namespace.QName(
                                "http://customerinquiry.shopn.platform.nhncorp.com/",
                                "Warning"), xmlWriter);
                        } else {
                            // we don't have to do any thing since minOccures is zero
                        }
                    }
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "Warning cannot be null!!");
                }
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static WarningListType parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                WarningListType object = new WarningListType();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"WarningListType".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (WarningListType) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    java.util.ArrayList list1 = new java.util.ArrayList();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Warning").equals(reader.getName())) {
                        // Process the array and step past its final element's end.
                        list1.add(WarningType.Factory.parse(reader));

                        //loop until we find a start element that is not part of this array
                        boolean loopDone1 = false;

                        while (!loopDone1) {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement())
                                reader.next();

                            // Step out of this element
                            reader.next();

                            // Step to next element event.
                            while (!reader.isStartElement() &&
                                !reader.isEndElement())
                                reader.next();

                            if (reader.isEndElement()) {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone1 = true;
                            } else {
                                if (new javax.xml.namespace.QName(
                                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                                    "Warning").equals(reader.getName())) {
                                    list1.add(WarningType.Factory.parse(reader));
                                } else {
                                    loopDone1 = true;
                                }
                            }
                        }

                        // call the converter utility  to convert and set the array
                        object.setWarning((WarningType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            WarningType.class, list1));
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class GetCustomerInquiryListRequest implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://customerinquiry.shopn.platform.nhncorp.com/",
            "GetCustomerInquiryListRequest", "ns1");

        /**
         * field for GetCustomerInquiryListRequest
         */
        protected GetCustomerInquiryListRequestType localGetCustomerInquiryListRequest;

        /**
         * Auto generated getter method
         * @return GetCustomerInquiryListRequestType
         */
        public GetCustomerInquiryListRequestType getGetCustomerInquiryListRequest() {
            return localGetCustomerInquiryListRequest;
        }

        /**
         * Auto generated setter method
         * @param param GetCustomerInquiryListRequest
         */
        public void setGetCustomerInquiryListRequest(
            GetCustomerInquiryListRequestType param) {
            this.localGetCustomerInquiryListRequest = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, MY_QNAME));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localGetCustomerInquiryListRequest == null) {
                java.lang.String namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace,
                    "GetCustomerInquiryListRequest", xmlWriter);

                // write the nil attribute
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "nil", "1",
                    xmlWriter);
                xmlWriter.writeEndElement();
            } else {
                localGetCustomerInquiryListRequest.serialize(MY_QNAME, xmlWriter);
            }
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static GetCustomerInquiryListRequest parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                GetCustomerInquiryListRequest object = new GetCustomerInquiryListRequest();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "nil");

                    if ("true".equals(nillableValue) ||
                        "1".equals(nillableValue)) {
                        // Skip the element and report the null value.  It cannot have subelements.
                        while (!reader.isEndElement())
                            reader.next();

                        return object;
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                new javax.xml.namespace.QName(
                                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                                    "GetCustomerInquiryListRequest").equals(
                                    reader.getName())) {
                                nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                    "nil");

                                if ("true".equals(nillableValue) ||
                                    "1".equals(nillableValue)) {
                                    object.setGetCustomerInquiryListRequest(null);
                                    reader.next();
                                } else {
                                    object.setGetCustomerInquiryListRequest(GetCustomerInquiryListRequestType.Factory.parse(
                                        reader));
                                }
                            } // End of if for expected property start element

                            else {
                                // 3 - A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                        reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class CustomerInquiryList_type0 implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = CustomerInquiryList_type0
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         * field for CustomerInquiry
         * This was an Array!
         */
        protected InquiryType[] localCustomerInquiry;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localCustomerInquiryTracker = false;

        public boolean isCustomerInquirySpecified() {
            return localCustomerInquiryTracker;
        }

        /**
         * Auto generated getter method
         * @return InquiryType[]
         */
        public InquiryType[] getCustomerInquiry() {
            return localCustomerInquiry;
        }

        /**
         * validate the array for CustomerInquiry
         */
        protected void validateCustomerInquiry(InquiryType[] param) {
        }

        /**
         * Auto generated setter method
         * @param param CustomerInquiry
         */
        public void setCustomerInquiry(InquiryType[] param) {
            validateCustomerInquiry(param);

            localCustomerInquiryTracker = param != null;

            this.localCustomerInquiry = param;
        }

        /**
         * Auto generated add method for the array for convenience
         * @param param InquiryType
         */
        public void addCustomerInquiry(InquiryType param) {
            if (localCustomerInquiry == null) {
                localCustomerInquiry = new InquiryType[] {  };
            }

            //update the setting tracker
            localCustomerInquiryTracker = true;

            java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localCustomerInquiry);
            list.add(param);
            this.localCustomerInquiry = (InquiryType[]) list.toArray(new InquiryType[list.size()]);
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://customerinquiry.shopn.platform.nhncorp.com/");

                if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":CustomerInquiryList_type0",
                        xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "CustomerInquiryList_type0", xmlWriter);
                }
            }

            if (localCustomerInquiryTracker) {
                if (localCustomerInquiry != null) {
                    for (int i = 0; i < localCustomerInquiry.length; i++) {
                        if (localCustomerInquiry[i] != null) {
                            localCustomerInquiry[i].serialize(new javax.xml.namespace.QName(
                                "", "CustomerInquiry"), xmlWriter);
                        } else {
                            // we don't have to do any thing since minOccures is zero
                        }
                    }
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "CustomerInquiry cannot be null!!");
                }
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static CustomerInquiryList_type0 parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                CustomerInquiryList_type0 object = new CustomerInquiryList_type0();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"CustomerInquiryList_type0".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (CustomerInquiryList_type0) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    java.util.ArrayList list1 = new java.util.ArrayList();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CustomerInquiry").equals(
                            reader.getName())) {
                        // Process the array and step past its final element's end.
                        list1.add(InquiryType.Factory.parse(reader));

                        //loop until we find a start element that is not part of this array
                        boolean loopDone1 = false;

                        while (!loopDone1) {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement())
                                reader.next();

                            // Step out of this element
                            reader.next();

                            // Step to next element event.
                            while (!reader.isStartElement() &&
                                !reader.isEndElement())
                                reader.next();

                            if (reader.isEndElement()) {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone1 = true;
                            } else {
                                if (new javax.xml.namespace.QName("",
                                    "CustomerInquiry").equals(
                                    reader.getName())) {
                                    list1.add(InquiryType.Factory.parse(reader));
                                } else {
                                    loopDone1 = true;
                                }
                            }
                        }

                        // call the converter utility  to convert and set the array
                        object.setCustomerInquiry((InquiryType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            InquiryType.class, list1));
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class AccessCredentialsType implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = AccessCredentialsType
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         * field for AccessLicense
         */
        protected java.lang.String localAccessLicense;

        /**
         * field for Timestamp
         */
        protected java.lang.String localTimestamp;

        /**
         * field for Signature
         */
        protected java.lang.String localSignature;

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAccessLicense() {
            return localAccessLicense;
        }

        /**
         * Auto generated setter method
         * @param param AccessLicense
         */
        public void setAccessLicense(java.lang.String param) {
            this.localAccessLicense = param;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getTimestamp() {
            return localTimestamp;
        }

        /**
         * Auto generated setter method
         * @param param Timestamp
         */
        public void setTimestamp(java.lang.String param) {
            this.localTimestamp = param;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getSignature() {
            return localSignature;
        }

        /**
         * Auto generated setter method
         * @param param Signature
         */
        public void setSignature(java.lang.String param) {
            this.localSignature = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://customerinquiry.shopn.platform.nhncorp.com/");

                if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":AccessCredentialsType", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "AccessCredentialsType", xmlWriter);
                }
            }

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "AccessLicense", xmlWriter);

            if (localAccessLicense == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AccessLicense cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAccessLicense);
            }

            xmlWriter.writeEndElement();

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "Timestamp", xmlWriter);

            if (localTimestamp == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Timestamp cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTimestamp);
            }

            xmlWriter.writeEndElement();

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "Signature", xmlWriter);

            if (localSignature == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Signature cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSignature);
            }

            xmlWriter.writeEndElement();

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static AccessCredentialsType parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                AccessCredentialsType object = new AccessCredentialsType();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"AccessCredentialsType".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (AccessCredentialsType) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "AccessLicense").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "AccessLicense" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAccessLicense(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Timestamp").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Timestamp" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setTimestamp(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Signature").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Signature" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setSignature(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class AnswerCustomerInquiryRequest implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://customerinquiry.shopn.platform.nhncorp.com/",
            "AnswerCustomerInquiryRequest", "ns1");

        /**
         * field for AnswerCustomerInquiryRequest
         */
        protected AnswerCustomerInquiryRequestType localAnswerCustomerInquiryRequest;

        /**
         * Auto generated getter method
         * @return AnswerCustomerInquiryRequestType
         */
        public AnswerCustomerInquiryRequestType getAnswerCustomerInquiryRequest() {
            return localAnswerCustomerInquiryRequest;
        }

        /**
         * Auto generated setter method
         * @param param AnswerCustomerInquiryRequest
         */
        public void setAnswerCustomerInquiryRequest(
            AnswerCustomerInquiryRequestType param) {
            this.localAnswerCustomerInquiryRequest = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, MY_QNAME));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localAnswerCustomerInquiryRequest == null) {
                java.lang.String namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace,
                    "AnswerCustomerInquiryRequest", xmlWriter);

                // write the nil attribute
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "nil", "1",
                    xmlWriter);
                xmlWriter.writeEndElement();
            } else {
                localAnswerCustomerInquiryRequest.serialize(MY_QNAME, xmlWriter);
            }
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static AnswerCustomerInquiryRequest parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                AnswerCustomerInquiryRequest object = new AnswerCustomerInquiryRequest();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "nil");

                    if ("true".equals(nillableValue) ||
                        "1".equals(nillableValue)) {
                        // Skip the element and report the null value.  It cannot have subelements.
                        while (!reader.isEndElement())
                            reader.next();

                        return object;
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                new javax.xml.namespace.QName(
                                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                                    "AnswerCustomerInquiryRequest").equals(
                                    reader.getName())) {
                                nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                    "nil");

                                if ("true".equals(nillableValue) ||
                                    "1".equals(nillableValue)) {
                                    object.setAnswerCustomerInquiryRequest(null);
                                    reader.next();
                                } else {
                                    object.setAnswerCustomerInquiryRequest(AnswerCustomerInquiryRequestType.Factory.parse(
                                        reader));
                                }
                            } // End of if for expected property start element

                            else {
                                // 3 - A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                        reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class InquiryType implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = InquiryType
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         * field for InquiryID
         */
        protected java.lang.String localInquiryID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localInquiryIDTracker = false;

        /**
         * field for OrderID
         */
        protected java.lang.String localOrderID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localOrderIDTracker = false;

        /**
         * field for ProductOrderID
         */
        protected java.lang.String localProductOrderID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localProductOrderIDTracker = false;

        /**
         * field for ProductName
         */
        protected java.lang.String localProductName;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localProductNameTracker = false;

        /**
         * field for ProductID
         */
        protected java.lang.String localProductID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localProductIDTracker = false;

        /**
         * field for ProductOrderOption
         */
        protected java.lang.String localProductOrderOption;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localProductOrderOptionTracker = false;

        /**
         * field for CustomerID
         */
        protected java.lang.String localCustomerID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localCustomerIDTracker = false;

        /**
         * field for Title
         */
        protected java.lang.String localTitle;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localTitleTracker = false;

        /**
         * field for Category
         */
        protected java.lang.String localCategory;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localCategoryTracker = false;

        /**
         * field for InquiryDateTime
         */
        protected java.util.Calendar localInquiryDateTime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localInquiryDateTimeTracker = false;

        /**
         * field for InquiryContent
         */
        protected java.lang.String localInquiryContent;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localInquiryContentTracker = false;

        /**
         * field for AnswerContentID
         */
        protected java.lang.String localAnswerContentID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAnswerContentIDTracker = false;

        /**
         * field for AnswerContent
         */
        protected java.lang.String localAnswerContent;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAnswerContentTracker = false;

        /**
         * field for AnswerTempleteID
         */
        protected java.lang.String localAnswerTempleteID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAnswerTempleteIDTracker = false;

        /**
         * field for IsAnswered
         */
        protected boolean localIsAnswered;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localIsAnsweredTracker = false;

        /**
         * field for CustomerName
         */
        protected java.lang.String localCustomerName;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localCustomerNameTracker = false;

        public boolean isInquiryIDSpecified() {
            return localInquiryIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getInquiryID() {
            return localInquiryID;
        }

        /**
         * Auto generated setter method
         * @param param InquiryID
         */
        public void setInquiryID(java.lang.String param) {
            localInquiryIDTracker = param != null;

            this.localInquiryID = param;
        }

        public boolean isOrderIDSpecified() {
            return localOrderIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getOrderID() {
            return localOrderID;
        }

        /**
         * Auto generated setter method
         * @param param OrderID
         */
        public void setOrderID(java.lang.String param) {
            localOrderIDTracker = param != null;

            this.localOrderID = param;
        }

        public boolean isProductOrderIDSpecified() {
            return localProductOrderIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getProductOrderID() {
            return localProductOrderID;
        }

        /**
         * Auto generated setter method
         * @param param ProductOrderID
         */
        public void setProductOrderID(java.lang.String param) {
            localProductOrderIDTracker = param != null;

            this.localProductOrderID = param;
        }

        public boolean isProductNameSpecified() {
            return localProductNameTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getProductName() {
            return localProductName;
        }

        /**
         * Auto generated setter method
         * @param param ProductName
         */
        public void setProductName(java.lang.String param) {
            localProductNameTracker = param != null;

            this.localProductName = param;
        }

        public boolean isProductIDSpecified() {
            return localProductIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getProductID() {
            return localProductID;
        }

        /**
         * Auto generated setter method
         * @param param ProductID
         */
        public void setProductID(java.lang.String param) {
            localProductIDTracker = param != null;

            this.localProductID = param;
        }

        public boolean isProductOrderOptionSpecified() {
            return localProductOrderOptionTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getProductOrderOption() {
            return localProductOrderOption;
        }

        /**
         * Auto generated setter method
         * @param param ProductOrderOption
         */
        public void setProductOrderOption(java.lang.String param) {
            localProductOrderOptionTracker = param != null;

            this.localProductOrderOption = param;
        }

        public boolean isCustomerIDSpecified() {
            return localCustomerIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getCustomerID() {
            return localCustomerID;
        }

        /**
         * Auto generated setter method
         * @param param CustomerID
         */
        public void setCustomerID(java.lang.String param) {
            localCustomerIDTracker = param != null;

            this.localCustomerID = param;
        }

        public boolean isTitleSpecified() {
            return localTitleTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getTitle() {
            return localTitle;
        }

        /**
         * Auto generated setter method
         * @param param Title
         */
        public void setTitle(java.lang.String param) {
            localTitleTracker = param != null;

            this.localTitle = param;
        }

        public boolean isCategorySpecified() {
            return localCategoryTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getCategory() {
            return localCategory;
        }

        /**
         * Auto generated setter method
         * @param param Category
         */
        public void setCategory(java.lang.String param) {
            localCategoryTracker = param != null;

            this.localCategory = param;
        }

        public boolean isInquiryDateTimeSpecified() {
            return localInquiryDateTimeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.util.Calendar
         */
        public java.util.Calendar getInquiryDateTime() {
            return localInquiryDateTime;
        }

        /**
         * Auto generated setter method
         * @param param InquiryDateTime
         */
        public void setInquiryDateTime(java.util.Calendar param) {
            localInquiryDateTimeTracker = param != null;

            this.localInquiryDateTime = param;
        }

        public boolean isInquiryContentSpecified() {
            return localInquiryContentTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getInquiryContent() {
            return localInquiryContent;
        }

        /**
         * Auto generated setter method
         * @param param InquiryContent
         */
        public void setInquiryContent(java.lang.String param) {
            localInquiryContentTracker = param != null;

            this.localInquiryContent = param;
        }

        public boolean isAnswerContentIDSpecified() {
            return localAnswerContentIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAnswerContentID() {
            return localAnswerContentID;
        }

        /**
         * Auto generated setter method
         * @param param AnswerContentID
         */
        public void setAnswerContentID(java.lang.String param) {
            localAnswerContentIDTracker = param != null;

            this.localAnswerContentID = param;
        }

        public boolean isAnswerContentSpecified() {
            return localAnswerContentTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAnswerContent() {
            return localAnswerContent;
        }

        /**
         * Auto generated setter method
         * @param param AnswerContent
         */
        public void setAnswerContent(java.lang.String param) {
            localAnswerContentTracker = param != null;

            this.localAnswerContent = param;
        }

        public boolean isAnswerTempleteIDSpecified() {
            return localAnswerTempleteIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAnswerTempleteID() {
            return localAnswerTempleteID;
        }

        /**
         * Auto generated setter method
         * @param param AnswerTempleteID
         */
        public void setAnswerTempleteID(java.lang.String param) {
            localAnswerTempleteIDTracker = param != null;

            this.localAnswerTempleteID = param;
        }

        public boolean isIsAnsweredSpecified() {
            return localIsAnsweredTracker;
        }

        /**
         * Auto generated getter method
         * @return boolean
         */
        public boolean getIsAnswered() {
            return localIsAnswered;
        }

        /**
         * Auto generated setter method
         * @param param IsAnswered
         */
        public void setIsAnswered(boolean param) {
            // setting primitive attribute tracker to true
            localIsAnsweredTracker = true;

            this.localIsAnswered = param;
        }

        public boolean isCustomerNameSpecified() {
            return localCustomerNameTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getCustomerName() {
            return localCustomerName;
        }

        /**
         * Auto generated setter method
         * @param param CustomerName
         */
        public void setCustomerName(java.lang.String param) {
            localCustomerNameTracker = param != null;

            this.localCustomerName = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://customerinquiry.shopn.platform.nhncorp.com/");

                if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":InquiryType", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "InquiryType", xmlWriter);
                }
            }

            if (localInquiryIDTracker) {
                namespace = "";
                writeStartElement(null, namespace, "InquiryID", xmlWriter);

                if (localInquiryID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "InquiryID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localInquiryID);
                }

                xmlWriter.writeEndElement();
            }

            if (localOrderIDTracker) {
                namespace = "";
                writeStartElement(null, namespace, "OrderID", xmlWriter);

                if (localOrderID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "OrderID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localOrderID);
                }

                xmlWriter.writeEndElement();
            }

            if (localProductOrderIDTracker) {
                namespace = "";
                writeStartElement(null, namespace, "ProductOrderID", xmlWriter);

                if (localProductOrderID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "ProductOrderID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localProductOrderID);
                }

                xmlWriter.writeEndElement();
            }

            if (localProductNameTracker) {
                namespace = "";
                writeStartElement(null, namespace, "ProductName", xmlWriter);

                if (localProductName == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "ProductName cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localProductName);
                }

                xmlWriter.writeEndElement();
            }

            if (localProductIDTracker) {
                namespace = "";
                writeStartElement(null, namespace, "ProductID", xmlWriter);

                if (localProductID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "ProductID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localProductID);
                }

                xmlWriter.writeEndElement();
            }

            if (localProductOrderOptionTracker) {
                namespace = "";
                writeStartElement(null, namespace, "ProductOrderOption",
                    xmlWriter);

                if (localProductOrderOption == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "ProductOrderOption cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localProductOrderOption);
                }

                xmlWriter.writeEndElement();
            }

            if (localCustomerIDTracker) {
                namespace = "";
                writeStartElement(null, namespace, "CustomerID", xmlWriter);

                if (localCustomerID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "CustomerID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localCustomerID);
                }

                xmlWriter.writeEndElement();
            }

            if (localTitleTracker) {
                namespace = "";
                writeStartElement(null, namespace, "Title", xmlWriter);

                if (localTitle == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "Title cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localTitle);
                }

                xmlWriter.writeEndElement();
            }

            if (localCategoryTracker) {
                namespace = "";
                writeStartElement(null, namespace, "Category", xmlWriter);

                if (localCategory == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "Category cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localCategory);
                }

                xmlWriter.writeEndElement();
            }

            if (localInquiryDateTimeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "InquiryDateTime", xmlWriter);

                if (localInquiryDateTime == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "InquiryDateTime cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localInquiryDateTime));
                }

                xmlWriter.writeEndElement();
            }

            if (localInquiryContentTracker) {
                namespace = "";
                writeStartElement(null, namespace, "InquiryContent", xmlWriter);

                if (localInquiryContent == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "InquiryContent cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localInquiryContent);
                }

                xmlWriter.writeEndElement();
            }

            if (localAnswerContentIDTracker) {
                namespace = "";
                writeStartElement(null, namespace, "AnswerContentID", xmlWriter);

                if (localAnswerContentID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "AnswerContentID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAnswerContentID);
                }

                xmlWriter.writeEndElement();
            }

            if (localAnswerContentTracker) {
                namespace = "";
                writeStartElement(null, namespace, "AnswerContent", xmlWriter);

                if (localAnswerContent == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "AnswerContent cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAnswerContent);
                }

                xmlWriter.writeEndElement();
            }

            if (localAnswerTempleteIDTracker) {
                namespace = "";
                writeStartElement(null, namespace, "AnswerTempleteID", xmlWriter);

                if (localAnswerTempleteID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "AnswerTempleteID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAnswerTempleteID);
                }

                xmlWriter.writeEndElement();
            }

            if (localIsAnsweredTracker) {
                namespace = "";
                writeStartElement(null, namespace, "IsAnswered", xmlWriter);

                if (false) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "IsAnswered cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localIsAnswered));
                }

                xmlWriter.writeEndElement();
            }

            if (localCustomerNameTracker) {
                namespace = "";
                writeStartElement(null, namespace, "CustomerName", xmlWriter);

                if (localCustomerName == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "CustomerName cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localCustomerName);
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static InquiryType parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                InquiryType object = new InquiryType();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"InquiryType".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (InquiryType) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "InquiryID").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "InquiryID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setInquiryID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OrderID").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "OrderID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setOrderID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ProductOrderID").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ProductOrderID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setProductOrderID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ProductName").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ProductName" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setProductName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ProductID").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ProductID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setProductID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ProductOrderOption").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ProductOrderOption" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setProductOrderOption(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CustomerID").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "CustomerID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setCustomerID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Title").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Title" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setTitle(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Category").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Category" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setCategory(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "InquiryDateTime").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "InquiryDateTime" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setInquiryDateTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "InquiryContent").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "InquiryContent" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setInquiryContent(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AnswerContentID").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "AnswerContentID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAnswerContentID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AnswerContent").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "AnswerContent" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAnswerContent(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AnswerTempleteID").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "AnswerTempleteID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAnswerTempleteID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IsAnswered").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "IsAnswered" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setIsAnswered(org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CustomerName").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "CustomerName" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setCustomerName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class WarningType implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = WarningType
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         * field for Code
         */
        protected java.lang.String localCode;

        /**
         * field for Message
         */
        protected java.lang.String localMessage;

        /**
         * field for Detail
         */
        protected java.lang.String localDetail;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localDetailTracker = false;

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getCode() {
            return localCode;
        }

        /**
         * Auto generated setter method
         * @param param Code
         */
        public void setCode(java.lang.String param) {
            this.localCode = param;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getMessage() {
            return localMessage;
        }

        /**
         * Auto generated setter method
         * @param param Message
         */
        public void setMessage(java.lang.String param) {
            this.localMessage = param;
        }

        public boolean isDetailSpecified() {
            return localDetailTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getDetail() {
            return localDetail;
        }

        /**
         * Auto generated setter method
         * @param param Detail
         */
        public void setDetail(java.lang.String param) {
            localDetailTracker = param != null;

            this.localDetail = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://customerinquiry.shopn.platform.nhncorp.com/");

                if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":WarningType", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "WarningType", xmlWriter);
                }
            }

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "Code", xmlWriter);

            if (localCode == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Code cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCode);
            }

            xmlWriter.writeEndElement();

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "Message", xmlWriter);

            if (localMessage == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Message cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMessage);
            }

            xmlWriter.writeEndElement();

            if (localDetailTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "Detail", xmlWriter);

                if (localDetail == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "Detail cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localDetail);
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static WarningType parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                WarningType object = new WarningType();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"WarningType".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (WarningType) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Code").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Code" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Message").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Message" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setMessage(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Detail").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Detail" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setDetail(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class BaseResponseType implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = BaseResponseType
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         * field for RequestID
         */
        protected java.lang.String localRequestID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRequestIDTracker = false;

        /**
         * field for ResponseType
         */
        protected java.lang.String localResponseType;

        /**
         * field for ResponseTime
         */
        protected long localResponseTime;

        /**
         * field for Error
         */
        protected ErrorType localError;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localErrorTracker = false;

        /**
         * field for WarningList
         */
        protected WarningListType localWarningList;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localWarningListTracker = false;

        /**
         * field for QuotaStatus
         */
        protected QuotaStatusType localQuotaStatus;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localQuotaStatusTracker = false;

        public boolean isRequestIDSpecified() {
            return localRequestIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRequestID() {
            return localRequestID;
        }

        /**
         * Auto generated setter method
         * @param param RequestID
         */
        public void setRequestID(java.lang.String param) {
            localRequestIDTracker = param != null;

            this.localRequestID = param;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getResponseType() {
            return localResponseType;
        }

        /**
         * Auto generated setter method
         * @param param ResponseType
         */
        public void setResponseType(java.lang.String param) {
            this.localResponseType = param;
        }

        /**
         * Auto generated getter method
         * @return long
         */
        public long getResponseTime() {
            return localResponseTime;
        }

        /**
         * Auto generated setter method
         * @param param ResponseTime
         */
        public void setResponseTime(long param) {
            this.localResponseTime = param;
        }

        public boolean isErrorSpecified() {
            return localErrorTracker;
        }

        /**
         * Auto generated getter method
         * @return ErrorType
         */
        public ErrorType getError() {
            return localError;
        }

        /**
         * Auto generated setter method
         * @param param Error
         */
        public void setError(ErrorType param) {
            localErrorTracker = param != null;

            this.localError = param;
        }

        public boolean isWarningListSpecified() {
            return localWarningListTracker;
        }

        /**
         * Auto generated getter method
         * @return WarningListType
         */
        public WarningListType getWarningList() {
            return localWarningList;
        }

        /**
         * Auto generated setter method
         * @param param WarningList
         */
        public void setWarningList(WarningListType param) {
            localWarningListTracker = param != null;

            this.localWarningList = param;
        }

        public boolean isQuotaStatusSpecified() {
            return localQuotaStatusTracker;
        }

        /**
         * Auto generated getter method
         * @return QuotaStatusType
         */
        public QuotaStatusType getQuotaStatus() {
            return localQuotaStatus;
        }

        /**
         * Auto generated setter method
         * @param param QuotaStatus
         */
        public void setQuotaStatus(QuotaStatusType param) {
            localQuotaStatusTracker = param != null;

            this.localQuotaStatus = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://customerinquiry.shopn.platform.nhncorp.com/");

                if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":BaseResponseType", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "BaseResponseType", xmlWriter);
                }
            }

            if (localRequestIDTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "RequestID", xmlWriter);

                if (localRequestID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "RequestID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRequestID);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "ResponseType", xmlWriter);

            if (localResponseType == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ResponseType cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localResponseType);
            }

            xmlWriter.writeEndElement();

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "ResponseTime", xmlWriter);

            if (localResponseTime == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "ResponseTime cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localResponseTime));
            }

            xmlWriter.writeEndElement();

            if (localErrorTracker) {
                if (localError == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "Error cannot be null!!");
                }

                localError.serialize(new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "Error"), xmlWriter);
            }

            if (localWarningListTracker) {
                if (localWarningList == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "WarningList cannot be null!!");
                }

                localWarningList.serialize(new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "WarningList"), xmlWriter);
            }

            if (localQuotaStatusTracker) {
                if (localQuotaStatus == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "QuotaStatus cannot be null!!");
                }

                localQuotaStatus.serialize(new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "QuotaStatus"), xmlWriter);
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static BaseResponseType parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                BaseResponseType object = null;

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"BaseResponseType".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (BaseResponseType) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }

                            throw new org.apache.axis2.databinding.ADBException(
                                "The an abstract class can not be instantiated !!!");
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "RequestID").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "RequestID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRequestID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "ResponseType").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ResponseType" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setResponseType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "ResponseTime").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ResponseTime" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setResponseTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Error").equals(reader.getName())) {
                        object.setError(ErrorType.Factory.parse(reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "WarningList").equals(reader.getName())) {
                        object.setWarningList(WarningListType.Factory.parse(
                            reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "QuotaStatus").equals(reader.getName())) {
                        object.setQuotaStatus(QuotaStatusType.Factory.parse(
                            reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class BaseCheckoutRequestType extends BaseRequestType
        implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = BaseCheckoutRequestType
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         * field for DetailLevel
         */
        protected java.lang.String localDetailLevel;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localDetailLevelTracker = false;

        /**
         * field for Version
         */
        protected java.lang.String localVersion;

        public boolean isDetailLevelSpecified() {
            return localDetailLevelTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getDetailLevel() {
            return localDetailLevel;
        }

        /**
         * Auto generated setter method
         * @param param DetailLevel
         */
        public void setDetailLevel(java.lang.String param) {
            localDetailLevelTracker = param != null;

            this.localDetailLevel = param;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getVersion() {
            return localVersion;
        }

        /**
         * Auto generated setter method
         * @param param Version
         */
        public void setVersion(java.lang.String param) {
            this.localVersion = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                "http://customerinquiry.shopn.platform.nhncorp.com/");

            if ((namespacePrefix != null) &&
                (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":BaseCheckoutRequestType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "BaseCheckoutRequestType", xmlWriter);
            }

            if (localRequestIDTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "RequestID", xmlWriter);

                if (localRequestID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "RequestID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRequestID);
                }

                xmlWriter.writeEndElement();
            }

            if (localAccessCredentials == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "AccessCredentials cannot be null!!");
            }

            localAccessCredentials.serialize(new javax.xml.namespace.QName(
                "http://customerinquiry.shopn.platform.nhncorp.com/",
                "AccessCredentials"), xmlWriter);

            if (localDetailLevelTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "DetailLevel", xmlWriter);

                if (localDetailLevel == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "DetailLevel cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localDetailLevel);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "Version", xmlWriter);

            if (localVersion == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Version cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVersion);
            }

            xmlWriter.writeEndElement();

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static BaseCheckoutRequestType parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                BaseCheckoutRequestType object = null;

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"BaseCheckoutRequestType".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (BaseCheckoutRequestType) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }

                            throw new org.apache.axis2.databinding.ADBException(
                                "The an abstract class can not be instantiated !!!");
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "RequestID").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "RequestID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRequestID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "AccessCredentials").equals(reader.getName())) {
                        object.setAccessCredentials(AccessCredentialsType.Factory.parse(
                            reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "DetailLevel").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "DetailLevel" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setDetailLevel(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Version").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Version" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setVersion(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class QuotaStatusType implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = QuotaStatusType
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         * field for RemainingQuota
         */
        protected long localRemainingQuota;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRemainingQuotaTracker = false;

        /**
         * field for ExpirationTime
         */
        protected java.util.Calendar localExpirationTime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localExpirationTimeTracker = false;

        public boolean isRemainingQuotaSpecified() {
            return localRemainingQuotaTracker;
        }

        /**
         * Auto generated getter method
         * @return long
         */
        public long getRemainingQuota() {
            return localRemainingQuota;
        }

        /**
         * Auto generated setter method
         * @param param RemainingQuota
         */
        public void setRemainingQuota(long param) {
            // setting primitive attribute tracker to true
            localRemainingQuotaTracker = param != java.lang.Long.MIN_VALUE;

            this.localRemainingQuota = param;
        }

        public boolean isExpirationTimeSpecified() {
            return localExpirationTimeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.util.Calendar
         */
        public java.util.Calendar getExpirationTime() {
            return localExpirationTime;
        }

        /**
         * Auto generated setter method
         * @param param ExpirationTime
         */
        public void setExpirationTime(java.util.Calendar param) {
            localExpirationTimeTracker = param != null;

            this.localExpirationTime = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://customerinquiry.shopn.platform.nhncorp.com/");

                if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":QuotaStatusType", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "QuotaStatusType", xmlWriter);
                }
            }

            if (localRemainingQuotaTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "RemainingQuota", xmlWriter);

                if (localRemainingQuota == java.lang.Long.MIN_VALUE) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "RemainingQuota cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localRemainingQuota));
                }

                xmlWriter.writeEndElement();
            }

            if (localExpirationTimeTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "ExpirationTime", xmlWriter);

                if (localExpirationTime == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "ExpirationTime cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localExpirationTime));
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static QuotaStatusType parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                QuotaStatusType object = new QuotaStatusType();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"QuotaStatusType".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (QuotaStatusType) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "RemainingQuota").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "RemainingQuota" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRemainingQuota(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        object.setRemainingQuota(java.lang.Long.MIN_VALUE);
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "ExpirationTime").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ExpirationTime" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setExpirationTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class ExtensionMapper {
        public static java.lang.Object getTypeObject(
            java.lang.String namespaceURI, java.lang.String typeName,
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) &&
                "GetCustomerInquiryListResponseType".equals(typeName)) {
                return GetCustomerInquiryListResponseType.Factory.parse(reader);
            }

            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) && "WarningListType".equals(typeName)) {
                return WarningListType.Factory.parse(reader);
            }

            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) &&
                "CustomerInquiryList_type0".equals(typeName)) {
                return CustomerInquiryList_type0.Factory.parse(reader);
            }

            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) &&
                "AccessCredentialsType".equals(typeName)) {
                return AccessCredentialsType.Factory.parse(reader);
            }

            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) && "InquiryType".equals(typeName)) {
                return InquiryType.Factory.parse(reader);
            }

            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) && "WarningType".equals(typeName)) {
                return WarningType.Factory.parse(reader);
            }

            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) && "BaseResponseType".equals(typeName)) {
                return BaseResponseType.Factory.parse(reader);
            }

            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) &&
                "BaseCheckoutRequestType".equals(typeName)) {
                return BaseCheckoutRequestType.Factory.parse(reader);
            }

            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) && "QuotaStatusType".equals(typeName)) {
                return QuotaStatusType.Factory.parse(reader);
            }

            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) &&
                "GetCustomerInquiryListRequestType".equals(typeName)) {
                return GetCustomerInquiryListRequestType.Factory.parse(reader);
            }

            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) &&
                "AnswerCustomerInquiryResponseType".equals(typeName)) {
                return AnswerCustomerInquiryResponseType.Factory.parse(reader);
            }

            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) &&
                "BaseCheckoutResponseType".equals(typeName)) {
                return BaseCheckoutResponseType.Factory.parse(reader);
            }

            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) &&
                "AnswerCustomerInquiryRequestType".equals(typeName)) {
                return AnswerCustomerInquiryRequestType.Factory.parse(reader);
            }

            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) && "ErrorType".equals(typeName)) {
                return ErrorType.Factory.parse(reader);
            }

            if ("http://customerinquiry.shopn.platform.nhncorp.com/".equals(
                namespaceURI) && "BaseRequestType".equals(typeName)) {
                return BaseRequestType.Factory.parse(reader);
            }

            throw new org.apache.axis2.databinding.ADBException(
                "Unsupported type " + namespaceURI + " " + typeName);
        }
    }

    public static class AnswerCustomerInquiryResponseType
        extends BaseCheckoutResponseType implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = AnswerCustomerInquiryResponseType
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                "http://customerinquiry.shopn.platform.nhncorp.com/");

            if ((namespacePrefix != null) &&
                (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":AnswerCustomerInquiryResponseType",
                    xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "AnswerCustomerInquiryResponseType", xmlWriter);
            }

            if (localRequestIDTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "RequestID", xmlWriter);

                if (localRequestID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "RequestID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRequestID);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "ResponseType", xmlWriter);

            if (localResponseType == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ResponseType cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localResponseType);
            }

            xmlWriter.writeEndElement();

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "ResponseTime", xmlWriter);

            if (localResponseTime == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "ResponseTime cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localResponseTime));
            }

            xmlWriter.writeEndElement();

            if (localErrorTracker) {
                if (localError == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "Error cannot be null!!");
                }

                localError.serialize(new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "Error"), xmlWriter);
            }

            if (localWarningListTracker) {
                if (localWarningList == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "WarningList cannot be null!!");
                }

                localWarningList.serialize(new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "WarningList"), xmlWriter);
            }

            if (localQuotaStatusTracker) {
                if (localQuotaStatus == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "QuotaStatus cannot be null!!");
                }

                localQuotaStatus.serialize(new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "QuotaStatus"), xmlWriter);
            }

            if (localDetailLevelTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "DetailLevel", xmlWriter);

                if (localDetailLevel == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "DetailLevel cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localDetailLevel);
                }

                xmlWriter.writeEndElement();
            }

            if (localVersionTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "Version", xmlWriter);

                if (localVersion == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "Version cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localVersion);
                }

                xmlWriter.writeEndElement();
            }

            if (localReleaseTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "Release", xmlWriter);

                if (localRelease == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "Release cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRelease);
                }

                xmlWriter.writeEndElement();
            }

            if (localTimestampTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "Timestamp", xmlWriter);

                if (localTimestamp == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "Timestamp cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localTimestamp));
                }

                xmlWriter.writeEndElement();
            }

            if (localMessageIDTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "MessageID", xmlWriter);

                if (localMessageID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "MessageID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localMessageID);
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static AnswerCustomerInquiryResponseType parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                AnswerCustomerInquiryResponseType object = new AnswerCustomerInquiryResponseType();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"AnswerCustomerInquiryResponseType".equals(
                                type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (AnswerCustomerInquiryResponseType) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "RequestID").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "RequestID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRequestID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "ResponseType").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ResponseType" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setResponseType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "ResponseTime").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ResponseTime" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setResponseTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Error").equals(reader.getName())) {
                        object.setError(ErrorType.Factory.parse(reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "WarningList").equals(reader.getName())) {
                        object.setWarningList(WarningListType.Factory.parse(
                            reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "QuotaStatus").equals(reader.getName())) {
                        object.setQuotaStatus(QuotaStatusType.Factory.parse(
                            reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "DetailLevel").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "DetailLevel" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setDetailLevel(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Version").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Version" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setVersion(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Release").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Release" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRelease(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Timestamp").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Timestamp" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setTimestamp(org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "MessageID").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "MessageID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setMessageID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class GetCustomerInquiryListRequestType
        extends BaseCheckoutRequestType implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = GetCustomerInquiryListRequestType
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         * field for ServiceType
         */
        protected java.lang.String localServiceType;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localServiceTypeTracker = false;

        /**
         * field for MallID
         */
        protected java.lang.String localMallID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localMallIDTracker = false;

        /**
         * field for InquiryTimeFrom
         */
        protected java.util.Calendar localInquiryTimeFrom;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localInquiryTimeFromTracker = false;

        /**
         * field for InquiryTimeTo
         */
        protected java.util.Calendar localInquiryTimeTo;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localInquiryTimeToTracker = false;

        /**
         * field for InquiryExtraData
         */
        protected java.lang.String localInquiryExtraData;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localInquiryExtraDataTracker = false;

        /**
         * field for IsAnswered
         */
        protected boolean localIsAnswered;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localIsAnsweredTracker = false;

        public boolean isServiceTypeSpecified() {
            return localServiceTypeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getServiceType() {
            return localServiceType;
        }

        /**
         * Auto generated setter method
         * @param param ServiceType
         */
        public void setServiceType(java.lang.String param) {
            localServiceTypeTracker = param != null;

            this.localServiceType = param;
        }

        public boolean isMallIDSpecified() {
            return localMallIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getMallID() {
            return localMallID;
        }

        /**
         * Auto generated setter method
         * @param param MallID
         */
        public void setMallID(java.lang.String param) {
            localMallIDTracker = param != null;

            this.localMallID = param;
        }

        public boolean isInquiryTimeFromSpecified() {
            return localInquiryTimeFromTracker;
        }

        /**
         * Auto generated getter method
         * @return java.util.Calendar
         */
        public java.util.Calendar getInquiryTimeFrom() {
            return localInquiryTimeFrom;
        }

        /**
         * Auto generated setter method
         * @param param InquiryTimeFrom
         */
        public void setInquiryTimeFrom(java.util.Calendar param) {
            localInquiryTimeFromTracker = param != null;

            this.localInquiryTimeFrom = param;
        }

        public boolean isInquiryTimeToSpecified() {
            return localInquiryTimeToTracker;
        }

        /**
         * Auto generated getter method
         * @return java.util.Calendar
         */
        public java.util.Calendar getInquiryTimeTo() {
            return localInquiryTimeTo;
        }

        /**
         * Auto generated setter method
         * @param param InquiryTimeTo
         */
        public void setInquiryTimeTo(java.util.Calendar param) {
            localInquiryTimeToTracker = param != null;

            this.localInquiryTimeTo = param;
        }

        public boolean isInquiryExtraDataSpecified() {
            return localInquiryExtraDataTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getInquiryExtraData() {
            return localInquiryExtraData;
        }

        /**
         * Auto generated setter method
         * @param param InquiryExtraData
         */
        public void setInquiryExtraData(java.lang.String param) {
            localInquiryExtraDataTracker = param != null;

            this.localInquiryExtraData = param;
        }

        public boolean isIsAnsweredSpecified() {
            return localIsAnsweredTracker;
        }

        /**
         * Auto generated getter method
         * @return boolean
         */
        public boolean getIsAnswered() {
            return localIsAnswered;
        }

        /**
         * Auto generated setter method
         * @param param IsAnswered
         */
        public void setIsAnswered(boolean param) {
            // setting primitive attribute tracker to true
            localIsAnsweredTracker = true;

            this.localIsAnswered = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                "http://customerinquiry.shopn.platform.nhncorp.com/");

            if ((namespacePrefix != null) &&
                (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":GetCustomerInquiryListRequestType",
                    xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "GetCustomerInquiryListRequestType", xmlWriter);
            }

            if (localRequestIDTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "RequestID", xmlWriter);

                if (localRequestID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "RequestID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRequestID);
                }

                xmlWriter.writeEndElement();
            }

            if (localAccessCredentials == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "AccessCredentials cannot be null!!");
            }

            localAccessCredentials.serialize(new javax.xml.namespace.QName(
                "http://customerinquiry.shopn.platform.nhncorp.com/",
                "AccessCredentials"), xmlWriter);

            if (localDetailLevelTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "DetailLevel", xmlWriter);

                if (localDetailLevel == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "DetailLevel cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localDetailLevel);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "Version", xmlWriter);

            if (localVersion == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Version cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVersion);
            }

            xmlWriter.writeEndElement();

            if (localServiceTypeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "ServiceType", xmlWriter);

                if (localServiceType == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "ServiceType cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localServiceType);
                }

                xmlWriter.writeEndElement();
            }

            if (localMallIDTracker) {
                namespace = "";
                writeStartElement(null, namespace, "MallID", xmlWriter);

                if (localMallID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "MallID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localMallID);
                }

                xmlWriter.writeEndElement();
            }

            if (localInquiryTimeFromTracker) {
                namespace = "";
                writeStartElement(null, namespace, "InquiryTimeFrom", xmlWriter);

                if (localInquiryTimeFrom == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "InquiryTimeFrom cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localInquiryTimeFrom));
                }

                xmlWriter.writeEndElement();
            }

            if (localInquiryTimeToTracker) {
                namespace = "";
                writeStartElement(null, namespace, "InquiryTimeTo", xmlWriter);

                if (localInquiryTimeTo == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "InquiryTimeTo cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localInquiryTimeTo));
                }

                xmlWriter.writeEndElement();
            }

            if (localInquiryExtraDataTracker) {
                namespace = "";
                writeStartElement(null, namespace, "InquiryExtraData", xmlWriter);

                if (localInquiryExtraData == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "InquiryExtraData cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localInquiryExtraData);
                }

                xmlWriter.writeEndElement();
            }

            if (localIsAnsweredTracker) {
                namespace = "";
                writeStartElement(null, namespace, "IsAnswered", xmlWriter);

                if (false) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "IsAnswered cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localIsAnswered));
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static GetCustomerInquiryListRequestType parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                GetCustomerInquiryListRequestType object = new GetCustomerInquiryListRequestType();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"GetCustomerInquiryListRequestType".equals(
                                type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (GetCustomerInquiryListRequestType) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "RequestID").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "RequestID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRequestID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "AccessCredentials").equals(reader.getName())) {
                        object.setAccessCredentials(AccessCredentialsType.Factory.parse(
                            reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "DetailLevel").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "DetailLevel" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setDetailLevel(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Version").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Version" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setVersion(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ServiceType").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ServiceType" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setServiceType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MallID").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "MallID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setMallID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "InquiryTimeFrom").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "InquiryTimeFrom" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setInquiryTimeFrom(org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "InquiryTimeTo").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "InquiryTimeTo" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setInquiryTimeTo(org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "InquiryExtraData").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "InquiryExtraData" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setInquiryExtraData(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IsAnswered").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "IsAnswered" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setIsAnswered(org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class AnswerCustomerInquiryResponse implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://customerinquiry.shopn.platform.nhncorp.com/",
            "AnswerCustomerInquiryResponse", "ns1");

        /**
         * field for AnswerCustomerInquiryResponse
         */
        protected AnswerCustomerInquiryResponseType localAnswerCustomerInquiryResponse;

        /**
         * Auto generated getter method
         * @return AnswerCustomerInquiryResponseType
         */
        public AnswerCustomerInquiryResponseType getAnswerCustomerInquiryResponse() {
            return localAnswerCustomerInquiryResponse;
        }

        /**
         * Auto generated setter method
         * @param param AnswerCustomerInquiryResponse
         */
        public void setAnswerCustomerInquiryResponse(
            AnswerCustomerInquiryResponseType param) {
            this.localAnswerCustomerInquiryResponse = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, MY_QNAME));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localAnswerCustomerInquiryResponse == null) {
                java.lang.String namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace,
                    "AnswerCustomerInquiryResponse", xmlWriter);

                // write the nil attribute
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "nil", "1",
                    xmlWriter);
                xmlWriter.writeEndElement();
            } else {
                localAnswerCustomerInquiryResponse.serialize(MY_QNAME, xmlWriter);
            }
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static AnswerCustomerInquiryResponse parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                AnswerCustomerInquiryResponse object = new AnswerCustomerInquiryResponse();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "nil");

                    if ("true".equals(nillableValue) ||
                        "1".equals(nillableValue)) {
                        // Skip the element and report the null value.  It cannot have subelements.
                        while (!reader.isEndElement())
                            reader.next();

                        return object;
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                new javax.xml.namespace.QName(
                                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                                    "AnswerCustomerInquiryResponse").equals(
                                    reader.getName())) {
                                nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                    "nil");

                                if ("true".equals(nillableValue) ||
                                    "1".equals(nillableValue)) {
                                    object.setAnswerCustomerInquiryResponse(null);
                                    reader.next();
                                } else {
                                    object.setAnswerCustomerInquiryResponse(AnswerCustomerInquiryResponseType.Factory.parse(
                                        reader));
                                }
                            } // End of if for expected property start element

                            else {
                                // 3 - A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                        reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class GetCustomerInquiryListResponse implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://customerinquiry.shopn.platform.nhncorp.com/",
            "GetCustomerInquiryListResponse", "ns1");

        /**
         * field for GetCustomerInquiryListResponse
         */
        protected GetCustomerInquiryListResponseType localGetCustomerInquiryListResponse;

        /**
         * Auto generated getter method
         * @return GetCustomerInquiryListResponseType
         */
        public GetCustomerInquiryListResponseType getGetCustomerInquiryListResponse() {
            return localGetCustomerInquiryListResponse;
        }

        /**
         * Auto generated setter method
         * @param param GetCustomerInquiryListResponse
         */
        public void setGetCustomerInquiryListResponse(
            GetCustomerInquiryListResponseType param) {
            this.localGetCustomerInquiryListResponse = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, MY_QNAME));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localGetCustomerInquiryListResponse == null) {
                java.lang.String namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace,
                    "GetCustomerInquiryListResponse", xmlWriter);

                // write the nil attribute
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "nil", "1",
                    xmlWriter);
                xmlWriter.writeEndElement();
            } else {
                localGetCustomerInquiryListResponse.serialize(MY_QNAME,
                    xmlWriter);
            }
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static GetCustomerInquiryListResponse parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                GetCustomerInquiryListResponse object = new GetCustomerInquiryListResponse();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "nil");

                    if ("true".equals(nillableValue) ||
                        "1".equals(nillableValue)) {
                        // Skip the element and report the null value.  It cannot have subelements.
                        while (!reader.isEndElement())
                            reader.next();

                        return object;
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                new javax.xml.namespace.QName(
                                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                                    "GetCustomerInquiryListResponse").equals(
                                    reader.getName())) {
                                nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                    "nil");

                                if ("true".equals(nillableValue) ||
                                    "1".equals(nillableValue)) {
                                    object.setGetCustomerInquiryListResponse(null);
                                    reader.next();
                                } else {
                                    object.setGetCustomerInquiryListResponse(GetCustomerInquiryListResponseType.Factory.parse(
                                        reader));
                                }
                            } // End of if for expected property start element

                            else {
                                // 3 - A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                        reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class BaseCheckoutResponseType extends BaseResponseType
        implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = BaseCheckoutResponseType
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         * field for DetailLevel
         */
        protected java.lang.String localDetailLevel;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localDetailLevelTracker = false;

        /**
         * field for Version
         */
        protected java.lang.String localVersion;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localVersionTracker = false;

        /**
         * field for Release
         */
        protected java.lang.String localRelease;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localReleaseTracker = false;

        /**
         * field for Timestamp
         */
        protected java.util.Calendar localTimestamp;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localTimestampTracker = false;

        /**
         * field for MessageID
         */
        protected java.lang.String localMessageID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localMessageIDTracker = false;

        public boolean isDetailLevelSpecified() {
            return localDetailLevelTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getDetailLevel() {
            return localDetailLevel;
        }

        /**
         * Auto generated setter method
         * @param param DetailLevel
         */
        public void setDetailLevel(java.lang.String param) {
            localDetailLevelTracker = param != null;

            this.localDetailLevel = param;
        }

        public boolean isVersionSpecified() {
            return localVersionTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getVersion() {
            return localVersion;
        }

        /**
         * Auto generated setter method
         * @param param Version
         */
        public void setVersion(java.lang.String param) {
            localVersionTracker = param != null;

            this.localVersion = param;
        }

        public boolean isReleaseSpecified() {
            return localReleaseTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRelease() {
            return localRelease;
        }

        /**
         * Auto generated setter method
         * @param param Release
         */
        public void setRelease(java.lang.String param) {
            localReleaseTracker = param != null;

            this.localRelease = param;
        }

        public boolean isTimestampSpecified() {
            return localTimestampTracker;
        }

        /**
         * Auto generated getter method
         * @return java.util.Calendar
         */
        public java.util.Calendar getTimestamp() {
            return localTimestamp;
        }

        /**
         * Auto generated setter method
         * @param param Timestamp
         */
        public void setTimestamp(java.util.Calendar param) {
            localTimestampTracker = param != null;

            this.localTimestamp = param;
        }

        public boolean isMessageIDSpecified() {
            return localMessageIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getMessageID() {
            return localMessageID;
        }

        /**
         * Auto generated setter method
         * @param param MessageID
         */
        public void setMessageID(java.lang.String param) {
            localMessageIDTracker = param != null;

            this.localMessageID = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                "http://customerinquiry.shopn.platform.nhncorp.com/");

            if ((namespacePrefix != null) &&
                (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":BaseCheckoutResponseType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "BaseCheckoutResponseType", xmlWriter);
            }

            if (localRequestIDTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "RequestID", xmlWriter);

                if (localRequestID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "RequestID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRequestID);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "ResponseType", xmlWriter);

            if (localResponseType == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ResponseType cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localResponseType);
            }

            xmlWriter.writeEndElement();

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "ResponseTime", xmlWriter);

            if (localResponseTime == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "ResponseTime cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localResponseTime));
            }

            xmlWriter.writeEndElement();

            if (localErrorTracker) {
                if (localError == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "Error cannot be null!!");
                }

                localError.serialize(new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "Error"), xmlWriter);
            }

            if (localWarningListTracker) {
                if (localWarningList == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "WarningList cannot be null!!");
                }

                localWarningList.serialize(new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "WarningList"), xmlWriter);
            }

            if (localQuotaStatusTracker) {
                if (localQuotaStatus == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "QuotaStatus cannot be null!!");
                }

                localQuotaStatus.serialize(new javax.xml.namespace.QName(
                    "http://customerinquiry.shopn.platform.nhncorp.com/",
                    "QuotaStatus"), xmlWriter);
            }

            if (localDetailLevelTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "DetailLevel", xmlWriter);

                if (localDetailLevel == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "DetailLevel cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localDetailLevel);
                }

                xmlWriter.writeEndElement();
            }

            if (localVersionTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "Version", xmlWriter);

                if (localVersion == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "Version cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localVersion);
                }

                xmlWriter.writeEndElement();
            }

            if (localReleaseTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "Release", xmlWriter);

                if (localRelease == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "Release cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRelease);
                }

                xmlWriter.writeEndElement();
            }

            if (localTimestampTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "Timestamp", xmlWriter);

                if (localTimestamp == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "Timestamp cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localTimestamp));
                }

                xmlWriter.writeEndElement();
            }

            if (localMessageIDTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "MessageID", xmlWriter);

                if (localMessageID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "MessageID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localMessageID);
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static BaseCheckoutResponseType parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                BaseCheckoutResponseType object = null;

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"BaseCheckoutResponseType".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (BaseCheckoutResponseType) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }

                            throw new org.apache.axis2.databinding.ADBException(
                                "The an abstract class can not be instantiated !!!");
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "RequestID").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "RequestID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRequestID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "ResponseType").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ResponseType" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setResponseType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "ResponseTime").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ResponseTime" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setResponseTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Error").equals(reader.getName())) {
                        object.setError(ErrorType.Factory.parse(reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "WarningList").equals(reader.getName())) {
                        object.setWarningList(WarningListType.Factory.parse(
                            reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "QuotaStatus").equals(reader.getName())) {
                        object.setQuotaStatus(QuotaStatusType.Factory.parse(
                            reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "DetailLevel").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "DetailLevel" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setDetailLevel(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Version").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Version" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setVersion(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Release").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Release" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRelease(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Timestamp").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Timestamp" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setTimestamp(org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "MessageID").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "MessageID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setMessageID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class ErrorType implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = ErrorType
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         * field for Code
         */
        protected java.lang.String localCode;

        /**
         * field for Message
         */
        protected java.lang.String localMessage;

        /**
         * field for Detail
         */
        protected java.lang.String localDetail;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localDetailTracker = false;

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getCode() {
            return localCode;
        }

        /**
         * Auto generated setter method
         * @param param Code
         */
        public void setCode(java.lang.String param) {
            this.localCode = param;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getMessage() {
            return localMessage;
        }

        /**
         * Auto generated setter method
         * @param param Message
         */
        public void setMessage(java.lang.String param) {
            this.localMessage = param;
        }

        public boolean isDetailSpecified() {
            return localDetailTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getDetail() {
            return localDetail;
        }

        /**
         * Auto generated setter method
         * @param param Detail
         */
        public void setDetail(java.lang.String param) {
            localDetailTracker = param != null;

            this.localDetail = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://customerinquiry.shopn.platform.nhncorp.com/");

                if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":ErrorType", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "ErrorType", xmlWriter);
                }
            }

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "Code", xmlWriter);

            if (localCode == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Code cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCode);
            }

            xmlWriter.writeEndElement();

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "Message", xmlWriter);

            if (localMessage == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Message cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMessage);
            }

            xmlWriter.writeEndElement();

            if (localDetailTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "Detail", xmlWriter);

                if (localDetail == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "Detail cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localDetail);
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static ErrorType parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                ErrorType object = new ErrorType();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"ErrorType".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (ErrorType) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Code").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Code" + "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Message").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Message" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setMessage(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Detail").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Detail" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setDetail(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class AnswerCustomerInquiryRequestType
        extends BaseCheckoutRequestType implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = AnswerCustomerInquiryRequestType
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         * field for MallID
         */
        protected java.lang.String localMallID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localMallIDTracker = false;

        /**
         * field for ServiceType
         */
        protected java.lang.String localServiceType;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localServiceTypeTracker = false;

        /**
         * field for InquiryID
         */
        protected java.lang.String localInquiryID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localInquiryIDTracker = false;

        /**
         * field for AnswerContent
         */
        protected java.lang.String localAnswerContent;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAnswerContentTracker = false;

        /**
         * field for AnswerContentID
         */
        protected java.lang.String localAnswerContentID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAnswerContentIDTracker = false;

        /**
         * field for ActionType
         */
        protected java.lang.String localActionType;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localActionTypeTracker = false;

        /**
         * field for AnswerTempleteID
         */
        protected java.lang.String localAnswerTempleteID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAnswerTempleteIDTracker = false;

        public boolean isMallIDSpecified() {
            return localMallIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getMallID() {
            return localMallID;
        }

        /**
         * Auto generated setter method
         * @param param MallID
         */
        public void setMallID(java.lang.String param) {
            localMallIDTracker = param != null;

            this.localMallID = param;
        }

        public boolean isServiceTypeSpecified() {
            return localServiceTypeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getServiceType() {
            return localServiceType;
        }

        /**
         * Auto generated setter method
         * @param param ServiceType
         */
        public void setServiceType(java.lang.String param) {
            localServiceTypeTracker = param != null;

            this.localServiceType = param;
        }

        public boolean isInquiryIDSpecified() {
            return localInquiryIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getInquiryID() {
            return localInquiryID;
        }

        /**
         * Auto generated setter method
         * @param param InquiryID
         */
        public void setInquiryID(java.lang.String param) {
            localInquiryIDTracker = param != null;

            this.localInquiryID = param;
        }

        public boolean isAnswerContentSpecified() {
            return localAnswerContentTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAnswerContent() {
            return localAnswerContent;
        }

        /**
         * Auto generated setter method
         * @param param AnswerContent
         */
        public void setAnswerContent(java.lang.String param) {
            localAnswerContentTracker = param != null;

            this.localAnswerContent = param;
        }

        public boolean isAnswerContentIDSpecified() {
            return localAnswerContentIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAnswerContentID() {
            return localAnswerContentID;
        }

        /**
         * Auto generated setter method
         * @param param AnswerContentID
         */
        public void setAnswerContentID(java.lang.String param) {
            localAnswerContentIDTracker = param != null;

            this.localAnswerContentID = param;
        }

        public boolean isActionTypeSpecified() {
            return localActionTypeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getActionType() {
            return localActionType;
        }

        /**
         * Auto generated setter method
         * @param param ActionType
         */
        public void setActionType(java.lang.String param) {
            localActionTypeTracker = param != null;

            this.localActionType = param;
        }

        public boolean isAnswerTempleteIDSpecified() {
            return localAnswerTempleteIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAnswerTempleteID() {
            return localAnswerTempleteID;
        }

        /**
         * Auto generated setter method
         * @param param AnswerTempleteID
         */
        public void setAnswerTempleteID(java.lang.String param) {
            localAnswerTempleteIDTracker = param != null;

            this.localAnswerTempleteID = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                "http://customerinquiry.shopn.platform.nhncorp.com/");

            if ((namespacePrefix != null) &&
                (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":AnswerCustomerInquiryRequestType",
                    xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "AnswerCustomerInquiryRequestType", xmlWriter);
            }

            if (localRequestIDTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "RequestID", xmlWriter);

                if (localRequestID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "RequestID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRequestID);
                }

                xmlWriter.writeEndElement();
            }

            if (localAccessCredentials == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "AccessCredentials cannot be null!!");
            }

            localAccessCredentials.serialize(new javax.xml.namespace.QName(
                "http://customerinquiry.shopn.platform.nhncorp.com/",
                "AccessCredentials"), xmlWriter);

            if (localDetailLevelTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "DetailLevel", xmlWriter);

                if (localDetailLevel == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "DetailLevel cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localDetailLevel);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
            writeStartElement(null, namespace, "Version", xmlWriter);

            if (localVersion == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Version cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVersion);
            }

            xmlWriter.writeEndElement();

            if (localMallIDTracker) {
                namespace = "";
                writeStartElement(null, namespace, "MallID", xmlWriter);

                if (localMallID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "MallID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localMallID);
                }

                xmlWriter.writeEndElement();
            }

            if (localServiceTypeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "ServiceType", xmlWriter);

                if (localServiceType == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "ServiceType cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localServiceType);
                }

                xmlWriter.writeEndElement();
            }

            if (localInquiryIDTracker) {
                namespace = "";
                writeStartElement(null, namespace, "InquiryID", xmlWriter);

                if (localInquiryID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "InquiryID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localInquiryID);
                }

                xmlWriter.writeEndElement();
            }

            if (localAnswerContentTracker) {
                namespace = "";
                writeStartElement(null, namespace, "AnswerContent", xmlWriter);

                if (localAnswerContent == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "AnswerContent cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAnswerContent);
                }

                xmlWriter.writeEndElement();
            }

            if (localAnswerContentIDTracker) {
                namespace = "";
                writeStartElement(null, namespace, "AnswerContentID", xmlWriter);

                if (localAnswerContentID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "AnswerContentID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAnswerContentID);
                }

                xmlWriter.writeEndElement();
            }

            if (localActionTypeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "ActionType", xmlWriter);

                if (localActionType == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "ActionType cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localActionType);
                }

                xmlWriter.writeEndElement();
            }

            if (localAnswerTempleteIDTracker) {
                namespace = "";
                writeStartElement(null, namespace, "AnswerTempleteID", xmlWriter);

                if (localAnswerTempleteID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "AnswerTempleteID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAnswerTempleteID);
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static AnswerCustomerInquiryRequestType parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                AnswerCustomerInquiryRequestType object = new AnswerCustomerInquiryRequestType();

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"AnswerCustomerInquiryRequestType".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (AnswerCustomerInquiryRequestType) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "RequestID").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "RequestID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRequestID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "AccessCredentials").equals(reader.getName())) {
                        object.setAccessCredentials(AccessCredentialsType.Factory.parse(
                            reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "DetailLevel").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "DetailLevel" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setDetailLevel(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "Version").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "Version" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setVersion(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MallID").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "MallID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setMallID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ServiceType").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ServiceType" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setServiceType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "InquiryID").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "InquiryID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setInquiryID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AnswerContent").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "AnswerContent" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAnswerContent(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AnswerContentID").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "AnswerContentID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAnswerContentID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ActionType").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "ActionType" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setActionType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AnswerTempleteID").equals(
                            reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "AnswerTempleteID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAnswerTempleteID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class BaseRequestType implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = BaseRequestType
           Namespace URI = http://customerinquiry.shopn.platform.nhncorp.com/
           Namespace Prefix = ns1
         */

        /**
         * field for RequestID
         */
        protected java.lang.String localRequestID;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRequestIDTracker = false;

        /**
         * field for AccessCredentials
         */
        protected AccessCredentialsType localAccessCredentials;

        public boolean isRequestIDSpecified() {
            return localRequestIDTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRequestID() {
            return localRequestID;
        }

        /**
         * Auto generated setter method
         * @param param RequestID
         */
        public void setRequestID(java.lang.String param) {
            localRequestIDTracker = param != null;

            this.localRequestID = param;
        }

        /**
         * Auto generated getter method
         * @return AccessCredentialsType
         */
        public AccessCredentialsType getAccessCredentials() {
            return localAccessCredentials;
        }

        /**
         * Auto generated setter method
         * @param param AccessCredentials
         */
        public void setAccessCredentials(AccessCredentialsType param) {
            this.localAccessCredentials = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://customerinquiry.shopn.platform.nhncorp.com/");

                if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":BaseRequestType", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "BaseRequestType", xmlWriter);
                }
            }

            if (localRequestIDTracker) {
                namespace = "http://customerinquiry.shopn.platform.nhncorp.com/";
                writeStartElement(null, namespace, "RequestID", xmlWriter);

                if (localRequestID == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "RequestID cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRequestID);
                }

                xmlWriter.writeEndElement();
            }

            if (localAccessCredentials == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "AccessCredentials cannot be null!!");
            }

            localAccessCredentials.serialize(new javax.xml.namespace.QName(
                "http://customerinquiry.shopn.platform.nhncorp.com/",
                "AccessCredentials"), xmlWriter);

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals(
                "http://customerinquiry.shopn.platform.nhncorp.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace, attName,
                    attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                    namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                    attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static BaseRequestType parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                BaseRequestType object = null;

                int event;
                javax.xml.namespace.QName currentQName = null;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    currentQName = reader.getName();

                    if (reader.getAttributeValue(
                        "http://www.w3.org/2001/XMLSchema-instance",
                        "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                ":") + 1);

                            if (!"BaseRequestType".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                    .getNamespaceURI(nsPrefix);

                                return (BaseRequestType) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }

                            throw new org.apache.axis2.databinding.ADBException(
                                "The an abstract class can not be instantiated !!!");
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "RequestID").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                        if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "RequestID" +
                                    "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRequestID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "http://customerinquiry.shopn.platform.nhncorp.com/",
                            "AccessCredentials").equals(reader.getName())) {
                        object.setAccessCredentials(AccessCredentialsType.Factory.parse(
                            reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // 1 - A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // 2 - A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }
}
