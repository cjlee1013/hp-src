/**
 * SellerServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.9  Built on : Nov 16, 2018 (12:05:37 GMT)
 */
package com.nhncorp.platform.shopn.seller;


/**
 *  SellerServiceCallbackHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class SellerServiceCallbackHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public SellerServiceCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public SellerServiceCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for approveCancelApplication method
     * override this method for handling normal response from approveCancelApplication operation
     */
    public void receiveResultapproveCancelApplication(
        SellerServiceStub.ApproveCancelApplicationResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from approveCancelApplication operation
     */
    public void receiveErrorapproveCancelApplication(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for withholdReturn method
     * override this method for handling normal response from withholdReturn operation
     */
    public void receiveResultwithholdReturn(
        SellerServiceStub.WithholdReturnResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from withholdReturn operation
     */
    public void receiveErrorwithholdReturn(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for placeProductOrder method
     * override this method for handling normal response from placeProductOrder operation
     */
    public void receiveResultplaceProductOrder(
        SellerServiceStub.PlaceProductOrderResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from placeProductOrder operation
     */
    public void receiveErrorplaceProductOrder(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for modifyECouponValidDate method
     * override this method for handling normal response from modifyECouponValidDate operation
     */
    public void receiveResultmodifyECouponValidDate(
        SellerServiceStub.ModifyECouponValidDateResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from modifyECouponValidDate operation
     */
    public void receiveErrormodifyECouponValidDate(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for reDeliveryExchange method
     * override this method for handling normal response from reDeliveryExchange operation
     */
    public void receiveResultreDeliveryExchange(
        SellerServiceStub.ReDeliveryExchangeResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from reDeliveryExchange operation
     */
    public void receiveErrorreDeliveryExchange(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for delayProductOrder method
     * override this method for handling normal response from delayProductOrder operation
     */
    public void receiveResultdelayProductOrder(
        SellerServiceStub.DelayProductOrderResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from delayProductOrder operation
     */
    public void receiveErrordelayProductOrder(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getProductOrderInfoList method
     * override this method for handling normal response from getProductOrderInfoList operation
     */
    public void receiveResultgetProductOrderInfoList(
        SellerServiceStub.GetProductOrderInfoListResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getProductOrderInfoList operation
     */
    public void receiveErrorgetProductOrderInfoList(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for rejectReturn method
     * override this method for handling normal response from rejectReturn operation
     */
    public void receiveResultrejectReturn(
        SellerServiceStub.RejectReturnResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from rejectReturn operation
     */
    public void receiveErrorrejectReturn(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getProductOrderIDList method
     * override this method for handling normal response from getProductOrderIDList operation
     */
    public void receiveResultgetProductOrderIDList(
        SellerServiceStub.GetProductOrderIDListResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getProductOrderIDList operation
     */
    public void receiveErrorgetProductOrderIDList(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getChangedProductOrderList method
     * override this method for handling normal response from getChangedProductOrderList operation
     */
    public void receiveResultgetChangedProductOrderList(
        SellerServiceStub.GetChangedProductOrderListResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getChangedProductOrderList operation
     */
    public void receiveErrorgetChangedProductOrderList(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for useECoupon method
     * override this method for handling normal response from useECoupon operation
     */
    public void receiveResultuseECoupon(
        SellerServiceStub.UseECouponResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from useECoupon operation
     */
    public void receiveErroruseECoupon(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for withholdExchange method
     * override this method for handling normal response from withholdExchange operation
     */
    public void receiveResultwithholdExchange(
        SellerServiceStub.WithholdExchangeResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from withholdExchange operation
     */
    public void receiveErrorwithholdExchange(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for releaseExchangeHold method
     * override this method for handling normal response from releaseExchangeHold operation
     */
    public void receiveResultreleaseExchangeHold(
        SellerServiceStub.ReleaseExchangeHoldResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from releaseExchangeHold operation
     */
    public void receiveErrorreleaseExchangeHold(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for releaseReturnHold method
     * override this method for handling normal response from releaseReturnHold operation
     */
    public void receiveResultreleaseReturnHold(
        SellerServiceStub.ReleaseReturnHoldResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from releaseReturnHold operation
     */
    public void receiveErrorreleaseReturnHold(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for cancelSale method
     * override this method for handling normal response from cancelSale operation
     */
    public void receiveResultcancelSale(
        SellerServiceStub.CancelSaleResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from cancelSale operation
     */
    public void receiveErrorcancelSale(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for shipProductOrder method
     * override this method for handling normal response from shipProductOrder operation
     */
    public void receiveResultshipProductOrder(
        SellerServiceStub.ShipProductOrderResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from shipProductOrder operation
     */
    public void receiveErrorshipProductOrder(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for requestReturn method
     * override this method for handling normal response from requestReturn operation
     */
    public void receiveResultrequestReturn(
        SellerServiceStub.RequestReturnResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from requestReturn operation
     */
    public void receiveErrorrequestReturn(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for approveCollectedExchange method
     * override this method for handling normal response from approveCollectedExchange operation
     */
    public void receiveResultapproveCollectedExchange(
        SellerServiceStub.ApproveCollectedExchangeResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from approveCollectedExchange operation
     */
    public void receiveErrorapproveCollectedExchange(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for rejectExchange method
     * override this method for handling normal response from rejectExchange operation
     */
    public void receiveResultrejectExchange(
        SellerServiceStub.RejectExchangeResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from rejectExchange operation
     */
    public void receiveErrorrejectExchange(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for changeECouponStatus method
     * override this method for handling normal response from changeECouponStatus operation
     */
    public void receiveResultchangeECouponStatus(
        SellerServiceStub.ChangeECouponStatusResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from changeECouponStatus operation
     */
    public void receiveErrorchangeECouponStatus(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for approveReturnApplication method
     * override this method for handling normal response from approveReturnApplication operation
     */
    public void receiveResultapproveReturnApplication(
        SellerServiceStub.ApproveReturnApplicationResponseE result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from approveReturnApplication operation
     */
    public void receiveErrorapproveReturnApplication(java.lang.Exception e) {
    }
}
