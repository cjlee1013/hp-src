/**
 * ProductServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.9  Built on : Nov 16, 2018 (12:05:37 GMT)
 */
package com.nhncorp.platform.shopn;


/**
 *  ProductServiceCallbackHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class ProductServiceCallbackHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public ProductServiceCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public ProductServiceCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for deleteProduct method
     * override this method for handling normal response from deleteProduct operation
     */
    public void receiveResultdeleteProduct(
        com.nhncorp.platform.shopn.ProductServiceStub.DeleteProductResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from deleteProduct operation
     */
    public void receiveErrordeleteProduct(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for manageProduct method
     * override this method for handling normal response from manageProduct operation
     */
    public void receiveResultmanageProduct(
        com.nhncorp.platform.shopn.ProductServiceStub.ManageProductResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from manageProduct operation
     */
    public void receiveErrormanageProduct(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getMobileImageInfo method
     * override this method for handling normal response from getMobileImageInfo operation
     */
    public void receiveResultgetMobileImageInfo(
        com.nhncorp.platform.shopn.ProductServiceStub.GetMobileImageInfoResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getMobileImageInfo operation
     */
    public void receiveErrorgetMobileImageInfo(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for manageSupplementProduct method
     * override this method for handling normal response from manageSupplementProduct operation
     */
    public void receiveResultmanageSupplementProduct(
        com.nhncorp.platform.shopn.ProductServiceStub.ManageSupplementProductResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from manageSupplementProduct operation
     */
    public void receiveErrormanageSupplementProduct(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for manageStandardOption method
     * override this method for handling normal response from manageStandardOption operation
     */
    public void receiveResultmanageStandardOption(
        com.nhncorp.platform.shopn.ProductServiceStub.ManageStandardOptionResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from manageStandardOption operation
     */
    public void receiveErrormanageStandardOption(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getOption method
     * override this method for handling normal response from getOption operation
     */
    public void receiveResultgetOption(
        com.nhncorp.platform.shopn.ProductServiceStub.GetOptionResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getOption operation
     */
    public void receiveErrorgetOption(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getStandardOption method
     * override this method for handling normal response from getStandardOption operation
     */
    public void receiveResultgetStandardOption(
        com.nhncorp.platform.shopn.ProductServiceStub.GetStandardOptionResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getStandardOption operation
     */
    public void receiveErrorgetStandardOption(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getAllOriginAreaList method
     * override this method for handling normal response from getAllOriginAreaList operation
     */
    public void receiveResultgetAllOriginAreaList(
        com.nhncorp.platform.shopn.ProductServiceStub.GetAllOriginAreaListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getAllOriginAreaList operation
     */
    public void receiveErrorgetAllOriginAreaList(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for changeProductSaleStatus method
     * override this method for handling normal response from changeProductSaleStatus operation
     */
    public void receiveResultchangeProductSaleStatus(
        com.nhncorp.platform.shopn.ProductServiceStub.ChangeProductSaleStatusResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from changeProductSaleStatus operation
     */
    public void receiveErrorchangeProductSaleStatus(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getModelList method
     * override this method for handling normal response from getModelList operation
     */
    public void receiveResultgetModelList(
        com.nhncorp.platform.shopn.ProductServiceStub.GetModelListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getModelList operation
     */
    public void receiveErrorgetModelList(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for manageMobileImageInfo method
     * override this method for handling normal response from manageMobileImageInfo operation
     */
    public void receiveResultmanageMobileImageInfo(
        com.nhncorp.platform.shopn.ProductServiceStub.ManageMobileImageInfoResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from manageMobileImageInfo operation
     */
    public void receiveErrormanageMobileImageInfo(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getAttributeValueList method
     * override this method for handling normal response from getAttributeValueList operation
     */
    public void receiveResultgetAttributeValueList(
        com.nhncorp.platform.shopn.ProductServiceStub.GetAttributeValueListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getAttributeValueList operation
     */
    public void receiveErrorgetAttributeValueList(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getCategoryInfo method
     * override this method for handling normal response from getCategoryInfo operation
     */
    public void receiveResultgetCategoryInfo(
        com.nhncorp.platform.shopn.ProductServiceStub.GetCategoryInfoResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getCategoryInfo operation
     */
    public void receiveErrorgetCategoryInfo(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getAttributeList method
     * override this method for handling normal response from getAttributeList operation
     */
    public void receiveResultgetAttributeList(
        com.nhncorp.platform.shopn.ProductServiceStub.GetAttributeListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getAttributeList operation
     */
    public void receiveErrorgetAttributeList(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getSupplementProduct method
     * override this method for handling normal response from getSupplementProduct operation
     */
    public void receiveResultgetSupplementProduct(
        com.nhncorp.platform.shopn.ProductServiceStub.GetSupplementProductResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getSupplementProduct operation
     */
    public void receiveErrorgetSupplementProduct(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getAttributeValueUnitList method
     * override this method for handling normal response from getAttributeValueUnitList operation
     */
    public void receiveResultgetAttributeValueUnitList(
        com.nhncorp.platform.shopn.ProductServiceStub.GetAttributeValueUnitListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getAttributeValueUnitList operation
     */
    public void receiveErrorgetAttributeValueUnitList(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for restoreProduct method
     * override this method for handling normal response from restoreProduct operation
     */
    public void receiveResultrestoreProduct(
        com.nhncorp.platform.shopn.ProductServiceStub.RestoreProductResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from restoreProduct operation
     */
    public void receiveErrorrestoreProduct(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getProduct method
     * override this method for handling normal response from getProduct operation
     */
    public void receiveResultgetProduct(
        com.nhncorp.platform.shopn.ProductServiceStub.GetProductResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getProduct operation
     */
    public void receiveErrorgetProduct(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getAllCategoryList method
     * override this method for handling normal response from getAllCategoryList operation
     */
    public void receiveResultgetAllCategoryList(
        com.nhncorp.platform.shopn.ProductServiceStub.GetAllCategoryListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getAllCategoryList operation
     */
    public void receiveErrorgetAllCategoryList(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getSubOriginAreaList method
     * override this method for handling normal response from getSubOriginAreaList operation
     */
    public void receiveResultgetSubOriginAreaList(
        com.nhncorp.platform.shopn.ProductServiceStub.GetSubOriginAreaListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getSubOriginAreaList operation
     */
    public void receiveErrorgetSubOriginAreaList(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getSubCategoryList method
     * override this method for handling normal response from getSubCategoryList operation
     */
    public void receiveResultgetSubCategoryList(
        com.nhncorp.platform.shopn.ProductServiceStub.GetSubCategoryListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getSubCategoryList operation
     */
    public void receiveErrorgetSubCategoryList(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getOriginAreaList method
     * override this method for handling normal response from getOriginAreaList operation
     */
    public void receiveResultgetOriginAreaList(
        com.nhncorp.platform.shopn.ProductServiceStub.GetOriginAreaListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getOriginAreaList operation
     */
    public void receiveErrorgetOriginAreaList(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for manageOption method
     * override this method for handling normal response from manageOption operation
     */
    public void receiveResultmanageOption(
        com.nhncorp.platform.shopn.ProductServiceStub.ManageOptionResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from manageOption operation
     */
    public void receiveErrormanageOption(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getStandardOptionCategory method
     * override this method for handling normal response from getStandardOptionCategory operation
     */
    public void receiveResultgetStandardOptionCategory(
        com.nhncorp.platform.shopn.ProductServiceStub.GetStandardOptionCategoryResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getStandardOptionCategory operation
     */
    public void receiveErrorgetStandardOptionCategory(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getBrandList method
     * override this method for handling normal response from getBrandList operation
     */
    public void receiveResultgetBrandList(
        com.nhncorp.platform.shopn.ProductServiceStub.GetBrandListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getBrandList operation
     */
    public void receiveErrorgetBrandList(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getProductList method
     * override this method for handling normal response from getProductList operation
     */
    public void receiveResultgetProductList(
        com.nhncorp.platform.shopn.ProductServiceStub.GetProductListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getProductList operation
     */
    public void receiveErrorgetProductList(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getModificationRequiredProductList method
     * override this method for handling normal response from getModificationRequiredProductList operation
     */
    public void receiveResultgetModificationRequiredProductList(
        com.nhncorp.platform.shopn.ProductServiceStub.GetModificationRequiredProductListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getModificationRequiredProductList operation
     */
    public void receiveErrorgetModificationRequiredProductList(
        java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getManufacturerList method
     * override this method for handling normal response from getManufacturerList operation
     */
    public void receiveResultgetManufacturerList(
        com.nhncorp.platform.shopn.ProductServiceStub.GetManufacturerListResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getManufacturerList operation
     */
    public void receiveErrorgetManufacturerList(java.lang.Exception e) {
    }
}
