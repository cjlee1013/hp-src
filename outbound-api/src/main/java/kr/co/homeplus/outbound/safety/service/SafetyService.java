package kr.co.homeplus.outbound.safety.service;

import com.nostech.safen.SafeNo;
import kr.co.homeplus.outbound.safety.config.SafetyConfig;
import kr.co.homeplus.outbound.safety.model.SafetyNo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static kr.co.homeplus.outbound.safety.constants.SafetyConstants.EMPTY_STRING;

@Slf4j
@Service
@RequiredArgsConstructor
public class SafetyService {

    private final SafetyConfig safetyConfig;

    /**
     * 안심번호 발급요청
     * @param safetyNo
     * @return
     */
    public String issueSafetyNo(SafetyNo safetyNo) {

        String result = "";
        SafeNo safeNo = new SafeNo();

        try {
            result = safeNo.SafeNoMap(safetyConfig.getCorpCode(), safetyNo.getPhoneNo(), safetyNo.getPhoneNo(), safetyConfig.getGroupCode(), safetyNo.getExpireDt(), EMPTY_STRING, EMPTY_STRING);
        } catch(Exception e) {
            log.error("safetyNo issue error, safetyNo: {}", safeNo);
        }

        return result.trim();
    }

    /**
     * 안심번호 해지요청
     * @param safetyNo
     * @return
     */
    public String cancelSafetyNo(SafetyNo safetyNo) {
        String result = "";
        SafeNo safeNo = new SafeNo();

        try {
            result = safeNo.SafeNoCan(safetyConfig.getCorpCode(), safetyNo.getIssuePhoneNo(), safetyConfig.getGroupCode());
        } catch (Exception e) {
            log.error("safetyNo cancel error, safetyNo: {}", safeNo);
        }

        return result.trim();
    }
}
