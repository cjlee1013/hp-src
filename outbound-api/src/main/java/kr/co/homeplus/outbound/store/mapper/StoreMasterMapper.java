package kr.co.homeplus.outbound.store.mapper;

import java.util.List;
import kr.co.homeplus.outbound.core.db.annotation.MasterConnection;
import kr.co.homeplus.outbound.store.model.MarketStoreSetDto;

@MasterConnection
public interface StoreMasterMapper {

    int upsertMarketStore(List<MarketStoreSetDto> marketStoreSetDto);

}


