package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 상품 상세정보 Get Entry
 */
@Getter
@Setter
@ToString
@Document(collection = "market_item_spec_hist")
@ApiModel("상품 연동 이력 History")
public class ItemSpecHistSetDto {

	@Id
	private String id;

	private Date expireAt;

	@Indexed
	private String itemNo;
	private String partnerId;

	private Object request;

	private Object response;

	private String spec;

	private String methodTxt;

	private String marketItemHistDt;

}