package kr.co.homeplus.outbound.item.constants;

/**
 * 상품 관련 공통 상수
 */
public class ItemConstants {

    public static final long MAX_STOCK_QTY = 100000;  // 마켓재고수량

    public static final String ITEM_DESC_BOTTOM_IMG = "https://image.homeplus.kr/ir/f35afb9b-2cbf-40e8-be52-90ba4cb64a5e";  // 마켓재고수량
}
