package kr.co.homeplus.outbound.market.eleven.item.service;

import kr.co.homeplus.outbound.market.eleven.item.model.ResultMessageDto;
import kr.co.homeplus.outbound.market.eleven.item.model.ProductGetDto;
import kr.co.homeplus.outbound.market.eleven.item.model.ProductResultDto;
import kr.co.homeplus.outbound.market.eleven.item.model.ProductSetDto;
import kr.co.homeplus.outbound.market.eleven.item.model.StorePriceListDto;

public interface ElevenItemSendService {

    /**
     * 11번가 상품 조회
     * @param marketItemNo
     * @return
     */
    ProductGetDto getItem (String marketItemNo);

    /**
     * 11번가 상품 판매 중지 처리
     * @param marketItemNo
     * @return
     */
    ProductResultDto setItemStop(String marketItemNo);

    /**
     * 11번강 상품 판매 처리
     * @param marketItemNo
     * @return
     */
    ProductResultDto setItemActive(String marketItemNo);

    /**
     * 11번가 상품 기본정보 등록
     * @param productSetDto
     * @param itemNo
     * @return
     */
    ProductResultDto newItemBasicInfo( ProductSetDto productSetDto, String itemNo);

    /**
     * 11번가 상품 기본정보 수정
     * @param productSetDto
     * @param itemNo
     * @return
     */
    ProductResultDto modItemBasicInfo( ProductSetDto productSetDto, String itemNo );

    /**
     * 지점별 가격정보 등록/수정
     * @param storePriceListDto
     * @param itemNo
     * @return
     */
    ResultMessageDto addStorePriceList( StorePriceListDto storePriceListDto, String itemNo);

}

