package kr.co.homeplus.outbound.category.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kr.co.homeplus.outbound.category.service.CategoryService;
import kr.co.homeplus.outbound.response.ResponseResult;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "마켓연동 카테고리관리")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found")
    ,   @ApiResponse(code = 405, message = "Method Not Allowed")
    ,   @ApiResponse(code = 422, message = "Unprocessable Entity")
    ,   @ApiResponse(code = 500, message = "Internal Server Error")
})
@RequestMapping("/category")
@RestController
@RequiredArgsConstructor
@Slf4j
public class CategoryController {

    private final CategoryService categoryService;

    @ApiOperation(value = "마켓연동 카테고리 수집", response = ResponseResult.class)
    @GetMapping(value = "/setCategory")
    public ResponseObject setCategory(
        @RequestParam @ApiParam(name = "partnerId", value = "마켓연동 판매업체 ID", required = true) String partnerId) throws Exception {
        return ResourceConverter.toResponseObject(categoryService.setCategory(partnerId));
    }

}
