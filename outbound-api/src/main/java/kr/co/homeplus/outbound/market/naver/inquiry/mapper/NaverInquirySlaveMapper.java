package kr.co.homeplus.outbound.market.naver.inquiry.mapper;

import kr.co.homeplus.outbound.core.db.annotation.SlaveConnection;
import kr.co.homeplus.outbound.market.naver.inquiry.model.NaverInquiryGetDto;

@SlaveConnection
public interface NaverInquirySlaveMapper {
  NaverInquiryGetDto selectSellerQnaOrderInfo(NaverInquiryGetDto naverInquiryGetDto);
}
