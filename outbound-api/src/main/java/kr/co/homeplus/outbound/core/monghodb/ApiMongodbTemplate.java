package kr.co.homeplus.outbound.core.monghodb;


import java.util.List;

import kr.co.homeplus.outbound.constants.ExceptionCode;
import kr.co.homeplus.outbound.core.exception.handler.BusinessLogicException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

/**
 * MongoDB
 */
@Slf4j
@Repository
@RequiredArgsConstructor
public class ApiMongodbTemplate {
    private final MongoTemplate mongoTemplate;

    public Boolean getExists(Query query, Class<?> entityClass) {
        try {
            return mongoTemplate.exists(query, entityClass);
        } catch (Exception e) {
            return null;
        }
    }

    public <T> List<T> getFindQuery(Query query, Class<T> entityClass) throws BusinessLogicException {
        try {
            return mongoTemplate.find(query, entityClass);
        } catch (Exception e) {
            throw new BusinessLogicException(ExceptionCode.SYS_ERROR_CODE_9008, e.getLocalizedMessage());
        }
    }

    public <T> T getFindById(Object id, Class<T> entityClass) throws BusinessLogicException {
        try {
            return mongoTemplate.findById(id, entityClass);
        } catch (Exception e) {
            throw new BusinessLogicException(ExceptionCode.SYS_ERROR_CODE_9008, e.getLocalizedMessage());
        }
    }

    public void addObject(Object objectToSave) throws BusinessLogicException {
        try {
            mongoTemplate.insert(objectToSave);
        } catch (Exception e) {
            throw new BusinessLogicException(ExceptionCode.SYS_ERROR_CODE_9008, e.getLocalizedMessage());
        }
    }

    public void saveObject(Object objectToSave) throws BusinessLogicException {
        try {
            mongoTemplate.save(objectToSave);
        } catch (Exception e) {
            throw new BusinessLogicException(ExceptionCode.SYS_ERROR_CODE_9008, e.getLocalizedMessage());
        }
    }
}
