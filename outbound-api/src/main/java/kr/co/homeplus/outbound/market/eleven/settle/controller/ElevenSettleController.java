package kr.co.homeplus.outbound.market.eleven.settle.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.outbound.market.eleven.settle.model.ElevenSettleItemGetDto;
import kr.co.homeplus.outbound.market.eleven.settle.service.ElevenSettleService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "마켓연동 > 11번가")
@Slf4j
@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class ElevenSettleController {

    private final ElevenSettleService elevenSettleService;

    @ApiOperation(value = "11번가 정산데이터 수집")
    @GetMapping("/eleven/getSettleList")
    public ResponseObject<List<ElevenSettleItemGetDto>> getSettleList(String startTime, String endTime) throws Exception {
        return ResourceConverter.toResponseObject(
            elevenSettleService.getSettleList(startTime,endTime));
    }

}
