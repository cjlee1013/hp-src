package kr.co.homeplus.outbound.market.eleven.shipping.model;

import java.io.Serializable;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType( XmlAccessType.FIELD )
@XmlRootElement( name = "ResultOrder" )
@Data
public class ElevenOrderCommonGetDto {
    @XmlElement( name = "result_code")
    private String resultCode;

    @XmlElement( name = "result_text")
    private String resultMessage;

}
