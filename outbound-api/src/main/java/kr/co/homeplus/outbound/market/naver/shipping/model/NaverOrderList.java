package kr.co.homeplus.outbound.market.naver.shipping.model;

import java.util.List;
import kr.co.homeplus.outbound.market.naver.model.Order;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NaverOrderList {
    private boolean hasMoreData;
    private String fromDt;
    private List<Order> orderList;
}
