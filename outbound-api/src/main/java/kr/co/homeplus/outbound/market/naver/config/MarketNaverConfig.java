package kr.co.homeplus.outbound.market.naver.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
@Setter
@ConfigurationProperties(prefix = "market.naver")
public class MarketNaverConfig {
    private String sellerId;
    private String accessLicense;
    private String secretKey;
    private String targetEndpoint;
    private String copNo;
    private String detailLevel;
    private Order order = new Order();
    private Inquiry inquiry = new Inquiry();
    private Question question = new Question();
    private Shipping shipping = new Shipping();
    private Apis apis = new Apis();

    @Getter
    @Setter
    public class Order {
        private String serviceName;
        private String serviceVersion;
    }

    @Getter
    @Setter
    public class Inquiry {
        private String serviceName;
        private String serviceVersion;
        private String serviceType;
    }

    @Getter
    @Setter
    public class Question {
        private String serviceName;
        private String serviceVersion;
    }

    @Getter
    @Setter
    public class Shipping {
        private String deliveryCompanyCode;
    }

    @Getter
    @Setter
    public class Apis {
        private String host;
        private String hmacKey;
        private String pamphletEventPath;
        private String apiKey;
        private String settlementPath;
    }
}
