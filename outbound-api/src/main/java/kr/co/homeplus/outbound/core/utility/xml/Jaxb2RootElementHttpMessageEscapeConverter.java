package kr.co.homeplus.outbound.core.utility.xml;

import com.sun.xml.bind.marshaller.CharacterEscapeHandler;
import java.io.IOException;
import javax.xml.bind.JAXBException;
import javax.xml.bind.MarshalException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.util.ClassUtils;

@Slf4j
public class Jaxb2RootElementHttpMessageEscapeConverter extends Jaxb2RootElementHttpMessageConverter {

    private CharacterEscapeHandler characterEscapeHandler = null ;

    @Override
    protected void writeToResult( Object o, HttpHeaders headers, Result result ) throws IOException {
        try {

            Class<?> clazz = ClassUtils.getUserClass(o);
            Marshaller marshaller = createMarshaller(clazz);
            setCharset(headers.getContentType(), marshaller);

            setCharset( headers.getContentType(), marshaller );
            marshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );

            if ( null != characterEscapeHandler ) {
                marshaller.setProperty( CharacterEscapeHandler.class.getName(), characterEscapeHandler );
            }

            marshaller.marshal( o, result );
            logger.debug( " \n- + Request XML - \n" +   ( ( StreamResult ) result ).getOutputStream().toString() );

        } catch ( MarshalException ex ) {
            throw new HttpMessageNotWritableException( "Could not marshal [" + o + "]: " + ex.getMessage(), ex );
        } catch ( JAXBException ex ) {
            throw new HttpMessageConversionException( "Could not instantiate JAXBContext: " + ex.getMessage(), ex );
        }
    }

    private void setCharset( MediaType contentType, Marshaller marshaller ) throws PropertyException {
        if ( contentType != null && contentType.getCharset() != null ) {
            marshaller.setProperty( Marshaller.JAXB_ENCODING, contentType.getCharset().toString() );
        }
    }
    public void setCharacterEscapeHandler( CharacterEscapeHandler characterEscapeHandler ) {
        this.characterEscapeHandler = characterEscapeHandler;
    }

}
