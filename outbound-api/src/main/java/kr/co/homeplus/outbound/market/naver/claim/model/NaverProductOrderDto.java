package kr.co.homeplus.outbound.market.naver.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NaverProductOrderDto {
    @ApiModelProperty(value= "상품주문번호")
    private String productOrderId;
    @ApiModelProperty(value= "상품주문상태")
    private String productOrderStatus;
    @ApiModelProperty(value= "클레임타입코드")
    private String claimType;
    @ApiModelProperty(value= "클레임처리상태코드")
    private String claimStatus;
    @ApiModelProperty(value= "상품번호")
    private String productId;
    @ApiModelProperty(value= "상품명")
    private String productName;
    @ApiModelProperty(value= "상품종류(일반")
    private String productClass;
    @ApiModelProperty(value= "상품옵션(옵션명)")
    private String productOption;
    @ApiModelProperty(value= "수량")
    private int quantity;
    @ApiModelProperty(value= "상품가격")
    private int unitPrice;
    @ApiModelProperty(value= "옵션금액")
    private int optionPrice;
    @ApiModelProperty(value= "옵션코드")
    private String optionCode;
    @ApiModelProperty(value= "상품주문금액(할인적용전금액)")
    private int totalProductAmount;
    @ApiModelProperty(value= "상품별할인액(즉시할인+상품할인쿠폰+복수구매할인)")
    private int productDiscountAmount;
    @ApiModelProperty(value= "상품별즉시할인금액")
    private int productImediateDiscountAmount;
    @ApiModelProperty(value= "상품별상품할인쿠폰금액")
    private int productProductDiscountAmount;
    @ApiModelProperty(value= "상품별복수구매할인금액")
    private int productMultiplePurchaseDiscountAmount;
    @ApiModelProperty(value= "총결제금액(할인적용후금액)")
    private int totalPaymentAmount;
    @ApiModelProperty(value= "판매자상품코드")
    private String sellerProductCode;
    @ApiModelProperty(value= "가맹점아이디")
    private String mallId;
    @ApiModelProperty(value= "배송지주소")
    private NaverAddressDto shippingAddress;
    @ApiModelProperty(value= "구매자선택배송방법")
    private String expectedDeliveryMethod;
    @ApiModelProperty(value= "묶음배송번호")
    private String packageNumber;
    @ApiModelProperty(value= "배송비형태(선불")
    private String shippingFeeType;
    @ApiModelProperty(value= "배송비정책(조건별무료등)")
    private String deliveryPolicyType;
    @ApiModelProperty(value= "배송비합계")
    private int deliveryFeeAmount;
    @ApiModelProperty(value= "지역별추가배송비")
    private int sectionDeliveryFee;
    @ApiModelProperty(value= "배송비최종할인액")
    private int deliveryDiscountAmount;
    @ApiModelProperty(value= "배송메모")
    private String shippingMemo;
    @ApiModelProperty(value= "출고지주소")
    private NaverAddressDto takingAddress;
    @ApiModelProperty(value= "발송기한")
    private String shippingDueDate;
    @ApiModelProperty(value= "구매확정일")
    private String decisionDate;
    @ApiModelProperty(value= "사은품")
    private String freeGift;
    @ApiModelProperty(value= "발주상태코드")
    private String placeOrderStatus;
    @ApiModelProperty(value= "발주확인일")
    private String placeOrderDate;
    @ApiModelProperty(value= "발송지연사유코드")
    private String delayedDispatchReason;
    @ApiModelProperty(value= "발송지연상세사유")
    private String delayedDispatchDetailedReason;
    @ApiModelProperty(value= "판매자부담할인액")
    private int sellerBurdenDiscountAmount;
    @ApiModelProperty(value= "판매자부담즉시할인금액")
    private int sellerBurdenImediateDiscountAmount;
    @ApiModelProperty(value= "판매자부담상품할인쿠폰금액")
    private int sellerBurdenProductDiscountAmount;
    @ApiModelProperty(value= "판매자부담복수구매할인금액")
    private int sellerBurdenMultiplePurchaseDiscountAmount;
    @ApiModelProperty(value= "수수료과금구분(결제수수료,(구)판매수수료,채널수수료)")
    private String commissionRatingType;
    @ApiModelProperty(value= "수수료결제방식코드")
    private String commissionPrePayStatus;
    @ApiModelProperty(value= "결제수수료")
    private int paymentCommission;
    @ApiModelProperty(value= "(구)판매수수료")
    private int saleCommission;
    @ApiModelProperty(value= "채널수수료")
    private int channelCommission;
    @ApiModelProperty(value= "네이버쇼핑매출연동수수료")
    private int knowledgeShoppingSellingInterlockCommission;
    @ApiModelProperty(value= "정산예정금액")
    private String expectedSettlementAmount;
    @ApiModelProperty(value= "유입경로(검색광고,공동구매,밴드,네이버쇼핑,네이버쇼핑외)")
    private String inflowPath;
    @ApiModelProperty(value= "자동생성된아이템번호")
    private String itemNo;
    @ApiModelProperty(value= "옵션관리코드")
    private String optionManageCode;
    @ApiModelProperty(value= "주문등록번호")
    private String purchaserSocialSecurityNo;
    @ApiModelProperty(value= "판매자내부코드1")
    private String sellerCustomCode1;
    @ApiModelProperty(value= "판매자내부코드2")
    private String sellerCustomCode2;
    @ApiModelProperty(value= "클레임번호")
    private String claimId;
    @ApiModelProperty(value= "개인통관고유부호")
    private String individualCustomUniqueCode;
    @ApiModelProperty(value= "선물수신상태코드")
    private String giftReceivingStatus;
    @ApiModelProperty(value= "판매자부담스토어할인금액")
    private int sellerBurdenStoreDiscountAmount;
    private String sellerBurdenMultiplePurchaseDiscountType;    //판매자부담복수구매할인타입(QUANTITY:수량별할인,IGNORE_QUANTITY:수량무시할인)
    private String branchId;
    private boolean isAcceptAlternativeProduct;
    private String deliveryAttributeType;                       //배송속성타입코드(NORMAL,TODAY,HOPE,TODAY_ARRIVAL,DAWN_ARRIVAL)
    private String deliveryOperationDate;
    private String deliveryStartTime;
    private String deliveryEndTime;
    private String slotId;
    private String branchBenefitType;                           //지점혜택타입코드(PLUS_1:N+1혜택)
    private int branchBenefitValue;

}
