package kr.co.homeplus.outbound.market.eleven.inquiry.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryAlimiAnswerResultDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryAlimiAnswerSetDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryAlimiSetDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryAnswerResultDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryAnswerSetDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryGetDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquirySetDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.service.ElevenInquiryService;
import kr.co.homeplus.outbound.market.naver.util.DateUtils;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "마켓연동 > 11번가 > 문의")
@Slf4j
@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class ElevenInquiryController {

    private final ElevenInquiryService elevenInquiryService;

    @ApiOperation(value = "문의조회")
    @GetMapping("/eleven/getInquiryList")
    public ResponseObject<List<ElevenInquiryGetDto>> getInquiryList(
        @RequestParam( required = false ) @ApiParam( value = "검색 시작 일자(yyyyMMdd)" ) @DateTimeFormat( iso = ISO.DATE ) final Date StartDate,
        @RequestParam( required = false ) @ApiParam( value = "검색 종료 일자(yyyyMMdd)" ) @DateTimeFormat( iso = ISO.DATE ) final Date EndDate
    ) throws Exception {
        ElevenInquirySetDto paramDto = new ElevenInquirySetDto();
        String timePattern = "yyyyMMdd";
        String failMsg = "";
        String failCode = "-1";

        // 조회할 판매자 문의 등록 시간을 설정한다. 설정한 시간이 없을 경우 default 시간값을 설정한다.
        if ( null == StartDate || null == EndDate ) {
            Calendar cal = Calendar.getInstance();
            paramDto.setEndTime( DateFormatUtils.format( cal, timePattern ) ); //하루 전
            paramDto.setStartTime( DateFormatUtils.format( cal, timePattern ) ); //오늘
        } else {
            if(7 < DateUtils.daysBetween(StartDate, EndDate)) {
                failMsg = "조회 기간은 최대 1주 입니다. \n조회기간(From) : "+ StartDate +"\n조회기간(To) : "+ EndDate;
                log.error(failMsg);
                return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, failCode, failMsg);
            }
            paramDto.setStartTime( DateFormatUtils.format ( StartDate, timePattern ) );
            paramDto.setEndTime( DateFormatUtils.format( EndDate, timePattern ) );
        }

        return ResourceConverter.toResponseObject(elevenInquiryService.getInquiryList(paramDto));
    }

    @ApiOperation(value = "문의답변")
    @PostMapping("/eleven/setInquiryAnswer")
    public ResponseObject<ElevenInquiryAnswerResultDto> setInquiryAnswer(@RequestBody @Valid ElevenInquiryAnswerSetDto ElevenInquiryAnswerSetDto){
        return elevenInquiryService.setInquiryAnswer(ElevenInquiryAnswerSetDto);
    }

    @ApiOperation(value = "긴급알리미 조회")
    @GetMapping("/eleven/getInquiryAlimiList")
    public ResponseObject<List<ElevenInquiryGetDto>>getInquiryAlimiList(
        @RequestParam( required = false ) @ApiParam( value = "검색 시작 일자(yyyyMMdd)" ) @DateTimeFormat( iso = ISO.DATE ) final Date StartDate,
        @RequestParam( required = false ) @ApiParam( value = "검색 종료 일자(yyyyMMdd)" ) @DateTimeFormat( iso = ISO.DATE ) final Date EndDate
    ) throws Exception {
        ElevenInquiryAlimiSetDto paramDto = new ElevenInquiryAlimiSetDto();
        String timePattern = "yyyyMMdd";
        String failMsg = "";
        String failCode = "-1";

        // 조회할 긴급알리미 등록 시간을 설정한다. 설정한 시간이 없을 경우 default 시간값을 설정한다.
        if ( null == StartDate || null == EndDate ) {
            Calendar cal = Calendar.getInstance();
            paramDto.setEndTime( DateFormatUtils.format( cal, timePattern ) ); //오늘
            cal.add( Calendar.HOUR, -12 );
            paramDto.setStartTime( DateFormatUtils.format( cal, timePattern ) ); //하루 전
        } else {
            if(5 < DateUtils.daysBetween(StartDate, EndDate)) {
                failMsg = "조회 기간은 최대 5일입니다. \n조회기간(From) : "+ StartDate +"\n조회기간(To) : "+ EndDate;
                log.error(failMsg);
                return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, failCode, failMsg);
            }
            paramDto.setStartTime( DateFormatUtils.format( StartDate, timePattern ) );
            paramDto.setEndTime( DateFormatUtils.format( EndDate, timePattern ) );
        }

        return ResourceConverter.toResponseObject(elevenInquiryService.getInquiryAlimiList(paramDto));
    }

    @ApiOperation(value = "긴급알리미 답변")
    @PostMapping("/eleven/setInquiryAlimiAnswer")
    public ResponseObject<ElevenInquiryAlimiAnswerResultDto> setInquiryAlimiAnswer(@RequestBody @Valid ElevenInquiryAlimiAnswerSetDto elevenInquiryAlimiAnswerSetDto){
        return elevenInquiryService.setInquiryAlimiAnswer(elevenInquiryAlimiAnswerSetDto);
    }
}
