package kr.co.homeplus.outbound.cert.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 안전인증 정보
 */
@Data
public class ItemSafeCertGetDto {

	@ApiModelProperty(value = "결과코드")
	private String resultCode;

	@ApiModelProperty(value = "결과메시지")
	private String resultMsg;

	@ApiModelProperty(value = "결과")
	private ItemSafeCertDetailGetDto resultData;
}
