package kr.co.homeplus.outbound.market.eleven.item.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.outbound.market.eleven.item.model.ItemSendParamDto;
import kr.co.homeplus.outbound.market.eleven.item.service.ElevenItemService;
import kr.co.homeplus.outbound.response.ResponseResult;
import kr.co.homeplus.outbound.store.model.StoreGetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@Api(tags = "마켓연동 상품관리 (11번가)")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found")
    ,   @ApiResponse(code = 405, message = "Method Not Allowed")
    ,   @ApiResponse(code = 422, message = "Unprocessable Entity")
    ,   @ApiResponse(code = 500, message = "Internal Server Error")
})
@RequestMapping("/item")
@RestController
@RequiredArgsConstructor
@Slf4j
public class ElevenItemController {

    private final ElevenItemService elevenItemService;

    @ApiOperation(value = "11번가 상품 조회", response = ResponseResult.class)
    @GetMapping(value = "/11st/getItem")
    public ResponseObject getItem(
        @RequestParam @ApiParam(name = "marketItemNo", value = "11번가상품번호", required = true) final String marketItemNo) throws Exception {
        return ResourceConverter.toResponseObject(elevenItemService.getItem(marketItemNo));
    }

    @ApiOperation(value = "11번가 상품등록/수정", response = ResponseResult.class)
    @PostMapping(value = "/11st/sendItem")
    public ResponseObject sendItem(@RequestBody ItemSendParamDto itemSendParamDto) throws Exception {
        return ResourceConverter.toResponseObject(elevenItemService.sendItem(itemSendParamDto.getItemList()));
    }

    @ApiOperation(value = "11번가 상품등록/수정(Swagger 전송 용도)", response = ResponseResult.class)
    @PostMapping(value = "/11st/sendItemForDirect")
    public ResponseObject sendItemForSwagger(@RequestBody ItemSendParamDto itemSendParamDto) throws Exception {
        return ResourceConverter.toResponseObject(elevenItemService.sendItemForDirect(itemSendParamDto.getItemList()));
    }

    @ApiOperation(value = "11번가 강제 판매중지", response = ResponseResult.class)
    @GetMapping(value = "/11st/setForceStopItem")
    public ResponseObject setForceStopItem(
        @RequestParam @ApiParam(name = "itemNo", value = "상품번호", required = true) String itemNo) throws Exception {
        return ResourceConverter.toResponseObject(elevenItemService.setForceStopItem(itemNo));
    }

    @ApiOperation(value = "11번가 강제 판매중지해제", response = ResponseResult.class)
    @GetMapping(value = "/11st/setForceActiveItem")
    public ResponseObject setForceActiveItem(
        @RequestParam @ApiParam(name = "itemNo", value = "상품번호", required = true) String itemNo) throws Exception {
        return ResourceConverter.toResponseObject(elevenItemService.setForceActiveItem(itemNo));
    }
    
}
