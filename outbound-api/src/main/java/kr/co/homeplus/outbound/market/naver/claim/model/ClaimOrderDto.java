package kr.co.homeplus.outbound.market.naver.claim.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClaimOrderDto {
    private String orderDate;
    private String orderId;
    private String ordererId;
    private String ordererName;
    private String ordererTel1;
    private String ordererTel2;
    private String paymentDate;
    private String paymentMeans;
    private String paymentDueDate;
    private int orderDiscountAmount;
    private int generalPaymentAmount;
    private int naverMileagePaymentAmount;
    private int chargeAmountPaymentAmount;
    private int checkoutAccumulationPaymentAmount;
    private int payLaterPaymentAmount;
    private String isDeliveryMemoParticularInput;
    private String payLocationType;
    private String ordererNo;
}
