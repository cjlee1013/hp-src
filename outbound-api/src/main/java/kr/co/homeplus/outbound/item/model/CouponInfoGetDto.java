package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("쿠폰(즉시할인) 정보 조회 시 사용")
public class CouponInfoGetDto {
    @ApiModelProperty(value = "쿠폰번호")
    private Long couponNo;

    @ApiModelProperty(value = "할인타입(1:정률 ,2:정액)")
    private String discountType;

    @ApiModelProperty(value = "할인액")
    private Integer discountPrice;

    @ApiModelProperty(value = "할인율")
    private Integer discountRate;

    @ApiModelProperty(value = "쿠폰적용 최소구매금액")
    private Integer discountMax;

    @ApiModelProperty(value = "즉시할인 종료일")
    private String couponEndDt;

    @ApiModelProperty(value = "club 프로모션 행사여부")
    private String clubCouponYn;

    @ApiModelProperty(value = "쿠폰번호")
    private Long clubCouponNo;

    @ApiModelProperty(value = "club 할인타입(1:정률 ,2:정액)")
    private String clubDiscountType;

    @ApiModelProperty(value = "club 할인액")
    private Integer clubDiscountPrice;

    @ApiModelProperty(value = "club 할인율")
    private Integer clubDiscountRate;

    @ApiModelProperty(value = "club 쿠폰적용 최소구매금액")
    private Integer clubDiscountMax;

}
