package kr.co.homeplus.outbound.ticket.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import static kr.co.homeplus.outbound.core.constants.ClaimTicketConstants.*;

@ApiModel(description = "티켓 취소 API 응답 객체")
@Getter
@XmlRootElement(namespace= XML_ROOT_NAME_SPACE, name= CLAIM_TICKET_XML_ROOT_NAME)
@XmlAccessorType(XmlAccessType.FIELD)
public class ClaimTicketResponse {

    @ApiModelProperty(value = "취소리턴코드")
    @XmlElement(name="RESULTCODE", namespace= XML_ROOT_NAME_SPACE)
    private String resultCode;

    @ApiModelProperty(value = "취소리턴메시지")
    @XmlElement(name="RESULTMSG", namespace= XML_ROOT_NAME_SPACE)
    private String resultMsg;

    @ApiModelProperty(value = "INDATA")
    @XmlElement(name=CLAIM_TICKET_XML_IN_DATA_NAME, namespace= XML_ROOT_NAME_SPACE)
    private InData inData;

    @ApiModel(description = "티켓 취소 API 응답 객체")
    @Getter
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class InData {
        @ApiModelProperty(value = "쿠폰 번호")
        @XmlElement(name = "COUPONNUM", namespace = XML_ROOT_NAME_SPACE)
        private String couponNumber;
    }
}
