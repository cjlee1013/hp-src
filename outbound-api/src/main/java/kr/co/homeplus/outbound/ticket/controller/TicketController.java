package kr.co.homeplus.outbound.ticket.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import kr.co.homeplus.outbound.ticket.model.ClaimCoopTicketCheckSetDto;
import kr.co.homeplus.outbound.ticket.model.TicketClaimInfo;
import kr.co.homeplus.outbound.ticket.model.TicketIssueInfo;
import kr.co.homeplus.outbound.ticket.service.CoopApiConnectCancelService;
import kr.co.homeplus.outbound.ticket.service.CoopApiConnectService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "[coop] 티켓관리")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found")
    ,   @ApiResponse(code = 405, message = "Method Not Allowed")
    ,   @ApiResponse(code = 422, message = "Unprocessable Entity")
    ,   @ApiResponse(code = 500, message = "Internal Server Error")
})
@Slf4j
@RequestMapping("/coop")
@RestController
@RequiredArgsConstructor
public class TicketController {

    /**
     * 쿠프 티켓 발급
     */
    private final CoopApiConnectService coopApiConnectService;
    /**
     * 쿠픈 티켓 취소
     */
    private final CoopApiConnectCancelService coopApiConnectCancelService;

    /**
     * [coop] 티켓 발급요청
     * - 문의 : 모바일주문개발TF
     */
    @ApiOperation(value = "티켓 발급요청", response = String.class)
    @PostMapping(value = "/createTicket")
    public ResponseObject<String> createTicket(@RequestBody TicketIssueInfo ticketIssueInfo) throws Exception {
        return ResourceConverter.toResponseObject(coopApiConnectService.createTicket(ticketIssueInfo));
    }

    /**
     * [coop] 티켓 발송요청 (비암호화)
     * - 문의 : 모바일주문개발TF
     */
    @ApiOperation(value = "티켓 발송요청", response = String.class)
    @PostMapping(value = "/sendTicket")
    public ResponseObject<String> sendTicket(@RequestBody TicketIssueInfo ticketIssueInfo) throws Exception {
        return ResourceConverter.toResponseObject(coopApiConnectService.sendTicket(ticketIssueInfo));
    }

    /**
     * [coop] 티켓 발송요청 (암호화)
     * - 문의 : 모바일주문개발TF
     */
//    @ApiOperation(value = "티켓 발송요청", response = String.class)
//    @PostMapping(value = "/sendTicketEncrypt")
//    public ResponseObject<String> sendTicketEncrypt(@RequestBody TicketIssueInfo ticketIssueInfo) throws Exception {
//        return ResourceConverter.toResponseObject(coopApiConnectService.sendTicketEncrypt(ticketIssueInfo));
//    }

    /**
     * [coop] 발급한 티켓의 발급정보 조회 (비암호화)
     * - 문의 : 모바일주문개발TF
     */
    @ApiOperation(value = "발급한 티켓의 발급정보 조회", response = String.class)
    @PostMapping(value = "/getIssuedTicketInfo")
    public ResponseObject<String> getIssuedTicketInfo(@RequestBody TicketIssueInfo ticketIssueInfo) throws Exception {
        return ResourceConverter.toResponseObject(coopApiConnectService.getIssuedTicketInfo(ticketIssueInfo));
    }

    /** ************************************** claim ************************************************************* */

    /**
     * [coop] 발급취소 요청
     * - 문의 : 모바일정산개발TF
     */
    @ApiOperation(value = "발급한 티켓의 취소 처리(티켓쿠폰번호 기준)", response = String.class)
    @PostMapping(value = "/cancelTicket")
    public ResponseObject<TicketClaimInfo> cancelTicket(@RequestBody ClaimCoopTicketCheckSetDto claimCoopTicketCheckSetDto) throws Exception {

        TicketClaimInfo ticketClaimInfo = new TicketClaimInfo();
        ticketClaimInfo.setSellerItemCd(claimCoopTicketCheckSetDto.getSellerItemCd());
        ticketClaimInfo.setTicketCd(claimCoopTicketCheckSetDto.getTicketCd());

        return ResourceConverter.toResponseObject(coopApiConnectCancelService.cancelTicket(ticketClaimInfo));
    }

    /**
     * [coop] 발급취소 요청
     * - 문의 : 모바일정산개발TF
     */
    @ApiOperation(value = "발급한 티켓의 취소 처리(거래번호 기준)", response = String.class)
    @GetMapping(value = "/cancelTicketByOrderTicketNo")
    public ResponseObject<TicketClaimInfo> cancelTicketByOrderTicketNo(
            @ApiParam(name = "sellerItemCd", value = "상품코드(COUPONCODE)", required = true) @RequestParam String sellerItemCd,
            @ApiParam(name = "orderTicketNo", value = "거래번호(SEQNUMBER)", required = true) @RequestParam String orderTicketNo
    ) throws Exception {

        TicketClaimInfo ticketClaimInfo = new TicketClaimInfo();
        ticketClaimInfo.setSellerItemCd(sellerItemCd);
        ticketClaimInfo.setOrderTicketNo(orderTicketNo);

        return ResourceConverter.toResponseObject(coopApiConnectCancelService.cancelTicketByOrderTicketNo(ticketClaimInfo));
    }

    /**
     * [coop] 발급취소 가능여부 확인 (거래번호 기준)
     * - 문의 : 모바일정산개발TF
     */
    @ApiOperation(value = "발급취소 가능여부 확인 (거래번호 기준)", response = String.class)
    @PostMapping(value = "/cancelTicketCheck")
    public ResponseObject<TicketClaimInfo> cancelTicketCheck(
            @ApiParam(name = "sellerItemCd", value = "상품코드(COUPONCODE)", required = true) @RequestParam String sellerItemCd,
            @ApiParam(name = "orderTicketNo", value = "거래번호(SEQNUMBER)", required = true) @RequestParam String orderTicketNo
    ) throws Exception {

        TicketClaimInfo ticketClaimInfo = new TicketClaimInfo();
        ticketClaimInfo.setSellerItemCd(sellerItemCd);
        ticketClaimInfo.setOrderTicketNo(orderTicketNo);

        return ResourceConverter.toResponseObject(coopApiConnectCancelService.cancelTicketCheck(ticketClaimInfo));
    }

    /**
     * [coop] 클레임용-발급한 티켓의 발급정보 확인(티켓쿠폰번호 기준)
     * - 문의 : 모바일정산개발TF
     */
    @ApiOperation(value = "클레임용-발급한 티켓의 발급정보 확인(티켓쿠폰번호 기준)", response = String.class)
    @PostMapping(value = "/cancelTicketInfo")
    public ResponseObject<TicketClaimInfo> cancelTicketInfo(@RequestBody ClaimCoopTicketCheckSetDto claimCoopTicketCheckSetDto) throws Exception {

        TicketClaimInfo ticketClaimInfo = new TicketClaimInfo();
        ticketClaimInfo.setSellerItemCd(claimCoopTicketCheckSetDto.getSellerItemCd());
        ticketClaimInfo.setTicketCd(claimCoopTicketCheckSetDto.getTicketCd());

        return ResourceConverter.toResponseObject(coopApiConnectCancelService.getClaimTicketInfo(ticketClaimInfo));
    }

    /**
     * [coop] 클레임용-발급한 티켓의 발급정보 확인(거래번호 기준)
     * - 문의 : 모바일정산개발TF
     */
    @ApiOperation(value = "클레임용-발급한 티켓의 발급정보 확인(거래번호 기준)", response = String.class)
    @PostMapping(value = "/cancelTicketInfoByOrderTicketNo")
    public ResponseObject<TicketClaimInfo> cancelTicketInfoByOrderTicketNo(
            @ApiParam(name = "sellerItemCd", value = "상품코드(COUPONCODE)", required = true) @RequestParam String sellerItemCd,
            @ApiParam(name = "orderTicketNo", value = "거래번호(SEQNUMBER)", required = true) @RequestParam String orderTicketNo
    ) throws Exception {

        TicketClaimInfo ticketClaimInfo = new TicketClaimInfo();
        ticketClaimInfo.setSellerItemCd(sellerItemCd);
        ticketClaimInfo.setOrderTicketNo(orderTicketNo);

        return ResourceConverter.toResponseObject(coopApiConnectCancelService.getClaimTicketInfoByOrderTicketNo(ticketClaimInfo));
    }
}
