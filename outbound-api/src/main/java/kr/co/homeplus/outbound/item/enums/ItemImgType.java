package kr.co.homeplus.outbound.item.enums;

import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.collect.ImmutableBiMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ItemImgType {

        MN("MN", "메인이미지", "s", "ItemMainImage", "ItemMain")
    ,   LST("LST", "리스트이미지", "r", "ItemListImage", "ItemList")
    ,   LBL("LBL", "라벨이미지", null, "ItemLabelImage", null)
    ,   DETAIL("DETAIL", "상품 상세 설명 이미지", null, "ItemDetail", null)
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;

    @Getter
    private final String fileNmSuffix;

    @Getter
    private final String processKey;

    @Getter
    private final String processKeyMain;

    @Getter
    private static final ImmutableBiMap<ItemImgType, String> biMap;

    static {
        BiMap<ItemImgType, String> map = EnumHashBiMap.create(ItemImgType.class);

        for (ItemImgType policy : values()) {
            map.put(policy, policy.name());
        }

        biMap = ImmutableBiMap.copyOf(map);
    }
}
