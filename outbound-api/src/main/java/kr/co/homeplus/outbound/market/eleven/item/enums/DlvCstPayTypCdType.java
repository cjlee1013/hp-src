package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

public enum DlvCstPayTypCdType {
	@XmlEnumValue("01")
	CAN_ADVANCE_PAY("01", "선결제가능"),

	@XmlEnumValue("02")
	CANT_ADVANCE_PAY("02", "선결제불가"),

	@XmlEnumValue("03")
	REQUIRED_ADVANCE_PAY("03", "선결제필수");

	@Getter
	private String code;
	@Getter
	private String desc;

	private DlvCstPayTypCdType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}
