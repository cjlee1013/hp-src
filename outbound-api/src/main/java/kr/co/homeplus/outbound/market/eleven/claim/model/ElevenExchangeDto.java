package kr.co.homeplus.outbound.market.eleven.claim.model;

import lombok.Data;

@Data
public class ElevenExchangeDto {
    //무료교환 여부
    private String affliateBndlDlvSeq;
    //사유코드에 대한 상세내역
    private String clmReqCont;
    //클레임 수량
    private long clmReqQty;
    //클레임 사유코드
    private String clmReqRsn;
    //외부몰 클레임 번호
    private long clmReqSeq;
    //클레임 상태
    private String clmStat;
    //옵션명
    private String optName;
    //11번가 주문번호
    private long ordNo;
    //주문순번
    private long ordPrdSeq;
    //상품번호
    private long prdNo;
    //클레임 요청 일시
    private String reqDt;
    //수거지 이름
    private String ordNm;
    //수거지 전화번호
    private String ordTlphnNo;
    //수거지 휴대폰번호
    private String ordPrtblTel;
    //수거지 우편번호
    private String rcvrMailNo;
    //수거지 우편번호 순번
    private String rcvrMailNoSeq;
    //수거지 기본주소
    private String rcvrBaseAddr;
    //수거지 상세주소
    private String rcvrDtlsAddr;
    //수거지 주소 유형
    private String rcvrTypeAdd;
    //수거지 건물관리번호
    private String rcvrTypeBilNo;
    //교환상품 발송방법
    private String twMthd;
    //교환상품 수령지 이름
    private String exchNm;
    //교환상품 수령지 전화번호
    private String exchTlphnNo;
    //교환상품 수령지 휴대폰번호
    private String exchPrtblTel;
    //교환상품 수령지 우편번호
    private String exchMailNo;
    //교환상품 수령지 우편번호 순번
    private String exchMailNoSeq;
    //교환상품 수령지 기본주소
    private String exchBaseAddr;
    //교환상품 수령지 상세주소
    private String exchDtlsAddr;
    //교환상품 수령지 주소 유형
    private String exchTypeAdd;
    //교환상품 수령지 건물관리번호
    private String exchTypeBilNo;
    //교환배송비
    private long clmLstDlvCst;
    //11번가 지정반품 택배비
    private long appmtDlvCst;
    //결제방법
    private String clmDlvCstMthd;
    //수거송장번호
    private String twPrdInvcNo;
    //수거택배사코드
    private String dlvEtprsCd;
}
