package kr.co.homeplus.outbound.ticket.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "발급대상 티켓정보")
@Getter
@Setter
public class TicketClaimInfo {

    /** 전송 항목 */
    @ApiModelProperty(value = "업체상품코드(연동상품번호)-COUPONCODE(요청)", position = 1)
    private String sellerItemCd;
    @ApiModelProperty(value = "티켓쿠폰번호-COUPONNUM(요청)", position = 1)
    private String ticketCd;
    @ApiModelProperty(value = "주문티켓번호(발급거래번호)-SEQNUMBER(요청)", position = 1)
    private String orderTicketNo;

    /** 응답 항목 */
    @ApiModelProperty(value = "응답코드(응답)", position =1)
    private String resultCode;
    @ApiModelProperty(value = "응답메세지(응답)", position =1)
    private String resultMsg;
    @ApiModelProperty(value = "티켓쿠폰 번호", position =1)
    private String couponNumber;
    @ApiModelProperty(value = "사용여부 (Y :사용, N :미사용, C :사용취소)(응답)", position =1)
    private String useYn;
    @ApiModelProperty(value = "티켓쿠폰 사용 일자(응답)", position =1)
    private String useDate;
    @ApiModelProperty(value = "티켓쿠폰 사용 장소(응답)", position =1)
    private String usePlace;
    @ApiModelProperty(value = "최종 수신자 번호(응답)", position =1)
    private String sendHp;
    @ApiModelProperty(value = "유효기간 시작일(응답)", position =1)
    private String useStart;
    @ApiModelProperty(value = "유효기간 종료일(응답)", position =1)
    private String useEnd;
    @ApiModelProperty(value = "발급일자(응답)", position =1)
    private String insDate;
    @ApiModelProperty(value = "임시공간(미 사용시 공백 회신)(응답)", position =1)
    private String filler01;
    @ApiModelProperty(value = "임시공간(미 사용시 공백 회신)(응답)", position =1)
    private String filler02;
    @ApiModelProperty(value = "임시공간(미 사용시 공백 회신)(응답)", position =1)
    private String filler03;
    @ApiModelProperty(value = "핀번호", position =1)
    private String pinNumber;

    public String printParam(){
        return " 요청 : sellerItemCd : ["+sellerItemCd+ "]"
                + " ticketCd : ["+ticketCd+ "]"
                + " orderTicketNo : ["+orderTicketNo+ "]"
                + " 응답 : resultCode : ["+resultCode+ "]"
                + " resultMsg : ["+resultMsg+ "]"
                + " couponNumber : ["+couponNumber+ "]"
                + " useYn : ["+useYn+ "]"
                + " useDate : ["+useDate+ "]"
                + " usePlace : ["+usePlace+ "]"
                + " sendHp : ["+sendHp+ "]"
                + " useStart : ["+useStart+ "]"
                + " useEnd : ["+useEnd+ "]"
                + " insDate : ["+insDate+ "]"
                + " filler01 : ["+filler01+ "]"
                + " filler02 : ["+filler02+ "]"
                + " filler02 : ["+filler02+ "]"
                + " pinNumber : ["+pinNumber+ "]"
                ;
    }
}
