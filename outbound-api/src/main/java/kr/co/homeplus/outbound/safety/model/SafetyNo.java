package kr.co.homeplus.outbound.safety.model;

import lombok.Getter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Getter
public class SafetyNo {

    private String phoneNo;
    private String expireDt;
    private String issuePhoneNo;

    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }
}
