package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

/**
 *판매방식  타입
 */

public enum SellMethodType {
	@XmlEnumValue("01")
	FIXED("01", "고정가판매"),

	@XmlEnumValue("02")
	GROUP("02", "공동구매"),

	@XmlEnumValue("03")
	AUCTION("03", "경매"),

	@XmlEnumValue("04")
	RESERVE("04", "예약판매"),

	@XmlEnumValue("05")
	USED("05", "중고");

	@Getter
	private String code;
	@Getter
	private String desc;

	private SellMethodType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}
