package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

/**
 * 배송 주체
 */
public enum DlvClfType {

    @XmlEnumValue( "01" )
    ELEVENTH_STREET( "01", "11번가 배송" ),

    @XmlEnumValue( "02" )
    INDIVIDUAL( "02", "업체배송" ),

    @XmlEnumValue( "02" )
    ELEVENTH_STREET_ADBOAD( "02", "11번가해외배송" );

    @Getter
    private String code;
    @Getter
    private String desc;

    private DlvClfType( String code, String desc ) {
        this.desc = desc;
        this.code = code;
    }
}
