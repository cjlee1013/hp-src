package kr.co.homeplus.outbound.market.eleven.claim.controller;

import com.nhncorp.platform.shopn.seller.SellerServiceStub.ClaimRequestReasonType;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ProductOrderChangeType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.outbound.market.eleven.claim.model.ElevenCancelDto;
import kr.co.homeplus.outbound.market.eleven.claim.model.ElevenClaimInfoDto;
import kr.co.homeplus.outbound.market.eleven.claim.model.ElevenResponseDto;
import kr.co.homeplus.outbound.market.eleven.claim.service.ElevenClaimService;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverClaimDataDto;
import kr.co.homeplus.outbound.market.naver.claim.service.NaverClaimService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "마켓연동 > 네이버")
@Slf4j
@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class ElevenMarketClaimController {

    private final ElevenClaimService elevenClaimService;

    @ApiOperation(value = "11번가 취소주문조회")
    @GetMapping("/eleven/getClaimCancelList")
    public ResponseObject<ElevenClaimInfoDto<ElevenCancelDto>>  getClaimRequestList(
        @RequestParam("startDt") String startDt,
        @RequestParam("endDt") String endDt
    ) throws Exception {
        return elevenClaimService.getElevenClaimInfo(startDt, endDt);
    }

    @ApiOperation(value = "11번가 판매취소")
    @GetMapping("/eleven/setCancelSale")
    public ResponseObject<ElevenResponseDto> setCancelSale(
        @RequestParam("ordNo") String ordNo,
        @RequestParam("ordPrdSeq") String ordPrdSeq,
        @RequestParam("ordCnRsnCd") String ordCnRsnCd,
        @RequestParam("ordCnDtlsRsn") String ordCnDtlsRsn
    ) throws Exception {
        return elevenClaimService.setRegNotSelling(ordNo, ordPrdSeq, ordCnRsnCd, ordCnDtlsRsn);
    }

}
