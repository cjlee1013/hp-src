package kr.co.homeplus.outbound.market.eleven.shipping.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlAccessorType( XmlAccessType.FIELD )
@XmlRootElement( name = "orders", namespace = "http://skt.tmall.business.openapi.spring.service.client.domain/" )
public class ElevenShipCompleteListGetDto extends ElevenOrderCommonGetDto{

    //판매완료 리스트
    @XmlElement( name = "order", namespace = "http://skt.tmall.business.openapi.spring.service.client.domain/" )
    private List< ElevenShipCompleteItemGetDto > elevenShipCompleteItemGetDto;
}
