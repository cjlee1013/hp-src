package kr.co.homeplus.outbound.market.eleven.item.model;

import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Data;

@Data
@ApiModel("마켓연동 상품연동 대상 ")
public class ItemSendParamDto {

	List<String> itemList;
}