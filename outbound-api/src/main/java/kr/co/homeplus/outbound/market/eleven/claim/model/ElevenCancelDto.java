package kr.co.homeplus.outbound.market.eleven.claim.model;

import lombok.Data;

@Data
public class ElevenCancelDto {
    //클레임요청일시
    private String createDt;
    //배송번호
    private long dlvNo;
    //사유코드에대한 상세내역
    private String ordCnDtlsRsn;
    //클레임수량
    private long ordCnQty;
    //클레임등록주체
    private String ordCnMnbdCd;
    //클레임사유코드
    private String ordCnRsnCd;
    //클레임상태
    private String ordCnStatCd;
    //11st주문번호
    private long ordNo;
    //11st claimNo
    private long ordPrdCnSeq;
    //주문순번
    private long ordPrdSeq;
    //상품번호
    private long prdNo;
    //클레임옵션명
    private String slctPrdOptNm;
    //원클릭체크아웃주문코드
    private long referSeq;
}
