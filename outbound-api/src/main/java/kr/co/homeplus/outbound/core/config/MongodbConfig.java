package kr.co.homeplus.outbound.core.config;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;


@Configuration
public class MongodbConfig extends AbstractMongoConfiguration {

    @Value("${nosql.data.mongodb.uri}")
    private String mongoUri;

    @Value("${nosql.data.mongodb.username}")
    private String username;

    @Value("${nosql.data.mongodb.password}")
    private String password;

    @Value("${nosql.data.mongodb.options.ssl-enabled}")
    private boolean sslEnabled;

    @Value("${nosql.data.mongodb.options.ssl-invalid-host-name-allowed}")
    private boolean sslInvalidHostNameAllowed;

    @Value("${nosql.data.mongodb.options.max-wait-time}")
    private int maxWaitTime;

    @Value("${nosql.data.mongodb.options.max-connection-idle-time}")
    private int maxConnectionIdleTime;

    @Value("${nosql.data.mongodb.options.max-connection-life-time}")
    private int maxConnectionLifeTime;

    @Value("${nosql.data.mongodb.options.socket-timeout}")
    private int socketTimeout;

    @Value("${nosql.data.mongodb.options.connect-timeout}")
    private int connectTimeout;

    @Value("${nosql.data.mongodb.options.server-selection-timeout}")
    private int serverSelectionTimeout;

    @Override
    public MongoClient mongoClient() {
        MongoClientURI mongoClientURI = new MongoClientURI(this.mongoUri, MongoClientOptions.builder()
                .sslEnabled(sslEnabled)
                .sslInvalidHostNameAllowed(sslInvalidHostNameAllowed)
                .maxWaitTime(maxWaitTime)
                .maxConnectionIdleTime(maxConnectionIdleTime)
                .maxConnectionLifeTime(maxConnectionLifeTime)
                .socketTimeout(socketTimeout)
                .connectTimeout(connectTimeout)
                .serverSelectionTimeout(serverSelectionTimeout)
        );

        MongoCredential mongoCredential =
                MongoCredential.createCredential(this.username, mongoClientURI.getDatabase(),this.password.toCharArray());
        ServerAddress serverAddress = new ServerAddress(mongoClientURI.getHosts().get(0));
        MongoClientOptions options = MongoClientOptions.builder().build();
        return new MongoClient(serverAddress, mongoCredential, options);
    }

    @Override
    protected String getDatabaseName() {
        return new MongoClientURI(this.mongoUri).getDatabase();
    }
}
