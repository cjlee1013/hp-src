package kr.co.homeplus.outbound.cert.service;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import kr.co.homeplus.outbound.cert.constants.ItemCertConstants;
import kr.co.homeplus.outbound.cert.enums.CertUrl;
import kr.co.homeplus.outbound.cert.model.ItemLifeCertGetDto;
import kr.co.homeplus.outbound.cert.model.ItemRadioCertGetDto;
import kr.co.homeplus.outbound.cert.model.ItemSafeCertGetDto;
import kr.co.homeplus.outbound.core.constants.ResourceRouteName;
import kr.co.homeplus.outbound.response.ResponseResult;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
@RequiredArgsConstructor
@Slf4j
public class ItemSafeCertService {

    private final ResourceClient resourceClient;

    @Value("${safe-cert-api-key}")
    private String safeCertKey;

    @Value("${life-cert-api-key}")
    private String lifeCertKey;

    /**
     * 전파적합성 인증
     * @param certNo
     * @return
     */
    public ResponseResult checkItemRadioCert(final String certNo) {
        ResponseResult responseResult = new ResponseResult();

        try {
            String apiUri = CertUrl.RADIO_CERT_INFO.getUrl() + certNo;
            ItemRadioCertGetDto res = resourceClient
                .getForResponseEntity(ResourceRouteName.RADIO_CERT, apiUri, ItemRadioCertGetDto.class)
                .getBody().getData();

            responseResult.setReturnKey(ItemCertConstants.CERT_N);
            responseResult.setReturnMsg(ItemCertConstants.FAIL);

            if(!ObjectUtils.isEmpty(res)) {
                if(res.getResultCode().equals("0000") && res.getAuthYn().equals("Y")) {
                    responseResult.setReturnKey(ItemCertConstants.CERT_Y);
                    responseResult.setReturnMsg(ItemCertConstants.SUCCESS);
                }
            }

        }catch (Exception e ){
            log.error("RadioCert[" +certNo +"] :" + e.getMessage());
            responseResult.setReturnKey(ItemCertConstants.CERT_N);
            responseResult.setReturnMsg(ItemCertConstants.ERROR);
        }
        return responseResult;
    }

    /**
     * 안전인증
     * @param certNo
     * @return
     */
    public ResponseResult checkItemSafeCert(final String certNo) {
        ResponseResult responseResult = new ResponseResult();

        try{
            HttpHeaders headers = new HttpHeaders();
            headers.set("AuthKey", safeCertKey);

            String param = "?certNum=" + certNo;
            String url = CertUrl.SAFE_CERT_INFO.getUrl() + param;

            ItemSafeCertGetDto res = resourceClient
                .getForResponseEntity(ResourceRouteName.SAFE_CERT, url, headers, ItemSafeCertGetDto.class)
                .getBody().getData();

            responseResult.setReturnKey(ItemCertConstants.CERT_N);
            responseResult.setReturnMsg(ItemCertConstants.FAIL);

            if (!ObjectUtils.isEmpty(res)){
                if(res.getResultCode().equals("2000") && res.getResultData().getCertState().equals("적합")) {
                    responseResult.setReturnKey(ItemCertConstants.CERT_Y);
                    responseResult.setReturnMsg(ItemCertConstants.SUCCESS);
                }
            }

        }catch (Exception e ){
            log.error("SafeCert[" + certNo +"] :" + e.getMessage());
            responseResult.setReturnKey(ItemCertConstants.CERT_N);
            responseResult.setReturnMsg(ItemCertConstants.ERROR);
        }

        return responseResult;
    }

    public ResponseResult checkItemLifeCert(String certNo) {
        ResponseResult responseResult = new ResponseResult();

        try {
            String url = CertUrl.LIFE_CERT_INFO.getUrl();
            String param = "&AuthKey="+lifeCertKey + "&SlfsfcfstNo="+certNo;

            ItemLifeCertGetDto res = resourceClient
                .getForResponseEntity(ResourceRouteName.LIFE_CERT, url+param, ItemLifeCertGetDto.class)
                .getBody().getData();

            responseResult.setReturnKey(ItemCertConstants.CERT_N);
            responseResult.setReturnMsg(ItemCertConstants.FAIL);

            if(!ObjectUtils.isEmpty(res) && res.getCount() > 0 ) {
                responseResult.setReturnMsg(res.getResultcode());
                if(res.getResultcode().equals("0000")
                    && res.getRow().getValid_yn().equals("Y")) {
                    responseResult.setReturnKey(ItemCertConstants.CERT_Y);
                    responseResult.setReturnMsg(ItemCertConstants.SUCCESS);
                }
            }

        } catch (Exception e) {
            log.error("LifeCert["+ certNo +"] :" + e.getMessage());
            responseResult.setReturnKey(ItemCertConstants.CERT_N);
            responseResult.setReturnMsg(ItemCertConstants.ERROR);
        }

        return responseResult;
    }

    public ResponseResult checkItemCert(String certGroup, String certNo) {

        ResponseResult responseResult = new ResponseResult();

        certNo = URLEncoder.encode(certNo, StandardCharsets.UTF_8);
        switch (certGroup) {
            case "IT" :
            case "KD" :
                responseResult = this.checkItemSafeCert(certNo);
                break;
            case "ER" :
                responseResult = this.checkItemRadioCert(certNo);
                break;
            case "ET" :
                responseResult = this.checkItemLifeCert(certNo);
                break;

            default:
                responseResult.setReturnKey(ItemCertConstants.CERT_N);
                responseResult.setReturnMsg(ItemCertConstants.ERROR);
                break;
        }
        return responseResult;
    }
}
