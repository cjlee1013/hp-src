package kr.co.homeplus.outbound.core.maketLog.exception;

public class MarketLogServiceException extends RuntimeException {

    public MarketLogServiceException(String message) {
        super(message);
    }

    public MarketLogServiceException(String message, Throwable e) {
        super(message, e);
    }

    public MarketLogServiceException(Throwable throwable) {
        super(throwable);
    }
}
