package kr.co.homeplus.outbound.ticket.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import static kr.co.homeplus.outbound.core.constants.ClaimTicketConstants.*;
import  kr.co.homeplus.outbound.ticket.model.ClaimTicketInfoOrderTicketResponse.CouponInfo.*;

@ApiModel(description = "티켓 발행 정보 조회 (홈플러스 거래번호 기준) API 응답 객체")
@Getter
@XmlRootElement(namespace= XML_ROOT_NAME_SPACE, name= CLAIM_TICKET_INFO_ORDER_TICKET_XML_ROOT_NAME)
@XmlAccessorType(XmlAccessType.FIELD)
public class ClaimTicketInfoOrderTicketResponse {

    @ApiModelProperty(value = "취소리턴코드")
    @XmlElement(name="RESULTCODE", namespace= XML_ROOT_NAME_SPACE)
    private String resultCode;

    @ApiModelProperty(value = "취소리턴메시지")
    @XmlElement(name="RESULTMSG", namespace= XML_ROOT_NAME_SPACE)
    private String resultMsg;

    @ApiModelProperty(value = "COUPON_INFO")
    @XmlElement(name=CLAIM_TICKET_INFO_ORDER_TICKET_XML_COUPON_NAME , namespace= XML_ROOT_NAME_SPACE)
    private CouponInfo couponInfo;

    public DownloadCoupon getDownloadCoupon() {
        return couponInfo.getDownloadCoupon();
    }

    public static class CouponInfo {

        @ApiModelProperty(value = "DownloadCoupon")
        @XmlElement(name=CLAIM_TICKET_INFO_ORDER_TICKET_XML_DOWN_COUPON_NAME , namespace= XML_ROOT_NAME_SPACE)
        private DownloadCoupon downloadCoupon;

        DownloadCoupon getDownloadCoupon() {
            return downloadCoupon;
        }

        @ApiModel(description = "티켓 발행 정보 조회 (홈플러스 거래번호 기준)-상세")
        @Getter
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class DownloadCoupon {

            @ApiModelProperty(value = "쿠폰 번호")
            @XmlElement(name = "COUPONNUMBER", namespace = XML_ROOT_NAME_SPACE)
            private String couponNumber;

            @ApiModelProperty(value = "쿠폰 사용 여부")
            @XmlElement(name = "USE_YN", namespace = XML_ROOT_NAME_SPACE)
            private String useYn;

            @ApiModelProperty(value = "쿠폰 사용 일자")
            @XmlElement(name = "USE_DATE", namespace = XML_ROOT_NAME_SPACE)
            private String useDate;

            @ApiModelProperty(value = "쿠폰 사용 장소")
            @XmlElement(name = "USE_PLACE", namespace = XML_ROOT_NAME_SPACE)
            private String usePlace;

            @ApiModelProperty(value = "최종 수신자 번호")
            @XmlElement(name = "SEND_HP", namespace = XML_ROOT_NAME_SPACE)
            private String sendHp;

            @ApiModelProperty(value = "유효기간 시작일")
            @XmlElement(name = "USE_START", namespace = XML_ROOT_NAME_SPACE)
            private String useStart;

            @ApiModelProperty(value = "유효기간 종료일")
            @XmlElement(name = "USE_END", namespace = XML_ROOT_NAME_SPACE)
            private String useEnd;

            @ApiModelProperty(value = "발급일자")
            @XmlElement(name = "INS_DATE", namespace = XML_ROOT_NAME_SPACE)
            private String insDate;

            @ApiModelProperty(value = "임시공간01")
            @XmlElement(name = "FILLER01", namespace = XML_ROOT_NAME_SPACE)
            private String filler01;

            @ApiModelProperty(value = "임시공간02")
            @XmlElement(name = "FILLER01", namespace = XML_ROOT_NAME_SPACE)
            private String filler02;

            @ApiModelProperty(value = "임시공간03")
            @XmlElement(name = "FILLER01", namespace = XML_ROOT_NAME_SPACE)
            private String filler03;

            @ApiModelProperty(value = "핀번호")
            @XmlElement(name = "COUPONNUMBER", namespace = XML_ROOT_NAME_SPACE)
            private String pinNumber;
        }
    }

}
