package kr.co.homeplus.outbound.market.naver.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NaverDeliveryDto {
    @ApiModelProperty(value= "배송상세상태")
    private String deliveryStatus;
    @ApiModelProperty(value= "배송방법코드")
    private String deliveryMethod;
    @ApiModelProperty(value= "택배사코드")
    private String deliveryCompany;
    @ApiModelProperty(value= "송장번호")
    private String trackingNumber;
    @ApiModelProperty(value= "발송일시")
    private String sendDate;
    @ApiModelProperty(value= "집화일시")
    private String pickupDate;
    @ApiModelProperty(value= "배송완료일시")
    private String deliveredDate;
    @ApiModelProperty(value= "오류송장여부")
    private String isWrongTrackingNumber;
    @ApiModelProperty(value= "오류송장등록일시")
    private String wrongTrackingNumberRegisteredDate;
    @ApiModelProperty(value= "오류사유")
    private String wrongTrackingNumberType;
}
