package kr.co.homeplus.outbound.item.mapper;


import java.util.List;
import kr.co.homeplus.outbound.core.db.annotation.MasterConnection;
import kr.co.homeplus.outbound.item.model.ItemMarketImgSendSetDto;
import kr.co.homeplus.outbound.item.model.ItemMarketSetDto;
import org.apache.ibatis.annotations.Param;

@MasterConnection
public interface ItemMasterMapper {

    /**
     * 마켓연동 전송 처리 상태 변경
     * @param itemMarketSetDto
     * @param itemList
     * @return
     */
    int updateItemMarketSendStart(@Param("dto") ItemMarketSetDto itemMarketSetDto ,
                        @Param("itemList") List<String> itemList);

    /**
     * 마켓연동 판매 중지 처리
     * @param itemMarketSetDto
     * @return
     */
    int updateItemMarket(ItemMarketSetDto itemMarketSetDto);


    /**
     * 마켓연동 판매상태 변경
     * @param itemMarketSetDto
     * @return
     */
    int updateItemMarketStatus(ItemMarketSetDto itemMarketSetDto);

    /**
     * 상품 이미지 업로드 연동
     * @param itemMarketImgSendSetDto
     * @return int
     */
    int upsertItemMarketImageSend(ItemMarketImgSendSetDto itemMarketImgSendSetDto);

}
