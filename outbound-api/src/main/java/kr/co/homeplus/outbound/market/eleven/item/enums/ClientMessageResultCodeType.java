package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

/**
 * 상품연동 Result Code
 */
public enum ClientMessageResultCodeType {
	@XmlEnumValue("101")
	PENDNG("101", "승인대기"),

	@XmlEnumValue("102")
	PRE_APPROVAL("102", "승인전"),

	@XmlEnumValue("103")
	SALELLING("103", "판매중"),

	@XmlEnumValue("104")
	OUT_OF_STOCK("104", "품절"),

	@XmlEnumValue("105")
	EXHIBIT_STOP("105", "전시중지"),

	@XmlEnumValue("105")
	SELLING_STOP("105", "판매정상중지"),

	@XmlEnumValue("106")
	SELLING_FORCED_STOP("106", "판매강제종료중지"),

	@XmlEnumValue("200")
	SUCCESS("200", "성공"),

	@XmlEnumValue("210")
	NEW_SUCCESS("210", "성공"),

	@XmlEnumValue("500")
	FAIL("500", "실패");

	@Getter
	private String code;
	@Getter

	private String desc;

	private ClientMessageResultCodeType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}
