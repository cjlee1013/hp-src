package kr.co.homeplus.outbound.store.mapper;

import java.util.List;
import kr.co.homeplus.outbound.core.db.annotation.SlaveConnection;
import kr.co.homeplus.outbound.store.model.StoreGetDto;
import kr.co.homeplus.outbound.store.model.StoreGetParamDto;

@SlaveConnection
public interface StoreSlaveMapper {

    List<StoreGetDto> getStoreList(
        StoreGetParamDto storeGetParamDto);
}


