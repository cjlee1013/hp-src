package kr.co.homeplus.outbound.market.naver.inquiry.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.GetQuestionAnswerListResponseType;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.ManageQuestionAnswerResponseType;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.QuestionAnswerListType;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.QuestionAnswerReturnType;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryResponseType;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.CustomerInquiryList_type0;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListResponseType;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.InquiryType;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.outbound.core.utility.StringUtil;
import kr.co.homeplus.outbound.market.eleven.inquiry.mapper.ElevenInquirySlaveMapper;
import kr.co.homeplus.outbound.market.naver.config.MarketNaverConfig;
import kr.co.homeplus.outbound.market.naver.inquiry.enums.CsType;
import kr.co.homeplus.outbound.market.naver.inquiry.mapper.NaverInquirySlaveMapper;
import kr.co.homeplus.outbound.market.naver.inquiry.model.NaverInquiryGetDto;
import kr.co.homeplus.outbound.market.naver.inquiry.model.NaverInquirySetDto;
import kr.co.homeplus.outbound.market.naver.util.DateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class NaverInquiryService {

    private final MarketNaverConfig marketNaverConfig;
    private final String INQUIRY_CHANNEL_PRODUCT = "P"; // 상품 문의
    private final String INQUIRY_CHANNEL_CUSTOMER = "C";	// 판매자 문의
    private final NaverInquirySlaveMapper naverInquirySlaveMapper;
    private final ElevenInquirySlaveMapper elevenInquirySlaveMapper;
    private final ExtQuestionAnswerClientStub extQuestionAnswerClientStub;
    private final ExtCustomerInquiryClientStub extCustomerInquiryClientStub;

    // 판매자 문의 조회
    public List<NaverInquiryGetDto> getCustomerInquiryList(NaverInquirySetDto naverInquirySetDto) {
        List<NaverInquiryGetDto> inquiryList = new ArrayList<>();

        try {
            Security.addProvider(new BouncyCastleProvider());
            GetCustomerInquiryListResponseType res = extCustomerInquiryClientStub.getCustomerInquiryList(naverInquirySetDto);
            Boolean isResponseOk = extCustomerInquiryClientStub.isResponseOk(res);

            //결과 출력
            if(isResponseOk == Boolean.TRUE) {
                CustomerInquiryList_type0 listType = res.getCustomerInquiryList();
                if (listType != null) {
                    InquiryType[] inquiryTypes = listType.getCustomerInquiry();
                    if (inquiryTypes != null && inquiryTypes.length > 0){
                        for (InquiryType inquiryType : inquiryTypes) {
                            inquiryList.add(this.convertCustomerInquiry(inquiryType));
                        }
                        log.info("getCustomerInquiryList : size {} , {}", inquiryList.size(), new ObjectMapper().writeValueAsString(inquiryList));
                    }

                    if (res.getHasMoreData()) {
                        naverInquirySetDto.setInquiryExtraData(res.getInquiryExtraData());
                        this.getCustomerInquiryList(naverInquirySetDto);
                    }
                }
            } else {
                log.error("판매자 문의 내역 수집 실패 :: {}", res.getError().getMessage());
            }
        } catch (Exception e) {
            log.error("판매자 문의 내역 수집 실패 :: {}", e.getMessage());
            //this.failSmsSendByAdd(req.getInquiryTimeFrom(), e.getMessage());
        }
        return inquiryList;
    }

    // 상품 문의 조회
    public List<NaverInquiryGetDto> getProductInquiryList(NaverInquirySetDto naverInquirySetDto) {
        List<NaverInquiryGetDto> inquiryList = new ArrayList<>();

        try {
            Security.addProvider(new BouncyCastleProvider());
            GetQuestionAnswerListResponseType res = extQuestionAnswerClientStub.getQuestionAnswerList(naverInquirySetDto);
            Boolean isResponseOk = extQuestionAnswerClientStub.isResponseOk(res);

            //결과 출력
            if(isResponseOk == Boolean.TRUE) {
                QuestionAnswerListType listType = res.getQuestionAnswerList();
                if (listType != null) {
                    QuestionAnswerReturnType[] questionAnswerList = listType.getQuestionAnswerList();
                    if (inquiryList != null && questionAnswerList.length > 0){
                        for (QuestionAnswerReturnType questionType : questionAnswerList) {
                            inquiryList.add(this.convertProductInquiry(questionType));
                        }
                        log.info("getProductInquiryList : size {} , {}", inquiryList.size(), new ObjectMapper().writeValueAsString(inquiryList));
                    }
                    // 페이징으로 결과가 넘어오므로, 전체 페이지 수 이전까지 반복 요청한다.
                    if (res.getTotalPage() > res.getPage()) {
                        naverInquirySetDto.setPageNumber(naverInquirySetDto.getPageNumber() + 1);
                        this.getProductInquiryList(naverInquirySetDto);
                    }
                }
            } else {
                log.error("상품 문의 내역 수집 실패 :: {}", res.getError().getMessage());
            }
        } catch (Exception e) {
            log.error("상품 문의 내역 수집 실패 :: {}", e.getMessage());
            //this.failSmsSendByAdd(req.getInquiryTimeFrom(), e.getMessage());
        }

        return inquiryList;
    }

    // 문의 답변
    public NaverInquiryGetDto setInquiryAnswer(NaverInquiryGetDto req) throws Exception {
        Security.addProvider(new BouncyCastleProvider());

        if (INQUIRY_CHANNEL_PRODUCT.equals(req.getInquiryChannel())) {
            ManageQuestionAnswerResponseType res = extQuestionAnswerClientStub.manageQuestionAnswer(req);
            Boolean isResponseOk = extQuestionAnswerClientStub.isResponseOk(res);
            if(isResponseOk == Boolean.TRUE) {
                req.setIssendYn("Y");
            } else {
                log.error("QuestionAnswer#manageQuestionAnswer error :: [{}] {}", req.getMessageNo(), res.getError().getMessage());
                req.setIssendYn("N");
                req.setFailMsg("[Message]" + res.getError().getMessage() + "\n[Detail]" + res.getError().getDetail());
            }
        } else {
            AnswerCustomerInquiryResponseType res = extCustomerInquiryClientStub.answerCustomerInquiry(req);
            Boolean isResponseOk = extCustomerInquiryClientStub.isResponseOk(res);
            if(isResponseOk == Boolean.TRUE) {
                req.setIssendYn("Y");
                try {
                    // 답변 번호 찾기 (수정 시 필요)
                    if (StringUtil.isEmpty(req.getAnswerContentId())) {
                        req.setAnswerContentId(findAnswerContentID(req));
                    }
                } catch (Exception e) {
                    log.error("AnswerContentId find fail. messageNo[{}], errMsg[{}]", req.getMessageNo(), e.getMessage());
                }
            } else {
                req = getACInqueryRes(res, req);
            }
        }
        log.info("naverInquiryGetDto :: [{}]", req);

        return req;
    }

    private NaverInquiryGetDto getACInqueryRes(AnswerCustomerInquiryResponseType res, NaverInquiryGetDto naverInquiryGetDto){
        String code = res.getError().getCode();
        String message = res.getError().getMessage();
        String detail = res.getError().getDetail();

        // ERR-NC-101004
        // 이민석-N쇼핑  1 day ago
        // 먼저 해당 코드는 현 시점에서 없는 문의에 대해 반환하는 오류 코드가 맞습니다.
        // 다만, 오류 원인에 따라 (1)과거에 등록되었으나 호출 시점에는 삭제된 문의글을 조회하려거나, (2)처음부터 존재하지 않은 문의글을 다루려고 할 때에도 - 동일한 오류 코드로 반환됩니다.
        // 이때는 메시지 내용에 따라 그 뉘앙스를 파악해주셔야 할 것 같습니다.
        // e.g.)
        // 문의번호 XXX에 해당하는 문의가 없습니다.
        // 문의번호 XXX에 해당하는 문의는 삭제된 문의 입니다.

        // 전체 정의된 에러 코드가 없기때문에 상황에 따라 추가 해야됨
        // setIssendYn == N 이면 계속 시도 하기 때문에 적절한 처리 필요
        if(code.equals("ERR-NC-101004")){
            naverInquiryGetDto.setIssendYn("Y");
        }else{
            log.error("CustomerInquiry#answerCustomerInquiry error :: [{}] {}", naverInquiryGetDto.getMessageNo(), message);
            naverInquiryGetDto.setIssendYn("N");
            naverInquiryGetDto.setFailMsg("[Message]" + message + "\n[Detail]" + detail);
        }

        log.info("getACInqueryRes#" + code + "#" + message + "#" + detail);

        return naverInquiryGetDto;
    }

    // 답변 번호 찾기 (수정 시 필요)
    private String findAnswerContentID(NaverInquiryGetDto naverInquiryGetDto) {
        String answerContentID = null;

        NaverInquirySetDto req = new NaverInquirySetDto();
        req.setInquiryTimeFrom(new SimpleDateFormat("yyyyMMddHHmmss").format(
                org.apache.commons.lang.time.DateUtils.addMinutes(DateUtils.convertDate(naverInquiryGetDto.getReceiveDate()), -3)));
        req.setInquiryTimeTo(new SimpleDateFormat("yyyyMMddHHmmss").format(
            org.apache.commons.lang.time.DateUtils.addMinutes(DateUtils.convertDate(naverInquiryGetDto.getReceiveDate()), 3)));
        req.setAnswered(true);

        List<NaverInquiryGetDto> inquiryList = this.getCustomerInquiryList(req);

        for (NaverInquiryGetDto dto : inquiryList) {
            if (naverInquiryGetDto.getMessageNo().toString().equals(dto.getMessageNo().toString())) {
                answerContentID = dto.getAnswerContentId();
                break;
            }
        }

        return answerContentID;
    }

    private NaverInquiryGetDto convertCustomerInquiry(InquiryType inquiryType){
        NaverInquiryGetDto target = new NaverInquiryGetDto();

        target.setMarketType("NAVER");
        target.setCopOrdNo(inquiryType.getOrderID());
        if (!StringUtil.isEmpty(inquiryType.getProductOrderID())) {
            target.setCopOrdGoodSeq(inquiryType.getProductOrderID().split(",")[0]);
        }
        target.setMessageNo(Long.parseLong(inquiryType.getInquiryID()));
        target.setMessageType(CsType.EMERGENCY.getCode());
        target.setReceiveDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(inquiryType.getInquiryDateTime().getTime()));
        target.setCopGoodId(inquiryType.getProductID());
        target.setRequestTitle(inquiryType.getTitle());
        target.setRequestComments(inquiryType.getInquiryContent());
        target.setContactType(inquiryType.getCategory());
        target.setInquirerId(inquiryType.getCustomerID());
        target.setInquirerName(inquiryType.getCustomerName());
        target.setAnswerContentId(inquiryType.getAnswerContentID());
        target.setInquiryChannel(INQUIRY_CHANNEL_CUSTOMER);

        Map<String, Object> marketItemInfo = elevenInquirySlaveMapper.selectMarketItemInfo("coopnaver",target.getCopGoodId());
        target.setGoodId(marketItemInfo.get("itemNo").toString());

        return target;
    }

    private NaverInquiryGetDto convertProductInquiry(QuestionAnswerReturnType questionType) {
        NaverInquiryGetDto target = new NaverInquiryGetDto();

        target.setMarketType("NAVER");
        target.setMessageNo(questionType.getQuestionAnswerId());
        target.setMessageType(CsType.QNA.getCode());
        // 네이버에서 문의 시간을 주지 않기때문에 문의 수집 일시로 대체한다.
//    	target.setReceiveDate(DateTime.check(questionType.getCreateDate(), "yyyy-MM-dd"));
        target.setReceiveDate(DateUtils.getCurrentYmdHis());
        target.setCopGoodId(String.valueOf(questionType.getProductId()));
        target.setRequestTitle(StringUtil.isEmpty(questionType.getSubject()) ? "상품 문의입니다."
            : questionType.getSubject());
        target.setRequestComments(questionType.getQuestion());
        target.setContactType("상품");
        target.setInquirerId(questionType.getWriterId());
        target.setInquirerName(questionType.getWriterName());
        target.setInquiryChannel(INQUIRY_CHANNEL_PRODUCT);

        Map<String, Object> marketItemInfo = elevenInquirySlaveMapper.selectMarketItemInfo("coopnaver",target.getCopGoodId());
        target.setGoodId(marketItemInfo.get("itemNo").toString());

        return target;
    }
}
