package kr.co.homeplus.outbound.market.eleven.item.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import kr.co.homeplus.outbound.core.utility.xml.AdapterXmlCDATA;
import lombok.Data;

@XmlAccessorType( XmlAccessType.FIELD )
@XmlRootElement( name = "item" )
@Data
public class ProductOptionValueSetDto  {
    /**
     * 옵션값
     */
    private String code;

	/**
     * 옵션값
     */
    @XmlJavaTypeAdapter( AdapterXmlCDATA.class )
    private String name;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

}