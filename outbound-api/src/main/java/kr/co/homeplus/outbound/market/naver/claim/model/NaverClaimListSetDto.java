package kr.co.homeplus.outbound.market.naver.claim.model;

import com.nhncorp.platform.shopn.seller.SellerServiceStub.ProductOrderChangeType;
import lombok.Data;

@Data
public class NaverClaimListSetDto {
    private String startDt;
    private String endDt;
    private ProductOrderChangeType productOrderChangeType;
}
