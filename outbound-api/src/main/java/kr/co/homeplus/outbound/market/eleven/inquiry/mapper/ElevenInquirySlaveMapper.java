package kr.co.homeplus.outbound.market.eleven.inquiry.mapper;

import java.util.Map;
import kr.co.homeplus.outbound.core.db.annotation.SlaveConnection;
import org.apache.ibatis.annotations.Param;

@SlaveConnection
public interface ElevenInquirySlaveMapper {
  Map<String,Object> selectMarketItemInfo(@Param("partnerId") String partnerId, @Param("marketItemNo") String marketItemNo);
}
