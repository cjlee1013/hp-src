package kr.co.homeplus.outbound.item.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.outbound.common.enums.NaverStoreStatusType;
import kr.co.homeplus.outbound.item.model.ItemStoreGetDto;
import kr.co.homeplus.outbound.item.model.naver.NaverApisPamphletEventDTO;
import kr.co.homeplus.outbound.item.model.naver.StoStoreInfo;
import kr.co.homeplus.outbound.item.service.naver.NaverItemService;
import kr.co.homeplus.outbound.market.eleven.item.model.ItemSendParamDto;
import kr.co.homeplus.outbound.response.ResponseResult;
import kr.co.homeplus.outbound.store.model.StoreGetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "마켓연동 상품관리 ( N마트 )")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found")
    ,   @ApiResponse(code = 405, message = "Method Not Allowed")
    ,   @ApiResponse(code = 422, message = "Unprocessable Entity")
    ,   @ApiResponse(code = 500, message = "Internal Server Error")
})
@RequestMapping("/item")
@RestController
@RequiredArgsConstructor
@Slf4j
public class NaverItemController {

    private final NaverItemService naverItemService;


    // TODO : 1. 네이버 신규등록/수정 상품 전송 || 2. 네이버 신규등록/수정 상품 전송 - [ADMIN 호출] || 3. 네이버 상품 상태 조회 || 4. 네이버 상품 동기화 처리 || 5. 네이버 중복 등록 상품 삭제 처리 || 6. 네이버 상품 판매종료 전송 전용 || 7. 네이버 상품 정보 조회
    // TODO : 8. 네이버 상품 옵션 정보 조회 || 9. 당일 묶음 배송 그룹을 등록 (?) || 10. 네이버 전단 행사 목록 조회 || 11. 네이버 전단 행사 상품 등록 || 12. 네이버 전단 행사 삭제 || 13. 네이버 전단 실시간 동기화 - [ADMIN 호출]

    @ApiOperation(value = "네이버 상품 정보 조회", notes = "네이버 상품 정보 조회", response = ResponseResult.class)
    @GetMapping(value = "/naver/getProduct")
    public ResponseObject getProduct(@RequestParam @ApiParam("제휴사 상품번호") final String coopProductId) throws Exception {
        return ResourceConverter.toResponseObject(naverItemService.getProduct(coopProductId));
    }

    @ApiOperation(value = "네이버 상품 옵션 정보 조회", notes = "네이버 상품 옵션 정보 조회", response = ResponseResult.class)
    @GetMapping(value = "/naver/getOption")
    public ResponseObject getOption(@RequestParam @ApiParam("제휴사 상품번호") final String coopProductId) throws Exception {
        return ResourceConverter.toResponseObject(naverItemService.getOption(coopProductId));
    }
    
    @ApiOperation(value = "네이버 상품 등록/수정", response = ResponseResult.class)
    @GetMapping(value = "/naver/sendItem")
    public ResponseObject sendItem(@RequestParam @ApiParam(name = "itemList", value = "상품번호" ) List<String> itemList) throws Exception {
        return ResourceConverter.toResponseObject(naverItemService.sendItem(itemList));
    }

    @ApiOperation(value = "네이버 상품 등록/수정", response = ResponseResult.class)
    @PostMapping(value = "/naver/sendItem")
    public ResponseObject sendItem (@RequestBody ItemSendParamDto itemSendParamDto) throws Exception {
        return ResourceConverter.toResponseObject(naverItemService.sendItem(itemSendParamDto.getItemList()));
    }

    @ApiOperation(value = "네이버 상품등록/수정(Swagger 전송 용도)", response = ResponseResult.class)
    @PostMapping(value = "/naver/sendItemForDirect")
    public ResponseObject sendItemForSwagger(@RequestBody ItemSendParamDto itemSendParamDto) throws Exception {
        return ResourceConverter.toResponseObject(naverItemService.sendItemForDirect(itemSendParamDto.getItemList()));
    }

    @ApiOperation(value = "네이버 이미지 테스트", response = ResponseResult.class)
    @GetMapping(value = "/naver/sendImageList")
    public ResponseObject sendImageList(@RequestParam @ApiParam(name = "imageList", value = "이미지 URL List" ) List<String> imageList) throws Exception {
        return ResourceConverter.toResponseObject(naverItemService.sendImageList(imageList));
    }

    @ApiOperation(value = "네이버 상품상태 변경", response = ResponseResult.class)
    @GetMapping(value = "/naver/changeProductSaleStatus")
    public ResponseObject changeProductSaleStatus(
            @RequestParam @ApiParam(name = "productId", value = "상품ID", required = true) final String productId ,
            @RequestParam @ApiParam(name = "statusType" , value = "상품상태" , required = true) NaverStoreStatusType statusType) throws Exception {
        return ResourceConverter.toResponseObject(naverItemService.changeProductSaleStatus(productId, statusType));
    }

    @ApiOperation( value ="네이버 전단 행사 목록 조회", notes ="네이버 등록된 전단 행사 목록을 조회한다(상점번호 입력 시 개별 조회)",  response = ResponseResult.class )
    @RequestMapping( value = "/pamphletEventList", method = RequestMethod.GET, produces = { "application/json", "application/xml" } )
    public ResponseObject pamphletEventList(NaverApisPamphletEventDTO req) throws Exception {
        return ResourceConverter.toResponseObject(naverItemService.eventList(req));
    }

    @ApiOperation( value ="네이버 전단 행사 상품 등록 전체", notes ="어드민에서 전단 행사 상품 등록 후 호출되며 네이버에 전체 전단 행사 상품을 업데이트한다.",  response = ResponseResult.class )
    @GetMapping(value = "/pamphletEventUpdate")
    public ResponseObject pamphletEventUpdate() throws Exception {
        naverItemService.pamphletEventUpdate();
        return ResourceConverter.toResponseObject(new ResponseObject<>());
    }

    @ApiOperation( value ="네이버 전단 행사 상품 등록 점포", notes ="어드민에서 전단 행사 상품 등록 후 호출되며 네이버에 전체 전단 행사 상품을 업데이트한다.",  response = ResponseResult.class )
    @PostMapping(value = "/pamphletEventUpdateStore")
    public ResponseObject pamphletEventUpdateStore(@RequestBody ItemStoreGetDto itemStoreGetDto) throws Exception {
        return ResourceConverter.toResponseObject(naverItemService.pamphletEventUpdateStore(itemStoreGetDto.getStoreId()));
    }

    @ApiOperation( value ="네이버 전단 행사 삭제", notes ="네이버에 등록된 전단 행사를 모두 삭제한다",  response = ResponseResult.class )
    @RequestMapping( value = "/pamphletEventDelete", method = RequestMethod.GET, produces = { "application/json", "application/xml" } )
    public void pamphletEventDelete(ModelMap model) {
        try {
            NaverApisPamphletEventDTO req = new NaverApisPamphletEventDTO();
            List<NaverApisPamphletEventDTO> eventList = naverItemService.eventList(req);
            for (NaverApisPamphletEventDTO dto : eventList) {
                naverItemService.eventDelete(dto);
            }
            model.addAttribute("message", "네이버 전단 행사 삭제 완료");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}