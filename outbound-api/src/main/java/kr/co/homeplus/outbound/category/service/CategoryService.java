package kr.co.homeplus.outbound.category.service;

import com.nhncorp.platform.shopn.ProductServiceStub;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.security.Security;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import kr.co.homeplus.outbound.category.enums.CategoryLevelType;
import kr.co.homeplus.outbound.category.mapper.CategoryMasterMapper;
import kr.co.homeplus.outbound.category.model.MarketCategoryDetailSetDto;
import kr.co.homeplus.outbound.category.model.MarketCategorySetDto;
import kr.co.homeplus.outbound.category.model.sk11st.CategoryGetDto;
import kr.co.homeplus.outbound.category.service.naver.AccessCredentials;
import kr.co.homeplus.outbound.category.service.naver.ExtProductClientStub;
import kr.co.homeplus.outbound.common.enums.MarketType;
import kr.co.homeplus.outbound.core.constants.CodeConstants;
import kr.co.homeplus.outbound.core.constants.UriConstants;
import kr.co.homeplus.outbound.core.db.TransactionManagerName;
import kr.co.homeplus.outbound.core.utility.DateUtil;
import kr.co.homeplus.outbound.core.utility.StringUtil;
import kr.co.homeplus.outbound.response.ResponseResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.axis2.AxisFault;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@Service
@Slf4j
@RequiredArgsConstructor
public class CategoryService {

    private final CategorySendService categorySendService;
    private final CategoryMasterMapper categoryMasterMapper;

    @Value("${plus.resource-routes.gmarket.url}")
    public String gmarketUrl;

    @Value("${plus.resource-routes.auction.url}")
    public String auctionUrl;

    private ExtProductClientStub extProductClientStub;

    private ExtProductClientStub getExtProductClientStub() throws AxisFault {
        if (extProductClientStub == null) {
            Security.addProvider(new BouncyCastleProvider());
            extProductClientStub = new ExtProductClientStub();
            AccessCredentials ac = new AccessCredentials();
            extProductClientStub.setAccessCredentials(ac);	// 인증 클래스 셋팅
        }

        return extProductClientStub;
    }

    
    public ResponseResult setCategory(String partnerId) {
        
        int result = 0;

        switch (partnerId) {
            
            case CodeConstants.ELEVEN_PARTNER_ID :
                result = this.getElevenStCategory(partnerId);    
                break;

            case CodeConstants.AUCTION_PARTNER_ID :
                result = this.getAuctionCategory(partnerId);
                break;
            case CodeConstants.GMARKET_PARTNER_ID :
                result = this.getGmarketCategory(partnerId);
                break;
            case CodeConstants.NMART_PARTNER_ID :
                result = this.getNMartCategory(partnerId);
                break;
            default:
                log.info("잘못된 요청입니다.");
                break;
        }
        
        ResponseResult responseResult = new ResponseResult();
        responseResult.setReturnKey("카테고리 수집 대상 : " + result);

        return responseResult;
    }

    /**
     * 11번가 카테고리 조회/등록
     * @param partnerId
     * @return
     */
    public Integer getElevenStCategory(String partnerId) {

        String curDate = DateUtil.getNowMillYmdHis();

        List<CategoryGetDto> categoryList = categorySendService.getCategoryList().getCategoryList();

        List<CategoryGetDto> lcateList = categoryList.stream().filter(category-> category.getDepth() == 1 ).collect( Collectors.toList());
        List<CategoryGetDto> mcateList = categoryList.stream().filter(category-> category.getDepth() == 2 ).collect( Collectors.toList());
        List<CategoryGetDto> scateList = categoryList.stream().filter(category-> category.getDepth() == 3 ).collect( Collectors.toList());

        List<MarketCategorySetDto> marketCategorySetDtoList = new ArrayList<>();
        List<MarketCategoryDetailSetDto> marketCategoryDetailSetDtoList = new ArrayList<>();

        for (CategoryGetDto dto : categoryList) {
            if (CodeConstants.LEAF_YN_Y.equals(dto.getLeafYn())) {
                marketCategoryDetailSetDtoList.add(setMarketCategoryDetail(dto, lcateList, mcateList, scateList));
            }

            marketCategorySetDtoList.add(
                convertToMarketCategory(partnerId, dto.getDispNo(), dto.getParentDispNo(),
                    dto.getDispNm(), dto.getDepth(), dto.getLeafYn()));
        }

        saveMarketCategory(marketCategorySetDtoList);
        saveMarketCategoryDetail(marketCategoryDetailSetDtoList);

        //연동되지 않은 카테고리는 사용여부를 사용안함으로 처리
        int delCnt = this.marketCategoryUseYnN(partnerId, curDate);

        log.info(
            "11번가 카테고리 전체 : " + marketCategorySetDtoList.size() + " Detail :" + marketCategoryDetailSetDtoList
                .size() + " 삭제 :" + delCnt);

        return categoryList.size();
    }

    /**
     * 11번가 카테고리 디테일 Convert
     * @param dto
     * @param lcate
     * @param mcate
     * @param scate
     * @return
     */
    private MarketCategoryDetailSetDto setMarketCategoryDetail(CategoryGetDto dto,
        List<CategoryGetDto> lcate, List<CategoryGetDto> mcate, List<CategoryGetDto> scate) {

        MarketCategoryDetailSetDto marketCategoryDetailSetDto = new MarketCategoryDetailSetDto();

        marketCategoryDetailSetDto.setPartnerId(MarketType.ELEVEN.getId());
        marketCategoryDetailSetDto.setUseYn(CodeConstants.USE_YN_Y);
        marketCategoryDetailSetDto.setMarketCateCd(dto.getDispNo());

        //leaf
        if (dto.getDepth() == 4) {
            marketCategoryDetailSetDto.setMarketDcateCd(dto.getDispNo());
            marketCategoryDetailSetDto.setMarketDcateNm(dto.getDispNm());
            marketCategoryDetailSetDto.setMarketScateCd(dto.getParentDispNo());

            for (CategoryGetDto small : scate) {
                if ( small.getDispNo().equals(marketCategoryDetailSetDto.getMarketScateCd())) {
                    marketCategoryDetailSetDto.setMarketScateCd(small.getDispNo());
                    marketCategoryDetailSetDto.setMarketScateNm(small.getDispNm());
                    marketCategoryDetailSetDto.setMarketMcateCd(small.getParentDispNo());
                    break;
                }
            }
        } else if (dto.getDepth() == 3) {
            marketCategoryDetailSetDto.setMarketScateCd(dto.getDispNo());
            marketCategoryDetailSetDto.setMarketScateNm(dto.getDispNm());
            marketCategoryDetailSetDto.setMarketMcateCd(dto.getParentDispNo());

        } else {
            throw new IllegalArgumentException("Wrong Category : " + dto.getDepth());
        }

        //middle
        for (CategoryGetDto middle : mcate) {
            if (middle.getDispNo().equals(marketCategoryDetailSetDto.getMarketMcateCd())) {
                marketCategoryDetailSetDto.setMarketMcateCd(middle.getDispNo());
                marketCategoryDetailSetDto.setMarketMcateNm(middle.getDispNm());
                marketCategoryDetailSetDto.setMarketLcateCd(middle.getParentDispNo());
                break;
            }
        }
        //large
        for (CategoryGetDto large : lcate) {
            if (large.getDispNo().equals(marketCategoryDetailSetDto.getMarketLcateCd())) {

                marketCategoryDetailSetDto.setMarketLcateCd(large.getDispNo());
                marketCategoryDetailSetDto.setMarketLcateNm(large.getDispNm());
                break;
            }
        }
        return marketCategoryDetailSetDto;
    }

    /**
     * 옥션 카테고리 조회/저장
     * @param partnerId
     * @return
     */
    private Integer getAuctionCategory(String partnerId) {

        List<MarketCategorySetDto> marketCategorySetDtoList = new ArrayList<>();
        List<MarketCategoryDetailSetDto> marketCategoryDetailSetDto = new ArrayList<>();

        String curDate = DateUtil.getNowMillYmdHis();

        try {

            String url = auctionUrl + UriConstants.AUCTION_CATEGORY;
            NodeList cateList = this.execute(url, "CategoryDetailT");

            for (int i = 0; i < cateList.getLength(); i++) {
                Node node = cateList.item(i);

                if (Node.ELEMENT_NODE == node.getNodeType()) {
                    Element element = (Element) node;

                    String level = element.getAttribute("d2p1:Level");
                    String cateCd = element.getAttribute("d2p1:ID");
                    String cateNm = element.getAttribute("d2p1:Name");

                    if ("L".equals(level)) {
                        marketCategorySetDtoList.add(
                            this.convertToMarketCategory(partnerId, cateCd, "0", cateNm, 1, "N"));
                    } else {

                        //child nodes
                        NodeList childNodes = cateList.item(i).getChildNodes();
                        MarketCategoryDetailSetDto detail = this.setMarketCategoryDetail(childNodes);

                        marketCategorySetDtoList.add(MarketCategorySetDto.builder()
                            .partnerId(partnerId)
                            .cateCd(cateCd)
                            .cateNm(cateNm)
                            .depth(CategoryLevelType.getTypeOf(level).getDepth())
                            .pcateCd(this.getPcateCd(detail, level))
                            .leafYn(element.getAttribute("d2p1:IsLeaf").equals("false") ? CodeConstants.LEAF_YN_N : CodeConstants.LEAF_YN_Y)
                            .useYn(CodeConstants.USE_YN_Y)
                            .build());

                        if ("true".equals(element.getAttribute("d2p1:IsLeaf"))) {

                            detail.setPartnerId(partnerId);
                            detail.setMarketCateCd(cateCd);
                            detail.setUseYn(CodeConstants.USE_YN_Y);

                            marketCategoryDetailSetDto.add(detail);
                        }
                    }
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage());
        }

        saveMarketCategory(marketCategorySetDtoList);
        saveMarketCategoryDetail(marketCategoryDetailSetDto);

        //연동되지 않은 카테고리는 사용여부를 사용안함으로 처리
        int delCnt = this.marketCategoryUseYnN(partnerId, curDate);

        log.info("옥션 카테고리 전체 : " + marketCategorySetDtoList.size() + " Detail :"
            + marketCategoryDetailSetDto.size() + " 삭제 :" + delCnt);

        return marketCategorySetDtoList.size();
    }

    /**
     * G마켓 카테고리 조회/저장
     * @param partnerId
     * @return
     */
    public Integer getGmarketCategory(String partnerId) {

        List<MarketCategorySetDto> marketCategorySetDtoList = new ArrayList<>();
        List<MarketCategoryDetailSetDto> marketCategoryDetailSetDto = new ArrayList<>();

        String curDate = DateUtil.getNowMillYmdHis();

        try {

            String url = gmarketUrl + UriConstants.GMARKET_CATEGORY;
            NodeList largeCateList = this.execute( url, "LARGE_CATEGORY");

            //large category
            for (int i = 0; i < largeCateList.getLength(); i++) {
                Node largeNode = largeCateList.item(i);

                if (Node.ELEMENT_NODE == largeNode.getNodeType()) {
                    Element lElement = (Element) largeNode;

                    marketCategorySetDtoList.add(this.convertToMarketCategory(partnerId,
                        lElement.getAttribute("id"), "0", lElement.getAttribute("name"), 1, CodeConstants.LEAF_YN_N));

                    //middle category
                    NodeList middleList = largeCateList.item(i).getChildNodes();

                    for (int j = 0; j < middleList.getLength(); j++) {
                        Node middleNode = middleList.item(j);

                        if (Node.ELEMENT_NODE == middleNode.getNodeType()) {
                            Element mElement = (Element) middleNode;

                            marketCategorySetDtoList.add(this.convertToMarketCategory(partnerId
                                , mElement.getAttribute("id"), lElement.getAttribute("id")
                                , mElement.getAttribute("name"), 2, CodeConstants.LEAF_YN_N));

                            //small category
                            NodeList smallList = middleList.item(j).getChildNodes();

                            for (int k = 0; k < smallList.getLength(); k++) {

                                Node smallNode = smallList.item(k);

                                if (smallNode.getNodeType() == Node.ELEMENT_NODE) {
                                    Element sElement = (Element) smallNode;

                                    marketCategorySetDtoList
                                        .add(this.convertToMarketCategory(partnerId
                                            , sElement.getAttribute("id"),
                                            mElement.getAttribute("id")
                                            , sElement.getAttribute("name"), 3, CodeConstants.LEAF_YN_Y));

                                    //detail
                                    marketCategoryDetailSetDto.add(
                                        MarketCategoryDetailSetDto.builder()
                                            .partnerId(partnerId)
                                            .marketCateCd(sElement.getAttribute("id"))
                                            .marketLcateCd(lElement.getAttribute("id"))
                                            .marketLcateNm(lElement.getAttribute("name"))
                                            .marketMcateCd(mElement.getAttribute("id"))
                                            .marketMcateNm(mElement.getAttribute("name"))
                                            .marketScateCd(sElement.getAttribute("id"))
                                            .marketScateNm(sElement.getAttribute("name"))
                                            .useYn(CodeConstants.USE_YN_Y)
                                            .build());
                                }
                            }
                        }
                    }
                }
            }

            saveMarketCategory(marketCategorySetDtoList);
            saveMarketCategoryDetail(marketCategoryDetailSetDto);

            //연동되지 않은 카테고리는 사용여부를 사용안함으로 처리
            int delCnt = this.marketCategoryUseYnN(partnerId, curDate);

            log.info("지마켓 카테고리 전체 : " + marketCategorySetDtoList.size() + " Detail :"
                + marketCategoryDetailSetDto.size() + " 삭제 :" + delCnt);

        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }

        return marketCategorySetDtoList.size();
    }

    /**
     * N마트 카테고리 조회/저장
     * @param partnerId
     * @return
     */
    public Integer getNMartCategory(String partnerId) {

        List<MarketCategorySetDto> marketCategorySetDtoList = new ArrayList<>();
        List<MarketCategoryDetailSetDto> marketCategoryDetailSetDto = new ArrayList<>();

        String curDate = DateUtil.getNowMillYmdHis();

        try {
            ProductServiceStub.CategoryType[] list = getExtProductClientStub().getAllCategoryList("N").getCategoryList().getCategory(); // 전체 카테고리 조회
            List<ProductServiceStub.CategoryType> arrResult = Arrays.stream(list).filter(categoryInfo -> categoryInfo.getLast().equalsIgnoreCase("N")).collect(Collectors.toList()); // 부모 카테고리 필터링
            arrResult
                .stream()
                .forEach(category -> {
                    String lCateNm[] = category.getCategoryName().split(">");
                    // 대 카테고리
                    if(lCateNm.length == 1) {
                        // 대 카테고리 정보
                        marketCategorySetDtoList.add(this.convertToMarketCategory(partnerId,
                            category.getId(), "0", category.getName(), 1, CodeConstants.USE_YN_N));

                        Arrays
                            .stream(list)
                            .forEach(subCategory -> {
                                String mCateNm[] = subCategory.getCategoryName().split(">");
                                // 중 카테고리
                                if(mCateNm.length == 2 && lCateNm[0].equalsIgnoreCase(mCateNm[0])) {
                                    // 소 카테고리가 있는지 여부를 체크
                                    if(Arrays.stream(list).filter(data -> data.getCategoryName().split(">").length == 3 && data.getCategoryName().split(">")[1].equalsIgnoreCase(mCateNm[1])).count() > 0) {
                                        // 중 카테고리 정보 ( Leaf :: N )
                                        marketCategorySetDtoList
                                            .add(this.convertToMarketCategory(partnerId
                                                , subCategory.getId(),
                                                category.getId()
                                                , subCategory.getName(), 2, CodeConstants.USE_YN_N));

                                        Arrays
                                            .stream(list)
                                            .forEach(subCategory1 -> {
                                                String sCateNm[] = subCategory1.getCategoryName().split(">");
                                                // 소 카테고리
                                                if(sCateNm.length == 3 && mCateNm[1].equalsIgnoreCase(sCateNm[1])) {
                                                    // 세 카테고리가 있는지 여부를 체크
                                                    if(Arrays.stream(list).filter(data -> data.getCategoryName().split(">").length == 4 && data.getCategoryName().split(">")[2].equalsIgnoreCase(sCateNm[2])).count() > 0) {
                                                        // 소 카테고리 정보 ( Leaf :: N )
                                                        marketCategorySetDtoList
                                                            .add(this.convertToMarketCategory(partnerId
                                                                , subCategory1.getId(),
                                                                subCategory.getId()
                                                                , subCategory1.getName(), 3, CodeConstants.USE_YN_N));

                                                        Arrays
                                                            .stream(list)
                                                            .forEach(subCategory2 -> {
                                                                String dCateNm[] = subCategory2.getCategoryName().split(">");
                                                                // 세 카테고리
                                                                if(dCateNm.length == 4 && sCateNm[2].equalsIgnoreCase(dCateNm[2])) {
                                                                    // 세 카테고리 정보 ( Leaf :: Y )
                                                                    marketCategorySetDtoList
                                                                        .add(this.convertToMarketCategory(partnerId
                                                                            , subCategory2.getId(),
                                                                            subCategory1.getId()
                                                                            , subCategory2.getName(), 4, CodeConstants.USE_YN_Y));

                                                                    // 세 카테고리 디테일
                                                                    marketCategoryDetailSetDto.add(
                                                                        MarketCategoryDetailSetDto.builder()
                                                                            .partnerId(partnerId)
                                                                            .marketCateCd(subCategory2.getId())
                                                                            .marketLcateCd(category.getId())
                                                                            .marketLcateNm(category.getName())
                                                                            .marketMcateCd(subCategory.getId())
                                                                            .marketMcateNm(subCategory.getName())
                                                                            .marketScateCd(subCategory1.getId())
                                                                            .marketScateNm(subCategory1.getName())
                                                                            .marketDcateCd(subCategory2.getId())
                                                                            .marketDcateNm(subCategory2.getName())
                                                                            .useYn(CodeConstants.USE_YN_Y)
                                                                            .build());
                                                                }
                                                            });
                                                    } else {
                                                        // 소 카테고리 정보 ( Leaf :: Y )
                                                        marketCategorySetDtoList
                                                            .add(this.convertToMarketCategory(partnerId
                                                                , subCategory1.getId(),
                                                                subCategory.getId()
                                                                , subCategory1.getName(), 3, CodeConstants.USE_YN_Y));

                                                        // 소 카테고리 디테일
                                                        marketCategoryDetailSetDto.add(
                                                            MarketCategoryDetailSetDto.builder()
                                                                .partnerId(partnerId)
                                                                .marketCateCd(subCategory1.getId())
                                                                .marketLcateCd(category.getId())
                                                                .marketLcateNm(category.getName())
                                                                .marketMcateCd(subCategory.getId())
                                                                .marketMcateNm(subCategory.getName())
                                                                .marketScateCd(subCategory1.getId())
                                                                .marketScateNm(subCategory1.getName())
                                                                .useYn(CodeConstants.USE_YN_Y)
                                                                .build());
                                                    }
                                                }
                                            });
                                    } else {
                                        // 중 카테고리 정보 ( Leaf :: Y )
                                        marketCategorySetDtoList
                                            .add(this.convertToMarketCategory(partnerId
                                                , subCategory.getId(),
                                                category.getId()
                                                , subCategory.getName(), 2, CodeConstants.USE_YN_N));

                                        // 중 카테고리 디테일
                                        marketCategoryDetailSetDto.add(
                                            MarketCategoryDetailSetDto.builder()
                                                .partnerId(partnerId)
                                                .marketCateCd(subCategory.getId())
                                                .marketLcateCd(category.getId())
                                                .marketLcateNm(category.getName())
                                                .marketMcateCd(subCategory.getId())
                                                .marketMcateNm(subCategory.getName())
                                                .useYn(CodeConstants.USE_YN_Y)
                                                .build());
                                    }
                                }
                            });
                    }
                });

            saveMarketCategory(marketCategorySetDtoList);
            saveMarketCategoryDetail(marketCategoryDetailSetDto);

            //연동되지 않은 카테고리는 사용여부를 사용안함으로 처리
            int delCnt = this.marketCategoryUseYnN(partnerId, curDate);

            log.info("N마트 카테고리 전체 : " + marketCategorySetDtoList.size() + " Detail :"
                + marketCategoryDetailSetDto.size() + " 삭제 :" + delCnt);
        } catch (Exception e) {
            log.error("CategoryService getNMartCategory Error :: {}", e.getStackTrace());
            log.error("CategoryService getNMartCategory Error Detail :: {}", e);
        }

        return marketCategorySetDtoList.size();
    }

    /**
     * MarketCategory 저장
     */
    @Transactional(value = TransactionManagerName.ITEM_MASTER, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Integer saveMarketCategory(List<MarketCategorySetDto> marketCategorySetDtoList) {

        int result = 0;

        if (!ObjectUtils.isEmpty(marketCategorySetDtoList)) {
            result = categoryMasterMapper.upsertMarketCategory(marketCategorySetDtoList);
            log.debug("마켓카테고리 저장걸과 : " + result);
        }
        return result;
    }

    /**
     * MarketCategoryDetail 저장
     */
    @Transactional(value = TransactionManagerName.ITEM_MASTER, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Integer saveMarketCategoryDetail(
        List<MarketCategoryDetailSetDto> marketCategoryDetailSetDto) {

        int result = 0;

        if (!ObjectUtils.isEmpty(marketCategoryDetailSetDto)) {
            result = categoryMasterMapper.upsertMarketCategoryDetail(marketCategoryDetailSetDto);
            log.debug("market Category 저장걸과 : " + result);
        }
        return result;

    }

    @Transactional(value = TransactionManagerName.ITEM_MASTER, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int marketCategoryUseYnN(String partnerId, String curDate) {

        int result = 0;
        if(StringUtils.isNotEmpty(partnerId) && StringUtils.isNotEmpty(curDate)) {
            categoryMasterMapper.updateMarketCategoryUseYnN(partnerId, curDate);
            result = categoryMasterMapper.updateMarketCategoryDetailUseYnN(partnerId, curDate); //하나만 저장
        }
        return result;
    }

    /**
     * Set MaretkCategory
     */
    private MarketCategorySetDto convertToMarketCategory(String partnerId, String cateCd,  String pCateCd,  String cateNm, Integer depth, String leafYn) {
        return MarketCategorySetDto.builder()
            .partnerId(partnerId)
            .cateCd(cateCd)
            .pcateCd(pCateCd)
            .cateNm(cateNm)
            .depth(depth)
            .leafYn(leafYn)
            .useYn(CodeConstants.USE_YN_Y)
            .build();
    }

    /**
     * 옥션 Category Detail 조합
     */
    private MarketCategoryDetailSetDto setMarketCategoryDetail(NodeList childNodes) {

        MarketCategoryDetailSetDto detail = new MarketCategoryDetailSetDto();

        for (int i = 0; i < childNodes.getLength(); i++) {

            Node childNode = childNodes.item(i);
            if (Node.ELEMENT_NODE == childNode.getNodeType()) {

                Element childElement = (Element) childNode;
                String tag = childElement.getTagName();
                switch (tag) {
                    case "d2p1:LCategory":
                        detail.setMarketLcateCd(childElement.getAttribute("d2p1:ID"));
                        detail.setMarketLcateNm(childElement.getAttribute("d2p1:Name"));
                        break;

                    case "d2p1:MCategory":

                        detail.setMarketMcateCd(childElement.getAttribute("d2p1:ID"));
                        detail.setMarketMcateNm(childElement.getAttribute("d2p1:Name"));
                        break;

                    case "d2p1:SCategory":

                        detail.setMarketScateCd(childElement.getAttribute("d2p1:ID"));
                        detail.setMarketScateNm(childElement.getAttribute("d2p1:Name"));
                        break;

                    case "d2p1:DCategory":
                        detail.setMarketDcateCd(childElement.getAttribute("d2p1:ID"));
                        detail.setMarketDcateNm(childElement.getAttribute("d2p1:Name"));
                        break;

                    default:
                        break;
                }
            }
        }
        return detail;
    }

    /**
     * Parent Category ID 조회
     */
    private String getPcateCd(MarketCategoryDetailSetDto marketCategoryDetailSetDto, String level) {
        String pCateCd = "";
        if (!StringUtil.isEmpty(level)) {
            switch (level) {
                case "L":
                    pCateCd = "0";
                    break;
                case "M":
                    pCateCd = marketCategoryDetailSetDto.getMarketLcateCd();
                    break;
                case "S":
                    pCateCd = marketCategoryDetailSetDto.getMarketMcateCd();
                    break;
                case "D":
                    pCateCd = marketCategoryDetailSetDto.getMarketScateCd();
                    break;
            }
        }
        return pCateCd;
    }

    /**
     * XML Node Parsing
     * @param url
     * @param tagTitle
     * @return
     */
    private NodeList execute(String url, String tagTitle) throws Exception {

        //Get Document Builder
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document document = null;

        try {
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

            factory.setValidating(true);
            factory.setFeature("http://xml.org/sax/features/validation", true);

            DocumentBuilder builder = factory.newDocumentBuilder();
            URLConnection urlConnection = new URL(url).openConnection();

            document = builder.parse(urlConnection.getInputStream());
            document.getDocumentElement().normalize();

        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        return document.getElementsByTagName(tagTitle);
    }
}

