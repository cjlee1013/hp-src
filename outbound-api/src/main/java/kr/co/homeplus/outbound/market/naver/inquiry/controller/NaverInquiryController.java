package kr.co.homeplus.outbound.market.naver.inquiry.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.outbound.market.naver.inquiry.model.NaverInquiryGetDto;
import kr.co.homeplus.outbound.market.naver.inquiry.model.NaverInquirySetDto;
import kr.co.homeplus.outbound.market.naver.inquiry.service.NaverInquiryService;
import kr.co.homeplus.outbound.market.naver.util.DateUtils;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "마켓연동 > 네이버 > 문의")
@Slf4j
@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class NaverInquiryController {

    private final NaverInquiryService naverInquiryService;

    @ApiOperation(value = "네이버 문의 조회(판매자문의)")
    @PostMapping("/naver/getCustomerInquiryList")
    public ResponseObject<List<NaverInquiryGetDto>> getCustomerInquiryList(@RequestBody @Valid NaverInquirySetDto naverInquirySetDto) {
        if (!(DateUtils.isValid(naverInquirySetDto.getInquiryTimeFrom(), "yyyyMMddHHmmss")
            && DateUtils.isValid(naverInquirySetDto.getInquiryTimeTo(), "yyyyMMddHHmmss"))) {
            log.info("getCustomerInquiryList - invalid search date. [{} ~ {}]", naverInquirySetDto.getInquiryTimeFrom(), naverInquirySetDto.getInquiryTimeTo());

            naverInquirySetDto.setInquiryTimeFrom(new SimpleDateFormat("yyyyMMddHHmmss").format(
                org.apache.commons.lang.time.DateUtils.addHours(DateUtils.convertDate(LocalDateTime.now()), -1)));
            naverInquirySetDto.setInquiryTimeTo(DateUtils.getCurrentDigitYmdHis());
        }

        return ResourceConverter.toResponseObject(naverInquiryService.getCustomerInquiryList(naverInquirySetDto));
    }

    @ApiOperation(value = "네이버 문의 조회(상품문의)")
    @PostMapping("/naver/getProductInquiryList")
    public ResponseObject<List<NaverInquiryGetDto>> getProductInquiryList(@RequestBody @Valid NaverInquirySetDto naverInquirySetDto) {
        if (!(DateUtils.isValid(naverInquirySetDto.getInquiryTimeFrom(), "yyyyMMddHHmmss")
            && DateUtils.isValid(naverInquirySetDto.getInquiryTimeTo(), "yyyyMMddHHmmss"))) {
            log.info("getProductInquiryList - invalid search date. [{} ~ {}]", naverInquirySetDto.getInquiryTimeFrom(), naverInquirySetDto.getInquiryTimeTo());

            naverInquirySetDto.setInquiryTimeFrom(new SimpleDateFormat("yyyyMMddHHmmss").format(
                org.apache.commons.lang.time.DateUtils.addHours(DateUtils.convertDate(LocalDateTime.now()), -1)));
            naverInquirySetDto.setInquiryTimeTo(DateUtils.getCurrentDigitYmdHis());
        }

        return ResourceConverter.toResponseObject(naverInquiryService.getProductInquiryList(naverInquirySetDto));
    }

    @ApiOperation(value = "네이버 문의 답변")
    @PostMapping("/naver/setInquiryAnswer")
    public ResponseObject<NaverInquiryGetDto> setInquiryAnswer(@RequestBody @Valid NaverInquiryGetDto naverInquiryGetDto) {
        try {
            NaverInquiryGetDto returnDto = naverInquiryService.setInquiryAnswer(naverInquiryGetDto);

            if(returnDto.getIssendYn().equals("Y")) {
                return ResourceConverter.toResponseObject(naverInquiryService.setInquiryAnswer(naverInquiryGetDto));
            } else {
                return ResourceConverter.toResponseObject(returnDto, HttpStatus.OK, "FAIL", "문의 답변 에러");
            }
        } catch (Exception e) {
            return ResourceConverter.toResponseObject(naverInquiryGetDto, HttpStatus.OK, "FAIL", "문의 답변 에러");
        }
    }

}
