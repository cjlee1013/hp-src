package kr.co.homeplus.outbound.constants;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * [공통] 에러코드 정의
 * 1001 ~ 8999 사용자 에러코드
 * 9000 ~ 9999 시스템에러 SP에러(9999) 등등
 */
@RequiredArgsConstructor
public enum ExceptionCode {

		SYS_ERROR_CODE_9001("9001", "시스템 에러 : 관리자에게 문의해 주세요.")
	,   SYS_ERROR_CODE_9002("9002", "통신 에러 : 잘못된 URL을 호출 하였습니다.")
	,   SYS_ERROR_CODE_9003("9003", "통신 에러 : Http Method가 잘못 되었습니다.")
	,   SYS_ERROR_CODE_9004("9004", "DB 에러! 재시도 해주세요.")
	,   SYS_ERROR_CODE_9005("9005", "CACHE 에러!")
	,   SYS_ERROR_CODE_9006("9006", "(%d) 통신에 실패 하였습니다. 재시도 해주세요.")
	,   SYS_ERROR_CODE_9007("9007", "(%d) 통신 에러! 재시도 해주세요.")
	,   SYS_ERROR_CODE_9008("9008", "NOSQL 에러! 재시도 해주세요.")
	,   SYS_ERROR_CODE_9009("9009", "(%d) 통신에 실패 하였습니다. 재시도 해주세요. (%s)")
	,   SYS_ERROR_CODE_9999("9999", "DB 에러 : 프로시저 오류! 재시도 해주세요.")

	,   ERROR_CODE_1001("1001", " 파라미터 유효성 검사 오류입니다.")

	,	ERROR_CODE_1002("1002", "마스터 상품정보 조회 실패")
	,	ERROR_CODE_1003("1003", "취급중지상품(판매중지)")
	,	ERROR_CODE_1004("1004", "카테고리 미맵핑상태")
	,	ERROR_CODE_1005("1005", "가상점 단독판매상품 판매중지처리")
	,	ERROR_CODE_1006("1006", "예약상품 판매중지처리")
	,	ERROR_CODE_1007("1007", "이미지 필수값 확인")
	,	ERROR_CODE_1008("1008", "성인상품 판매중지처리 ")
	,	ERROR_CODE_1009("1009", "외부연동 연동설정 중지 상품")
	,	ERROR_CODE_1010("1010", "옵션등록 실패")
	,	ERROR_CODE_1011("1011", "상품등록/수정 연동오류")
	,	ERROR_CODE_1012("1012", "상품등록/수정 유효성검사 미통과")
	,	ERROR_CODE_1013("1013", "상품수정 유효성검사 미통과")
	,	ERROR_CODE_1014("1014", "상품등록/수정 실패")

	,   ERROR_CODE_2001("2001", "전송 데이터가 존재하지 않습니다.")
	,   ERROR_CODE_2002("2002", "자점별 가격 정보 조회 실패.")
	;

	@Getter
	private final String code;
	@Getter
	private final String description;

	@Override
	public String toString() {
		return code + ": " + description;
	}
}
