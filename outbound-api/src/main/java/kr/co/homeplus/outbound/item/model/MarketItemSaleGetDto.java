package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 상품 판매정보 Get Entry
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("상품 지점 정보")
public class MarketItemSaleGetDto {

	@ApiModelProperty(value = "점포코드" )
	private Integer storeId;

	@ApiModelProperty(value = "점포명" )
	private String storeNm;

	@ApiModelProperty(value = "점포유형(클럽/하이퍼)" )
	private String storeAttr;

	@ApiModelProperty(value = "상품번호" )
	private String itemNo;

	@ApiModelProperty(value = "마켓지점코드" )
	private String marketStoreId;

	@ApiModelProperty(value = "정상가격")
	private Long originPrice;

	@ApiModelProperty(value = "매입가")
	private long purchasePrice;

	@ApiModelProperty(value = "판매가격")
	private long salePrice;

	@ApiModelProperty(value ="simple 프로모션 적용가")
	private long simplePrice;

	@ApiModelProperty(value ="즉시할인 적용가(simple 프로모션 진행 시 포함가")
	private long couponPrice;

	@ApiModelProperty(value = "판매수량")
	private int stockQty;

	@ApiModelProperty(value = "재고관리유형")
	private String stockType;

	@ApiModelProperty(value ="택배점 취급여부(취급:Y,취급안함:N)")
	private String dealYn;

	@ApiModelProperty(value = "미취급 상품 여부")
	private String pfrYn;

	@ApiModelProperty(value = "미취급 상품 여부(취급중지 설정시)")
	private String stopDealYn;

	@ApiModelProperty(value = "가상점 단독판매 여부")
	private String virtualStoreOnlyYn;

	@ApiModelProperty(value = "배송정책 정상등록 여부 ")
	private String shipPolicyYn;

	@ApiModelProperty(value = "RMS 행사번호")
	private Long rpmPromoCompDetailId;

	@ApiModelProperty(value = "쿠폰행사번호")
	private Long couponNo;

	@ApiModelProperty(value = "마켓연동최종판매상태", hidden = true)
	private String sellStatus;

}