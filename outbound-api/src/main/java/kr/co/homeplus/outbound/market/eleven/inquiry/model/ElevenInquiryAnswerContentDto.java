package kr.co.homeplus.outbound.market.eleven.inquiry.model;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName("ProductQna")
public class ElevenInquiryAnswerContentDto {
	private String answerCont;
}
