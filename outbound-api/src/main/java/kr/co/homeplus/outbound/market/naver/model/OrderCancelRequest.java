package kr.co.homeplus.outbound.market.naver.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class OrderCancelRequest {
    private List<String> marketOrderItemNoList;
}
