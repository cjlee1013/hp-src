package kr.co.homeplus.outbound.market.eleven.settle.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@XmlAccessorType( XmlAccessType.FIELD )
@XmlRootElement( name = "ResultMessage" )
@Data
public class ElevenCommonResult{
    @XmlElement( name = "result_code", namespace = "http://skt.tmall.business.openapi.spring.service.client.domain/" )
    private String resultCode;

    @XmlElement( name = "result_text", namespace = "http://skt.tmall.business.openapi.spring.service.client.domain/" )
    private String resultMessage;
}
