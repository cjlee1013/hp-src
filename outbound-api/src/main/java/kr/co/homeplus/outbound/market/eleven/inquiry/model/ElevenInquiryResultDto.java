package kr.co.homeplus.outbound.market.eleven.inquiry.model;

import lombok.Data;

@Data
public class ElevenInquiryResultDto{

    /**
     * 답변 내용
     */
    private String answerCont;

    /**
     * 답변 처리일자
     */
    private String answerDt;

    /**
     * 처리 상태
     * Y : 답변완료
     * N : 미답변
     */
    private String answerYn;

    /**
     * 상품 번호
     */
    private String brdInfoClfNo;

    /**
     * 질문 내용
     */
    private String brdInfoCont;

    /**
     * QnA 글 번호
     */
    private long brdInfoNo;

    /**
     * 제목
     */
    private String brdInfoSbjct;

    /**
     * 구매 여부
     * Y : 구매
     * N : 미구매
     */
    private String buyYn;

    /**
     * 문의일자
     */
    private String createDt;

    /**
     * 전시상태
     * Y : 전시
     * N : 전시안함
     */
    private String dispYn;

    /**
     * 고객ID
     */
    private String memID;

    /**
     * 고객 이름
     */
    private String memNM;

    /**
     * 상품명
     */
    private String prdNm;

    /**
     * 문의 유형 코드
     * 01 : 상품
     * 02 : 배송
     * 03 : 반품/환불/취소
     * 04 : 교환/변경
     * 05 : 기타
     */
    private String qnaDtlsCd;

    /**
     * 문의 유형
     */
    private String qnaDtlsCdNm;
}
