package kr.co.homeplus.outbound.market.eleven.inquiry.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.core.ParameterizedTypeReference;

@Data
public class ElevenInquiryAlimiResultDto {
	  /**
     * 게시물 일련번호
     */
    private long emerNtceSeq;

    /**
     * 알리미 유형
     */
    private String emerTypeCd;

    /**
     * 게시물 대분류
     */
    private String emerNtceCd;

    /**
     * 게시물 소분류
     */
    private String emerNtceDtlCd;

    /**
     * 제목
     */
    private String emerNtceSubject;

    /**
     * 알림일시 (등록일)
     */
    private String createDt;

    /**
     * 회신기한
     */
    private String emerReplyDt;

    /**
     * 처리상태
     */
    private String emerNtceCrntCd;

    /**
     * 상품명
     */
    private String prdNm;

    /**
     * 주문번호
     */
    private String ordNo;

    /**
     * 구매자ID
     */
    private String memId;

    /**
     * 구매자명
     */
    private String memNm;

    /**
     * 작성자 부서명
     */
    private String deptNm;

    /**
     * 작성자명
     */
    private String empNm;

    /**
     * 알림 내용
     */
    private String emerCtnt;
    
    /**
     * 답변내용 리스트
     */
    private ElevenInquiryAlimiReplyDto emerReplyList;
}
