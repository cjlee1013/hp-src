package kr.co.homeplus.outbound.core.maketLog.advice;

import java.io.StringWriter;
import java.lang.reflect.Method;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import kr.co.homeplus.outbound.core.constants.CodeConstants;
import kr.co.homeplus.outbound.core.maketLog.annotation.MarketLogTarget;
import kr.co.homeplus.outbound.core.maketLog.service.MarketItemLogService;
import kr.co.homeplus.outbound.item.model.ItemSpecHistParamDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.ObjectUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
@RequiredArgsConstructor
public class MarketLogAspect {

    private final MarketItemLogService marketItemLogService;

    @Around("@annotation(kr.co.homeplus.outbound.core.maketLog.annotation.MarketLogTarget)")
    public Object saveLog(final ProceedingJoinPoint pj) throws Throwable {

        final Object response = pj.proceed();

        final MethodSignature signature = (MethodSignature) pj.getSignature();
        final Method method = signature.getMethod();
        final Object[] request = pj.getArgs();

        final MarketLogTarget annotaion = method.getAnnotation(MarketLogTarget.class);
        StringWriter sw = new StringWriter();

        try {
            switch (annotaion.partnerId()) {
                case CodeConstants.ELEVEN_PARTNER_ID:
                case CodeConstants.NMART_PARTNER_ID:
                    JAXBContext context = JAXBContext.newInstance(annotaion.reqClass());
                    Marshaller marshaller = context.createMarshaller();

                    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                    if (CodeConstants.ELEVEN_PARTNER_ID.equals(annotaion.partnerId())){
                        marshaller.setProperty(Marshaller.JAXB_ENCODING, "EUC-KR");
                    }

                    marshaller.marshal(request[0], sw);
                    break;
            }

            marketItemLogService.setItemSpecHist(ItemSpecHistParamDto.builder()
                .methodTxt(annotaion.methodTxt())
                .partnerId(annotaion.partnerId())
                .spec(ObjectUtils.isNotEmpty(sw) ? sw.toString() : "")
                .request(request[0])
                .itemNo((String) request[1])
                .response(response).build());

        } catch (Exception e ) {
            log.error("## marketLogService Error : " + e.getMessage());
        }

        return response;
    }
}
