package kr.co.homeplus.outbound.cert.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kr.co.homeplus.outbound.cert.service.ItemSafeCertService;
import kr.co.homeplus.outbound.response.ResponseResult;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "상품 안전인증 조회")
@ApiResponses(value = {
            @ApiResponse(code = 404, message = "Not Found")
        ,   @ApiResponse(code = 405, message = "Method Not Allowed")
        ,   @ApiResponse(code = 422, message = "Unprocessable Entity")
        ,   @ApiResponse(code = 500, message = "Internal Server Error")
})

@RequestMapping("/item")
@RestController
@RequiredArgsConstructor
public class ItemSafeCertController {
    private final ItemSafeCertService itemSafeCertService;

    @ApiOperation(value = "전파적합성평가 인증조회(전파인증) ", response = ResponseResult.class)
    @GetMapping(value = "/checkItemRadioCert")
    public ResponseObject checkItemRadioCert(
        @RequestParam @ApiParam(name = "certNo", value = "인증번호", required = true) String certNo) {
        return ResourceConverter.toResponseObject(itemSafeCertService.checkItemRadioCert(certNo));
    }

    @ApiOperation(value = "안전인증정보 조회(KC인증)", response = ResponseResult.class)
    @GetMapping(value = "/checkItemSafeCert")
    public ResponseObject checkItemSafeCert(
        @RequestParam @ApiParam(name = "certNo", value = "인증번호", required = true) String certNo) throws Exception{
        return ResourceConverter.toResponseObject(itemSafeCertService.checkItemSafeCert(certNo));
    }

    @ApiOperation(value = "생활인증", response = ResponseResult.class)
    @GetMapping(value = "/checkItemLifeCert")
    public ResponseObject checkItemLifeCert(
        @RequestParam @ApiParam(name = "certNo", value = "인증번호", required = true) String certNo) throws Exception{
        return ResourceConverter.toResponseObject(itemSafeCertService.checkItemLifeCert(certNo));
    }

    @ApiOperation(value = "안전인증조회 ", response = ResponseResult.class)
    @GetMapping(value = "/checkItemCert")
    public ResponseObject checkItemLifeCert(
        @RequestParam @ApiParam(name = "certGroup", value = "인증그룹", required = true) String certGroup,
        @RequestParam @ApiParam(name = "certNo", value = "인증번호", required = true) String certNo) {
        return ResourceConverter
            .toResponseObject(itemSafeCertService.checkItemCert(certGroup, certNo));
    }
}

