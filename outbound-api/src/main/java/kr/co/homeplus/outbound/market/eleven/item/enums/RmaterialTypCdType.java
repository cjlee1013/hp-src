package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

/**
 * 원재료 유형 코드
 */
public enum RmaterialTypCdType {
	@XmlEnumValue("01")
	AGRICULTURAL("01", "농산물"),

	@XmlEnumValue("02")
	AQUATICPRODUCTS("02", "수산물"),

	@XmlEnumValue("03")
	PROCESS("03", "가공품"),

	@XmlEnumValue("04")
	NOTDISPLAY("04", "원산지 의무 표시대상 아님"),

	@XmlEnumValue("05")
	REFDETAIL("05", "상품별 원산지는 상세설명 참조");

	@Getter
	private String code;

	@Getter
	private String desc;

	private RmaterialTypCdType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}
