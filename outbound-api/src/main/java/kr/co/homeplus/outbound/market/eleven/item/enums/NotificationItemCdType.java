package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

/**
 * 고시항목 기본 Default
 */
public enum NotificationItemCdType {
    @XmlEnumValue( "42154823" )
    IMPORTED_FOOD( "42154823", "상품상세설명 참조" ),

    @XmlEnumValue( "42155152" )
    PACKING_UNIT( "42155152", "상품상세설명 참조" ),

    @XmlEnumValue( "23756754" )
    CONSUMER( "23756754", "상품상세설명 참조" ),

    @XmlEnumValue( "23757095" )
    NUTRIENT( "23757095", "상품상세설명 참조" ),

    @XmlEnumValue( "23757000" )
    FOOD_TYPE( "23757000", "상품상세설명 참조" ),

    @XmlEnumValue( "23757245" )
    ORIGINAL_MATERIAL( "23757245", "상품상세설명 참조" ),

    @XmlEnumValue( "23757260" )
    GENETICALLY( "23757260", "상품상세설명 참조" ),

    @XmlEnumValue( "176317774" )
    PRODUCT_NAME( "176317774", "상품상세설명 참조" ),

    @XmlEnumValue( "176312674" )
    WARNING_MSG( "176312674", "상품상세설명 참조" ),

    @XmlEnumValue( "176400445" )
    MAKER( "176400445", "상품상세설명 참조" ),

    @XmlEnumValue( "176398001" )
    EXPIRE_DT( "176398001", "상품상세설명 참조" );

    @Getter
    private String code;
    @Getter
    private String desc;

    private NotificationItemCdType( String code, String desc ) {
        this.desc = desc;
        this.code = code;
    }
}
