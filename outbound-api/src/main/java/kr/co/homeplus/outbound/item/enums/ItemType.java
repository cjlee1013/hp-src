package kr.co.homeplus.outbound.item.enums;

import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.collect.ImmutableBiMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ItemType {

        N( "N", "새상품")
    ,   U("U", "중고")
    ,   R("R", "리퍼")
    ,   B("B", "반품(리세일)")
    ,   C("C", "주문제작")
    ,   T("T", "렌탈")
    ,   M("M", "휴대폰,")
    ,   E("E", "e-ticket")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;

    @Getter
    private static final ImmutableBiMap<ItemType, String> biMap;

    static {
        BiMap<ItemType, String> map = EnumHashBiMap.create(ItemType.class);

        for (ItemType policy : values()) {
            map.put(policy, policy.name());
        }

        biMap = ImmutableBiMap.copyOf(map);
    }

    public static String valueDescFor(final String type) {
        final ItemType policy =  biMap.inverse().get(type);

        return policy.description;
    }
}
