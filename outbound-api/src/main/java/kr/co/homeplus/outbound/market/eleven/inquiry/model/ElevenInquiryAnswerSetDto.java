package kr.co.homeplus.outbound.market.eleven.inquiry.model;

import lombok.Data;

@Data
public class ElevenInquiryAnswerSetDto {

  /**
   * Qna글번호
   */
	private String brdInfoNo;

	/**
	 * 상품번호
	 */
  private String prdNo;

  /**
   * 답변내용
   */
  private String content;
}
