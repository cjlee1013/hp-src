package kr.co.homeplus.outbound.market.eleven.order.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@XmlRootElement(name = "order")
@XmlAccessorType(XmlAccessType.FIELD)
public class ElevenOrder {
    /**
     * 추가구성 상품 유무
     * Y : 추가구성 상품 있음
     * N : 추가구성 상품 없음
     */
    private String addPrdYn;

    /**
     * 희망배송시간
     * HHMM 시분
     */
    private String bgnTm;

    /**
     * 묶음 배송 일련번호
     */
    private Integer bndlDlvSeq;

    /**
     * 묶음 배송 유무
     * Y : 묶음 배송
     * N : 개별 배송
     */
    private String bndlDlvYN;

    /**
     * 배송비
     */
    private Integer dlvCst;

    /**
     * 배송비 착불 여부
     * 01 : 선불
     * 02 : 착불
     * 03 : 무료
     */
    private String dlvCstType;

    /**
     * 희망배송일
     * yyyyMMdd
     */
    private String dlvDt;

    /**
     * 배송번호
     */
    private String dlvNo;

    /**
     *
     */
    private String giftCd;

    /**
     * 마트 배송코드
     * 00 : 근거리
     * 01 : 택배
     * 02 : 업체배송
     * 03 : 업체직배송
     * 04 : 냉장
     */
    private String martDlvCd;

    /**
     * 회원ID
     */
    private String memID;

    /**
     * 주문 총액 : 판매단가 * 수량 ( 주문 - 취소 - 반품 ) + 옵션가
     */
    private Integer ordAmt;

    /**
     * 주문자 기본 주소
     */
    private String ordBaseAddr;

    /**
     * 배송시 요청사항
     */
    private String ordDlvReqCont;

    /**
     * 주문 일시
     */
    private String ordDt;

    /**
     * 구매자 상세주소
     */
    private String ordDtlsAddr;

    /**
     * 구매자 우편주소
     */
    private String ordMailNo;

    /**
     * 구매자 이름
     */
    private String ordNm;

    /**
     * 11번가 주문번호
     */
    private String ordNo;

    /**
     * 주문 상품 옵션 결제 금액
     */
    private Integer ordOptWonStl;

    /**
     * 결제 금액 : 주문 금액 + 배송비 - 판매자 할인 금액 - mo쿠폰
     */
    private Integer ordPayAmt;

    /**
     * 주문 순번
     */
    private Integer ordPrdSeq;

    /**
     * 구매자 휴대폰 번호
     */
    private String ordPrtblTel;

    /**
     * 주문 수량
     */
    private Integer ordQty;

    /**
     * 결제 완료일시
     */
    private String ordStlEndDt;

    /**
     * 주문자 전호번호
     */
    private String ordTlphnNo;

    /**
     * 발주확인일시
     */
    private String plcodrCnfDt;

    /**
     * 상품명
     */
    private String prdNm;

    /**
     * 배송 기본주소
     */
    private String rcvrBaseAddr;

    /**
     * 배송 상세주소
     */
    private String rcvrDtlsAddr;

    /**
     * 배송지 우편번호
     */
    private String rcvrMailNo;

    /**
     * 수령자명
     */
    private String rcvrNm;

    /**
     * 수령자 핸드폰번호
     */
    private String rcvrPrtblNo;

    /**
     * 수령자 전화번호
     */
    private String rcvrTlphn;

    /**
     * 판매가(객단가)
     */
    private Integer selPrc;

    /**
     * 판매자 할인금액
     */
    private Integer sellerDscPrc;

    /**
     * 판매자 상품번호
     */
    private String sellerPrdCd;

    /**
     * 주문 상품 옵션명
     */
    private String slctPrdOptNm;

    /**
     * 마트점포코드
     */
    private String strNo;

    /**
     * 11번가 할인 금액
     */
    private Integer tmallDscPrc;

    /**
     * 홈플러스 프로모션 번호
     */
    private String martPrmtNo;

    /**
     * 홈플러스 프로모션 명칭
     */
    private String martPrmtNm;

    /**
     * 11번가상품번호
     */
    private String prdNo;

    /**
     * 예약배송종료시간
     */
    private String endTM;

    /**
     * 대체안함선택여부
     */
    private String prdSoldOutCd;


    private Long prdStockNo;
    private String sellerStockCd;
    private String martPrmtCd;
    private String giftNo;


}
