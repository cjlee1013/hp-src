package kr.co.homeplus.outbound.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 마켓연동 ID Enum
 * (ID 변경 시 변경 필요)
 */
@RequiredArgsConstructor
public enum MarketType {
        ELEVEN( "coopeleven", "11번가")
    ,   GMAREKT( "coopgmarket", "G마켓")
    ,   AUCTION( "coopauction", "옥션") ;

    @Getter
    private final String id;

    @Getter
    private final String description;

}
