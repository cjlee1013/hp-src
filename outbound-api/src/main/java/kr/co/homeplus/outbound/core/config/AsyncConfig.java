package kr.co.homeplus.outbound.core.config;


import java.util.concurrent.Executor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


@Configuration
@EnableAsync
public class AsyncConfig {

    @Value("${api.async.corePoolSize}")
    private int corePoolSize;

    @Value("${api.async.maxPoolSize}")
    private int maxPoolSize;

    @Value("${api.async.queueCapacity}")
    private int queueCapacity;


    @Bean
    public Executor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(corePoolSize);
        taskExecutor.setMaxPoolSize(maxPoolSize);
        taskExecutor.setQueueCapacity(queueCapacity);
        taskExecutor.setThreadNamePrefix("Async Executor-");
        taskExecutor.initialize();
        return taskExecutor;
    }
}
