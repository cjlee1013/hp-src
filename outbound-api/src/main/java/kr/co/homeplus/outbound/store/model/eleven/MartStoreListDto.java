package kr.co.homeplus.outbound.store.model.eleven;

import io.swagger.annotations.ApiModel;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@ApiModel(value = "11번가지점 등록수정")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "MartStoreList")
public class MartStoreListDto {

    @XmlElement(name="store")
    private List<MartStoreGetDto> store;
}

