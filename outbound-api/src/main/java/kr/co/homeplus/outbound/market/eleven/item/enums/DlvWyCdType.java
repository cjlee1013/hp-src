package kr.co.homeplus.outbound.market.eleven.item.enums;

import io.swagger.annotations.ApiModel;
import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

@ApiModel("배송방법")
public enum DlvWyCdType {
	@XmlEnumValue("01")
	PARCEL("01", "택배"),

	@XmlEnumValue("02")
	POST("02", "우편(소포/등기)"),

	@XmlEnumValue("03")
	DIRECT("03", "직접전달(화물택배)"),

	@XmlEnumValue("04")
	QUICK("04", "퀵서비스"),

	@XmlEnumValue("05")
	NECESSARY("05", "필요없음");

	@Getter
	private String code;
	@Getter
	private String desc;

	private DlvWyCdType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}
