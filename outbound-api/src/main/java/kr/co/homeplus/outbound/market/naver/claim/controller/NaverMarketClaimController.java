package kr.co.homeplus.outbound.market.naver.claim.controller;

import com.nhncorp.platform.shopn.seller.SellerServiceStub.ClaimRequestReasonType;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ProductOrderChangeType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverCancelReqDto;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverClaimDataDto;
import kr.co.homeplus.outbound.market.naver.claim.service.NaverClaimService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "마켓연동 > 네이버")
@Slf4j
@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class NaverMarketClaimController {

    private final NaverClaimService naverClaimService;

    @ApiOperation(value = "네이버 클레임조회")
    @GetMapping("/naver/getClaimRequestList")
    public ResponseObject<List<NaverClaimDataDto>> getClaimRequestList(
        @RequestParam("startDt") String startDt,
        @RequestParam("endDt") String endDt,
        @RequestParam("productOrderChangeType") ProductOrderChangeType productOrderChangeType
    ) throws Exception {
        return ResourceConverter.toResponseObject(naverClaimService.getChangedProductOrderList(startDt, endDt, productOrderChangeType));
    }

    @ApiOperation(value = "네이버 클레임 상품 조회")
    @GetMapping("/naver/getMarketClaimStatus")
    public ResponseObject<List<NaverClaimDataDto>> getMarketClaimStatus(
        @RequestParam("productOrderItemNo") String productOrderItemNo
    ) throws Exception {
        return ResourceConverter.toResponseObject(naverClaimService.getProductOrderList(productOrderItemNo));
    }

    @ApiOperation(value = "네이버 판매취소")
    @PostMapping("/naver/setCancelSale")
    public ResponseObject<String> setCancelSale(@RequestBody NaverCancelReqDto reqDto)throws Exception {
        return ResourceConverter.toResponseObject(naverClaimService.setCancelSale(reqDto.getMarketOrderItemNo(), reqDto.getClaimReasonType()));
    }

    @ApiOperation(value = "네이버 판매취소")
    @PostMapping("/naver/setReturnSale")
    public ResponseObject<String> setReturnSale(@RequestBody NaverCancelReqDto reqDto)throws Exception {
        return ResourceConverter.toResponseObject(naverClaimService.setRequestReturn(reqDto.getMarketOrderItemNo(), reqDto.getClaimReasonType()));
    }

    @ApiOperation(value = "네이버 취소/반품 승인")
    @PostMapping("/naver/setClaimApprove")
    public ResponseObject<String> setClaimApprove(@RequestBody NaverCancelReqDto reqDto)throws Exception {
        return ResourceConverter.toResponseObject(naverClaimService.setClaimApprove(reqDto.getMarketOrderItemNo(), reqDto.getClaimType()));
    }

    @ApiOperation(value = "네이버 취소/반품/교환 거절")
    @PostMapping("/naver/setClaimReject")
    public ResponseObject<String> setClaimReject(@RequestBody NaverCancelReqDto reqDto)throws Exception {
        return ResourceConverter.toResponseObject(naverClaimService.setClaimReject(reqDto.getMarketOrderItemNo(), reqDto.getClaimType(), reqDto.getClaimDetailReason()));
    }

    @ApiOperation(value = "네이버 반품/교환 보류")
    @PostMapping("/naver/setClaimHold")
    public ResponseObject<String> setClaimHold(@RequestBody NaverCancelReqDto reqDto)throws Exception {
        return ResourceConverter.toResponseObject(naverClaimService.setClaimHold(reqDto.getMarketOrderItemNo(), reqDto.getClaimType(), reqDto.getClaimReasonType(), reqDto.getClaimDetailReason()));
    }

    @ApiOperation(value = "네이버 취소/반품/교환 취소")
    @PostMapping("/naver/setClaimWithdraw")
    public ResponseObject<String> setClaimWithdraw(@RequestBody NaverCancelReqDto reqDto)throws Exception {
        return ResourceConverter.toResponseObject(naverClaimService.setClaimReleaseHold(reqDto.getMarketOrderItemNo(), reqDto.getClaimType()));
    }

    @ApiOperation(value = "네이버 교환완료")
    @PostMapping("/naver/setExchangeCollect")
    public ResponseObject<String> setExchangeCollect(@RequestBody NaverCancelReqDto reqDto)throws Exception {
        return ResourceConverter.toResponseObject(naverClaimService.setExchangeApprove(reqDto.getMarketOrderItemNo()));
    }

}
