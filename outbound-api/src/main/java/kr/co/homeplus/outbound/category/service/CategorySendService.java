package kr.co.homeplus.outbound.category.service;

import kr.co.homeplus.outbound.category.model.sk11st.CategoryListGetDto;

public interface CategorySendService {

    CategoryListGetDto getCategoryList();
}

