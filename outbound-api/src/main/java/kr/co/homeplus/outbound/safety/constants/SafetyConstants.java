package kr.co.homeplus.outbound.safety.constants;

public class SafetyConstants {

    // profiles
    public static final String PROFILES_LOCAL = "local";
    public static final String PROFILES_DEV = "dev";
    public static final String PROFILES_QA = "qa";
    public static final String PROFILES_STG = "stg";
    public static final String PROFILES_PRD = "prd";

    // number
    public static final int FIVE = 5;
    public static final int FIFTEEN = 15;

    public static final String EMPTY_STRING = "";

}
