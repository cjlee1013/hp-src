package kr.co.homeplus.outbound.market.eleven.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URL;
import java.text.MessageFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.net.ssl.HttpsURLConnection;
import kr.co.homeplus.outbound.core.utility.ObjectUtils;
import kr.co.homeplus.outbound.market.eleven.config.MarketElevenConfig;
import kr.co.homeplus.outbound.market.eleven.enums.ElevenClaimUriInfo;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

@Slf4j
@Component
@RequiredArgsConstructor
public class ElevenConnectUtil {

    private final MarketElevenConfig marketElevenConfig;
    private static MarketElevenConfig elevenConfig;

    // static 으로 config를 사용하기 위해서 post Construct 함.
    @PostConstruct
    private void initialize() {
        elevenConfig = marketElevenConfig;
    }

    /**
     * 통신 url 생성
     *  - 11번가 통신시 파라미터가 슬러시(/) 로 구분되어 요청되므로
     *  가변인자를 입력받아 생성하도록 함.
     *
     * @param url 기본 url
     * @param parameter 요청파라미터(가변인자)
     * @return 통신 가능 url
     * @throws Exception 오류 처리.
     */
    public static URL createURL(String url, Object... parameter) throws Exception {
        try {
            StringBuilder urlBuilder = new StringBuilder();
            urlBuilder.append(elevenConfig.getTargetEndpoint()).append(url);
            // 입력받은 파라미터 만큼 반복하여 생성.
            for(Object param : parameter) {
                urlBuilder.append("/");
                // 문자열로 변경하여 저장.
                urlBuilder.append(String.valueOf(param));
            }
            return new URL(urlBuilder.toString());
        } catch (Exception e) {
            log.error("create URL Error : {}", e.getMessage());
            throw new Exception("URL 생성실패");
        }
    }

    public static URL createURL(ElevenClaimUriInfo url, Map<String, Object> parameter) throws Exception {
        try {
            String basicUrl = elevenConfig.getTargetEndpoint().concat(url.getUrl());
            // 입력받은 파라미터 만큼 반복하여 생성.
            for(String key : parameter.keySet()){
                if(basicUrl.contains(key)){
                    basicUrl = basicUrl.replace(addParameterKey(key), ObjectUtils.toString(parameter.get(key)));
                }
            }
            return new URL(basicUrl);
        } catch (Exception e) {
            log.error("create URL Error : {}", e.getMessage());
            throw new Exception("URL 생성실패");
        }
    }

    /**
     * 통신요청 (실행)
     * - 처리를 편리하게 하기위해 ResponeObject를 사용하여 응답받음.
     *
     * @param url 생성된 통신가능 url
     * @param returnClass<T> 응답DTO
     * @return ReponseObject
     * @throws Exception 오류 처리.
     */
    public static <T> ResponseObject<T> execute(URL url, Class<T> returnClass) throws Exception {
        try {
            // 자체 생성한 xml 파싱 함수를 사용하여 처리.
            return createResponse(xmlParsingData(executeSsl(url), returnClass));
        } catch (JsonMappingException e) {
            log.error("JsonMappingException :: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            log.error("JsonProcessingException :: {}", e.getMessage());
        } catch (Exception e) {
            log.error("Exception :: {}", e.getMessage());
        }
        return ResponseObject.Builder.<T>builder().data(null).returnCode("9999").returnMessage("통신 오류가 발생하였습니다.").build();
    }

    /**
     * 통신요청 (실행)
     * - 처리를 편리하게 하기위해 ResponeObject를 사용하여 응답받음.
     *
     * @param url 생성된 통신가능 url
     * @param returnClass<T> 응답DTO
     * @return ReponseObject
     * @throws Exception 오류 처리.
     */
    public static <T> ResponseObject<T> execute(URL url, TypeReference<T> typeReference) throws Exception {
        try {
            return createResponse(xmlParsingData(executeSsl(url), typeReference));
        } catch (JsonMappingException e) {
            log.error("JsonMappingException :: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            log.error("JsonProcessingException :: {}", e.getMessage());
        } catch (Exception e) {
            log.error("Exception :: {}", e.getMessage());
        }
        return ResponseObject.Builder.<T>builder().data(null).returnCode("9999").returnMessage("통신 오류가 발생하였습니다.").build();
    }

    /**
     * https 통신용
     *
     * @param urlInfo
     * @return
     * @throws Exception
     */
    private static String executeSsl(URL urlInfo) throws Exception {
        HttpsURLConnection sslUrlConnection = (HttpsURLConnection)urlInfo.openConnection();
        sslUrlConnection.setRequestMethod(RequestMethod.GET.name());
        sslUrlConnection.setRequestProperty("openapiKey", elevenConfig.getApiKey());
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(sslUrlConnection.getInputStream(), "EUC-KR"))) {
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (Exception e) {
            log.error("[11st][{}] connect fail :: {}", urlInfo.getPath(), ExceptionUtils.getStackTrace(e));
            throw new Exception("ssl 통신 오류...");
        }
        return stringBuilder.toString();
    }

    /**
     * xml파싱
     * - 입력받은 class를 사용하여 xml을 Parsing 한다.
     * @param xmlData
     * @param returnClass<T>
     * @return
     * @throws Exception
     */
    private static <T> T xmlParsingData(String xmlData, Class<T> returnClass) throws Exception {
        ObjectMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        log.info("{}", returnClass.getSimpleName());
        return mapper.readValue(xmlData, returnClass);
    }

    /**
     * xml파싱
     * - 입력받은 class를 사용하여 xml을 Parsing 한다.
     * @param xmlData
     * @param returnClass<T>
     * @return
     * @throws Exception
     */
    private static <T> T xmlParsingData(String xmlData, TypeReference<T> typeReference) throws Exception {
        ObjectMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.readValue(xmlData, typeReference);
    }

    /**
     * restTemplate 용.
     *
     * @param returnClass
     * @param <T>
     * @return
     */
    @Deprecated
    private static <T> ParameterizedTypeReference<T> createType(Class<T> returnClass){
        ResolvableType resolvableType = ResolvableType.forClass(returnClass);
        return ParameterizedTypeReference.forType(resolvableType.getType());
    }

    /**
     * 응답 데이터 생성
     * - xml로 입력받은 데이터를 확인하여
     *   정상/오류에 따라 분기처리
     *
     * @param data<T> 응답데이터
     * @return ResponseObject
     * @throws Exception 오류처리
     */
    private static <T> ResponseObject<T> createResponse(T data) throws Exception {
        try {
            // 입력받은 데이터를 LinkedHashMap으로 변환.(응답 코드/메시지 확인을 위함)
            LinkedHashMap<String, Object> resultMap = ObjectUtils.getConvertMap(data);
            // 변환한 MAP에 result_code가 있는 지 여부 확인.
            // 없으면 정상, 있으면 오류
            if(resultMap.getOrDefault("result_code", null) == null) {
                return ResponseObject.Builder.<T>builder().data(data).returnCode("SUCCESS").returnMessage("통신에 성공하였습니다.").build();
            } else {
                return ResponseObject.Builder.<T>builder().data(null).returnCode(ObjectUtils.toString(resultMap.get("result_code"))).returnMessage(ObjectUtils.toString(resultMap.get("result_text"))).build();
            }
        } catch (Exception e) {
            throw new Exception("결과체크 오류");
        }
    }

    private static String addParameterKey(String key) {
        return MessageFormat.format("[{0}]", key);
    }

    private static <T> Class<T> getGenericClass(ParameterizedType parameterizedType) throws Exception {
        Type type = parameterizedType.getActualTypeArguments()[0];
        log.info("1 {}", type.getTypeName());
        try {
            while(type instanceof ParameterizedType){
                type = ((ParameterizedType)type).getActualTypeArguments()[0];
                log.info("2 {}", type.getTypeName());
            }
        } catch (Exception e) {
            log.info("{}", e.getMessage());
        }
        log.info("3 {}", type.getTypeName());
        return (Class<T>)Class.forName(type.getTypeName());
    }
}
