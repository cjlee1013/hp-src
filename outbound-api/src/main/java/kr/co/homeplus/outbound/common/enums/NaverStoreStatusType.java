package kr.co.homeplus.outbound.common.enums;

/**
 * <pre>
 * Class Name : NaverStoreStatusType.java
 * Description : 상점 상태 정보
 *
 *  Modification Information
 *  Modify Date 		Modifier	Comment
 *  -----------------------------------------------
 *  2021. 5. 27.        jh6108.kim   신규작성
 * </pre>
 *
 * @author jh6108.kim
 * @since 2021. 5. 27.
 */
public enum NaverStoreStatusType {
    ACTIVE("1", "ACTIVE")
    ,   DISABLE("2", "DISABLE")
    ,   DELETE("3", "DELETE")
    ,   SALE("SALE", "판매중")
    ,   SUSP("SUSP", "판매중지")
    ,   OSTK("OSTK", "품절")

    ;

    private String code;
    private String desc;

    public String getDesc() {
        return desc;
    }

    public String getCode() {
        return code;
    }

    NaverStoreStatusType(String code, String desc) {
        this.desc = desc;
        this.code = code;
    }
}
