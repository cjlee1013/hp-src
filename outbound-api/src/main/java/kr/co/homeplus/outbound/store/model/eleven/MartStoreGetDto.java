package kr.co.homeplus.outbound.store.model.eleven;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "11번가지점 등록수정")
public class MartStoreGetDto {

    @ApiModelProperty(value = "마트번호")
    private String martNo;

    @ApiModelProperty(value = "지점번호")
    private String strNo;

    @ApiModelProperty(value = "지점명(최대 한글25자,영/숫자50자)")
    private String strNm;

    @ApiModelProperty(value = "사업자명 영수증출력용")
    private String bsnsNm;

    @ApiModelProperty(value = "사업자등록번호 - 없이 숫자만")
    private String cobsEnprNo;

    @ApiModelProperty(value = "대표자명")
    private String cobsRptv;

    @ApiModelProperty(value = "대표전화번호")
    private String rptvTlphnNo;

    @ApiModelProperty(value = "우편번호 -없이 숫자만")
    private String mailNo;

    @ApiModelProperty(value = "기본주소 (최대 한글 100자 영/숫자 200자)")
    private String baseAddr;

    @ApiModelProperty(value = "상세주소 (최대 한글 100자 영/숫자 200자)")
    private String dtlsAddr;

    @ApiModelProperty(value = "마트배송코드")
    private String martDlvCd;

    @ApiModelProperty(value = "배송회차사용여부")
    private String dlvLotUseYn;

    @ApiModelProperty(value = "셀러점포코드")
    private String sellerStrCd;

    @ApiModelProperty(value = "배송지역안내")
    private String dlvAreaCont;

    @ApiModelProperty(value = "주문마감시간안내")
    private String closeTmCont;

    @ApiModelProperty(value = "배송시간안내")
    private String dlvTmCont;

    @ApiModelProperty(value = "휴무일정보")
    private String hldyCont;

    @ApiModelProperty(value = "기타")
    private String etcCont;

    @ApiModelProperty(value = "대체가능여부")
    private String replaceEnableYn;

}

