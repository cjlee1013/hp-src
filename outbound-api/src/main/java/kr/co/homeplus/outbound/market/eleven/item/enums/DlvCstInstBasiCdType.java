package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

public enum DlvCstInstBasiCdType {

	@XmlEnumValue("01")
	FREE("01", "무료"),

	@XmlEnumValue("02")
	FIXED("02", "고정배송비"),

	@XmlEnumValue("03")
	CONDITIONAL("03", "상품조건부무료"),

	@XmlEnumValue("04")
	QUANTITY("04", "수량별 차등"),

	@XmlEnumValue("05")
	Individual("05", "1개당배송비"),

	@XmlEnumValue("07")
	SELLER_CONDITIONAL("07", "판매자 조건부 배송비"),

	@XmlEnumValue("08")
	DELIVERY_CONDITIONAL("08", "출고지 조건부 배송비"),

	@XmlEnumValue("09")
	ELEVENTH_STREET("09", "11번가 통합출고지 배송비"),

	@XmlEnumValue("10")
	ELEVENTH_STREET_ABROAD("10", "11번가 해외배송 조건부 배송비"), ;

	@Getter
	private String code;
	@Getter
	private String desc;

	private DlvCstInstBasiCdType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}
