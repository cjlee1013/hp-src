package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 상품 상세정보 Get Entry
 */
@Getter
@Setter
@ToString
@ApiModel("상품 상세조회")
public class MarketItemGetDto {

	@ApiModelProperty(value = "기본정보")
	private MarketItemBasicGetDto basic;

	@ApiModelProperty(value = "이미지")
	private List<MarketItemImgGetDto> img;

	@ApiModelProperty(value = "안전인증정보")
	private List<MarketItemCertGetDto> cert;

	@ApiModelProperty(value = "고시정보")
	private List<MarketItemNoticeGetDto> notice;

	@ApiModelProperty(value = "옵션")
	private List<MarketItemOptionGetDto> opt;

	@ApiModelProperty(value = "지점별판매정보")
	private List<MarketItemSaleGetDto> storeList;

	@ApiModelProperty("점포행사(rms)")
	private EventInfoGetDto promo;

	@ApiModelProperty("쿠폰")
	private CouponInfoGetDto coupon;

	@ApiModelProperty("마스터 상품 전시여부")
	private String masterDispYn;

	@ApiModelProperty("마스터 상품 프로모션 대상 여부")
	private String masterPromoYn;

	@ApiModelProperty("마스터 상품 쿠폰 대상 여부")
	private String masterCouponYn;

	@ApiModelProperty("행사코드")
	private String customCd;

	@ApiModelProperty("상태메시지")
	private String msg;

}