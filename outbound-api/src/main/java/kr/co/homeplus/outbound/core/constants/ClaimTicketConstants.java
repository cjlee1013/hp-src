package kr.co.homeplus.outbound.core.constants;

public class ClaimTicketConstants {

    public static final String XML_ROOT_NAME_SPACE = "http://gsapi.m2i.kr/";
    public static final String XML_ROOT_NAME_SPACE_V3 = "http://v3api.inumber.co.kr/";

    /** xml root name */
    // 티켓 발급취소 요청 (티켓쿠폰번호로 단일취소)
    public static final String CLAIM_TICKET_XML_ROOT_NAME = "COUPONCANCEL_OUT_04";
    public static final String CLAIM_TICKET_XML_IN_DATA_NAME = "INDATA";

    // 티켓 취소시 정보조회
    public static final String CLAIM_TICKET_INFO_XML_ROOT_NAME = "DOWNLOADINFOONE_OUT";

    // 티켓 발급취소 (홈플러스 거래번호 기준)
    // 발급취소 가능여부(홈플러스 거래번호 기준) 같이 사용
    public static final String CLAIM_TICKET_ORDER_TICKET_XML_ROOT_NAME = "API_OUT_T01";

    // 티켓 발행 정보 조회 (홈플러스 거래번호 기준 - 기본)
    public static final String CLAIM_TICKET_INFO_ORDER_TICKET_XML_ROOT_NAME = "DOWNLOADINFO_OUT";
    public static final String CLAIM_TICKET_INFO_ORDER_TICKET_XML_COUPON_NAME = "COUPON_INFO";
    public static final String CLAIM_TICKET_INFO_ORDER_TICKET_XML_DOWN_COUPON_NAME = "DownloadCoupon";
}
