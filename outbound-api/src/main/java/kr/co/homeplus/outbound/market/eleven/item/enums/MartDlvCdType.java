package kr.co.homeplus.outbound.market.eleven.item.enums;

import lombok.Getter;

public enum MartDlvCdType {
	ShortDistance("00", "근거리"),
	Parcel("01", "택배"),
	BusinessParcel("02", "업체택배"),
	BusinessDirectDelivery("03", "업체직배송"),
	Refrigeration("04", "냉장")
	;

	@Getter
	private String code;
	@Getter
	private String desc;

	private MartDlvCdType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}
