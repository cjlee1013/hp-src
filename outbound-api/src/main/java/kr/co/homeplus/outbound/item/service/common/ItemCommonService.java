package kr.co.homeplus.outbound.item.service.common;

import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.outbound.common.enums.DescriptionType;
import kr.co.homeplus.outbound.constants.ExceptionCode;
import kr.co.homeplus.outbound.core.constants.CodeConstants;
import kr.co.homeplus.outbound.core.utility.StringUtil;
import kr.co.homeplus.outbound.item.constants.EventConstants;
import kr.co.homeplus.outbound.item.constants.ItemConstants;
import kr.co.homeplus.outbound.item.enums.ItemImgType;
import kr.co.homeplus.outbound.item.enums.ItemStatus;
import kr.co.homeplus.outbound.item.enums.ItemType;
import kr.co.homeplus.outbound.item.enums.MarketItemStatus;
import kr.co.homeplus.outbound.item.model.EventInfoGetDto;
import kr.co.homeplus.outbound.item.model.ItemMarketSetDto;
import kr.co.homeplus.outbound.item.model.MarketItemGetDto;
import kr.co.homeplus.outbound.item.model.MarketItemImgGetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ItemCommonService {

    @Value("${plus.resource-routes.imageFront.url}")
    private String imgFrontUrl;

    /**
     * 마켓연동 전시 여부 체크
     * @param marketItemGetDto
     * @return
     */
    public boolean getDisplayValidCheck (MarketItemGetDto marketItemGetDto) {
        /*
         - 미취급 상태 체크
         - 마켓연동 연동 중지
         - 카테고리 맵핑
         - 가상점 단독판매
         - 예약상품
         - 이미지 체크
         - 성인상품,전통주 체크
         - 가격정보 없음
         */
         if(ObjectUtils.isNotEmpty(marketItemGetDto)){
            //default : N 처리 후 Y로 변경
            marketItemGetDto.setMasterDispYn(CodeConstants.DISPLAY_YN_N);

             //미취급여부
            if( CodeConstants.PFR_YN_N.equals(marketItemGetDto.getBasic().getPfrYn())               //pfr_yn
            || CodeConstants.ONLINE_YN_N.equals(marketItemGetDto.getBasic().getOnlineYn())          //온라인판매여부
            || CodeConstants.STOP_DEAL_YN_Y.equals(marketItemGetDto.getBasic().getStopDealYn())     //취급중지
            || !ItemStatus.A.getType().equals(marketItemGetDto.getBasic().getItemStatus())          //판매중상태
            || !ItemType.N.getType().equals(marketItemGetDto.getBasic().getItemType())              //새상품만 판매
            || StringUtils.isEmpty(marketItemGetDto.getBasic().getDcateCd()))                       //카테고리사용여부
            {
                marketItemGetDto.setMsg(ExceptionCode.ERROR_CODE_1003.toString());
                log.debug("Fail :" + ExceptionCode.ERROR_CODE_1003.toString());
                return false;
            }

             //마켓연동 미취급
             if( CodeConstants.USE_YN_N.equals(marketItemGetDto.getBasic().getMarketStopDealYn())) {
                 marketItemGetDto.setMsg(ExceptionCode.ERROR_CODE_1009.toString());
                 log.debug("Fail :" + ExceptionCode.ERROR_CODE_1009.toString());
                 return false;
             }

             //카테고리 맵핑
             //마켓카테고리 맵핑 시 연동안함 등처리 할 경우 '-' 로 입력
            if (StringUtils.isEmpty( marketItemGetDto.getBasic().getMarketCateCd())
                || "-".equals(marketItemGetDto.getBasic().getMarketCateCd())) {
                marketItemGetDto.setMsg( ExceptionCode.ERROR_CODE_1004.toString());
                log.debug("Fail :" + ExceptionCode.ERROR_CODE_1004.toString());
                return false;
            }

            //가상점 단독판매는 판매중지처리
            if (StringUtil.isNotEmpty(marketItemGetDto.getBasic().getVirtualStoreOnlyYn()) &&
            CodeConstants.VIRTUAL_STORE_ONLY_YN_Y.equals(marketItemGetDto.getBasic().getVirtualStoreOnlyYn())) {
                marketItemGetDto.setMsg(ExceptionCode.ERROR_CODE_1005.toString());
                log.debug("Fail :" + ExceptionCode.ERROR_CODE_1005.toString());
                return false;
            }

            //예약상품
            if (StringUtil.isNotEmpty(marketItemGetDto.getBasic().getRsvYn()) &&
            CodeConstants.RSV_YN_Y.equals(marketItemGetDto.getBasic().getRsvYn())) {
                marketItemGetDto.setMsg(ExceptionCode.ERROR_CODE_1006.toString());
                log.debug("Fail :" + ExceptionCode.ERROR_CODE_1006.toString());
                return false;
            }

            //이미지 체크
            List<MarketItemImgGetDto> imgMNList = marketItemGetDto
                .getImg().stream().filter(img -> !StringUtils.isEmpty(img.getImgUrl())  && img.getImgType().equals(ItemImgType.MN.getType())).collect(Collectors.toList());
            List<MarketItemImgGetDto> imgLSTList = marketItemGetDto
                .getImg().stream().filter(img -> !StringUtils.isEmpty(img.getImgUrl()) && img.getImgType().equals(
                ItemImgType.LST.getType())).collect(Collectors.toList());
            List<MarketItemImgGetDto> checkMainYn = marketItemGetDto
                .getImg().stream().filter(img -> !StringUtils.isEmpty(img.getImgUrl())  && img.getImgType().equals(ItemImgType.MN.getType()) && img.getMainYn().equals("Y")).collect(Collectors.toList());

            if (imgMNList.size() < 1 || imgLSTList.size() != 1 || checkMainYn.size() != 1) {
                marketItemGetDto.setMsg(ExceptionCode.ERROR_CODE_1007.toString());
                log.debug("Fail :" + ExceptionCode.ERROR_CODE_1007.toString());
                return false;
            }

            //전통주, 성인상품
             if(StringUtils.isNotEmpty(marketItemGetDto.getBasic().getAdultType()) &&
                !CodeConstants.ADULT_TYPE_NORMAL.equals(marketItemGetDto.getBasic().getAdultType())) {
                 marketItemGetDto.setMsg(ExceptionCode.ERROR_CODE_1008.toString());
                 log.debug("Fail :" + ExceptionCode.ERROR_CODE_1008.toString());
                 return false;
             }

             //가격정보 없음.
             if (ObjectUtils.isEmpty(marketItemGetDto.getStoreList())) {
                 marketItemGetDto.setMsg(ExceptionCode.ERROR_CODE_2002.toString());
                 log.debug("Fail :" + ExceptionCode.ERROR_CODE_2002.toString());
                 return false;
             }

        } else {
            log.warn(ExceptionCode.ERROR_CODE_1002.toString());
            return false;
        }

        marketItemGetDto.setMasterDispYn(CodeConstants.DISPLAY_YN_Y);
        return true;
    }

    /**
     * 상품상세 Html 생성
     * @param marketItemGetDto
     * @return
     */
    public String getHtmlDetail(MarketItemGetDto marketItemGetDto) {

        /**
         * 상품 상세 페이지 순서
         * 1. 상품고시
         * 2. 원산지
         * 3. 상품상세설명
         * 4. 라벨이미지
         */
        StringBuffer html = new StringBuffer();
        StringBuffer head = new StringBuffer();
        StringBuffer end = new StringBuffer();

        head.append("<head><title></title></head><body leftmargin='0' topmargin='0'><div style='width:810px;'>");
        end.append("</div></body></html>");

        //고시정보
        StringBuilder noticeStr = new StringBuilder();
        if (StringUtils.isNotEmpty(marketItemGetDto.getBasic().getNoticeHtml())) {
            noticeStr.append("<TABLE border='1' cellpadding='5' cellspacing='0' style='border-collapse:collapse;font: 14px/1.2;border:1px solid #aaa;' width=98%><colgroup><col width=24% /><col /></colgroup>");
            noticeStr.append(marketItemGetDto.getBasic().getNoticeHtml());
            noticeStr.append("</TABLE>");
        }


        //상품상세
        StringBuffer descStr = new StringBuffer();
        if( StringUtils.isNotEmpty(marketItemGetDto.getBasic().getItemDesc())) {

            Document doc = Jsoup.parse(marketItemGetDto.getBasic().getItemDesc());
            Elements imgs = doc.getElementsByTag("img");

            if(imgs.size() > 0) {
                for(Element img : imgs){
                    descStr.append("<img src=").append(img.attr("src")).append(">");
                }
            }
        }else {
            descStr.append(marketItemGetDto.getBasic().getItemNm());
        }

        List<MarketItemImgGetDto> imgLSTList = marketItemGetDto
            .getImg().stream().filter(img -> !StringUtils.isEmpty(img.getImgUrl()) && img.getImgType().equals(
            ItemImgType.LBL.getType())).collect(Collectors.toList());

        StringBuilder labelImg = new StringBuilder();
        imgLSTList.forEach(
            img -> {
                labelImg.append("   <tr> <td ALIGN=\"left\"> </td> ")
                .append("       <td width=\"100%\" VALIGN=\"TOP\" align=\"center\">")
                .append("           <img src=\"").append(imgFrontUrl).append(img.getImgUrl()).append("\"/>")
                .append("       </td>")
                .append("   </tr>");
            }
        );

        labelImg.append("   <tr> <td ALIGN=\"left\"> </td> ")
                .append("       <td width=\"100%\" VALIGN=\"TOP\" align=\"center\">")
                .append("           <img src=\"").append(ItemConstants.ITEM_DESC_BOTTOM_IMG).append("\"/><br><br>")
                .append("       </td>")
                .append("   </tr>");

        html.append(head)
            //고시정보
            .append(noticeStr)

            //상품공통상세
            .append("   <!--[s]상품상세설명-->")
            .append("<table width=\"100%\" CELLSPACING=\"0\" CELLPADDING=\"0\" BORDER=\"0\" style=\"margin-top:10px;\">")

            //원산지
            .append("   <tr>")
            .append("     <td ALIGN=\"left\"> </td>")
            .append("       <td width=\"100%\" VALIGN=\"TOP\">")
            .append("<p>원산지 : ").append(StringUtils.isNotEmpty(marketItemGetDto.getBasic().getOriginTxt()) ? marketItemGetDto
            .getBasic().getOriginTxt() : "해당상품에 표시").append("</p>")
            .append("       </td>")
            .append("   </tr>")

            //상품상세
            .append("   <tr>")
            .append("     <td ALIGN=\"left\"> </td>")
            .append("       <td width=\"100%\" VALIGN=\"TOP\">")
            .append(descStr)
            .append("       </td>")
            .append("   </tr>")

            //라벨이미지
            .append(labelImg)
            .append("</table>")
            .append("   <!--[e]상품상세설명 -->")

            .append(end);
            return html.toString();
    }
    /**
     * 특수 문자 제거
     *
     * @param str
     * @return String
     */
    public String removeSpecialChar( DescriptionType descriptionType, String str ) {

        // String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
        String match = null;

        switch ( descriptionType ) {
            case TITLE:
                match = "[^\uAC00-\uD7A30-9a-zA-Z\\s\\[\\]+\\-=±×÷%℃￠￡￥._]";
                break;
            case OPTION:
                match = "[\'\"%&<>#†\\∏‡,]";
                break;
            case NOTIFICATION:
                match = "[\'\"%&<>#†\\∏‡,]";
                break;
            case DETAIL:
                match = "[]";
                break;
        }

        str = str.replaceAll( match, "" );
        return str;
    }

    /**
     * 프로모션 검증
     * @param marketItemGetDto
     */
    public void validPromo(MarketItemGetDto marketItemGetDto) {
        //초기 N처리
        marketItemGetDto.setMasterCouponYn(EventConstants.CODE_N);
        marketItemGetDto.setMasterPromoYn(EventConstants.CODE_N);

        StringBuilder rmsCustomCd = new StringBuilder();
        StringBuilder couponCustomCd = new StringBuilder();

        //상품할인
        if(ObjectUtils.isNotEmpty(marketItemGetDto.getCoupon())) {

            if (null != marketItemGetDto.getCoupon().getCouponNo()) {
                //coupon_no|discount_type|discount_value^club_coupon_no|discount_type|discount_value
                StringBuilder cpnPromoStr = new StringBuilder();

                cpnPromoStr
                        .append(marketItemGetDto.getCoupon().getDiscountType()).append(CodeConstants.PROMO_CODE_DELIMITER)
                        .append(marketItemGetDto.getCoupon().getDiscountType().equals(EventConstants.DISCOUNT_TYPE_RATE) ? marketItemGetDto.getCoupon().getDiscountRate() : marketItemGetDto.getCoupon().getDiscountPrice());

                couponCustomCd
                        .append(marketItemGetDto.getCoupon().getCouponNo()) //coupon_no
                        .append(CodeConstants.PROMO_CODE_DELIMITER).append(cpnPromoStr) //promo_info
                        .append(CodeConstants.STORE_CODE_DELIMITER); //^

                marketItemGetDto.setMasterCouponYn(EventConstants.CODE_Y);
            }

            if (ObjectUtils.isNotEmpty(couponCustomCd) && null != marketItemGetDto.getCoupon().getClubCouponNo() && EventConstants.CODE_Y.equals(marketItemGetDto.getCoupon().getClubCouponYn())) {
                couponCustomCd.append(marketItemGetDto.getCoupon().getClubCouponNo())
                        .append(CodeConstants.PROMO_CODE_DELIMITER).append(marketItemGetDto.getCoupon().getClubDiscountType()).append(CodeConstants.PROMO_CODE_DELIMITER)
                        .append(marketItemGetDto.getCoupon().getClubDiscountType().equals(EventConstants.DISCOUNT_TYPE_RATE) ? marketItemGetDto.getCoupon().getClubDiscountRate() : marketItemGetDto.getCoupon().getClubDiscountPrice());

                marketItemGetDto.setMasterCouponYn(EventConstants.CODE_Y);
            }
        }

        //점포행사
        if( ObjectUtils.isNotEmpty(marketItemGetDto.getPromo()) && null != marketItemGetDto.getPromo().getRpmPromoCompDetailId()) {
            //초기 N처리
            marketItemGetDto.getPromo().setClubPromoYn(EventConstants.CODE_N);

            StringBuilder rmsPromStr = new StringBuilder();

            /**
             * buy_item_value는 PICK/TOGETHER만 넣고 simple은 "" 처리
             * rmsPromoStr = change_type|change_value|buy_item_type|
             */
             rmsPromStr
                 .append(marketItemGetDto.getPromo().getChangeType()) //change_type
                 .append(CodeConstants.PROMO_CODE_DELIMITER) // |

                 .append(EventConstants.CHANGE_TYPE_RATE.equals(marketItemGetDto.getPromo().getChangeType()) ? marketItemGetDto.getPromo().getChangePercent() : marketItemGetDto.getPromo().getChangeAmount()) //change_amount
                 .append(CodeConstants.PROMO_CODE_DELIMITER) // |

                 .append(StringUtils.isNotEmpty(marketItemGetDto.getPromo().getBuyItemType()) ? marketItemGetDto.getPromo().getBuyItemType() : "") //buyItemType
                 .append(CodeConstants.PROMO_CODE_DELIMITER) ;// |



            switch (marketItemGetDto.getPromo().getEventKind()) {
                case EventConstants.EVENT_KIND_BASIC:
                    /**
                     * SIMPLE RMS 행사 체크
                     * - buy_item_cnt = 1
                     */
                    if (1 == marketItemGetDto.getPromo().getBuyItemCnt()) {
                        /**
                         * rmsCustomCd = B# + promo_no + rmsPromStr + ^ ( 클럽 추가 시  + club_promo_no + rmsPromStr)
                         */
                        rmsCustomCd.append(EventConstants.SIMPLE).append(CodeConstants.RMS_CODE_DELIMITER).append(marketItemGetDto.getPromo().getRpmPromoCompDetailId())
                            .append(CodeConstants.PROMO_CODE_DELIMITER).append(rmsPromStr).append(CodeConstants.STORE_CODE_DELIMITER);

                        //Club 일치 확인
                        if (null != marketItemGetDto.getPromo().getClubRpmPromoCompDetailId() && this.equalChangeType(marketItemGetDto.getPromo())) {
                            marketItemGetDto.getPromo().setClubPromoYn(EventConstants.CODE_Y);
                            rmsCustomCd.append(marketItemGetDto.getPromo().getClubRpmPromoCompDetailId()).append(CodeConstants.PROMO_CODE_DELIMITER).append(rmsPromStr);
                        }

                        marketItemGetDto.setMasterPromoYn(EventConstants.CODE_Y);

                    }
                    break;

                case EventConstants.EVENT_KIND_PICK:
                    /**
                     *  PICK RMS 행사 체크
                     *  - change_type = 0 && change_percent 100%
                     *  - GET|BUY 동일 상품 여부 체크
                     */
                    if (EventConstants.CHANGE_TYPE_RATE.equals(marketItemGetDto.getPromo().getChangeType()) && marketItemGetDto.getPromo().getChangePercent() == 100
                        && marketItemGetDto.getPromo().getBuyItemNo().equals(marketItemGetDto.getPromo().getGetItemNo())) {

                        /**
                         * rmsCustomCd = P# + promo_no + rmsPromStr(+ buy_item_value)  ^ ( 클럽 추가 시  + club_promo_no + rmsPromStr)
                         */
                        rmsPromStr.append(ObjectUtils.isEmpty(marketItemGetDto.getPromo().getBuyItemValue()) ? "" : marketItemGetDto.getPromo().getBuyItemValue());
//
                        rmsCustomCd.append(EventConstants.PICK).append(CodeConstants.RMS_CODE_DELIMITER).append(marketItemGetDto.getPromo().getRpmPromoCompDetailId())
                            .append(CodeConstants.PROMO_CODE_DELIMITER).append(rmsPromStr).append(CodeConstants.STORE_CODE_DELIMITER);

                        //Club 일치 확인
                        if (null != marketItemGetDto.getPromo().getClubRpmPromoCompDetailId() && this.equalChangeType(marketItemGetDto.getPromo())) {
                            marketItemGetDto.getPromo().setClubPromoYn(EventConstants.CODE_Y);
                            rmsCustomCd.append(marketItemGetDto.getPromo().getClubRpmPromoCompDetailId()).append(CodeConstants.PROMO_CODE_DELIMITER)
                                .append(rmsPromStr);
                        }

                        marketItemGetDto.setMasterPromoYn(EventConstants.CODE_Y);
                    }

                    break;

//                case EventConstants.EVENT_KIND_TOGETHER:
//                    /**
//                     * TODO: TOGETHER 주석처리 10.26
//                     *  TOGETHER RMS 행사체크
//                     *  - buy_item_value 1 이상일 경우, BUY|GET 동일상품 체크
//                     *
//                     *  rmsCustomCd = T# + promo_no + rmsPromStr(+ buy_item_value)  ^ ( 클럽 추가 시  + club_promo_no + rmsPromStr)
//                     *
//                     */
//                   if (marketItemGetDto.getPromo().getBuyItemValue() >= 1
//                            && marketItemGetDto.getPromo().getBuyItemNo().equals(marketItemGetDto.getPromo().getGetItemNo())) {
//
//                        rmsPromStr.append(ObjectUtils.isEmpty(marketItemGetDto.getPromo().getBuyItemValue()) ? "" : marketItemGetDto.getPromo().getBuyItemValue());
//                        rmsCustomCd.append(EventConstants.TOGHTHER).append(CodeConstants.RMS_CODE_DELIMITER).append(marketItemGetDto.getPromo().getRpmPromoCompDetailId())
//                            .append(CodeConstants.PROMO_CODE_DELIMITER).append(rmsPromStr).append(CodeConstants.STORE_CODE_DELIMITER);
//
//                        //Club 일치 확인
//                        if (null != marketItemGetDto.getPromo().getClubRpmPromoCompDetailId()) {
//                            if (marketItemGetDto.getPromo().getBuyItemValue().equals(marketItemGetDto.getPromo().getClubBuyItemValue())
//                                && this.equalChangeType(marketItemGetDto.getPromo())) {
//                                marketItemGetDto.getPromo().setClubPromoYn(EventConstants.CODE_Y);
//                                rmsCustomCd.append(marketItemGetDto.getPromo().getClubRpmPromoCompDetailId()).append(CodeConstants.PROMO_CODE_DELIMITER)
//                                    .append(rmsPromStr);
//                            }
//                        }
//
//                        marketItemGetDto.setMasterPromoYn(EventConstants.CODE_Y);
//                    }
//
//                    break;
                 case EventConstants.EVENT_KIND_INTERVAL:
                    //전송하지 않음.
                    break;
            }
        }

        //custom_code = rms_code*coupon_code
        marketItemGetDto.setCustomCd(rmsCustomCd + CodeConstants.COUPON_CODE_DELIMITER + couponCustomCd);
    }

     /**
     * HYPER | CLUB 행사 일치 여부 확인
     * @param promo
     * @return boolean
     */
    private boolean equalChangeType(EventInfoGetDto promo) {
        boolean result = false;

        if( promo.getChangeType().equals(promo.getClubChangeType())) {
            switch (promo.getChangeType()) {
                case EventConstants.CHANGE_TYPE_RATE:
                    if (promo.getChangePercent() == promo.getClubChangePercent()) {
                        result = true;
                    }
                    break;

                case EventConstants.CHANGE_TYPE_PRICE:
                case EventConstants.CHANGE_TYPE_FIXED:
                    if (promo.getChangeAmount() == promo.getClubChangeAmount()) {
                        result = true;
                    }
                    break;
            }
        }
        return result;
    }

    /**
     * PICK 행사 시 최대 구매수량 계산
     * @return
     */
    public int getPickPurchaseLimitQty(int buyItemValue, int purchaseLimitQty) {
        int msvCnt = buyItemValue -1 ;
        int pickLimitCnt = msvCnt * (purchaseLimitQty / buyItemValue) ;
        return pickLimitCnt;
    }
    /**
     * 마켓연동 판매중 상태 변경 DTO 생성
     * @param marketItemGetDto
     * @return
     */
    public ItemMarketSetDto setMarketItemActive(MarketItemGetDto marketItemGetDto) {
        return ItemMarketSetDto.builder()
            .partnerId(marketItemGetDto.getBasic().getPartnerId())
            .itemNo(marketItemGetDto.getBasic().getItemNo())
            .marketItemStatus(MarketItemStatus.A.getType())
            .sendYn(CodeConstants.USE_YN_Y)
            .marketItemNo(marketItemGetDto.getBasic().getMarketItemNo())
            .promoYn(marketItemGetDto.getMasterPromoYn())
            .couponYn(marketItemGetDto.getMasterCouponYn())
            .couponEndDt(StringUtils.isNotEmpty(marketItemGetDto.getCoupon().getCouponEndDt()) ? marketItemGetDto.getCoupon().getCouponEndDt() : null)
            .sendMsg(StringUtils.left(marketItemGetDto.getMsg(),190))
            .build();
    }

    /**
     * 마켓연동 판매중지 상태 변경 DTO 생성
     * @param marketItemGetDto
     * @return
     */
    public ItemMarketSetDto setMarketItemStop(MarketItemGetDto marketItemGetDto, String marketItemStatus) {
        return ItemMarketSetDto.builder()
            .partnerId(marketItemGetDto.getBasic().getPartnerId())
            .itemNo(marketItemGetDto.getBasic().getItemNo())
            .sendYn(CodeConstants.USE_YN_Y)
            .marketItemNo(marketItemGetDto.getBasic().getMarketItemNo())
            .marketItemStatus(marketItemStatus)
            .promoYn(marketItemGetDto.getMasterPromoYn())
            .couponYn(marketItemGetDto.getMasterCouponYn())
            .couponEndDt(ObjectUtils.isNotEmpty(marketItemGetDto.getCoupon()) && StringUtils.isNotEmpty(marketItemGetDto.getCoupon().getCouponEndDt()) ? marketItemGetDto.getCoupon().getCouponEndDt() : null)
            .sendMsg(StringUtils.left(marketItemGetDto.getMsg(),190))
            .build();
    }

}

