package kr.co.homeplus.outbound.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * N 마트 환경
 */
@RequiredArgsConstructor
public enum NaverMartProfile {

    /* [S] Naver SmartStore Store */
    STORE_SERVICE_NAME( "BranchService", "서비스 이름")
    ,   STORE_SERVICE_VERSION( "2.0", "서비스 버전")
    ,   STORE_TODAY_DELIVERY( "TODAY_DELIVERY", "배송유형 : 당일배송")
    ,   STORE_DAWN_DELIVERY( "DAWN_DELIVERY", "배송유형 : 새벽배송")
    ,   STORE_SHORT_DISTANCE( "1", "배송권역 : 근거리(고정)")
    ,   STORE_DEADLINE( "50", "배송슬롯 : 사전 마감시간")
    ,   STORE_SLOT_CAPA_RATE( "0.4", "배송슬롯 : capa 비율")
    ,   STORE_SLOT_LIMIT_CNT( "0", "배송슬롯 : slot정보 전송 제한 capa 수")
    ,   STORE_SLOT_SEND_PERIOD( "4", "배송슬롯 : 전송 기간")
    /* [E] Naver SmartStore Store */

    /* [S] Naver SmartStore Product */
    ,   PRODUCT_SERVICE_NAME( "ProductService", "서비스 이름")
    ,   PRODUCT_SERVICE_VERSION( "2.0", "서비스 버전")
    /* [E] Naver SmartStore Product */

    /* [S] Naver SmartStore Image */
    ,   IMAGE_SERVICE_NAME( "ImageService", "서비스 이름")
    ,   IMAGE_SERVICE_VERSION( "2.0", "서비스 버전")
    /* [E] Naver SmartStore Image */

    /* [S] Naver SmartStore Delivery */
    ,   DELIVERY_SERVICE_NAME( "DeliveryInfoService", "서비스 이름")
    ,   DELIVERY_SERVICE_VERSION( "2.0", "서비스 버전")
    /* [E] Naver SmartStore Delivery */

    /* [S] Naver SmartStore Address */
    ,   ADDRESS_SERVICE_NAME( "AddressBookService", "서비스 이름")
    ,   ADDRESS( "2.0", "서비스 버전")
    /* [E] Naver SmartStore Address */

    /* [S] Naver SmartStore Shipping */
    ,   DELIVERY_COMPANY_CODE( "HOMEPLUS", "deliveryCompanyCode")
    /* [E] Naver SmartStore Shipping */

    /* [S] Naver SmartStore Inquiry */
    ,   INQUIRY_SERVICE_NAME( "CustomerInquiryService", "서비스 이름")
    ,   INQUIRY_SERVICE_VERSION( "1.0", "서비스 버전")
    ,   INQUIRY_SERVICE_TYPE( "SHOPN", "서비스 타입")
    /* [E] Naver SmartStore Inquiry */

    /* [S] Naver SmartStore Question */
    ,   QUESTION_SERVICE_NAME( "QuestionAnswerService", "서비스 이름")
    ,   QUESTION_SERVICE_VERSION( "2.0", "서비스 버전")
    /* [E] Naver SmartStore Question */

    /* [S] Naver - GoodsFlow 배송추적 API */
    ,   GOODSFLOW_API_LOGISTICSCODE( "HOMEPLUS", "logisticscode")
    ,   GOODSFLOW_API_HOST( "https://ws1.goodsflow.com/", "host")
    ,   GOODSFLOW_API_KEY( "04e6749f-7ddb-43b4-1764-762f2699346a", "key")
    ,   GOODSFLOW_API_GETINVOICE_NAME( "/sellerdelivery/seller/v1/GetInvoice/", "getinvoice")
    ,   GOODSFLOW_API_ACKINVOICE_NAME( "/sellerdelivery/seller/v1/AckInvoice/", "ackinvoice")
    ,   GOODSFLOW_API_TRACERESPONSE_NAME( "/sellerdelivery/seller/v1/TraceResponse/", "traceresponse")
    ,   GOODSFLOW_API_TRACEINFO_NAME( "/sellerdelivery/seller/v1/TraceInfo/", "traceinfo")
    /* [E] Naver - GoodsFlow 배송추적 API */

    ;

    @Getter
    private final String value;

    @Getter
    private final String description;

}
