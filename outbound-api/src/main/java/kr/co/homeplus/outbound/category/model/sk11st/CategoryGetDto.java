package kr.co.homeplus.outbound.category.model.sk11st;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
public class CategoryGetDto {

    @ApiModelProperty(value = "트리 구조의 깊이")
    private int depth;

    @ApiModelProperty(value = "카테고리 이름")
    private String dispNm;

    @ApiModelProperty(value = "카테고리 번호")
    private String dispNo;

    @ApiModelProperty(value = "상위 카테고리번호")
    private String parentDispNo;

    @ApiModelProperty(value = "하위여부")
    private String leafYn;

}
