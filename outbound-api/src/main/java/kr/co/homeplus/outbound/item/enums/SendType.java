package kr.co.homeplus.outbound.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

public enum SendType {

	REGULAR("0", "정기전송"),
	SPOT("1", "수시전송"),
	IMMEDIATE("2", "즉시전송");

	@Getter
	private final String code;
	@Getter
	private final String desc;

	SendType(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}
}