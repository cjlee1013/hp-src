package kr.co.homeplus.outbound.market.naver.inquiry.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class NaverInquiryGetDto {
    private Long messageNo;						//문의 번호
    private String marketType;						//마켓유형
    private String messageType;					//문의 유형 (B : 게시판, N : 쪽지, E : 긴급메세지)
    private String storeId;						//GHS 점포 코드
    private String copOrdNo;					//제휴사 주문 번호
    private String copOrdGoodSeq;				//장바구니 번호
    private String ordNo;						//주문 번호
    private String receiveDate;					//문의 날짜
    private String copGoodId;					//제휴사 상품 번호
    private String goodId;						//홈플러스 상품 번호
    private String requestTitle;				//문의 제목
    private String requestComments;				//문의 내용
    private String contactType;					//문의 유형
    private String contactCode;					//문의 구분
    private String emergencyContact;			//긴급메세지 응답 유형
    private String inquirerId;					//문의자 ID
    private String inquirerName;				//문의자명
    private String inquirerPhone;				//문의자 연락처
    private String responseTitle;				//답변 제목
    private String responseContents;			//답변 내용
    private String messageSection;				//긴급메세지 문의 분야
    private String isresponseYn;				//답변 여부
    private String isresponseDate;				//답변 날짜
    private String issendYn;					//전송 여부
    private String issendDate;					//전송 날짜
    private String failMsg;						//실패 내용
    private String buyYn;						//구매 여부
    private String answerContentId;				//답변 번호
    private String inquiryChannel;				//문의 채널 (P:상품 문의, C:판매자 문의)
}
