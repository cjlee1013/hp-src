package kr.co.homeplus.outbound.market.eleven.inquiry.enums;

/**
 * 게시물 소분류
 */

public enum EmerNtceDtlCdType {
    SHIPPING( "01", "배송", "63310" ),
    RETURN( "02", "반품", "63360" ),
    EXCHANGE_CANCLE( "03", "교환/취소", "63361" ),
    REFUND( "04", "환불", "63360" ),
    ETC( "05", "기타", "63420" );

    private String code;
    private String desc;
    private String homeplusCd;

    private EmerNtceDtlCdType( String code, String desc, String homeplusCd ) {
        this.setDesc( desc );
        this.setCode( code );
        this.setHomeplusCd( homeplusCd );
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode( String code ) {
        this.code = code;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc
     *            the desc to set
     */
    public void setDesc( String desc ) {
        this.desc = desc;
    }

    /**
     * @return the homeplusCd
     */
    public String getHomeplusCd() {
        return homeplusCd;
    }

    /**
     * @param homeplusCd
     *            the homeplusCd to set
     */
    public void setHomeplusCd( String homeplusCd ) {
        this.homeplusCd = homeplusCd;
    }

    public static EmerNtceDtlCdType getTypeOf( String emerDtlCd ) {
        for ( EmerNtceDtlCdType dtlsCd : EmerNtceDtlCdType.values() ) {
            if ( dtlsCd.getCode().equals( emerDtlCd ) ) {
                return dtlsCd;
            }
        }
        return ETC;
    }

}
