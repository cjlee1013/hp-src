package kr.co.homeplus.outbound.category.mapper;

import java.util.List;
import kr.co.homeplus.outbound.category.model.MarketCategoryDetailSetDto;
import kr.co.homeplus.outbound.category.model.MarketCategorySetDto;
import kr.co.homeplus.outbound.core.db.annotation.MasterConnection;
import org.apache.ibatis.annotations.Param;
import org.junit.jupiter.params.ParameterizedTest;
import org.springframework.security.core.parameters.P;

@MasterConnection
public interface CategoryMasterMapper {

    /**
     * 마켓연동 카테고리 수정/등록
     */
    int upsertMarketCategory(List<MarketCategorySetDto> categoryList);

    /**
     * 마켓연동 카테고리 디테일 수정/등록
     */
    int upsertMarketCategoryDetail(List<MarketCategoryDetailSetDto> marketCategoryDetailSetDto);

    /**
     * 마켓연동 카테고리 사용안함 처리
     */
    int updateMarketCategoryUseYnN(@Param("partnerId") String partnerId
        , @Param("curDate") String curDate);

    /**
     * 마켓연동 카테고리 디테일 사용 안함 처리
     */
    int updateMarketCategoryDetailUseYnN(@Param("partnerId") String partnerId
        , @Param("curDate") String curDate);
}

