package kr.co.homeplus.outbound.market.eleven.settle.service;

import java.net.URL;
import java.util.List;
import kr.co.homeplus.outbound.market.eleven.config.MarketElevenConfig;
import kr.co.homeplus.outbound.market.eleven.util.ElevenConnectUtil;
import kr.co.homeplus.outbound.market.eleven.settle.model.ElevenSettleItemGetDto;
import kr.co.homeplus.outbound.market.eleven.settle.model.ElevenSettleListGetDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ElevenSettleService {

    private final MarketElevenConfig marketElevenConfig;

    public List<ElevenSettleItemGetDto> getSettleList(String startTime, String endTime) throws Exception {
        // URL : /settlement/settlementList/{startTime}/{endTime}
        URL url = ElevenConnectUtil.createURL(marketElevenConfig.getSettle().getSettlementList(), startTime,endTime);
        ResponseObject<ElevenSettleListGetDto> responseObject = ElevenConnectUtil.execute(url,
            ElevenSettleListGetDto.class);
        ElevenSettleListGetDto elevenSettleListGetDto = responseObject.getData();
        if (elevenSettleListGetDto == null || elevenSettleListGetDto.getElevenSettleItemGetDtoList() == null){
            log.info("getSettleList="+ responseObject.getReturnCode() + ":" + responseObject.getReturnMessage());
            return null;
        }
        return responseObject.getData().getElevenSettleItemGetDtoList();
    }
}
