package kr.co.homeplus.outbound.market.naver.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class NaverChangedProductOrderInfo {
    @ApiModelProperty(value= "주문번호")
    private String orderId;
    @ApiModelProperty(value= "상품주문번호")
    private String productOrderId;
    @ApiModelProperty(value= "최종변경상태코드")
    private String lastChangedStatus;
    @ApiModelProperty(value= "최종변경일시")
    private String lastChangedDate;
    @ApiModelProperty(value= "상품주문상태코드")
    private String productOrderStatus;
    @ApiModelProperty(value= "클레임타입코드")
    private String claimType;
    @ApiModelProperty(value= "클레임처리상태코드")
    private String claimStatus;
    @ApiModelProperty(value= "결제일시")
    private String paymentDate;
    @ApiModelProperty(value= "배송지정보수정여부")
    private String isReceiverAddressChanged;
    @ApiModelProperty(value= "선물수신상태코드")
    private String giftReceivingStatus;
}
