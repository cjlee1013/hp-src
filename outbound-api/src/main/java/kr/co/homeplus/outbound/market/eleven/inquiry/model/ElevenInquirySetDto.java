package kr.co.homeplus.outbound.market.eleven.inquiry.model;

import lombok.Data;

@Data
public class ElevenInquirySetDto {
  /**
   * 검색 시작일
   */
  private String startTime; //검색 시작일

  /**
   * 검색 종료일
   */
  private String endTime;

  /**
   * 처리여부
   * 00 : 전체조회
   * 01 : 답변완료
   * 02 : 미답변
   */
  private String answerStatus;

  /**
   * HT, TC구분값
   */
  private String corpCdType;
}