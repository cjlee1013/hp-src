package kr.co.homeplus.outbound.store.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.outbound.common.enums.MarketType;
import kr.co.homeplus.outbound.core.constants.CodeConstants;
import kr.co.homeplus.outbound.market.eleven.item.enums.MartDlvCdType;
import kr.co.homeplus.outbound.market.eleven.item.model.ResultMessageDto;
import kr.co.homeplus.outbound.response.ResponseResult;
import kr.co.homeplus.outbound.store.mapper.StoreMasterMapper;
import kr.co.homeplus.outbound.store.mapper.StoreSlaveMapper;
import kr.co.homeplus.outbound.store.model.MarketStoreSetDto;
import kr.co.homeplus.outbound.store.model.StoreGetDto;
import kr.co.homeplus.outbound.store.model.StoreGetParamDto;
import kr.co.homeplus.outbound.store.model.eleven.MartStoreGetDto;
import kr.co.homeplus.outbound.store.model.eleven.MartStoreResultDto;
import kr.co.homeplus.outbound.store.model.eleven.MartStoreSetDto;
import kr.co.homeplus.outbound.store.model.eleven.StoreMailNoListDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class StoreService {

    private final StoreSendService storeSendService;
    private final StoreSlaveMapper storeSlaveMapper;
    private final StoreMasterMapper storeMasterMapper;

    /**
     * 11번가 지점 조회
     * @param storeId
     * @return
     */
    public List<MartStoreGetDto> getStoreInfo(Integer storeId) {
        //11번가에 등록된 판매자 지점번호은 String 형태로 00** 등록 되어 있어 변환 해줌.
        return storeSendService.getStoreInfo(String.format("%04d", storeId)).getStore();
    }

    /**
     * 11번가 전체 지점 조회
     * @return
     */
    public List<MartStoreGetDto> getStoreInfoAll(){
        return storeSendService.getStoreInfoAll().getStore();
    }

    /**
     * 11번가 지점 등록/수정
     * @return
     */
    public ResponseResult setStoreInfo(StoreGetParamDto storeGetParamDto) throws Exception {

        //11번가 지점 조회
        List<MartStoreGetDto> storeListBy11st =  storeSendService.getStoreInfoAll().getStore();

        //홈플러스 점포 조회
        List<StoreGetDto> storeList = storeSlaveMapper.getStoreList(storeGetParamDto);

        List<MarketStoreSetDto> marketStoreSetDtoList = new ArrayList<>();

        if(ObjectUtils.isNotEmpty(storeList)) {

            storeList.stream().forEach(store -> {
            if (StringUtils.isNotEmpty(store.getMarketStoreId())) {
                    //수정
                    List<MartStoreGetDto> _list = storeListBy11st.stream().filter(es-> es.getStrNo().equals(store.getMarketStoreId())).collect(
                        Collectors.toList());

                    if(ObjectUtils.isNotEmpty(_list) && _list.size() == 1 ) {
                        if ( !equalsObject(store, _list.get(0))) {
                            ResultMessageDto response = storeSendService.modifyStoreInfo(convertMartStore(store));

                            if ("0".equals(response.getResultCode())) {
                                marketStoreSetDtoList.add(
                                    MarketStoreSetDto.builder()
                                        .marketStoreId(store.getMarketStoreId())
                                        .partnerId(MarketType.ELEVEN.getId())
                                        .storeId(store.getOriginStoreId())
                                        .useYn(CodeConstants.USE_YN_Y).build());
                            } else {
                                //연동오류
                                log.info(
                                    "11번가 지점 신규 등록 에러 [" + store.getStoreId() + "}" + response.toString());
                            }
                        }

                    }else {
                        log.info("11번가 내 이미 등록된 지점이 존재합니다.[수정] :" + store.getStoreId());
                    }

                } else {
                    //등록
                    if ( storeListBy11st.stream().noneMatch( es -> es.getSellerStrCd().equals(store.getMarketStoreId()))) {
                        //11번가에 등록된 지점이 없을 경우에만 신규 전송
                        MartStoreResultDto response = storeSendService
                            .addStoreInfo(convertMartStore(store));

                        if (StringUtils.isNotEmpty(response.getStrNo())) {
                            marketStoreSetDtoList.add(
                                MarketStoreSetDto.builder()
                                    .marketStoreId(response.getStrNo())
                                    .partnerId(MarketType.ELEVEN.getId())
                                    .storeId(store.getOriginStoreId())
                                    .useYn(CodeConstants.USE_YN_Y).build());
                        } else {
                            log.info(
                                "11번가 지점 신규 등록 실패 [" + store.getStoreId() + "}" + response.toString());
                        }
                    }else {
                        log.info("11번가 내 이미 등록된 지점이 존재합니다. [등록]  : " + store.getStoreId());
                    }
                }

            });
        }
        // 지점 삭제는 진행하지 않는다.
        int result = 0;

        //지점 정보 등록
        if (ObjectUtils.isNotEmpty(marketStoreSetDtoList)) {
            //TODO: 지점정보가 기존 데이터와 다를 경우 업데이트를 할 것인가 말것인가.......
            result = storeMasterMapper.upsertMarketStore(marketStoreSetDtoList);
        }

        ResponseResult responseResult = new ResponseResult();
        responseResult.setReturnKey(String.valueOf(result));

        return responseResult;
    }

    /**
     * 전송 데이터 비교
     * @param store
     * @param dto
     * @return
     */
    private boolean equalsObject (StoreGetDto store, MartStoreGetDto dto) {

        if ( !dto.getStrNo().equals( store.getMarketStoreId() ) ) { // 스토어코드
            return false;
        } else if ( !dto.getStrNm().equals( store.getStoreNm() ) ) { // 스토어명
            return false;
        } else if ( !dto.getBaseAddr().equals( store.getAddr1() ) ) { // 기본주소
            return false;
        } else if ( !dto.getDtlsAddr().equals( store.getAddr2() ) ) { // 상세주소
            return false;
        } else if ( !dto.getSellerStrCd().equals( store.getStoreId() ) ) { // 셀러지점코드
            return false;
        } else if ( !dto.getCobsRptv().equals( store.getMgrNm() )){ //대표이사명
            return false;
        } else if ( !dto.getReplaceEnableYn().equals( store.getFcYn() )){ //대체가능여부
            return false;
        }
        return true;
    }
    /**
     * StoreGetDto Convert to MartStoreSetDto
     * @param storeGetDto
     * @return MartStoreSetDto
     */
    private MartStoreSetDto convertMartStore(StoreGetDto storeGetDto) {
        return MartStoreSetDto.builder().martNo("6")
            .strNo(StringUtils.isEmpty(storeGetDto.getMarketStoreId()) == true ? null : storeGetDto.getMarketStoreId())
            .strNm(storeGetDto.getStoreNm())
            .bsnsNm(storeGetDto.getPartnerNm())
            .cobsEnprNo(storeGetDto.getPartnerNo().replace("-", ""))
            .cobsRptv(storeGetDto.getMgrNm())
            .rptvTlphnNo(storeGetDto.getPhone())
            .mailNo(storeGetDto.getZipcode())
            .baseAddr(storeGetDto.getAddr1())
            .dtlsAddr(storeGetDto.getAddr2())
            .martDlvCd(MartDlvCdType.ShortDistance.getCode())
            .sellerStrCd(storeGetDto.getStoreId())
            .dlvLotUseYn("Y")
            .useYn(CodeConstants.USE_YN_Y)
            .dlvAreaCont("일부 지역 제외")
            .closeTmCont(storeGetDto.getOprTime())
            .dlvTmCont("23시(일부 지역 제외) 주문일로부터 7일까지 예약 가능하며, 선택하신 시간대에 배송해드립니다.")
            .hldyCont(storeGetDto.getClosedInfo())
            .etcCont("")
            .replaceEnableYn(storeGetDto.getFcYn()).build();
    }

    public StoreMailNoListDto findStoreMainInfo(String strNo) {
        return storeSendService.findStoreMailNoInfo(strNo);
    }
}

