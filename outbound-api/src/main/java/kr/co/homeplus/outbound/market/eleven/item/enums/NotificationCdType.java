package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

/**
 * 고시정보 Default
 */
public enum NotificationCdType {

    @XmlEnumValue( "891031" )
    PROCESSED_FCuponCheckTypeOOD( "891031", "가공식품" );

    @Getter
    private String code;
    @Getter
    private String desc;

    private NotificationCdType( String code, String desc ) {
        this.desc = desc;
        this.code = code;
    }
}
