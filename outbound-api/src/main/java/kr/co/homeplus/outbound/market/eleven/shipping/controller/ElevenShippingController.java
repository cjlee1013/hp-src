package kr.co.homeplus.outbound.market.eleven.shipping.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.homeplus.outbound.market.eleven.shipping.model.ElevenShipCompleteItemGetDto;
import kr.co.homeplus.outbound.market.eleven.shipping.service.ElevenShippingService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "마켓연동 > 11번가")
@Slf4j
@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class ElevenShippingController {

    private final ElevenShippingService elevenShippingService;

    @ApiOperation(value = "11번가 배송출발")
    @GetMapping("/eleven/setShipStart")
    public ResponseObject<Boolean> setShipStart(String dlvNo) throws Exception {
        return ResourceConverter.toResponseObject(elevenShippingService.setShipStart(dlvNo));
    }

    @ApiOperation(value = "11번가 부분배송출발")
    @GetMapping("/eleven/setShipStartPart")
    public ResponseObject<Boolean> setShipStartPart(String dlvNo, String ordNo, String ordPrdSeq) throws Exception {
        return ResourceConverter.toResponseObject(
            elevenShippingService.setShipStartPart(dlvNo,ordNo,ordPrdSeq));
    }

    @ApiOperation(value = "11번가 반품수거")
    @GetMapping("/eleven/setReturnInvc")
    public ResponseObject<Boolean> setReturnInvc(String dlvNo, String clmReqSeq) throws Exception {
        return ResourceConverter.toResponseObject(
            elevenShippingService.setReturnInvc(dlvNo,clmReqSeq));
    }

    @ApiOperation(value = "11번가 반품완료")
    @GetMapping("/eleven/setSellerClaimFix")
    public ResponseObject<Boolean> setSellerClaimFix(String ordNo, String ordPrdSeq) throws Exception {
        return ResourceConverter.toResponseObject(
            elevenShippingService.setSellerClaimFix(ordNo,ordPrdSeq));
    }

    @ApiOperation(value = "11번가 구매확정 주문")
    @GetMapping("/eleven/getShipComplete")
    public ResponseObject<List<ElevenShipCompleteItemGetDto>> getShipComplete(String startTime, String endTime) throws Exception {
        return ResourceConverter.toResponseObject(
                elevenShippingService.getShipComplete(startTime,endTime));
    }
}
