package kr.co.homeplus.outbound.market.eleven.item.model;


import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "StoreProductList")
@Data
public class StorePriceListDto {

	private List<PriceSetDto> StoreProduct;

}