package kr.co.homeplus.outbound.category.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class
MarketCategorySetDto {

    //partner 아이디
    private String partnerId;

    //카테고리 번호
    private String cateCd;

    //부모카테고리번호
    private String pcateCd;

    //카테고리명
    private String cateNm;

    //depth
    private int depth;

    //사용여부
    private String useYn;

    //리프여부
    private String leafYn;

}
