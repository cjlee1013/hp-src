package kr.co.homeplus.outbound.market.eleven.claim.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class ElevenResponseDto {
    private String result_code;
    private String result_text;
}
