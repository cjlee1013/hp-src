package kr.co.homeplus.outbound.ticket.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "티켓상태 조회(COOP용-클레임) 요청 DTO")
public class ClaimCoopTicketCheckSetDto {
    @ApiModelProperty(value = "상품코드(티켓)", position = 1)
    private String sellerItemCd;
    @ApiModelProperty(value = "티켓쿠폰번호(티켓)", position = 2)
    private String ticketCd;
}