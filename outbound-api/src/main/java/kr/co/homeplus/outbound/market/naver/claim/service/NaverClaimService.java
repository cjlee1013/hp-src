package kr.co.homeplus.outbound.market.naver.claim.service;

import com.nhncorp.platform.shopn.seller.SellerServiceStub;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.AccessCredentialsType;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.Address;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ApproveCancelApplicationRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ApproveCancelApplicationResponseE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ApproveCollectedExchangeRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ApproveCollectedExchangeResponseE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ApproveReturnApplicationRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ApproveReturnApplicationResponseE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.CancelSaleRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.CancelSaleResponseE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ChangedProductOrderInfo;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ClaimRequestReasonType;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.GetChangedProductOrderListRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.GetProductOrderInfoListRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.GetProductOrderInfoListResponseE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.HoldbackClassType;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.PlaceProductOrderRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.PlaceProductOrderRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.PlaceProductOrderResponseE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ProductOrderChangeType;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ProductOrderInfo;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ReDeliveryExchangeRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ReDeliveryExchangeRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ReDeliveryExchangeResponseE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.RejectExchangeRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.RejectExchangeResponseE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.RejectReturnRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.RejectReturnResponseE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ReleaseExchangeHoldRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ReleaseExchangeHoldResponseE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ReleaseReturnHoldRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ReleaseReturnHoldResponseE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.RequestReturnRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.RequestReturnResponseE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.WithholdExchangeRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.WithholdExchangeResponseE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.WithholdReturnRequestE;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.WithholdReturnResponseE;
import java.security.Security;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import kr.co.homeplus.outbound.market.naver.claim.enums.NaverMarketClaimReasonType;
import kr.co.homeplus.outbound.market.naver.claim.enums.NaverMarketHoldType;
import kr.co.homeplus.outbound.market.naver.claim.model.ClaimOrderDto;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverAddressDto;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverClaimDataDto;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverClaimDataDto.MarketClaimProductOrderDto;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverExchangeInfoDto;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverProductOrderDto;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverReturnInfoDto;
import kr.co.homeplus.outbound.market.naver.config.MarketNaverConfig;
import kr.co.homeplus.outbound.market.naver.claim.model.CancelInfo;
import kr.co.homeplus.outbound.market.naver.util.NaverCryptoUtil;
import kr.co.homeplus.outbound.market.naver.util.NaverLogUtil;
import kr.co.homeplus.outbound.market.naver.util.NaverModelMapper;
import kr.co.homeplus.outbound.market.naver.util.NaverSendUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class NaverClaimService {

    private final MarketNaverConfig marketNaverConfig;
    private String timeStamp;

    /**
     * N마트 - 클레임 조회(취소/반품/교환)
     *
     * @param startDt 조회시작일
     * @param endDt 조회종료일
     * @param statusCode 조회코드 (CANCEL_REQUESTED, CANCELED, RETURN_REQUESTED, RETURNED, EXCHANGE_REQUESTED, EXCHANGED, EXCHANGE_REDELIVERY_READY)
     * @throws Exception
     */
    public List<NaverClaimDataDto> getChangedProductOrderList(String startDt, String endDt, ProductOrderChangeType statusCode) throws Exception {
        List<NaverClaimDataDto> orderList = new ArrayList<>();

        Security.addProvider(new BouncyCastleProvider());
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());
        GetChangedProductOrderListRequestE changedProductOrderListRequestE = new SellerServiceStub.GetChangedProductOrderListRequestE();
        changedProductOrderListRequestE.setGetChangedProductOrderListRequest(NaverSendUtil.createGetChangedProductOrderListRequest(startDt, endDt, statusCode));
        changedProductOrderListRequestE.getGetChangedProductOrderListRequest().setAccessCredentials(this.createAccessCredentialsType("GetChangedProductOrderList"));

        //GetChangedProductOrderList Response 수신
        SellerServiceStub.GetChangedProductOrderListResponseE changedProductOrderListResponseE = sellerServiceStub.getChangedProductOrderList(changedProductOrderListRequestE);
        //결과 출력
        if("SUCCESS".equals(changedProductOrderListResponseE.getGetChangedProductOrderListResponse().getResponseType())) {
            ChangedProductOrderInfo[] changedProductOrderInfos = changedProductOrderListResponseE.getGetChangedProductOrderListResponse().getChangedProductOrderInfoList();
            if (changedProductOrderInfos != null) {
                for (int i=0; i<changedProductOrderInfos.length; i++) {
                    GetProductOrderInfoListRequestE productOrderInfoListRequestE = new SellerServiceStub.GetProductOrderInfoListRequestE();
                    productOrderInfoListRequestE.setGetProductOrderInfoListRequest(NaverSendUtil.createGetProductOrderInfoListRequest(changedProductOrderInfos[i].getProductOrderID()));
                    productOrderInfoListRequestE.getGetProductOrderInfoListRequest().setAccessCredentials(this.createAccessCredentialsType("GetProductOrderInfoList"));

                    GetProductOrderInfoListResponseE productOrderInfoListResponseE = sellerServiceStub.getProductOrderInfoList(productOrderInfoListRequestE);
                    if(!"SUCCESS".equals(productOrderInfoListResponseE.getGetProductOrderInfoListResponse().getResponseType())) {
                        NaverLogUtil.errorLog(productOrderInfoListResponseE.getGetProductOrderInfoListResponse().getError());
                        continue;
                    }
                    ProductOrderInfo[] productOrderInfos = productOrderInfoListResponseE.getGetProductOrderInfoListResponse().getProductOrderInfoList();

                    NaverClaimDataDto claimDataDto = new NaverClaimDataDto();

                    for (int j=0; j<productOrderInfos.length; j++) {
                        MarketClaimProductOrderDto marketClaimProductOrderDto = new MarketClaimProductOrderDto();
                        SellerServiceStub.Order order = productOrderInfos[j].getOrder();
                        ClaimOrderDto convertedOrder = NaverModelMapper.orderMap(order, this.timeStamp);
                        claimDataDto.setOrder(convertedOrder);

                        if(orderList.size() == 0 || orderList.stream().noneMatch(dto -> dto.getOrder().getOrderId().equals(convertedOrder.getOrderId()))) {
                            orderList.add(claimDataDto);
                        }

                        List<MarketClaimProductOrderDto> claimMarketProductList = new ArrayList<>();
                        SellerServiceStub.ProductOrder productOrder = productOrderInfos[j].getProductOrder();
                        NaverProductOrderDto convertedProductOrder = NaverModelMapper.productOrderMap(productOrder, this.timeStamp);

                        SellerServiceStub.Address shippingAddress = productOrder.getShippingAddress();
                        NaverAddressDto convertedShippingAddress = NaverModelMapper.addressMap(shippingAddress, this.timeStamp);
                        convertedProductOrder.setShippingAddress(convertedShippingAddress);

                        SellerServiceStub.Address takingAddress = productOrder.getTakingAddress();
                        NaverAddressDto convertedTakingAddress = NaverModelMapper.addressMap(takingAddress, this.timeStamp);
                        convertedProductOrder.setTakingAddress(convertedTakingAddress);
                        marketClaimProductOrderDto.setProductOrder(convertedProductOrder);

                        SellerServiceStub.CancelInfo cancelInfo = productOrderInfos[j].getCancelInfo();
                        if(cancelInfo != null){
                            CancelInfo convertCancelInfo = NaverModelMapper.cancelInfoMap(cancelInfo, this.timeStamp);
                            marketClaimProductOrderDto.setCancelInfo(convertCancelInfo);
                        }
                        SellerServiceStub.ReturnInfo returnInfo = productOrderInfos[j].getReturnInfo();
                        if(returnInfo != null){
                            NaverReturnInfoDto convertReturnInfo = NaverModelMapper.returnInfoMap(returnInfo, this.timeStamp);
                            marketClaimProductOrderDto.setReturnInfo(convertReturnInfo);
                            if(marketClaimProductOrderDto.getReturnInfo().getCollectAddress() == null) {
                                marketClaimProductOrderDto.getReturnInfo().setCollectAddress(NaverAddressDto.builder().build());
                            }
                            if(marketClaimProductOrderDto.getReturnInfo().getReturnReceiveAddress() == null) {
                                marketClaimProductOrderDto.getReturnInfo().setReturnReceiveAddress(NaverAddressDto.builder().build());
                            }
                        }
                        SellerServiceStub.ExchangeInfo exchangeInfo = productOrderInfos[j].getExchangeInfo();
                        if(exchangeInfo != null) {
                            NaverExchangeInfoDto convertExchangeInfo = NaverModelMapper.exchangeInfoMap(exchangeInfo, this.timeStamp);
                            marketClaimProductOrderDto.setExchangeInfo(convertExchangeInfo);
                            if(marketClaimProductOrderDto.getExchangeInfo().getCollectAddress() == null) {
                                marketClaimProductOrderDto.getExchangeInfo().setCollectAddress(NaverAddressDto.builder().build());
                            }
                            if(marketClaimProductOrderDto.getExchangeInfo().getReturnReceiveAddress() == null) {
                                marketClaimProductOrderDto.getExchangeInfo().setReturnReceiveAddress(NaverAddressDto.builder().build());
                            }
                        }
                        if(orderList.stream().filter(dto -> dto.getOrder().getOrderId().equals(convertedOrder.getOrderId())).findFirst().get().getMarketClaimProductOrderList() == null) {
                            orderList.stream().filter(dto -> dto.getOrder().getOrderId().equals(convertedOrder.getOrderId())).findFirst().get().setMarketClaimProductOrderList(new ArrayList<>());
                        }
                        orderList.stream().filter(dto -> dto.getOrder().getOrderId().equals(convertedOrder.getOrderId())).findFirst().get().getMarketClaimProductOrderList().add(marketClaimProductOrderDto);
                    }
                }
            }
        } else {
            NaverLogUtil.errorLog(changedProductOrderListResponseE.getGetChangedProductOrderListResponse().getError());
        }
        return orderList;
    }

    public List<NaverClaimDataDto> getProductOrderList(String productOrderId) throws Exception {
        List<NaverClaimDataDto> orderList = new ArrayList<>();

        Security.addProvider(new BouncyCastleProvider());
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());
        GetProductOrderInfoListRequestE productOrderInfoListRequestE = new SellerServiceStub.GetProductOrderInfoListRequestE();
        productOrderInfoListRequestE.setGetProductOrderInfoListRequest(NaverSendUtil.createGetProductOrderInfoListRequest(productOrderId));
        productOrderInfoListRequestE.getGetProductOrderInfoListRequest().setAccessCredentials(this.createAccessCredentialsType("GetProductOrderInfoList"));

        GetProductOrderInfoListResponseE productOrderInfoListResponseE = sellerServiceStub.getProductOrderInfoList(productOrderInfoListRequestE);

        if("SUCCESS".equals(productOrderInfoListResponseE.getGetProductOrderInfoListResponse().getResponseType())) {
            ProductOrderInfo[] productOrderInfos = productOrderInfoListResponseE.getGetProductOrderInfoListResponse().getProductOrderInfoList();

            NaverClaimDataDto claimDataDto = new NaverClaimDataDto();

            SellerServiceStub.Order order = productOrderInfos[0].getOrder();
            ClaimOrderDto convertedOrder = NaverModelMapper.orderMap(order, this.timeStamp);
            claimDataDto.setOrder(convertedOrder);

            List<MarketClaimProductOrderDto> claimMarketProductList = new ArrayList<>();
            for(ProductOrderInfo productOrderInfo : productOrderInfos) {
                MarketClaimProductOrderDto marketClaimProductOrderDto = new MarketClaimProductOrderDto();

                SellerServiceStub.ProductOrder productOrder = productOrderInfo.getProductOrder();
                NaverProductOrderDto convertedProductOrder = NaverModelMapper.productOrderMap(productOrder, this.timeStamp);

                SellerServiceStub.Address shippingAddress = productOrder.getShippingAddress();
                NaverAddressDto convertedShippingAddress = NaverModelMapper.addressMap(shippingAddress, this.timeStamp);
                convertedProductOrder.setShippingAddress(convertedShippingAddress);
                SellerServiceStub.Address takingAddress = productOrder.getTakingAddress();
                NaverAddressDto convertedTakingAddress = NaverModelMapper.addressMap(takingAddress, this.timeStamp);
                convertedProductOrder.setTakingAddress(convertedTakingAddress);
                marketClaimProductOrderDto.setProductOrder(convertedProductOrder);

                SellerServiceStub.CancelInfo cancelInfo = productOrderInfo.getCancelInfo();
                if(cancelInfo != null){
                    CancelInfo convertCancelInfo = NaverModelMapper.cancelInfoMap(cancelInfo, this.timeStamp);
                    marketClaimProductOrderDto.setCancelInfo(convertCancelInfo);
                }
                SellerServiceStub.ReturnInfo returnInfo = productOrderInfo.getReturnInfo();
                if(returnInfo != null){
                    NaverReturnInfoDto convertReturnInfo = NaverModelMapper.returnInfoMap(returnInfo, this.timeStamp);
                    marketClaimProductOrderDto.setReturnInfo(convertReturnInfo);
                }
                SellerServiceStub.ExchangeInfo exchangeInfo = productOrderInfo.getExchangeInfo();
                if(exchangeInfo != null) {
                    NaverExchangeInfoDto convertExchangeInfo = NaverModelMapper.exchangeInfoMap(exchangeInfo, this.timeStamp);
                    marketClaimProductOrderDto.setExchangeInfo(convertExchangeInfo);
                }
                claimMarketProductList.add(marketClaimProductOrderDto);
            }
            claimDataDto.setMarketClaimProductOrderList(claimMarketProductList);
            orderList.add(claimDataDto);
        } else {
            NaverLogUtil.errorLog(productOrderInfoListResponseE.getGetProductOrderInfoListResponse().getError());
        }
        return orderList;
    }

    public HashMap<String, String> setCancelSaleList(List<String> productOrderIds, String claimReasonType) throws Exception {
        HashMap<String, String> result = new HashMap<>();
        for(String productOrderId : productOrderIds) {
            String resultCode = "";
            try {
                resultCode = this.setCancelSale(productOrderId, claimReasonType);
            } catch (Exception e){
                result.put(productOrderId, resultCode);
                continue;
            }
            result.put(productOrderId, resultCode);
        }
        return result;
    }

    /**
     * N마트 - 취소처리
     *
     * @param productOrderId
     * @param claimRequestReasonType
     * @return
     * @throws Exception
     */
    public String setCancelSale(String productOrderId, String claimReasonType) throws Exception {
        // provider설정.
        Security.addProvider(new BouncyCastleProvider());
        // 네이버제공 통신모듈 선언
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());
        // 주문취소 통신데이터 설정.
        CancelSaleRequestE cancelSaleRequestE = new CancelSaleRequestE();
        // 주문취소 데이터 설정.
        cancelSaleRequestE.setCancelSaleRequest(NaverSendUtil.createCancelSaleRequest(productOrderId, this.getClaimReasonType(claimReasonType)));
        // 주문취소용 접속보안데이터 설정.
        cancelSaleRequestE.getCancelSaleRequest().setAccessCredentials(this.createAccessCredentialsType("CancelSale"));
        // 네이버와 통신 결과
        CancelSaleResponseE cancelSaleResponseE = sellerServiceStub.cancelSale(cancelSaleRequestE);
        if(!"SUCCESS".equals(cancelSaleResponseE.getCancelSaleResponse().getResponseType())) {
            NaverLogUtil.errorLog(cancelSaleResponseE.getCancelSaleResponse().getError());
        }
        return cancelSaleResponseE.getCancelSaleResponse().getResponseType();
    }

    public String setClaimApprove(String productOrderId, String claimType) throws Exception {
        if(claimType.equals("CANCEL")) {
            return this.setCancelApprove(productOrderId);
        } else if (claimType.equals("RETURN")) {
            return this.setApproveReturn(productOrderId);
        } else {
            // 교환인 경우에만 완료처리.
            return this.setCompleteExchange(productOrderId);
        }
    }

    public String setClaimReject(String productOrderId, String claimType, String claimDetailReason) throws Exception {
        if (claimType.equals("RETURN")) {
            return this.setRejectReturn(productOrderId, claimDetailReason);
        } else {
            return this.setRejectExchange(productOrderId, claimDetailReason);
        }
    }

    public String setClaimHold(String productOrderId, String claimType, String claimReasonType, String claimDetailReason) throws Exception {
        try {
            HoldbackClassType holdbackClassType = this.getHoldReasonType(claimReasonType);
            if(holdbackClassType != null) {
                if (claimType.equals("RETURN")) {
                    return this.setWithholdReturn(productOrderId, holdbackClassType, claimDetailReason);
                } else {
                    return this.setWithholdExchange(productOrderId, holdbackClassType, claimDetailReason);
                }
            } else {
                throw new Exception("타입이 없습니다.");
            }
        } catch (Exception e) {
            log.error("N마켓 상품번호({}) 보류 등록 실패 :: {}", productOrderId, e.getMessage());
            return null;
        }
    }

    public String setClaimReleaseHold(String productOrderId, String claimType) throws Exception {
        if (claimType.equals("RETURN")) {
            return this.setReleaseReturnHold(productOrderId);
        } else {
            return this.setReleaseExchangeHold(productOrderId);
        }
    }

    /**
     * N마트 - 취소승인처리
     *
     * @param productOrderId
     * @throws Exception
     */
    public String setCancelApprove(String productOrderId) throws Exception {
        // provider설정.
        Security.addProvider(new BouncyCastleProvider());
        // 네이버제공 통신모듈 선언
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());
        // 취소승인 통신데이터 설정.
        ApproveCancelApplicationRequestE approveCancelApplicationRequestE = new ApproveCancelApplicationRequestE();
        // 취소승인 데이터 설정.
        approveCancelApplicationRequestE.setApproveCancelApplicationRequest(NaverSendUtil.createApproveCancelRequest(productOrderId));
        // 취소승인용 접속보안데이터 설정.
        approveCancelApplicationRequestE.getApproveCancelApplicationRequest().setAccessCredentials(this.createAccessCredentialsType("ApproveCancelApplication"));
        // 네이버와 통신 결과
        ApproveCancelApplicationResponseE approveCancelApplicationResponseE = sellerServiceStub.approveCancelApplication(approveCancelApplicationRequestE);

        if(!"SUCCESS".equals(approveCancelApplicationResponseE.getApproveCancelApplicationResponse().getResponseType())) {
            NaverLogUtil.errorLog(approveCancelApplicationResponseE.getApproveCancelApplicationResponse().getError());
        }

        return approveCancelApplicationResponseE.getApproveCancelApplicationResponse().getResponseType();
    }

    /**
     * N마트 - 반품신청
     *
     * @param productOrderId
     * @param reasonType
     * @return
     * @throws Exception
     */
    public String setRequestReturn(String productOrderId, String reasonType) throws Exception {
        // provider설정.
        Security.addProvider(new BouncyCastleProvider());
        // 네이버제공 통신모듈 선언
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());
        // 반품접수 통신데이터 설정.
        RequestReturnRequestE requestReturnRequestE = new RequestReturnRequestE();
        // 반품접수 데이터 설정.
        requestReturnRequestE.setRequestReturnRequest(NaverSendUtil.createRequestReturnRequest(productOrderId, this.getClaimReasonType(reasonType)));
        // 반품접수용 접속보안데이터 설정.
        requestReturnRequestE.getRequestReturnRequest().setAccessCredentials(this.createAccessCredentialsType("RequestReturn"));

        RequestReturnResponseE requestReturnResponseE = sellerServiceStub.requestReturn(requestReturnRequestE);

        if(!"SUCCESS".equals(requestReturnResponseE.getRequestReturnResponse().getResponseType())) {
            NaverLogUtil.errorLog(requestReturnResponseE.getRequestReturnResponse().getError());
        }

        return requestReturnResponseE.getRequestReturnResponse().getResponseType();
    }

    /**
     * N마트 - 반품승인처리
     *
     * @param productOrderId
     * @return
     * @throws Exception
     */
    public String setApproveReturn(String productOrderId) throws Exception {
        // provider설정.
        Security.addProvider(new BouncyCastleProvider());
        // 네이버제공 통신모듈 선언
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());
        // 반품접수 통신데이터 설정.
        ApproveReturnApplicationRequestE approveReturnApplicationRequestE = new ApproveReturnApplicationRequestE();
        // 반품접수 데이터 설정.
        approveReturnApplicationRequestE.setApproveReturnApplicationRequest(NaverSendUtil.createApproveReturnRequest(productOrderId));
        // 반품접수용 접속보안데이터 설정.
        approveReturnApplicationRequestE.getApproveReturnApplicationRequest().setAccessCredentials(this.createAccessCredentialsType("ApproveReturnApplication"));

        ApproveReturnApplicationResponseE approveReturnApplicationResponseE = sellerServiceStub.approveReturnApplication(approveReturnApplicationRequestE);

        if(!"SUCCESS".equals(approveReturnApplicationResponseE.getApproveReturnApplicationResponse().getResponseType())) {
            NaverLogUtil.errorLog(approveReturnApplicationResponseE.getApproveReturnApplicationResponse().getError());
        }

        return approveReturnApplicationResponseE.getApproveReturnApplicationResponse().getResponseType();
    }

    /**
     * N마트 - 교환승인처리
     *
     * @param productOrderId
     * @return
     * @throws Exception
     */
    public String setExchangeApprove(String productOrderId) throws Exception {
        // provider설정.
        Security.addProvider(new BouncyCastleProvider());
        // 네이버제공 통신모듈 선언
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());
        // 교환승인 통신데이터 설정.
        ApproveCollectedExchangeRequestE approveCollectedExchangeRequestE = new ApproveCollectedExchangeRequestE();
        // 교환승인 데이터 설정.
        approveCollectedExchangeRequestE.setApproveCollectedExchangeRequest(NaverSendUtil.createApproveExchangeRequest(productOrderId));
        // 교환승인용 접속보안데이터 설정.
        approveCollectedExchangeRequestE.getApproveCollectedExchangeRequest().setAccessCredentials(this.createAccessCredentialsType("ApproveCollectedExchange"));

        ApproveCollectedExchangeResponseE approveCollectedExchangeResponseE = sellerServiceStub.approveCollectedExchange(approveCollectedExchangeRequestE);

        log.info("교환상품 수거완료 요청 :: {}", approveCollectedExchangeRequestE.getApproveCollectedExchangeRequest().getProductOrderID());

        if(!"SUCCESS".equals(approveCollectedExchangeResponseE.getApproveCollectedExchangeResponse().getResponseType())) {
            NaverLogUtil.errorLog(approveCollectedExchangeResponseE.getApproveCollectedExchangeResponse().getError());
        }
        return approveCollectedExchangeResponseE.getApproveCollectedExchangeResponse().getResponseType();
    }

    /**
     * N마트 - 반품거부
     *
     * @param productOrderId
     * @param rejectDetailContent
     * @return
     * @throws Exception
     */
    public String setRejectReturn(String productOrderId, String rejectDetailContent) throws Exception {
        // provider설정.
        Security.addProvider(new BouncyCastleProvider());
        // 네이버제공 통신모듈 선언
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());
        // 반품거절 통신데이터 설정.
        RejectReturnRequestE rejectReturnRequestE = new RejectReturnRequestE();
        // 반품거절 데이터 설정.
        rejectReturnRequestE.setRejectReturnRequest(NaverSendUtil.createRejectReturnRequest(productOrderId, rejectDetailContent));
        // 반품거절용 접속보안데이터 설정.
        rejectReturnRequestE.getRejectReturnRequest().setAccessCredentials(this.createAccessCredentialsType("RejectReturn"));

        RejectReturnResponseE rejectReturnResponseE  = sellerServiceStub.rejectReturn(rejectReturnRequestE);

        if(!"SUCCESS".equals(rejectReturnResponseE.getRejectReturnResponse().getResponseType())) {
            NaverLogUtil.errorLog(rejectReturnResponseE.getRejectReturnResponse().getError());
        }
        return rejectReturnResponseE.getRejectReturnResponse().getResponseType();
    }

    /**
     * N마트 - 반품보류
     *
     * @param productOrderId
     * @param holdCode
     * @param withholdDetailContent
     * @return
     * @throws Exception
     */
    public String setWithholdReturn(String productOrderId, HoldbackClassType holdCode, String withholdDetailContent) throws Exception {
        // provider설정.
        Security.addProvider(new BouncyCastleProvider());
        // 네이버제공 통신모듈 선언
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());
        // 반품보류 통신데이터 설정.
        WithholdReturnRequestE withholdReturnRequestE = new WithholdReturnRequestE();
        // 반품보류 데이터 설정.
        withholdReturnRequestE.setWithholdReturnRequest(NaverSendUtil.createWithholdReturnRequest(productOrderId, holdCode, withholdDetailContent));
        // 반품보류용 접속보안데이터 설정.
        withholdReturnRequestE.getWithholdReturnRequest().setAccessCredentials(this.createAccessCredentialsType("WithholdReturn"));

        WithholdReturnResponseE withholdReturnResponseE = sellerServiceStub.withholdReturn(withholdReturnRequestE);

        if(!"SUCCESS".equals(withholdReturnResponseE.getWithholdReturnResponse().getResponseType())) {
            NaverLogUtil.errorLog(withholdReturnResponseE.getWithholdReturnResponse().getError());
        }
        return withholdReturnResponseE.getWithholdReturnResponse().getResponseType();
    }

    /**
     * N마트 - 반품보류해제
     *
     * @param productOrderId
     * @return
     * @throws Exception
     */
    public String setReleaseReturnHold(String productOrderId) throws Exception {
        // provider설정.
        Security.addProvider(new BouncyCastleProvider());
        // 네이버제공 통신모듈 선언
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());
        // 반품보류해제 통신데이터 설정.
        ReleaseReturnHoldRequestE releaseReturnHoldRequestE = new ReleaseReturnHoldRequestE();
        // 반품보류해제 데이터 설정.
        releaseReturnHoldRequestE.setReleaseReturnHoldRequest(NaverSendUtil.createReleaseReturnHoldRequest(productOrderId));
        // 반품보류해제용 접속보안데이터 설정.
        releaseReturnHoldRequestE.getReleaseReturnHoldRequest().setAccessCredentials(this.createAccessCredentialsType("ReleaseReturnHold"));

        ReleaseReturnHoldResponseE releaseReturnHoldResponseE = sellerServiceStub.releaseReturnHold(releaseReturnHoldRequestE);

        if(!"SUCCESS".equals(releaseReturnHoldResponseE.getReleaseReturnHoldResponse().getResponseType())) {
            NaverLogUtil.errorLog(releaseReturnHoldResponseE.getReleaseReturnHoldResponse().getError());
        }
        return releaseReturnHoldResponseE.getReleaseReturnHoldResponse().getResponseType();
    }

    /**
     * N마트 - 교환거부
     *
     * @param productOrderId
     * @param rejectDetailContent
     * @return
     * @throws Exception
     */
    public String setRejectExchange(String productOrderId, String rejectDetailContent) throws Exception {
        // provider설정.
        Security.addProvider(new BouncyCastleProvider());
        // 네이버제공 통신모듈 선언
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());
        // 교환거절 통신데이터 설정.
        RejectExchangeRequestE rejectExchangeRequestE = new RejectExchangeRequestE();
        // 교환거절 데이터 설정.
        rejectExchangeRequestE.setRejectExchangeRequest(NaverSendUtil.createRejectExchangeRequest(productOrderId, rejectDetailContent));
        // 교환거절용 접속보안데이터 설정.
        rejectExchangeRequestE.getRejectExchangeRequest().setAccessCredentials(this.createAccessCredentialsType("RejectExchange"));

        RejectExchangeResponseE rejectExchangeResponseE  = sellerServiceStub.rejectExchange(rejectExchangeRequestE);

        if(!"SUCCESS".equals(rejectExchangeResponseE.getRejectExchangeResponse().getResponseType())) {
            NaverLogUtil.errorLog(rejectExchangeResponseE.getRejectExchangeResponse().getError());
        }
        return rejectExchangeResponseE.getRejectExchangeResponse().getResponseType();
    }

    /**
     * N마트 - 교환보류처리
     *
     * @param productOrderId
     * @param holdCode
     * @param withholdDetailContent
     * @return
     * @throws Exception
     */
    public String setWithholdExchange(String productOrderId, HoldbackClassType holdCode, String withholdDetailContent) throws Exception {
        // provider설정.
        Security.addProvider(new BouncyCastleProvider());
        // 네이버제공 통신모듈 선언
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());
        // 교환보류 통신데이터 설정.
        WithholdExchangeRequestE withholdExchangeRequestE = new WithholdExchangeRequestE();
        // 교환보류 데이터 설정.
        withholdExchangeRequestE.setWithholdExchangeRequest(NaverSendUtil.createWithholdExchangeRequest(productOrderId, holdCode, withholdDetailContent));
        // 교환보류용 접속보안데이터 설정.
        withholdExchangeRequestE.getWithholdExchangeRequest().setAccessCredentials(this.createAccessCredentialsType("WithholdExchange"));

        WithholdExchangeResponseE withholdExchangeResponseE = sellerServiceStub.withholdExchange(withholdExchangeRequestE);

        if(!"SUCCESS".equals(withholdExchangeResponseE.getWithholdExchangeResponse().getResponseType())) {
            NaverLogUtil.errorLog(withholdExchangeResponseE.getWithholdExchangeResponse().getError());
        }
        return withholdExchangeResponseE.getWithholdExchangeResponse().getResponseType();
    }

    /**
     * N마트 - 교환보류해제
     *
     * @param productOrderId
     * @return
     * @throws Exception
     */
    public String setReleaseExchangeHold(String productOrderId) throws Exception {
        // provider설정.
        Security.addProvider(new BouncyCastleProvider());
        // 네이버제공 통신모듈 선언
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());
        // 교환보류해제 통신데이터 설정.
        ReleaseExchangeHoldRequestE releaseExchangeHoldRequestE = new ReleaseExchangeHoldRequestE();
        // 교환보류해제 데이터 설정.
        releaseExchangeHoldRequestE.setReleaseExchangeHoldRequest(NaverSendUtil.createReleaseExchangeHoldRequest(productOrderId));
        // 교환보류해제용 접속보안데이터 설정.
        releaseExchangeHoldRequestE.getReleaseExchangeHoldRequest().setAccessCredentials(this.createAccessCredentialsType("ReleaseExchangeHold"));

        ReleaseExchangeHoldResponseE releaseExchangeHoldResponseE = sellerServiceStub.releaseExchangeHold(releaseExchangeHoldRequestE);

        if(!"SUCCESS".equals(releaseExchangeHoldResponseE.getReleaseExchangeHoldResponse().getResponseType())) {
            NaverLogUtil.errorLog(releaseExchangeHoldResponseE.getReleaseExchangeHoldResponse().getError());
        }
        return releaseExchangeHoldResponseE.getReleaseExchangeHoldResponse().getResponseType();
    }

    public String setPlaceProductOrder(String productOrderId) throws Exception {
        // provider설정.
        Security.addProvider(new BouncyCastleProvider());
        // 네이버제공 통신모듈 선언
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());
        PlaceProductOrderRequestE placeProductOrderRequestE = new PlaceProductOrderRequestE();
        PlaceProductOrderRequest placeProductOrderRequest = new PlaceProductOrderRequest();
        placeProductOrderRequest.setProductOrderID(productOrderId);
        placeProductOrderRequest.setAccessCredentials(this.createAccessCredentialsType("PlaceProductOrder"));
        placeProductOrderRequest.setDetailLevel(marketNaverConfig.getDetailLevel());
        placeProductOrderRequest.setVersion(marketNaverConfig.getOrder().getServiceVersion());
        placeProductOrderRequest.setRequestID(UUID.randomUUID().toString());
        placeProductOrderRequestE.setPlaceProductOrderRequest(placeProductOrderRequest);

        PlaceProductOrderResponseE placeProductOrderResponseE = sellerServiceStub.placeProductOrder(placeProductOrderRequestE);

        if(!"SUCCESS".equals(placeProductOrderResponseE.getPlaceProductOrderResponse().getResponseType())) {
            NaverLogUtil.errorLog(placeProductOrderResponseE.getPlaceProductOrderResponse().getError());
        }
        return placeProductOrderResponseE.getPlaceProductOrderResponse().getResponseType();
    }

    public String setCompleteExchange(String productOrderId) throws Exception {
        // provider설정.
        Security.addProvider(new BouncyCastleProvider());
        // 네이버제공 통신모듈 선언
        SellerServiceStub sellerServiceStub = NaverCryptoUtil.createSellerServiceStub(marketNaverConfig.getTargetEndpoint(), marketNaverConfig.getOrder().getServiceName());

        ReDeliveryExchangeRequestE reDeliveryExchangeRequestE = new ReDeliveryExchangeRequestE();
        // 파라미터 설정
        reDeliveryExchangeRequestE.setReDeliveryExchangeRequest(NaverSendUtil.createReDeliveryExchangeRequest(productOrderId));
        // 암복구화 값 설정.
        reDeliveryExchangeRequestE. getReDeliveryExchangeRequest().setAccessCredentials(this.createAccessCredentialsType("ReDeliveryExchange"));

        log.info("교환상품 재배송 요청 :: {}", reDeliveryExchangeRequestE.getReDeliveryExchangeRequest().getProductOrderID());

        ReDeliveryExchangeResponseE reDeliveryExchangeResponseE = sellerServiceStub.reDeliveryExchange(reDeliveryExchangeRequestE);

        if(!"SUCCESS".equals(reDeliveryExchangeResponseE.getReDeliveryExchangeResponse().getResponseType())) {
            NaverLogUtil.errorLog(reDeliveryExchangeResponseE.getReDeliveryExchangeResponse().getError());
        }
        return reDeliveryExchangeResponseE.getReDeliveryExchangeResponse().getResponseType();
    }

    /**
     * N마트 - API 별 접속자격 취득
     *
     * @param operationName
     * @return
     */
    private AccessCredentialsType createAccessCredentialsType(String operationName) {
        AccessCredentialsType accessCredentialsType = NaverCryptoUtil.createSellerAccessCredentialType(marketNaverConfig.getOrder().getServiceName(), operationName);
        this.timeStamp = accessCredentialsType.getTimestamp();
        return accessCredentialsType;
    }

    /**
     * N마트 - 복호화
     *
     * @param encryptedData
     * @return
     */
    private String convertDecrypt(String encryptedData) {
        return NaverCryptoUtil.decrypt(this.timeStamp, encryptedData);
    }

    private ClaimRequestReasonType getClaimReasonType(String claimReasonType) {
        log.info("getClaimReasonType : {}", NaverMarketClaimReasonType.getClaimRequestReasonType(claimReasonType));
        return NaverMarketClaimReasonType.getClaimRequestReasonType(claimReasonType);
    }

    private HoldbackClassType getHoldReasonType(String claimReasonType) {
        log.info("getHoldReasonType : {}", NaverMarketHoldType.getHoldReasonType(claimReasonType));
        return NaverMarketHoldType.getHoldReasonType(claimReasonType);
    }

}
