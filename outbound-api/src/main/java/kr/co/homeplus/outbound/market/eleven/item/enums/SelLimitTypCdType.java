package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

/**
 *  최대구매수량 설정코드
 */

public enum SelLimitTypCdType {
	@XmlEnumValue("00")
	NO_LIMIT("00", "무제한"),

	@XmlEnumValue("01")
	LIMIT_ONCE("01", "1회 제한"),

	@XmlEnumValue("02")
	LIMIT_PERIOD("02", "기간 제한");

	@Getter
	private String code;
	@Getter
	private String desc;

	private SelLimitTypCdType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}
