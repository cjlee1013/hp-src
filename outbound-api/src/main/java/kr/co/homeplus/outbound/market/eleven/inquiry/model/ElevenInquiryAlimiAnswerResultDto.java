package kr.co.homeplus.outbound.market.eleven.inquiry.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ElevenInquiryAlimiAnswerResultDto {

	/**
     * 긴급알리미 고유 key값
     */
    private String emerNtceSeq;

    /**
     * 주문번호
     */
    private String ordNo;

    /**
     * 결과코드
     * 100 : 공지건에 대한 확인처리 성공
     * 200 : 답변건에 대한 답변처리 성공
     */
    @JsonProperty("result_code")
    private String resultCode;

    /**
     * 결과내용
     */
    @JsonProperty("result_text")
    private String resultText;
}
