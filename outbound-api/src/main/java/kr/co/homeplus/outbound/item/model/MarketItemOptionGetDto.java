package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 옵션 정보
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("용도옵션")
public class MarketItemOptionGetDto {

	@ApiModelProperty(value = "옵션번호")
	private Long   optNo;

	@ApiModelProperty(value = "옵션1값")
	private String opt1Val;

	@ApiModelProperty(value = "옵션2값")
	private String opt2Val;

}
