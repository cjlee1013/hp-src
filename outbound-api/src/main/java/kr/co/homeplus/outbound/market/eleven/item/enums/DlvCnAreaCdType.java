package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

/**
 * 배송가능지역 코드
 */
public enum DlvCnAreaCdType {
	@XmlEnumValue("01")
	WHOLE_COUNTRY("01", "전국"),

	@XmlEnumValue("02")
	WHOLE_COUNTRY_EXCEPT_JEJU("02", "전국( 제주 도서산간 제외 )"),

	@XmlEnumValue("03")
	SEOUL("03", "서울"),

	@XmlEnumValue("04")
	INCHEON("04", "인천"),

	@XmlEnumValue("05")
	GUANGZHOU("05", "광주"),

	@XmlEnumValue("06")
	DAEGU("06", "대구"),

	@XmlEnumValue("07")
	DAEJEON("07", "대전"),

	@XmlEnumValue("08")
	BUSAN("08", "부산"),

	@XmlEnumValue("09")
	ULSAN("09", "울산"),

	@XmlEnumValue("10")
	GYEONGGI("10", "경기"),

	@XmlEnumValue("11")
	KANGWON("11", "강원"),

	@XmlEnumValue("12")
	CHUNGNAM("12", "충남"),
	// TODO 11: 둘중하나는 충남임 확인 필요.

	@XmlEnumValue("13")
	CHUNGBUK("13", "충북"),

	@XmlEnumValue("14")
	KYUNGNAM("14", "경남"),

	@XmlEnumValue("15")
	KYUNGBUK("15", "경북"),

	@XmlEnumValue("16")
	JEONNAM("16", "전남"),

	@XmlEnumValue("17")
	JEONBUK("17", "전북"),

	@XmlEnumValue("18")
	JEJU("18", "제주"),

	@XmlEnumValue("19")
	SEOUL_GYEONGGI("19", "서울/경기"),

	@XmlEnumValue("20")
	SEOUL_GYEONGGI_DAEJEON("20", "서울/경기/대전"),

	@XmlEnumValue("21")
	CHUNGBUK_CHUNGNAM("21", "충북/충남"),

	@XmlEnumValue("22")
	KYUNGBUK_KYUNGNAM("22", "경북/경남"),

	@XmlEnumValue("23")
	JEONBUK_JEONNAM("23", "전북/전남"),

	@XmlEnumValue("24")
	BUSAN_ULSAN("24", "부산/울산"),

	@XmlEnumValue("25")
	EXCEPT_SEOUL_GYEONGGI_JEJU("25", "서울/경기/제주도서산간 제외지역"),

	@XmlEnumValue("26")
	EXCEPT_SOME_PLACE("26", "일부지역 제외");

	@Getter
	private String code;
	@Getter
	private String desc;

	private DlvCnAreaCdType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}