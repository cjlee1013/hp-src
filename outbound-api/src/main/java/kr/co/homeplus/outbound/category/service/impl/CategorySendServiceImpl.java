package kr.co.homeplus.outbound.category.service.impl;

import java.util.HashMap;
import java.util.Map;
import kr.co.homeplus.outbound.category.model.sk11st.CategoryListGetDto;
import kr.co.homeplus.outbound.category.service.CategorySendService;
import kr.co.homeplus.outbound.core.constants.ElevenUriConstants;
import kr.co.homeplus.outbound.core.constants.UriConstants;
import kr.co.homeplus.outbound.core.utility.RestUtil;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class CategorySendServiceImpl extends RestUtil implements CategorySendService {

	protected CategorySendServiceImpl(RestTemplate restTemplate) {
		super(restTemplate);
	}

	@Override
	public CategoryListGetDto getCategoryList() {
		Map< String, String > params = new HashMap< String, String >();
		return execute(HttpMethod.GET, getHost()+ ElevenUriConstants.ELEVEN_CATEGORY, null,
			new ParameterizedTypeReference<>() {
			}, params);
	}
}

