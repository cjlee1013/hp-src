package kr.co.homeplus.outbound.market.eleven.inquiry.enums;

/**
 * 처리상태
 */

public enum EmerNtceCrntCdType {
    UNCHECKED( "01", "미확인" , false),
    ANSWER_WAIT( "02", "답변대기" , false),
    ANSWER_COMPLETE( "03", "답변완료" , true),
    REANSWER_REQUEST( "04", "재답변요청" , false),
    REANSWER_COMPLETE( "05", "재답변완료" , true),
    COMPLETE( "06", "처리완료" , true);

    private String code;
    private String desc;
    private Boolean isSendYn;

    private EmerNtceCrntCdType( String code, String desc , Boolean isSendYn) {
        setDesc( desc );
        setCode( code );
        setIsSendYn( isSendYn );   
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode( String code ) {
        this.code = code;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc
     *            the desc to set
     */
    public void setDesc( String desc ) {
        this.desc = desc;
    }
    
    /**
     * @return the isSendYn
     */
    public Boolean getIsSendYn() {
        return isSendYn;
    }

    /**
     * @param isSendYn
     *            the isSendYn to set
     */
    public void setIsSendYn( Boolean isSendYn ) {
        this.isSendYn = isSendYn;
    }
    
    public static EmerNtceCrntCdType getTypeOf( String dtlsCd ) {
        for ( EmerNtceCrntCdType ntceCd : EmerNtceCrntCdType.values() ) {
            if ( ntceCd.getCode().equals( dtlsCd ) ) {
                return ntceCd;
            }
        }
        throw new IllegalArgumentException( "일치하는 타입이 없습니다." );
    }
}
