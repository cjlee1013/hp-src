package kr.co.homeplus.outbound.market.naver.claim.enums;

import com.nhncorp.platform.shopn.seller.SellerServiceStub.ClaimRequestReasonType;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum NaverMarketClaimReasonType {

    CLAIM_CANCEL_01("C001", ClaimRequestReasonType.SOLD_OUT),
    CLAIM_CANCEL_02("C002", ClaimRequestReasonType.PRODUCT_UNSATISFIED),
    CLAIM_CANCEL_03("C003", ClaimRequestReasonType.WRONG_ORDER),
    CLAIM_CANCEL_04("C004", ClaimRequestReasonType.INTENT_CHANGED),
    CLAIM_CANCEL_05("C005", ClaimRequestReasonType.INTENT_CHANGED),
    CLAIM_CANCEL_06("C006", ClaimRequestReasonType.INCORRECT_INFO),
    CLAIM_CANCEL_07("C007", ClaimRequestReasonType.SOLD_OUT),
    CLAIM_CANCEL_08("C008", ClaimRequestReasonType.INTENT_CHANGED),
    CLAIM_CANCEL_09("C009", ClaimRequestReasonType.SOLD_OUT),

    CLAIM_CANCEL_10("OOS1", ClaimRequestReasonType.SOLD_OUT),
    CLAIM_CANCEL_11("OOS2", ClaimRequestReasonType.SOLD_OUT),
    CLAIM_CANCEL_12("OOS3", ClaimRequestReasonType.INTENT_CHANGED),

    CLAIM_RETURN_01("R001", ClaimRequestReasonType.SOLD_OUT),
    CLAIM_RETURN_02("R002", ClaimRequestReasonType.INTENT_CHANGED),
    CLAIM_RETURN_03("R003", ClaimRequestReasonType.BROKEN),
    CLAIM_RETURN_04("R004", ClaimRequestReasonType.DROPPED_DELIVERY),
    CLAIM_RETURN_05("R005", ClaimRequestReasonType.DROPPED_DELIVERY),
    CLAIM_RETURN_06("R006", ClaimRequestReasonType.WRONG_OPTION),
    CLAIM_RETURN_07("R007", ClaimRequestReasonType.INTENT_CHANGED),
    CLAIM_RETURN_08("R008", ClaimRequestReasonType.PRODUCT_UNSATISFIED),
    CLAIM_RETURN_09("R009", ClaimRequestReasonType.INTENT_CHANGED),
    CLAIM_RETURN_10("R010", ClaimRequestReasonType.SOLD_OUT),
    CLAIM_RETURN_11("R011", ClaimRequestReasonType.DELAYED_DELIVERY),
    CLAIM_RETURN_12("R012", ClaimRequestReasonType.INTENT_CHANGED),
    
    CLAIM_EXCHANGE_01("X001", ClaimRequestReasonType.INTENT_CHANGED),
    CLAIM_EXCHANGE_02("X002", ClaimRequestReasonType.BROKEN),
    CLAIM_EXCHANGE_03("X003", ClaimRequestReasonType.WRONG_OPTION),
    CLAIM_EXCHANGE_04("X004", ClaimRequestReasonType.PRODUCT_UNSATISFIED),
    CLAIM_EXCHANGE_05("X005", ClaimRequestReasonType.INTENT_CHANGED),
    CLAIM_EXCHANGE_06("X006", ClaimRequestReasonType.SOLD_OUT),
    ;

    private final String refitClaimReasonType;
    private final ClaimRequestReasonType naverClaimRequestReasonType;

    public static ClaimRequestReasonType getClaimRequestReasonType(String refitClaimReasonType) {
        return Stream.of(NaverMarketClaimReasonType.values()).filter(e -> e.refitClaimReasonType.equals(refitClaimReasonType)).iterator().next().naverClaimRequestReasonType;
    }

}
