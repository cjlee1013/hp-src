package kr.co.homeplus.outbound.market.naver.claim.enums;

import com.nhncorp.platform.shopn.seller.SellerServiceStub.HoldbackClassType;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum NaverMarketHoldType {

    CLAIM_HOLD_01("P001", HoldbackClassType.RETURN_PRODUCT_NOT_DELIVERED),
    CLAIM_HOLD_02("P002", HoldbackClassType.RETURN_DELIVERYFEE),
    CLAIM_HOLD_03("P003", HoldbackClassType.EXTRAFEEE),
    CLAIM_HOLD_04("P004", HoldbackClassType.ETC),
    CLAIM_HOLD_05("P005", HoldbackClassType.ETC),
    ;

    private final String refitHoldReasonType;
    private final HoldbackClassType naverHoldReasonType;

    public static HoldbackClassType getHoldReasonType(String refitHoldReasonType) {
        return Stream.of(NaverMarketHoldType.values()).filter(e -> e.refitHoldReasonType.equals(refitHoldReasonType)).iterator().next().naverHoldReasonType;
    }

}
