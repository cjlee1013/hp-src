package kr.co.homeplus.outbound.market.eleven.shipping.service;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import kr.co.homeplus.outbound.market.eleven.config.MarketElevenConfig;
import kr.co.homeplus.outbound.market.eleven.enums.DlvMthdCd;
import kr.co.homeplus.outbound.market.eleven.enums.ResultType;
import kr.co.homeplus.outbound.market.eleven.settle.model.ElevenSettleListGetDto;
import kr.co.homeplus.outbound.market.eleven.shipping.model.ElevenShipCompleteItemGetDto;
import kr.co.homeplus.outbound.market.eleven.shipping.model.ElevenShipCompleteListGetDto;
import kr.co.homeplus.outbound.market.eleven.util.ElevenConnectUtil;
import kr.co.homeplus.outbound.market.eleven.shipping.model.ElevenOrderCommonGetDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ElevenShippingService {

    private final MarketElevenConfig marketElevenConfig;

    public Boolean setShipStart(String dlvNo) throws Exception {
        // URL : /ordservices/reqdelivery/{sendDt}/{dlvMthdCd}/{dlvEtprsCd}/{invcNo}/{dlvNo}
        URL url = ElevenConnectUtil.createURL(marketElevenConfig.getShipping().getReqDelivery(), getShippingDate(), DlvMthdCd.DIRECT.getCode(),"null","null",dlvNo);
        ResponseObject<ElevenOrderCommonGetDto> responseObject = ElevenConnectUtil.execute(url,
            ElevenOrderCommonGetDto.class);
        ElevenOrderCommonGetDto resultData = responseObject.getData();
        if (resultData == null){
            log.info("setShipStart="+ responseObject.getReturnCode() + ":" + responseObject.getReturnMessage());
            return false;
        }
        if (ResultType.SUCCESS.getCode().equals(resultData.getResultCode())) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean setShipStartPart(String dlvNo, String ordNo, String ordPrdSeq) throws Exception {
        // URL : /ordservices/reqdelivery/{sendDt}/{dlvMthdCd}/{dlvEtprsCd}/{invcNo}/{dlvNo}/{partDlvYn}/{ordNo}/{ordPrdSeq}
        URL url = ElevenConnectUtil.createURL(marketElevenConfig.getShipping().getReqDeliveryPart(), getShippingDate(),DlvMthdCd.DIRECT.getCode(),"null","null",dlvNo,"Y",ordNo,ordPrdSeq);
        ResponseObject<ElevenOrderCommonGetDto> responseObject = ElevenConnectUtil.execute(url,
            ElevenOrderCommonGetDto.class);
        ElevenOrderCommonGetDto resultData = responseObject.getData();
        if (resultData == null){
            log.info("setShipStartPart="+ responseObject.getReturnCode() + ":" + responseObject.getReturnMessage());
            return false;
        }
        if (ResultType.SUCCESS.getCode().equals(resultData.getResultCode())) {
            return true;
        } else {
            return false;
        }
    }


    public Boolean setReturnInvc(String dlvNo, String clmReqSeq) throws Exception {
        // URL : /claimservice/claimservice/returninvc/{sendDt}/{dlvNo}/{dlvEtprsCd}/{invcNo}/{dlvMthdCd}/{clmReqSeq}
        URL url = ElevenConnectUtil.createURL(marketElevenConfig.getShipping().getReturnInvc(), getShippingDate(),dlvNo,"00099","null",DlvMthdCd.PARCEL.getCode(),clmReqSeq);
        ResponseObject<ElevenOrderCommonGetDto> responseObject = ElevenConnectUtil.execute(url,
            ElevenOrderCommonGetDto.class);
        ElevenOrderCommonGetDto resultData = responseObject.getData();
        if (resultData == null){
            log.info("setReturnInvc="+ responseObject.getReturnCode() + ":" + responseObject.getReturnMessage());
            return false;
        }
        if (ResultType.SUCCESS.getCode().equals(resultData.getResultCode())) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean setSellerClaimFix(String ordNo, String ordPrdSeq) throws Exception {
        // URL : /claimservice/sellerclaimfix/{ordNo}/{ordPrdSeq}/{clmReqRsn}/{claimProcess}/{dlvEtprsCd}/{invcNo}/{dlvMthdCd}/{clmReqCont}/{clmDlvCstMthd}
        URL url = ElevenConnectUtil.createURL(marketElevenConfig.getShipping().getSellerclaimfix(), ordNo,ordPrdSeq,"116","01","00099","null",DlvMthdCd.PARCEL,"판매자 반품요청","04");
        ResponseObject<ElevenOrderCommonGetDto> responseObject = ElevenConnectUtil.execute(url,
            ElevenOrderCommonGetDto.class);
        ElevenOrderCommonGetDto resultData = responseObject.getData();
        if (resultData == null){
            log.info("setSellerClaimFix="+ responseObject.getReturnCode() + ":" + responseObject.getReturnMessage());
            return false;
        }
        if (ResultType.SUCCESS.getCode().equals(resultData.getResultCode())) {
            return true;
        } else {
            return false;
        }
    }

    private String getShippingDate(){
        // 배송예정일 (직접 배송이므로 호출 당일 배송)
        return LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmm", Locale.KOREA));
    }

    public List<ElevenShipCompleteItemGetDto> getShipComplete(String startTime, String endTime) throws Exception {
        // URL : /ordservices/completed/{startTime}/{endTime}
        URL url = ElevenConnectUtil.createURL(marketElevenConfig.getShipping().getCompleted(), startTime,endTime);
        ResponseObject<ElevenShipCompleteListGetDto> responseObject = ElevenConnectUtil.execute(url,
                ElevenShipCompleteListGetDto.class);
        ElevenShipCompleteListGetDto elevenShipCompleteListGetDto = responseObject.getData();
        if (elevenShipCompleteListGetDto == null || elevenShipCompleteListGetDto.getElevenShipCompleteItemGetDto() == null){
            log.info("getShipComplete="+ responseObject.getReturnCode() + ":" + responseObject.getReturnMessage());
            return null;
        }
        return responseObject.getData().getElevenShipCompleteItemGetDto();
    }
}
