package kr.co.homeplus.outbound.safety.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.homeplus.outbound.safety.model.SafetyNo;
import kr.co.homeplus.outbound.safety.service.SafetyService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Api(tags = "안심번호")
@Slf4j
@Controller
@RequestMapping("/safety")
@RequiredArgsConstructor
public class SafetyController {

    private final SafetyService safetyService;

    /**
     * 안심번호 발급요청
     * @param safetyNo
     * @return
     */
    @ApiOperation(value = "안심번호 발급요청", response = String.class)
    @PostMapping("/issueSafetyNo")
    @ResponseBody
    public ResponseObject<String> issueSafetyNo(@RequestBody SafetyNo safetyNo) {
        return ResourceConverter.toResponseObject(safetyService.issueSafetyNo(safetyNo));
    }

    /**
     * 안심번호 해지요청
     * @param safetyNo
     * @return
     */
    @ApiOperation(value = "안심번호 해지요청", response = String.class)
    @PostMapping("/cancelSafetyNo")
    @ResponseBody
    public ResponseObject<String> cancelSafetyNo(@RequestBody SafetyNo safetyNo) {
        return ResourceConverter.toResponseObject(safetyService.cancelSafetyNo(safetyNo));
    }
}
