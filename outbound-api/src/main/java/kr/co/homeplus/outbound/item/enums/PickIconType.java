package kr.co.homeplus.outbound.item.enums;

import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.collect.ImmutableBiMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * PICK 아이콘
 */
@RequiredArgsConstructor
public enum PickIconType {

        ONE("2", "1+1", "03", null),
        TWO("3", "2+1", "04", null),
        THREE("4", "3+1", "05", null ),
        FOUR("5", "4+1", "06", null ),
        FIVE("6", "5+1", "07", null ),
        SIX("7", "6+1", "08", null ),
        SEVEN("8", "7+1", "09", null ),
        EIGHT("9", "8+1", "10", null ),
        NINE("10", "9+1", "11", null ),
        TEN("11", "10+1", "12", null );


    @Getter
    private final String value;

    @Getter
    private final String description;

    @Getter
    private final String elevenCd;

    @Getter
    private final String naverCd;

    @Getter
    private static final ImmutableBiMap<PickIconType, String> biMap;

    static {
        BiMap<PickIconType, String> map = EnumHashBiMap.create(PickIconType.class);

        for (PickIconType type : values()) {
            map.put(type, type.getValue());
        }

        biMap = ImmutableBiMap.copyOf(map);
    }

    public static PickIconType valueFor(final String value) {
        final PickIconType item =  biMap.inverse().get(value);
        return item;
    }

}
