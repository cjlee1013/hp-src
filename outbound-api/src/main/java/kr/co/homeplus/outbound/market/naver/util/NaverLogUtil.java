package kr.co.homeplus.outbound.market.naver.util;

import com.nhncorp.platform.shopn.seller.SellerServiceStub.ErrorType;
import kr.co.homeplus.outbound.core.exception.handler.LogicException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NaverLogUtil {
    public static void errorLog(ErrorType errorType) throws Exception {
        log.error("\nErrorCode : {}\nErrorMessage : {}\nErrorDetail : {}", errorType.getCode(), errorType.getMessage(), errorType.getDetail());
        throw new LogicException(errorType.getCode(), errorType.getMessage());
    }
}
