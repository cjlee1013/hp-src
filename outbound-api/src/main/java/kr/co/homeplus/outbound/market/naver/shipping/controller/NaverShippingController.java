package kr.co.homeplus.outbound.market.naver.shipping.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.homeplus.outbound.market.naver.shipping.model.NaverOrderList;
import kr.co.homeplus.outbound.market.naver.shipping.service.NaverShippingService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "마켓연동 > 네이버")
@Slf4j
@RestController
@RequestMapping("/market/naver")
@RequiredArgsConstructor
public class NaverShippingController {

    private final NaverShippingService naverShippingService;

    @ApiOperation(value = "네이버 배송출발")
    @GetMapping("/setShipStart")
    public ResponseObject<Boolean> setShipStart(String productOrderId, String trackingNo) throws Exception {
        return ResourceConverter.toResponseObject(naverShippingService.setShipStart(productOrderId,trackingNo));
    }

    @ApiOperation(value = "네이버 반품수거")
    @GetMapping("/setPickStart")
    public ResponseObject<Boolean> setPickStart(String productOrderId) throws Exception {
        return ResourceConverter.toResponseObject(naverShippingService.setPickStart(productOrderId));
    }
    @ApiOperation(value = "네이버 배송상태 변경조회")
    @GetMapping("/getShippingList")
    public ResponseObject<NaverOrderList> getShippingList(String fromDt, String toDt, String lastChangedStatusCode) throws Exception {
        return ResourceConverter.toResponseObject(naverShippingService.getChangedProductOrderList(fromDt, toDt, lastChangedStatusCode));
    }
}
