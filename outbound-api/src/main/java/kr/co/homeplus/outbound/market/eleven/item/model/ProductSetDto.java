package kr.co.homeplus.outbound.market.eleven.item.model;


import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import kr.co.homeplus.outbound.core.utility.xml.AdapterXmlCDATA;
import kr.co.homeplus.outbound.market.eleven.item.enums.CuponCheckType;
import kr.co.homeplus.outbound.market.eleven.item.enums.DlvCnAreaCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.DlvCstInstBasiCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.DlvCstPayTypCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.DlvWyCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.OrgnTypCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.PrdStatCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.RmaterialTypCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.SelLimitTypCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.SellMethodType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Product")
@Data
public class ProductSetDto {

	/**
	 * 상품 번호
	 */
	@XmlTransient
	private String prdNo;

	/**
	 * 상품 등록, 수정 API 이용 시 prdTypCd를 반드시 11로 셋팅
	 */
	private String prdTypCd;

	/**
	 * 판매 방식
	 */
	private SellMethodType selMthdCd;

	/**
	 * 카테고리 번호
	 */
	private String dispCtgrNo;

	/**
	 * 카테고리번호 수정 고정값1 이 존재하면 카테고리 수정이 이루어집니다.
	 */
	private String originDispCtgrNo;

	/**
	 * 제조사
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String company;

	/**
	 * 브랜드
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String brand;

	/**
	 * 모델명
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String modelNm;

	/**
	 * 상품명
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String prdNm;

	/**
	 * 상품 홍보 문구
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String advrtStmt;

	/**
	 * 원산지 구분
	 */
	private OrgnTypCdType orgnTypCd;

	/**
	 * 원산지명
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String orgnNmVal;

	/**
	 * 제조일자 : 추후확인
	 */
	private String mnfcDy;

	/**
	 * 유효일자 :
	 */
	private String eftvDy;
	/**
	 * 정가 : 추후확인
	 */
	private String maktPrc;

	/**
	 * 정가 : 추후확인
	 */
	private String artist;

	/**
	 * 원산지 코드
	 */
	private String orgnTypDtlsCd; // 기존에 값을 이렇게 넣지 않고 있음.

	/**
	 * 판매자 상품코드
	 */
	private String sellerPrdCd;

	/**
	 * 부가세/면세상품
	 */
	private String suplDtyfrPrdClfCd;

	/**
	 * 상품상태
	 */
	private PrdStatCdType prdStatCd;

	/**
	 * 미성년자 구매가능
	 */
	private String minorSelCnYn;

	/**
	 * 판매가
	 */
	private String selPrc;

	/**
	 * 재고수량
	 */
	private String prdSelQty;

	/**
	 * 최소구매수량 - 구매수량 타입 설정
	 */
	private SelLimitTypCdType selMinLimitTypCd;

	/**
	 * 최소구매수량 - 구매수량
	 */
	private String selMinLimitQty;


	/**
	 * 최대구매수량 - 구매수량 타입 설정
	 */
	private SelLimitTypCdType selLimitTypCd;

	/**
	 * 최대구매수량 - 구매수량
	 */
	private String selLimitQty;

	/**
	 * 최대구매수량 재구매기간
	 */
	private String townSelLmtDy;

	/**
	 * 판매기간-기간설정안함
	 */
	private String selTermUseYn;

	/**
	 * 판매 시작일/예약 시작일
	 */
	private String aplBgnDy;

	/**
	 * 판매 종료일/예약 종료일
	 */
	private String aplEndDy;
	/**
	 * 배송 가능 지역
	 */
	private DlvCnAreaCdType dlvCnAreaCd;

	/**
	 * 배송방법
	 */
	private DlvWyCdType dlvWyCd;

	/**
	 * 배송비 설정
	 */
	private DlvCstInstBasiCdType dlvCstInstBasiCd;

	/**
	 * 묶음배송여부
	 */
	private String bndlDlvCnYn;

	/**
	 * 무료 배송 기준 금액
	 */
	private String prdFrDlvBasiAmt;

	/**
	 * 배송비
	 */
	private String dlvCst1;

	/**
	 * 결재방법
	 */
	private DlvCstPayTypCdType dlvCstPayTypCd;

	/**
	 * AS 안내정보
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String asDetail;

	/**
	 * 반품 배송비
	 */
	private String rtngdDlvCst;

	/**
	 * 교환 배송비
	 */
	private String exchDlvCst;

	/**
	 * 반품/교환 안내
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String rtngExchDetail;

	/**
	 * 대표 이미지 url
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String prdImage01;

	/**
	 * 대표 이미지 소스
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String prdImage01Src;

	/**
	 * 추가 이미지1
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String prdImage02;

	/**
	 * 추가이미지1 url
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String prdImage02Src;

	/**
	 * 추가 이미지2
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String prdImage03;

	/**
	 * 추가이미지2 url
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String prdImage03Src;

	/**
	 * 추가 이미지3
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String prdImage04;

	/**
	 * 추가이미지3 url
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String prdImage04Src;

	/**
	 * 상품상세정보
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String htmlDetail;

	/**
	 * 쿠폰 적용여부
	 */
	private CuponCheckType cuponcheck;

	/**
	 * 원/%
	 */
	private String cupnDscMthdCd;

	/**
	 * 쿠폰적용 값
	 */
	private String dscAmtPercnt;

	/**
	 * 쿠폰지급기간 설정 여부
	 */
	private String cupnUseLmtDyYn;

	/**
	 * 쿠폰지급기간 시작일
	 */
	private String cupnIssStartDy;

	/**
	 * 쿠폰지급기간 종료일
	 */
	private String cupnIssEndDy;

	/**
	 * 상품 고시 정보
	 */
	@XmlElement(name = "ProductNotification")
	private List<ProductNotificationSetDto> productNotificationSetDtoList;

	/**
	 * 가격비교 사이트 등록 여부
	 */
	private String prcCmpExpYn;

	@XmlElement(name = "ProductCert")
	private List<ProductCertSetDto> productCertSetDtoList;

	/**
	 * [선택] 옵션 사용 여부 Y : 사용함 N : 사용안함
	 */
	private String optUpdateYn;

	/**
	 * [선택] 선택형 옵션 여부 Y : 사용함 N : 사용안함
	 */
	private String optSelectYn;

	/**
	 * [선택] 고정값 1 옵션을 등록하실 경우 1 고정값을 주셔야 합니다.
	 */
	private String txtColCnt;

	/**
	 * [선택] 옵션명 40Byte 까지만 입력가능하며 특수 문자[',",%,&,<,>,#,†]는 입력할 수 없습니다.
	 */
	@XmlJavaTypeAdapter(AdapterXmlCDATA.class)
	private String colTitle;

	/**
	 * [선택] 상품상세 옵션값 노출 방식 선택 00 : 등록순 01 : 옵션값 가나다순 02 : 옵션값 가나다 역순 03 : 옵션가격 낮은 순 04 : 옵션가격 높은 순
	 *
	 * 일단 옵션값 가나다순으로....
	 */
	private String prdExposeClfCd;

	/**
	 * 축산물 이력번호 코드 01 : 이력번호 표시대상 제품 02 : 이력번호 표시대상 아님 03 : 상세설명 참조
	 */
	private String beefTraceStat;

	/**
	 * 축산물 이력번호
	 */
	private String beefTraceNo;

	/**
	 * 단위수량
	 */
	private String buyUntQty;

	@XmlElement(name = "ProductOption")
	private List<ProductOptionSetDto> productOptionSetDtoList;

	/**
	 * 원재료 유형코드
	 */
	private RmaterialTypCdType rmaterialTypCd;

	/**
	 * 복수 구매할인 설정 기준 ( Y : 설정함, N : 설정안함)
	 */
	private String pluYN;

	/**
	 * 복수구매할인 설정 기준 (복수 구매할인"을 설정 하지 않을시에는 Element를 모두 삭제해 주세요.)
	 *
	 * 01 : 수량기준 , 02 : 금액기준
	 */
	private String pluDscCd;

	/**
	 * 복수구매할인 기준 금액 및 수량
	 */
	private String pluDscBasis;

	/**
	 * 복수구매할인 금액/율
	 */
	private String pluDscAmtPercnt;

	/**
	 * 복수구매할인 구분코드
	 * 01 : % , 02 : 원
	 */
	private String pluDscMthdCd;

	/**
	 * 복수구매할인 적용기간 설정
	 * Y : 설정함  , N : 설정안함
	 */
	private String pluUseLmtDyYn;

	/**
	 * 복수구매할인 적용기간 시작일
	 */
	private String pluIssStartDy;

	/**
	 * 복수구매할인 적용기간 종료일
	 */
	private String pluIssEndDy;

}
