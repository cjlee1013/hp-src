package kr.co.homeplus.outbound.market.eleven.inquiry.model;

import lombok.Data;

@Data
public class ElevenInquiryAlimiAnswerSetDto {

  	/**
     * 게시물 일련번호
     */
    private String emerNtceSeq;

    /**
     * 확인처리
     */
    private String confimYn;

    /**
     * 답변내용
     */
    private String answerCtnt;
}
