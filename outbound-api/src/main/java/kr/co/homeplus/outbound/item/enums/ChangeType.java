package kr.co.homeplus.outbound.item.enums;

import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.collect.ImmutableBiMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 마켓 아이콘 타입 (기준, 설명, 마켓별)
 */
@RequiredArgsConstructor
public enum ChangeType {

        RATE("0", "할인율", "01", null)
    ,   AMOUNT("1", "할인금액", "02", null)
    ,   FIX("2", "고정가", null, null );

    @Getter
    private final String type;

    @Getter
    private final String description;

    @Getter
    private final String elevenCd;

    @Getter
    private final String naverCd;

    @Getter
    private static final ImmutableBiMap<ChangeType, String> biMap;

    static {
        BiMap<ChangeType, String> map = EnumHashBiMap.create(ChangeType.class);

        for (ChangeType type : values()) {
            map.put(type, type.getType());
        }

        biMap = ImmutableBiMap.copyOf(map);
    }

    public static ChangeType valueFor(final String type) {
        final ChangeType item =  biMap.inverse().get(type);
        return item;
    }

}
