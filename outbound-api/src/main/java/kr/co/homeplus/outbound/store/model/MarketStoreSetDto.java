package kr.co.homeplus.outbound.store.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel("홈플러스 점포 조회")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MarketStoreSetDto {

    @ApiModelProperty(value = "마켓ID")
    private String partnerId;

    @ApiModelProperty(value = "점포코드" )
    private Integer storeId;

    @ApiModelProperty(value = "마켓지점코드" )
    private String marketStoreId;

    @ApiModelProperty(value = "사용여부")
    private String useYn;

}

