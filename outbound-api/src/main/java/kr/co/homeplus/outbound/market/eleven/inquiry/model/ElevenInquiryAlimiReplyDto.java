package kr.co.homeplus.outbound.market.eleven.inquiry.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;
import lombok.Data;

@Data
public class ElevenInquiryAlimiReplyDto {
    /**
     * 답변 내용
     */
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private List<String> emerReplyCtnt;
}