package kr.co.homeplus.outbound.market.eleven.claim.service;

import com.fasterxml.jackson.core.type.TypeReference;
import java.util.LinkedHashMap;
import kr.co.homeplus.outbound.market.eleven.claim.model.ElevenCancelDto;
import kr.co.homeplus.outbound.market.eleven.claim.model.ElevenClaimInfoDto;
import kr.co.homeplus.outbound.market.eleven.claim.model.ElevenExchangeDto;
import kr.co.homeplus.outbound.market.eleven.claim.model.ElevenResponseDto;
import kr.co.homeplus.outbound.market.eleven.claim.model.ElevenReturnDto;
import kr.co.homeplus.outbound.market.eleven.config.MarketElevenConfig;
import kr.co.homeplus.outbound.market.eleven.enums.ElevenClaimUriInfo;
import kr.co.homeplus.outbound.market.eleven.util.ElevenConnectUtil;
import kr.co.homeplus.outbound.market.naver.util.DateUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ElevenClaimService {
    private final MarketElevenConfig elevenConfig;

    /**
     * 11번가 - 취소신청목록조회
     * @param startDt 조회시작일
     * @param endDt 조회종료일
     * @return
     * @throws Exception
     */
    public ResponseObject<ElevenClaimInfoDto<ElevenCancelDto>> getElevenClaimInfo(String startDt, String endDt) throws Exception {
        startDt = DateUtils.customDateStr(startDt, DateUtils.DIGIT_YMD_HI);
        endDt = DateUtils.customDateStr(endDt, DateUtils.DIGIT_YMD_HI);
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("startTime", startDt);
        map.put("endTime", endDt);
        return ElevenConnectUtil.execute(
            ElevenConnectUtil.createURL(ElevenClaimUriInfo.CLAIM_GET_CANCEL_REG_LIST, map),
            new TypeReference<ElevenClaimInfoDto<ElevenCancelDto>>() {
            }
        );
    }

    /**
     * 11번가 - 취소신청목록조회
     * @param startDt 조회시작일
     * @param endDt 조회종료일
     * @return
     * @throws Exception
     */
    public ResponseObject<ElevenClaimInfoDto<ElevenReturnDto>> getElevenClaimReturnInfo(String startDt, String endDt) throws Exception {
        startDt = DateUtils.customDateStr(startDt, DateUtils.DIGIT_YMD_HI);
        endDt = DateUtils.customDateStr(endDt, DateUtils.DIGIT_YMD_HI);
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("startTime", startDt);
        map.put("endTime", endDt);
        return ElevenConnectUtil.execute(
            ElevenConnectUtil.createURL(ElevenClaimUriInfo.CLAIM_GET_RETURN_REG_LIST, map),
            new TypeReference<ElevenClaimInfoDto<ElevenReturnDto>>() {
            }
        );
    }

    /**
     * 11번가 - 취소신청목록조회
     * @param startDt 조회시작일
     * @param endDt 조회종료일
     * @return
     * @throws Exception
     */
    public ResponseObject<ElevenClaimInfoDto<ElevenExchangeDto>> getElevenClaimExchangeInfo(String startDt, String endDt) throws Exception {
        startDt = DateUtils.customDateStr(startDt, DateUtils.DIGIT_YMD_HI);
        endDt = DateUtils.customDateStr(endDt, DateUtils.DIGIT_YMD_HI);
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("startTime", startDt);
        map.put("endTime", endDt);
        return ElevenConnectUtil.execute(
            ElevenConnectUtil.createURL(ElevenClaimUriInfo.CLAIM_GET_EXCHANGE_REG_LIST, map),
            new TypeReference<ElevenClaimInfoDto<ElevenExchangeDto>>() {
            }
        );
    }

    /**
     * 11번가 - 판매불가처리(취소신청)
     *
     * @param ordNo 11번가 주문번호
     * @param ordPrdSeq 주문순번
     * @param ordCnRsnCd 불가사유
     * @param ordCnDtlsRsn 사
     * @return유
     * @throws Exception
     */
    public ResponseObject<ElevenResponseDto> setRegNotSelling(String ordNo, String ordPrdSeq, String ordCnRsnCd, String ordCnDtlsRsn) throws Exception {
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("ordNo", ordNo);
        map.put("ordPrdSeq", ordPrdSeq);
        map.put("ordCnRsnCd", ordCnRsnCd);
        map.put("ordCnDtlsRsn", ordCnDtlsRsn);
        return ElevenConnectUtil.execute(
            ElevenConnectUtil.createURL(ElevenClaimUriInfo.CLAIM_SET_NOT_SELLING_REG, map),
            new TypeReference<ElevenResponseDto>() {
            }
        );
    }

    /**
     * 11번가 - 취소승인처리
     *
     * @param ordPrdCnSeq 11번가클레임번호
     * @param ordNo 11번가주문번호
     * @param ordPrdSeq 11번가 주문순서
     * @return
     * @throws Exception
     */
    public ResponseObject<ElevenResponseDto> setApproveCancel(String ordPrdCnSeq, String ordNo, String ordPrdSeq) throws Exception {
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("ordPrdCnSeq", ordPrdCnSeq);
        map.put("ordNo", ordNo);
        map.put("ordPrdSeq", ordPrdSeq);
        return ElevenConnectUtil.execute(
            ElevenConnectUtil.createURL(ElevenClaimUriInfo.CLAIM_SET_CANCEL_APPROVE, map),
            new TypeReference<ElevenResponseDto>() {
            }
        );
    }

    /**
     * 11번가 - 취소거부처리
     *
     * @param ordNo
     * @param ordPrdSeq
     * @param ordPrdCnSeq
     * @param dlvMthdCd
     * @param sendDt
     * @param dlvEtprsCd
     * @param invcNo
     * @throws Exception
     */
    public ResponseObject<ElevenResponseDto> setRejectCancel(String ordNo, String ordPrdSeq, String ordPrdCnSeq, String dlvMthdCd, String sendDt, String dlvEtprsCd, String invcNo) throws Exception {
        // ordNo, ordPrdSeq, ordPrdCnSeq, dlvMthdCd, sendDt, dlvEtprsCd, invcNo
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("ordPrdCnSeq", ordPrdCnSeq);
        map.put("ordNo", ordNo);
        map.put("ordPrdSeq", ordPrdSeq);
        return ElevenConnectUtil.execute(
            ElevenConnectUtil.createURL(ElevenClaimUriInfo.CLAIM_SET_CANCEL_REJECT, map),
            new TypeReference<ElevenResponseDto>() {
            }
        );
    }

    /**
     * 11번가 - 반품승인
     *
     * @param clmReqSeq 11번가클레임번호
     * @param ordNo 11번가주문번호
     * @param ordPrdSeq 11번가 주문순서
     * @throws Exception
     */
    public ResponseObject<ElevenResponseDto> setApproveReturn(String clmReqSeq, String ordNo, String ordPrdSeq) throws Exception {
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("clmReqSeq", clmReqSeq);
        map.put("ordNo", ordNo);
        map.put("ordPrdSeq", ordPrdSeq);
        return ElevenConnectUtil.execute(
            ElevenConnectUtil.createURL(ElevenClaimUriInfo.CLAIM_SET_RETURN_APPROVE, map),
            new TypeReference<ElevenResponseDto>() {
            }
        );
    }

    /**
     * 11번가 - 반품거부처리
     *
     * @param ordNo
     * @param ordPrdSeq
     * @param ordPrdCnSeq
     * @param dlvMthdCd
     * @param sendDt
     * @param dlvEtprsCd
     * @param invcNo
     * @throws Exception
     */
    public ResponseObject<ElevenResponseDto> setRejectReturn(String ordNo, String ordPrdSeq, String clmReqSeq, String refsRsnCd, String refsRsn) throws Exception {
        // ordNo, ordPrdSeq, ordPrdCnSeq, dlvMthdCd, sendDt, dlvEtprsCd, invcNo
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("clmReqSeq", clmReqSeq);
        map.put("ordNo", ordNo);
        map.put("ordPrdSeq", ordPrdSeq);
        map.put("refsRsnCd", refsRsnCd);
        map.put("refsRsn", refsRsn);
        return ElevenConnectUtil.execute(
            ElevenConnectUtil.createURL(ElevenClaimUriInfo.CLAIM_SET_RETURN_REJECT, map),
            new TypeReference<ElevenResponseDto>() {
            }
        );
    }

    /**
     * 11번가 - 반품보류처리
     *
     * @param ordNo 11번가 주문번호
     * @param ordPrdSeq 주문순번
     * @param clmReqSeq 클레임번호/반품신청번호
     * @param deferRefsRsnCd 보류사유코드
     * @param deferRefsRsn 사유(한글일 경우 url인코딩)
     * @return
     * @throws Exception
     */
    public ResponseObject<ElevenResponseDto> setHoldExchange(String ordNo, String ordPrdSeq, String clmReqSeq, String deferRefsRsnCd, String deferRefsRsn) throws Exception {
        // ordNo, ordPrdSeq, ordPrdCnSeq, dlvMthdCd, sendDt, dlvEtprsCd, invcNo
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("clmReqSeq", clmReqSeq);
        map.put("ordNo", ordNo);
        map.put("ordPrdSeq", ordPrdSeq);
        map.put("deferRefsRsnCd", deferRefsRsnCd);
        map.put("deferRefsRsn", deferRefsRsn);
        return ElevenConnectUtil.execute(
            ElevenConnectUtil.createURL(ElevenClaimUriInfo.CLAIM_SET_RETURN_HOLD, map),
            new TypeReference<ElevenResponseDto>() {
            }
        );
    }

    /**
     * 11번가 - 교환승인처리
     *
     * @param clmReqSeq 11번가클레임번호
     * @param ordNo 11번가주문번호
     * @param ordPrdSeq 11번가 주문순서
     * @throws Exception
     */
    public ResponseObject<ElevenResponseDto> setApproveExchange(String clmReqSeq, String ordNo, String ordPrdSeq) throws Exception {
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("clmReqSeq", clmReqSeq);
        map.put("ordNo", ordNo);
        map.put("ordPrdSeq", ordPrdSeq);
        return ElevenConnectUtil.execute(
            ElevenConnectUtil.createURL(ElevenClaimUriInfo.CLAIM_SET_EXCHANGE_APPROVE, map),
            new TypeReference<ElevenResponseDto>() {
            }
        );
    }

    /**
     * 11번가 - 교환거부처리
     *
     * @param ordNo 11번가 주문번호
     * @param ordPrdSeq 주문순번
     * @param clmReqSeq 클레임번호
     * @param refsRsnCd 사유코드
     * @param refsRsn 사유
     * @throws Exception
     */
    public ResponseObject<ElevenResponseDto> setRejectExchange(String ordNo, String ordPrdSeq, String clmReqSeq, String refsRsnCd, String refsRsn) throws Exception {
        // ordNo, ordPrdSeq, ordPrdCnSeq, dlvMthdCd, sendDt, dlvEtprsCd, invcNo
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("clmReqSeq", clmReqSeq);
        map.put("ordNo", ordNo);
        map.put("ordPrdSeq", ordPrdSeq);
        map.put("refsRsnCd", refsRsnCd);
        map.put("refsRsn", refsRsn);
        return ElevenConnectUtil.execute(
            ElevenConnectUtil.createURL(ElevenClaimUriInfo.CLAIM_SET_EXCHANGE_REJECT, map),
            new TypeReference<ElevenResponseDto>() {
            }
        );
    }

}
