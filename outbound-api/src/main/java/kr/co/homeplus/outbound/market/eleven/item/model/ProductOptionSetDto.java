package kr.co.homeplus.outbound.market.eleven.item.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import kr.co.homeplus.outbound.core.utility.xml.AdapterXmlCDATA;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ProductOption")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductOptionSetDto {

	/**
	 * [선택] 옵션값
			40Byte 까지만 입력가능하며 특수 문자[',",%,&,<,>,#,†]는 입력할 수 없습니다.
			한 상품안에서 옵션값은 중복이 될수 없습니다.
	 */
	@XmlJavaTypeAdapter( AdapterXmlCDATA.class )
	private String colValue0;

	/**
	 * [선택] 옵션가
			기본 판매가의 +100%/-50%까지 설정하실 수 있습니다.
			옵션가격이 0원인 상품이 반드시 1개 이상 있어야 합니다.
	 */
	private String colOptPrice;

	/**
	 * [선택] 옵션재고수량
			멀티옵션일 경우는 일괄설정이 되므로 입력하시면 안됩니다.
			옵션상태(useYn)가 N일 때만 0 입력 가능합니다.
	 */
	private String colCount;

	/**
	 * [선택] 옵션상태
			멀티옵션일 경우는 지원하지 않는 기능입니다.
			Y : 사용함
			N : 품절
	 */
	private String useYn;

	/**
	 * [선택] 옵션추가무게
			배송 주체(dlvClf) 코드가 03(11번가 해외 배송)인 경우 또는 "전세계배송 상품" 인경우 옵션 등록시 필수입니다.
			단위 g
	 */
	private String optWght;

}