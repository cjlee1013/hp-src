package kr.co.homeplus.outbound.market.naver.inquiry.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class NaverInquirySetDto {
	private String inquiryTimeFrom; //YYYYMMDDHHMISS
	private String inquiryTimeTo;
	private String inquiryExtraData;
	private boolean isAnswered = false;		//답변 여부
	private int pageNumber = 1;				//상품 문의 페이지 번호
}