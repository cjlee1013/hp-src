package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("마켓연동 상품관리")
public class ItemMarketSetDto {

	private String partnerId;
	private String itemNo;
	private String marketItemNo;
	private String marketItemStatus;
	private String promoYn;
	private String couponYn;
	private String couponEndDt;

	private String sendYn;
	private String sendDt;
	private String sendType;
	private String sendMsg;

}