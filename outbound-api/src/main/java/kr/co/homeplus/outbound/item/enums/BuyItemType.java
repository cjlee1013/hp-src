package kr.co.homeplus.outbound.item.enums;

import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.collect.ImmutableBiMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 할인 설정 기준 유형
 */
@RequiredArgsConstructor
public enum BuyItemType {

        BUYQTY("1", "구매수량", "01", null)
    ,   BUYAMOUNT("2", "구매금액", "02", null);

    @Getter
    private final String type;

    @Getter
    private final String description;

    @Getter
    private final String elevenCd;

    @Getter
    private final String naverCd;

    @Getter
    private static final ImmutableBiMap<BuyItemType, String> biMap;

    static {
        BiMap<BuyItemType, String> map = EnumHashBiMap.create(BuyItemType.class);

        for (BuyItemType type : values()) {
            map.put(type, type.getType());
        }

        biMap = ImmutableBiMap.copyOf(map);
    }

    public static BuyItemType valueFor(final String type) {
        final BuyItemType item =  biMap.inverse().get(type);
        return item;
    }

}
