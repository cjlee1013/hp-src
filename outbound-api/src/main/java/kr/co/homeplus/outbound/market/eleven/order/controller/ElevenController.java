package kr.co.homeplus.outbound.market.eleven.order.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.outbound.market.eleven.order.model.ElevenOrder;
import kr.co.homeplus.outbound.market.eleven.order.service.ElevenApiConnectService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "마켓연동 > 주문(11번가)")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found")
    ,   @ApiResponse(code = 405, message = "Method Not Allowed")
    ,   @ApiResponse(code = 422, message = "Unprocessable Entity")
    ,   @ApiResponse(code = 500, message = "Internal Server Error")
})
@Slf4j
@RequestMapping("/market")
@RestController
@RequiredArgsConstructor
public class ElevenController {

    private final ElevenApiConnectService elevenApiConnectService;
    @ApiOperation(value = "주문수집", response = String.class)
    @GetMapping(value = "/eleven/orderList")
    public ResponseObject<String> createOrderComplete(
        @ApiParam(name = "startTime", value = "시작시간(yyyyMMddHHmm)", required = true) @RequestParam String startTime,
        @ApiParam(name = "endTime", value = "종료시간(yyyyMMddHHmm)", required = true) @RequestParam String endTime
    ) throws Exception {
        return ResourceConverter.toResponseObject(elevenApiConnectService.createOrderComplete(startTime, endTime));
    }

    @ApiOperation(value = "주문수집", response = String.class)
    @GetMapping(value = "/eleven/orderListParsing")
    public ResponseObject<List<ElevenOrder>> createOrderCompleteParsing(
        @ApiParam(name = "startTime", value = "시작시간(yyyyMMddHHmm)", required = true) @RequestParam String startTime,
        @ApiParam(name = "endTime", value = "종료시간(yyyyMMddHHmm)", required = true) @RequestParam String endTime
    ) throws Exception {
        return ResourceConverter.toResponseObject(elevenApiConnectService.createOrderCompleteParsing(startTime, endTime));
    }
}
