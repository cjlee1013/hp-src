package kr.co.homeplus.outbound.cert.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 안전인증 조회 결과
 */
@Data
public class ItemSafeCertDetailGetDto {

	@ApiModelProperty(value = "인증정보아이디")
	private String certUid;

	@ApiModelProperty(value = "인증번호(필증번호)")
	private String certNum;

	@ApiModelProperty(value = "인증상태")
	private String certState;

	@ApiModelProperty(value = "인증일자")
	private String certDate;

	@ApiModelProperty(value = "인증구분")
	private String certDiv;

	@ApiModelProperty(value = "제품명")
	private String productName;

	@ApiModelProperty(value = "브랜드명")
	private String brandName;

	@ApiModelProperty(value = "모델명")
	private String modelName;

	@ApiModelProperty(value = "법정제품분류명")
	private String categoryName;

	@ApiModelProperty(value = "수입/제조 구분")
	private String importDiv;

	@ApiModelProperty(value = "제조사명")
	private String makerName;

	@ApiModelProperty(value = "수입사")
	private String importerName;

	@ApiModelProperty(value = "최초인증번호")
	private String firstCertNum;

	@ApiModelProperty(value = "등록일")
	private String signDate;

	@ApiModelProperty(value = "상세내용")
	private String remark;
}
