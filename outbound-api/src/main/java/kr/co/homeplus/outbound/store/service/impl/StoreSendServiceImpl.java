package kr.co.homeplus.outbound.store.service.impl;

import java.util.HashMap;
import java.util.Map;
import kr.co.homeplus.outbound.core.constants.ElevenUriConstants;
import kr.co.homeplus.outbound.core.utility.RestUtil;
import kr.co.homeplus.outbound.market.eleven.item.model.ResultMessageDto;
import kr.co.homeplus.outbound.store.model.eleven.MartStoreListDto;
import kr.co.homeplus.outbound.store.model.eleven.MartStoreResultDto;
import kr.co.homeplus.outbound.store.model.eleven.MartStoreSetDto;
import kr.co.homeplus.outbound.store.model.eleven.StoreMailNoListDto;
import kr.co.homeplus.outbound.store.service.StoreSendService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class StoreSendServiceImpl extends RestUtil implements StoreSendService {

    protected StoreSendServiceImpl(RestTemplate restTemplate) {
        super(restTemplate);
    }

    @Override
    public MartStoreListDto getStoreInfo(String storeId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("sellerStrCd", storeId);

        return execute(HttpMethod.GET, getHost() + ElevenUriConstants.SELECT_STORE,
                null, new ParameterizedTypeReference<>() {
                }, params);
    }

    @Override
    public MartStoreListDto getStoreInfoAll() {
        Map< String, String > params = new HashMap< String, String >();
        return execute(HttpMethod.GET, getHost() + ElevenUriConstants.SELECT_STORE_ALL,
                null, new ParameterizedTypeReference<>() {
                }, params) ;
    }

    @Override
    public MartStoreResultDto addStoreInfo(MartStoreSetDto martStoreSetDto) {
        Map< String, String > params = new HashMap< String, String >();
        return execute(HttpMethod.POST, getHost() + ElevenUriConstants.SET_STORE, martStoreSetDto,
            new ParameterizedTypeReference<>(){}, params);
    }

    @Override
    public ResultMessageDto modifyStoreInfo(MartStoreSetDto martStoreSetDto) {
        Map< String, String > params = new HashMap< String, String >();
        return execute(HttpMethod.PUT, getHost() + ElevenUriConstants.SET_STORE, martStoreSetDto,
            new ParameterizedTypeReference<>(){}, params);
    }

    @Override
    public StoreMailNoListDto findStoreMailNoInfo(String strNo) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("strNo", strNo);
        params.put("mailNoUseYn", "Y");

        return execute(HttpMethod.GET, getHost() + ElevenUriConstants.STORE_MAIN_NO_LIST,
            null, new ParameterizedTypeReference<>() {
            }, params);
    }
}

