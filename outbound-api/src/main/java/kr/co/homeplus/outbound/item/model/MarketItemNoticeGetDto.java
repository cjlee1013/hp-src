package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("상품정보고시정보")
public class MarketItemNoticeGetDto {

	@ApiModelProperty(value = "마켓연동ID")
	private String partnerId;

	@ApiModelProperty(value = "고시항목번호")
	private String marketNoticeNo;

	@ApiModelProperty(value = "고시항목명")
	private String marketNoticeNm;

	@ApiModelProperty(value = "고시정책번호")
	private String marketGnoticeNo;

	@ApiModelProperty(value = "고시정책명")
	private String marketGnoticeNm;

}
