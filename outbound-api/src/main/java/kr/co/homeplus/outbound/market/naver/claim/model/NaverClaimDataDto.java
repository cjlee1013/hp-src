package kr.co.homeplus.outbound.market.naver.claim.model;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class NaverClaimDataDto {
    private ClaimOrderDto order;
    private List<MarketClaimProductOrderDto> marketClaimProductOrderList = new ArrayList<>();

    @Data
    public static class MarketClaimProductOrderDto {
        private NaverProductOrderDto productOrder;
        //    private NaverDeliveryDto delivery;
        private CancelInfo cancelInfo;
        private NaverReturnInfoDto returnInfo;
        private NaverExchangeInfoDto exchangeInfo;
    }
}
