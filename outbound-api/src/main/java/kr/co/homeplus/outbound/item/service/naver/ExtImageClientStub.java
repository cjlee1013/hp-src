package kr.co.homeplus.outbound.item.service.naver;

import com.nhncorp.platform.shopn.ImageServiceStub;
import java.rmi.RemoteException;
import java.util.Random;
import java.util.UUID;
import kr.co.homeplus.outbound.common.enums.NaverMartProfile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import kr.co.homeplus.outbound.category.service.naver.AccessCredentials;

/**
 * <pre>
 * Class Name : ExtImageClientStub.java
 * Description : Naver Start Store > ImageServiceStub (Extends)
 *
 * Modification Information
 * Modify Date    Modifier    Comment
 * -----------------------------------------------
 * 2021. 7.  5.   jh6108.kim   신규작성
 * </pre>
 *
 * @author jh6108.kim
 * @since 2021. 7. 5.
 */
@Slf4j
@Component
public class ExtImageClientStub extends ImageServiceStub {

	private AccessCredentials ac = null;
	private Random random = new Random();
	private static Boolean isReal;
	private static String sellerId;
	private static String targetEndpoint;
	private static String serviceName = NaverMartProfile.IMAGE_SERVICE_NAME.getValue();
	private static String serviceVersion = NaverMartProfile.IMAGE_SERVICE_VERSION.getValue();

	@Value("${market.naver.isReal}")
	public void setIsReal(Boolean bIsReal) {
		isReal =  bIsReal;
	}

	@Value("${market.naver.sellerId}")
	public void setSellerId(String sSellerId) {
		sellerId =  sSellerId;
	}

	@Value("${market.naver.targetEndpoint}")
	public void setTargetEndpoint(String sTargetEndpoint) { targetEndpoint =  sTargetEndpoint; }

	/**
	 * AccessCredentials
	 */
	public void setAccessCredentials(AccessCredentials ac) { this.ac = ac; }

	private String createRequestID() {
		return UUID.randomUUID().toString()+"_IMGC" + random.nextInt(999999999);
	}

	/**
	 * Constructor that takes in a configContext
	 * @deprecated
	 */
	public ExtImageClientStub(org.apache.axis2.context.ConfigurationContext configurationContext, java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
		super(configurationContext, targetEndpoint, false);
	}

	/**
	 * Default Constructor
	 */
	public ExtImageClientStub(org.apache.axis2.context.ConfigurationContext configurationContext) throws org.apache.axis2.AxisFault {
		super(configurationContext, targetEndpoint + serviceName, false);
	}

	/**
	 * Default Constructor
	 */
	public ExtImageClientStub() throws org.apache.axis2.AxisFault {
		super(null, targetEndpoint + serviceName, false);
	}

    /**
     * Constructor taking the target endpoint
     * @deprecated
     */
    public ExtImageClientStub(java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
        super(null, targetEndpoint, false);
    }

	/**
	 * ImageSevice API
	 * @param response
	 * @return Boolean
	 */
    public Boolean isResponseOk(com.nhncorp.platform.shopn.ImageServiceStub.UploadImageResponseType response) {

		String resType = response.getResponseType();

		if(resType == null) {
			resType = "";
		}

		Boolean returnResult;
		if("SUCCESS".equals(resType) || "SUCCESS-WARNING".equals(resType)) {
			returnResult = Boolean.TRUE;
		} else {
			com.nhncorp.platform.shopn.ImageServiceStub.ErrorType error = response.getError();
			log.error(serviceName + "_API_Code : "    + error.getCode());
			log.error(serviceName + "_API_Message : " + error.getMessage());
			log.error(serviceName + "_API_Detail : "  + error.getDetail());
			returnResult = Boolean.FALSE;
		}

		if(resType.indexOf("WARNING") >= 0) {
			com.nhncorp.platform.shopn.ImageServiceStub.WarningListType warnListType = response.getWarningList();
			if(warnListType !=null) {
				com.nhncorp.platform.shopn.ImageServiceStub.WarningType[] warns = warnListType.getWarning();
				if(warns !=null && warns.length > 0) {
					int wLength = warns.length;
					for(int i=0; i<wLength ; i++) {
						log.warn("warn "+i+" = " + warns[i].getCode());
						log.warn("warn "+i+" = " + warns[i].getMessage());
						log.warn("warn "+i+" = " + warns[i].getDetail());
					}
				}
			}
		}

		return returnResult;
	}		
     
	/**
	 * Image Upload
	 *
	 * @param imageList
	 * @return
	 * @throws RemoteException
	 */
	public UploadImageResponseType uploadImage(String[] imageList) throws RemoteException {
		
		AccessCredentialsType credentials = ac.createImage(serviceName, "UploadImage");
		
		ImageURLListType imageUrlListType = new ImageURLListType();
		imageUrlListType.setURL(imageList);

		UploadImageRequestType requestType = new UploadImageRequestType();
		UploadImageRequest requset = new UploadImageRequest();

		requestType.setAccessCredentials(credentials);
		requestType.setVersion(serviceVersion);
		requestType.setRequestID(createRequestID());
		requestType.setSellerId(sellerId);
		requestType.setImageURLList(imageUrlListType);

		requset.setUploadImageRequest(requestType);
		
		UploadImageResponse response = super.uploadImage(requset);
		UploadImageResponseType responseType = response.getUploadImageResponse();
		 
		return responseType;
	}
 
}