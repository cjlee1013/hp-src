package kr.co.homeplus.outbound.market.eleven.item.model;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import kr.co.homeplus.outbound.core.utility.xml.AdapterXmlCDATA;
import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "martPrmtList")
public class MartPrmtListSetDto {

	@XmlJavaTypeAdapter( AdapterXmlCDATA.class )
	private String martPrmtNm;

}