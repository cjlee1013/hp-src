package kr.co.homeplus.outbound.market.eleven.inquiry.model;

import lombok.Data;

/**
 *  판매자 문의 답변 내역
 */

@Data
public class ElevenInquiryReplyHistoryDto {

	/**
	* column name : COP_NO
	* 제휴사 번호
	*/
	private String copNo;

	/**
	* column name : MESSAGE_NO
	* 질문번호
	*/
	private Long messageNo;

	/**
	* column name : REPLY_NO
	* 답변번호
	*/
	private int replyNo;

	/**
	* column name : CONTENT
	* 답변내용
	*/
	private String content;

	/**
	* column name : TITLE
	* 답변제목
	*/
	private String title;

	/**
	* column name : RECEIVE_DT
	* 등록일자
	*/
	private String receiveDt;

	/**
	* column name : RESPONSE_DT
	* 답변일자
	*/
	private String responseDt;

	/**
	* column name : REG_DT
	* SYSDATE
	*/
	private String regDt;

} 

