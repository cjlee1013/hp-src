package kr.co.homeplus.outbound.market.eleven.inquiry.model;

import java.util.Date;
import java.util.List;
import lombok.Data;

@Data
public class ElevenInquiryGetDto {
  /**
   * column name : MESSAGE_NO
   * 문의 번호
   */
  private Long messageNo;

  /**
   * column name : marketType
   * 마켓유형
   */
  private String marketType;

  /**
   * column name : MESSAGE_TYPE
   * 문의 유형 (B : 게시판, N : 쪽지, E : 긴급메세지)
   */
  private String messageType;

  /**
   * column name : STORE_ID
   * GHS 점포 코드
   */
  private String storeId;

  /**
   * column name : COP_ORD_GOOD_SEQ
   * 제휴사 주문번호(장바구니 번호)
   */
  private String copOrdGoodSeq;

  /**
   * column name : RECEIVE_DATE
   * 문의 날짜
   */
  private String receiveDate;

  /**
   * column name : COP_GOOD_ID
   * 제휴사 상품 번호
   */
  private String copGoodId;

  /**
   * column name : GOOD_ID
   * 홈플러스 상품 번호
   */
  private Integer goodId;

  /**
   * column name : REQUEST_TITLE
   * 문의 제목
   */
  private String requestTitle;

  /**
   * column name : REQUEST_COMMENTS
   * 문의 내용
   */
  private String requestComments;

  /**
   * column name : CONTACT_TYPE
   * 문의 분야
   */
  private String contactType;

  /**
   * column name : CONTACT_CODE
   * 문의 분야
   */
  private String contactCode;

  /**
   * column name : EMERGENCY_CONTACT
   * 긴급메세지 응답 유형
   */
  private String emergencyContact;

  /**
   * column name : INQUIRER_ID
   * 문의자 ID
   */
  private String inquirerId;

  /**
   * column name : INQUIRER_NAME
   * 문의자명
   */
  private String inquirerName;

  /**
   * column name : INQUIRER_PHONE
   * 문의자 연락처
   */
  private String inquirerPhone;

  /**
   * column name : RESPONSE_TITLE
   * 답변 제목
   */
  private String responseTitle;

  /**
   * column name : RESPONSE_CONTENTS
   * 답변 내용
   */
  private String responseContents;

  /**
   * column name : MESSAGE_SECTION
   * 긴급메세지 문의 분야
   */
  private String messageSection;

  /**
   * column name : ISRESPONSE_YN
   * 답변 여부
   */
  private String isresponseYn;

  /**
   * column name : ISRESPONSE_DATE
   * 답변 날짜
   */
  private String isresponseDate;

  /**
   * column name : ISSEND_YN
   * 전송 여부
   */
  private String issendYn;

  /**
   * column name : ISSEND_DATE
   * 전송 날짜
   */
  private Date issendDate;

  /**
   * column name : FAIL_MSG
   * 에러 내용
   */
  private String failMsg;

  /**
   * column name : BUY_YN
   * 구매 여부
   */
  private String buyYn;

  /**
   * column name : MOD_DT
   * 수정일시
   */
  private Date modDt;

  /**
   * column name : REG_DT
   * 등록일시
   */
  private Date regDt;

  /**
   * 문의 답변 이력
   */
  private List<ElevenInquiryReplyHistoryDto> elevenInquiryReplyHistoryList;
}
