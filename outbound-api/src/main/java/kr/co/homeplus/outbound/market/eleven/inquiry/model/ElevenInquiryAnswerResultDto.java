package kr.co.homeplus.outbound.market.eleven.inquiry.model;
import lombok.Data;

@Data
public class ElevenInquiryAnswerResultDto {

	  /**
     * QnA 원본 글번호
     */
    private String brdInfoNo;

    /**
     * QnA 상품번호
     */
    private String productNo;

    /**
     * 결과내용
     */
    private String message;

    /**
     * 결과코드
     */
    private String resultCode;
}
