package kr.co.homeplus.outbound.market.eleven.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
@Setter
@ConfigurationProperties(prefix = "market.eleven")
public class MarketElevenConfig {
    private String targetEndpoint;
    private String apiKey;
    private Shipping shipping = new Shipping();
    private Settle settle = new Settle();

    @Data
    public class Shipping {
        private String reqDelivery;
        private String reqDeliveryPart;
        private String completed;
        private String returnInvc;
        private String sellerclaimfix;
    }

    @Data
    public class Settle {
        private String settlementList;
    }
}
