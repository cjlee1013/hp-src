package kr.co.homeplus.outbound.market.eleven.settle.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class ElevenSettleItemGetDto {
    //해외취소배송비
    private long abrdCnDlvCst;

    //반품추가배송비
    private long addClmDlvCst;

    //도서산간초도배송비
    private long bmClmFstDlvCst;

    //도서산간배송비
    private long bmStlDlvCst;

    // 판매자부담 카드추가할인
    private long cardDscSellerDfrmAmt;

    //반품/교환 배송비
    private long clmDlvCst;

    //초도배송비
    private long clmFstDlvCst;

    //판매자할인쿠폰
    private long cupnAmt;

    //알비백 보증금
    private long debaGtnStlAmt;

    //공제금액
    private long deductAmt;

    //선결제배송비
    private long dlvAmt;

    //배송번호
    private long dlvNo;

    //서비스이용료정책
    //동의/미동의/별도계약/고정
    private String feeTypeNm;

    //회원ID
    private String memId;

    //회원명
    private String memNm;

    //옵션가
    private long optAmt;

    //주문번호
    private String ordNo;


    //주문순번
    private long ordPrdSeq;

    //주문수량
    private long ordQty;

    //결제완료일
    //(YYYY/MM/DD)
    private String ordStlEndDt;

    //구매확정일
    //(YYYY/MM/DD)
    private String pocnfrmDt;

    //상품명
    private String prdNm;

    //상품번호
    private long prdNo;

    // [정산]스토어장바구니할인
    private long sbdscFlctnStlAmt;

    // [공제]스토어장바구니할인
    private long sbdscSellerDfrmAmtSum;

    //수수료
    private long selFee;

    //기본서비스이용료율
    //(0.00%)
    private String selFixedFee;

    //판매가
    //(판매단가*(주문수량-취소수량))
    private long selPrc;

    //판매금액합계
    //판매가+옵션가+배송비
    private long selPrcAmt;

    //판매자기본할인금액
    private long sellerCupnAmt;

    //지정택배이용료
    private long sellerDfrmAppDlvAmt;

    //칩이용료
    private long sellerDfrmChpCstPrd;

    //후불광고비
    private long sellerDfrmDeferredAdFee;

    //전세계배송 판매자책임반품
    private long sellerDfrmGblCnDlvCst;

    //수출대행수수료
    private long sellerDfrmGblDlvFee;

    //무이자할부수수료
    private long sellerDfrmIntfreeFee;

    //복수구매할인금액
    private long sellerDfrmMultiDscCst;

    //ocb공제금액
    private long sellerDfrmOcbAmt;

    //포인트공제금액
    private long sellerDfrmPntPrd;

    //판매자상품코드
    private long sellerPrdNo;

    //NO
    private long seqNo;

    //옵션명
    private String slctPrdOptNm;

    //발송완료일
    //(YYYY/MM/DD)
    private String sndEndDt;

    //정산금액
    //(정산금액 – 공제금액)
    private long stlAmt;

    //송금예정일
    //(YYYY/MM/DD)
    private String stlPlnDy;

    //판매자추가할인
    private long tmallApplyDscAmt;

    // 11번가추가할인
    private long tmallOverDscAmt;

    //총갯수
    private long totalCount;
}
