package kr.co.homeplus.outbound.core.maketLog.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.apache.poi.ss.formula.functions.T;

/**
 * <p>외부연동 API 연동 이력을 저장합니다.</p>
 * 연동 데이터를 mongo-DB에 저장합니다.
 * 연동 method 는 데이터 저장을 위해 아래와 같이 parameter를 지정합니다.
 *
 * parameter 1 : 연동 Object
 * parameter 2 : itemNo
 */

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MarketLogTarget {

    /**
     * 외부 연동 ID
     * @return
     */
    String partnerId() default "";

    /**
     * API method 명
     * @return
     */
    String methodTxt() default "";

    /**
     * 연동 Class
     * @return
     */
    Class<?> reqClass();
}
