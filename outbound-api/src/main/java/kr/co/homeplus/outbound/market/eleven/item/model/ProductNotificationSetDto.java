package kr.co.homeplus.outbound.market.eleven.item.model;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType( XmlAccessType.FIELD )
@XmlRootElement( name = "ProductNotification" )
public class ProductNotificationSetDto {

    private String type;

    @XmlElement( name = "item" )
    private List< ProductCodeDto> productCodeDtoList;

}