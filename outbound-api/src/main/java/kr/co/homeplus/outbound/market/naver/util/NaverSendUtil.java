package kr.co.homeplus.outbound.market.naver.util;

import com.nhncorp.platform.shopn.seller.SellerServiceStub.ApproveCancelApplicationRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ApproveCollectedExchangeRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ApproveReturnApplicationRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.CancelSaleRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ClaimRequestReasonType;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.DeliveryMethodType;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.GetChangedProductOrderListRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.GetProductOrderInfoListRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.HoldbackClassType;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ProductOrderChangeType;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ReDeliveryExchangeRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.RejectExchangeRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.RejectReturnRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ReleaseExchangeHoldRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.ReleaseReturnHoldRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.RequestReturnRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.WithholdExchangeRequest;
import com.nhncorp.platform.shopn.seller.SellerServiceStub.WithholdReturnRequest;
import java.util.UUID;
import javax.annotation.PostConstruct;
import kr.co.homeplus.outbound.market.naver.config.MarketNaverConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class NaverSendUtil {

    private final MarketNaverConfig marketNaverConfig;
    private static MarketNaverConfig naverConfig;

    @PostConstruct
    private void initialize() {
        naverConfig = marketNaverConfig;
    }

    /**
     * 상품주문변경리스트 요청 기본데이터 생성
     *
     * @param startDt 조회시작일
     * @param endDt 조회종료일
     * @param productOrderChangeType 조회변경타입
     * @return 상품주문변경리스트 요청 기본데이터
     */
    public static GetChangedProductOrderListRequest createGetChangedProductOrderListRequest(String startDt, String endDt, ProductOrderChangeType productOrderChangeType) {
        GetChangedProductOrderListRequest changedProductOrderListRequest = new GetChangedProductOrderListRequest();
        changedProductOrderListRequest.setDetailLevel(naverConfig.getDetailLevel());
        changedProductOrderListRequest.setVersion(naverConfig.getOrder().getServiceVersion());
        changedProductOrderListRequest.setRequestID(UUID.randomUUID().toString());
        changedProductOrderListRequest.setMallID(naverConfig.getSellerId());
        changedProductOrderListRequest.setInquiryTimeFrom(DateUtils.convertCalendar(DateUtils.getRangeStartTime(startDt, 0)));
        changedProductOrderListRequest.setInquiryTimeTo(DateUtils.convertCalendar(DateUtils.getRangeEndTime(endDt, 0)));
        changedProductOrderListRequest.setLastChangedStatusCode(productOrderChangeType);
        return changedProductOrderListRequest;
    }

    /**
     * 상품주문정보리스트 요청 기본데이터 생성
     *
     * @param productIdList 상품주문번호
     * @return 상품주문정보리스트 요청 기본데이터
     */
    public static GetProductOrderInfoListRequest createGetProductOrderInfoListRequest(String productIdList) {
        GetProductOrderInfoListRequest productOrderInfoListRequest = new GetProductOrderInfoListRequest();
        productOrderInfoListRequest.addProductOrderIDList(productIdList);
        productOrderInfoListRequest.setVersion(naverConfig.getOrder().getServiceVersion());
        productOrderInfoListRequest.setDetailLevel(naverConfig.getDetailLevel());
        return productOrderInfoListRequest;
    }

    /**
     * 주문취소요청 기본데이터 생성.
     *
     * @param productOrderId 상품주문번호
     * @param claimRequestReasonType 클레임사유코드
     * @return 주문취소요청 기본데이터
     */
    public static CancelSaleRequest createCancelSaleRequest(String productOrderId, ClaimRequestReasonType claimRequestReasonType) {
        CancelSaleRequest cancelSaleRequest = new CancelSaleRequest();
        cancelSaleRequest.setDetailLevel(naverConfig.getDetailLevel());
        cancelSaleRequest.setVersion(naverConfig.getOrder().getServiceVersion());
        cancelSaleRequest.setRequestID(UUID.randomUUID().toString());
        cancelSaleRequest.setProductOrderID(productOrderId);
        cancelSaleRequest.setCancelReasonCode(claimRequestReasonType);
        return cancelSaleRequest;
    }

    /**
     * 취소주문 승인 기본데이터 생성
     *
     * @param productOrderId 상품주문번호
     * @return 취소주문 승인 기본데이터
     */
    public static ApproveCancelApplicationRequest createApproveCancelRequest(String productOrderId) {
        ApproveCancelApplicationRequest approveCancelRequest = new ApproveCancelApplicationRequest();
        approveCancelRequest.setDetailLevel(naverConfig.getDetailLevel());
        approveCancelRequest.setVersion(naverConfig.getOrder().getServiceVersion());
        approveCancelRequest.setRequestID(UUID.randomUUID().toString());
        approveCancelRequest.setProductOrderID(productOrderId);
        return approveCancelRequest;
    }

    /**
     * 반품주문접수 요청 기본데이터 생성
     *
     * @param productOrderId 상품주문번호
     * @param reasonType 클레임사유코드
     * @return 반품주문접수 요청 기본데이터
     */
    public static RequestReturnRequest createRequestReturnRequest(String productOrderId, ClaimRequestReasonType reasonType) {
        RequestReturnRequest requestReturnRequest = new RequestReturnRequest();
        requestReturnRequest.setDetailLevel(naverConfig.getDetailLevel());
        requestReturnRequest.setVersion(naverConfig.getOrder().getServiceVersion());
        requestReturnRequest.setRequestID(UUID.randomUUID().toString());
        requestReturnRequest.setProductOrderID(productOrderId);
        requestReturnRequest.setReturnReasonCode(reasonType);
        requestReturnRequest.setCollectDeliveryMethodCode(DeliveryMethodType.RETURN_INDIVIDUAL);
        requestReturnRequest.setCollectDeliveryCompanyCode("HOMEPLUS");
        return requestReturnRequest;
    }

    /**
     * 반품승인 기본데이터 생성
     *
     * @param productOrderId 상품주문번호
     * @return 반품승인 기본데이터
     */
    public static ApproveReturnApplicationRequest createApproveReturnRequest(String productOrderId) {
        ApproveReturnApplicationRequest approveReturnRequest = new ApproveReturnApplicationRequest();
        approveReturnRequest.setDetailLevel(naverConfig.getDetailLevel());
        approveReturnRequest.setVersion(naverConfig.getOrder().getServiceVersion());
        approveReturnRequest.setRequestID(UUID.randomUUID().toString());
        approveReturnRequest.setProductOrderID(productOrderId);
        return approveReturnRequest;
    }

    /**
     * 교환승인 기본데이터 생성
     *
     * @param productOrderId 상품주문번호
     * @return 교환승인 기본데이터
     */
    public static ApproveCollectedExchangeRequest createApproveExchangeRequest(String productOrderId) {
        ApproveCollectedExchangeRequest approveCollectedExchangeRequest = new ApproveCollectedExchangeRequest();
        approveCollectedExchangeRequest.setDetailLevel(naverConfig.getDetailLevel());
        approveCollectedExchangeRequest.setVersion(naverConfig.getOrder().getServiceVersion());
        approveCollectedExchangeRequest.setRequestID(UUID.randomUUID().toString());
        approveCollectedExchangeRequest.setProductOrderID(productOrderId);
        return approveCollectedExchangeRequest;
    }

    /**
     * 반품거절 기본데이터 생성
     *
     * @param productOrderId 주문상품번호
     * @param rejectDetailContent 거절사유
     * @return 반품거절 기본데이터
     */
    public static RejectReturnRequest createRejectReturnRequest(String productOrderId, String rejectDetailContent) {
        RejectReturnRequest rejectReturnRequest = new RejectReturnRequest();
        rejectReturnRequest.setDetailLevel(naverConfig.getDetailLevel());
        rejectReturnRequest.setVersion(naverConfig.getOrder().getServiceVersion());
        rejectReturnRequest.setRequestID(UUID.randomUUID().toString());
        rejectReturnRequest.setProductOrderID(productOrderId);
        rejectReturnRequest.setRejectDetailContent(rejectDetailContent);
        return rejectReturnRequest;
    }

    /**
     * 반품보류처리 기본데이터 생성
     *
     * @param productOrderId 주문상품번호
     * @param holdCode 보류코드
     * @param withholdDetailContent 보류상세사유
     * @return 반품보류처리 기본데이터
     */
    public static WithholdReturnRequest createWithholdReturnRequest(String productOrderId, HoldbackClassType holdCode, String withholdDetailContent) {
        WithholdReturnRequest withholdReturnRequest = new WithholdReturnRequest();
        withholdReturnRequest.setDetailLevel(naverConfig.getDetailLevel());
        withholdReturnRequest.setVersion(naverConfig.getOrder().getServiceVersion());
        withholdReturnRequest.setRequestID(UUID.randomUUID().toString());
        withholdReturnRequest.setProductOrderID(productOrderId);
        withholdReturnRequest.setReturnHoldCode(holdCode);
        withholdReturnRequest.setReturnHoldDetailContent(withholdDetailContent);
        return withholdReturnRequest;
    }

    /**
     * 반품보류 해제 기본데이터 생성
     *
     * @param productOrderId 주문상품번호
     * @return 반품보류해제 기본데이터
     */
    public static ReleaseReturnHoldRequest createReleaseReturnHoldRequest(String productOrderId) {
        ReleaseReturnHoldRequest releaseReturnHoldRequest = new ReleaseReturnHoldRequest();
        releaseReturnHoldRequest.setDetailLevel(naverConfig.getDetailLevel());
        releaseReturnHoldRequest.setVersion(naverConfig.getOrder().getServiceVersion());
        releaseReturnHoldRequest.setRequestID(UUID.randomUUID().toString());
        releaseReturnHoldRequest.setProductOrderID(productOrderId);
        return releaseReturnHoldRequest;
    }

    /**
     * 교환거절 요청 기본데이터 생성
     *
     * @param productOrderId 주문상품번호
     * @param rejectDetailContent 거절상세사유
     * @return 교환거절 요청 기본데이터
     */
    public static RejectExchangeRequest createRejectExchangeRequest(String productOrderId, String rejectDetailContent) {
        RejectExchangeRequest rejectExchangeRequest = new RejectExchangeRequest();
        rejectExchangeRequest.setDetailLevel(naverConfig.getDetailLevel());
        rejectExchangeRequest.setVersion(naverConfig.getOrder().getServiceVersion());
        rejectExchangeRequest.setRequestID(UUID.randomUUID().toString());
        rejectExchangeRequest.setProductOrderID(productOrderId);
        rejectExchangeRequest.setRejectDetailContent(rejectDetailContent);
        return rejectExchangeRequest;
    }

    /**
     * 교환보류 요청 기본데이터 생성
     *
     * @param productOrderId 주문상품번호
     * @param holdCode 보류코드
     * @param withholdDetailContent 보류상세사유
     * @return 교환보류 요청 기본데이터
     */
    public static WithholdExchangeRequest createWithholdExchangeRequest(String productOrderId, HoldbackClassType holdCode, String withholdDetailContent){
        WithholdExchangeRequest withholdExchangeRequest = new WithholdExchangeRequest();
        withholdExchangeRequest.setDetailLevel(naverConfig.getDetailLevel());
        withholdExchangeRequest.setVersion(naverConfig.getOrder().getServiceVersion());
        withholdExchangeRequest.setRequestID(UUID.randomUUID().toString());
        withholdExchangeRequest.setProductOrderID(productOrderId);
        withholdExchangeRequest.setExchangeHoldCode(holdCode);
        withholdExchangeRequest.setExchangeHoldDetailContent(withholdDetailContent);
        return withholdExchangeRequest;
    }

    /**
     * 교환보류 해제 기본데이터 생성
     * @param productOrderId 주문상품번호
     * @return 교환보류 해제 기본데이터
     */
    public static ReleaseExchangeHoldRequest createReleaseExchangeHoldRequest(String productOrderId) {
        ReleaseExchangeHoldRequest releaseExchangeHoldRequest = new ReleaseExchangeHoldRequest();
        releaseExchangeHoldRequest.setDetailLevel(naverConfig.getDetailLevel());
        releaseExchangeHoldRequest.setVersion(naverConfig.getOrder().getServiceVersion());
        releaseExchangeHoldRequest.setRequestID(UUID.randomUUID().toString());
        releaseExchangeHoldRequest.setProductOrderID(productOrderId);
        return releaseExchangeHoldRequest;
    }

    /**
     * 교환완료 기본데이터 생성
     * @param productOrderId 주문상품번호
     * @return 교환완료 기본데이터
     */
    public static ReDeliveryExchangeRequest createReDeliveryExchangeRequest(String productOrderId) {
        ReDeliveryExchangeRequest reDeliveryExchangeRequest = new ReDeliveryExchangeRequest();
        reDeliveryExchangeRequest.setDetailLevel(naverConfig.getDetailLevel());
        reDeliveryExchangeRequest.setVersion(naverConfig.getOrder().getServiceVersion());
        reDeliveryExchangeRequest.setRequestID(UUID.randomUUID().toString());
        reDeliveryExchangeRequest.setProductOrderID(productOrderId);
        reDeliveryExchangeRequest.setReDeliveryMethodCode(DeliveryMethodType.DIRECT_DELIVERY);
        reDeliveryExchangeRequest.setReDeliveryCompanyCode("HOMEPLUS");
        return reDeliveryExchangeRequest;
    }
}
