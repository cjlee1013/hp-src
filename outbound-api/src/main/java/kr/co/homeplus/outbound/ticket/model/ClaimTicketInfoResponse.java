package kr.co.homeplus.outbound.ticket.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import static kr.co.homeplus.outbound.core.constants.ClaimTicketConstants.*;

@ApiModel(description = "티켓 취소시 발행 정보 조회 API 응답 객체")
@Getter
@XmlRootElement(namespace= XML_ROOT_NAME_SPACE, name= CLAIM_TICKET_INFO_XML_ROOT_NAME)
@XmlAccessorType(XmlAccessType.FIELD)
public class ClaimTicketInfoResponse {

    @ApiModelProperty(value = "취소리턴코드")
    @XmlElement(name="RESULTCODE", namespace= XML_ROOT_NAME_SPACE)
    private String resultCode;

    @ApiModelProperty(value = "취소리턴메시지")
    @XmlElement(name="RESULTMSG", namespace= XML_ROOT_NAME_SPACE)
    private String resultMsg;

    @ApiModelProperty(value = "쿠폰 번호")
    @XmlElement(name="COUPONNUMBER", namespace= XML_ROOT_NAME_SPACE)
    private String couponNumber;

    @ApiModelProperty(value = "쿠폰 사용 여부")
    @XmlElement(name="USE_YN", namespace= XML_ROOT_NAME_SPACE)
    private String useYn;

    @ApiModelProperty(value = "쿠폰 사용 일자")
    @XmlElement(name="USE_DATE", namespace= XML_ROOT_NAME_SPACE)
    private String useDate;

    @ApiModelProperty(value = "쿠폰 사용 장소")
    @XmlElement(name="USE_PLACE", namespace= XML_ROOT_NAME_SPACE)
    private String usePlace;

    @ApiModelProperty(value = "최종 수신자 번호")
    @XmlElement(name="SEND_HP", namespace= XML_ROOT_NAME_SPACE)
    private String sendHp;

    @ApiModelProperty(value = "유효기간 시작일")
    @XmlElement(name="USE_START", namespace= XML_ROOT_NAME_SPACE)
    private String useStart;

    @ApiModelProperty(value = "유효기간 종료일")
    @XmlElement(name="USE_END", namespace= XML_ROOT_NAME_SPACE)
    private String useEnd;

    @ApiModelProperty(value = "발급일자")
    @XmlElement(name="INS_DATE", namespace= XML_ROOT_NAME_SPACE)
    private String insDate;

    @ApiModelProperty(value = "임시공간01")
    @XmlElement(name="FILLER01", namespace= XML_ROOT_NAME_SPACE)
    private String filler01;

    @ApiModelProperty(value = "임시공간02")
    @XmlElement(name="FILLER01", namespace= XML_ROOT_NAME_SPACE)
    private String filler02;

    @ApiModelProperty(value = "임시공간03")
    @XmlElement(name="FILLER01", namespace= XML_ROOT_NAME_SPACE)
    private String filler03;

}
