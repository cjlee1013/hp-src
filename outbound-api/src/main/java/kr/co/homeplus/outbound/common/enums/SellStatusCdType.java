package kr.co.homeplus.outbound.common.enums;

/**
 * <pre>
 * Class Name : SendType.java
 * Description : 제휴사 상품 전송 상태 타입
 *
 *  Modification Information
 *  Modify Date 		Modifier	Comment
 *  -----------------------------------------------
 *  2021. 6. 3.		   jh6108.kim	신규작성
 *
 * </pre>
 *
 * @author song7749
 * @since 2021. 6. 3.
 */
public enum SellStatusCdType {

    ONSALE("OnSale", "판매가능"), STOP("Stop", "판매중지");

    private final String code;
    private final String desc;

    SellStatusCdType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return this.code;
    }

    public String getDesc() {
        return this.desc;
    }

    public static SellStatusCdType valueOfCode(String code) {
        for (SellStatusCdType st : SellStatusCdType.values()) {
            if (st.getCode().equals(code)) {
                return st;
            }
        }
        return null;
    }

}
