package kr.co.homeplus.outbound.common.enums;

public enum DescriptionType {

    TITLE("01", "제목"),
    OPTION("02", "옵션" ),
    NOTIFICATION("03", "고시정보"),
    DETAIL("04", "상세정보");

    private final String code;
    private final String desc;

    DescriptionType(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String getCode() {
		return this.code;
	}

    public String getDesc() {
        return desc;
    }
}