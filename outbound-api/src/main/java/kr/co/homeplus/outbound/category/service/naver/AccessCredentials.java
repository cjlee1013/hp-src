package kr.co.homeplus.outbound.category.service.naver;

import com.nhncorp.psinfra.toolkit.SimpleCryptLib;
import java.security.SignatureException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * Class Name : AccessCredentials.java
 * Description : Naver SmartStore AccessCredentials ( 인증 )
 *
 *  Modification Information
 *  Modify Date    Modifier    Comment
 *  -----------------------------------------------
 *  2021. 5. 18.   jh6108.kim   신규작성
 * </pre>
 *
 * @author jh6108.kim
 * @since 2021. 5. 18.
 */
@Slf4j
@Component
public class AccessCredentials {

    private static String accessLicense = null;
    private static String secretKey     = null;

    @Value("${market.naver.accessLicense}")
    public void setAccessLicense(String sAccessLicense) {
        accessLicense =  sAccessLicense;
    }

    @Value("${market.naver.secretKey}")
    public void setSecretKey(String sSecretKey) {
        secretKey =  sSecretKey;
    }

    /**
     * PRODUCT 관련 API 호출시 필요한 인증 정보를 생성한다.
     *
     * @param serviceName
     * @param operationName
     * @return com.nhncorp.platform.shopn.ProductServiceStub.AccessCredentialsType
     * @throws
     */
    public com.nhncorp.platform.shopn.ProductServiceStub.AccessCredentialsType createProduct(String serviceName, String operationName) {

        com.nhncorp.platform.shopn.ProductServiceStub.AccessCredentialsType accessCredentialsType = new com.nhncorp.platform.shopn.ProductServiceStub.AccessCredentialsType();

        // 서명생성
        String timeStamp = SimpleCryptLib.getTimestamp();
        String data = timeStamp + serviceName + operationName;

        try {
            // 인증정보
            accessCredentialsType.setAccessLicense(accessLicense);
            accessCredentialsType.setSignature(SimpleCryptLib.generateSign(data, secretKey));
            accessCredentialsType.setTimestamp(timeStamp);
        } catch (SignatureException e) {
            log.error("AccessCredentials createProduct Error :: ", e.fillInStackTrace());
            return null;
        }

        return accessCredentialsType;
    }

    /**
     * BRANCH 관련 API 호출시 필요한 인증 정보를 생성한다.
     *
     * @param serviceName
     * @param operationName
     * @return com.nhncorp.platform.shopn.ProductServiceStub.AccessCredentialsType
     * @throws
     */
    public com.nhncorp.platform.shopn.BranchServiceStub.AccessCredentialsType createBranch(String serviceName, String operationName) {

        com.nhncorp.platform.shopn.BranchServiceStub.AccessCredentialsType accessCredentialsType = new com.nhncorp.platform.shopn.BranchServiceStub.AccessCredentialsType();

        //서명생성
        String timeStamp = SimpleCryptLib.getTimestamp();
        String data = timeStamp + serviceName + operationName;

        try {
            // 인증정보
            accessCredentialsType.setAccessLicense(accessLicense);
            accessCredentialsType.setSignature(SimpleCryptLib.generateSign(data, secretKey));
            accessCredentialsType.setTimestamp(timeStamp);
        } catch (SignatureException e) {
            log.error("AccessCredentials createBranch Error :: ", e.fillInStackTrace());
            return null;
        }

        return accessCredentialsType;
    }

    /**
     * IMAGE 관련 API 호출시 필요한 인증 정보를 생성한다.
     *
     * @param serviceName
     * @param operationName
     * @return com.nhncorp.platform.shopn.ProductServiceStub.AccessCredentialsType
     * @throws
     */
    public com.nhncorp.platform.shopn.ImageServiceStub.AccessCredentialsType createImage(String serviceName , String operationName) {

        com.nhncorp.platform.shopn.ImageServiceStub.AccessCredentialsType accessCredentialsType = new com.nhncorp.platform.shopn.ImageServiceStub.AccessCredentialsType();

        //서명생성
        String timeStamp = SimpleCryptLib.getTimestamp();
        String data = timeStamp + serviceName + operationName;

        try {
            // 인증정보
            accessCredentialsType.setAccessLicense(accessLicense);
            accessCredentialsType.setSignature(SimpleCryptLib.generateSign(data, secretKey));
            accessCredentialsType.setTimestamp(timeStamp);
        } catch (SignatureException e) {
            log.error("AccessCredentials createImage Error :: ", e.fillInStackTrace());
            return null;
        }

        return accessCredentialsType;
    }

}