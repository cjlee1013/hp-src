package kr.co.homeplus.outbound.core.db.construct;

import javax.sql.DataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

/**
 * SqlSessionFactoryBuilder
 */
public class SqlSessionFactoryBuilder {
    public static final String CONFIG_LOCATION_PATH = "classpath:/mybatis-config.xml";
    public static final String MAPPER_LOCATIONS_PATH = "classpath:/kr/co/homeplus/**/*.xml";
    public static final String TYPE_ALIASES_PACKAGE = "kr.co.homeplus.outbound";

    /**
     * SqlSessionFactory build
     * @param dataSource DataSource
     * @return SqlSessionFactory
     * @throws Exception
     */
    public static SqlSessionFactory build(DataSource dataSource) throws Exception {
        SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
        PathMatchingResourcePatternResolver pathResolver = new PathMatchingResourcePatternResolver();
        sessionFactoryBean.setDataSource(dataSource);
        sessionFactoryBean.setTypeAliasesPackage(TYPE_ALIASES_PACKAGE);
        sessionFactoryBean.setConfigLocation(pathResolver.getResource(CONFIG_LOCATION_PATH));
        sessionFactoryBean.setMapperLocations(pathResolver.getResources(MAPPER_LOCATIONS_PATH));
        return sessionFactoryBean.getObject();
    }
}
