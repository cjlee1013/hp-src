package kr.co.homeplus.outbound.market.eleven.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DlvMthdCd {

    PARCEL("01", "택배"),
    POST("02", "우편(소포/등기)"),
    DIRECT("03", "직접전달(화물택배)"),
    QUICK("04", "퀵서비스"),
    NECESSARY("05", "필요없음");

    private String code;
    private String desc;

}
