package kr.co.homeplus.outbound.market.naver.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nhncorp.platform.shopn.seller.SellerServiceStub;
import com.nhncorp.psinfra.toolkit.SimpleCryptLib;
import kr.co.homeplus.outbound.market.naver.config.MarketNaverConfig;
import kr.co.homeplus.outbound.market.naver.model.Address;
import kr.co.homeplus.outbound.market.naver.model.ChangedProductOrderResult;
import kr.co.homeplus.outbound.market.naver.model.Order;
import kr.co.homeplus.outbound.market.naver.model.OrderCancelRequest;
import kr.co.homeplus.outbound.market.naver.model.OrderCancelResponse;
import kr.co.homeplus.outbound.market.naver.model.PlaceOrderFail;
import kr.co.homeplus.outbound.market.naver.model.PlaceOrderRequest;
import kr.co.homeplus.outbound.market.naver.model.PlaceOrderResponse;
import kr.co.homeplus.outbound.market.naver.model.ProductOrder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.stereotype.Service;

import java.security.Security;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class NaverService {

    private final MarketNaverConfig marketNaverConfig;

    private String timeStamp;	// 암복호화 시 사용
    private byte[] encryptKey;	// 암복호화 시 사용

    private static SimpleDateFormat DEFAULT_YMD_HIS_DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 네이버 주문 조회(1000건 초과할 경우 반복)
     * @param fromDt
     * @param toDt
     * @param lastChangedStatusCode
     * @return
     * @throws Exception
     */
    public List<Order> getChangedProductOrderList(String fromDt, String toDt, String lastChangedStatusCode) throws Exception {
        List<Order> orderList = new ArrayList<>();
        boolean hasMoreData;
        String originFromDt = fromDt;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        log.info("[NAVER search START] {} - {} ~ {}, status: {}", format.format(new Date()), fromDt, toDt, lastChangedStatusCode);

        do {
            ChangedProductOrderResult changedProductOrderResult = getChangedProductOrderList(fromDt, toDt, lastChangedStatusCode, orderList);
            hasMoreData = changedProductOrderResult.isHasMoreData();
            fromDt = changedProductOrderResult.getFromDt();
        } while (hasMoreData);

        log.info("NAVER 주문 조회 - 조회범위: {} ~ {}, 주문건수: {}", originFromDt, toDt, orderList.size());

        if (orderList.size() > 0) {
            ObjectMapper om = new ObjectMapper();
            for (Order order : orderList) {
                log.info("order: {}", om.writeValueAsString(order));
            }
        }

        log.info("[NAVER search END] " + format.format(new Date()));
        return orderList;
    }

    /**
     * 네이버 주문 조회
     * @param fromDt
     * @param toDt
     * @param lastChangedStatusCode
     * @param orderList
     * @return
     * @throws Exception
     */
    private ChangedProductOrderResult getChangedProductOrderList(String fromDt, String toDt, String lastChangedStatusCode, List<Order> orderList) throws Exception {

        boolean hasMoreData = false;
        String newFromDt = "";

        Security.addProvider(new BouncyCastleProvider());
        SellerServiceStub.GetChangedProductOrderListRequestE changedProductOrderListRequestE = new SellerServiceStub.GetChangedProductOrderListRequestE();
        SellerServiceStub.GetChangedProductOrderListRequest changedProductOrderListRequest = new SellerServiceStub.GetChangedProductOrderListRequest();
        SellerServiceStub sellerServiceStub = new SellerServiceStub(marketNaverConfig.getTargetEndpoint()+marketNaverConfig.getOrder().getServiceName());

        changedProductOrderListRequest.setAccessCredentials(getAccessCredentialType(marketNaverConfig.getOrder().getServiceName(), "GetChangedProductOrderList"));
        log.info("GetChangedProductOrderList timeStamp: {}", timeStamp);

        //GetChangedProductOrderList 파라메터 설정
        changedProductOrderListRequest.setDetailLevel("Full");
        changedProductOrderListRequest.setVersion(marketNaverConfig.getOrder().getServiceVersion());
        changedProductOrderListRequest.setRequestID(UUID.randomUUID().toString());
        changedProductOrderListRequest.setMallID(marketNaverConfig.getSellerId());
        changedProductOrderListRequest.setInquiryTimeFrom(getCalendar(fromDt, "from"));
        changedProductOrderListRequest.setInquiryTimeTo(getCalendar(toDt, "to"));

        changedProductOrderListRequest.setLastChangedStatusCode(getProductOrderChangeType(lastChangedStatusCode));
        changedProductOrderListRequestE.setGetChangedProductOrderListRequest(changedProductOrderListRequest);

        //GetChangedProductOrderList Response 수신
        SellerServiceStub.GetChangedProductOrderListResponseE changedProductOrderListResponseE = sellerServiceStub.getChangedProductOrderList(changedProductOrderListRequestE);
        SellerServiceStub.GetChangedProductOrderListResponse changedProductOrderListResponse = changedProductOrderListResponseE.getGetChangedProductOrderListResponse();

        //결과 출력
        if ("SUCCESS".equals(changedProductOrderListResponse.getResponseType()) || "SUCCESS-WARNING".equals(changedProductOrderListResponse.getResponseType())) {
            SellerServiceStub.ChangedProductOrderInfo[] changedProductOrderInfos = changedProductOrderListResponse.getChangedProductOrderInfoList();

            if (changedProductOrderInfos != null) {
                log.info("changedProductOrderInfos.length: {}", changedProductOrderInfos.length);

                List<List<String>> productOrderIdGroupList = new ArrayList<>();
                List<String> productOrderIdSubList = new ArrayList<>();
                for (int i=0; i< changedProductOrderInfos.length; i++) {
                    if (i%300 == 0) {
                        productOrderIdSubList = new ArrayList<>();
                    }

                    productOrderIdSubList.add(changedProductOrderInfos[i].getProductOrderID());

                    if (i%300 == 299 || i == changedProductOrderInfos.length-1) {
                        productOrderIdGroupList.add(productOrderIdSubList);
                    }
                }

                for (List<String> productOrderIdList : productOrderIdGroupList) {
                    SellerServiceStub.ProductOrderInfo[] productOrderInfos = getProducOrderList(productOrderIdList, sellerServiceStub);
                    log.info("productOrderInfos.length: {}", productOrderInfos == null ? 0 : productOrderInfos.length);
                    setOrderList(productOrderInfos, orderList);
                }
            }

            hasMoreData = changedProductOrderListResponse.getHasMoreData();
            if (hasMoreData) {
                SimpleDateFormat hasMoreDataDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                newFromDt = hasMoreDataDateFormat.format(changedProductOrderListResponse.getMoreDataTimeFrom().getTime());
                log.info("hasMoreData - returnedDataCount: {}, moreDataTimeFrom: {}, inquiryExtraData: {}, newFromDt: {}", changedProductOrderListResponse.getReturnedDataCount(), changedProductOrderListResponse.getMoreDataTimeFrom().getTime(), changedProductOrderListResponse.getInquiryExtraData(), newFromDt);
            }

        } else {
            log.info("GetChangedProductOrderList 조회 실패");
            log.info("GetChangedProductOrderListResponse - Error.Code : {}", changedProductOrderListResponse.getError().getCode());
            log.info("GetChangedProductOrderListResponse - Error.Message : {}", changedProductOrderListResponse.getError().getMessage());
            log.info("GetChangedProductOrderListResponse - Error.Detail : {}", changedProductOrderListResponse.getError().getDetail());
        }

        ChangedProductOrderResult changedProductOrderResult = new ChangedProductOrderResult();
        changedProductOrderResult.setHasMoreData(hasMoreData);
        changedProductOrderResult.setFromDt(newFromDt);

        return changedProductOrderResult;
    }

    private SellerServiceStub.ProductOrderInfo[] getProducOrderList(List<String> productOrderIdList, SellerServiceStub sellerServiceStub) throws Exception {

        if (productOrderIdList == null || productOrderIdList.size() == 0) {
            return null;
        }

        SellerServiceStub.GetProductOrderInfoListRequestE productOrderInfoListRequestE = new SellerServiceStub.GetProductOrderInfoListRequestE();
        SellerServiceStub.GetProductOrderInfoListRequest productOrderInfoListRequest = new SellerServiceStub.GetProductOrderInfoListRequest();
        productOrderInfoListRequest.setAccessCredentials(getAccessCredentialType(marketNaverConfig.getOrder().getServiceName(), "GetProductOrderInfoList"));
        log.info("GetProductOrderInfoList timeStamp: {}", timeStamp);
        productOrderInfoListRequest.setVersion(marketNaverConfig.getOrder().getServiceVersion());
        productOrderInfoListRequest.setDetailLevel("Full");
        for (String productOrderId : productOrderIdList) {
            productOrderInfoListRequest.addProductOrderIDList(productOrderId);
        }

        productOrderInfoListRequestE.setGetProductOrderInfoListRequest(productOrderInfoListRequest);

        SellerServiceStub.GetProductOrderInfoListResponseE productOrderInfoListResponseE = sellerServiceStub.getProductOrderInfoList(productOrderInfoListRequestE);
        SellerServiceStub.GetProductOrderInfoListResponse productOrderInfoListResponse = productOrderInfoListResponseE.getGetProductOrderInfoListResponse();
        SellerServiceStub.ProductOrderInfo[] productOrderInfos = productOrderInfoListResponse.getProductOrderInfoList();

        if (productOrderInfoListResponse.getFailedProductOrderIDList() != null && productOrderInfoListResponse.getFailedProductOrderIDList().length > 0) {
            log.info("GetProductOrderInfoList 조회 실패");
            Arrays.asList(productOrderInfoListResponse.getFailedProductOrderIDList()).stream().forEach(failedProductOrderId -> {
                log.info("failedProductOrderId: {}", failedProductOrderId);
            });
        }

        if (!"SUCCESS".equals(productOrderInfoListResponse.getResponseType()) && !"SUCCESS-WARNING".equals(productOrderInfoListResponse.getResponseType())) {
            log.info("GetProductOrderInfoListResponse - Error.Code: {}", productOrderInfoListResponse.getError().getCode());
            log.info("GetProductOrderInfoListResponse - Error.Message: {}", productOrderInfoListResponse.getError().getMessage());
            log.info("GetProductOrderInfoListResponse - Error.Detail: {}", productOrderInfoListResponse.getError().getDetail());
            return null;
        }

        return productOrderInfos;
    }

    private void setOrderList(SellerServiceStub.ProductOrderInfo[] productOrderInfos, List<Order> orderList) throws Exception {

        if (productOrderInfos == null || productOrderInfos.length == 0) {
            return;
        }

        generateEncryptKey();

        for (int j=0; j<productOrderInfos.length; j++) {
            SellerServiceStub.Order order = productOrderInfos[j].getOrder();
            Order convertedOrder = mapOrder(order);
            if (!orderList.stream().anyMatch(n -> n.getOrderID().equals(convertedOrder.getOrderID()))) {
                orderList.add(convertedOrder);
            }

            SellerServiceStub.ProductOrder productOrder = productOrderInfos[j].getProductOrder();
            ProductOrder convertedProductOrder = mapProductOrder(productOrder);
            convertedProductOrder.setOrderID(convertedOrder.getOrderID());

            SellerServiceStub.Address shippingAddress = productOrder.getShippingAddress();
            Address convertedShippingAddress = mapAddress(shippingAddress);
            convertedProductOrder.setShippingAddress(convertedShippingAddress);

            SellerServiceStub.Address takingAddress = productOrder.getTakingAddress();
            Address convertedTakingAddress = mapAddress(takingAddress);
            convertedProductOrder.setTakingAddress(convertedTakingAddress);

            orderList.stream().filter(n -> n.getOrderID().equals(convertedProductOrder.getOrderID())).findFirst().get().getProductOrderList().add(convertedProductOrder);
        }
    }

    /**
     * 네이버 주문 발주
     * @param placeOrderRequest
     * @return
     * @throws Exception
     */
    public PlaceOrderResponse placeOrder(PlaceOrderRequest placeOrderRequest) throws Exception {

        PlaceOrderResponse placeOrderResponse = new PlaceOrderResponse();
        List<String> placeOrderSuccessMarketOrderItemNoList = new ArrayList<String>();
        List<PlaceOrderFail> placeOrderFailMarketOrderItemList = new ArrayList<PlaceOrderFail>();
        placeOrderResponse.setPlaceOrderSuccessMarketOrderItemNoList(placeOrderSuccessMarketOrderItemNoList);
        placeOrderResponse.setPlaceOrderFailMarketOrderItemList(placeOrderFailMarketOrderItemList);

        Security.addProvider(new BouncyCastleProvider());

        SellerServiceStub sellerServiceStub = new SellerServiceStub(marketNaverConfig.getTargetEndpoint()+marketNaverConfig.getOrder().getServiceName());
        for (String marketOrderItemNo : placeOrderRequest.getMarketOrderItemNoList()) {
            SellerServiceStub.PlaceProductOrderRequestE placeProductOrderRequestE = new SellerServiceStub.PlaceProductOrderRequestE();
            SellerServiceStub.PlaceProductOrderRequest placeProductOrderRequest = new SellerServiceStub.PlaceProductOrderRequest();

            placeProductOrderRequest.setAccessCredentials(getAccessCredentialType(marketNaverConfig.getOrder().getServiceName(), "PlaceProductOrder"));
            placeProductOrderRequest.setVersion(marketNaverConfig.getOrder().getServiceVersion());
            placeProductOrderRequest.setRequestID(UUID.randomUUID().toString());
            placeProductOrderRequest.setDetailLevel("Full");
            placeProductOrderRequest.setProductOrderID(marketOrderItemNo);

            placeProductOrderRequestE.setPlaceProductOrderRequest(placeProductOrderRequest);

            SellerServiceStub.PlaceProductOrderResponseE placeProductOrderResponseE = sellerServiceStub.placeProductOrder(placeProductOrderRequestE);
            SellerServiceStub.PlaceProductOrderResponse placeProductOrderResponse = placeProductOrderResponseE.getPlaceProductOrderResponse();

            log.info("marketOrderItemNo: {}, responseType: {}", marketOrderItemNo, placeProductOrderResponse.getResponseType());

            if ("SUCCESS".equals(placeProductOrderResponse.getResponseType()) || "SUCCESS-WARNING".equals(placeProductOrderResponse.getResponseType())) {
                if (placeProductOrderResponse.getResponseType().indexOf("WARNING") >= 0) {
                    SellerServiceStub.WarningList_type0 warningList = placeProductOrderResponse.getWarningList();
                    if (warningList != null) {
                        SellerServiceStub.WarningType[] warningTypes = warningList.getWarning();
                        if (warningTypes != null && warningTypes.length > 0) {
                            int idxWarn = 0;
                            for (SellerServiceStub.WarningType warningType : warningTypes) {
                                log.warn("warning({}) - code: {}, message: {}, detail: {}", idxWarn++, warningType.getCode(), warningType.getMessage(), warningType.getDetail());
                            }
                        }
                    }
                }

                placeOrderSuccessMarketOrderItemNoList.add(marketOrderItemNo);
            } else {
                PlaceOrderFail placeOrderFail = new PlaceOrderFail();
                placeOrderFail.setMarketOrderItemNo(marketOrderItemNo);

                SellerServiceStub.ErrorType errorType = placeProductOrderResponse.getError();
                if (errorType != null) {
                    placeOrderFail.setErrorCode(errorType.getCode());
                    placeOrderFail.setErrorMsg(errorType.getMessage());
                    placeOrderFail.setErrorDetail(errorType.getDetail());

                    log.info("PlaceOrder Fail! - marketOrderItemNo: {}, errorCode: {}, errorMessage: {}, errorDetail: {}", marketOrderItemNo, errorType.getCode(), errorType.getMessage(), errorType.getDetail());
                }

                placeOrderFailMarketOrderItemList.add(placeOrderFail);
            }
        }

        return placeOrderResponse;
    }

    /**
     * 네이버 주문 취소
     * @param orderCancelRequest
     * @return
     * @throws Exception
     */
    public OrderCancelResponse orderCancel(OrderCancelRequest orderCancelRequest) throws Exception {

        OrderCancelResponse orderCancelResponse = new OrderCancelResponse();
        List<String> orderCancelSuccessMarketOrderItemNoList = new ArrayList<String>();
        List<String> orderCancelFailMarketOrderItemNoList = new ArrayList<String>();
        orderCancelResponse.setOrderCancelSuccessMarketOrderItemNoList(orderCancelSuccessMarketOrderItemNoList);
        orderCancelResponse.setOrderCancelFailMarketOrderItemNoList(orderCancelFailMarketOrderItemNoList);

        Security.addProvider(new BouncyCastleProvider());

        SellerServiceStub sellerServiceStub = new SellerServiceStub(marketNaverConfig.getTargetEndpoint()+marketNaverConfig.getOrder().getServiceName());
        for (String marketOrderItemNo : orderCancelRequest.getMarketOrderItemNoList()) {
            SellerServiceStub.CancelSaleRequest cancelSaleRequest = new SellerServiceStub.CancelSaleRequest();
            SellerServiceStub.CancelSaleRequestE cancelSaleRequestE = new SellerServiceStub.CancelSaleRequestE();

            cancelSaleRequest.setAccessCredentials(getAccessCredentialType(marketNaverConfig.getOrder().getServiceName(), "PlaceProductOrder"));
            cancelSaleRequest.setVersion(marketNaverConfig.getOrder().getServiceVersion());
            cancelSaleRequest.setRequestID(UUID.randomUUID().toString());
            cancelSaleRequest.setDetailLevel("Full");
            cancelSaleRequest.setProductOrderID(marketOrderItemNo);
            cancelSaleRequest.setCancelReasonCode(SellerServiceStub.ClaimRequestReasonType.WRONG_ORDER);

            cancelSaleRequestE.setCancelSaleRequest(cancelSaleRequest);

            SellerServiceStub.CancelSaleResponseE cancelSaleResponseE = sellerServiceStub.cancelSale(cancelSaleRequestE);
            SellerServiceStub.CancelSaleResponse cancelSaleResponse = cancelSaleResponseE.getCancelSaleResponse();

            log.info("marketOrderItemNo: {}, responseType: {}", marketOrderItemNo, cancelSaleResponse.getResponseType());

            if ("SUCCESS".equals(cancelSaleResponse.getResponseType()) || "SUCCESS-WARNING".equals(cancelSaleResponse.getResponseType())) {
                if (cancelSaleResponse.getResponseType().indexOf("WARNING") >= 0) {
                    SellerServiceStub.WarningList_type0 warningList = cancelSaleResponse.getWarningList();
                    if (warningList != null) {
                        SellerServiceStub.WarningType[] warningTypes = warningList.getWarning();
                        if (warningTypes != null && warningTypes.length > 0) {
                            int idxWarn = 0;
                            for (SellerServiceStub.WarningType warningType : warningTypes) {
                                log.warn("warning({}) - code: {}, message: {}, detail: {}", idxWarn++, warningType.getCode(), warningType.getMessage(), warningType.getDetail());
                            }
                        }
                    }
                }

                orderCancelSuccessMarketOrderItemNoList.add(marketOrderItemNo);
            } else {
                orderCancelFailMarketOrderItemNoList.add(marketOrderItemNo);
            }
        }

        return orderCancelResponse;
    }

    private SellerServiceStub.AccessCredentialsType getAccessCredentialType(String serviceName , String operationName) {

        //서명생성
        String timeStamp = SimpleCryptLib.getTimestamp();
        String data      = timeStamp + serviceName + operationName;
        String signature ;

        try {

            signature = SimpleCryptLib.generateSign(data, marketNaverConfig.getSecretKey());

            //인증정보
            SellerServiceStub.AccessCredentialsType accessCredentialsType = new com.nhncorp.platform.shopn.seller.SellerServiceStub.AccessCredentialsType();
            accessCredentialsType.setAccessLicense(marketNaverConfig.getAccessLicense());
            accessCredentialsType.setSignature(signature);
            accessCredentialsType.setTimestamp(timeStamp);
            this.timeStamp  = timeStamp;
            this.encryptKey = null;
            return accessCredentialsType;

        } catch (SignatureException e) {
            //서명정보 실패
            log.error(e.getMessage());
            // :todo: 에러처리
            return null;
        }
    }

    private void generateEncryptKey() throws SignatureException {
        log.info("before generate encryptKey - encryptKey: {}, timeStamp: {}", this.encryptKey, timeStamp);
        this.encryptKey =  SimpleCryptLib.generateKey(timeStamp, marketNaverConfig.getSecretKey());
        log.info("after generate encryptKey - encryptKey: {}, timeStamp: {}", new String(this.encryptKey), timeStamp);
    }

    private String decrypt(String encryptedData) {
        String decryptedStr = "";

        if (encryptedData == null || "".equals(encryptedData)) {
            return "";
        }

        try {
            decryptedStr = new String(SimpleCryptLib.decrypt(encryptKey, encryptedData), "UTF-8");
        } catch (Exception e) {
            log.info("decrypt fail! encryptedData: {}, timeStamp: {}", encryptedData, timeStamp);
            decryptedStr = encryptedData;
        }

        return decryptedStr;
    }

    private SellerServiceStub.ProductOrderChangeType getProductOrderChangeType(String lastChangedStatusCode) {
        SellerServiceStub.ProductOrderChangeType productOrderChangeType = SellerServiceStub.ProductOrderChangeType.PAYED;

        switch (lastChangedStatusCode) {
            case "PAY_WAITING":
                productOrderChangeType = SellerServiceStub.ProductOrderChangeType.PAY_WAITING;
                break;
            case "PAYED":
                productOrderChangeType = SellerServiceStub.ProductOrderChangeType.PAYED;
                break;
            case "DISPATCHED":
                productOrderChangeType = SellerServiceStub.ProductOrderChangeType.DISPATCHED;
                break;
            case "CANCEL_REQUESTED":
                productOrderChangeType = SellerServiceStub.ProductOrderChangeType.CANCEL_REQUESTED;
                break;
            case "RETURN_REQUESTED":
                productOrderChangeType = SellerServiceStub.ProductOrderChangeType.RETURN_REQUESTED;
                break;
            case "EXCHANGE_REQUESTED":
                productOrderChangeType = SellerServiceStub.ProductOrderChangeType.EXCHANGE_REQUESTED;
                break;
            case "EXCHANGE_REDELIVERY_READY":
                productOrderChangeType = SellerServiceStub.ProductOrderChangeType.EXCHANGE_REDELIVERY_READY;
                break;
            case "HOLDBACK_REQUESTED":
                productOrderChangeType = SellerServiceStub.ProductOrderChangeType.HOLDBACK_REQUESTED;
                break;
            case "CANCELED":
                productOrderChangeType = SellerServiceStub.ProductOrderChangeType.CANCELED;
                break;
            case "RETURNED":
                productOrderChangeType = SellerServiceStub.ProductOrderChangeType.RETURNED;
                break;
            case "EXCHANGED":
                productOrderChangeType = SellerServiceStub.ProductOrderChangeType.EXCHANGED;
                break;
            case "PURCHASE_DECIDED":
                productOrderChangeType = SellerServiceStub.ProductOrderChangeType.PURCHASE_DECIDED;
                break;
        }

        return productOrderChangeType;
    }

    private Calendar getCalendar(String date, String dateType) {

        Calendar calendar = Calendar.getInstance();

        int year = Integer.valueOf(date.substring(0,4));
        int month = Integer.valueOf(date.substring(4,6));
        int day = Integer.valueOf(date.substring(6,8));
        int hour = Integer.valueOf(date.substring(8,10));
        int minute = Integer.valueOf(date.substring(10,12));
        int second = "from".equalsIgnoreCase(dateType) ? 0 : 59;
        calendar.set(year, month-1, day, hour, minute, second);

        return calendar;
    }

    private String convertCalendarToDate(Calendar calendar) {
        if (calendar == null) {
            return null;
        }

        return DEFAULT_YMD_HIS_DATEFORMAT.format(calendar.getTime());
    }

    private Order mapOrder(SellerServiceStub.Order naverOrder) {
        Order order = new Order();
        order.setOrderID(naverOrder.getOrderID());
        order.setOrderDate(convertCalendarToDate(naverOrder.getOrderDate()));
        order.setOrdererID(decrypt(naverOrder.getOrdererID()));
        order.setOrdererName(decrypt(naverOrder.getOrdererName()));
        order.setOrdererTel1(decrypt(naverOrder.getOrdererTel1()));
        order.setOrdererTel2(decrypt(naverOrder.getOrdererTel2()));
        order.setPaymentDate(convertCalendarToDate(naverOrder.getPaymentDate()));
        order.setPaymentMeans(naverOrder.getPaymentMeans());
        order.setPaymentDueDate(convertCalendarToDate(naverOrder.getPaymentDueDate()));
        order.setOrderDiscountAmount(naverOrder.getOrderDiscountAmount());
        order.setGeneralPaymentAmount(naverOrder.getGeneralPaymentAmount());
        order.setNaverMileagePaymentAmount(naverOrder.getNaverMileagePaymentAmount());
        order.setChargeAmountPaymentAmount(naverOrder.getChargeAmountPaymentAmount());
        order.setCheckoutAccumlationPaymentAmount(naverOrder.getCheckoutAccumulationPaymentAmount());
        order.setPayLaterPaymentAmount(naverOrder.getPayLaterPaymentAmount());
        order.setIsDeliveryMemoParticularInput(naverOrder.getIsDeliveryMemoParticularInput());
        order.setPayLocationType(naverOrder.getPayLocationType());
        order.setOrdererNo(naverOrder.getOrdererNO());
        return order;
    }

    private ProductOrder mapProductOrder(SellerServiceStub.ProductOrder naverProductOrder) {
        ProductOrder productOrder = new ProductOrder();
        productOrder.setProductOrderID(naverProductOrder.getProductOrderID());
        productOrder.setProductOrderStatus(naverProductOrder.getProductOrderStatus() == null ? null : naverProductOrder.getProductOrderStatus().getValue());
        productOrder.setClaimType(naverProductOrder.getClaimType() == null ? null : naverProductOrder.getClaimType().getValue());
        productOrder.setClaimStatus(naverProductOrder.getClaimStatus() == null ? null : naverProductOrder.getClaimStatus().getValue());
        productOrder.setProductID(naverProductOrder.getProductID());
        productOrder.setProductName(naverProductOrder.getProductName());
        productOrder.setProductClass(naverProductOrder.getProductClass());
        productOrder.setProductOption(naverProductOrder.getProductOption());
        productOrder.setQuantity(naverProductOrder.getQuantity());
        productOrder.setUnitPrice(naverProductOrder.getUnitPrice());
        productOrder.setOptionPrice(naverProductOrder.getOptionPrice());
        productOrder.setOptionCode(naverProductOrder.getOptionCode());
        productOrder.setTotalProductAmount(naverProductOrder.getTotalProductAmount());
        productOrder.setProductDiscountAmount(naverProductOrder.getProductDiscountAmount());
        productOrder.setProductImediateDiscountAmount(naverProductOrder.getProductImediateDiscountAmount());
        productOrder.setProductProductDiscountAmount(naverProductOrder.getProductProductDiscountAmount());
        productOrder.setProductMultiplePurchaseDiscountAmount(naverProductOrder.getProductMultiplePurchaseDiscountAmount());
        productOrder.setTotalPaymentAmount(naverProductOrder.getTotalPaymentAmount());
        productOrder.setSellerProductCode(naverProductOrder.getSellerProductCode());
        productOrder.setMallID(naverProductOrder.getMallID());
        productOrder.setExpectedDeliveryMethod(naverProductOrder.getExpectedDeliveryMethod() == null ? null : naverProductOrder.getExpectedDeliveryMethod().getValue());
        productOrder.setPackageNumber(naverProductOrder.getPackageNumber());
        productOrder.setShippingFeeType(naverProductOrder.getShippingFeeType());
        productOrder.setDeliveryPolicyType(naverProductOrder.getDeliveryPolicyType());
        productOrder.setDeliveryFeeAmount(naverProductOrder.getDeliveryFeeAmount());
        productOrder.setSectionDeliveryFee(naverProductOrder.getSectionDeliveryFee());
        productOrder.setDeliveryDiscountAmount(naverProductOrder.getDeliveryDiscountAmount());
        productOrder.setShippingMemo(naverProductOrder.getShippingMemo());
        productOrder.setShippingDueDate(convertCalendarToDate(naverProductOrder.getShippingDueDate()));
        productOrder.setDecisionDate(convertCalendarToDate(naverProductOrder.getDecisionDate()));
        productOrder.setFreeGift(naverProductOrder.getFreeGift());
        productOrder.setPlaceOrderStatus(naverProductOrder.getPlaceOrderStatus() == null ? null : naverProductOrder.getPlaceOrderStatus().getValue());
        productOrder.setPlaceOrderDate(convertCalendarToDate(naverProductOrder.getPlaceOrderDate()));
        productOrder.setDelayedDispatchReason(naverProductOrder.getDelayedDispatchDetailedReason());
        productOrder.setDelayedDispatchDetailedReason(naverProductOrder.getDelayedDispatchDetailedReason());
        productOrder.setSellerBurdenDiscountAmount(naverProductOrder.getSellerBurdenDiscountAmount());
        productOrder.setSellerBurdenImediateDiscountAmount(naverProductOrder.getSellerBurdenImediateDiscountAmount());
        productOrder.setSellerBurdenProductDiscountAmount(naverProductOrder.getSellerBurdenProductDiscountAmount());
        productOrder.setSellerBurdenMultiplePurchaseDiscountAmount(naverProductOrder.getSellerBurdenMultiplePurchaseDiscountAmount());
        productOrder.setCommissionRatingType(naverProductOrder.getCommissionRatingType());
        productOrder.setComissionPrePayStatus(naverProductOrder.getCommissionPrePayStatus() == null ? null : naverProductOrder.getCommissionPrePayStatus().getValue());
        productOrder.setPaymentCommission(naverProductOrder.getPaymentCommission());
        productOrder.setSaleCommission(naverProductOrder.getSaleCommission());
        productOrder.setChannelCommission(naverProductOrder.getChannelCommission());
        productOrder.setKnowledgeShoppingSellingInterlockCommission(naverProductOrder.getKnowledgeShoppingSellingInterlockCommission());
        productOrder.setExpectedSettlementAmount(naverProductOrder.getExpectedSettlementAmount());
        productOrder.setInflowPath(naverProductOrder.getInflowPath());
        productOrder.setItemNo(naverProductOrder.getItemNo());
        productOrder.setOptionManageCode(naverProductOrder.getOptionManageCode());
        productOrder.setPurchaserSocialSecurityNo(naverProductOrder.getPurchaserSocialSecurityNo());
        productOrder.setSellerCustomCode1(naverProductOrder.getSellerCustomCode1());
        productOrder.setSellerCustomCode2(naverProductOrder.getSellerCustomCode2());
        productOrder.setClaimID(naverProductOrder.getClaimID());
        productOrder.setIndividualCustomUniqueCode(naverProductOrder.getIndividualCustomUniqueCode());
        productOrder.setGiftReceivingStatus(naverProductOrder.getGiftReceivingStatus() == null ? null : naverProductOrder.getGiftReceivingStatus().getValue());
        productOrder.setSellerBurdenStoreDiscountAmount(naverProductOrder.getSellerBurdenStoreDiscountAmount());
        productOrder.setSellerBurdenMultiplePurchaseDiscountType(naverProductOrder.getSellerBurdenMultiplePurchaseDiscountType() == null ? null : naverProductOrder.getSellerBurdenMultiplePurchaseDiscountType().getValue());
        productOrder.setBranchID(naverProductOrder.getBranchID());
        productOrder.setAcceptAlternativeProduct(naverProductOrder.getIsAcceptAlternativeProduct());
        productOrder.setDeliveryAttributeType(naverProductOrder.getDeliveryAttributeType() == null ? null : naverProductOrder.getDeliveryAttributeType().getValue());
        productOrder.setDeliveryOperationDate(naverProductOrder.getDeliveryOperationDate());
        productOrder.setDeliveryStartTime(naverProductOrder.getDeliveryStartTime());
        productOrder.setDeliveryEndTime(naverProductOrder.getDeliveryEndTime());
        productOrder.setSlotID(naverProductOrder.getSlotID());
        productOrder.setBranchBenefitType(naverProductOrder.getBranchBenefitType() == null ? null : naverProductOrder.getBranchBenefitType().getValue());
        productOrder.setBranchBenefitValue(naverProductOrder.getBranchBenefitValue());

        return productOrder;
    }

    private Address mapAddress(SellerServiceStub.Address naverAddress) {
        Address address = new Address();
        address.setAddressType(naverAddress.getAddressType() == null ? null : naverAddress.getAddressType().getValue());
        address.setZipCode(naverAddress.getZipCode());
        address.setBaseAddress(decrypt(naverAddress.getBaseAddress()));
        address.setDetailedAddress(decrypt(naverAddress.getDetailedAddress()));
        address.setRoadNameAddress(naverAddress.getIsRoadNameAddress());
        address.setCity(naverAddress.getCity());
        address.setState(naverAddress.getState());
        address.setCountry(naverAddress.getCountry());
        address.setTel1(decrypt(naverAddress.getTel1()));
        address.setTel2(decrypt(naverAddress.getTel2()));
        address.setName(decrypt(naverAddress.getName()));
        address.setPickupLocationType(naverAddress.getPickupLocationType() == null ? null : naverAddress.getPickupLocationType().getValue());
        address.setPickupLocationContent(decrypt(naverAddress.getPickupLocationContent()));
        address.setEntryMethod(naverAddress.getEntryMethod() == null ? null : naverAddress.getEntryMethod().getValue());
        address.setEntryMethodContent(decrypt(naverAddress.getEntryMethodContent()));

        return address;
    }
}
