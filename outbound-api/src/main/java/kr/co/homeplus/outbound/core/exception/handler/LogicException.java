package kr.co.homeplus.outbound.core.exception.handler;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

@Data
@EqualsAndHashCode(callSuper = false)
public class LogicException extends Exception {

    private String responseCode;
    private String responseMsg;
    private Object data;

    public LogicException(String responseCode, String responseMsg) {
        this.responseCode = responseCode;
        this.responseMsg = responseMsg;
        this.data = null;
    }

    public LogicException(String responseCode, String responseMsg, Object data) {
        this.responseCode = responseCode;
        this.responseMsg = responseMsg;
        this.data = data;
    }
}
