package kr.co.homeplus.outbound.item.mapper;


import java.util.List;
import java.util.Map;
import kr.co.homeplus.outbound.core.db.annotation.SlaveConnection;
import kr.co.homeplus.outbound.item.model.ItemMarketGetDto;
import kr.co.homeplus.outbound.item.model.ItemMarketImgSendGetDto;
import kr.co.homeplus.outbound.item.model.ItemMarketListParamDto;
import kr.co.homeplus.outbound.item.model.MarketItemGetDto;
import kr.co.homeplus.outbound.item.model.naver.StoStoreInfo;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

@SlaveConnection
public interface ItemSlaveMapper {

    /**
     * 대표지점 상품 조회
     * @param itemList
     * @param storeId
     * @param partnerId
     * @return
     */
    @MapKey("basic.itemNo")
    Map<String, MarketItemGetDto> getItemMaster(@Param("itemList") List<String> itemList
        , @Param("storeId") int storeId
        , @Param("clubStoreId") int clubStoreId
        , @Param("partnerId") String partnerId);


    /**
     * 마켓 연동 전송 대상 조회
     * @param itemMarketListParamDto
     * @return
     */
    List<ItemMarketGetDto> getItemMarketList(ItemMarketListParamDto itemMarketListParamDto);

    /**
     * 마켓상품 조회
     * @param partnerId
     * @param itemNo
     * @return
     */
    ItemMarketGetDto getItemMarket (@Param("partnerId") String partnerId,
                                    @Param("itemNo") String itemNo);
    /**
     * 상품 연동 이미지 조회
     * @param itemNo
     * @param partnerId
     *
     * @return
     */
    ItemMarketImgSendGetDto selectItemMarketImageSend (@Param("itemNo") String itemNo,
                                                @Param("partnerId") String partnerId);

    /**
     * 점포리스트 조회
     * @return
     */
    List<StoStoreInfo> getStoreList();

    /**
     * 마트전단 전시 상품 조회
     * @return
     */
    List<String> getDispLeafletList();
}
