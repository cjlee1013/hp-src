package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("상품 이미지정보")
public class MarketItemImgGetDto {

	@ApiModelProperty(value = "이미지경로")
	private String imgUrl;

	@ApiModelProperty(value = "메인여부")
	private String mainYn;

	@ApiModelProperty(value = "이미지 유형 (MN: 메인, LST: 리스트, LBL: 라벨)")
	private String imgType;

}
