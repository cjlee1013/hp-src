package kr.co.homeplus.outbound.market.eleven.inquiry.enums;


import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 게시물 대분류
 */

public enum EmerNtceCdType {
    SELL( "01", "판매" ),
    SAFE_DEAL( "02", "안전거래" ),
    EXHIBITIONS_NOTICE( "03", "기획전공지" ),
    PROMOTION_EXTEND( "04", "프로모션 연장" ),
    INVOICE_NUMBER_ERROR( "05", "송장번호 오류" );

    private String code;
    private String desc;

    private EmerNtceCdType( String code, String desc ) {
        this.setDesc( desc );
        this.setCode( code );
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode( String code ) {
        this.code = code;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc
     *            the desc to set
     */
    public void setDesc( String desc ) {
        this.desc = desc;
    }

    public static EmerNtceCdType getTypeOf( String dtlsCd ) {
        for ( EmerNtceCdType ntceCd : EmerNtceCdType.values() ) {
            if ( ntceCd.getCode().equals( dtlsCd ) ) {
                return ntceCd;
            }
        }
        return SELL;
    }

}
