
package kr.co.homeplus.outbound.cert.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 공공API URL 정의
 */
@AllArgsConstructor
public enum CertUrl {

	    RADIO_CERT_INFO("/openapi/service/AuthenticationInfoService/getAuthStatus.do?mtlCefNo=", "전파적합성인증")
	,	SAFE_CERT_INFO ("/openapi/api/cert/certificationDetail.json", "KC인증")
	,	LIFE_CERT_INFO("/openapi/ServiceSvl?ServiceName=slfsfcfstChk", "생활환경안전인증")
	,	EMPTY("", "")

	;

	@Getter
	private final String url;
	@Getter
	private final String desc;
}
