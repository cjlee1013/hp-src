package kr.co.homeplus.outbound.store.model.eleven;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@ApiModel(value = "11번가 지점등록/수정 결과")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlRootElement( name = "ResultMartStore" )
public class MartStoreResultDto {

    @XmlElement( name = "result_message" )
    private String resultMessage;

    @XmlElement( name = "result_code" )
    private String resultCode;

    @ApiModelProperty(value = "등록된지점 번호")
    @XmlElement( name = "strNo" )
    private String strNo;

}

