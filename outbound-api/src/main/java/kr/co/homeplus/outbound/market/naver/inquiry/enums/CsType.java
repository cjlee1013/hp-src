package kr.co.homeplus.outbound.market.naver.inquiry.enums;

public enum CsType {
	QNA("B", "판매자 문의"),
	NOTE("N", "판매자 문의 쪽지"),
	EMERGENCY("E", "긴급 메시지");
	
	private String desc;
	private String code;

	public String getDesc(){
		return desc;
	}

	public String getCode(){
		return code;
	}

	CsType(String  code, String desc){
		this.desc = desc;
		this.code = code;
	}
}
