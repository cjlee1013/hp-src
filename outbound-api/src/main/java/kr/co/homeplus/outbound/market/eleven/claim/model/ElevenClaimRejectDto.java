package kr.co.homeplus.outbound.market.eleven.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "11번가 클레임 거부 처리 DTO")
public class ElevenClaimRejectDto {
    @ApiModelProperty(value = "주문번호(공통)")
    private String ordNo;
    @ApiModelProperty(value = "주문순번(공통)")
    private String ordPrdSeq;
    @ApiModelProperty(value = "취소클레임번호(취소)")
    private String ordPrdCnSeq;
    @ApiModelProperty(value = "배송방식(취소)")
    private String dlvMthdCd;
    @ApiModelProperty(value = "보낸일자[YYYYMMDD](취소)")
    private String sendDt;
    @ApiModelProperty(value = "택배사코드(취소)")
    private String dlvEtprsCd;
    @ApiModelProperty(value = "송장번호(취소)")
    private String invcNo;
    @ApiModelProperty(value = "클레임번호(반품,교환)")
    private String clmReqSeq;
    @ApiModelProperty(value = "사유코드(반품,교환)")
    private String refsRsnCd;
    @ApiModelProperty(value = "사유(반품,교환)")
    private String refsRsn;
}
