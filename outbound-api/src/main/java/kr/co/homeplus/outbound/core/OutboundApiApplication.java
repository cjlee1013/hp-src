package kr.co.homeplus.outbound.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"kr.co.homeplus.outbound", "kr.co.homeplus.plus"})
public class OutboundApiApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(OutboundApiApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(OutboundApiApplication.class);
    }
}
