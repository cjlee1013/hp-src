package kr.co.homeplus.outbound.market.eleven.inquiry.model;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@XmlAccessorType( XmlAccessType.FIELD )
@XmlRootElement( name = "productQnas", namespace = "http://skt.tmall.business.openapi.spring.service.client.domain/" )
public class ElevenInquiryRestResult {

    @XmlElement( name = "productQna", namespace = "http://skt.tmall.business.openapi.spring.service.client.domain/" )
    private List<ElevenInquiryResultDto> productQnAList;

    @XmlElement( name = "result_code", namespace = "http://skt.tmall.business.openapi.spring.service.client.domain/" )
    private String resultCode;

    @XmlElement( name = "result_message", namespace = "http://skt.tmall.business.openapi.spring.service.client.domain/" )
    private String resultMessage;

    /**
     * @return the productQnAList
     */
    public List< ElevenInquiryResultDto > getProductQnAListRestVO() {
        return productQnAList;
    }

    /**
     * @param productQnAList
     *            the productQnAList to set
     */
    public void setProductQnAListRestVO(
        List< ElevenInquiryResultDto > productQnAList ) {
        this.productQnAList = productQnAList;
    }

    /**
     * @return the resultCode
     */
    public String getResultCode() {
        return resultCode;
    }

    /**
     * @param resultCode
     *            the resultCode to set
     */
    public void setResultCode( String resultCode ) {
        this.resultCode = resultCode;
    }

    /**
     * @return the resultMessage
     */
    public String getResultMessage() {
        return resultMessage;
    }

    /**
     * @param resultMessage
     *            the resultMessage to set
     */
    public void setResultMessage( String resultMessage ) {
        this.resultMessage = resultMessage;
    }
}
