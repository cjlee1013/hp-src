package kr.co.homeplus.outbound.ticket.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import kr.co.homeplus.outbound.core.constants.UriConstants;
import kr.co.homeplus.outbound.core.utility.SetParameter;
import kr.co.homeplus.outbound.core.utility.StringUtil;
import kr.co.homeplus.outbound.escrow.util.EscrowParamKey;
import kr.co.homeplus.outbound.ticket.model.TicketIssueInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CoopApiConnectService {

    @Value("${plus.resource-routes.coop.url}")
    private String COOP_URL;
    @Value("${plus.resource-routes.coop-encrypt.url}")
    private String COOP_ENCRYPT_URL;
    @Value("${enc-key.coop.key}")
    private String COOP_KEY_CODE;
    @Value("${enc-key.coop.iv}")
    private String COOP_IV_PASS;

    /**
     * [coop] 티켓 발급 - 단일발급 (유효기간종료일 회신)
     */
    public String createTicket(TicketIssueInfo ticketIssueInfo) throws Exception {
        log.info("[coop][orderTicketNo_{}] [coop] 티켓 발급 - 단일발급 (유효기간종료일 회신)", ticketIssueInfo.getOrderTicketNo());

        // prepare set parameter
        List<SetParameter> setParameterList = Arrays.asList(
            SetParameter.create(EscrowParamKey.TICKET_CODE, COOP_KEY_CODE),
            SetParameter.create(EscrowParamKey.TICKET_PASS, COOP_IV_PASS),
            SetParameter.create(EscrowParamKey.TICKET_COUPON_CODE, ticketIssueInfo.getSellerItemCd()),
            SetParameter.create(EscrowParamKey.TICKET_SEQ_NUMBER, ticketIssueInfo.getOrderTicketNo()),
            SetParameter.create(EscrowParamKey.TICKET_HP, ticketIssueInfo.getShipMobileNo()),
            SetParameter.create(EscrowParamKey.TICKET_CALLBACK, ticketIssueInfo.getShipMobileNo()),
            SetParameter.create(EscrowParamKey.TICKET_TITLE, ""),
            SetParameter.create(EscrowParamKey.TICKET_ADDMSG, "")
        );

        // connection
        URL url = new URL(COOP_URL + UriConstants.COOP_CREATE_TICKET + StringUtil.getParameter(setParameterList));
        log.info("[coop][orderTicketNo_{}] 티켓 발급 - 요청 :: {}", ticketIssueInfo.getOrderTicketNo(), url.toString());

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(HttpMethod.GET.name());

        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (Exception e) {
            log.error("[coop][orderTicketNo_{}] 티켓 발급 - connect fail :: {}", ticketIssueInfo.getOrderTicketNo(), ExceptionUtils.getStackTrace(e));
        }

        log.info("[coop][orderTicketNo_{}] 티켓 발급 - 응답 :: {}", ticketIssueInfo.getOrderTicketNo(), stringBuilder.toString());
        return stringBuilder.toString();
    }

    /**
     * [coop] 티켓 재발송 - 거래번호 기준 (비암호화)
     */
    public String sendTicket(TicketIssueInfo ticketIssueInfo) throws Exception {
        log.info("[coop][orderTicketNo_{}] [coop] 티켓 재발송 - 거래번호 기준 (비암호화)", ticketIssueInfo.getOrderTicketNo());

        // prepare set parameter
        List<SetParameter> setParameterList = Arrays.asList(
            SetParameter.create(EscrowParamKey.TICKET_CODE, COOP_KEY_CODE),
            SetParameter.create(EscrowParamKey.TICKET_PASS, COOP_IV_PASS),
            SetParameter.create(EscrowParamKey.TICKET_COUPON_CODE, ticketIssueInfo.getSellerItemCd()),
            SetParameter.create(EscrowParamKey.TICKET_SEQ_NUMBER, ticketIssueInfo.getOrderTicketNo()),
            SetParameter.create(EscrowParamKey.TICKET_HP, ticketIssueInfo.getShipMobileNo()),
            SetParameter.create(EscrowParamKey.TICKET_CALLBACK, ticketIssueInfo.getShipMobileNo()),
            SetParameter.create(EscrowParamKey.TICKET_TITLE, ""),
            SetParameter.create(EscrowParamKey.TICKET_ADDMSG, "")
        );

        // connection
        URL url = new URL(COOP_URL + UriConstants.COOP_SEND_TICKET + StringUtil.getParameter(setParameterList));
        log.info("[coop][orderTicketNo_{}] 티켓 발송 - 요청 :: {}", ticketIssueInfo.getOrderTicketNo(), url.toString());

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(HttpMethod.GET.name());

        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (Exception e) {
            log.error("[coop][orderTicketNo_{}] 티켓 발송 - connect fail :: {}", ticketIssueInfo.getOrderTicketNo(), ExceptionUtils.getStackTrace(e));
        }

        log.info("[coop][orderTicketNo_{}] 티켓 발송 - 응답 :: {}", ticketIssueInfo.getOrderTicketNo(), stringBuilder.toString());
        return stringBuilder.toString();
    }

    /**
     * [coop] 티켓 재발송 - 거래번호 기준 (암호화)
     */
//    public String sendTicketEncrypt(TicketIssueInfo ticketIssueInfo) throws Exception {
//        StringEncrypter encrypter = new StringEncrypter(COOP_KEY_CODE, COOP_IV_PASS);
//
//        // prepare set parameter
//        List<SetParameter> setParameterList = Arrays.asList(
//            SetParameter.create(EscrowParamKey.TICKET_CODE, COOP_KEY_CODE),
//            SetParameter.create(EscrowParamKey.TICKET_PASS, COOP_IV_PASS),
//            SetParameter.create(EscrowParamKey.TICKET_COUPON_CODE, ticketIssueInfo.getSellerItemCd()),
//            SetParameter.create(EscrowParamKey.TICKET_SEQ_NUMBER, encrypter.encrypt(ticketIssueInfo.getOrderTicketNo()+"")),
//            SetParameter.create(EscrowParamKey.TICKET_HP, encrypter.encrypt(ticketIssueInfo.getShipMobileNo())),
//            SetParameter.create(EscrowParamKey.TICKET_CALLBACK, encrypter.encrypt(ticketIssueInfo.getShipMobileNo())),
//            SetParameter.create(EscrowParamKey.TICKET_TITLE, encrypter.encrypt("")),
//            SetParameter.create(EscrowParamKey.TICKET_ADDMSG, encrypter.encrypt(""))
//        );
//
//        // connection
//        URL url = new URL(COOP_ENCRYPT_URL + UriConstants.COOP_SEND_TICKET + StringUtil.getParameter(setParameterList));
//        log.info("[coop][orderTicketNo_{}] 티켓 발송 (암호화) - 요청 :: {}", ticketIssueInfo.getOrderTicketNo(), url.toString());
//
//        HttpURLConnection con = (HttpURLConnection) url.openConnection();
//        con.setRequestMethod(HttpMethod.GET.name());
//
//        BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
//        String inputLine;
//        StringBuffer stringBuffer = new StringBuffer();
//
//        while ((inputLine = reader.readLine()) != null) {
//            stringBuffer.append(inputLine);
//        }
//        reader.close();
//
//        log.info("[coop][orderTicketNo_{}] 티켓 발송 (암호화) - 응답 :: {}", ticketIssueInfo.getOrderTicketNo(), stringBuffer.toString());
//        return stringBuffer.toString();
//    }

    /**
     * [coop] 티켓 발행 정보 조회 (거래번호 기준 - 기본)
     */
    public String getIssuedTicketInfo(TicketIssueInfo ticketIssueInfo) throws Exception {
        log.info("[coop][orderTicketNo_{}] [coop] 티켓 발행 정보 조회 (거래번호 기준 - 기본) (비암호화)", ticketIssueInfo.getOrderTicketNo());

        // prepare set parameter
        List<SetParameter> setParameterList = Arrays.asList(
            SetParameter.create(EscrowParamKey.TICKET_CODE, COOP_KEY_CODE),
            SetParameter.create(EscrowParamKey.TICKET_PASS, COOP_IV_PASS),
            SetParameter.create(EscrowParamKey.TICKET_COUPON_CODE, ticketIssueInfo.getSellerItemCd()),
            SetParameter.create(EscrowParamKey.TICKET_SEQ_NUMBER, ticketIssueInfo.getOrderTicketNo())
        );

        // connection
        URL url = new URL(COOP_URL + UriConstants.COOP_GET_TICKET_INFO + StringUtil.getParameter(setParameterList));
        log.info("[coop][orderTicketNo_{}] 티켓 발행 정보 조회 (거래번호 기준 - 기본) - 요청 :: {}", ticketIssueInfo.getOrderTicketNo(), url.toString());

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(HttpMethod.GET.name());

        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (Exception e) {
            log.error("[coop][orderTicketNo_{}] 티켓 발행 정보 조회 (거래번호 기준 - 기본) - connect fail :: {}", ticketIssueInfo.getOrderTicketNo(), ExceptionUtils.getStackTrace(e));
        }

        log.info("[coop][orderTicketNo_{}] 티켓 발행 정보 조회 (거래번호 기준 - 기본) - 응답 :: {}", ticketIssueInfo.getOrderTicketNo(), stringBuilder.toString());
        return stringBuilder.toString();
    }
}
