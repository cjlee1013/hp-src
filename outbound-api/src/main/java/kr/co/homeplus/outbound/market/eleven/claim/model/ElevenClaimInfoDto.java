package kr.co.homeplus.outbound.market.eleven.claim.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class ElevenClaimInfoDto<T> {
    private List<T> order;
    private String result_code;
    private String result_text;

    public void setOrder(T order) {
        if(this.order == null){
            this.order = new ArrayList<>();
            this.order.add(order);
        } else {
            this.order.add(order);
        }
    }
}
