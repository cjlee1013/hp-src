package kr.co.homeplus.outbound.escrow.util;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EscrowParamKey {
    // E티켓 발급&발송에 필요한 파라미터
    public static final String TICKET_CODE = "CODE";                    // Client 코드
    public static final String TICKET_PASS = "PASS";                    // 연동 Password
    public static final String TICKET_COUPON_CODE = "COUPONCODE";       // 상품코드
    public static final String TICKET_COUPON_NUMBER = "COUPONNUMBER";   // 티켓쿠폰번호
    public static final String TICKET_SEQ_NUMBER = "SEQNUMBER";         // 거래번호
    public static final String TICKET_HP = "HP";                        // 고객전화번호
    public static final String TICKET_CALLBACK = "CALLBACK";            // 발송자전화번호
    public static final String TICKET_TITLE = "TITLE";                  // MMS 제목 (최대 40Byte) - optional
    public static final String TICKET_ADDMSG = "ADDMSG";                // 추가메세지(메시지상단) - optional

    public static final String TICKET_CLAIM_COUPON_NUMBER = "COUPONNUM";   // 티켓쿠폰번호(클레임시 - 입력파라미터, 응답파라미터와 상이하기에 별도로 설정)
}
