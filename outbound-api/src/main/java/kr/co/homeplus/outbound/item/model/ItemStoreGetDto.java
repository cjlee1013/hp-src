package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("홈플러스 점포 조회")
public class ItemStoreGetDto {

    @ApiModelProperty(value = "점포코드" )
    private String storeId;

    @ApiModelProperty(value = "점포명")
    private String storeNm;
}
