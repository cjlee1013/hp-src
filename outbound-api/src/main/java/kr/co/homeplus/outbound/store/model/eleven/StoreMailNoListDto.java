package kr.co.homeplus.outbound.store.model.eleven;


import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "MartStoreList")
@Getter
@Setter
public class StoreMailNoListDto {

	/**
	 * 지점 우편번호 리스트
	 */
	@XmlElement(name = "store")
	private List<StoreMailNoRestVO> StoreMailNoRestVOList;
}
