package kr.co.homeplus.outbound.category.model.sk11st;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@XmlAccessorType( XmlAccessType.FIELD )
@XmlRootElement( name = "categorys", namespace = "http://skt.tmall.business.openapi.spring.service.client.domain/" )
public class CategoryListGetDto {

    @XmlElement( name = "category", namespace = "http://skt.tmall.business.openapi.spring.service.client.domain/" )
    private List< CategoryGetDto > categoryList;

}
