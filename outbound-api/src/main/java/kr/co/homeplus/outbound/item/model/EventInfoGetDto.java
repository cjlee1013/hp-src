package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "RMS 행사 대상 상품 정보 팝업에서 선택해서 올라온 상품의 행사 정보")
public class EventInfoGetDto {

    @ApiModelProperty(value = "RMS 행사번호")
    private Long rpmPromoCompDetailId;

    @ApiModelProperty(value= "행사 유형(SIMP:simple, THRES:Threshold, MIX:Mixmatch)")
    private String promoType;

    @ApiModelProperty(value = "행사종류 (BASIC:기본할인, PICK:골라담기, TOGETHER:함께할인, INTERVAL:구간할인)")
    private String eventKind;

    @ApiModelProperty(value = "행사시작일")
    private String startDate;

    @ApiModelProperty(value = "행사종료일")
    private String endDate;

    @ApiModelProperty(value = "할인유형 (0:할인율, 1:할인금액, 2:고정금액)")
    private String changeType;

    @ApiModelProperty(value = "mix&match 적용 상품 동종여부 (Y:동종, N:이종)")
    private String sameKindYn;

    @ApiModelProperty(value = "할인율")
    private Integer changePercent;

    @ApiModelProperty(value = "할인액(고정금액)")
    private Integer changeAmount;

    @ApiModelProperty(value = "MixMatch 할인 기준 유형 (1:구매수량, 2:구매금액)")
    private String buyItemType;

    @ApiModelProperty(value = "MixMatch 할인 기준 값 (구매수량 or 구매금액)")
    private Integer buyItemValue;

    @ApiModelProperty(value = "MixMatch 구매 대상 상품 개수")
    private Integer buyItemCnt;

    @ApiModelProperty(value = "buy 상품번호")
    private String buyItemNo;

    @ApiModelProperty(value ="get 상품번호")
    private String getItemNo;

    @ApiModelProperty(value = "CLUB RMS 행사 동일여부")
    private String clubPromoYn;

    @ApiModelProperty(value = "RMS 행사번호")
    private Long clubRpmPromoCompDetailId;

    @ApiModelProperty(value= "행사 유형(SIMP:simple, THRES:Threshold, MIX:Mixmatch)")
    private String clubPromoType;

    @ApiModelProperty(value = "행사종류 (BASIC:기본할인, PICK:골라담기, TOGETHER:함께할인, INTERVAL:구간할인)")
    private String clubEventKind;

    @ApiModelProperty(value = "행사시작일")
    private String clubStartDate;

    @ApiModelProperty(value = "행사종료일")
    private String clubEndDate;

    @ApiModelProperty(value = "할인유형 (0:할인율, 1:할인금액, 2:고정금액)")
    private String clubChangeType;

    @ApiModelProperty(value = "mix&match 적용 상품 동종여부 (Y:동종, N:이종)")
    private String clubSameKindYn;

    @ApiModelProperty(value = "할인율")
    private Integer clubChangePercent;

    @ApiModelProperty(value = "할인액(고정금액)")
    private Integer clubChangeAmount;

    @ApiModelProperty(value = "MixMatch 할인 기준 유형 (1:구매수량, 2:구매금액)")
    private Integer clubBuyItemType;

    @ApiModelProperty(value = "MixMatch 할인 기준 값 (구매수량 or 구매금액)")
    private Integer clubBuyItemValue;

    @ApiModelProperty(value = "MixMatch 구매 대상 상품 개수")
    private Integer clubBuyItemCnt;

    @ApiModelProperty(value = "buy 상품번호")
    private String clubBuyItemNo;

    @ApiModelProperty(value ="get 상품번호")
    private String clubGetItemNo;

}