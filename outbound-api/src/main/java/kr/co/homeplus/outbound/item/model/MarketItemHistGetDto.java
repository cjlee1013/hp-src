package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 상품 상세정보 Get Entry
 */
@Getter
@Setter
@ToString

@Document(collection = "market_item_hist")
@ApiModel("상품 연동 히스토리")
public class MarketItemHistGetDto {

	@Id
	private String id;
	private Date expireAt;

	@ApiModelProperty(value = "히스토리 등록일", hidden = true)
	private String marketItemHistDt;

	@Indexed
	@ApiModelProperty("상품번호")
	private String itemNo;

	@ApiModelProperty("마켓연동 아이디")
	private String partnerId;

	@ApiModelProperty(value = "dto")
	private MarketItemGetDto info;

}