package kr.co.homeplus.outbound.category.service.naver;

import com.nhncorp.platform.shopn.ProductServiceStub;
import java.rmi.RemoteException;
import java.util.Random;
import java.util.UUID;
import kr.co.homeplus.outbound.common.enums.NaverMartProfile;
import kr.co.homeplus.outbound.common.enums.NaverStoreStatusType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * Class Name : ExtProductClientStub.java
 * Description : Naver Start Store > ProductServiceStub (Extends)
 *
 *  Modification Information
 *  Modify Date    Modifier    Comment
 *  -----------------------------------------------
 *  2021. 5. 18.   jh6108.kim   신규작성
 * </pre>
 *
 * @author jh6108.kim
 * @since 2021. 5. 18.
 */
@Slf4j
@Component
public class ExtProductClientStub extends ProductServiceStub {

    /**
     * Profile
     */
    private static Boolean isReal = null;
    private static String sellerId;
    private static String targetEndpoint;
    private static String serviceName = NaverMartProfile.PRODUCT_SERVICE_NAME.getValue();

    @Value("${market.naver.isReal}")
    public void setIsReal(Boolean bIsReal) {
        isReal =  bIsReal;
    }

    @Value("${market.naver.sellerId}")
    public void setSellerId(String sSellerId) {
        sellerId =  sSellerId;
    }

    @Value("${market.naver.targetEndpoint}")
    public void setTargetEndpoint(String sTargetEndpoint) {
        targetEndpoint =  sTargetEndpoint;
    }

    /**
     * AccessCredentials
     */
    private AccessCredentials ac = null;
    private Random random = new Random();
    private static String serviceVersion = NaverMartProfile.PRODUCT_SERVICE_VERSION.getValue();

    public void setAccessCredentials(AccessCredentials ac) { this.ac = ac; }

    private String createRequestID() {
        return UUID.randomUUID().toString() + "_PROD" + random.nextInt(999999999);
    }

    /**
     * Constructor that takes in a configContext
     *
     * @param configurationContext
     * @param targetEndpoint
     * @deprecated
     */
    public ExtProductClientStub(
        org.apache.axis2.context.ConfigurationContext configurationContext, java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
        super(configurationContext, targetEndpoint, false);
    }

    /**
     * Default Constructor
     *
     * @param configurationContext
     */
    public ExtProductClientStub(org.apache.axis2.context.ConfigurationContext configurationContext) throws org.apache.axis2.AxisFault {
        super(configurationContext, targetEndpoint + serviceName, false);
    }

    /**
     * Default Constructor
     */
    public ExtProductClientStub() throws org.apache.axis2.AxisFault {
        super(null, targetEndpoint + serviceName, false);
    }

    /**
     * 해당 생성자는 targetEndpoint 조작 하므로 직접 호출하지 말것
     * Constructor taking the target endpoint
     *
     * @deprecated
     */
    public ExtProductClientStub(java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
        super(null, targetEndpoint, false);
    }

    /**
     * ProductService API 호출결과
     * @param response
     * @return
     */
    public Boolean isResponseOk(BaseProductResponseType response) {

        Boolean returnResult;

        String resType = response.getResponseType();
        if(org.springframework.util.StringUtils.isEmpty(resType)) {
            resType = "";
        }

        // 결과 출력
        if("SUCCESS".equals(resType) || "SUCCESS-WARNING".equals(resType)) {
            returnResult = Boolean.TRUE;
        } else {
            log.error(serviceName + "_API_Code : "    + response.getError().getCode());
            log.error(serviceName + "_API_Message : " + response.getError().getMessage());
            log.error(serviceName + "_API_Detail : "  + response.getError().getDetail());

            returnResult = Boolean.FALSE;
        }

        if(resType.indexOf("WARNING") >= 0) {
            WarningListType warnListType = response.getWarningList();
            if(warnListType != null) {
                WarningType[]  warns = warnListType.getWarning();
                if(warns !=null && warns.length > 0) {
                    int wLength = warns.length;
                    for(int i = 0; i < wLength; i++) {
                        log.warn("warn " + i + " = " + warns[i].getCode());
                        log.warn("warn " + i + " = " + warns[i].getMessage());
                        log.warn("warn " + i + " = " + warns[i].getDetail());
                    }
                }
            }
        }

        return returnResult ;
    }

    /**
     * 상품 등록 API
     * @param productType
     * @return
     * @throws RemoteException
     */
    public ManageProductResponseType manageProduct(ProductType productType) throws RemoteException {

        ManageProductRequestType requestType = new ManageProductRequestType();
        ManageProductRequest request         = new ManageProductRequest();

        AccessCredentialsType credentials = ac.createProduct(serviceName, "ManageProduct");
        requestType.setAccessCredentials(credentials);
        requestType.setRequestID(createRequestID());
        requestType.setVersion(serviceVersion);
        requestType.setSellerId(sellerId);

        if(null != productType.getBranchBenefitDiscount()) {
            if( (null != productType.getBranchBenefitDiscount().getBranchBenefitType()) && ( null == productType.getSellerCustomCode1() || null == productType.getSellerCustomCode2()) ){
                productType.setStatusType(NaverStoreStatusType.SUSP.getCode());
            }
        }
        requestType.setProduct(productType);
        request.setManageProductRequest(requestType);

        ManageProductResponse response = super.manageProduct(request);
        ManageProductResponseType responseType = response.getManageProductResponse();

        return responseType;
    }

    /**
     * 상품 옵션 등록
     * @param optionType
     * @return
     * @throws RemoteException
     */
    public ManageOptionResponseType manageOption(OptionType optionType) throws RemoteException {

        ManageOptionRequestType requestType = new ManageOptionRequestType();
        ManageOptionRequest request         = new ManageOptionRequest();

        AccessCredentialsType credentials = ac.createProduct(serviceName, "ManageOption");
        requestType.setAccessCredentials(credentials);
        requestType.setRequestID(createRequestID());
        requestType.setVersion(serviceVersion);
        requestType.setSellerId(sellerId);
        requestType.setOption(optionType);

        request.setManageOptionRequest(requestType);

        ManageOptionResponse response = super.manageOption(request);
        ManageOptionResponseType responseType = response.getManageOptionResponse();

        return responseType;
    }

    /**
     * 상품상태 변경 API
     * @param saleStatusType
     * @return
     * @throws RemoteException
     */
    public ChangeProductSaleStatusResponseType changeProductSaleStatus(SaleStatusType saleStatusType) throws RemoteException {

        ChangeProductSaleStatusRequestType requestType = new ChangeProductSaleStatusRequestType();
        ChangeProductSaleStatusRequest     request     = new ChangeProductSaleStatusRequest();

        AccessCredentialsType credentials = ac.createProduct(serviceName, "ChangeProductSaleStatus");
        requestType.setAccessCredentials(credentials);
        requestType.setRequestID(createRequestID());
        requestType.setVersion(serviceVersion);
        requestType.setSellerId(sellerId);
        requestType.setSaleStatus(saleStatusType);

        request.setChangeProductSaleStatusRequest(requestType);

        ChangeProductSaleStatusResponse response = super.changeProductSaleStatus(request);
        ChangeProductSaleStatusResponseType responseType = response.getChangeProductSaleStatusResponse();

        return responseType;
    }

    /**
     * StoreFarm 등록 상품 조회
     * @param coopProductId
     * @return
     * @throws RemoteException
     */
    public GetProductResponseType getProduct(String coopProductId) throws RemoteException {

        GetProductRequestType requestType = new GetProductRequestType();
        GetProductRequest     request     = new GetProductRequest();

        AccessCredentialsType credentials = ac.createProduct(serviceName, "GetProduct");
        requestType.setAccessCredentials(credentials);
        requestType.setRequestID(createRequestID());
        requestType.setVersion(serviceVersion);
        requestType.setSellerId(sellerId);
        requestType.setProductId(Long.valueOf(coopProductId));

        request.setGetProductRequest(requestType);

        GetProductResponse response = super.getProduct(request);
        GetProductResponseType responseType = response.getGetProductResponse();

        return responseType;
    }

    /**
     * 옵션정보 조회
     *
     * @param productId
     * @return
     * @throws RemoteException
     */
    public GetOptionResponseType getOption(Long productId) throws RemoteException {

        GetOptionRequestType requestType = new GetOptionRequestType();
        GetOptionRequest     request     = new GetOptionRequest();

        AccessCredentialsType credentials = ac.createProduct(serviceName, "GetOption");
        requestType.setAccessCredentials(credentials);
        requestType.setRequestID(createRequestID());
        requestType.setVersion(serviceVersion);
        requestType.setSellerId(sellerId);
        requestType.setProductId(productId);

        request.setGetOptionRequest(requestType);

        GetOptionResponse response = super.getOption(request);
        GetOptionResponseType responseType = response.getGetOptionResponse();

        return responseType;
    }

    /**
     * 상품삭제
     *
     * @param productId
     * @return
     * @throws RemoteException
     */
    public DeleteProductResponseType deleteProduct(String productId) throws RemoteException {

        DeleteProductRequestType requestType = new DeleteProductRequestType();
        DeleteProductRequest     request     = new DeleteProductRequest();

        AccessCredentialsType credentials = ac.createProduct(serviceName, "DeleteProduct");
        requestType.setAccessCredentials(credentials);
        requestType.setRequestID(createRequestID());
        requestType.setVersion(serviceVersion);
        requestType.setSellerId(sellerId);
        requestType.setProductId(Long.valueOf(productId));

        request.setDeleteProductRequest(requestType);

        DeleteProductResponse response = super.deleteProduct(request);
        DeleteProductResponseType responseType = response.getDeleteProductResponse();

        return responseType;
    }

    /**
     * 모든 카테고리 정보 조회
     *
     * @param last
     * @return
     * @throws RemoteException
     */
    public GetAllCategoryListResponseType getAllCategoryList(String last) throws RemoteException {

        GetAllCategoryListRequestType requestType = new GetAllCategoryListRequestType();
        GetAllCategoryListRequest     request     = new GetAllCategoryListRequest();

        AccessCredentialsType credentials = ac.createProduct(serviceName, "GetAllCategoryList");
        requestType.setAccessCredentials(credentials);
        requestType.setRequestID(createRequestID());
        requestType.setVersion(serviceVersion);
        requestType.setLast(last); // 최하위 카테고리만 조회 여부 Y/N

        request.setGetAllCategoryListRequest(requestType);

        GetAllCategoryListResponse response = super.getAllCategoryList(request);
        GetAllCategoryListResponseType responseType = response.getGetAllCategoryListResponse();

        return responseType;
    }

    /**
     * 카테고리 정보를 계층 구조로 조회
     *
     * @param categoryId
     * @return
     * @throws RemoteException
     */
    public GetSubCategoryListResponseType getSubCategoryList(String categoryId) throws RemoteException {

        GetSubCategoryListRequestType requestType = new GetSubCategoryListRequestType();
        GetSubCategoryListRequest     request     = new GetSubCategoryListRequest();

        AccessCredentialsType credentials = ac.createProduct(serviceName, "GetSubCategoryList");
        requestType.setAccessCredentials(credentials);
        requestType.setRequestID(createRequestID());
        requestType.setVersion(serviceVersion);
        requestType.setCategoryId(categoryId);

        request.setGetSubCategoryListRequest(requestType);

        GetSubCategoryListResponse response = super.getSubCategoryList(request);
        GetSubCategoryListResponseType responseType = response.getGetSubCategoryListResponse();

        return responseType;
    }

    /**
     * 카테고리 정보를 조회하여 인증유형 ID 및 Name 조회
     * 어린이 제품안전 특별법 테스트를 위해서 만듬 ( 추후 필요할수도 있음 )
     *
     * @param categoryId
     * @return
     * @throws RemoteException
     */
    public GetCategoryInfoResponseType getCategoryInfo(String categoryId) throws RemoteException {

        GetCategoryInfoRequestType requestType = new GetCategoryInfoRequestType();
        GetCategoryInfoRequest     request     = new GetCategoryInfoRequest();

        AccessCredentialsType credentials = ac.createProduct(serviceName, "GetCategoryInfo");
        requestType.setAccessCredentials(credentials);
        requestType.setRequestID(createRequestID());
        requestType.setVersion(serviceVersion);
        requestType.setCategoryId(categoryId);

        request.setGetCategoryInfoRequest(requestType);

        GetCategoryInfoResponse response = super.getCategoryInfo(request);
        GetCategoryInfoResponseType responseType = response.getGetCategoryInfoResponse();

        return responseType;
    }

    /**
     * StoreFarm 등록 상품 조회
     * @param page
     * @param pageSize
     * @param statusTypeList
     * @param coopGoodId
     * @return
     * @throws RemoteException
     */
    public GetProductListResponseType getProductList (int page, int pageSize, StatusTypeList_type0 statusTypeList, String coopGoodId) throws RemoteException {

        GetProductListRequestType requestType = new GetProductListRequestType();
        GetProductListRequest request = new GetProductListRequest();

        AccessCredentialsType credentials = ac.createProduct(serviceName, "GetProductList");
        requestType.setAccessCredentials(credentials);
        requestType.setRequestID(createRequestID());
        requestType.setVersion(serviceVersion);
        requestType.setSellerId(sellerId);
        requestType.setPage(page);
        requestType.setPageSize(pageSize);
        requestType.setStatusTypeList(statusTypeList);

        if (StringUtils.isNotBlank(coopGoodId)) {
            requestType.setProductId(Long.parseLong(coopGoodId));
        }

        request.setGetProductListRequest(requestType);

        GetProductListResponse response = super.getProductList(request);
        GetProductListResponseType responseType = response.getGetProductListResponse();

        return responseType;
    }

}