package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 기본정보 Get Entry
 */
@Getter
@Setter
@ApiModel("마스터 정보")
public class MarketItemBasicGetDto {

	@ApiModelProperty(value = "상품번호")
	private String itemNo;

	@ApiModelProperty(value = "상품명")
	private String itemNm;

	@ApiModelProperty(value = "상품유형 (N:새상품, U:중고, R:리퍼, B:반품(리세일), C:주문제작, T:렌탈, M:휴대폰, E:e-ticket(기프티콘), P:업체배송상품 )")
	private String itemType;

	@ApiModelProperty(value = "상품상태(A:판매중, P:일시중지, E:판매종료, N:점포미취급상품)")
	private String itemStatus;

	@ApiModelProperty(value = "점포구분")
	private String storeType;

	@ApiModelProperty(value = "마켓ID")
	private String partnerId;

	@ApiModelProperty(value = "점포구분")
	private String dispYn;

	@ApiModelProperty(value = "마켓카테고리")
	private String marketCateCd;

	@ApiModelProperty(value = "마켓카테고리")
	private String marketItemStatus;

	@ApiModelProperty(value = "마켓상품번호")
	private String marketItemNo;

	@ApiModelProperty(value = "배송속성 ((직택배상품: DRCT_DLV, 직배전용상품: DRCT, 픽업전용상품: PICK, 퀵전용상품: QUICK))")
	private String shipAttr;

	@ApiModelProperty(value = "부가세 여부(Y:과세,N:비과세,Z:영세)")
	private String taxYn;

	@ApiModelProperty(value = "정상가격")
	private long originPrice;

	@ApiModelProperty(value = "심플적용가")
	private long simplePrice;

	@ApiModelProperty(value = "쿠폰적용가(심플포함)")
	private long couponPrice;

	//TODO: 카테고리별 속성 처리 추가 되면 삭제
	@ApiModelProperty(value = "대카테고리")
	private String lcateCd;

	@ApiModelProperty(value = "매입가")
	private long purchasePrice;

	@ApiModelProperty(value = "원산지")
	private String originTxt;

	@ApiModelProperty(value = "판매가격")
	private long salePrice;

	@ApiModelProperty(value = "장바구니 제한여부")
	private String cartLimitYn;

	@ApiModelProperty(value = "판매단위")
	private int saleUnit;

	@ApiModelProperty(value = "구매제한 - 최소구매수량")
	private int purchaseMinQty;

	@ApiModelProperty(value = "1인당 구매제한 사용여부 - 미사용:Y / 사용:N")
	private String purchaseLimitYn;

	@ApiModelProperty(value = "구매제한 타입 - 1회 : O / 기간제한 : P")
	private String purchaseLimitDuration;

	@ApiModelProperty(value = "구매제한 일자 - ?일")
	private int purchaseLimitDay;

	@ApiModelProperty(value = "구매제한 개수 - ?개")
	private int purchaseLimitQty;

	@ApiModelProperty(value = "판매기간설정여부")
	private String salePeriodYn;

	@ApiModelProperty(value = "판매시작기간")
	private String saleStartDt;

	@ApiModelProperty(value="판매종료기간")
	private String saleEndDt;

	@ApiModelProperty(value = "판매수량")
	private int stockQty;

	@ApiModelProperty(value = "재고관리유형")
	private String stockType;

	@ApiModelProperty(value ="택배점 취급여부(취급:Y,취급안함:N)")
	private String dealYn;

	@ApiModelProperty(value ="새벽배송 취급여부(취급:Y,취급안함:N)")
	private String auroraYn;

	@ApiModelProperty(value ="취급중지여부" , hidden = true)
	private String stopDealYn;

	@ApiModelProperty(value ="마켓연동취급중지여부")
	private String marketStopDealYn;

	@ApiModelProperty(value ="hyper/club/express/ds별 온라인 취급여부")
	private String onlineYn;

	@ApiModelProperty(value ="온라인 취급상태 (Y: 취급, N: 취급안함)")
	private String pfrYn;

	@ApiModelProperty(value ="가상점단독판매여부( Y:단독판매, N:단독판매 안함 )")
	private String virtualStoreOnlyYn;

	@ApiModelProperty(value = "성인상품유형 (NORMAL: 일반, ADULT: 성인, LOCAL_LIQUOR: 전통주)")
	private String adultType;

	@ApiModelProperty(value = "이미지노출여부 (Y: 노출, N: 노출안함)")
	private String imgDispYn;

	@ApiModelProperty(value = "해외배송대행여부" )
	private String globalAgcyYn;

	@ApiModelProperty(value = "대체여부(Y : 대체불가, N: 대체가능)")
	private String substitutionYn;

	@ApiModelProperty(value = "isbn정보")
	private String isbn;

	@ApiModelProperty(value = "픽업상품 여부 (Y: 픽업전용, N: 일반)")
	private String pickYn;

	@ApiModelProperty(value = "선물하기 여부 (Y : 설정 , N: 설정안함)")
	private String giftYn;

	@ApiModelProperty(value = "선물하기 여부 (Y : 설정 , N: 설정안함)")
	private String rsvYn;

	@ApiModelProperty(value = "가격비교연동여부")
	private String epYn;

	@ApiModelProperty(value = "제조일자")
	private String makeDt;

	@ApiModelProperty(value = "유효일자")
	private String expDt;

	@ApiModelProperty(value = "브랜드명")
	private String brandNm;

	@ApiModelProperty(value = "제조사명")
	private String makerNm;

	@ApiModelProperty(value = "상품상세정보")
	private String itemDesc;

	@ApiModelProperty(value = "선택옵션사용여부 (사용:Y,미사용:N)")
	private String optSelUseYn;

	@ApiModelProperty(value = "선택형 옵션단계")
	private Integer optTitleDepth;

	@ApiModelProperty(value = "선택형 옵션 1단계 타이틀")
	private String opt1Title;

	@ApiModelProperty(value = "고시정보 html")
	private String noticeHtml;
	
	@ApiModelProperty(value= "online sale cd")
	private String onlineSaleCd;

	@ApiModelProperty(value = "아이콘")
	private String marketIconType;

	@ApiModelProperty(value = "총 단위량 ( 유닛 당 펜스를 계산하는 데 사용되는 총용량 )")
	private int totalUnitQty;

	@ApiModelProperty(value = "단위 수량")
	private int unitQty;

	@ApiModelProperty(value = "측정 단위")
	private String unitMeasure;

	@ApiModelProperty(value = "단위 노출여부")
	private String unitDispYn;

	@ApiModelProperty(value = "전단여부")
	private String leafletYn;

	@ApiModelProperty(value = "세분류")
	private String dcateCd;
}