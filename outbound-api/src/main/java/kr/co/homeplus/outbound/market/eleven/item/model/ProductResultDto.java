package kr.co.homeplus.outbound.market.eleven.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import kr.co.homeplus.outbound.market.eleven.item.enums.ClientMessageResultCodeType;
import lombok.Data;

@ApiModel("11번가 상품 중지 및 중지해제 처리")
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ClientMessage")
public class ProductResultDto {
	@ApiModelProperty("출력결과 메시지")
	private String message;

	@ApiModelProperty("상품번호")
	private String productNo;

	@ApiModelProperty("결과코드")
	private ClientMessageResultCodeType resultCode;

}
