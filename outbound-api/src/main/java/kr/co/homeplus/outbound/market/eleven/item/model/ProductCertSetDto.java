package kr.co.homeplus.outbound.market.eleven.item.model;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@XmlAccessorType( XmlAccessType.FIELD )
@XmlRootElement( name = "ProductCert" )
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductCertSetDto {

	/**
	 * [선택] 인증유형
		101 : [공산품] 안전인증
		103 : [공산품] 자율안전확인
		124 : [공산품] 안전/품질표시
		123 : [공산품] 어린이보호포장
		102 : [전기용품] 안전인증
		104 : [전기용품] 안전확인
		105 : [방송통신기자재] 적합성평가 (적합인증, 적합등록)
		108 : 유기농산물
		109 : 무농약농산물
		110 : 저농약농산물
		111 : 유기축산물
		112 : 무항생제축산물
		113 : 친환경수산물
		114 : 위해요소 중점관리(HACCP)
		115 : 농산물 우수관리인증(GAP)
		116 : 가공식품 표준화인증(KS)
		117 : 유기가공식품 인증
		118 : 수산물 품질인증
		119 : 수산특산물 품질인증
		120 : 수산전통식품 품질인증
		122 : 건강기능식품 광고심의
		127 : [전기용품] 공급자적합성확인
		128 : [어린이제품] 안전인증
		129 : [어린이제품] 안전확인
		131 : 해당없음(대상이 아닌 경우)
		132 : 상품상세설명 참조
	 */
	private String certTypeCd;

	/**
	 * [선택] 인증번호
		ex)제C-07-10호
	 */
	private String certKey;

}