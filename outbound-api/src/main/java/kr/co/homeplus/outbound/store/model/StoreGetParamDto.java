package kr.co.homeplus.outbound.store.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("상품관리 조회")
public class StoreGetParamDto {

    @ApiModelProperty(value = "점포ID")
    private Integer storeId;

    @ApiModelProperty(value = "마켓ID")
    private String partnerId;
}
