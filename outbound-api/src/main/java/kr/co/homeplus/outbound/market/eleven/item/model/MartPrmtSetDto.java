package kr.co.homeplus.outbound.market.eleven.item.model;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "martPrmt")
public class MartPrmtSetDto {

	/**
	 * 프로모션 상품 코드
	 */
	private String martPrmtCd;

	/**
	 * 프로모션 상품 번호
	 */
	private String martPrmtNo;

	/**
	 * 프로모션 상품명
	 */
	@XmlElement( name = "martPrmtList" )
	private List<MartPrmtListSetDto> martPrmtList;



}