package kr.co.homeplus.outbound.market.eleven.item.model;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import kr.co.homeplus.outbound.market.eleven.item.enums.SelStatCdType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "StoreProductList")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PriceSetDto{

	/**
	 * 마트번호
	 */
	private String martNo;

	/**
	 * 지점번호
	 */
	private String strNo;

	/**
	 * 11번가 상품번호
	 */
	private String prdNo;

	/**
	 * 상품 가격
	 */
	private String selPrc;

	/**
	 * 판매 상태
	 */
	private SelStatCdType selStatCd;

	/**
	 * 재고수량
	 */
	private String prdSelQty;

	/**
	 * 프로모션
	 */
	@XmlElement( name = "martPrmt" )
	private List<MartPrmtSetDto> martPrmt;

}