package kr.co.homeplus.outbound.core.utility;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public abstract class RestUtil {

    @Value("${plus.resource-routes.eleven.url}")
    public String host;

    @Value("${enc-key.eleven}")
    protected String enc;

    private final RestTemplate restTemplate;

    protected RestUtil(@Qualifier("sk11RestTemplate") RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    protected String getHost() { return host;}

    public <T> T execute(HttpMethod httpMethod, String url, Object request, ParameterizedTypeReference<T> response,
        Map<String, String> params) {

        if (StringUtils.isEmpty(url)) {
            throw new IllegalArgumentException( "url 이(가) 필요 합니다." );
        }

        if (ObjectUtils.isEmpty(httpMethod)) {
            throw new IllegalArgumentException( "httpMethod 이(가) 필요 합니다." );
        }

        HttpHeaders headers = this.createHttpHeaders();

        log.debug( "\n - Request Object- \n" + request );
        log.debug( "\n - Request Param- \n" + params );
        log.debug( "\n - Request url- \n" + url );

        ResponseEntity<T> result = null;

        try {
            HttpEntity<Object> req = new HttpEntity<Object>(request, headers);
            result = restTemplate.exchange(url,
                httpMethod, req,
                response, params);


        }catch (HttpStatusCodeException e)  {
            log.error(e.getResponseBodyAsString());
        }catch (Exception e) {
            log.error(e.getMessage());
        }

        log.debug("- HEADER \n " + result.getHeaders().toString());
        log.debug("- BODY \n " + result.getBody().toString());

        return result.getBody();
    }

    public <T> T execute(URI url, HttpMethod method, HttpEntity<?> requestEntity, ParameterizedTypeReference<T> responseType) throws Exception {
        T t = restTemplate.exchange(url, method, requestEntity, responseType).getBody();

        return t;
    }

    public <T> T execute(String url, HttpMethod method, HttpEntity<?> requestEntity, ParameterizedTypeReference<T> responseType) throws Exception {
        URI uri = URI.create(url);
        return this.execute(uri, method, requestEntity, responseType);
    }

    /**
     * 11번가 Header 생성
     * @return
     */
    private HttpHeaders createHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.setContentType( new MediaType( "text", "xml", Charset.forName( "euc-kr" ) ) );
        httpHeaders.setAccept( Collections.singletonList( MediaType.APPLICATION_XML ) );
        httpHeaders.set("openapikey", enc);

        return httpHeaders;
    }

}