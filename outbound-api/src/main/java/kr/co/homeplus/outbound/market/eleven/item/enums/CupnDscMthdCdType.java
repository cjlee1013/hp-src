package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

/**
 * 할인단위 코드
 */
public enum CupnDscMthdCdType {

	@XmlEnumValue("01")
	AMT("01", "원"),

	@XmlEnumValue("02")
	PERCENT("02", "퍼센트(%)");

	@Getter
	private String code;
	@Getter
	private String desc;

	private CupnDscMthdCdType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}
