package kr.co.homeplus.outbound.market.naver.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Address {
    private String addressType;             //주소타입(DOSMETIC:국내, FOREIGN:국외)
    private String zipCode;
    private String baseAddress;
    private String detailedAddress;
    private boolean isRoadNameAddress;
    private String city;
    private String state;
    private String country;
    private String tel1;
    private String tel2;
    private String name;
    private String pickupLocationType;      //수령위치코드(FRONT_OF_DOOR:문앞,MANAGEMENT_OFFICE:경비실보관,DIRECT_RECEIVE:직접수령,OTHER:기타)
    private String pickupLocationContent;
    private String entryMethod;             //출입방법코드(LOBBY_PW:공동현관비밀번호입력,MANAGEMENT_OFFICE:경비실호출,FREE:자유출입가능,OTHER:기타출입방법)
    private String entryMethodContent;
}
