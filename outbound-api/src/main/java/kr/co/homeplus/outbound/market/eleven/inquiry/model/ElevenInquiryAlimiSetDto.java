package kr.co.homeplus.outbound.market.eleven.inquiry.model;

import lombok.Data;

@Data
public class ElevenInquiryAlimiSetDto {
    /**
     * 검색 시작일
     */
    private String startTime;

    /**
     * 검색 종료일
     */
    private String endTime;

    /**
     * 처리 상태
     * 01 : 미확인
     * 02 : 답변대기
     * 03 : 답변완료
     * 04 : 재답변요청
     * 05 : 재답변완료
     * 06 : 처리완료
     */
    private String emerNtceCrntCd;

    private int ordNo;
    
    /**
     * HT, TC구분값
     */
    private String corpCdType;
}
