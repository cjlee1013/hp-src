package kr.co.homeplus.outbound.market.naver.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PlaceOrderFail {
    private String marketOrderItemNo;
    private String errorCode;
    private String errorMsg;
    private String errorDetail;
}
