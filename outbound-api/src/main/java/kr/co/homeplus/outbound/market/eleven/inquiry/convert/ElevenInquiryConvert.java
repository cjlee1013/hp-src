package kr.co.homeplus.outbound.market.eleven.inquiry.convert;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import kr.co.homeplus.outbound.core.utility.StringUtil;
import kr.co.homeplus.outbound.market.eleven.config.MarketElevenConfig;
import kr.co.homeplus.outbound.market.eleven.inquiry.enums.EmerNtceCdType;
import kr.co.homeplus.outbound.market.eleven.inquiry.enums.EmerNtceCrntCdType;
import kr.co.homeplus.outbound.market.eleven.inquiry.enums.EmerNtceDtlCdType;
import kr.co.homeplus.outbound.market.eleven.inquiry.enums.QnADtlsCdType;
import kr.co.homeplus.outbound.market.eleven.inquiry.mapper.ElevenInquirySlaveMapper;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryAlimiResultDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryGetDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryReplyHistoryDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryResultDto;
import kr.co.homeplus.outbound.market.naver.inquiry.enums.CsType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class ElevenInquiryConvert {

    private final MarketElevenConfig marketElevenConfig;
    private final ElevenInquirySlaveMapper elevenInquirySlaveMapper;
    private static MarketElevenConfig elevenConfig;
    private static ElevenInquirySlaveMapper elevenSlaveMapper;
    private static String MARKET_TYPE = "ELEVEN";

    @PostConstruct
    private void initialize() {
      elevenConfig = marketElevenConfig;
      elevenSlaveMapper = elevenInquirySlaveMapper;
    }

    /**
     * 11번가 문의 내역 Convert
     */
    public static List<ElevenInquiryGetDto> convertInquiryList(List<ElevenInquiryResultDto> elevenInquiryResultDto ) throws Exception {
        List<ElevenInquiryGetDto> resultList = new ArrayList<>();
        int index = 1;

        if ( null == elevenInquiryResultDto ) {
            throw new IllegalArgumentException( "수집된 상품QnA 데이터가 존재하지 않습니다." );
        }

        for (ElevenInquiryResultDto dto : elevenInquiryResultDto) {
          ElevenInquiryGetDto elevenInquiryGetDto = new ElevenInquiryGetDto();

          if(!ObjectUtils.isEmpty(dto.getBrdInfoClfNo())) {
            Map<String, Object> marketItemInfo = elevenSlaveMapper.selectMarketItemInfo("coopeleven",dto.getBrdInfoClfNo());
            if(!ObjectUtils.isEmpty(marketItemInfo.get("itemNo"))) {
              elevenInquiryGetDto.setGoodId( Integer.parseInt(marketItemInfo.get("itemNo").toString()) );
            }
          }

          elevenInquiryGetDto.setMarketType(MARKET_TYPE);
          elevenInquiryGetDto.setMessageType( CsType.QNA.getCode() );
          elevenInquiryGetDto.setMessageNo( dto.getBrdInfoNo() );
          elevenInquiryGetDto.setCopGoodId( dto.getBrdInfoClfNo() );
          elevenInquiryGetDto.setRequestComments( dto.getBrdInfoCont() );
          elevenInquiryGetDto.setReceiveDate(dto.getCreateDt());
          elevenInquiryGetDto.setBuyYn( dto.getBuyYn() );
          elevenInquiryGetDto.setContactType( convertToContactCode( dto.getQnaDtlsCdNm() ).toString() );
          elevenInquiryGetDto.setContactCode( convertToContactCode( dto.getQnaDtlsCdNm() ).getCode() );
          elevenInquiryGetDto.setInquirerId( dto.getMemID() );
          elevenInquiryGetDto.setInquirerName( dto.getMemNM() );

            // 답변 여부에 따라 답변 플래그 및 컨텐츠 설정을 달리한다.
            if ( dto.getAnswerYn().equals("Y") ) {
                elevenInquiryGetDto.setResponseContents( dto.getAnswerCont() );
                elevenInquiryGetDto.setIsresponseYn("Y");
                elevenInquiryGetDto.setIssendYn("Y");
                elevenInquiryGetDto.setIsresponseDate(dto.getAnswerDt());

                // 이미 등록된 답변이 있을 경우 답변 히스토리 List에 set 한다.
                ArrayList<ElevenInquiryReplyHistoryDto> replyHistoryList = new ArrayList<>();
                ElevenInquiryReplyHistoryDto historyDto = new ElevenInquiryReplyHistoryDto();
                historyDto.setCopNo( MARKET_TYPE );
                historyDto.setMessageNo( dto.getBrdInfoNo() );
                historyDto.setReplyNo( index );
                historyDto.setContent( dto.getAnswerCont() );
                historyDto.setReceiveDt(dto.getCreateDt());
                historyDto.setResponseDt(dto.getAnswerDt());

                replyHistoryList.add( historyDto );
                elevenInquiryGetDto.setElevenInquiryReplyHistoryList( replyHistoryList );
            } else {
              elevenInquiryGetDto.setIsresponseYn("N");
              elevenInquiryGetDto.setIssendYn("N");
            }
          resultList.add(elevenInquiryGetDto);
        }
        return resultList;
    }

    /**
    * 11번가 긴급알리미 내역 Convert
    */
    public static List< ElevenInquiryGetDto > convertAlimiList(List<ElevenInquiryAlimiResultDto> elevenInquiryAlimiResultDto) throws Exception {
        List< ElevenInquiryGetDto > resultList = new ArrayList<>();

        int index = 1;

        if (ObjectUtils.isEmpty(elevenInquiryAlimiResultDto)) {
            throw new IllegalArgumentException( "수집된 긴급알리미 데이터가 존재하지 않습니다." );
        }

        for ( ElevenInquiryAlimiResultDto dto : elevenInquiryAlimiResultDto ) {
          ElevenInquiryGetDto elevenInquiryGetDto = new ElevenInquiryGetDto();
          elevenInquiryGetDto.setMarketType( MARKET_TYPE );
          elevenInquiryGetDto.setMessageType( CsType.EMERGENCY.getCode() );
          elevenInquiryGetDto.setMessageNo( dto.getEmerNtceSeq() );
          elevenInquiryGetDto.setRequestTitle( dto.getEmerNtceSubject() );
          elevenInquiryGetDto.setReceiveDate(dto.getCreateDt());

            String returnTime = "회신기한 : " + dto.getEmerReplyDt();
            // 주문번호가 없는 경우, 상품명/고객명 모두 null로 들어온다.
            if(dto.getOrdNo().equals(null) || dto.getOrdNo().equals("")) {
            	// 주문번호가 없는 경우 답변 시 HC/HT 구분 방법이 없기 때문에, 주문번호 값에 사업자 코드를 대신 삽입
              elevenInquiryGetDto.setCopOrdGoodSeq("57010");
            	String requestContents = StringUtil.getMaxVarchar2(returnTime + "\n" + dto.getEmerCtnt() );
              elevenInquiryGetDto.setRequestComments(convertHTML(requestContents));
            } else {
              elevenInquiryGetDto.setCopOrdGoodSeq( dto.getOrdNo() );
            	String itemNm = "상품명 : " + dto.getPrdNm() + "\n";
            	String requestContents = StringUtil.getMaxVarchar2(returnTime + "\n" + itemNm + "\n" + dto.getEmerCtnt() );
              elevenInquiryGetDto.setRequestComments(convertHTML(requestContents));
              elevenInquiryGetDto.setInquirerName( dto.getMemNm() );
              elevenInquiryGetDto.setInquirerId( dto.getMemId() );
            }
            elevenInquiryGetDto.setContactType( convertToEmerContactType( dto.getEmerNtceDtlCd() ).getDesc() );
            elevenInquiryGetDto.setContactCode( convertToEmerContactType( dto.getEmerNtceDtlCd() ).getHomeplusCd() );
            elevenInquiryGetDto.setMessageSection( convertToMessageSection( dto.getEmerNtceCd() ).getDesc() );

            // 답변 상태에 따라 답변 및 전송 관련 정보를 설정한다.
            if (convertIsReplyYn(dto.getEmerNtceCrntCd()) && !ObjectUtils.isEmpty(dto.getEmerReplyList())) {
              List< ElevenInquiryReplyHistoryDto > copQnaReplyHistoryList = new ArrayList<>();
              for ( String replyCtnt : dto.getEmerReplyList().getEmerReplyCtnt() ) {
                ElevenInquiryReplyHistoryDto copQnaReplyHistory = new ElevenInquiryReplyHistoryDto();
                copQnaReplyHistory.setCopNo( MARKET_TYPE );
                copQnaReplyHistory.setMessageNo( dto.getEmerNtceSeq() );
                copQnaReplyHistory.setReplyNo( index );
                copQnaReplyHistory.setContent( replyCtnt );
                copQnaReplyHistory.setReceiveDt( extractionYmd(dto.getCreateDt()) );

                copQnaReplyHistoryList.add( copQnaReplyHistory );
                // 가장 최근의 답변일 경우 해당 문의 row의 답변 내역에 setting한다.
                if ( dto.getEmerReplyList().getEmerReplyCtnt().size() == index ) {
                  elevenInquiryGetDto.setResponseContents( replyCtnt );
                }
                index++;
              }
              elevenInquiryGetDto.setIsresponseYn( "Y" );
              elevenInquiryGetDto.setIssendYn( "Y" );
              elevenInquiryGetDto.setElevenInquiryReplyHistoryList( copQnaReplyHistoryList );
            } else {
              elevenInquiryGetDto.setIsresponseYn( "N" );
              elevenInquiryGetDto.setIssendYn( "N" );
            }
            resultList.add( elevenInquiryGetDto );
            index = 1;
        }
        return resultList;
    }

    /**
     * 11번가의 문의 타입을 홈플러스 코드로 변환한다.
     * 
     * @param dtlsNm
     * 
     * @return contactCode
     */
    public static QnADtlsCdType convertToContactCode( String dtlsNm ) {
      QnADtlsCdType contactCode = QnADtlsCdType.getTypeOf( dtlsNm );
        return contactCode;
    }

    /**
     * HTML 코드로 들어오는 문의 내용을 일반 String으로 변환
     *
     * @param requestContents
     * @return removeTag
     */
    private static String convertHTML(String requestContents) {
    	// &를 제거한다.
    	String ampersandToVacuum = requestContents.replaceAll("amp;", "");
    	// 유니코드를 html 코드로 변환한다.
    	String unicodeToHtml = StringEscapeUtils.unescapeHtml(ampersandToVacuum);
    	// html 태그를 제거한다.
    	String removeTag = unicodeToHtml.replaceAll("<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>", "");

    	return removeTag;
    }

    /**
     * 11번가의 긴급알리미 문의 소분류 코드를 명칭 및 홈플러스 코드로 변환한다.
     *
     * @param emerNtceDtlCd
     * @return contactCode
     */
    private static EmerNtceDtlCdType convertToEmerContactType( String emerNtceDtlCd ) {
      EmerNtceDtlCdType contactCode = EmerNtceDtlCdType.getTypeOf( emerNtceDtlCd );
      return contactCode;
    }

    /**
     * 11번가의 긴급알리미 문의 대분류 코드를 명칭으로 변환한다.
     *
     * @param emerNtceCd
     * @return messageSection
     */
    public static EmerNtceCdType convertToMessageSection( String emerNtceCd ) {
      EmerNtceCdType messageSection = EmerNtceCdType.getTypeOf( emerNtceCd );
      return messageSection;
    }

    /**
     * 긴급알리미 답변 상태에 따른 답변 및 전송 여부 플래그를 리턴한다.
     *
     * @param replyStatus
     * @return contactCode
     */
    public static Boolean convertIsReplyYn( String replyStatus ) {
      EmerNtceCrntCdType emerReplyCd = EmerNtceCrntCdType.getTypeOf( replyStatus );
      return emerReplyCd.getIsSendYn();
    }

  /**
   * 11번가에서 넘어온 긴급알리미 날짜 데이터 형식을 변환한다.
   *
   * @param receiveDate
   * @return stringToDate
   * @throws Exception
   */
  public static String extractionYmd(String receiveDate) throws Exception{
    Pattern p = Pattern.compile("(([0-9]{4})(1[0-2]|0[1-9])(3[01]|0[1-9]|[12][0-9]))");
    Matcher m = p.matcher(receiveDate);
    String strDate = "";

    if(m.find()) {
      strDate = m.group();
    } else {
      strDate = receiveDate;
    }

    return strDate;
  }
}
