package kr.co.homeplus.outbound.market.naver.claim.enums;

public enum DateType {
    YEAR, MONTH, DAY, HOUR, MIN, SEC
}
