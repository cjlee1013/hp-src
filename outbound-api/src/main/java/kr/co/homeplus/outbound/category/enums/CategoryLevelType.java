package kr.co.homeplus.outbound.category.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 카테고리 Level
 */
@RequiredArgsConstructor
public enum CategoryLevelType {

        LARGE( "L", 1)
    ,   MIDDLE("M", 2)
    ,   SMALL( "S",3)
    ,   DIVIDE( "D",4 );

    @Getter
    private final String type;

    @Getter
    private final Integer depth;

    public static CategoryLevelType getTypeOf( String code ) {
        for ( CategoryLevelType o : CategoryLevelType.values() ) {
            if ( o.getType().equals( code ) ) {
                return o;
            }
        }
        throw new IllegalArgumentException( "일치하는 타입이 없습니다." );
    }
}
