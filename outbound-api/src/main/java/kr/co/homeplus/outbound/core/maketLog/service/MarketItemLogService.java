package kr.co.homeplus.outbound.core.maketLog.service;

import java.util.Date;
import kr.co.homeplus.outbound.core.maketLog.exception.MarketLogServiceException;
import kr.co.homeplus.outbound.core.monghodb.ApiMongodbTemplate;
import kr.co.homeplus.outbound.core.utility.DateUtil;
import kr.co.homeplus.outbound.item.model.ItemSpecHistParamDto;
import kr.co.homeplus.outbound.item.model.ItemSpecHistSetDto;
import kr.co.homeplus.outbound.item.model.MarketItemGetDto;
import kr.co.homeplus.outbound.item.model.MarketItemHistGetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MarketItemLogService {

    private final ApiMongodbTemplate apiMongodbTemplate;
    /**
     * 외부 연동 API 이력 저장
     * Spec 형태로 저장
     * @param paramDto
     */
    @Async
    public void setItemSpecHist( ItemSpecHistParamDto paramDto) {

        ItemSpecHistSetDto itemSpecHistSetDto = new ItemSpecHistSetDto();
        try {
            //30일
            itemSpecHistSetDto.setExpireAt(DateUtils.addMinutes(new Date(), 43800));
            itemSpecHistSetDto.setMarketItemHistDt(DateUtil.getNowMillYmdHis());

            itemSpecHistSetDto.setSpec(paramDto.getSpec());
            itemSpecHistSetDto.setItemNo(paramDto.getItemNo());
            itemSpecHistSetDto.setRequest(paramDto.getRequest());
            itemSpecHistSetDto.setResponse(paramDto.getResponse());
            itemSpecHistSetDto.setPartnerId(paramDto.getPartnerId());
            itemSpecHistSetDto.setMethodTxt(paramDto.getMethodTxt());

            apiMongodbTemplate.saveObject(itemSpecHistSetDto);

        } catch(Exception e) {
            log.error("setItemSpecHist :  = {}", ExceptionUtils.getStackTrace(e));
            throw new MarketLogServiceException(e.getMessage());
        }
    }

    /**
     * 외부 연동 Refit 상품 데이터 저장
     * @param marketItemGetDto
     */
    @Async
    public void setMarketItemHist(MarketItemGetDto marketItemGetDto) {

        MarketItemHistGetDto marketItemHistGetDto = new MarketItemHistGetDto();
        try {
            //30일
            marketItemHistGetDto.setExpireAt(DateUtils.addMinutes(new Date(), 43800));
            marketItemHistGetDto.setMarketItemHistDt(DateUtil.getNowMillYmdHis());
            marketItemHistGetDto.setItemNo(marketItemGetDto.getBasic().getItemNo());
            marketItemHistGetDto.setPartnerId(marketItemGetDto.getBasic().getPartnerId());

            marketItemHistGetDto.setInfo(marketItemGetDto);
            apiMongodbTemplate.saveObject(marketItemHistGetDto);

        } catch(Exception e) {
            log.error(" setMarketItemHist : " + marketItemHistGetDto.getId() + " = {}", ExceptionUtils.getStackTrace(e));
        }
    }

}
