package kr.co.homeplus.outbound.store.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kr.co.homeplus.outbound.common.enums.MarketType;
import kr.co.homeplus.outbound.response.ResponseResult;
import kr.co.homeplus.outbound.store.model.StoreGetParamDto;
import kr.co.homeplus.outbound.store.model.eleven.MartStoreGetDto;
import kr.co.homeplus.outbound.store.service.StoreService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "마켓연동 점포관리")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found")
    ,   @ApiResponse(code = 405, message = "Method Not Allowed")
    ,   @ApiResponse(code = 422, message = "Unprocessable Entity")
    ,   @ApiResponse(code = 500, message = "Internal Server Error")
})
@RequestMapping("/store")
@RestController
@RequiredArgsConstructor
@Slf4j
public class StoreController {

    private final StoreService storeService;

    @ApiOperation(value = "11번가 지점 조회 ", response = MartStoreGetDto.class)
    @GetMapping(value = "/getStoreInfo")
    public ResponseObject getStoreList(
        @RequestParam @ApiParam(name = "storeId", value = "홈플러스 점포번호", required = false) Integer storeId) throws Exception {
        return ResourceConverter.toResponseObject(storeService.getStoreInfo(storeId));
    }

    @ApiOperation(value = "11번가 지점 조회(전체) ", response = MartStoreGetDto.class)
    @GetMapping(value = "/getStoreInfoAll")
    public ResponseObject getStoreList() throws Exception {
        return  ResourceConverter.toResponseObject(storeService.getStoreInfoAll());
    }

    @ApiOperation(value = "11번가 등록/수정", response = ResponseResult.class)
    @PostMapping(value = "setStoreInfo")
    public ResponseObject setStoreInfo(@RequestBody StoreGetParamDto storeGetParamDto) throws Exception {
        storeGetParamDto.setPartnerId(MarketType.ELEVEN.getId());
        return  ResourceConverter.toResponseObject(storeService.setStoreInfo(storeGetParamDto));
    }

    @ApiOperation(value = "[테스트 조회용] 11번가 우편번호조회 ", response = ResponseResult.class)
    @GetMapping(value = "findStoreMailNoInfo")
    public ResponseObject findStoreMailNoInfo( @RequestParam @ApiParam(name = "strNo", value = "11번가 지점번호") String strNo) throws Exception {
         return  ResourceConverter.toResponseObject(storeService.findStoreMainInfo(strNo));
    }
}
