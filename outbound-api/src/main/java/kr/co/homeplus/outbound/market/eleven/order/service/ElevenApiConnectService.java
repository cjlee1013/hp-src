package kr.co.homeplus.outbound.market.eleven.order.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import kr.co.homeplus.outbound.core.constants.UriConstants;
import kr.co.homeplus.outbound.market.eleven.order.model.ElevenOrder;
import kr.co.homeplus.outbound.market.eleven.order.model.ElevenOrders;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class ElevenApiConnectService {

    @Value("${plus.resource-routes.eleven.url}")
    private String ELEVEN_URL;
    @Value("${market.eleven.apiKey}")
    private String ELEVEN_API_KEY;

    public String createOrderComplete(String startTime, String endTime) throws Exception {

        String apiUri = UriConstants.ELEVEN_GET_ORDER_COMPLETE;
        URL url = new URL(ELEVEN_URL + apiUri + "/" + startTime + "/" + endTime);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(HttpMethod.GET.name());
        con.addRequestProperty("openapikey", ELEVEN_API_KEY);

        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream(), "EUC-KR"))) {
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (Exception e) {
            log.error("[11st][createOrderComplete] 주문수집 - connect fail :: {}", ExceptionUtils.getStackTrace(e));
        }
        log.info("[11st][createOrderComplete] 주문수집 - 응답 :: {}", stringBuilder.toString());

        return stringBuilder.toString();
    }

    /**
     * 11번가 주문수집 xml 파싱 후 객체 리턴
     * @param startTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public List<ElevenOrder> createOrderCompleteParsing(String startTime, String endTime) throws Exception {

        String apiUri = UriConstants.ELEVEN_GET_ORDER_COMPLETE;
        URL url = new URL(ELEVEN_URL + apiUri + "/" + startTime + "/" + endTime);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(HttpMethod.GET.name());
        con.addRequestProperty("openapikey", ELEVEN_API_KEY);

        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream(), "EUC-KR"))) {
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (Exception e) {
            log.error("[11st][createOrderComplete] 주문수집 - connect fail :: {}", ExceptionUtils.getStackTrace(e));
            return null;
        }

        log.info("[11st][createOrderComplete] 주문수집 - 응답 :: {}", stringBuilder.toString());

        // JAXB parsing
        StringReader stringReader = new StringReader(stringBuilder.toString());
        JAXBContext jaxbContext = JAXBContext.newInstance(ElevenOrders.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        ElevenOrders orders = (ElevenOrders) unmarshaller.unmarshal(stringReader);

        return orders.getElevenOrderList();
    }
}
