package kr.co.homeplus.outbound.market.naver.inquiry.service;

import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AccessCredentialsType;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryRequest;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryRequestType;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.AnswerCustomerInquiryResponseType;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.BaseResponseType;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.ErrorType;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListRequest;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListRequestType;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.GetCustomerInquiryListResponseType;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.WarningListType;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub.WarningType;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.UUID;
import kr.co.homeplus.outbound.core.utility.StringUtil;
import kr.co.homeplus.outbound.market.naver.config.MarketNaverConfig;
import kr.co.homeplus.outbound.market.naver.inquiry.model.NaverInquiryGetDto;
import kr.co.homeplus.outbound.market.naver.inquiry.model.NaverInquirySetDto;
import kr.co.homeplus.outbound.market.naver.util.DateUtils;
import kr.co.homeplus.outbound.market.naver.util.NaverCryptoUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExtCustomerInquiryClientStub {

	private final MarketNaverConfig marketNaverConfig;
	private String timeStamp;

    /**
	 * 문의 서비스 응답 성공여부 검사 
	 * 
	 * @return Boolean.TRUE SUCCESS
	 * Boolean.FALSE FAIL
	 */
	public Boolean isResponseOk(BaseResponseType response) {
		String resType = response.getResponseType();
		if(resType==null)
			resType = "";
		
		Boolean returnResult = null;
		//결과 출력
		if("SUCCESS".equals(resType) || "SUCCESS-WARNING".equals(resType)) {
			//응답메시지 처리 getxxx
			log.debug("isResponseOk_RequestID = "+ response.getRequestID());
			//ChangedProductOrderInfo[] infos = response.getChangedProductOrderInfoList();  
			//log4j.debug(infos);
			returnResult = Boolean.TRUE;
			
		} else {
			ErrorType error = response.getError();
			log.error(marketNaverConfig.getInquiry().getServiceName() + "_API_Code : "    + error.getCode());
			log.error(marketNaverConfig.getInquiry().getServiceName() + "_API_Message : " + error.getMessage());
			log.error(marketNaverConfig.getInquiry().getServiceName() + "_API_Detail : "  + error.getDetail());
			returnResult = Boolean.FALSE;
		}
		
		if(resType.contains("WARNING")) {
			// :TODO: 경고 발생시 공통 처리 필요
			WarningListType warnList = response.getWarningList();
			if(warnList !=null) {
				WarningType[]  warns = warnList.getWarning();
				if(warns !=null && warns.length > 0) {
					int wLength = warns.length; 
					for(int i=0; i<wLength ; i++) {
						// :TODO: 경고 발생시 어떻게 처리할 것인지 확인 필요
						log.warn("warn "+i+" CODE = " + warns[i].getCode());
						log.warn("warn "+i+" MSG  = " + warns[i].getMessage());
						log.warn("warn "+i+" DTL  = " + warns[i].getDetail());
					}
				}
			}			
		}
		
		return returnResult ;
	}
    
	/**
	 * 판매자 문의 내역 조회
	 * 
	 * @param
	 * @return
	 * @throws RemoteException
	 * @throws ParseException 
	 */
	public GetCustomerInquiryListResponseType getCustomerInquiryList(NaverInquirySetDto naverInquirySetDto) throws RemoteException {
		GetCustomerInquiryListRequestType requestType = new GetCustomerInquiryListRequestType();

		requestType.setAccessCredentials(this.createCustomerInquiryAcType("GetCustomerInquiryList"));
		requestType.setDetailLevel("Full");
		requestType.setVersion(marketNaverConfig.getInquiry().getServiceVersion());
		requestType.setRequestID(UUID.randomUUID().toString());
		requestType.setMallID(marketNaverConfig.getSellerId());
		requestType.setInquiryTimeFrom(DateUtils.convertCalendar(StringUtil.getDisplayLongDate(naverInquirySetDto.getInquiryTimeFrom(),"-")));
		requestType.setInquiryTimeTo(DateUtils.convertCalendar(StringUtil.getDisplayLongDate(naverInquirySetDto.getInquiryTimeTo(),"-")));
		requestType.setServiceType(marketNaverConfig.getInquiry().getServiceType());
		requestType.setIsAnswered(naverInquirySetDto.isAnswered());
		if (!StringUtil.isEmpty(naverInquirySetDto.getInquiryExtraData())) {
			requestType.setInquiryExtraData(naverInquirySetDto.getInquiryExtraData());
		}

		GetCustomerInquiryListRequest request = new GetCustomerInquiryListRequest();
		request.setGetCustomerInquiryListRequest(requestType);
		CustomerInquiryServiceStub customerInquiryServiceStub = new CustomerInquiryServiceStub(marketNaverConfig.getTargetEndpoint() + marketNaverConfig.getInquiry().getServiceName());
		return customerInquiryServiceStub.getCustomerInquiryList(request).getGetCustomerInquiryListResponse();
	}
	
	/**
	 * 판매자 문의 답변 메시지 전송
	 *
	 * @param req
	 * @return
	 * @throws RemoteException
	 * @throws ParseException
	 */
	public AnswerCustomerInquiryResponseType answerCustomerInquiry(NaverInquiryGetDto req) throws RemoteException, ParseException {
		AnswerCustomerInquiryRequestType requestType = new AnswerCustomerInquiryRequestType();
		requestType.setAccessCredentials(this.createCustomerInquiryAcType("AnswerCustomerInquiry"));
		requestType.setRequestID(UUID.randomUUID().toString());
		requestType.setVersion(marketNaverConfig.getInquiry().getServiceVersion());
		requestType.setMallID(marketNaverConfig.getSellerId());
		requestType.setDetailLevel("Full");
		requestType.setServiceType(marketNaverConfig.getInquiry().getServiceType());

		requestType.setInquiryID(req.getMessageNo().toString());
		if (StringUtil.isEmpty(req.getAnswerContentId())) {
			requestType.setActionType("INSERT");
		} else {
			requestType.setActionType("UPDATE");
			requestType.setAnswerContentID(req.getAnswerContentId());
		}
		requestType.setAnswerContent(req.getResponseContents());

		AnswerCustomerInquiryRequest request = new AnswerCustomerInquiryRequest();
		request.setAnswerCustomerInquiryRequest(requestType);
		CustomerInquiryServiceStub customerInquiryServiceStub = new CustomerInquiryServiceStub(marketNaverConfig.getTargetEndpoint() + marketNaverConfig.getInquiry().getServiceName());

		return customerInquiryServiceStub.answerCustomerInquiry(request).getAnswerCustomerInquiryResponse();
	}

	private AccessCredentialsType createCustomerInquiryAcType(String operationName) {
		AccessCredentialsType accessCredentialsType = NaverCryptoUtil.createCustomerInquiryAcType(marketNaverConfig.getInquiry().getServiceName(), operationName);
		this.timeStamp = accessCredentialsType.getTimestamp();
		return accessCredentialsType;
	}

}
