package kr.co.homeplus.outbound.market.eleven.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import kr.co.homeplus.outbound.market.eleven.item.enums.DlvClfType;
import lombok.Getter;
import lombok.Setter;


@XmlAccessorType( XmlAccessType.FIELD )
@XmlRootElement( name = "Product" )
@ApiModel("11번가 단일상품조회")
@Getter
@Setter
public class ProductGetDto {

    @ApiModelProperty(value = "AS안내정보")
    private String asDetail;

    @ApiModelProperty(value = "묶음배송여부 ( Y : 묶음배송/  N : 묶음배송아님)")
    private String bndlDlvCnYn;

    @ApiModelProperty(value = "즉시할인쿠폰 적용여부 (Y : 적용함/N : 적용안함)")
    private String cuponcheck;

    @ApiModelProperty(value = "카테고리 번호")
    private String dispCtgrNo;

    @ApiModelProperty(value ="카테고리 상태코드")
    private String dispCtgrStatCd;

    @ApiModelProperty(value = "교환배송비")
    private String exchDlvCst;

    @ApiModelProperty(value = "상품상세정보(상품등록시 작성하신 상품상세정보 HTML)")
    private String htmlDetail;

    @ApiModelProperty(value = "처리결과 메시지")
    private String message;

    @ApiModelProperty(value = "상품명")
    private String prdNm;

    @ApiModelProperty(value = "상품번호")
    private String prdNo;

    @ApiModelProperty(value = "반품배송비")
    private String rtngdDlvCst;

    @ApiModelProperty(value = "판매가")
    private String selPrc;

    @ApiModelProperty(value = "판매자상태")
    private String selStatCd;

    @ApiModelProperty(value = "판매상태")
    private String selStatNm;

    @ApiModelProperty(value = "판매자상품코드")
    private String sellerPrdCd;

    @ApiModelProperty(value = "배송주체(배송유형)")
    private DlvClfType dlvClf;

    @ApiModelProperty(value = "판매시작일")
    private String aplBgnDy;

    @ApiModelProperty(value = "판매종료일")
    private String aplEndDy;

}
