package kr.co.homeplus.outbound.market.naver.util;

import com.nhncorp.platform.shopn.QuestionAnswerServiceStub;
import com.nhncorp.platform.shopn.customerinquiry.CustomerInquiryServiceStub;
import com.nhncorp.platform.shopn.seller.SellerServiceStub;
import com.nhncorp.psinfra.toolkit.SimpleCryptLib;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import kr.co.homeplus.outbound.market.naver.config.MarketNaverConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class NaverCryptoUtil {

    private final MarketNaverConfig marketNaverConfig;
    private static MarketNaverConfig naverConfig;

    @PostConstruct
    private void initialize() {
        naverConfig = marketNaverConfig;
    }

    public static SellerServiceStub.AccessCredentialsType createSellerAccessCredentialType(String serviceName , String operationName) {
        //서명생성
        try {
            //인증정보
            SellerServiceStub.AccessCredentialsType accessCredentialsType = new SellerServiceStub.AccessCredentialsType();
            accessCredentialsType.setAccessLicense(naverConfig.getAccessLicense());
            accessCredentialsType.setTimestamp(SimpleCryptLib.getTimestamp());
            accessCredentialsType.setSignature(generateSign(accessCredentialsType.getTimestamp(), serviceName, operationName));
            return accessCredentialsType;

        } catch (SignatureException e) {
            //서명정보 실패
            log.error(e.getMessage());
            // :todo: 에러처리
            return null;
        }
    }

    public static CustomerInquiryServiceStub.AccessCredentialsType createCustomerInquiryAcType(String serviceName , String operationName) {
        //서명생성
        try {
            //인증정보
            CustomerInquiryServiceStub.AccessCredentialsType accessCredentialsType = new CustomerInquiryServiceStub.AccessCredentialsType();
            accessCredentialsType.setAccessLicense(naverConfig.getAccessLicense());
            accessCredentialsType.setTimestamp(SimpleCryptLib.getTimestamp());
            accessCredentialsType.setSignature(generateSign(accessCredentialsType.getTimestamp(), serviceName, operationName));
            return accessCredentialsType;

        } catch (SignatureException e) {
            //서명정보 실패
            log.error(e.getMessage());
            // :todo: 에러처리
            return null;
        }
    }

    public static QuestionAnswerServiceStub.AccessCredentialsType createQeuestionAnswerAcType(String serviceName , String operationName) {
        //서명생성
        try {
            //인증정보
            QuestionAnswerServiceStub.AccessCredentialsType accessCredentialsType = new QuestionAnswerServiceStub.AccessCredentialsType();
            accessCredentialsType.setAccessLicense(naverConfig.getAccessLicense());
            accessCredentialsType.setTimestamp(SimpleCryptLib.getTimestamp());
            accessCredentialsType.setSignature(generateSign(accessCredentialsType.getTimestamp(), serviceName, operationName));
            return accessCredentialsType;

        } catch (SignatureException e) {
            //서명정보 실패
            log.error(e.getMessage());
            // :todo: 에러처리
            return null;
        }
    }

    public static byte[] generateEncryptKey(String timeStamp) throws SignatureException {
        return SimpleCryptLib.generateKey(timeStamp, naverConfig.getSecretKey());
    }

    public static String decrypt(String timeStamp, String encryptedData) {
        String decryptedStr = "";
        if (encryptedData == null || encryptedData.isEmpty()) {
            return "";
        }
        try {
            decryptedStr = new String(SimpleCryptLib.decrypt(generateEncryptKey(timeStamp), encryptedData), "UTF-8");
        } catch (Exception e) {
            log.debug("decrypt fail! encryptedData: {}", encryptedData);
            decryptedStr = encryptedData;
        }
        return decryptedStr;
    }

    public static Converter<Object, String> getObjectConverter(String timeStamp) {
        return new Converter<Object, String>() {
            public String convert(MappingContext<Object, String> context) {
                if(context.getSource() instanceof Calendar) {
                    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(((Calendar)context.getSource()).getTime());
                } else {
                    return context.getSource() == null ? null : decrypt(timeStamp, String.valueOf(context.getSource()));
                }
            }
        };
    }

    public static SellerServiceStub createSellerServiceStub(String endPoint, String serviceName) throws Exception {
        return new SellerServiceStub(endPoint.concat(serviceName));
    }

    private static String generateSign(String timeStamp, String serviceName , String operationName) throws SignatureException {
        return SimpleCryptLib.generateSign(generateData(timeStamp, serviceName, operationName), naverConfig.getSecretKey());
    }

    private static String generateData(String timeStamp, String serviceName , String operationName) {
        return timeStamp + serviceName + operationName;
    }
}
