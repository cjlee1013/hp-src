package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("마켓연동 이미지")
public class ItemMarketImgSendGetDto {

    @ApiModelProperty(value = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "판매자ID")
    private String partnerId;

    @ApiModelProperty(value = "이미지 경로1(대표이미지)")
    private String imgUrl1;

    @ApiModelProperty(value = "이미지 경로2")
    private String imgUrl2;

    @ApiModelProperty(value = "이미지 경로3")
    private String imgUrl3;

    @ApiModelProperty(value = "이미지 경로4")
    private String imgUrl4;

    @ApiModelProperty(value = "이미지 경로5")
    private String imgUrl5;

    @ApiModelProperty(value = "이미지 경로6")
    private String imgUrl6;

    @ApiModelProperty(value = "리스팅형 이미지")
    private String listImgUrl;

    @ApiModelProperty(value = "라벨 이미지")
    private String lableImgUrl;

    @ApiModelProperty(value = "전송 여부(Y:전송완료, N:미전송, A:일부전송완료)")
    private String sendStatus;

    @ApiModelProperty(value = "등록자")
    private String regId;

    @ApiModelProperty(value = "등록일시")
    private String regDt;

    @ApiModelProperty(value = "수정자")
    private String chgId;

    @ApiModelProperty(value = "수정일시")
    private String chgDt;

}
