package kr.co.homeplus.outbound.store.service;

import kr.co.homeplus.outbound.market.eleven.item.model.ResultMessageDto;
import kr.co.homeplus.outbound.store.model.eleven.MartStoreListDto;
import kr.co.homeplus.outbound.store.model.eleven.MartStoreResultDto;
import kr.co.homeplus.outbound.store.model.eleven.MartStoreSetDto;
import kr.co.homeplus.outbound.store.model.eleven.StoreMailNoListDto;

public interface StoreSendService {

    /**
     * 11번가 지점조회
     * @param storeId
     * @return
     */
    MartStoreListDto getStoreInfo(String storeId);

    /**
     * 11번가 지점 전체조회
     * @return
     */
    MartStoreListDto getStoreInfoAll();

    /**
     * 11번가 지점 등록
     * @return
     */
    MartStoreResultDto addStoreInfo(MartStoreSetDto martStoreSetDto);

    /**
     * 11번가 지점 등록
     * @return
     */
    ResultMessageDto modifyStoreInfo(MartStoreSetDto martStoreSetDto);

    /**
     * 지점별 우편번호 조회
     * @param strNo
     * @return
     */
    StoreMailNoListDto findStoreMailNoInfo(String strNo);

}

