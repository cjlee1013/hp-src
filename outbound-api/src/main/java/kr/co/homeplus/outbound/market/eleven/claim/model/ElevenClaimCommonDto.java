package kr.co.homeplus.outbound.market.eleven.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "11번가 클레임 조회 기본 DTO")
public class ElevenClaimCommonDto {
    @ApiModelProperty(value = "조회시작일")
    private String startTime;
    @ApiModelProperty(value = "조회종료일")
    private String endTime;
}
