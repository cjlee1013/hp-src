package kr.co.homeplus.outbound.market.eleven.settle.model;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@XmlAccessorType( XmlAccessType.FIELD )
@XmlRootElement( name = "seStlDtlLists", namespace = "http://skt.tmall.business.openapi.spring.service.client.domain/" )
public class ElevenSettleListGetDto extends ElevenCommonResult {
    @XmlElement( name = "seStlDtlList", namespace = "http://skt.tmall.business.openapi.spring.service.client.domain/" )
    List<ElevenSettleItemGetDto> elevenSettleItemGetDtoList;
}
