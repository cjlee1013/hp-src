package kr.co.homeplus.outbound.item.enums;

import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.collect.ImmutableBiMap;
import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 *부가세/면세 코드
 */
@RequiredArgsConstructor
public enum TaxYnType {
	TAXATION( "Y", "과세상품", "01", null),
	TAX_FREE("N",  "면세상품", "02", null),
	ZERO_TAX("Z",  "영세상품", "03", null);

	@Getter
	private final String type;

	@Getter
	private final String desc;

	@Getter
	private final String elevenCd;

	@Getter
	private final String naverCd;

	private static final ImmutableBiMap<TaxYnType, String> biMap;

	static {
		BiMap<TaxYnType, String> map = EnumHashBiMap.create(TaxYnType.class);

		for (TaxYnType tax : values()) {
			map.put(tax, tax.getType());
		}

		biMap = ImmutableBiMap.copyOf(map);
	}

	public static TaxYnType valueFor(final String type) {
		final TaxYnType tax =  biMap.inverse().get(type);

		return tax;
	}

}
