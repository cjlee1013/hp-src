package kr.co.homeplus.outbound.market.naver.settle.model;

import lombok.Data;

@Data
public class NaverSettleSetDto {
  /**
   * periodType(기간 타입)
   * PAY_DATE: 결제완료일 / 취소완료일
   * SETTLE_BASIS_DATE: 정산 기준일(구매 확정이 되는 일자)
   * SETTLE_EXPECT_DATE: 정산 예정일(정산 금액이 입금될 예정일)
   * SETTLE_COMPLETE_DATE: 정산 완료일(정산 금액에 실제로 은행 시스템을 통해 입금 완료 처리된 일자)
   */
  private String periodType;
  private String startDate;
  private String endDate;
  private int pageNumber;
}
