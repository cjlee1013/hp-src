package kr.co.homeplus.outbound.core.constants;

import javax.print.DocFlavor.STRING;

/**
 * 코드 관련 공통 상수
 */
public class CodeConstants {
    // 파트너 사용자 아이디
    public static final String PARTNER_USER_ID = "PARTNER";

	// 시스템 사용자 아이디 (BATCH 용)
	public static final String SYSTEM_USER_ID = "SYSTEM";

    // INBOUND 사용자 아이디
    public static final String INBOUND_USER_ID = "PLUS";

    // OUTBOUND 사용자 아이디
    public static final String OUTBOUND_USER_ID = "MARKET";

    // 상품 > 상품 수수료 map key
    public static final String PROD_SALE_COMMISSION = "commission";

    // 공통 > 시스템 구분자
    public static final String SYS_DIVISION = "∑";

    // 상품 > 금칙어 유효성체크 시 구분자
    public static final String BANNED_WORD_STRING_DIVISION = SYS_DIVISION;

    // 홈플러스 파트너 ID
    public static final String ELEVEN_PARTNER_ID    = "coopeleven";
    public static final String GMARKET_PARTNER_ID   = "coopgmarket";
    public static final String AUCTION_PARTNER_ID   = "coopauction";
    public static final String NMART_PARTNER_ID     = "coopnaver";

    public static final String USE_YN_Y             = "Y";
    public static final String USE_YN_N             = "N";

    //pft_yn
    public static final String PFR_YN_Y                 = "Y";
    public static final String PFR_YN_N                 = "N";

    //전시여부
    public static final String DISPLAY_YN_Y             = "Y";
    public static final String DISPLAY_YN_N             = "N";

    //취급중지
    public static final String STOP_DEAL_YN_Y           = "Y";
    public static final String STOP_DEAL_YN_N           = "N";

    //온라인여부
    public static final String ONLINE_YN_Y              = "Y";
    public static final String ONLINE_YN_N              = "N";

    //가상점단독여부
    public static final String VIRTUAL_STORE_ONLY_YN_Y  = "Y";
    public static final String VIRTUAL_STORE_ONLY_YN_N  = "N";

    //예약여부
    public static final String RSV_YN_Y                 = "Y";
    public static final String RSV_YN_N                 = "N";

    //택배점 취급여부
    public static final String DEAL_YN_Y                = "Y";
    public static final String DEAL_YN_N                = "N";

    // 전송 타입
    public static final String REGULAR                  = "00";
    public static final String SPOT                     = "01";

    // 성인여부
    public static final String ADULT_TYPE_NORMAL        = "NORMAL";

    // 대표점포
    public static final int DEFAULT_STORE_ID            = 37;
    public static final int CLUB_DEFAULT_STORE_ID       = 38;

    //기본 배송정보
    public static final String DEFAULT_FREE_CONDITION  = "40000";
    public static final String DEFAULT_SHIP_FEE        = "3000";
    public static final String DEFAULT_CLAIM_FEE       = "4000";

    //기본 연동정보
    public static final String DEFAULT_MAX_STOCK_QTY   = "99999";
    public static final String NOTHING                 = "없음";
    public static final String ZERO                    = "0";
    public static final String ITEM_REFERENCE_MSG      = "상품 상세설명 참조";

    public static final String ELEVEN_MART_NO          = "6";  //HC

    public static final String LEAF_YN_Y               = "Y";
    public static final String LEAF_YN_N               = "N";

    public static final int    ITEM_SEND_LIMIT_CNT     = 100;
    public static final int    THREAD_DEFAULT_SLEEP    = 1000;

    public static final String RMS_CODE_DELIMITER      = "#";
    public static final String PROMO_CODE_DELIMITER    = "|";
    public static final String COUPON_CODE_DELIMITER   = "_";
    public static final String STORE_CODE_DELIMITER    = "^";

    public static final String STORE_ATTR_SPECIAL      = "SPC";

    //행사상품
    public static final String ELEVEN_EVENT_ICON_TYPE  = "14";
    //전단상품
    public static final String ELEVEN_LEAFLET_ICON_TYPE = "15";
    //복수할인
    public static final String ELEVEN_MULTI_EVENT_ICON_TYPE = "16";

}


