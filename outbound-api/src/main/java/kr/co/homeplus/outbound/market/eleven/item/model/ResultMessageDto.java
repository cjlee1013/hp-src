package kr.co.homeplus.outbound.market.eleven.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@ApiModel("11번가 기본 Response Object")
@Data
@XmlAccessorType( XmlAccessType.FIELD )
@XmlRootElement(name = "ResultMessage")
public class ResultMessageDto {

    @ApiModelProperty(value = "결과코드 (0:성공)")
    @XmlElement(name="result_code")
    protected String resultCode;

    @ApiModelProperty(value = "결과내용")
    @XmlElement(name="result_message")
    protected String resultMessage;
}

