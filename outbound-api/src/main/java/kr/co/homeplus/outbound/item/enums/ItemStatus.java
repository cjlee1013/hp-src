package kr.co.homeplus.outbound.item.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ItemStatus {
        T( "T", "임시저장")
    ,   A("A", "판매중")
    ,   P("P", "일시중지")
    ,   S("S", "영구중지")
    ,   E("E", "판매종료/취급중지")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;

}
