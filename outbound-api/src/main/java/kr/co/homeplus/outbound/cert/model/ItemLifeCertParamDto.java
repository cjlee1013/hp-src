package kr.co.homeplus.outbound.cert.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 생활화학 인증 정보
 */
@Getter
@Setter
@Builder
public class ItemLifeCertParamDto {

	@ApiModelProperty(value = "서비스명")
	private String ServiceName;

	@ApiModelProperty(value = "인증키")
	private String AuthKey;

	@ApiModelProperty(value = "신고번호")
	private String SlfsfcfstNo;
}
