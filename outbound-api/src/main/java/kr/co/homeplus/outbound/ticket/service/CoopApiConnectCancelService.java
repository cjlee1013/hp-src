package kr.co.homeplus.outbound.ticket.service;

import kr.co.homeplus.outbound.core.constants.UriConstants;
import kr.co.homeplus.outbound.core.utility.SetParameter;
import kr.co.homeplus.outbound.core.utility.StringUtil;
import kr.co.homeplus.outbound.escrow.util.EscrowParamKey;
import kr.co.homeplus.outbound.ticket.model.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
//import org.json.JSONObject;
//import org.json.XML;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CoopApiConnectCancelService {

    @Value("${plus.resource-routes.coop.url}")
    private String COOP_URL;
    @Value("${plus.resource-routes.coop-encrypt.url}")
    private String COOP_ENCRYPT_URL;
    @Value("${enc-key.coop.key}")
    private String COOP_KEY_CODE;
    @Value("${enc-key.coop.iv}")
    private String COOP_IV_PASS;

    /**
     * [coop] 발급취소 요청 (티켓쿠폰번호로 단일 취소)
     */
    public TicketClaimInfo cancelTicket(TicketClaimInfo ticketClaimInfo) throws Exception {
        log.info("[coop_claim] 티켓 발급취소 요청 (티켓쿠폰번호로 단일취소) [couponNo_{}]", ticketClaimInfo.getTicketCd());

        // prepare set parameter
        List<SetParameter> setParameterList = Arrays.asList(
            SetParameter.create(EscrowParamKey.TICKET_CODE, COOP_KEY_CODE),
            SetParameter.create(EscrowParamKey.TICKET_PASS, COOP_IV_PASS),
            SetParameter.create(EscrowParamKey.TICKET_COUPON_CODE, ticketClaimInfo.getSellerItemCd()),
            SetParameter.create(EscrowParamKey.TICKET_CLAIM_COUPON_NUMBER, ticketClaimInfo.getTicketCd())
        );

        // connection
        URL url = new URL(COOP_URL + UriConstants.COOP_TICKET_CANCEL + StringUtil.getParameter(setParameterList));
        log.info("[coop_claim] 티켓 발급취소 요청(티켓쿠폰번호로 단일취소) [couponNo_{}]  - 요청 :: {}"
                        , ticketClaimInfo.getTicketCd(), url.toString());

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(HttpMethod.GET.name());

        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (Exception e) {
            log.error("[coop_claim] 티켓 발급취소 요청(티켓쿠폰번호로 단일취소) [couponNo_{}] - 응답 :: {}", ticketClaimInfo.getTicketCd(), ExceptionUtils.getStackTrace(e));
        }
        log.info("[coop_claim] 티켓 발급취소 요청(티켓쿠폰번호로 단일취소) [couponNo_{}] - 응답 :: {}"
                        , ticketClaimInfo.getTicketCd(), stringBuilder.toString());

        TicketClaimInfo resultTicketClaimInfo = new TicketClaimInfo();

        resultTicketClaimInfo.setSellerItemCd(ticketClaimInfo.getSellerItemCd());
        resultTicketClaimInfo.setOrderTicketNo(ticketClaimInfo.getOrderTicketNo());
        resultTicketClaimInfo.setTicketCd(ticketClaimInfo.getTicketCd());

        // JAXB parsing
        StringReader stringReader = new StringReader(stringBuilder.toString());
        JAXBContext jaxbContext = JAXBContext.newInstance(ClaimTicketResponse.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        ClaimTicketResponse claimTicketResponse = (ClaimTicketResponse) unmarshaller.unmarshal(stringReader);

        resultTicketClaimInfo.setResultMsg(claimTicketResponse.getResultMsg());
        resultTicketClaimInfo.setResultCode(claimTicketResponse.getResultCode());
        resultTicketClaimInfo.setCouponNumber(claimTicketResponse.getInData().getCouponNumber());

        log.info("[coop_claim] 티켓 발급취소 요청(티켓쿠폰번호로 단일취소) resultTicketClaimInfo {} ", resultTicketClaimInfo.printParam());

        return resultTicketClaimInfo;
    }
    /**
     * [coop_claim] 티켓 발급취소 (홈플러스 거래번호 기준)
     */
    public TicketClaimInfo cancelTicketByOrderTicketNo(TicketClaimInfo ticketClaimInfo) throws Exception {
        log.info("[coop_claim] 티켓 발급취소 (거래번호 기준) [orderTicketNo_{}] ", ticketClaimInfo.getOrderTicketNo());

        // prepare set parameter
        List<SetParameter> setParameterList = Arrays.asList(
                SetParameter.create(EscrowParamKey.TICKET_CODE, COOP_KEY_CODE),
                SetParameter.create(EscrowParamKey.TICKET_PASS, COOP_IV_PASS),
                SetParameter.create(EscrowParamKey.TICKET_COUPON_CODE, ticketClaimInfo.getSellerItemCd()),
                SetParameter.create(EscrowParamKey.TICKET_SEQ_NUMBER, ticketClaimInfo.getOrderTicketNo())
        );

        // connection
        URL url = new URL(COOP_URL + UriConstants.COOP_TICKET_CANCEL_BY_ORDER_TICKET_NO + StringUtil.getParameter(setParameterList));
        log.info("[coop_claim] 티켓 발급취소 (거래번호 기준) [orderTicketNo_{}] - 요청 :: {}", ticketClaimInfo.getOrderTicketNo(), url.toString());

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(HttpMethod.GET.name());

        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (Exception e) {
            log.error("[coop_claim] 티켓 발급취소 (거래번호 기준) [orderTicketNo_{}]  - 응답 :: {}", ticketClaimInfo.getOrderTicketNo(), ExceptionUtils.getStackTrace(e));
        }
        log.info("[coop_claim] 티켓 발급취소 (거래번호 기준) [orderTicketNo_{}]  - 응답 :: {}", ticketClaimInfo.getOrderTicketNo(), stringBuilder.toString());

        TicketClaimInfo resultTicketClaimInfo = new TicketClaimInfo();

        resultTicketClaimInfo.setSellerItemCd(ticketClaimInfo.getSellerItemCd());
        resultTicketClaimInfo.setOrderTicketNo(ticketClaimInfo.getOrderTicketNo());

        // JAXB parsing
        StringReader stringReader = new StringReader(stringBuilder.toString());
        JAXBContext jaxbContext = JAXBContext.newInstance(ClaimTicketResponse.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        ClaimTicketOrderTicketResponse claimTicketOrderTicketResponse = (ClaimTicketOrderTicketResponse) unmarshaller.unmarshal(stringReader);

        resultTicketClaimInfo.setResultMsg(claimTicketOrderTicketResponse.getResultMsg());
        resultTicketClaimInfo.setResultCode(claimTicketOrderTicketResponse.getResultCode());

        log.info("[coop_claim] 티켓 발급취소 (거래번호 기준) resultTicketClaimInfo {} ", resultTicketClaimInfo.printParam());

        return resultTicketClaimInfo;
    }

    /**
     * [coop_claim] 발급취소 가능여부 확인 (거래번호 기준)
     */
    public TicketClaimInfo cancelTicketCheck(TicketClaimInfo ticketClaimInfo) throws Exception {
        log.info("[coop_claim] 티켓 발급취소 가능여부 확인 (거래번호 기준) [orderTicketNo_{}] ", ticketClaimInfo.getOrderTicketNo());

        // prepare set parameter
        List<SetParameter> setParameterList = Arrays.asList(
                SetParameter.create(EscrowParamKey.TICKET_CODE, COOP_KEY_CODE),
                SetParameter.create(EscrowParamKey.TICKET_PASS, COOP_IV_PASS),
                SetParameter.create(EscrowParamKey.TICKET_COUPON_CODE, ticketClaimInfo.getSellerItemCd()),
                SetParameter.create(EscrowParamKey.TICKET_SEQ_NUMBER, ticketClaimInfo.getOrderTicketNo())
        );

        // connection
        URL url = new URL(COOP_URL + UriConstants.COOP_TICKET_CANCEL_CHECK + StringUtil.getParameter(setParameterList));
        log.info("[coop_claim] 티켓 발급취소 가능여부 확인(거래번호 기준) [orderTicketNo_{}]  - 요청 :: {}", ticketClaimInfo.getOrderTicketNo(), url.toString());

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(HttpMethod.GET.name());

        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (Exception e) {
            log.error("[coop_claim] 티켓 발급취소 가능여부 확인(거래번호 기준) [orderTicketNo_{}]  - 응답 :: {}", ticketClaimInfo.getOrderTicketNo(), ExceptionUtils.getStackTrace(e));
        }
        log.info("[coop_claim] 티켓 발급취소 가능여부 확인(거래번호 기준) [orderTicketNo_{}]  - 응답 :: {}", ticketClaimInfo.getOrderTicketNo(), stringBuilder.toString());

        TicketClaimInfo resultTicketClaimInfo = new TicketClaimInfo();

        resultTicketClaimInfo.setSellerItemCd(ticketClaimInfo.getSellerItemCd());
        resultTicketClaimInfo.setOrderTicketNo(ticketClaimInfo.getOrderTicketNo());
        resultTicketClaimInfo.setTicketCd(ticketClaimInfo.getTicketCd());

        // JAXB parsing
        StringReader stringReader = new StringReader(stringBuilder.toString());
        JAXBContext jaxbContext = JAXBContext.newInstance(ClaimTicketResponse.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        ClaimTicketCheckResponse claimTicketCheckResponse = (ClaimTicketCheckResponse) unmarshaller.unmarshal(stringReader);

        resultTicketClaimInfo.setResultMsg(claimTicketCheckResponse.getResultMsg());
        resultTicketClaimInfo.setResultCode(claimTicketCheckResponse.getResultCode());

        log.info("[coop_claim] 티켓 발급취소 가능여부 확인(거래번호 기준) resultTicketClaimInfo {} ", resultTicketClaimInfo.printParam());

        return resultTicketClaimInfo;
    }

    /**
     * [coop_claim] 티켓 발행 정보 조회 (티켓쿠폰번호 기준)
     * 사용유무를 조회한다.
     */
    public TicketClaimInfo getClaimTicketInfo(TicketClaimInfo ticketClaimInfo) throws Exception {
        log.info("[coop_claim] 티켓 발행 정보 조회 (티켓쿠폰번호 기준) [couponNo_{}] ", ticketClaimInfo.getTicketCd());

        // prepare set parameter
        List<SetParameter> setParameterList = Arrays.asList(
                SetParameter.create(EscrowParamKey.TICKET_CODE, COOP_KEY_CODE),
                SetParameter.create(EscrowParamKey.TICKET_PASS, COOP_IV_PASS),
                SetParameter.create(EscrowParamKey.TICKET_COUPON_CODE, ticketClaimInfo.getSellerItemCd()),
                SetParameter.create(EscrowParamKey.TICKET_COUPON_NUMBER, ticketClaimInfo.getTicketCd())
        );

        // connection
        URL url = new URL(COOP_URL + UriConstants.COOP_GET_TICKET_INFO_BY_COUPON_NUMBER + StringUtil.getParameter(setParameterList));
        log.info("[coop_claim] 티켓 발행 정보 조회 (티켓쿠폰번호 기준) [couponNo_{}] - 요청 :: {}", ticketClaimInfo.getTicketCd(), url.toString());

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(HttpMethod.GET.name());

        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (Exception e) {
            log.error("[coop_claim] 티켓발행 정보 조회 (티켓쿠폰번호 기준) [couponNo_{}] - 응답 :: {}", ticketClaimInfo.getTicketCd(), ExceptionUtils.getStackTrace(e));
        }
        log.info("[coop_claim] 티켓발행 정보 조회 (티켓쿠폰번호 기준 ) [couponNo_{}] - 응답 :: {}", ticketClaimInfo.getTicketCd(), stringBuilder.toString());

        TicketClaimInfo resultTicketClaimInfo = new TicketClaimInfo();

        resultTicketClaimInfo.setSellerItemCd(ticketClaimInfo.getSellerItemCd());
        resultTicketClaimInfo.setOrderTicketNo(ticketClaimInfo.getOrderTicketNo());
        resultTicketClaimInfo.setTicketCd(ticketClaimInfo.getTicketCd());

        // JAXB parsing
        StringReader stringReader = new StringReader(stringBuilder.toString());
        JAXBContext jaxbContext = JAXBContext.newInstance(ClaimTicketInfoResponse.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        ClaimTicketInfoResponse claimTicketInfoResponse = (ClaimTicketInfoResponse) unmarshaller.unmarshal(stringReader);

        resultTicketClaimInfo.setResultMsg(claimTicketInfoResponse.getResultMsg());
        resultTicketClaimInfo.setResultCode(claimTicketInfoResponse.getResultCode());

        if ( "00".equals(claimTicketInfoResponse.getResultCode()) ) {
            resultTicketClaimInfo.setCouponNumber(claimTicketInfoResponse.getCouponNumber());
            resultTicketClaimInfo.setUseYn(claimTicketInfoResponse.getUseYn());
            resultTicketClaimInfo.setUseYn(claimTicketInfoResponse.getUseYn());
            resultTicketClaimInfo.setUseDate(claimTicketInfoResponse.getUseDate());
            resultTicketClaimInfo.setUsePlace(claimTicketInfoResponse.getUsePlace());
            resultTicketClaimInfo.setSendHp(claimTicketInfoResponse.getSendHp());
            resultTicketClaimInfo.setUseStart(claimTicketInfoResponse.getUseStart());
            resultTicketClaimInfo.setUseEnd(claimTicketInfoResponse.getUseEnd());
            resultTicketClaimInfo.setInsDate(claimTicketInfoResponse.getInsDate());
            resultTicketClaimInfo.setFiller01(claimTicketInfoResponse.getFiller01());
            resultTicketClaimInfo.setFiller02(claimTicketInfoResponse.getFiller02());
            resultTicketClaimInfo.setFiller03(claimTicketInfoResponse.getFiller03());
        }

        log.info("[coop_claim] 티켓 발행 정보 조회 (티켓쿠폰번호 기준) resultTicketClaimInfo {} ", resultTicketClaimInfo.printParam());

        return resultTicketClaimInfo;
    }

    /**
     * [coop_claim] 티켓 발행 정보 조회 (거래번호 기준)
     */
    public TicketClaimInfo getClaimTicketInfoByOrderTicketNo(TicketClaimInfo ticketClaimInfo) throws Exception {
        log.info("[coop_claim] 티켓 발행 정보 조회 (거래번호 기준) [orderTicketNo_{}]", ticketClaimInfo.getOrderTicketNo());

        // prepare set parameter
        List<SetParameter> setParameterList = Arrays.asList(
                SetParameter.create(EscrowParamKey.TICKET_CODE, COOP_KEY_CODE),
                SetParameter.create(EscrowParamKey.TICKET_PASS, COOP_IV_PASS),
                SetParameter.create(EscrowParamKey.TICKET_COUPON_CODE, ticketClaimInfo.getSellerItemCd()),
                SetParameter.create(EscrowParamKey.TICKET_SEQ_NUMBER, ticketClaimInfo.getOrderTicketNo())
        );

        // connection
        URL url = new URL(COOP_URL + UriConstants.COOP_GET_TICKET_INFO + StringUtil.getParameter(setParameterList));
        log.info("[coop_claim] 티켓 발행 정보 조회 (거래번호 기준) [orderTicketNo_{}] - 요청 :: {}", ticketClaimInfo.getOrderTicketNo(), url.toString());

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(HttpMethod.GET.name());

        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (Exception e) {
            log.error("[coop_claim] 티켓 발행 정보 조회 (거래번호 기준) [orderTicketNo_{}] - 응답 :: {}", ticketClaimInfo.getOrderTicketNo(), ExceptionUtils.getStackTrace(e));
        }
        log.info("[coop_claim] 티켓 발행 정보 조회 (거래번호 기준) [orderTicketNo_{}] - 응답 :: {}", ticketClaimInfo.getOrderTicketNo(), stringBuilder.toString());

        TicketClaimInfo resultTicketClaimInfo = new TicketClaimInfo();
        resultTicketClaimInfo.setSellerItemCd(ticketClaimInfo.getSellerItemCd());
        resultTicketClaimInfo.setOrderTicketNo(ticketClaimInfo.getOrderTicketNo());

        // JAXB parsing
        StringReader stringReader = new StringReader(stringBuilder.toString());
        JAXBContext jaxbContext = JAXBContext.newInstance(ClaimTicketInfoOrderTicketResponse.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        ClaimTicketInfoOrderTicketResponse claimTicketInfoOrderTicketResponse = (ClaimTicketInfoOrderTicketResponse) unmarshaller.unmarshal(stringReader);

        resultTicketClaimInfo.setResultMsg(claimTicketInfoOrderTicketResponse.getResultMsg());
        resultTicketClaimInfo.setResultCode(claimTicketInfoOrderTicketResponse.getResultCode());

         if ( "00".equals(claimTicketInfoOrderTicketResponse.getResultCode()) ) {
            ClaimTicketInfoOrderTicketResponse.CouponInfo.DownloadCoupon downloadCoupon= claimTicketInfoOrderTicketResponse.getDownloadCoupon();

            resultTicketClaimInfo.setCouponNumber(downloadCoupon.getCouponNumber());
            resultTicketClaimInfo.setTicketCd(downloadCoupon.getCouponNumber());
            resultTicketClaimInfo.setUseYn(downloadCoupon.getUseYn());
            resultTicketClaimInfo.setUseDate(downloadCoupon.getUseDate());
            resultTicketClaimInfo.setUsePlace(downloadCoupon.getUsePlace());
            resultTicketClaimInfo.setSendHp(downloadCoupon.getSendHp());
            resultTicketClaimInfo.setUseStart(downloadCoupon.getUseStart());
            resultTicketClaimInfo.setUseEnd(downloadCoupon.getUseEnd());
            resultTicketClaimInfo.setInsDate(downloadCoupon.getInsDate());
            resultTicketClaimInfo.setFiller01(downloadCoupon.getFiller01());
            resultTicketClaimInfo.setFiller02(downloadCoupon.getFiller02());
            resultTicketClaimInfo.setFiller03(downloadCoupon.getFiller03());
            resultTicketClaimInfo.setPinNumber(downloadCoupon.getPinNumber());
        }

        log.info("[coop_claim] 티켓 발행 정보 조회 (거래번호 기준) resultTicketClaimInfo {} ", resultTicketClaimInfo.printParam());

        return resultTicketClaimInfo;
    }
}

