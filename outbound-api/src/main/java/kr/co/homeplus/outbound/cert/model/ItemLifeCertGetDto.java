package kr.co.homeplus.outbound.cert.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 생활화학인증 정보
 */
@Data
public class ItemLifeCertGetDto {

	@ApiModelProperty(value = "결과(총건수)")
	private int count;

	@ApiModelProperty(value = "결과코드")
	private String resultcode;

	@ApiModelProperty(value = "메시지")
	private String msg;

	@ApiModelProperty(value = "현재페이지넘버")
	private String pagenum;

	@ApiModelProperty(value = "한페이지결과수")
	private String pagesize;

	@ApiModelProperty(value = "detail")
	ItemLifeCertDetailGetDto row;

}
