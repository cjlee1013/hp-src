package kr.co.homeplus.outbound.core.constants;

/**
 * 제휴사 연동 URI
 */
public class ElevenUriConstants {

    //카테고리
    public final static String ELEVEN_CATEGORY = "/cateservice/category";

    //지점
    public final static String SELECT_STORE_ALL = "/martservices/store";
    public final static String SELECT_STORE = "/martservices/store/{sellerStrCd}";

    public final static String SET_STORE = "/martservices/store";

    //상품
    public final static String SELECT_PRODUCT = "/prodmarketservice/prodmarket/{prdNo}";

    public final static String UPDATE_STOPDISPLAY = "/prodstatservice/stat/stopdisplay/{prdNo}";
    public final static String UPDATE_RESTARTDISPLAY = "/prodstatservice/stat/restartdisplay/{prdNo}";

    public final static String INSERT_PRODUCT = "/prodservices/product";
    public final static String UPDATE_PRODUCT =  "/prodservices/product/{prdNo}";

    //지점정보
    public final static String STORE_PRICE_LIST = "/martservices/storeproductlist";
    public final static String STORE_MAIN_NO_LIST = "/martservices/storeMailNoList/{strNo}/{mailNoUseYn}";

}

