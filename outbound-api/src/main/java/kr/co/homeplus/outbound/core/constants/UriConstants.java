package kr.co.homeplus.outbound.core.constants;

/**
 * 제휴사 연동 URI
 */
public class UriConstants {

    public final static String GMARKET_CATEGORY = "/Category/Category.xml";
    public final static String AUCTION_CATEGORY = "/category/categories.xml";

    /**
     * 티켓발급 - 단일발급(유효기간종료일 회신, 판매가 추가)
     */
    public final static String COOP_CREATE_TICKET = "/serviceapi.asmx/ServiceCouponCreate_02";
    /**
     * 재발송(거래번호 기준)
     */
    public final static String COOP_SEND_TICKET = "/serviceapi.asmx/ServiceCouponSendSeq";
    /**
     * 티켓 발행 정보 조회(거래번호기준 - 기본)
     */
    public final static String COOP_GET_TICKET_INFO = "/serviceapi.asmx/ServiceDownloadInfo";
    /**
     * 발급취소(티켓쿠폰번호로 단일 취소)
     */
    public final static String COOP_TICKET_CANCEL = "/serviceapi.asmx/ServiceCouponCancel";
    /**
     * 발급취소(거래번호로 단일 취소)
     */
    public final static String COOP_TICKET_CANCEL_BY_ORDER_TICKET_NO = "/serviceapi_02.asmx/ServiceCancelSeq";
    /**
     * 발급취소 가능여부 확인(거래번호 기준)
     */
    public final static String COOP_TICKET_CANCEL_CHECK = "/serviceapi_02.asmx/ServiceCancelSeqYN";
    /**
     * 쿠폰 발행 정보 조회(티켓쿠폰번호기준 - 기본)
     */
    public final static String COOP_GET_TICKET_INFO_BY_COUPON_NUMBER = "/serviceapi.asmx/ServiceDownloadCouponInfo";

    /**
     * 11번가 주문수집
     */
    public final static String ELEVEN_GET_ORDER_COMPLETE = "/ordservices/martcomplete";

    /**
     * 11번가 문의 수집
     */
    public final static String ELEVEN_GET_INQUIRY_LIST = "/prodqnaservices/prodqnalist/{startTime}/{endTime}/{answerStatus}";

    /**
     * 11번가 문의 답변
     */
    public final static String ELEVEN_SET_INQUIRY_ANSWER = "/prodqnaservices/prodqnaanswer/{brdInfoNo}/{prdNo}";

    /**
     * 11번가 긴급알리미 수집
     */
    public final static String ELEVEN_GET_INQUIRY_ALIMI_LIST = "/alimi/getalimilist/{startTime}/{endTime}";

    /**
     * 11번가 긴급알리미 답변
     */
    public final static String ELEVEN_SET_INQUIRY_ALIMI_ANSWER = "/alimi/alimianswer/{emerNtceSeq}/{confimYn}/{answerCtnt}";
}
