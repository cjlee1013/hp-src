package kr.co.homeplus.outbound.core.utility.xml;

import java.io.IOException;
import java.io.Writer;

/**
 * <pre>
 * Class Name : JaxbCharacterEscapeHandler.java
 * Description : Jaxb Character Escape
 *
 *  Modification Information
 *  Modify Date    Modifier    Comment
 *  -----------------------------------------------
 *  2015. 1. 28.    yhkim       신규작성
 *
 * </pre>
 *
 * @author yhkim
 * @since 2015. 1. 28.
 */
public class JaxbCharacterEscapeHandler implements com.sun.xml.bind.marshaller.CharacterEscapeHandler {
    @Override
    public void escape(char[] ch, int start, int length, boolean isAttVal, Writer out)
        throws IOException {
        out.write(ch, start, length);
    }
}

