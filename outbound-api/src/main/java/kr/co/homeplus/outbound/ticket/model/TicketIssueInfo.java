package kr.co.homeplus.outbound.ticket.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "발급대상 티켓정보")
@Getter
@Setter
public class TicketIssueInfo {

    /** 내부 관리 항목 */
    @ApiModelProperty(value = "주문구매번호", position = 1)
    private Long purchaseOrderNo;
    @ApiModelProperty(value = "번들번호", position = 1)
    private Long bundleNo;
    @ApiModelProperty(value = "배송번호", position = 1)
    private Long shipNo;
    @ApiModelProperty(value = "회원번호", position = 1)
    private Long userNo;


    /** 전송 항목 */
    @ApiModelProperty(value = "주문티켓번호(발급거래번호)", position = 1)
    private Long orderTicketNo;
    @ApiModelProperty(value = "주문상품번호", position = 1)
    private Long orderItemNo;
    @ApiModelProperty(value = "업체상품코드(연동상품번호)", position = 1)
    private String sellerItemCd;
    @ApiModelProperty(value = "받는사람 연락처", position = 1)
    private String shipMobileNo;
    @ApiModelProperty(value = "티켓상태미발급(T0) / 사용가능(T1) / 사용완료(T2) / 취소요청(T3) / 취소완료(T4) ", position = 1)
    private String ticketStatus;
    @ApiModelProperty(value = "발급상태 발급대기(I1) / 발급중(I2) / 발급완료(I3) / 발급취소(I4) / 발급실패(I8) / 취소실패(I9)", position = 1)
    private String issueStatus;
    @ApiModelProperty(value = "발급요청횟수", position = 1)
    private int issueReqCnt;
    @ApiModelProperty(value = "발급채널(COOP:쿠프마케팅...)", position = 1)
    private String issueChannel;
    @ApiModelProperty(value = "티켓전송횟수(SMS 재전송 횟수)", position = 1)
    private int ticketSendCnt;

    /** 응답 항목 */
    @ApiModelProperty(value = "티켓코드(발급시스템을 통해 부여)", position = 1)
    private String ticketCd;
    @ApiModelProperty(value = "발급리턴코드", position = 1)
    private String issueReturnCd;
    @ApiModelProperty(value = "발급리턴메시지", position = 1)
    private String issueReturnMsg;
    @ApiModelProperty(value = "핀번호(모바일발송코드)", position = 1)
    private String ticketPinCd;
    @ApiModelProperty(value = "유효기간 종료일", position = 1)
    private String useEndDay;
    @ApiModelProperty(value = "연동 Password", position = 1)
    private String connectPassword;

}
