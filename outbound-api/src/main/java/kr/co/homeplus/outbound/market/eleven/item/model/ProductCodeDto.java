package kr.co.homeplus.outbound.market.eleven.item.model;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import kr.co.homeplus.outbound.core.utility.xml.AdapterXmlCDATA;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType( XmlAccessType.FIELD )
@XmlRootElement( name = "item" )
public class ProductCodeDto  {

    private String code;

    @XmlJavaTypeAdapter( AdapterXmlCDATA.class )
    private String name;

}