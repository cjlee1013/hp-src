package kr.co.homeplus.outbound.safety.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
@Setter
@ConfigurationProperties(prefix = "safen")
public class SafetyConfig {
    private String corpCode;
    private String groupCode;
}
