package kr.co.homeplus.outbound.item.enums;

import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.collect.ImmutableBiMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.logstash.logback.encoder.org.apache.commons.lang3.ObjectUtils;

/**
 * 상품인증 유형
 * sendYn ='Y'인 것만 마켓에 전송함.
 * */
@RequiredArgsConstructor
public enum ItemCertType {

	IT_SAFETY_CT("IT_SAFETY_CT", "Y","102",  "", "안전인증")
	, IT_SAFETY_CK("IT_SAFETY_CK", "N","104", "","안전확인신고")
	, IT_SPL("IT_SPL", "N","","","공급자적합성확인")
	, IT_D("IT_D" , "N" , "","","상품상세 설명 참고")

	, KD_SAFETY_CT("KD_SAFETY_CT","Y","", "128", "안전인증")
	, KD_SAFETY_CK("KD_SAFETY_CK","Y", "","129", "안전확인신고")
	, KD_SPL("KD_SPL" ,"N" ,"","","공급자적합성확인")
	, KD_D("KD_D", "N","" ,"","상품상세 설명 참고")


	, ET_LF_CH_ITEM("ET_LF_CH_ITEM","N","", "101", "안전확인대상 생활화학제품")
	, ET_MEDI("ET_MEDI", "N","" ,"", "의료기기")
	, ER_SPL("ER_SPL", "Y","105","", "전자파적합성")
	, ET_CER("ET_CER","N", "" ,"","소비효율 등급")
	, ET_CT("ET_CT" , "N","" , "","기타 인증")
	, ET_D("ET_D", "Y","131", "","상품상세 설명 참고")
	, ER_D("ER_D", "N","" ,"","상품상세 설명 참고");

	@Getter
	private final String type;

	@Getter
	private final String sendYn;

	@Getter
	private final String elevenCd;

	@Getter
	private final String naverCd;

	@Getter
	private final String desc;

	@Getter
	private static final ImmutableBiMap<ItemCertType, String> biMap;

	static {
		BiMap<ItemCertType, String> map = EnumHashBiMap.create(ItemCertType.class);

		for (ItemCertType cert : values()) {
			map.put(cert, cert.name());
		}

		biMap = ImmutableBiMap.copyOf(map);
	}

	public static ItemCertType valueFor(final String type) {
		final ItemCertType cert = biMap.inverse().get(type);

		return ObjectUtils.isNotEmpty(cert) ? cert : ItemCertType.ER_D;
	}

}