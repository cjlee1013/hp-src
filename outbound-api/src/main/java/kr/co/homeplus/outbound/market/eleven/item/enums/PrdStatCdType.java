package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

/**
 * 상품상태
 */
public enum PrdStatCdType {
	@XmlEnumValue("01")
	NEW("01", "새상품"),

	@XmlEnumValue("02")
	USED("02", "중고상품"),

	@XmlEnumValue("03")
	STOCKED("03", "반품/재고상품");

	@Getter
	private String code;
	@Getter
	private String desc;

	private PrdStatCdType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}
