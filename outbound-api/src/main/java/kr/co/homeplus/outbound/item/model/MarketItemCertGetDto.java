package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 인증 정보
 */
@Getter
@Setter
@ToString
public class MarketItemCertGetDto {

	@ApiModelProperty(value = "인증그룹 (공통코드 : item_cert_group) - (제품안전인증:IT, 어린이제품:KD, 전파인증:ER, 기타인증:ET)")
	private String	certGroup;

	@ApiModelProperty(value = "인증그룹명")
	private String	certGroupNm;

	@ApiModelProperty(value = "인증유형 (공통코드 : item_cert_type)")
	private String 	certType;

	@ApiModelProperty(value = "인증유형명")
	private String 	certTypeNm;

	@ApiModelProperty(value = "인증번호")
	private String 	certNo;

	@ApiModelProperty(value = "링크여부")
	private String linkYn;

}
