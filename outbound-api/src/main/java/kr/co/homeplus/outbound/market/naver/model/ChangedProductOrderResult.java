package kr.co.homeplus.outbound.market.naver.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ChangedProductOrderResult {
    private boolean hasMoreData;
    private String fromDt;
}
