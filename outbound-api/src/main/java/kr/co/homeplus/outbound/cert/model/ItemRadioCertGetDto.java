package kr.co.homeplus.outbound.cert.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import lombok.Data;

/**
 * 적합성 전파인증 정보
 */
@Data
@XmlAccessorType( XmlAccessType.FIELD )
public class ItemRadioCertGetDto {
	private String resultMsg;
	private String resultCode;
	private String authYn;
}
