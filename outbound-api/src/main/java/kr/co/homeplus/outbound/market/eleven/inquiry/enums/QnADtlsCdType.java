package kr.co.homeplus.outbound.market.eleven.inquiry.enums;

public enum QnADtlsCdType {
    ITEM( "01", "상품" ),
    SHIPPING( "02", "배송" ),
    REFUND( "03", "반품/취소" ),
    EXCHANGE( "04", "교환/변경" ),
    OTHER( "05", "기타" );

    private String code;
    private String desc;

    public String getDesc() {
        return desc;
    }

    public String getCode() {
        return code;
    }

    QnADtlsCdType( String code, String desc ) {
        this.desc = desc;
        this.code = code;
    }

    public static QnADtlsCdType getTypeOf( String dtlsNm ) {
        for ( QnADtlsCdType dtlsCd : QnADtlsCdType.values() ) {
            if ( dtlsCd.getDesc().equals( dtlsNm ) ) {
                return dtlsCd;
            }
        }
        throw new IllegalArgumentException( "일치하는 타입이 없습니다." );
    }
}
