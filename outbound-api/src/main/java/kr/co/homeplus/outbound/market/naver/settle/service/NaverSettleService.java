package kr.co.homeplus.outbound.market.naver.settle.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.outbound.market.naver.config.MarketNaverConfig;
import kr.co.homeplus.outbound.market.naver.settle.model.NaverSettleDetailGetDto;
import kr.co.homeplus.outbound.market.naver.settle.model.NaverSettleListGetDto;
import kr.co.homeplus.outbound.market.naver.settle.model.NaverSettleResponseGetDto;
import kr.co.homeplus.outbound.market.naver.settle.model.NaverSettleSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
@RequiredArgsConstructor
public class NaverSettleService {

    private final MarketNaverConfig marketNaverConfig;
    private final RestTemplate restTemplate;

    public NaverSettleListGetDto getSettleList(NaverSettleSetDto naverSettleSetDto) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String url = getSettlementUrl(naverSettleSetDto);
        try {
            ResponseEntity<NaverSettleResponseGetDto> responseEntity = connect(url);
            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                NaverSettleResponseGetDto naverSettleResponseGetDto = mapper.convertValue(
                        responseEntity.getBody(), NaverSettleResponseGetDto.class);
                if (naverSettleResponseGetDto != null) {
                    return naverSettleResponseGetDto.getBody();
                }
            }
        } catch (Exception e) {
            log.error("Naver-getSettleList:"+e.getMessage());
        }
        return null;
    }

    private String getSettlementUrl(NaverSettleSetDto naverSettleSetDto) {
        String host = marketNaverConfig.getApis().getHost();
        String path = marketNaverConfig.getApis().getSettlementPath();
        String params =
            "?periodType=" + naverSettleSetDto.getPeriodType()
            + "&startDate=" + naverSettleSetDto.getStartDate()
            + "&endDate=" + naverSettleSetDto.getEndDate()
            + "&pageNumber=" + naverSettleSetDto.getPageNumber();
        return host + path + params;
    }

    private <T> ResponseEntity<T> connect(String url) {
        HttpMethod method = HttpMethod.GET;
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.set("X-Naver-API-Key", marketNaverConfig.getApis().getApiKey());
        HttpEntity httpEntity = new HttpEntity<>(httpHeaders);
        return restTemplate.exchange(url, method,httpEntity,new ParameterizedTypeReference<T>(){});
    }
}
