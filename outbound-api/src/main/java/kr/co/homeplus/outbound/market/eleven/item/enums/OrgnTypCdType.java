package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

/**
 * 원산지 코드
 */
public enum OrgnTypCdType {
	@XmlEnumValue("01")
	DOMESTIC("01", "국내"),

	@XmlEnumValue("02")
	ABROAD("02", "국외"),

	@XmlEnumValue("03")
	ETC("03", "기타");

	@Getter
	private String code;
	@Getter
	private String desc;

	private OrgnTypCdType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}
