package kr.co.homeplus.outbound.store.model.eleven;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreMailNoRestVO {

	/**
	 * 우편번호
	 */
	private String mailNo;

	/**
	 * 지점번호
	 */
	private String strNo;

	/**
	 * 권역코드
	 */
	private String areaCd;

	/**
	 * 사용여부
	 */
	private String useYn;

}
