package kr.co.homeplus.outbound.market.eleven.claim.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "11번가 클레임 승인 DTO")
public class ElevenClaimApproveDto {
    private String ordPrdCnSeq;
    private String ordNo;
    private String ordPrdSeq;
}
