package kr.co.homeplus.outbound.item.constants;

/**
 * 행사 관련 공통 상수
 */
public class EventConstants {

    // 행사 유형
    public static final String PROMO_TYPE_SIMPLE = "SIMP";
    public static final String PROMO_TYPE_MIXMATCH = "MIX";
    public static final String PROMO_TYPE_THRESHOLD = "THRES";

    // 행사 종류
    public static final String EVENT_KIND_BASIC = "BASIC";
    public static final String EVENT_KIND_PICK = "PICK";
    public static final String EVENT_KIND_TOGETHER = "TOGETHER";
    public static final String EVENT_KIND_INTERVAL = "INTERVAL";
    public static final String EVENT_KIND_GIFT = "GIFT";
    public static final String EVENT_KIND_ADD = "ADD";

    // 할인 유형
    public static final String CHANGE_TYPE_RATE = "0";  // 할인율(%)
    public static final String CHANGE_TYPE_PRICE = "1";  // 할인금액
    public static final String CHANGE_TYPE_FIXED = "2";  // 고정금액(ex:3개 구매시 9,900원)

    // MIX&MATCH 할인 유형
    public static final String MIX_TYPE_QTY = "1";  // 구매수량
    public static final String MIX_TYPE_AMT = "2";  // 구매금액

    // THRESHOLD 할인 유형
    public static final String THRES_TYPE_QTY = "1";  // 구매수량
    public static final String THRES_TYPE_AMT = "0";  // 구매금액

    // Y/N
    public static final String CODE_Y = "Y";
    public static final String CODE_N = "N";

    // 영역
    public static final String AREA_STICKER = "STICKER";
    public static final String AREA_LABEL = "LABEL";

    // 행사 스티커 Text
    public static final String STICKER_BASIC = "행사상품";
    public static final String STICKER_TOGETHER = "함께할인";
    public static final String STICKER_GIFT = "사은품";
    public static final String STICKER_ADD = "덤";

    // 점포 유형
    public static final String STORE_DS = "DS";

    // 점포 형태
    public static final String STORE_KIND_NOR = "NOR";
    public static final String STORE_KIND_DLV = "DLV";
    public static final String STORE_KIND_SHORT_NOR = "N";

    // 대상상품 적용 성격
    public static final String ITEM_BUY = "BUY";
    public static final String ITEM_GET = "GET";

    // 검색 URL 분류
    public static final String BY_ITEM = "by-id";
    public static final String BY_SUPPLIER = "by-supplier";

    // 점포 유형
    public static final String STORE_TYPE_HYPER = "HYPER";
    public static final String STORE_TYPE_CLUB = "CLUB";
    public static final String STORE_TYPE_EXPRESS = "EXP";
    public static final String STORE_TYPE_DS = "DS";
    public static final String STORE_TYPE_AURORA = "AURORA";
    public static final String STORE_TYPE_SHORT_DS = "D";

    // 사이트 유형
    public static final String SITE_TYPE_HOME = "HOME";
    public static final String SITE_TYPE_CLUB = "CLUB";

    // device type
    public static final String DEVICE_PC = "PC";
    public static final String DEVICE_APP = "APP";
    public static final String DEVICE_MWEB = "MWEB";

    // paging
    public static final int PER_PAGE = 20;

    // client Type
    public static final String CLIENT_TYPE_FRONT = "FRONT";
    public static final String CLIENT_TYPE_ESCROW = "ESCROW";

    // ers event target type
    public static final String ERS_TARGET_TYPE_ITEM = "13";
    public static final String ERS_TARGET_TYPE_ITEM_PRICE = "23";
    public static final String ERS_TARGET_TYPE_SUPPLIER_PRICE = "21";
    public static final String ERS_TARGET_TYPE_SUPPLIER_QTY = "31";

    //coupon discount_type
    public static final String DISCOUNT_TYPE_RATE = "1";
    public static final String DISCOUNT_TYPE_PRICE = "2";

    //promo custom_code
    public static final String SIMPLE = "B";
    public static final String PICK = "P";
    public static final String TOGHTHER = "T";
}
