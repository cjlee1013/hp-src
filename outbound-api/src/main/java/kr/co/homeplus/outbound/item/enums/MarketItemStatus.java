package kr.co.homeplus.outbound.item.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum MarketItemStatus {
    A("A", "판매중")
    ,   P("P", "연동중")
    ,   S("S", "미연동")
    ,   E("E", "연동실패")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;

}
