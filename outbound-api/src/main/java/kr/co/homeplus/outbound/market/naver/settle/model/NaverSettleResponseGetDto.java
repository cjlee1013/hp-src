package kr.co.homeplus.outbound.market.naver.settle.model;

import java.beans.ConstructorProperties;
import lombok.Data;

@Data
public class NaverSettleResponseGetDto {
  private String code;
  private String message;
  private NaverSettleListGetDto body;

  @ConstructorProperties({"code","message","body"})
  public NaverSettleResponseGetDto(String code,String message,NaverSettleListGetDto body) {
    this.code = code;
    this.message = message;
    this.body = body;
  }
}
