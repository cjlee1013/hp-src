package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

/**
 * 축산물 이력번호
 */
public enum BeefTraceStatType {

    @XmlEnumValue( "01" )
    YES( "01", "이력번호 표시 대상" ),

    @XmlEnumValue( "02" )
    NO( "02", "이력번호 표시 대상 아님" ),

    @XmlEnumValue( "03" )
    ETC( "03", "상세설명 참조" );

    @Getter
    private String code;

    @Getter
    private String desc;

    private BeefTraceStatType(String code, String desc) {
        this.code= code;
        this.desc= desc;
    }
}
