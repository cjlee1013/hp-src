package kr.co.homeplus.outbound.item.model.naver;

import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <pre>
 * Class Name : StoStoreInfo.java
 * Description : 상품 가격 정보 목록 조회
 *
 *  Modification Information
 *  Modify Date    Modifier    Comment
 *  -----------------------------------------------
 *  2021. 5. 27.   jh6108.kim   신규작성
 * </pre>
 *
 * @author jh6108.kim
 * @since 2021. 5. 27.
 */
@Getter
@Setter
@ToString
public class StoStoreInfo {

    private static final long serialVersionUID = -6776082604809062991L;

    /**
     * column name : STORE_ID 점포ID
     */
    private String storeId;

    /**
     * column name : GOOD_ID 상품ID 리스트
     */
    private List<String> storeIdList;

    /**
     * column name : STORE_NM 점포명
     */
    private String storeNm;

    /**
     * column name : STORE_CD 점포타입 1 : 직배 2 : 택배
     */
    private String storeCd;

    /**
     * column name : PICKUP_YN 픽업여부
     */
    private String pickupYn;

    /**
     * column name : ZIPNUM_CD 우편번호
     */
    private String zipnumCd;

    /**
     * column name : ADDRTXT_1 주소1
     */
    private String addrtxt1;

    /**
     * column name : ADDRTXT_2 주소2
     */
    private String addrtxt2;

    /**
     * column name : TEL_TXT 전화번호
     */
    private String telTxt;

    /**
     * column name : FAX_TXT FAX번호
     */
    private String faxTxt;

    /**
     * column name : STATUS_CD 상태 1 : ACTIVE 2 :DISABLE 3 :DELETE
     */
    private String statusCd;

    /**
     * column name : REGSTAFF_ID 등록자
     */
    private String regstaffId;

    /**
     * column name : REG_DT 등록일시
     */
    private Date regDt;

    /**
     * column name : MODSTAFF_ID 수정자
     */
    private String modstaffId;

    /**
     * column name : MOD_DT 수정일시
     */
    private Date modDt;

    /**
     * column name : UPSTORE_ID 상위점포ID(즉 거점점포 아이디)
     */
    private String upstoreId;

    /**
     * column name : TSTORE_ID 거점점포의 대표점포ID
     */
    private String tstoreId;

    /**
     * column name : BASESTORE_YN 거점점포 여부
     */
    private String basestoreYn;

    /**
     * column name : CORP_CD 코드 테이블 57000 참조
     */
    private String corpCd;

    /**
     * column name : STORE_NM2 null
     */
    private String storeNm2;

    /**
     * column name : STORE_NM3 null
     */
    private String storeNm3;

    /**
     * column name : GMARKBRANCH_CD GMARKET 제휴처 코드
     */
    private String gmarkbranchCd;

    /**
     * column name : DMSPROC_CD DMS 실행 구분 1 : OLD 2 : NEW, 3: VSS-DMS
     */
    private String dmsprocCd;

    /**
     * column name : AREA_CODE KPI 관련 지역코드
     */
    private String areaCode;

    /**
     * column name : AREA_SORT KPI 관련 지역순번
     */
    private Integer areaSort;

    /**
     * column name : STORE_FORM STORE FROMATION 1: 단층식 2: 복층식 KPI 계산시 사용됨
     */
    private String storeForm;

    /**
     * column name : VAN_CNT KPI 관련 밴수
     */
    private Integer vanCnt;

    /**
     * column name : RMS_SEND_YN null
     */
    private String rmsSendYn;

    /**
     * column name : PCS_YN null
     */
    private String pcsYn;

    /**
     * column name : SLOT_CHANGE_YN 'Y'이면 2시간 SLOT, 'N'이면 기존 SLOT
     */
    private String slotChangeYn;

    /**
     * column name : SIDO_TXT 시/도 구분
     */
    private String sidoTxt;

    /**
     * column name : GUGUN_TXT 구/군 구분
     */
    private String gugunTxt;

    /**
     * column name : ONLINE_YN 온라인 점포여부 Y:온라인 점포 N:오프라인점포
     */
    private String onlineYn;

    /**
     * column name : ELEVENST_STORE_NO 11번가에 등록된 점포번호
     */
    private String elevenstStoreNo;

    private String flexibleYn;

    /**
     * column name : REPLACE_ENABLE_YN 대체가능여부 Y:가능 N:불가
     */
    private String replaceEnableYn;

    /**
     * column name : FCSTORE_YN
     * FC점포여부(Y : FC점포)
     */
    private String fcstoreYn;


    /**
     * column name : CLOSEDAY_SEQ
     * 점포휴무일순번
     */
    private String closedaySeq;

    /**
     * column name : CLOSE_DT
     * 점포휴무일
     */
    private String closeDt;

    /**
     * column name : USE_SATUS
     * 점포휴무일 사용여부
     */
    private String useStatus;

    /**
     * column name : STORE_CLOSE_STATUS
     * 점포휴무여
     */
    private String storeCloseStatus;

    /**
     * 배송유형
     */
    private String deliverType;

    /**
     * 권역
     */
    private long areaNo = 0;

    /**
     * column name : VAN_SLOT_YN
     * 순환출차여부
     */
    private String vanSlotYn;

}