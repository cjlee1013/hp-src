package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

/**
 * 판매상태
 */
public enum SelStatCdType {
	@XmlEnumValue("103")
	SELLING("103", "판매중"),

	@XmlEnumValue("104")
	NO_SELLING("104", "품절");

	@Getter
	private String code;
	@Getter
	private String desc;

	private SelStatCdType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}
