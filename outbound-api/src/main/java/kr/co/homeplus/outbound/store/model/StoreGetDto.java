package kr.co.homeplus.outbound.store.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("홈플러스 점포 조회")
@Data
public class StoreGetDto {

    @ApiModelProperty(value = "점포코드" )
    private String storeId;

    @ApiModelProperty(value = "점포명")
    private String storeNm;

    @ApiModelProperty(value = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "점포구분")
    private String storeKind;

    @ApiModelProperty(value = "기준점포")
    private Integer originStoreId;

    @ApiModelProperty(value = "FC점포")
    private Integer fcStoreId;

    @ApiModelProperty(value = "대표자명")
    private String mgrNm;

    @ApiModelProperty(value = "우편번호")
    private String zipcode;

    @ApiModelProperty(value = "주소")
    private String addr1;

    @ApiModelProperty(value = "상세주소")
    private String addr2;

    @ApiModelProperty(value = "전화번호")
    private String phone;

    @ApiModelProperty(value = "휴무안내")
    private String closedInfo;

    @ApiModelProperty(value = "운영시간")
    private String oprTime;

    @ApiModelProperty(value = "마켓ID")
    private String partnerId;

    @ApiModelProperty(value = "마켓 지점 ID")
    private String marketStoreId;

    @ApiModelProperty(value = "사용여부")
    private String useYn;

    @ApiModelProperty(value = "fc여부")
    private String fcYn;

    @ApiModelProperty(value = "판매업체명")
    private String partnerNm;

    @ApiModelProperty(value = "사업자번호")
    private String partnerNo;

    @ApiModelProperty(value= "대표자명")
    private String partnerOwner;

}
