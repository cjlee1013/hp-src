package kr.co.homeplus.outbound.market.eleven.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ElevenClaimUriInfo {

    CLAIM_GET_CANCEL_REG_LIST("/claimservice/cancelorders/[startTime]/[endTime]"),
    CLAIM_SET_CANCEL_APPROVE("/claimservice/cancelreqconf/[ordPrdCnSeq]/[ordNo]/[ordPrdSeq]"),
    CLAIM_SET_CANCEL_REJECT("/cancelreqreject/[ordNo]/[ordPrdSeq]/[ordPrdCnSeq]/[dlvMthdCd]/[sendDt]/[dlvEtprsCd]/[invcNo]"),
    CLAIM_GET_CANCEL_COMPLETE_LIST("/claimservice/canceledorders/[startTime]/[endTime]"),
    CLAIM_GET_CANCEL_WITHDRAW_LIST("/claimservice/withdrawcanceledorders/[startTime]/[endTime]"),
    CLAIM_GET_CANCEL_SHIP_AFTER_LIST("/claimservice/officecancellist/[startTime]/[endTime]"),

    CLAIM_GET_RETURN_REG_LIST("/claimservice/returnorders/[startTime]/[endTime]"),
    CLAIM_SET_RETURN_APPROVE("/claimservice/returnreqconf/[ordPrdCnSeq]/[ordNo]/[ordPrdSeq]"),
    CLAIM_SET_RETURN_REJECT("/claimservice/returnreqreject/[ordNo]/[ordPrdSeq]/[clmReqSeq]/[refsRsnCd]/[refsRsn]"),
    CLAIM_GET_RETURN_COMPLETE_LIST("/claimservice/returnedorders/[startTime]/[endTime]"),
    CLAIM_SET_RETURN_HOLD("/claimservice/returnclaimdefer/[ordNo]/[ordPrdSeq]/[clmReqSeq]/[deferRefsRsnCd]/[deferRefsRsn]"),
    CLAIM_GET_RETURN_WITHDRAW_LIST("/claimservice/retractretorders/[startTime]/[endTime]"),

    CLAIM_GET_EXCHANGE_REG_LIST("/claimservice/exchangeorders/[startTime]/[endTime]"),
    CLAIM_SET_EXCHANGE_APPROVE("/claimservice/exchangereqconf/[clmReqSeq]/[ordNo]/[ordPrdSeq]/[dlvEtprsCd]/[invcNo]"),
    CLAIM_SET_EXCHANGE_REJECT("/claimservice/exchangereqreject/[ordNo]/[ordPrdSeq]/[clmReqSeq]/[refsRsnCd]/[refsRsn]"),
    CLAIM_GET_EXCHANGE_COMPLETE_LIST("/claimservice/exchangedorders/[startTime]/[endTime]"),
    CLAIM_GET_EXCHANGE_WITHDRAW_LIST("/claimservice/retractexcorders/[startTime]/[endTime]"),

    CLAIM_SET_NOT_SELLING_REG("/claimservice/reqrejectorder/[ordNo]/[ordPrdSeq]/[ordCnRsnCd]/[ordCnDtlsRsn]"),
    ;

    @Getter
    private final String url;

}
