package kr.co.homeplus.outbound.market.eleven.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultType {
  @XmlEnumValue( "0" )
  SUCCESS( "0", "성공" ),

  @XmlEnumValue( "-1" )
  FAIL( "-1", "실패" );

  private String code;
  private String message;
}
