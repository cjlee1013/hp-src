package kr.co.homeplus.outbound.item.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 상품 상세정보 Get Entry
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ItemSpecHistParamDto {

	private String itemNo;

	private Object request;

	private Object response;

	private String spec;

	private String partnerId;

	private String methodTxt;

}