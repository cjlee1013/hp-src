package kr.co.homeplus.outbound.cert.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 생활화학인증 정보
 */
@Data
public class ItemLifeCertDetailGetDto {

	@ApiModelProperty(value = "인증확인")
	private String valid_yn;

	@ApiModelProperty(value = "신고/승인번호")
	private String slfsfcfst_no;

	@ApiModelProperty(value = "인증시작일")
	private String start_date;

	@ApiModelProperty(value = "인증만료일")
	private String expired_date;

	@ApiModelProperty(value = "갱신/변경일")
	private String mod_date;

	@ApiModelProperty(value = "신규/갱신")
	private String renew_yn;

	@ApiModelProperty(value = "대표/파생/변경")
	private String prdt_type;

	@ApiModelProperty(value = "검사기준")
	private String safe_sd;

	@ApiModelProperty(value = "품목")
	private String item;

	@ApiModelProperty(value = "제품명(모델명)")
	private String prdt_nm;

	@ApiModelProperty(value = "용도 및 제형")
	private String df;

	@ApiModelProperty(value = "시험검사기관")
	private String inspct_org;

	@ApiModelProperty(value = "성상")
	private String sttus;

	@ApiModelProperty(value = "포장단위")
	private String pkg_unit;

	@ApiModelProperty(value = "유효기간")
	private String eff_prd;

	@ApiModelProperty(value = "제조/수입")
	private String mf_icm;

	@ApiModelProperty(value = "업체명")
	private String comp_nm;

}
