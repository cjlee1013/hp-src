package kr.co.homeplus.outbound.market.naver.claim.model;

import lombok.Data;

@Data
public class NaverCancelReqDto {
    private String marketOrderItemNo;
    private String claimType;
    private String claimReasonType;
    private String claimDetailReason;
}
