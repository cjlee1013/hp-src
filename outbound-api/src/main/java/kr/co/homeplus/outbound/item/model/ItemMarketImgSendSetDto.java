package kr.co.homeplus.outbound.item.model;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("마켓연동 이미지 등록수정")
public class ItemMarketImgSendSetDto {

    private String itemNo;
    private String partnerId;
    private String imgUrl1;
    private String imgUrl2;
    private String imgUrl3;
    private String imgUrl4;
    private String imgUrl5;
    private String imgUrl6;
    private String listImgUrl;
    private String lableImgUrl;
    private String sendStatus;
    private String regId;

}
