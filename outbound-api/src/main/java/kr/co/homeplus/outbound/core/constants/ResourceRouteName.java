package kr.co.homeplus.outbound.core.constants;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

/**
 * resource api 명을 관리
 * apiId의 url은 application.yml의 plus.resource-routes 에 정의 됨
 */
@Data
public class ResourceRouteName {
    /**
     * 11st
     */
    //TODO: ResourceClient 사용 못하기 때문에 임시로 설정
    @Value("${plus.resource-routes.eleven.url}")
    public String ELEVEN;

    /**
     * G마켓
     */
    public static final String GMARKET = "gmarket";

    /**
     * 옥션
     */
    public static final String AUCTION = "auction";


    /**
     * radio-cert
     */
    public static final String RADIO_CERT    = "radio-cert";

    /**
     * safe-cert
     */
    public static final String SAFE_CERT     = "safe-cert";

    /**
     * life-cert
     */
    public static final String LIFE_CERT     = "life-cert";

}
