package kr.co.homeplus.outbound.category.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MarketCategoryDetailSetDto {

    //마켓연동업체 아이디
    private String partnerId;

    //마켓 최하위 카테고리 번호
    private String marketCateCd;

    //마켓연동 대카테고리 번호
    private String marketLcateCd;

    //마켓연동 대카테고리명
    private String marketLcateNm;

    //마켓연동 중카테고리 번호
    private String marketMcateCd;

    //마켓연동 중카테고리명
    private String marketMcateNm;

    //마켓연동 소카테고리 번호
    private String marketScateCd;

    //마켓연동 소카테고리명
    private String marketScateNm;

    //마켓연동 세카테고리 번호
    private String marketDcateCd;

    //마켓연동 세카테고리명
    private String marketDcateNm;

    //사용여부(Y: 사용, N:삭제)
    private String useYn;

}
