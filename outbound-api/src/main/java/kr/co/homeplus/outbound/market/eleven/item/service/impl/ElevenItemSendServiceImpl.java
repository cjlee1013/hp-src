package kr.co.homeplus.outbound.market.eleven.item.service.impl;

import java.util.HashMap;
import java.util.Map;
import kr.co.homeplus.outbound.core.constants.CodeConstants;
import kr.co.homeplus.outbound.core.maketLog.annotation.MarketLogTarget;
import kr.co.homeplus.outbound.market.eleven.item.model.ResultMessageDto;
import kr.co.homeplus.outbound.core.constants.ElevenUriConstants;
import kr.co.homeplus.outbound.core.utility.RestUtil;
import kr.co.homeplus.outbound.market.eleven.item.model.ProductGetDto;
import kr.co.homeplus.outbound.market.eleven.item.model.ProductResultDto;
import kr.co.homeplus.outbound.market.eleven.item.model.ProductSetDto;
import kr.co.homeplus.outbound.market.eleven.item.model.StorePriceListDto;
import kr.co.homeplus.outbound.market.eleven.item.service.ElevenItemSendService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class ElevenItemSendServiceImpl extends RestUtil implements ElevenItemSendService {

	protected ElevenItemSendServiceImpl(RestTemplate restTemplate) {
		super(restTemplate);
	}
	@Override
	public ProductGetDto getItem(String marketItemNo) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("prdNo", marketItemNo);
		return execute(HttpMethod.GET, getHost() + ElevenUriConstants.SELECT_PRODUCT,
			null, new ParameterizedTypeReference<ProductGetDto>() {}, params);
	}

	@Override
	public ProductResultDto setItemStop(String marketItemNo) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("prdNo", marketItemNo);
		return execute(HttpMethod.PUT, getHost() + ElevenUriConstants.UPDATE_STOPDISPLAY,
			null, new ParameterizedTypeReference<ProductResultDto>() {}, params);
	}

	@Override
	public ProductResultDto setItemActive(String marketItemNo) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("prdNo", marketItemNo);
		return execute(HttpMethod.PUT, getHost() + ElevenUriConstants.UPDATE_RESTARTDISPLAY,
			null, new ParameterizedTypeReference<ProductResultDto>() {}, params);
	}

	@Override
	@MarketLogTarget(partnerId = CodeConstants.ELEVEN_PARTNER_ID, methodTxt = "newItemBasicInfo", reqClass = ProductSetDto.class)
	public ProductResultDto newItemBasicInfo(ProductSetDto productSk11stDTO, String itemNo) {
		return execute(HttpMethod.POST, getHost() + ElevenUriConstants.INSERT_PRODUCT
			, productSk11stDTO, new ParameterizedTypeReference<ProductResultDto>() {}, new HashMap<String, String>());
	}

	@Override
	@MarketLogTarget(partnerId = CodeConstants.ELEVEN_PARTNER_ID, methodTxt = "modItemBasicInfo", reqClass = ProductSetDto.class)
	public ProductResultDto modItemBasicInfo(ProductSetDto productSk11stDTO, String itemNo) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("prdNo", productSk11stDTO.getPrdNo());
		return execute(HttpMethod.PUT, getHost() + ElevenUriConstants.UPDATE_PRODUCT
			, productSk11stDTO, new ParameterizedTypeReference<ProductResultDto>() {}, params);
	}

	@Override
	@MarketLogTarget(partnerId = CodeConstants.ELEVEN_PARTNER_ID, methodTxt = "addStorePriceList", reqClass = StorePriceListDto.class)
	public ResultMessageDto addStorePriceList(StorePriceListDto storePriceListDto, String itemNo) {
		return execute (HttpMethod.POST, getHost() + ElevenUriConstants.STORE_PRICE_LIST, storePriceListDto,
			new ParameterizedTypeReference<ResultMessageDto>() {}, new HashMap<String, String>());
	}

}

