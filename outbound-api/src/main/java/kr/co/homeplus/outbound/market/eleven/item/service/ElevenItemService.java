package kr.co.homeplus.outbound.market.eleven.item.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.outbound.common.enums.DescriptionType;
import kr.co.homeplus.outbound.constants.ExceptionCode;
import kr.co.homeplus.outbound.core.constants.CodeConstants;
import kr.co.homeplus.outbound.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.outbound.core.maketLog.service.MarketItemLogService;
import kr.co.homeplus.outbound.item.constants.EventConstants;
import kr.co.homeplus.outbound.item.constants.ItemConstants;
import kr.co.homeplus.outbound.item.enums.BuyItemType;
import kr.co.homeplus.outbound.item.enums.ChangeType;
import kr.co.homeplus.outbound.item.enums.ItemCertType;
import kr.co.homeplus.outbound.item.enums.ItemStatus;
import kr.co.homeplus.outbound.item.enums.MarketItemStatus;
import kr.co.homeplus.outbound.item.enums.PickIconType;
import kr.co.homeplus.outbound.item.enums.SendType;
import kr.co.homeplus.outbound.item.enums.TaxYnType;
import kr.co.homeplus.outbound.item.mapper.ItemMasterMapper;
import kr.co.homeplus.outbound.item.mapper.ItemSlaveMapper;
import kr.co.homeplus.outbound.item.model.ItemMarketGetDto;
import kr.co.homeplus.outbound.item.model.ItemMarketSetDto;
import kr.co.homeplus.outbound.item.model.MarketItemGetDto;
import kr.co.homeplus.outbound.item.model.MarketItemSaleGetDto;
import kr.co.homeplus.outbound.item.service.common.ItemCommonService;
import kr.co.homeplus.outbound.market.eleven.item.enums.ClientMessageResultCodeType;
import kr.co.homeplus.outbound.market.eleven.item.enums.CuponCheckType;
import kr.co.homeplus.outbound.market.eleven.item.enums.DlvCnAreaCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.DlvCstInstBasiCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.DlvCstPayTypCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.DlvWyCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.NotificationCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.NotificationItemCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.OrgnTypCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.PrdStatCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.ResultMessageResultCodeType;
import kr.co.homeplus.outbound.market.eleven.item.enums.RmaterialTypCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.SelLimitTypCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.SelStatCdType;
import kr.co.homeplus.outbound.market.eleven.item.enums.SellMethodType;
import kr.co.homeplus.outbound.market.eleven.item.model.MartPrmtListSetDto;
import kr.co.homeplus.outbound.market.eleven.item.model.MartPrmtSetDto;
import kr.co.homeplus.outbound.market.eleven.item.model.PriceSetDto;
import kr.co.homeplus.outbound.market.eleven.item.model.ProductCertSetDto;
import kr.co.homeplus.outbound.market.eleven.item.model.ProductCodeDto;
import kr.co.homeplus.outbound.market.eleven.item.model.ProductGetDto;
import kr.co.homeplus.outbound.market.eleven.item.model.ProductNotificationSetDto;
import kr.co.homeplus.outbound.market.eleven.item.model.ProductOptionSetDto;
import kr.co.homeplus.outbound.market.eleven.item.model.ProductResultDto;
import kr.co.homeplus.outbound.market.eleven.item.model.ProductSetDto;
import kr.co.homeplus.outbound.market.eleven.item.model.ResultMessageDto;
import kr.co.homeplus.outbound.market.eleven.item.model.StorePriceListDto;
import kr.co.homeplus.outbound.response.ResponseResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ElevenItemService {

    @Value("${plus.resource-routes.imageFront.url}")
    private String imgFrontUrl;

    private final ElevenItemSendService elevenItemSendService;
    private final ItemCommonService itemCommonService;

    private final ItemMasterMapper itemMasterMapper;
    private final ItemSlaveMapper itemSlaveMapper;

    private final MarketItemLogService marketItemLogService;

    /**
     * 상품전송 (단일상품에 한함)
     * @return
     */
    public ResponseResult sendItem( List<String> itemList ) throws Exception{

        if (ObjectUtils.isEmpty(itemList)) {
            throw new BusinessLogicException(ExceptionCode.ERROR_CODE_1001);
        }

        List<ResponseResult> resultList = new ArrayList();

        //마스터 정보 조회 (대표점 기준)
        Map<String, MarketItemGetDto> marketItemGetDtoList = itemSlaveMapper
            .getItemMaster(itemList, CodeConstants.DEFAULT_STORE_ID, CodeConstants.CLUB_DEFAULT_STORE_ID, CodeConstants.ELEVEN_PARTNER_ID);

        List<String> processList = new ArrayList<>();

        marketItemGetDtoList.forEach(
            (itemNo, item) -> {
                if( StringUtils.isNotEmpty(item.getBasic().getMarketItemNo())) {
                     this.modifyItem(item);
                } else {
                     this.newItem(item);
                }

                ResponseResult res = new ResponseResult();
                res.setReturnKey(item.getBasic().getItemNo());
                res.setReturnMsg(item.getMsg());
                resultList.add(res);

                //처리 결과 확인용 list 생성
                processList.add(item.getBasic().getItemNo());
            }
        );

        //TODO: 마스터 정보 조회 되지 않은 상품에 대한 처리 추가 필요.
        itemList.removeAll(processList);
        log.debug("실패된 상품 " + itemList.toString());

        ResponseResult responseResult = new ResponseResult();
        responseResult.setReturnKey(String.valueOf(marketItemGetDtoList.size()));
        responseResult.setReturnMsg("성공 : " + processList.size() + " 살패  : " + itemList.size());
        responseResult.setResultList(resultList);

        return responseResult;
    }

    /**
     * 배치 통하지 않고 Swagger 에서 바로 호출 할 경우
     * @param itemList
     * @return
     */
    public ResponseResult sendItemForDirect(List<String> itemList) throws Exception {

        ItemMarketSetDto itemMarketSetDto = new ItemMarketSetDto();
        itemMarketSetDto.setSendType(SendType.SPOT.getCode()); //수시로 고정
        itemMarketSetDto.setSendYn(CodeConstants.USE_YN_N);
        itemMarketSetDto.setMarketItemStatus(MarketItemStatus.P.getType());
        itemMarketSetDto.setPartnerId(CodeConstants.ELEVEN_PARTNER_ID);

        itemMasterMapper.updateItemMarketSendStart(itemMarketSetDto, itemList);

        ResponseResult responseResult = sendItem(itemList);

        return responseResult;
    }

   /**
    * 상품 신규등록
    * @param marketItemGetDto
    * @return
    */
   private ResponseResult newItem(MarketItemGetDto marketItemGetDto) {

       if (itemCommonService.getDisplayValidCheck(marketItemGetDto)) {
           try {
               //기본정보
               ProductSetDto productSetDto = setItemBasicInfo(marketItemGetDto);

               //프로모션
               this.setPromo(marketItemGetDto,productSetDto);

               ProductResultDto productResponse = elevenItemSendService.newItemBasicInfo(productSetDto, marketItemGetDto.getBasic().getItemNo());
               marketItemGetDto.setMsg(productResponse.getMessage());

               if (StringUtils.isNotEmpty(productResponse.getProductNo())) {

                       marketItemGetDto.getBasic().setMarketItemNo(productResponse.getProductNo());
                       StorePriceListDto storePriceListDto = setStorePriceInfo(marketItemGetDto);

                       ResultMessageDto priceResponse = elevenItemSendService.addStorePriceList(storePriceListDto, marketItemGetDto.getBasic().getItemNo());

                       if(priceResponse.getResultCode().equals(ResultMessageResultCodeType.SUCCESS.getCode())) {
                           //성공처리
                           itemMasterMapper.updateItemMarket(itemCommonService.setMarketItemActive(marketItemGetDto));

                           //연동 상품 히스토리 저장
                           marketItemLogService.setMarketItemHist(marketItemGetDto);

                       }else{
                           //실패 시
                           marketItemGetDto.setMsg(priceResponse.getResultMessage());
                           this.setStopItem(marketItemGetDto, MarketItemStatus.E.getType());
                       }

                   } else {
                   //연동 실패처리
                   this.setStopItem(marketItemGetDto, MarketItemStatus.E.getType());
               }

           }catch(Exception e) {
               e.printStackTrace();
               try {
                   //연동 오류 판매 중지 처리
                   if(StringUtils.isNotEmpty(marketItemGetDto.getBasic().getMarketItemNo())) {
                       elevenItemSendService
                           .setItemStop(marketItemGetDto.getBasic().getMarketItemNo());
                   }
                   marketItemGetDto.setMsg("System Exception.");
                   this.setStopItem(marketItemGetDto, MarketItemStatus.E.getType());
               } catch (Exception ee) {
                   ee.printStackTrace();
                   log.error("Fail process Exception :" + e.getMessage());
               }

           }

       } else {
           // 판매중지 상태 변경
           this.setStopItem(marketItemGetDto, MarketItemStatus.S.getType());
       }

       ResponseResult responseResult = new ResponseResult();
       responseResult.setReturnKey("");

       return responseResult;
       }

    /**
     * 행사 (rms, 상품쿠폰 > 상품할인), 아이콘 셋틴
     * @param marketItemGetDto
     * @param productSetDto
     */
    private void setPromo(MarketItemGetDto marketItemGetDto, ProductSetDto productSetDto) {

        //전단 아이콘
        marketItemGetDto.getBasic().setMarketIconType(CodeConstants.USE_YN_Y.equals(marketItemGetDto.getBasic().getLeafletYn())? CodeConstants.ELEVEN_LEAFLET_ICON_TYPE : null);

        //행사 유효성 체크 및 기본정보 셋팅
        itemCommonService.validPromo(marketItemGetDto);

        //즉시할인 행사
        if( EventConstants.CODE_Y.equals(marketItemGetDto.getMasterCouponYn())) {
            // 즉시할인 = 행사상품
            marketItemGetDto.getBasic().setMarketIconType(CodeConstants.ELEVEN_EVENT_ICON_TYPE);
        }

        //rms 행사 validation
        if( EventConstants.CODE_Y.equals(marketItemGetDto.getMasterPromoYn())) {
             switch (marketItemGetDto.getPromo().getEventKind()) {
                 /**
                  * 특이사항 : 11번가는 아이콘 정보를 지점 정보에 포함되므로 임시로 basic 애 저장한다.
                 */
                case EventConstants.EVENT_KIND_BASIC :
                    //심플할인 = 행사항품
                    marketItemGetDto.getBasic().setMarketIconType(CodeConstants.ELEVEN_EVENT_ICON_TYPE);
                    break;

                case EventConstants.EVENT_KIND_PICK :
                    marketItemGetDto.getBasic()
                        .setMarketIconType(PickIconType.valueFor(String.valueOf(marketItemGetDto.getPromo().getBuyItemValue())).getElevenCd());

                    //최대 구매수량 재계산
                    productSetDto.setSelLimitQty(String.valueOf(itemCommonService.getPickPurchaseLimitQty(marketItemGetDto.getPromo().getBuyItemValue(), marketItemGetDto.getBasic().getPurchaseLimitQty())));

                    break;
                case EventConstants.EVENT_KIND_TOGETHER :
                    /**
                     *  할인 수량은 2개 이상 등록 가능
                     *  복수구매의 할인  금액은 판매가의 50%이상 설정하실 수 없습니다
                     *  날짜 포맷 YYYYMMDD
                     */
                    //복수구매 할인설정 여부
                    productSetDto.setPluYN(EventConstants.CODE_Y);

                    //할인 설정 기준 (01:수량, 02: 금액)
                    productSetDto.setPluDscCd(BuyItemType.valueFor(marketItemGetDto.getPromo().getBuyItemType()).getElevenCd());

                    //할인구분코드 (→ 01:%, 02: 원)
                    productSetDto.setPluDscMthdCd(ChangeType.valueFor(marketItemGetDto.getPromo().getChangeType()).getElevenCd());

                    // 기준금액
                    productSetDto.setPluDscBasis(String.valueOf(marketItemGetDto.getPromo().getBuyItemValue()));

                    //할인금액,% 설정
                    if (ChangeType.RATE.getType().equals(marketItemGetDto.getPromo().getChangeType())){
                        productSetDto.setPluDscAmtPercnt(String.valueOf(marketItemGetDto.getPromo().getChangePercent()));
                    } else if (ChangeType.AMOUNT.getType().equals(marketItemGetDto.getPromo().getChangeType())) {
                        productSetDto.setPluDscAmtPercnt(String.valueOf(marketItemGetDto.getPromo().getChangeAmount()));
                    }

                    //할인기간은 무조건 설정
                    productSetDto.setPluUseLmtDyYn(EventConstants.CODE_Y);

                    //할인기간 설정
                    productSetDto.setPluIssStartDy(marketItemGetDto.getPromo().getStartDate().replaceAll("-", ""));
                    productSetDto.setPluIssEndDy(marketItemGetDto.getPromo().getEndDate().replaceAll("-", ""));

                    //복수조건
                    marketItemGetDto.getBasic().setMarketIconType(CodeConstants.ELEVEN_MULTI_EVENT_ICON_TYPE);
                    break;
            }
        }
    }

    /**
     * 상품수정
     * @param marketItemGetDto
     * @return
     */
    private ResponseResult modifyItem(MarketItemGetDto marketItemGetDto)  {

        try {

            if (itemCommonService.getDisplayValidCheck(marketItemGetDto)) {

                //상품 기본정보
                ProductSetDto productSetDto = this.setItemBasicInfo(marketItemGetDto);
                productSetDto.setPrdNo(marketItemGetDto.getBasic().getMarketItemNo());

                //상품 할인정보
                this.setPromo(marketItemGetDto, productSetDto);

                //판매상태로 변경 처리
                elevenItemSendService.setItemActive(marketItemGetDto.getBasic().getMarketItemNo());

                //상품 마스터 정보 전송
                ProductResultDto productResponse = elevenItemSendService
                    .modItemBasicInfo(productSetDto, marketItemGetDto.getBasic().getItemNo());
                marketItemGetDto.setMsg(productResponse.getMessage());

                if (productResponse.getResultCode().equals(ClientMessageResultCodeType.SUCCESS)
                    || productResponse.getResultCode().equals(ClientMessageResultCodeType.NEW_SUCCESS)) {

                        if (productResponse.getMessage().contains("판매중/전시전인 상품만 판매중지가 가능합니다")) {
                            if (marketItemGetDto.getBasic().getMarketItemStatus().equals("A")) {
                                //11번가는 판매중지, 홈플러스는 판매중 상태 싱크 맞추기
                                marketItemGetDto.setMsg(productResponse.getMessage());
                                itemMasterMapper.updateItemMarket(
                                    itemCommonService.setMarketItemStop(marketItemGetDto, MarketItemStatus.E.getType()));
                            }
                        } else {
                            //지점별 가격 정보 전송
                            ResultMessageDto priceResponse = elevenItemSendService.addStorePriceList(setStorePriceInfo(marketItemGetDto),
                                marketItemGetDto.getBasic().getItemNo());
                            if (priceResponse.getResultCode().equals(ResultMessageResultCodeType.SUCCESS.getCode())) {

                                itemMasterMapper.updateItemMarket(
                                    itemCommonService.setMarketItemActive(marketItemGetDto));

                                //연동 상품 히스토리 저장
                                marketItemLogService.setMarketItemHist(marketItemGetDto);

                            } else {
                                if (!priceResponse.getResultMessage().contains("쇼킹딜")) {
                                    marketItemGetDto.setMsg(priceResponse.getResultMessage());
                                    this.setStopItem(marketItemGetDto, MarketItemStatus.E.getType());
                                }
                            }
                        }

                } else {
                    //연동 실패처리
                    this.setStopItem(marketItemGetDto, MarketItemStatus.E.getType());
                }

            } else {
                // 상품 validation 미통과 상품
                this.setStopItem(marketItemGetDto, MarketItemStatus.S.getType());
                // 판매 중지 처리
                elevenItemSendService.setItemStop(marketItemGetDto.getBasic().getMarketItemNo());

            }
        } catch (Exception e) {
            try {
                /** 1. 상태값 안맞을 수 있으므로!!! 상품 중지 처리 후 업데이트 하도록 한다
                 *  2. 상품중지 실패 할 경우 상품이 연동대기 상태로 남아 있으므로! 꼭 확인하여 처리 할 수 있도록 한다.
                 *  TODO : 우선순위를 정해야 하는지 확인 !
                 */
                elevenItemSendService.setItemStop(marketItemGetDto.getBasic().getMarketItemNo());

                marketItemGetDto.setMsg("System Exception.");
                this.setStopItem(marketItemGetDto, MarketItemStatus.E.getType());

            } catch (Exception ee) {
                log.error("Fail process Exception :" + e.getMessage());
            }

            log.error(e.getMessage());
        }

        ResponseResult responseResult = new ResponseResult();
        responseResult.setReturnKey("");

        return responseResult;
    }

    /**
     * 상품 지점별 정보 생성
     * @param marketItemGetDto
     * @return
     */
    private StorePriceListDto setStorePriceInfo(MarketItemGetDto marketItemGetDto) {

        StorePriceListDto dto = new StorePriceListDto();
        List<PriceSetDto> priceSetDtoList = new ArrayList<>();

        List<MartPrmtSetDto> martPrmtSetDtoList= new ArrayList<>();
        List<MartPrmtListSetDto> martPrmtListSetDtoList = new ArrayList<>();

        MartPrmtSetDto martPrmtSetDto = new MartPrmtSetDto();
        MartPrmtListSetDto martPrmtListSetDto = new MartPrmtListSetDto() ;

        //아이콘,promo code set
        if( StringUtils.isNotEmpty(marketItemGetDto.getBasic().getMarketIconType())) {

             martPrmtSetDto.setMartPrmtNo(marketItemGetDto.getBasic().getMarketIconType());
             martPrmtSetDto.setMartPrmtCd(marketItemGetDto.getBasic().getMarketIconType());

            if ( EventConstants.CODE_Y.equals(marketItemGetDto.getMasterPromoYn()) || EventConstants.CODE_Y.equals(marketItemGetDto.getMasterCouponYn())) {
                //custom code
                martPrmtListSetDto.setMartPrmtNm(marketItemGetDto.getCustomCd());
                martPrmtListSetDtoList.add(martPrmtListSetDto);
                martPrmtSetDto.setMartPrmtList(martPrmtListSetDtoList);

            }
            martPrmtSetDtoList.add(martPrmtSetDto);
        }

         /**
         ** 특이사항
         * - 지점별 가격 10원 이상 등록가능
         * - 재고는 0 보다 이상  100000 이하
         */
       for (MarketItemSaleGetDto store : marketItemGetDto.getStoreList()) {

           long saleCnt = store.getStockQty();

           if (store.getStockQty() > ItemConstants.MAX_STOCK_QTY) {
               saleCnt = ItemConstants.MAX_STOCK_QTY;
           } else if (store.getStockQty() < 1) {
               saleCnt = 0;
           }

           PriceSetDto price = PriceSetDto.builder()
           .martNo(CodeConstants.ELEVEN_MART_NO)
           .prdNo(marketItemGetDto.getBasic().getMarketItemNo())
           .prdSelQty(String.valueOf(saleCnt))
           .selPrc(String.valueOf(store.getCouponPrice()))
           .selStatCd(getSelStatCd(store, marketItemGetDto))
           .strNo(store.getMarketStoreId())
           .martPrmt(martPrmtSetDtoList)
           .build();

           store.setSellStatus(price.getSelStatCd().getDesc());
           priceSetDtoList.add(price);
        }

        if( ObjectUtils.isNotEmpty(priceSetDtoList) ) {
            dto.setStoreProduct(priceSetDtoList);
        }

      return dto;
    }

    /**
     * 판매상태 Check
     * @param saleDto
     * @param marketItemGetDto
     * @return
     */
    private SelStatCdType getSelStatCd (MarketItemSaleGetDto saleDto, MarketItemGetDto marketItemGetDto) {

        /**
         * - 클럽 행사는 하이퍼 마스터 행사가 진행 줄일때만 적용
         * - 하이퍼는 적용이지만 클럽은 미적용일 시 판매 중지 처리
         */
        if (CodeConstants.STORE_ATTR_SPECIAL.equals(saleDto.getStoreAttr())) {
            //rms 행사 클럽 미적용일 경우
            if (EventConstants.CODE_Y.equals(marketItemGetDto.getMasterPromoYn())
                && EventConstants.CODE_N.equals(marketItemGetDto.getPromo().getClubPromoYn())) {
                return SelStatCdType.NO_SELLING;
            }

            if (EventConstants.CODE_Y.equals(marketItemGetDto.getMasterCouponYn())
                && EventConstants.CODE_N.equals(marketItemGetDto.getCoupon().getClubCouponYn())) {
                return SelStatCdType.NO_SELLING;
            }
        }

        /**
         *  - 배송정책 등록여부
         *  - 취급 중지여부
         *  - PFR 통과여부
         *  - 판매가 > 0원
         *  - 재고 > 0개
         */
        return (CodeConstants.USE_YN_Y.equals(saleDto.getShipPolicyYn())         //배송정책 등록여부
            && CodeConstants.STOP_DEAL_YN_N.equals(saleDto.getStopDealYn())  //취급중지여부
            && CodeConstants.PFR_YN_Y.equals(saleDto.getPfrYn())             //PFR 통과 여부
            && saleDto.getSalePrice() > 0                                    //판매가 0원 이상
            && saleDto.getStockQty() > 0 ) ?                               //재고 0개 이상
             SelStatCdType.SELLING : SelStatCdType.NO_SELLING;
    }

    /**
     * 상품 기본정보 생성
     * @param marketItemGetDto
     * @return
     */
    private ProductSetDto setItemBasicInfo(MarketItemGetDto marketItemGetDto) throws Exception {

        ProductSetDto productSetDto = new ProductSetDto();

        // 상품 등록, 수정 API 이용 시 prdTypCd를 반드시 11로 셋팅
        productSetDto.setPrdTypCd("11");
        // 판매 방식 : 고정가판매
        productSetDto.setSelMthdCd(SellMethodType.FIXED);

        //카테고리
        productSetDto.setDispCtgrNo( marketItemGetDto.getBasic().getMarketCateCd() );

        //카테고리(고정)
        productSetDto.setOriginDispCtgrNo("1");

        //제조사
        productSetDto.setCompany(CodeConstants.NOTHING);

        //브랜드
        productSetDto.setBrand(CodeConstants.NOTHING);

        //모델명
        productSetDto.setModelNm(CodeConstants.NOTHING);

        //상품명
        productSetDto.setPrdNm(itemCommonService.removeSpecialChar( DescriptionType.TITLE, marketItemGetDto
            .getBasic().getItemNm()));

        //홍보문구 공백으로 변경처리됨 > 21.01.04
        productSetDto.setAdvrtStmt(" ");

        //원산지 구분
        productSetDto.setOrgnTypCd( OrgnTypCdType.ETC );
        productSetDto.setRmaterialTypCd( RmaterialTypCdType.REFDETAIL);

        //원산지명
        productSetDto.setOrgnNmVal(StringUtils.isNotEmpty(marketItemGetDto.getBasic().getOriginTxt()) ? marketItemGetDto
            .getBasic().getOriginTxt() : CodeConstants.ITEM_REFERENCE_MSG);

        //원산지 코드
        productSetDto.setOrgnTypDtlsCd( "1" );

        //판매자 상품코드
        productSetDto.setSellerPrdCd( marketItemGetDto.getBasic().getItemNo() );

        //과세여부
        productSetDto.setSuplDtyfrPrdClfCd(TaxYnType.valueFor(marketItemGetDto.getBasic().getTaxYn()).getElevenCd());

        //미성년자 구매가능
        productSetDto.setMinorSelCnYn(CodeConstants.USE_YN_Y); //성인 상품은 안보내기로 했으니까 Y로 고정

        //상품상태
        productSetDto.setPrdStatCd( PrdStatCdType.NEW );

        //대표점 판매가
        productSetDto.setSelPrc(String.valueOf(marketItemGetDto.getBasic().getSalePrice()));

        //재고
        productSetDto.setPrdSelQty( marketItemGetDto.getMasterDispYn().equals(CodeConstants.DISPLAY_YN_Y) ? CodeConstants.DEFAULT_MAX_STOCK_QTY : CodeConstants.ZERO );

        //최소구매수량
        productSetDto.setSelMinLimitTypCd(SelLimitTypCdType.LIMIT_ONCE); //1회제한
        productSetDto.setSelMinLimitQty( String.valueOf(marketItemGetDto.getBasic().getPurchaseMinQty()));

        // TODO: 축산,수산 일 경우만 단위배수 구매 수량 등록
        /*if(marketItemGetDto.getBasic().getLcateCd().equals("")) {
        productSetDto.setBuyUntQty(String.valueOf(marketItemGetDto.getBasic().getSaleUnit()));
        */

        // 최대구매수량
        switch (marketItemGetDto.getBasic().getPurchaseLimitYn()) {
            case CodeConstants.USE_YN_N:
                //제한안함
                productSetDto.setSelLimitTypCd( SelLimitTypCdType.NO_LIMIT);
                break;
            case CodeConstants.USE_YN_Y :

                if("O".equals(marketItemGetDto.getBasic().getPurchaseLimitDuration())) {
                    productSetDto.setSelLimitTypCd(SelLimitTypCdType.LIMIT_ONCE);
                } else if ("P".equals(marketItemGetDto.getBasic().getPurchaseLimitDuration())) {
                    productSetDto.setSelLimitTypCd(SelLimitTypCdType.LIMIT_PERIOD);
                    //제한기간
                    productSetDto.setTownSelLmtDy(String.valueOf(marketItemGetDto.getBasic().getPurchaseLimitDay()));
                }
                //최대구매제한갯수
                productSetDto.setSelLimitQty(String.valueOf(marketItemGetDto.getBasic().getPurchaseLimitQty()));
                break;
        }

        // 판매기간-판매기간설정여부
        productSetDto.setSelTermUseYn( marketItemGetDto.getBasic().getSalePeriodYn() );

        if(CodeConstants.USE_YN_Y.equals(marketItemGetDto.getBasic().getSalePeriodYn())){
            //판매기간 설정일 경우, 시작일 등록일 등록 (ex 2021-04-15)
            productSetDto.setAplBgnDy(marketItemGetDto.getBasic().getSaleStartDt());
            productSetDto.setAplEndDy(marketItemGetDto.getBasic().getSaleEndDt());
        }

        // 배송 가능 지역
        productSetDto.setDlvCnAreaCd( DlvCnAreaCdType.EXCEPT_SOME_PLACE );

        // 배송방법
        productSetDto.setDlvWyCd( DlvWyCdType.DIRECT );

        // 배송비 설정
        productSetDto.setDlvCstInstBasiCd( DlvCstInstBasiCdType.SELLER_CONDITIONAL );

        // 묶음배송 여부
        productSetDto.setBndlDlvCnYn( CodeConstants.USE_YN_Y );

        // 가격비교 노출여
        productSetDto.setPrcCmpExpYn( marketItemGetDto.getBasic().getEpYn() );

        // 무료 배송 기준 금액
        productSetDto.setPrdFrDlvBasiAmt( CodeConstants.DEFAULT_FREE_CONDITION);

        // 배송비
        productSetDto.setDlvCst1( CodeConstants.DEFAULT_SHIP_FEE );

        // 결재방법
        productSetDto.setDlvCstPayTypCd( DlvCstPayTypCdType.REQUIRED_ADVANCE_PAY );

        // AS 안내정보
        productSetDto.setAsDetail( "1577-3355" );

        // 반품 배송비
        productSetDto.setRtngdDlvCst( CodeConstants.DEFAULT_CLAIM_FEE );

        // 교환 배송비
        productSetDto.setExchDlvCst( CodeConstants.ZERO);

        // 반품/교환 안내
        productSetDto.setRtngExchDetail( "." );

        //제조일자
        productSetDto.setMnfcDy(marketItemGetDto.getBasic().getMakeDt());

        //유효일자
        productSetDto.setEftvDy(marketItemGetDto.getBasic().getExpDt());

        //이미지
        //TODO: dev/qa 환경 이미지는 외부에서 접속이 되지 않아 운영에 특정이미지로 전송(시그니처물티슈)
        productSetDto.setPrdImage01("https://image.homeplus.kr/td/8887f56b-d693-4810-b5ba-9ee192b2d81d");
        productSetDto.setPrdImage02("https://image.homeplus.kr/td/25390050-82cf-4ecd-9693-eb7547e076cb");
        productSetDto.setPrdImage03("https://image.homeplus.kr/td/4be2001f-0c0f-4166-b128-8b5c84ddcc2b");

        /*if(ObjectUtils.isNotEmpty(marketItemGetDto.getImg())) {

            MarketItemImgGetDto main = marketItemGetDto
                .getImg().stream().filter(i-> i.getImgType().equals("MN") && i.getMainYn().equals(CodeConstants.USE_YN_Y)).findFirst().orElse(null);
            //메인
            productSetDto.setPrdImage01( imgFrontUrl + main.getImgUrl());

            List<String> addImg = marketItemGetDto
                .getImg().stream().filter(i-> i.getImgType().equals("MN") && i.getMainYn().equals(CodeConstants.USE_YN_N))
                .map(MarketItemImgGetDto::getImgUrl)
                .distinct()
                .collect(Collectors.toList());

            //추가이미지
            if(ObjectUtils.isNotEmpty(addImg)) {
                productSetDto.setPrdImage02(addImg.size() >= 1 && StringUtils.isNotEmpty(addImg.get(0)) ? imgFrontUrl + addImg.get(0) : null);
                productSetDto.setPrdImage03(addImg.size() >= 2 && StringUtils.isNotEmpty(addImg.get(1)) ? imgFrontUrl + addImg.get(1) : null);
                productSetDto.setPrdImage04(addImg.size() >= 3 && StringUtils.isNotEmpty(addImg.get(2)) ? imgFrontUrl + addImg.get(2) : null);
            }
        }*/

        //상품상세
        productSetDto.setHtmlDetail( itemCommonService.getHtmlDetail(marketItemGetDto));

        //쿠폰
        productSetDto.setCuponcheck( CuponCheckType.REMOVE );

        //복수구매할인
        productSetDto.setPluYN( EventConstants.CODE_N);

        //고시
        List<ProductNotificationSetDto > notificationCdList = new ArrayList<>();
        List<ProductCodeDto> itemList = new ArrayList<>();

        if (ObjectUtils.isNotEmpty(marketItemGetDto.getNotice()) ) {

        	marketItemGetDto.getNotice().stream().forEach( n-> {
        		itemList.add(ProductCodeDto.builder().code(n.getMarketNoticeNo())
		                .name(CodeConstants.ITEM_REFERENCE_MSG)
		                .build());
        	});

            //정책번호는 모두 동일하므로 첫번째 거를 가져옴.
        	notificationCdList.add ( ProductNotificationSetDto.builder()
		                .type(marketItemGetDto.getNotice().get(0).getMarketGnoticeNo())
		                .productCodeDtoList(itemList)
		                .build());

        } else {
		        for ( NotificationCdType ncdType : NotificationCdType.values() ) {
		            for ( NotificationItemCdType itemCdType : NotificationItemCdType.values() ) {
		                itemList.add( ProductCodeDto.builder().code(itemCdType.getCode())
		                .name(itemCdType.getDesc())
		                .build());
		            }
		
		            notificationCdList.add ( ProductNotificationSetDto.builder()
		                .type(ncdType.getCode())
		                .productCodeDtoList(itemList)
		                .build());
		        }
        	}

        productSetDto.setProductNotificationSetDtoList(notificationCdList);

        //옵션
        if( CodeConstants.USE_YN_Y.equals(marketItemGetDto.getBasic().getOptSelUseYn())
            && marketItemGetDto.getOpt().size() > 0 ) {

            productSetDto.setOptUpdateYn(CodeConstants.USE_YN_Y);
            productSetDto.setOptSelectYn(CodeConstants.USE_YN_Y);
            productSetDto.setTxtColCnt("1");
            productSetDto.setColTitle("옵션선택");
            productSetDto.setPrdExposeClfCd("01");

            List<ProductOptionSetDto> optList = new ArrayList<>();
            marketItemGetDto.getOpt().stream().forEach(
                o-> {
                    optList.add(ProductOptionSetDto.builder()
                    .colValue0(itemCommonService.removeSpecialChar(DescriptionType.OPTION, o.getOpt1Val()))
                    .colOptPrice(CodeConstants.ZERO)
                    .colCount(CodeConstants.DEFAULT_MAX_STOCK_QTY)
                    .useYn(CodeConstants.USE_YN_Y)
                    .build());
            });

            productSetDto.setProductOptionSetDtoList(optList);
        }

        //안전인증
        if (ObjectUtils.isNotEmpty( marketItemGetDto.getCert())) {
            List<ProductCertSetDto> certList = new ArrayList<>();

            marketItemGetDto.getCert().stream().forEach(
                c->{
                    if( CodeConstants.USE_YN_Y.equals(ItemCertType.valueFor(c.getCertType()).getSendYn())) {
                        certList.add(ProductCertSetDto.builder()
                            .certKey(ItemCertType.valueFor(c.getCertType()).getElevenCd())
                            .certTypeCd((StringUtils.isNotEmpty(c.getCertNo()))
                                ? CodeConstants.ITEM_REFERENCE_MSG
                                : CodeConstants.ITEM_REFERENCE_MSG)
                            .build()
                        );
                    }
                }
            );

            productSetDto.setProductCertSetDtoList(certList);
        }

        // TODO:수입 축산물일 경우 이력번호 처리 방법
        //productSetDto.setBeefTraceStat(BeefTraceStatType.YES.getCode());
        //productSetDto.setBeefTraceNo("배송될 상품에 표시");

        return productSetDto;
    }

    /**
     * 11번가 상품 조회
     * @param coopItemNo
     * @return
     */
   public ProductGetDto getItem (String coopItemNo){
        return elevenItemSendService.getItem(coopItemNo);
   }
   
    /**
     * 판매중지처리
     * @param marketItemGetDto
     */
    private void setStopItem(MarketItemGetDto marketItemGetDto, String marketItemStatus)  {
        itemMasterMapper.updateItemMarket(itemCommonService.setMarketItemStop(marketItemGetDto, marketItemStatus));
    }

    /**
     * 11번가 상품 강제 중지
     * @param itemNo
     * @return
     */
    public ResponseResult setForceStopItem(String itemNo) {

        ItemMarketGetDto dto = itemSlaveMapper.getItemMarket(CodeConstants.ELEVEN_PARTNER_ID, itemNo);

        ResponseResult responseResult = new ResponseResult();
        responseResult.setReturnMsg(ExceptionCode.ERROR_CODE_2001.getDescription());

        if ( ObjectUtils.isNotEmpty(dto)) {

            if (StringUtils.isNotEmpty(dto.getMarketItemNo())) {
                ProductResultDto response = elevenItemSendService.setItemStop(dto.getMarketItemNo());
                responseResult.setReturnMsg(response.getMessage());

                if (ClientMessageResultCodeType.SUCCESS.getCode()
                    .equals(response.getResultCode().getCode())) {

                    itemMasterMapper.updateItemMarket(ItemMarketSetDto.builder()
                        .partnerId(dto.getPartnerId())
                        .itemNo(dto.getItemNo())
                        .sendYn(CodeConstants.USE_YN_Y)
                        .marketItemNo(dto.getMarketItemNo())
                        .marketItemStatus(ItemStatus.E.getType())
                        .promoYn(dto.getPromoYn())
                        .sendMsg(response.getMessage())
                        .build());
                }
            }
        }

        return responseResult;
    }

    /**
     * 11번가 상품 강제 중지 해제
     * 외부에 기능 제공하지 않음 (API 내에서만 사용 할 수 있도록!)
     * @param itemNo
     * @return
     */
    public ResponseResult setForceActiveItem(String itemNo) {

        ItemMarketGetDto dto = itemSlaveMapper.getItemMarket(CodeConstants.ELEVEN_PARTNER_ID, itemNo);

        ResponseResult responseResult = new ResponseResult();
        responseResult.setReturnMsg(ExceptionCode.ERROR_CODE_2001.getDescription());

        if ( ObjectUtils.isNotEmpty(dto)) {

            if (StringUtils.isNotEmpty(dto.getMarketItemNo())) {
                ProductResultDto response = elevenItemSendService.setItemActive(dto.getMarketItemNo());
                responseResult.setReturnMsg(response.getMessage());

                if (ClientMessageResultCodeType.SUCCESS.getCode()
                    .equals(response.getResultCode().getCode())) {

                    itemMasterMapper.updateItemMarket(ItemMarketSetDto.builder()
                        .partnerId(dto.getPartnerId())
                        .itemNo(dto.getItemNo())
                        .sendYn(CodeConstants.USE_YN_Y)
                        .marketItemNo(dto.getMarketItemNo())
                        .marketItemStatus(ItemStatus.A.getType())
                        .promoYn(dto.getPromoYn())
                        .couponYn(dto.getCouponYn())
                        .couponEndDt(dto.getCouponEndDt())
                        .sendMsg(response.getMessage())
                        .build());
                }
            }
        }

        return responseResult;
    }


}

