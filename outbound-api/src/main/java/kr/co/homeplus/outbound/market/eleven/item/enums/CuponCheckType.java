package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;

/**
 * 기본즉시할인 설정여부
 */
public enum CuponCheckType {

	@XmlEnumValue("Y")
	MODIFY("Y", "적용함"),

	@XmlEnumValue("N")
	REMOVE("N", "적용안함"),

	@XmlEnumValue("S")
	NOT_MODIFY("S", "기존값 유지");

	@Getter
	private String code;
	@Getter
	private String desc;

	private CuponCheckType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}