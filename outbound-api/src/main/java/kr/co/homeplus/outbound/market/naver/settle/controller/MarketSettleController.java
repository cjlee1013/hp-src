package kr.co.homeplus.outbound.market.naver.settle.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.homeplus.outbound.market.naver.settle.model.NaverSettleListGetDto;
import kr.co.homeplus.outbound.market.naver.settle.model.NaverSettleSetDto;
import kr.co.homeplus.outbound.market.naver.settle.service.NaverSettleService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "마켓연동 > 네이버")
@Slf4j
@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketSettleController {

    private final NaverSettleService naverSettleService;

    @ApiOperation(value = "네이버 정산내역")
    @PostMapping("/naver/getSettleList")
    public ResponseObject<NaverSettleListGetDto> getSettleList(@RequestBody NaverSettleSetDto naverSettleSetDto) throws Exception {
        return ResourceConverter.toResponseObject(naverSettleService.getSettleList(naverSettleSetDto));
    }
}
