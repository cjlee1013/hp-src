package kr.co.homeplus.outbound.item.service.naver;

import com.nhncorp.platform.shopn.BranchServiceStub;
import java.rmi.RemoteException;
import java.util.Random;
import java.util.UUID;
import kr.co.homeplus.outbound.category.service.naver.AccessCredentials;
import kr.co.homeplus.outbound.common.enums.NaverMartProfile;
import kr.co.homeplus.outbound.common.enums.NaverStoreStatusType;
import kr.co.homeplus.outbound.core.constants.CodeConstants;
import kr.co.homeplus.outbound.item.model.naver.StoStoreInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.axis2.AxisFault;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * Class Name : ExtBranchClientStub.java
 * Description : Naver Start Store > BranchServiceStub (Extends)
 *
 *  Modification Information
 *  Modify Date    Modifier    Comment
 *  -----------------------------------------------
 *  2021. 5. 27.   jh6108.kim   신규작성
 * </pre>
 *
 * @author jh6108.kim
 * @since 2021. 5. 27.
 */
@Component
@Slf4j
public class ExtBranchClientStub extends BranchServiceStub {
	
	private AccessCredentials ac = null;
	private Random random = new Random();
	private static Boolean isReal;
	private static String sellerId;
	private static String targetEndpoint;
	private static String serviceName = NaverMartProfile.STORE_SERVICE_NAME.getValue();
	private static String serviceVersion = NaverMartProfile.STORE_SERVICE_VERSION.getValue();

	@Value("${market.naver.isReal}")
	public void setIsReal(Boolean bIsReal) {
		isReal =  bIsReal;
	}

	@Value("${market.naver.sellerId}")
	public void setSellerId(String sSellerId) {
		sellerId =  sSellerId;
	}

	@Value("${market.naver.targetEndpoint}")
	public void setTargetEndpoint(String sTargetEndpoint) { targetEndpoint =  sTargetEndpoint; }


	/**
	 * AccessCredentials
	 */
	public void setAccessCredentials(AccessCredentials ac) { this.ac = ac; }

	private String createRequestID() {
		return UUID.randomUUID().toString() + "_PROD" + random.nextInt(999999999);
	}

	/**
	 * Constructor that takes in a configContext
	 * @deprecated
	 */
	public ExtBranchClientStub(
			org.apache.axis2.context.ConfigurationContext configurationContext,
			java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
		super(configurationContext, targetEndpoint, false);
	}

	/**
	 * Default Constructor
	 */
	public ExtBranchClientStub(
			org.apache.axis2.context.ConfigurationContext configurationContext)
			throws org.apache.axis2.AxisFault {
		super(configurationContext, targetEndpoint+serviceName, false);
	}

	/**
	 * Default Constructor
	 */
	public ExtBranchClientStub() throws org.apache.axis2.AxisFault {
		super(null, targetEndpoint+serviceName, false);
	}

    /**
     * Constructor taking the target endpoint
     * @deprecated
     */
    public ExtBranchClientStub(java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
        super(null,targetEndpoint, false);
    }
    
	/**
	 * BranchService API response
	 * @param response
	 * @return
	 */
    public Boolean isResponseOk(BaseStoreResponseType response) {

		String resType = response.getResponseType();

		if(resType == null) {
			resType = "";
		}

		Boolean returnResult = null;

		if("SUCCESS".equals(resType) || "SUCCESS-WARNING".equals(resType)) {

			returnResult = Boolean.TRUE;

		} else {
			ErrorType error = response.getError();
			log.error(serviceName + "_API_Code : "    + error.getCode());
			log.error(serviceName + "_API_Message : " + error.getMessage());
			log.error(serviceName + "_API_Detail : "  + error.getDetail());
			returnResult = Boolean.FALSE;
		}

		if(resType.indexOf("WARNING") >= 0) {
			WarningListType warnListType = response.getWarningList();
			if(warnListType !=null) {
				WarningType[]  warns = warnListType.getWarning();
				if(warns !=null && warns.length > 0) {
					int wLength = warns.length;
					for(int i=0; i<wLength ; i++) {
						log.warn("warn "+i+" = " + warns[i].getCode());
						log.warn("warn "+i+" = " + warns[i].getMessage());
						log.warn("warn "+i+" = " + warns[i].getDetail());
					}
				}
			}
		}

		return returnResult;
	}

    public String getResult(BaseStoreResponseType response) {

    	String result = "";
		String resType = response.getResponseType();

		if(resType == null) {
			resType = "";
		}

		if("SUCCESS".equals(resType) || "SUCCESS-WARNING".equals(resType)) {
			result = resType;
		} else {
			ErrorType error = response.getError();
			log.error(serviceName + "_API_Code : "    + error.getCode());
			log.error(serviceName + "_API_Message : " + error.getMessage());
			log.error(serviceName + "_API_Detail : "  + error.getDetail());

			result = error.getCode() + " - " + error.getDetail();
		}

		if(resType.indexOf("WARNING")>=0) {
			WarningListType warnListType = response.getWarningList();
			if(warnListType !=null) {
				WarningType[]  warns = warnListType.getWarning();
				if(warns !=null && warns.length > 0) {
					int wLength = warns.length;
					for(int i=0; i<wLength ; i++) {
						log.warn("warn "+i+" = " + warns[i].getCode());
						log.warn("warn "+i+" = " + warns[i].getMessage());
						log.warn("warn "+i+" = " + warns[i].getDetail());
					}
				}
			}
		}
		return result ;
	}
 
    /**
     * 장보기 지점관리 API - 지점을 생성하는 API로 메시지에 포함되어 있는 License 기반으로 상위 스토어(채널) 하위로 지점을 생성한다.
	 *
     * @param storeInfo
     * @return CreateBranchResponseType
     * @throws
     */
    public CreateBranchResponseType createBranch(StoStoreInfo storeInfo) {
    	
    	CreateBranchRequestType requestType		= new CreateBranchRequestType();
    	CreateBranchRequest request				= new CreateBranchRequest();
    	CreateBranchResponse response			= null;
		AccessCredentialsType credentials		= ac.createBranch(serviceName, "CreateBranch");
		
		String[] dlvTpe = new String[]{"TODAY_DELIVERY"};
		PossibleDeliveryTypes_type0 param = new PossibleDeliveryTypes_type0();
		param.setPossibleDeliveryType(dlvTpe);

		requestType.setAccessCredentials(credentials);
		requestType.setVersion(serviceVersion);
		requestType.setSellerId(sellerId);
		requestType.setBranchId(storeInfo.getStoreId());
		requestType.setBranchName(storeInfo.getStoreNm());
		requestType.setPossibleDeliveryTypes(param);
		requestType.setBranchContractInfo(storeInfo.getTelTxt());
		if(NaverStoreStatusType.ACTIVE.getCode().equals(storeInfo.getStatusCd())) {
			requestType.setActivationYn(true);
		} else {
			requestType.setActivationYn(false);
		}
		
		request.setCreateBranchRequest(requestType);
		
		try {
			response = super.createBranch(request); 
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return response.getCreateBranchResponse();
    }

	/**
	 * 장보기 지점관리 API - 지점과 그 하위 정보를 조회하는 API 로, 지점 ID로 조회를 한다. 배송 슬롯, 우편번호, 휴일 정보는 매핑 데이터가 많아 조회 결과에서 제외한다.
	 *
	 * @param storeInfo
	 * @return RetrieveBranchResponseType
	 * @throws
	 */
    public RetrieveBranchResponseType retrieveBranch(StoStoreInfo storeInfo) {
    	
    	RetrieveBranchRequestType requestType	= new RetrieveBranchRequestType();
    	RetrieveBranchRequest request			= new RetrieveBranchRequest();
    	RetrieveBranchResponse response			= null;
		AccessCredentialsType credentials		= ac.createBranch(serviceName, "RetrieveBranch");
		
		requestType.setAccessCredentials(credentials);
		requestType.setVersion(serviceVersion);
		requestType.setSellerId(sellerId);
		requestType.setBranchId(storeInfo.getStoreId());
		request.setRetrieveBranchRequest(requestType);
		
		try {
			response = super.retrieveBranch(request); 
		} catch (AxisFault e){
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return response.getRetrieveBranchResponse();
    }

    public ModifyBranchResponseType modifyBranch(StoStoreInfo storeInfo, boolean active) {
    	
    	ModifyBranchRequestType requestType		= new ModifyBranchRequestType();
    	ModifyBranchRequest request				= new ModifyBranchRequest();
    	ModifyBranchResponse response			= null;
		AccessCredentialsType credentials		= ac.createBranch(serviceName, "ModifyBranch");
		
		String[] dlvTpe = new String[]{"TODAY_DELIVERY"};
		PossibleDeliveryTypes_type1 param = new PossibleDeliveryTypes_type1();
		param.setPossibleDeliveryType(dlvTpe);

		requestType.setAccessCredentials(credentials);
		requestType.setVersion(serviceVersion);
		requestType.setSellerId(sellerId);
		requestType.setBranchId(storeInfo.getStoreId());
		requestType.setBranchName(storeInfo.getStoreNm());
		requestType.setBranchContractInfo(storeInfo.getTelTxt());
//		if(NaverStoreStatusType.ACTIVE.getCode().equals(storeInfo.getStatusCd())) {
//			requestType.setActivationYn(true);
//		} else {
//			requestType.setActivationYn(false);
//		}
		requestType.setActivationYn(active);		
		
		request.setModifyBranchRequest(requestType);
		
		try {
			response = super.modifyBranch(request); 
		} catch (AxisFault e){
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return response.getModifyBranchResponse();
    }
    
    public AddBranchAreaResponseType addBranchArea(StoStoreInfo storeInfo) throws RemoteException {
    	
		AddBranchAreaRequestType requestType		= new AddBranchAreaRequestType();
		AddBranchAreaRequest request				= new AddBranchAreaRequest();
		AddBranchAreaResponse response				= null;
		AccessCredentialsType credentials			= ac.createBranch(serviceName, "AddBranchArea");

		requestType.setAccessCredentials(credentials);
		requestType.setVersion(serviceVersion);
		requestType.setSellerId(sellerId);
		requestType.setBranchId(storeInfo.getStoreId());
		requestType.setDeliveryType(storeInfo.getDeliverType());
		requestType.setAreaNo(storeInfo.getAreaNo());
		if(NaverStoreStatusType.ACTIVE.getCode().equals(storeInfo.getStatusCd())) {
			requestType.setActivationYn(true);
		} else {
			requestType.setActivationYn(false);
		}
		
		request.setAddBranchAreaRequest(requestType);

		try {
			response = super.addBranchArea(request); 

		} catch (AxisFault e){
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return response.getAddBranchAreaResponse();
    }
    
    public ModifyBranchAreaResponseType modifyBranchArea(StoStoreInfo storeInfo, boolean active) throws RemoteException {
    	
    	ModifyBranchAreaRequestType requestType		= new ModifyBranchAreaRequestType();
    	ModifyBranchAreaRequest request				= new ModifyBranchAreaRequest();
    	ModifyBranchAreaResponse response			= null;
    	AccessCredentialsType credentials			= ac.createBranch(serviceName, "ModifyBranchArea");
    	
    	requestType.setAccessCredentials(credentials);
    	requestType.setVersion(serviceVersion);
    	requestType.setSellerId(sellerId);
    	requestType.setBranchId(storeInfo.getStoreId());
    	requestType.setDeliveryType(storeInfo.getDeliverType());
    	requestType.setAreaNo(storeInfo.getAreaNo());
    	requestType.setActivationYn(active);
    	request.setModifyBranchAreaRequest(requestType);
    	
    	try {
    		response = super.modifyBranchArea(request); 
    		
    	} catch (AxisFault e){
    		e.printStackTrace();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	
    	return response.getModifyBranchAreaResponse();
    }
    
    public AddBranchAreaZipCdResponseType addBranchAreaZipCd(StoStoreInfo storeInfo, boolean active) throws RemoteException {
    	
    	AddBranchAreaZipCdRequestType requestType		= new AddBranchAreaZipCdRequestType();
    	AddBranchAreaZipCdRequest request				= new AddBranchAreaZipCdRequest();
		AccessCredentialsType credentials				= ac.createBranch(serviceName, "AddBranchAreaZipCd");
		AddBranchAreaZipCdResponse response				= null;
		
		requestType.setAccessCredentials(credentials);
		requestType.setVersion(serviceVersion);
		requestType.setSellerId(sellerId);
		requestType.setBranchId(storeInfo.getStoreId());
		requestType.setDeliveryType(storeInfo.getDeliverType());
		requestType.setAreaNo(storeInfo.getAreaNo());
		requestType.setZipCd(storeInfo.getZipnumCd());
		requestType.setActivationYn(active);
		
		request.setAddBranchAreaZipCdRequest(requestType);
		
		try {
			response = super.addBranchAreaZipCd(request); 
		} catch (AxisFault e){
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return response.getAddBranchAreaZipCdResponse();
    }
    
    public ModifyBranchAreaZipCdResponseType modifyBranchAreaZipCd(StoStoreInfo storeInfo, boolean active) throws RemoteException {
    	
    	ModifyBranchAreaZipCdRequestType requestType	= new ModifyBranchAreaZipCdRequestType();
    	ModifyBranchAreaZipCdRequest request			= new ModifyBranchAreaZipCdRequest();
    	ModifyBranchAreaZipCdResponse response			= null;
		AccessCredentialsType credentials				= ac.createBranch(serviceName, "ModifyBranchAreaZipCd");

		requestType.setAccessCredentials(credentials);
		requestType.setVersion(serviceVersion);
		requestType.setSellerId(sellerId);
		requestType.setBranchId(storeInfo.getStoreId());
		requestType.setDeliveryType(storeInfo.getDeliverType());
		requestType.setAreaNo(storeInfo.getAreaNo());
		requestType.setZipCd(storeInfo.getZipnumCd());
		requestType.setActivationYn(active);
		
		request.setModifyBranchAreaZipCdRequest(requestType);
		
		try {
			response = super.modifyBranchAreaZipCd(request); 
		} catch (AxisFault e){
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return response.getModifyBranchAreaZipCdResponse();
    }
    
    public RetrieveBranchAreaZipCdResponseType retrieveBranchAreaZipCdBranch(StoStoreInfo storeInfo) throws RemoteException {
    	
    	RetrieveBranchAreaZipCdRequestType requestType	= new RetrieveBranchAreaZipCdRequestType();
    	RetrieveBranchAreaZipCdRequest request			= new RetrieveBranchAreaZipCdRequest();
    	RetrieveBranchAreaZipCdResponse response		= null;
		AccessCredentialsType credentials				= ac.createBranch(serviceName, "RetrieveBranchAreaZipCd");

		requestType.setAccessCredentials(credentials);
		requestType.setVersion(serviceVersion);
		requestType.setSellerId(sellerId);
		requestType.setBranchId(storeInfo.getStoreId());
		requestType.setDeliveryType(storeInfo.getDeliverType());
		requestType.setAreaNo(storeInfo.getAreaNo());
		requestType.setZipCd(storeInfo.getZipnumCd());
		
		request.setRetrieveBranchAreaZipCdRequest(requestType);
		
		try {
			response = super.retrieveBranchAreaZipCd(request); 
		} catch (AxisFault e){
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return response.getRetrieveBranchAreaZipCdResponse();
    }
    
    public AddBranchHolidayResponseType addBranchHoliday(StoStoreInfo storeInfo) throws RemoteException {
    	
    	AddBranchHolidayRequestType requestType	= new AddBranchHolidayRequestType();
    	AddBranchHolidayRequest request			= new AddBranchHolidayRequest();
    	AddBranchHolidayResponse response		= null;
		AccessCredentialsType credentials		= ac.createBranch(serviceName, "AddBranchHoliday");

		requestType.setAccessCredentials(credentials);
		requestType.setVersion(serviceVersion);
		requestType.setSellerId(sellerId);
		requestType.setBranchId(storeInfo.getStoreId());
		requestType.setDeliveryType("TODAY_DELIVERY");
		requestType.setHoliday(storeInfo.getCloseDt());
		if(CodeConstants.USE_YN_N.equals(storeInfo.getUseStatus()) || CodeConstants.USE_YN_N.equals(storeInfo.getStoreCloseStatus())) {
			requestType.setActivationYn(false);
		} else {
			requestType.setActivationYn(true);
		}
		
		request.setAddBranchHolidayRequest(requestType);

		try {
			response = super.addBranchHoliday(request); 
		} catch (AxisFault e){
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return response.getAddBranchHolidayResponse();
    }
    
    public ModifyBranchHolidayResponseType modifyBranchHoliday(StoStoreInfo storeInfo) throws RemoteException {
    	
    	ModifyBranchHolidayRequestType requestType	= new ModifyBranchHolidayRequestType();
    	ModifyBranchHolidayRequest request			= new ModifyBranchHolidayRequest();
    	ModifyBranchHolidayResponse response		= null;
		AccessCredentialsType credentials			= ac.createBranch(serviceName, "ModifyBranchHoliday");

		requestType.setAccessCredentials(credentials);
		requestType.setVersion(serviceVersion);
		requestType.setSellerId(sellerId);
		requestType.setBranchId(storeInfo.getStoreId());
		requestType.setDeliveryType("TODAY_DELIVERY");
		requestType.setHoliday(storeInfo.getCloseDt());
		if(CodeConstants.USE_YN_N.equals(storeInfo.getUseStatus()) || CodeConstants.USE_YN_N.equals(storeInfo.getStoreCloseStatus())) {
			requestType.setActivationYn(false);
		} else {
			requestType.setActivationYn(true);
		}
		
		request.setModifyBranchHolidayRequest(requestType);
		
		try {
			response = super.modifyBranchHoliday(request); 
		} catch (AxisFault e){
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return response.getModifyBranchHolidayResponse();
    }
    
    public RetrieveBranchHolidayResponseType retrieveBranchHoliday(StoStoreInfo storeInfo) throws RemoteException {
    	
    	RetrieveBranchHolidayRequestType requestType	= new RetrieveBranchHolidayRequestType();
    	RetrieveBranchHolidayRequest request			= new RetrieveBranchHolidayRequest();
    	RetrieveBranchHolidayResponse response			= null;
    	AccessCredentialsType credentials				= ac.createBranch(serviceName, "RetrieveBranchHoliday");
    	
    	requestType.setAccessCredentials(credentials);
    	requestType.setVersion(serviceVersion);
    	requestType.setSellerId(sellerId);
    	requestType.setBranchId(storeInfo.getStoreId());
    	requestType.setDeliveryType("TODAY_DELIVERY");
    	requestType.setHoliday(storeInfo.getCloseDt());
    	
    	request.setRetrieveBranchHolidayRequest(requestType);
    	
    	try {
    		response = super.retrieveBranchHoliday(request); 
    	} catch (AxisFault e){
    		e.printStackTrace();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	
    	return response.getRetrieveBranchHolidayResponse();
    }

}