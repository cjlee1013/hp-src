package kr.co.homeplus.outbound.market.naver.util;

import com.google.gson.Gson;
import com.nhncorp.platform.shopn.seller.SellerServiceStub;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import kr.co.homeplus.outbound.market.naver.claim.model.CancelInfo;
import kr.co.homeplus.outbound.market.naver.claim.model.ClaimOrderDto;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverAddressDto;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverDeliveryDto;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverExchangeInfoDto;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverProductOrderDto;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverReturnInfoDto;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

@Slf4j
public class NaverModelMapper {

    public static ClaimOrderDto orderMap(SellerServiceStub.Order order, String timeStamp) {
        return ClaimOrderDto.builder()
            .orderDate(getConvertObject(order.getOrderDate(), timeStamp))
            .orderId(order.getOrderID())
            .ordererId(getConvertObject(order.getOrdererID(), timeStamp))
            .ordererName(getConvertObject(order.getOrdererName(), timeStamp))
            .ordererTel1(getConvertObject(order.getOrdererTel1(), timeStamp))
            .ordererTel2(getConvertObject(order.getOrdererTel2(), timeStamp))
            .paymentDate(getConvertObject(order.getPaymentDate(), timeStamp))
            .paymentMeans(order.getPaymentMeans())
            .orderDiscountAmount(order.getOrderDiscountAmount())
            .generalPaymentAmount(order.getGeneralPaymentAmount())
            .naverMileagePaymentAmount(order.getNaverMileagePaymentAmount())
            .chargeAmountPaymentAmount(order.getChargeAmountPaymentAmount())
            .checkoutAccumulationPaymentAmount(order.getCheckoutAccumulationPaymentAmount())
            .payLaterPaymentAmount(order.getPayLaterPaymentAmount())
            .isDeliveryMemoParticularInput(order.getIsDeliveryMemoParticularInput())
            .payLocationType(order.getPayLocationType())
            .ordererNo(order.getOrdererNO())
            .build();
    }

    public static NaverProductOrderDto productOrderMap(SellerServiceStub.ProductOrder productOrder, String timeStamp) {
        return NaverProductOrderDto.builder()
            .productOrderId(productOrder.getProductOrderID())
            .productOrderStatus(getConvertObject(productOrder.getProductOrderStatus(), timeStamp))
            .claimType(getConvertObject(productOrder.getClaimType(), timeStamp))
            .claimStatus(getConvertObject(productOrder.getClaimStatus(), timeStamp))
            .productId(productOrder.getProductID())
            .productName(productOrder.getProductName())
            .productClass(productOrder.getProductClass())
            .productOption(productOrder.getProductOption())
            .quantity(productOrder.getQuantity())
            .unitPrice(productOrder.getUnitPrice())
            .optionPrice(productOrder.getOptionPrice())
            .optionCode(productOrder.getOptionCode())
            .totalProductAmount(productOrder.getTotalProductAmount())
            .productDiscountAmount(productOrder.getProductDiscountAmount())
            .productImediateDiscountAmount(productOrder.getProductImediateDiscountAmount())
            .productProductDiscountAmount(productOrder.getProductProductDiscountAmount())
            .productMultiplePurchaseDiscountAmount(productOrder.getProductMultiplePurchaseDiscountAmount())
            .totalPaymentAmount(productOrder.getTotalPaymentAmount())
            .sellerProductCode(productOrder.getSellerProductCode())
            .mallId(productOrder.getMallID())
            .expectedDeliveryMethod(getConvertObject(productOrder.getExpectedDeliveryMethod(), timeStamp))
            .packageNumber(productOrder.getPackageNumber())
            .shippingFeeType(productOrder.getShippingFeeType())
            .deliveryPolicyType(productOrder.getDeliveryPolicyType())
            .deliveryFeeAmount(productOrder.getDeliveryFeeAmount())
            .sectionDeliveryFee(productOrder.getSectionDeliveryFee())
            .deliveryDiscountAmount(productOrder.getDeliveryDiscountAmount())
            .shippingMemo(productOrder.getShippingMemo())
            .shippingDueDate(getConvertObject(productOrder.getShippingDueDate(), timeStamp))
            .decisionDate(getConvertObject(productOrder.getDecisionDate(), timeStamp))
            .freeGift(productOrder.getFreeGift())
            .placeOrderStatus(getConvertObject(productOrder.getPlaceOrderStatus(), timeStamp))
            .placeOrderDate(getConvertObject(productOrder.getPlaceOrderDate(), timeStamp))
            .delayedDispatchReason(getConvertObject(productOrder.getDelayedDispatchReason(), timeStamp))
            .delayedDispatchDetailedReason(productOrder.getDelayedDispatchDetailedReason())
            .sellerBurdenDiscountAmount(productOrder.getSellerBurdenDiscountAmount())
            .sellerBurdenImediateDiscountAmount(productOrder.getSellerBurdenImediateDiscountAmount())
            .sellerBurdenProductDiscountAmount(productOrder.getSellerBurdenProductDiscountAmount())
            .sellerBurdenMultiplePurchaseDiscountAmount(productOrder.getSellerBurdenMultiplePurchaseDiscountAmount())
            .commissionRatingType(productOrder.getCommissionRatingType())
            .commissionPrePayStatus(getConvertObject(productOrder.getCommissionPrePayStatus(), timeStamp))
            .paymentCommission(productOrder.getPaymentCommission())
            .saleCommission(productOrder.getSaleCommission())
            .channelCommission(productOrder.getChannelCommission())
            .knowledgeShoppingSellingInterlockCommission(productOrder.getKnowledgeShoppingSellingInterlockCommission())
            .inflowPath(productOrder.getInflowPath())
            .itemNo(productOrder.getItemNo())
            .optionManageCode(productOrder.getOptionManageCode())
            .purchaserSocialSecurityNo(getConvertObject(productOrder.getPurchaserSocialSecurityNo(), timeStamp))
            .sellerCustomCode1(productOrder.getSellerCustomCode1())
            .sellerCustomCode2(productOrder.getSellerCustomCode2())
            .claimId(productOrder.getClaimID())
            .individualCustomUniqueCode(getConvertObject(productOrder.getIndividualCustomUniqueCode(), timeStamp))
            .giftReceivingStatus(getConvertObject(productOrder.getGiftReceivingStatus(), timeStamp))
            .sellerBurdenStoreDiscountAmount(productOrder.getSellerBurdenStoreDiscountAmount())
            .sellerBurdenMultiplePurchaseDiscountType(getConvertObject(productOrder.getSellerBurdenMultiplePurchaseDiscountType(), timeStamp))
            .branchId(productOrder.getBranchID())
            .isAcceptAlternativeProduct(productOrder.getIsAcceptAlternativeProduct())
            .deliveryAttributeType(getConvertObject(productOrder.getDeliveryAttributeType(), timeStamp))
            .deliveryOperationDate(getConvertObject(productOrder.getDeliveryOperationDate(), timeStamp))
            .deliveryStartTime(productOrder.getDeliveryStartTime())
            .deliveryEndTime(productOrder.getDeliveryEndTime())
            .slotId(productOrder.getSlotID())
            .branchBenefitType(getConvertObject(productOrder.getBranchBenefitType(), timeStamp))
            .branchBenefitValue(productOrder.getBranchBenefitValue())
            .build();
    }

    public static NaverAddressDto addressMap(SellerServiceStub.Address address, String timeStamp) {
        if(address != null) {
            return NaverAddressDto.builder()
                .addressType(getConvertObject(address.getAddressType(), timeStamp))
                .zipCode(address.getZipCode())
                .baseAddress(getConvertObject(address.getBaseAddress(), timeStamp))
                .detailedAddress(getConvertObject(address.getDetailedAddress(), timeStamp))
                .isRoadNameAddress(String.valueOf(address.getIsRoadNameAddress()))
                .city(address.getCity())
                .state(address.getState())
                .country(address.getCountry())
                .tel1(getConvertObject(address.getTel1(), timeStamp))
                .tel2(getConvertObject(address.getTel2(), timeStamp))
                .name(getConvertObject(address.getName(), timeStamp))
                .pickupLocationType(getConvertObject(address.getPickupLocationType(), timeStamp))
                .pickupLocationContent(getConvertObject(address.getPickupLocationContent(), timeStamp))
                .entryMethod(getConvertObject(address.getEntryMethod(), timeStamp))
                .entryMethodContent(getConvertObject(address.getEntryMethodContent(), timeStamp))
                .build();
        } else {
            return NaverAddressDto.builder().build();
        }
    }

    public static NaverDeliveryDto deliveryMap(SellerServiceStub.Delivery delivery, String timeStamp) {
        return NaverDeliveryDto.builder()
            .deliveryStatus(delivery.getDeliveryStatus())
            .deliveryMethod(getConvertObject(delivery.getDeliveryMethod(), timeStamp))
            .deliveryCompany(delivery.getDeliveryCompany())
            .trackingNumber(delivery.getTrackingNumber())
            .sendDate(getConvertObject(delivery.getSendDate(), timeStamp))
            .pickupDate(getConvertObject(delivery.getPickupDate(), timeStamp))
            .deliveredDate(getConvertObject(delivery.getDeliveredDate(), timeStamp))
            .isWrongTrackingNumber(getConvertObject(delivery.getIsWrongTrackingNumber(), timeStamp))
            .wrongTrackingNumberRegisteredDate(getConvertObject(delivery.getWrongTrackingNumberRegisteredDate(), timeStamp))
            .wrongTrackingNumberType(delivery.getWrongTrackingNumberType())
            .build();
    }

    public static CancelInfo cancelInfoMap(SellerServiceStub.CancelInfo cancelInfo, String timeStamp) {
        return CancelInfo.builder()
            .claimStatus(getConvertObject(cancelInfo.getClaimStatus(), timeStamp))
            .claimRequestDate(getConvertObject(cancelInfo.getClaimRequestDate(), timeStamp))
            .requestChannel(cancelInfo.getRequestChannel())
            .cancelReason(getConvertObject(cancelInfo.getCancelReason(), timeStamp))
            .cancelDetailedReason(getConvertObject(cancelInfo.getCancelDetailedReason(), timeStamp))
            .cancelCompletedDate(getConvertObject(cancelInfo.getCancelCompletedDate(), timeStamp))
            .cancelApprovalDate(getConvertObject(cancelInfo.getCancelApprovalDate(), timeStamp))
            .refundExpectedDate(getConvertObject(cancelInfo.getRefundExpectedDate(), timeStamp))
            .refundStandbyStatus(cancelInfo.getRefundStandbyStatus())
            .refundStandbyReason(cancelInfo.getRefundStandbyReason())
            .build();
    }

    public static NaverReturnInfoDto returnInfoMap(SellerServiceStub.ReturnInfo returnInfo, String timeStamp) {
        return NaverReturnInfoDto.builder()
            .claimStatus(getConvertObject(returnInfo.getClaimStatus(), timeStamp))
            .claimRequestDate(getConvertObject(returnInfo.getClaimRequestDate(), timeStamp))
            .requestChannel(returnInfo.getRequestChannel())
            .returnReason(getConvertObject(returnInfo.getReturnReason(), timeStamp))
            .returnDetailedReason(returnInfo.getReturnDetailedReason())
            .holdbackStatus(getConvertObject(returnInfo.getHoldbackStatus(), timeStamp))
            .holdbackReason(getConvertObject(returnInfo.getHoldbackReason(), timeStamp))
            .holdbackDetailedReason(returnInfo.getHoldbackDetailedReason())
            .collectAddress(addressMap(returnInfo.getCollectAddress(), timeStamp))
            .returnReceiveAddress(addressMap(returnInfo.getReturnReceiveAddress(), timeStamp))
            .collectStatus(returnInfo.getCollectStatus())
            .collectDeliveryMethod(getConvertObject(returnInfo.getCollectDeliveryMethod(), timeStamp))
            .collectDeliveryCompany(returnInfo.getCollectDeliveryCompany())
            .collectTrackingNumber(returnInfo.getCollectTrackingNumber())
            .collectCompletedDate(getConvertObject(returnInfo.getCollectCompletedDate(), timeStamp))
            .claimDeliveryFeeDemandAmount(returnInfo.getClaimDeliveryFeeDemandAmount())
            .claimDeliveryFeePayMethod(returnInfo.getClaimDeliveryFeePayMethod())
            .claimDeliveryFeePayMeans(returnInfo.getClaimDeliveryFeePayMeans())
            .etcFeeDemandAmount(returnInfo.getEtcFeeDemandAmount())
            .etcFeePayMethod(returnInfo.getEtcFeePayMethod())
            .etcFeePayMeans(returnInfo.getEtcFeePayMeans())
            .refundStandbyStatus(returnInfo.getRefundStandbyStatus())
            .refundStandbyReason(returnInfo.getRefundStandbyReason())
            .refundExpectedDate(getConvertObject(returnInfo.getRefundExpectedDate(), timeStamp))
            .returnCompletedDate(getConvertObject(returnInfo.getReturnCompletedDate(), timeStamp))
            .holdbackConfigDate(getConvertObject(returnInfo.getHoldbackConfigDate(), timeStamp))
            .holdbackConfigurer(returnInfo.getHoldbackConfigurer())
            .holdbackReleaseDate(getConvertObject(returnInfo.getHoldbackReleaseDate(), timeStamp))
            .holdbackReleaser(returnInfo.getHoldbackReleaser())
            .claimDeliveryFeeProductOrderIds(returnInfo.getClaimDeliveryFeeProductOrderIds())
            .claimDeliveryFeeDiscountAmount(returnInfo.getClaimDeliveryFeeDiscountAmount())
            .build();
    }

    public static NaverExchangeInfoDto exchangeInfoMap(SellerServiceStub.ExchangeInfo exchangeInfo, String timeStamp) {
        return NaverExchangeInfoDto.builder()
            .claimStatus(getConvertObject(exchangeInfo.getClaimStatus(), timeStamp))
            .claimRequestDate(getConvertObject(exchangeInfo.getClaimRequestDate(), timeStamp))
            .requestChannel(exchangeInfo.getRequestChannel())
            .exchangeReason(getConvertObject(exchangeInfo.getExchangeReason(), timeStamp))
            .exchangeDetailedReason(exchangeInfo.getExchangeDetailedReason())
            .holdbackStatus(getConvertObject(exchangeInfo.getHoldbackStatus(), timeStamp))
            .holdbackReason(getConvertObject(exchangeInfo.getHoldbackReason(), timeStamp))
            .holdbackDetailedReason(exchangeInfo.getHoldbackDetailedReason())
            .collectAddress(addressMap(exchangeInfo.getCollectAddress(), timeStamp))
            .returnReceiveAddress(addressMap(exchangeInfo.getReturnReceiveAddress(), timeStamp))
            .collectStatus(exchangeInfo.getCollectStatus())
            .collectDeliveryMethod(getConvertObject(exchangeInfo.getCollectDeliveryMethod(), timeStamp))
            .collectDeliveryCompany(exchangeInfo.getCollectDeliveryCompany())
            .collectTrackingNumber(exchangeInfo.getCollectTrackingNumber())
            .collectCompletedDate(getConvertObject(exchangeInfo.getCollectCompletedDate(), timeStamp))
            .reDeliveryStatus(exchangeInfo.getReDeliveryStatus())
            .reDeliveryMethod(getConvertObject(exchangeInfo.getReDeliveryMethod(), timeStamp))
            .reDeliveryCompany(exchangeInfo.getReDeliveryCompany())
            .reDeliveryTrackingNumber(exchangeInfo.getReDeliveryTrackingNumber())
            .claimDeliveryFeeDemandAmount(exchangeInfo.getClaimDeliveryFeeDemandAmount())
            .claimDeliveryFeePayMethod(exchangeInfo.getClaimDeliveryFeePayMethod())
            .claimDeliveryFeePayMeans(exchangeInfo.getClaimDeliveryFeePayMeans())
            .etcFeeDemandAmount(exchangeInfo.getEtcFeeDemandAmount())
            .etcFeePayMethod(exchangeInfo.getEtcFeePayMethod())
            .etcFeePayMeans(exchangeInfo.getEtcFeePayMeans())
            .holdbackConfigDate(getConvertObject(exchangeInfo.getHoldbackConfigDate(), timeStamp))
            .holdbackConfigurer(exchangeInfo.getHoldbackConfigurer())
            .holdbackReleaseDate(getConvertObject(exchangeInfo.getHoldbackReleaseDate(), timeStamp))
            .holdbackReleaser(exchangeInfo.getHoldbackReleaser())
            .reDeliveryOperationDate(getConvertObject(exchangeInfo.getReDeliveryOperationDate(), timeStamp))
            .claimDeliveryFeeProductOrderIds(exchangeInfo.getClaimDeliveryFeeProductOrderIds())
            .claimDeliveryFeeDiscountAmount(exchangeInfo.getClaimDeliveryFeeDiscountAmount())
            .build();
    }

    private static String getConvertObject(Object param, String timeStamp) {
        if(param instanceof Calendar) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(((Calendar)param).getTime());
        } else if (param instanceof Boolean) {
            return String.valueOf(param);
        } else {
            return param == null ? null : NaverCryptoUtil.decrypt(timeStamp, String.valueOf(param));
        }
    }

    public static ModelMapper createModelMapper(PropertyMap<?, ?> propertyMap) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.addMappings(propertyMap);
        return modelMapper;
    }
}
