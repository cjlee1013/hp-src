package kr.co.homeplus.outbound.market.naver.inquiry.service;

import com.nhncorp.platform.shopn.QuestionAnswerServiceStub;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.AccessCredentialsType;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.BaseResponseType;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.ErrorType;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.GetQuestionAnswerListRequest;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.GetQuestionAnswerListRequestType;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.GetQuestionAnswerListResponseType;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.ManageQuestionAnswerRequest;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.ManageQuestionAnswerRequestType;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.ManageQuestionAnswerResponseType;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.WarningListType;
import com.nhncorp.platform.shopn.QuestionAnswerServiceStub.WarningType;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.UUID;
import kr.co.homeplus.outbound.core.utility.StringUtil;
import kr.co.homeplus.outbound.market.naver.config.MarketNaverConfig;
import kr.co.homeplus.outbound.market.naver.inquiry.model.NaverInquiryGetDto;
import kr.co.homeplus.outbound.market.naver.inquiry.model.NaverInquirySetDto;
import kr.co.homeplus.outbound.market.naver.util.NaverCryptoUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExtQuestionAnswerClientStub {

	private final MarketNaverConfig marketNaverConfig;
	private String timeStamp;

	/**
	 * 문의 서비스 응답 성공여부 검사
	 *
	 * @return Boolean.TRUE SUCCESS
	 * Boolean.FALSE FAIL
	 */
	public Boolean isResponseOk(BaseResponseType response) {
		String resType = response.getResponseType();
		if(resType==null)
			resType = "";

		Boolean returnResult = null;
		//결과 출력
		if("SUCCESS".equals(resType) || "SUCCESS-WARNING".equals(resType)) {
			//응답메시지 처리 getxxx
			log.debug("isResponseOk_RequestID = "+ response.getRequestID());
			returnResult = Boolean.TRUE;

		} else {
			ErrorType error = response.getError();
			log.error(marketNaverConfig.getQuestion().getServiceName() + "_API_Code : "    + error.getCode());
			log.error(marketNaverConfig.getQuestion().getServiceName() + "_API_Message : " + error.getMessage());
			log.error(marketNaverConfig.getQuestion().getServiceName() + "_API_Detail : "  + error.getDetail());
			returnResult = Boolean.FALSE;
		}

		if(resType.contains("WARNING")) {
			// :TODO: 경고 발생시 공통 처리 필요
			WarningListType warnList = response.getWarningList();
			if(warnList !=null) {
				WarningType[]  warns = warnList.getWarning();
				if(warns !=null && warns.length > 0) {
					int wLength = warns.length;
					for(int i=0; i<wLength ; i++) {
						// :TODO: 경고 발생시 어떻게 처리할 것인지 확인 필요
						log.warn("warn "+i+" CODE = " + warns[i].getCode());
						log.warn("warn "+i+" MSG  = " + warns[i].getMessage());
						log.warn("warn "+i+" DTL  = " + warns[i].getDetail());
					}
				}
			}
		}

		return returnResult ;
	}

	/**
	 * 상품 문의 내역 조회
	 *
	 * @param
	 * @return
	 * @throws RemoteException
	 * @throws ParseException
	 */
	public GetQuestionAnswerListResponseType getQuestionAnswerList(NaverInquirySetDto naverInquirySetDto) throws RemoteException{
		GetQuestionAnswerListRequestType requestType = new GetQuestionAnswerListRequestType();

		requestType.setAccessCredentials(this.createQuestionAnswerAcType("GetQuestionAnswerList"));
		requestType.setVersion(marketNaverConfig.getQuestion().getServiceVersion());
		requestType.setRequestID(UUID.randomUUID().toString());
		requestType.setSellerId(marketNaverConfig.getSellerId());
		requestType.setFromDate(StringUtil.getDisplayDate(naverInquirySetDto.getInquiryTimeFrom(),"-"));
		requestType.setToDate(StringUtil.getDisplayDate(naverInquirySetDto.getInquiryTimeTo(),"-"));
		requestType.setAnswered("N");

		GetQuestionAnswerListRequest request = new GetQuestionAnswerListRequest();
		request.setGetQuestionAnswerListRequest(requestType);
		QuestionAnswerServiceStub questionAnswerServiceStub = new QuestionAnswerServiceStub(marketNaverConfig.getTargetEndpoint() + marketNaverConfig.getQuestion().getServiceName());

		return questionAnswerServiceStub.getQuestionAnswerList(request).getGetQuestionAnswerListResponse();
}

	/**
	 * 상품 문의 답변 메시지 전송
	 *
	 * @param req
	 * @return
	 * @throws RemoteException
	 * @throws ParseException
	 */
	public ManageQuestionAnswerResponseType manageQuestionAnswer(NaverInquiryGetDto req) throws RemoteException {
		ManageQuestionAnswerRequestType requestType = new ManageQuestionAnswerRequestType();
		requestType.setAccessCredentials(this.createQuestionAnswerAcType("ManageQuestionAnswer"));
		requestType.setRequestID(UUID.randomUUID().toString());
		requestType.setVersion(marketNaverConfig.getQuestion().getServiceVersion());
		requestType.setSellerId(marketNaverConfig.getSellerId());
		requestType.setQuestionAnswerId(req.getMessageNo());
		requestType.setAnswer(req.getResponseContents());

		ManageQuestionAnswerRequest request = new ManageQuestionAnswerRequest();
		request.setManageQuestionAnswerRequest(requestType);

		QuestionAnswerServiceStub questionAnswerServiceStub = new QuestionAnswerServiceStub(marketNaverConfig.getTargetEndpoint() + marketNaverConfig.getQuestion().getServiceName());

		return questionAnswerServiceStub.manageQuestionAnswer(request).getManageQuestionAnswerResponse();
	}

	private AccessCredentialsType createQuestionAnswerAcType(String operationName) {
		AccessCredentialsType accessCredentialsType = NaverCryptoUtil.createQeuestionAnswerAcType(marketNaverConfig.getQuestion().getServiceName(), operationName);
		this.timeStamp = accessCredentialsType.getTimestamp();
		return accessCredentialsType;
	}

}
