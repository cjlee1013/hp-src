package kr.co.homeplus.outbound.market.eleven.item.enums;

import javax.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;


public enum ResultMessageResultCodeType {
	@XmlEnumValue("0")
	SUCCESS("0", "성공"),

	@XmlEnumValue("-100")
	API_AUTH_FAIL("-100", "API인증실패"),

	@XmlEnumValue("-500")
	FAIL("-500", "실패"),

	@XmlEnumValue("-509")
	MARTNO_NOT_VALID("-509", "마트번호 유효하지않음"),

	@XmlEnumValue("-510")
	STRNO_NOT_VALID("-510", "지점번호 유효하지않음"),

	@XmlEnumValue("-511")
	PRDNO_NOT_VALID("-511", "상품번호 유효하지않음"),

	@XmlEnumValue("-590")
	OVER_FLOW("-590", "수량초과"),

	@XmlEnumValue("-800")
	DB_FAIL("-800", "DB실패"),

	@XmlEnumValue("-999")
	API_PROC_FAIL("-999", "API실행실패");

	@Getter
	private String code;
	@Getter
	private String desc;

	private ResultMessageResultCodeType(String code, String desc) {
		this.desc = desc;
		this.code = code;
	}
}
