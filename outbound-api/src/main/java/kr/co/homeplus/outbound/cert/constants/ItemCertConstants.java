package kr.co.homeplus.outbound.cert.constants;

/**
 * 안전인증 메시지 Constants
 */
public class ItemCertConstants {

    public final static String SUCCESS  = "인증성공";
    public final static String FAIL     = "인증실패";
    public final static String ERROR    = "ERROR";


    public final static String CERT_Y   = "Y";
    public final static String CERT_N   = "N";

}
