package kr.co.homeplus.outbound.market.naver;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.homeplus.outbound.market.naver.model.Order;
import kr.co.homeplus.outbound.market.naver.model.OrderCancelRequest;
import kr.co.homeplus.outbound.market.naver.model.OrderCancelResponse;
import kr.co.homeplus.outbound.market.naver.model.PlaceOrderRequest;
import kr.co.homeplus.outbound.market.naver.model.PlaceOrderResponse;
import kr.co.homeplus.outbound.market.naver.service.NaverOrderService;
import kr.co.homeplus.outbound.market.naver.service.NaverService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "마켓연동 > 네이버")
@Slf4j
@RestController
@RequestMapping("/market/naver")
@RequiredArgsConstructor
public class NaverController {

    private final NaverService naverService;
    private final NaverOrderService naverOrderService;

    @ApiOperation(value = "네이버 주문조회")
    @GetMapping("/orderList")
    public ResponseObject<List<Order>> getOrderList(String fromDt, String toDt, String lastChangedStatusCode) throws Exception {
        if ("PAYED".equals(lastChangedStatusCode)) {
            return ResourceConverter.toResponseObject(naverOrderService.getChangedProductOrderList(fromDt, toDt, lastChangedStatusCode));
        }
        return ResourceConverter.toResponseObject(naverService.getChangedProductOrderList(fromDt, toDt, lastChangedStatusCode));
    }

    @ApiOperation(value = "네이버 발주")
    @PostMapping("/placeOrder")
    public ResponseObject<PlaceOrderResponse> placeOrder(@RequestBody PlaceOrderRequest placeOrderRequest) throws Exception {
        return ResourceConverter.toResponseObject(naverService.placeOrder(placeOrderRequest));
    }

    @ApiOperation(value = "네이버 취소")
    @PostMapping("/orderCancel")
    public ResponseObject<OrderCancelResponse> orderCancel(@RequestBody OrderCancelRequest orderCancelRequest) throws Exception {
        return ResourceConverter.toResponseObject(naverService.orderCancel(orderCancelRequest));
    }
}
