package kr.co.homeplus.outbound.core.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kr.co.homeplus.outbound.core.utility.xml.Jaxb2RootElementHttpMessageEscapeConverter;
import kr.co.homeplus.outbound.core.utility.xml.JaxbCharacterEscapeHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Configuration
public class RestConfig {

    @Value("${restTemplate.factory.readTimeout}")
    private int READ_TIMEOUT;

    @Value("${restTemplate.factory.connectTimeout}")
    private int CONNECT_TIMEOUT;

    @Value("${enc-key.eleven}")
    private String elevenStEnc;

    @Bean
    @Qualifier("sk11RestTemplate")
    public RestTemplate restTemplate() {

        // timeout
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();

        factory.setReadTimeout(READ_TIMEOUT);
        factory.setConnectTimeout(CONNECT_TIMEOUT);

        RestTemplate restTemplate = new RestTemplate(factory);

        // messageConverter
        List<HttpMessageConverter<?>> messageConverters =
            new ArrayList<HttpMessageConverter<?>>();

        Jaxb2RootElementHttpMessageEscapeConverter jaxb2RootElementHttpMessageEscapeConverter = new Jaxb2RootElementHttpMessageEscapeConverter();
        jaxb2RootElementHttpMessageEscapeConverter.setCharacterEscapeHandler(new JaxbCharacterEscapeHandler());

        messageConverters.add(jaxb2RootElementHttpMessageEscapeConverter);
        messageConverters.add(new FormHttpMessageConverter());
        messageConverters.add(new StringHttpMessageConverter());
        messageConverters.add(new ByteArrayHttpMessageConverter());

        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        mappingJackson2HttpMessageConverter.setSupportedMediaTypes(
            Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_XML));
        messageConverters.add(mappingJackson2HttpMessageConverter);

        restTemplate.setMessageConverters(messageConverters);

        log.debug("RestTemplate Config (read/connect - timeout) : " + READ_TIMEOUT  + " / " + CONNECT_TIMEOUT );

        return restTemplate;
    }

}
