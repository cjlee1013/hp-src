package kr.co.homeplus.outbound.item.model.naver;

public class NaverApisPamphletEventDTO  {
    private static final long serialVersionUID = 6202723502260703677L;

    private String eventId;
    private String deliveryType;
    private String branchId;
    private String eventName;
    private String startDate;
    private String endDate;
    private String[] productIds;

    public String getEventId() {
        return eventId;
    }
    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
    public String getDeliveryType() {
        return deliveryType;
    }
    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }
    public String getBranchId() {
        return branchId;
    }
    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }
    public String getEventName() {
        return eventName;
    }
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    public String getEndDate() {
        return endDate;
    }
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    public String[] getProductIds() {
        return productIds;
    }
    public void setProductIds(String[] productIds) {
        this.productIds = productIds;
    }
}
