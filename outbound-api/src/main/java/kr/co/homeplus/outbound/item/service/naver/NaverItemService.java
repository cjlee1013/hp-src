package kr.co.homeplus.outbound.item.service.naver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.naver.api.security.client.MACManager;
import com.naver.api.util.Type;
import com.nhncorp.platform.shopn.ImageServiceStub.ImageReturnListType;
import com.nhncorp.platform.shopn.ImageServiceStub.ImageReturnType;
import com.nhncorp.platform.shopn.ImageServiceStub.UploadImageResponseType;
import com.nhncorp.platform.shopn.ProductServiceStub.BranchBenefitDiscountContentType;
import com.nhncorp.platform.shopn.ProductServiceStub.BranchBenefitDiscountListType;
import com.nhncorp.platform.shopn.ProductServiceStub.BranchBenefitDiscountType;
import com.nhncorp.platform.shopn.ProductServiceStub.CategoryReturnType;
import com.nhncorp.platform.shopn.ProductServiceStub.CertificationInfoType;
import com.nhncorp.platform.shopn.ProductServiceStub.CertificationListType;
import com.nhncorp.platform.shopn.ProductServiceStub.CertificationType;
import com.nhncorp.platform.shopn.ProductServiceStub.ChangeProductSaleStatusResponseType;
import com.nhncorp.platform.shopn.ProductServiceStub.CombinationOptionItemListType;
import com.nhncorp.platform.shopn.ProductServiceStub.CombinationOptionItemType;
import com.nhncorp.platform.shopn.ProductServiceStub.CombinationOptionNamesType;
import com.nhncorp.platform.shopn.ProductServiceStub.CombinationOptionType;
import com.nhncorp.platform.shopn.ProductServiceStub.DeliveryType;
import com.nhncorp.platform.shopn.ProductServiceStub.EtcSummaryType;
import com.nhncorp.platform.shopn.ProductServiceStub.GetCategoryInfoResponseType;
import com.nhncorp.platform.shopn.ProductServiceStub.GetOptionResponseType;
import com.nhncorp.platform.shopn.ProductServiceStub.GetProductResponseType;
import com.nhncorp.platform.shopn.ProductServiceStub.ImageType;
import com.nhncorp.platform.shopn.ProductServiceStub.ManageOptionResponseType;
import com.nhncorp.platform.shopn.ProductServiceStub.ManageProductResponseType;
import com.nhncorp.platform.shopn.ProductServiceStub.ModelType;
import com.nhncorp.platform.shopn.ProductServiceStub.OptionType;
import com.nhncorp.platform.shopn.ProductServiceStub.OptionalListType;
import com.nhncorp.platform.shopn.ProductServiceStub.OriginAreaType;
import com.nhncorp.platform.shopn.ProductServiceStub.ProductSummaryType;
import com.nhncorp.platform.shopn.ProductServiceStub.ProductType;
import com.nhncorp.platform.shopn.ProductServiceStub.SaleStatusType;
import com.nhncorp.platform.shopn.ProductServiceStub.StringCodeType;
import com.nhncorp.platform.shopn.ProductServiceStub.URLType;
import com.nhncorp.platform.shopn.ProductServiceStub.UnitCapacityType;
import java.rmi.RemoteException;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import javax.annotation.PostConstruct;
import kr.co.homeplus.outbound.category.service.naver.AccessCredentials;
import kr.co.homeplus.outbound.category.service.naver.ExtProductClientStub;
import kr.co.homeplus.outbound.common.enums.NaverStoreStatusType;
import kr.co.homeplus.outbound.constants.ExceptionCode;
import kr.co.homeplus.outbound.core.constants.CodeConstants;
import kr.co.homeplus.outbound.core.constants.PatternConstants;
import kr.co.homeplus.outbound.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.outbound.core.maketLog.service.MarketItemLogService;
import kr.co.homeplus.outbound.core.utility.DateUtil;
import kr.co.homeplus.outbound.core.utility.RestUtil;
import kr.co.homeplus.outbound.item.constants.EventConstants;
import kr.co.homeplus.outbound.item.constants.ItemConstants;
import kr.co.homeplus.outbound.item.enums.MarketItemStatus;
import kr.co.homeplus.outbound.item.enums.PickIconType;
import kr.co.homeplus.outbound.item.enums.SendType;
import kr.co.homeplus.outbound.item.mapper.ItemMasterMapper;
import kr.co.homeplus.outbound.item.mapper.ItemSlaveMapper;
import kr.co.homeplus.outbound.item.model.ItemMarketImgSendGetDto;
import kr.co.homeplus.outbound.item.model.ItemMarketImgSendSetDto;
import kr.co.homeplus.outbound.item.model.ItemMarketSetDto;
import kr.co.homeplus.outbound.item.model.ItemSpecHistParamDto;
import kr.co.homeplus.outbound.item.model.MarketItemBasicGetDto;
import kr.co.homeplus.outbound.item.model.MarketItemCertGetDto;
import kr.co.homeplus.outbound.item.model.MarketItemGetDto;
import kr.co.homeplus.outbound.item.model.MarketItemOptionGetDto;
import kr.co.homeplus.outbound.item.model.MarketItemSaleGetDto;
import kr.co.homeplus.outbound.item.model.naver.NaverApisPamphletEventDTO;
import kr.co.homeplus.outbound.item.model.naver.StoStoreInfo;
import kr.co.homeplus.outbound.item.service.common.ItemCommonService;
import kr.co.homeplus.outbound.market.eleven.item.enums.SelStatCdType;
import kr.co.homeplus.outbound.market.eleven.item.model.PriceSetDto;
import kr.co.homeplus.outbound.market.eleven.item.model.StorePriceListDto;
import kr.co.homeplus.outbound.response.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.ObjectUtils;
import net.logstash.logback.encoder.org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.axis2.AxisFault;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@Repository
public class NaverItemService extends RestUtil {

    @Value("${plus.resource-routes.imageFront.url}")
    private String imgFrontUrl;

    @Value("${market.naver.bundleGroupId}")
    private String bundleGroupId;

    @Value("${market.naver.apis.host}")
    private String naverApisHost;

    @Value("${market.naver.apis.hmacKey}")
    private String naverApisHmacKey;

    @Value("${market.naver.apis.pamphletEvent-path}")
    private String naverApisPamphletEventPath;


    private final ItemCommonService itemCommonService;

    private final ItemMasterMapper itemMasterMapper;
    private final ItemSlaveMapper itemSlaveMapper;

    private final MarketItemLogService marketItemLogService;

    /*
     * Naver Smart Store API ( Product )
     *
     */
    private ExtProductClientStub extProductClientStub;

    protected NaverItemService(RestTemplate restTemplate,
            ItemCommonService itemCommonService,
            ItemMasterMapper itemMasterMapper,
            ItemSlaveMapper itemSlaveMapper,
            MarketItemLogService marketItemLogService) {
        super(restTemplate);
        this.itemCommonService = itemCommonService;
        this.itemMasterMapper = itemMasterMapper;
        this.itemSlaveMapper = itemSlaveMapper;
        this.marketItemLogService = marketItemLogService;
    }

    private ExtProductClientStub getExtProductClientStub() throws AxisFault {
        if (extProductClientStub == null) {
            Security.addProvider(new BouncyCastleProvider());
            extProductClientStub = new ExtProductClientStub();
            AccessCredentials ac = new AccessCredentials();
            extProductClientStub.setAccessCredentials(ac);	// 인증 클래스 셋팅
        }

        return extProductClientStub;
    }

    /*
     * Naver Smart Store API ( Image )
     *
     */
    private ExtImageClientStub extImageClientStub;

    private ExtImageClientStub getExtImageClientStub() throws AxisFault {
        if (extImageClientStub == null) {
            Security.addProvider(new BouncyCastleProvider());
            extImageClientStub = new ExtImageClientStub();
            AccessCredentials ac = new AccessCredentials();
            extImageClientStub.setAccessCredentials(ac);	// 인증 클래스 셋팅
        }

        return extImageClientStub;
    }

    /**
     * N마트 상품정보 조회
     * @param coopProductId
     * @return GetProductResponseType
     */
    public GetProductResponseType getProduct(String coopProductId) throws Exception {
        return getExtProductClientStub().getProduct(coopProductId);
    }

    /**
     * N마트 상품정보(옵션) 조회
     * @param coopProductId
     * @return GetOptionResponseType
     */
    public GetOptionResponseType getOption(String coopProductId) throws Exception {
        return getExtProductClientStub().getOption(Long.parseLong(coopProductId));
    }

    /**
     * 이미지 전송
     * @return
     */
    public ResponseResult sendImageList(List<String> imageList) throws Exception {

        if (ObjectUtils.isEmpty(imageList)) {
            throw new BusinessLogicException(ExceptionCode.ERROR_CODE_2001);
        }

        UploadImageResponseType responseType = getExtImageClientStub().uploadImage(imageList.toArray(new String[imageList.size()]));

        ResponseResult responseResult = new ResponseResult();
        responseResult.setReturnKey(String.valueOf(imageList.size()));

        return responseResult;
    }

    /**
     * N마트 Swagger 전송 용도
     * @param itemList
     * @return
     * @throws Exception
     */
    public ResponseResult sendItemForDirect(List<String> itemList) throws Exception {

        ItemMarketSetDto itemMarketSetDto = new ItemMarketSetDto();
        itemMarketSetDto.setSendType(SendType.SPOT.getCode()); //수시로 고정
        itemMarketSetDto.setSendYn(CodeConstants.USE_YN_N);
        itemMarketSetDto.setMarketItemStatus(MarketItemStatus.P.getType());
        itemMarketSetDto.setPartnerId(CodeConstants.NMART_PARTNER_ID);

        itemMasterMapper.updateItemMarketSendStart(itemMarketSetDto, itemList);

        ResponseResult responseResult = this.sendItem(itemList);

        return responseResult;
    }
    /**
     * 상품 전송
     * @return
     */
    public ResponseResult sendItem(List<String> itemList) throws Exception {

        ResponseResult responseResult = new ResponseResult();
        List<ResponseResult> resultList = new ArrayList();
        if (ObjectUtils.isEmpty(itemList)) {
            throw new BusinessLogicException(ExceptionCode.ERROR_CODE_2001);
        }

        //마스터 정보 조회 (대표점 기준)
        Map<String, MarketItemGetDto> marketItemGetDtoMap = itemSlaveMapper.getItemMaster(itemList, CodeConstants.DEFAULT_STORE_ID, CodeConstants.CLUB_DEFAULT_STORE_ID, CodeConstants.NMART_PARTNER_ID);

        itemList.stream().forEach(
                itemNo -> {
                    MarketItemGetDto item = marketItemGetDtoMap.get(itemNo);
                    // 1.유효성 검사
                    if (itemCommonService.getDisplayValidCheck(item)) {
                        if(StringUtils.isNotEmpty(item.getBasic().getMarketItemNo())) {
                            this.modifyItem(item);
                        } else {
                            this.setItem(item);
                        }
                    } else {
                        if (ObjectUtils.isEmpty(item)) {
                            item = new MarketItemGetDto();
                            item.setMsg(ExceptionCode.ERROR_CODE_1002.toString());
                            item.setBasic(new MarketItemBasicGetDto());
                            item.getBasic().setPartnerId(CodeConstants.NMART_PARTNER_ID);
                            item.getBasic().setItemNo(itemNo);

                            itemMasterMapper.updateItemMarketStatus(itemCommonService.setMarketItemStop(item, MarketItemStatus.E.getType()));
                        } else {
                            if(StringUtils.isNotEmpty(item.getBasic().getMarketItemNo())) {
                                //1.판매중지 처리
                                try {
                                    this.changeProductSaleStatus(item.getBasic().getMarketItemNo(), NaverStoreStatusType.SUSP);
                                } catch ( Exception e ) {
                                    log.error("(1) 상품 판매 중지 처리 에러 : " + ExceptionUtils.getStackTrace(e));
                                    //기존에 품절처리 되었던 경우 SKIP
                                }
                            }

                            itemMasterMapper.updateItemMarket(itemCommonService.setMarketItemStop(item, MarketItemStatus.E.getType()));
                        }

                        responseResult.setReturnMsg(item.getMsg() + " : " + itemNo);
                    }

                    ResponseResult res = new ResponseResult();
                    res.setReturnKey("마켓번호 : " + item.getBasic().getMarketItemNo());
                    res.setReturnMsg(item.getMsg() + " : " + itemNo);
                    resultList.add(res);
                }
        );

        responseResult.setReturnKey("처리건수 : " + marketItemGetDtoMap.size());
        responseResult.setResultList(resultList);

        return responseResult;
    }

    /**
     * 상품 신규등록/수정
     * @param marketItemGetDto
     * @return
     */
    private ResponseResult setItem(MarketItemGetDto marketItemGetDto) {

        ResponseResult responseResult = new ResponseResult();

        /* 상품 마스터 정보 전송
         * 1. 상품 이미지 체크 ( 전송여부 )
         * 1-1. 상품 이미지 셋팅 ( 전송 또는 재사용 )
         * 2. 상품 정보 셋팅
         * 2-1. 상품 정보 전송
         * 3. 점포별 가격 전송
         * 4. 처리 결과 업데이트
         * 4-1. 오류시 판매 중치 처리 전송
         */
        try {

            ProductType productType = setItemBasicInfo(marketItemGetDto);
            ManageProductResponseType manageProductResponseType = getExtProductClientStub().manageProduct(productType); // [2] >> [2-1]

            // SPEC 전송이력
            // TODO: soap message 변환은 다음기회에.
            marketItemLogService.setItemSpecHist(ItemSpecHistParamDto.builder()
                .itemNo(marketItemGetDto.getBasic().getItemNo())
                .request(productType)
                .partnerId(CodeConstants.NMART_PARTNER_ID)
                .response(manageProductResponseType)
                .methodTxt("manageProduct").build());

            Boolean isOkManageProduct = getExtProductClientStub().isResponseOk(manageProductResponseType);

            if(manageProductResponseType.getResponseType().equalsIgnoreCase("ERROR")) {
                responseResult.setReturnCode(ExceptionCode.ERROR_CODE_1014.getCode());
                responseResult.setReturnMsg(manageProductResponseType.getError().getDetail());
                marketItemGetDto.setMsg(manageProductResponseType.getError().getDetail());
            } else {
                marketItemGetDto.setMsg(manageProductResponseType.getResponseType());
            }

            // 신규 상품번호 채번되었다면.
            if (isOkManageProduct == true && ObjectUtils.isNotEmpty(manageProductResponseType.getProductId())) {

                marketItemGetDto.getBasic().setMarketItemNo(Long.toString((manageProductResponseType.getProductId())));
                itemMasterMapper.updateItemMarket(itemCommonService.setMarketItemActive(marketItemGetDto)); // [4]

                //2.옵션등록
                OptionType optionType = this.setItemOptionInfo(marketItemGetDto);

                if (optionType != null) {
                    ManageOptionResponseType manageOptionResponseType = null;

                    //SmartStore 상품 옵션등록
                    try {
                        manageOptionResponseType = getExtProductClientStub().manageOption(optionType);

                        marketItemLogService.setItemSpecHist(ItemSpecHistParamDto.builder()
                            .itemNo(marketItemGetDto.getBasic().getItemNo())
                            .request(optionType)
                            .partnerId(CodeConstants.NMART_PARTNER_ID)
                            .response(manageOptionResponseType)
                            .methodTxt("manageOption").build());

                    } catch (Exception e){
                        marketItemGetDto.setMsg("옵션등록 에러 ITEM_NO:"+marketItemGetDto.getBasic().getItemNo());
                        log.info("옵션등록 에러 ITEM_NO:"+marketItemGetDto.getBasic().getItemNo()+" "+e.getMessage());
                    }

                    try {
                        Boolean isOkManageOption = getExtProductClientStub().isResponseOk(manageOptionResponseType);

                        if (isOkManageOption == Boolean.FALSE) {
                            marketItemGetDto.setMsg("옵션등록 실패 ITEM_NO:"+marketItemGetDto.getBasic().getItemNo());
                            log.error("옵션등록 실패 ITEM_NO:"+marketItemGetDto.getBasic().getItemNo() + ", Code:"+ manageOptionResponseType.getError().getCode()
                                    + ", Message:"+ manageOptionResponseType.getError().getMessage()+ ", Detail:"+ manageOptionResponseType.getError().getDetail());
                        } else {
                            marketItemGetDto.setMsg("상품 옵션 등록 성공 ITEM_NO:"+marketItemGetDto.getBasic().getItemNo());
                            log.info("상품 옵션 등록 성공 ITEM_NO:"+marketItemGetDto.getBasic().getItemNo());
                        }
                    } catch (Exception e){
                        marketItemGetDto.setMsg("옵션등록 에러 ITEM_NO:"+marketItemGetDto.getBasic().getItemNo());
                        log.error("옵션등록 에러 ITEM_NO:"+marketItemGetDto.getBasic().getItemNo()+" "+e.getMessage());
                    }
                    //연동 상품 히스토리 저장
                    marketItemLogService.setMarketItemHist(marketItemGetDto);

                    responseResult.setReturnKey("신규 상품 처리 완료 되었습니다. (N마트)");

                } else {
                    //todo 판매종료 처리 : 점포가 없는경우 품절처리?
                    responseResult.setReturnKey("상품번호: " + marketItemGetDto.getBasic().getItemNo() + " (점포 미등록). (N마트)");
                    responseResult.setReturnCode(ExceptionCode.ERROR_CODE_1010.getCode());
                    responseResult.setReturnMsg(marketItemGetDto.getMsg());
                    marketItemGetDto.setMsg(ExceptionCode.ERROR_CODE_1010.toString());
                }

            } else {
                // 연동 실패처리
                itemMasterMapper.updateItemMarket(itemCommonService.setMarketItemStop(marketItemGetDto, MarketItemStatus.E.getType())); // [4]
                responseResult.setReturnCode(ExceptionCode.ERROR_CODE_1014.getCode());
                responseResult.setReturnMsg(marketItemGetDto.getMsg());
                marketItemGetDto.setMsg(ExceptionCode.ERROR_CODE_1014.toString());
                log.error("Error :: {}", manageProductResponseType.getError().getDetail());
            }

        } catch(Exception e) {
            log.error("Error :: {}", e.getStackTrace());
            responseResult.setReturnCode(ExceptionCode.ERROR_CODE_1011.getCode());
            responseResult.setReturnMsg(marketItemGetDto.getMsg());
            marketItemGetDto.setMsg(ExceptionCode.ERROR_CODE_1011.toString());
//                // 연동 오류 판매 중지 처리
//                this.setStopItem(marketItemGetDto); [4-1]
        }

        return responseResult;
    }

    /**
     * 상품수정
     * @param marketItemGetDto
     * @return
     */
    private ResponseResult modifyItem(MarketItemGetDto marketItemGetDto) {
        ResponseResult responseResult = new ResponseResult();

        //1.판매중지 처리
        try {
            this.changeProductSaleStatus(marketItemGetDto.getBasic().getMarketItemNo(), NaverStoreStatusType.SUSP);
        } catch ( Exception e ) {
            log.error("(2) 상품 판매 중지 처리 에러 : " + ExceptionUtils.getStackTrace(e));
            //기존에 품절처리 되었던 경우 SKIP
        }

        //2.옵션제거
        this.clearOption(marketItemGetDto.getBasic().getItemNo(), marketItemGetDto.getBasic().getMarketItemNo());

        //3.상품 등록/수정
        ResponseResult result = this.setItem(marketItemGetDto);

        //4.판매중 처리
        if (result.getReturnCode().equals("0")) {
            try {
                this.changeProductSaleStatus(marketItemGetDto.getBasic().getMarketItemNo(), NaverStoreStatusType.SALE);
            } catch ( Exception e ) {
                String msg = "(5) 상품 판매중 처리 에러 : " + ExceptionUtils.getStackTrace(e);
                log.error(msg);
                //기존에 품절처리 되었던 경우 SKIP
            }

            responseResult.setReturnKey(marketItemGetDto.getBasic().getMarketItemNo());
            responseResult.setReturnMsg(marketItemGetDto.getMsg());

            //연동 상품 히스토리 저장
            marketItemLogService.setMarketItemHist(marketItemGetDto);

            return responseResult;
        } else {
            return result;
        }
    }

    @PostConstruct
    public void naverHmacInit() throws Exception {
        MACManager.initialize(Type.KEY, naverApisHmacKey);
        MACManager.syncWithServerTimeByHttpAsync(30000, 30000);
    }

    private HttpHeaders getHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }

    private String getUrl() throws Exception {
        return getUrl(null, false);
    }

    private String getUrl(String param) throws Exception {
        return getUrl(param, false);
    }

    private String getUrl(String param, boolean isPath) throws Exception {
        String url = naverApisHost + naverApisPamphletEventPath + (StringUtils.isEmpty(param) ? "" : ((isPath ? "/" : "?") + param));
        return MACManager.getEncryptUrl(url);
    }

    public List<NaverApisPamphletEventDTO> eventList(NaverApisPamphletEventDTO req) throws Exception {
        if (req == null) req = new NaverApisPamphletEventDTO();
        String url = this.getUrl(StringUtils.isEmpty(req.getBranchId()) ? "" : "branchId="+req.getBranchId());

        HttpEntity httpEntity = new HttpEntity<>(req, getHeaders());
        ParameterizedTypeReference<List<NaverApisPamphletEventDTO>> typeRef = new ParameterizedTypeReference<List<NaverApisPamphletEventDTO>>() {};
        List<NaverApisPamphletEventDTO> response = execute(url, HttpMethod.GET, httpEntity, typeRef);
        return response;

    }

    /**
     * 네이버 점포 전단행사 등록/수정 전체
     * @throws Exception
     */
    public void pamphletEventUpdate() throws Exception {
        List<StoStoreInfo> allStoreList = itemSlaveMapper.getStoreList();

        for (int i = 0; i < allStoreList.size(); i++) {
            this.setPamphletEventUpdateStore(allStoreList.get(i).getStoreId());
        }
    }

    /**
     * 네이버 점포 전단행사 등록/수정 점포
     * @param storeId
     * @return
     * @throws Exception
     */
    public ResponseResult pamphletEventUpdateStore(String storeId) throws Exception {
        this.setPamphletEventUpdateStore(storeId);

        ResponseResult responseResult = new ResponseResult();
        responseResult.setReturnKey(storeId);

        return responseResult;
    }

    /**
     * 네이버 점포 전단행사 등록/수정
     * @param storeId
     * @throws Exception
     */
    private void setPamphletEventUpdateStore(String storeId) throws Exception {
        NaverApisPamphletEventDTO req = new NaverApisPamphletEventDTO();
        req.setBranchId(storeId);
        // 지점별 행사 목록 조회
        List<NaverApisPamphletEventDTO> eventList = this.eventList(req);

        for (int j = 0; j < eventList.size(); j++) {
            // 지점당 하나의 행사만 관리
            if (j == 0) {
                req = eventList.get(j);
            } else {// 중복 등록된 행사는 삭제 처리
                this.eventDelete(eventList.get(j));
            }
        }

        this.eventUpdate(req);
    }

    private NaverApisPamphletEventDTO eventUpdate(NaverApisPamphletEventDTO req) throws Exception {
        ObjectMapper om = new ObjectMapper();

        List<String> list = itemSlaveMapper.getDispLeafletList();

        // 배송타입 (당일 배송 고정) - 당일배송: 'TODAY_ARRIVAL', 새벽배송: 'DAWN_ARRIVAL'
        req.setDeliveryType("TODAY_ARRIVAL");
        // 행사명 : 노출되는 곳이 없어 임의 지정
        req.setEventName("전단행사_" + req.getBranchId());
        // 행사 기간은 당일 ~ 9999.12.31 무기한으로 지정
        req.setStartDate(DateUtil.getFormatDate(new Date(), "yyyy/MM/dd HH:mm"));
        req.setEndDate(DateUtil.getFormatDate(new SimpleDateFormat("yyyyMMdd").parse("99991231"), "yyyy/MM/dd HH:mm"));
        req.setProductIds(list.toArray(new String[list.size()]));

        log.info("eventUpdate req: {}", om.writeValueAsString(req.getBranchId()));

        HttpEntity httpEntity = new HttpEntity<>(req, getHeaders());
        ParameterizedTypeReference<NaverApisPamphletEventDTO> typeRef = new ParameterizedTypeReference<NaverApisPamphletEventDTO>() {};
        NaverApisPamphletEventDTO response = null;
        if (StringUtils.isEmpty(req.getEventId())) {
            if (req.getProductIds().length > 0) {
                log.info("eventUpdate execute start : {}", req.getEventId());

                response = execute(getUrl(), HttpMethod.POST, httpEntity, typeRef);

                log.info("eventUpdate execute end : {}", om.writeValueAsString(response));
            } else {
                log.info("eventUpdate target products not found. [storeId::{}]", req.getBranchId());
            }
        } else {
            HttpMethod method = HttpMethod.DELETE;
            if (req.getProductIds().length > 0) {
                method = HttpMethod.PATCH;
            }
            log.info("eventUpdate EventId method start : {}, {}", req.getEventId(), method);
            response = execute(getUrl(req.getEventId(), true), method, httpEntity, typeRef);

            log.info("eventUpdate EventId method end : {}", om.writeValueAsString(response));
        }

        return response;
    }

    public String eventDelete(NaverApisPamphletEventDTO req) throws Exception {
        HttpEntity httpEntity = new HttpEntity<>(req, getHeaders());
        ParameterizedTypeReference<String> typeRef = new ParameterizedTypeReference<String>() {};
        String response = null;
        if (!StringUtils.isEmpty(req.getEventId())) {
            response = execute(getUrl(req.getEventId(), true), HttpMethod.DELETE, httpEntity, typeRef);
        }
        return response;
    }

    /**
     * 상품 지점별 정보 생성
     * @param marketItemGetDto
     * @return
     */
    private StorePriceListDto setStorePriceInfo(MarketItemGetDto marketItemGetDto) {

        StorePriceListDto dto = new StorePriceListDto();

        List<PriceSetDto> priceSetDtoList = new ArrayList<>();

        String marketItemNo = marketItemGetDto.getBasic().getMarketItemNo();

        marketItemGetDto.getStoreList().forEach( store -> {
            priceSetDtoList.add(PriceSetDto.builder()
                .martNo(CodeConstants.ELEVEN_MART_NO)
                .prdNo(marketItemNo)
                .prdSelQty(store.getStockQty() < 0 ? CodeConstants.ZERO : String.valueOf(store.getStockQty()))
                .selPrc(String.valueOf(store.getSalePrice()))
                .selStatCd(getSelStatCd(store))
                .strNo(store.getMarketStoreId())
                .build());
        });

        if( ObjectUtils.isNotEmpty(priceSetDtoList) ) {
            dto.setStoreProduct(priceSetDtoList);
        }

        return dto;
    }

    private SelStatCdType getSelStatCd(MarketItemSaleGetDto marketItemSaleGetDto) {

        SelStatCdType selStatCdType = SelStatCdType.NO_SELLING;

        if( ObjectUtils.isNotEmpty(marketItemSaleGetDto) ) {

            //마스터 정보와 가격비교 필요한가
            if( CodeConstants.USE_YN_Y.equals(marketItemSaleGetDto.getShipPolicyYn())         //배송정책 등록여부
                && CodeConstants.STOP_DEAL_YN_N.equals(marketItemSaleGetDto.getStopDealYn())  //취급중지여부
                && CodeConstants.PFR_YN_Y.equals(marketItemSaleGetDto.getPfrYn())             //PFR 통과 여부
                && marketItemSaleGetDto.getSalePrice() > 0                                    //판매가 0원 이상
                && marketItemSaleGetDto.getStockQty() > 0 )                                   //재고 0개 이상
            {
                selStatCdType = SelStatCdType.SELLING;
            } else {
                selStatCdType= SelStatCdType.NO_SELLING;
            }
        }

        return selStatCdType;
    }

    /**
     * 상품 상태변경
     *
     * @param productId
     * @param statusType
     * @return Boolean
     */
    public Boolean changeProductSaleStatus(String productId, NaverStoreStatusType statusType) throws Exception {

        SaleStatusType saleStatusType = new SaleStatusType();
        saleStatusType.setProductId(Long.valueOf(productId));

        /**
         * 판매 상태
         * 관련 코드 : 상품 판매 상태 코드
         * SALE (판매 중), SUSP (판매 중지), OSTK (품절)만 입력할 수 있다 .
         * 기존 상품 상태가 SALE(판매 중) 상태인 경우에만 OSTK(품절) 상태로 변경할 수 있으며 , OSTK(품절) 상태로 변경하면 해당 상품의 재고 수량은 0으로 변경된다 .
         * 상품의 재고 수량이 0인 경우에는 경우에는 상품의 상태는 전달된 값과 상관없이 OSTK(품절) 상태를 유지함
         */
        saleStatusType.setStatusType(statusType.getCode());

        ChangeProductSaleStatusResponseType responseType;

        responseType = getExtProductClientStub().changeProductSaleStatus(saleStatusType);

        Boolean isOkchangeProductSaleStatus = getExtProductClientStub().isResponseOk(responseType);

        if (isOkchangeProductSaleStatus == Boolean.FALSE) {
            log.info("상품 판매 상태 [" + statusType + "] 수정 실패 market_item_no:"+productId + ", Code:"+ responseType.getError().getCode()
                + ", Message:"+ responseType.getError().getMessage()+ ", Detail:"+ responseType.getError().getDetail());

            String message = "상품 판매 상태 [" + statusType + "] 수정 실패 market_item_no:"+productId + ", Code:"+ responseType.getError().getCode()
                + ", Message:"+ responseType.getError().getMessage()+ ", Detail:"+ responseType.getError().getDetail();

            if (responseType.getError().getMessage().indexOf("자세한 정보는 Detail 엘리먼트를 참조하세요") > -1) {
                message = "상품 판매 상태 [" + statusType + "] 수정 실패 market_item_no:"+productId + ", Code:"+ responseType.getError().getCode()
                    + ", Detail:"+ responseType.getError().getDetail();
            }

            throw new IllegalArgumentException( message );
        }

        return isOkchangeProductSaleStatus;
    }

    /**
     * 옵션등록 (각점포정보 및 옵션정보를 셋팅)
     * @param marketItemGetDto
     * @return
     */
    private OptionType setItemOptionInfo(MarketItemGetDto marketItemGetDto) {
        CombinationOptionType combinationOptionType = new CombinationOptionType();

        CombinationOptionNamesType optionNames = new CombinationOptionNamesType();
        CombinationOptionItemListType optionItemList = new CombinationOptionItemListType();
        Long salePrice = marketItemGetDto.getBasic().getSimplePrice();

        Boolean checkCoupon = false;
        Boolean chechPromo = false;

        if (
                ("Y".equals(marketItemGetDto.getMasterCouponYn()) &&
                        (ObjectUtils.isNotEmpty(marketItemGetDto.getCoupon().getCouponNo()) || ObjectUtils.isNotEmpty(marketItemGetDto.getCoupon().getClubCouponNo()))
                )
        ) {
            checkCoupon = true;
        }

        if (
                ("Y".equals(marketItemGetDto.getMasterPromoYn()) &&
                        (ObjectUtils.isNotEmpty(marketItemGetDto.getPromo().getRpmPromoCompDetailId()) || ObjectUtils.isNotEmpty(marketItemGetDto.getPromo().getClubRpmPromoCompDetailId()))
                )
        ) {
            chechPromo = true;
        }

        boolean hasOption = false;

        Iterator<MarketItemSaleGetDto> iterator = marketItemGetDto.getStoreList().iterator();

        while(iterator.hasNext()) {

            MarketItemSaleGetDto store = iterator.next();

            if (
                    CodeConstants.PFR_YN_N.equals(store.getPfrYn())
                            || CodeConstants.USE_YN_N.equals(store.getShipPolicyYn())
                            || CodeConstants.STOP_DEAL_YN_Y.equals(store.getStopDealYn())
                            || CodeConstants.VIRTUAL_STORE_ONLY_YN_Y.equals(store.getVirtualStoreOnlyYn())
            ) {
                iterator.remove();
                continue;
            }

            if (
                    (checkCoupon == false || (checkCoupon == true && ObjectUtils.isNotEmpty(store.getCouponNo())))
                            && (chechPromo == false || (chechPromo == true && ObjectUtils.isNotEmpty(store.getRpmPromoCompDetailId())))) {
                //유효성 검사 통과
            } else {
                iterator.remove();
                continue;
            }

            optionNames.setName1("지점코드");

            Long optionPrice = this.getOptionPrice(store, salePrice);

            long saleCnt = store.getStockQty();

            if (store.getStockQty() < 1 || "Y".equals(store.getStopDealYn())) {
                saleCnt = 0;
            } else if (store.getStockQty() > ItemConstants.MAX_STOCK_QTY) {
                saleCnt = ItemConstants.MAX_STOCK_QTY;
            }

            if ( marketItemGetDto.getOpt().size() > 0 && "Y".equals(marketItemGetDto.getBasic().getOptSelUseYn()) ) {
                for ( MarketItemOptionGetDto option : marketItemGetDto.getOpt() ) {
                    String optionName = option.getOpt1Val().replace(",", " ");
                    if (optionName.length() > 25) {
                        optionName = optionName.substring(0, 22) + "...";
                    }

                    CombinationOptionItemType optionItem = new CombinationOptionItemType();
                    optionItem.setValue1(String.format("%04d", store.getStoreId()));
                    optionItem.setSellerManagerCode(String.format("%04d", store.getStoreId()));
                    optionItem.setValue2(optionName);
                    //optionItem.setPrice(isSale ? godGoodprice.getSaleAmt() - BASE_STORE_SALE_AMT : 0);
                    optionItem.setPrice(optionPrice);
                    optionItem.setQuantity(saleCnt);
                    optionItem.setUsable("Y");

                    optionItemList.addItem(optionItem);

                    hasOption = true;
                }
            }

            if (hasOption) {
                optionNames.setName2("상품옵션");
            } else {
                CombinationOptionItemType optionItem = new CombinationOptionItemType();
                optionItem.setValue1(String.format("%04d", store.getStoreId()));
                optionItem.setSellerManagerCode(String.format("%04d", store.getStoreId()));
                optionItem.setPrice(optionPrice);
                optionItem.setQuantity(Long.valueOf(saleCnt));
                optionItem.setUsable("Y");

                optionItemList.addItem(optionItem);
            }
        }

        combinationOptionType.setItemList(optionItemList);
        combinationOptionType.setNames(optionNames);

        if (optionItemList.getItem() == null || optionItemList.getItem().length == 0) {
            return null;
        }

//        if(isAllStoreOstk(optionItemList, godGoodinfo)) return null;	// 전점 품절일 경우 판매중지

        OptionType optionType = new OptionType();
        optionType.setProductId(Long.valueOf(marketItemGetDto.getBasic().getMarketItemNo()));
        optionType.setCombination(combinationOptionType);
        optionType.setSortType("ABC");

        return optionType;
    }

    /**
     * 행사 (rms, 상품쿠폰 > 상품할인)
     * @param marketItemGetDto
     * @param productType
     */
    private void setPromo(MarketItemGetDto marketItemGetDto, ProductType productType) {
        //행사 유효성 체크 및 기본정보 셋팅
        itemCommonService.validPromo(marketItemGetDto);

        //즉시할인 행사
        if( EventConstants.CODE_Y.equals(marketItemGetDto.getMasterCouponYn())) {
            // 즉시할인 = 균일가
            marketItemGetDto.getBasic().setMarketIconType("15");
        }

        //쿠폰
        if(EventConstants.CODE_Y.equals(marketItemGetDto.getMasterCouponYn())) {
            productType.setSellerCustomCode1(marketItemGetDto.getCustomCd());
        }
        //rms 행사 validation
        if(EventConstants.CODE_Y.equals(marketItemGetDto.getMasterPromoYn())) {
            Calendar startCal = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat( PatternConstants.DATE_DF_FORMAT );
            String curDt = df.format(startCal.getTime());

            if (DateUtil.isDateDFBefore(curDt, marketItemGetDto.getPromo().getStartDate())) {
                marketItemGetDto.getPromo().setStartDate(curDt);
            }

            productType.setSellerCustomCode1(marketItemGetDto.getCustomCd());

            switch (marketItemGetDto.getPromo().getEventKind()) {
                /**
                 * 특이사항 : 11번가는 아이콘 정보를 지점 정보에 포함되므로 임시로 basic 애 저장한다.
                 */
                case EventConstants.EVENT_KIND_BASIC :
                    //심플할인 = 행사항품
                    marketItemGetDto.getBasic().setMarketIconType("14");
                    break;

                case EventConstants.EVENT_KIND_PICK :
                    marketItemGetDto.getBasic()
                            .setMarketIconType(
                                    PickIconType.valueFor(String.valueOf(marketItemGetDto.getPromo().getBuyItemValue())).getElevenCd());

                    BranchBenefitDiscountType branchBenefitTypePick = new BranchBenefitDiscountType();
                    branchBenefitTypePick.setBranchBenefitType("PLUS_1");

                    BranchBenefitDiscountListType branchBenefitDiscountListTypePick = new BranchBenefitDiscountListType();
                    BranchBenefitDiscountContentType branchBenefitDiscountContentTypePick = new BranchBenefitDiscountContentType();
                    branchBenefitDiscountContentTypePick.setAmount("100%");
                    branchBenefitDiscountContentTypePick.setOrderAmount((marketItemGetDto.getPromo().getBuyItemValue() - 1) + "개");
                    branchBenefitDiscountListTypePick.addBranchBenefitDiscountContent(branchBenefitDiscountContentTypePick);
                    branchBenefitTypePick.setBranchBenefitDiscountList(branchBenefitDiscountListTypePick);

                    // 프로모션 시작일은 현재일로 변경 : goodinfo.getEbayPromotion().getPromoStartDate() -> startCal.getTime()
                    branchBenefitTypePick.setStartDate(marketItemGetDto.getPromo().getStartDate());
                    branchBenefitTypePick.setEndDate(marketItemGetDto.getPromo().getEndDate());

                    productType.setBranchBenefitDiscount(branchBenefitTypePick);

                    if (marketItemGetDto.getBasic().getPurchaseLimitQty() > 1) {
                        // 1회 최대 구매수량 설정 - 계산식 : 제휴사 최대구매수량 = {홈플러스최대구매수량 / (N+1) } X N
                        Long maxPurchaseQuantityPerOrder = (long) Math.floor((marketItemGetDto.getBasic().getPurchaseLimitQty() / marketItemGetDto.getPromo().getBuyItemValue()) * (marketItemGetDto.getPromo().getBuyItemValue() - 1));
                        productType.setMaxPurchaseQuantityPerOrder(maxPurchaseQuantityPerOrder);
                    }

                    break;
                /**
                 * 오픈시 미지원 내부방침 10/21 함께할인
                 */
//                case EventConstants.EVENT_KIND_TOGETHER :
//                    /**
//                     *  할인 수량은 2개 이상 등록 가능
//                     *  복수구매의 할인  금액은 판매가의 50%이상 설정하실 수 없습니다
//                     *  날짜 포맷 YYYYMMDD
//                     */
//
//                    MultiPurchaseDiscountType multiPurchaseDiscountType = new MultiPurchaseDiscountType();
//
//                    //복수 구매 할인액(1000) or 할인율(10%)
//                    if (ChangeType.RATE.getType().equals(marketItemGetDto.getPromo().getChangeType())){
//                        multiPurchaseDiscountType.setAmount((marketItemGetDto.getPromo().getChangePercent()) + "%");
//                    } else if (ChangeType.AMOUNT.getType().equals(marketItemGetDto.getPromo().getChangeType())) {
//                        multiPurchaseDiscountType.setAmount(String.valueOf(marketItemGetDto.getPromo().getChangeAmount()));
//                    }
//
//                    Integer orderAmount = marketItemGetDto.getPromo().getBuyItemValue() < 2 ? 2 : marketItemGetDto.getPromo().getBuyItemValue();
//                    multiPurchaseDiscountType.setOrderAmount((orderAmount) + "개");
//
//                    //복수 구매 시작일
//                    multiPurchaseDiscountType.setStartDate(marketItemGetDto.getPromo().getStartDate());
//                    //복수 구매 종료일
//                    multiPurchaseDiscountType.setEndDate(marketItemGetDto.getPromo().getEndDate());
//
//                    marketItemGetDto.getBasic().setMarketIconType("16");
//                    productType.setMultiPurchaseDiscount(multiPurchaseDiscountType);
//                    break;
            }
        }
    }

    /**
     * 상품 기본정보 생성 ( N마트 )
     * @param marketItemGetDto
     * @return ProductType
     */
    private ProductType setItemBasicInfo(MarketItemGetDto marketItemGetDto) throws Exception {

        ProductType productType = new ProductType();
        ItemMarketImgSendGetDto itemMarketImgSendGetDto;

        if(StringUtils.isNotEmpty(marketItemGetDto.getBasic().getMarketItemNo())) {
            productType.setProductId(Long.parseLong(marketItemGetDto.getBasic().getMarketItemNo()));
        }

        marketItemGetDto.getBasic().setItemNm(marketItemGetDto.getBasic().getItemNm().replaceAll("[*]", "x").replaceAll("[?\\\\<\\\\>\\\\\\\\\\\\\"]", ""));

        try {
            //1.이미지처리
            itemMarketImgSendGetDto = itemImageUpload(marketItemGetDto);
        } catch (AxisFault e) {
            log.error("setItemBasicInfo >> itemImageUpload Error :: {}", e.getStackTrace());
            return null;
        }
        // TODO: 8/19/21 null일 경우 처리 남음 재처리할지 버릴지
        /*
         * 판매 상태
         * 관련 코드 : 상품 판매 상태 코드
         * 상품 등록 시에는 SALE (판매 중)만 입력할 수 있으며 , 다른 값이 전달되더라도 전달되더라도 SALE (판 매 중)로 저장된다 .
         * 상품 수정 시에는 SALE (판매 중), ), SUSP (판매 중지 )만 입력할 수 있다 .
         * StockQuantity의 값이 0인 경우 상품 상태 는 OSTK (품절 )로 저장된다 .
         */
        String statusType = NaverStoreStatusType.SUSP.getCode();
        if ("Y".equals(marketItemGetDto.getMasterDispYn())) {
            statusType = NaverStoreStatusType.SALE.getCode();
        }

        productType.setStatusType(statusType);
        // 상품 판매 유형 NEW: 새상품, OLD: 중고상품, REFUR: 리퍼상품, DSP: 진열상품
        productType.setSaleType("NEW");
        // 주문 제작 상품 여부 Y/N
        productType.setCustomMade("N");
        // Leaf 카테고리 ID
        productType.setCategoryId(marketItemGetDto.getBasic().getMarketCateCd());
        // 상품 상세 레이아웃 타입코드 BASIC : 베이직형, IMAGE: 이미지 집중형
        productType.setLayoutType("BASIC");

        // 상품명
        productType.setName(marketItemGetDto.getBasic().getItemNm());

        //판매자 상품코드
        productType.setSellerManagementCode(marketItemGetDto.getBasic().getItemNo());

        /*
         * 모델정보 (제조사명)
         * 제조사명 항목은 최대 50자 이하로 입력해 주세요.
         */
        ModelType modeltype = new ModelType();

        modeltype.setManufacturerName(marketItemGetDto.getBasic().getMakerNm());
        modeltype.setBrandName(marketItemGetDto.getBasic().getBrandNm());
        productType.setModel(modeltype);

        // GetOriginAreaList
        OriginAreaType originAreaType = new OriginAreaType();
        originAreaType.setCode("03");
        originAreaType.setContent("");
        productType.setOriginArea(originAreaType);

        //부가세 TAX: 과세 상품, DUTYFREE: 면세상품, SMALL: 영세 상품
        productType.setTaxType(
            marketItemGetDto.getBasic().getTaxYn() != null && marketItemGetDto.getBasic().getTaxYn().equals("Y") ? "TAX" : "DUTYFREE");
        //미성년자 구매 가능 여부 Y/N
        productType.setMinorPurchasable(!"NORMAL".equals(marketItemGetDto.getBasic().getAdultType()) ? "N" : "Y");

        //상품 이미지 정보
        ImageType imageType = new ImageType();
        imageType.setRepresentative(new URLType());
        imageType.getRepresentative().setURL(itemMarketImgSendGetDto.getImgUrl1());
        imageType.setOptionalList(new OptionalListType());

        ArrayList<URLType> urls = new ArrayList<>();
        if (!StringUtils.isEmpty(itemMarketImgSendGetDto.getImgUrl2())) {
            URLType type = new URLType();
            type.setURL(itemMarketImgSendGetDto.getImgUrl2());
            urls.add(type);
        }
        if (!StringUtils.isEmpty(itemMarketImgSendGetDto.getImgUrl3())) {
            URLType type = new URLType();
            type.setURL(itemMarketImgSendGetDto.getImgUrl3());
            urls.add(type);
        }
        if (!StringUtils.isEmpty(itemMarketImgSendGetDto.getImgUrl4())) {
            URLType type = new URLType();
            type.setURL(itemMarketImgSendGetDto.getImgUrl4());
            urls.add(type);
        }
        if (!StringUtils.isEmpty(itemMarketImgSendGetDto.getImgUrl5())) {
            URLType type = new URLType();
            type.setURL(itemMarketImgSendGetDto.getImgUrl5());
            urls.add(type);
        }
        if (!StringUtils.isEmpty(itemMarketImgSendGetDto.getImgUrl6())) {
            URLType type = new URLType();
            type.setURL(itemMarketImgSendGetDto.getImgUrl6());
            urls.add(type);
        }
        imageType.getOptionalList().setOptional(urls.toArray(new URLType[urls.size()]));

        //이미지 정보
        productType.setImage(imageType);
        //상품 상세 정보
        productType.setDetailContent(itemCommonService.getHtmlDetail(marketItemGetDto) + " ");

        //A/S 전화번호
        String sfterServiceTelephoneNumber = "1577-3355";
        productType.setAfterServiceTelephoneNumber(sfterServiceTelephoneNumber);

        //A/S 안내
        // 상품등록 전송실패 : A/S 안내 항목에 등록불가인 특수문자가 포함되어 있습니다. \ * ? " < >
        String afterServiceGuideContent = "상품상세에 표시";
        productType.setAfterServiceGuideContent(afterServiceGuideContent);
        //구매평 노출여부 Y/N
        //productType.setPurchaseReviewExposure(goodinfo.getPurchase_review_exposure());
        //지식쇼핑 등록여부(가격비교)
        productType.setKnowledgeShoppingProductRegistration(marketItemGetDto.getBasic().getEpYn());

        Calendar startCal = Calendar.getInstance();

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH");
        //판매시작일
        productType.setSaleStartDate(format1.format(startCal.getTime()) + ":00");
        //판매종료일 : 판매시작일 + 365
        Calendar endCal = Calendar.getInstance();
        endCal.add(Calendar.DATE, 365);
        productType.setSaleEndDate(format1.format(endCal.getTime()) + ":59");

        //판매가격
        productType.setSalePrice(marketItemGetDto.getBasic().getSimplePrice());

        //단위수량
        productType.setDoubleMinimumSaleUnitQuantity(marketItemGetDto.getBasic().getSaleUnit());

        //재고수량 : 기본 999
        productType.setStockQuantity(999);

        // 최소 구매수량이 null 이 아닐경우 세팅
        // 최소 구매 수량 :: 네이버의 경우 최소구매수량을 2개 이상으로 제어하는 경우에만 등록한다.
        if ( marketItemGetDto.getBasic().getPurchaseMinQty() > 1) {
            productType.setMinPurchaseQuantity( marketItemGetDto.getBasic().getPurchaseMinQty() );
        }


        // 1회 최대 구매수량
        if (marketItemGetDto.getBasic().getPurchaseLimitQty() > 0) {
            productType.setMaxPurchaseQuantityPerOrder(marketItemGetDto.getBasic().getPurchaseLimitQty());
        }

        // 상품 배송 정보 세팅
        productType.setDelivery(getDeliveryType(bundleGroupId));

        //행사
        this.setPromo(marketItemGetDto, productType);

        ProductSummaryType productSummaryType = new ProductSummaryType();
        EtcSummaryType etcSummaryType = new EtcSummaryType();
        etcSummaryType.setNoRefundReason("상품상세에 표시");
        etcSummaryType.setQualityAssuranceStandard("상품상세에 표시");
        etcSummaryType.setCompensationProcedure("상품상세에 표시");
        etcSummaryType.setTroubleShootingContents("상품상세에 표시");
        etcSummaryType.setItemName("상품상세에 표시");
        etcSummaryType.setModelName("상품상세에 표시");
        etcSummaryType.setManufacturer("상품상세에 표시");
        etcSummaryType.setAfterServiceDirector("홈플러스 온라인마트 고객센터");
        etcSummaryType.setReturnCostReason("상품상세에 표시");
        etcSummaryType.setCertificateDetails("상품상세에 표시");
        productSummaryType.setEtc(etcSummaryType);

        productType.setProductSummary(productSummaryType);

        //인증정보 셋팅
        this.setCertificationListType(marketItemGetDto, productType);

        // 단위 용량, 표시단위 한글로 변경
        if ("Y".equals(marketItemGetDto.getBasic().getUnitDispYn())) {
            UnitCapacityType unitCapacityType = new UnitCapacityType();
            // 총 용량
            unitCapacityType.setTotalCapacity(marketItemGetDto.getBasic().getTotalUnitQty());
            // 단위 용량
            unitCapacityType.setUnitCapacity(marketItemGetDto.getBasic().getUnitQty());
            // 표시 단위
            unitCapacityType.setIndicationUnit(marketItemGetDto.getBasic().getUnitMeasure());
            productType.setUnitCapacity(unitCapacityType);
        }

        return productType;
    }

    public void setCertificationListType(MarketItemGetDto marketItemGetDto, ProductType productType) throws Exception {
        GetCategoryInfoResponseType getCategoryInfoResponseType = null;

        // CHI 어린이제품 인증 대상
        productType.setChildCertifiedProductExclusion("Y");
        // KC KC 인증 대상
        productType.setKCCertifiedProductExclusion("Y");

        String categoryId = null;
        try {
            categoryId = marketItemGetDto.getBasic().getMarketCateCd();
            getCategoryInfoResponseType = getExtProductClientStub().getCategoryInfo(categoryId);
        } catch (RemoteException e) {
            log.info("인증유형 정보 셋팅 카테고리 정보 조회 API 호출 : Exception >> itemNo : " + marketItemGetDto.getBasic().getItemNo() + ", categoryId : " + categoryId);
            e.printStackTrace();

            throw e;
        }

        // GetCategoryInfo를 통해
        // (1)상품 등록 시 반드시 입력해야하는 인증 종류(ExceptionalCategoryList)과
        // (2)해당 상품 등록 시 등록 가능한 인증 유형(CertificationInfoList)을 조회
        // 전송 결과 확인
        Boolean isOkGetReturnsCategoryInfo = getExtProductClientStub().isResponseOk(getCategoryInfoResponseType);

        if(isOkGetReturnsCategoryInfo == Boolean.TRUE) {

            CategoryReturnType categoryReturnType = getCategoryInfoResponseType.getCategory();

            if (categoryReturnType != null){
                // 반드시 입력해야하는 인증 종류
                // 인증정보 확인
                if(categoryReturnType.getExceptionalCategoryList() != null) {
                    StringCodeType[] stringCodeTypes = categoryReturnType.getExceptionalCategoryList().getExceptionalCategory();

                    ObjectMapper om = new ObjectMapper();
                    log.info("stringCodeTypes: {}", om.writeValueAsString(stringCodeTypes));

                    if(stringCodeTypes != null) {
                        CertificationListType certificationListType = null;

                        for (StringCodeType stringCodeType : stringCodeTypes) {

                            // 등록 가능한 인증 유형
                            if(categoryReturnType.getCertificationInfoList() != null) {
                                CertificationInfoType[] certificationInfoTypes = categoryReturnType.getCertificationInfoList().getCertificationInfo();
                                if(certificationInfoTypes != null) {

                                    // 반드시 입력해야하는 인증 코드가 있는 경우 등록 가능한 인증 유형중 해당 하는 값을 "인증 정보 목록"에 등록한다.
                                    loops: for (CertificationInfoType certificationInfoType : certificationInfoTypes) {
                                        if (certificationInfoType.getKindTypeList() != null) {
                                            StringCodeType[] kindTypes = certificationInfoType.getKindTypeList().getKindType();
                                            if(kindTypes != null) {
                                                if ("BOK".equals(stringCodeType.getCode())) {
                                                    productType.setIsbn13(marketItemGetDto.getBasic().getIsbn());
                                                    productType.setIndependentPublicationYn("N");
                                                }

                                                for (StringCodeType kindType : kindTypes) {

                                                    // 인증번호 항목은 영어, 숫자, 특수문자('-', '_')만 허용합니다.
                                                    // "인증 정보"에 추가로 들어갈 "인증기관이름", "인증번호"를 셋팅한다.
                                                    if (stringCodeType.getCode().equals(kindType.getCode())) {
                                                        CertificationType certificationType = new CertificationType();
                                                        certificationType.setId(certificationInfoType.getCode());
                                                        certificationType.setKindType(stringCodeType.getCode());

                                                        for (MarketItemCertGetDto dto : marketItemGetDto.getCert()) {
                                                            if ("CHI".equals(stringCodeType.getCode()) && "KD".equals(dto.getCertGroup())) {
                                                                certificationType.setName(dto.getCertGroupNm());
                                                                certificationType.setNumber(dto.getCertNo().replaceAll("[^0-9\\-_A-Za-z]", "") );

                                                            } else if ("KC".equals(stringCodeType.getCode()) && "IT".equals(dto.getCertGroup())) {
                                                                certificationType.setName(dto.getCertGroupNm());
                                                                certificationType.setNumber(dto.getCertNo().replaceAll("[^0-9\\-_A-Za-z]", "") );
                                                            }
                                                        }

                                                        if (certificationListType == null) {
                                                            certificationListType = new CertificationListType();
                                                        }

                                                        certificationListType.addCertification(certificationType);

                                                        break loops;
                                                    }
                                                }
                                            }

                                        }

                                    }
                                }
                            }
                        }

                        if (certificationListType != null) {
                            CertificationListType certificationListTypeTemp = null;
                            for (CertificationType certificationType : certificationListType.getCertification()) {
                                if ("CHI".equals(certificationType.getKindType())) {
                                    productType.getModel().setModelName(marketItemGetDto.getBasic().getItemNm());
                                    productType.setChildCertifiedProductExclusion("N");
                                } else if ("KC".equals(certificationType.getKindType())) {
                                    productType.setKCCertifiedProductExclusion("N");
                                } else if ("GRN".equals(certificationType.getKindType())) {
                                    // 친환경 인증 대상은 인증 제외
                                    productType.setGreenCertifiedProductExclusion("Y");
                                }

                                // 친환경 인증 대상인 경우엔 "인증 유형 정보"를 넣지 않는다.
                                if (!"GRN".equals(certificationType.getKindType())) {
                                    if (certificationListTypeTemp == null) {
                                        certificationListTypeTemp = new CertificationListType();
                                    }
                                    certificationListTypeTemp.addCertification(certificationType);
                                }
                            }

                            if (certificationListTypeTemp != null) {
                                productType.setCertificationList(certificationListTypeTemp);
                            }
                        }
                    }
                } else {
                    log.info("인증유형 정보 셋팅 카테고리 정보 조회 API 호출 : 인증이 필요없는 상품 카테고리 코드 >> itemNo : " + marketItemGetDto.getBasic().getItemNo() + ", categoryId : " + categoryId);
                }
            }

        } else {
            log.info("인증유형 정보 셋팅 카테고리 정보 조회 API 호출 : isResponseOk = False >> itemNo : " + marketItemGetDto.getBasic().getItemNo() + ", categoryId : " + categoryId);
        }

    }

    /**
     * 배송비 및 배송 정책 설정
     *
     * @param bundleGroupId
     * @return DeliveryType
     */
    private DeliveryType getDeliveryType(String bundleGroupId) {
        // 배송 정보
        DeliveryType deliveryType = new DeliveryType();
        // 배송방법 1:택배,소포,등기 2:직접배송(화물배달)
        deliveryType.setType("1");
        // 묶음배송여부 Y/N
        deliveryType.setBundleGroupAvailable("Y");
        // 묶음배송그룹코드 225429 / 운영 : 41464360
        deliveryType.setBundleGroupId(Long.valueOf(bundleGroupId));
        /*
         * 방문 수령 주소 코드 제거
         */
        // 홈플러스용 배송주소코드 100018196
        //	deliveryType.setVisitAddressId(Long.valueOf("100018196"));
        // 배송비유형 1: 무료, 2: 조건부 무료, 3: 유료, 4: 수량별 부과 - 반복 구간, 5: 수량별 부과 - 구각 직접 설정
        deliveryType.setFeeType("2");
        // 기본배송비 3000원 고정
        deliveryType.setBaseFee(3000);
        // 무료조건금액 40000원 고정
        deliveryType.setFreeConditionalAmount(40000);
        // 배송비 결제 방법 1: 무료, 2: 조건부 무료, 3: 유료, 4: 수량별 부과 - 반복 구간, 5: 수량별 부과 - 구각 직접 설정,
        deliveryType.setPayType("2");
        // 반품/교환 택배사 코드 (0~9)
        deliveryType.setReturnDeliveryCompanyPriority("0");
        // 반품 배송비 4000원 고정
        deliveryType.setReturnFee(4000);
        // 교환 배송비 0원 고정
        deliveryType.setExchangeFee(0);
        // 배송 속성 타입 코드 (장보기 배송 유형) : TODAY_ARRIVAL 당일배송 장보기 관련 기능
        deliveryType.setAttributeType("TODAY_ARRIVAL");

        return deliveryType;
    }

    /**
     * 이미지 업로드
     *
     * @param marketItemGetDto
     * @return ItemMarketImgSendGetDto
     * @throws AxisFault
     */
    private ItemMarketImgSendGetDto itemImageUpload(MarketItemGetDto marketItemGetDto) throws AxisFault {

        // 기존 전송된 이미지 유무와 전송 상태 체크
        ItemMarketImgSendGetDto itemMarketImgSendGetDto = itemSlaveMapper.selectItemMarketImageSend(
            marketItemGetDto.getBasic().getItemNo(), CodeConstants.NMART_PARTNER_ID);
        if (ObjectUtils.isNotEmpty(itemMarketImgSendGetDto) && "Y".equals(itemMarketImgSendGetDto.getSendStatus())) {
            // 변경 이력이 없으면 기존 업로드 URL 전달한다.
            return itemMarketImgSendGetDto;
        } else {
            itemMarketImgSendGetDto = new ItemMarketImgSendGetDto();
        }

        // 이미지 업로드
        UploadImageResponseType response;
        try {
            response = getExtImageClientStub().uploadImage(getUploadUrls(marketItemGetDto));
            log.debug("productImageUpload [ITEM_NO] :: " + marketItemGetDto.getBasic().getItemNo());
        } catch (Exception e) {
            log.info("productImageUpload Error. Retry By [ITEM_NO : {}, Error : {}]", marketItemGetDto
                .getBasic().getItemNo(), e.getMessage());
            try { Thread.sleep(1000); } catch(InterruptedException ie){}
            try {
                response = getExtImageClientStub().uploadImage(getUploadUrls(marketItemGetDto));
                log.debug("productImageUpload Retry Success. ITEM_NO :: " + marketItemGetDto.getBasic().getItemNo());
            } catch (Exception e2) {
                log.info("productImageUpload Error. Retry By [ITEM_NO : {}, Error : {}]", marketItemGetDto
                    .getBasic().getItemNo(), e.getMessage());
                return itemMarketImgSendGetDto;
            }
        }

        ItemMarketImgSendSetDto itemMarketImgSendSetDto = new ItemMarketImgSendSetDto();

        itemMarketImgSendSetDto.setItemNo(marketItemGetDto.getBasic().getItemNo());
        itemMarketImgSendSetDto.setPartnerId(CodeConstants.NMART_PARTNER_ID);
        itemMarketImgSendSetDto.setRegId(CodeConstants.SYSTEM_USER_ID);
        itemMarketImgSendSetDto.setSendStatus("Y");

        //전송 결과 확인
        Boolean isOkUpload = getExtImageClientStub().isResponseOk(response);
        if(isOkUpload == Boolean.TRUE) {
            ImageReturnListType ImageReturnListType = response.getImageList();
            if (ImageReturnListType != null){
                ImageReturnType[] imageReturnType = ImageReturnListType.getImage();
                if (imageReturnType != null && imageReturnType.length > 0) {

                    for (int i = 0; i < imageReturnType.length; i++) {
                        switch (i) {
                            case 0 : itemMarketImgSendSetDto.setImgUrl1(imageReturnType[i].getURL());
                                break;
                            case 1 : itemMarketImgSendSetDto.setImgUrl2(imageReturnType[i].getURL());
                                break;
                            case 2 : itemMarketImgSendSetDto.setImgUrl3(imageReturnType[i].getURL());
                                break;
                            case 3 : itemMarketImgSendSetDto.setImgUrl4(imageReturnType[i].getURL());
                                break;
                            case 4 : itemMarketImgSendSetDto.setImgUrl5(imageReturnType[i].getURL());
                                break;
                            case 5 : itemMarketImgSendSetDto.setImgUrl6(imageReturnType[i].getURL());
                                break;
                        }
                    }

                    itemMasterMapper.upsertItemMarketImageSend(itemMarketImgSendSetDto);
                }
            }

            itemMarketImgSendGetDto.setImgUrl1(itemMarketImgSendSetDto.getImgUrl1());
            itemMarketImgSendGetDto.setImgUrl2(itemMarketImgSendSetDto.getImgUrl2());
            itemMarketImgSendGetDto.setImgUrl3(itemMarketImgSendSetDto.getImgUrl3());
            itemMarketImgSendGetDto.setImgUrl4(itemMarketImgSendSetDto.getImgUrl4());
            itemMarketImgSendGetDto.setImgUrl5(itemMarketImgSendSetDto.getImgUrl5());
            itemMarketImgSendGetDto.setImgUrl6(itemMarketImgSendSetDto.getImgUrl6());
        } else {
            log.info("이미지 업로드 전송 결과 실패 :: [ ITEM_NO : {}, Error : {} ]", marketItemGetDto.getBasic().getItemNo(), response.getError().getMessage());
            itemMarketImgSendSetDto.setSendStatus("N");
            itemMasterMapper.upsertItemMarketImageSend(itemMarketImgSendSetDto);

            return null;
        }

        return itemMarketImgSendGetDto;
    }

    /**
     * 옵션 내용 모두 제거(ManageOption내 옵션 내용을 빈 값으로 전달)
     * @param itemNo
     * @param coopProductId
     */
    private void clearOption(String itemNo, String coopProductId) {
        OptionType optionType = new OptionType();
        optionType.setProductId(Long.valueOf(coopProductId));
        optionType.setCombination(null);
        optionType.setSortType("ABC");

        if (optionType != null) {
            ManageOptionResponseType manageOptionResponseType = null;

            //SmartStore 상품 옵션 제거
            try{
                manageOptionResponseType = getExtProductClientStub().manageOption(optionType);
                marketItemLogService.setItemSpecHist(ItemSpecHistParamDto.builder()
                    .itemNo(itemNo)
                    .request(optionType)
                    .partnerId(CodeConstants.NMART_PARTNER_ID)
                    .response(manageOptionResponseType)
                    .methodTxt("manageOption").build());

            }catch (Exception e){
                log.info("옵션정보 제거 에러 ITEM_NO:"+itemNo+" "+e.getMessage());
            }

            try {
                Boolean isOkManageOption = getExtProductClientStub().isResponseOk(manageOptionResponseType);

                if (isOkManageOption == Boolean.FALSE){
                    log.info("옵션정보 제거 에러 실패 ITEM_NO:"+itemNo + ", Code:"+ manageOptionResponseType.getError().getCode()
                            + ", Message:"+ manageOptionResponseType.getError().getMessage()+ ", Detail:"+ manageOptionResponseType.getError().getDetail());
                } else {
                    log.info("상품 옵션정보 제거 성공 ITEM_NO:"+itemNo);
                }
            } catch (Exception e) {
                log.info("옵션제거 에러 ITEM_NO:" + itemNo + " "+e.getMessage());
            }
        }
    }

    /**
     * 점포별 옵션가 조회
     * @param store
     * @return
     */
    private Long getOptionPrice(MarketItemSaleGetDto store, Long simplePrice) {
        Long optionPrice = Long.valueOf(0);

        if (simplePrice != store.getCouponPrice()) {
            optionPrice = store.getCouponPrice() - simplePrice;
        } else if (simplePrice != store.getSimplePrice()) {
            optionPrice = store.getSimplePrice() - simplePrice;
        } else if (simplePrice != store.getSimplePrice()) {
            optionPrice = store.getSimplePrice() - simplePrice;
        }

        return optionPrice;
    }

    /**
     * upload url convert
     * - MarketItemGetDto >> ItemImg to Array
     *
     * @param marketItemGetDto
     * @return
     */
    private String[] getUploadUrls(MarketItemGetDto marketItemGetDto) {
//        marketItemGetDto.getImg().get(1).setImgUrl("/td/94100a40-7f80-43bb-8b47-2b311bbd0a2e");
        ArrayList<String> imageUrls = new ArrayList<>();
        /*
         * 상품 기본 정보의 상품 이미지 리스트는 Priority 기준으로 정렬 되어 있다.
         * 1 메인 이미지
         * 2 ~ 6 기본 이미지
         * 7 리스팅 이미지
         * 8 라벨 이미지
         */
        marketItemGetDto.getImg()
            .stream()
            .forEach(img -> {
                    if(img.getImgType().equalsIgnoreCase("MN")) {
                        imageUrls.add(imgFrontUrl + img.getImgUrl());
                    }
                });

        return imageUrls.toArray(new String[imageUrls.size()]);
    }


}