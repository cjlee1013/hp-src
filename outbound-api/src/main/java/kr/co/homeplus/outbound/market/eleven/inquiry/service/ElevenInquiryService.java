package kr.co.homeplus.outbound.market.eleven.inquiry.service;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.outbound.core.constants.UriConstants;
import kr.co.homeplus.outbound.market.eleven.config.MarketElevenConfig;
import kr.co.homeplus.outbound.market.eleven.inquiry.convert.ElevenInquiryConvert;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryAlimiAnswerResultDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryAlimiAnswerSetDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryAlimiResultDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryAlimiSetDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryAnswerContentDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryAnswerResultDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryAnswerSetDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryGetDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryRestResult;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquiryResultDto;
import kr.co.homeplus.outbound.market.eleven.inquiry.model.ElevenInquirySetDto;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
@RequiredArgsConstructor
public class ElevenInquiryService {

    private final MarketElevenConfig marketElevenConfig;
    private final RestTemplate restTemplate;

    @Value("${plus.resource-routes.eleven.url}")
    public String elevenStUrl;

    public List<ElevenInquiryGetDto> getInquiryList(ElevenInquirySetDto elevenInquirySetDto) throws Exception {
        elevenInquirySetDto.setAnswerStatus("00");
        //elevenInquirySetDto.setCorpCdType("57020");

        List<ElevenInquiryResultDto> inquiryList = this.execute(
            HttpMethod.GET
            ,elevenStUrl + UriConstants.ELEVEN_GET_INQUIRY_LIST
            ,null
            , new ParameterizedTypeReference<List<ElevenInquiryResultDto>>(){}
            ,(Map) new BeanMap(elevenInquirySetDto));

        //log.info("ElevenInquiryRestResult : {}" , inquiryList);

        List<ElevenInquiryGetDto> convertInquiryList = new ArrayList<>();

        if(!ObjectUtils.isEmpty(inquiryList) && inquiryList.size() > 0) {
            convertInquiryList = ElevenInquiryConvert.convertInquiryList(inquiryList);
        }

        log.info("convertInquiryList : {}" , convertInquiryList);

        return convertInquiryList;
    }

    public ResponseObject<ElevenInquiryAnswerResultDto> setInquiryAnswer(ElevenInquiryAnswerSetDto elevenInquiryAnswerSetDto) {
        ElevenInquiryAnswerResultDto elevenInquiryAnswerResultDto = new ElevenInquiryAnswerResultDto();
        ElevenInquiryAnswerContentDto contentDto = new ElevenInquiryAnswerContentDto();

        contentDto.setAnswerCont(elevenInquiryAnswerSetDto.getContent());

        try{
            elevenInquiryAnswerResultDto = this.execute(
                HttpMethod.PUT
                ,elevenStUrl + UriConstants.ELEVEN_SET_INQUIRY_ANSWER
                , contentDto
                , new ParameterizedTypeReference<ElevenInquiryAnswerResultDto>(){}
                ,(Map) new BeanMap(elevenInquiryAnswerSetDto));
        } catch (Exception e) {
            log.error("[11번가] setInquiryAnswer() 에러발생 : " + elevenInquiryAnswerSetDto.getBrdInfoNo());
            return ResourceConverter.toResponseObject(elevenInquiryAnswerResultDto, HttpStatus.OK, "FAIL", "문의 답변 에러");
        }

        return ResourceConverter.toResponseObject(elevenInquiryAnswerResultDto);
    }

    public List<ElevenInquiryGetDto> getInquiryAlimiList(ElevenInquiryAlimiSetDto elevenInquiryAlimiSetDto) throws Exception {
        //elevenInquiryAlimiSetDto.setCorpCdType("57010");

        List<ElevenInquiryAlimiResultDto> inquiryList = this.execute(
            HttpMethod.GET
            ,elevenStUrl + UriConstants.ELEVEN_GET_INQUIRY_ALIMI_LIST
            ,null
            , new ParameterizedTypeReference<List<ElevenInquiryAlimiResultDto>>(){}
            ,(Map) new BeanMap(elevenInquiryAlimiSetDto));

        //log.info("ElevenInquiryRestResult : {}" , inquiryList);

        List<ElevenInquiryGetDto> convertInquiryList = new ArrayList<>();

        if(!ObjectUtils.isEmpty(inquiryList) && inquiryList.size() > 0) {
            convertInquiryList = ElevenInquiryConvert.convertAlimiList(inquiryList);
        }

        log.info("convertInquiryAlimiList : {}" , convertInquiryList);

        return convertInquiryList;
    }

    public ResponseObject<ElevenInquiryAlimiAnswerResultDto> setInquiryAlimiAnswer(ElevenInquiryAlimiAnswerSetDto elevenInquiryAlimiAnswerSetDto) {
        ElevenInquiryAlimiAnswerResultDto elevenInquiryAlimiAnswerResultDto = new ElevenInquiryAlimiAnswerResultDto();

        try{
            elevenInquiryAlimiAnswerResultDto = this.execute(
                HttpMethod.GET
                ,elevenStUrl + UriConstants.ELEVEN_SET_INQUIRY_ALIMI_ANSWER
                , null
                , new ParameterizedTypeReference<ElevenInquiryAlimiAnswerResultDto>(){}
                ,(Map) new BeanMap(elevenInquiryAlimiAnswerSetDto));
        } catch (Exception e) {
            log.error("[11번가] setInquiryAlimiAnswer() 에러발생 : " + elevenInquiryAlimiAnswerSetDto.getEmerNtceSeq());
            return ResourceConverter.toResponseObject(elevenInquiryAlimiAnswerResultDto, HttpStatus.OK, "FAIL", "긴급알리미 답변 에러");
        }

        return ResourceConverter.toResponseObject(elevenInquiryAlimiAnswerResultDto);
    }

    public <T> T execute(HttpMethod httpMethod, String url, Object request, ParameterizedTypeReference<T> response, Map< String, String > params) {
        if (StringUtils.isEmpty(url)) {
            throw new IllegalArgumentException( "url 이(가) 필요 합니다." );
        }

        if (ObjectUtils.isEmpty(httpMethod)) {
            throw new IllegalArgumentException( "httpMethod 이(가) 필요 합니다." );
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.setContentType( new MediaType( "text", "xml", Charset.forName( "euc-kr" ) ) );
        httpHeaders.setAccept( Collections.singletonList( MediaType.APPLICATION_XML ) );
        httpHeaders.set("openapikey", marketElevenConfig.getApiKey());

        log.debug( "\n - Request Object- \n" + request );
        log.debug( "\n - Request Param- \n" + params );
        log.debug( "\n - Request url- \n" + url );

        ResponseEntity<T> result = null;

        try {
            HttpEntity<Object> req = new HttpEntity<Object>(request, httpHeaders);
            result = restTemplate.exchange(url,
                httpMethod, req,
                response, params);

        }catch (HttpStatusCodeException e)  {
            log.error(e.getResponseBodyAsString());
        }catch (Exception e) {
            log.error(e.getMessage());
        }

        //log.debug("- HEADER \n " + result.getHeaders().toString());
        //log.debug("- BODY \n " + result.getBody().toString());

        return result.getBody();
    }
}
