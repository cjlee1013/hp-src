package kr.co.homeplus.outbound.market.eleven.controller;

import kr.co.homeplus.outbound.core.OutboundApiApplication;
import kr.co.homeplus.outbound.market.eleven.claim.service.ElevenClaimService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.config.location=classpath:application.yml")
@ContextConfiguration(classes = OutboundApiApplication.class)
@EnableConfigurationProperties()
@Slf4j
public class ElevenClaimControllerTest {

    @Autowired
    private ElevenClaimService elevenClaimService;

    @Test
    public void getClaimList() throws Exception {
//        naverService.getChangedProductOrderList("20210706135000", "20210706135959", "PAYED");
        log.info("cancelInfo {}", elevenClaimService.getElevenClaimInfo("2021-08-31 00:00:00", "2021-08-31 23:59:59"));
        log.info("cancelInfo {}", elevenClaimService.getElevenClaimReturnInfo("2021-09-01 00:00:00", "2021-09-01 23:59:59"));
        log.info("cancelInfo {}", elevenClaimService.getElevenClaimExchangeInfo("2021-09-01 00:00:00", "2021-09-01 23:59:59"));
//        naverClaimService.setTestDelivary("2021071215026441", DeliveryMethodType.DIRECT_DELIVERY, "HOMEPLUS");
    }
}
