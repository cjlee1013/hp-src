package kr.co.homeplus.outbound.market.naver.controller;

import com.nhncorp.platform.shopn.seller.SellerServiceStub.ProductOrderChangeType;
import java.util.List;
import kr.co.homeplus.outbound.core.OutboundApiApplication;
import kr.co.homeplus.outbound.market.eleven.claim.service.ElevenClaimService;
import kr.co.homeplus.outbound.market.naver.claim.model.NaverClaimDataDto;
import kr.co.homeplus.outbound.market.naver.claim.service.NaverClaimService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.config.location=classpath:application.yml")
@ContextConfiguration(classes = OutboundApiApplication.class)
@EnableConfigurationProperties()
public class NaverClaimControllerTest {

    @Autowired
    private NaverClaimService naverClaimService;

    @Test
    public void getClaimList() throws Exception {
        String startDt = "2021-12-16 13:30:00";
        String endDt = "2021-12-16 13:33:00";
        List<NaverClaimDataDto> aaa = naverClaimService.getChangedProductOrderList(startDt, endDt, ProductOrderChangeType.RETURNED);
        for(NaverClaimDataDto dto : aaa) {
            log.info(" order {}", dto.getOrder());
            dto.getMarketClaimProductOrderList().stream().forEach(dtos -> log.info("{} / {}", dto.getOrder().getOrderId(), dtos.getProductOrder()));
        }
//        naverClaimService.getProductOrderList("2021083115273341");
//        naverClaimService.setTestDelivary("2021071215026441", DeliveryMethodType.DIRECT_DELIVERY, "HOMEPLUS");
    }
}
