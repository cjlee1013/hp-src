package kr.co.homeplus.outbound.store.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.poi.sl.usermodel.ObjectMetaData.Application;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@Suite.SuiteClasses({TestConfig.class})
@SpringBootTest(classes = Application.class)
class StoreControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void getStoreList()  throws Exception {
        mockMvc.perform(get("/store/getStoreInfo?storeId=0037"))
            .andDo(print())
            .andExpect(status().isOk());
    }
}