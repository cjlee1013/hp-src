package kr.co.homeplus.outbound.ticket;

import kr.co.homeplus.outbound.ticket.model.ClaimTicketInfoOrderTicketResponse;
import kr.co.homeplus.outbound.ticket.model.ClaimTicketInfoResponse;
import kr.co.homeplus.outbound.ticket.model.ClaimTicketResponse;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

import static org.assertj.core.api.Assertions.assertThat;

public class CoopApiConnectCancelTest {

    @Test
    void cancelTicket() throws JAXBException {
        // given
        /*
        String coopStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                + "<CREATECOUPON_OUT_06 xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://gsapi.m2i.kr/\">\n"
                + "  <RESULTCODE>00</RESULTCODE>\n"
                + "  <RESULTMSG>정상처리</RESULTMSG>\n"
                + "  <COUPONNUMBER>981670522040</COUPONNUMBER>\n"
                + "  <PINNUMBER>508146314829</PINNUMBER>\n"
                + "  <END_DAY>2021-11-06</END_DAY>\n"
                + "  <INDATA>\n"
                + "  \t<CODE>0161</CODE>\n"
                + "  \t<PASS>home123</PASS>\n"
                + "  \t<COUPONCODE>00A3110200202</COUPONCODE>\n"
                + "  \t<SEQNUMBER>4939</SEQNUMBER>\n"
                + "  </INDATA>\n"
                + "</CREATECOUPON_OUT_06>";
        */
        String coopStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                + "<COUPONCANCEL_OUT_04 xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://gsapi.m2i.kr/\">\n"
                + " 	<RESULTCODE>00</RESULTCODE> \n"
                + " 	<RESULTMSG>처리완료</RESULTMSG> \n"
                + " 	<INDATA> \n"
                + " 		<CODE>0161</CODE> \n"
                + " 		<PASS>home123</PASS> \n"
                + " 		<COUPONCODE>00CA47YO00201</COUPONCODE> \n"
                + " 		<COUPONNUM>761776559334</COUPONNUM> \n"
                + " 	</INDATA> \n"
                + " </COUPONCANCEL_OUT_04>  \n";

        // when
        StringReader stringReader = new StringReader(coopStr);
        JAXBContext jaxbContext = JAXBContext.newInstance(ClaimTicketResponse.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        ClaimTicketResponse claimTicketResponse = (ClaimTicketResponse) unmarshaller.unmarshal(stringReader);

        // then
        assertThat(claimTicketResponse).isNotNull();
        assertThat(claimTicketResponse.getResultCode()).isEqualTo("00");
        assertThat(claimTicketResponse.getResultMsg()).isEqualTo("처리완료");

        assertThat(claimTicketResponse.getInData().getCouponNumber()).isEqualTo("761776559334");

    }

    @Test
    void cancelTicketInfo() throws JAXBException {
        String coopStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                + "<DOWNLOADINFOONE_OUT xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://gsapi.m2i.kr/\">\n"
                + " <RESULTCODE>00</RESULTCODE>  \n"
                + " <RESULTMSG>완료</RESULTMSG>  \n"
                + " <COUPONNUMBER>41115635945964</COUPONNUMBER>  \n"
                + " <USE_YN>N</USE_YN>  \n"
                + " <USE_DATE>-</USE_DATE>  \n"
                + " <USE_PLACE>-</USE_PLACE>  \n"
                + " <SEND_HP>01030298508</SEND_HP>  \n"
                + " <USE_START>2021-08-25</USE_START>  \n"
                + " <USE_END>2021-11-26</USE_END>  \n"
                + " <INS_DATE>2021-08-25</INS_DATE>  \n"
                + " <FILLER01 />  \n"
                + " <FILLER02 />  \n"
                + " <FILLER03 /> \n"
                + "</DOWNLOADINFOONE_OUT> \n";

        // when
        StringReader stringReader = new StringReader(coopStr);
        JAXBContext jaxbContext = JAXBContext.newInstance(ClaimTicketInfoResponse.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        ClaimTicketInfoResponse claimTicketInfoResponse = (ClaimTicketInfoResponse) unmarshaller.unmarshal(stringReader);

        // then
        assertThat(claimTicketInfoResponse).isNotNull();
        assertThat(claimTicketInfoResponse.getResultCode()).isEqualTo("00");
        assertThat(claimTicketInfoResponse.getResultMsg()).isEqualTo("완료");

        assertThat(claimTicketInfoResponse.getUseYn()).isEqualTo("N");
        assertThat(claimTicketInfoResponse.getUseDate()).isEqualTo("-");
        assertThat(claimTicketInfoResponse.getUsePlace()).isEqualTo("-");
        assertThat(claimTicketInfoResponse.getSendHp()).isEqualTo("01030298508");
        assertThat(claimTicketInfoResponse.getUseStart()).isEqualTo("2021-08-25");
        assertThat(claimTicketInfoResponse.getUseEnd()).isEqualTo("2021-11-26");
        assertThat(claimTicketInfoResponse.getInsDate()).isEqualTo("2021-08-25");
        assertThat(claimTicketInfoResponse.getFiller01()).isEmpty();
//        assertThat(claimTicketInfoResponse.getFiller02()).isNull();
//        assertThat(claimTicketInfoResponse.getFiller03()).isNull();
    }

    @Test
    void cancelTicketInfoOrderTicket() throws JAXBException {
        String coopStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                + "<DOWNLOADINFO_OUT xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://gsapi.m2i.kr/\"> \n"
                + "  <RESULTCODE>00</RESULTCODE> \n "
                + "  <RESULTMSG>완료</RESULTMSG>   \n "
                + "  <DOWN_COUNT>1</DOWN_COUNT>   \n "
                + "  <COUPON_INFO> \n "
                + "      <DownloadCoupon> \n "
                + "            <COUNT>1</COUNT> \n "
                + "            <COUPONNUMBER>763020784004</COUPONNUMBER>       \n "
                + "            <USE_YN>N</USE_YN>       \n "
                + "            <USE_DATE>-</USE_DATE>       \n "
                + "            <USE_PLACE>-</USE_PLACE>       \n "
                + "            <SEND_HP>01044212993</SEND_HP>       \n "
                + "            <USE_START>2021-06-08</USE_START>       \n "
                + "            <USE_END>2021-07-08</USE_END>       \n "
                + "            <INS_DATE>2021-06-08</INS_DATE>       \n "
                + "            <FILLER01 />       \n "
                + "            <FILLER02 />       \n "
                + "            <FILLER03 />       \n "
                + "            <PINNUMBER>360835404370</PINNUMBER>       \n "
                + "            <STATE>C</STATE>     \n "
                + "      </DownloadCoupon>   \n "
                + "  </COUPON_INFO> \n "
                + "</DOWNLOADINFO_OUT> \n ";

        // when
//        StringReader stringReader = new StringReader(coopStr);
//        JAXBContext jaxbContext = JAXBContext.newInstance(ClaimTicketInfoResponse.class);
//        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
//        ClaimTicketInfoResponse claimTicketInfoResponse = (ClaimTicketInfoResponse) unmarshaller.unmarshal(stringReader);
//
//        // then
//        assertThat(claimTicketInfoResponse).isNotNull();
//        assertThat(claimTicketInfoResponse.getResultCode()).isEqualTo("00");
//        assertThat(claimTicketInfoResponse.getResultMsg()).isEqualTo("완료");
//
//        assertThat(claimTicketInfoResponse.getUseYn()).isEqualTo("N");
//        assertThat(claimTicketInfoResponse.getUseDate()).isEqualTo("-");
//        assertThat(claimTicketInfoResponse.getUsePlace()).isEqualTo("-");
//        assertThat(claimTicketInfoResponse.getSendHp()).isEqualTo("01030298508");
//        assertThat(claimTicketInfoResponse.getUseStart()).isEqualTo("2021-08-25");
//        assertThat(claimTicketInfoResponse.getUseEnd()).isEqualTo("2021-11-26");
//        assertThat(claimTicketInfoResponse.getInsDate()).isEqualTo("2021-08-25");
//        assertThat(claimTicketInfoResponse.getFiller01()).isEmpty();
//        assertThat(claimTicketInfoResponse.getFiller02()).isNull();
//        assertThat(claimTicketInfoResponse.getFiller03()).isNull();

        // JAXB parsing
        StringReader stringReader = new StringReader(coopStr);
        JAXBContext jaxbContext = JAXBContext.newInstance(ClaimTicketInfoOrderTicketResponse.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        ClaimTicketInfoOrderTicketResponse claimTicketInfoOrderTicketResponse = (ClaimTicketInfoOrderTicketResponse) unmarshaller.unmarshal(stringReader);

        // then
        assertThat(claimTicketInfoOrderTicketResponse).isNotNull();
        assertThat(claimTicketInfoOrderTicketResponse.getResultCode()).isEqualTo("00");
        assertThat(claimTicketInfoOrderTicketResponse.getResultMsg()).isEqualTo("완료");

        ClaimTicketInfoOrderTicketResponse.CouponInfo.DownloadCoupon downloadCoupon = claimTicketInfoOrderTicketResponse.getDownloadCoupon();

        System.out.println("couponInfo ["+downloadCoupon.getCouponNumber() + "]");

        assertThat(downloadCoupon.getCouponNumber()).isEqualTo("763020784004");
        assertThat(downloadCoupon.getUseYn()).isEqualTo("N");
        assertThat(downloadCoupon.getUseDate()).isEqualTo("-");
        assertThat(downloadCoupon.getUsePlace()).isEqualTo("-");
        assertThat(downloadCoupon.getSendHp()).isEqualTo("01044212993");
        assertThat(downloadCoupon.getUseStart()).isEqualTo("2021-06-08");
        assertThat(downloadCoupon.getUseEnd()).isEqualTo("2021-07-08");
        assertThat(downloadCoupon.getInsDate()).isEqualTo("2021-06-08");
        assertThat(downloadCoupon.getFiller01()).isEmpty();

    }

}
