package kr.co.homeplus.mileage.core.mileage.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import kr.co.homeplus.mileage.core.TestConfig;
import kr.co.homeplus.mileage.external.model.ExternalMyPageSetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponSearchSetDto;
import kr.co.homeplus.mileage.office.model.history.MileageHistoryReqDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSearchSetDto;
import kr.co.homeplus.mileage.sql.MileageCommonSql;
import kr.co.homeplus.mileage.sql.MileageCouponSql;
import kr.co.homeplus.mileage.sql.MileageExternalSql;
import kr.co.homeplus.mileage.sql.MileagePaymentSql;
import kr.co.homeplus.mileage.sql.MileageUseSql;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * SampleController test
 * https://github.com/json-path/JsonPath 참조.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Suite.SuiteClasses({TestConfig.class})
public class SampleControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void test_sql() throws Exception {
//        MileageHistoryReqDto reqDto = new MileageHistoryReqDto();
//        reqDto.setMileageCategory("A");
//        reqDto.setSchStartDt("2021-02-14");
//        reqDto.setSchEndDt("2021-02-14");
//        System.out.println(MileageCommonSql.selectMileageHistoryList(reqDto));
//        MileagePaymentSearchSetDto searchSetDto = new MileagePaymentSearchSetDto();
//        searchSetDto.setSchType("A");
//        searchSetDto.setUseYn("A");
//        searchSetDto.setRequestType("A");
//        searchSetDto.setMileageKind("A");
//        searchSetDto.setSchStartDt("2021-02-14");
//        searchSetDto.setSchEndDt("2021-02-15");
//        System.out.println(MileagePaymentSql.selectMileagePaymentInfoList(searchSetDto));
        System.out.println(MileageUseSql.selectUseCancelMileageInfo("108177298", "3000023917"));
    }

    @Test
    public void test_point() throws Exception {
        mockMvc.perform(get("/point"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.data.id", is("123")))
            .andExpect(jsonPath("$.data.password", is("739835d28f0f1e6373b5f43332637d9e")))
            .andExpect(jsonPath("$.data.name", is("plus")))
            .andExpect(jsonPath("$.data.age", is(1)));
    }

    @Test
    public void test_pointXss() throws Exception {
        mockMvc.perform(get("/pointXss?text=<script></script>"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.data", is("&lt;script&gt;&lt;/script&gt;")));
    }

    @Test
    public void test_getSlaveSampleText() throws Exception {
        mockMvc.perform(get("/pointSlaveDbConnection"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    public void test_sql_view() throws Exception {
//        ExternalMyPageSetDto dto = new ExternalMyPageSetDto();
//        dto.setPage(1);
//        dto.setPerPage(100);
//        dto.setSchStartDt("2020-12-01");
//        dto.setSchEndDt("2020-12-31");
//        dto.setSchType("A");
//        dto.setUserNo("15404140");
//        System.out.println(MileageExternalSql.selectExternalMileageList(dto));
//        MileagePaymentSearchSetDto searchSetDto = new MileagePaymentSearchSetDto();
//        searchSetDto.setSchStartDt("2020-12-25");
//        searchSetDto.setSchEndDt("2021-01-25");
//        searchSetDto.setRequestType("01");
//        searchSetDto.setMileageKind("01");
//        searchSetDto.setUseYn("Y");
//        searchSetDto.setSchType("01");
//        MileageHistoryReqDto searchSetDto = new MileageHistoryReqDto();
//        searchSetDto.setUserNo(12484963L);
//        searchSetDto.setSchStartDt("2021-02-01");
//        searchSetDto.setSchEndDt("2021-02-10");
//        searchSetDto.setMileageCategory("ALL");
//        System.out.println(MileageCommonSql.selectMileageHistoryList(searchSetDto));
        System.out.println(MileagePaymentSql.selectMileageSaveReturnInfo("3000013065", "H1001"));

    }


}

