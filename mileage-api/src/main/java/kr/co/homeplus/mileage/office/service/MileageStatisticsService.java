package kr.co.homeplus.mileage.office.service;

import java.util.List;
import kr.co.homeplus.mileage.office.model.stat.MileageStatSumListGet;
import kr.co.homeplus.mileage.office.model.stat.MileageStatSumListSet;
import kr.co.homeplus.mileage.office.model.stat.MileageStatTypeSumListGet;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service("MileageStatisticsService")
@RequiredArgsConstructor
public class MileageStatisticsService {
    // Office Mapper Management Service
    private final OfficeMapperManageService officeMapperService;

    /**
     * 마일리지 일자별 금액 합계 통계 조회
     * @param MileageStatSumListSet 마일리지 통계 조회 DTO
     * @return <List<MileageStatSumListGet>>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public List<MileageStatSumListGet> getDailyMileageSummary(MileageStatSumListSet requestDto) throws Exception {

        List<MileageStatSumListGet> mileageHistoryGetDto = officeMapperService.getDailyMileageSummary(requestDto);

        return mileageHistoryGetDto;
    }

    /**
     * 마일리지 일자별 금액 합계 통계 조회
     * @param MileageStatSumListSet 마일리지 통계 조회 DTO
     * @return <List<MileageStatSumListGet>>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public List<MileageStatTypeSumListGet> getDailyMileageTypeSummary(MileageStatSumListSet requestDto) throws Exception {

        List<MileageStatTypeSumListGet> mileageHistoryGetDto = officeMapperService.getDailyMileageTypeSummary(requestDto);

        return mileageHistoryGetDto;
    }
}
