package kr.co.homeplus.mileage.office.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.mileage.office.model.stat.MileageStatSumListGet;
import kr.co.homeplus.mileage.office.model.stat.MileageStatSumListSet;
import kr.co.homeplus.mileage.office.model.stat.MileageStatTypeSumListGet;
import kr.co.homeplus.mileage.office.service.MileageStatisticsService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/office/stat")
@Api(tags = "MileageStatisticsController", description = "BackOffice 마일리지 통계 Controller")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
}
)
@RequiredArgsConstructor
public class MileageStatisticsController {

    private final MileageStatisticsService statService;

    @ApiOperation(value = "마일리지 일자별 금액 합계 통계 조회", response = ResponseObject.class)
    @PostMapping("/getDailyMileageSummary")
    public ResponseObject<List<MileageStatSumListGet>> getDailyMileageSummary(@RequestBody @Valid MileageStatSumListSet requestDto) throws Exception {
        return ResourceConverter.toResponseObject(statService.getDailyMileageSummary(requestDto));
    }

    @ApiOperation(value = "마일리지 일자/타입별 금액 합계 통계 조회", response = ResponseObject.class)
    @PostMapping("/getDailyMileageTypeSummary")
    public ResponseObject<List<MileageStatTypeSumListGet>> getDailyMileageTypeSummary(@RequestBody @Valid MileageStatSumListSet requestDto) throws Exception {
        return ResourceConverter.toResponseObject(statService.getDailyMileageTypeSummary(requestDto));
    }

}
