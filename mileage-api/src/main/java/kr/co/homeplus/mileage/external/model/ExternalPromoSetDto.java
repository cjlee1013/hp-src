package kr.co.homeplus.mileage.external.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
@ApiModel(description = "외부연동 > 마일리지 적립 > 행사적립")
public class ExternalPromoSetDto {

    @ApiModelProperty(value = "마일리지타입번호", position = 1)
    @NotNull(message = "마일리지타입번호가 없습니다.")
    private String mileageTypeNo;

    @ApiModelProperty(value = "요청사유", position = 2)
    @Size(max = 50, message = "요청사유는 50자를 초과할 수 없습니다.")
    @NotNull(message = "요청사유가 없습니다.")
    private String requestReason;

    @ApiModelProperty(value = "고객번호", position = 3)
    @NotNull(message = "고객번호가 없습니다.")
    private String userNo;

    @ApiModelProperty(value = "마일리지금액", position = 4)
    @Min(value = 10, message = "최소 10 마일리지 이상 적립가능합니다.")
    @Max(value = 10000000, message = "1,000만 마일리지를 넘어갈수 없습니다.")
    private long mileageAmt;

    @ApiModelProperty(value = "거래번호", position = 5)
    @Length(max = 20, message = "20자까지 입력가능합니다.")
    private String tradeNo;

    @ApiModelProperty(value = "요청자", position = 6)
    @NotNull(message = "요청자가 없습니다.")
    private String regId;

}
