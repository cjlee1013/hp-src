package kr.co.homeplus.mileage.office.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentGetDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSearchGetDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSearchSetDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDetailDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentModifySetDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSetDto;
import kr.co.homeplus.mileage.office.service.MileagePaymentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/office/payment")
@Api(tags = "MileagePaymentController", description = "BackOffice 마일리지 지급/회수 조회 Controller")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
}
)
@RequiredArgsConstructor
public class MileagePaymentController {

    //마일리지 지급/회수 Service
    private final MileagePaymentService mileagePaymentService;

    @ApiOperation(value = "마일리지 지급/회수 요청", response = ResponseObject.class)
    @PostMapping("/setMileageRequestInfo")
    public ResponseObject<MileagePaymentGetDto> setMileageRequestInfo(@RequestBody @Valid MileagePaymentSetDto mileagePaymentSetDto) throws Exception{
        return mileagePaymentService.setMileagePaymentInfo(mileagePaymentSetDto);
    }

    @ApiOperation(value = "마일리지 지급/회수 수정 요청", response = ResponseObject.class)
    @PostMapping("/modifyMileageRequestInfo")
    public ResponseObject<MileagePaymentGetDto> modifyMileageRequestInfo(@RequestBody @Valid MileagePaymentModifySetDto mileagePaymentModifySetDto) throws Exception{
        return mileagePaymentService.setModifyMileagePaymentInfo(mileagePaymentModifySetDto);
    }

    @ApiOperation(value = "마일리지 지급/회수 조회", response = ResponseObject.class)
    @PostMapping("/getSearchMileagePaymentInfo")
    public ResponseObject<List<MileagePaymentSearchGetDto>> getSearchMileagePaymentInfo(@RequestBody @Valid MileagePaymentSearchSetDto mileagePaymentSearchSetDto) throws Exception{
        return mileagePaymentService.getSearchMileagePaymentInfo(mileagePaymentSearchSetDto);
    }

    @ApiOperation(value = "마일리지 지급/회수 기본정보 조회", response = ResponseObject.class)
    @PostMapping("/getSearchMileagePaymentInfo/basic/{mileageReqNo}")
    public ResponseObject<MileageReqDto> getSearchMileagePaymentBasicInfo(@PathVariable(value = "mileageReqNo") String mileageReqNo) throws Exception{
        return mileagePaymentService.getSearchMileagePaymentBasicInfo(mileageReqNo);
    }

    @ApiOperation(value = "마일리지 지급/회수 대상회원 조회", response = ResponseObject.class)
    @PostMapping("/getSearchMileagePaymentInfo/user/{mileageReqNo}")
    public ResponseObject<List<MileageReqDetailDto>> getSearchMileagePaymentUserInfo(@PathVariable(value = "mileageReqNo") String mileageReqNo) throws Exception{
        return mileagePaymentService.getSearchMileagePaymentUserInfo(mileageReqNo);
    }

}
