package kr.co.homeplus.mileage.office.mapper;

import java.util.HashMap;
import java.util.List;
import kr.co.homeplus.mileage.core.db.annotation.MasterConnection;
import kr.co.homeplus.mileage.management.model.save.SaveMileageDto;
import kr.co.homeplus.mileage.management.model.use.ReturnMileageSetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponRequestDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDetailDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeManageSetDto;
import kr.co.homeplus.mileage.sql.MileageCommonSql;
import kr.co.homeplus.mileage.sql.MileageCouponSql;
import kr.co.homeplus.mileage.sql.MileagePaymentSql;
import kr.co.homeplus.mileage.sql.MileageTypeSql;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.mapping.StatementType;

@MasterConnection
public interface OfficeWriteMapper {

    //########################################################################################################################
    // 마일리지 적립/회수
    //########################################################################################################################
    //마일리지 적립/회수 요청 등록
    @Options(useGeneratedKeys = true, keyProperty = "mileageReqNo")
    @InsertProvider(type = MileagePaymentSql.class, method = "insertMileageRequestInfo")
    int insertMileageRequestInfo(MileageReqDto mileageReqDto);

    //마일리지 적립/회수 요청 수정
    @UpdateProvider(type = MileagePaymentSql.class, method = "updateMileageReqStatus")
    int updateMileageReqStatus(HashMap<String, Object> parameterMap);

    // 마일리지 적립/회수 요청 상세 등록
    @SelectKey(statement = MileageCommonSql.getDetailCode, keyProperty = "detailCode", keyColumn = "detailCode", before = true, resultType = java.lang.String.class)
    @InsertProvider(type = MileagePaymentSql.class, method = "insertMileageRequestDetailInfo")
    int insertMileageRequestDetailInfo(MileageReqDetailDto mileageReqDetailDto);

    // 마일리지 적립/회수 요청 상세 수정
    @UpdateProvider(type = MileagePaymentSql.class, method = "updateMileageReqUserStatus")
    int updateMileageReqUserStatus(HashMap<String, Object> parameterMap);

    //마일리지 적립 정보 조회
    @SelectProvider(type = MileagePaymentSql.class, method = "selectMakeMileageSaveInfo")
    List<SaveMileageDto> selectMakeMileageSaveInfo(@Param("mileageReqNo") long mileageReqNo, @Param("expireDay") int expireDay);

    @SelectProvider(type = MileagePaymentSql.class, method = "selectMakeMileageReturnInfo")
    List<ReturnMileageSetDto> selectMakeMileageReturnInfo(@Param("mileageReqNo") long mileageReqNo);
    //########################################################################################################################

    //########################################################################################################################
    // 마일리지 타입
    //########################################################################################################################
    //마일리지 타입 유형 등록
    @Options(useGeneratedKeys = true, keyProperty="mileageTypeNo")
    @InsertProvider(type = MileageTypeSql.class, method = "insertMileageTypeInfo")
    int insertMileageTypeInfo(MileageTypeManageSetDto mileageTypeManageSetDto);
    //마일리지 타입 유형 수정
    @UpdateProvider(type = MileageTypeSql.class, method = "updateMileageTypeInfo")
    int updateMileageTypeInfo(HashMap<String, Object> parameterMap);
    //########################################################################################################################

    //########################################################################################################################
    // 마일리지 쿠폰 등록
    //########################################################################################################################
    // 마일리지 쿠폰 발급 등록 요청
    @Options(useGeneratedKeys = true, keyProperty="couponManageNo")
    @InsertProvider(type = MileageCouponSql.class, method = "insertMileageCouponManage")
    int insertMileageCouponManage(MileageCouponRequestDto setDto);
    // 마일리지 쿠폰 발급
    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = MileageCouponSql.class, method = "callByMileageCouponPublish")
    void callByMileageCouponPublish(HashMap<String, String> parameterMap);
    // 쿠폰 정보 등록(다회성)
    @Options(useGeneratedKeys = true, keyProperty="coupon_info_seq")
    @InsertProvider(type = MileageCouponSql.class, method = "insertMileageCouponInfo")
    int insertMileageCouponInfo(HashMap<String, Object> parameterMap);
    // 쿠폰 정보 업데이트
    @UpdateProvider(type = MileageCouponSql.class, method = "updateMileageCouponInfo")
    int updateMileageCouponInfo(HashMap<String, Object> parameterMap);
    // 마일리지 쿠폰 등록 정보 업데이트
    @UpdateProvider(type = MileageCouponSql.class, method = "updateMileageCouponManage")
    int updateMileageCouponManage(HashMap<String, Object> parameterMap);
    // 마일리지요청번호로 마일리지적립번호 찾기
    @SelectProvider(type = MileageCouponSql.class, method = "selectFindMileageCouponSaveNo")
    String selectFindMileageCouponSaveNo(@Param("mileageReqNo") String mileageReqNo);
    // 쿠폰회수 업데이트
    @UpdateProvider(type = MileageCouponSql.class, method = "updateMileageCouponInfoReturn")
    int updateMileageCouponInfoReturn(@Param("couponInfoSeq") long couponInfoSeq, @Param("requestId") String requestId, @Param("isComplete") boolean isComplete);

    //########################################################################################################################
}
