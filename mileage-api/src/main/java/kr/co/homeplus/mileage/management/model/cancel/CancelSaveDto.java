package kr.co.homeplus.mileage.management.model.cancel;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CancelSaveDto {

    @ApiModelProperty(value = "등록키", position = 1)
    private String mileageManageNo;

    @ApiModelProperty(value = "마일리지 적립/회수 요청상세번호", position = 2)
    private long mileageReqDetailNo;

    @ApiModelProperty(value = "마일리지 유형번호", position = 3)
    private long mileageTypeNo;

    @ApiModelProperty(value = "고객번호", position = 4)
    private String userNo;

    @ApiModelProperty(value = "마일리지유형", position = 5)
    private String mileageCategory;

    @ApiModelProperty(value = "마일리지종류", position = 6)
    private String mileageKind;

    @ApiModelProperty(value = "적립금액", position = 7)
    private long mileageAmt;

    @ApiModelProperty(value = "취소가능금액", position = 8)
    private long cancelRemainAmt;

    @ApiModelProperty(value = "사용시작일", position = 9)
    private String useStartDt;

    @ApiModelProperty(value = "유효월일", position = 10)
    private String expireDt;

    @ApiModelProperty(value = "등록아이디", position = 11)
    private String regId;

    @ApiModelProperty(value = "수정아이디", position = 12)
    private String chgId;

    @ApiModelProperty(value = "취소거래번호", position = 13)
    @Length(max = 50, message = "50자까지 입력가능합니다.")
    private String tradeNo;

    @ApiModelProperty(value = "사유", position = 14, hidden = true)
    private String requestReason;
}
