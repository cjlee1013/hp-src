package kr.co.homeplus.mileage.office.model.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "지급/회수 상세 정보 Entity")
public class MileageReqDetailDto {

    @ApiModelProperty(value = "마일리지요청상세번호", position = 1)
    private String mileageReqDetailNo;

    @ApiModelProperty(value = "마일리지요청번호", position = 2)
    private String mileageReqNo;

    @ApiModelProperty(value = "고객번호", position = 3)
    @NotNull(message = "고객번호가 없습니다.")
    private String userNo;

    @ApiModelProperty(value = "마일리지금액", position = 4)
    @Max(value = 10000000, message = "1,000만 마일리지를 넘어갈수 없습니다.")
    private long mileageAmt;

    @ApiModelProperty(value = "마일리지상세상태", position = 5)
    private String mileageDetailStatus;

    @ApiModelProperty(value = "마일리지상세코드(오류코드)", position = 6)
    private String detailCode;

    @ApiModelProperty(value = "마일리지상세메시지(오류)", position = 7)
    private String detailMessage;

    @ApiModelProperty(value = "사용유무", position = 8)
    private String useYn;

    @ApiModelProperty(value = "등록아이디", position = 9)
    private String regId;

    @ApiModelProperty(value = "수정아이디", position = 10)
    private String chgId;

}
