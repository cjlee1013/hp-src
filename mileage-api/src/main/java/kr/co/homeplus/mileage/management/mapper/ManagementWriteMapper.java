package kr.co.homeplus.mileage.management.mapper;

import java.util.HashMap;
import java.util.List;
import kr.co.homeplus.mileage.management.model.use.MileageManageDto;
import kr.co.homeplus.mileage.management.model.use.MileageUseDto;
import kr.co.homeplus.mileage.management.model.user.MileageHistoryManageSetDto;
import kr.co.homeplus.mileage.management.model.save.SaveMileageDto;
import kr.co.homeplus.mileage.core.db.annotation.MasterConnection;
import kr.co.homeplus.mileage.management.model.user.MileageMstDto;
import kr.co.homeplus.mileage.sql.MileageCommonSql;
import kr.co.homeplus.mileage.sql.MileageMstSql;
import kr.co.homeplus.mileage.sql.MileagePaymentSql;
import kr.co.homeplus.mileage.sql.MileageSaveSql;
import kr.co.homeplus.mileage.sql.MileageUseSql;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Options.FlushCachePolicy;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

/**
 * 마일리지 사용/취소 등과 같이 DB에 값을 변경할때 사용.
 */
@MasterConnection
public interface ManagementWriteMapper {

    //============================================================================================================
    // 마일리지 적립
    //============================================================================================================
    //save Mileage
    @Options(useGeneratedKeys = true, keyProperty = "mileageSaveNo")
    @InsertProvider(type = MileageSaveSql.class, method = "insertUserMileageSave")
    int insertUserMileageSave(SaveMileageDto saveMileageDto);

    //user Mileage modify
    @UpdateProvider(type = MileageMstSql.class, method = "updateUserMileageMst")
    int updateUserMileageMst(@Param("userNo") String userNo, @Param("mileageAmt") long mileageAmt);

    //마일리지 사용/적립 히스토리.
    @InsertProvider(type = MileageCommonSql.class, method = "insertUserMileageHistory")
    int insertUserMileageHistory(MileageHistoryManageSetDto mileageHistoryManageSetDto);
    //============================================================================================================

    //============================================================================================================
    // 마일리지 취소 관련 Method
    //============================================================================================================
    //마일리지 적립 내용 되살리기
    @UpdateProvider(type = MileageSaveSql.class, method = "updateUserMileageSaveUsed")
    int updateUserMileageSaveUsed(SaveMileageDto saveMileageDto);

    @UpdateProvider(type = MileageUseSql.class, method = "updateMileageManageInfo")
    int updateMileageManageInfo(HashMap<String, Object> parameterMap);
    //============================================================================================================

    //============================================================================================================
    // 마일리지 사용 관련 Method
    //============================================================================================================
    // 마일리지 사용정보 저장.
    @Options(useGeneratedKeys = true, keyProperty = "mileageUseNo")
    @InsertProvider(type = MileageUseSql.class, method = "insertUserMileageUse")
    int insertUserMileageUse(MileageUseDto mileageUseDto);

    // 마일리지 관리 정보 저장
    @Options(useCache = false, flushCache = FlushCachePolicy.TRUE)
    @InsertProvider(type = MileageUseSql.class, method = "insertMileageManageInfo")
    int insertMileageManageInfo(MileageManageDto mileageManageDto);
    // 마일리쿠폰 회수
    @SelectProvider(type = MileagePaymentSql.class, method = "selectMileageCouponReturnInfo")
    List<SaveMileageDto> selectMileageCouponReturnInfo(@Param("tradeNo") String tradeNo);
    //============================================================================================================

    //============================================================================================================
    // 마일리지 유저 정보 관련 Method
    //============================================================================================================
    @InsertProvider(type = MileageMstSql.class, method = "insertUserMileageAccount")
    int insertUserMileageAccount(String userNo);

    @Options(useCache = false, flushCache = FlushCachePolicy.TRUE)
    @ResultType(value = MileageMstDto.class)
    @SelectProvider(type = MileageMstSql.class, method = "selectUserMileageMstInfo")
    MileageMstDto selectUserMileageMstInfo(String userNo);
    //============================================================================================================
}
