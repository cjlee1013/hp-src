package kr.co.homeplus.mileage.utils;

import java.util.Locale;
import kr.co.homeplus.mileage.core.exception.MileageLogicException;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.mileage.enums.impl.EnumImpl;
import kr.co.homeplus.plus.api.support.client.model.ResponsePagination;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

@Slf4j
public class MileageCommonResponseFactory<T> {

    /**
     * 마일리지 공통 응답 처리(기본)
     *
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public static <T> ResponseObject<T> getResponseObject(String returnCode, String returnMessage) throws Exception {
        return getResponseObject(returnCode, null, null, returnMessage, Boolean.FALSE, getHttpStatus(returnCode));
    }

    /**
     * 마일리지 공통 응답 처리(기본)
     *
     * @param enumInfo 응답코드정보
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo) throws Exception {
        return getResponseObject(enumInfo, null, null, Boolean.FALSE, getHttpStatus(enumInfo));
    }

    //Enum(응답코드, 응답메시지), 응답데이터
    /**
     * 마일리지 공통 응답 처리(응답데이터)
     *
     * @param enumInfo 응답코드정보
     * @param data 응답데이터
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    public static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, T data) throws Exception {
        return getResponseObject(enumInfo, data, null, Boolean.FALSE, getHttpStatus(enumInfo));
    }

    //Enum(응답코드, 응답메시지), 응답데이터, 응답메시지 변경시

    /**
     * 마일리지 공통 응답 처리(응답데이터, 응답메시지)
     * @param enumInfo 응답코드정보
     * @param data 응답데이터
     * @param message 응답메시지
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    public static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, T data, String message) throws Exception {
        return getResponseObject(enumInfo, data, message, Boolean.FALSE, getHttpStatus(enumInfo));
    }

    //Enum(응답코드, 응답메시지), 응답데이터, 응답메시지 변경시(문구치환)
    /**
     * 마일리지 공통 응답 처리(응답데이터, 응답메시지(문구치환))
     *
     * @param enumInfo 응답코드정보
     * @param data 응답데이터
     * @param message 응답메시지
     * @param replaceWord 변경할 문자열
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    public static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, T data, String message, String replaceWord) throws Exception {
        return getResponseObject(enumInfo, data, replaceReturnMsg(enumInfo.getResponseMessage(), replaceWord, message), Boolean.TRUE, getHttpStatus(enumInfo));
    }

    //Enum(응답코드, 응답메시지), 응답데이터, HttpStatus
    /**
     * 마일리지 공통 응답 처리(응답데이터, HttpStatus 상태변경시)
     *
     * @param enumInfo 응답코드정보
     * @param data 응답데이터
     * @param httpStatus httpStatus 코드정보
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    public static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, T data, HttpStatus httpStatus) throws Exception {
        return getResponseObject(enumInfo, data, null, Boolean.FALSE, httpStatus);
    }

    //Enum(응답코드, 응답메시지), 응답메시지 변경시
    /**
     * 마일리지 공통 응답 처리(응답메시지 변경시)
     *
     * @param enumInfo 응답코드정보
     * @param message 응답메시지
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, String message) throws Exception {
        return getResponseObject(enumInfo, null, message, Boolean.FALSE, getHttpStatus(enumInfo));
    }

    //Enum(응답코드, 응답메시지), 응답메시지 변경시(문구치환)
    /**
     * 마일리지 공통 응답 처리(응답메시지 문구 치환)
     *
     * @param enumInfo 응답코드정보
     * @param message 응답메시지
     * @param replaceWord 변경할 문자열
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    public static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, String message, String replaceWord) throws Exception {
        return getResponseObject(enumInfo, null, replaceReturnMsg(enumInfo.getResponseMessage(), replaceWord, message), Boolean.TRUE, getHttpStatus(enumInfo));
    }

    //Enum(응답코드, 응답메시지), 응답메시지 변경시, HttpStatus
    /**
     * 마일리지 공통 응답 처리(응답메시지, Httpstatus상태 변경시)
     *
     * @param enumInfo 응답코드정보
     * @param message 응답메시지
     * @param httpStatus httpStatus 코드정보
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    public static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, String message, HttpStatus httpStatus) throws Exception {
        return getResponseObject(enumInfo, null, message, Boolean.FALSE, httpStatus);
    }

    //Enum(응답코드, 응답메시지), 응답메시지 변경시(문구치환), HttpStatus
    /**
     * 마일리지 공통 응답 처리(응답메시지(문구치환), HttpStatus 상태 변경시)
     * @param enumInfo 응답코드정보
     * @param message 응답메시지
     * @param replaceWord 변경할 문자열
     * @param httpStatus httpStatus 코드정보
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    public static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, String message, String replaceWord, HttpStatus httpStatus) throws Exception {
        return getResponseObject(enumInfo, null, replaceReturnMsg(enumInfo.getResponseMessage(), replaceWord, message), Boolean.TRUE, httpStatus);
    }

    //Enum(응답코드, 응답메시지), HttpStatus
    /**
     * 마일리지 공통 응답 처리(HttpStatus 변경시.)
     *
     * @param enumInfo 응답코드정보
     * @param httpStatus httpStatus 코드정보
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    public static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, HttpStatus httpStatus) throws Exception {
        return getResponseObject(enumInfo, null, null, Boolean.FALSE, httpStatus);
    }

    //Enum(응답코드, 응답메시지), 응답데이터, 응답메시지 변경시, HttpStatus
    /**
     * 마일리지 공통 응답 처리(응답데이터, 응답메시지, Httpstatus상태 변경시)
     * @param enumInfo 응답코드정보
     * @param data 응답데이터
     * @param message 응답메시지
     * @param httpStatus httpStatus 코드정보
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    public static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, T data, String message, HttpStatus httpStatus) throws Exception {
        return getResponseObject(enumInfo, data, message, Boolean.FALSE, httpStatus);
    }

    //Enum(응답코드, 응답메시지), 응답데이터, 응답메시지 변경시(문구치환), HttpStatus
    /**
     * 마일리지 공통 응답 처리(응답데이터, 응답메시지(문구치환) HttpStatus 상태 변경시)
     *
     * @param enumInfo 응답코드정보
     * @param data 응답데이터
     * @param message 응답메시지
     * @param replaceWord 변경할 문자열
     * @param httpStatus httpStatus 코드정보
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    public static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, T data, String message, String replaceWord, HttpStatus httpStatus) throws Exception {
        return getResponseObject(enumInfo, data, replaceReturnMsg(enumInfo.getResponseMessage(), replaceWord, message), Boolean.TRUE, httpStatus);
    }

    //Enum(응답코드, 응답메시지), 응답데이터, 응답메시지 변경시(문구치환)
    /**
     * 마일리지 공통 응답 처리(응답데이터, 응답메시지(문구치환))
     *
     * @param enumInfo 응답코드정보
     * @param data 응답데이터
     * @param message 응답메시지
     * @param replaceWord 변경할 문자열
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    public static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, T data, ResponsePagination pagination, String message, String replaceWord) throws Exception {
        return getResponseObject(enumInfo, data, pagination, replaceReturnMsg(enumInfo.getResponseMessage(), replaceWord, message), Boolean.TRUE, getHttpStatus(enumInfo));
    }

    /**
     * 페이징처리시 공통 응답
     *
     * @param enumInfo 응답코드정보
     * @param data 응답데이터
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    public static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, T data, ResponsePagination pagination) {
        return getResponseObject(enumInfo, data, pagination, null, Boolean.FALSE, getHttpStatus(enumInfo));
    }

    /**
     * 페이징처리시 공통 응답 Spec 생성용.
     *
     * @param enumInfo 응답코드정보
     * @param data 응답데이터
     * @param returnMessage 응답메시지
     * @param isConvert Replace 여부
     * @param httpStatus httpStatus 코드정보
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    private static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, T data, ResponsePagination pagination, String returnMessage, boolean isConvert, HttpStatus httpStatus){
        return ResourceConverter.toResponseObject(data, httpStatus, enumInfo.getResponseCode(), isConvert ? returnMessage : enumInfo.getResponseMessage(), pagination);
    }

    /**
     * 페이징처리시 공통 응답 Spec 생성용.
     *
     * @param enumInfo 응답코드정보
     * @param data 응답데이터
     * @param returnMessage 응답메시지
     * @param isConvert Replace 여부
     * @param httpStatus httpStatus 코드정보
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    private static <T> ResponseObject<T> getResponseObject(String returnCode, T data, ResponsePagination pagination, String returnMessage, boolean isConvert, HttpStatus httpStatus){
        return ResourceConverter.toResponseObject(data, httpStatus, returnCode, returnMessage, pagination);
    }

    /**
     * 마일리지 공통 응답처리 Spec 생성용.
     *
     * @param enumInfo 응답코드정보
     * @param data 응답데이터
     * @param returnMessage 응답메시지
     * @param isConvert Replace 여부
     * @param httpStatus httpStatus 코드정보
     * @param <T> 응답데이터 Class 타입.
     * @return <T> ResponseObject<T>
     */
    private static <T> ResponseObject<T> getResponseObject(EnumImpl enumInfo, T data, String returnMessage, boolean isConvert, HttpStatus httpStatus) throws Exception {
        if(httpStatus.isError()){
            throw new MileageLogicException(enumInfo, data, returnMessage, isConvert);
        }
        return ResponseObject.Builder.<T>builder()
            .data(data)
            .returnCode(enumInfo.getResponseCode())
            .returnMessage(isConvert ? returnMessage : enumInfo.getResponseMessage())
            .status(httpStatus)
            .build();
    }

    /**
     * 응답메시지 문구치환
     *
     * @param sentence 기준문구
     * @param replaceWord 변경하고자하는 문구
     * @param replaceStr 변경할 대상문구
     * @return String
     */
    private static String replaceReturnMsg(String sentence, String replaceWord, String replaceStr)  {
        return StringUtils.replace(sentence, replaceWord.toLowerCase(Locale.KOREA), String.valueOf(replaceStr));
    }

    /**
     * 응답코드에 따른 HttpStatus 처리
     *
     * @param enumInfo 응답코드정보
     * @see EnumImpl
     * @return HttpStatus 상태
     */
    private static HttpStatus getHttpStatus(EnumImpl enumInfo){
        return ("SUCCESS,0000").contains(enumInfo.getResponseCode()) ? HttpStatus.OK : HttpStatus.UNPROCESSABLE_ENTITY;
    }

    /**
     * 응답코드에 따른 HttpStatus 처리
     *
     * @param returnCode 응답코드정보
     * @see EnumImpl
     * @return HttpStatus 상태
     */
    private static HttpStatus getHttpStatus(String returnCode){
        return ("SUCCESS,0000").contains(returnCode) ? HttpStatus.OK : HttpStatus.UNPROCESSABLE_ENTITY;
    }
}
