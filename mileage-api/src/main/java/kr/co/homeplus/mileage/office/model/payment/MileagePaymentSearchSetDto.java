package kr.co.homeplus.mileage.office.model.payment;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MileagePaymentSearchSetDto {

    @ApiModelProperty(value = "조회구분(실행예정일/실행일)", position = 1)
    @NotNull(message = "조회구분이 없습니다.")
    private String schType;

    @ApiModelProperty(value = "조회시작일", position = 2)
    @NotNull(message = "조회시작일이 없습니다.")
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", position = 3)
    @NotNull(message = "조회종료일이 없습니다.")
    private String schEndDt;

    @ApiModelProperty(value = "구분", position = 4)
    private String requestType;

    @ApiModelProperty(value = "마일리지종류", position = 5)
    private String mileageKind;

    @ApiModelProperty(value = "사용유무", position = 6)
    private String useYn;

    @ApiModelProperty(value = "상세검색", position = 7)
    private String schDetailCode;

    @ApiModelProperty(value = "상세검색내용", position = 8)
    private String schDetailDesc;

}
