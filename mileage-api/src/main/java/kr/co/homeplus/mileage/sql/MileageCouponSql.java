package kr.co.homeplus.mileage.sql;

import java.util.HashMap;
import java.util.LinkedHashMap;
import kr.co.homeplus.mileage.constants.CustomConstants;
import kr.co.homeplus.mileage.entity.EmployeeInfoEntity;
import kr.co.homeplus.mileage.entity.MileageCouponInfoEntity;
import kr.co.homeplus.mileage.entity.MileageCouponManageEntity;
import kr.co.homeplus.mileage.entity.MileageReqDetailEntity;
import kr.co.homeplus.mileage.entity.MileageSaveEntity;
import kr.co.homeplus.mileage.entity.MileageTypeEntity;
import kr.co.homeplus.mileage.enums.MileageCommonCode;
import kr.co.homeplus.mileage.enums.MysqlFunction;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponRequestDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponSearchSetDto;
import kr.co.homeplus.mileage.utils.EntityFactory;
import kr.co.homeplus.mileage.utils.ObjectUtils;
import kr.co.homeplus.mileage.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MileageCouponSql {

    public static String insertMileageCouponManage(MileageCouponRequestDto setDto) {
        MileageCouponManageEntity mileageCouponManage = EntityFactory.createEntityIntoValue(MileageCouponManageEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(mileageCouponManage))
            .INTO_COLUMNS(
                mileageCouponManage.mileageTypeNo, mileageCouponManage.couponType, mileageCouponManage.couponNm, mileageCouponManage.issueStartDt,
                mileageCouponManage.issueEndDt, mileageCouponManage.issueQty, mileageCouponManage.mileageAmt, mileageCouponManage.personalIssueType,
                mileageCouponManage.personalIssueQty, mileageCouponManage.displayMessage, mileageCouponManage.couponNo, mileageCouponManage.useYn,
                mileageCouponManage.regId, mileageCouponManage.regDt, mileageCouponManage.chgId, mileageCouponManage.chgDt
            )
            .INTO_COLUMNS(mileageCouponManage.issueYn)
            .INTO_VALUES(
                SqlUtils.convertInsertParamToCamelCase(
                    mileageCouponManage.mileageTypeNo, mileageCouponManage.couponType, mileageCouponManage.couponNm, mileageCouponManage.issueStartDt,
                    mileageCouponManage.issueEndDt, mileageCouponManage.issueQty, mileageCouponManage.mileageAmt, mileageCouponManage.personalIssueType,
                    mileageCouponManage.personalIssueQty, mileageCouponManage.displayMessage, mileageCouponManage.couponNo, mileageCouponManage.useYn,
                    "requestId", mileageCouponManage.regDt, "requestId", mileageCouponManage.chgDt
                )
            )
            .INTO_VALUES("'N'");
        log.debug("insertMileageCouponManage \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileageMultiCouponRegCheck(@Param("couponNo") String couponNo) {
        MileageCouponManageEntity mileageCouponManage = EntityFactory.createEntityIntoValue(MileageCouponManageEntity.class);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.appendSingleQuoteWithAlias(couponNo, "coupon_no"),
                SqlUtils.sqlFunction(MysqlFunction.COUNT, mileageCouponManage.couponNo, "cnt")
            )
            .FROM(EntityFactory.getTableName(mileageCouponManage))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageCouponManage.couponNo, couponNo)
            );
        log.debug("selectMileageMultiCouponRegCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String callByMileageCouponPublish(HashMap<String, String> parameterMap) {
        return SqlUtils.sqlFunction(MysqlFunction.SP_MILEAGE_COUPON_PUBLISH, parameterMap.values().toArray());
    }

    public static String selectMileageCouponInfo(@Param("couponNo") String couponNo) {
        MileageCouponManageEntity mileageCouponManage = EntityFactory.createEntityIntoValue(MileageCouponManageEntity.class, Boolean.TRUE);
        MileageCouponInfoEntity mileageCouponInfo = EntityFactory.createEntityIntoValue(MileageCouponInfoEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.MAX, mileageCouponManage.couponType, mileageCouponManage.couponType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, mileageCouponManage.personalIssueType, mileageCouponManage.personalIssueType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, mileageCouponManage.personalIssueQty, mileageCouponManage.personalIssueQty),
                SqlUtils.sqlFunction(MysqlFunction.MAX, mileageCouponManage.couponManageNo, mileageCouponManage.couponManageNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, mileageCouponManage.mileageAmt, mileageCouponManage.mileageAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, mileageCouponManage.mileageTypeNo, mileageCouponManage.mileageTypeNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, mileageCouponManage.displayMessage, mileageCouponManage.displayMessage),
                SqlUtils.sqlFunction(MysqlFunction.MAX, mileageCouponManage.couponNm, mileageCouponManage.couponNm),
                SqlUtils.sqlFunction(MysqlFunction.MAX, mileageCouponManage.issueQty, mileageCouponManage.issueQty),
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL_BLANK,
                    SqlUtils.sqlFunction(MysqlFunction.MAX, mileageCouponInfo.couponInfoSeq),
                    mileageCouponInfo.couponInfoSeq
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, mileageCouponManage.regDt),
                        CustomConstants.YYYYMMDD
                    ),
                    mileageCouponManage.regDt
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageCouponManage))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(mileageCouponInfo, ObjectUtils.toArray(mileageCouponManage.couponManageNo)))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT_EQUAL, mileageCouponManage.issueStartDt, "CURDATE()"),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT_EQUAL, mileageCouponManage.issueEndDt, "CURDATE()"),
                SqlUtils.equalColumnByInput(mileageCouponManage.useYn, "Y"),
                SqlUtils.OR(
                    SqlUtils.equalColumnByInput(mileageCouponManage.couponNo, couponNo),
                    SqlUtils.equalColumnByInput(mileageCouponInfo.couponNo, couponNo)
                )
            );
        log.debug("selectMileageCouponInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileageCouponRegCheck(LinkedHashMap<String, Object> parameterMap) {
        MileageCouponInfoEntity mileageCouponInfo = EntityFactory.createEntityIntoValue(MileageCouponInfoEntity.class, Boolean.TRUE);
        MileageCouponManageEntity mileageCouponManage = EntityFactory.createEntityIntoValue(MileageCouponManageEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, mileageCouponInfo.userNo, "cnt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageCouponInfo))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageCouponManage, ObjectUtils.toArray(mileageCouponInfo.couponManageNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageCouponInfo.userNo, parameterMap.get("user_no")),
                SqlUtils.equalColumnByInput(mileageCouponInfo.couponManageNo, parameterMap.get("coupon_manage_no"))
            );

        if(ObjectUtils.toString(parameterMap.get("personal_issue_type")).equals("D")){
            // 해당 건은 일별 조건을 일별로 설정.
            query.WHERE(SqlUtils.equalColumn(mileageCouponInfo.useDt, "CURDATE()"));
        } else {
            // 해당 건은 쿠폰등록 기간으로 설정하여 조회.
            query.WHERE(SqlUtils.between(mileageCouponInfo.useDt, mileageCouponManage.issueStartDt, mileageCouponManage.issueEndDt));
        }

        log.debug("selectMileageCouponRegCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String insertMileageCouponInfo(LinkedHashMap<String, Object> parameterMap) {
        MileageCouponInfoEntity mileageCouponInfo = EntityFactory.createEntityIntoValue(MileageCouponInfoEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(mileageCouponInfo))
            .INTO_COLUMNS(
                mileageCouponInfo.couponNo, mileageCouponInfo.couponManageNo, mileageCouponInfo.userNo, mileageCouponInfo.userId,
                mileageCouponInfo.userNm, mileageCouponInfo.issueDt, mileageCouponInfo.useDt, mileageCouponInfo.regDt,
                mileageCouponInfo.chgDt, mileageCouponInfo.recoveryYn
            )
            .INTO_VALUES(
                SqlUtils.appendSingleQuote(parameterMap.get("coupon_no")),
                ObjectUtils.toString(parameterMap.get("coupon_manage_no")),
                ObjectUtils.toString(parameterMap.get("user_no")),
                SqlUtils.appendSingleQuote(parameterMap.get("user_id")),
                SqlUtils.appendSingleQuote(parameterMap.get("user_nm")),
                SqlUtils.appendSingleQuote(parameterMap.get("reg_dt")),
                "CURDATE()",
                "NOW()",
                "NOW()",
                "'N'"
            );
        log.debug("insertMileageCouponInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateMileageCouponInfo(LinkedHashMap<String, Object> parameterMap) {
        MileageCouponInfoEntity mileageCouponInfo = EntityFactory.createEntityIntoValue(MileageCouponInfoEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(mileageCouponInfo))
            .SET(
                SqlUtils.equalColumn(mileageCouponInfo.chgDt, "NOW()")
            );
        if(ObjectUtils.toString(parameterMap.get("mileage_save_no")) != null){
            query.SET(
                SqlUtils.equalColumnByInput(mileageCouponInfo.mileageSaveNo, parameterMap.get("mileage_save_no"))
            ).WHERE(
                SqlUtils.equalColumnByInput(mileageCouponInfo.couponInfoSeq, parameterMap.get("coupon_info_seq"))
            );
        } else {
            query.SET(
                SqlUtils.equalColumnByInput(mileageCouponInfo.userNo, parameterMap.get("user_no")),
                SqlUtils.equalColumnByInput(mileageCouponInfo.userId, parameterMap.get("user_id")),
                SqlUtils.equalColumnByInput(mileageCouponInfo.userNm, parameterMap.get("user_nm")),
                SqlUtils.equalColumn(mileageCouponInfo.useDt, "CURDATE()")
            ).WHERE(
                SqlUtils.equalColumnByInput(mileageCouponInfo.couponInfoSeq, parameterMap.get("coupon_info_seq")),
                SqlUtils.equalColumnByInput(mileageCouponInfo.couponNo, parameterMap.get("coupon_no")),
                SqlUtils.sqlFunction(MysqlFunction.IS_NULL, mileageCouponInfo.userNo),
                SqlUtils.sqlFunction(MysqlFunction.IS_NULL, mileageCouponInfo.useDt)
            );
        }
        log.debug("updateMileageCouponInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateMileageCouponManage(LinkedHashMap<String, Object> parameterMap) {
        MileageCouponManageEntity mileageCouponManage = EntityFactory.createEntityIntoValue(MileageCouponManageEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(mileageCouponManage))
            .SET(
                SqlUtils.equalColumnByInput(mileageCouponManage.chgId, parameterMap.get("request_id")),
                SqlUtils.equalColumn(mileageCouponManage.chgDt, "NOW()")
            );
        if(ObjectUtils.toString(parameterMap.get("issue_yn")) == null){
            query.SET(
                SqlUtils.equalColumnByInput(mileageCouponManage.couponNm, parameterMap.get("coupon_nm")),
                SqlUtils.equalColumnByInput(mileageCouponManage.displayMessage, parameterMap.get("display_message")),
                SqlUtils.equalColumnByInput(mileageCouponManage.useYn, parameterMap.get("use_yn"))
            );
        } else {
            query.SET(
                SqlUtils.equalColumnByInput(mileageCouponManage.issueYn, parameterMap.get("issue_yn"))
            );
        }
        query.WHERE(
            SqlUtils.equalColumnByInput(mileageCouponManage.couponManageNo, parameterMap.get("coupon_manage_no"))
        );
        log.debug("updateMileageCouponManage \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectFindMileageCouponSaveNo(@Param("mileageReqNo") String mileageReqNo) {
        MileageReqDetailEntity mileageReqDetail = EntityFactory.createEntityIntoValue(MileageReqDetailEntity.class, Boolean.TRUE);
        MileageSaveEntity mileageSave = EntityFactory.createEntityIntoValue(MileageSaveEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                mileageSave.mileageSaveNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageReqDetail))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageSave, ObjectUtils.toArray(mileageReqDetail.mileageReqDetailNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageReqDetail.mileageReqNo, mileageReqNo),
                SqlUtils.equalColumnByInput(mileageReqDetail.mileageDetailStatus, MileageCommonCode.MILEAGE_REQUEST_DETAIL_STATUS_COMPLETE.getResponseCode())
            );
        log.debug("selectFindMileageCouponSaveNo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectCouponIsPossibleCheck(@Param("couponManageNo") long couponManageNo) {
        MileageCouponInfoEntity mileageCouponInfo = EntityFactory.createEntityIntoValue(MileageCouponInfoEntity.class);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, mileageCouponInfo.mileageSaveNo, "cnt")
            )
            .FROM(EntityFactory.getTableName(mileageCouponInfo))
            .WHERE(SqlUtils.equalColumnByInput(mileageCouponInfo.couponManageNo, couponManageNo));
        log.debug("selectCouponIsPossibleCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectCouponUsedCheck(@Param("couponNo") String couponNo) {
        MileageCouponInfoEntity mileageCouponInfo = EntityFactory.createEntityIntoValue(MileageCouponInfoEntity.class);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.appendSingleQuoteWithAlias(couponNo, "coupon_no"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(mileageCouponInfo.recoveryYn, "N"),
                        SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, mileageCouponInfo.useDt)),
                        "N"
                    ),
                    "isPossible"
                )
            )
            .FROM(EntityFactory.getTableName(mileageCouponInfo))
            .WHERE(SqlUtils.equalColumnByInput(mileageCouponInfo.couponNo, couponNo));
        log.debug("selectCouponUsedCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectCouponManageInfoList(MileageCouponSearchSetDto setDto) {
        MileageCouponManageEntity mileageCouponManage = EntityFactory.createEntityIntoValue(MileageCouponManageEntity.class, Boolean.TRUE);
        MileageTypeEntity mileageType = EntityFactory.createEntityIntoValue(MileageTypeEntity.class, Boolean.TRUE);
        EmployeeInfoEntity employeeInfo = EntityFactory.createEntityIntoValue(EmployeeInfoEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                mileageCouponManage.couponManageNo,
                mileageCouponManage.couponNm,
                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(mileageCouponManage.issueStartDt, "' ~ '", mileageCouponManage.issueEndDt), "issue_dt"),
                mileageCouponManage.issueYn,
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(mileageCouponManage.couponType, "ONE"), "'일회성'", "'다회성'"), mileageCouponManage.couponType),
                mileageCouponManage.issueQty,
                mileageCouponManage.mileageAmt,
                mileageType.mileageTypeName,
                mileageCouponManage.regDt,
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(employeeInfo.name).FROM(EntityFactory.getTableNameWithAlias("dms", employeeInfo)).WHERE(SqlUtils.equalColumn(employeeInfo.empid, mileageCouponManage.regId))
                        ),
                        mileageCouponManage.regId
                    ),
                    mileageCouponManage.regId
                ),
                mileageCouponManage.chgDt,
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(employeeInfo.name).FROM(EntityFactory.getTableNameWithAlias("dms", employeeInfo)).WHERE(SqlUtils.equalColumn(employeeInfo.empid, mileageCouponManage.chgId))
                        ),
                        mileageCouponManage.chgId
                    ),
                    mileageCouponManage.chgId
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageCouponManage))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageType, ObjectUtils.toArray(mileageCouponManage.mileageTypeNo)))
            .WHERE(
                SqlUtils.customOperator(">=", mileageCouponManage.issueStartDt, SqlUtils.appendSingleQuote(setDto.getSchStartDt())),
                SqlUtils.customOperator("<=", mileageCouponManage.issueEndDt, SqlUtils.appendSingleQuote(setDto.getSchEndDt()))
            )
            .ORDER_BY(SqlUtils.orderByesc(mileageCouponManage.couponManageNo))
            ;
        if(!setDto.getSchCouponType().equals("ALL")){
            query.WHERE(
                SqlUtils.equalColumnByInput(mileageCouponManage.couponType, setDto.getSchCouponType())
            );
        }
        if(!setDto.getSchIssueYn().equals("ALL")){
            query.WHERE(SqlUtils.equalColumnByInput(mileageCouponManage.issueYn, setDto.getSchIssueYn()));
        }
        if(StringUtils.isNotEmpty(setDto.getSchDetailDesc())){
            if(setDto.getSchDetailCode().equals("MANAGE_NO")){
                query.WHERE(
                    SqlUtils.equalColumnByInput(mileageCouponManage.couponManageNo, setDto.getSchDetailDesc())
                );
            }
            if(setDto.getSchDetailCode().equals("COUPON_NM")) {
                query.WHERE(
                    SqlUtils.middleLike(mileageCouponManage.couponNm, setDto.getSchDetailDesc())
                );
            }
        }
        log.debug("selectCouponManageInfoList \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileageCouponManageInfo(@Param("couponManageNo") long couponManageNo) {
        MileageCouponManageEntity mileageCouponManage = EntityFactory.createEntityIntoValue(MileageCouponManageEntity.class, Boolean.TRUE);
        MileageTypeEntity mileageType = EntityFactory.createEntityIntoValue(MileageTypeEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                mileageCouponManage.couponManageNo,
                mileageCouponManage.mileageTypeNo,
                mileageType.mileageTypeName,
                mileageCouponManage.couponType,
                mileageCouponManage.couponNm,
                mileageCouponManage.issueStartDt,
                mileageCouponManage.issueEndDt,
                mileageCouponManage.issueQty,
                mileageCouponManage.mileageAmt,
                mileageCouponManage.personalIssueType,
                mileageCouponManage.personalIssueQty,
                mileageCouponManage.displayMessage,
                mileageCouponManage.couponNo,
                mileageCouponManage.issueYn,
                mileageCouponManage.useYn
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageCouponManage))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageType, ObjectUtils.toArray(mileageCouponManage.mileageTypeNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageCouponManage.couponManageNo, couponManageNo)
            );
        log.debug("selectMileageCouponManageInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileageCouponUserInfo(@Param("couponManageNo") long couponManageNo) {
        MileageCouponManageEntity mileageCouponManage = EntityFactory.createEntityIntoValue(MileageCouponManageEntity.class, Boolean.TRUE);
        MileageCouponInfoEntity mileageCouponInfo = EntityFactory.createEntityIntoValue(MileageCouponInfoEntity.class, Boolean.TRUE);
        EmployeeInfoEntity employeeInfo = EntityFactory.createEntityIntoValue(EmployeeInfoEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                mileageCouponInfo.couponInfoSeq,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(mileageCouponInfo.userNo, "'-'"), mileageCouponInfo.userNo),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(mileageCouponInfo.userNm, "'-'"), mileageCouponInfo.userNm),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(mileageCouponInfo.userId, "'-'"), mileageCouponInfo.userId),
                mileageCouponInfo.couponNo,
                mileageCouponInfo.issueDt,
                mileageCouponInfo.useDt,
                mileageCouponInfo.recoveryYn,
                SqlUtils.subTableQuery(
                    new SQL().SELECT(employeeInfo.name).FROM(EntityFactory.getTableNameWithAlias("dms", employeeInfo)).WHERE(SqlUtils.equalColumn(employeeInfo.empid, mileageCouponManage.regId)),
                    "reg_id"
                ),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(employeeInfo.name).FROM(EntityFactory.getTableNameWithAlias("dms", employeeInfo)).WHERE(SqlUtils.equalColumn(employeeInfo.empid, mileageCouponInfo.recoveryId)),
                    "recovery_id"
                ),
                mileageCouponInfo.recoveryDt
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageCouponInfo))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageCouponManage, ObjectUtils.toArray(mileageCouponInfo.couponManageNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageCouponManage.couponManageNo, couponManageNo)
            );
        log.debug("selectMileageCouponUserInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateMileageCouponInfoReturn(@Param("couponInfoSeq") long couponInfoSeq, @Param("requestId") String requestId, @Param("isComplete") boolean isComplete) {
        MileageCouponInfoEntity mileageCouponInfo = EntityFactory.createEntityIntoValue(MileageCouponInfoEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(mileageCouponInfo))
            .SET(
                SqlUtils.equalColumn(mileageCouponInfo.chgDt, "NOW()")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(mileageCouponInfo.couponInfoSeq, couponInfoSeq)
            );
        if(isComplete){
            query.SET(
                SqlUtils.equalColumnByInput(mileageCouponInfo.recoveryYn, "Y"),
                SqlUtils.equalColumn(mileageCouponInfo.recoveryDt, "NOW()"),
                SqlUtils.equalColumnByInput(mileageCouponInfo.recoveryId, requestId)
            ).WHERE(
                SqlUtils.equalColumnByInput(mileageCouponInfo.recoveryYn, "C")
            );
        } else {
            query.SET(
                SqlUtils.equalColumn(
                    mileageCouponInfo.recoveryYn,
                    SqlUtils.sqlFunction(
                        MysqlFunction.CASE,
                        ObjectUtils.toArray(
                            SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, mileageCouponInfo.userNo),
                            "'C'", "'Y'"
                        )
                    )
                )
            ).WHERE(
                SqlUtils.equalColumnByInput(mileageCouponInfo.recoveryYn, "N")
            );
        }
        log.debug("updateMileageCouponInfoReturn \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectCouponOneTimeInfo(@Param("couponManageNo") long couponManageNo){
        MileageCouponManageEntity mileageCouponManage = EntityFactory.createEntityIntoValue(MileageCouponManageEntity.class, Boolean.TRUE);
        MileageCouponInfoEntity mileageCouponInfo = EntityFactory.createEntityIntoValue(MileageCouponInfoEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                mileageCouponInfo.couponNo,
                SqlUtils.sqlFunction(MysqlFunction.CASE_Y_N, SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, mileageCouponInfo.useDt), "coupon_use_yn")
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageCouponManage))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageCouponInfo, ObjectUtils.toArray(mileageCouponManage.couponManageNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageCouponManage.couponManageNo, couponManageNo),
                SqlUtils.equalColumnByInput(mileageCouponManage.issueYn, "Y"),
                SqlUtils.equalColumnByInput(mileageCouponManage.couponType, "ONE")
            );
        log.debug("selectCouponOneTimeInfo \n[{}]", query.toString());
        return query.toString();
    }
}
