package kr.co.homeplus.mileage.office.model.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "마일리지 적립/회수 요청 DTO", value = "MileageRequestDto")
public class MileageRequestDto {

    @ApiModelProperty(value = "마일리지유형", position = 1)
    @NotNull(message = "마일리지 유형이 없습니다.")
    private String mileageTypeNo;

    @ApiModelProperty(value = "요청유형(01:지급, 02:회수)", position = 2)
    @NotNull(message = "요청유형이 없습니다.")
    private String requestType;

    @ApiModelProperty(value = "요청사유", position = 3)
    @Size(max = 50, message = "요청사유는 50자를 초과할 수 없습니다.")
    @NotNull(message = "요청사유가 없습니다.")
    private String requestReason;

    @ApiModelProperty(value = "고객노출문구", position = 4)
    @Size(max = 10, message = "고객노출문구는 10자를 초과할 수 없습니다.")
    @NotNull(message = "고객노출문구가 없습니다.")
    private String displayMessage;

    @ApiModelProperty(value = "요청상세사유", position = 5)
    @Size(max = 200, message = "요청사유는 200자를 초과할 수 없습니다.")
    private String requestMessage;

    @ApiModelProperty(value = "실행요청일", position = 6)
    @NotNull(message = "실행요청일이 없습니다.")
    private String processDt;

    @ApiModelProperty(value = "실행여부", position = 7)
    @NotNull(message = "실행여부")
    private String useYn;

    @ApiModelProperty(value = "즉시실행여부", position = 8)
    @NotNull(message = "즉시실행여부")
    private String directlyYn;

    @ApiModelProperty(value = "요청자", position = 9)
    private String regId;

    @ApiModelProperty(value = "거래번호(없을시에 미입력)", position = 10)
    @Length(max = 20, message = "20자까지 입력가능합니다.")
    private String tradeNo;

    @ApiModelProperty(value = "지정유효기간", position = 11, hidden = true)
    private int expireDay;

}
