package kr.co.homeplus.mileage.sql;

import java.util.Arrays;
import kr.co.homeplus.mileage.constants.CustomConstants;
import kr.co.homeplus.mileage.entity.MileageReqDetailEntity;
import kr.co.homeplus.mileage.entity.MileageReqEntity;
import kr.co.homeplus.mileage.enums.MysqlFunction;
import kr.co.homeplus.mileage.entity.EmployeeInfoEntity;
import kr.co.homeplus.mileage.entity.MileageHistoryEntity;
import kr.co.homeplus.mileage.entity.MileageManageEntity;
import kr.co.homeplus.mileage.entity.MileageSaveEntity;
import kr.co.homeplus.mileage.entity.MileageTypeEntity;
import kr.co.homeplus.mileage.entity.MileageUseEntity;
import kr.co.homeplus.mileage.management.model.user.MileageHistoryManageSetDto;
import kr.co.homeplus.mileage.office.model.history.MileageHistoryReqDto;
import kr.co.homeplus.mileage.utils.EntityFactory;
import kr.co.homeplus.mileage.utils.ObjectUtils;
import kr.co.homeplus.mileage.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MileageCommonSql {

    public static final String getDetailCode = " (SELECT CASE WHEN #{detailCode} IS NULL THEN "
        + "CASE WHEN "
        + "( "
        + "SELECT COUNT(1)"
        + "  FROM mileage_req_detail mrd"
        + " WHERE mrd.mileage_req_no = #{mileageReqNo}"
        + "   AND mrd.user_no = #{userNo}"
        + "   AND mrd.use_yn = 'Y'"
        + "   AND mrd.detail_code = '00'"
        + "   AND mrd.mileage_detail_status = 'D1'"
        + " ) > 0 "
        + " THEN \"01\" ELSE \"00\" END ELSE #{detailCode} END AS detailCode"
        + " )";

    /**
     * 마일리지 히스토리 INSERT 쿼리 생성.
     *
     * @param mileageHistoryManageSetDto 마일리지 히스토리 정보 DTO
     * @return 마일리지 히스토리 INSERT Query
     */
    public static String insertUserMileageHistory(MileageHistoryManageSetDto mileageHistoryManageSetDto){
        // 마일리지 히스토리 insert 에 사용할 컬럼정보.
        String[] historyColumn = EntityFactory.getColumnInfo(MileageHistoryEntity.class);

        SQL query = new SQL()
            // INSERT Table 설정.
            .INSERT_INTO(EntityFactory.getTableName(MileageHistoryEntity.class))
            // INSERT 할 컬럼 설정.
            // mileage_history_no 는 AUTO_INCREMENT 를 사용하여 처리하므로 INSERT 시 제외.
            .INTO_COLUMNS(Arrays.copyOfRange(historyColumn, 1, historyColumn.length))
            // INSERT 할 Value 설정.
            // INTO_COLUMN 에 대응할 수 있도록 mileage_history_no 제외.
            .INTO_VALUES(SqlUtils.convertInsertParamToCamelCase(Arrays.copyOfRange(historyColumn, 1, historyColumn.length)));

        log.debug("insertUserMileageHistory \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileageHistoryList(MileageHistoryReqDto reqDto) {
        MileageHistoryEntity mileageHistory = EntityFactory.createEntityIntoValue(MileageHistoryEntity.class, Boolean.TRUE);
        MileageSaveEntity mileageSave = EntityFactory.createEntityIntoValue(MileageSaveEntity.class, Boolean.TRUE);
        MileageUseEntity mileageUse = EntityFactory.createEntityIntoValue(MileageUseEntity.class, Boolean.TRUE);
        MileageManageEntity mileageManage = EntityFactory.createEntityIntoValue(MileageManageEntity.class, Boolean.TRUE);
        MileageTypeEntity mileageType = EntityFactory.createEntityIntoValue(MileageTypeEntity.class, Boolean.TRUE);
        EmployeeInfoEntity employeeInfo = EntityFactory.createEntityIntoValue(EmployeeInfoEntity.class, Boolean.TRUE);

        SQL saveQuery = new SQL()
            .SELECT_DISTINCT(
                SqlUtils.aliasToRow(mileageHistory.regDt, "act_dt"),
                mileageType.mileageTypeNo,
                mileageType.mileageTypeName,
                mileageHistory.mileageCategory,
                mileageHistory.mileageKind,
                mileageHistory.userNo,
                mileageSave.mileageAmt,
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(mileageHistory.mileageKind, "05"), "'FRONT'", mileageSave.chgId), mileageSave.chgId),
                mileageHistory.tradeNo,
                mileageSave.mileageSaveNo,
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(mileageSave.expireDt,CustomConstants.YYYYMMDD), mileageSave.expireDt)
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageHistory))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    mileageSave,
                    ObjectUtils.toArray(mileageHistory.mileageCategory, mileageHistory.mileageKind, mileageHistory.userNo),
                    ObjectUtils.toArray(SqlUtils.equalColumn(mileageHistory.mileageNo, mileageSave.mileageSaveNo))
                )
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageType, ObjectUtils.toArray(mileageSave.mileageTypeNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageHistory.mileageCategory, "01"),
                SqlUtils.betweenDay(mileageHistory.regDt, reqDto.getSchStartDt(), reqDto.getSchEndDt())
            );
        SQL useQuery = new SQL()
            .SELECT_DISTINCT(
                SqlUtils.aliasToRow(mileageHistory.regDt, "act_dt"),
                mileageType.mileageTypeNo,
                mileageType.mileageTypeName,
                mileageHistory.mileageCategory,
                mileageHistory.mileageKind,
                mileageHistory.userNo,
                mileageManage.mileageAmt,
                mileageUse.chgId,
                mileageHistory.tradeNo,
                mileageManage.mileageSaveNo,
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(mileageManage.expireDt,CustomConstants.YYYYMMDD), mileageManage.expireDt)
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageHistory))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    mileageUse,
                    ObjectUtils.toArray(mileageHistory.mileageCategory, mileageHistory.mileageKind, mileageHistory.userNo),
                    ObjectUtils.toArray(SqlUtils.equalColumn(mileageHistory.mileageNo, mileageUse.mileageUseNo))
                )
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageManage, ObjectUtils.toArray(mileageUse.mileageUseNo, mileageUse.userNo), ObjectUtils.toArray(SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, mileageManage.mileageAmt, "0"))))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageType, ObjectUtils.toArray(mileageManage.mileageTypeNo)))
            .WHERE(
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, mileageHistory.mileageCategory, ObjectUtils.toArray("'01'", "'03'")),
                SqlUtils.betweenDay(mileageHistory.regDt, reqDto.getSchStartDt(), reqDto.getSchEndDt())
            );
        SQL cancelQuery = new SQL()
            .SELECT_DISTINCT(
                SqlUtils.aliasToRow(mileageHistory.regDt, "act_dt"),
                mileageType.mileageTypeNo,
                mileageType.mileageTypeName,
                mileageHistory.mileageCategory,
                mileageHistory.mileageKind,
                mileageHistory.userNo,
                mileageHistory.mileageAmt,
                mileageManage.chgId,
                mileageHistory.tradeNo,
                mileageManage.mileageSaveNo,
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(mileageManage.expireDt,CustomConstants.YYYYMMDD), mileageManage.expireDt)
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageHistory))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    mileageManage,
                    ObjectUtils.toArray(mileageHistory.userNo),
                    ObjectUtils.toArray(SqlUtils.equalColumn(mileageHistory.mileageNo, mileageManage.mileageManageNo)))
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageType, ObjectUtils.toArray(mileageManage.mileageTypeNo)))
            .INNER_JOIN()
            .WHERE(
                SqlUtils.equalColumnByInput(mileageHistory.mileageCategory, "03"),
                SqlUtils.betweenDay(mileageHistory.regDt, reqDto.getSchStartDt(), reqDto.getSchEndDt())
            );

        if(reqDto.getUserNo() != 0){
            saveQuery.WHERE(SqlUtils.equalColumnByInput(mileageHistory.userNo, reqDto.getUserNo()));
            useQuery.WHERE(SqlUtils.equalColumnByInput(mileageHistory.userNo, reqDto.getUserNo()));
            cancelQuery.WHERE(SqlUtils.equalColumnByInput(mileageHistory.userNo, reqDto.getUserNo()));
        }

        String fromCondition;
        switch (reqDto.getMileageCategory()){
            case "01" :
                fromCondition = SqlUtils.subTableQuery(saveQuery, "data");
                break;
            case "03" :
                fromCondition = SqlUtils.subTableQuery(cancelQuery, "data");
                break;
            case "02" :
            case "04" :
            case "05" :
                useQuery.WHERE(
                    SqlUtils.equalColumnByInput(mileageHistory.mileageCategory, reqDto.getMileageCategory())
                );
                fromCondition = SqlUtils.subTableQuery(useQuery, "data");
                break;
            default:
                fromCondition =
                    "(\n"
                    .concat(saveQuery.toString())
                    .concat("\nUNION ALL\n")
                    .concat(useQuery.toString())
                    .concat("\nUNION ALL\n")
                    .concat(cancelQuery.toString())
                    .concat("\n) data");
                break;
        }
        SQL query = new SQL()
            .SELECT(
                "act_dt",
                SqlUtils.sqlFunction(
                    MysqlFunction.MILEAGE_UPPER_COMMON_TABLE,
                    ObjectUtils.toArray(
                        SqlUtils.nonAlias(mileageHistory.mileageKind), SqlUtils.nonAlias(mileageHistory.mileageKind),
                        SqlUtils.nonAlias(mileageHistory.mileageCategory), SqlUtils.nonAlias(mileageHistory.mileageCategory)
                    ),
                    mileageType.mileageKind
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.MILEAGE_COMMON_TABLE,
                    SqlUtils.nonAlias(mileageHistory.mileageCategory),
                    ObjectUtils.toArray(SqlUtils.nonAlias(mileageHistory.mileageCategory)),
                    mileageType.mileageCategory
                ),
                SqlUtils.nonAlias(mileageType.mileageTypeNo),
                SqlUtils.nonAlias(mileageType.mileageTypeName),
                SqlUtils.nonAlias(mileageHistory.userNo),
                SqlUtils.nonAlias(mileageHistory.mileageAmt),
                SqlUtils.caseWhenElse(
                    SqlUtils.nonAlias(mileageHistory.mileageCategory),
                    ObjectUtils.toArray(
                        "'02'", "'-'",
                        "'03'", "'FRONT'"
                    ),
                    SqlUtils.sqlFunction(
                        MysqlFunction.IFNULL,
                        ObjectUtils.toArray(
                            SqlUtils.subTableQuery(
                                new SQL()
                                    .SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(employeeInfo.name, SqlUtils.nonAlias(mileageSave.chgId))))
                                    .FROM(EntityFactory.getTableNameWithAlias("dms", employeeInfo))
                                    .WHERE(
                                        SqlUtils.equalColumn(employeeInfo.empid, SqlUtils.nonAlias(mileageSave.chgId))
                                    )
                            ),
                            "chg_id"
                        )
                    ),
                    "chg_id"
                ),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(mileageSave.requestReason)
                    .FROM(EntityFactory.getTableNameWithAlias(mileageSave))
                    .WHERE(
                        SqlUtils.equalColumn(mileageSave.mileageSaveNo, "data.mileage_save_no")
                    ),
                    "request_reason"
                ),
                SqlUtils.nonAlias(mileageManage.expireDt),
                SqlUtils.nonAlias(mileageHistory.tradeNo)
            )
            .FROM(fromCondition)
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.nonAlias(mileageHistory.mileageAmt), "0")
            )
            .ORDER_BY(SqlUtils.orderByesc("data.act_dt"));

        log.debug("selectMileageHistoryList Query :: \n [{}]", query.toString());
        return query.toString();

    }

}
