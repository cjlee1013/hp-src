package kr.co.homeplus.mileage.management.model.user;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserSearchHistoryResDto<T> {

    @ApiModelProperty(value = "고객번호", position = 1)
    @NotNull(message = "사용자번호를 입력해주세요.")
    private String userNo;

    //Default 1
    @ApiModelProperty(value = "요청 페이지", position = 2)
    private int totalPage;

    @ApiModelProperty(value = "현재 페이지", position = 3)
    private int nowPage;

    //Default 10
    @ApiModelProperty(value = "페이지당 View Count", position = 4)
    private int viewCount;

    @ApiModelProperty(value = "응답 데이터", position = 5)
    private T responseData;
}
