package kr.co.homeplus.mileage.entity;

public class MileageHistoryEntity {

    // 마일리지히스토리관리번호
    public String mileageHistoryNo;

    // 고객관리번호
    public String userNo;

    // 마일리지유형
    public String mileageCategory;

    // 마일리지종류
    public String mileageKind;

    // 마일리지 거래번호(적립시 마일리지적립번호, 사용시 마일리지사용번호)
    public String mileageNo;

    // 거래번호
    public String tradeNo;

    // 마일리지금액
    public String mileageAmt;

    // 히스토리메시지
    public String historyMessage;

    // 히스토리상세메시지
    public String historyEtcMessage;

    // 등록일
    public String regDt;
}
