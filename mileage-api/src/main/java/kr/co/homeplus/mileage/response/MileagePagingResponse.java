package kr.co.homeplus.mileage.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.mileage.utils.MileageCommonUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "마일리지 조회시 페이징용 응답 DTO", value = "MileagePagingResponse")
public class MileagePagingResponse<T> {

    //Default 1
    @ApiModelProperty(value = "요청 페이지", position = 1)
    private int totalPage;

    @ApiModelProperty(value = "현재 페이지", position = 2)
    private int nowPage;

    //Default 10
    @ApiModelProperty(value = "페이지당 View Count", position = 3)
    private int viewCount;

    //Default 10
    @ApiModelProperty(value = "데이터 전체 Count", position = 4)
    private int totalCount;

    @ApiModelProperty(value = "응답 데이터", position = 5)
    private T responseData;

    public void setTotalCount(int totalCount){
        this.totalCount = totalCount;
        this.totalPage = MileageCommonUtil.getCalculationPage(totalCount, this.viewCount);
    }

}
