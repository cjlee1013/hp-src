package kr.co.homeplus.mileage.management.service;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.mileage.core.exception.MileageLogicException;
import kr.co.homeplus.mileage.enums.ActivityMileageCode;
import kr.co.homeplus.mileage.enums.MileageSaveCode;
import kr.co.homeplus.mileage.management.model.use.MileageUseDto;
import kr.co.homeplus.mileage.management.model.save.SaveMileageDto;
import kr.co.homeplus.mileage.management.model.use.ExpireMileageSetDto;
import kr.co.homeplus.mileage.management.model.use.ReturnMileageSetDto;
import kr.co.homeplus.mileage.management.model.use.UseMileageSetDto;
import kr.co.homeplus.mileage.management.model.user.MileageMstDto;
import kr.co.homeplus.mileage.office.service.OfficeMapperManageService;
import kr.co.homeplus.mileage.utils.MileageCommonResponseFactory;
import kr.co.homeplus.mileage.utils.MileageCommonUtil;
import kr.co.homeplus.mileage.utils.ObjectUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 마일리지 사용 Service
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UseMileageService {
    // Management Mapper Service
    private final ManagementMapperService managementMapperService;
    private final OfficeMapperManageService officeMapperManageService;

    /**
     * 마일리지 사용
     * @param useMileageSetDto 마일리지 사용 DTO
     * @return ResponseObject<MileageMstDto>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    @Transactional(rollbackFor = {Exception.class, MileageLogicException.class})
    public ResponseObject<MileageMstDto> setMileageUse(UseMileageSetDto useMileageSetDto) throws Exception{

        // mileage_mst 회원 정보가 있는지 확인
        if(!managementMapperService.checkUserMileageMst(useMileageSetDto.getUserNo())){
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_USE_ERR_01);
        }

        //사용요청금액 확인을 위한 마일리지 마스터 정보 습득(회원번호)
        MileageMstDto mileageMstDto = managementMapperService.getUserMileageMstInfo(useMileageSetDto.getUserNo());

        // 사용자의 최종마일리지 금액 >= 사용요청금액 여부 확인
        if(mileageMstDto.getMileageAmt() < useMileageSetDto.getRequestUseAmt()){
            return MileageCommonResponseFactory.getResponseObject(ActivityMileageCode.MILEAGE_USE_ERR_02, mileageMstDto);
        }

        // 추가.
        // request_type 01 일 때 동일 trade_no 제어
//        if (useMileageSetDto.getRequestUseType().equals("01")){
//            if(managementMapperService.getMileageUsedCheck(useMileageSetDto.getUserNo(), Long.parseLong(useMileageSetDto.getTradeNo()))) {
//                cancel MileageCommonResponseFactory.getResponseObject(ActivityMileageCode.MILEAGE_USE_ERR_09);
//            }
//        }

        //mileage_use 에 사용내역 저장.
        //DTO 전환
        MileageUseDto mileageUseDto = MileageCommonUtil.makeUseMileageEntity(useMileageSetDto);
        //마일리지 사용 처리 만들기.
        if(!managementMapperService.addUserMileageUse(mileageUseDto)){
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_USE_ERR_03);
        }
        // 마일리지 사용처리.
        this.mileageUsableProcess(mileageUseDto);
        // 마일리지 마스터 DB에 마일리지 원복
        //사용처리를 위해 최종금액만 -1 처리
        if (!managementMapperService.modifyUserMileageMst(useMileageSetDto.getUserNo(), (useMileageSetDto.getRequestUseAmt() * -1))) {
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_USE_ERR_05);
        }

        // 마일리지 히스토리에 저장.
        if(!managementMapperService.addUserMileageHistory(MileageCommonUtil.initMileageHistorySetDto(mileageUseDto))){
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_USE_ERR_06);
        }

        // 응답시 승인번호 추가
        MileageMstDto responseDto = managementMapperService.getUserMileageMstInfoFromWrite(useMileageSetDto.getUserNo());
        // 사용시 생성된 마일리지 사용 관리 번호를 승인번호로 처리.
        responseDto.setApprovalNo(mileageUseDto.getMileageUseNo());

        return MileageCommonResponseFactory.getResponseObject(ActivityMileageCode.MILEAGE_USE_SUCCESS, responseDto);
    }

    /**
     * 마일리지 회수
     * @param returnMileageSetDto 마일리지 회수 DTO
     * @return ResponseObject<MileageMstDto>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    @Transactional(rollbackFor = {Exception.class, MileageLogicException.class}, propagation = Propagation.NESTED)
    public String setMileageReturn(ReturnMileageSetDto returnMileageSetDto) throws Exception{
        // 회수결과정보
        String returnCode = "00";
        //1. point_mst 에 회원 정보가 있는지 확인
        if(!managementMapperService.checkUserMileageMst(returnMileageSetDto.getUserNo())){
            //회원없음으로 리턴.
            return "02";
        }
        //사용요청금액 확인을 위한 마일리지 마스터 정보 습득(회원번호)
        MileageMstDto mileageMstDto = managementMapperService.getUserMileageMstInfo(returnMileageSetDto.getUserNo());
        // 사용자의 최종마일리지 금액 >= 사용요청금액 여부 확인
        // 회수는 고객의 마일리지에 대한 차감을 목적이므로, 사용자의 금액이 요청금액보다 작을 경우에는 회수요청에 오류업데이트
        // 로직상에 오류없이 고객의 현재 최종금액을 사용처리 금액으로 전환하여 프로세스를 진행한다.
        if(mileageMstDto.getMileageAmt() < returnMileageSetDto.getRequestReturnAmt()){
            // 금액 치환.
            returnMileageSetDto.setRequestReturnAmt(mileageMstDto.getMileageAmt());
            // 회수금액부족
            if(mileageMstDto.getMileageAmt() == 0){
                //금액없음으로 리턴.
                return "03";
            }
            returnCode = "06";
        }
        //mileage_use 에 회수로 사용기록.
        //마일리지 사용 Entity 생성.
        MileageUseDto mileageUseDto = MileageCommonUtil.makeUseMileageEntity(returnMileageSetDto);
        log.info("mileageUsableProcess1 {}", mileageUseDto);
        //마일리지 사용 처리 만들기.
        if(!managementMapperService.addUserMileageUse(mileageUseDto)){
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_USE_ERR_03);
        }
        log.info("mileageUsableProcess1 {}", mileageUseDto);
        // 원회수요청금액
        long mileageReturnAmt = returnMileageSetDto.getRequestReturnAmt();
        // 회수요청금액(처리용)
        long mileageUseRemainAmt = mileageReturnAmt;

        if("03,04,05".contains(returnMileageSetDto.getRequestReturnType())){
            // 마일리지 페이백, 주문적립 해당 적립에 해당하는 것부터 처리하도록 수정 필요.
            mileageUseRemainAmt = this.mileageSetReturnProcess(mileageUseDto, returnMileageSetDto.getRequestReturnType());
        }

        if(mileageUseRemainAmt > 0 ){
            mileageUseDto.setMileageAmt(mileageUseRemainAmt);
            //마일리지 사용처리.
            this.mileageUsableProcess(mileageUseDto);
            log.info("mileageUsableProcess2 {}", mileageUseDto);
        }
        // 회수금액 재설정.
        mileageUseDto.setMileageAmt(mileageReturnAmt);
        // 마일리지 마스터 DB에 마일리지 원복
        // 사용처리를 위해 최종금액만 -1 처리
        if (!managementMapperService.modifyUserMileageMst(mileageUseDto.getUserNo(), (mileageUseDto.getMileageAmt() * -1))) {
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_USE_ERR_05);
        }
        // mileage_history 에 해당 기록 적재.
        if(!managementMapperService.addUserMileageHistory(MileageCommonUtil.initMileageHistorySetDto(mileageUseDto))){
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_USE_ERR_06);
        }

        return returnCode;
    }

    /**
     * 마일리지 사용처리.
     * @param mileageUseDto 마일리지 사용 Entity
     * @throws Exception 오류 발생시 오류 처리. Exception
     */
    private long mileageUsableProcess(MileageUseDto mileageUseDto) throws Exception{
        // 사용요청처리한 금액에 해당하는 금액 건수만큼 적립내역 취득
        List<SaveMileageDto> pointUsableList = managementMapperService.getUserMileageUsableList(mileageUseDto.getUserNo(), mileageUseDto.getMileageAmt());

        if(pointUsableList.size() == 0){
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_USE_ERR_08);
        }
        // 마일리지 개별 사용처리.
        return this.mileageUsedProcess(mileageUseDto, pointUsableList);
    }

    /**
     * 마일리지 적립 지정 취소 프로세스
     * (주문적립, 페이백, 쿠폰 처리)
     */
    private long mileageSetReturnProcess(MileageUseDto mileageUseDto, String type) throws Exception {
        List<SaveMileageDto> saveInfoList = this.getMileageSaveReturnInfo(mileageUseDto, type);
        if(saveInfoList.size() == 0){
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_USE_ERR_08);
        }
        List<SaveMileageDto> convertList = saveInfoList.stream().filter(dto -> dto.getMileageRemainAmt() > 0L).collect(Collectors.toList());
        if(convertList.size() == 0){
            return mileageUseDto.getMileageAmt();
        } else {
            // 마일리지 개별 사용처리.
            return this.mileageUsedProcess(mileageUseDto, convertList);
        }
    }

    private long mileageUsedProcess(MileageUseDto mileageUseDto, List<SaveMileageDto> pointUsableList) throws Exception {
        //사용할 금액 처리 변수
        long recalculationReqAmt = mileageUseDto.getMileageAmt();

        // 마일리지 적립 mileage_remain_amt 사용처리(loop)
        for (SaveMileageDto subtractInfoDto : pointUsableList) {
            //변경아이디 설정
            subtractInfoDto.setChgId(mileageUseDto.getChgId());

            //적립테이블 마일리지 사용처리 업데이트
            if(recalculationReqAmt < subtractInfoDto.getMileageRemainAmt()){
                subtractInfoDto.setMileageRemainAmt(recalculationReqAmt);
            }

            //마일리지 관리 정보 저장.
            if(!managementMapperService.addMileageManageInfo(MileageCommonUtil.makeMileageManageEntity(mileageUseDto, subtractInfoDto))){
                throw new MileageLogicException(ActivityMileageCode.MILEAGE_USE_ERR_07);
            }

            // 금액 사용 처리
            if (!managementMapperService.modifyMileageSaveInfo(subtractInfoDto)) {
                throw new MileageLogicException(ActivityMileageCode.MILEAGE_USE_ERR_04);
            }

            if(StringUtils.isNotEmpty(subtractInfoDto.getCouponInfoSeq())){
                officeMapperManageService.modifyMileageCouponReturn(Long.parseLong(subtractInfoDto.getCouponInfoSeq()), subtractInfoDto.getChgId(), Boolean.TRUE);
            }

            // 사용해야할 금액
            recalculationReqAmt -= subtractInfoDto.getMileageRemainAmt();
        }

        return recalculationReqAmt;
    }

    private List<SaveMileageDto> getMileageSaveReturnInfo(MileageUseDto mileageUseDto, String type) {
        if(type.equalsIgnoreCase("05")){
            return managementMapperService.getMileageCouponReturnInfo(mileageUseDto.getTradeNo());
        } else {
            return managementMapperService.getMileageSaveReturnInfo(mileageUseDto.getTradeNo(), getMileageCodeInfo(type));
        }

    }

    private String getMileageCodeInfo(String type) {
        return Arrays.stream(MileageSaveCode.values()).filter(code -> code.getMileageCodeType().equalsIgnoreCase(type)).map(MileageSaveCode::name).collect(Collectors.joining(","));
    }
}



