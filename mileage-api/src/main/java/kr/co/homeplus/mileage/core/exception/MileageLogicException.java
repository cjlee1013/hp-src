package kr.co.homeplus.mileage.core.exception;

import kr.co.homeplus.mileage.enums.impl.EnumImpl;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

@Data
@EqualsAndHashCode(callSuper = false)
public class MileageLogicException extends Exception{

    private String responseCode;
    private String responseMsg;
    private String logMsg;
    private Object data;

    public MileageLogicException(EnumImpl enumCode){
        this.responseCode = enumCode.getResponseCode();
        this.responseMsg = enumCode.getResponseMessage();
        this.logMsg = super.getMessage();
    }

    public MileageLogicException(EnumImpl enumCode, String logMsg){
        this.responseCode = enumCode.getResponseCode();
        this.responseMsg = enumCode.getResponseMessage();
        this.logMsg = logMsg;
    }

    public MileageLogicException(EnumImpl enumCode, String errMsg, Exception e){
        this.responseCode = enumCode.getResponseCode();
        this.logMsg = e.getMessage();
        this.responseMsg = errMsg;
    }

    public MileageLogicException(EnumImpl enumCode, String replaceCode, String replaceStr){
        this.responseCode = enumCode.getResponseCode();
        this.responseMsg = StringUtils.replace(enumCode.getResponseMessage(), replaceCode, replaceStr);
        this.logMsg = StringUtils.replace(enumCode.getResponseMessage(), replaceCode, replaceStr);
    }
    public MileageLogicException(EnumImpl enumCode, Object data, String returnMessage, boolean isConvert){
        this.responseCode = enumCode.getResponseCode();
        this.responseMsg = (isConvert ? returnMessage : enumCode.getResponseMessage());
        this.data = data;
    }

}
