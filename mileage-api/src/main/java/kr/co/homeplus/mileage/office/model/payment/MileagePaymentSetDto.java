package kr.co.homeplus.mileage.office.model.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "마일리지 적립/회수 요청")
public class MileagePaymentSetDto {

    @ApiModelProperty(value = "마일리지요청(기본정보)", position = 1)
    private MileageRequestDto mileageRequestDto;

    @ApiModelProperty(value = "마일리지요청상세정보", position = 2, dataType = "List")
    @NotNull(message = "마일리지 적립/회수 대상회원")
    private List<MileageReqDetailDto> mileageReqDetailList;

}
