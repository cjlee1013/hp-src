package kr.co.homeplus.mileage.entity;

/**
 * 마일리지 관리 Entity
 */
public class MileageManageEntity {

    //마일리지관리번호
    public String mileageManageNo;

    //마일리지유형번호
    public String mileageTypeNo;

    //유저번호
    public String userNo;

    //마일리지사용번호
    public String mileageUseNo;

    //마일리지적립번호
    public String mileageSaveNo;

    //마일리지금액
    public String mileageAmt;

    //취소가능금액
    public String cancelRemainAmt;

    //유효년월
    public String expireDt;

    //취소여부
    public String cancelYn;

    //등록일
    public String regDt;

    //등록아이디
    public String regId;

    //수정일
    public String chgDt;

    //수정아이디
    public String chgId;

}
