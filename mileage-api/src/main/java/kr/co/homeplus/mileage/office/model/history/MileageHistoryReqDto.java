package kr.co.homeplus.mileage.office.model.history;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
@ApiModel(description = "마일리지 관리 > 마일리지 내역 조회 요청")
public class MileageHistoryReqDto {
    @ApiModelProperty(value = "검색 작일", position = 1)
    private String schStartDt;
    @ApiModelProperty(value = "검색종료일", position = 2)
    private String schEndDt;
    @ApiModelProperty(value = "마일리지구분", position = 3)
    private String mileageCategory;
    @ApiModelProperty(value = "회원번호", position = 4)
    @DecimalMin(value = "1", message = "회원번호")
    private long userNo;
}
