package kr.co.homeplus.mileage.external.service;

import java.util.List;
import kr.co.homeplus.mileage.external.model.ExternalMyPageGetDto;
import kr.co.homeplus.mileage.external.model.ExternalMyPageSetDto;

public interface ExternalMapperService {

    String getMileageTypeInfo(String mileageCategory, String mileageKind);

    String getMileageCodeTypeInfo(String mileageCode);

    List<ExternalMyPageGetDto> getExternalMileageList(ExternalMyPageSetDto externalMyPageSetDto);

    boolean getAlreadySaveCheck(String mileageCode, String tradeNo, String userNo);

}
