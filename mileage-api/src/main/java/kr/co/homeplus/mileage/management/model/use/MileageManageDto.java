package kr.co.homeplus.mileage.management.model.use;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "마일리지 관리(사용) Entity")
public class MileageManageDto {

    @ApiModelProperty(value = "마일리지관리번호", position = 1)
    private String mileageManageNo;

    @ApiModelProperty(value = "마일리지유형번호", position = 2)
    private long mileageTypeNo;

    @ApiModelProperty(value = "유저번호", position = 3)
    private String userNo;

    @ApiModelProperty(value = "마일리지사용번호", position = 4)
    private long mileageUseNo;

    @ApiModelProperty(value = "마일리지적립번호", position = 5)
    private long mileageSaveNo;

    @ApiModelProperty(value = "마일리지금액", position = 6)
    private long mileageAmt;

    @ApiModelProperty(value = "취소가능금액", position = 7)
    private long cancelRemainAmt;

    @ApiModelProperty(value = "유효년월", position = 8)
    private String expireDt;

    @ApiModelProperty(value = "취소여부", position = 9)
    private String cancelYn;

    @ApiModelProperty(value = "등록일", position = 10)
    private String regDt;

    @ApiModelProperty(value = "등록아이디", position = 11)
    private String regId;

    @ApiModelProperty(value = "수정일", position = 12)
    private String chgDt;

    @ApiModelProperty(value = "수정아이디", position = 13)
    private String chgId;

}
