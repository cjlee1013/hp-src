package kr.co.homeplus.mileage.office.model.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "마일리지 적립/회수 Entity")
public class MileageReqDto {

    @ApiModelProperty(value = "마일리지요청번호", position = 1)
    private String mileageReqNo;

    @ApiModelProperty(value = "마일리지유형번호", position = 2)
    private String mileageTypeNo;

    @ApiModelProperty(value = "요청유형", position = 3)
    private String requestType;

    @ApiModelProperty(value = "거래번호(존재시에만)", position = 4)
    @Length(max = 20, message = "20자까지 입력가능합니다.")
    private String tradeNo;

    @ApiModelProperty(value = "요청대상건수", position = 5)
    private String requestCnt;

    @ApiModelProperty(value = "요청대상금액", position = 6)
    private String requestAmt;

    @ApiModelProperty(value = "요청상태", position = 7)
    private String requestStatus;

    @ApiModelProperty(value = "요청사유", position = 8)
    private String requestReason;

    @ApiModelProperty(value = "고객노출문구", position = 9)
    private String displayMessage;

    @ApiModelProperty(value = "요청메시지(상세사유)", position = 10)
    private String requestMessage;

    @ApiModelProperty(value = "사용여부", position = 11)
    private String useYn;

    @ApiModelProperty(value = "즉시여부", position = 12)
    private String directlyYn;

    @ApiModelProperty(value = "실행예일", position = 13)
    private String processDt;

    @ApiModelProperty(value = "처리완료", position = 14)
    private String completeDt;

    @ApiModelProperty(value = "등록일", position = 15)
    private String regDt;

    @ApiModelProperty(value = "등록아이디", position = 16)
    private String regId;

    @ApiModelProperty(value = "수정일", position = 17)
    private String chgDt;

    @ApiModelProperty(value = "수정아이디", position = 18)
    private String chgId;

    @ApiModelProperty(value = "마일리지유형명", position = 19)
    private String mileageTypeName;

}
