package kr.co.homeplus.mileage.office.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.mileage.office.model.type.MileagePromoTypeGetDto;
import kr.co.homeplus.mileage.office.model.type.MileagePromoTypeSetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeManageModifyDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeManageSetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeSearchGetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeSearchSetDto;
import kr.co.homeplus.mileage.office.service.MileageTypeService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/office/type")
@Api(tags = "MileageTypeController", description = "BackOffice 마일리지 적립 타입 관리 Controller")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
}
)
@RequiredArgsConstructor
public class MileageTypeController {

    private final MileageTypeService mileageTypeService;

    @ApiOperation(value = "마일리지 타입 등록 요청", response = ResponseObject.class)
    @PostMapping("/setMileageTypeInfo")
    public ResponseObject<String> setMileageTypeInfo(@RequestBody @Valid MileageTypeManageSetDto mileageTypeManageSetDto) throws Exception{
        return mileageTypeService.setMileageTypeManagementInfo(mileageTypeManageSetDto);
    }

    @ApiOperation(value = "마일리지 타입 수정 요청", response = ResponseObject.class)
    @PostMapping("/modifyMileageTypeInfo")
    public ResponseObject<String> modifyMileageTypInfo(@RequestBody @Valid MileageTypeManageModifyDto mileageTypeManageModifyDto) throws Exception{
        return mileageTypeService.modifyMileageTypeManagementInfo(mileageTypeManageModifyDto);
    }

    @ApiOperation(value = "마일리지 타입 조회 요청", response = MileageTypeSearchGetDto.class)
    @PostMapping("/getMileageTypeInfo")
    public ResponseObject<List<MileageTypeSearchGetDto>> getMileageTypInfo(@RequestBody @Valid MileageTypeSearchSetDto mileageTypeSearchSetDto) throws Exception{
        return mileageTypeService.getMileageTypeManagementInfo(mileageTypeSearchSetDto);
    }

    @ApiOperation(value = "마일리지 타입 기본정보 조회", response = MileageTypeManageModifyDto.class)
    @PostMapping("/getMileageTypeInfo/basic/{mileageTypeNo}")
    public ResponseObject<MileageTypeManageModifyDto> getSearchMileageTypeBasicInfo(@PathVariable(value = "mileageTypeNo") String mileageTypeNo) throws Exception{
        return mileageTypeService.getMileageTypeManagementBasicInfo(mileageTypeNo);
    }

    @ApiOperation(value = "마일리지(행사관리용) 타입 조회 요청", response = MileagePromoTypeGetDto.class)
    @PostMapping("/getMileagePromoTypeInfo")
    public ResponseObject<List<MileagePromoTypeGetDto>> getMileagePromotionTypeInfo(@RequestBody @Valid MileagePromoTypeSetDto setDto) throws Exception{
        return mileageTypeService.getMileagePromotionTypeInfo(setDto);
    }
}
