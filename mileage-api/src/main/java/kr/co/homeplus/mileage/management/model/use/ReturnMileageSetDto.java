package kr.co.homeplus.mileage.management.model.use;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
@ApiModel(description = "마일리지 회수 요청 DTO", value = "ReturnMileageSetDto")
public class ReturnMileageSetDto {

    @ApiModelProperty(value = "고객번호", position = 1)
    @NotNull(message = "고객번호")
    private String userNo;

    @ApiModelProperty(value = "마일리지회수구분", position = 2)
    @Pattern(regexp = "01|02|03|04|05", message = "회수구분")
    private String requestReturnType;

    @ApiModelProperty(value = "거래번호(회수)", position = 3)
    @NotNull(message = "거래번호")
    @Length(max = 20, message = "20자까지 입력가능합니다.")
    private String tradeNo;

    @ApiModelProperty(value = "회수요청금액", position = 4)
    private long requestReturnAmt;

    @ApiModelProperty(value = "회수내역 상세메시지", position = 5)
    private String requestReturnMessage;

    @ApiModelProperty(value = "등록아이디", position = 6)
    private String regId;

    @ApiModelProperty(value = "등록아이디", position = 7, hidden = true)
    private String mileageReqDetailNo;

}
