package kr.co.homeplus.mileage.enums.impl;

public interface EnumImpl {
    String getResponseCode();
    String getResponseMessage();
}
