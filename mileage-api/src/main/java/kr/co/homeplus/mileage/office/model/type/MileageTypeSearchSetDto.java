package kr.co.homeplus.mileage.office.model.type;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MileageTypeSearchSetDto {

    @ApiModelProperty(value = "점포유형", position = 1)
    @NotNull(message = "점포유형이 없습니다.")
    private String storeType;

    @ApiModelProperty(value = "마일리지종류", position = 2)
    @NotNull(message = "마일리지종류가 없습니다.")
    private String mileageKind;

    @ApiModelProperty(value = "사용유무", position = 3)
    @NotNull(message = "사용유무가 없습니다.")
    private String useYn;

    @ApiModelProperty(value = "상세검색", position = 4)
    @NotNull(message = "상세검색가 없습니다.")
    private String schDetailCode;

    @ApiModelProperty(value = "상세검색내용", position = 5)
    private String schDetailDesc;

}
