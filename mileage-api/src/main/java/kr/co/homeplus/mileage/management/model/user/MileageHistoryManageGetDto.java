package kr.co.homeplus.mileage.management.model.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class MileageHistoryManageGetDto {

    @ApiModelProperty(value = "고객번호", position = 1)
    private String userNo;

    @ApiModelProperty(value = "사용/적립금액", position = 2)
    private long mileageAmount;

    @ApiModelProperty(value = "사용/적립코드", position = 3)
    private String mileageCode;

    @ApiModelProperty(value = "거래번호", position = 4)
    @Length(max = 20, message = "20자까지 입력가능합니다.")
    private String tradeNo;

    @ApiModelProperty(value = "사용/적립내역 메시지", position = 5)
    private String mileageMsg;

    @ApiModelProperty(value = "기타메시지", position = 6)
    private String message;

    @ApiModelProperty(value = "등록일", position = 7)
    private String regDt;

}
