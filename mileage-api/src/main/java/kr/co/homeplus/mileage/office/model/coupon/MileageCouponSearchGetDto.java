package kr.co.homeplus.mileage.office.model.coupon;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MileageCouponSearchGetDto {
    @ApiModelProperty(value = "번호", position = 1)
    private String couponManageNo;

    @ApiModelProperty(value = "제목", position = 2)
    private String couponNm;

    @ApiModelProperty(value = "난수 발급기간", position = 3)
    private String issueDt;

    @ApiModelProperty(value = "실행", position = 4)
    private String issueYn;

    @ApiModelProperty(value = "난수종류", position = 5)
    private String couponType;

    @ApiModelProperty(value = "발급수량", position = 6)
    private String issueQty;

    @ApiModelProperty(value = "지급마일리지", position = 7)
    private String mileageAmt;

    @ApiModelProperty(value = "마일리지 유형", position = 8)
    private String mileageTypeName;

    @ApiModelProperty(value = "등록일", position = 9)
    private String regDt;

    @ApiModelProperty(value = "등록자", position = 10)
    private String regId;

    @ApiModelProperty(value = "수정일", position = 11)
    private String chgDt;

    @ApiModelProperty(value = "수정자", position = 12)
    private String chgId;
}
