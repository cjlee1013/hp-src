package kr.co.homeplus.mileage.office.model.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마일리지 지급/회수 관리 > 지급/회수 응답 정보", value = "MileageRequestDetailGetDto")
public class MileagePaymentGetDto {

    @ApiModelProperty(value = "마일리지 대상회원 등록", position = 1)
    public MileagePaymentReqDto register;

    @ApiModelProperty(value = "마일리지 적립", position = 2)
    public MileagePaymentDto payment;

    @ApiModelProperty(value = "마일리지요청번호", position = 3)
    private String mileageReqNo;

    public MileagePaymentGetDto(){
        this.register = new MileagePaymentReqDto();
        this.payment = new MileagePaymentDto();
    }

    @Data
    public static class MileagePaymentReqDto {

        @ApiModelProperty(value = "마일리지 대상회원 등록 성공", position = 1)
        public SuccessDto success;

        @ApiModelProperty(value = "마일리지 대상회원 등록 실패", position = 2)
        public FailDto fail;

        public MileagePaymentReqDto(){
            this.success = new SuccessDto();
            this.fail = new FailDto();
        }
    }

    @Data
    public static class MileagePaymentDto {

        @ApiModelProperty(value = "마일리지 적립/회수 성공", position = 1)
        public SuccessDto success;

        @ApiModelProperty(value = "마일리지 적립/회수 실패", position = 2)
        public FailDto fail;

        public MileagePaymentDto(){
            this.success = new SuccessDto();
            this.fail = new FailDto();
        }
    }

    @Data
    public static class SuccessDto {
        private int successCnt;
        private int successAmt;

        public SuccessDto(){
            this.successCnt = 0;
            this.successAmt = 0;
        }

        public void totalAddByData(int amt){
            this.successCnt++;
            this.successAmt += amt;
        }

        public void addCnt(){
            this.successCnt++;
        }

        public void addAmt(int amt){
            this.successAmt += amt;
        }
    }

    @Data
    public static class FailDto{
        private int failCnt;
        private int failAmt;

        public FailDto(){
            this.failCnt = 0;
            this.failAmt = 0;
        }

        public void totalAddByData(int amt){
            this.failCnt++;
            this.failAmt += amt;
        }

        public void addCnt(){
            this.failCnt++;
        }

        public void addAmt(int amt){
            this.failAmt += amt;
        }
    }

}
