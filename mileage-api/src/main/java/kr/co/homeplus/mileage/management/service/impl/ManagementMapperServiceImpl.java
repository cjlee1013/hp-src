package kr.co.homeplus.mileage.management.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.mileage.enums.MileageSaveCode;
import kr.co.homeplus.mileage.management.mapper.ManagementReadMapper;
import kr.co.homeplus.mileage.management.mapper.ManagementWriteMapper;
import kr.co.homeplus.mileage.management.model.use.MileageUseDto;
import kr.co.homeplus.mileage.management.model.cancel.CancelSaveDto;
import kr.co.homeplus.mileage.management.model.save.SaveMileageDto;
import kr.co.homeplus.mileage.management.model.use.MileageManageDto;
import kr.co.homeplus.mileage.management.model.user.MileageHistoryManageGetDto;
import kr.co.homeplus.mileage.management.model.user.MileageHistoryManageSetDto;
import kr.co.homeplus.mileage.management.model.user.MileageMstDto;
import kr.co.homeplus.mileage.management.model.user.UserSearchHistoryReqDto;
import kr.co.homeplus.mileage.management.service.ManagementMapperService;
import kr.co.homeplus.mileage.utils.MileageCommonUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ManagementMapperServiceImpl implements ManagementMapperService {
    // Read Only - SlaveConnection Mapper
    private final ManagementWriteMapper writeMapper;
    // write/read Possible - MasterConnection Mapper
    private final ManagementReadMapper readMapper;

    //============================================================================================================
    // 마일리지 관리 - 유저 관련 Method
    //============================================================================================================
    /**
     * 마일리지 마스터 회원존재 여부 체크(회원번호 기준)
     *
     * @param userNo 회원관리번호
     * @return Boolean 존재여부
     */
    @Override
    public boolean checkUserMileageMst(String userNo)  {
        return MileageCommonUtil.isGreaterThanZero(readMapper.selectUserMileageMstCheck(userNo));
    }

    /**
     * 마일리지 회원 마스터 추가(계정생성)
     * @param userNo 회원관리번호
     * @return Boolean 존재여부
     */
    @Override
    public boolean addUserMileageAccount(String userNo) {
        return MileageCommonUtil.isGreaterThanZero(writeMapper.insertUserMileageAccount(userNo));
    }

    /**
     * 마일리지 마스터 회원정보
     *
     * @param userNo 회원관리번호
     * @return MileageMstDto 회원 Dto
     */
    @Override
    public MileageMstDto getUserMileageMstInfo(String userNo) {
        return readMapper.selectUserMileageMstInfo(userNo);
    }

    /**
     * 마일리지 마스터 회원정보
     *
     * @param userNo 회원관리번호
     * @return MileageMstDto 회원 Dto
     */
    @Override
    public MileageMstDto getUserMileageMstInfoFromWrite(String userNo) {
        return writeMapper.selectUserMileageMstInfo(userNo);
    }

    /**
     * 마일리지 적립시 mst 금액 업데이트
     *
     * @param userNo 회원관리번호
     * @param mileageAmt 마일리지금액
     * @return Boolean 업데이트 여부
     */
    @Override
    public Boolean modifyUserMileageMst(String userNo, long mileageAmt) {
        return MileageCommonUtil.isGreaterThanZero(writeMapper.updateUserMileageMst(userNo, mileageAmt));
    }

    /**
     * 마일리지 히스토리 조회
     *
     * @param userSearchHistoryReqDto 유저 마일리지 히스토리 조회 DTO
     * @return List<MileageHistoryManageGetDto> 마일리지 히스토리 조회 응답 DTO
     */
    @Override
    public List<MileageHistoryManageGetDto> getUserSearchHistoryList(
        UserSearchHistoryReqDto userSearchHistoryReqDto) {
        //offset 설정.
        userSearchHistoryReqDto.setPage(MileageCommonUtil.getPageOffset(userSearchHistoryReqDto.getPage(), userSearchHistoryReqDto.getViewCount()));
        return readMapper.selectUserMileageHistoryInfo(userSearchHistoryReqDto);
    }

    /**
     * 마일리지 히스토리 건수
     *
     * @param userSearchHistoryReqDto 유저 마일리지 히스토리 조회 DTO
     * @return int 전체 건수
     */
    @Override
    public int getUserMileageHistoryCount(UserSearchHistoryReqDto userSearchHistoryReqDto){
        return readMapper.selectUserMileageHistoryCount(userSearchHistoryReqDto);
    }

    /**
     * 마일리지 히스토리 등록
     *
     * @param mileageHistoryManageSetDto 마일리지 히스토리 등록 DTO
     * @return Boolean 등록여부
     */
    @Override
    public Boolean addUserMileageHistory(MileageHistoryManageSetDto mileageHistoryManageSetDto){
        return MileageCommonUtil.isGreaterThanZero(writeMapper.insertUserMileageHistory(mileageHistoryManageSetDto));
    }
    //============================================================================================================

    //============================================================================================================
    // 마일리지 관리 - 적립 관련 Method
    //============================================================================================================
    /**
     * 마일리지 Save DB 적재
     *
     * @param saveMileageSetDto 마일리지 적립 Dto
     * @return boolean 저장성공여부
     */
    @Override
    public Boolean addUserMileageSave(SaveMileageDto saveMileageSetDto) {
        return MileageCommonUtil.isGreaterThanZero(writeMapper.insertUserMileageSave(saveMileageSetDto));
    }

    /**
     * 마일리지 적립 수정 - 사용처리.
     *
     * @param saveMileageDto 마일리지 적립 DTO
     * @return Boolean 수정성공여부
     */
    @Override
    public Boolean modifyMileageSaveInfo(SaveMileageDto saveMileageDto){
        return MileageCommonUtil.isGreaterThanZero(writeMapper.updateUserMileageSaveUsed(saveMileageDto));
    }
    //============================================================================================================
    
    //============================================================================================================
    // 마일리지 관리 - 사용 관련 Method
    //============================================================================================================
    /**
     * 마일리지 사용 처리(mileage_use)
     *
     * @param mileageUseDto 마일리지 사용 DB Entity
     * @return Boolean 등록 성공/실패
     */
    @Override
    public boolean addUserMileageUse(MileageUseDto mileageUseDto) {
        return MileageCommonUtil.isGreaterThanZero(writeMapper.insertUserMileageUse(mileageUseDto));
    }

    /**
     * 요청한 마일리지만큼 마일리지 적재에서 조회
     * (SP를 활용하여 요청 금액 만큼 사용가능한 적립건을 리턴한다.)
     *
     * @param userNo 회원관리번호
     * @param reqAmt 사용 요청금액
     * @return List<SaveMileageDto>
     */
    @Override
    public List<SaveMileageDto> getUserMileageUsableList(String userNo, long reqAmt) {
        return readMapper.selectUserMileageUsableList(userNo, reqAmt);
    }

    /**
     * 마일리지 관리 데이터 저장
     * (마일리지 사용을 위한 적재건들 저장)
     *
     * @param mileageManageDto 마일리지 관리 DB Entity
     * @return Boolean 저장 성공/실패
     */
    @Override
    public Boolean addMileageManageInfo(MileageManageDto mileageManageDto) {
        return MileageCommonUtil.isGreaterThanZero(writeMapper.insertMileageManageInfo(mileageManageDto));
    }

    @Override
    public List<SaveMileageDto> getMileageSaveReturnInfo(String tradeNo, String mileageCode) {
        return readMapper.selectMileageSaveReturnInfo(tradeNo, mileageCode);
    }

    @Override
    public List<SaveMileageDto> getMileageCouponReturnInfo(String tradeNo) {
        return writeMapper.selectMileageCouponReturnInfo(tradeNo);
    }

    //============================================================================================================

    //============================================================================================================
    // 마일리지 관리 - 취소 관련 Method
    //============================================================================================================
    /**
     * 마일리지 취소시.
     * 마일리지 사용여부 확인.
     *
     * @param userNo 마일리지 사용 취소할 고객번호
     * @param tradeNo 마일리지 사용 거래번호
     * @return int 건수존재 확인.
     */
    @Override
    public Boolean getMileageUsedCheck(String userNo, String tradeNo){
        return MileageCommonUtil.isGreaterThanZero(readMapper.selectUserMileageUsedCheck(userNo, tradeNo));
    }

    /**
     * 마일리지 취소시
     * 취소 거래에 대한 마일리지 적립데이터 생성 쿼리
     *
     * @param userNo 회원관리번호
     * @param tradeNo 거래번호
     * @return List<CancelSaveDto>
     */
    @Override
    public List<CancelSaveDto> createMileageSaveInfoFromUsed(String userNo, String tradeNo){
        return readMapper.selectUseCancelMileageInfo(userNo, tradeNo);
    }

    /**
     * 마일리지 사용 내역 확인.
     * @param userNo 고객관리번호
     * @param tradeNo 거래번호
     * @return List<MileageUseDto> 마일리지사용정보 List
     */
    @Override
    public MileageManageDto getUseCancelInfoList(String userNo, String tradeNo){
        return readMapper.selectUseMileageUsedInfo(userNo, tradeNo);
    }

    /**
     * 마일리지 관리 업데이트
     * 마일지 사용취소 후 취소여부와 취소금액을 업데이트 한다.
     *
     * @param parameterMap 마일리지 관리 업데이트 정보 맵
     * @return Boolean 업데이트 성공/실패 여부
     */
    @Override
    public Boolean modifyMileageManageInfo(HashMap<String, Object> parameterMap){
        return MileageCommonUtil.isGreaterThanZero(writeMapper.updateMileageManageInfo(parameterMap));
    }
    //============================================================================================================


    
}
