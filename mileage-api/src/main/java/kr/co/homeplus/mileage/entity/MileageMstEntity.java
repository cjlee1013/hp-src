package kr.co.homeplus.mileage.entity;

public class MileageMstEntity {

    // 고객관리번호
    public String userNo;

    // 마일리지금액
    public String mileageAmt;

    // 등록일
    public String regDt;

    // 수정일
    public String chgDt;

}
