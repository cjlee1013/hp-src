package kr.co.homeplus.mileage.management.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.mileage.management.model.cancel.CancelSaveDto;
import kr.co.homeplus.mileage.management.model.use.MileageManageDto;
import kr.co.homeplus.mileage.management.model.user.MileageHistoryManageGetDto;
import kr.co.homeplus.mileage.management.model.user.MileageMstDto;
import kr.co.homeplus.mileage.management.model.save.SaveMileageDto;
import kr.co.homeplus.mileage.management.model.user.UserSearchHistoryReqDto;
import kr.co.homeplus.mileage.core.db.annotation.SlaveConnection;
import kr.co.homeplus.mileage.sql.MileageMstSql;
import kr.co.homeplus.mileage.sql.MileagePaymentSql;
import kr.co.homeplus.mileage.sql.MileageSaveSql;
import kr.co.homeplus.mileage.sql.MileageUseSql;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.mapping.StatementType;

/**
 * 마일리지 관련 조회시에 사용한다.
 */
@SlaveConnection
public interface ManagementReadMapper {

    //============================================================================================================

    //============================================================================================================
    // 마일리지 유저 정보 관련 Method
    //============================================================================================================
    //mileage_mst 회원정보 여부 확인
    @SelectProvider(type = MileageMstSql.class, method = "selectUserMileageMstCheck")
    int selectUserMileageMstCheck(String userNo);

    //mileage_mst 회원정보 데이터
    @ResultType(value = MileageMstDto.class)
    @SelectProvider(type = MileageMstSql.class, method = "selectUserMileageMstInfo")
    MileageMstDto selectUserMileageMstInfo(String userNo);

    List<MileageHistoryManageGetDto> selectUserMileageHistoryInfo(UserSearchHistoryReqDto userSearchHistoryReqDto);

    int selectUserMileageHistoryCount(UserSearchHistoryReqDto userSearchHistoryReqDto);
    //============================================================================================================

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = MileageSaveSql.class, method = "selectUserMileageUsableList")
    List<SaveMileageDto> selectUserMileageUsableList(@Param("userNo") String userNo, @Param("requestAmt") long requestAmt);

    @ResultType(value = CancelSaveDto.class)
    @SelectProvider(type = MileageUseSql.class, method = "selectUseCancelMileageInfo")
    List<CancelSaveDto> selectUseCancelMileageInfo(@Param("userNo") String userNo, @Param("tradeNo") String tradeNo);

//    @Select("SELECT COUNT(1) AS CNT FROM mileage_use WHERE user_no = #{userNo} AND trade_no = #{tradeNo}")
    @SelectProvider(type = MileageUseSql.class, method = "selectUserMileageUsedCheck")
    int selectUserMileageUsedCheck(@Param("userNo") String userNo, @Param("tradeNo") String tradeNo);
    //============================================================================================================

    @ResultType(value = MileageManageDto.class)
    @SelectProvider(type = MileageUseSql.class, method = "selectUseMileageUsedInfo")
    MileageManageDto selectUseMileageUsedInfo(@Param("userNo") String userNo, @Param("tradeNo") String tradeNo);

    @SelectProvider(type = MileagePaymentSql.class, method = "selectMileageSaveReturnInfo")
    List<SaveMileageDto> selectMileageSaveReturnInfo(@Param("tradeNo") String tradeNo, @Param("mileageCode") String mileageCode);

}
