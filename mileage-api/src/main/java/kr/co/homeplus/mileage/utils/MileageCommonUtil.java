package kr.co.homeplus.mileage.utils;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.mileage.core.exception.MileageLogicException;
import kr.co.homeplus.mileage.enums.ActivityMileageCode;
import kr.co.homeplus.mileage.enums.MileageCommonCode;
import kr.co.homeplus.mileage.management.model.use.MileageManageDto;
import kr.co.homeplus.mileage.management.model.use.MileageUseDto;
import kr.co.homeplus.mileage.management.model.cancel.CancelSaveDto;
import kr.co.homeplus.mileage.management.model.save.SaveMileageDto;
import kr.co.homeplus.mileage.management.model.use.ExpireMileageSetDto;
import kr.co.homeplus.mileage.management.model.use.ReturnMileageSetDto;
import kr.co.homeplus.mileage.management.model.use.UseMileageSetDto;
import kr.co.homeplus.mileage.management.model.user.MileageHistoryManageSetDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.util.ReflectionUtils;

@Slf4j
public class MileageCommonUtil {
    /**
     * Object to Map[bean]
     *
     * @param object DTO 클래스
     * @return HashMap
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public static HashMap<String, Object> getConvertMap(Object object) throws Exception{
        HashMap<String, Object> returnMap = new HashMap<>();
        Class<?> clz = object.getClass();

        for(Field field : clz.getDeclaredFields()){
            if(Modifier.isPrivate(field.getModifiers())){
                ReflectionUtils.makeAccessible(field);
            }
            returnMap.put(field.getName(), field.get(object));
        }
       return returnMap;
    }

    /**
     * 히스토리 적재 DTO생성
     *
     * @param object DTO 클래스
     * @return MileageHistoryManageSetDto 마일리지 저장용 DTO
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public static MileageHistoryManageSetDto initMileageHistorySetDto(Object object) throws Exception {
        HashMap<String, Object> parameterMap = getConvertMap(object);

        //Object 별 타입 체크
        String type;

        if(object instanceof SaveMileageDto){
            type = "save";
        } else if(object instanceof MileageUseDto){
            type = "use";
        } else if(object instanceof CancelSaveDto){
            type = "manage";
        } else {
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_SYSTEM_ERR_9001, "%s", object.getClass().getName());
        }

        return MileageHistoryManageSetDto.builder()
            .userNo(String.valueOf(parameterMap.get("userNo")))
            .mileageCategory(String.valueOf(parameterMap.get("mileageCategory")))
            .mileageKind(String.valueOf(parameterMap.get("mileageKind")))
            .mileageAmt(NumberUtils.toLong(String.valueOf(parameterMap.get("mileageAmt"))))
            .mileageNo(Long.parseLong(String.valueOf(parameterMap.get(insertByFirstCamelCase("mileageNo", type)))))
            .tradeNo(ObjectUtils.toString(parameterMap.get("tradeNo")))
            .build();
    }

    /**
     * 마일리지 사용 DB용 DTO 생성
     *
     * @param object 요청 DTO
     * @return MileageUseDto
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public static MileageUseDto makeUseMileageEntity(Object object) throws Exception{
        HashMap<String, Object>  parameterMap = getConvertMap(object);

        //Object 별 타입 체크
        String mileageCategory = "MILEAGE_CATEGORY_";
        String requestType;

        // 마일리지 사용 DTO 는 아래의 타입만 가능
        if(object instanceof UseMileageSetDto){
            requestType = "use";
        } else if(object instanceof ReturnMileageSetDto){
            requestType = "return";
        } else if (object instanceof ExpireMileageSetDto){
            requestType = "expire";
        } else {
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_SYSTEM_ERR_9001, "%s", object.getClass().getName());
        }

        return MileageUseDto.builder()
            .userNo(String.valueOf(parameterMap.get("userNo")))
            .tradeNo(String.valueOf(parameterMap.get("tradeNo")))
            .mileageCategory(getMileageCommonCode(mileageCategory, requestType))
            .mileageKind(String.valueOf(parameterMap.get(insertByFirstCamelCase("requestType", requestType))))
            .mileageAmt(NumberUtils.toLong(String.valueOf(parameterMap.get(insertByFirstCamelCase("requestAmt", requestType)))))
            .useMessage(String.valueOf(parameterMap.get(insertByFirstCamelCase("requestMessage", requestType))))
            .regId(String.valueOf(parameterMap.get("regId")))
            .chgId(String.valueOf(parameterMap.get("regId")))
            .build();
    }

    /**
     * 마일리기 관리 DB용 DTO 생성
     *
     * @param mileageUseDto 사용정보 Entity
     * @param saveMileageDto 마일리지 정보 DTO
     * @return MileageManageDto 마일리지 관리 Entity
     */
    public static MileageManageDto makeMileageManageEntity(MileageUseDto mileageUseDto, SaveMileageDto saveMileageDto) {
        return MileageManageDto.builder()
            .mileageTypeNo(saveMileageDto.getMileageTypeNo())
            .userNo(mileageUseDto.getUserNo())
            .mileageUseNo(NumberUtils.toLong(mileageUseDto.getMileageUseNo()))
            .mileageSaveNo(NumberUtils.toLong(saveMileageDto.getMileageSaveNo()))
            .mileageAmt(saveMileageDto.getMileageRemainAmt())
            .cancelRemainAmt(mileageUseDto.getMileageCategory().equals("04") ? 0 : saveMileageDto.getMileageRemainAmt())
            .expireDt(saveMileageDto.getExpireDt())
            .cancelYn(mileageUseDto.getMileageCategory().equals("04") ? "Y" : "N")
            .regId(mileageUseDto.getRegId())
            .chgId(mileageUseDto.getChgId())
            .build();
    }


//    public static SaveMileageSetDto initSaveMileageSetDto(Object object) throws Exception{
//        Map parameterMap = getConvertMap(object);
//
//        //Object 별 타입 체크
//        String type;
//
//        if(object instanceof SaveMileageReqDto){
//            type = "save";
//        }else if(object instanceof CancelMileageReqDto){
//            type = "cancel";
//        } else {
//            throw new MileageLogicException(ActivityMileageCode.MILEAGE_SYSTEM_ERR_9001, "%s", object.getClass().getName());
//        }
//
//        return SaveMileageSetDto.builder()
//            .userNo(parameterMap.get("userNo").toString())
//            .mileageAmount(NumberUtils.toLong(parameterMap.get("mileageAmount").toString()))
//            .saveCode(type.equalsIgnoreCase("cancel") ? "03" : parameterMap.get("saveCode").toString())
//            .tradeNo(NumberUtils.toLong(parameterMap.get("tradeNo").toString()))
//            .saveMsg(parameterMap.get(type.concat("Msg")).toString())
//            .regId(parameterMap.get("regId").toString())
//            .chgId(parameterMap.get("regId").toString())
//            .build();
//    }

    public static String getMileageCommonCode(String mileageCategory, String searchType){
        return Stream.of(MileageCommonCode.values())
            .filter(e -> e.name().contains(mileageCategory))
            .collect(Collectors.toMap(MileageCommonCode::name, MileageCommonCode::getResponseCode))
            .get(mileageCategory.concat(searchType.toUpperCase(Locale.KOREA)));
    }

    /**
     * 페이징 OFFSET
     * @param pageNum
     * @param viewCount
     * @return
     */
    public static int getPageOffset(int pageNum, int viewCount){
        return ((pageNum - 1) * viewCount);
    }

    /**
     * 전체 페이지 계산
     * @param pageCnt
     * @param viewCount
     * @return
     */
    public static int getCalculationPage(int pageCnt, int viewCount){
        return (int) Math.ceil((double) pageCnt / viewCount);
    }

    /**
     * 0보다 큰지 확인.
     * 입력받은 값이 0보다 큰지 확인.
     *
     * @param checkBit 입력값
     * @return Boolean 여부
     */
    public static Boolean isGreaterThanZero(int checkBit){
        return checkBit > 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    /**
     * compare Value 보다 큰지 확인.
     * 입력받은 값이 0보다 큰지 확인.
     *
     * @param object 입력값
     * @param compareValue 비교값
     * @return Boolean 여부
     */
    public static Boolean isGreaterThanValue(Object object, int compareValue){
        return NumberUtils.toInt(String.valueOf(object)) > compareValue ? Boolean.TRUE : Boolean.FALSE;
    }

    /**
     * CamelCase 사이에 특정데이터 입력
     * 처음 UPPER_CASE 앞에 입력 데이터값을 append 한다.
     * @param targetStr 변경대상문자열
     * @param inputStr 변경할문자열
     * @return
     */
    public static String insertByFirstCamelCase(String targetStr, String inputStr) {
        String[] splitByCamelCase = StringUtils.splitByCharacterTypeCamelCase(targetStr);
        StringBuilder sb = new StringBuilder();
        sb.append(splitByCamelCase[0]).append(StringUtils.capitalize(inputStr));
        for (int idx = 1; idx < splitByCamelCase.length; idx++) {
            sb.append(splitByCamelCase[idx]);
        }
        return sb.toString();
    }

    /**
     * List<DTO>> 형식 내 중복키 제거 유틸.
     *
     * @param keyExtractor
     * @param <T>
     * @return
     */
    public static <T> Predicate<T> distinctByKey( Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new HashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    /**
     * Object type 변수가 비어있는지 체크
     *
     * @param obj
     * @return Boolean : true / false
     */
    public static Boolean empty(Object obj) {
        if (obj instanceof String) {
            return "".equals(obj.toString().trim());
        } else if (obj instanceof List) {
            return ((List) obj).isEmpty();
        } else if (obj instanceof Map) {
            return ((Map) obj).isEmpty();
        } else if (obj instanceof Object[]) {
            return Array.getLength(obj) == 0;
        } else {
            return obj == null;
        }
    }

    /**
     * Object type 변수가 비어있지 않은지 체크
     *
     * @param obj
     * @return Boolean : true / false
     */
    public static Boolean notEmpty(Object obj) {
        return !empty(obj);
    }
}

