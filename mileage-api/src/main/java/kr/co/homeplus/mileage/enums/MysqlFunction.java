package kr.co.homeplus.mileage.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum MysqlFunction {
    COMMON_TABLE("(SELECT IFNULL(imc.mc_nm, '''') AS mc_cd FROM dms.itm_mng_code imc where imc.gmc_cd = ''{0}'' AND imc.mc_cd = {1})"),
    MILEAGE_COMMON_TABLE("(SELECT IFNULL(mcc.code_name, '''') AS code_name FROM mileage_common_code mcc where mcc.code_group = ''{0}'' AND mcc.code = {1})"),
    MILEAGE_UPPER_COMMON_TABLE("(SELECT IFNULL(mcc.code_name, '''') AS code_name FROM mileage_common_code mcc where mcc.code_group = ''{0}'' AND mcc.code = {1} AND mcc.upper_code_group = ''{2}'' AND mcc.upper_code = {3})"),
    COMMON_ROW_TABLE(
        "(SELECT *, CASE @GROUPING WHEN {1} THEN @RANKT := @RANK + 1 ELSE @RANK := 1 END AS RANKING, @GROUPING := {1}\n"
            + " FROM {0}, (SELECT @GROUPING:= '''', @RANK := 0) XX\n"
            + " ORDER BY {2} )"
    ),
    PAYMENT_METHOD_INFO(
        "(SELECT IFNULL(pm.method_nm, '''') AS method_nm\n"
            + " FROM payment_method pm\n"
            + " WHERE pm.site_type = {0} AND pm.method_cd = {1} AND pm.parent_method_cd = {2})"),
    BETWEEN("{0} BETWEEN {1} AND {2}"),
    BETWEEN_DAY("{0} BETWEEN {1} AND DATE_ADD({2}, INTERVAL 1 DAY)"),
    BETWEEN_MIN("{0} BETWEEN {1} AND DATE_ADD({2}, INTERVAL 1 MINUTE)"),
    BETWEEN_NOW_MONTH("{0} BETWEEN DATE_ADD(NOW(), INTERVAL -1 MONTH) AND NOW()"),
    BETWEEN_ORI("{0} BETWEEN {1} AND {2}"),
    CASE("CASE WHEN {0} THEN {1} ELSE {2} END"),
    CASE_N_Y("CASE WHEN {0} THEN ''N'' ELSE ''Y'' END"),
    CASE_Y_N("CASE WHEN {0} THEN ''Y'' ELSE ''N'' END"),
    CASE_WHEN("CASE [columnName] WHEN [...] THEN [...] END"),
    CAST_CHAR("CAST({0} AS CHAR)"),
    CAST_NCHAR("CAST({0} AS NCHAR)"),
    CONCAT("CONCAT([...])"),
    COUNT("COUNT({0})"),
    DAYOFWEEK("DAYOFWEEK({0})"),
    DATEADD_DAY("DATE_ADD({0}, INTERVAL {1} DAY)"),
    DATEADD_MIN("DATE_ADD({0}, INTERVAL {1} MINUTE)"),
    DATEADD_MON("DATE_ADD({0}, INTERVAL {1} MONTH)"),
    DATEADD_YEAR("DATE_ADD({0}, INTERVAL {1} YEAR)"),
    DATEADD_WEEK("DATE_ADD({0}, INTERVAL {1} WEEK)"),
    DATEDIFF("DATEDIFF({0}, {1})"),
    DATE_FORMAT("DATE_FORMAT({0}, ''{1}'')"),
    DATE_FULL_12("DATE_FORMAT({0}, ''%Y-%m-%d %h:%i:%s'')"),
    DATE_FULL_24("DATE_FORMAT({0}, ''%Y-%m-%d %H:%i:%s'')"),
    DATE_FULL_LAST("DATE_FORMAT({0}, ''%Y-%m-%d 23:59:59'')"),
    DATE_FULL_ZERO("DATE_FORMAT({0}, ''%Y-%m-%d 00:00:00'')"),
    DISTINCT("DISTINCT {0}"),
    ELSE("ELSE {0} END"),
    EXISTS("EXISTS ({0})"),
    IFNULL("IFNULL({0}, {1})"),
    IFNULL_BLANK("IFNULL({0}, '''')"),
    IFNULL_SUM_ZERO("IFNULL(SUM({0}), 0)"),
    IFNULL_ZERO("IFNULL({0}, 0)"),
    IN("{0} IN ({1})"),
    IS_NULL("{0} IS NULL"),
    IS_NOT_NULL("{0} IS NOT NULL"),
    LIKE("{0} LIKE ''{1}''"),
    LEFT("LEFT({0}, {1})"),
    LOWER("LOWER({0})"),
    MIN("MIN({0})"),
    MAX("MAX({0})"),
    NOT_IN("{0} NOT IN ({1})"),
    NOT_LIKE("{0} NOT LIKE ''{1}''"),
    NOT_EXISTS("NOT EXISTS ({0})"),
    OR("{0} OR {1}"),
    RIGHT("RIGHT({0}, {1})"),
    SUM("SUM({0})"),
    SUBSTR("SUBSTR({0}, {1}, {2})"),
    SUB_QUERY("SELECT {0} FROM {1} WHERE {2} "),
    ROUND("ROUND({0})"),
    UPPER("UPPER({0})"),
    SP_MILEAGE_COUPON_PUBLISH("up_nt_mileage_coupon_publish({0}, {1})"),
    ;

    @Getter
    private final String pattern;
}
