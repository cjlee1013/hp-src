package kr.co.homeplus.mileage.management.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.mileage.management.model.use.MileageManageDto;
import kr.co.homeplus.mileage.management.model.use.MileageUseDto;
import kr.co.homeplus.mileage.management.model.cancel.CancelSaveDto;
import kr.co.homeplus.mileage.management.model.save.SaveMileageDto;
import kr.co.homeplus.mileage.management.model.user.MileageHistoryManageGetDto;
import kr.co.homeplus.mileage.management.model.user.MileageHistoryManageSetDto;
import kr.co.homeplus.mileage.management.model.user.MileageMstDto;
import kr.co.homeplus.mileage.management.model.user.UserSearchHistoryReqDto;

public interface ManagementMapperService {

    //============================================================================================================
    // 마일리지 관리 - 유저 관련 Method
    //============================================================================================================
    /**
     * 마일리지 마스터 회원존재 여부 체크(회원번호 기준)
     *
     * @param userNo 회원관리번호
     * @return Boolean 존재여부
     */
    boolean checkUserMileageMst(String userNo);

    /**
     * 마일리지 회원 마스터 추가(계정생성)
     * @param userNo 회원관리번호
     * @return Boolean 존재여부
     */
    boolean addUserMileageAccount(String userNo);

    /**
     * 마일리지 마스터 회원정보
     *
     * @param userNo 회원관리번호
     * @return MileageMstDto 회원 Dto
     */
    MileageMstDto getUserMileageMstInfo(String userNo);

    /**
     * 마일리지 마스터 회원정보
     *
     * @param userNo 회원관리번호
     * @return MileageMstDto 회원 Dto
     */
    MileageMstDto getUserMileageMstInfoFromWrite(String userNo);

    /**
     * 마일리지 적립시 mst 금액 업데이트
     *
     * @param userNo 회원관리번호
     * @param mileageAmt 마일리지금액
     * @return Boolean 업데이트 여부
     */
    Boolean modifyUserMileageMst(String userNo, long mileageAmt);

    /**
     * 마일리지 히스토리 조회
     *
     * @param userSearchHistoryReqDto 유저 마일리지 히스토리 조회 DTO
     * @return List<MileageHistoryManageGetDto> 마일리지 히스토리 조회 응답 DTO
     */
    List<MileageHistoryManageGetDto> getUserSearchHistoryList(
        UserSearchHistoryReqDto userSearchHistoryReqDto);

    /**
     * 마일리지 히스토리 건수
     *
     * @param userSearchHistoryReqDto 유저 마일리지 히스토리 조회 DTO
     * @return int 전체 건수
     */
    int getUserMileageHistoryCount(UserSearchHistoryReqDto userSearchHistoryReqDto);

    /**
     * 마일리지 히스토리 등록
     *
     * @param mileageHistoryManageSetDto 마일리지 히스토리 등록 DTO
     * @return Boolean 등록여부
     */
    Boolean addUserMileageHistory(MileageHistoryManageSetDto mileageHistoryManageSetDto);
    //============================================================================================================

    //============================================================================================================
    // 마일리지 관리 - 적립 관련 Method
    //============================================================================================================
    /**
     * 마일리지 Save DB 적재
     *
     * @param saveMileageSetDto 마일리지 적립 Dto
     * @return boolean 저장성공여부
     */
    Boolean addUserMileageSave(SaveMileageDto saveMileageSetDto);

    /**
     * 마일리지 적립 수정 - 사용처리.
     *
     * @param saveMileageDto 마일리지 적립 DTO
     * @return Boolean 수정성공여부
     */
    Boolean modifyMileageSaveInfo(SaveMileageDto saveMileageDto);
    //============================================================================================================

    //============================================================================================================
    // 마일리지 관리 - 사용 관련 Method
    //============================================================================================================
    /**
     * 마일리지 사용 처리(mileage_use)
     *
     * @param mileageUseDto 마일리지 사용 DB Entity
     * @return Boolean 등록 성공/실패
     */
    boolean addUserMileageUse(MileageUseDto mileageUseDto);

    /**
     * 요청한 마일리지만큼 마일리지 적재에서 조회
     * (SP를 활용하여 요청 금액 만큼 사용가능한 적립건을 리턴한다.)
     *
     * @param userNo 회원관리번호
     * @param reqAmt 사용 요청금액
     * @return List<SaveMileageDto>
     */
    List<SaveMileageDto> getUserMileageUsableList(String userNo, long reqAmt);

    /**
     * 마일리지 관리 데이터 저장
     * (마일리지 사용을 위한 적재건들 저장)
     *
     * @param mileageManageDto 마일리지 관리 DB Entity
     * @return Boolean 저장 성공/실패
     */
    Boolean addMileageManageInfo(MileageManageDto mileageManageDto);

    /**
     * 마일리지 적립된 것부터 회수하도록 정보 취득
     *
     * @param tradeNo
     * @param mileageCode
     * @return
     */
    List<SaveMileageDto> getMileageSaveReturnInfo(String tradeNo, String mileageCode);

    List<SaveMileageDto> getMileageCouponReturnInfo(String tradeNo);
    //============================================================================================================

    //============================================================================================================
    // 마일리지 관리 - 취소 관련 Method
    //============================================================================================================
    /**
     * 마일리지 취소시.
     * 마일리지 사용여부 확인.
     *
     * @param userNo 마일리지 사용 취소할 고객번호
     * @param tradeNo 마일리지 사용 거래번호
     * @return int 건수존재 확인.
     */
    Boolean getMileageUsedCheck(String userNo, String tradeNo);

    /**
     * 마일리지 취소시
     * 취소 거래에 대한 마일리지 적립데이터 생성 쿼리
     *
     * @param userNo 회원관리번호
     * @param tradeNo 거래번호
     * @return List<CancelSaveDto>
     */
    List<CancelSaveDto> createMileageSaveInfoFromUsed(String userNo, String tradeNo);

    /**
     * 마일리지 사용 내역 확인.
     * @param userNo 고객관리번호
     * @param tradeNo 거래번호
     * @return List<MileageUseDto> 마일리지사용정보 List
     */
    MileageManageDto getUseCancelInfoList(String userNo, String tradeNo);

    /**
     * 마일리지 관리 업데이트
     * 마일지 사용취소 후 취소여부와 취소금액을 업데이트 한다.
     *
     * @param parameterMap 마일리지 관리 업데이트 정보 맵
     * @return Boolean 업데이트 성공/실패 여부
     */
    Boolean modifyMileageManageInfo(HashMap<String, Object> parameterMap);
    //============================================================================================================
}
