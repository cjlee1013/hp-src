package kr.co.homeplus.mileage.office.model.payment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MileagePaymentSearchGetDto {

    @ApiModelProperty(value = "번호", position = 1)
    private String mileageReqNo;

    @ApiModelProperty(value = "구분", position = 2)
    private String requestType;

    @ApiModelProperty(value = "구분(NM)", position = 2)
    private String requestTypeNm;

    @ApiModelProperty(value = "사유", position = 3)
    private String requestReason;

    @ApiModelProperty(value = "마일리지유형", position = 4)
    private String mileageTypeName;

    @ApiModelProperty(value = "대상수", position = 5)
    private String requestCnt;

    @ApiModelProperty(value = "대상금액", position = 6)
    private String requestAmt;

    @ApiModelProperty(value = "상태", position = 7)
    private String requestStatus;

    @ApiModelProperty(value = "실행예정일", position = 8)
    private String processDt;

    @ApiModelProperty(value = "완료일", position = 9)
    private String completeDt;

    @ApiModelProperty(value = "완료건수", position = 10)
    private String completeCnt;

    @ApiModelProperty(value = "등록일", position = 11)
    private String regDt;

    @ApiModelProperty(value = "등록자", position = 12)
    private String regId;

    @ApiModelProperty(value = "수정일", position = 13)
    private String chgDt;

    @ApiModelProperty(value = "수정자", position = 14)
    private String chgId;

}
