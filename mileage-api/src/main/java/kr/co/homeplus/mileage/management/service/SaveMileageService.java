package kr.co.homeplus.mileage.management.service;

import kr.co.homeplus.mileage.core.exception.MileageLogicException;
import kr.co.homeplus.mileage.enums.ActivityMileageCode;
import kr.co.homeplus.mileage.enums.MileageCommonCode;
import kr.co.homeplus.mileage.management.model.save.SaveMileageDto;
import kr.co.homeplus.mileage.utils.MileageCommonUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 마일리지 적립 Service
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SaveMileageService {
    // Management Mapper Service
    private final ManagementMapperService managementMapperService;
    
    /**
     * 구매/이벤트 적립
     *
     * @param saveMileageDto 마일리지 적립 Dto
     * @return Boolean 적립 성공/실패
     * @throws Exception 예외가 발생할 경우 던짐
     */
    @Transactional(propagation = Propagation.NESTED)
    public String setSaveMileage(SaveMileageDto saveMileageDto) throws Exception{

        //1. 마일리지 적립 대상자가 Master 에 있는지 확인
        //mileage 마스터 DB 에 데이터가 없는 경우 오류.
        //회원가입시 반드시 마일리지 DB에 해당 유저의 마일리지 정보가 있어야 함.
        if (!managementMapperService.checkUserMileageMst(saveMileageDto.getUserNo())) {
            log.error("마일리지 등록을 위한 사용자 결과 오류 :: {}", ActivityMileageCode.MILEAGE_SAVE_ERR_01.getResponseMessage());
            return "02";
        }

        if(saveMileageDto.getMileageCategory().equals("03")){
            saveMileageDto.setMileageCategory(MileageCommonCode.MILEAGE_CATEGORY_SAVE.getResponseCode());
            saveMileageDto.setMileageKind(MileageCommonCode.MILEAGE_KIND_SAVE_05.getResponseCode());
        }

        //2. 마일리지 적립 대상자 정보로 mileage_save 저장
        //update 실패시 오류
        if (!managementMapperService.addUserMileageSave(saveMileageDto)) {
            log.error("{}", ActivityMileageCode.MILEAGE_SAVE_ERR_02);
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_SAVE_ERR_02);
        }

        //마일리지 마스터 DB 업데이트
        if (!managementMapperService.modifyUserMileageMst(saveMileageDto.getUserNo(), saveMileageDto.getMileageAmt())) {
            log.error("{}", ActivityMileageCode.MILEAGE_SAVE_ERR_03);
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_SAVE_ERR_03);
        }

        // mileage_history 에 내용 저장.
        //MileageHistoryManageSetDto 내 build 를 사용하여 처리.
        if (!managementMapperService.addUserMileageHistory(MileageCommonUtil.initMileageHistorySetDto(saveMileageDto))) {
            log.error("{}", ActivityMileageCode.MILEAGE_SAVE_ERR_04);
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_SAVE_ERR_04);
        }

        return "00";
    }


}
