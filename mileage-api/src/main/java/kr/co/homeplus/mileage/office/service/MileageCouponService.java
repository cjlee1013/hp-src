package kr.co.homeplus.mileage.office.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.mileage.core.exception.MileageLogicException;
import kr.co.homeplus.mileage.enums.MileageCommonCode;
import kr.co.homeplus.mileage.enums.MileageResponseCode;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponInfoDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponModifyDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponOneTimeDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponRequestDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponReturnGetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponReturnSetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponSearchGetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponSearchSetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponUserInfoDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponUserRegDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentGetDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSetDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDetailDto;
import kr.co.homeplus.mileage.office.model.payment.MileageRequestDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeManageModifyDto;
import kr.co.homeplus.mileage.utils.MileageCommonResponseFactory;
import kr.co.homeplus.mileage.utils.ObjectUtils;
import kr.co.homeplus.mileage.utils.SqlUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class MileageCouponService {
    // Office Mapper Management Service
    private final OfficeMapperManageService officeMapperService;
    //
    private final MileagePaymentService paymentService;

    @Transactional(rollbackFor = {Exception.class, MileageLogicException.class})
    public ResponseObject<String> requestMileageCouponReg(MileageCouponRequestDto requestDto) throws Exception {
        log.info("마일리지 쿠폰 요청 :: {}", requestDto);
        //마일리지 유형이 존재하는 지 확인.
        if(requestDto.getIssueQty() < requestDto.getPersonalIssueQty()){
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_ERR06);
        }
        MileageTypeManageModifyDto typeInfo = officeMapperService.getMileageTypeModifyInfo(requestDto.getMileageTypeNo());
        if(typeInfo == null){
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_ERR01);
        } else if(typeInfo.getUseYn().equals("N") || !typeInfo.getMileageCategory().equals("01")) {
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_ERR04);
        }
        // 마일리지 등록 요청.
        if(requestDto.getCouponType().equals("MUL") && StringUtils.isEmpty(requestDto.getCouponNo())){
            // 오류 처리
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_ERR02);
        }
        // 마일리지 쿠폰 요청 등록
        if(!this.addMileageCouponManage(requestDto)){
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_ERR03);
        }
        log.info("마일리지 쿠폰 요청번호 ::: {}", requestDto.getCouponManageNo());
        // 1회성일 경우
        boolean isIssue = Boolean.TRUE;
        if(requestDto.getCouponType().equals("ONE")){
            log.info("마일리지 쿠폰 요청 건수가 100건 이하이므로 바로 생성 :: 발급요청 : {}건", requestDto.getIssueQty());
            officeMapperService.requestMileageCouponPublish(requestDto.getCouponManageNo(), requestDto.getIssueQty());
        }

        if(isIssue){
            if(!this.modifyMileageCouponMange(requestDto.getCouponManageNo(), requestDto.getRequestId())){
                log.error("마일리지 쿠폰 발행 결과 업데이트 실패 ::: {}", requestDto.getCouponManageNo());
            }
        }
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_SUCCESS);
    }

    @Transactional(rollbackFor = {Exception.class, MileageLogicException.class})
    public ResponseObject<String> requestMileageCouponModify(MileageCouponModifyDto modifyDto) throws Exception {
        log.info("마일리지 쿠폰 수정 요청 :: {}", modifyDto);
        // 제목, 고객노출문구만 수정 가능.
        if(!this.modifyMileageCouponInfo(modifyDto)){
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_ERR11);
        }
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_MODIFY_SUCCESS);
    }

    // 마일리지 등록
    @Transactional(rollbackFor = {Exception.class, MileageLogicException.class})
    public ResponseObject<String> getMileageCouponUserRegister(MileageCouponUserRegDto regDto) throws Exception {
        if(!this.isCouponCheck(regDto.getCouponNo())){
            throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_COUPON_REG_ERR01);
        }
        // 쿠폰정보 조회
        LinkedHashMap<String, Object> couponInfo = this.getMileageCouponInfo(regDto.getCouponNo());
        if(couponInfo == null){
            throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_COUPON_REG_ERR01);
        }
        long couponManageNo = ObjectUtils.toLong(couponInfo.get("coupon_manage_no"));
        long issueQty = ObjectUtils.toLong(couponInfo.get("issue_qty"));
        // 쿠폰관련 상태 확인
        if(!this.getCouponTotalRegCount(couponManageNo, issueQty)){
            throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_COUPON_REG_ERR06);
        }
        if(ObjectUtils.toString(couponInfo.get("coupon_type")).equals("ONE")){
            if(!this.isMileageCouponRegPossible(regDto.getCouponNo())){
                throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_COUPON_REG_ERR07);
            }
        }

        LinkedHashMap<String, Object> parameterToMap = SqlUtils.getConvertMapToSnakeCase(regDto);
        if(parameterToMap == null){
            throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_COUPON_REG_ERR04);
        }
        // 유저가 쿠펀번호로 등록한 횟수 조회.
        couponInfo.putAll(parameterToMap);

        log.info("마일리지 등록요청 쿠폰 정보 ::: {}", couponInfo);
        // 마일리지 등록한 횟수 체크
        LinkedHashMap<String, Long> couponRegCheckInfo = officeMapperService.getMileageCouponRegCheck(couponInfo);
        log.info("마일리지 등록가능횟수 체크 ::: {}", couponRegCheckInfo);

        // 실제등록횟수가 인당등록횟수이상인경우 오류
        if(ObjectUtils.toLong(couponInfo.get("personal_issue_qty")) <= couponRegCheckInfo.get("cnt")){
            throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_COUPON_REG_ERR02);
        }

        if(!this.getMileageCouponUserReg(couponInfo)){
            throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_COUPON_REG_ERR09);
        }
        // 쿠폰 지급 요청 타입 설정.
        couponInfo.put("request_type", "01");
        // 쿠폰지급요청.
        String mileageSaveNo = this.getMileageSaveProcess(couponInfo);
        if(mileageSaveNo == null) {
            throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_COUPON_REG_ERR08);
        }
        long couponInfoSeq = ObjectUtils.toLong(couponInfo.get("coupon_info_seq"));
        if(!this.setMileageSaveResult(couponInfoSeq, mileageSaveNo)){
            log.error("마일리지 쿠폰 정보에 적립내용 업데이트 중 오류 :: couponInfoSeq : [{}] / mileageSaveNo : [{}]", couponInfoSeq, mileageSaveNo);
        }

        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_REG_SUCCESS);
    }

    @Transactional(rollbackFor = {Exception.class, MileageLogicException.class})
    public ResponseObject<MileageCouponReturnGetDto> requestMileageCouponReturn(MileageCouponReturnSetDto setDto) throws Exception {
        // 회수 대상자 업데이트 처리.
        this.modifyMileageReturnInfo(setDto, Boolean.FALSE);
        MileageCouponReturnGetDto getDto = new MileageCouponReturnGetDto();
        if(setDto.getInfoList().stream().anyMatch(dto -> StringUtils.isNotEmpty(dto.getUseDt()))){
            // 회수처리
            ResponseObject<MileagePaymentGetDto> returnResponse = paymentService.setMileagePaymentInfo(this.createReturnPaymentDto(setDto));
            getDto.setSuccessCnt(returnResponse.getData().getPayment().getSuccess().getSuccessCnt());
            getDto.setFailCnt(returnResponse.getData().getPayment().getFail().getFailCnt());
        } else {
            getDto.addSuccessCnt(setDto.getInfoList().size());
        }
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_RETURN_SUCCESS, getDto);
    }

    private void modifyMileageReturnInfo(MileageCouponReturnSetDto setDto, boolean isComplete) throws Exception {
        try{
            for(MileageCouponUserInfoDto dto : setDto.getInfoList()){
                if(!officeMapperService.modifyMileageCouponReturn(dto.getCouponInfoSeq(), setDto.getRequestId(), isComplete)){
                    throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_COUPON_RETURN_ERR01);
                }
            }
        } catch (Exception e) {
            throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_COUPON_RETURN_ERR01);
        }
    }

    public ResponseObject<List<MileageCouponSearchGetDto>> getCouponManageInfoList(MileageCouponSearchSetDto setDto) throws Exception {
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_SEARCH_SUCCESS, officeMapperService.getMileageCouponManageInfoList(setDto));
    }

    public ResponseObject<MileageCouponInfoDto> getCouponManageInfo(long couponManageNo) throws Exception {
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_SEARCH_SUCCESS, officeMapperService.getCouponInfo(couponManageNo));
    }

    public ResponseObject<List<MileageCouponUserInfoDto>> getCouponUserInfoList(long couponManageNo) throws Exception {
        List<MileageCouponUserInfoDto> userList = officeMapperService.getMileageCouponUserInfo(couponManageNo);
        for(MileageCouponUserInfoDto dto : userList){
            dto.setUserMaskingNo(PrivacyMaskingUtils.maskingUserId(dto.getUserNo()));
            dto.setUserId(PrivacyMaskingUtils.maskingUserId(dto.getUserId()));
            if(StringUtils.isNotEmpty(dto.getUserNm())){
                dto.setUserNm(PrivacyMaskingUtils.maskingUserName(dto.getUserNm()));
            }
        }
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_SEARCH_SUCCESS, userList);
    }

    public ResponseObject<List<MileageCouponOneTimeDto>> getCouponOneTimeInfoDown(long couponManageNo) throws Exception {
        List<MileageCouponOneTimeDto> couponList = officeMapperService.getCouponOneTimeList(couponManageNo);
        if(couponList == null || couponList.size() == 0) {
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_SEARCH_FAIL);
        }
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COUPON_SEARCH_SUCCESS, couponList);
    }

    private MileagePaymentSetDto createReturnPaymentDto(MileageCouponReturnSetDto setDto) {
        return MileagePaymentSetDto.builder()
            .mileageRequestDto(
                MileageRequestDto.builder()
                    .mileageTypeNo(setDto.getMileageTypeNo())
                    .requestType("05")
                    .requestReason("쿠폰회수")
                    .tradeNo(String.valueOf(setDto.getCouponManageNo()))
                    .displayMessage("쿠폰회수")
                    .requestMessage("쿠폰회수")
                    .processDt(new SimpleDateFormat("yyyy-MM-dd").format(new Date()))
                    .useYn("Y")
                    .directlyYn("Y")
                    .regId(setDto.getRequestId())
                    .build()
            )
            .mileageReqDetailList(
                this.createMileageReturnUserList(setDto)
            )
            .build();
    }

    private List<MileageReqDetailDto> createMileageReturnUserList(MileageCouponReturnSetDto setDto) {
        List<MileageReqDetailDto> returnList = new ArrayList<>();
        List<MileageCouponUserInfoDto> userInfoList = setDto.getInfoList().stream().filter(dto -> StringUtils.isNotEmpty(dto.getUseDt()) && dto.getRecoveryYn().equals("N")).collect(Collectors.toList());
        for(MileageCouponUserInfoDto dto : userInfoList){
            returnList.add(
                MileageReqDetailDto.builder()
                    .userNo(dto.getUserNo())
                    .mileageAmt(setDto.getMileageAmt())
                    .mileageDetailStatus(MileageCommonCode.MILEAGE_REQUEST_STATUS_WAIT.getResponseMessage())
                    .useYn("Y")
                    .regId(setDto.getRequestId())
                    .chgId(setDto.getRequestId())
                    .build()
            );
        }
        return returnList;
    }

    private LinkedHashMap<String, Object> getMileageCouponInfo(String couponNo){
        return officeMapperService.getMileageCouponInfo(couponNo);
    }

    private int getMileageMultiCouponRegCheck(String couponNo){
        LinkedHashMap<String, Object> multiCouponCheckMap = officeMapperService.getMileageMultiCouponRegCheck(couponNo);
        if(multiCouponCheckMap == null){
            return 0;
        }
        return ObjectUtils.toInt(multiCouponCheckMap.get("cnt"));
    }

    private Boolean addMileageCouponManage(MileageCouponRequestDto requestDto) throws Exception {
        if(requestDto.getCouponType().equals("MUL")){
            if(requestDto.getCouponNo().length() > 13){
                throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_COUPON_ERR05);
            }
            if(this.getMileageMultiCouponRegCheck(requestDto.getCouponNo()) > 0){
                throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_COUPON_REG_ERR01);
            }
        } else {
            if(StringUtils.isNotEmpty(requestDto.getCouponNo())){
                requestDto.setCouponNo(null);
            }
        }
        return officeMapperService.addMileageCouponManage(requestDto);
    }

    private String getMileageSaveProcess(LinkedHashMap<String, Object> parameterMap) throws Exception {
        ResponseObject<MileagePaymentGetDto> mileageSaveReqResponse = paymentService.setMileagePaymentInfo(this.createMileagePaymentDto(parameterMap));
        if("0000,SUCCESS".contains(mileageSaveReqResponse.getReturnCode())){
            // 마일리지 적립번호찾기
            return officeMapperService.getMileageSaveNo(mileageSaveReqResponse.getData().getMileageReqNo());
        }
        return null;
    }

    private MileagePaymentSetDto createMileagePaymentDto(LinkedHashMap<String, Object> parameterMap) {
        return MileagePaymentSetDto.builder()
            .mileageRequestDto(this.createMileageRequestDto(parameterMap))
            .mileageReqDetailList(this.createMileageRequestUserList(parameterMap))
            .build();
    }

    private MileageRequestDto createMileageRequestDto(LinkedHashMap<String, Object> parameterMap) {
        return MileageRequestDto.builder()
            .mileageTypeNo(ObjectUtils.toString(parameterMap.get("mileage_type_no")))
            .requestType(ObjectUtils.toString(parameterMap.get("request_type")))
            .requestReason(ObjectUtils.toString(parameterMap.get("display_message")))
            .tradeNo(ObjectUtils.toString(parameterMap.get("coupon_no")))
            .displayMessage(ObjectUtils.toString(parameterMap.get("display_message")))
            .requestMessage(ObjectUtils.toString(parameterMap.get("display_message")))
            .processDt(new SimpleDateFormat("yyyy-MM-dd").format(new Date()))
            .useYn("Y")
            .directlyYn("Y")
            .regId(ObjectUtils.toString(parameterMap.get("request_id")))
            .build();
    }

    private List<MileageReqDetailDto> createMileageRequestUserList(LinkedHashMap<String, Object> parameterMap) {
        return new ArrayList<>(){{
            add(
                MileageReqDetailDto.builder()
                    .userNo(ObjectUtils.toString(parameterMap.get("user_no")))
                    .mileageAmt(ObjectUtils.toLong(parameterMap.get("mileage_amt")))
                    .mileageDetailStatus(MileageCommonCode.MILEAGE_REQUEST_STATUS_WAIT.getResponseMessage())
                    .useYn("Y")
                    .regId(ObjectUtils.toString(parameterMap.get("request_id")))
                    .chgId(ObjectUtils.toString(parameterMap.get("request_id")))
                    .build()
            );
        }};
    }

    // 쿠폰 사용처리.
    private Boolean getMileageCouponUserReg(LinkedHashMap<String, Object> parameterMap) {
        if(ObjectUtils.toString(parameterMap.get("coupon_type")).equals("ONE")){
            // update 발급된 쿠폰에 정보를 업데이트
            return officeMapperService.modifyMileageCouponInfo(parameterMap);
        }
        // insert
        return officeMapperService.addMileageCouponInfoByMulti(parameterMap);
    }

    private Boolean modifyMileageCouponMange(long couponManageNo, String requestId){
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        parameterMap.put("coupon_manage_no", couponManageNo);
        parameterMap.put("issue_yn", "Y");
        parameterMap.put("request_id", StringUtils.isNoneEmpty(requestId) ? requestId : "SYSTEM");
        return officeMapperService.modifyMileageCouponMange(parameterMap);
    }

    private Boolean modifyMileageCouponInfo(MileageCouponModifyDto modifyDto){
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        parameterMap.put("coupon_manage_no", modifyDto.getCouponManageNo());
        parameterMap.put("coupon_nm", modifyDto.getCouponNm());
        parameterMap.put("display_message", modifyDto.getDisplayMessage());
        parameterMap.put("use_yn", modifyDto.getUseYn());
        parameterMap.put("request_id", StringUtils.isNoneEmpty(modifyDto.getRequestId()) ? modifyDto.getRequestId() : "SYSTEM");
        return officeMapperService.modifyMileageCouponMange(parameterMap);
    }

    private Boolean setMileageSaveResult(long couponInfoSeq, String mileageSaveNo) {
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        parameterMap.put("coupon_info_seq", couponInfoSeq);
        parameterMap.put("mileage_save_no", mileageSaveNo);
        return officeMapperService.modifyMileageCouponInfo(parameterMap);
    }

    private Boolean isCouponCheck(String couponNo) {
        return (couponNo.startsWith("5") || couponNo.startsWith("#")) ? Boolean.TRUE : Boolean.FALSE;
    }

    private Boolean getCouponTotalRegCount(long couponManageNo, long issueQty) {
        long totalCount = officeMapperService.getCouponTotalRegCount(couponManageNo);
        if(totalCount >= issueQty && totalCount != 0){
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    private Boolean isMileageCouponRegPossible(String couponNo) {
        LinkedHashMap<String, String> possibleInfoMap = officeMapperService.getMileageCouponRegIsPossibleCheck(couponNo);
        if(possibleInfoMap == null){
            return Boolean.TRUE;
        }
        return possibleInfoMap.get("isPossible").contains("Y");
    }
}
