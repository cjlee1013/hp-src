package kr.co.homeplus.mileage.office.model.coupon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
@ApiModel(description = "마일리지 난수번호 관리 - 유저정보")
public class MileageCouponUserInfoDto {
    @ApiModelProperty(notes = "쿠폰정보Seq")
    private long couponInfoSeq;
    @ApiModelProperty(notes = "회원번호")
    private String userNo;
    @ApiModelProperty(notes = "회원ID")
    private String userId;
    @ApiModelProperty(notes = "이름")
    private String userNm;
    @ApiModelProperty(notes = "난수번호")
    private String couponNo;
    @ApiModelProperty(notes = "발급일시")
    private String issueDt;
    @ApiModelProperty(notes = "사용일시")
    private String useDt;
    @ApiModelProperty(notes = "회수")
    private String recoveryYn;
    @ApiModelProperty(notes = "발급주체")
    private String regId;
    @ApiModelProperty(notes = "회수자")
    private String recoveryId;
    @ApiModelProperty(notes = "회수일")
    private String recoveryDt;
    @ApiModelProperty(notes = "유저번호Masking")
    private String userMaskingNo;
    public void setUserNo(String userNo){
        if(userNo.equals("-")){
            this.userNo = null;
        } else {
            this.userNo = userNo;
        }
    }

    public void setUserId(String userId){
        if(userId.equals("-")){
            this.userId = null;
        } else {
            this.userId = userId;
        }
    }

    public void setUserNm(String userNm){
        if(userNm.equals("-")){
            this.userNm = null;
        } else {
            this.userNm = userNm;
        }
    }
}

