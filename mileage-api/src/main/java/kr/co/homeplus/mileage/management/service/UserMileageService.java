package kr.co.homeplus.mileage.management.service;

import java.util.List;
import kr.co.homeplus.mileage.management.model.user.UserSearchHistoryReqDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.mileage.management.model.user.MileageHistoryManageGetDto;
import kr.co.homeplus.mileage.management.model.user.MileageMstDto;
import kr.co.homeplus.mileage.management.model.user.UserSearchHistoryResDto;
import kr.co.homeplus.mileage.core.exception.MileageLogicException;
import kr.co.homeplus.mileage.enums.ActivityMileageCode;
import kr.co.homeplus.mileage.utils.MileageCommonUtil;
import kr.co.homeplus.mileage.utils.MileageCommonResponseFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserMileageService {
    // Management Mapper Service
    private final ManagementMapperService managementMapperService;

    /**
     * 유저 마일리지 계정 등록
     *
     * @param userNo 고객관리번호
     * @return ResponseObject<String> 계정등록에 대한 응답코드, 응답메시지
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public ResponseObject<String> addUserMileageAccountReg(String userNo) throws Exception {

        //기존 유저의 마일리지 계정인 존재하는 지 확인
        if (managementMapperService.checkUserMileageMst(userNo)) {
            return MileageCommonResponseFactory.getResponseObject("0000", ActivityMileageCode.MILEAGE_USER_REGISTER_ERR_01.getResponseMessage());
        }

        //유저의 마일리지 계정 등록(mileage_mst)
        if (!managementMapperService.addUserMileageAccount(userNo)) {
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_USER_REGISTER_ERR_02);
        }
        return MileageCommonResponseFactory.getResponseObject(ActivityMileageCode.MILEAGE_USER_REGISTER_SUCCESS);
    }

    /**
     * 마일리지 잔액 조회(유저)
     *
     * @param userNo 고객관리번호
     * @return MileageMstDto 고객 마일리지 잔액 정보 DTO
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public ResponseObject<MileageMstDto> checkUserMileageAccountBalance(String userNo) throws Exception {

        //기존 유저의 마일리지 계정인 존재하는 지 확인
        if (!managementMapperService.checkUserMileageMst(userNo)) {
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_USER_ACCOUNT_CHECK_ERR_01);
        }
        //마일리지 계좌잔액 조회
        MileageMstDto mileageMstDto;
        try {
            mileageMstDto = managementMapperService.getUserMileageMstInfo(userNo);
        } catch (Exception e) {
            throw new MileageLogicException(ActivityMileageCode.MILEAGE_USER_ACCOUNT_CHECK_ERR_02);
        }

        return MileageCommonResponseFactory.getResponseObject(ActivityMileageCode.MILEAGE_USER_ACCOUNT_CHECK_SUCCESS,
            mileageMstDto);
    }

    /**
     * 마일리지 내역(적립/사용/취소)
     *
     * @param userSearchHistoryReqDto 마일리지 히스토리 내역 조회 요청 DTO
     * @return UserSearchHistoryResDto<List<MileageHistoryManageGetDto>>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public ResponseObject<UserSearchHistoryResDto<List<MileageHistoryManageGetDto>>> getUserMileageHistory(UserSearchHistoryReqDto userSearchHistoryReqDto) throws Exception {

        log.info("getUserMileageHistory UserSearchHistoryReqDto[{}]", userSearchHistoryReqDto);

        //마일리지 히스토리 응답 DTO
        UserSearchHistoryResDto<List<MileageHistoryManageGetDto>> userSearchHistoryResDto;

        //우선 바디만 생성.
        userSearchHistoryResDto = UserSearchHistoryResDto.<List<MileageHistoryManageGetDto>>builder()
            .nowPage(userSearchHistoryReqDto.getPage())
            .responseData(managementMapperService.getUserSearchHistoryList(userSearchHistoryReqDto))
            .userNo(userSearchHistoryReqDto.getUserNo())
            .viewCount(userSearchHistoryReqDto.getViewCount())
            .totalPage(MileageCommonUtil.getCalculationPage(managementMapperService.getUserMileageHistoryCount(
                userSearchHistoryReqDto), userSearchHistoryReqDto.getViewCount()))
            .build();

        return MileageCommonResponseFactory.getResponseObject(ActivityMileageCode.MILEAGE_USER_ACCOUNT_CHECK_SUCCESS,
            userSearchHistoryResDto, "이력 조회");
    }
}
