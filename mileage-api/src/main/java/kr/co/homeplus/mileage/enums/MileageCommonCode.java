package kr.co.homeplus.mileage.enums;

import kr.co.homeplus.mileage.enums.impl.EnumImpl;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MileageCommonCode implements EnumImpl {

    MILEAGE_CATEGORY_SAVE("01", "적립", "mileage_category"),
    MILEAGE_CATEGORY_USE("02", "사용", "mileage_category"),
    MILEAGE_CATEGORY_CANCEL("03", "취소", "mileage_category"),
    MILEAGE_CATEGORY_RETURN("04", "회수", "mileage_category"),
    MILEAGE_CATEGORY_EXPIRE("05", "소멸", "mileage_category"),

    MILEAGE_KIND_SAVE_01("01", "주문적립", "mileage_kind"),
    MILEAGE_KIND_SAVE_02("02", "이벤트적립", "mileage_kind"),
    MILEAGE_KIND_SAVE_03("03", "고객보상", "mileage_kind"),
    MILEAGE_KIND_SAVE_04("04", "페이백", "mileage_kind"),
    MILEAGE_KIND_SAVE_05("05", "주문취소환원", "mileage_kind"),
    MILEAGE_KIND_SAVE_11("11", "일반리뷰적립", "mileage_kind"),
    MILEAGE_KIND_SAVE_12("12", "포토리뷰적립", "mileage_kind"),
    MILEAGE_KIND_SAVE_90("90", "행사적립", "mileage_kind"),
    MILEAGE_KIND_SAVE_99("99", "테스트", "mileage_kind"),

    MILEAGE_KIND_USE_01("01", "주문사용", "mileage_kind"),
    MILEAGE_KIND_USE_02("02", "이벤트참여", "mileage_kind"),

    MILEAGE_KIND_CANCEL_01("01", "주문취소", "mileage_kind"),
    MILEAGE_KIND_CANCEL_02("02", "이벤트취소", "mileage_kind"),

    MILEAGE_KIND_RETURN_01("01", "오지급", "mileage_kind"),
    MILEAGE_KIND_RETURN_02("02", "부정지급", "mileage_kind"),
    MILEAGE_KIND_RETURN_03("03", "주문적립취소", "mileage_kind"),
    MILEAGE_KIND_RETURN_04("04", "배송비PAYBACK", "mileage_kind"),
    MILEAGE_KIND_RETURN_05("05", "난수번호적립회수", "mileage_kind"),

    MILEAGE_KIND_EXPIRE_01("01", "유효기간만료", "mileage_kind"),

    MILEAGE_REQUEST_STATUS_WAIT("M1", "대기", "request_status"),
    MILEAGE_REQUEST_STATUS_PROCESSING("M2", "진행중", "request_status"),
    MILEAGE_REQUEST_STATUS_COMPLETE("M3", "완료", "request_status"),
    MILEAGE_REQUEST_STATUS_FAIL("M4", "실패", "request_status"),

    MILEAGE_REQUEST_DETAIL_STATUS_WAIT("D1", "대기", "mileage_detail_status"),
    MILEAGE_REQUEST_DETAIL_STATUS_COMPLETE("D3", "완료", "mileage_detail_status"),
    MILEAGE_REQUEST_DETAIL_STATUS_FAIL("D4", "실패", "mileage_detail_status"),

    MILEAGE_REQUEST_TYPE_SAVE("01", "적립", "request_type"),
    MILEAGE_REQUEST_TYPE_RETURN("02", "회수", "request_type"),
    MILEAGE_REQUEST_TYPE_ORDER_RETURN("03", "주문적립회수", "request_type"),
    MILEAGE_REQUEST_TYPE_PAYBACK_RETURN("04", "배송비PAYBACK회수", "request_type"),
    MILEAGE_REQUEST_TYPE_COUPON_RETURN("05", "난수번호적립회수", "request_type"),
    ;

    private final String responseCode;
    private final String responseMessage;
    private final String codeGroup;

}
