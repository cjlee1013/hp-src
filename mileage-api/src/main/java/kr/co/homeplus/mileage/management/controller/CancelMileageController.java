package kr.co.homeplus.mileage.management.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kr.co.homeplus.mileage.management.model.user.MileageMstDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.mileage.management.model.cancel.CancelMileageReqDto;
import kr.co.homeplus.mileage.management.service.CancelMileageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/cancel")
@Api(tags = "CancelMileageController", value = "마일리지 취소 Controller")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
}
)
@RequiredArgsConstructor
public class CancelMileageController {

    //마일리지 취소 Service
    private final CancelMileageService cancelMileageService;

    @ApiOperation(value = "마일리지 사용 취소")
    @Validated
    @PostMapping(value = "/cancelMileageRestore")
    public ResponseObject<MileageMstDto>  setCancelMileageRestore(@RequestBody CancelMileageReqDto cancelMileageReqDto) throws Exception {
        return cancelMileageService.setCancelMileageRestore(cancelMileageReqDto);
    }


}
