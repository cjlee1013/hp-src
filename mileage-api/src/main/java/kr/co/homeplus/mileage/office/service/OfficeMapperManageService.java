package kr.co.homeplus.mileage.office.service;


import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponInfoDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponOneTimeDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponRequestDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponSearchGetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponSearchSetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponUserInfoDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDto;
import kr.co.homeplus.mileage.management.model.save.SaveMileageDto;
import kr.co.homeplus.mileage.management.model.use.ReturnMileageSetDto;
import kr.co.homeplus.mileage.office.model.history.MileageHistoryDto;
import kr.co.homeplus.mileage.office.model.history.MileageHistoryReqDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSearchGetDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSearchSetDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDetailDto;
import kr.co.homeplus.mileage.office.model.stat.MileageStatSumListGet;
import kr.co.homeplus.mileage.office.model.stat.MileageStatSumListSet;
import kr.co.homeplus.mileage.office.model.stat.MileageStatTypeSumListGet;
import kr.co.homeplus.mileage.office.model.type.MileagePromoTypeGetDto;
import kr.co.homeplus.mileage.office.model.type.MileagePromoTypeSetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeManageModifyDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeManageSetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeSearchGetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeSearchSetDto;
import org.springframework.scheduling.annotation.Async;

public interface OfficeMapperManageService {

    //============================================================================================================
    // Back-Office 내역 조회 관련 Method
    //============================================================================================================
    // 마일리지 내역 조회
    List<MileageHistoryDto> getMileageHistoryInfo(MileageHistoryReqDto mileageHistoryReqDto);

    //============================================================================================================

    //============================================================================================================
    // Back-Office 지급/회수 관련 Method
    //============================================================================================================
    /**
     * 기존 등록여부 확인
     *
     * @param mileageReqNo 마일리지 지급/회수 요청번호
     * @return Boolean 확인유무
     */
    Boolean checkMileageRequestInfo(String mileageReqNo);

    /**
     * 마일리지 요청 수정데이터 체크
     * @param parameterMap 마일리지 지급/회수 수정 데이터
     * @return MileageReqDto 마일리지 지급/회수 요청 Entity
     */
    MileageReqDto getMileageReqModifyCheck(HashMap<String, Object> parameterMap);

    /**
     * 마일리지 요청정보 수정.
     *
     * @param parameterMap 마일리지 지급/회수 수정데이터 MAP
     * @return Boolean 수정성공여부
     */
    Boolean modifyMileageRequestInfo(HashMap<String, Object> parameterMap);

    /**
     * 마일리지 지급/회수 대상회원 상태 수정
     * @param parameterMap 마일리지 지급/회수 대상회원 상태변경 데이터
     * @return CompletableFuture<Boolean>
     */
    @Async
    CompletableFuture<Boolean> modifyMileageRequestDetailInfo(HashMap<String, Object> parameterMap);

    /**
     * 마일리지 지급/회수 요청 등록
     *
     * @param mileageReqDto 마일리지 지급/회수 Entity.
     * @return Boolean 등록성공여부
     */
    Boolean addMileageRequestInfo(MileageReqDto mileageReqDto);

    /**
     * 마일리지 적립/회수요청 상세정보 등록(Async)
     *
     * @param mileageReqDetailDto 마일리지 지급/회수 대상회원 등록 DTO
     * @return CompletableFuture<Boolean>
     */
    @Async
    CompletableFuture<Boolean> addMileageRequestDetailInfo(MileageReqDetailDto mileageReqDetailDto);

    /**
     * 마일리지 회수 정보 조회(회수데이터 생성)
     *
     * @param mileageReqNo 마일리지 지급/회수 요청번호
     * @return List<ReturnMileageSetDto> 마일리지 회수 정보
     */
    List<ReturnMileageSetDto> getMileageReturnInfo(long mileageReqNo);

    /**
     * 마일리지 적립 정보 조회(적립데이터 생성)
     *
     * @param mileageReqNo 마일리지 지급/회수 요청번호
     * @return List<SaveMileageDto>
     */
    List<SaveMileageDto> getSaveMileageInfoList(long mileageReqNo, int expireDay);

    /**
     * 마일리지 지급/회수 조회
     *
     * @param mileagePaymentSearchSetDto 마일리지 지급/회수 조회 DTO
     * @return List<MileagePaymentSearchGetDto> 마일리지 지급/회수 조회 결과 DTO
     */
    List<MileagePaymentSearchGetDto> getSearchMileagePaymentInfo(MileagePaymentSearchSetDto mileagePaymentSearchSetDto);

    /**
     * 마일리지 적립/회수 요청정보 조회
     *
     * @param mileageReqNo 마일리지 적립/회수 요청번호
     * @return MileageReqDto 마일리지 지급/회수 Entity
     */
    MileageReqDto getSearchMileagePaymentBasicInfo(String mileageReqNo);

    /**
     * 마일리지 적립/회수 대상회원 조회
     *
     * @param mileageReqNo 마일리지 적립/회수 요청번호
     * @return List<MileageReqDetailDto> 마일리지 적립/회수 대상회원 DTO
     */
    List<MileageReqDetailDto> getSearchMileagePaymentUserInfo(String mileageReqNo);
    //============================================================================================================

    //============================================================================================================
    // Back-Office 유형 관련 Method
    //============================================================================================================
    /**
     * 마일리지 유형 존재 유무.
     *
     * @param mileageTypeNo 마일리지 유형번호
     * @return Boolean 존재유무
     */
    Boolean checkMileageTypeInfo(String mileageTypeNo);

    /**
     * 마일리지 타입 등록
     *
     * @param mileageTypeManageSetDto 마일리지 타입 등록 DTO
     * @return Boolean 등록여부
     */
    Boolean setMileageTypeInfo(MileageTypeManageSetDto mileageTypeManageSetDto);

    /**
     * 마일리지 타입 수정
     *
     * @param parameterMap 마일리지 타입 수정정보 Map
     * @return Boolean 수정여부
     */
    Boolean modifyMileageTypeInfo(HashMap<String, Object> parameterMap);

    /**
     * 마일리지 유형 정보 조회
     *
     * @param mileageTypeSearchSetDto 마일리지 유형 정보 조회 DTO
     * @return List<MileageTypeSearchGetDto> 마일리지 유형 정보 조회 응답 DTO
     */
    List<MileageTypeSearchGetDto> getMileageTypeInfoList(
        MileageTypeSearchSetDto mileageTypeSearchSetDto);

    /**
     * 마일리지 타입 기본정보 조회
     *
     * @param mileageTypeNo 마일리지유형관리번호
     * @return MileageTypeManageModifyDto 마일리지 수정 DTO
     */
    MileageTypeManageModifyDto getMileageTypeModifyInfo(String mileageTypeNo);

    /**
     * 마일리지 타입 수정 대상 확인
     *
     * @param mileageTypeManageModifyDto 마일리지 수정 DTO
     * @return HashMap<String, Object> 수정 대상 정보 Map
     */
    HashMap<String, Object> getMileageTypeModifyCheck(MileageTypeManageModifyDto mileageTypeManageModifyDto);

    /**
     * 마일리지 회수 타입 가지고 오기.
     *
     * @return 회수의 마일리지 타입
     */
    String getMileageTypeByReturn();

    /**
     * 마일리지 행사 관리용 타입 조회
     * @param setDto 조회파라미터
     * @return
     */
    List<MileagePromoTypeGetDto> getMileagePromotionTypeInfo(MileagePromoTypeSetDto setDto);

    /**
     * 마일리지 코드 중복체크
     *
     * @param mileageCode 마일리지코드
     * @return 중복여부
     */
    boolean isMileageCodeDuplication(String mileageCode);

    /**
     * 마일리지 페이백 회수에 대한 마일리지 타입 코드 조회
     *
     * @param tradeNo
     * @return
     */
    String getMileageReturnType(String tradeNo, String mileageCode);

    String getMileageCouponReturnType(long couponInfoSeq);
    //============================================================================================================

    //============================================================================================================
    // Back-Office 마일리지 등록
    //============================================================================================================
    // 마일리지 쿠폰 등록 요청
    Boolean addMileageCouponManage(MileageCouponRequestDto setDto);
    // 쿠포 발행 요청
    void requestMileageCouponPublish(long couponManageNo, long issueQty);
    // 쿠폰정보조회
    LinkedHashMap<String, Object> getMileageCouponInfo(String couponNo);
    // 쿠폰등록건수조회
    LinkedHashMap<String, Long> getMileageCouponRegCheck(LinkedHashMap<String, Object> parameterMap);
    // 마일리지쿠폰등록(다회성)
    Boolean addMileageCouponInfoByMulti(HashMap<String, Object> parameterMap);
    // 마일리지 쿠폰등록(일회성), 마일리지 쿠폰정보 업데이트(마일리지적립정보, 회수정보)
    Boolean modifyMileageCouponInfo(HashMap<String, Object> parameterMap);
    // 마일리지 쿠폰등록요청 정보 업데이트
    Boolean modifyMileageCouponMange(HashMap<String, Object> parameterMap);
    // 마일리지 다회성 기등록건 체크
    LinkedHashMap<String, Object> getMileageMultiCouponRegCheck(String couponNo);
    // 마일리지 적립요청번호로 마일리지 적립번호 찾기
    String getMileageSaveNo(String mileageReqNo);
    // 마일리지 등록 토탈 횟수
    long getCouponTotalRegCount(long couponManageNo);
    // 쿠폰 등록 가능 여부
    LinkedHashMap<String, String> getMileageCouponRegIsPossibleCheck(String couponNo);
    // 마일리지 쿠폰 관리 조회
    List<MileageCouponSearchGetDto> getMileageCouponManageInfoList(MileageCouponSearchSetDto setDto);
    // 마일리지 쿠폰정보
    MileageCouponInfoDto getCouponInfo(long couponManageNo);
    // 마일리지 쿠폰 - 유저정보
    List<MileageCouponUserInfoDto> getMileageCouponUserInfo(long couponManageNo);

    List<MileageCouponOneTimeDto> getCouponOneTimeList(long couponManageNo);

    boolean modifyMileageCouponReturn(long couponInfoSeq, String requestId, boolean isComplete);

    //마일리지 일자별 금액 합계 통계 조회
    List<MileageStatSumListGet> getDailyMileageSummary(MileageStatSumListSet requestDto);
    //마일리지 일자/타입별 금액 합계 통계 조회
    List<MileageStatTypeSumListGet> getDailyMileageTypeSummary(MileageStatSumListSet requestDto);
    //============================================================================================================
}
