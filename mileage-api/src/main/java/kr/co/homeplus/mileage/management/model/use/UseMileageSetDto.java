package kr.co.homeplus.mileage.management.model.use;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
@ApiModel(description = "마일리지 사용 요청 DTO", value = "UseMileageSetDto")
public class UseMileageSetDto {

    @ApiModelProperty(value = "고객번호", position = 1)
    @NotNull(message = "고객번호")
    private String userNo;

    @ApiModelProperty(value = "마일리지사용구분", position = 2)
    @Pattern(regexp = "01|02{2}", message = "사용구분")
    private String requestUseType;

    @ApiModelProperty(value = "거래번호(사용)", position = 2)
    @NotNull(message = "거래번호")
    @Length(max = 20, message = "20자까지 입력가능합니다.")
    private String tradeNo;

    @ApiModelProperty(value = "사용요청금액", position = 3)
    private long requestUseAmt;

    @ApiModelProperty(value = "사용내역 상세메시지", position = 4)
    private String requestUseMessage;

    @ApiModelProperty(value = "등록아이디", position = 5)
    private String regId;

}
