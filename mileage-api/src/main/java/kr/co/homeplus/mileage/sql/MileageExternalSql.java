package kr.co.homeplus.mileage.sql;

import kr.co.homeplus.mileage.constants.CustomConstants;
import kr.co.homeplus.mileage.entity.MileageReqDetailEntity;
import kr.co.homeplus.mileage.entity.MileageReqEntity;
import kr.co.homeplus.mileage.entity.MileageSaveEntity;
import kr.co.homeplus.mileage.enums.MysqlFunction;
import kr.co.homeplus.mileage.external.model.ExternalMyPageSetDto;
import kr.co.homeplus.mileage.entity.MileageHistoryEntity;
import kr.co.homeplus.mileage.entity.MileageTypeEntity;
import kr.co.homeplus.mileage.utils.EntityFactory;
import kr.co.homeplus.mileage.utils.ObjectUtils;
import kr.co.homeplus.mileage.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MileageExternalSql {

    public static String selectExternalType(@Param("mileageCategory") String mileageCategory, @Param("mileageKind") String mileageKind) {
        MileageTypeEntity mileageType = EntityFactory.createEntityIntoValue(MileageTypeEntity.class);
        SQL query = new SQL()
            .SELECT( mileageType.mileageTypeNo )
            .FROM(EntityFactory.getTableName(mileageType))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageType.mileageCategory, mileageCategory),
                SqlUtils.equalColumnByInput(mileageType.mileageKind, mileageKind),
                SqlUtils.equalColumnByInput(mileageType.useYn, "Y"),
                SqlUtils.between("NOW()", mileageType.regStartDt, mileageType.regEndDt)
            )
            .ORDER_BY(mileageType.mileageTypeNo.concat(" DESC"))
            .LIMIT(1)
            .OFFSET(0);
        log.debug("selectExternalType \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileageCodeExternalType(@Param("mileageCode") String mileageCode) {
        MileageTypeEntity mileageType = EntityFactory.createEntityIntoValue(MileageTypeEntity.class);
        SQL query = new SQL()
            .SELECT( mileageType.mileageTypeNo )
            .FROM(EntityFactory.getTableName(mileageType))
            .WHERE(
                SqlUtils.between("NOW()", mileageType.regStartDt, mileageType.regEndDt),
                SqlUtils.equalColumnByInput(mileageType.mileageCode, mileageCode),
                SqlUtils.equalColumnByInput(mileageType.useYn, "Y")
            )
            .ORDER_BY(mileageType.mileageTypeNo.concat(" DESC"))
            .LIMIT(1)
            .OFFSET(0);
        log.debug("selectMileageCodeExternalType \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectExternalMileageList(ExternalMyPageSetDto setDto) {
        MileageHistoryEntity mileageHistory = EntityFactory.createEntityIntoValue(MileageHistoryEntity.class, Boolean.TRUE);
        MileageSaveEntity mileageSave = EntityFactory.createEntityIntoValue(MileageSaveEntity.class, Boolean.TRUE);
        SQL innerQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(mileageHistory.regDt, "%Y.%m.%d"), "issue_dt"),
                mileageHistory.mileageCategory,
                mileageHistory.mileageKind,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.AND(
                            SqlUtils.equalColumnByInput(mileageHistory.mileageCategory, "01"),
                            SqlUtils.equalColumnByInput(mileageHistory.mileageKind, "05")
                        ),
                        "NULL",
                        SqlUtils.caseWhenElse(
                            mileageHistory.mileageCategory,
                            ObjectUtils.toArray(
                                "'01'", mileageSave.requestReason
                            ),
                            "NULL"
                        )
                    ),
                    mileageSave.requestReason
                ),
                mileageSave.expireDt,
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, mileageHistory.tradeNo), 0), "''", mileageHistory.tradeNo), "trade_no"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    mileageHistory.mileageCategory,
                    ObjectUtils.toArray(
                        "'01'", "'Y'",
                        "'02'", "'N'",
                        "'03'", "'N'",
                        "'04'", "'N'",
                        "'05'", "'N'"
                    ),
                    "save_yn"
                ),
                mileageHistory.mileageAmt
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageHistory))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(mileageSave),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(mileageHistory.mileageNo, mileageSave.mileageSaveNo),
                        SqlUtils.equalColumnByInput(mileageHistory.mileageCategory, "01")
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(mileageHistory.userNo, setDto.getUserNo()),
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, mileageHistory.mileageCategory, "03"),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, mileageHistory.mileageAmt, "0"),
                SqlUtils.betweenDay(mileageHistory.regDt, setDto.getSchStartDt(), setDto.getSchEndDt())
            )
            .ORDER_BY(SqlUtils.orderByesc(mileageHistory.regDt, mileageHistory.mileageHistoryNo))
            ;

        if(!setDto.getSchType().equalsIgnoreCase("A")){
            if(setDto.getSchType().equals("2")){
                innerQuery.AND()
                    .WHERE(
                        SqlUtils.equalColumnByInput(mileageHistory.mileageCategory, "01")
                    );
            } else {
                innerQuery.AND()
                    .WHERE(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, mileageHistory.mileageCategory, "'01'")
                    );
            }
        }

        SQL query = new SQL()
            .SELECT(
                "issue_dt",
                SqlUtils.sqlFunction(
                    MysqlFunction.MILEAGE_COMMON_TABLE,
                    mileageHistory.mileageCategory,
                    ObjectUtils.toArray(
                        mileageHistory.mileageCategory
                    ),
                    "mileage_title"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CONCAT,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, SqlUtils.nonAlias(mileageSave.requestReason)),
                                SqlUtils.nonAlias(mileageSave.requestReason),
                                SqlUtils.sqlFunction(
                                    MysqlFunction.MILEAGE_UPPER_COMMON_TABLE,
                                    ObjectUtils.toArray(
                                        SqlUtils.nonAlias(mileageHistory.mileageKind), mileageHistory.mileageKind,
                                        SqlUtils.nonAlias(mileageHistory.mileageCategory), mileageHistory.mileageCategory
                                    )
                                )
                            )
                        ),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.equalColumnByInput("save_yn", "Y"),
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CONCAT,
                                    ObjectUtils.toArray(
                                        "' ('",
                                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.nonAlias(mileageSave.expireDt),CustomConstants.YYYYMMDD_COMMA)),
                                        "'까지 사용 가능)'"
                                    )
                                ),
                                "''"
                            )
                        )
                    ),
                    "mileage_message"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.AND(
                            SqlUtils.sqlFunction(MysqlFunction.NOT_IN, mileageHistory.mileageCategory, ObjectUtils.toArray("'04'", "'05'")),
                            SqlUtils.sqlFunction(MysqlFunction.NOT_IN, mileageHistory.mileageKind, ObjectUtils.toArray("'11'", "'12'"))
                        ),
                        mileageHistory.tradeNo, "''"
                    ),
                    "trade_no"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(SqlUtils.equalColumnByInput("save_yn", "Y"), mileageHistory.mileageAmt, SqlUtils.customOperator(CustomConstants.MULTIPLY, mileageHistory.mileageAmt, "-1")),
                    "mileage_amt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(SqlUtils.equalColumnByInput("save_yn", "Y"), '2', '1'),
                    "mileage_use_type"
                )
            )
            .FROM(
                SqlUtils.subTableQuery(innerQuery, "mh")
            );

        if(setDto.getPage() > 0){
            query
                .LIMIT(setDto.getPerPage())
                .OFFSET((setDto.getPage() - 1) * setDto.getPerPage());
        }
        log.debug("selectExternalMileageList \n[{}]", SqlUtils.orQuery(query.toString()));
        return SqlUtils.orQuery(query.toString());
    }

    public static String selectAlreadySaveCheck(@Param("mileageCode") String mileageCode, @Param("tradeNo") String tradeNo, @Param("userNo") String userNo) {
        MileageHistoryEntity mileageHistory = EntityFactory.createEntityIntoValue(MileageHistoryEntity.class, Boolean.TRUE);
        MileageTypeEntity mileageType = EntityFactory.createEntityIntoValue(MileageTypeEntity.class, Boolean.TRUE);
        MileageSaveEntity mileageSave = EntityFactory.createEntityIntoValue(MileageSaveEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.CASE_Y_N, SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.sqlFunction(MysqlFunction.COUNT, "1"), "0"), "CNT")
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageHistory))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageSave, ObjectUtils.toArray(mileageHistory.userNo), ObjectUtils.toArray(SqlUtils.equalColumn(mileageHistory.mileageNo, mileageSave.mileageSaveNo))))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageType, ObjectUtils.toArray(mileageSave.mileageTypeNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageHistory.tradeNo, tradeNo),
                SqlUtils.equalColumnByInput(mileageHistory.userNo, userNo),
                SqlUtils.equalColumnByInput(mileageType.mileageCode, mileageCode)
            );
        log.debug("selectAlreadySaveCheck \n[{}]", query.toString());
        return query.toString();
    }

}
