package kr.co.homeplus.mileage.management.model.cancel;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CancelMileageReqDto {

    @ApiModelProperty(value = "고객번호", position = 1)
    private String userNo;

    @ApiModelProperty(value = "취소금액", position = 2)
    @NotNull(message = "금액을 입력해주세요.")
    @Size(min = 1, message = "취소금액")
    private long requestCancelAmt;

    @ApiModelProperty(value = "취소 거래번호", position = 3)
    @NotNull(message = "취소 거래번호")
    @Length(max = 20, message = "20자까지 입력가능합니다.")
    private String tradeNo;

    @ApiModelProperty(value = "취소내역 상세메시지", position = 4)
    @Size(max = 100, message = "입력할 수 있는 최대 사이즈를 초과하였습니다.")
    private String cancelMessage;

    @ApiModelProperty(value = "등록아이디", position = 5)
    private String regId;
}
