package kr.co.homeplus.mileage.office.model.type;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마일리지 타입 수정 DTO")
public class MileageTypeManageModifyDto {

    @ApiModelProperty(value = "마일리지 유형번호", position = 1)
    @NotNull(message = "마일리지 유형번호가 없습니다.")
    private String mileageTypeNo;

    @ApiModelProperty(value = "마일리지 유형(명칭)", position = 2)
    @NotNull(message = "마일리지유형이 없습니다.")
    private String mileageTypeName;

    @ApiModelProperty(value = "마일리지 유형", position = 3)
    @NotNull(message = "마일리지유형이 없습니다.")
    private String mileageCategory;

    @ApiModelProperty(value = "마일리지 종류", position = 4)
    @NotNull(message = "마일리지종류이 없습니다.")
    private String mileageKind;

    @ApiModelProperty(value = "점포유형", position = 5)
    @NotNull(message = "점포유형이 없습니다.")
    private String storeType;

    @ApiModelProperty(value = "마일리지등록기간(시작일)", position = 6)
    @NotNull(message = "마일리지등록기간(시작일)이 없습니다.")
    private String regStartDt;

    @ApiModelProperty(value = "마일리지등록기간(종료일)", position = 7)
    @NotNull(message = "마일리지등록기간(종료일)이 없습니다.")
    private String regEndDt;

    @ApiModelProperty(value = "유효기간타입(TR:등록일로부터, TT:기간으로붙터)", position = 8)
    private String expireType;

    @ApiModelProperty(value = "유효기간일(TR 일때)", position = 9)
    private String expireDayInp;

    @ApiModelProperty(value = "사용가능기간(시작일, TT 일때)", position = 10)
    private String useStartDt;

    @ApiModelProperty(value = "사용가능기간(종료일, TT 일때)", position = 11)
    private String useEndDt;

    @ApiModelProperty(value = "마일리지타입 설명", position = 12)
    private String mileageTypeExplain;

    @ApiModelProperty(value = "마일리지코드", position = 13)
    private String mileageCode;

    @ApiModelProperty(value = "사용유무", position = 14)
    private String useYn;

    @ApiModelProperty(value = "등록아이디", position = 15)
    private String regId;

    @ApiModelProperty(value = "고객노출문구", position = 16)
    private String displayMessage;
}
