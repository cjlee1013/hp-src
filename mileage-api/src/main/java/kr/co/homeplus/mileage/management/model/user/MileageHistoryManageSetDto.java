package kr.co.homeplus.mileage.management.model.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MileageHistoryManageSetDto {

    @ApiModelProperty(value = "고객번호", position = 1)
    private String userNo;

    @ApiModelProperty(value = "마일리지유형", position = 2)
    private String mileageCategory;

    @ApiModelProperty(value = "마일리지종류", position = 3)
    private String mileageKind;

    @ApiModelProperty(value = "마일리지 사용/적립번호", position = 4)
    private long mileageNo;

    @ApiModelProperty(value = "거래번호", position = 5)
    @Length(max = 20, message = "20자까지 입력가능합니다.")
    private String tradeNo;

    @ApiModelProperty(value = "사용/적립금액", position = 6)
    private long mileageAmt;

    @ApiModelProperty(value = "사용/적립내역 메시지", position = 7)
    private String historyMessage;

    @ApiModelProperty(value = "기타메시지", position = 8)
    private String historyEtcMessage;
}
