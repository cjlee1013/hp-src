package kr.co.homeplus.mileage.office.service;

import java.text.MessageFormat;
import java.util.List;
import kr.co.homeplus.mileage.core.exception.MileageLogicException;
import kr.co.homeplus.mileage.management.model.user.MileageMstDto;
import kr.co.homeplus.mileage.management.service.ManagementMapperService;
import kr.co.homeplus.mileage.management.service.UserMileageService;
import kr.co.homeplus.mileage.office.model.history.MileageHistoryResDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.mileage.enums.ActivityMileageCode;
import kr.co.homeplus.mileage.office.model.history.MileageHistoryDto;
import kr.co.homeplus.mileage.office.model.history.MileageHistoryReqDto;
import kr.co.homeplus.mileage.utils.MileageCommonResponseFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.protocol.types.Field.Str;
import org.springframework.stereotype.Service;

@Service("MileageHistoryService")
@RequiredArgsConstructor
@Slf4j
public class MileageHistoryService {
    // Office Mapper Management Service
    private final OfficeMapperManageService officeMapperService;

    // Management Mapper Service
    private final ManagementMapperService managementMapperService;

    /**
     * 마일리지 내역 조회
     * @param mileageHistoryReqDto 마일리지 히스토리 조회 DTO
     * @return ResponseObject<MileagePagingResponse<List<MileageHistoryDto>>>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public ResponseObject<MileageHistoryResDto> getMileageHistoryList(MileageHistoryReqDto mileageHistoryReqDto) throws Exception {
        //마일리지 데이터 조회(페이징)
        List<MileageHistoryDto> mileageHistoryDto = officeMapperService.getMileageHistoryInfo(mileageHistoryReqDto);
        mileageHistoryDto.stream().forEach(
            dto -> dto.setHistoryMessage(MessageFormat.format("{0} (주문번호 : {1})", dto.getMileageKind(), dto.getTradeNo()))
        );
        // 마일리조회
        MileageMstDto mileageMstDto = new MileageMstDto();
        try {
            mileageMstDto = managementMapperService.getUserMileageMstInfo(String.valueOf(mileageHistoryReqDto.getUserNo()));
        } catch (Exception e) {
            log.error("마일리지 내역조회 중 마일리지 금액정보 취득 실패 :: 요청정보 [{}], 오류내용 :: {}", mileageHistoryReqDto.getUserNo(), e.getMessage());
        }

        MileageHistoryResDto resDto = new MileageHistoryResDto();
        resDto.setMileageHistoryList(mileageHistoryDto);
        resDto.setMileageAmt(mileageMstDto.getMileageAmt());
        resDto.setExpiredMileageAmt(mileageMstDto.getExpiredMileageAmt());

        return MileageCommonResponseFactory.getResponseObject(
            ActivityMileageCode.MILEAGE_SUCCESS,
            resDto
        );
    }

    /**
     * 마일리지 내역 조회(고객센터)
     * @param mileageHistoryReqDto 마일리지 히스토리 조회 DTO
     * @return ResponseObject<MileagePagingResponse<List<MileageHistoryDto>>>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public ResponseObject<List<MileageHistoryDto>> getCustomerMileageList(MileageHistoryReqDto mileageHistoryReqDto) throws Exception {
        //마일리지 데이터 조회(페이징)
        List<MileageHistoryDto> mileageHistoryDto = officeMapperService.getMileageHistoryInfo(mileageHistoryReqDto);
        mileageHistoryDto.forEach(
            dto -> dto.setHistoryMessage(MessageFormat.format("{0} (주문번호 : {1})", dto.getMileageKind(), dto.getTradeNo()))
        );
        return MileageCommonResponseFactory.getResponseObject(
            ActivityMileageCode.MILEAGE_SUCCESS,
            mileageHistoryDto
        );
    }
}
