package kr.co.homeplus.mileage.office.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponInfoDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponModifyDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponOneTimeDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponRequestDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponReturnGetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponReturnSetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponSearchGetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponSearchSetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponUserInfoDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponUserRegDto;
import kr.co.homeplus.mileage.office.service.MileageCouponService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/office/coupon")
@Api(tags = "MileageCouponController", description = "BackOffice 마일리지 쿠폰 등록/회수/조회 Controller")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
}
)
@RequiredArgsConstructor
public class MileageCouponController {

    private final MileageCouponService couponService;

    @ApiOperation(value = "마일리지쿠폰등록 요청", response = ResponseObject.class)
    @PostMapping("/requestMileageCouponReg")
    public ResponseObject<String> requestMileageCouponReg(@RequestBody @Valid MileageCouponRequestDto requestDto) throws Exception {
        return couponService.requestMileageCouponReg(requestDto);
    }

    @ApiOperation(value = "마일리지쿠폰수 요청", response = ResponseObject.class)
    @PostMapping("/requestMileageCouponModify")
    public ResponseObject<String> requestMileageCouponModify(@RequestBody @Valid MileageCouponModifyDto modifyDto) throws Exception {
        return couponService.requestMileageCouponModify(modifyDto);
    }

    @ApiOperation(value = "마일리지쿠폰등록유저", response = ResponseObject.class)
    @PostMapping("/userMileageCouponReg")
    public ResponseObject<String> userMileageCouponReg(@RequestBody @Valid MileageCouponUserRegDto regDto) throws Exception {
        return couponService.getMileageCouponUserRegister(regDto);
    }

    @ApiOperation(value = "마일리지난수번호관리 조회", response = MileageCouponSearchGetDto.class)
    @PostMapping("/mileageCouponManageList")
    public ResponseObject<List<MileageCouponSearchGetDto>> getMileageCouponManageList(@RequestBody MileageCouponSearchSetDto setDto) throws Exception {
        return couponService.getCouponManageInfoList(setDto);
    }

    @GetMapping("/mileageManageInfo/{couponManageNo}")
    @ApiOperation(value = "마일리지난수번호관리-쿠폰정보조회", response = MileageCouponSearchGetDto.class)
    public ResponseObject<MileageCouponInfoDto> getMileageCouponInfo(@PathVariable("couponManageNo") long couponManageNo) throws Exception {
        return couponService.getCouponManageInfo(couponManageNo);
    }

    @GetMapping("/mileageCouponUserInfo/{couponManageNo}")
    @ApiOperation(value = "마일리지난수번호관리-발급회원", response = MileageCouponSearchGetDto.class)
    public ResponseObject<List<MileageCouponUserInfoDto>> getMileageCouponUserInfo(@PathVariable("couponManageNo") long couponManageNo) throws Exception {
        return couponService.getCouponUserInfoList(couponManageNo);
    }

    @ApiOperation(value = "마일리지쿠폰회수 요청", response = ResponseObject.class)
    @PostMapping("/requestMileageCouponReturn")
    public ResponseObject<MileageCouponReturnGetDto> requestMileageCouponReturn(@RequestBody @Valid MileageCouponReturnSetDto setDto) throws Exception {
        return couponService.requestMileageCouponReturn(setDto);
    }

    @GetMapping("/mileageCouponInfo/down/{couponManageNo}")
    @ApiOperation(value = "마일리지난수번호관리-발급회원", response = MileageCouponSearchGetDto.class)
    public ResponseObject<List<MileageCouponOneTimeDto>> getCouponOneTimeInfoDown(@PathVariable("couponManageNo") long couponManageNo) throws Exception {
        return couponService.getCouponOneTimeInfoDown(couponManageNo);
    }



}
