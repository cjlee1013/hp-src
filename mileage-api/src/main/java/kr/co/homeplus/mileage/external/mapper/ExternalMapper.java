package kr.co.homeplus.mileage.external.mapper;

import java.util.List;
import kr.co.homeplus.mileage.core.db.annotation.SlaveConnection;
import kr.co.homeplus.mileage.external.model.ExternalMyPageGetDto;
import kr.co.homeplus.mileage.external.model.ExternalMyPageSetDto;
import kr.co.homeplus.mileage.sql.MileageExternalSql;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

@SlaveConnection
public interface ExternalMapper {

    @SelectProvider(type = MileageExternalSql.class, method = "selectExternalType")
    String selectExternalType(@Param("mileageCategory") String mileageCategory, @Param("mileageKind") String mileageKind);

    @SelectProvider(type = MileageExternalSql.class, method = "selectMileageCodeExternalType")
    String selectMileageCodeExternalType(@Param("mileageCode") String mileageCode);

    @SelectProvider(type = MileageExternalSql.class, method = "selectExternalMileageList")
    List<ExternalMyPageGetDto> selectExternalMileageList(ExternalMyPageSetDto externalMyPageSetDto);

    @SelectProvider(type = MileageExternalSql.class, method = "selectAlreadySaveCheck")
    String selectAlreadySaveCheck(@Param("mileageCode") String mileageCode, @Param("tradeNo") String tradeNo, @Param("userNo") String userNo);

}
