package kr.co.homeplus.mileage.management.service;

import java.util.HashMap;
import java.util.List;
import kr.co.homeplus.mileage.enums.MileageCommonCode;
import kr.co.homeplus.mileage.management.model.cancel.CancelSaveDto;
import kr.co.homeplus.mileage.management.model.use.MileageManageDto;
import kr.co.homeplus.mileage.management.model.user.MileageHistoryManageSetDto;
import kr.co.homeplus.mileage.management.model.user.MileageMstDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.mileage.management.model.cancel.CancelMileageReqDto;
import kr.co.homeplus.mileage.management.model.save.SaveMileageDto;
import kr.co.homeplus.mileage.core.exception.MileageLogicException;
import kr.co.homeplus.mileage.enums.ActivityMileageCode;
import kr.co.homeplus.mileage.utils.MileageCommonUtil;
import kr.co.homeplus.mileage.utils.MileageCommonResponseFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 마일리지 적립/사용 취소 Service
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CancelMileageService {
    // Management Mapper Service
    private final ManagementMapperService managementMapperService;
    //적립 Service
    private final SaveMileageService saveMileageService;

     /**
     * 마일리지 사용 취소 프로세스
     * @param cancelMileageReqDto 사용취소 정보 DTO
     * @return ResponseObject<String> 응답정보
     * @throws Exception 예외가 발생할 경우 던짐
     */
    @Transactional(rollbackFor = {Exception.class, MileageLogicException.class})
    public ResponseObject<MileageMstDto> setCancelMileageRestore(CancelMileageReqDto cancelMileageReqDto) throws Exception {
        log.info("거래번호({}) 마일리지 취소 요청 :: [{}]", cancelMileageReqDto.getTradeNo(), cancelMileageReqDto);
        if(cancelMileageReqDto.getRequestCancelAmt() < 0){
            return MileageCommonResponseFactory.getResponseObject(
                ActivityMileageCode.MILEAGE_CANCEL_ERR_10,
                MileageMstDto.builder().approvalNo(ActivityMileageCode.MILEAGE_CANCEL_ERR_10.getResponseCode()).build()
            );
        }

        // 마일리지 마스터 DB 에 고객정보가 있는지 확인
        if (!managementMapperService.checkUserMileageMst(cancelMileageReqDto.getUserNo())) {
            return MileageCommonResponseFactory.getResponseObject(
                ActivityMileageCode.MILEAGE_CANCEL_ERR_01,
                MileageMstDto.builder().approvalNo(ActivityMileageCode.MILEAGE_CANCEL_ERR_01.getResponseCode()).build()
            );
        }

        // 마일리지 사용건이 있는지 확인(유저관리번호, 사용시 사용한 마일리지번호)
        if(!managementMapperService.getMileageUsedCheck(cancelMileageReqDto.getUserNo(), cancelMileageReqDto.getTradeNo())){
            return MileageCommonResponseFactory.getResponseObject(
                ActivityMileageCode.MILEAGE_CANCEL_ERR_02,
                MileageMstDto.builder().approvalNo(ActivityMileageCode.MILEAGE_CANCEL_ERR_02.getResponseCode()).build()
            );
        }

        // 사용에 대한 관리 데이터 조회
        MileageManageDto userUsedInfo = managementMapperService.getUseCancelInfoList(cancelMileageReqDto.getUserNo(), cancelMileageReqDto.getTradeNo());
        log.info("거래번호({}) 마일리지 사용취소 정보:: [{}]", cancelMileageReqDto.getTradeNo(), userUsedInfo);
        // 데이터가 없는 경우 종료처리됨.
        if(userUsedInfo == null){
            return MileageCommonResponseFactory.getResponseObject(
                ActivityMileageCode.MILEAGE_CANCEL_ERR_08,
                MileageMstDto.builder().approvalNo(ActivityMileageCode.MILEAGE_CANCEL_ERR_08.getResponseCode()).build()
            );
        }

        // 취소요청금액이 전체 사용금액보다 적으면 오류.
        // 부분취소시에는 남은 사용금액보다 큰 금앨이 들어오면 오류.
        if(userUsedInfo.getCancelRemainAmt() < cancelMileageReqDto.getRequestCancelAmt()){
            return MileageCommonResponseFactory.getResponseObject(
                ActivityMileageCode.MILEAGE_CANCEL_ERR_07,
                MileageMstDto.builder().approvalNo(ActivityMileageCode.MILEAGE_CANCEL_ERR_07.getResponseCode()).build()
            );
        }

        // 전체 취소 여부에 따라 로직 처리 필요.
        List<CancelSaveDto> createCancelSaveInfo = managementMapperService.createMileageSaveInfoFromUsed(cancelMileageReqDto.getUserNo(), cancelMileageReqDto.getTradeNo());

        //가능건수가 한건도 없으면
        if(createCancelSaveInfo.size() == 0){
            return MileageCommonResponseFactory.getResponseObject(
                ActivityMileageCode.MILEAGE_CANCEL_ERR_08,
                MileageMstDto.builder().approvalNo(ActivityMileageCode.MILEAGE_CANCEL_ERR_08.getResponseCode()).build()
            );
        }

        // 취소처리 예정 금액.
        long cancelSuccessAmt = cancelMileageReqDto.getRequestCancelAmt();

        for(CancelSaveDto cancelSaveDto : createCancelSaveInfo){
            log.info("거래번호({}) 마일리지 사용취소 정보:: [{}]", cancelMileageReqDto.getTradeNo(), cancelSaveDto);
            // chg_id set
            cancelSaveDto.setChgId(cancelMileageReqDto.getRegId());
            //reg_id Set
            cancelSaveDto.setRegId(cancelMileageReqDto.getRegId());
            // 마일리지유형을 취소타입으로 변경.
            cancelSaveDto.setMileageCategory(MileageCommonCode.MILEAGE_CATEGORY_CANCEL.getResponseCode());

            boolean cancelYn = Boolean.TRUE;

            //전체취소금액이 취소가능금액보다 적을 경우.
            if(cancelSuccessAmt < cancelSaveDto.getCancelRemainAmt()){
                cancelSaveDto.setCancelRemainAmt(cancelSuccessAmt);
                cancelSaveDto.setMileageAmt(cancelSuccessAmt);
                cancelYn = Boolean.FALSE;
            }

            // 마일리지 관리 업데이트 맵 생성.
            // (cancelSaveDto 정보를 기반으로 생성.)
            HashMap<String, Object> manageUpdateMap  = MileageCommonUtil.getConvertMap(cancelSaveDto);

            SaveMileageDto saveMileageDto = new SaveMileageDto();
            BeanUtils.copyProperties(saveMileageDto, cancelSaveDto);
            log.info("거래번호({}) 마일리지 사용취소에 따른 재적립 정보:: [{}]", cancelMileageReqDto.getTradeNo(), saveMileageDto);
            //마일리지 적립 수행.
            if(!saveMileageService.setSaveMileage(saveMileageDto).equals("00")){
                return MileageCommonResponseFactory.getResponseObject(
                    ActivityMileageCode.MILEAGE_CANCEL_ERR_03,
                    MileageMstDto.builder().approvalNo(ActivityMileageCode.MILEAGE_CANCEL_ERR_03.getResponseCode()).build()
                );
            }
            // 취소 여부
            manageUpdateMap.put("cancelYn", (cancelYn) ? "Y" : "N");

            // 마일리지 관리 업데이트
            // 취소, 취소 잔액등.
            if(!managementMapperService.modifyMileageManageInfo(manageUpdateMap)){
                return MileageCommonResponseFactory.getResponseObject(
                    ActivityMileageCode.MILEAGE_CANCEL_ERR_09,
                    MileageMstDto.builder().approvalNo(ActivityMileageCode.MILEAGE_CANCEL_ERR_09.getResponseCode()).build()
                );
            }

            // 히스토리 DTO 생성.
            MileageHistoryManageSetDto mileageHistoryManageSetDto = MileageCommonUtil.initMileageHistorySetDto(cancelSaveDto);

            // 취소 메시지를 히스토리 메시지에 넣는다.
            mileageHistoryManageSetDto.setHistoryMessage(cancelMileageReqDto.getCancelMessage());

            // 마일리지 히스토리 테이블에 해당 기록 적재.
            if(!managementMapperService.addUserMileageHistory(mileageHistoryManageSetDto)){
                return MileageCommonResponseFactory.getResponseObject(
                    ActivityMileageCode.MILEAGE_USE_ERR_06,
                    MileageMstDto.builder().approvalNo(ActivityMileageCode.MILEAGE_USE_ERR_06.getResponseCode()).build()
                );
            }

            // 토탑취소금액.
            cancelSuccessAmt -= cancelSaveDto.getCancelRemainAmt();

            if(cancelSuccessAmt == 0){
                break;
            }
        }

        MileageMstDto returnDto = managementMapperService.getUserMileageMstInfoFromWrite(cancelMileageReqDto.getUserNo());
        returnDto.setApprovalNo("0000");

        return MileageCommonResponseFactory.getResponseObject(ActivityMileageCode.MILEAGE_CANCEL_SUCCESS, returnDto);
    }
}
