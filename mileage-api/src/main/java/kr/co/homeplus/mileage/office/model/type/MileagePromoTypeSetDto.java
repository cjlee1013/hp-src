package kr.co.homeplus.mileage.office.model.type;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@ApiModel(description = "마일리지 행사 관련 타입 조회 DTO")
public class MileagePromoTypeSetDto {
    @NotNull
    @ApiModelProperty(value = "점포유형", position = 1)
    private String storeType;
    @NotNull
    @ApiModelProperty(value = "검색유형", position = 2)
    private String schTypeCd;
    @ApiModelProperty(value = "검색어", position = 3)
    private String schTypeContents;
}
