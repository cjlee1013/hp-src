package kr.co.homeplus.mileage.external.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Data;

@Data
@ApiModel(description = "외부연동 > 마이페이지 > 마일리지 적립/차감 조회요청")
public class ExternalMyPageSetDto {

    @ApiModelProperty(value = "조회시작일", reference = "YYYY-MM-DD", required = true, position = 1)
    @NotEmpty(message = "조회시작일")
    @NotNull(message = "조회시작일")
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", reference = "YYYY-MM-DD", required = true, position = 2)
    @NotEmpty(message = "조회종료일")
    @NotNull(message = "조회종료일")
    private String schEndDt;

    @ApiModelProperty(value = "유저번호", required = true, position = 3)
    private String userNo;

    @ApiModelProperty(value = "조회구분(A:전체,1:사용.2:적립)", required = true, position = 4)
    @Pattern(regexp = "A|1|2", message = "조회구분")
    private String schType;

    @ApiModelProperty(value = "페이지번호", required = true, position = 5)
    @Min(value = 1, message = "페이지")
    private int page;

    @ApiModelProperty(value = "페이지당노출수", required = true, position = 6)
    @Min(value = 1, message = "페이지당노출수")
    private int perPage;
}
