package kr.co.homeplus.mileage.sql;

import java.util.Arrays;
import java.util.HashMap;
import kr.co.homeplus.mileage.constants.Constants;
import kr.co.homeplus.mileage.constants.CustomConstants;
import kr.co.homeplus.mileage.constants.SqlPatternConstants;
import kr.co.homeplus.mileage.entity.MileageManageEntity;
import kr.co.homeplus.mileage.entity.MileageSaveEntity;
import kr.co.homeplus.mileage.entity.MileageUseEntity;
import kr.co.homeplus.mileage.enums.MysqlFunction;
import kr.co.homeplus.mileage.management.model.use.MileageManageDto;
import kr.co.homeplus.mileage.management.model.use.MileageUseDto;
import kr.co.homeplus.mileage.utils.EntityFactory;
import kr.co.homeplus.mileage.utils.ObjectUtils;
import kr.co.homeplus.mileage.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MileageUseSql {

    // 마일리지 사용 테이블명
    public final static String useTable = EntityFactory.getTableName(MileageUseEntity.class);
    // 마일리지 관리 테이블명
    public final static String manageTable = EntityFactory.getTableName(MileageManageEntity.class);
    // 마일리지 사용 테이블명
    public final static String useAliasTable = EntityFactory.getTableNameWithAlias(MileageUseEntity.class);
    // 마일리지 관리 테이블명
    public final static String manageAliasTable = EntityFactory.getTableNameWithAlias(MileageManageEntity.class);

    /**
     * 마일리지 사용 등록
     * 
     * @param mileageUseDto 마일리지 사용등록 DTO
     * @return String 마일리지 사용등록 Query
     */
    public static String insertUserMileageUse(MileageUseDto mileageUseDto){
        MileageUseEntity useColumns = EntityFactory.createEntityIntoValue(MileageUseEntity.class, Boolean.FALSE);
        SQL query = new SQL()
            .INSERT_INTO(useTable)
            .INTO_COLUMNS(EntityFactory.getColumnInfo(MileageUseEntity.class))
            .INTO_VALUES(
                SqlUtils.convertInsertIntoValue(
                    mileageUseDto.getMileageUseNo(),
                    mileageUseDto.getUserNo(),
                    mileageUseDto.getTradeNo(),
                    mileageUseDto.getMileageCategory(),
                    mileageUseDto.getMileageKind(),
                    mileageUseDto.getMileageAmt(),
                    mileageUseDto.getUseMessage(),
                    SqlPatternConstants.NOW,
                    mileageUseDto.getRegId(),
                    SqlPatternConstants.NOW,
                    mileageUseDto.getChgId()
                )
            );

        log.info("insertUserMileageUse \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 관리 등록
     *
     * @param mileageManageDto 마일리지 관리 등록 DTO
     * @return String 마일리지 관리 등록 Query.
     */
    public static String insertMileageManageInfo(MileageManageDto mileageManageDto){
        String[] manageColumnInfo = EntityFactory.getColumnInfo(MileageManageEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(manageTable)
            .INTO_COLUMNS(manageColumnInfo)
            .INTO_VALUES(SqlUtils.convertInsertParamToCamelCase(manageColumnInfo));

        log.debug("insertMileageManageInfo \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 사용건 존재 여부 확인
     *
     * @param userNo 고객관리번호
     * @param tradeNo 거래번호
     * @return String 마일리지 사용건 존재 여부 확인 Query.
     * @throws Exception 오류시 오류처리.
     */
    public static String selectUserMileageUsedCheck(@Param("userNo") String userNo, @Param("tradeNo") String tradeNo) throws Exception {
        MileageUseEntity useColumns = EntityFactory.createEntityIntoValue(MileageUseEntity.class, Boolean.FALSE);
        
        SQL query = new SQL()
            .SELECT(SqlPatternConstants.getConditionByPatternWithAlias("COUNT", "CNT", "1"))
            .FROM(useTable)
            .WHERE(
                SqlUtils.equalColumnByInput(useColumns.userNo, userNo),
                SqlUtils.equalColumnByInput(useColumns.tradeNo, tradeNo)
            );

        log.debug("selectUserMileageUsedCheck \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 사용 정보 조회
     *
     * @param userNo 고객관리번호
     * @param tradeNo 거래번호
     * @return String 마일리지 사용 정보 조회 Query.
     * @throws Exception 오류시 오류처리.
     */
    public static String selectUseMileageUsedInfo(@Param("userNo") String userNo, @Param("tradeNo") String tradeNo) throws Exception {
        MileageUseEntity mu = EntityFactory.createEntityIntoValue(MileageUseEntity.class, Boolean.TRUE);
        MileageManageEntity mm = EntityFactory.createEntityIntoValue(MileageManageEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                SqlPatternConstants.getConditionByPatternWithAlias("MAX", "userNo", mm.userNo),
                SqlPatternConstants.getConditionByPatternWithAlias("SUM", "mileageAmt", mm.cancelRemainAmt),
                SqlPatternConstants.getConditionByPatternWithAlias("SUM", "cancelRemainAmt", mm.cancelRemainAmt)
            )
            .FROM(manageAliasTable)
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    useAliasTable,
                    Arrays.asList(
                        // 동일컬럼으로 맵핑하는 쿼리 생성.
                        // onClause(new Object[], Object)
                        // onClause(컬럼정보, 맵핑되는 컬럼의 instance)
                        SqlUtils.onClause(
                            new Object[]{
                                mu.mileageUseNo,
                                mu.userNo
                            },
                            mm
                        )
                    ),
                    null
                )
            )
            //mu.user_no
            .WHERE(
                SqlUtils.equalColumnMapping(mu.userNo),
                SqlUtils.equalColumnMapping(mu.tradeNo),
                SqlUtils.equalColumnByInput(mm.cancelYn, Constants.USE_N)
            );
        log.debug("selectUseMileageUsedInfo \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 사용취소시 마일리지 적립 Entity 쿼리
     *
     * @param userNo 고객관리번호
     * @param tradeNo 거래번호.
     * @return String 사용취소시 마일리지 적립 Entity Query.
     */
    public static String selectUseCancelMileageInfo(@Param("userNo") String userNo, @Param("tradeNo") String tradeNo){
        // 복합쿼리를 위한 컬럼들의 alias 컬럼화.
        MileageUseEntity mileageUse = EntityFactory.createEntityIntoValue(MileageUseEntity.class, Boolean.TRUE);
        MileageSaveEntity mileageSave = EntityFactory.createEntityIntoValue(MileageSaveEntity.class, Boolean.TRUE);
        MileageManageEntity mileageManage = EntityFactory.createEntityIntoValue(MileageManageEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(new String[]{
                mileageSave.mileageReqDetailNo,         //mileage_req_detail_no
                mileageSave.mileageTypeNo,         //mileage_type_no
                mileageUse.userNo,          //user_no
                mileageSave.mileageCategory,         //mileage_category
                mileageUse.mileageKind,         //mileage_kind
                SqlUtils.aliasToRow(mileageManage.cancelRemainAmt, "mileageAmt"),
                SqlUtils.aliasToRow(mileageManage.cancelRemainAmt, "cancelRemainAmt"),
                mileageSave.useStartDt,         //use_start_dt
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, "NOW()", mileageSave.expireDt),
                        SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(mileageSave.expireDt, SqlUtils.sqlFunction(MysqlFunction.DATEDIFF, ObjectUtils.toArray("NOW()", mileageSave.expireDt)))),
                        mileageSave.expireDt
                    ),
                    mileageSave.expireDt
                ),          //expire_dt
                mileageManage.mileageManageNo,       //mileage_manage_no
                mileageUse.tradeNo,           //trade_no
                mileageSave.requestReason
            })
            .FROM(useAliasTable)
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageManage, ObjectUtils.toArray(mileageUse.mileageUseNo, mileageUse.userNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageSave, ObjectUtils.toArray(mileageManage.mileageSaveNo, mileageManage.userNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageUse.userNo, userNo),
                SqlUtils.equalColumnByInput(mileageUse.tradeNo, tradeNo),
                SqlUtils.equalColumnByInput(mileageManage.cancelYn, Constants.USE_N),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, mileageManage.cancelRemainAmt, "0")
            )
            .ORDER_BY(mileageSave.expireDt, mileageManage.mileageManageNo)
            ;

        log.debug("selectUseCancelMileageInfo \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 관리 취소금액 업데이트
     *
     * @param parameterMap 마일리지 관리 업데이트 Map
     * @return String 마일리지 관리 취소금액 업데이트 Query
     */
    public static String updateMileageManageInfo(HashMap<String, Object> parameterMap){
        SQL query = new SQL();
        // cancelYn이 있어야 업데이트함.
        if(parameterMap.get("cancelYn") != null){
            MileageManageEntity mileageManage = EntityFactory.createEntityIntoValue(MileageManageEntity.class);
            query.UPDATE(manageTable)
                // cancel_yn = #{cancelYn}
                .SET(SqlUtils.equalColumnMapping(mileageManage.cancelYn))
                //cancel_remain_amt = cancel_remain_amt - #{cancelRemainAmt}
                .SET(SqlUtils.conditionForUpdateCalculation(mileageManage.cancelRemainAmt, "-"))
                //chg_dt = NOW()
                .SET(SqlUtils.nonSingleQuote(mileageManage.chgDt, "NOW()"))
                //chg_id = #{chgId}
                .SET(SqlUtils.equalColumnMapping(mileageManage.chgId))
                //mileage_manage_no = #{mileageManageNo}
                .WHERE(SqlUtils.equalColumnMapping(mileageManage.mileageManageNo));
        }

        log.debug("updateMileageManageInfo \n[{}]", query.toString());
        return query.toString();
    }
}
