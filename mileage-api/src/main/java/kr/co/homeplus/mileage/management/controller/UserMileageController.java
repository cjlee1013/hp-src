package kr.co.homeplus.mileage.management.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.mileage.management.model.user.MileageHistoryManageGetDto;
import kr.co.homeplus.mileage.management.model.user.UserSearchHistoryReqDto;
import kr.co.homeplus.mileage.management.model.user.UserSearchHistoryResDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.mileage.management.model.user.MileageMstDto;
import kr.co.homeplus.mileage.management.service.UserMileageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/user")
@Api(tags = "UserMileageController", value = "마일리지 회원 관리 Controller")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
}
)
@RequiredArgsConstructor
public class UserMileageController {

    //유저 마일리지 계정 정보 Service
    private final UserMileageService mileageService;

    @ApiOperation(value = "유저 마일리지 계정 등록")
    @GetMapping(value = "/mileageUserRegister")
    public ResponseObject<String> addUserMileageAccountReg(@RequestParam(value = "userNo") String userNo) throws Exception {
        return mileageService.addUserMileageAccountReg(userNo);
    }

    @ApiOperation(value = "유저 마일리지 계정 확인 및 잔액 조회")
    @GetMapping(value = "/userMileageCheck")
    public ResponseObject<MileageMstDto> checkUserMileageAccountBalance(@RequestParam(value = "userNo") String userNo) throws Exception{
        return mileageService.checkUserMileageAccountBalance(userNo);
    }

    @ApiOperation(value = "유저 마일리지 히스토리")
    @PostMapping(value = "/userMileageList")
    public ResponseObject<UserSearchHistoryResDto<List<MileageHistoryManageGetDto>>> getUserMileageHistory(@RequestBody UserSearchHistoryReqDto userSearchHistoryReqDto) throws Exception{
        return mileageService.getUserMileageHistory(userSearchHistoryReqDto);
    }



}
