package kr.co.homeplus.mileage.office.model.coupon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "쿠폰 1회성 정보 다운로드")
public class MileageCouponOneTimeDto {
    @ApiModelProperty(value = "쿠폰번호")
    private String couponNo;
    @ApiModelProperty(value = "쿠폰사용여부")
    private String couponUseYn;
}
