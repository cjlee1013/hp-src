package kr.co.homeplus.mileage.sql;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.stream.Collectors;
import kr.co.homeplus.mileage.constants.Constants;
import kr.co.homeplus.mileage.constants.SqlPatternConstants;
import kr.co.homeplus.mileage.entity.EmployeeInfoEntity;
import kr.co.homeplus.mileage.entity.MileageCouponInfoEntity;
import kr.co.homeplus.mileage.entity.MileageReqEntity;
import kr.co.homeplus.mileage.entity.MileageSaveEntity;
import kr.co.homeplus.mileage.enums.MileageCommonCode;
import kr.co.homeplus.mileage.enums.MysqlFunction;
import kr.co.homeplus.mileage.entity.MileageTypeEntity;
import kr.co.homeplus.mileage.office.model.type.MileagePromoTypeSetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeManageModifyDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeManageSetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeSearchSetDto;
import kr.co.homeplus.mileage.utils.EntityFactory;
import kr.co.homeplus.mileage.utils.MileageCommonUtil;
import kr.co.homeplus.mileage.utils.ObjectUtils;
import kr.co.homeplus.mileage.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MileageTypeSql {

    // 마일리지 유형 테이블명.
    public final static String typeTable = EntityFactory.getTableNameWithAlias(MileageTypeEntity.class);
    // 마일리지 유형 테이블 컬럼.
    public final static MileageTypeEntity mileageType = EntityFactory.createEntityIntoValue(MileageTypeEntity.class, Boolean.TRUE);

    /**
     * 마일리지 유형 조회
     *
     * @param mileageTypeSearchSetDto 마일리지 유형 조회 파라미터 DTO
     * @return String 마일리지 유형 조회 Query
     * @throws Exception 오류 발생시 오류처리.
     */
    public static String selectMileageTypeInfo(MileageTypeSearchSetDto mileageTypeSearchSetDto) throws Exception {
        EmployeeInfoEntity employeeInfo = EntityFactory.createEntityIntoValue(EmployeeInfoEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                mileageType.mileageTypeNo,
                mileageType.mileageCode,
                mileageType.storeType,
                mileageType.mileageTypeName,
                "(SELECT IFNULL(mcc.code_name, '') FROM mileage_common_code mcc WHERE mcc.code_group = 'mileage_kind' AND mcc.upper_code = mt.mileage_category AND mcc.code = mt.mileage_kind AND mcc.use_yn = 'Y') AS mileageKind",
                mileageType.useYn,
                mileageType.regDt,
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(employeeInfo.name).FROM(EntityFactory.getTableNameWithAlias("dms", employeeInfo)).WHERE(SqlUtils.equalColumn(employeeInfo.empid, mileageType.regId))
                        ),
                        mileageType.regId
                    ),
                    mileageType.regId
                ),
                mileageType.chgDt,
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(employeeInfo.name).FROM(EntityFactory.getTableNameWithAlias("dms", employeeInfo)).WHERE(SqlUtils.equalColumn(employeeInfo.empid, mileageType.chgId))
                        ),
                        mileageType.chgId
                    ),
                    mileageType.chgId
                ),
                mileageType.displayMessage
            )
            .FROM(typeTable)
            .WHERE(
                SqlUtils.customOperator(
                    Constants.EQUAL,
                    mileageType.mileageCategory,
                    SqlUtils.appendSingleQuote(MileageCommonCode.MILEAGE_CATEGORY_SAVE.getResponseCode())
                )
            )
            .ORDER_BY(SqlUtils.orderByesc(mileageType.mileageTypeNo))
            ;

        if (!mileageTypeSearchSetDto.getStoreType().equals("A")){
            query.WHERE(SqlUtils.equalColumnByInput(mileageType.storeType, mileageTypeSearchSetDto.getStoreType()));
        }

        if (!mileageTypeSearchSetDto.getMileageKind().equals("A")){
            query.WHERE(SqlUtils.equalColumnByInput(mileageType.mileageKind, mileageTypeSearchSetDto.getMileageKind()));
        }

        if (!mileageTypeSearchSetDto.getUseYn().equals("A")){
            query.WHERE(SqlUtils.equalColumnByInput(mileageType.useYn, mileageTypeSearchSetDto.getUseYn()));
        }

        if (mileageTypeSearchSetDto.getSchDetailDesc() != null && MileageCommonUtil.notEmpty(mileageTypeSearchSetDto.getSchDetailDesc())){
            if(mileageTypeSearchSetDto.getSchDetailCode().equalsIgnoreCase("MILEAGE_TYPE_NO")){
                query.WHERE(SqlUtils.equalColumnByInput(mileageType.mileageTypeNo, mileageTypeSearchSetDto.getSchDetailDesc()));
            } else if (mileageTypeSearchSetDto.getSchDetailCode().equals("MILEAGE_TYPE_NAME")) {
                query.WHERE(SqlPatternConstants.getConditionByPattern("LIKE", mileageType.mileageTypeName, mileageTypeSearchSetDto.getSchDetailDesc()));
            } else if (mileageTypeSearchSetDto.getSchDetailCode().equals("CODE")) {
                query.WHERE(SqlUtils.equalColumnByInput(mileageType.mileageCode, mileageTypeSearchSetDto.getSchDetailDesc()));
            }

        }

        log.debug("selectMileageTypeInfo \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 유형 기본정보 조회
     *
     * @param mileageTypeNo 마일리지유형관리번호
     * @return String 마일리지 유형 기본정보 조회 Query
     * @throws Exception 오류 발생시 오류처리.
     */
    public static String selectMileageTypeBasicInfo (@Param("mileageTypeNo") long mileageTypeNo) throws Exception {
        SQL query = new SQL()
            .SELECT(
                mileageType.mileageTypeNo,
                mileageType.mileageCode,
                mileageType.mileageTypeName,
                mileageType.mileageCategory,
                mileageType.mileageKind,
                mileageType.storeType,
                SqlPatternConstants.getConditionByPatternWithAlias("DATE_INPUT", "reg_start_dt", mileageType.regStartDt, "%Y-%m-%d"),
                SqlPatternConstants.getConditionByPatternWithAlias("DATE_INPUT", "reg_end_dt", mileageType.regEndDt, "%Y-%m-%d"),
                mileageType.expireType,
                mileageType.expireDayInp,
                SqlPatternConstants.getConditionByPatternWithAlias("DATE_INPUT", "use_start_dt", mileageType.useStartDt, "%Y-%m-%d"),
                SqlPatternConstants.getConditionByPatternWithAlias("DATE_INPUT", "use_end_dt", mileageType.useEndDt, "%Y-%m-%d"),
                mileageType.mileageTypeExplain,
                mileageType.useYn,
                mileageType.regId,
                mileageType.displayMessage
            )
            .FROM(typeTable)
            .WHERE(
                SqlUtils.equalColumnByInput(mileageType.mileageTypeNo, mileageTypeNo)
            );

        log.debug("selectMileageTypeBasicInfo \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 유형 수정가능여부 확인
     * @param mileageTypeManageModifyDto 마일리지 유형 수정 MAP
     * @return String 마일리지 유형 수정가능여부 확인 Query.
     */
    public static String selectMileageTypeModifyCheck(MileageTypeManageModifyDto mileageTypeManageModifyDto) {
        SQL query = new SQL()
            .SELECT(
                SqlUtils.aliasToRow("CASE WHEN #{mileageTypeName} != IFNULL(mileage_type_name, '') THEN #{mileageTypeName} ELSE NULL END", "mileageTypeName"),
                SqlUtils.aliasToRow("CASE WHEN #{mileageKind} != IFNULL(mileage_kind, '') THEN #{mileageKind} ELSE NULL END", "mileageKind"),
                SqlUtils.aliasToRow("CASE WHEN #{storeType} != IFNULL(store_type, '') THEN #{storeType} ELSE NULL END", "storeType"),
                SqlUtils.aliasToRow("CASE WHEN #{regStartDt} != DATE_FORMAT(reg_start_dt, '%Y-%m-%d') THEN #{regStartDt} ELSE NULL END", "regStartDt"),
                SqlUtils.aliasToRow("CASE WHEN #{regEndDt} != DATE_FORMAT(reg_end_dt, '%Y-%m-%d') THEN #{regEndDt} ELSE NULL END", "regEndDt"),
                SqlUtils.aliasToRow("CASE WHEN #{expireType} != IFNULL(expire_type, '') THEN #{expireType} ELSE NULL END", "expireType"),
                SqlUtils.aliasToRow("CASE WHEN #{expireDayInp} != IFNULL(expire_day_inp, '') THEN #{expireDayInp} ELSE NULL END", "expireDayInp"),
                SqlUtils.aliasToRow("CASE WHEN #{useStartDt} != DATE_FORMAT(use_start_dt, '%Y-%m-%d') THEN #{useStartDt} ELSE NULL END", "useStartDt"),
                SqlUtils.aliasToRow("CASE WHEN #{useEndDt} != DATE_FORMAT(use_end_dt, '%Y-%m-%d') THEN #{useEndDt} ELSE NULL END", "useEndDt"),
                SqlUtils.aliasToRow("CASE WHEN LENGTH(#{mileageTypeExplain}) != LENGTH(IFNULL(mileage_type_explain, '')) THEN #{mileageTypeExplain} ELSE NULL END", "mileageTypeExplain"),
                SqlUtils.aliasToRow("CASE WHEN LENGTH(#{displayMessage}) != LENGTH(IFNULL(display_message, '')) THEN #{displayMessage} ELSE NULL END", "displayMessage"),
                SqlUtils.aliasToRow("CASE WHEN #{mileageCode} != IFNULL(mileage_code, '') THEN #{mileageCode} ELSE NULL END", "mileageCode"),
                SqlUtils.aliasToRow("CASE WHEN #{useYn} != IFNULL(use_yn, '') THEN #{useYn} ELSE NULL END", "useYn")
            )
            .FROM(typeTable)
            .WHERE(
                SqlUtils.equalColumnByInput(mileageType.mileageTypeNo, Long.parseLong(mileageTypeManageModifyDto.getMileageTypeNo()))
            );

        log.debug("selectMileageTypeModifyCheck \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 유형 등록
     *
     * @param mileageTypeManageSetDto 마일리지 유형 등록 DTO
     * @return String 마일리지 유형 등록 Query
     */
    public static String insertMileageTypeInfo (MileageTypeManageSetDto mileageTypeManageSetDto) {
        // 마일리지 유형 테이블 컬럼.
        MileageTypeEntity mileageTypeEntity = EntityFactory.createEntityIntoValue(MileageTypeEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(mileageTypeEntity));
        query.INTO_COLUMNS(
            mileageTypeEntity.mileageTypeName,
            mileageTypeEntity.mileageCategory,
            mileageTypeEntity.mileageKind,
            mileageTypeEntity.storeType,
            mileageTypeEntity.regStartDt,
            mileageTypeEntity.regEndDt,
            mileageTypeEntity.expireType
        );

        if(mileageTypeManageSetDto.getExpireType().equalsIgnoreCase("TR")) {
            query.INTO_COLUMNS(mileageTypeEntity.expireDayInp);
        } else {
            query.INTO_COLUMNS(mileageTypeEntity.useStartDt, mileageTypeEntity.useEndDt);
        }

        query.INTO_COLUMNS(
            mileageTypeEntity.mileageTypeExplain,
            mileageTypeEntity.useYn,
            mileageTypeEntity.regDt,
            mileageTypeEntity.regId,
            mileageTypeEntity.chgDt,
            mileageTypeEntity.chgId,
            mileageTypeEntity.mileageCode,
            mileageTypeEntity.displayMessage
        );

        // INSERT VALUE 생성.
        query.INTO_VALUES(
            SqlUtils.convertInsertParamToCamelCase(
                mileageTypeEntity.mileageTypeName,
                mileageTypeEntity.mileageCategory,
                mileageTypeEntity.mileageKind,
                mileageTypeEntity.storeType,
                mileageTypeEntity.regStartDt,
                mileageTypeEntity.regEndDt,
                mileageTypeEntity.expireType
            )
        );

        if(mileageTypeManageSetDto.getExpireType().equalsIgnoreCase("TR")) {
            query.INTO_VALUES(SqlUtils.convertInsertParamToCamelCase(mileageTypeEntity.expireDayInp));
        } else {
            query.INTO_VALUES(SqlUtils.convertInsertParamToCamelCase(mileageTypeEntity.useStartDt, mileageTypeEntity.useEndDt));
        }

        query.INTO_VALUES(
            SqlUtils.convertInsertParamToCamelCase(
                mileageTypeEntity.mileageTypeExplain,
                mileageTypeEntity.useYn,
                mileageTypeEntity.regDt,
                mileageTypeEntity.regId,
                mileageTypeEntity.chgDt,
                mileageTypeEntity.regId,
                mileageTypeEntity.mileageCode,
                mileageTypeEntity.displayMessage
            )
        );

        log.debug("insertMileageTypeInfo \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 유형 업데이트
     *
     * @param parameterMap 마일리지 유형 업데이트 Map
     * @return 마일리지 유형 업데이트 Query.
     */
    public static String updateMileageTypeInfo (HashMap<String, Object> parameterMap) {
        SQL query = new SQL()
            .UPDATE(typeTable);

        parameterMap.forEach((key, value) -> {
            if (value != null && !key.equals("mileageTypeNo")) {
                if(key.equals("useStartDt") || key.equals("useEndDt")){
                    query.SET(
                        SqlUtils.nonSingleQuote(
                            key.equalsIgnoreCase("useStartDt") ? mileageType.useStartDt : mileageType.useEndDt,
                            MessageFormat.format(
                                key.equalsIgnoreCase("useStartDt") ? SqlPatternConstants.DATE_FULL_ZERO : SqlPatternConstants.DATE_FULL_LAST,
                                SqlUtils.convertColumnToMapper(key)
                            )
                        )
                    );
                } else {
                    query.SET(SqlUtils.equalColumnMapping(SqlUtils.convertCamelCaseToSnakeCase(key)));
                }

                if(key.equals("chgId")){
                    query.SET(SqlUtils.nonSingleQuote(mileageType.chgDt, "NOW()"));
                }
            }
        });

        query.WHERE(
            SqlUtils.equalColumnMapping(mileageType.mileageTypeNo)
        );

        log.debug("updateMileageTypeInfo \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 행사관리용 타입조회
     * @param setDto 타입조회 파라미터
     * @return
     */
    public static String selectMileagePromotionTypeInfo(MileagePromoTypeSetDto setDto) {
        MileageTypeEntity mileageType = EntityFactory.createEntityIntoValue(MileageTypeEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                mileageType.mileageTypeNo,
                mileageType.mileageTypeName,
                mileageType.expireType,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(mileageType.expireType, "TR"),
                        mileageType.expireDayInp,
                        SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(mileageType.useStartDt, "' ~ '", mileageType.useEndDt))
                    ),
                    "expire_dt"
                ),
                mileageType.mileageTypeExplain
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageType))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageType.storeType, setDto.getStoreType()),
                SqlUtils.equalColumnByInput(mileageType.mileageCategory, "01"),
                SqlUtils.equalColumnByInput(mileageType.mileageKind, 90),
                SqlUtils.equalColumnByInput(mileageType.useYn, "Y")
            );
        if(StringUtils.isNotEmpty(setDto.getSchTypeContents())){
            switch (setDto.getSchTypeCd().toUpperCase(Locale.KOREA)){
                case "TYPE_NO" :
                    query.WHERE(SqlUtils.equalColumnByInput(mileageType.mileageTypeNo, setDto.getSchTypeContents()));
                    break;
                case "TYPE_NM" :
                    query.WHERE(SqlUtils.middleLike(mileageType.mileageTypeName, setDto.getSchTypeContents()));
                    break;
                default:
                    break;
            }
        }
        log.debug("selectMileagePromotionTypeInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileageTypeCodeCheck(@Param("mileageCode") String mileageCode) {
        MileageTypeEntity mileageType = EntityFactory.createEntityIntoValue(MileageTypeEntity.class);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, mileageType.mileageCode, "cnt")
            )
            .FROM(EntityFactory.getTableName(mileageType))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageType.mileageCode, mileageCode)
            );
        log.debug("selectMileageTypeCodeCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileageReturnType(@Param("tradeNo") String tradeNo, @Param("mileageCode") String mileageCode) {
        MileageTypeEntity mileageType = EntityFactory.createEntityIntoValue(MileageTypeEntity.class, Boolean.TRUE);
        MileageReqEntity mileageReq = EntityFactory.createEntityIntoValue(MileageReqEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                mileageType.mileageTypeNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageType))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageReq, ObjectUtils.toArray(mileageType.mileageTypeNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageReq.tradeNo, tradeNo),
                SqlUtils.sqlFunction(MysqlFunction.IN, mileageType.mileageCode,
                    Arrays.stream(mileageCode.split(",")).map(str -> "'" + str + "'").toArray())
            )
            .ORDER_BY(SqlUtils.orderByesc(mileageType.mileageTypeNo))
            .LIMIT(1)
            .OFFSET(0)
            ;
        log.debug("selectMileageReturnType \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileageCouponType(@Param("couponInfoSeq") long couponInfoSeq) {
        MileageCouponInfoEntity mileageCouponInfo = EntityFactory.createEntityIntoValue(MileageCouponInfoEntity.class, Boolean.TRUE);
        MileageSaveEntity mileageSave = EntityFactory.createEntityIntoValue(MileageSaveEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                mileageType.mileageTypeNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageCouponInfo))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageSave, ObjectUtils.toArray(mileageCouponInfo.mileageSaveNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageCouponInfo.couponInfoSeq, couponInfoSeq)
            );
        log.debug("selectMileageCouponType \n[{}]", query.toString());
        return query.toString();
    }
}
