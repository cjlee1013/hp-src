package kr.co.homeplus.mileage.management.model.save;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SaveMileageDto {

    @ApiModelProperty(value = "등록키", position = 1)
    private String mileageSaveNo;

    @ApiModelProperty(value = "마일리지 적립/회수 요청상세번호", position = 2)
    private long mileageReqDetailNo;

    @ApiModelProperty(value = "마일리지 유형번호", position = 3)
    private long mileageTypeNo;

    @ApiModelProperty(value = "고객번호", position = 4)
    private String userNo;

    @ApiModelProperty(value = "마일리지유형", position = 5)
    private String mileageCategory;

    @ApiModelProperty(value = "마일리지종류", position = 6)
    private String mileageKind;

    @ApiModelProperty(value = "적립금액", position = 7)
    private long mileageAmt;

    @ApiModelProperty(value = "마일리지잔액", position = 8)
    private long mileageRemainAmt;

    @ApiModelProperty(value = "사용시작일", position = 9)
    private String useStartDt;

    @ApiModelProperty(value = "유효월일", position = 10)
    private String expireDt;

    @ApiModelProperty(value = "등록자아이디", position = 11)
    private String regId;

    @ApiModelProperty(value = "수정자아이디", position = 12)
    private String chgId;

    @ApiModelProperty(value = "수정자아이디", position = 13, hidden = true)
    private String tradeNo;

    @ApiModelProperty(value = "사유", position = 14, hidden = true)
    private String requestReason;

    @JsonIgnore
    private String couponInfoSeq;
}
