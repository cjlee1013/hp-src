package kr.co.homeplus.mileage.management.model.user;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserSearchHistoryReqDto {

    @ApiModelProperty(value = "고객번호", position = 1)
    @NotNull(message = "사용자번호를 입력해주세요.")
    private String userNo;

    //Default 1
    @ApiModelProperty(value = "요청 페이지", position = 2)
    private int page;

    //Default 10
    @ApiModelProperty(value = "페이지당 View Count", position = 3)
    private int viewCount;

    @ApiModelProperty(value = "조회시작일", position = 4)
    private String searchStartDay;

    @ApiModelProperty(value = "조회종료일", position = 5)
    private String searchEndDay;
}
