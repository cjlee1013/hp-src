package kr.co.homeplus.mileage.office.model.coupon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마일리지 난수번호 관리 - 기본정보 DTO")
public class MileageCouponInfoDto {
    @ApiModelProperty(notes = "쿠폰번호", position = 1)
    private long couponManageNo;
    @ApiModelProperty(notes = "마일리지타입번호", position = 2)
    private long mileageTypeNo;
    @ApiModelProperty(notes = "마일리지타입유형명", position = 3)
    private String mileageTypeName;
    @ApiModelProperty(notes = "난수종류", position = 4)
    private String couponType;
    @ApiModelProperty(notes = "제목", position = 5)
    private String couponNm;
    @ApiModelProperty(notes = "발급기간(시작)", position = 6)
    private String issueStartDt;
    @ApiModelProperty(notes = "발급기간(종료)", position = 7)
    private String issueEndDt;
    @ApiModelProperty(notes = "발급수량", position = 8)
    private int issueQty;
    @ApiModelProperty(notes = "마일리지금액", position = 9)
    private int mileageAmt;
    @ApiModelProperty(notes = "1인당발급타입", position = 10)
    private String personalIssueType;
    @ApiModelProperty(notes = "1인당발급수량", position = 11)
    private int personalIssueQty;
    @ApiModelProperty(notes = "고객노출문구", position = 12)
    private String displayMessage;
    @ApiModelProperty(notes = "쿠폰번호", position = 13)
    private String couponNo;
    @ApiModelProperty(notes = "발급여부", position = 14)
    private String issueYn;
    @ApiModelProperty(notes = "사용여부", position = 15)
    private String useYn;
}
