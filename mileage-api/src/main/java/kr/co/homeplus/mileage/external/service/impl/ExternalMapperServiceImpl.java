package kr.co.homeplus.mileage.external.service.impl;

import java.util.List;
import kr.co.homeplus.mileage.external.mapper.ExternalMapper;
import kr.co.homeplus.mileage.external.model.ExternalMyPageGetDto;
import kr.co.homeplus.mileage.external.model.ExternalMyPageSetDto;
import kr.co.homeplus.mileage.external.service.ExternalMapperService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExternalMapperServiceImpl implements ExternalMapperService {

    private final ExternalMapper mapper;

    @Override
    public String getMileageTypeInfo(String mileageCategory, String mileageKind) {
        return mapper.selectExternalType(mileageCategory, mileageKind);
    }

    @Override
    public String getMileageCodeTypeInfo(String mileageCode) {
        return mapper.selectMileageCodeExternalType(mileageCode);
    }

    @Override
    public List<ExternalMyPageGetDto> getExternalMileageList(ExternalMyPageSetDto externalMyPageSetDto) {
        return mapper.selectExternalMileageList(externalMyPageSetDto);
    }

    @Override
    public boolean getAlreadySaveCheck(String mileageCode, String tradeNo, String userNo) {
        String checkResult = mapper.selectAlreadySaveCheck(mileageCode, tradeNo, userNo);
        if(StringUtils.isNotEmpty(checkResult)) {
            return checkResult.equals("Y");
        } else {
            return Boolean.FALSE;
        }

    }
}
