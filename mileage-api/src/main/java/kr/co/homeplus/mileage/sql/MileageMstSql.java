package kr.co.homeplus.mileage.sql;

import kr.co.homeplus.mileage.constants.CustomConstants;
import kr.co.homeplus.mileage.constants.SqlPatternConstants;
import kr.co.homeplus.mileage.enums.MysqlFunction;
import kr.co.homeplus.mileage.entity.MileageMstEntity;
import kr.co.homeplus.mileage.entity.MileageSaveEntity;
import kr.co.homeplus.mileage.utils.EntityFactory;
import kr.co.homeplus.mileage.utils.ObjectUtils;
import kr.co.homeplus.mileage.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MileageMstSql {
    // 마일리지 회원 마스터 테이블 명.
    public static final String mstTable = EntityFactory.getTableName(MileageMstEntity.class);
    // 마일리 회원 마스터 컬럼.
    public static final MileageMstEntity mm = EntityFactory.createEntityIntoValue(MileageMstEntity.class);

    /**
     * 회원 마일리지 금액 조회.
     *
     * @param userNo 조회할 고객관리번호
     * @return String 회원 마일리지 금액 조회 Select Query
     */
    public static String selectUserMileageMstInfo(@Param("userNo") String userNo){
        MileageMstEntity mileageMst = EntityFactory.createEntityIntoValue(MileageMstEntity.class, Boolean.TRUE);
        MileageSaveEntity mileageSave = EntityFactory.createEntityIntoValue(MileageSaveEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            // SELECT 할 컬럼 설정.
            .SELECT(
                mileageMst.userNo,
                mileageMst.mileageAmt,
                SqlUtils.subQuery(
                    SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, mileageSave.mileageRemainAmt),
                    EntityFactory.getTableNameWithAlias(mileageSave),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(mileageMst.userNo, mileageSave.userNo),
                        SqlUtils.sqlFunction(
                            MysqlFunction.BETWEEN_ORI,
                            ObjectUtils.toArray(
                                mileageSave.expireDt,
                                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, "NOW()"),
                                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, SqlUtils.sqlFunction(MysqlFunction.DATEADD_WEEK, ObjectUtils.toArray("NOW()", "2")))
                            )
                        )
                    ),
                    "expired_mileage_amt"
                )
            )
            // SELECT 대상 Table 설정.
            .FROM(EntityFactory.getTableNameWithAlias(mileageMst))
            // SELECT 조회 조건 설정.
            // 컬럼과 입력값을 "=" 로 한 쿼리 생성 Method (mm.user_no = 'user_no')
            .WHERE(SqlUtils.equalColumnByInput(mileageMst.userNo, userNo));

        log.debug("selectUserMileageMstInfo \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 회원 여부 확인 조회.
     *
     * @param userNo 회원여부를 확인할 고객관리번호
     * @return String 마일리지 회원여부 확인 조회 Select Query
     * @throws Exception 오류시 오류 처리.
     */
    public static String selectUserMileageMstCheck(@Param("userNo") String userNo) throws Exception{
        SQL query = new SQL()
            // COUNT 쿼리 생성.
            // SqlPatternConstants 내의 COUNT Pattern 을 사용하여 생성.
            .SELECT(SqlPatternConstants.getConditionByPatternWithAlias("count", "CNT", "1"))
            // SELECT 대상 Table 설정.
            .FROM(mstTable)
            // SELECT 조회 조건 설정.
            // 컬럼과 입력값을 "=" 로 한 쿼리 생성 Method (mm.user_no = 'user_no')
            .WHERE(SqlUtils.equalColumnByInput(mm.userNo, userNo));

        log.debug("selectUserMileageMstCheck \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 계좌 생성.
     *
     * @param userNo 마일리지 계좌를 생성할 고객관리번호.
     * @return String 마일리지 계좌 생성용 Insert Query
     */
    public static String insertUserMileageAccount(@Param("userNo") String userNo) {
        SQL query = new SQL()
            // INSERT 대상 Table 설정.
            .INSERT_INTO(mstTable)
            // INSERT 할 컬럼 설정.
            // 고객관리번호만 대상으로 설정.
            .INTO_COLUMNS(mm.userNo)
            // INSERT 할 Value 설정.
            .INTO_VALUES(SqlUtils.appendSingleQuote(userNo));

        log.debug("insertUserMileageAccount \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 고객 마일리지 계좌 정보 수정.
     * (적립/사용/취소/회수/소멸)
     *
     * @param userNo 업데이트 대상 고객관리번호
     * @param mileageAmt 대상 금액
     * @return String 고객 마일리지 계좌 정보 수정용 Update Query
     */
    public static String updateUserMileageMst(@Param("userNo") String userNo, @Param("mileageAmt") long mileageAmt){
        SQL query = new SQL()
            // UPDATE 대상 Table 설정.
            .UPDATE(mstTable)
            // UPDATE 대상 SET 정보설정.
            // 마일리지금액 컬럼에 대상금액을 + 하는 쿼리 생성( mileage_amt = mileage_amt + 대상금액)
            .SET(SqlUtils.conditionValueForUpdateCalculation(mm.mileageAmt, mileageAmt, CustomConstants.PLUS))
            // 마일리지 계좌의 chg_dt 를 현재 시간으로 업데이트 하도록 함.
            .SET(SqlUtils.nonSingleQuote(mm.chgDt, "NOW()"))
            // 마일리지 계좌 수정 대상 고객정보 설정.(user_no = 대상고객관리번호)
            .WHERE(SqlUtils.equalColumnByInput(mm.userNo, userNo));

        log.debug("updateUserMileageMst \n[{}]", query.toString());
        return query.toString();
    }




}
