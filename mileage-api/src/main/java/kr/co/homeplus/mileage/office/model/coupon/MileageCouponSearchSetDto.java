package kr.co.homeplus.mileage.office.model.coupon;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "마일리지 난수번호 관리 조회 DTO")
public class MileageCouponSearchSetDto {
    // 시작일자
    private String schStartDt;
    // 종료일자
    private String schEndDt;
    //난수종류
    private String schCouponType;
    //사용 여부
    private String schIssueYn;
    //상세검색
    private String schDetailCode;
    //마일리지 종류
    private String schDetailDesc;
}
