package kr.co.homeplus.mileage.office.model.type;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마일리지 행사 관련 조회 응답 DTO")
public class MileagePromoTypeGetDto {
    @ApiModelProperty(value = "마일리지타입번호", position = 1)
    private long mileageTypeNo;
    @ApiModelProperty(value = "마일리지타입명", position = 2)
    private String mileageTypeName;
    @ApiModelProperty(value = "유효기간타입(TT:기간내/TR:등록일로부터)", position = 3)
    private String expireType;
    @ApiModelProperty(value = "유효기간일", position = 4)
    private String expireDt;
    @ApiModelProperty(value = "고객노출문구", position = 5)
    private String mileageTypeExplain;
}
