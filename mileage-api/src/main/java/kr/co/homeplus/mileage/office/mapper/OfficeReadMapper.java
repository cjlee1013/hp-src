package kr.co.homeplus.mileage.office.mapper;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.mileage.core.db.annotation.SlaveConnection;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponInfoDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponOneTimeDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponSearchGetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponSearchSetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponUserInfoDto;
import kr.co.homeplus.mileage.office.model.history.MileageHistoryDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDto;
import kr.co.homeplus.mileage.office.model.history.MileageHistoryReqDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSearchGetDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSearchSetDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDetailDto;
import kr.co.homeplus.mileage.office.model.stat.MileageStatSumListGet;
import kr.co.homeplus.mileage.office.model.stat.MileageStatSumListSet;
import kr.co.homeplus.mileage.office.model.stat.MileageStatTypeSumListGet;
import kr.co.homeplus.mileage.office.model.type.MileagePromoTypeGetDto;
import kr.co.homeplus.mileage.office.model.type.MileagePromoTypeSetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeManageModifyDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeSearchGetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeSearchSetDto;
import kr.co.homeplus.mileage.sql.MileageCommonSql;
import kr.co.homeplus.mileage.sql.MileageCouponSql;
import kr.co.homeplus.mileage.sql.MileagePaymentSql;
import kr.co.homeplus.mileage.sql.MileageTypeSql;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.web.bind.annotation.RequestBody;

@SlaveConnection
public interface OfficeReadMapper {

    //########################################################################################################################
    // 마일리지 내역 조회
    //########################################################################################################################
    @ResultType(value = MileageHistoryDto.class)
    @SelectProvider(type = MileageCommonSql.class, method = "selectMileageHistoryList")
    List<MileageHistoryDto> selectMileageHistoryList(MileageHistoryReqDto mileageHistoryReqDto);
    //########################################################################################################################

    //########################################################################################################################
    // 마일리지 적립/회수 조회
    //########################################################################################################################
    //마일리지 적립/회수내역조회
    @ResultType(value = MileagePaymentSearchGetDto.class)
    @SelectProvider(type = MileagePaymentSql.class, method = "selectMileagePaymentInfoList")
    List<MileagePaymentSearchGetDto> selectMileagePaymentInfoList(MileagePaymentSearchSetDto mileagePaymentSearchSetDto);

    // 마일리지 유형 정보 확인
    @Select("SELECT COUNT(1) AS CNT FROM mileage_type WHERE mileage_type_no = #{mileageTypeNo} AND use_yn = 'Y'")
    int selectMileageTypeInfoCheck(@Param("mileageTypeNo") String mileageTypeNo);

    // 마일리지 적립/회수 요청 확인
    @Select("SELECT COUNT(1) AS CNT FROM mileage_req WHERE mileage_req_no = #{mileageReqNo}")
    int selectMileageRequestInfoCheck(@Param("mileageReqNo") String mileageReqNo);

    // 마일리지 적립/회수 요청 상세 정보 조회
    @ResultType(value = MileageReqDetailDto.class)
    @SelectProvider(type = MileagePaymentSql.class, method = "selectMileageRequestDetailInfoList")
    List<MileageReqDetailDto> selectMileageRequestDetailInfoList(@Param("mileageReqNo") String mileageReqNo);

    // 마일리지 적립/회수 요청 정보 조회
    @ResultType(value = MileageReqDto.class)
    @SelectProvider(type = MileagePaymentSql.class, method = "selectMileageRequestInfoList")
    MileageReqDto selectMileageRequestInfoList(@Param("mileageReqNo") String mileageReqNo);

    // 마일리지 지급/회수 기본정보 수정내역 체크
    @ResultType(value = MileageReqDto.class)
    @SelectProvider(type = MileagePaymentSql.class, method = "selectMileageReqModifyCheck")
    MileageReqDto selectMileageReqModifyCheck(HashMap<String, Object> parameterMap);
    //########################################################################################################################

    //########################################################################################################################
    // 마일리지 타입
    //########################################################################################################################
    @SelectProvider(type = MileageTypeSql.class, method = "selectMileageTypeModifyCheck")
    HashMap<String, Object> selectMileageTypeModifyCheck(MileageTypeManageModifyDto mileageTypeManageModifyDto);

    //마일리지 타입 조회
    @ResultType(value = MileageTypeSearchGetDto.class)
    @SelectProvider(type = MileageTypeSql.class, method = "selectMileageTypeInfo")
    List<MileageTypeSearchGetDto> selectMileageTypeInfo(MileageTypeSearchSetDto mileageTypeSearchSetDto);

    // 마일리지 타입 기본정보 조회
    @ResultType(value = MileageTypeManageModifyDto.class)
    @SelectProvider(type = MileageTypeSql.class, method = "selectMileageTypeBasicInfo")
    MileageTypeManageModifyDto selectMileageTypeBasicInfo(@Param("mileageTypeNo") long mileageTypeNo);

    //회수 타입 조회
    @Select("SELECT MAX(mileage_type_no) AS mileage_type_no FROM mileage_type WHERE mileage_category = '04' AND use_yn = 'Y'")
    String selectMileageTypeNoByCategory();

    // 마일리지 행사 관리용 타입 조회
    @SelectProvider(type = MileageTypeSql.class, method = "selectMileagePromotionTypeInfo")
    List<MileagePromoTypeGetDto> selectMileagePromotionTypeInfo(MileagePromoTypeSetDto setDto);

    @SelectProvider(type = MileageTypeSql.class, method = "selectMileageTypeCodeCheck")
    int selectMileageTypeCodeCheck(@Param("mileageCode") String mileageCode);

    @SelectProvider(type = MileageTypeSql.class, method = "selectMileageReturnType")
    String selectMileageReturnType(@Param("tradeNo") String tradeNo, @Param("mileageCode") String mileageCode);

    @SelectProvider(type = MileageTypeSql.class, method = "selectMileageCouponType")
    String selectMileageCouponType(@Param("couponInfoSeq") long couponInfoSeq);
    //########################################################################################################################

    //########################################################################################################################
    // 마일리지 쿠폰 등록
    //########################################################################################################################
    // 쿠폰 정보 조회
    @SelectProvider(type = MileageCouponSql.class, method = "selectMileageCouponInfo")
    LinkedHashMap<String, Object> selectMileageCouponInfo(@Param("couponNo") String couponNo);
    // 쿠폰 등록 건수 조회
    @SelectProvider(type = MileageCouponSql.class, method = "selectMileageCouponRegCheck")
    LinkedHashMap<String, Long> selectMileageCouponRegCheck(LinkedHashMap<String, Object> parameterMap);
    // 다회성쿠폰 등록 여부 체크
    @SelectProvider(type = MileageCouponSql.class, method = "selectMileageMultiCouponRegCheck")
    LinkedHashMap<String, Object> selectMileageMultiCouponRegCheck(@Param("couponNo") String couponNo);

    @SelectProvider(type = MileageCouponSql.class, method = "selectCouponIsPossibleCheck")
    long selectCouponIsPossibleCheck(@Param("couponManageNo") long couponManageNo);

    @SelectProvider(type = MileageCouponSql.class, method = "selectCouponUsedCheck")
    LinkedHashMap<String, String> selectCouponUsedCheck(@Param("couponNo") String couponNo);
    // 마일리지 난수번호 조회
    @SelectProvider(type = MileageCouponSql.class, method = "selectCouponManageInfoList")
    List<MileageCouponSearchGetDto> selectCouponManageInfoList(MileageCouponSearchSetDto setDto);
    // 마일리지 쿠폰정보
    @SelectProvider(type = MileageCouponSql.class, method = "selectMileageCouponManageInfo")
    MileageCouponInfoDto selectMileageCouponManageInfo(@Param("couponManageNo") long couponManageNo);
    // 마일리지 쿠폰 - 유저정보
    @SelectProvider(type = MileageCouponSql.class, method = "selectMileageCouponUserInfo")
    List<MileageCouponUserInfoDto> selectMileageCouponUserInfo(@Param("couponManageNo") long couponManageNo);

    @SelectProvider(type = MileageCouponSql.class, method = "selectCouponOneTimeInfo")
    List<MileageCouponOneTimeDto> selectCouponOneTimeInfo(@Param("couponManageNo") long couponManageNo);
    //마일리지 일자별 금액 합계 통계 조회
    List<MileageStatSumListGet> getDailyMileageSummary(@RequestBody @Valid MileageStatSumListSet requestDto);
    //마일리지 일자/타입별 금액 합계 통계 조회
    List<MileageStatTypeSumListGet> getDailyMileageTypeSummary(@RequestBody @Valid MileageStatSumListSet requestDto);
    //########################################################################################################################
}
