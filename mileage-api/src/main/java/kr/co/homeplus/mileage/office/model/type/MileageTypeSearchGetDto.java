package kr.co.homeplus.mileage.office.model.type;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MileageTypeSearchGetDto {

    @ApiModelProperty(value = "마일리지번호", position = 1)
    private String mileageTypeNo;

    @ApiModelProperty(value = "점포유형", position = 2)
    private String storeType;

    @ApiModelProperty(value = "마일리지유형", position = 3)
    private String mileageTypeName;

    @ApiModelProperty(value = "마일리지종류", position = 4)
    private String mileageKind;

    @ApiModelProperty(value = "사용", position = 5)
    private String useYn;

    @ApiModelProperty(value = "등록아이디", position = 6)
    private String regId;

    @ApiModelProperty(value = "등록일", position = 7)
    private String regDt;

    @ApiModelProperty(value = "수정아이디", position = 8)
    private String chgId;

    @ApiModelProperty(value = "수정일", position = 9)
    private String chgDt;

    @ApiModelProperty(value = "마일리지코드", position = 10)
    private String mileageCode;
}
