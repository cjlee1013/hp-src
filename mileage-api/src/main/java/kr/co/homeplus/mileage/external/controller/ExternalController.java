package kr.co.homeplus.mileage.external.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.mileage.external.model.ExternalItemSetDto;
import kr.co.homeplus.mileage.external.model.ExternalMileageSaveDto;
import kr.co.homeplus.mileage.external.model.ExternalMyPageGetDto;
import kr.co.homeplus.mileage.external.model.ExternalMyPageSetDto;
import kr.co.homeplus.mileage.external.model.ExternalOmniSetDto;
import kr.co.homeplus.mileage.external.model.ExternalPromoSetDto;
import kr.co.homeplus.mileage.external.model.cancel.ExternalSaveCancelSetDto;
import kr.co.homeplus.mileage.external.service.ExternalService;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponUserRegDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/external")
@Api(tags = "ExternalController", value = "외부연동 Controller")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
}
)
@RequiredArgsConstructor
public class ExternalController {

    private final ExternalService externalService;

    @PostMapping("/externalOmni")
    @ApiOperation(value = "옴니외부연동")
    public ResponseObject<String> setOmniMileageSave(@RequestBody @Valid ExternalOmniSetDto externalOmniSetDto) throws Exception {
        return externalService.requestOmniSave(externalOmniSetDto);
    }

    @PostMapping("/externalItem")
    @ApiOperation(value = "구매평외부연동")
    public ResponseObject<String> setItemMileageSave(@RequestBody @Valid ExternalItemSetDto externalItemSetDto) throws Exception {
        return externalService.requestItemSave(externalItemSetDto);
    }

    @PostMapping("/externalMileageSave")
    @ApiOperation(value = "외부마일리지적립연동")
    public ResponseObject<String> setExternalMileageSave(@RequestBody @Valid ExternalPromoSetDto externalPromoSetDto) throws Exception {
        return externalService.requestExternalMileageSave(externalPromoSetDto);
    }

    @PostMapping("/externalMileageList")
    @ApiOperation(value = "마일리지 적립/차감내역")
    public ResponseObject<List<ExternalMyPageGetDto>> getMileageList(@RequestBody @Valid ExternalMyPageSetDto externalMyPageSetDto) throws Exception {
        return externalService.requestMileageList(externalMyPageSetDto);
    }

    @PostMapping("/externalCouponReg")
    @ApiOperation(value = "마일리지 쿠폰등록 외부연동")
    public ResponseObject<String> getMileageList(@RequestBody @Valid MileageCouponUserRegDto mileageCouponUserRegDto) throws Exception {
        return externalService.mileageCouponUserRegister(mileageCouponUserRegDto);
    }

    @PostMapping("/external/mileageSave")
    @ApiOperation(value = "외부마일리지적립연동")
    public ResponseObject<String> setExternalSave(@RequestBody @Valid ExternalMileageSaveDto externalPromoSetDto) throws Exception {
        return externalService.requestExternalMileageSave(externalPromoSetDto);
    }

    @PostMapping("/external/mileageCancel")
    @ApiOperation(value = "외부마일리지적립취소연동")
    public ResponseObject<String> setExternalReturn(@RequestBody @Valid ExternalSaveCancelSetDto setDto) throws Exception {
        return externalService.requestExternalMileageReturn(setDto);
    }

    @PostMapping("/external/retryMileageSave")
    @ApiOperation(value = "외부마일리지적립연동(재시도)")
    public ResponseObject<String> setRetryExternalSave(@RequestBody @Valid ExternalMileageSaveDto externalPromoSetDto) throws Exception {
        return externalService.requestRetryExternalMileageSave(externalPromoSetDto);
    }

}
