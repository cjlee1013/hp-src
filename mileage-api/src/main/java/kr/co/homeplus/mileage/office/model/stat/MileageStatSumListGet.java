package kr.co.homeplus.mileage.office.model.stat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "마일리지 일자별 금액 통계")
public class MileageStatSumListGet {
    @ApiModelProperty(value = "일자", position = 1)
    @RealGridColumnInfo(headText = "일자", sortable = true, width = 100)
    private String basicDt;

    @ApiModelProperty(value = "전일잔액", position = 2)
    @RealGridColumnInfo(headText = "전일잔액", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long prevRemainAmt;

    @ApiModelProperty(value = "적립", position = 3)
    @RealGridColumnInfo(headText = "적립", groupName = "적립 (+)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long saveAmt;

    @ApiModelProperty(value = "사용취소", position = 4)
    @RealGridColumnInfo(headText = "사용취소", groupName = "적립 (+)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cancelAmt;

    @ApiModelProperty(value = "사용", position = 5)
    @RealGridColumnInfo(headText = "사용", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long useAmt;

    @ApiModelProperty(value = "유효기간만료", position = 6)
    @RealGridColumnInfo(headText = "유효기간만료", groupName = "차감 (-)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long expireAmt;

    @ApiModelProperty(value = "회수", position = 7)
    @RealGridColumnInfo(headText = "회수", groupName = "차감 (-)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long returnAmt;

    @ApiModelProperty(value = "잔액", position = 8)
    @RealGridColumnInfo(headText = "잔액", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long remainAmt;

    @ApiModelProperty(value = "당일 기말 합계", position = 9)
    @RealGridColumnInfo(headText = "당일 기말 합계", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long totalAmt;

}
