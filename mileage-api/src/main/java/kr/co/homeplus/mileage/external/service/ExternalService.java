package kr.co.homeplus.mileage.external.service;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.mileage.core.exception.MileageLogicException;
import kr.co.homeplus.mileage.enums.MileageCommonCode;
import kr.co.homeplus.mileage.enums.MileageResponseCode;
import kr.co.homeplus.mileage.external.model.ExternalItemSetDto;
import kr.co.homeplus.mileage.external.model.ExternalMileageSaveDto;
import kr.co.homeplus.mileage.external.model.ExternalMyPageGetDto;
import kr.co.homeplus.mileage.external.model.ExternalMyPageSetDto;
import kr.co.homeplus.mileage.external.model.ExternalOmniSetDto;
import kr.co.homeplus.mileage.external.model.ExternalPromoSetDto;
import kr.co.homeplus.mileage.external.model.cancel.ExternalSaveCancelSetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponUserRegDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentGetDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSetDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDetailDto;
import kr.co.homeplus.mileage.office.model.payment.MileageRequestDto;
import kr.co.homeplus.mileage.office.service.MileageCouponService;
import kr.co.homeplus.mileage.office.service.MileagePaymentService;
import kr.co.homeplus.mileage.utils.MileageCommonResponseFactory;
import kr.co.homeplus.mileage.utils.ObjectUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.client.model.ResponsePagination;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExternalService {

    private final ExternalMapperService mapperService;

    private final MileagePaymentService paymentService;

    private final MileageCouponService couponService;

    public ResponseObject<String> requestOmniSave(ExternalOmniSetDto externalOmniSetDto) throws Exception {
        LinkedHashMap<String, Object> parameter = ObjectUtils.getConvertMap(externalOmniSetDto);
        return mileageSaveProcess(createMileageRequestDto(parameter), createMileageReqDetailDto(parameter), "O");
    }

    public ResponseObject<String> requestItemSave(ExternalItemSetDto externalItemSetDto) throws Exception {
        LinkedHashMap<String, Object> parameter = ObjectUtils.getConvertMap(externalItemSetDto);
        return mileageSaveProcess(createMileageRequestDto(parameter), createMileageReqDetailDto(parameter), "P");
    }

    public ResponseObject<String> requestExternalMileageSave(ExternalPromoSetDto externalPromoSetDto) throws Exception {
        if(StringUtils.isEmpty(externalPromoSetDto.getMileageTypeNo())){
            throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_EXTERNAL_ERR01);
        }
        LinkedHashMap<String, Object> parameter = ObjectUtils.getConvertMap(externalPromoSetDto);
        MileageRequestDto requestDto = createMileageRequestDto(parameter);
        // 적립 지정
        requestDto.setMileageTypeNo(externalPromoSetDto.getMileageTypeNo());
        return mileageSaveProcess(requestDto, createMileageReqDetailDto(parameter), "MX");
    }

    public ResponseObject<String> requestExternalMileageSave(ExternalMileageSaveDto setDto) throws Exception {
        LinkedHashMap<String, Object> parameter = ObjectUtils.getConvertMap(setDto);
        MileageRequestDto requestDto = createMileageRequestDto(parameter);
        requestDto.setMileageTypeNo(mapperService.getMileageCodeTypeInfo(setDto.getMileageCode()));
        return this.mileageProcess(requestDto, createMileageReqDetailDto(parameter));
    }

    public ResponseObject<String> requestRetryExternalMileageSave(ExternalMileageSaveDto setDto) throws Exception {
        LinkedHashMap<String, Object> parameter = ObjectUtils.getConvertMap(setDto);
        MileageRequestDto requestDto = createMileageRequestDto(parameter);
        requestDto.setMileageTypeNo(mapperService.getMileageCodeTypeInfo(setDto.getMileageCode()));
        // 기적립 체크
        if(mapperService.getAlreadySaveCheck(setDto.getMileageCode(), setDto.getTradeNo(), setDto.getUserNo())){
            throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_EXTERNAL_SAVE_ERR01);
        }
        return this.mileageProcess(requestDto, createMileageReqDetailDto(parameter));
    }


    public ResponseObject<List<ExternalMyPageGetDto>> requestMileageList(ExternalMyPageSetDto externalMyPageSetDto) throws Exception {
        List<ExternalMyPageGetDto> mileageList = mapperService.getExternalMileageList(externalMyPageSetDto);
        int page = externalMyPageSetDto.getPage();
        int perPage = externalMyPageSetDto.getPerPage();
        externalMyPageSetDto.setPage(0);
        int totalCount = mapperService.getExternalMileageList(externalMyPageSetDto).size();
        return MileageCommonResponseFactory.getResponseObject(
            MileageResponseCode.MILEAGE_RESPONSE_SUCCESS,
            mileageList,
            ResponsePagination.Builder.builder().setAsPage(page, perPage, getTotalPage(totalCount, perPage)).totalCount(totalCount).build(),
            "마일리지 내역조회", "%s"
        );
    }

    public ResponseObject<String> requestExternalMileageReturn(ExternalSaveCancelSetDto setDto) throws Exception {
        LinkedHashMap<String, Object> parameter = ObjectUtils.getConvertMap(setDto);
        MileageRequestDto requestDto = createMileageRequestReturnDto(parameter);
        return this.mileageProcess(requestDto, createMileageReqDetailDto(parameter));
    }


    private ResponseObject<String> mileageProcess(MileageRequestDto requestDto, MileageReqDetailDto reqDetailDto) throws Exception {
        ResponseObject<MileagePaymentGetDto> resultDto = paymentService.setMileagePaymentInfo(MileagePaymentSetDto.builder().mileageRequestDto(requestDto).mileageReqDetailList(Collections.singletonList(reqDetailDto)).build());
        if(!resultDto.getReturnCode().equalsIgnoreCase("0000") ||
            (resultDto.getData().getPayment().getFail().getFailCnt() != 0 || resultDto.getData().getRegister().getFail().getFailCnt() != 0)
        ){
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_ERROR_0003, requestDto.getRequestReason(), "%s");
        }
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_SUCCESS, requestDto.getRequestReason(), "%s");
    }

    private ResponseObject<String> mileageSaveProcess(MileageRequestDto requestDto, MileageReqDetailDto reqDetailDto, String type) throws Exception {
        // type - O : 옴니, I : 구매평, P : 포토구매평 / null : 외부마일리지적립
        switch (type){
            case "O" :
                requestDto.setMileageTypeNo(getMileageTypeInfo(MileageCommonCode.MILEAGE_CATEGORY_SAVE, MileageCommonCode.MILEAGE_KIND_SAVE_04));
                break;
            case "I" :
                requestDto.setMileageTypeNo(getMileageTypeInfo(MileageCommonCode.MILEAGE_CATEGORY_SAVE, MileageCommonCode.MILEAGE_KIND_SAVE_11));
                break;
            case "P" :
                requestDto.setMileageTypeNo(getMileageTypeInfo(MileageCommonCode.MILEAGE_CATEGORY_SAVE, MileageCommonCode.MILEAGE_KIND_SAVE_12));
                break;
            default:
                break;
        }

        ResponseObject<MileagePaymentGetDto> resultDto = paymentService.setMileagePaymentInfo(MileagePaymentSetDto.builder().mileageRequestDto(requestDto).mileageReqDetailList(Collections.singletonList(reqDetailDto)).build());
        if(!resultDto.getReturnCode().equalsIgnoreCase("0000") ||
            (resultDto.getData().getPayment().getFail().getFailCnt() != 0 || resultDto.getData().getRegister().getFail().getFailCnt() != 0)
        ){
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_ERROR_0003, requestDto.getRequestReason(), "%s");
        }
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_SUCCESS, resultDto.getData().getMileageReqNo(), "%s");
    }

    public ResponseObject<String> mileageCouponUserRegister(MileageCouponUserRegDto regDto) throws Exception {
        return couponService.getMileageCouponUserRegister(regDto);
    }

    public String getMileageTypeInfo(MileageCommonCode mileageCategory, MileageCommonCode mileageKind){
        return mapperService.getMileageTypeInfo(mileageCategory.getResponseCode(), mileageKind.getResponseCode());
    }

    private MileageRequestDto createMileageRequestDto(LinkedHashMap<String, Object> param) {
        String requestReason = ObjectUtils.toString(param.get("requestReason"));
        return MileageRequestDto.builder()
            .requestType(MileageCommonCode.MILEAGE_REQUEST_TYPE_SAVE.getResponseCode())
            .requestReason(requestReason)
            .displayMessage(requestReason.length() > 20 ? requestReason.substring(0, 20) : requestReason)
            .requestMessage("외부연동 마일리지 요청")
            .processDt(new SimpleDateFormat("yyyy-MM-dd").format(new Date()))
            .useYn("Y")
            .directlyYn("Y")
            .regId(ObjectUtils.toString(param.get("regId")))
            .tradeNo(ObjectUtils.toString(param.get("tradeNo")))
            .expireDay(ObjectUtils.toInt(param.get("expireDay")))
            .build();
    }

    private MileageReqDetailDto createMileageReqDetailDto(LinkedHashMap<String, Object> param) {
        return MileageReqDetailDto.builder()
            .userNo(ObjectUtils.toString(param.get("userNo")))
            .mileageAmt(ObjectUtils.toInt(param.get("mileageAmt")))
            .mileageDetailStatus(MileageCommonCode.MILEAGE_REQUEST_STATUS_WAIT.getResponseMessage())
            .useYn("Y")
            .regId(ObjectUtils.toString(param.get("regId")))
            .chgId(ObjectUtils.toString(param.get("regId")))
            .build();
    }

    private MileageRequestDto createMileageRequestReturnDto(LinkedHashMap<String, Object> param) {
        String requestReason = ObjectUtils.toString(param.get("requestReason"));
        return MileageRequestDto.builder()
            .requestType(ObjectUtils.toString(param.get("cancelType")).equals("01") ? "03" : "04")
            .requestReason(requestReason)
            .displayMessage(requestReason.length() > 20 ? requestReason.substring(0, 20) : requestReason)
            .requestMessage("외부연동 마일리지 회수 요청")
            .processDt(new SimpleDateFormat("yyyy-MM-dd").format(new Date()))
            .useYn("Y")
            .directlyYn("Y")
            .regId(ObjectUtils.toString(param.get("regId")))
            .tradeNo(ObjectUtils.toString(param.get("tradeNo")))
            .build();
    }

    private Integer getTotalPage(int totalCount, int perPage) {
        int totalPage = totalCount / perPage;
        if(totalCount % perPage > 0){
            totalPage ++;
        }
        return totalPage;
    }

}
