package kr.co.homeplus.mileage.management.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import kr.co.homeplus.mileage.management.model.use.UseMileageSetDto;
import kr.co.homeplus.mileage.management.model.user.MileageMstDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.mileage.management.service.UseMileageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/use")
@Api(tags = "UseMileageController", value = "마일리지 사용 Controller")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
}
)
@RequiredArgsConstructor
public class UseMileageController {

    //
    private final UseMileageService useMileageService;

    @ApiOperation(value = "마일리지 사용")
    @PostMapping(value = "/mileageUse")
    public ResponseObject<MileageMstDto> setMileageUse(@RequestBody @Valid UseMileageSetDto useMileageSetDto) throws Exception {
        return useMileageService.setMileageUse(useMileageSetDto);
    }

}
