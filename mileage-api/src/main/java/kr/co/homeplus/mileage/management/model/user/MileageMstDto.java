package kr.co.homeplus.mileage.management.model.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MileageMstDto {

    @ApiModelProperty(value = "고객번호", position = 1)
    private String userNo;

    @ApiModelProperty(value = "마일리지금액", position = 2)
    private long mileageAmt;

    @ApiModelProperty(value = "승인번호", position = 3)
    private String approvalNo;

    @ApiModelProperty(value = "소멸예정금액(2주이내)", position = 3)
    private long expiredMileageAmt;
}
