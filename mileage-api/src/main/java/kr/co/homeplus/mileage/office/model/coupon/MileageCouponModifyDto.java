package kr.co.homeplus.mileage.office.model.coupon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@ApiModel(description = "마일리지 쿠폰등록 수정.")
public class MileageCouponModifyDto {
    @NotNull(message = "쿠폰관리번호")
    @ApiModelProperty(value = "쿠폰관리번호", position = 1)
    private long couponManageNo;
    @NotNull(message = "제목(쿠폰명)")
    @ApiModelProperty(value = "제목(쿠폰명)", position = 2)
    private String couponNm;
    @NotNull(message = "고객노출문구")
    @ApiModelProperty(value = "고객노출문구", position = 3)
    private String displayMessage;
    @NotNull(message = "사용여부")
    @ApiModelProperty(value = "사용여부", position = 4)
    private String useYn;
    @NotNull(message = "요청ID")
    @ApiModelProperty(value = "요청ID", position = 5)
    private String requestId;
}
