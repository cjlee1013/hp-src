package kr.co.homeplus.mileage.entity;

public class MileageCouponManageEntity {
    // 마일리지쿠폰관리번호
    public String couponManageNo;
    // 마일리지타입번호
    public String mileageTypeNo;
    // ONE:일회성,MUL:다회성
    public String couponType;
    // 쿠폰명
    public String couponNm;
    // 발급시작일
    public String issueStartDt;
    // 발급종료일
    public String issueEndDt;
    // 발급수량
    public String issueQty;
    // 발급 마일리지 금액
    public String mileageAmt;
    // 인당발급타입\nD:일별,P:기간별(period)
    public String personalIssueType;
    // 인당발급수량
    public String personalIssueQty;
    // 고객노출문구
    public String displayMessage;
    // 쿠폰번호(다회성)
    public String couponNo;
    //
    public String issueYn;
    // 사용여부
    public String useYn;
    // 등록ID
    public String regId;
    // 등록일
    public String regDt;
    // 수정ID
    public String chgId;
    // 수정일
    public String chgDt;

}
