package kr.co.homeplus.mileage.office.model.stat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "마일리지 통계 검색 조건")
public class MileageStatSumListSet {
    @ApiModelProperty(value = "검색날짜타입", position = 1) //D:일별, M:월별
    private String dateType;

    @ApiModelProperty(value = "조회 시작일", position = 2)
    private String StartDt;

    @ApiModelProperty(value = "조회 종료일", position = 3)
    private String EndDt;

    @ApiModelProperty(value = "마일리지 종류", position = 4)
    private String mileageKind;

}
