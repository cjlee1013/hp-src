package kr.co.homeplus.mileage.office.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponInfoDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponOneTimeDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponRequestDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponSearchGetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponSearchSetDto;
import kr.co.homeplus.mileage.office.model.coupon.MileageCouponUserInfoDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDto;
import kr.co.homeplus.mileage.management.model.save.SaveMileageDto;
import kr.co.homeplus.mileage.management.model.use.ReturnMileageSetDto;
import kr.co.homeplus.mileage.office.mapper.OfficeReadMapper;
import kr.co.homeplus.mileage.office.mapper.OfficeWriteMapper;
import kr.co.homeplus.mileage.office.model.history.MileageHistoryDto;
import kr.co.homeplus.mileage.office.model.history.MileageHistoryReqDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSearchGetDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSearchSetDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDetailDto;
import kr.co.homeplus.mileage.office.model.stat.MileageStatSumListGet;
import kr.co.homeplus.mileage.office.model.stat.MileageStatSumListSet;
import kr.co.homeplus.mileage.office.model.stat.MileageStatTypeSumListGet;
import kr.co.homeplus.mileage.office.model.type.MileagePromoTypeGetDto;
import kr.co.homeplus.mileage.office.model.type.MileagePromoTypeSetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeManageModifyDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeManageSetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeSearchGetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeSearchSetDto;
import kr.co.homeplus.mileage.office.service.OfficeMapperManageService;
import kr.co.homeplus.mileage.utils.MileageCommonUtil;
import kr.co.homeplus.mileage.utils.SqlUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OfficeMapperManageServiceImpl implements OfficeMapperManageService {
    // read Only - SlaveConnection Mapper
    private final OfficeReadMapper officeReadMapper;
    // write/read Possible - MasterConnection Mapper
    private final OfficeWriteMapper officeWriteMapper;

    //============================================================================================================
    // Back-Office 내역 조회 관련 Method
    //============================================================================================================
    // 마일리지 내역 조회
    @Override
    public List<MileageHistoryDto> getMileageHistoryInfo(MileageHistoryReqDto mileageHistoryReqDto){
        return officeReadMapper.selectMileageHistoryList(mileageHistoryReqDto);
    }
    //============================================================================================================

    //============================================================================================================
    // Back-Office 지급/회수 관련 Method
    //============================================================================================================
    /**
     * 기존 등록여부 확인
     *
     * @param mileageReqNo 마일리지 지급/회수 요청번호
     * @return Boolean 확인유무
     */
    @Override
    public Boolean checkMileageRequestInfo(String mileageReqNo){
        return MileageCommonUtil.isGreaterThanZero(officeReadMapper.selectMileageRequestInfoCheck(mileageReqNo));
    }

    /**
     * 마일리지 요청 수정데이터 체크
     * @param parameterMap 마일리지 지급/회수 수정 데이터
     * @return MileageReqDto 마일리지 지급/회수 요청 Entity
     */
    @Override
    public MileageReqDto getMileageReqModifyCheck(HashMap<String, Object> parameterMap){
        return officeReadMapper.selectMileageReqModifyCheck(parameterMap);
    }

    /**
     * 마일리지 요청정보 수정.
     *
     * @param parameterMap 마일리지 지급/회수 수정데이터 MAP
     * @return Boolean 수정성공여부
     */
    @Override
    public Boolean modifyMileageRequestInfo(HashMap<String, Object> parameterMap){
        return MileageCommonUtil.isGreaterThanZero(officeWriteMapper.updateMileageReqStatus(parameterMap));
    }

    /**
     * 마일리지 지급/회수 대상회원 상태 수정
     * @param parameterMap 마일리지 지급/회수 대상회원 상태변경 데이터
     * @return CompletableFuture<Boolean>
     */
    @Override
    @Async
    public CompletableFuture<Boolean> modifyMileageRequestDetailInfo(HashMap<String, Object> parameterMap){
        try {
            return CompletableFuture.completedFuture(
                MileageCommonUtil.isGreaterThanZero(
                    officeWriteMapper.updateMileageReqUserStatus(parameterMap)
                )
            );
        } catch (Exception e){
            log.error("마일리지 적립/회수요청 상세정보 수정 중 오류발생 : [{}]", e.getMessage());
            return CompletableFuture.completedFuture(Boolean.FALSE);
        }
    }

    /**
     * 마일리지 지급/회수 요청 등록
     *
     * @param mileageReqDto 마일리지 지급/회수 Entity.
     * @return Boolean 등록성공여부
     */
    @Override
    public Boolean addMileageRequestInfo(MileageReqDto mileageReqDto){
        return MileageCommonUtil.isGreaterThanZero(officeWriteMapper.insertMileageRequestInfo(mileageReqDto));
    }

    /**
     * 마일리지 적립/회수요청 상세정보 등록(Async)
     *
     * @param mileageReqDetailDto 마일리지 지급/회수 대상회원 등록 DTO
     * @return CompletableFuture<Boolean>
     */
    @Override
    @Async
    public CompletableFuture<Boolean> addMileageRequestDetailInfo(MileageReqDetailDto mileageReqDetailDto) {
        try {
            if(mileageReqDetailDto.getMileageAmt() < 0){
                return CompletableFuture.completedFuture(Boolean.FALSE);
            }
            if(MileageCommonUtil.isGreaterThanZero(officeWriteMapper.insertMileageRequestDetailInfo(mileageReqDetailDto))){
                return CompletableFuture.completedFuture(mileageReqDetailDto.getDetailCode().equals("00") ? Boolean.TRUE : Boolean.FALSE);
            }
            return CompletableFuture.completedFuture(Boolean.FALSE);
        } catch (Exception e){
            log.error("마일리지 적립/회수요청 상세정보 등록 중 오류발생 : [{}]", e.getMessage());
            return CompletableFuture.completedFuture(Boolean.FALSE);
        }
    }

    /**
     * 마일리지 회수 정보 조회(회수데이터 생성)
     *
     * @param mileageReqNo 마일리지 지급/회수 요청번호
     * @return List<ReturnMileageSetDto> 마일리지 회수 Data List.
     */
    @Override
    public List<ReturnMileageSetDto> getMileageReturnInfo(long mileageReqNo) {
        return officeWriteMapper.selectMakeMileageReturnInfo(mileageReqNo);
    }

    /**
     * 마일리지 적립 정보 조회(적립데이터 생성)
     *
     * @param mileageReqNo 마일리지 지급/회수 요청번호
     * @return List<SaveMileageDto>
     */
    @Override
    public List<SaveMileageDto> getSaveMileageInfoList(long mileageReqNo, int expireDay){
        return officeWriteMapper.selectMakeMileageSaveInfo(mileageReqNo, expireDay);
    }

    /**
     * 마일리지 지급/회수 조회
     *
     * @param mileagePaymentSearchSetDto 마일리지 지급/회수 조회 DTO
     * @return List<MileagePaymentSearchGetDto> 마일리지 지급/회수 조회 결과 DTO
     */
    @Override
    public List<MileagePaymentSearchGetDto> getSearchMileagePaymentInfo(
        MileagePaymentSearchSetDto mileagePaymentSearchSetDto) {
        return officeReadMapper.selectMileagePaymentInfoList(mileagePaymentSearchSetDto);
    }

    /**
     * 마일리지 적립/회수 요청정보 조회
     *
     * @param mileageReqNo 마일리지 적립/회수 요청번호
     * @return MileageReqDto 마일리지 지급/회수 Entity
     */
    @Override
    public MileageReqDto getSearchMileagePaymentBasicInfo(String mileageReqNo){
        return officeReadMapper.selectMileageRequestInfoList(mileageReqNo);
    }

    /**
     * 마일리지 적립/회수 대상회원 조회
     *
     * @param mileageReqNo 마일리지 적립/회수 요청번호
     * @return List<MileageReqDetailDto> 마일리지 적립/회수 대상회원 DTO
     */
    @Override
    public List<MileageReqDetailDto> getSearchMileagePaymentUserInfo(String mileageReqNo){
        return officeReadMapper.selectMileageRequestDetailInfoList(mileageReqNo);
    }
    //============================================================================================================

    //============================================================================================================
    // Back-Office 유형 관련 Method
    //============================================================================================================
    /**
     * 마일리지 유형 존재 유무.
     *
     * @param mileageTypeNo 마일리지 유형번호
     * @return Boolean 존재유무
     */
    @Override
    public Boolean checkMileageTypeInfo(String mileageTypeNo){
        //mapper SELECT 쿼리.
        return MileageCommonUtil.isGreaterThanZero(officeReadMapper.selectMileageTypeInfoCheck(mileageTypeNo));
    }

    /**
     * 마일리지 타입 등록
     * 
     * @param mileageTypeManageSetDto 마일리지 타입 등록 DTO
     * @return Boolean 등록여부
     */
    @Override
    public Boolean setMileageTypeInfo(MileageTypeManageSetDto mileageTypeManageSetDto){
        return MileageCommonUtil.isGreaterThanZero(officeWriteMapper.insertMileageTypeInfo(mileageTypeManageSetDto));
    }

    /**
     * 마일리지 타입 수정
     * 
     * @param parameterMap 마일리지 타입 수정정보 Map
     * @return Boolean 수정여부
     */
    @Override
    public Boolean modifyMileageTypeInfo(HashMap<String, Object> parameterMap){
        return MileageCommonUtil.isGreaterThanZero(officeWriteMapper.updateMileageTypeInfo(parameterMap));
    }

    /**
     * 마일리지 유형 정보 조회
     * 
     * @param mileageTypeSearchSetDto 마일리지 유형 정보 조회 DTO
     * @return List<MileageTypeSearchGetDto> 마일리지 유형 정보 조회 응답 DTO 
     */
    @Override
    public List<MileageTypeSearchGetDto> getMileageTypeInfoList(MileageTypeSearchSetDto mileageTypeSearchSetDto){
        return officeReadMapper.selectMileageTypeInfo(mileageTypeSearchSetDto);
    }

    /**
     * 마일리지 타입 기본정보 조회
     * 
     * @param mileageTypeNo 마일리지유형관리번호
     * @return MileageTypeManageModifyDto 마일리지 수정 DTO
     */
    @Override
    public MileageTypeManageModifyDto getMileageTypeModifyInfo(String mileageTypeNo){
        return officeReadMapper.selectMileageTypeBasicInfo(Long.parseLong(mileageTypeNo));
    }
    
    /**
     * 마일리지 타입 수정 대상 확인
     * 
     * @param mileageTypeManageModifyDto 마일리지 수정 DTO
     * @return HashMap<String, Object> 수정 대상 정보 Map
     */
    @Override
    public HashMap<String, Object> getMileageTypeModifyCheck(MileageTypeManageModifyDto mileageTypeManageModifyDto){
        return officeReadMapper.selectMileageTypeModifyCheck(mileageTypeManageModifyDto);
    }

    @Override
    public String getMileageTypeByReturn(){
        try {
            return officeReadMapper.selectMileageTypeNoByCategory();
        } catch (Exception e){
            log.error("getMileageTypeByReturn Error :: {}", e.getMessage());
            return "";
        }
    }

    @Override
    public List<MileagePromoTypeGetDto> getMileagePromotionTypeInfo(MileagePromoTypeSetDto setDto) {
        return officeReadMapper.selectMileagePromotionTypeInfo(setDto);
    }

    @Override
    public boolean isMileageCodeDuplication(String mileageCode) {
        return SqlUtils.isGreaterThanZero(officeReadMapper.selectMileageTypeCodeCheck(mileageCode));
    }

    @Override
    public String getMileageReturnType(String tradeNo, String mileageCode) {
        return officeReadMapper.selectMileageReturnType(tradeNo, mileageCode);
    }

    @Override
    public String getMileageCouponReturnType(long couponInfoSeq) {
        return officeReadMapper.selectMileageCouponType(couponInfoSeq);
    }
    //============================================================================================================

    //============================================================================================================
    // Back-Office 마일리지 등록
    //============================================================================================================
    @Override
    public Boolean addMileageCouponManage(MileageCouponRequestDto setDto) {
        return SqlUtils.isGreaterThanZero(officeWriteMapper.insertMileageCouponManage(setDto));
    }
    @Override
    public void requestMileageCouponPublish(long couponManageNo, long issueQty) {
        LinkedHashMap<String, String> parameter = new LinkedHashMap<>();
        parameter.put("couponManageNo", String.valueOf(couponManageNo));
        parameter.put("issueQty", String.valueOf(issueQty));
        officeWriteMapper.callByMileageCouponPublish(parameter);
    }
    // 쿠폰정보조회
    @Override
    public LinkedHashMap<String, Object> getMileageCouponInfo(String couponNo) {
        return officeReadMapper.selectMileageCouponInfo(couponNo);
    }
    // 쿠폰등록건수조회
    @Override
    public LinkedHashMap<String, Long> getMileageCouponRegCheck(LinkedHashMap<String, Object> parameterMap){
        return officeReadMapper.selectMileageCouponRegCheck(parameterMap);
    }
    // 마일리지쿠폰등록(다회성)
    @Override
    public Boolean addMileageCouponInfoByMulti(HashMap<String, Object> parameterMap){
        return SqlUtils.isGreaterThanZero(officeWriteMapper.insertMileageCouponInfo(parameterMap));
    }
    // 마일리지 쿠폰등록(일회성), 마일리지 쿠폰정보 업데이트(마일리지적립정보, 회수정보)
    @Override
    public Boolean modifyMileageCouponInfo(HashMap<String, Object> parameterMap){
        return SqlUtils.isGreaterThanZero(officeWriteMapper.updateMileageCouponInfo(parameterMap));
    }
    // 마일리지 쿠폰등록요청 정보 업데이트
    @Override
    public Boolean modifyMileageCouponMange(HashMap<String, Object> parameterMap){
        return SqlUtils.isGreaterThanZero(officeWriteMapper.updateMileageCouponManage(parameterMap));
    }
    // 마일리지 다회성 기등록건 체크
    @Override
    public LinkedHashMap<String, Object> getMileageMultiCouponRegCheck(String couponNo){
        return officeReadMapper.selectMileageMultiCouponRegCheck(couponNo);
    }
    @Override
    public String getMileageSaveNo(String mileageReqNo) {
        return officeWriteMapper.selectFindMileageCouponSaveNo(mileageReqNo);
    }
    // 마일리지 등록 토탈 횟수
    @Override
    public long getCouponTotalRegCount(long couponManageNo){
        return officeReadMapper.selectCouponIsPossibleCheck(couponManageNo);
    }
    // 쿠폰 등록 가능 여부
    @Override
    public LinkedHashMap<String, String> getMileageCouponRegIsPossibleCheck(String couponNo){
        return officeReadMapper.selectCouponUsedCheck(couponNo);
    }
    // 마일리지 쿠폰 관리 조회
    @Override
    public List<MileageCouponSearchGetDto> getMileageCouponManageInfoList(MileageCouponSearchSetDto setDto) {
        return officeReadMapper.selectCouponManageInfoList(setDto);
    }
    // 마일리지 쿠폰정보
    @Override
    public MileageCouponInfoDto getCouponInfo(long couponManageNo) {
        return officeReadMapper.selectMileageCouponManageInfo(couponManageNo);
    }
    // 마일리지 쿠폰 - 유저정보
    @Override
    public List<MileageCouponUserInfoDto> getMileageCouponUserInfo(long couponManageNo) {
        return officeReadMapper.selectMileageCouponUserInfo(couponManageNo);
    }

    @Override
    public List<MileageCouponOneTimeDto> getCouponOneTimeList(long couponManageNo) {
        return officeReadMapper.selectCouponOneTimeInfo(couponManageNo);
    }

    @Override
    public boolean modifyMileageCouponReturn(long couponInfoSeq, String requestId, boolean isComplete) {
        return SqlUtils.isGreaterThanZero(officeWriteMapper.updateMileageCouponInfoReturn(couponInfoSeq, requestId, isComplete));
    }

    //마일리지 일자별 금액 합계 통계 조회
    @Override
    public List<MileageStatSumListGet> getDailyMileageSummary(MileageStatSumListSet requestDto){
        return officeReadMapper.getDailyMileageSummary(requestDto);

    }
    //마일리지 일자/타입별 금액 합계 통계 조회
    @Override
    public List<MileageStatTypeSumListGet> getDailyMileageTypeSummary(MileageStatSumListSet requestDto){
        return officeReadMapper.getDailyMileageTypeSummary(requestDto);
    }

    //============================================================================================================
}
