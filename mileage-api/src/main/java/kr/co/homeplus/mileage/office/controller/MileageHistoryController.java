package kr.co.homeplus.mileage.office.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.mileage.office.model.history.MileageHistoryDto;
import kr.co.homeplus.mileage.office.model.history.MileageHistoryResDto;
import kr.co.homeplus.mileage.office.service.MileageHistoryService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.mileage.office.model.history.MileageHistoryReqDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/office/history")
@Api(tags = "MileageHistoryController", value = "BackOffice 마일리지 내역 조회 Controller")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
}
)
@RequiredArgsConstructor
public class MileageHistoryController {

    private final MileageHistoryService historyService;

    @ApiOperation(value = "마일리지 내역 조회 요청", response = MileageHistoryResDto.class)
    @PostMapping("/getMileageHistoryList")
    public ResponseObject<MileageHistoryResDto> getMileageHistoryList(@RequestBody MileageHistoryReqDto mileageHistoryReqDto) throws Exception {
        return historyService.getMileageHistoryList(mileageHistoryReqDto);
    }

    @ApiOperation(value = "마일리지 내역 조회 요청(고객센터)", response = MileageHistoryDto.class)
    @PostMapping("/getMileageList")
    public ResponseObject<List<MileageHistoryDto>> getCustomerMileageList(@RequestBody @Valid MileageHistoryReqDto mileageHistoryReqDto) throws Exception {
        return historyService.getCustomerMileageList(mileageHistoryReqDto);
    }

}
