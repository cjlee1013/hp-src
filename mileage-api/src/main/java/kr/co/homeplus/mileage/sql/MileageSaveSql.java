package kr.co.homeplus.mileage.sql;

import kr.co.homeplus.mileage.constants.CustomConstants;
import kr.co.homeplus.mileage.constants.SqlPatternConstants;
import kr.co.homeplus.mileage.entity.MileageSaveEntity;
import kr.co.homeplus.mileage.management.model.save.SaveMileageDto;
import kr.co.homeplus.mileage.utils.EntityFactory;
import kr.co.homeplus.mileage.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MileageSaveSql {

    // 마일리지 사용 테이블명.
    public final static String saveTable = EntityFactory.getTableName(MileageSaveEntity.class);

    public final static MileageSaveEntity saveColumns = EntityFactory.createEntityIntoValue(MileageSaveEntity.class);

    //CALLABLE
    public static String selectUserMileageUsableList(@Param("userNo") String userNo, @Param("requestAmt") long requestAmt) {
        return SqlUtils.getConditionForProcedure("UP_NT_MILEAGE_USABLE_CHECK", userNo, requestAmt);
    }

    /**
     * 마일리지 적립 등록
     *
     * @param saveMileageDto 마일리지 적립 등록 DTO
     * @return String 마일리지 적립 등록
     */
    public static String insertUserMileageSave(SaveMileageDto saveMileageDto){
        SQL query = new SQL()
            .INSERT_INTO(saveTable)
            .INTO_COLUMNS(
                saveColumns.mileageSaveNo,
                saveColumns.mileageReqDetailNo,
                saveColumns.mileageTypeNo,
                saveColumns.userNo,
                saveColumns.mileageCategory,
                saveColumns.mileageKind,
                saveColumns.mileageAmt,
                saveColumns.mileageRemainAmt,
                saveColumns.useStartDt,
                saveColumns.expireDt,
                saveColumns.regDt,
                saveColumns.regId,
                saveColumns.chgDt,
                saveColumns.chgId,
                saveColumns.requestReason
            )
            .INTO_VALUES(
                SqlUtils.convertInsertIntoValue(
                    saveMileageDto.getMileageSaveNo(),
                    saveMileageDto.getMileageReqDetailNo(),
                    saveMileageDto.getMileageTypeNo(),
                    saveMileageDto.getUserNo(),
                    saveMileageDto.getMileageCategory(),
                    saveMileageDto.getMileageKind(),
                    saveMileageDto.getMileageAmt(),
                    saveMileageDto.getMileageAmt(),
                    saveMileageDto.getUseStartDt(),
                    saveMileageDto.getExpireDt(),
                    SqlPatternConstants.NOW,
                    saveMileageDto.getRegId(),
                    SqlPatternConstants.NOW,
                    (saveMileageDto.getChgId() != null ? saveMileageDto.getChgId() : saveMileageDto.getRegId()),
                    saveMileageDto.getRequestReason()
                )
            );

        log.info("insertUserMileageSave \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 적립취소시 적립 마일리지 사용처리
     *
     * @param saveMileageDto 마일리지 적립 정보 DTO
     * @return String 마일리지 적립 사용처리 쿼리
     */
    public static String updateUserMileageSaveUsed(SaveMileageDto saveMileageDto){
        SQL query = new SQL()
            .UPDATE(saveTable)
            .SET(
                SqlUtils.conditionValueForUpdateCalculation(saveColumns.mileageRemainAmt, saveMileageDto.getMileageRemainAmt(), CustomConstants.MINUS)
            )
            .SET(SqlUtils.nonSingleQuote(saveColumns.chgDt, "NOW()"))
            .SET(SqlUtils.equalColumnMapping(saveColumns.chgId))
            .WHERE(
                SqlUtils.equalColumnMapping(saveColumns.mileageSaveNo),
                SqlUtils.equalColumnMapping(saveColumns.userNo)
            );

        log.debug("updateUserMileageSaveUsed \n[{}]", query.toString());
        return query.toString();
    }

}
