package kr.co.homeplus.mileage.enums;

import kr.co.homeplus.mileage.enums.impl.EnumImpl;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ActivityMileageCode implements EnumImpl {

      MILEAGE_SYSTEM_ERR_9001("9001", "변환을 지원하지 않는 CLASS 입니다. (%s)")
    , MILEAGE_SYSTEM_ERR_9002("9002", "변환 할 수 없는 타입입니다.")

    , MILEAGE_SUCCESS("0000", "마일리지 %s에 성공하였습니다.")
    //마일리지 적립
    , MILEAGE_SAVE_SUCCESS("0000", "마일리지 적립에 성공하였습니다.")
    , MILEAGE_SAVE_ERR_01("3101", "마일리지 마스터 DB에 회원의 정보가 없습니다.")
    , MILEAGE_SAVE_ERR_02("3102", "마일리지 적립 DB에 적재가 실패하였습니다.(mileage_save insert 실패)")
    , MILEAGE_SAVE_ERR_03("3103", "마일리지 마스터에 고객 잔여 마일리지 업데이트 실패")
    , MILEAGE_SAVE_ERR_04("3104", "마일리지 히스토리 기록 실패")
    , MILEAGE_SAVE_ERR_05("3105", "마일리지 적립 DB에 적재가 실패하였습니다.(mileage_save insert 실패)")

    //마일리지 사용
    , MILEAGE_USE_SUCCESS("0000", "마일리지 사용에 성공하였습니다.")
    , MILEAGE_USE_ERR_01("3301", "마일리지 마스터에 회원 정보가 없습니다.")
    , MILEAGE_USE_ERR_02("3302", "사용가능 마일리지가 요청한 마일리지보다 적습니다.")
    , MILEAGE_USE_ERR_03("3303", "마일리지 사용처리에 실패하였습니다.(mileage_use insert 실패)")
    , MILEAGE_USE_ERR_04("3304", "마일리지 적립 DB 업데이트에 실패하였습니다.(mileage_remain_amt update 실패)")
    , MILEAGE_USE_ERR_05("3305", "마일리지 마스터에 고객 잔여 마일리지 업데이트 실패(mileage_amt 복원실패)")
    , MILEAGE_USE_ERR_06("3306", "마일리지 히스토리에 복원기록 적재에 실패 하였습니다.")
    , MILEAGE_USE_ERR_07("3307", "마일리지 관리에 정보를 저장하지 못하였습니다..")
    , MILEAGE_USE_ERR_08("3308", "사용가능한 마일리지가 없습니다.")
    , MILEAGE_USE_ERR_09("3309", "이미 마일리지를 사용한 거래번호입니다.")

    //유저 마일리지 계정 등록
    , MILEAGE_USER_REGISTER_SUCCESS("SUCCESS", "유저의 마일리지 계정 등록에 성공하였습니다.")
    , MILEAGE_USER_REGISTER_ERR_01("3001", "이미 해당 유저의 마일리지 계정이 존재합니다.")
    , MILEAGE_USER_REGISTER_ERR_02("3002", "유저의 마일리지 계정 등록중 실패하였습니다.")

    , MILEAGE_USER_ACCOUNT_CHECK_SUCCESS("0000", "유저의 마일리지 잔액조회에 성공하였습니다.")
    , MILEAGE_USER_ACCOUNT_CHECK_ERR_01("3011", "유저의 마일리지 계정정보가 없습니다.")
    , MILEAGE_USER_ACCOUNT_CHECK_ERR_02("3012", "유저의 마일리지 잔액조회에 실패하였습니다.")

    , MILEAGE_TYPE_ERR_3531("3531", "필수 파라미터(%s)가 없습니다.")

    //사용 취소
    , MILEAGE_CANCEL_SUCCESS("0000", "마일리지 사용취소에 성공하였습니다.")
    , MILEAGE_CANCEL_ERR_01("3211", "유저의 마일리지 계정정보가 없습니다.")
    , MILEAGE_CANCEL_ERR_02("3212", "마일리지 사용내역이 없습니다.")
    , MILEAGE_CANCEL_ERR_03("3213", "마일리지 적립에 실패하였습니다. 재시도 부탁드립니다.")
    , MILEAGE_CANCEL_ERR_04("3214", "마일리지 마스터에 고객 잔여 마일리지 업데이트 실패(mileage_amt 복원실패)")
    , MILEAGE_CANCEL_ERR_05("3215", "마일리지 히스토리에 복원기록 적재에 실패 하였습니다.")
    , MILEAGE_CANCEL_ERR_06("3216", "마일리지 취소코드가 잘못 되었습니다.")
    , MILEAGE_CANCEL_ERR_07("3217", "마일리지 취소요청 금액이 잘못 되었습니다.")
    , MILEAGE_CANCEL_ERR_08("3218", "이미 취소처리가 완료된 건입니다.")
    , MILEAGE_CANCEL_ERR_09("3219", "마일리지 사용처리가 실패되었습니다. 재시도 부탁드립니다.")
    , MILEAGE_CANCEL_ERR_10("3220", "마일리지 취소금액이 잘못되었습니다.")

    //적립 취소
    , MILEAGE_RESTORE_SUCCESS("0000", "마일리지 회수에 성공하였습니다.")
    , MILEAGE_RESTORE_ERR_01("3201", "마일리지 마스터에 회원 정보가 없습니다.")
    , MILEAGE_RESTORE_ERR_02("3202", "마일리지 적립내역이 없습니다.")
    , MILEAGE_RESTORE_ERR_03("3203", "마일리지 사용처리에 실패하였습니다.(mileage_use insert 실패)")
    , MILEAGE_RESTORE_ERR_04("3204", "마일리지 적립 DB 업데이트에 실패하였습니다.(mileage_remain_amt 복원 실패)")
    , MILEAGE_RESTORE_ERR_05("3205", "마일리지 마스터에 고객 잔여 마일리지 업데이트 실패(mileage_amt 복원실패)")
    , MILEAGE_RESTORE_ERR_06("3206", "마일리지 히스토리에 복원기록 적재에 실패 하였습니다.");

    private final String responseCode;
    private final String responseMessage;

}
