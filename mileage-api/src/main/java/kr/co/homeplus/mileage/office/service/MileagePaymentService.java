package kr.co.homeplus.mileage.office.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import kr.co.homeplus.mileage.core.exception.MileageLogicException;
import kr.co.homeplus.mileage.enums.ExceptionCode;
import kr.co.homeplus.mileage.enums.MileageCommonCode;
import kr.co.homeplus.mileage.enums.MileageResponseCode;
import kr.co.homeplus.mileage.enums.MileageSaveCode;
import kr.co.homeplus.mileage.management.model.save.SaveMileageDto;
import kr.co.homeplus.mileage.management.model.use.ReturnMileageSetDto;
import kr.co.homeplus.mileage.management.service.SaveMileageService;
import kr.co.homeplus.mileage.management.service.UseMileageService;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentGetDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentGetDto.MileagePaymentDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentModifySetDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSearchGetDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSearchSetDto;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSetDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDetailDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDto;
import kr.co.homeplus.mileage.office.model.payment.MileageRequestDto;
import kr.co.homeplus.mileage.utils.MileageCommonResponseFactory;
import kr.co.homeplus.mileage.utils.MileageCommonUtil;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class MileagePaymentService {
    // Office Mapper Management Service
    private final OfficeMapperManageService officeMapperService;
    //
    private final SaveMileageService saveMileageService;
    //
    private final UseMileageService useMileageService;

    /**
     * 마일리지 지급/회수 조회
     *
     * @param mileagePaymentSearchSetDto 마일리지 지급/회수 조회 DTO
     * @return ResponseObject<List<MileagePaymentSearchGetDto>>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public ResponseObject<List<MileagePaymentSearchGetDto>> getSearchMileagePaymentInfo(MileagePaymentSearchSetDto mileagePaymentSearchSetDto) throws Exception {
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_SUCCESS, officeMapperService.getSearchMileagePaymentInfo(mileagePaymentSearchSetDto), "마일리지 지급/회수 내역 조회", "%s");
    }

    /**
     * 마일리지 적립/회수 요청정보(기본정보) 조회
     *
     * @param mileageReqNo 마일리지 적립/회수 요청번호
     * @return ResponseObject<MileageReqDto>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public ResponseObject<MileageReqDto> getSearchMileagePaymentBasicInfo(String mileageReqNo) throws Exception {
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_SUCCESS, officeMapperService.getSearchMileagePaymentBasicInfo(mileageReqNo), "마일리지 지급/회수 기본정보조회", "%s");
    }

    /**
     * 마일리지 적립/회수 요청정보(유저정보) 조회
     *
     * @param mileageReqNo 마일리지 적립/회수 요청번호
     * @return ResponseObject<List<MileageReqDetailDto>>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public ResponseObject<List<MileageReqDetailDto>> getSearchMileagePaymentUserInfo(String mileageReqNo) throws Exception {
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_SUCCESS, officeMapperService.getSearchMileagePaymentUserInfo(mileageReqNo), "마일리지 지급/회수 대상회원 조회", "%s");
    }

    /**
     * 마일리지 지급/회수 요청
     *
     * @param mileagePaymentSetDto 마일리지 지급/회수 등록 DTO
     * @return ResponseObject<MileagePaymentGetDto>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    @Transactional(rollbackFor = {Exception.class, MileageLogicException.class})
    public ResponseObject<MileagePaymentGetDto> setMileagePaymentInfo(MileagePaymentSetDto mileagePaymentSetDto) throws Exception{
        log.info("마일리지 지급/회수 요청 :: {}", mileagePaymentSetDto);
        if(mileagePaymentSetDto.getMileageReqDetailList().size() == 0){
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_ERROR_0001, "마일리지 유형", "%s");
        }

        try {
            log.info("mileagePaymentSetDto :: {}", mileagePaymentSetDto);
            // 마일리지 타입 정보 취득
            String mileageTypeNo = getConvertRequestType(mileagePaymentSetDto.getMileageRequestDto());
            // 마일리지 타입 없으면 에러
            if(StringUtils.isEmpty(mileageTypeNo)){
                return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_ERROR_0001, "마일리지 유형", "%s");
            }
            // 마일리지 회수 타입 설정.
            mileagePaymentSetDto.getMileageRequestDto().setMileageTypeNo(mileageTypeNo);
            //마일리지 유형이 존재하는 지 확인.
            if(!officeMapperService.checkMileageTypeInfo(mileagePaymentSetDto.getMileageRequestDto().getMileageTypeNo())){
                return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_ERROR_0001, "마일리지 유형", "%s");
            }
        } catch (Exception e){
            log.error("setMileagePaymentInfo 마일리지 타입 조회 실패 :: {}", e.getMessage());
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_ERROR_0001, "마일리지 유형", "%s");
        }
        //마일리지 요청 성공시 요청번호 리턴.
        MileageReqDto mileageReqDto = this.addMileageRequestInfo(mileagePaymentSetDto.getMileageRequestDto());
        // 마일리지 요청 번호가 없으면 실패처리
        if(mileageReqDto.getMileageReqNo() == null){
            log.error("마일리지 지급/회수 요청 실패 : {}", mileageReqDto.getMileageReqNo());
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_ERROR_0002, "마일리지 요청 등록", "%s");
        }
        //결과 DTO 설정.
        MileagePaymentGetDto resultDto = new MileagePaymentGetDto();
        // 마일리지 지급/회수유저정보 처리.
        for(MileageReqDetailDto detailDto : mileagePaymentSetDto.getMileageReqDetailList()){
            //마일리지요청번호
            detailDto.setMileageReqNo(mileageReqDto.getMileageReqNo());
            //마일리지상세상태(대기)
            detailDto.setMileageDetailStatus(this.convertPaymentRequestType(detailDto.getMileageDetailStatus()));
            //마일리지요청상세코드(오류)
            detailDto.setDetailCode(this.getMileageDetailRuleCheckCode(mileageReqDto.getRequestType(), detailDto));
            //마일리지상세데이터사용유무
            detailDto.setUseYn("Y");
            // 마일리지요청상세 정보로그
            log.info("마일리지 지급/회수 상세 요청 정보 :: {}", detailDto);
            //마일리지요청상세에 등록(오류발생시 NULL)
            CompletableFuture<Boolean> future = officeMapperService.addMileageRequestDetailInfo(detailDto);
            if(future.get()){
                //성공시
                resultDto.register.success.totalAddByData((int)detailDto.getMileageAmt());
            }else{
                //실패시
                resultDto.register.fail.totalAddByData((int)detailDto.getMileageAmt());
            }
        }
        //즉시 적립인 경우
        if(mileagePaymentSetDto.getMileageRequestDto().getDirectlyYn().equals("Y")){
            if (this.modifyMileageRequestInfoByKey(
                mileageReqDto.getMileageReqNo(),
                mileagePaymentSetDto.getMileageRequestDto().getRegId()
            )){
                if(mileagePaymentSetDto.getMileageRequestDto().getRequestType().equals("01")){
                    resultDto.setPayment(this.actMileageSave(mileageReqDto.getMileageReqNo(), mileagePaymentSetDto.getMileageRequestDto().getExpireDay(), mileagePaymentSetDto.getMileageRequestDto().getRegId()).get());
                }else{
                    // 회수처리.
                    resultDto.setPayment(this.actMileageReturn(mileageReqDto.getMileageReqNo(), mileagePaymentSetDto.getMileageRequestDto().getRegId()).get());
                }
            }
            resultDto.setMileageReqNo(mileageReqDto.getMileageReqNo());
        }
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_SUCCESS, resultDto, "마일리지 요청", "%s");
    }

    /**
     * 마일리지 요청 수정.
     *
     * @param mileagePaymentModifySetDto 마일리지 지급/회수 수정 DTO
     * @return ResponseObject<MileagePaymentGetDto>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public ResponseObject<MileagePaymentGetDto> setModifyMileagePaymentInfo(MileagePaymentModifySetDto mileagePaymentModifySetDto) throws Exception{

        if(!officeMapperService.checkMileageRequestInfo(mileagePaymentModifySetDto.getMileageReqNo())){
            log.error("마일리지 기본정보 확인(요청번호) 오류 : [{}]", mileagePaymentModifySetDto.getMileageReqNo());
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_ERROR_0001, "마일리지 기본정보", "%s");
        }

        //        mileageRequestDetailSetDto.getMileageReqDetailList().stream().filter(distinctByKey(m -> m.getUserNo())).forEach(mileageReqDetailDto -> log.info("{}", mileageReqDetailDto));

        //마일리지 요청 수정용 데이터 체크
        HashMap<String, Object> modifyParameterMap = MileageCommonUtil.getConvertMap(mileagePaymentModifySetDto.getMileageRequestDto());
        //거래번호 입력.
        modifyParameterMap.put("mileageReqNo", mileagePaymentModifySetDto.getMileageReqNo());

        //업데이트를 위한 차이점 체크데이터 맵.
        HashMap<String, Object> modifyDataMap = this.getModifyMileageReqMap(modifyParameterMap);

        //업데이트 대상이 없는 경우.
        if(modifyDataMap == null && mileagePaymentModifySetDto.getMileageReqDetailList().size() == 0){
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COMMON_ERROR_01);
        }

        if(modifyDataMap != null){
            //업데이트
            if(!officeMapperService.modifyMileageRequestInfo(modifyDataMap)){
                return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_ERROR_0001, "마일리지 기본정보 수정", "%s");
            }
        }
        //결과 DTO 설정.
        MileagePaymentGetDto resultDto = new MileagePaymentGetDto();
        if(mileagePaymentModifySetDto.getMileageReqDetailList().size() != 0){
            //상세정보 업데이트(삭제건 먼저 처리)
            for(MileageReqDetailDto detailDto : mileagePaymentModifySetDto.getMileageReqDetailList().stream().filter(x -> x.getUseYn().equals("N")).collect(Collectors.toList())){
                //수정아이디 셋팅(regId = chgId)
                detailDto.setChgId(detailDto.getRegId());
                //마일리지상세상태
                detailDto.setMileageDetailStatus(this.convertPaymentRequestType(detailDto.getMileageDetailStatus()));
                //마일리지요청상세에 등록(오류발생시 NULL)
                @SuppressWarnings("unchecked")
                CompletableFuture<Boolean> future = officeMapperService.modifyMileageRequestDetailInfo(new HashMap<String, Object>(BeanUtils.describe(detailDto)));

                if(future.get()){
                    resultDto.register.success.totalAddByData((int)detailDto.getMileageAmt());
                }else{
                    //실패시(DB 입력실패)
                    resultDto.register.fail.totalAddByData((int)detailDto.getMileageAmt());
                }
            }

            for(MileageReqDetailDto detailDto : mileagePaymentModifySetDto.getMileageReqDetailList().stream().filter(x -> x.getUseYn().equals("Y")).collect(Collectors.toList())){
                log.info("detailDto(Y) : {}", detailDto);
                //마일리지요청번호
                detailDto.setMileageReqNo(mileagePaymentModifySetDto.getMileageReqNo());
                //마일리지상세상태(대기)
                detailDto.setMileageDetailStatus(this.convertPaymentRequestType(detailDto.getMileageDetailStatus()));
                //마일리지요청상세코드(오류)
                detailDto.setDetailCode(this.getMileageDetailRuleCheckCode(mileagePaymentModifySetDto.getMileageRequestDto().getRequestType(), detailDto));

                //마일리지요청상세에 등록(오류발생시 NULL)
                CompletableFuture<Boolean> future = officeMapperService.addMileageRequestDetailInfo(detailDto);

                if(future.get()){
                    resultDto.register.success.totalAddByData((int)detailDto.getMileageAmt());
                }else{
                    //실패시(DB 입력실패)
                    resultDto.register.fail.totalAddByData((int)detailDto.getMileageAmt());
                }
            }
        }
        //즉시 적립인 경우
        if(mileagePaymentModifySetDto.getMileageRequestDto().getDirectlyYn().equals("Y")){
            if (this.modifyMileageRequestInfoByKey(
                mileagePaymentModifySetDto.getMileageReqNo(),
                mileagePaymentModifySetDto.getMileageRequestDto().getRegId()
            )){
                if(mileagePaymentModifySetDto.getMileageRequestDto().getRequestType().equals("01")){
                    resultDto.setPayment(this.actMileageSave(mileagePaymentModifySetDto.getMileageReqNo(), mileagePaymentModifySetDto.getMileageRequestDto().getExpireDay(),  mileagePaymentModifySetDto.getMileageRequestDto().getRegId()).get());
                }else{
                    resultDto.setPayment(this.actMileageReturn(mileagePaymentModifySetDto.getMileageReqNo(), mileagePaymentModifySetDto.getMileageRequestDto().getRegId()).get());
                }
            }
        }

        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_SUCCESS, resultDto, "마일리지 요청정보 수정", "%s");
    }

    /**
     * 마일리지 요청정보 수정.
     *
     * @param mileageReqNo 마일리지 지급/회수 요청번호
     * @param chgId 수정아이디
     * @return Boolean
     */
    private Boolean modifyMileageRequestInfoByKey(String mileageReqNo, String chgId){
        HashMap<String, Object> parameterMap = new HashMap<>();
        //mileage_req update Key
        parameterMap.put("mileageReqNo", mileageReqNo);
        //업데이트
        parameterMap.put("chgId", chgId);
        //상태
        parameterMap.put("requestStatus", MileageCommonCode.MILEAGE_REQUEST_STATUS_PROCESSING.getResponseCode());

        //마일리지 요청정보 업데이트
        return officeMapperService.modifyMileageRequestInfo(parameterMap);
    }

    /**
     * 마일리지 지급/회수 요청정보 등록
     *
     * @param mileageRequestDto 마일리지 지급/회수 등록 DTO
     * @return MileageReqDto
     */
    private MileageReqDto addMileageRequestInfo(MileageRequestDto mileageRequestDto) throws Exception {
        log.debug("mileageRequestDto {}", mileageRequestDto);
        try {
            //create MileageReqDto(마일리지 적립/회수 등록 DTO
            MileageReqDto mileageReqDto = MileageReqDto.builder()
                .mileageTypeNo(mileageRequestDto.getMileageTypeNo())
                .requestType(mileageRequestDto.getRequestType())
                .tradeNo(mileageRequestDto.getTradeNo())
                .requestStatus(MileageCommonCode.MILEAGE_REQUEST_STATUS_WAIT.getResponseCode()) //대기 상태로 저장.
                .requestReason(mileageRequestDto.getRequestReason())
                .displayMessage(mileageRequestDto.getDisplayMessage())
                .requestMessage(mileageRequestDto.getRequestMessage())
                .useYn(mileageRequestDto.getUseYn())
                .directlyYn(mileageRequestDto.getDirectlyYn())
                .processDt(mileageRequestDto.getProcessDt())
                .regId(mileageRequestDto.getRegId())
                .build();
            //데이터 insert
            if(!officeMapperService.addMileageRequestInfo(mileageReqDto)){
                throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_ERROR_0002, "마일리지 요청", "%s");
            }
            return mileageReqDto;
        } catch (Exception e){
            throw new MileageLogicException(ExceptionCode.SYS_ERROR_CODE_9004, e.getMessage());
        }
    }

    private String getConvertRequestType(MileageRequestDto requestDto){
        switch (requestDto.getRequestType()){
            case "01" :
            case "05" :
                return requestDto.getMileageTypeNo();
            case "02" :
                return officeMapperService.getMileageTypeByReturn();
            case "03" :
                return officeMapperService.getMileageReturnType(requestDto.getTradeNo(), getMileageCodeInfo("03"));
            case "04" :
                return officeMapperService.getMileageReturnType(requestDto.getTradeNo(), getMileageCodeInfo("04"));
            default:
                return null;
        }
    }

    /**
     * 마일리지 요청 수정 파라미터 생성.
     *
     * @param parameterMap 마일리지 지급/회수 수정데이터 MAP
     * @return HashMap<String, Object>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    private HashMap<String, Object> getModifyMileageReqMap(HashMap<String, Object> parameterMap) throws Exception {
        //마일리지요청 수정여부 확인
        MileageReqDto resultEntity = officeMapperService.getMileageReqModifyCheck(parameterMap);

        //수정내용이 있을 경우만 처리.
        if(resultEntity != null){
            //return HashMap 생성
            HashMap<String, Object> returnMap = new HashMap<>();
            //마일리지요청번호 응답값 맵핑
            resultEntity.setMileageReqNo((String)parameterMap.get("mileageReqNo"));
            //수정아이디 설정
            returnMap.put("chgId", parameterMap.get("regId"));

            //lambda null 이 아닌 데이터만 업데이트 처리.
            HashMap<String, Object> map = MileageCommonUtil.getConvertMap(resultEntity);

            map.forEach((key, value)->{
                if(value != null){
                    returnMap.put(key, value);
                }
            });

            return returnMap;
        }

        return null;
    }

    /**
     * 마일리지 상세 룰체크
     *
     * @param requestType 마일리지 지급/회수 요청유형
     * @param mileageReqDetailDto 마일리지 지급/회수 대상회원 DTO
     * @return String
     */
    private String getMileageDetailRuleCheckCode(String requestType, MileageReqDetailDto mileageReqDetailDto) {
        int maximumAmt = 10000;
        //1회 가능 금액 초과 적립 -> 04, 회수 -> 05
        if(requestType.equals("01")){
            maximumAmt = 100000;
        }
        // 최대 지급/회수 금액 검증.
        if (mileageReqDetailDto.getMileageAmt() > (100 * (maximumAmt))) {
            return requestType.equals("01") ? "04" : "05";
        }
        //회원중복은 insert 처리.
        return null;
    }

    /**
     * 상태코드 리턴.
     * @param requestType 마일리지 지급/회수 요청유형
     * @return String
     */
    private String convertPaymentRequestType(String requestType){
        return Arrays.stream(MileageCommonCode.values()).filter(e -> e.name()
            .contains("MILEAGE_REQUEST_DETAIL")).filter(mileageCommonCode -> mileageCommonCode.getResponseMessage().equals(requestType)).collect(Collectors.toMap(MileageCommonCode::getResponseMessage, MileageCommonCode::getResponseCode)).get(requestType);
    }

    /**
     * 마일리지 즉시 지급(마일리지 적립)
     * @param mileageReqNo 마일리지 지급/회수 요청번호
     * @param regId 등록아이디
     * @return CompletableFuture<MileagePaymentSaveDto>
     */
    @Async
    public CompletableFuture<MileagePaymentDto> actMileageSave(String mileageReqNo, int expireDay, String regId) {
        MileagePaymentDto mileagePaymentDto = new MileagePaymentDto();
        //상세요청 업데이트를 위한 HashMap
        HashMap<String, Object> parameterMap = new HashMap<>();
        //조회해온 수만큼
        for(SaveMileageDto saveDto : officeMapperService.getSaveMileageInfoList(Long.parseLong(mileageReqNo), expireDay)){
            //등록아이디
            saveDto.setRegId(regId);
            //수정아이디
            saveDto.setChgId(regId);
            //mileage_req_detail update Key
            parameterMap.put("mileageReqDetailNo", saveDto.getMileageReqDetailNo());
            //업데이트
            parameterMap.put("chgId", regId);

            log.info("마일리지 적립 요청 정보 : {}", saveDto);
            String returnCode;
            try {
                returnCode = saveMileageService.setSaveMileage(saveDto);
                //마일리지 적립 수행.
                if(returnCode.equals("00")){
                    mileagePaymentDto.success.totalAddByData((int) saveDto.getMileageAmt());
                    parameterMap.put("mileageDetailStatus", MileageCommonCode.MILEAGE_REQUEST_DETAIL_STATUS_COMPLETE.getResponseCode());
                }else{
                    mileagePaymentDto.fail.totalAddByData((int) saveDto.getMileageAmt());
                    parameterMap.put("mileageDetailStatus", MileageCommonCode.MILEAGE_REQUEST_DETAIL_STATUS_FAIL.getResponseCode());
                }
                log.info("마일리지 적립 요청 결과정보 : 마일리지적립번호 : {}", returnCode);
            } catch (Exception e) {
                log.error("마일리지 적립 요청 오류 : {}", e.getMessage());
                mileagePaymentDto.fail.totalAddByData((int) saveDto.getMileageAmt());
                returnCode = "99";
            }
            parameterMap.put("detailCode", returnCode);
            //마일리지 상세 정보 업데이트
            officeMapperService.modifyMileageRequestDetailInfo(parameterMap);
            //MAP 초기화
            parameterMap.clear();
        }
        // 완전 초기화
        parameterMap = new HashMap<>();
        //mileage_req update Key
        parameterMap.put("mileageReqNo", mileageReqNo);
        //업데이트
        parameterMap.put("chgId", regId);
        //상태
        parameterMap.put("requestStatus", MileageCommonCode.MILEAGE_REQUEST_STATUS_COMPLETE.getResponseCode());

        if(mileagePaymentDto.fail.getFailCnt() != 0){
            parameterMap.put("requestStatus", MileageCommonCode.MILEAGE_REQUEST_STATUS_FAIL.getResponseCode());
        }
        //마일리지 요청정보 업데이트
        officeMapperService.modifyMileageRequestInfo(parameterMap);

        return CompletableFuture.completedFuture(mileagePaymentDto);
    }

    /**
     * 마일리지 즉시 지급(마일리지 적립)
     * @param mileageReqNo 마일리지 지급/회수 요청번호
     * @param regId 등록아이디
     * @return CompletableFuture<MileagePaymentSaveDto>
     */
    @Async
    public CompletableFuture<MileagePaymentDto> actMileageReturn(String mileageReqNo, String regId) {
        MileagePaymentDto mileagePaymentDto = new MileagePaymentDto();
        //상세요청 업데이트를 위한 HashMap
        HashMap<String, Object> parameterMap = new HashMap<>();
        // 마일리지 회수 처리
        for(ReturnMileageSetDto returnDto : officeMapperService.getMileageReturnInfo(Long.parseLong(mileageReqNo))){
            log.info("회수처리 데이터 정보 :: {}", returnDto);
            //mileage_req_detail update Key
            parameterMap.put("mileageReqDetailNo", returnDto.getMileageReqDetailNo());
            //업데이트
            parameterMap.put("chgId", regId);
            // 결과 코
            String returnCode;

            try{
                // 회수처리
                returnCode = useMileageService.setMileageReturn(returnDto);
                // 회수처리 결과 처리.
                if(returnCode.equals("00")){
                    mileagePaymentDto.success.totalAddByData((int)returnDto.getRequestReturnAmt());
                    parameterMap.put("mileageDetailStatus", MileageCommonCode.MILEAGE_REQUEST_DETAIL_STATUS_COMPLETE.getResponseCode());
                }else{
                    mileagePaymentDto.fail.totalAddByData((int) returnDto.getRequestReturnAmt());
                    parameterMap.put("mileageDetailStatus", MileageCommonCode.MILEAGE_REQUEST_DETAIL_STATUS_FAIL.getResponseCode());
                }
            } catch (MileageLogicException mle){
                log.error("마일리지 회수 오류 발생 :: {}", mle.getResponseMsg());
                mileagePaymentDto.fail.totalAddByData((int) returnDto.getRequestReturnAmt());
                returnCode = "99";
            } catch ( Exception e){
                log.error("마일리지 회수 오류 발생 :: {}", e.getMessage());
                mileagePaymentDto.fail.totalAddByData((int) returnDto.getRequestReturnAmt());
                returnCode = "99";
            }
            //상세코드 업데이트
            parameterMap.put("detailCode", returnCode);
            //마일리지 상세 정보 업데이트
            officeMapperService.modifyMileageRequestDetailInfo(parameterMap);
            //MAP 초기화
            parameterMap.clear();
        }

        //mileage_req update Key
        parameterMap.put("mileageReqNo", mileageReqNo);
        //업데이트
        parameterMap.put("chgId", regId);
        //상태
        parameterMap.put("requestStatus", MileageCommonCode.MILEAGE_REQUEST_STATUS_COMPLETE.getResponseCode());

        if(mileagePaymentDto.fail.getFailCnt() != 0){
            parameterMap.put("requestStatus", MileageCommonCode.MILEAGE_REQUEST_STATUS_FAIL.getResponseCode());
        }
        //마일리지 요청정보 업데이트
        officeMapperService.modifyMileageRequestInfo(parameterMap);

        return CompletableFuture.completedFuture(mileagePaymentDto);
    }

    private String getMileageCodeInfo(String type) {
        return Arrays.stream(MileageSaveCode.values()).filter(code -> code.getMileageCodeType().equalsIgnoreCase(type)).map(MileageSaveCode::name).collect(Collectors.joining(","));
    }
}
