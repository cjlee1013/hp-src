package kr.co.homeplus.mileage.entity;

/**
 * 마일리지 지급/회수 요청(회원정보)
 */
public class MileageReqDetailEntity {

    //마일리지요청상세번호 [0]
    public String mileageReqDetailNo;

    //마일리지요청번호 [1]
    public String mileageReqNo;

    //고객번호 [2]
    public String userNo;

    //마일리지금액 [3]
    public String mileageAmt;

    //마일리지상세상태 [4]
    public String mileageDetailStatus;

    //마일리지상세코드(오류코드) [5]
    public String detailCode;

    //마일리지상세메시지(오류) [6]
    public String detailMessage;

    //사용유무 [7]
    public String useYn;

    //등록일 [8]
    public String regDt;

    //등록아이디 [9]
    public String regId;

    //수정일 [10]
    public String chgDt;

    //수정아이디 [11]
    public String chgId;
}
