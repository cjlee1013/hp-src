package kr.co.homeplus.mileage.enums;

import kr.co.homeplus.mileage.enums.impl.EnumImpl;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MileageResponseCode implements EnumImpl {

    MILEAGE_RESPONSE_SUCCESS("0000", "%s(을)/를 정상적으로 처리되었습니다."),
    //데이터 체크 오류시 사용.
    MILEAGE_RESPONSE_ERROR_0001("8001", "%s(을)/를 찾을 수 없습니다."),
    MILEAGE_RESPONSE_ERROR_0002("8002", "%s(을)/를 처리중에 오류가 발생했습니다."),
    MILEAGE_RESPONSE_ERROR_0003("8003", "%s(을)/를 실패하였습니다."),
    MILEAGE_RESPONSE_COMMON_ERROR_01("9001", "수정할 데이터가 없습니다."),

    MILEAGE_RESPONSE_EXTERNAL_ERR01("8201", "마일리지 적립타입이 없습니다."),

    MILEAGE_RESPONSE_EXTERNAL_SAVE_ERR01("8401", "기적립 거래번호입니다."),

    MILEAGE_RESPONSE_TYPE_CODE_DUPLICATION("8501", "이미 등록 된 마일리지 코드입니다.\n중복되지 않은 마일리지 코드를 입력하세요."),

    MILEAGE_RESPONSE_COUPON_SUCCESS("0000", "마일리지 쿠폰 발급요청에 성공하였습니다."),
    MILEAGE_RESPONSE_COUPON_MODIFY_SUCCESS("0000", "마일리지 쿠폰정보 수정에 성공하였습니다."),
    MILEAGE_RESPONSE_COUPON_ERR01("8101", "존재하지 않는 타입입니다."),
    MILEAGE_RESPONSE_COUPON_ERR02("8102", "쿠폰번호가 없어 다회성쿠폰발급에 실패했습니다."),
    MILEAGE_RESPONSE_COUPON_ERR03("8103", "마일리지 쿠폰 요청에 실패하였습니다."),
    MILEAGE_RESPONSE_COUPON_ERR04("8104", "쿠폰 지급으로 사용할 수 없는 타입입니다."),
    MILEAGE_RESPONSE_COUPON_ERR05("8105", "등록가능한 쿠폰번호길이를 초과하였습니다."),
    MILEAGE_RESPONSE_COUPON_ERR06("8106", "인당 등록가능횟수가 쿠폰발행횟수를 초과할 수 없습니다."),
    MILEAGE_RESPONSE_COUPON_ERR11("8111", "쿠폰정보 수정에 실패하였습니다."),

    MILEAGE_RESPONSE_COUPON_REG_SUCCESS("0000", "마일리지 쿠폰등록에 성공하였습니다."),
    MILEAGE_RESPONSE_COUPON_REG_ERR01("8111", "등록할 수 없는 쿠폰번호입니다."),
    MILEAGE_RESPONSE_COUPON_REG_ERR02("8112", "등록할 수 있는 회수를 초과하였습니다."),
    MILEAGE_RESPONSE_COUPON_REG_ERR03("8113", "이미 등록되어 있는 쿠폰번호입니다."),
    MILEAGE_RESPONSE_COUPON_REG_ERR04("8114", "쿠폰발행 요청작업 중 오류가 발생하였습니다."),
    MILEAGE_RESPONSE_COUPON_REG_ERR06("8116", "모든 쿠폰이 소진되어 등록할 수 없습니다."),
    MILEAGE_RESPONSE_COUPON_REG_ERR07("8117", "해당 쿠폰은 이미 사용완료된 쿠폰입니다."),
    // 8118 : 마일리지 적립 요청 실패.
    MILEAGE_RESPONSE_COUPON_REG_ERR08("8118", "요청하신 쿠폰 등록중 오류가 발생하였습니다."),
    MILEAGE_RESPONSE_COUPON_REG_ERR09("8119", "요청하신 쿠폰 등록중 오류가 발생하였습니다."),
    MILEAGE_RESPONSE_COUPON_RETURN_SUCCESS("0000", "마일리지 쿠폰회수에 성공하였습니다."),
    MILEAGE_RESPONSE_COUPON_RETURN_ERR01("8301", "쿠폰 회수중 오류가 발생했습니다."),

    MILEAGE_RESPONSE_COUPON_SEARCH_SUCCESS("0000", "조회에 성공하였습니다."),
    MILEAGE_RESPONSE_COUPON_SEARCH_FAIL("0001", "조회건이 없습니다."),
    ;

    private final String responseCode;
    private final String responseMessage;
}
