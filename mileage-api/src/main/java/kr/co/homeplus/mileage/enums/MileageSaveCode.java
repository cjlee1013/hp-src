package kr.co.homeplus.mileage.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum  MileageSaveCode {
    H1001("03", "ORDER"),
    H1002("03", "ORDER"),
    H1003("03", "ORDER"),
    C2001("03", "ORDER"),
    C2002("03", "ORDER"),
    C2003("03", "ORDER"),
    E1001("03", "ORDER"),
    E1002("03", "ORDER"),
    E1003("03", "ORDER"),
    H7001("04", "PAYBACK"),
    C7001("04", "PAYBACK"),
    ;

    @Getter
    private final String mileageCodeType;

    @Getter
    private final String mileageCodeTypeStr;
}
