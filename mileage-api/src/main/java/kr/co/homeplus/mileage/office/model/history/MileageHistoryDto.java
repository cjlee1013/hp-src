package kr.co.homeplus.mileage.office.model.history;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마일리지 관리 > 마일리지 내역 조회 응답")
public class MileageHistoryDto {
    @ApiModelProperty(value = "발생일시", position = 1)
    private String actDt;
    @ApiModelProperty(value = "마일리지타입번호", position = 2)
    private String mileageTypeNo;
    @ApiModelProperty(value = "마일리지유형", position = 3)
    private String mileageTypeName;
    @ApiModelProperty(value = "마일리지종류", position = 4)
    private String mileageKind;
    @ApiModelProperty(value = "구분", position = 5)
    private String mileageCategory;
    @ApiModelProperty(value = "내역", position = 6)
    private String historyMessage;
    @ApiModelProperty(value = "회원번호", position = 7)
    private String userNo;
    @ApiModelProperty(value = "회원명", position = 8)
    private String userNm;
    @ApiModelProperty(value = "금액", position = 9)
    private long mileageAmt;
    @ApiModelProperty(value = "처리자", position = 10)
    private String chgId;
    @ApiModelProperty(value = "유효기간", position = 11)
    private String expireDt;
    @ApiModelProperty(value = "거래번호", position = 12)
    private String tradeNo;
    @ApiModelProperty(value = "사유", position = 13)
    private String requestReason;
}