package kr.co.homeplus.mileage.entity;

public class MileageCouponInfoEntity {
    // 쿠폰정보SEQ
    public String couponInfoSeq;
    // 쿠폰번호
    public String couponNo;
    // 마일리지쿠폰관리번호
    public String couponManageNo;
    // 고객번호
    public String userNo;
    // 유저ID
    public String userId;
    // 고객명
    public String userNm;
    // 발급일
    public String issueDt;
    // 쿠폰사용일
    public String useDt;
    // 마일리지적립번호
    public String mileageSaveNo;
    // 회수자ID
    public String recoveryId;
    // 회수일
    public String recoveryDt;
    // 등록일
    public String regDt;
    // 수정일
    public String chgDt;
    // 회수여부
    public String recoveryYn;
}
