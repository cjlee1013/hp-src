package kr.co.homeplus.mileage.office.model.coupon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@ApiModel(description = "마일리지 쿠폰 유저 등록 DTO")
public class MileageCouponUserRegDto {
    @NotNull(message = "마일리지쿠폰번호")
    @ApiModelProperty(value = "마일리지쿠폰번호")
    private String couponNo;
    @NotNull(message = "고객번호")
    @ApiModelProperty(value = "고객번호")
    private long userNo;
    @NotNull(message = "회원ID")
    @ApiModelProperty(value = "회원ID")
    private String userId;
    @NotNull(message = "회원명")
    @ApiModelProperty(value = "회원명")
    private String userNm;
    @NotNull(message = "등록자ID")
    @ApiModelProperty(value = "등록자ID(마이페이지:MYPAGE,ADMIN:empNo")
    private String requestId;

    private void setCouponNo(String couponNo) {
        this.couponNo = couponNo.toUpperCase();
    }
}
