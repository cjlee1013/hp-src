package kr.co.homeplus.mileage.office.service;

import static com.google.common.collect.Maps.newHashMap;

import java.util.HashMap;
import java.util.List;
import kr.co.homeplus.mileage.core.exception.MileageLogicException;
import kr.co.homeplus.mileage.enums.ActivityMileageCode;
import kr.co.homeplus.mileage.enums.MileageResponseCode;
import kr.co.homeplus.mileage.office.model.type.MileagePromoTypeGetDto;
import kr.co.homeplus.mileage.office.model.type.MileagePromoTypeSetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeManageModifyDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeManageSetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeSearchGetDto;
import kr.co.homeplus.mileage.office.model.type.MileageTypeSearchSetDto;
import kr.co.homeplus.mileage.utils.MileageCommonResponseFactory;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service("MileageTypeService")
@Slf4j
@RequiredArgsConstructor
public class MileageTypeService {
    // Office Mapper Management Service
    private final OfficeMapperManageService officeMapperService;

    /**
     * 마일리지 타입 등록 요청
     *
     * @param mileageTypeManageSetDto 마일리지 유형 등록 DTO
     * @return ResponseObject<String> 성공/실패 여부 응답.
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public ResponseObject<String> setMileageTypeManagementInfo(MileageTypeManageSetDto mileageTypeManageSetDto) throws Exception{
        //상황별 파라미터 체크
        if(mileageTypeManageSetDto.getExpireType().equals("TR")
            && ( mileageTypeManageSetDto.getExpireDayInp() == null || Integer.parseInt(mileageTypeManageSetDto.getExpireDayInp()) == 0)){
            return MileageCommonResponseFactory.getResponseObject(ActivityMileageCode.MILEAGE_TYPE_ERR_3531,"유효기간일", "%s");
        }

        if(mileageTypeManageSetDto.getExpireType().equals("TT")
            && ( mileageTypeManageSetDto.getUseStartDt() == null || mileageTypeManageSetDto.getUseEndDt() == null)){
            return MileageCommonResponseFactory.getResponseObject(ActivityMileageCode.MILEAGE_TYPE_ERR_3531,"사용기간", "%s");
        }

        // 마일리지코드 있는지 확인
        if(officeMapperService.isMileageCodeDuplication(mileageTypeManageSetDto.getMileageCode())){
            throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_TYPE_CODE_DUPLICATION);
        }

        if(!officeMapperService.setMileageTypeInfo(mileageTypeManageSetDto)){
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_ERROR_0002,"마일리지 타입 등록", "%s");
        }

        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_SUCCESS, String.valueOf(mileageTypeManageSetDto.getMileageTypeNo()), "마일리지 타입 등록", "%s");
    }

    /**
     * 마일리지 타입 수정 요청
     * @param mileageTypeManageModifyDto
     * @return
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public ResponseObject<String> modifyMileageTypeManagementInfo(MileageTypeManageModifyDto mileageTypeManageModifyDto) throws Exception{

        //상황별 파라미터 체크
        if(mileageTypeManageModifyDto.getExpireType().equals("TR")
            && ( mileageTypeManageModifyDto.getExpireDayInp() == null || Integer.parseInt(mileageTypeManageModifyDto.getExpireDayInp()) == 0)){
            return MileageCommonResponseFactory.getResponseObject(ActivityMileageCode.MILEAGE_TYPE_ERR_3531,"유효기간일", "%s");
        }

        if(mileageTypeManageModifyDto.getExpireType().equals("TT")
            && ( mileageTypeManageModifyDto.getUseStartDt() == null || mileageTypeManageModifyDto.getUseEndDt() == null)){
            return MileageCommonResponseFactory.getResponseObject(ActivityMileageCode.MILEAGE_TYPE_ERR_3531,"사용기간", "%s");
        }

        //수정할 내역이 있는 지 확인
        HashMap<String, Object> mileageTypeModifyCheck = officeMapperService.getMileageTypeModifyCheck(mileageTypeManageModifyDto);

        log.info("mileageTypeModifyCheck ::{}", mileageTypeModifyCheck);
        //없으면 에러.
        if(mileageTypeModifyCheck == null){
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_COMMON_ERROR_01);
        }

        if(mileageTypeModifyCheck.get("mileageCode") != null){
            if(officeMapperService.isMileageCodeDuplication(mileageTypeManageModifyDto.getMileageCode())){
                throw new MileageLogicException(MileageResponseCode.MILEAGE_RESPONSE_TYPE_CODE_DUPLICATION);
            }
        }


        //업데이트을 위한 Key 설정
        mileageTypeModifyCheck.put("mileageTypeNo", mileageTypeManageModifyDto.getMileageTypeNo());

        //수정아이디.
        mileageTypeModifyCheck.put("chgId", mileageTypeManageModifyDto.getRegId());

        //수정 업데이트
        if(!officeMapperService.modifyMileageTypeInfo(this.makeMileageTypeModifyInfo(mileageTypeModifyCheck))){
            return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_ERROR_0002,"마일리지 타입 수정", "%s");
        }

        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_SUCCESS,"마일리지 타입 수정", "%s");
    }

    /**
     * 마일리지 타입 조회
     *
     * @param mileageTypeSearchSetDto 마일리지 유형 정보 조회 DTO
     * @return List<MileageTypeSearchGetDto> 마일리지 유형 정보 조회 응답 DTO
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public ResponseObject<List<MileageTypeSearchGetDto>> getMileageTypeManagementInfo(MileageTypeSearchSetDto mileageTypeSearchSetDto) throws Exception{
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_SUCCESS, officeMapperService.getMileageTypeInfoList(mileageTypeSearchSetDto), "마일리지 타입조회", "%s");
    }

    /**
     * 마일리지 타입 기본정보 조회
     *
     * @param mileageTypeNo 마일리지유형관리번호
     * @return MileageTypeManageModifyDto 마일리지 수정 DTO
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public ResponseObject<MileageTypeManageModifyDto> getMileageTypeManagementBasicInfo(String mileageTypeNo) throws Exception{
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_SUCCESS, officeMapperService.getMileageTypeModifyInfo(mileageTypeNo), "마일리지 타입상세조회", "%s");
    }

    public ResponseObject<List<MileagePromoTypeGetDto>> getMileagePromotionTypeInfo(MileagePromoTypeSetDto setDto) throws Exception{
        return MileageCommonResponseFactory.getResponseObject(MileageResponseCode.MILEAGE_RESPONSE_SUCCESS, officeMapperService.getMileagePromotionTypeInfo(setDto), "마일리지 타입상세조회", "%s");
    }

    /**
     * 마일리지 수정 - 수정데이터 만들기.
     * @param parameterMap 변경내역 정보 Map
     * @return HashMap<String, Object> 수정정보 Map
     */
    private HashMap<String, Object> makeMileageTypeModifyInfo(HashMap<String, Object> parameterMap) {
        HashMap<String, Object> mileageTypeModifyInfoMap = new HashMap<>();

        parameterMap.forEach((key, value)->{
            if(value != null || key.equals("mileageTypeNo")){
                mileageTypeModifyInfoMap.put(key, value);
            }
        });

        return mileageTypeModifyInfoMap;
    }


}
