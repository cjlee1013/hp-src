package kr.co.homeplus.mileage.entity;

public class MileageTypeEntity {
    // 마일리지유형번호
    public String mileageTypeNo;
    // 마일리지유형
    public String mileageTypeName;
    // 마일리지구분
    public String mileageCategory;
    // 마일리지종류
    public String mileageKind;
    // 점포유형
    public String storeType;
    // 등록시작일
    public String regStartDt;
    // 등록종료일
    public String regEndDt;
    // 유효기간유형
    public String expireType;
    // 유효기간설정일
    public String expireDayInp;
    // 사용시작일
    public String useStartDt;
    // 사용종료일
    public String useEndDt;
    // 마일리지 타입설명
    public String mileageTypeExplain;
    // 사용유무
    public String useYn;
    // 등록일
    public String regDt;
    // 등록아이디
    public String regId;
    // 수정일
    public String chgDt;
    // 수정아이디
    public String chgId;
    // 마일리지코드
    public String mileageCode;
    // 고객노출문구
    public String displayMessage;
}
