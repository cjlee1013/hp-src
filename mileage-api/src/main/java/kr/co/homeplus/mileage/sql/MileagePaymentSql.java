package kr.co.homeplus.mileage.sql;

import java.util.Arrays;
import java.util.HashMap;
import kr.co.homeplus.mileage.constants.Constants;
import kr.co.homeplus.mileage.constants.CustomConstants;
import kr.co.homeplus.mileage.constants.SqlPatternConstants;
import kr.co.homeplus.mileage.entity.EmployeeInfoEntity;
import kr.co.homeplus.mileage.entity.MileageCouponInfoEntity;
import kr.co.homeplus.mileage.entity.MileageSaveEntity;
import kr.co.homeplus.mileage.enums.MileageCommonCode;
import kr.co.homeplus.mileage.enums.MysqlFunction;
import kr.co.homeplus.mileage.entity.MileageReqDetailEntity;
import kr.co.homeplus.mileage.entity.MileageReqEntity;
import kr.co.homeplus.mileage.entity.MileageTypeEntity;
import kr.co.homeplus.mileage.office.model.payment.MileagePaymentSearchSetDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDetailDto;
import kr.co.homeplus.mileage.office.model.payment.MileageReqDto;
import kr.co.homeplus.mileage.utils.EntityFactory;
import kr.co.homeplus.mileage.utils.MileageCommonUtil;
import kr.co.homeplus.mileage.utils.ObjectUtils;
import kr.co.homeplus.mileage.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MileagePaymentSql {

    // 마일리지 지급/회수 기본정보 테이블.(Alias 포함)
    public static final String paymentBasicTable = EntityFactory.getTableNameWithAlias(MileageReqEntity.class);
    // 마일리지 지급/회수 대상회원 테이블.(Alias 포함)
    public static final String paymentUserTable = EntityFactory.getTableNameWithAlias(MileageReqDetailEntity.class);
    // 마일리지 유형 테이블.(Alias 포함)
    public static final String typeTable = EntityFactory.getTableNameWithAlias(MileageTypeEntity.class);

    // 마일리지 지급/회수 기본정보 컬럼정보.(Alias 포함)
    public static final MileageTypeEntity mt = EntityFactory.createEntityIntoValue(MileageTypeEntity.class, Boolean.TRUE);
    // 마일리지 지급/회수 대상회원 컬럼정보.(Alias 포함)
    public static final MileageReqEntity mr = EntityFactory.createEntityIntoValue(MileageReqEntity.class, Boolean.TRUE);
    // 마일리지 유형 컬럼정보.(Alias 포함)
    public static final MileageReqDetailEntity mrd = EntityFactory.createEntityIntoValue(MileageReqDetailEntity.class, Boolean.TRUE);

    /**
     * 마일리지 적립 정보 생성.
     *
     * @param mileageReqNo 마일리지 지급/회수 관리번호
     * @return String 마일리지 적립 정보 생성 Query.
     */
    public static String selectMakeMileageSaveInfo(@Param("mileageReqNo") long mileageReqNo, @Param("expireDay") int expireDay){
        // 각 테이블 컬럼에 Alias 추가
        SQL query = new SQL()
            // SELECT 할 컬럼 설정.
            .SELECT(
                mrd.mileageReqDetailNo,
                mr.mileageTypeNo,
                mrd.userNo,
                mt.mileageCategory,
                mt.mileageKind,
                mrd.mileageAmt,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        mt.useStartDt + " IS NULL",
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray("NOW()", "%Y-%m-%d 00:00:00")),
                        mt.useStartDt
                    ),
                    "use_start_dt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(mt.expireType, "TT"),
                        mt.useEndDt,
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(
                                SqlUtils.equalColumnByInput("0", expireDay),
                                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray("NOW()", mt.expireDayInp))),
                                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray("NOW()", expireDay)))
                            )
                        )
                    ),
                    "expire_dt"
                ),
                SqlUtils.aliasToRow(mrd.mileageAmt, "mileage_remain_amt"),
                mr.tradeNo,
                mr.requestReason
            )
            // SELECT 대상 Table 설정.
            .FROM(paymentUserTable)
            // INNER JOIN 설정.
            .INNER_JOIN(
                // INNER JOIN 을 생성할 수 있는 Method.
                // createJoinCondition ( String, List<Object>, Object[])
                // createJoinCondition ( INNER JOIN 대상 테이블, 동일한 컬럼으로 맵핑되는 컬럼정보, 특정값을 입력하여 맵핑하는 컬럼정보)
                SqlUtils.createJoinCondition(
                    paymentBasicTable,
                    Arrays.asList(
                        // 동일컬럼으로 맵핑하는 쿼리 생성.
                        // onClause(new Object[], Object)
                        // onClause(컬럼정보, 맵핑되는 컬럼의 instance)
                        SqlUtils.onClause(
                            new Object[]{
                                mrd.mileageReqNo
                            },
                            mr
                        )
                    ),
                    new Object[]{
                        // 마일리지 지급/회수 요청정보의 사용유무가 'Y' 인 경우만 포함되도록 함.
                        // 입력받은 데이터로 맵핑하는 쿼리 생성. ( mr.use_yn = 'Y' )
                        SqlUtils.singleQuote(mr.useYn, Constants.USE_Y),
                        // 마일리지 지급/회수 요청정보의 상태가 '대기' 인 경우만 포함되도록 함.
                        // 입력받은 데이터를 SingleQuote 를 추가하여 맵핑하는 쿼리 생성. ( mr.request_status = 'M1' )
                        SqlUtils.singleQuote(mr.requestStatus, MileageCommonCode.MILEAGE_REQUEST_STATUS_PROCESSING.getResponseCode()),
                    }
                )
            )
            .INNER_JOIN(
                // INNER JOIN 을 생성할 수 있는 Method.
                // createJoinCondition ( String, List<Object>, Object[])
                // createJoinCondition ( INNER JOIN 대상 테이블, 동일한 컬럼으로 맵핑되는 컬럼정보, 특정값을 입력하여 맵핑하는 컬럼정보)
                SqlUtils.createJoinCondition(
                    typeTable,
                    Arrays.asList(
                        // 동일컬럼으로 맵핑하는 쿼리 생성.
                        // onClause(new Object[], Object)
                        // onClause(컬럼정보, 맵핑되는 컬럼의 instance)
                        SqlUtils.onClause(new Object[]{
                                mt.mileageTypeNo
                            },
                            mr
                        )
                    ),
                    new Object[]{
                        // 입력받은 데이터를 SingleQuote 를 추가하여 맵핑하는 쿼리 생성. ( mt.use_yn = 'M1' )
                        // 마일리지 유형의 사용유무가 Y 인 경우만 포함되도록 설정.
                        SqlUtils.singleQuote(mt.useYn, Constants.USE_Y)
                    }
                )
            )
            .WHERE(
                // SELECT 대상 조회 쿼리
                // 마일리지 지급/회수 요청정보의 관리번호가 입력받은 값이 경우.
                // 입력받은 데이터를 컬럼에 맵핑하여 쿼리 생성.( mr.mileage_req_no = mileageReqNo )
                SqlUtils.equalColumnByInput(mr.mileageReqNo, mileageReqNo),
                // 마일리지 지급/회수 대상회원의 사용유무가 'Y' 인 경우.
                // 입려받은 데이터에 SingleQuote 를 추가하여 쿼리 생성. ( mrd.use_yn = 'Y')
                SqlUtils.singleQuote(mrd.useYn, Constants.USE_Y),
                // 마일리지 지급/회수 대상회원의 상세코드가 '00' 인 경우.
                // 입려받은 데이터에 SingleQuote 를 추가하여 쿼리 생성. ( mrd.detailCode = '00')
                SqlUtils.singleQuote(mrd.detailCode, "00"),
                // 마일리지 지급/회수 요청정의 요청유형이  '01' 인 경우.
                // 입려받은 데이터에 SingleQuote 를 추가하여 쿼리 생성.( mr.request_type = '01')
                SqlUtils.singleQuote(mr.requestType, MileageCommonCode.MILEAGE_REQUEST_TYPE_SAVE.getResponseCode()),
                // 마일리지 지급/회수 대상회원의 상태가 '대기' 인 경우.
                // 입려받은 데이터에 SingleQuote 를 추가하여 쿼리 생성. ( mrd.mileage_detail_status = 'D1')
                SqlUtils.singleQuote(mrd.mileageDetailStatus, MileageCommonCode.MILEAGE_REQUEST_DETAIL_STATUS_WAIT.getResponseCode())
            );

        log.debug("selectMakeMileageSaveInfo \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 회수 정보 생성
     *
     * @param mileageReqNo 마일리지 지급/회수 관리번호
     * @return String 마일리지 회수 정보 생성 Query
     */
    public static String selectMakeMileageReturnInfo(@Param("mileageReqNo") long mileageReqNo){
        SQL query = new SQL()
            .SELECT(new String[]{
                mrd.userNo,
                SqlUtils.aliasToRow(mr.requestType, "request_return_type"),
                "CASE WHEN mr.trade_no IS NULL THEN mr.mileage_req_no ELSE mr.trade_no END AS trade_no",
                SqlUtils.aliasToRow(mrd.mileageAmt, "requestReturnAmt"),
                SqlUtils.aliasToRow(mr.displayMessage, "request_Return_Message"),
                mrd.regId,
                mrd.mileageReqDetailNo
            })
            .FROM(paymentUserTable)
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    mr,
                    ObjectUtils.toArray(mrd.mileageReqNo),
                    ObjectUtils.toArray(
                        SqlUtils.singleQuote(mr.useYn, Constants.USE_Y),
                        SqlUtils.equalColumnByInput(mr.requestStatus, MileageCommonCode.MILEAGE_REQUEST_STATUS_PROCESSING.getResponseCode())
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(mr.mileageReqNo, mileageReqNo),
                SqlUtils.singleQuote(mrd.useYn, Constants.USE_Y),
                SqlUtils.singleQuote(mrd.detailCode, "00"),
                SqlUtils.sqlFunction(MysqlFunction.IN, mr.requestType, ObjectUtils.toArray("'02'", "'03'", "'04'", "'05'")),
                SqlUtils.singleQuote(mrd.mileageDetailStatus, MileageCommonCode.MILEAGE_REQUEST_DETAIL_STATUS_WAIT.getResponseCode())
            );

        log.debug("selectMakeMileageReturnInfo \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 지급/회수 요청 등록
     *
     * @param mileageReqDto 마일리지 지급/회수 요청 등록 DTO
     * @return String 마일리지 지급/회수 요청 등록 Query
     */
    public static String insertMileageRequestInfo(MileageReqDto mileageReqDto){

        // chg_id 가 없을 떄 reg_id로 chg_id 설정.
        if(mileageReqDto.getChgId() == null || mileageReqDto.getChgId().isEmpty()){
            mileageReqDto.setChgId(mileageReqDto.getRegId());
        }

        MileageReqEntity mrNonAlias = EntityFactory.createEntityIntoValue(MileageReqEntity.class);

        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(mrNonAlias))
            .INTO_COLUMNS(
                mrNonAlias.mileageReqNo,
                mrNonAlias.mileageTypeNo,
                mrNonAlias.requestType,
                mrNonAlias.requestStatus,
                mrNonAlias.requestReason,
                mrNonAlias.displayMessage,
                mrNonAlias.requestMessage,
                mrNonAlias.useYn,
                mrNonAlias.directlyYn,
                mrNonAlias.processDt,
                mrNonAlias.regDt,
                mrNonAlias.regId,
                mrNonAlias.chgDt,
                mrNonAlias.chgId
            )
            .INTO_VALUES(
                SqlUtils.convertInsertParamToCamelCase(
                    mrNonAlias.mileageReqNo,
                    mrNonAlias.mileageTypeNo,
                    mrNonAlias.requestType,
                    mrNonAlias.requestStatus,
                    mrNonAlias.requestReason,
                    mrNonAlias.displayMessage,
                    mrNonAlias.requestMessage,
                    mrNonAlias.useYn,
                    mrNonAlias.directlyYn,
                    mrNonAlias.processDt,
                    mrNonAlias.regDt,
                    mrNonAlias.regId,
                    mrNonAlias.chgDt,
                    mrNonAlias.chgId
                )
            );

        // 즉시일경우 processDt를 오늘날짜로 셋팅.
        if(mileageReqDto.getDirectlyYn().equals("Y")){
            query.INTO_COLUMNS(mrNonAlias.completeDt)
                .INTO_VALUES("NOW()");
        }

        if(!StringUtils.isEmpty(mileageReqDto.getTradeNo())){
            query.INTO_COLUMNS(mrNonAlias.tradeNo)
                .INTO_VALUES(SqlUtils.convertInsertParamToCamelCase(mrNonAlias.tradeNo));
        }

        log.info("insertMileageRequestInfo {}", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 지급/회수 요청 수정 가능여부조회
     *
     * @param parameterMap 수정 데이터 Map
     * @return String 마일리지 지급/회수 요청 수정 가능여부조회 Query
     */
    public static String selectMileageReqModifyCheck(HashMap<String, Object> parameterMap) {
        SQL query = new SQL()
            .SELECT(
                "CASE WHEN #{mileageTypeNo} != mr.mileage_type_no THEN #{mileageTypeNo} ELSE NULL END                   AS mileageTypeNo",
                    " CASE WHEN #{requestType} != mr.request_type THEN #{requestType} ELSE NULL END                          AS requestType",
                    " CASE WHEN #{requestReason} != mr.request_reason THEN #{requestReason} ELSE NULL END                    AS requestReason",
                    " CASE WHEN LENGTH(#{displayMessage}) != LENGTH(mr.display_message) THEN #{displayMessage} ELSE NULL END AS displayMessage",
                    " CASE WHEN LENGTH(#{requestMessage}) != LENGTH(mr.request_message) THEN #{requestMessage} ELSE NULL END AS requestMessage",
                    " CASE WHEN #{useYn} != mr.use_yn THEN #{useYn} ELSE NULL END                                            AS useYn",
                    " CASE WHEN #{directlyYn} != mr.directly_yn THEN #{directlyYn} ELSE NULL END                             AS directlyYn",
                    " CASE WHEN #{processDt} != mr.process_dt THEN #{processDt} ELSE NULL END                                AS processDt"
            ).FROM(paymentBasicTable)
            .WHERE(
                SqlUtils.equalColumnMapping(mr.mileageReqNo)
            );

        log.debug("selectMileageReqModifyCheck \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 지급/회수 조회
     *
     * @param mileagePaymentSearchSetDto 마일리지 지급/회수 조회 파라미터 DTO
     * @return String 마일리지 지급/회수 조회 Query
     * @throws Exception 오류시 오류처리.
     */
    public static String selectMileagePaymentInfoList(MileagePaymentSearchSetDto mileagePaymentSearchSetDto) throws Exception {
        EmployeeInfoEntity employeeInfo = EntityFactory.createEntityIntoValue(EmployeeInfoEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                mr.mileageReqNo,
                mr.requestType,
                SqlUtils.sqlFunction(MysqlFunction.MILEAGE_COMMON_TABLE, mr.requestType, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, mr.requestType, ObjectUtils.toArray("'03'", "'04'", "'05'")), "'02'", mr.requestType))), "requestTypeNm"),
                mr.requestReason,
                mt.mileageTypeName,
                SqlUtils.aliasToRow(
                    "(SELECT COUNT(1) FROM mileage_req_detail mrd WHERE mrd.mileage_req_no = mr.mileage_req_no AND mrd.use_yn = 'Y')",
                    "requestCnt"
                ),
                SqlUtils.aliasToRow(
                    "IFNULL((SELECT SUM(mrd.mileage_amt) FROM mileage_req_detail mrd WHERE mrd.mileage_req_no = mr.mileage_req_no AND mrd.use_yn = 'Y'),0)",
                    "requestAmt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.MILEAGE_COMMON_TABLE, mr.requestStatus, ObjectUtils.toArray(mr.requestStatus), "requestStatus"),
                SqlPatternConstants.getConditionByPatternWithAlias("DATE_INPUT", "processDt", mr.processDt, "%Y-%m-%d"),
                SqlPatternConstants.getConditionByPatternWithAlias("DATE_INPUT", "completeDt", mr.completeDt, "%Y-%m-%d"),
                SqlUtils.aliasToRow(
                    "(SELECT COUNT(1) FROM mileage_req_detail mrd WHERE mrd.mileage_req_no = mr.mileage_req_no AND mrd.mileage_detail_status = 'D3')",
                    "completeCnt"
                ),
                mr.regDt,
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(employeeInfo.name).FROM(EntityFactory.getTableNameWithAlias("dms", employeeInfo)).WHERE(SqlUtils.equalColumn(employeeInfo.empid, mr.regId))
                        ),
                        mr.regId
                    ),
                    mr.regId
                ),
                mr.chgDt,
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(employeeInfo.name).FROM(EntityFactory.getTableNameWithAlias("dms", employeeInfo)).WHERE(SqlUtils.equalColumn(employeeInfo.empid, mr.chgId))
                        ),
                        mr.chgId
                    ),
                    mr.chgId
                )
            )
            .FROM(paymentBasicTable)
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(typeTable, Arrays.asList(SqlUtils.onClause(new Object[]{mt.mileageTypeNo}, mr)), null ));

        query.WHERE(
                SqlUtils.betweenDay(
                    SqlUtils.sqlFunction(
                        MysqlFunction.DATE_FORMAT,
                        ObjectUtils.toArray(
                            (mileagePaymentSearchSetDto.getSchType().equals("01") ? mr.processDt : mr.completeDt),
                            "%Y-%m-%d"
                        )
                    ),
                    mileagePaymentSearchSetDto.getSchStartDt(),
                    mileagePaymentSearchSetDto.getSchEndDt()
                )
            );

        if(!mileagePaymentSearchSetDto.getRequestType().equals("A")){
            query.WHERE(SqlUtils.equalColumnMapping(mr.requestType));
        }

        if(!mileagePaymentSearchSetDto.getMileageKind().equals("A")){
            if(!"02".equals(mileagePaymentSearchSetDto.getRequestType())){
                query.WHERE(SqlUtils.equalColumnMapping(mt.mileageKind));
            }
        }

        if(!mileagePaymentSearchSetDto.getUseYn().equals("A")){
            query.WHERE(SqlUtils.equalColumnMapping(mt.useYn));
        }

        if (mileagePaymentSearchSetDto.getSchDetailDesc() != null &&
            MileageCommonUtil.notEmpty(mileagePaymentSearchSetDto.getSchDetailDesc())) {
            if(mileagePaymentSearchSetDto.getSchDetailCode().equals("MILEAGE_REQ_NO")){
                query.WHERE(SqlUtils.nonSingleQuote(mr.mileageReqNo, mileagePaymentSearchSetDto.getSchDetailDesc()));
            } else if(mileagePaymentSearchSetDto.getSchDetailCode().equals("REQUEST_REASON")){
                query.WHERE(SqlPatternConstants.getConditionByPattern("LIKE", mr.requestReason, mileagePaymentSearchSetDto.getSchDetailDesc()));
            } else if(mileagePaymentSearchSetDto.getSchDetailCode().equals("CODE")){
                query.WHERE(SqlUtils.equalColumnByInput(mt.mileageCode, mileagePaymentSearchSetDto.getSchDetailDesc()));
            }
        }

        log.debug("selectMileagePaymentInfoList \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 지급/회수 기본정보 조회
     *
     * @param mileageReqNo 마일리지 지급/회수 요청관리번호
     * @return String 마일리지 지급/회수 기본정보 조회 Query
     */
    public static String selectMileageRequestInfoList(@Param("mileageReqNo") String mileageReqNo) {
        SQL query = new SQL()
            .SELECT(
                mr.mileageReqNo,
                mr.mileageTypeNo,
                mt.mileageTypeName,
                mr.requestType,
                mr.tradeNo,
                mr.requestCnt,
                mr.requestAmt,
                SqlUtils.sqlFunction(MysqlFunction.MILEAGE_COMMON_TABLE, mr.requestStatus, ObjectUtils.toArray(mr.requestStatus), "request_status"),
                mr.requestReason,
                mr.requestMessage,
                mr.displayMessage,
                mr.useYn,
                mr.directlyYn,
                mr.processDt,
                mr.completeDt,
                mr.regDt,
                mr.regId,
                mr.chgDt,
                mr.chgId
            )
            .FROM(paymentBasicTable)
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    typeTable,
                    Arrays.asList(
                        SqlUtils.onClause(
                            new Object[]{
                                mt.mileageTypeNo
                            },
                            mr
                        )
                    ),
                    null
                )
                )
            .WHERE(
                SqlUtils.equalColumnByInput(mr.mileageReqNo, Long.parseLong(mileageReqNo))
            );

        log.debug("selectMileageRequestInfoList \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 지급/회수 요청 상세조회
     *
     * @param mileageReqNo 마일리지 지급/회수 요청관리번호
     * @return String 마일리지 지급/회수 요청 상세조회 Query.
     */
    public static String selectMileageRequestDetailInfoList(@Param("mileageReqNo") String mileageReqNo) {
        SQL query = new SQL()
            .SELECT(
                mrd.mileageReqDetailNo,
                mrd.mileageReqNo,
                mrd.userNo,
                mrd.mileageAmt,
                SqlUtils.sqlFunction(MysqlFunction.MILEAGE_COMMON_TABLE, mrd.mileageDetailStatus, ObjectUtils.toArray(mrd.mileageDetailStatus), "mileage_detail_status"),
                mrd.detailMessage,
                mrd.regDt,
                mrd.regId,
                mrd.chgDt,
                mrd.chgId
            )
            .FROM(paymentUserTable)
            .WHERE(
                SqlUtils.equalColumnMapping(mrd.mileageReqNo),
                SqlUtils.equalColumnByInput(mrd.useYn, "Y")
            );

        log.debug("selectMileageRequestDetailInfoList \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 지급/회수 기본정보 수정
     *
     * @param parameterMap 마일리지 지급/회수 기본정보 수정데이터 Map
     * @return 마일리지 지급/회수 기본정보 수정 Query
     */
    public static String updateMileageReqStatus(HashMap<String, Object> parameterMap) {
        SQL query = new SQL()
            .UPDATE(paymentBasicTable);

        parameterMap.forEach((key, value) -> {
            if (value != null && !key.equals("mileageReqNo")) {
                query.SET(SqlUtils.equalColumnMapping(SqlUtils.convertCamelCaseToSnakeCase(key)));
                if(key.equals("chgId")){
                    query.SET(SqlUtils.nonSingleQuote(mr.chgDt, "NOW()"));
                }
            }
        });

        query.WHERE(
            SqlUtils.equalColumnMapping(mr.mileageReqNo)
        );

        log.debug("updateMileageReqStatus \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 지급/회수 대상회원 수정
     *
     * @param parameterMap 마일리지 지급/회수 대상회원 수정데이터 Map
     * @return 마일리지 지급/회수 대상회원 수정 Query.
     */
    public static String updateMileageReqUserStatus(HashMap<String, Object> parameterMap) {
        SQL query = new SQL()
            .UPDATE(paymentUserTable);

        parameterMap.forEach((key, value) -> {
            if (value != null && !key.equals("mileageReqDetailNo")) {
                query.SET(SqlUtils.equalColumnMapping(SqlUtils.convertCamelCaseToSnakeCase(key)));
                if(key.equals("chgId")){
                    query.SET(SqlUtils.nonSingleQuote(mrd.chgDt, "NOW()"));
                }
                if(key.equals("detailCode") && !value.equals("00")){
                    query.SET(
                        SqlUtils.nonSingleQuote(
                            mrd.detailMessage,
                            SqlUtils.sqlFunction(MysqlFunction.MILEAGE_COMMON_TABLE, mrd.detailCode, ObjectUtils.toArray(SqlUtils.appendSingleQuote(value)))
                        )
                    );
                }
            }
        });

        query.WHERE(
            SqlUtils.equalColumnMapping(mrd.mileageReqDetailNo)
        );

        log.debug("updateMileageReqUserStatus \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 지급/회수 대상회원 등록
     *
     * @param mileageReqDetailDto 마일리지 지급/회수 대상회원 등록 DTO
     * @return String 마일리지 지급/회수 대상회원 등록 Query.
     */
    public static String insertMileageRequestDetailInfo(MileageReqDetailDto mileageReqDetailDto) {
        MileageReqDetailEntity mileageReqDetail = EntityFactory.createEntityIntoValue(MileageReqDetailEntity.class);
        if(!mileageReqDetailDto.getDetailCode().equals("00")){
            mileageReqDetailDto.setMileageDetailStatus(MileageCommonCode.MILEAGE_REQUEST_DETAIL_STATUS_FAIL.getResponseCode());
        }
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(MileageReqDetailEntity.class))
            .INTO_COLUMNS(
                mileageReqDetail.mileageReqNo,
                mileageReqDetail.userNo,
                mileageReqDetail.mileageAmt,
                mileageReqDetail.mileageDetailStatus,
                mileageReqDetail.detailCode,
                mileageReqDetail.useYn,
                mileageReqDetail.regId,
                mileageReqDetail.chgId,
                mileageReqDetail.regDt,
                mileageReqDetail.chgDt,
                mileageReqDetail.detailMessage
            )
            .INTO_VALUES(
                SqlUtils.convertInsertIntoValue(
                    mileageReqDetailDto.getMileageReqNo(),
                    mileageReqDetailDto.getUserNo(),
                    mileageReqDetailDto.getMileageAmt(),
                    mileageReqDetailDto.getMileageDetailStatus(),
                    mileageReqDetailDto.getDetailCode(),
                    mileageReqDetailDto.getUseYn(),
                    mileageReqDetailDto.getRegId(),
                    mileageReqDetailDto.getRegId(),
                    SqlPatternConstants.NOW,
                    SqlPatternConstants.NOW,
                    SqlUtils.sqlFunction(MysqlFunction.MILEAGE_COMMON_TABLE, mileageReqDetail.detailCode, ObjectUtils.toArray(mileageReqDetailDto.getDetailCode()))
                )
            );
        log.debug("insertMileageRequestDetailInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileageSaveReturnInfo(@Param("tradeNo") String tradeNo, @Param("mileageCode") String mileageCode) {
        MileageReqEntity mileageReq = EntityFactory.createEntityIntoValue(MileageReqEntity.class, Boolean.TRUE);
        MileageReqDetailEntity mileageReqDetail = EntityFactory.createEntityIntoValue(MileageReqDetailEntity.class, Boolean.TRUE);
        MileageSaveEntity mileageSave = EntityFactory.createEntityIntoValue(MileageSaveEntity.class, Boolean.TRUE);
        MileageTypeEntity mileageType = EntityFactory.createEntityIntoValue(MileageTypeEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                mileageSave.mileageSaveNo,
                mileageSave.mileageTypeNo,
                mileageSave.userNo,
                mileageSave.mileageCategory,
                mileageSave.mileageKind,
                mileageSave.mileageAmt,
                mileageSave.mileageRemainAmt,
                mileageSave.expireDt
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageReq))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageReqDetail, ObjectUtils.toArray(mileageReq.mileageReqNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageSave, ObjectUtils.toArray(mileageReqDetail.mileageReqDetailNo, mrd.userNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageType, ObjectUtils.toArray(mileageSave.mileageTypeNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageReq.tradeNo, tradeNo),
                SqlUtils.sqlFunction(MysqlFunction.IN, mileageType.mileageCode,
                    Arrays.stream(mileageCode.split(",")).map(str -> "'" + str + "'").toArray())
            );
        log.debug("selectMileageSaveReturnInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileageCouponReturnInfo(@Param("tradeNo") String tradeNo) {
        MileageCouponInfoEntity mileageCouponInfo = EntityFactory.createEntityIntoValue(MileageCouponInfoEntity.class, Boolean.TRUE);
        MileageSaveEntity mileageSave = EntityFactory.createEntityIntoValue(MileageSaveEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                mileageSave.mileageSaveNo,
                mileageSave.mileageTypeNo,
                mileageSave.userNo,
                mileageSave.mileageCategory,
                mileageSave.mileageKind,
                mileageSave.mileageAmt,
                mileageSave.mileageRemainAmt,
                mileageSave.expireDt,
                mileageCouponInfo.couponInfoSeq
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageCouponInfo))
            .INNER_JOIN(SqlUtils.createJoinCondition(mileageSave, ObjectUtils.toArray(mileageCouponInfo.mileageSaveNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(mileageCouponInfo.couponManageNo, tradeNo),
                SqlUtils.equalColumnByInput(mileageCouponInfo.recoveryYn, "C")
            );
        log.debug("selectMileageCouponReturnInfo \n[{}]", query.toString());
        return query.toString();
    }

}