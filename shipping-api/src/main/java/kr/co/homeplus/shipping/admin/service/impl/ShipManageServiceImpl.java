package kr.co.homeplus.shipping.admin.service.impl;

import static kr.co.homeplus.shipping.constants.ResourceRouteName.PARTNER;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.shipping.admin.mapper.ShipManageMasterMapper;
import kr.co.homeplus.shipping.admin.mapper.ShipManageSlaveMapper;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipInfoPrintGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageConfirmOrderSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageListSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageNotiDelaySetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllExcelSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllPopListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllPopListSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipCompleteByBundleSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipCompletePopListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipCompleteSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipStoreInfoGetDto;
import kr.co.homeplus.shipping.admin.service.ShipManageService;
import kr.co.homeplus.shipping.common.model.history.BundleProcessInsertSetDto;
import kr.co.homeplus.shipping.common.service.HistoryService;
import kr.co.homeplus.shipping.common.service.MessageSendService;
import kr.co.homeplus.shipping.core.exception.ShippingException;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.enums.ShipMethod;
import kr.co.homeplus.shipping.partner.service.PartnerCommonService;
import kr.co.homeplus.shipping.utils.DateUtil;
import kr.co.homeplus.shipping.utils.ResponseUtil;
import kr.co.homeplus.shipping.utils.ShipInfoUtil;
import kr.co.homeplus.shipping.utils.StringUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipManageServiceImpl implements ShipManageService {
    private final ShipManageSlaveMapper shipManageSlaveMapper;
    private final ShipManageMasterMapper shipManageMasterMapper;
    private final HistoryService historyService;
    private final MessageSendService messageSendService;
    private final PartnerCommonService partnerCommonService;

    /**
     * 배송관리 조회
     */
    @Override
    public List<ShipManageListGetDto> getShipManageList(ShipManageListSetDto shipManageListSetDto) {
      log.info("getShipManageList param : {}", shipManageListSetDto);

      List<ShipManageListGetDto> shipManageListGetDtoList = shipManageSlaveMapper.selectShipManageList(shipManageListSetDto);

      Iterator<ShipManageListGetDto> iterator = shipManageListGetDtoList.iterator();

      while (iterator.hasNext()) {
        ShipManageListGetDto dto = iterator.next();

        // 취소 데이터 노출 제외
        if(Integer.parseInt(dto.getItemQty()) < 1) {
          iterator.remove();
        } else {
          dto.setBuyerMobileNo(PrivacyMaskingUtils.maskingPhone(dto.getBuyerMobileNo()));
          dto.setShipMobileNo(PrivacyMaskingUtils.maskingPhone(dto.getShipMobileNo()));
          dto.setBuyerNm(PrivacyMaskingUtils.maskingUserName(dto.getBuyerNm()));
          dto.setReceiverNm(PrivacyMaskingUtils.maskingUserName(dto.getReceiverNm()));
          dto.setAddr(PrivacyMaskingUtils.maskingAddress(dto.getAddr()));
        }
      }

      return shipManageListGetDtoList;
    }

    /**
     * 주문 확인
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, ShippingException.class})
    public ResponseObject<String> setConfirmOrder(ShipManageConfirmOrderSetDto shipManageConfirmOrderSetDto, String route) {
        log.info("setConfirmOrder param : {}", shipManageConfirmOrderSetDto);

        int result = 0;
        String chgId = shipManageConfirmOrderSetDto.getChgId();
        List<String> bundleNoList = shipManageConfirmOrderSetDto.getBundleNoList();

        // 주문확인 처리
        for (String bundleNo : bundleNoList) {
          String failMsg = "";
          Map<String, Object> shipInfo = new HashMap<>();
          List<Map<String,Object>> orderList = shipManageMasterMapper.selectShipInfoByBundleNo(bundleNo);

          if(orderList.size() == 0) {
            failMsg = "처리할 상품주문 목록이 없습니다";
          }

          for (Map<String,Object> orderMap : orderList) {
            String orderItemNo = orderMap.get("orderItemNo").toString();

            // 상품번호기준 배송정보 조회
            shipInfo = shipManageMasterMapper.selectShipInfoByOrderItemNo(orderItemNo);

            // 상품주문번호 확인
            if (ObjectUtils.isEmpty(shipInfo)) {
              failMsg = "상품 주문번호를 확인하세요[상품주문번호"+orderItemNo+"]";
              break;
            }

            // PO에서 접근 시, 파트너 물품인지 체크
            if (PARTNER.equals(route)) {
              if (!partnerCommonService.isPartnerItemByOrderItemNo(orderItemNo, chgId)) {
                failMsg = "본 계정의 물품이 아닙니다[상품주문번호"+orderItemNo+"]";
                break;
              }
            }

            // 신규주문건인지 확인
            if (!"D1".equals(shipInfo.get("shipStatus"))) {
              failMsg = "주문상태를 확인하세요[상품주문번호" + orderItemNo + "/" + shipInfo.get("shipStatus") + "]";
              break;
            }
          }

          // 검증 성공
          if(StringUtils.isEmpty(failMsg)) {
            // 업데이트
            int returnVal = shipManageMasterMapper.updateConfirmOrder(bundleNo, chgId);

            // 업데이트 성공
            if (returnVal > 0) {
              historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("주문확인", "", "", route, chgId, bundleNo));
              result++;
            } else {
              failMsg = "DB업데이트 처리 에러";
            }
          }

          // 검증 OR DB업데이트 실패
          if(!StringUtils.isEmpty(failMsg)) {
            historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("주문확인 실패", failMsg, "", route, chgId, bundleNo));
          }
        }

        return ResponseUtil.getResoponseObject(Integer.toString(result));
    }

    /**
     * 발송지연안내
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, ShippingException.class})
    public ResponseObject<String> setNotiDelay(ShipManageNotiDelaySetDto shipManageNotiDelaySetDto, String route) {
        log.info("setNotiDelay param : {}", shipManageNotiDelaySetDto);

        int result = 0;
        String delayShipDt = shipManageNotiDelaySetDto.getDelayShipDt();
        String delayShipCd = shipManageNotiDelaySetDto.getDelayShipCd();
        String delayShipMsg = shipManageNotiDelaySetDto.getDelayShipMsg();
        String chgId = shipManageNotiDelaySetDto.getChgId();
        String bundleNo = shipManageNotiDelaySetDto.getBundleNo();

        String failMsg = "";
        Map<String, Object> shipInfo = new HashMap<>();
        List<Map<String,Object>> orderList = shipManageMasterMapper.selectShipInfoByBundleNo(bundleNo);

        if(orderList.size() == 0) {
          failMsg = "처리할 상품주문 목록이 없습니다";
        }

        for (Map<String,Object> orderMap : orderList) {

          String orderItemNo = orderMap.get("orderItemNo").toString();

          // 상품번호기준 배송정보 조회
          shipInfo = shipManageMasterMapper.selectShipInfoByOrderItemNo(orderItemNo);

          // 상품주문번호 확인
          if (ObjectUtils.isEmpty(shipInfo)) {
            failMsg = "상품 주문번호를 확인하세요[상품주문번호" + orderItemNo + "]";
            break;
          }

          // PO에서 접근 시, 파트너 물품인지 체크
          if (PARTNER.equals(route)) {
            if (!partnerCommonService.isPartnerItemByOrderItemNo(orderItemNo, chgId)) {
              failMsg = "본 계정의 물품이 아닙니다[상품주문번호" + orderItemNo + "]";
              break;
            }
          }

          // 신규주문, 배송준비중 건인지 확인
          if (!"D1".equals(shipInfo.get("shipStatus")) && !"D2".equals(shipInfo.get("shipStatus"))) {
            failMsg = "주문상태를 확인하세요[상품주문번호" + orderItemNo + "/" + shipInfo.get("shipStatus") + "]";
            break;
          }

          // 기 발송지연안내 확인
          if (!ObjectUtils.isEmpty(shipInfo.get("delayShipCd")) || !ObjectUtils.isEmpty(shipInfo.get("delayShipDt"))) {
            failMsg = "이미 발송지연안내가 되었습니다.";
            break;
          }
        }

        // 검증 성공
        if(StringUtils.isEmpty(failMsg)) {
          // 업데이트
          int returnVal = shipManageMasterMapper.updateNotiDelay(bundleNo, chgId, delayShipCd, delayShipMsg, delayShipDt);

          // 업데이트 성공
          if (returnVal > 0) {
            historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("발송지연안내", "", "", route, chgId, bundleNo));
            result++;
          } else {
            failMsg = "DB업데이트 처리 에러";
          }
        }

        // 검증 OR DB업데이트 실패 , 종료
        if(!StringUtils.isEmpty(failMsg)) {
          historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("발송지연안내 실패", failMsg, "", route, chgId, bundleNo));

          return ResponseUtil.getResoponseObjectFail("발송지연안내를 실패했습니다.");
        }

        // 알림톡 발송
        // DS상품만 발송
        if (shipInfo.get("mallType").toString().equals("DS")) {
          messageSendService.sendMessageShipDelay(bundleNo);
        }

        return ResponseUtil.getResoponseObject(Integer.toString(result));
    }

    /**
     * 일괄발송처리
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, ShippingException.class})
    public ResponseObject<Map<String, Object>> setShipAllExcel(List<ShipManageShipAllExcelSetDto> shipManageShipAllExcelSetDtoList, String route) {
      log.info("setShipAllExcel param : {}", shipManageShipAllExcelSetDtoList);

      Map<String, Object> resultMap = new HashMap<>();
      List<String> messageBundleNoList = new ArrayList<>();
      String shipMethod = "";
      String chgId = "";
      int successCnt = 0;

      for (ShipManageShipAllExcelSetDto shipAllExcelSetDto : shipManageShipAllExcelSetDtoList) {
        Map<String, Object> shipInfo = new HashMap<>();
        String bundleNo = shipAllExcelSetDto.getBundleNo();
        List<String> failMsgList = new ArrayList<>();

        chgId = shipAllExcelSetDto.getChgId();
        shipMethod = shipAllExcelSetDto.getShipMethod();

        // 실패사유가 있는 건은 제외 (manage-web 에서 처리된 건)
        if (!ObjectUtils.isEmpty(shipAllExcelSetDto.getFailMsg())) {
          continue;
        }

        // 배송번호 기준 상품주문리스트 조회
        List<ShipManageShipAllPopListGetDto> orderItemList = shipManageMasterMapper.selectShipAllPopList(bundleNo);

        if (orderItemList.size() == 0) {
          failMsgList.add("처리할 상품주문리스트가 없습니다.[배송번호" + bundleNo + "]");
        }

        /* 상품주문별로 검증 시작 */
        for (ShipManageShipAllPopListGetDto dto : orderItemList) {
          String orderItemNo = dto.getOrderItemNo();

          // 상품번호기준 배송정보 조회
          shipInfo = shipManageMasterMapper.selectShipInfoByOrderItemNo(orderItemNo);

          // 상품주문번호 확인
          if (ObjectUtils.isEmpty(shipInfo)) {
            failMsgList.add("상품 주문번호를 확인하세요[상품주문번호" + orderItemNo + "]");
            continue;
          }

          // PO에서 접근 시, 파트너 물품인지 체크
          if (PARTNER.equals(route)) {
            if (!partnerCommonService.isPartnerItemByOrderItemNo(orderItemNo, chgId)) {
              failMsgList.add("본 계정의 물품이 아닙니다[상품주문번호" + orderItemNo + "]");
              continue;
            }
          }

          // 배송방법 설정
          shipAllExcelSetDto.setShipMethod(shipInfo.get("mallType").toString() + "_" + shipMethod);

          // 배송방법 확인
          if (!ShipMethod.TD_DLV.getCode().equals(shipAllExcelSetDto.getShipMethod())
              && !ShipMethod.DS_DLV.getCode().equals(shipAllExcelSetDto.getShipMethod())
              && !ShipMethod.DS_DRCT.getCode().equals(shipAllExcelSetDto.getShipMethod())) {
            failMsgList.add("배송방법을 확인하세요[상품주문번호" + orderItemNo + "/" + ShipMethod.getShipMethodNm(shipAllExcelSetDto.getShipMethod()) + "]");
            continue;
          }

          // 상품준비중, 배송중인 건인지 확인
          if (!"D2".equals(shipInfo.get("shipStatus")) && !"D3".equals(shipInfo.get("shipStatus"))
              && !"D4".equals(shipInfo.get("shipStatus"))) {
            failMsgList.add("주문상태를 확인하세요[상품주문번호" + orderItemNo + "/" + shipInfo.get("shipStatusNm") + "]");
            continue;
          }

          // 주문취소 확인
          if (Integer.parseInt(shipInfo.get("claimQty").toString()) < 1) {
            failMsgList.add("주문 취소 건입니다[상품주문번호" + orderItemNo + "/" + shipInfo.get("claimStatusNm") + "]");
            continue;
          }
        }
        /* 상품주문별로 검증 종료*/

        // 검증 성공
        if (failMsgList.size() == 0) {
          // 업데이트
          int returnVal = shipManageMasterMapper.updateShipAllExcel(shipAllExcelSetDto);

          // 업데이트 성공
          if (returnVal > 0) {

            // 상세 히스토리 설정
            String historyDeatil = ShipInfoUtil.isDlv(shipAllExcelSetDto.getShipMethod()) ?
                "배송방법:" + shipAllExcelSetDto.getShipMethod() + ",송장번호:" + shipAllExcelSetDto.getInvoiceNo()
                : "배송방법:" + shipAllExcelSetDto.getShipMethod() + ",배송예정일:" + shipAllExcelSetDto.getScheduleShipDt();

            String historyDetailBefore =
                ShipInfoUtil.isDlv(shipInfo.get("shipMethod").toString()) ?
                    "배송상태:" + shipInfo.get("shipStatus") + ",배송방법:" + shipInfo.get("shipMethod")
                        + ",송장번호:" + shipInfo.get("invoiceNo")
                    : "배송상태:" + shipInfo.get("shipStatus") + ",배송방법:" + shipInfo.get("shipMethod")
                        + ",배송예정일:" + shipInfo.get("scheduleShipDt");

            historyService.setShippingProcessHistoryBundle(
                new BundleProcessInsertSetDto("일괄발송처리", historyDeatil, historyDetailBefore, route, chgId, bundleNo));

            // 알림톡 전송 리스트에 추가
            if (shipInfo.get("shipStatus").equals("D2")) {
              messageBundleNoList.add(bundleNo);
            }
            successCnt++;
          } else {
            failMsgList.add("DB업데이트 에러");
          }
        }

        // 검증 OR DB업데이트 실패
        if (failMsgList.size() != 0) {
          historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("일괄발송처리 실패", "", "", route, chgId, bundleNo));
          shipAllExcelSetDto.setFailMsg(String.join(",", failMsgList));
        }
      }

      resultMap.put("successCnt", Integer.toString(successCnt));
      resultMap.put("resultList", shipManageShipAllExcelSetDtoList);

      // 알림톡 발송
      // 택배배송
      if (ShipInfoUtil.isDlv(shipMethod)) {
        messageSendService.sendMessageShipDlv(messageBundleNoList);
        // 직접배송(DS)
      } else if (ShipMethod.DS_DRCT.getCode().equals(shipMethod)) {
        messageSendService.sendMessageShipDsDrct(messageBundleNoList);
      }

      return ResponseUtil.getResoponseObject(resultMap);
    }

    /**
     * 선택발송처리
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, ShippingException.class})
    public ResponseObject<String> setShipAll(ShipManageShipAllSetDto shipManageShipAllSetDto, String route) {
      log.info("setShipAll param : {}", shipManageShipAllSetDto);

      String failMsg = "";
      int result = 0;

      // 파라미터 검증
      failMsg = this.validShipAll(shipManageShipAllSetDto);
      if (!StringUtils.isEmpty(failMsg)) {
        return ResponseUtil.getResoponseObject(ExceptionCode.ERROR_CODE_1001, failMsg);
      }

      Map<String, Object> shipInfo = new HashMap<>();
      String chgId = shipManageShipAllSetDto.getChgId();
      String bundleNo = shipManageShipAllSetDto.getBundleNo();
      String shipMethod = shipManageShipAllSetDto.getShipMethod();
      String invoiceNo = shipManageShipAllSetDto.getInvoiceNo();
      String scheduleShipDt = shipManageShipAllSetDto.getScheduleShipDt();

      // 배송번호 기준 상품주문리스트 조회
      List<ShipManageShipAllPopListGetDto> orderItemList = shipManageMasterMapper.selectShipAllPopList(bundleNo);

      if (orderItemList.size() == 0) {
        failMsg = ExceptionCode.ERROR_CODE_3019.getResponseMessage() + "\n- 배송번호 : " + bundleNo;
      }

      /* 상품주문별로 검증 시작 */
      for (ShipManageShipAllPopListGetDto dto : orderItemList) {
        String orderItemNo = dto.getOrderItemNo();

        // 배송정보 조회
        shipInfo = shipManageMasterMapper.selectShipInfoByOrderItemNo(orderItemNo);

        // 상품주문번호 확인
        if (ObjectUtils.isEmpty(shipInfo)) {
          failMsg = ExceptionCode.ERROR_CODE_3008.getResponseMessage() + "\n- 상품주문번호 : " + orderItemNo;
          break;
        }

        // PO에서 접근 시, 파트너 물품인지 체크
        if (PARTNER.equals(route)) {
          if (!partnerCommonService.isPartnerItemByOrderItemNo(orderItemNo, chgId)) {
            failMsg = ExceptionCode.ERROR_CODE_3017.getResponseMessage() + "\n- 상품주문번호 : " + orderItemNo;
            break;
          }
        }

        // mall_type이 다른 배송건 변경 불가
        if (!ShipInfoUtil.getMallTypeByShipMethod(shipMethod).equals(shipInfo.get("mallType"))) {
          failMsg = ExceptionCode.ERROR_CODE_3018.getResponseMessage() + "\n- 상품주문번호 : " + orderItemNo;
          break;
        }

        // 상품준비중, 배송중, 배송완료인 건인지 확인
        if (!"D2".equals(shipInfo.get("shipStatus")) && !"D3".equals(shipInfo.get("shipStatus"))
            && !"D4".equals(shipInfo.get("shipStatus"))) {
          failMsg = ExceptionCode.ERROR_CODE_3009.getResponseMessage() + "\n- 상품주문번호 : " + orderItemNo + "\n- 주문상태 : " + shipInfo.get("shipStatusNm");
          break;
        }

        // 주문취소 확인
        if (Integer.parseInt(shipInfo.get("claimQty").toString()) < 1) {
          failMsg = ExceptionCode.ERROR_CODE_3010.getResponseMessage() + "\n- 상품주문번호 : " + orderItemNo + "\n- 취소상태 : " + shipInfo.get("claimStatusNm");
          break;
        }

        // 변경 불가 배송방법 확인
        if (ShipMethod.TD_DRCT.getCode().equals(shipInfo.get("shipMethod")) || ShipMethod.TD_PICK.getCode().equals(shipInfo.get("shipMethod"))) {
          failMsg = ExceptionCode.ERROR_CODE_3011.getResponseMessage() + "\n- 상품주문번호 : " + orderItemNo + "\n- 배송방법 : " + shipInfo.get("shipMethodNm");
          break;
        }
      }
      /* 상품주문별로 검증 종료 */

      // 검증 성공
      if (StringUtils.isEmpty(failMsg)) {
        // 업데이트
        int returnVal = shipManageMasterMapper.updateShipAll(shipManageShipAllSetDto);

        // 업데이트 성공
        if (returnVal > 0) {
          // 상세 히스토리 설정
          String historyDeatil = ShipInfoUtil.isDlv(shipMethod) ?
              "배송방법:" + shipMethod + ",송장번호:" + invoiceNo
              : "배송방법:" + shipMethod + ",배송예정일:" + scheduleShipDt;

          String historyDetailBefore = ShipInfoUtil.isDlv(shipInfo.get("shipMethod").toString()) ?
              "배송상태:" + shipInfo.get("shipStatus") + ",배송방법:" + shipInfo.get("shipMethod") + ",송장번호:" + shipInfo.get("invoiceNo")
              : "배송상태:" + shipInfo.get("shipStatus") + ",배송방법:" + shipInfo.get("shipMethod") + ",배송예정일:" + shipInfo.get("scheduleShipDt");

          historyService.setShippingProcessHistoryBundle(
              new BundleProcessInsertSetDto("선택발송처리"
                  , historyDeatil
                  , historyDetailBefore
                  , route
                  , chgId
                  , bundleNo));
          result = returnVal;
        } else {
          failMsg = "DB업데이트 처리 에러";
        }
      }

      // 검증 OR DB업데이트 실패 , 종료
      if (!StringUtils.isEmpty(failMsg)) {
        historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("선택발송처리 실패", failMsg, "", route, chgId, bundleNo));
        return ResponseUtil.getResoponseObjectFail(failMsg);
      }

      // 알림톡 발송
      if (shipInfo.get("shipStatus").equals("D2")) {
        // 택배배송
        if (ShipInfoUtil.isDlv(shipMethod)) {
          messageSendService.sendMessageShipDlv(bundleNo);
        // 직접배송(DS)
        } else if (ShipMethod.DS_DRCT.getCode().equals(shipMethod)) {
          messageSendService.sendMessageShipDsDrct(bundleNo);
        // 우편배송(DS)
        } else if(ShipMethod.DS_POST.getCode().equals(shipMethod)) {
          messageSendService.sendMessageShipDsPost(bundleNo);
        }
      }

      return ResponseUtil.getResoponseObject(Integer.toString(result));
    }

    /**
     * 선택발송처리(주문취소건가능)
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, ShippingException.class})
    public ResponseObject<String> setShipAllExClaim(ShipManageShipAllSetDto shipManageShipAllSetDto, String route) {
      log.info("setShipAllExClaim param : {}", shipManageShipAllSetDto);

      String failMsg = "";
      int result = 0;

      // 파라미터 검증
      failMsg = this.validShipAll(shipManageShipAllSetDto);
      if (!StringUtils.isEmpty(failMsg)) {
        return ResponseUtil.getResoponseObject(ExceptionCode.ERROR_CODE_1001, failMsg);
      }

      Map<String, Object> shipInfo = new HashMap<>();
      String chgId = shipManageShipAllSetDto.getChgId();
      String bundleNo = shipManageShipAllSetDto.getBundleNo();
      String shipMethod = shipManageShipAllSetDto.getShipMethod();
      String invoiceNo = shipManageShipAllSetDto.getInvoiceNo();
      String scheduleShipDt = shipManageShipAllSetDto.getScheduleShipDt();

      // 배송번호 기준 상품주문리스트 조회
      List<ShipManageShipAllPopListGetDto> orderItemList = shipManageMasterMapper.selectShipAllPopListExClaim(bundleNo);

      if (orderItemList.size() == 0) {
        failMsg = ExceptionCode.ERROR_CODE_3019.getResponseMessage() + "\n- 배송번호 : " + bundleNo;
      }

      /* 상품주문별로 검증 시작 */
      for (ShipManageShipAllPopListGetDto dto : orderItemList) {
        String orderItemNo = dto.getOrderItemNo();

        // 배송정보 조회
        shipInfo = shipManageMasterMapper.selectShipInfoByOrderItemNo(orderItemNo);

        // 상품주문번호 확인
        if (ObjectUtils.isEmpty(shipInfo)) {
          failMsg = ExceptionCode.ERROR_CODE_3008.getResponseMessage() + "\n- 상품주문번호 : " + orderItemNo;
          break;
        }

        // PO에서 접근 시, 파트너 물품인지 체크
        if (PARTNER.equals(route)) {
          if (!partnerCommonService.isPartnerItemByOrderItemNo(orderItemNo, chgId)) {
            failMsg = ExceptionCode.ERROR_CODE_3017.getResponseMessage() + "\n- 상품주문번호 : " + orderItemNo;
            break;
          }
        }

        // mall_type이 다른 배송건 변경 불가
        if (!ShipInfoUtil.getMallTypeByShipMethod(shipMethod).equals(shipInfo.get("mallType"))) {
          failMsg = ExceptionCode.ERROR_CODE_3018.getResponseMessage() + "\n- 상품주문번호 : " + orderItemNo;
          break;
        }

        // 상품준비중, 배송중, 배송완료인 건인지 확인
        if (!"D2".equals(shipInfo.get("shipStatus")) && !"D3".equals(shipInfo.get("shipStatus"))
            && !"D4".equals(shipInfo.get("shipStatus"))) {
          failMsg = ExceptionCode.ERROR_CODE_3009.getResponseMessage() + "\n- 상품주문번호 : " + orderItemNo + "\n- 주문상태 : " + shipInfo.get("shipStatusNm");
          break;
        }

        // 변경 불가 배송방법 확인
        if (ShipMethod.TD_DRCT.getCode().equals(shipInfo.get("shipMethod")) || ShipMethod.TD_PICK.getCode().equals(shipInfo.get("shipMethod"))) {
          failMsg = ExceptionCode.ERROR_CODE_3011.getResponseMessage() + "\n- 상품주문번호 : " + orderItemNo + "\n- 배송방법 : " + shipInfo.get("shipMethodNm");
          break;
        }
      }
      /* 상품주문별로 검증 종료 */

      // 검증 성공
      if (StringUtils.isEmpty(failMsg)) {
        // 업데이트
        int returnVal = shipManageMasterMapper.updateShipAll(shipManageShipAllSetDto);

        // 업데이트 성공
        if (returnVal > 0) {
          // 상세 히스토리 설정
          String historyDeatil = ShipInfoUtil.isDlv(shipMethod) ?
              "배송방법:" + shipMethod + ",송장번호:" + invoiceNo
              : "배송방법:" + shipMethod + ",배송예정일:" + scheduleShipDt;

          String historyDetailBefore = ShipInfoUtil.isDlv(shipInfo.get("shipMethod").toString()) ?
              "배송상태:" + shipInfo.get("shipStatus") + ",배송방법:" + shipInfo.get("shipMethod") + ",송장번호:" + shipInfo.get("invoiceNo")
              : "배송상태:" + shipInfo.get("shipStatus") + ",배송방법:" + shipInfo.get("shipMethod") + ",배송예정일:" + shipInfo.get("scheduleShipDt");

          historyService.setShippingProcessHistoryBundle(
              new BundleProcessInsertSetDto("선택발송처리"
                  , historyDeatil
                  , historyDetailBefore
                  , route
                  , chgId
                  , bundleNo));
          result = returnVal;
        } else {
          failMsg = "DB업데이트 처리 에러";
        }
      }

      // 검증 OR DB업데이트 실패 , 종료
      if (!StringUtils.isEmpty(failMsg)) {
        historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("선택발송처리 실패", failMsg, "", route, chgId, bundleNo));
        return ResponseUtil.getResoponseObjectFail(failMsg);
      }

      // 알림톡 발송
      if (shipInfo.get("shipStatus").equals("D2")) {
        // 택배배송
        if (ShipInfoUtil.isDlv(shipMethod)) {
          messageSendService.sendMessageShipDlv(bundleNo);
          // 직접배송(DS)
        } else if (ShipMethod.DS_DRCT.getCode().equals(shipMethod)) {
          messageSendService.sendMessageShipDsDrct(bundleNo);
          // 우편배송(DS)
        } else if(ShipMethod.DS_POST.getCode().equals(shipMethod)) {
          messageSendService.sendMessageShipDsPost(bundleNo);
        }
      }

      return ResponseUtil.getResoponseObject(Integer.toString(result));
    }

    /**
     * 선택발송처리 유효성 검사
     */
    private String validShipAll(ShipManageShipAllSetDto shipManageShipAllSetDto) {
        String shipMethod = shipManageShipAllSetDto.getShipMethod();

        /* 배송방법코드 확인 */
        if(!ShipMethod.isShipMethodNm(shipMethod)){
          return "존재하지 않는 배송방법코드입니다 : " + shipMethod;
        }

        /* 택배배송 */
        if (ShipInfoUtil.isDlv(shipMethod)) {
            String invoiceNo = shipManageShipAllSetDto.getInvoiceNo();
            String dlvCd = shipManageShipAllSetDto.getDlvCd();

            // 송장번호 - NULL, 공백, 30자이상, 영문+숫자조합
            if (StringUtils.isEmpty(invoiceNo)
                || StringUtil.spaceCheck(invoiceNo)
                || invoiceNo.length() > 30
                || !invoiceNo.matches("^[a-zA-Z0-9]*$")) {
                return "송장번호를 확인해주세요 : " + invoiceNo;
            }

            // 택배사 - NULL
            if (StringUtils.isEmpty(dlvCd)) {
                return "택배사가 입력되지 않았습니다";
            }
        }

        /* 직접배송 */
        if (ShipInfoUtil.isDrct(shipMethod)) {
            String scheduleShipDt = shipManageShipAllSetDto.getScheduleShipDt();

            // 배송예정일 - NULL, 공백, yyyy-mm-dd 형식
            if (StringUtils.isEmpty(scheduleShipDt)
                || StringUtil.spaceCheck(scheduleShipDt)
                || !scheduleShipDt.matches("\\d{4}-\\d{2}-\\d{2}")) {
                return "배송예정일을 확인하세요.";
            }

            if (LocalDate.parse(scheduleShipDt).isBefore(LocalDate.now())) {
                return "배송예정일을 오늘보다 과거로 설정할 수 없습니다.";
            }
        }

        return "";
    }

    /**
     * 선택발송처리 팝업 리스트 조회
     */
    @Override
    public List<ShipManageShipAllPopListGetDto> getShipAllPopList(ShipManageShipAllPopListSetDto shipManageShipAllPopListSetDto) {
      log.info("getShipAllPopList param : {}", shipManageShipAllPopListSetDto);

      return shipManageMasterMapper.selectShipAllPopList(shipManageShipAllPopListSetDto.getBundleNo());
    }

    /**
     * 배송완료 팝업 리스트 조회
     */
    @Override
    public List<ShipManageShipCompletePopListGetDto> getShipCompletePopList(ShipManageShipCompleteSetDto shipManageShipCompleteSetDto, String route) {
      log.info("getShipCompletePopList param : {}", shipManageShipCompleteSetDto);

      List<String> bundleNoList = shipManageShipCompleteSetDto.getBundleNoList();
      String partnerId = PARTNER.equals(route) ? shipManageShipCompleteSetDto.getChgId() : "";
      List<ShipManageShipCompletePopListGetDto> shipManageShipCompleteListGetDtoPopList = new ArrayList<>();

      for (String bundleNo : bundleNoList) {
        shipManageShipCompleteListGetDtoPopList.addAll(shipManageMasterMapper.selectShipCompleteList(bundleNo, partnerId));
      }

      for (ShipManageShipCompletePopListGetDto dto : shipManageShipCompleteListGetDtoPopList) {
        dto.setReceiverNm(PrivacyMaskingUtils.maskingUserName(dto.getReceiverNm()));
      }

      return shipManageShipCompleteListGetDtoPopList;
    }

    /**
     * 배송완료
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, ShippingException.class})
    public ResponseObject<String> setShipComplete(ShipManageShipCompleteSetDto shipManageShipCompleteSetDto, String route) {
        log.info("setShipComplete param : {}", shipManageShipCompleteSetDto);

        int result = 0;
        List<String> bundleNoList = shipManageShipCompleteSetDto.getBundleNoList();
        String chgId = shipManageShipCompleteSetDto.getChgId();
        String partnerId = PARTNER.equals(route) ? shipManageShipCompleteSetDto.getChgId() : "";
        List<String> orgBundleNoList;

        for (String bundleNo : bundleNoList) {
          List<ShipManageShipCompletePopListGetDto> shipCompleteList = shipManageMasterMapper.selectShipCompleteList(bundleNo, partnerId);
          String failMsg = "";
          String mallType = null;

          if(shipCompleteList.size() == 0) {
            failMsg = "처리할 상품주문 목록이 없습니다";
          }

          for (ShipManageShipCompletePopListGetDto dto : shipCompleteList) {
            Map<String, Object> shipInfo = new HashMap<>();
            String orderItemNo = dto.getOrderItemNo();

            // 배송정보 조회
            shipInfo = shipManageMasterMapper.selectShipInfoByOrderItemNo(orderItemNo);

            // 상품주문번호 확인
            if (ObjectUtils.isEmpty(shipInfo)) {
              failMsg = "상품 주문번호를 확인하세요";
              break;
            }

            // PO에서 접근 시, 파트너 물품인지 체크
            if (PARTNER.equals(route)) {
              if(!partnerCommonService.isPartnerItemByOrderItemNo(orderItemNo, partnerId)) {
                failMsg = "본 계정의 물품이 아닙니다["+orderItemNo+"]";
                break;
              }
            }

            // samsung 상품 말일 완료처리 불가
            if("samsung".equals(shipInfo.get("partnerId")) && DateUtil.isEndOfCurrentMonth()) {
              failMsg = "samgsung 상품은 매월 말일 배송완료 처리가 불가합니다.\n다음 달 1일 이후 완료처리 진행 부탁드립니다.";
              break;
            }

            // 배송중인 건만 가능
            if (!"D3".equals(shipInfo.get("shipStatus"))) {
              failMsg = "배송중인 건만 가능합니다[현재 주문상태:" + shipInfo.get("shipStatus") + "]";
              break;
            }
            if (mallType == null) {
              mallType = shipInfo.get("mallType").toString();
            }
          }

          // 검증 성공
          if(StringUtils.isEmpty(failMsg)) {
            // 업데이트
            int returnVal;
            if (mallType == null || "DS".equals(mallType)) {
              returnVal = shipManageMasterMapper.updateShipComplete(bundleNo, chgId);

              // 업데이트 성공
              if (returnVal > 0) {
                historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("배송완료", "", "", route, chgId, bundleNo));
                result++;
              } else {
                failMsg = "DB업데이트 처리 에러";
              }
            } else {
              orgBundleNoList = shipManageSlaveMapper.getOrgBundleNo(bundleNo);
              for (String orgBundleNo : orgBundleNoList) {
                returnVal = shipManageMasterMapper.updateShipComplete(orgBundleNo, chgId);

                // 업데이트 성공
                if (returnVal > 0) {
                  historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("배송완료", "", "", route, chgId, orgBundleNo));
                  result++;
                } else {
                  failMsg = "DB업데이트 처리 에러";
                }
              }
            }

          }

          // 검증 OR DB업데이트 실패 , 종료
          if(!StringUtils.isEmpty(failMsg)) {
            historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("배송완료 실패", failMsg, "", route, chgId, bundleNo));
          }
        }

        return ResponseUtil.getResoponseObject(Integer.toString(result));
    }

  /**
   * 구매확정(Bundle단위)
   */
  @Override
  public ResponseObject<String> setPurchaseCompleteByBundle(ShipManageShipCompleteByBundleSetDto shipManageShipCompleteByBundleSetDto, String route) {
    log.info("setPurchaseCompleteByBundle param : {}", shipManageShipCompleteByBundleSetDto);

    try {
      int result = 0;
      String chgId = shipManageShipCompleteByBundleSetDto.getChgId();
      String bundleNo = shipManageShipCompleteByBundleSetDto.getBundleNo();

      // 구매확정 처리
      int returnVal = shipManageMasterMapper.updatePurchaseCompleteByBundle(bundleNo, chgId);

      if (returnVal > 0){
        historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("구매확정", "","", route, chgId, bundleNo));
        result++;
      } else {
        historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("구매확정 실패", "","", route, chgId, bundleNo));
      }

      log.info("setPurchaseCompleteByBundle result count : {}", result);

      return ResponseUtil.getResoponseObject(Integer.toString(result));
    } catch (Exception e) {
      log.error("setPurchaseCompleteByBundle Error : {}", e.getMessage());
      return ResponseUtil.getResoponseObject(ExceptionCode.ERROR_CODE_3022);
    }
  }

  /**
   * 점포정보 조회
   */
  public ShipStoreInfoGetDto getStoreInfo(String storeId) {
    return shipManageSlaveMapper.selectStoreInfo(storeId);
  }

  /**
   * 점포명 조회
   */
  public String getStoreNm(String storeId) {
    String storeNm = shipManageSlaveMapper.selectStoreNm(storeId);

    if (StringUtils.isEmpty(storeNm)) {
      storeNm = "";
    }
    return storeNm;
  }

  /**
   * 배송정보출력 조회
   */
  @Override
  public List<ShipInfoPrintGetDto> getShipInfoPrintList(String bundleNo) {
    log.info("getShipInfoPrintList param : {}", bundleNo);

    List<ShipInfoPrintGetDto> shipInfoPrintList = shipManageSlaveMapper.selectShipInfoPrintList(bundleNo);

    return shipInfoPrintList;
  }
}
