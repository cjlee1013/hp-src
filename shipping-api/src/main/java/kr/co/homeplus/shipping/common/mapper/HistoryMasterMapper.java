package kr.co.homeplus.shipping.common.mapper;

import kr.co.homeplus.shipping.common.model.history.*;
import kr.co.homeplus.shipping.core.db.annotation.MasterConnection;

@MasterConnection
public interface HistoryMasterMapper {
  // 동일송장 히스토리 생성
  int setShippingHistoryInvoice(ShippingHistoryInsertSetDto shippingHistoryInsertSetDto);
  int setPickHistoryInvoice(PickHistoryInsertSetDto pickHistoryInsertSetDto);
  int setExchHistoryInvoice(ExchHistoryInsertSetDto exchHistoryInsertSetDto);

  // 주문조회 히스토리 생성
  int setShippingProcessHistory(ShippingProcessInsertSetDto historyInsertSetDto);
  int setShippingProcessHistoryBundle(BundleProcessInsertSetDto historyInsertSetDto);
  int setPickProcessHistory(PickProcessInsertSetDto historyInsertSetDto);
  int setExchProcessHistory(ExchProcessInsertSetDto historyInsertSetDto);

  // 멀티배송용 히스토리 생성
  int setMultiShippingHistoryInvoice(MultiShippingHistoryInsertSetDto shippingHistoryInsertSetDto);
  int setMultiShippingProcessHistory(MultiShippingProcessInsertSetDto historyInsertSetDto);

  // 공용(외부키 없이) 히스토리 생성
  int setCommonHistoryInvoice(CommonHistoryInsertSetDto commonHistoryInsertSetDto);
}