package kr.co.homeplus.shipping.external.service;

import java.util.List;
import kr.co.homeplus.shipping.external.model.store.*;

/**
 * shipping service interface
 */
public interface StoreService {
    List<StoreShipResultGetDto> setShipStart(List<StoreShipStartSetDto> storeShipStartSetDto);

    List<StoreShipResultGetDto> setShipComplete(List<StoreShipCompleteSetDto> storeShipCompleteSetDto);

    List<StoreShipResultGetDto> setSordNo(List<StoreShipVaninfoSetDto> storeShipStartSetDto);

    List<StoreClaimResultGetDto> setPickStart(List<StorePickStartSetDto> storePickStartSetDto);

    List<StoreClaimResultGetDto> setPickComplete(List<StorePickCompleteSetDto> storePickCompleteSetDto);

    List<StoreClaimResultGetDto> setExchStart(List<StoreExchStartSetDto> storeExchStartSetDto);

    List<StoreClaimResultGetDto> setExchComplete(List<StoreExchCompleteSetDto> storeExchCompleteSetDto);
}
