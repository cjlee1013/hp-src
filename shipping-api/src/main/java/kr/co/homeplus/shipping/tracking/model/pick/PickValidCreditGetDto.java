package kr.co.homeplus.shipping.tracking.model.pick;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "신용코드 확인 파라미터")
public class PickValidCreditGetDto {
    @ApiModelProperty(notes = "택배사별 신용코드")
    private String creditCd;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    @ApiModelProperty(notes = "성공여부")
    private String success;

    @ApiModelProperty(notes = "결과값")
    private String detail;

    public PickValidCreditGetDto(String creditCd, String dlvCd, String success, String detail) {
        this.creditCd = creditCd;
        this.dlvCd = dlvCd;
        this.success = success;
        this.detail = detail;
    }
}
