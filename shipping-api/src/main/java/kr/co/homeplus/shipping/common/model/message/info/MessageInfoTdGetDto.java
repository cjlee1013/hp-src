package kr.co.homeplus.shipping.common.model.message.info;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "메세지 발송 정보 - TD 선물메시지")
public class MessageInfoTdGetDto extends MessageInfoBaseDto{
    @ApiModelProperty(notes = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "구매자")
    private String buyerNm;

    @ApiModelProperty(notes = "수령인")
    private String receiverNm;

    @ApiModelProperty(notes = "선물메시지")
    private String giftMsg;

    @ApiModelProperty(notes = "배송예정시간")
    private String slotShipTime;
}
