package kr.co.homeplus.shipping.tracking.model.pick;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.shipping.tracking.model.common.CommonResultSetDto;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class PickResultSetDto extends CommonResultSetDto {
    @ApiModelProperty(notes = "클레임번들번호")
    private String claimPickShippingNo;
    public PickResultSetDto(String claimPickShippingNo, String success,String result,String detail) {
        super(success, result, detail);
        this.claimPickShippingNo = claimPickShippingNo;
    }

    public String getClaimPickShippingNo() {
        return claimPickShippingNo;
    }
}
