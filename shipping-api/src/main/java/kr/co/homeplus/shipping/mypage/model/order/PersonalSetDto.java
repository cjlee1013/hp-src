package kr.co.homeplus.shipping.mypage.model.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
@ApiModel(description = "운송업체정보 제공내역")
public class PersonalSetDto {
    @ApiModelProperty(value = "조회시작일", position = 1)
    private String schStartDt;
    @ApiModelProperty(value = "조회종료일", position = 2)
    private String schEndDt;
    @ApiModelProperty(value = "페이지", position = 3)
    private Integer page;
    @ApiModelProperty(value = "페이지당뷰수", position = 4)
    private Integer perPage;
    @ApiModelProperty(value = "고객번호", hidden = true, position = 5)
    private Long userNo;
    @ApiModelProperty(value = "사이트종류", position = 6)
    private String siteType;

    private int startPage;

    public void setStartPage(){
        if (page <= 0) {
            startPage = 0;
        } else {
            startPage = (page-1)*perPage;
        }
    }
}
