package kr.co.homeplus.shipping.admin.model.monitoring;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "발송지연조회 응답")
public class MonitoringDelayGetDto {
    @ApiModelProperty(notes = "판매자명")
    private String partnerNm;

    @ApiModelProperty(notes = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;

    @ApiModelProperty(notes = "주문일시")
    private String orderDt;

    @ApiModelProperty(notes = "배송상태")
    private String shipStatusNm;

    @ApiModelProperty(notes = "배송상태코드")
    private String shipStatus;

    @ApiModelProperty(notes = "마켓유형")
    private String marketType;

    @ApiModelProperty(notes = "마켓주문번호")
    private String marketOrderNo;

    @ApiModelProperty(notes = "주문확인일")
    private String confirmDt;

    @ApiModelProperty(notes = "주문확인지연일")
    private String delayConfirmDay;

    @ApiModelProperty(notes = "배송요청일")
    private String reserveShipDt;

    @ApiModelProperty(notes = "배송예정일")
    private String scheduleShipDt;

    @ApiModelProperty(notes = "발송기한")
    private String orgShipDt;

    @ApiModelProperty(notes = "2차발송기한")
    private String delayShipDt;

    @ApiModelProperty(notes = "발송지연처리일")
    private String delayShipRegDt;

    @ApiModelProperty(notes = "발송지연일수")
    private String delayShipDay;

    @ApiModelProperty(notes = "상품주문번호")
    private String orderItemNo;

    @ApiModelProperty(notes = "배송방법")
    private String shipMethodNm;

    @ApiModelProperty(notes = "배송방법코드")
    private String shipMethod;

    @ApiModelProperty(notes = "택배사")
    private String dlvCdNm;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "회원번호")
    private String userNo;

    @ApiModelProperty(notes = "회원번호마스킹")
    private String userNoMask;

    @ApiModelProperty(notes = "구매자")
    private String buyerNm;

    @ApiModelProperty(notes = "수령인")
    private String receiverNm;

    @ApiModelProperty(notes = "점포유형")
    private String storeype;

    @ApiModelProperty(notes = "사이트유형")
    private String siteType;
}
