package kr.co.homeplus.shipping.admin.service.impl;

import java.util.List;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.shipping.admin.mapper.MonitoringSlaveMapper;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringDelayGetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringDelaySetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringPickGetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringPickSetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringShipGetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringShipSetDto;
import kr.co.homeplus.shipping.admin.service.MonitoringService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MonitoringServiceImpl implements MonitoringService {
    private final MonitoringSlaveMapper monitoringSlaveMapper;

    public List<MonitoringDelayGetDto> getMonitoringDelayList(MonitoringDelaySetDto monitoringDelaySetDto){
        List<MonitoringDelayGetDto> monitoringDelayGetDtoList = monitoringSlaveMapper.selectMonitoringDelayList(monitoringDelaySetDto);

        for (MonitoringDelayGetDto dto : monitoringDelayGetDtoList) {
            dto.setBuyerNm(PrivacyMaskingUtils.maskingUserName(dto.getBuyerNm()));
            dto.setReceiverNm(PrivacyMaskingUtils.maskingUserName(dto.getReceiverNm()));
            dto.setUserNoMask(PrivacyMaskingUtils.maskingUserId(dto.getUserNo()));
        }

        return monitoringDelayGetDtoList;
    }

    public List<MonitoringShipGetDto> getMonitoringShipList(MonitoringShipSetDto monitoringShipSetDto){
        List<MonitoringShipGetDto> monitoringShipGetDtoList = monitoringSlaveMapper.selectMonitoringShipList(monitoringShipSetDto);

        for (MonitoringShipGetDto dto : monitoringShipGetDtoList) {
            dto.setBuyerNm(PrivacyMaskingUtils.maskingUserName(dto.getBuyerNm()));
            dto.setReceiverNm(PrivacyMaskingUtils.maskingUserName(dto.getReceiverNm()));
            dto.setUserNoMask(PrivacyMaskingUtils.maskingUserId(dto.getUserNo()));
        }

        return monitoringShipGetDtoList;
    }

    public List<MonitoringPickGetDto> getMonitoringPickList(MonitoringPickSetDto monitoringPickSetDto){
        List<MonitoringPickGetDto> monitoringPickGetDtoList = monitoringSlaveMapper.selectMonitoringPickList(monitoringPickSetDto);

        for (MonitoringPickGetDto dto : monitoringPickGetDtoList) {
            dto.setPickMobileNo(PrivacyMaskingUtils.maskingPhone(dto.getPickMobileNo()));
            dto.setBuyerNm(PrivacyMaskingUtils.maskingUserName(dto.getBuyerNm()));
            dto.setPickReceiverNm(PrivacyMaskingUtils.maskingUserName(dto.getPickReceiverNm()));
            dto.setPickAddr(PrivacyMaskingUtils.maskingAddress(dto.getPickAddr()));
            dto.setUserNoMask(PrivacyMaskingUtils.maskingUserId(dto.getUserNo()));
        }

        return monitoringPickGetDtoList;
    }
}
