package kr.co.homeplus.shipping.partner.model.sell.ticketManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 이티켓판매관리 주문확인 요청 파라미터")
public class TicketManageConfirmOrderSetDto {
    @ApiModelProperty(notes = "배송SEQ 리스트")
    private List<String> shipNoList;

    @ApiModelProperty(notes = "수정자")
    private String chgId;
}