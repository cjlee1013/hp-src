package kr.co.homeplus.shipping.admin.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 예약배송관리 리스트 응답 DTO")
public class ShipReserveManageListGetDto {
    @ApiModelProperty(notes = "몰유형")
    private String mallType;

    @ApiModelProperty(notes = "점포유형")
    private String storeType;

    @ApiModelProperty(notes = "판매업체")
    private String storeNm;

    @ApiModelProperty(notes = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;

    @ApiModelProperty(notes = "상품주문번호")
    private String orderItemNo;

    @ApiModelProperty(notes = "주문상태")
    private String shipStatusNm;

    @ApiModelProperty(notes = "주문상태코드")
    private String shipStatus;

    @ApiModelProperty(notes = "주문일시")
    private String orderDt;

    @ApiModelProperty(notes = "희망배송일")
    private String reserveShipDt;

    @ApiModelProperty(notes = "배송요청일")
    private String shipDt;

    @ApiModelProperty(notes = "배송Shift")
    private String shiftId;

    @ApiModelProperty(notes = "단축번호")
    private String sordNo;

    @ApiModelProperty(notes = "배송시간")
    private String slotShipTime;

    @ApiModelProperty(notes = "배송방법")
    private String shipMethodNm;

    @ApiModelProperty(notes = "배송방법코드")
    private String shipMethod;

    @ApiModelProperty(notes = "택배사")
    private String dlvCdNm;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "상품번호")
    private String itemNo;

    @ApiModelProperty(notes = "상품명")
    private String itemNm1;

    @ApiModelProperty(notes = "옵션")
    private String txtOptVal;

    @ApiModelProperty(notes = "수량")
    private String itemQty;

    @ApiModelProperty(notes = "구매자")
    private String buyerNm;

    @ApiModelProperty(notes = "구매자 연락처")
    private String buyerMobileNo;

    @ApiModelProperty(notes = "법인")
    private String corpOrder;

    @ApiModelProperty(notes = "수령인")
    private String receiverNm;

    @ApiModelProperty(notes = "수령인 연락처")
    private String shipMobileNo;

    @ApiModelProperty(notes = "우편번호")
    private String zipcode;

    @ApiModelProperty(notes = "주소")
    private String addr;

    @ApiModelProperty(notes = "배송메세지")
    private String dlvShipMsg;

    @ApiModelProperty(notes = "결제일시")
    private String paymentFshDt;

    @ApiModelProperty(notes = "사이트유형")
    private String siteType;

    @ApiModelProperty(notes = "배송유형")
    private String shipType;

    @ApiModelProperty(notes = "점포ID")
    private String storeId;

    @ApiModelProperty(notes = "회원번호")
    private String userNo;
}
