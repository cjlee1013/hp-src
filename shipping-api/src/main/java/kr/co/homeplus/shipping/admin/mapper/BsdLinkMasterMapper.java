package kr.co.homeplus.shipping.admin.mapper;

import kr.co.homeplus.shipping.admin.model.shipManage.BsdInvoiceSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdLinkTranSetDto;
import kr.co.homeplus.shipping.core.db.annotation.MasterConnection;

@MasterConnection
public interface BsdLinkMasterMapper {
  int insertBsdLinkTran(BsdLinkTranSetDto bsdLinkTranSetDto);
  int updateBsdLinkResult(BsdLinkTranSetDto bsdLinkTranSetDto);
  int updateShippingInvoice(BsdInvoiceSetDto bsdInvoiceSetDto);
  int updateMultiOrderInvoice(BsdInvoiceSetDto bsdInvoiceSetDto);
}
