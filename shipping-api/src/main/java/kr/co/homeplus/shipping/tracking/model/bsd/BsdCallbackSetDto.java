package kr.co.homeplus.shipping.tracking.model.bsd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "BSD 추적 콜백")
public class BsdCallbackSetDto {
    @ApiModelProperty(notes = "BSD일련번호")
    private String bsdLinkNo;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "수거상태")
    private String callbackStatus;

    @ApiModelProperty(notes = "처리시간")
    private String callbackDt;

    public BsdCallbackSetDto(String bsdLinkNo, String invoiceNo, String callbackDt) {
        this.bsdLinkNo = bsdLinkNo;
        this.invoiceNo = invoiceNo;
        this.callbackDt = callbackDt;
    }

    public BsdCallbackSetDto(String bsdLinkNo, String invoiceNo,
        String callbackStatus, String callbackDt) {
        this.bsdLinkNo = bsdLinkNo;
        this.invoiceNo = invoiceNo;
        this.callbackStatus = callbackStatus;
        this.callbackDt = callbackDt;
    }

    public String getCallbackStatus() {
        return callbackStatus;
    }
}
