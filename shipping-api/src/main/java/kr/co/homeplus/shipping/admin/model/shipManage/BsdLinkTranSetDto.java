package kr.co.homeplus.shipping.admin.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > BSD연동조회 리스트 요청 DTO")
public class BsdLinkTranSetDto {
    @ApiModelProperty(notes = "다중주문아이템번호리스트")
    private List<String> bsdLinkNoList;
}
