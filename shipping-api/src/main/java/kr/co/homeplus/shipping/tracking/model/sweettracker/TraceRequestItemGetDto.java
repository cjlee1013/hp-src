package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModelProperty;
import java.beans.ConstructorProperties;
import lombok.Data;

@Data
public class TraceRequestItemGetDto {
  @ApiModelProperty(notes = "배송번호")
  private String fid;

  @ApiModelProperty(notes = "송장번호")
  private String num;

  @ApiModelProperty(notes = "성공여부")
  private String success;

  @ApiModelProperty(notes = "에러코드")
  private String e_code;

  @ApiModelProperty(notes = "에러메시지")
  private String e_message;

  public String getFid() {
    return fid;
  }

  public String getNum() {
    return num;
  }

  public String getSuccess() {
    return success;
  }

  public String getE_code() {
    return e_code;
  }

  public String getE_message() {
    return e_message;
  }

  public String getDetail(){
    if ("true".equals(success)) {
      return "R";
    } else {
      switch (e_code) {
        case "1":
        case "01":
          return "F1";
        case "2":
        case "02":
          return "F2";
        case "4":
        case "04":
          return "F4";
        default:
          return "F9";
      }
    }
  }
  @ConstructorProperties({"fid","num","success","e_code","e_message"})
  public TraceRequestItemGetDto(String fid, String num, String success, String e_code,
      String e_message) {
    this.fid = fid;
    this.num = num;
    this.success = success;
    this.e_code = e_code;
    this.e_message = e_message;
  }
}