package kr.co.homeplus.shipping.external.model.store;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "옴니연동 배송상태값")
public class StoreShipInfoGetDto {
    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;

    @ApiModelProperty(notes = "배송번호")
    private String shipNo;

    @ApiModelProperty(notes = "배송상태")
    private String shipStatus;
}
