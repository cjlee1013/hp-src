package kr.co.homeplus.shipping.market.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.market.model.MarketInquiryDetailGetDto;
import kr.co.homeplus.shipping.market.model.MarketInquiryDetailSetDto;
import kr.co.homeplus.shipping.market.model.MarketInquiryListGetDto;
import kr.co.homeplus.shipping.market.model.MarketInquiryListSetDto;
import kr.co.homeplus.shipping.market.service.MarketInquiryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/market")
@Api(tags = "제휴사 고객문의")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class MarketInquiryController {
  private final MarketInquiryService marketInquiryService;

  @ApiOperation(value = "제휴사 고객문의 리스트 조회")
  @PostMapping("/getMarketInquiryList")
  public ResponseObject<List<MarketInquiryListGetDto>> getMarketInquiryList(@RequestBody @Valid MarketInquiryListSetDto marketInquiryListSetDto) {
    return ResourceConverter.toResponseObject(marketInquiryService.getMarketInquiryList(marketInquiryListSetDto));
  }

  @ApiOperation(value = "제휴사 고객문의 상세 조회")
  @PostMapping("/getMarketInquiryDetail")
  public ResponseObject<MarketInquiryDetailGetDto> getMarketInquiryDetail(@RequestBody @Valid MarketInquiryDetailSetDto marketInquiryDetailSetDto) {
    return ResourceConverter.toResponseObject(marketInquiryService.getMarketInquiryDetail(marketInquiryDetailSetDto));
  }
}