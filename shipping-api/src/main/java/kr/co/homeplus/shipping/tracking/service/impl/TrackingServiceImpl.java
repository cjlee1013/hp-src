package kr.co.homeplus.shipping.tracking.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdResultSetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.common.DlvCdMappingGetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchResultSetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiResultSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickAddressGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickResultSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingResultSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingTrackingGetDto;
import kr.co.homeplus.shipping.tracking.mapper.TrackingMasterMapper;
import kr.co.homeplus.shipping.tracking.mapper.TrackingSlaveMapper;
import kr.co.homeplus.shipping.tracking.service.TrackingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class TrackingServiceImpl implements TrackingService {

    private final TrackingMasterMapper trackingMasterMapper;
    private final TrackingSlaveMapper trackingSlaveMapper;

    @Override
    public Map<String,ShippingTrackingGetDto> getShippingInvoice(List<ShippingRequestSetDto> shippingRequestSetDto) {
        List<ShippingTrackingGetDto> shippingTrackingGetDtoList;
        try {
            shippingTrackingGetDtoList = trackingSlaveMapper.getShippingInvoice(shippingRequestSetDto);
        } catch (Exception e) {
            return null;
        }
        Map<String,ShippingTrackingGetDto> map = new HashMap<>();
        for (ShippingTrackingGetDto shippingTrackingGetDto : shippingTrackingGetDtoList) {
            map.put(shippingTrackingGetDto.getNo(),shippingTrackingGetDto);
        }
        return map;
    }

    @Override
    public List<ShippingRequestGetDto> setShippingTrackingResult(ShippingResultSetDto shippingResultSetDto,Map<String,ShippingTrackingGetDto> shippingTrackingGetDtoMap) {
        List<ShippingRequestGetDto> shippingRequestGetDtoList = new ArrayList<>();
        ShippingRequestGetDto shippingRequestGetDto;
        ShippingTrackingGetDto shippingTrackingGetDto;
        shippingTrackingGetDto = shippingTrackingGetDtoMap.get(shippingResultSetDto.getShipNo());
        if (shippingTrackingGetDto != null) {
            for (String shipNo : shippingTrackingGetDto.getShipNos()) {
                shippingResultSetDto.setShipNo(shipNo);
                trackingMasterMapper.setShippingTrackingResult(shippingResultSetDto);
                shippingRequestGetDto = new ShippingRequestGetDto(shipNo, shippingResultSetDto.getSuccess(), shippingResultSetDto.getDetail());
                shippingRequestGetDtoList.add(shippingRequestGetDto);
            }
        }
        return shippingRequestGetDtoList;
    }

    @Override
    public int setShippingTrackingCallback(ShippingCallbackSetDto shippingCallbackSetDto)
    {
        try {
            if ("D4".equals(shippingCallbackSetDto.getCallbackStatus())) {
                List<String> shipNoList = trackingSlaveMapper.getShippingNo(shippingCallbackSetDto.getShipNo());
                for (String shipNo : shipNoList) {
                    shippingCallbackSetDto.setShipNo(shipNo);
                    trackingMasterMapper.setShippingComplete(shippingCallbackSetDto);
                }
            }
            return 1;
        } catch (Exception e) {
            return -9999;
        }
    }

    @Override
    public Map<String, PickTrackingGetDto> getPickInvoice(List<PickRequestSetDto> pickRequestSetDtoList) {
        List<PickTrackingGetDto> pickTrackingGetDtoList;
        try {
            pickTrackingGetDtoList = trackingSlaveMapper.getPickInvoice(pickRequestSetDtoList);
        } catch (Exception e) {
            return null;
        }
        Map<String,PickTrackingGetDto> map = new HashMap<>();
        for (PickTrackingGetDto pickTrackingGetDto : pickTrackingGetDtoList) {
            map.put(pickTrackingGetDto.getNo(),pickTrackingGetDto);
        }
        return map;
    }

    @Override
    public List<PickRequestGetDto> setPickTrackingResult(PickResultSetDto pickResultSetDto, Map<String, PickTrackingGetDto> pickTrackingGetDtoMap) {
        trackingMasterMapper.setPickTrackingResult(pickResultSetDto);
        List<PickRequestGetDto> pickRequestGetDtoList = new ArrayList<>();
        PickRequestGetDto pickRequestGetDto;
        PickTrackingGetDto pickTrackingGetDto;
        pickTrackingGetDto = pickTrackingGetDtoMap.get(pickResultSetDto.getClaimPickShippingNo());
        if (pickTrackingGetDto != null) {
            for (String claimPickShippingNo : pickTrackingGetDto.getClaimPickShippingNos()) {
                pickRequestGetDto = new PickRequestGetDto(claimPickShippingNo, pickResultSetDto.getSuccess(), pickResultSetDto.getDetail());
                pickRequestGetDtoList.add(pickRequestGetDto);
            }
        }
        return pickRequestGetDtoList;
    }


    @Override
    public int setPickTrackingCallback(PickCallbackSetDto pickCallbackSetDto) {
        try {
            if ("P3".equals(pickCallbackSetDto.getCallbackStatus())) {
                return trackingMasterMapper.setPickComplete(pickCallbackSetDto);
            }
            return 1;
        } catch (Exception e) {
            return -9999;
        }
    }

    @Override
    public Map<String, ExchTrackingGetDto> getExchInvoice(List<ExchRequestSetDto> exchRequestSetDtoList) {
        List<ExchTrackingGetDto> exchTrackingGetDtoList;
        try {
            exchTrackingGetDtoList = trackingSlaveMapper.getExchInvoice(exchRequestSetDtoList);
        } catch (Exception e) {
            return null;
        }
        Map<String,ExchTrackingGetDto> map = new HashMap<>();
        for (ExchTrackingGetDto exchTrackingGetDto : exchTrackingGetDtoList) {
            map.put(exchTrackingGetDto.getNo(),exchTrackingGetDto);
        }
        return map;
    }

    @Override
    public List<ExchRequestGetDto> setExchTrackingResult(ExchResultSetDto exchResultSetDto, Map<String, ExchTrackingGetDto> exchTrackingGetDtoMap) {
            trackingMasterMapper.setExchTrackingResult(exchResultSetDto);
            List<ExchRequestGetDto> exchRequestGetDtoList = new ArrayList<>();
            ExchRequestGetDto exchRequestGetDto;
            ExchTrackingGetDto exchTrackingGetDto;
            exchTrackingGetDto = exchTrackingGetDtoMap.get(exchResultSetDto.getClaimExchShippingNo());
            if (exchTrackingGetDto != null) {
                for (String claimExchShippingNo : exchTrackingGetDto.getClaimExchShippingNos()) {
                    exchRequestGetDto = new ExchRequestGetDto(claimExchShippingNo, exchResultSetDto.getSuccess(), exchResultSetDto.getDetail());
                    exchRequestGetDtoList.add(exchRequestGetDto);
                }
            }
            return exchRequestGetDtoList;
    }

    @Override
    public int setExchTrackingCallback(ExchCallbackSetDto exchCallbackSetDto) {
        try {
            if ("D3".equals(exchCallbackSetDto.getCallbackStatus())) {
                return trackingMasterMapper.setExchComplete(exchCallbackSetDto);
            }
            return 1;
        } catch (Exception e) {
            return -9999;
        }
    }

    @Override
    public List<PickAddressGetDto> getPickAddress(List<PickRequestSetDto> pickRequestSetDto) {
        return trackingSlaveMapper.getPickAddress(pickRequestSetDto);
    }

    @Override
    public PickRequestGetDto setPickRequestResult(PickResultSetDto pickResultSetDto) {
        if ("Y".equals(pickResultSetDto.getSuccess())) {
            trackingMasterMapper.setPickRequestSuccess(pickResultSetDto.getClaimPickShippingNo());
        } else if ("N".equals(pickResultSetDto.getSuccess())){
            trackingMasterMapper.setPickRequestFail(pickResultSetDto.getClaimPickShippingNo());
        }
        return new PickRequestGetDto(pickResultSetDto.getClaimPickShippingNo(),
                pickResultSetDto.getSuccess(), pickResultSetDto.getDetail());
    }

    @Override
    public PickRequestGetDto setPickRequestCancel(PickResultSetDto pickResultSetDto) {
        if ("Y".equals(pickResultSetDto.getSuccess())) {
            trackingMasterMapper.setPickRequestCancel(pickResultSetDto);
        }
        return new PickRequestGetDto(pickResultSetDto.getClaimPickShippingNo(),
            pickResultSetDto.getSuccess(), pickResultSetDto.getDetail());
    }

    @Override
    public int setPickRequestCallback(PickCallbackSetDto pickCallbackSetDto) {
        int ret;
        try {
            return trackingMasterMapper.setPickRequestCallback(pickCallbackSetDto);
        } catch (Exception e) {
            return -9999;
        }
    }

    @Override
    public List<DlvCdMappingGetDto> getDlvCdMapping() {
        return trackingSlaveMapper.getDlvCdMapping();
    }

    @Override
    public Map<String,MultiTrackingGetDto> getMultiInvoice(List<MultiRequestSetDto> multiRequestSetDto) {
        List<MultiTrackingGetDto> multiTrackingGetDtoList;
        try {
            multiTrackingGetDtoList = trackingSlaveMapper.getMultiInvoice(multiRequestSetDto);
        } catch (Exception e) {
            return null;
        }
        Map<String,MultiTrackingGetDto> map = new HashMap<>();
        for (MultiTrackingGetDto multiTrackingGetDto : multiTrackingGetDtoList) {
            map.put(multiTrackingGetDto.getNo(),multiTrackingGetDto);
        }
        return map;
    }

    @Override
    public List<MultiRequestGetDto> setMultiTrackingResult(MultiResultSetDto multiResultSetDto,Map<String,MultiTrackingGetDto> multiTrackingGetDtoMap) {
        trackingMasterMapper.setMultiTrackingResult(multiResultSetDto);
        List<MultiRequestGetDto> multiRequestGetDtoList = new ArrayList<>();
        MultiRequestGetDto multiRequestGetDto;
        MultiTrackingGetDto multiTrackingGetDto = multiTrackingGetDtoMap.get(multiResultSetDto.getMultiOrderItemNo());
        if (multiTrackingGetDto != null) {
            for (String multiOrderItemNo : multiTrackingGetDto.getMultiOrderItemNos()) {
                multiRequestGetDto = new MultiRequestGetDto(multiOrderItemNo, multiResultSetDto.getSuccess(), multiResultSetDto.getDetail());
                multiRequestGetDtoList.add(multiRequestGetDto);
            }
        }
        return multiRequestGetDtoList;
    }

    @Override
    public int setMultiTrackingCallback(MultiCallbackSetDto multiCallbackSetDto)
    {
        try {
            if ("D4".equals(multiCallbackSetDto.getCallbackStatus())) {
                return trackingMasterMapper.setMultiComplete(multiCallbackSetDto);
            }
            return 1;
        } catch (Exception e) {
            return -9999;
        }
    }

    @Override
    public Map<String, BsdTrackingGetDto> getBsdInvoice(List<BsdRequestSetDto> bsdRequestSetDtoList) {
        List<BsdTrackingGetDto> bsdTrackingGetDtoList;
        try {
            bsdTrackingGetDtoList = trackingSlaveMapper.getBsdInvoice(bsdRequestSetDtoList);
        } catch (Exception e) {
            return null;
        }
        Map<String,BsdTrackingGetDto> map = new HashMap<>();
        for (BsdTrackingGetDto bsdTrackingGetDto : bsdTrackingGetDtoList) {
            map.put(bsdTrackingGetDto.getNo(),bsdTrackingGetDto);
        }
        return map;
    }

    @Override
    public List<BsdRequestGetDto> setBsdTrackingResult(BsdResultSetDto bsdResultSetDto,
        Map<String, BsdTrackingGetDto> bsdTrackingGetDtoMap) {
        trackingMasterMapper.setBsdTrackingResult(bsdResultSetDto);
        List<BsdRequestGetDto> bsdRequestGetDtoList = new ArrayList<>();
        BsdRequestGetDto bsdRequestGetDto;
        BsdTrackingGetDto bsdTrackingGetDto = bsdTrackingGetDtoMap.get(bsdResultSetDto.getBsdLinkNo());
        if (bsdTrackingGetDto != null) {
            for (String bsdLinkNo : bsdTrackingGetDto.getBsdLinkNos()) {
                bsdRequestGetDto = new BsdRequestGetDto(bsdLinkNo, bsdResultSetDto.getSuccess(), bsdResultSetDto.getDetail());
                bsdRequestGetDtoList.add(bsdRequestGetDto);
            }
        }
        return bsdRequestGetDtoList;
    }

}
