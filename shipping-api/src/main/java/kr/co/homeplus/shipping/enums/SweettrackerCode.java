package kr.co.homeplus.shipping.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum SweettrackerCode {

			LEVEL_CODE_1("1", "배송준비")
	,   LEVEL_CODE_2("2", "집화완료")
	,   LEVEL_CODE_3("3", "배송진행중")
	,   LEVEL_CODE_4("4", "지점도착")
	,   LEVEL_CODE_5("5", "배송출발")
	,   LEVEL_CODE_6("6", "배송완료")
	,   LEVEL_CODE_99("99", "스캔오류")

	,   PICK_CODE_10("10", "주문 데이터 에러 (주문 접수된 데이터가 유효하지 않음)")
	,   PICK_CODE_12("12", "신용코드 오류 (신용코드가 정상적이지 않거나 거래종료 또는 Block됨)")
	,   PICK_CODE_30("30", "접수 등록 (반품주문 접수 등록)")
	,   PICK_CODE_40("40", "반품 운송장 출력 (반품 운송장번호 발번됨)")
	,   PICK_CODE_50("50", "미집하 (택배사에서 반품주문건에 대해 처리할 수 없는 상황)")
	,   PICK_CODE_51("51", "미집하1 (중복 접수건)")
	,   PICK_CODE_52("52", "미집하2 (타택배사 처리)")
	,   PICK_CODE_53("53", "미집하3 (송화인 통화 안됨)")
	,   PICK_CODE_54("54", "미집하4 (주소 오류)")
	,   PICK_CODE_60("60", "집하 완료 (고객으로 부터 화물을 인계 받음)")
	;

	@Getter
	private final String code;
	@Getter
	private final String description;

	public String toString() {
		return code + ": " + description;
	}
}
