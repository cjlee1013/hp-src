package kr.co.homeplus.shipping.external.model.store;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "옴니연동 클레임 기본 파라미터")
public class StoreClaimCommonSetDto {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "클레임번들번호", required = true)
    private String claimBundleNo;

    public String getClaimBundleNo() {
        return claimBundleNo;
    }
}
