package kr.co.homeplus.shipping.admin.mapper;

import java.util.List;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdLinkListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdLinkListSetDto;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface BsdLinkSlaveMapper {
  List<BsdLinkListGetDto> selectBsdLinkList(BsdLinkListSetDto bsdLinkListSetDto);
}
