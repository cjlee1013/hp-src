package kr.co.homeplus.shipping.partner.service.impl;

import java.util.List;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.shipping.common.service.HistoryService;
import kr.co.homeplus.shipping.enums.DateType;
import kr.co.homeplus.shipping.partner.mapper.ShipStatusMasterMapper;
import kr.co.homeplus.shipping.partner.mapper.ShipStatusSlaveMapper;
import kr.co.homeplus.shipping.partner.model.sell.shipStatus.ShipStatusCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.shipStatus.ShipStatusListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.shipStatus.ShipStatusListSetDto;
import kr.co.homeplus.shipping.partner.service.ShipStatusService;
import kr.co.homeplus.shipping.utils.DateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipStatusServiceImpl implements ShipStatusService {
    private final ShipStatusSlaveMapper shipStatusSlaveMapper;
    private final ShipStatusMasterMapper shipStatusMasterMapper;
    private final HistoryService historyService;

    /**
     * 배송현황 조회
     */
    @Override
    public List<ShipStatusListGetDto> getShipStatusList(ShipStatusListSetDto shipStatusListSetDto) {
      log.info("getShipStatusList param : {}", shipStatusListSetDto);

      List<ShipStatusListGetDto> shipStatusListGetDtoList = shipStatusSlaveMapper.selectShipStatusList(shipStatusListSetDto);

      return this.getMaskingShipStatusList(shipStatusListGetDtoList);
    }

    /**
     * 배송현황 카운트 리스트 조회
     */
    @Override
    public List<ShipStatusListGetDto> getShipStatusCntList(String schType, String partnerId) {
      log.info("getShipStatusCntList param : {}", partnerId);

      List<ShipStatusListGetDto> shipStatusListGetDtoList = shipStatusSlaveMapper.selectShipStatusCntList(schType, partnerId);

      return this.getMaskingShipStatusList(shipStatusListGetDtoList);
    }

    /**
     * 배송현황 리스트 마스킹
     */
    private List<ShipStatusListGetDto> getMaskingShipStatusList(List<ShipStatusListGetDto> shipStatusList) {
      for (ShipStatusListGetDto dto : shipStatusList) {
        // 배송완료 7일이 지나면 마스킹
        if (DateUtil.isAfter(dto.getCompleteDt(), DateType.DAY, 8)) {
          dto.setCompleteAfter3MonthYn("Y");
          dto.setBuyerNm(PrivacyMaskingUtils.maskingUserName(dto.getBuyerNm()));
          dto.setBuyerMobileNo(PrivacyMaskingUtils.maskingPhone(dto.getBuyerMobileNo()));
          dto.setReceiverNm(PrivacyMaskingUtils.maskingUserName(dto.getReceiverNm()));
          dto.setShipMobileNo(PrivacyMaskingUtils.maskingPhone(dto.getShipMobileNo()));
          dto.setAddr(PrivacyMaskingUtils.maskingAddress(dto.getAddr()));
          dto.setPersonalOverseaNo(PrivacyMaskingUtils.maskingCustomsIdNumber(dto.getPersonalOverseaNo()));
        }

        // 구매확정 90일이 지나면 클레임 불가
        if (DateUtil.isAfter(dto.getDecisionDt(), DateType.DAY, 91)) {
          dto.setClaimYn("N");
        }
      }

      return shipStatusList;
    }

    /**
     * 배송현황 카운트 조회
     */
    @Override
    public ShipStatusCntGetDto getShipStatusCnt(String partnerId) {
      log.info("getShipStatusCnt param : {}", partnerId);

      ShipStatusCntGetDto shipStatusCntGetDto = new ShipStatusCntGetDto();

      shipStatusCntGetDto.setShippingCnt(shipStatusSlaveMapper.selectShipStatusCnt("shippingCnt", partnerId));
      shipStatusCntGetDto.setCompleteCnt(shipStatusSlaveMapper.selectShipStatusCnt("completeCnt", partnerId));
      shipStatusCntGetDto.setNoReceiveCnt(shipStatusSlaveMapper.selectShipStatusCnt("noReceiveCnt", partnerId));

      return shipStatusCntGetDto;
    }

    /**
     * 배송현황 메인 카운트 조회
     */
    @Override
    public ShipStatusCntGetDto getShipStatusMainCnt(String partnerId) {
      log.info("getShipStatusMainCnt param : {}", partnerId);

      ShipStatusCntGetDto shipStatusCntGetDto = new ShipStatusCntGetDto();

      shipStatusCntGetDto.setShippingCnt(shipStatusSlaveMapper.selectShipStatusMainCnt("shippingCnt", partnerId));
      shipStatusCntGetDto.setCompleteCnt(shipStatusSlaveMapper.selectShipStatusMainCnt("completeCnt", partnerId));
      shipStatusCntGetDto.setNoReceiveCnt(shipStatusSlaveMapper.selectShipStatusMainCnt("noReceiveCnt", partnerId));

      return shipStatusCntGetDto;
    }
}
