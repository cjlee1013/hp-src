package kr.co.homeplus.shipping.market.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class MarketInquiryListSetDto {
    @ApiModelProperty(notes = "일자타입")
    private String schDtType;

    @ApiModelProperty(notes = "시작일")
    private String schStartDt;

    @ApiModelProperty(notes = "종료일")
    private String schEndDt;

    @ApiModelProperty(notes = "점포ID")
    private String schStoreId;

    @ApiModelProperty(notes = "검색어")
    private String schKeyword;

    @ApiModelProperty(notes = "검색어조건")
    private String schKeywordType;

    @ApiModelProperty(notes = "마켓연동")
    private String schMarketType;

    @ApiModelProperty(notes = "처리상태")
    private String schIsresponseStatus;

    @ApiModelProperty(notes = "등록구분")
    private String schMessageType;

    @ApiModelProperty(notes = "문의타입")
    private String schContactType;
}
