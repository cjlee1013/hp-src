package kr.co.homeplus.shipping.market.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.market.model.NaverInquiryAnswerSetDto;
import kr.co.homeplus.shipping.market.service.NaverInquiryService;
import kr.co.homeplus.shipping.utils.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/market")
@Api(tags = "제휴사 고객문의")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class NaverInquiryController {
  private final NaverInquiryService naverInquiryService;

  @ApiOperation(value = "네이버 문의답변")
  @PostMapping("/setNaverInquiryAnswer")
  public ResponseObject<String> setNaverInquiryAnswer(@RequestBody @Valid NaverInquiryAnswerSetDto naverInquiryAnswerSetDto) {
      try {
        return naverInquiryService.setNaverInquiryAnswer(naverInquiryAnswerSetDto);
      } catch (Exception e) {
        log.error("setNaverInquiryAnswer error {}",e.getMessage());
        return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_4001.getResponseMessage());
      }
  }
}