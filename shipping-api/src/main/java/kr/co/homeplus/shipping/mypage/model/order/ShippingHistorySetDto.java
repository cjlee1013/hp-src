package kr.co.homeplus.shipping.mypage.model.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송 히스토리 정보")
public class ShippingHistorySetDto {
    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;
}
