package kr.co.homeplus.shipping.enums;

public enum DateType {
    YEAR, MONTH, DAY, HOUR, MIN, SEC
}
