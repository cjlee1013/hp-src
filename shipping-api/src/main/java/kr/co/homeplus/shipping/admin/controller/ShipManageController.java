package kr.co.homeplus.shipping.admin.controller;

import static kr.co.homeplus.shipping.constants.ResourceRouteName.ADMIN;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipInfoPrintGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageConfirmOrderSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageListSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageNotiDelaySetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllExcelSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllPopListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllPopListSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipCompleteByBundleSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipCompletePopListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipCompleteSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipStoreInfoGetDto;
import kr.co.homeplus.shipping.admin.service.ShipManageService;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.mypage.model.order.ShippingHistoryGetDto;
import kr.co.homeplus.shipping.mypage.service.MypageService;
import kr.co.homeplus.shipping.utils.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/shipManage")
@Api(tags = "어드민 - 택배배송관리")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class ShipManageController {
  private final ShipManageService shipManageService;
  private final MypageService mypageService;

  @ApiOperation(value = "택배배송관리 조회")
  @PostMapping("/getShipManageList")
  public ResponseObject<List<ShipManageListGetDto>> getShipManageList(@RequestBody @Valid ShipManageListSetDto shipManageListSetDto) {
    return ResourceConverter.toResponseObject(shipManageService.getShipManageList(shipManageListSetDto));
  }

  @ApiOperation(value = "주문확인")
  @PostMapping("/setConfirmOrder")
  public ResponseObject<String> setConfirmOrder(@RequestBody @Valid ShipManageConfirmOrderSetDto shipManageConfirmOrderSetDto) {
      try {
        return shipManageService.setConfirmOrder(shipManageConfirmOrderSetDto, ADMIN);
      } catch (Exception e) {
        return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3001.getResponseMessage());
      }
  }

  @ApiOperation(value = "발송지연안내")
  @PostMapping("/setNotiDelay")
  public ResponseObject<String> setNotiDelay(@RequestBody @Valid ShipManageNotiDelaySetDto shipManageNotiDelaySetDto) {
      try {
        return shipManageService.setNotiDelay(shipManageNotiDelaySetDto, ADMIN);
      } catch (Exception e) {
        return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3002.getResponseMessage());
      }
  }

  @ApiOperation(value = "일괄발송처리")
  @PostMapping("/setShipAllExcel")
  public ResponseObject<Map<String, Object>> setShipAllExcel(@RequestBody @Valid List<ShipManageShipAllExcelSetDto> shipManageShipAllExcelSetDtoList) {
    try {
      return shipManageService.setShipAllExcel(shipManageShipAllExcelSetDtoList, ADMIN);
    } catch (Exception e) {
      return ResourceConverter.toResponseObject(new HashMap<>(), HttpStatus.OK, "FAIL", ExceptionCode.ERROR_CODE_3003.getResponseMessage());
    }
  }

  @ApiOperation(value = "선택발송처리")
  @PostMapping("/setShipAll")
  public ResponseObject<String> setShipAll(@RequestBody @Valid ShipManageShipAllSetDto shipManageShipAllSetDto){
      try {
        return shipManageService.setShipAll(shipManageShipAllSetDto, ADMIN);
      } catch (Exception e) {
        return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3004.getResponseMessage());
      }
  }

  @ApiOperation(value = "선택발송처리(주문취소건가능)")
  @PostMapping("/setShipAllExClaim")
  public ResponseObject<String> setShipAllExClaim(@RequestBody @Valid ShipManageShipAllSetDto shipManageShipAllSetDto) {
    try {
      return shipManageService.setShipAllExClaim(shipManageShipAllSetDto, ADMIN);
    } catch (Exception e) {
      return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3004.getResponseMessage());
    }
  }

  @ApiOperation(value = "선택발송처리 팝업 리스트 조회")
  @PostMapping("/getShipAllPopList")
  public ResponseObject<List<ShipManageShipAllPopListGetDto>> getShipAllPopList(@RequestBody @Valid ShipManageShipAllPopListSetDto shipManageShipAllPopListSetDto) {
    return ResourceConverter.toResponseObject(shipManageService.getShipAllPopList(shipManageShipAllPopListSetDto));
  }

  @ApiOperation(value = "배송완료")
  @PostMapping("/setShipComplete")
  public ResponseObject<String> setShipComplete(@RequestBody @Valid ShipManageShipCompleteSetDto shipManageShipCompleteSetDto) {
    try {
      return shipManageService.setShipComplete(shipManageShipCompleteSetDto, ADMIN);
    } catch (Exception e) {
      return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3005.getResponseMessage());
    }
  }

  @ApiOperation(value = "배송완료(Bundle단위)")
  @PostMapping("/setShipCompleteByBundle")
  public ResponseObject<String> setShipCompleteByBundle(@RequestBody @Valid ShipManageShipCompleteByBundleSetDto shipManageShipCompleteByBundleSetDto) {
    try {
      ShipManageShipCompleteSetDto shipManageShipCompleteSetDto = new ShipManageShipCompleteSetDto();
      List<String> bundleNoList = new ArrayList<>();

      bundleNoList.add(shipManageShipCompleteByBundleSetDto.getBundleNo());

      shipManageShipCompleteSetDto.setBundleNoList(bundleNoList);
      shipManageShipCompleteSetDto.setChgId(shipManageShipCompleteByBundleSetDto.getChgId());

      return shipManageService.setShipComplete(shipManageShipCompleteSetDto, ADMIN);
    } catch (Exception e) {
      return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3005.getResponseMessage());
    }
  }

  @ApiOperation(value = "구매확정(Bundle단위)")
  @PostMapping("/setPurchaseCompleteByBundle")
  public ResponseObject<String> setPurchaseCompleteByBundle(@RequestBody @Valid ShipManageShipCompleteByBundleSetDto shipManageShipCompleteByBundleSetDto) {
    return shipManageService.setPurchaseCompleteByBundle(shipManageShipCompleteByBundleSetDto, ADMIN);
  }

  @ApiOperation(value = "배송완료 팝업 리스트 조회")
  @PostMapping("/getShipCompletePopList")
  public ResponseObject<List<ShipManageShipCompletePopListGetDto>> getShipCompletePopList(@RequestBody @Valid ShipManageShipCompleteSetDto shipManageShipCompleteSetDto) {
    return ResourceConverter.toResponseObject(shipManageService.getShipCompletePopList(shipManageShipCompleteSetDto, ADMIN));
  }

  @ApiOperation(value = "배송 히스토리 조회")
  @GetMapping("/getShipHistoryList")
  public ResponseObject<List<ShippingHistoryGetDto>> getShipHistoryList(
      @ApiParam(name = "dlvCd", value = "택배사코드", required = true, defaultValue = "") @RequestParam String dlvCd
      , @ApiParam(name = "invoiceNo", value = "송장번호", required = true, defaultValue = "") @RequestParam String invoiceNo) {
    return ResourceConverter.toResponseObject(mypageService.getInvoiceHistory(dlvCd, invoiceNo));
  }

  @ApiOperation(value = "점포정보 조회")
  @GetMapping("/getStoreInfo")
  public ResponseObject<ShipStoreInfoGetDto> getStoreInfo(
      @ApiParam(name = "storeId", value = "점포코드", required = true, defaultValue = "") @RequestParam String storeId) {
    return ResourceConverter.toResponseObject(shipManageService.getStoreInfo(storeId));
  }

  @ApiOperation(value = "점포명 조회")
  @GetMapping("/getStoreNm")
  public ResponseObject<String> getStoreNm(
      @ApiParam(name = "storeId", value = "점포코드", required = true, defaultValue = "") @RequestParam String storeId) {
    return ResourceConverter.toResponseObject(shipManageService.getStoreNm(storeId));
  }

  @ApiOperation(value = "배송정보출력 조회")
  @GetMapping("/getShipInfoPrintList")
  public ResponseObject<List<ShipInfoPrintGetDto>> getShipInfoPrintList(
      @ApiParam(name = "bundleNo", value = "배송번호", required = true, defaultValue = "") @RequestParam String bundleNo) {
    return ResourceConverter.toResponseObject(shipManageService.getShipInfoPrintList(bundleNo));
  }
}