package kr.co.homeplus.shipping.tracking.model.shipping;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.shipping.tracking.model.common.CommonResultSetDto;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class ShippingResultSetDto extends CommonResultSetDto {
    @ApiModelProperty(notes = "배송번호")
    private String shipNo;

    public ShippingResultSetDto(String shipNo, String success,
        String result, String detail) {
        super(success, result, detail);
        this.shipNo = shipNo;
    }

    public String getShipNo() {
        return shipNo;
    }

    public void setShipNo(String shipNo) {
        this.shipNo = shipNo;
    }
}
