package kr.co.homeplus.shipping.utils;

import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import org.springframework.http.HttpStatus;

/*
 응답 처리 유틸
 */
public class ResponseUtil {
  // Exception 처리용
  public static <T> ResponseObject<T> getResoponseObject(ExceptionCode code, T data) {
    return ResourceConverter.toResponseObject(data, HttpStatus.OK, code.getCode(), code.getResponseMessage());
  }

  public static ResponseObject<String> getResoponseObject(ExceptionCode code, String addMsg) {
    return ResourceConverter.toResponseObject("0", HttpStatus.OK, code.getCode(), code.getResponseMessage() + addMsg);
  }

  public static ResponseObject<String> getResoponseObjectFail(String failMsg) {
    return ResourceConverter.toResponseObject("0", HttpStatus.OK, "FAIL", failMsg);
  }

  public static ResponseObject<String> getResoponseObject(ExceptionCode code) {
    return getResoponseObject(code, "");
  }

  // 정상 종료 처리용
  public static <T> ResponseObject<T> getResoponseObject(T data) {
    return ResourceConverter.toResponseObject(data);
  }
}
