package kr.co.homeplus.shipping.external.model.market;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.shipping.external.model.store.StoreShipVaninfoSetDto;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@ApiModel(description = "배송 상태 변경")
public class MarketShipSetDto {
    @ApiModelProperty(notes = "마켓주문번호")
    private String marketOrderNo;

    @ApiModelProperty(notes = "마켓상품주문번호")
    private String marketOrderItemNo;

    @ApiModelProperty(notes = "마켓타입")
    private String marketType;
}
