package kr.co.homeplus.shipping.partner.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageCompleteSetDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageConfirmOrderSetDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageListSetDto;
import kr.co.homeplus.shipping.partner.service.TicketManageService;
import kr.co.homeplus.shipping.utils.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/partner/sell")
@Api(tags = "파트너 - 이티켓판매관리")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class TicketManageController {
  private final TicketManageService ticketManageService;

  @ApiOperation(value = "이티켓판매관리 조회")
  @PostMapping("/getTicketManageList")
  public ResponseObject<List<TicketManageListGetDto>> getTicketManageList(@RequestBody @Valid TicketManageListSetDto ticketManageListSetDto) {
    return ResourceConverter.toResponseObject(ticketManageService.getTicketManageList(ticketManageListSetDto));
  }

  @ApiOperation(value = "이티켓판매관리 카운트 리스트 조회")
  @GetMapping("/getTicketManageCntList")
  public ResponseObject<List<TicketManageListGetDto>> getTicketManageCntList(
        @ApiParam(name = "schType", value = "검색타입", required = true) @RequestParam String schType,
        @ApiParam(name = "partnerId", value = "파트너ID", required = true) @RequestParam String partnerId) {
    return ResourceConverter.toResponseObject(ticketManageService.getTicketManageCntList(schType, partnerId));
  }

  @ApiOperation(value = "이티켓판매관리 카운트 조회")
  @GetMapping("/getTicketManageCnt")
  public ResponseObject<TicketManageCntGetDto> getTicketManageCnt(@ApiParam(name = "partnerId", value = "파트너ID", required = true) @RequestParam String partnerId) {
    return ResourceConverter.toResponseObject(ticketManageService.getTicketManageCnt(partnerId));
  }

  @ApiOperation(value = "주문확인")
  @PostMapping("/setTicketConfirmOrder")
  public ResponseObject<String> setTicketConfirmOrder(@RequestBody @Valid TicketManageConfirmOrderSetDto ticketManageConfirmOrderSetDto) {
    try {
      return ticketManageService.setTicketConfirmOrder(ticketManageConfirmOrderSetDto);
    } catch (Exception e) {
      return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3020.getResponseMessage());
    }
  }

  @ApiOperation(value = "발급완료")
  @PostMapping("/setTicketComplete")
  public ResponseObject<String> setTicketComplete(@RequestBody @Valid TicketManageCompleteSetDto ticketManageCompleteSetDto) {
    try {
      return ticketManageService.setTicketComplete(ticketManageCompleteSetDto);
    } catch (Exception e) {
      return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3021.getResponseMessage());
    }
  }
}