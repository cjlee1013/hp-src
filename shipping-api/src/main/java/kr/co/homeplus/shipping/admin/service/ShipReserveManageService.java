package kr.co.homeplus.shipping.admin.service;

import java.util.List;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipReserveManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipReserveManageListSetDto;

/**
 * shipping service interface
 */
public interface ShipReserveManageService {
  List<ShipReserveManageListGetDto> getShipReserveManageList(ShipReserveManageListSetDto shipReserveManageListSetDto);
}

