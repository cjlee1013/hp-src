package kr.co.homeplus.shipping.external.model.store;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "수거 시작 상태 변경")
public class StorePickStartSetDto extends StoreClaimVaninfoSetDto {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "수거예정시간(yyyy-MM-dd HH:mm:ss)", required = true)
    private String pickStartDt;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "차량ID")
    private String vanId;

}
