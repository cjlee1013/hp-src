package kr.co.homeplus.shipping.common.model.message.info;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "메세지 발송 정보 - 발송지연안내")
public class MessageInfoShipDelayGetDto extends MessageInfoBaseDto{
    @ApiModelProperty(notes = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "상품명")
    private String itemNm;

    @ApiModelProperty(notes = "2차출고기한")
    private String delayShipDt;

    @ApiModelProperty(notes = "지연사유")
    private String delayShipCd;
}
