package kr.co.homeplus.shipping.mypage.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.mypage.model.order.ShippingInvoiceGetDto;
import kr.co.homeplus.shipping.mypage.model.order.ShippingHistoryGetDto;
import kr.co.homeplus.shipping.mypage.service.MypageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/mypage/order")
@Api(tags = "마이페이지 API")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class OrderController {
  private final MypageService mypageService;

  @ApiOperation(value = "배송완료 처리", response = ResponseObject.class, notes = "필수값 : 배송번호")
  @RequestMapping(value = "/manualShipComplete", method = {RequestMethod.GET})
  public ResponseObject<String> manualShipComplete(String shipNo) {
    int results = mypageService.manualShipComplete(shipNo);

    if (results > 0 ) {
      return ResourceConverter.toResponseObject("SUCCESS");
    } else {
      switch (results) {
        case -1:
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.ERROR_CODE_2001.getResponseCode(), ExceptionCode.ERROR_CODE_2001.getResponseMessage());
        case -2:
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.ERROR_CODE_2002.getResponseCode(), ExceptionCode.ERROR_CODE_2002.getResponseMessage());
        case -3:
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.ERROR_CODE_2003.getResponseCode(), ExceptionCode.ERROR_CODE_2003.getResponseMessage());
        default :
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
      }
    }
  }

  @ApiOperation(value = "구매확정 처리", response = ResponseObject.class, notes = "필수값 : 배송번호")
  @RequestMapping(value = "/manualPurchaseComplete", method = {RequestMethod.GET})
  public ResponseObject<String> manualPurchaseComplete(String shipNo) {
    int results = mypageService.manualPurchaseComplete(shipNo);

    if (results > 0 ) {
      return ResourceConverter.toResponseObject("SUCCESS");
    } else {
      switch (results) {
        case -1:
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.ERROR_CODE_2001.getResponseCode(), ExceptionCode.ERROR_CODE_2001.getResponseMessage());
        case -2:
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.ERROR_CODE_2002.getResponseCode(), ExceptionCode.ERROR_CODE_2002.getResponseMessage());
        case -3:
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.ERROR_CODE_2003.getResponseCode(), ExceptionCode.ERROR_CODE_2003.getResponseMessage());
        default :
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
      }
    }
  }

  @ApiOperation(value = "배송조회", response = ResponseObject.class, notes = "필수값 : 배송번호")
  @RequestMapping(value = "/getShippingHistory", method = {RequestMethod.GET})
  public ResponseObject<ShippingInvoiceGetDto> getShippingHistory(String shipNo) {
    ShippingInvoiceGetDto results = mypageService.getShippingHistory(shipNo);

    if (results == null) {
      return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_2001.getResponseCode(), ExceptionCode.ERROR_CODE_2001.getResponseMessage());
    }
    return ResourceConverter.toResponseObject(results);
  }

  @ApiOperation(value = "수거배송조회", response = ResponseObject.class, notes = "필수값 : 클레임 반품수거 번호")
  @RequestMapping(value = "/getPickHistory", method = {RequestMethod.GET})
  public ResponseObject<ShippingInvoiceGetDto> getPickHistory(String claimPickShippingNo) {
    ShippingInvoiceGetDto results = mypageService.getPickHistory(claimPickShippingNo);

    if (results == null) {
      return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_2101.getResponseCode(), ExceptionCode.ERROR_CODE_2101.getResponseMessage());
    }
    return ResourceConverter.toResponseObject(results);
  }

  @ApiOperation(value = "교환배송조회", response = ResponseObject.class, notes = "필수값 : 클레임 교환배송 번호")
  @RequestMapping(value = "/getExchHistory", method = {RequestMethod.GET})
  public ResponseObject<ShippingInvoiceGetDto> getExchHistory(String claimExchShippingNo) {
    ShippingInvoiceGetDto results = mypageService.getExchHistory(claimExchShippingNo);

    if (results == null) {
      return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_2201.getResponseCode(), ExceptionCode.ERROR_CODE_2201.getResponseMessage());
    }
    return ResourceConverter.toResponseObject(results);
  }

  @ApiOperation(value = "송장번호배송조회", response = ResponseObject.class, notes = "필수값 : 택배사코드, 송장번호")
  @RequestMapping(value = "/getInvoiceHistory", method = {RequestMethod.GET})
  public ResponseObject<List<ShippingHistoryGetDto>> getInvoiceHistory(String dlvCd, String invoiceNo) {
    List<ShippingHistoryGetDto> results = mypageService.getInvoiceHistory(dlvCd,invoiceNo);

    if (results == null) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    } else if (results.size() == 0) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.ERROR_CODE_2301.getResponseCode(), ExceptionCode.ERROR_CODE_2301.getResponseMessage());
    }
    return ResourceConverter.toResponseObject(results);
  }

  @ApiOperation(value = "멀티배송조회", response = ResponseObject.class, notes = "필수값 : 다중주문아이템번호")
  @RequestMapping(value = "/getMultiShippingHistory", method = {RequestMethod.GET})
  public ResponseObject<ShippingInvoiceGetDto> getMultiShippingHistory(String multiOrderItemNo) {
    ShippingInvoiceGetDto results = mypageService.getMultiShippingHistory(multiOrderItemNo);

    if (results == null) {
      return ResourceConverter.toResponseObject(null, HttpStatus.OK, ExceptionCode.ERROR_CODE_2401.getResponseCode(), ExceptionCode.ERROR_CODE_2401.getResponseMessage());
    }
    return ResourceConverter.toResponseObject(results);
  }

  @ApiOperation(value = "멀티배송 배송완료 처리", response = ResponseObject.class, notes = "필수값 : 다중주문아이템번호")
  @RequestMapping(value = "/manualMultiShipComplete", method = {RequestMethod.GET})
  public ResponseObject<String> manualMultiShipComplete(String multiOrderItemNo) {
    int results = mypageService.manualMultiShipComplete(multiOrderItemNo);

    if (results > 0 ) {
      return ResourceConverter.toResponseObject("SUCCESS");
    } else {
      switch (results) {
        case -1:
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.ERROR_CODE_2001.getResponseCode(), ExceptionCode.ERROR_CODE_2001.getResponseMessage());
        case -2:
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.ERROR_CODE_2002.getResponseCode(), ExceptionCode.ERROR_CODE_2002.getResponseMessage());
        case -3:
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.ERROR_CODE_2003.getResponseCode(), ExceptionCode.ERROR_CODE_2003.getResponseMessage());
        default :
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
      }
    }
  }

  @ApiOperation(value = "멀티배송 구매확정 처리", response = ResponseObject.class, notes = "필수값 : 다중주문아이템번호")
  @RequestMapping(value = "/manualMultiPurchaseComplete", method = {RequestMethod.GET})
  public ResponseObject<String> manualMultiPurchaseComplete(String multiOrderItemNo) {
    int results = mypageService.manualMultiPurchaseComplete(multiOrderItemNo);

    if (results > 0 ) {
      return ResourceConverter.toResponseObject("SUCCESS");
    } else {
      switch (results) {
        case -1:
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.ERROR_CODE_2001.getResponseCode(), ExceptionCode.ERROR_CODE_2001.getResponseMessage());
        case -2:
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.ERROR_CODE_2002.getResponseCode(), ExceptionCode.ERROR_CODE_2002.getResponseMessage());
        case -3:
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.ERROR_CODE_2003.getResponseCode(), ExceptionCode.ERROR_CODE_2003.getResponseMessage());
        default :
          return ResourceConverter.toResponseObject("FALSE", HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
      }
    }
  }
}
