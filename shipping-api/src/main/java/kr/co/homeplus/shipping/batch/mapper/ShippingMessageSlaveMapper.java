package kr.co.homeplus.shipping.batch.mapper;

import java.util.List;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface ShippingMessageSlaveMapper {
  List<String> selectBundleReceive5day();
}
