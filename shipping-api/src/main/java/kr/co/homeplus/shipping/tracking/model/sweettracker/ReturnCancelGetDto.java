package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.beans.ConstructorProperties;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "스윗트래커 추적요청 응답값")
public class ReturnCancelGetDto {
    @ApiModelProperty(notes = "결과", dataType = "List")
    private List<ReturnCancelItemGetDto> data;

    public List<ReturnCancelItemGetDto> getData() {
        return data;
    }

    @ConstructorProperties({"data"})
    public ReturnCancelGetDto(
        List<ReturnCancelItemGetDto> data) {
        this.data = data;
    }
}
