package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;
import kr.co.homeplus.shipping.common.model.history.BundleHistoryInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.ClaimBundleHistoryInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.CommonHistoryInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.ExchProcessInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.MultiShippingHistoryInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.MultiShippingProcessInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.PickHistoryInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.ExchHistoryInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.PickProcessInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.ShippingHistoryInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.ShippingProcessInsertSetDto;
import kr.co.homeplus.shipping.enums.SweettrackerCode;
import kr.co.homeplus.shipping.tracking.model.common.DlvCdMappingGetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingCallbackSetDto;
import lombok.Data;
import org.springframework.security.core.parameters.P;

@Data
@ApiModel(description = "배송추적 콜백 스윗트래커 응답값")
public class TraceCallbackSetDto {

    @ApiModelProperty(notes = "인증키")
    private String secret_value;

    @ApiModelProperty(notes = "식별값")
    private String fid;

    @ApiModelProperty(notes = "택배사")
    private String courier_code;

    @ApiModelProperty(notes = "운송장번호")
    private String invoice_no;

    @ApiModelProperty(notes = "배송단계")
    private String level;

    @ApiModelProperty(notes = "택배사 처리시간")
    private String time_trans;

    @ApiModelProperty(notes = "스윗트래커 등록시간")
    private String time_sweet;

    @ApiModelProperty(notes = "택배 위치")
    private String where;

    @ApiModelProperty(notes = "사업소 기반 전화번호")
    private String telno_office;

    @ApiModelProperty(notes = "배송기사 전화번호")
    private String telno_man;

    @ApiModelProperty(notes = "배송상세 정보")
    private String details;

    @ApiModelProperty(notes = "수취인 주소")
    private String recv_addr;

    @ApiModelProperty(notes = "수취인 이름")
    private String recv_name;

    @ApiModelProperty(notes = "발신인 이름")
    private String send_name;

    @ApiModelProperty(notes = "배송기사 이름")
    private String man;

    @ApiModelProperty(notes = "배송예정 시간")
    private String estmate;

    public ShippingHistoryInsertSetDto toShippingHistoryInsertSetDto(Map<String, DlvCdMappingGetDto> mapping) {
        DlvCdMappingGetDto dlvCdMappingGetDto = mapping.get(courier_code);
        if (dlvCdMappingGetDto == null) {
            return null;
        }
        String dlvCd = dlvCdMappingGetDto.getDlvCd();
        return new ShippingHistoryInsertSetDto(dlvCd, invoice_no, level, time_trans, time_sweet, where, telno_office, telno_man, details, recv_addr, recv_name, send_name, man, estmate,"batch",fid);
    }
    public BundleHistoryInsertSetDto toBundleHistoryInsertSetDto(Map<String, DlvCdMappingGetDto> mapping) {
        DlvCdMappingGetDto dlvCdMappingGetDto = mapping.get(courier_code);
        if (dlvCdMappingGetDto == null) {
            return null;
        }
        String dlvCd = dlvCdMappingGetDto.getDlvCd();
        return new BundleHistoryInsertSetDto(dlvCd, invoice_no, level, time_trans, time_sweet, where, telno_office, telno_man, details, recv_addr, recv_name, send_name, man, estmate,"batch",fid);
    }
    public ShippingCallbackSetDto toShippingCallbackSetDto(){
        if (compareCode(SweettrackerCode.LEVEL_CODE_6)) {
            return new ShippingCallbackSetDto(fid,invoice_no,"D4",time_trans);
        } else {
            return null;
        }
    }

    public ShippingProcessInsertSetDto toShippingProcessInsertSetDto() {
        return new ShippingProcessInsertSetDto(getLevelDescription(),details,"","batch","batch",fid);
    }

    public PickHistoryInsertSetDto toPickHistoryInsertSetDto(Map<String, DlvCdMappingGetDto> mapping) {
        DlvCdMappingGetDto dlvCdMappingGetDto = mapping.get(courier_code);
        if (dlvCdMappingGetDto == null) {
            return null;
        }
        String dlvCd = dlvCdMappingGetDto.getDlvCd();
        return new PickHistoryInsertSetDto(dlvCd, invoice_no, level, time_trans, time_sweet, where, telno_office, telno_man, details, recv_addr, recv_name, send_name, man, estmate,"batch",fid);
    }
    public PickCallbackSetDto toPickCallbackSetDto(){
        if (compareCode(SweettrackerCode.LEVEL_CODE_6)) {
            return new PickCallbackSetDto(fid,invoice_no,"P3",time_trans);
        } else {
            return null;
        }

    }
    public PickProcessInsertSetDto toPickProcessInsertSetDto() {
        return new PickProcessInsertSetDto(getLevelDescription(),details,"","batch","batch",fid);
    }

    public ExchHistoryInsertSetDto toExchHistoryInsertSetDto(Map<String, DlvCdMappingGetDto> mapping) {
        DlvCdMappingGetDto dlvCdMappingGetDto = mapping.get(courier_code);
        if (dlvCdMappingGetDto == null) {
            return null;
        }
        String dlvCd = dlvCdMappingGetDto.getDlvCd();
        return new ExchHistoryInsertSetDto(dlvCd, invoice_no, level, time_trans, time_sweet, where, telno_office, telno_man, details, recv_addr, recv_name, send_name, man, estmate,"batch",fid);
    }
    public ExchCallbackSetDto toExchCallbackSetDto(){
        if (compareCode(SweettrackerCode.LEVEL_CODE_6)) {
            return new ExchCallbackSetDto(fid,invoice_no,"D3",time_trans);
        } else {
            return null;
        }
    }
    public ExchProcessInsertSetDto toExchProcessInsertSetDto() {
        return new ExchProcessInsertSetDto(getLevelDescription(),details,"","batch","batch",fid);
    }

    public ClaimBundleHistoryInsertSetDto toClaimBundleHistoryInsertSetDto(Map<String, DlvCdMappingGetDto> mapping) {
        DlvCdMappingGetDto dlvCdMappingGetDto = mapping.get(courier_code);
        if (dlvCdMappingGetDto == null) {
            return null;
        }
        String dlvCd = dlvCdMappingGetDto.getDlvCd();
        return new ClaimBundleHistoryInsertSetDto(dlvCd, invoice_no, level, time_trans, time_sweet, where, telno_office, telno_man, details, recv_addr, recv_name, send_name, man, estmate,"batch",fid);
    }


    public MultiShippingHistoryInsertSetDto toMultiHistoryInsertSetDto(Map<String, DlvCdMappingGetDto> mapping) {
        DlvCdMappingGetDto dlvCdMappingGetDto = mapping.get(courier_code);
        if (dlvCdMappingGetDto == null) {
            return null;
        }
        String dlvCd = dlvCdMappingGetDto.getDlvCd();
        return new MultiShippingHistoryInsertSetDto(dlvCd, invoice_no, level, time_trans, time_sweet, where, telno_office, telno_man, details, recv_addr, recv_name, send_name, man, estmate,"batch",fid);
    }
    public MultiCallbackSetDto toMultiCallbackSetDto(){
        if (compareCode(SweettrackerCode.LEVEL_CODE_6)) {
            return new MultiCallbackSetDto(fid,invoice_no,"D4",time_trans);
        } else {
            return null;
        }
    }
    public MultiShippingProcessInsertSetDto toMultiProcessInsertSetDto() {
        return new MultiShippingProcessInsertSetDto(getLevelDescription(),details,"","batch","batch",fid);
    }

    public CommonHistoryInsertSetDto toCommonHistoryInsertSetDto(Map<String, DlvCdMappingGetDto> mapping) {
        DlvCdMappingGetDto dlvCdMappingGetDto = mapping.get(courier_code);
        if (dlvCdMappingGetDto == null) {
            return null;
        }
        String dlvCd = dlvCdMappingGetDto.getDlvCd();
        return new CommonHistoryInsertSetDto(dlvCd, invoice_no, level, time_trans, time_sweet, where, telno_office, telno_man, details, recv_addr, recv_name, send_name, man, estmate,"batch");
    }

    private String getLevelDescription() {
        if (level == null) {
            return null;
        }
        if (level.equals(SweettrackerCode.LEVEL_CODE_1.getCode())) {
            return SweettrackerCode.LEVEL_CODE_1.getDescription();
        } else if (level.equals(SweettrackerCode.LEVEL_CODE_2.getCode())) {
            return SweettrackerCode.LEVEL_CODE_2.getDescription();
        } else if (level.equals(SweettrackerCode.LEVEL_CODE_3.getCode())) {
            return SweettrackerCode.LEVEL_CODE_3.getDescription();
        } else if (level.equals(SweettrackerCode.LEVEL_CODE_4.getCode())) {
            return SweettrackerCode.LEVEL_CODE_4.getDescription();
        } else if (level.equals(SweettrackerCode.LEVEL_CODE_5.getCode())) {
            return SweettrackerCode.LEVEL_CODE_5.getDescription();
        } else if (level.equals(SweettrackerCode.LEVEL_CODE_6.getCode())) {
            return SweettrackerCode.LEVEL_CODE_6.getDescription();
        } else {
            return SweettrackerCode.LEVEL_CODE_99.getDescription();
        }
    }

    private boolean compareCode (SweettrackerCode code) {
        if ("99".equals(level)) {
            if (code.getDescription().equals(details)) {
                return true;
            }
        } else {
            if (code.getCode().equals(level)) {
                return true;
            }
        }
        return false;
    }
}
