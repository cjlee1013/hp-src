package kr.co.homeplus.shipping.tracking.mapper;

import java.util.List;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.common.DlvCdMappingGetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickAddressGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingTrackingGetDto;

@SlaveConnection
public interface TrackingSlaveMapper {
  List<ShippingTrackingGetDto> getShippingInvoice(List<ShippingRequestSetDto> shippingTrackingGetDto);
  List<PickTrackingGetDto> getPickInvoice(List<PickRequestSetDto> pickTrackingGetDto);
  List<ExchTrackingGetDto> getExchInvoice(List<ExchRequestSetDto> exchTrackingGetDto);
  List<PickAddressGetDto> getPickAddress(List<PickRequestSetDto> pickRequestSetDto);
  List<MultiTrackingGetDto> getMultiInvoice(List<MultiRequestSetDto> shippingTrackingGetDto);
  List<BsdTrackingGetDto> getBsdInvoice(List<BsdRequestSetDto> bsdRequestSetDto);
  List<DlvCdMappingGetDto> getDlvCdMapping();
  List<String> getShippingNo(String shipNo);
}