package kr.co.homeplus.shipping.common.model.message.info;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "메세지 발송 정보 - 수취확인 요청")
public class MessageInfoConfirmReceiveGetDto extends MessageInfoBaseDto{
    @ApiModelProperty(notes = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "상품명")
    private String itemNm;
}
