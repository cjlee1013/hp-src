package kr.co.homeplus.shipping.common.model.message;

import io.swagger.annotations.ApiModelProperty;
import java.util.Map;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class MessageSendBodyArgument {

    @ApiModelProperty(value = "회원번호 또는 캠페인 별 유저 고유 식별 키")
    private String identifier;

    @ApiModelProperty(value = "수신주소정보(sms일 경우 전화번호, 메일일 경우 이메일주소 등)")
    @Size(max = 25)
    private String toToken;

    @ApiModelProperty("개인 매핑 데이터")
    private Map<String, String> mappingData;

    public MessageSendBodyArgument (String identifier, String toToken, Map<String,String> mappingData) {
        this.identifier = identifier;
        this.toToken = toToken;
        this.mappingData = mappingData;
    }
}
