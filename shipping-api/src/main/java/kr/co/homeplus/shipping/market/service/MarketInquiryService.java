package kr.co.homeplus.shipping.market.service;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.shipping.market.mapper.MarketInquirySlaveMapper;
import kr.co.homeplus.shipping.market.model.MarketInquiryDetailGetDto;
import kr.co.homeplus.shipping.market.model.MarketInquiryDetailSetDto;
import kr.co.homeplus.shipping.market.model.MarketInquiryListGetDto;
import kr.co.homeplus.shipping.market.model.MarketInquiryListSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MarketInquiryService {
  private final MarketInquirySlaveMapper marketInquirySlaveMapper;
  private final ResourceClient resourceClient;

  /**
   *  제휴사 고객문의 리스트 조회
   */
  public List<MarketInquiryListGetDto> getMarketInquiryList(
      MarketInquiryListSetDto marketInquiryListSetDto) {
    log.info("getMarketInquiryList param : {}", marketInquiryListSetDto);

    List<MarketInquiryListGetDto> marketInquiryList = marketInquirySlaveMapper.selectMarketInquiryList(marketInquiryListSetDto);

    for(MarketInquiryListGetDto dto : marketInquiryList) {
      dto.setInquirerName(PrivacyMaskingUtils.maskingUserName(dto.getInquirerName()));
      dto.setIsresponseNm(PrivacyMaskingUtils.maskingUserName(dto.getIsresponseNm()));
      dto.setInquirerPhone(PrivacyMaskingUtils.maskingPhone(dto.getInquirerPhone()));
    }

    return marketInquiryList;
  }

  /**
   *  제휴사 고객문의 상세 조회
   */
  public MarketInquiryDetailGetDto getMarketInquiryDetail(MarketInquiryDetailSetDto marketInquiryDetailSetDto) {
    log.info("getMarketInquiryDetail param : {}", marketInquiryDetailSetDto);

    MarketInquiryDetailGetDto marketInquiryDetail = marketInquirySlaveMapper.selectMarketInquiryDetail(marketInquiryDetailSetDto);

    return marketInquiryDetail;
  }
}
