package kr.co.homeplus.shipping.tracking.model.pick;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PickAddressGetDto {
  @ApiModelProperty(notes = "주문번호")
  private String claimPickShippingNo;

  @ApiModelProperty(notes = "택배사 코드")
  private String pickDlvCd;

  @ApiModelProperty(notes = "운송장 번호")
  private String orgInvoiceNo;

  @ApiModelProperty(notes = "주문 일자 / 등록 일자 (yyyyMMdd)")
  private String regDt;

  @ApiModelProperty(notes = "신용코드 (우체국의 경우 점포코드)")
  private String creditCd;
/* 필수값 아님
  @ApiModelProperty(notes = "서브 신용코드")
  private String creditCdSub;
*/
  @ApiModelProperty(notes = "송화인 명")
  private String pickReceiverNm;

  @ApiModelProperty(notes = "송화인 우편번호")
  private String pickZipcode;

  @ApiModelProperty(notes = "송화인 주소1")
  private String pickBaseAddr;

  @ApiModelProperty(notes = "송화인 주소2")
  private String pickDetailAddr;

  @ApiModelProperty(notes = "송화인 전화번호")
  private String pickMobileNo;

  @ApiModelProperty(notes = "송화인 전화번호2")
  private String pickPhone;

  @ApiModelProperty(notes = "수화인 명")
  private String businessNm;

  @ApiModelProperty(notes = "수화인 우편번호")
  private String returnZipcode;

  @ApiModelProperty(notes = "수화인 주소1")
  private String returnAddr1;

  @ApiModelProperty(notes = "수화인 주소2")
  private String returnAddr2;

  @ApiModelProperty(notes = "수화인 전화번호1")
  private String phone1;

  @ApiModelProperty(notes = "수화인 전화번호2")
  private String phone2;

  @ApiModelProperty(notes = "화물내역 상품명 (간단하게)")
  private String itemName;

  @ApiModelProperty(notes = "배송 메모")
  private String pickMemo;

  /* 우체국용 파라미터 미사용
  @ApiModelProperty(notes = "화물 크기 (1.극소 2.소 3. 중 4.대 5.극대)")
  private String dsoGbn;

  @ApiModelProperty(notes = "화물무게(1:초소형, 2:2Kg이하, 3:5Kg이하, 4:10Kg이하, 5:20Kg이하, 6:30Kg이하)")
  private String dsoWght;

  @ApiModelProperty(notes = "점포코드(우체국) / 협력사코드(CJ대한통운)")
  private String comDivCde;
   */
  public String getClaimPickShippingNo() { return claimPickShippingNo; }

  public String getPickDlvCd() {
    return pickDlvCd;
  }

  public String getOrgInvoiceNo() {
    return orgInvoiceNo;
  }

  public String getRegDt() {
    return regDt;
  }

  public String getCreditCd() {
    return creditCd;
  }
/* 필수값 아님
  public String getCreditCdSub() {
    return creditCdSub;
  }
*/
  public String getPickReceiverNm() {
    return pickReceiverNm;
  }

  public String getPickZipcode() {
    return pickZipcode;
  }

  public String getPickBaseAddr() {
    return pickBaseAddr;
  }

  public String getPickDetailAddr() {
    return pickDetailAddr;
  }

  public String getPickMobileNo() {
    return pickMobileNo;
  }

  public String getPickPhone() {
    return pickPhone;
  }

  public String getBusinessNm() {
    return businessNm;
  }

  public String getReturnZipcode() {
    return returnZipcode;
  }

  public String getReturnAddr1() {
    return returnAddr1;
  }

  public String getReturnAddr2() {
    return returnAddr2;
  }

  public String getPhone1() {
    return phone1;
  }

  public String getPhone2() {
    return phone2;
  }

  public String getItemName() {
    return itemName;
  }

  public String getPickMemo() {
    return pickMemo;
  }

  /* 우체국용 파라미터 미사용
  public String getDsoGbn() {
    return dsoGbn;
  }

  public String getDsoWght() {
    return dsoWght;
  }

  public String getComDivCde() {
    return comDivCde;
  }
   */
}
