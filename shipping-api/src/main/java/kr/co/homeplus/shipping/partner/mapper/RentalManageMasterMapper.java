package kr.co.homeplus.shipping.partner.mapper;

import kr.co.homeplus.shipping.core.db.annotation.MasterConnection;
import org.apache.ibatis.annotations.Param;

@MasterConnection
public interface RentalManageMasterMapper {
  int updateCounselReservation(@Param("shipNo") String shipNo, @Param("chgId") String chgId);
  int updateCounselComplete(@Param("shipNo") String shipNo, @Param("chgId") String chgId);
}