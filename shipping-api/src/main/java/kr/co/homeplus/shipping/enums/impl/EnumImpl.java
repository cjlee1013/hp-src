package kr.co.homeplus.shipping.enums.impl;

public interface EnumImpl {
    String getResponseCode();
    String getResponseMessage();
}