package kr.co.homeplus.shipping.external.model.store;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "옴니연동 배송 공통 반환값")
public class StoreShipResultGetDto {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "구매점포번호")
    private String purchaseStoreInfoNo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "성공여부")
    private boolean success;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "결과값")
    private String detail;

    public StoreShipResultGetDto(String purchaseStoreInfoNo, boolean success) {
        this.purchaseStoreInfoNo = purchaseStoreInfoNo;
        this.success = success;
    }

    public StoreShipResultGetDto(String purchaseStoreInfoNo, boolean success,String detail) {
        this.purchaseStoreInfoNo = purchaseStoreInfoNo;
        this.success = success;
        this.detail = detail;
    }
}
