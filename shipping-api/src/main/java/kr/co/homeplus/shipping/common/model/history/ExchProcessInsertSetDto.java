package kr.co.homeplus.shipping.common.model.history;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송 히스토리 입력")
public class ExchProcessInsertSetDto extends ProcessHistoryInsertSetDto{
    @ApiModelProperty(notes = "등록자")
    private String claimExchShippingNo;

    public ExchProcessInsertSetDto(String historyReason,
        String historyDetailDesc, String historyDetailBefore, String regChannel, String regId, String claimExchShippingNo) {
        super(historyReason,historyDetailDesc,historyDetailBefore,regChannel,regId);
        this.claimExchShippingNo = claimExchShippingNo;
    }
}
