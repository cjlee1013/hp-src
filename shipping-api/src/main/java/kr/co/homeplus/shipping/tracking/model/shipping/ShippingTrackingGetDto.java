package kr.co.homeplus.shipping.tracking.model.shipping;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import kr.co.homeplus.shipping.tracking.model.common.CommonTrackingGetDto;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class ShippingTrackingGetDto implements CommonTrackingGetDto {
    @ApiModelProperty(notes = "배송번호")
    private String shipNo;

    @ApiModelProperty(notes = "배송번호")
    private String shipNos;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    public String[] getShipNos() { return shipNos.split(","); }

    @Override
    public String getNo() { return shipNo; }

    @Override
    public String getInvoiceNo() { return invoiceNo; }

    @Override
    public String getDlvCd() {
        return dlvCd;
    }
}
