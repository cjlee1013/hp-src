package kr.co.homeplus.shipping.tracking.model.pick;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.shipping.tracking.model.common.CommonTrackingGetDto;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class PickTrackingGetDto implements CommonTrackingGetDto {
    @ApiModelProperty(notes = "클레임번들번호")
    private String claimPickShippingNo;

    @ApiModelProperty(notes = "배송번호")
    private String claimPickShippingNos;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    public String[] getClaimPickShippingNos() { return claimPickShippingNos.split(","); }

    @Override
    public String getNo() {
        return claimPickShippingNo;
    }

    @Override
    public String getInvoiceNo() {
        return invoiceNo;
    }

    @Override
    public String getDlvCd() {
        return dlvCd;
    }
}
