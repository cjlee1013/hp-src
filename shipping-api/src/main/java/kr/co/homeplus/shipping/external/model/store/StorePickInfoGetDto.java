package kr.co.homeplus.shipping.external.model.store;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "옴니연동 수거상태값")
public class StorePickInfoGetDto {
    @ApiModelProperty(notes = "클레임수거번호")
    private String claimPickShippingNo;

    @ApiModelProperty(notes = "클레임번들번호")
    private String claimBundleNo;

    @ApiModelProperty(notes = "수거상태")
    private String pickStatus;

    public String getClaimPickShippingNo() { return claimPickShippingNo; }

    public String getClaimBundleNo() { return claimBundleNo; }

    public String getPickStatus() {
        return pickStatus;
    }
}
