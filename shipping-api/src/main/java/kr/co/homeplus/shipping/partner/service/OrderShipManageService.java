package kr.co.homeplus.shipping.partner.service;

import java.util.List;
import kr.co.homeplus.shipping.partner.model.sell.OrderDetailGetDto;
import kr.co.homeplus.shipping.partner.model.sell.orderShipManage.OrderShipManageCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.orderShipManage.OrderShipManageListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.orderShipManage.OrderShipManageListSetDto;

public interface OrderShipManageService {
  List<OrderShipManageListGetDto> getOrderShipManageList(OrderShipManageListSetDto orderShipManageListSetDto);
  List<OrderShipManageListGetDto> getOrderShipManageCntList(String schType, String partnerId);
  OrderShipManageCntGetDto getOrderShipManageCnt(String partnerId);
  OrderDetailGetDto getOrderDetailPop(String bundleNo, String partnerId);
  OrderShipManageCntGetDto getOrderShipManageMainCnt(String partnerId);
}

