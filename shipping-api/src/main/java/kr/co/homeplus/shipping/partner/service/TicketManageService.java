package kr.co.homeplus.shipping.partner.service;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageCompleteSetDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageConfirmOrderSetDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageListSetDto;

public interface TicketManageService {
  List<TicketManageListGetDto> getTicketManageList(TicketManageListSetDto ticketManageListSetDto);
  List<TicketManageListGetDto> getTicketManageCntList(String schType, String partnerId);
  TicketManageCntGetDto getTicketManageCnt(String partnerId);
  ResponseObject<String> setTicketConfirmOrder(TicketManageConfirmOrderSetDto ticketManageConfirmOrderSetDto);
  ResponseObject<String> setTicketComplete(TicketManageCompleteSetDto ticketManageCompleteSetDto);
}