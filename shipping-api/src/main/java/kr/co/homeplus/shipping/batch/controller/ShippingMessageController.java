package kr.co.homeplus.shipping.batch.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.batch.service.ShippingMessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/batch/shippingMessage")
@Api(tags = "배치 - 배송메세지")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class ShippingMessageController {
  private final ShippingMessageService ShippingMessageService;

  @ApiOperation(value = "수취확인 요청 알림톡")
  @PostMapping("/sendComfirmReceiveTalk")
  public ResponseObject<String> sendComfirmReceiveTalk() {
      return ShippingMessageService.sendComfirmReceiveTalk();
  }

}
