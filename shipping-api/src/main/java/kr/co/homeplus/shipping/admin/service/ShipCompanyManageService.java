package kr.co.homeplus.shipping.admin.service;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.admin.model.shipManage.ReturnDlvCompanyListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipLinkCompanyCodeListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipLinkCompanySetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipLinkCompanySetListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipCompanyManageInfoSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipCompanyManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipCompanyManageListSetDto;

public interface ShipCompanyManageService {
  List<ShipCompanyManageListGetDto> getShipCompanyManageList(ShipCompanyManageListSetDto shipCompanyManageListSetDto);
  ResponseObject<String> setShipCompanyInfo(ShipCompanyManageInfoSetDto shipCompanyManageInfoSetDto);
  List<ShipLinkCompanySetListGetDto> getShipLinkCompanySetList();
  ResponseObject<String> setShipLinkCompanySet(ShipLinkCompanySetDto shipLinkCompanySetDto);
  List<ShipLinkCompanyCodeListGetDto> getLinkCompanyCodeList(String company);
  List<ReturnDlvCompanyListGetDto> getReturnDlvCompanyList();
}

