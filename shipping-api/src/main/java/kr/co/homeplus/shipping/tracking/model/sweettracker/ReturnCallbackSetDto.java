package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "배송추적 콜백 스윗트래커 응답값")
public class ReturnCallbackSetDto {
    List<ReturnCallbackItemSetDto> result_data;

    public List<ReturnCallbackItemSetDto> getResult_data() {
        return result_data;
    }
}
