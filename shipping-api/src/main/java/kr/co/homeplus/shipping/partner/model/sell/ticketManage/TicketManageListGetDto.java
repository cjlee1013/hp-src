package kr.co.homeplus.shipping.partner.model.sell.ticketManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.shipping.partner.model.sell.orderShipManage.OrderShipManageListGetDto;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 이티켓판매관리 리스트 응답 DTO")
public class TicketManageListGetDto extends OrderShipManageListGetDto {
  @ApiModelProperty(notes = "주문티켓번호")
  private String orderTicketNo;

  @ApiModelProperty(notes = "연동유형")
  private String linkType;

  @ApiModelProperty(notes = "발급채널")
  private String issueChannel;

  @ApiModelProperty(notes = "티켓상태코드")
  private String ticketStatus;

  @ApiModelProperty(notes = "티켓상태")
  private String ticketStatusNm;

  @ApiModelProperty(notes = "발급상태코드")
  private String issueStatus;

  @ApiModelProperty(notes = "발급상태")
  private String issueStatusNm;

  @ApiModelProperty(notes = "발급일")
  private String issueDt;

  @ApiModelProperty(notes = "티켓유효시작일시")
  private String ticketValidStartDt;

  @ApiModelProperty(notes = "티켓유효종료일시")
  private String ticketValidEndDt;
}