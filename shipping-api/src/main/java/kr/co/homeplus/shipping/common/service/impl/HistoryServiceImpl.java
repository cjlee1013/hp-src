package kr.co.homeplus.shipping.common.service.impl;

import kr.co.homeplus.shipping.common.mapper.HistoryMasterMapper;
import kr.co.homeplus.shipping.common.model.history.*;
import kr.co.homeplus.shipping.common.service.HistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HistoryServiceImpl implements HistoryService {

    private final HistoryMasterMapper historyMasterMapper;

    @Override
    public int setShippingHistoryInvoice(ShippingHistoryInsertSetDto historyInsertSetDto) {
        return historyMasterMapper.setShippingHistoryInvoice(historyInsertSetDto);
    }
    @Override
    public int setPickHistoryInvoice(PickHistoryInsertSetDto historyInsertSetDto) {
        return historyMasterMapper.setPickHistoryInvoice(historyInsertSetDto);
    }
    @Override
    public int setExchHistoryInvoice(ExchHistoryInsertSetDto historyInsertSetDto) {
        return historyMasterMapper.setExchHistoryInvoice(historyInsertSetDto);
    }

    @Override
    public int setShippingProcessHistory(ShippingProcessInsertSetDto historyInsertSetDto) {
        return historyMasterMapper.setShippingProcessHistory(historyInsertSetDto);
    }

    @Override
    public int setShippingProcessHistoryBundle(BundleProcessInsertSetDto historyInsertSetDto) {
        return historyMasterMapper.setShippingProcessHistoryBundle(historyInsertSetDto);
    }

    @Override
    public int setPickProcessHistory(PickProcessInsertSetDto historyInsertSetDto) {
        return historyMasterMapper.setPickProcessHistory(historyInsertSetDto);
    }

    @Override
    public int setExchProcessHistory(ExchProcessInsertSetDto historyInsertSetDto) {
        return historyMasterMapper.setExchProcessHistory(historyInsertSetDto);
    }

    @Override
    public int setMultiShippingHistoryInvoice(MultiShippingHistoryInsertSetDto historyInsertSetDto) {
        return historyMasterMapper.setMultiShippingHistoryInvoice(historyInsertSetDto);
    }
    @Override
    public int setMultiShippingProcessHistory(MultiShippingProcessInsertSetDto historyInsertSetDto) {
        return historyMasterMapper.setMultiShippingProcessHistory(historyInsertSetDto);
    }

    @Override
    public int setCommonHistoryInvoice(CommonHistoryInsertSetDto historyInsertSetDto) {
        return historyMasterMapper.setCommonHistoryInvoice(historyInsertSetDto);
    }
}
