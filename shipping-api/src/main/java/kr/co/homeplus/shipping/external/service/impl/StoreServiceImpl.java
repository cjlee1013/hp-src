package kr.co.homeplus.shipping.external.service.impl;

import java.util.*;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import kr.co.homeplus.shipping.common.model.history.BundleProcessInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.ShippingProcessInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.ExchProcessInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.PickProcessInsertSetDto;
import kr.co.homeplus.shipping.common.service.HistoryService;
import kr.co.homeplus.shipping.common.service.MessageSendService;
import kr.co.homeplus.shipping.constants.ResourceRouteName;
import kr.co.homeplus.shipping.external.mapper.StoreMasterMapper;
import kr.co.homeplus.shipping.external.mapper.StoreSlaveMapper;
import kr.co.homeplus.shipping.external.model.store.*;
import kr.co.homeplus.shipping.external.model.market.*;
import kr.co.homeplus.shipping.external.service.StoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StoreServiceImpl implements StoreService {

    private final StoreMasterMapper storeMasterMapper;
    private final StoreSlaveMapper storeSlaveMapper;

    private final HistoryService historyService;
    private final RefitCryptoService cryptoService;

    private final MessageSendService messageSendService;

    private final ResourceClient resourceClient;

    private List<StoreShipInfoGetDto> getShipInfo(String purchaseStoreInfoNo) {
        List<StoreShipInfoGetDto> storeShipInfoGetDtoList;
        try {
            storeShipInfoGetDtoList = storeSlaveMapper.getShipInfo(purchaseStoreInfoNo);
        } catch (Exception e) {
            return null;
        }
        return storeShipInfoGetDtoList;
    }

    private Map<String,StorePickInfoGetDto> getPickInfo(List<StoreClaimCommonSetDto> storeClaimCommonSetDto) {
        List<StorePickInfoGetDto> storePickInfoGetDto;
        try {
            storePickInfoGetDto = storeSlaveMapper.getPickInfo(storeClaimCommonSetDto);
        } catch (Exception e) {
            return null;
        }
        Map<String,StorePickInfoGetDto> pickInfoMap = new HashMap<>();
        for (StorePickInfoGetDto storePickInfo : storePickInfoGetDto) {
            pickInfoMap.put(storePickInfo.getClaimBundleNo(),storePickInfo);
        }
        return pickInfoMap;
    }

    private Map<String,StoreExchInfoGetDto> getExchInfo(List<StoreClaimCommonSetDto> storeClaimCommonSetDto) {
        List<StoreExchInfoGetDto> storeExchInfoGetDto;
        try {
            storeExchInfoGetDto = storeSlaveMapper.getExchInfo(storeClaimCommonSetDto);
        } catch (Exception e) {
            return null;
        }
        Map<String,StoreExchInfoGetDto> exchInfoMap = new HashMap<>();
        for (StoreExchInfoGetDto storeExchInfo : storeExchInfoGetDto) {
            exchInfoMap.put(storeExchInfo.getClaimBundleNo(),storeExchInfo);
        }
        return exchInfoMap;
    }

    @Override
    public List<StoreShipResultGetDto> setShipStart(List<StoreShipStartSetDto> storeShipStartSetDtoList) {
        int result,resultSum;
        String shipStatus,maxShipStatus = "D1", purchaseStoreInfoNo, bundleNo,lastBundleNo = "",driverNo,shipNo;
        List<StoreShipResultGetDto> results = new ArrayList<>();
        List<StoreShipInfoGetDto> storeShipInfoGetDtoList;
        List<MarketOrderItemGetDto> marketOrderItemGetDtoList;
        ResourceClientRequest<Boolean> request;
        String marketUrl = null;
        boolean marketResult;
        for (StoreShipStartSetDto shipStart : storeShipStartSetDtoList) {
            purchaseStoreInfoNo = shipStart.getPurchaseStoreInfoNo();
            storeShipInfoGetDtoList = getShipInfo(purchaseStoreInfoNo);
            if (storeShipInfoGetDtoList == null || storeShipInfoGetDtoList.size() == 0) {
                results.add((new StoreShipResultGetDto(purchaseStoreInfoNo, false, "구매점포번호 없음")));
                continue;
            }
            resultSum = 0;
            driverNo = shipStart.getDriverTelNo();
            try {
                shipStart.setDriverTelNo(cryptoService.encrypt(driverNo));
            } catch (Exception e) {
            }
            for (StoreShipInfoGetDto storeShipInfoGetDto: storeShipInfoGetDtoList) {
                shipNo = storeShipInfoGetDto.getShipNo();
                bundleNo = storeShipInfoGetDto.getBundleNo();
                shipStart.setBundleNo(bundleNo);
                shipStatus = storeShipInfoGetDto.getShipStatus();
                if (!"D2".equals(shipStatus) && !"D3".equals(shipStatus)) {
                    continue;
                }
                try {
                    shipStart.setShipNo(shipNo);
                    result = storeMasterMapper.setShipStart(shipStart);
                    resultSum += result;
                } catch (Exception e) {
                    result = 0;
                }

                if (result > 0) {
                    // 마켓 연동 상품의 경우 배송정보 연동
                    marketOrderItemGetDtoList = storeSlaveMapper.getShippingMarket(shipNo);
                    for (MarketOrderItemGetDto item : marketOrderItemGetDtoList) {
                        switch (item.getMarketType().toLowerCase()) {
                            case "naver":
                                marketUrl = "/market/naver/setShipStart?productOrderId="+item.getMarketOrderItemNo()+"&trackingNo="+item.getPurchaseOrderNo();
                                break;
                            case "eleven":
                                marketUrl = "/market/eleven/setShipStartPart?dlvNo="+item.getDlvNo()+"&ordNo="+item.getMarketOrderNo()+"&ordPrdSeq="+item.getMarketOrderItemNo();
                                break;
                            default:
                                marketUrl = null;
                                break;
                        }
                        if (marketUrl != null) {
                            try {
                                request = ResourceClientRequest.<Boolean>getBuilder()
                                    .apiId(ResourceRouteName.OUTBOUND)
                                    .uri(marketUrl)
                                    .typeReference(new ParameterizedTypeReference<>() {})
                                    .build();
                                marketResult = resourceClient.get(request, new TimeoutConfig()).getBody().getData();
                                // 실패한것들은 차후 배치로 전환
                                if (marketResult) {
                                    storeMasterMapper.setMarketShipStart(new MarketShipSetDto(item.getMarketOrderNo(), item.getMarketOrderItemNo(), item.getMarketType()));
                                }
                            } catch (Exception e) {
                                historyService.setShippingProcessHistory(
                                        new ShippingProcessInsertSetDto("오류발생", "마켓배송정보 연동오류", "", "batch", "batch", shipNo));

                            }
                        }
                    }
                }

                if (!lastBundleNo.equals(bundleNo)) {
                    lastBundleNo = bundleNo;
                    maxShipStatus = shipStatus;
                    if ("D2".equals(shipStatus) && result > 0) {
                        messageSendService.sendMessageTDDRCT(bundleNo);
                        try {
                            historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("배송출발",shipStart.getDriverNm() + " " + shipStart.getVanNo(), "", "batch","batch", bundleNo));
                        } catch (Exception e) {

                        }
                    }
                    try {
                        storeMasterMapper.setShipVanInfo(shipStart);
                    } catch (Exception e) {
                        historyService.setShippingProcessHistory(
                            new ShippingProcessInsertSetDto("오류발생", "배송기사정보 업데이트 실패", "", "batch", "batch", shipNo));
                    }
                }
            }
            if (resultSum > 0 && ("D2".equals(maxShipStatus) || "D3".equals(maxShipStatus))) {
                results.add((new StoreShipResultGetDto(purchaseStoreInfoNo, true)));
            } else if (resultSum == 0) {
                results.add((new StoreShipResultGetDto(purchaseStoreInfoNo, true, "변경 없음")));
            } else {
                results.add((new StoreShipResultGetDto(purchaseStoreInfoNo, false, "DB업데이트 실패")));
            }
        }
        return results;
    }

    @Override
    public List<StoreShipResultGetDto> setShipComplete(List<StoreShipCompleteSetDto> storeShipCompleteSetDtoList) {
        List<StoreShipInfoGetDto> storeShipInfoGetDtoList;
        int result, resultSum;
        String shipStatus,maxShipStatus = "D1", purchaseStoreInfoNo,shipNo,lastBundleNo = "",bundleNo;
        List<StoreShipResultGetDto> results = new ArrayList<>();
        List<MarketOrderItemGetDto> marketOrderItemGetDtoList;
        for (StoreShipCompleteSetDto storeShipCompleteSetDto : storeShipCompleteSetDtoList) {
            purchaseStoreInfoNo = storeShipCompleteSetDto.getPurchaseStoreInfoNo();
            storeShipInfoGetDtoList = getShipInfo(purchaseStoreInfoNo);
            if (storeShipInfoGetDtoList == null || storeShipInfoGetDtoList.size() == 0) {
                results.add((new StoreShipResultGetDto(purchaseStoreInfoNo, false, "구매점포번호 없음")));
                continue;
            }
            resultSum = 0;
            for (StoreShipInfoGetDto storeShipInfoGetDto : storeShipInfoGetDtoList) {
                shipNo = storeShipInfoGetDto.getShipNo();
                bundleNo = storeShipInfoGetDto.getBundleNo();
                storeShipCompleteSetDto.setShipNo(shipNo);
                shipStatus = storeShipInfoGetDto.getShipStatus();
                if (shipStatus == null) {
                    results.add((new StoreShipResultGetDto(purchaseStoreInfoNo, false, "구매점포번호 없음")));
                } else {
                    if ("D1".equals(shipStatus)) {
                        continue;
                    }
                    try {
                        result = storeMasterMapper.setShipComplete(storeShipCompleteSetDto);
                        resultSum += result;
                    } catch (Exception e) {
                        result = 0;
                    }

                    if (!lastBundleNo.equals(bundleNo)) {
                        lastBundleNo = bundleNo;
                        maxShipStatus = shipStatus;
                        if (result > 0) {
                            if ("D2".equals(shipStatus)) {
                                try {
                                    storeMasterMapper.setShipVanInfo(storeShipCompleteSetDto);
                                } catch (Exception e) {
                                    historyService.setShippingProcessHistory(
                                        new ShippingProcessInsertSetDto("오류발생", "배송기사정보 업데이트 실패",
                                            "", "batch", "batch", shipNo));
                                }
                            }
                            if ("D3".equals(shipStatus)) {
                                try {
                                    historyService.setShippingProcessHistoryBundle(
                                        new BundleProcessInsertSetDto("배송완료", "배송완료", "배송중",
                                            "batch", "batch", bundleNo));
                                } catch (Exception e) {

                                }
                            }
                        }
                    }
                }
            }
            if (resultSum > 0 && ("D3".equals(maxShipStatus) || "D4".equals(maxShipStatus)|| "D5".equals(maxShipStatus))) {
                results.add((new StoreShipResultGetDto(purchaseStoreInfoNo, true)));
            } else if (resultSum == 0) {
                results.add((new StoreShipResultGetDto(purchaseStoreInfoNo, true, "변경 없음")));
            } else {
                results.add((new StoreShipResultGetDto(purchaseStoreInfoNo, false, "DB업데이트 실패")));
            }
        }
        return results;
    }

    @Override
    public List<StoreShipResultGetDto> setSordNo(List<StoreShipVaninfoSetDto> storeShipVaninfoSetDtoList) {
        int result, resultSum = 0;
        String shipStatus,maxShipStatus = "D1", purchaseStoreInfoNo, bundleNo,lastBundleNo = "",driverNo;
        List<StoreShipResultGetDto> results = new ArrayList<>();
        List<StoreShipInfoGetDto> storeShipInfoGetDtoList;
        for (StoreShipVaninfoSetDto storeShipVaninfoSetDto : storeShipVaninfoSetDtoList) {
            purchaseStoreInfoNo = storeShipVaninfoSetDto.getPurchaseStoreInfoNo();
            storeShipInfoGetDtoList = getShipInfo(purchaseStoreInfoNo);
            if (storeShipInfoGetDtoList == null || storeShipInfoGetDtoList.size() == 0) {
                results.add((new StoreShipResultGetDto(purchaseStoreInfoNo, false, "구매점포번호 없음")));
                continue;
            }
            driverNo = storeShipVaninfoSetDto.getDriverTelNo();
            try {
                storeShipVaninfoSetDto.setDriverTelNo(cryptoService.encrypt(driverNo));
            } catch (Exception e) {
            }
            for (StoreShipInfoGetDto storeShipInfoGetDto: storeShipInfoGetDtoList) {
                bundleNo = storeShipInfoGetDto.getBundleNo();
                storeShipVaninfoSetDto.setBundleNo(bundleNo);
                shipStatus = storeShipInfoGetDto.getShipStatus();

                if (!lastBundleNo.equals(bundleNo)) {
                    lastBundleNo = bundleNo;
                    maxShipStatus = shipStatus;
                    try {
                        result = storeMasterMapper.setShipVanInfo(storeShipVaninfoSetDto);
                    } catch (Exception e) {
                        result = 0;
                    }
                    resultSum += result;
                }
            }

            if (resultSum > 0 && ("D2".equals(maxShipStatus) || "D3".equals(maxShipStatus))) {
                results.add((new StoreShipResultGetDto(purchaseStoreInfoNo, true)));
            } else if (resultSum == 0) {
                results.add((new StoreShipResultGetDto(purchaseStoreInfoNo, true, "변경 없음")));
            } else {
                results.add((new StoreShipResultGetDto(purchaseStoreInfoNo, false, "DB업데이트 실패")));
            }
        }
        return results;
    }

    @Override
    public List<StoreClaimResultGetDto> setPickStart(List<StorePickStartSetDto> storePickStartSetDto) {
        Map<String,StorePickInfoGetDto> pickInfo = getPickInfo((List<StoreClaimCommonSetDto>)(List<?>)storePickStartSetDto);
        if (pickInfo == null) {
            // 데이터가 없는 경우
            return null;
        } else {
            int result;
            String claimPickShippingNo, claimBundleNo, pickStatus;
            StorePickInfoGetDto storePickInfoGetDto;
            List<StoreClaimResultGetDto> results = new ArrayList<>();
            for (StorePickStartSetDto pickStart : storePickStartSetDto) {
                claimBundleNo = pickStart.getClaimBundleNo();
                storePickInfoGetDto = pickInfo.get(claimBundleNo);
                pickStatus = storePickInfoGetDto.getPickStatus();
                claimPickShippingNo = storePickInfoGetDto.getClaimPickShippingNo();
                if (pickStatus == null || pickStatus.isEmpty()) {
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"클레임번들번호 없음")));
                } else if (pickStatus.equals("P1")) {
                    // 요청완료 -> 수거중 (정상)
                    try {
                        result = storeMasterMapper.setPickStart(pickStart);
                    } catch (Exception e) {
                        result = 0;
                    }
                    if (result > 0) {
                        try {
                            historyService.setPickProcessHistory(new PickProcessInsertSetDto("수거출발",pickStart.getDriverNm()+"("+pickStart.getDriverTelNo()+") "+pickStart.getVanNo(),"수거요청","batch","batch",claimPickShippingNo));
                        } catch (Exception e) {

                        }
                        try {
                            //성공 시 수거정보 업데이트
                            //String driverNo = cryptoService.encryptMobile(pickStart.getDriverTelNo());
                            //pickStart.setDriverTelNo(driverNo);
                            //storeMasterMapper.setPickVanInfo(storePickStartSetDto);
                            results.add((new StoreClaimResultGetDto(claimBundleNo, true)));
                        } catch (Exception e) {
                            results.add((new StoreClaimResultGetDto(claimBundleNo, true, "수거기사정보 업데이트 실패")));
                        }
                    } else {
                        results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"DB업데이트 실패")));
                    }
                } else if (pickStatus.equals("P2")) {
                    // 수거중 상태에서 수거시간 업데이트 (정상)
                    try {
                        result = storeMasterMapper.setPickSchedule(pickStart);
                    } catch (Exception e) {
                        result = 0;
                    }
                    if (result > 0) {
                        try {
                            historyService.setPickProcessHistory(new PickProcessInsertSetDto("수거예정시간변경",pickStart.getDriverNm()+"("+pickStart.getDriverTelNo()+") "+pickStart.getVanNo(),"수거중","batch","batch",claimPickShippingNo));
                        } catch (Exception e) {

                        }
                        try {
                            //성공 시 수거정보 업데이트
                            //String driverNo = cryptoService.encryptMobile(pickStart.getDriverTelNo());
                            //pickStart.setDriverTelNo(driverNo);
                            //storeMasterMapper.setPickVanInfo(storePickStartSetDto);
                            results.add((new StoreClaimResultGetDto(claimBundleNo, true)));
                        } catch (Exception e) {
                            results.add((new StoreClaimResultGetDto(claimBundleNo, true,"수거기사정보 업데이트 실패")));
                        }
                    } else {
                        results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"DB업데이트 실패")));
                    }
                } else if (pickStatus.equals("NN") || pickStatus.equals("P0")) {
                    // 아직 수거 요청 전인 경우
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"수거요청 이전상태")));
                } else if (pickStatus.equals("P3") || pickStatus.equals("P8")) {
                    // 이미 수거 완료 / 실패인 경우
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"이미 수거 완료")));
                } else {
                    // 기타 예외
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"알 수 없는 오류")));
                }
            }
            return results;
        }
    }

    @Override
    public List<StoreClaimResultGetDto> setPickComplete(List<StorePickCompleteSetDto> storePickCompleteSetDto) {
        Map<String,StorePickInfoGetDto> pickupInfo = getPickInfo((List<StoreClaimCommonSetDto>)(List<?>)storePickCompleteSetDto);
        if (pickupInfo == null) {
            // 데이터가 없는 경우
            return null;
        } else {
            int result;
            String claimPickShippingNo, claimBundleNo, pickStatus;
            StorePickInfoGetDto storePickInfoGetDto;
            List<StoreClaimResultGetDto> results = new ArrayList<>();
            for (StorePickCompleteSetDto pickupComplete : storePickCompleteSetDto) {
                claimBundleNo = pickupComplete.getClaimBundleNo();
                storePickInfoGetDto = pickupInfo.get(claimBundleNo);
                pickStatus = storePickInfoGetDto.getPickStatus();
                claimPickShippingNo = storePickInfoGetDto.getClaimPickShippingNo();
                if (pickStatus == null || pickStatus.isEmpty()) {
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"클레임번들번호 없음")));
                } else if (pickStatus.equals("P2")) {
                    try {
                        result = storeMasterMapper.setPickComplete(pickupComplete);
                    } catch (Exception e) {
                        result = 0;
                    }
                    if (result > 0) {
                        try {
                            historyService.setPickProcessHistory(new PickProcessInsertSetDto("수거완료","수거완료","수거중","batch","batch",claimPickShippingNo));
                        } catch (Exception e){

                        }
                        results.add((new StoreClaimResultGetDto(claimBundleNo, true)));
                    } else {
                        results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"DB업데이트 실패")));
                    }
                } else if (pickStatus.equals("NN") || pickStatus.equals("P0") || pickStatus.equals("P1")) {
                    // 아직 수거 출발 전인 경우
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"수거중 이전상태")));
                } else if (pickStatus.equals("P3")) {
                    // 수거 완료인 경우
                    results.add((new StoreClaimResultGetDto(claimBundleNo, true ,"이미 수거 완료")));
                } else if (pickStatus.equals("P8")) {
                    // 수거 실패인 경우
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"수거 실패")));
                } else {
                    // 기타 예외
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"알 수 없는 오류")));
                }
            }
            return results;
        }
    }


    @Override
    public List<StoreClaimResultGetDto> setExchStart(List<StoreExchStartSetDto> storeExchStartSetDto) {
        Map<String,StoreExchInfoGetDto> exchInfo = getExchInfo((List<StoreClaimCommonSetDto>)(List<?>)storeExchStartSetDto);
        if (exchInfo == null) {
            // 데이터가 없는 경우
            return null;
        } else {
            int result;
            String claimExchShippingNo, claimBundleNo, exchStatus;
            StoreExchInfoGetDto storeExchInfoGetDto;
            List<StoreClaimResultGetDto> results = new ArrayList<>();
            for (StoreExchStartSetDto exchStart : storeExchStartSetDto) {
                claimBundleNo = exchStart.getClaimBundleNo();
                storeExchInfoGetDto = exchInfo.get(claimBundleNo);
                exchStatus = storeExchInfoGetDto.getExchStatus();
                claimExchShippingNo = storeExchInfoGetDto.getClaimExchShippingNo();
                if (exchStatus == null || exchStatus.isEmpty()) {
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"클레임번들번호 없음")));
                } else if (exchStatus.equals("D1")) {
                    // 상품출고 -> 배송중 (정상)
                    try {
                        result = storeMasterMapper.setExchStart(exchStart);
                    } catch (Exception e) {
                        result = 0;
                    }
                    if (result > 0) {
                        try {
                            historyService.setExchProcessHistory(new ExchProcessInsertSetDto("배송출발",exchStart.getDriverNm()+"("+exchStart.getDriverTelNo()+") "+exchStart.getVanNo(),"교환요청","batch","batch",claimExchShippingNo));
                        } catch (Exception e) {

                        }
                        try {
                            //성공 시 배송정보 업데이트
                            //String driverNo = cryptoService.encryptMobile(exchStart.getDriverTelNo());
                            //exchStart.setDriverTelNo(driverNo);
                            //storeMasterMapper.setExchVanInfo(storeExchStartSetDto);
                            results.add((new StoreClaimResultGetDto(claimBundleNo, true)));
                        } catch (Exception e) {
                            results.add((new StoreClaimResultGetDto(claimBundleNo, true,"배송기사정보 업데이트 실패")));
                        }
                    } else {
                        results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"DB업데이트 실패")));
                    }
                } else if (exchStatus.equals("D2")) {
                    // 배송중 상태에서 예정시간 업데이트 (정상)
                    try {
                        result = storeMasterMapper.setExchSchedule(exchStart);
                    } catch (Exception e) {
                        result = 0;
                    }
                    if (result > 0) {
                        try {
                            historyService.setExchProcessHistory(new ExchProcessInsertSetDto("배송예정시간변경",exchStart.getDriverNm()+"("+exchStart.getDriverTelNo()+") "+exchStart.getVanNo(),"배송중","batch","batch",claimExchShippingNo));
                        } catch (Exception e) {

                        }
                        try {
                            //성공 시 배송정보 업데이트
                            //String driverNo = cryptoService.encryptMobile(exchStart.getDriverTelNo());
                            //exchStart.setDriverTelNo(driverNo);
                            //storeMasterMapper.setExchVanInfo(storeExchStartSetDto);
                            results.add((new StoreClaimResultGetDto(claimBundleNo, true)));
                        } catch (Exception e) {
                            results.add((new StoreClaimResultGetDto(claimBundleNo, true,"배송기사정보 업데이트 실패")));
                        }
                    } else {
                        results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"DB업데이트 실패")));
                    }
                } else if (exchStatus.equals("NN")) {
                    // 아직 배송시작 시간 전인 경우
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"상품출고 이전 상태")));
                } else if (exchStatus.equals("D3")) {
                    // 이미 배송 완료된 경우
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"이미 배송 완료")));
                } else {
                    // 기타 예외
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"알 수 없는 오류")));
                }
            }
            return results;
        }
    }

    @Override
    public List<StoreClaimResultGetDto> setExchComplete(List<StoreExchCompleteSetDto> storeExchCompleteSetDto) {
        Map<String,StoreExchInfoGetDto> receiveInfo = getExchInfo((List<StoreClaimCommonSetDto>)(List<?>)storeExchCompleteSetDto);
        if (receiveInfo == null) {
            // 데이터가 없는 경우
            return null;
        } else {
            int result;
            String claimExchShippingNo, claimBundleNo, exchStatus;
            StoreExchInfoGetDto storeExchInfoGetDto;
            List<StoreClaimResultGetDto> results = new ArrayList<>();
            for (StoreExchCompleteSetDto receiveComplete : storeExchCompleteSetDto) {
                claimBundleNo = receiveComplete.getClaimBundleNo();
                storeExchInfoGetDto = receiveInfo.get(claimBundleNo);
                exchStatus = storeExchInfoGetDto.getExchStatus();
                claimExchShippingNo = storeExchInfoGetDto.getClaimExchShippingNo();
                if (exchStatus == null || exchStatus.isEmpty()) {
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"클레임번들번호 없음")));
                } else if (exchStatus.equals("D2")) {
                    try {
                        result = storeMasterMapper.setExchComplete(receiveComplete);
                    } catch (Exception e) {
                        result = 0;
                    }
                    if (result > 0) {
                        try {
                            historyService.setExchProcessHistory(new ExchProcessInsertSetDto("배송완료","배송완료","배송중","batch","batch",claimExchShippingNo));
                        } catch (Exception e) {

                        }
                        results.add((new StoreClaimResultGetDto(claimBundleNo, true)));
                    } else {
                        results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"DB업데이트 실패")));
                    }
                } else if (exchStatus.equals("NN") || exchStatus.equals("D1")) {
                    // 아직 배송 출발 전인 경우
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"상품출고 이전 상태")));
                } else if (exchStatus.equals("D3")) {
                    // 이미 배송 완료된 경우
                    results.add((new StoreClaimResultGetDto(claimBundleNo, true ,"이미 배송 완료")));
                } else if (exchStatus.equals("D8")) {
                    // 배송 실패인 경우
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"배송 실패")));
                } else {
                    // 기타 예외
                    results.add((new StoreClaimResultGetDto(claimBundleNo, false ,"알 수 없는 오류")));
                }
            }
            return results;
        }
    }
}
