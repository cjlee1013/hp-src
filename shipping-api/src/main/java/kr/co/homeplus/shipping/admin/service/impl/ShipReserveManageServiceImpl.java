package kr.co.homeplus.shipping.admin.service.impl;

import java.util.Iterator;
import java.util.List;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.shipping.admin.mapper.ShipReserveManageSlaveMapper;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipReserveManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipReserveManageListSetDto;
import kr.co.homeplus.shipping.admin.service.ShipReserveManageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipReserveManageServiceImpl implements ShipReserveManageService {
    private final ShipReserveManageSlaveMapper shipReserveManageSlaveMapper;

    /**
     * 예약배송관리 조회
     */
    @Override
    public List<ShipReserveManageListGetDto> getShipReserveManageList(ShipReserveManageListSetDto shipReserveManageListSetDto) {
      log.info("getShipReserveManageList param : {}", shipReserveManageListSetDto);

      List<ShipReserveManageListGetDto> shipReserveManageListGetDtoList = shipReserveManageSlaveMapper.selectShipReserveManageList(shipReserveManageListSetDto);

      Iterator<ShipReserveManageListGetDto> iterator = shipReserveManageListGetDtoList.iterator();

      while (iterator.hasNext()) {
        ShipReserveManageListGetDto dto = iterator.next();

        // 취소 데이터 노출 제외
        if(Integer.parseInt(dto.getItemQty()) < 1) {
          iterator.remove();
        } else {
          dto.setBuyerMobileNo(PrivacyMaskingUtils.maskingPhone(dto.getBuyerMobileNo()));
          dto.setShipMobileNo(PrivacyMaskingUtils.maskingPhone(dto.getShipMobileNo()));
          dto.setBuyerNm(PrivacyMaskingUtils.maskingUserName(dto.getBuyerNm()));
          dto.setReceiverNm(PrivacyMaskingUtils.maskingUserName(dto.getReceiverNm()));
          dto.setAddr(PrivacyMaskingUtils.maskingAddress(dto.getAddr()));
        }
      }

      return shipReserveManageListGetDtoList;
    }
}
