package kr.co.homeplus.shipping.test.model.qa;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "배송상태변경")
public class QaShipStatusSetDto {
    @ApiModelProperty(notes = "배송상태")
    private String shipStatus;
    @ApiModelProperty(notes = "시간")
    private String shipDt;
    @ApiModelProperty(notes = "키타입")
    private String keyType;
    @ApiModelProperty(notes = "키목록")
    private List<String> keyList;
}
