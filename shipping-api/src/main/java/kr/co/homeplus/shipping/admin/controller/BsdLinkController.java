package kr.co.homeplus.shipping.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdInvoiceSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdLinkListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdLinkListSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdLinkTranSetDto;
import kr.co.homeplus.shipping.admin.service.BsdLinkService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/shipManage")
@Api(tags = "어드민 - BSD연동조회")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class BsdLinkController {
  private final BsdLinkService bsdLinkService;

  @ApiOperation(value = "BSD연동조회")
  @PostMapping("/getBsdLinkList")
  public ResponseObject<List<BsdLinkListGetDto>> getBsdLinkList(@RequestBody @Valid BsdLinkListSetDto bsdLinkListSetDto) {
    return ResourceConverter.toResponseObject(bsdLinkService.getBsdLinkList(bsdLinkListSetDto));
  }

  @ApiOperation(value = "BSD전송")
  @PostMapping("/setBsdLinkTran")
  public ResponseObject<String> setBsdLinkTran(@RequestBody @Valid BsdLinkTranSetDto bsdLinkTranSetDto) {
      return bsdLinkService.setBsdLinkTran(bsdLinkTranSetDto);
  }

  @ApiOperation(value = "BSD송장입력 (단일배송)")
  @PostMapping("/setShippingBsdInvoice")
  public ResponseObject<List<String>> setShippingBsdInvoice(@RequestBody @Valid List<BsdInvoiceSetDto> bsdInvoiceSetDtoList) {
    return bsdLinkService.setShippingBsdInvoice(bsdInvoiceSetDtoList);
  }

  @ApiOperation(value = "BSD송장입력 (다중배송)")
  @PostMapping("/setMultiOrderBsdInvoice")
  public ResponseObject<List<String>> setMultiOrderBsdInvoice(@RequestBody @Valid List<BsdInvoiceSetDto> bsdInvoiceSetDtoList) {
    return bsdLinkService.setMultiOrderBsdInvoice(bsdInvoiceSetDtoList);
  }
}
