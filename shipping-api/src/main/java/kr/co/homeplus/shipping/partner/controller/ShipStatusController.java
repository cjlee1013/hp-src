package kr.co.homeplus.shipping.partner.controller;

import static kr.co.homeplus.shipping.constants.ResourceRouteName.PARTNER;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipCompleteByBundleSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipCompletePopListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipCompleteSetDto;
import kr.co.homeplus.shipping.admin.service.ShipManageService;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.partner.model.sell.shipStatus.ShipStatusCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.shipStatus.ShipStatusListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.shipStatus.ShipStatusListSetDto;
import kr.co.homeplus.shipping.partner.service.ShipStatusService;
import kr.co.homeplus.shipping.utils.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/partner/sell")
@Api(tags = "파트너 - 배송현황")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class ShipStatusController {
  private final ShipStatusService shipStatusService;
  private final ShipManageService shipManageService;

  @ApiOperation(value = "배송현황 조회")
  @PostMapping("/getShipStatusList")
  public ResponseObject<List<ShipStatusListGetDto>> getShipStatusList(@RequestBody @Valid ShipStatusListSetDto shipStatusListSetDto) {
    return ResourceConverter.toResponseObject(shipStatusService.getShipStatusList(shipStatusListSetDto));
  }

  @ApiOperation(value = "배송현황 카운트 리스트 조회")
  @GetMapping("/getShipStatusCntList")
  public ResponseObject<List<ShipStatusListGetDto>> getShipStatusCntList(
        @ApiParam(name = "schType", value = "검색타입", required = true) @RequestParam String schType,
        @ApiParam(name = "partnerId", value = "파트너ID", required = true) @RequestParam String partnerId) {
    return ResourceConverter.toResponseObject(shipStatusService.getShipStatusCntList(schType, partnerId));
  }

  @ApiOperation(value = "배송현황 카운트 조회")
  @GetMapping("/getShipStatusCnt")
  public ResponseObject<ShipStatusCntGetDto> getShipStatusCnt(@ApiParam(name = "partnerId", value = "파트너ID", required = true) @RequestParam String partnerId) {
    return ResourceConverter.toResponseObject(shipStatusService.getShipStatusCnt(partnerId));
  }

  @ApiOperation(value = "배송완료")
  @PostMapping("/setShipComplete")
  public ResponseObject<String> setShipComplete(@RequestBody @Valid ShipManageShipCompleteSetDto shipManageShipCompleteSetDto) {
    try {
      return shipManageService.setShipComplete(shipManageShipCompleteSetDto, PARTNER);
    } catch (Exception e) {
      return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3005.getResponseMessage());
    }
  }

  @ApiOperation(value = "배송완료 팝업 리스트 조회")
  @PostMapping("/getShipCompletePopList")
  public ResponseObject<List<ShipManageShipCompletePopListGetDto>> getShipCompletePopList(@RequestBody @Valid ShipManageShipCompleteSetDto shipManageShipCompleteSetDto) {
    return ResourceConverter.toResponseObject(shipManageService.getShipCompletePopList(shipManageShipCompleteSetDto, PARTNER));
  }

  @ApiOperation(value = "배송완료(Bundle단위)")
  @PostMapping("/setShipCompleteByBundle")
  public ResponseObject<String> setShipCompleteByBundle(@RequestBody @Valid ShipManageShipCompleteByBundleSetDto shipManageShipCompleteByBundleSetDto) {
    try {
      ShipManageShipCompleteSetDto shipManageShipCompleteSetDto = new ShipManageShipCompleteSetDto();
      List<String> bundleNoList = new ArrayList<>();

      bundleNoList.add(shipManageShipCompleteByBundleSetDto.getBundleNo());

      shipManageShipCompleteSetDto.setBundleNoList(bundleNoList);
      shipManageShipCompleteSetDto.setChgId(shipManageShipCompleteByBundleSetDto.getChgId());

      return shipManageService.setShipComplete(shipManageShipCompleteSetDto, PARTNER);
    } catch (Exception e) {
      return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3005.getResponseMessage());
    }
  }
}