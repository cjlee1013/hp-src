package kr.co.homeplus.shipping.tracking.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.shipping.tracking.model.common.DlvCdMappingGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickAddressGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickValidCreditGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickValidCreditSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.InvalidCreditGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnCancelGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceRequestGetDto;

public interface SweettrackerService {
  TraceRequestGetDto requestTracking(String callbackUrl, Collection<?> commonTrackingGetDtoList);
  List<PickValidCreditGetDto> getInvalidCredit(List<PickValidCreditSetDto> pickValidCreditSetDtoList);
  ReturnRequestGetDto requestPick(List<PickAddressGetDto> pickAddressGetDtoList);
  ReturnCancelGetDto cancelPick(List<PickRequestSetDto> pickRequestSetDtoList);

  Map<String, DlvCdMappingGetDto> getDlvCdMappingStoH();
  Map<String, DlvCdMappingGetDto> getDlvCdMappingHtoS();
}
