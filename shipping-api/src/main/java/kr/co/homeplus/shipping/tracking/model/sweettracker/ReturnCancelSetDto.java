package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "스윗트래커 반품요청 파라미터")
public class ReturnCancelSetDto {

    @ApiModelProperty(notes = "tier코드")
    private String tierCode;

    @ApiModelProperty(notes = "목록", dataType = "List")
    private List<ReturnCancelItemSetDto> data;

    public ReturnCancelSetDto(String tierCode,List<ReturnCancelItemSetDto> data) {
        this.tierCode = tierCode;
        this.data = data;
    }
}
