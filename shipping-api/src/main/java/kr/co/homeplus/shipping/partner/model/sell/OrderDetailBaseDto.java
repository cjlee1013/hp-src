package kr.co.homeplus.shipping.partner.model.sell;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문기본정보 DTO")
public class OrderDetailBaseDto {
    @ApiModelProperty(notes = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "주문일")
    private String orderDt;

    @ApiModelProperty(notes = "구매자")
    private String buyerNm;

    @ApiModelProperty(notes = "구매자 연락처")
    private String buyerMobileNo;
}
