package kr.co.homeplus.shipping.shipping.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.external.model.store.*;
import kr.co.homeplus.shipping.external.service.StoreService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/external/store")
@Api(tags = "옴니연동 배송정보")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class StoreController {
  private final StoreService storeService;

  @ApiOperation(value = "배송중 처리/배송예정시간 업데이트", response = ResponseObject.class, notes = "필수값 : 번들번호 , 옵션 : 배송예정시간/차량ID/차량번호/기사명/기사전화번호")
  @RequestMapping(value = "/setShipStart", method = {RequestMethod.POST})
  public ResponseObject<List<StoreShipResultGetDto>> setShipStart(@RequestBody @Valid List<StoreShipStartSetDto> storeShipStartSetDto) {
    if (storeShipStartSetDto == null || storeShipStartSetDto.size() == 0) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    }
    List<StoreShipResultGetDto> results = storeService.setShipStart(storeShipStartSetDto);

    if (results == null) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    }
    return ResourceConverter.toResponseObject(results);
  }

  @ApiOperation(value = "배송완료 처리", response = ResponseObject.class, notes = "필수값 : 번들번호 , 옵션 : 배송완료시간(미입력시 현재시간)")
  @RequestMapping(value = "/setShipComplete", method = {RequestMethod.POST})
  public ResponseObject<List<StoreShipResultGetDto>> setShipComplete(@RequestBody @Valid List<StoreShipCompleteSetDto> storeShipCompleteSetDto) {
    if (storeShipCompleteSetDto == null || storeShipCompleteSetDto.size() == 0) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    }
    List<StoreShipResultGetDto> results = storeService.setShipComplete(storeShipCompleteSetDto);

    if (results == null) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    }
    return ResourceConverter.toResponseObject(results);
  }


  @ApiOperation(value = "단축번호 등록", response = ResponseObject.class, notes = "필수값 : 번들번호 단축번호")
  @RequestMapping(value = "/setSordNo", method = {RequestMethod.POST})
  public ResponseObject<List<StoreShipResultGetDto>> setSordNo(@RequestBody @Valid List<StoreShipVaninfoSetDto> storeShipVaninfoSetDto) {
    if (storeShipVaninfoSetDto == null || storeShipVaninfoSetDto.size() == 0) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    }
    List<StoreShipResultGetDto> results = storeService.setSordNo(storeShipVaninfoSetDto);

    if (results == null) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    }
    return ResourceConverter.toResponseObject(results);
  }

  @ApiOperation(value = "수거중 처리/수거예정시간 업데이트", response = ResponseObject.class, notes = "필수값 : 클레임번들번호 , 옵션 : 수거예정시간/차량ID/차량번호/기사명/기사전화번호")
  @RequestMapping(value = "/setPickStart", method = {RequestMethod.POST})
  public ResponseObject<List<StoreClaimResultGetDto>> setPickStart(@RequestBody @Valid List<StorePickStartSetDto> storePickStartSetDto) {
    if (storePickStartSetDto == null || storePickStartSetDto.size() == 0) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    }
    List<StoreClaimResultGetDto> results = storeService.setPickStart(storePickStartSetDto);

    if (results == null) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    }
    return ResourceConverter.toResponseObject(results);
  }

  @ApiOperation(value = "수거완료 처리", response = ResponseObject.class, notes = "필수값 : 번들번호 , 옵션 : 수거완료시간(미입력시 현재시간)")
  @RequestMapping(value = "/setPickComplete", method = {RequestMethod.POST})
  public ResponseObject<List<StoreClaimResultGetDto>> setPickComplete(@RequestBody @Valid List<StorePickCompleteSetDto> storePickCompleteSetDto) {
    if (storePickCompleteSetDto == null || storePickCompleteSetDto.size() == 0) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    }
    List<StoreClaimResultGetDto> results = storeService.setPickComplete(storePickCompleteSetDto);

    if (results == null) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    }
    return ResourceConverter.toResponseObject(results);
  }

  @ApiOperation(value = "교환배송중 처리/교환배송예정시간 업데이트", response = ResponseObject.class, notes = "필수값 : 클레임번들번호 , 옵션 : 교환배송예정시간/차량ID/차량번호/기사명/기사전화번호")
  @RequestMapping(value = "/setExchStart", method = {RequestMethod.POST})
  public ResponseObject<List<StoreClaimResultGetDto>> setExchStart(@RequestBody @Valid List<StoreExchStartSetDto> storeExchStartSetDto) {
    if (storeExchStartSetDto == null || storeExchStartSetDto.size() == 0) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    }
    List<StoreClaimResultGetDto> results = storeService.setExchStart(storeExchStartSetDto);

    if (results == null) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    }
    return ResourceConverter.toResponseObject(results);
  }

  @ApiOperation(value = "교환배송완료 처리", response = ResponseObject.class, notes = "필수값 : 클레임번들번호 , 옵션 : 교환완료시간(미입력시 현재시간)")
  @RequestMapping(value = "/setExchComplete", method = {RequestMethod.POST})
  public ResponseObject<List<StoreClaimResultGetDto>> setExchComplete(@RequestBody @Valid List<StoreExchCompleteSetDto> storeExchCompleteSetDto) {
    if (storeExchCompleteSetDto == null || storeExchCompleteSetDto.size() == 0) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    }
    List<StoreClaimResultGetDto> results = storeService.setExchComplete(storeExchCompleteSetDto);

    if (results == null) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    }
    return ResourceConverter.toResponseObject(results);
  }
}
