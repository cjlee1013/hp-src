package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import kr.co.homeplus.shipping.common.model.history.PickProcessInsertSetDto;
import kr.co.homeplus.shipping.enums.SweettrackerCode;
import kr.co.homeplus.shipping.tracking.model.common.DlvCdMappingGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickCallbackSetDto;
import lombok.Data;

@Data
@ApiModel(description = "배송추적 콜백 스윗트래커 응답값")
public class ReturnCallbackItemSetDto {

    @ApiModelProperty(notes = "처리시간")
    private String trnTim;

    @ApiModelProperty(notes = "처리여부(Y:처리,N:미처리)")
    private String trnYn;

    @ApiModelProperty(notes = "주문번호")
    private String ordCde;

    @ApiModelProperty(notes = "스윗트래커 변환 주문번호")
    private String stOrdCde;

    @ApiModelProperty(notes = "택배사 코드")
    private String comCode;

    @ApiModelProperty(notes = "운송장 번호")
    private String invoice;

    @ApiModelProperty(notes = "처리상태코드")
    private String resCde;

    @ApiModelProperty(notes = "처리상태명")
    private String resNme;

    @ApiModelProperty(notes = "반품 송장")
    private String dunCde;

    public String getOrdCde() {
        return ordCde;
    }

    public PickCallbackSetDto toPickCallbackSetDto(Map<String, DlvCdMappingGetDto> mapping) {
        String status;
        if ("Y".equals(trnYn)) {
            if (SweettrackerCode.PICK_CODE_30.getCode().equals(resCde)) {
                status = "P0";
            } else if (SweettrackerCode.PICK_CODE_40.getCode().equals(resCde)) {
                status = "P1";
            } else if (SweettrackerCode.PICK_CODE_60.getCode().equals(resCde)) {
                status = "P2";
            } else {
                status = "P8";
            }
        } else {
            status = "P8";
        }
        if (invoice == null && dunCde != null) {
            invoice = dunCde;
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HHmm");
        String callbackDt = LocalDateTime.parse(trnTim,formatter).toString();
        String dlvCd = mapping.get(comCode).getDlvCd();
        return new PickCallbackSetDto(ordCde,invoice,dlvCd,status,callbackDt);
    }

    public PickProcessInsertSetDto toPickProcessInsertSetDto() {
        return new PickProcessInsertSetDto("반품접수",resNme,"","batch","batch",ordCde);
    }
}
