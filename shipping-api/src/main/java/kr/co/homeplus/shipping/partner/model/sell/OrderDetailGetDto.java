package kr.co.homeplus.shipping.partner.model.sell;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "주문정보 상세 팝업 응답 DTO")
public class OrderDetailGetDto {
    @ApiModelProperty(notes = "주문기본정보")
    private OrderDetailBaseDto orderDetailBase;

    @ApiModelProperty(notes = "주문상품정보")
    private List<OrderDetailItemDto> orderDetailItemList;

    @ApiModelProperty(notes = "배송정보")
    private OrderDetailShipDto orderDetailShip;
}
