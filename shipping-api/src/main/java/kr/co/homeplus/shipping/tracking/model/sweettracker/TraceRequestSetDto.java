package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "스윗트래커 추적요청 파라미터")
public class TraceRequestSetDto {

    @ApiModelProperty(notes = "콜백URL")
    private String callback_url;

    @ApiModelProperty(notes = "tier코드")
    private String tier;

    @ApiModelProperty(notes = "key코드")
    private String key;

    @ApiModelProperty(notes = "목록", dataType = "List")
    private List<TraceRequestItemSetDto> list;

    public TraceRequestSetDto(String callback_url, String tier, String key, List<TraceRequestItemSetDto> list) {
        this.callback_url = callback_url;
        this.tier = tier;
        this.key = key;
        this.list = list;
    }
}
