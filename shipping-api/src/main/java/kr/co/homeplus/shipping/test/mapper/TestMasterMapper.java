package kr.co.homeplus.shipping.test.mapper;

import kr.co.homeplus.shipping.core.db.annotation.MasterConnection;
import kr.co.homeplus.shipping.test.model.qa.QaShipStatusSetDto;

@MasterConnection
public interface TestMasterMapper {
    int manualShipStatus(QaShipStatusSetDto qaShipStatusSetDto);
    int manualPickStatus(QaShipStatusSetDto qaShipStatusSetDto);
    int manualExchStatus(QaShipStatusSetDto qaShipStatusSetDto);
}
