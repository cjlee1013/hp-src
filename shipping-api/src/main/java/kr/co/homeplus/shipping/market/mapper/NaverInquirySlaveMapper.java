package kr.co.homeplus.shipping.market.mapper;

import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shipping.market.model.NaverInquiryAnswerSetDto;
import kr.co.homeplus.shipping.market.model.MarketInquiryInfoDto;

@SlaveConnection
public interface NaverInquirySlaveMapper {
  MarketInquiryInfoDto selectInquiryAnswerInfo(NaverInquiryAnswerSetDto naverInquiryAnswerSetDto);
}
