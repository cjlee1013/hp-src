package kr.co.homeplus.shipping.common.model.history;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송 히스토리 입력")
public class BundleProcessInsertSetDto extends ProcessHistoryInsertSetDto{
    @ApiModelProperty(notes = "번들번호")
    private String bundleNo;

    public BundleProcessInsertSetDto(String historyReason,
        String historyDetailDesc, String historyDetailBefore, String regChannel, String regId, String bundleNo) {
        super(historyReason,historyDetailDesc,historyDetailBefore,regChannel,regId);
        this.bundleNo = bundleNo;
    }
}
