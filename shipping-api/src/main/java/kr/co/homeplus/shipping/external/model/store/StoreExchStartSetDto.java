package kr.co.homeplus.shipping.external.model.store;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "교환 시작 상태 변경")
public class StoreExchStartSetDto extends StoreClaimVaninfoSetDto {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "교환예정시간(yyyy-MM-dd HH:mm:ss)", required = true)
    private String exchStartDt;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "차량ID")
    private String vanId;

}
