package kr.co.homeplus.shipping.tracking.model.exch;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송완료 상태 변경")
public class ExchCallbackSetDto {
    @ApiModelProperty(notes = "번들번호")
    private String claimExchShippingNo;

    @ApiModelProperty(notes = "송장번호")
    private String exchInvoiceNo;

    @ApiModelProperty(notes = "수거상태")
    private String callbackStatus;

    @ApiModelProperty(notes = "처리시간")
    private String callbackDt;

    public ExchCallbackSetDto(String claimExchShippingNo, String exchInvoiceNo, String callbackDt) {
        this.claimExchShippingNo = claimExchShippingNo;
        this.exchInvoiceNo = exchInvoiceNo;
        this.callbackDt = callbackDt;
    }

    public ExchCallbackSetDto(String claimExchShippingNo, String exchInvoiceNo,
        String callbackStatus, String callbackDt) {
        this.claimExchShippingNo = claimExchShippingNo;
        this.exchInvoiceNo = exchInvoiceNo;
        this.callbackStatus = callbackStatus;
        this.callbackDt = callbackDt;
    }

    public String getCallbackStatus() {
        return callbackStatus;
    }
}
