package kr.co.homeplus.shipping.tracking.model.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송조회 응답값")
public class ValidateInvoiceNoGetDto {

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "성공여부")
    private boolean success;

    @ApiModelProperty(notes = "결과값")
    private String detail;
}
