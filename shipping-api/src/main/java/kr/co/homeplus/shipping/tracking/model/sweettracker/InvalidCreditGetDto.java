package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.beans.ConstructorProperties;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "신용코드 확인 파라미터")
public class InvalidCreditGetDto {
    @ApiModelProperty(notes = "목록", dataType = "List")
    private List<InvalidCreditItemGetDto> data;

    public List<InvalidCreditItemGetDto> getData() {
        return data;
    }

    @ConstructorProperties({"list"})
    public InvalidCreditGetDto(
        List<InvalidCreditItemGetDto> data) {
        this.data = data;
    }
}
