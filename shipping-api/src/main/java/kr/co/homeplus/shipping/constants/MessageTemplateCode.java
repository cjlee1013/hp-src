package kr.co.homeplus.shipping.constants;

/**
 * resource api 명을 관리
 * apiId의 url은 application.yml의 plus.resource-routes 에 정의 됨
 */
public class MessageTemplateCode {
    // 배송_택배배송안내 (알림톡)
    public static final String TALK_SHIP_DLV = "T11134";

    // 배송_택배배송안내 (문자)
    public static final String SMS_SHIP_DLV = "S11134";

    // 배송_DS직접배송안내 (알림톡)
    public static final String TALK_SHIP_DS_DRCT = "T11145";

    // 배송_DS직접배송안내 (문자)
    public static final String SMS_SHIP_DS_DRCT = "S11145";

    // 배송_DS우편배송안내 (알림톡)
    public static final String TALK_SHIP_DS_POST = "T11133";

    // 배송_DS우편배송안내 (문자)
    public static final String SMS_SHIP_DS_POST = "S11133";

    // 배송_추석선물세트선물메세지 (알림톡)
    public static final String TALK_SHIP_BSD = "T11201";

    // 배송_추석선물세트선물메세지 (문자)
    public static final String SMS_SHIP_BSD = "S11201";

    // 배송_추석선물세트-메세지없음 (알림톡)
    public static final String TALK_SHIP_BSD_WO_MSG = "T11200";

    // 배송_추석선물세트-메세지없음 (문자)
    public static final String SMS_SHIP_BSD_WO_MSG = "S11200";

    // 배송_추석선물세트 주문자 (알림톡)
    public static final String TALK_BUYER_BSD = "T11203";

    // 배송_추석선물세트 주문자 (문자)
    public static final String SMS_BUYER_BSD = "S11203";

    // 배송_수취확인 요청_DS (알림톡)
    public static final String TALK_SHIP_CONFIRM_RECEIVE = "T11100";

    // 배송_수취확인 요청_DS (문자)
    public static final String SMS_SHIP_CONFIRM_RECEIVE = "S11100";

    // 배송_ DS배송 지연안내 (알림톡)
    public static final String TALK_SHIP_DELAY = "T11098";

    // 배송_ DS배송 지연안내 (문자)
    public static final String SMS_SHIP_DELAY = "S11098";

    // 배송_TD자차_선물메세지  (알림톡)
    public static final String TALK_SHIP_TD_DRCT = "T11215";

    // 배송_TD자차_선물메세지 (문자)
    public static final String SMS_SHIP_TD_DRCT = "S11215";
}
