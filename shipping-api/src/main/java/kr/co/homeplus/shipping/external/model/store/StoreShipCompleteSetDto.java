package kr.co.homeplus.shipping.external.model.store;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송완료 상태 변경")
public class StoreShipCompleteSetDto extends StoreShipStartSetDto {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "배송완료시간(yyyy-MM-dd HH:mm:ss)")
    private String completeRegDt;

    @ApiModelProperty(notes = "배송번호")
    private String shipNo;
}
