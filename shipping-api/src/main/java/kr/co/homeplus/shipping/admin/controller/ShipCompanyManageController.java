package kr.co.homeplus.shipping.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.admin.model.shipManage.ReturnDlvCompanyListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipLinkCompanyCodeListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipLinkCompanySetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipLinkCompanySetListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipCompanyManageInfoSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipCompanyManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipCompanyManageListSetDto;
import kr.co.homeplus.shipping.admin.service.ShipCompanyManageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/shipManage")
@Api(tags = "어드민 - 택배사코드관리")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class ShipCompanyManageController {
  private final ShipCompanyManageService shipCompanyManageService;

  @ApiOperation(value = "택배사코드관리 리스트 조회")
  @PostMapping("/getShipCompanyManageList")
  public ResponseObject<List<ShipCompanyManageListGetDto>> getShipCompanyManageList(@RequestBody @Valid ShipCompanyManageListSetDto shipCompanyManageListSetDto) {
    return ResourceConverter.toResponseObject(shipCompanyManageService.getShipCompanyManageList(shipCompanyManageListSetDto));
  }

  @ApiOperation(value = "택배사코드관리 상세정보 저장")
  @PostMapping("/setShipCompanyInfo")
  public ResponseObject<String> setShipCompanyInfo(@RequestBody @Valid ShipCompanyManageInfoSetDto shipCompanyManageInfoSetDto) {
      return shipCompanyManageService.setShipCompanyInfo(shipCompanyManageInfoSetDto);
  }

  @ApiOperation(value = "연동업체설정 리스트 조회")
  @GetMapping("/getShipLinkCompanySetList")
  public ResponseObject<List<ShipLinkCompanySetListGetDto>> getShipLinkCompanySetList() {
    return ResourceConverter.toResponseObject(shipCompanyManageService.getShipLinkCompanySetList());
  }

  @ApiOperation(value = "연동업체설정 저장")
  @PostMapping("/setShipLinkCompanySet")
  public ResponseObject<String> setShipLinkCompanySet(@RequestBody @Valid ShipLinkCompanySetDto shipLinkCompanySetDto) {
    return shipCompanyManageService.setShipLinkCompanySet(shipLinkCompanySetDto);
  }

  @ApiOperation(value = "연동업체코드 리스트 조회")
  @GetMapping("/getLinkCompanyCodeList")
  public ResponseObject<List<ShipLinkCompanyCodeListGetDto>> getLinkCompanyCodeList(
      @ApiParam(name = "company", value = "연동업체", required = false, defaultValue = "") @RequestParam String company) {
    return ResourceConverter.toResponseObject(shipCompanyManageService.getLinkCompanyCodeList(company));
  }

  @ApiOperation(value = "반품택배사 리스트 조회")
  @GetMapping("/getReturnDlvCompanyList")
  public ResponseObject<List<ReturnDlvCompanyListGetDto>> getReturnDlvCompanyList() {
    return ResourceConverter.toResponseObject(shipCompanyManageService.getReturnDlvCompanyList());
  }

}
