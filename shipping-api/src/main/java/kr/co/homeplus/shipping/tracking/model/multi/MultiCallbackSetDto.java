package kr.co.homeplus.shipping.tracking.model.multi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송완료 상태 변경")
public class MultiCallbackSetDto {
    @ApiModelProperty(notes = "다중주문아이템번호")
    private String multiOrderItemNo;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "수거상태")
    private String callbackStatus;

    @ApiModelProperty(notes = "배송완료시간(yyyy-MM-dd HH:mm:ss)")
    private String callbackDt;

    public MultiCallbackSetDto(String multiOrderItemNo ,String invoiceNo, String callbackDt) {
        this.multiOrderItemNo = multiOrderItemNo;
        this.invoiceNo = invoiceNo;
        this.callbackDt = callbackDt;
    }

    public MultiCallbackSetDto(String multiOrderItemNo, String invoiceNo, String callbackStatus,
        String callbackDt) {
        this.multiOrderItemNo = multiOrderItemNo;
        this.invoiceNo = invoiceNo;
        this.callbackStatus = callbackStatus;
        this.callbackDt = callbackDt;
    }

    public String getCallbackStatus() {
        return callbackStatus;
    }
}
