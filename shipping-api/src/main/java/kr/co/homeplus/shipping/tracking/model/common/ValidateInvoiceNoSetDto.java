package kr.co.homeplus.shipping.tracking.model.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송조회 파라미터")
public class ValidateInvoiceNoSetDto {

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;
}
