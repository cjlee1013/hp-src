package kr.co.homeplus.shipping.admin.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "반품택배사 리스트 조회 리스트 응답 DTO")
public class ReturnDlvCompanyListGetDto {
    @ApiModelProperty(notes = "연동업체코드")
    private String dlvCd;

    @ApiModelProperty(notes = "연동업체코드명")
    private String dlvNm;
}