package kr.co.homeplus.shipping.mypage.mapper;

import java.util.List;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shipping.mypage.model.order.ShippingInvoiceGetDto;
import kr.co.homeplus.shipping.mypage.model.order.ShippingHistoryGetDto;
import org.apache.ibatis.annotations.Param;

@SlaveConnection
public interface OrderSlaveMapper {
  String getShipInfo(String shipNo);
  ShippingInvoiceGetDto getShippingInvoice(String shipNo);
  ShippingInvoiceGetDto getPickInvoice(String claimPickShippingNo);
  ShippingInvoiceGetDto getExchInvoice(String claimExchShippingNo);
  List<ShippingHistoryGetDto> getInvoiceHistory(@Param("dlvCd") String dlvCd, @Param("invoiceNo") String invoiceNo);
  ShippingInvoiceGetDto getMultiShippingInvoice(String multiOrderItemNo);
}