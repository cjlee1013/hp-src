package kr.co.homeplus.shipping.admin.mapper;

import kr.co.homeplus.shipping.admin.model.shipManage.ShipLinkCompanySetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipCompanyManageInfoSetDto;
import kr.co.homeplus.shipping.core.db.annotation.MasterConnection;

@MasterConnection
public interface ShipCompanyManageMasterMapper {
  int insertShipCompanyInfo(ShipCompanyManageInfoSetDto shipCompanyManageInfoSetDto);
  int insertShipLinkCompanySet(ShipLinkCompanySetDto shipLinkCompanySetDto);
}
