package kr.co.homeplus.shipping.admin.mapper;

import java.util.List;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringDelayGetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringDelaySetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringPickGetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringPickSetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringShipGetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringShipSetDto;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface MonitoringSlaveMapper {
  List<MonitoringDelayGetDto> selectMonitoringDelayList(MonitoringDelaySetDto monitoringDelaySetDto);
  List<MonitoringShipGetDto> selectMonitoringShipList(MonitoringShipSetDto monitoringShipSetDto);
  List<MonitoringPickGetDto> selectMonitoringPickList(MonitoringPickSetDto monitoringPickSetDto);
}