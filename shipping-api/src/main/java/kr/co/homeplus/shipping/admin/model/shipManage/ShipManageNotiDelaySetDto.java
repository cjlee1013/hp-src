package kr.co.homeplus.shipping.admin.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 배송관리 발송지연안내 요청 파라미터")
public class ShipManageNotiDelaySetDto {
    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;

    @ApiModelProperty(notes = "수정자")
    private String chgId;

    @ApiModelProperty(notes = "발송지연사유코드")
    private String delayShipCd;

    @ApiModelProperty(notes = "발송지연사유메시지")
    private String delayShipMsg;

    @ApiModelProperty(notes = "2차발송기한")
    private String delayShipDt;
}
