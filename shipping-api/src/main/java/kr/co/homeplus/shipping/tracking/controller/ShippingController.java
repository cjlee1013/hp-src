package kr.co.homeplus.shipping.tracking.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.common.model.history.ShippingHistoryInsertSetDto;
import kr.co.homeplus.shipping.common.service.HistoryService;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.tracking.config.SweettrackerConfig;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingResultSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceCallbackGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceRequestItemGetDto;
import kr.co.homeplus.shipping.tracking.service.SweettrackerService;
import kr.co.homeplus.shipping.tracking.service.TrackingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/tracking/shipping")
@Api(tags = "배송 추적 및 조회")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class ShippingController {
  private final SweettrackerService sweettrackerService;
  private final TrackingService trackingService;
  private final HistoryService historyService;

  @ApiOperation(value = "배송 추적 요청", response = ResponseObject.class, notes = "필수값 : 배송번호, 송장번호, 택배사코드")
  @RequestMapping(value = "/requestTracking", method = {RequestMethod.POST})
  public ResponseObject<List<ShippingRequestGetDto>> requestTracking(@RequestBody @Valid List<ShippingRequestSetDto> shippingTrackingGetDtoList) {
    if (shippingTrackingGetDtoList.size() > 0) {
      Map<String,ShippingTrackingGetDto> shippingTrackingGetDtoMap = trackingService.getShippingInvoice(shippingTrackingGetDtoList);
      if (shippingTrackingGetDtoMap != null) {
        List<ShippingRequestGetDto> shippingRequestGetDtoList = new ArrayList<>();
        ShippingResultSetDto shippingResultSetDto;
        String callbackUrl = "/tracking/shipping/callbackTrackingSWT";
        TraceRequestGetDto traceRequestGetDto = sweettrackerService.requestTracking(callbackUrl,
            shippingTrackingGetDtoMap.values());
        if (traceRequestGetDto != null) {
          for (TraceRequestItemGetDto item : traceRequestGetDto.getList()) {
            shippingResultSetDto = new ShippingResultSetDto(item.getFid(), item.getSuccess(),
                item.getDetail(),item.getE_code() +":"+ item.getE_message());
            shippingRequestGetDtoList.addAll(
                trackingService.setShippingTrackingResult(shippingResultSetDto,
                    shippingTrackingGetDtoMap));
          }
          return ResourceConverter.toResponseObject(shippingRequestGetDtoList);
        }
      }
    }
    return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.ERROR_CODE_2001.getResponseCode(), ExceptionCode.ERROR_CODE_2001.getResponseMessage());
  }

  @ApiOperation(value = "배송 추적 콜백", response = ResponseObject.class, notes = "스윗트래커에서 호출")
  @RequestMapping(value = "/callbackTrackingSWT", method = {RequestMethod.POST})
  public TraceCallbackGetDto callbackTrackingSWT(TraceCallbackSetDto traceCallbackSetDto) {
    ShippingHistoryInsertSetDto shippingHistoryInsertSetDto = traceCallbackSetDto.toShippingHistoryInsertSetDto(sweettrackerService.getDlvCdMappingStoH());
    if (shippingHistoryInsertSetDto == null){
      log.info(traceCallbackSetDto.toString());
      return new TraceCallbackGetDto("false", "fail - invalid courier code");
    }
    int historyResult = historyService.setShippingHistoryInvoice(shippingHistoryInsertSetDto);
    if (historyResult > 0) {
      ShippingCallbackSetDto shippingCallbackSetDto = traceCallbackSetDto.toShippingCallbackSetDto();
      int callbackResult = 0;
      if (shippingCallbackSetDto != null) {
        historyService.setShippingProcessHistory(traceCallbackSetDto.toShippingProcessInsertSetDto());
        callbackResult = trackingService.setShippingTrackingCallback(shippingCallbackSetDto);
        if (callbackResult > 0) {
          return new TraceCallbackGetDto("true", "success");
        } else {
          return new TraceCallbackGetDto("false", "fail - invalid fid");
        }
      } else {
        return new TraceCallbackGetDto("true", "success");
      }
    }
    return new TraceCallbackGetDto("false", "system error");
  }
}
