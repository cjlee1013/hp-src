package kr.co.homeplus.shipping.partner.service;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageCounselCompleteSetDto;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageCounselReservationSetDto;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageListSetDto;

public interface RentalManageService {
  List<RentalManageListGetDto> getRentalManageList(RentalManageListSetDto rentalManageListSetDto);
  List<RentalManageListGetDto> getRentalManageCntList(String schType, String partnerId);
  RentalManageCntGetDto getRentalManageCnt(String partnerId);
  ResponseObject<String> setCounselReservation(RentalManageCounselReservationSetDto rentalManageCounselReservationSetDto);
  ResponseObject<String> setCounselComplete(RentalManageCounselCompleteSetDto rentalManageCounselCompleteSetDto);
}