package kr.co.homeplus.shipping.partner.model.sell.ticketManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 이티켓판매관리 발급완료 요청 Argument")
public class TicketManageCompleteArgDto {
    @ApiModelProperty(notes = "배송SEQ")
    private String shipNo;

    @ApiModelProperty(notes = "주문티켓번호")
    private List<String> orderTicketNoList;
}