package kr.co.homeplus.shipping.admin.model.statistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StatisticsStoreGetDto {
  @ApiModelProperty(notes = "점포")
  private String storeNm;
  @ApiModelProperty(notes = "점포코드")
  private String storeId;
  @ApiModelProperty(notes = "주문건수")
  private String orderQty;
  @ApiModelProperty(notes = "주문금액")
  private String orderPrice;
  @ApiModelProperty(notes = "취소건수")
  private String cancelQty;
  @ApiModelProperty(notes = "취소금액")
  private String cancelPrice;
  @ApiModelProperty(notes = "반품건수")
  private String returnQty;
  @ApiModelProperty(notes = "반품금액")
  private String returnPrice;
  @ApiModelProperty(notes = "대체건수")
  private String subQty;
  @ApiModelProperty(notes = "대체금액")
  private String subPrice;
}
