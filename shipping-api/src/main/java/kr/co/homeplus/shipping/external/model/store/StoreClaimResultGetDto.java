package kr.co.homeplus.shipping.external.model.store;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "옴니연동 클레임 공통 반환값")
public class StoreClaimResultGetDto {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "클레임번들번호")
    private String claimBundleNo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "성공여부")
    private boolean success;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "결과값")
    private String detail;

    public StoreClaimResultGetDto(String claimBundleNo, boolean success) {
        this.claimBundleNo = claimBundleNo;
        this.success = success;
    }

    public StoreClaimResultGetDto(String claimBundleNo, boolean success, String detail) {
        this.claimBundleNo = claimBundleNo;
        this.success = success;
        this.detail = detail;
    }
}
