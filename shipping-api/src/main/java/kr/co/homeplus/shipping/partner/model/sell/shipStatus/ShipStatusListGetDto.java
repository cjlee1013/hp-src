package kr.co.homeplus.shipping.partner.model.sell.shipStatus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.shipping.partner.model.sell.orderShipManage.OrderShipManageListGetDto;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 배송현황 리스트 응답 DTO")
public class ShipStatusListGetDto extends OrderShipManageListGetDto {
  @ApiModelProperty(notes = "미수취신고처리유형")
  private String noRcvProcessNm;

  @ApiModelProperty(notes = "구매확정일")
  private String decisionDt;

  @ApiModelProperty(notes = "클레임가능여부")
  private String claimYn;
}