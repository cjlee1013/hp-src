package kr.co.homeplus.shipping.admin.service;

import java.util.List;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringDelayGetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringDelaySetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringPickGetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringPickSetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringShipGetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringShipSetDto;

public interface MonitoringService {
    List<MonitoringDelayGetDto> getMonitoringDelayList(MonitoringDelaySetDto monitoringDelaySetDto);
    List<MonitoringShipGetDto> getMonitoringShipList(MonitoringShipSetDto monitoringShipSetDto);
    List<MonitoringPickGetDto> getMonitoringPickList(MonitoringPickSetDto monitoringPickSetDto);
}
