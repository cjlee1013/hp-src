package kr.co.homeplus.shipping.tracking.model.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class CommonResultSetDto {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "성공여부")
    private String success;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "결과값")
    private String result;

    @ApiModelProperty(notes = "상세")
    private String detail;

    public CommonResultSetDto(String success,
        String result, String detail) {
        this.success = success;
        this.result = result;
        this.detail = detail;
    }

    public String getSuccess() {
        return success;
    }

    public String getResult() {
        return result;
    }
}
