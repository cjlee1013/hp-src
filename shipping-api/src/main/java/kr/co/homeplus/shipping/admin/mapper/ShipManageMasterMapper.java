package kr.co.homeplus.shipping.admin.mapper;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllExcelSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllPopListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipCompletePopListGetDto;
import kr.co.homeplus.shipping.core.db.annotation.MasterConnection;
import org.apache.ibatis.annotations.Param;

@MasterConnection
public interface ShipManageMasterMapper {
  int updateConfirmOrder(@Param("bundleNo") String bundleNo, @Param("chgId") String chgId);
  int updateNotiDelay(@Param("bundleNo") String bundleNo, @Param("chgId") String chgId, @Param("delayShipCd") String delayShipCd, @Param("delayShipMsg") String delayShipMsg, @Param("delayShipDt") String delayShipDt);
  int updateShipAllExcel(ShipManageShipAllExcelSetDto shipManageShipAllExcelSetDto);
  int updateShipAll(ShipManageShipAllSetDto shipManageShipAllSetDto);
  int updateShipComplete(@Param("bundleNo") String bundleNo, @Param("chgId") String chgId);
  int updatePurchaseCompleteByBundle(@Param("bundleNo") String bundleNo, @Param("chgId") String chgId);

  Map<String,Object> selectShipInfoByOrderItemNo(@Param("orderItemNo") String orderItemNo);
  List<Map<String,Object>> selectShipInfoByBundleNo(@Param("bundleNo") String bundleNo);
  List<ShipManageShipAllPopListGetDto> selectShipAllPopList(@Param("bundleNo") String bundleNo);
  List<ShipManageShipAllPopListGetDto> selectShipAllPopListExClaim(@Param("bundleNo") String bundleNo);
  List<ShipManageShipCompletePopListGetDto> selectShipCompleteList(@Param("bundleNo") String bundleNo, @Param("partnerId") String partnerId);
}
