package kr.co.homeplus.shipping.common.model.history;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송 히스토리 입력")
public class StoreProcessInsertSetDto extends ProcessHistoryInsertSetDto{
    @ApiModelProperty(notes = "구매점포번호")
    private String purchaseStoreInfoNo;

    public StoreProcessInsertSetDto(String historyReason,
        String historyDetailDesc, String historyDetailBefore, String regChannel, String regId, String purchaseStoreInfoNo) {
        super(historyReason,historyDetailDesc,historyDetailBefore,regChannel,regId);
        this.purchaseStoreInfoNo = purchaseStoreInfoNo;
    }
}
