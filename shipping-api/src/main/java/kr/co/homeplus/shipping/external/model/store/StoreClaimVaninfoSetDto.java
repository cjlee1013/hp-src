package kr.co.homeplus.shipping.external.model.store;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "차량 정보")
public class StoreClaimVaninfoSetDto extends StoreClaimCommonSetDto {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "차량번호")
    private String vanNo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "배송기사명")
    private String driverNm;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "배송기사전화번호")
    private String driverTelNo;

    public String getVanNo() { return vanNo; }

    public String getDriverNm() { return driverNm; }

    public String getDriverTelNo() {
        return driverTelNo;
    }

    public void setDriverTelNo(String driverTelNo) {
        this.driverTelNo = driverTelNo;
    }
}
