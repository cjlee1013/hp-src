package kr.co.homeplus.shipping.admin.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.shipping.admin.mapper.StatisticsSlaveMapper;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsPartnerGetDto;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsPartnerSetDto;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsStoreGetDto;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsStoreSetDto;
import kr.co.homeplus.shipping.admin.service.StatisticsService;
import kr.co.homeplus.shipping.tracking.mapper.TrackingMasterMapper;
import kr.co.homeplus.shipping.tracking.mapper.TrackingSlaveMapper;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdResultSetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.common.DlvCdMappingGetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchResultSetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiResultSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickAddressGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickResultSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingResultSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingTrackingGetDto;
import kr.co.homeplus.shipping.tracking.service.TrackingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class StatisticsServiceImpl implements StatisticsService {
  private final StatisticsSlaveMapper statisticsSlaveMapper;

  @Override
  public List<StatisticsPartnerGetDto> getPartnerPayment(
      StatisticsPartnerSetDto statisticsPartnerSetDto) {
    return statisticsSlaveMapper.getPartnerPayment(statisticsPartnerSetDto);
  }

  @Override
  public List<StatisticsStoreGetDto> getStorePayment(
      StatisticsStoreSetDto statisticsStoreSetDto) {
    return statisticsSlaveMapper.getStorePayment(statisticsStoreSetDto);
  }

  @Override
  public List<StatisticsPartnerGetDto> getPartnerDecision(
      StatisticsPartnerSetDto statisticsPartnerSetDto) {
    return statisticsSlaveMapper.getPartnerDecision(statisticsPartnerSetDto);
  }

  @Override
  public List<StatisticsStoreGetDto> getStoreDecision(
      StatisticsStoreSetDto statisticsStoreSetDto) {
    return statisticsSlaveMapper.getStoreDecision(statisticsStoreSetDto);
  }
}
