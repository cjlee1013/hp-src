package kr.co.homeplus.shipping.admin.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 점포배송관리 리스트 응답 DTO")
public class ShipStoreManageListGetDto {
    @ApiModelProperty(notes = "몰유형")
    private String mallType;

    @ApiModelProperty(notes = "점포유형")
    private String storeType;

    @ApiModelProperty(notes = "점포")
    private String storeNm;

    @ApiModelProperty(notes = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;

    @ApiModelProperty(notes = "옴니주문번호")
    private String purchaseStoreInfoNo;

    @ApiModelProperty(notes = "주문상태")
    private String shipStatusNm;

    @ApiModelProperty(notes = "주문상태코드")
    private String shipStatus;

    @ApiModelProperty(notes = "주문일시")
    private String orderDt;

    @ApiModelProperty(notes = "배송요청일")
    private String reserveShipDt;

    @ApiModelProperty(notes = "배송Shift")
    private String shiftId;

    @ApiModelProperty(notes = "단축번호")
    private String sordNo;

    @ApiModelProperty(notes = "배송시간")
    private String slotShipTime;

    @ApiModelProperty(notes = "구매자")
    private String buyerNm;

    @ApiModelProperty(notes = "구매자 연락처")
    private String buyerMobileNo;

    @ApiModelProperty(notes = "법인")
    private String corpOrder;

    @ApiModelProperty(notes = "수령인")
    private String receiverNm;

    @ApiModelProperty(notes = "수령인 연락처")
    private String shipMobileNo;

    @ApiModelProperty(notes = "배송방법")
    private String shipMethodNm;

    @ApiModelProperty(notes = "배송방법코드")
    private String shipMethod;

    @ApiModelProperty(notes = "우편번호")
    private String zipcode;

    @ApiModelProperty(notes = "주소")
    private String addr;

    @ApiModelProperty(notes = "배송메세지")
    private String dlvShipMsg;

    @ApiModelProperty(notes = "결제일시")
    private String paymentFshDt;

    @ApiModelProperty(notes = "마켓유형")
    private String marketType;

    @ApiModelProperty(notes = "마켓주문번호")
    private String marketOrderNo;

    @ApiModelProperty(notes = "번들수량")
    private String bundleQty;
}
