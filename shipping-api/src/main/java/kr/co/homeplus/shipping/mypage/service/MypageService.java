package kr.co.homeplus.shipping.mypage.service;

import java.util.List;
import kr.co.homeplus.shipping.mypage.model.order.PersonalGetDto;
import kr.co.homeplus.shipping.mypage.model.order.PersonalSetDto;
import kr.co.homeplus.shipping.mypage.model.order.ShippingInvoiceGetDto;
import kr.co.homeplus.shipping.mypage.model.order.ShippingHistoryGetDto;

public interface MypageService {
    int manualShipComplete(String shipNo);
    int manualPurchaseComplete(String shipNo);
    ShippingInvoiceGetDto getShippingHistory(String shipNo);
    ShippingInvoiceGetDto getPickHistory(String claimPickShippingNo);
    ShippingInvoiceGetDto getExchHistory(String claimExchShippingNo);
    List<ShippingHistoryGetDto> getInvoiceHistory(String dlvCd, String invoiceNo);
    List<PersonalGetDto> getShipInfo(PersonalSetDto personalSetDto);
    int getShipInfoCnt(PersonalSetDto personalSetDto);
    int manualMultiShipComplete(String multiBundleNo);
    int manualMultiPurchaseComplete(String multiBundleNo);
    ShippingInvoiceGetDto getMultiShippingHistory(String multiOrderItemNo);
}
