package kr.co.homeplus.shipping.admin.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "BSD 송장 정보")
public class BsdInvoiceSetDto {
  @ApiModelProperty(notes = "배송번호")
  private String bundleNo;
}