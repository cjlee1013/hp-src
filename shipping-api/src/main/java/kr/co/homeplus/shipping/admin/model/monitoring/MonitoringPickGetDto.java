package kr.co.homeplus.shipping.admin.model.monitoring;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "회수지연조회 응답")
public class MonitoringPickGetDto {
    @ApiModelProperty(notes = "그룹클레임번호")
    private String claimNo;
    @ApiModelProperty(notes = "클레임번호")
    private String claimBundleNo;
    @ApiModelProperty(notes = "클레임구분")
    private String claimTypeNm;
    @ApiModelProperty(notes = "클레임구분코드")
    private String claimType;
    @ApiModelProperty(notes = "처리상태")
    private String claimStatusNm;
    @ApiModelProperty(notes = "처리상태코드")
    private String claimStatus;
    @ApiModelProperty(notes = "마켓유형")
    private String marketType;
    @ApiModelProperty(notes = "마켓주문번호")
    private String marketOrderNo;
    @ApiModelProperty(notes = "클레임신청일시")
    private String requestDt;
    @ApiModelProperty(notes = "수거상태")
    private String pickStatusNm;
    @ApiModelProperty(notes = "수거상태코드")
    private String pickStatus;
    @ApiModelProperty(notes = "수거예정일")
    private String shipDt;
    @ApiModelProperty(notes = "수거지연일")
    private String pickDelayDay;
    @ApiModelProperty(notes = "수거방법")
    private String pickShipType;
    @ApiModelProperty(notes = "택배사")
    private String pickDlvNm;
    @ApiModelProperty(notes = "택배사코드")
    private String pickDlvCd;
    @ApiModelProperty(notes = "송장번호")
    private String pickInvoiceNo;
    @ApiModelProperty(notes = "주문번호")
    private String purchaseOrderNo;
    @ApiModelProperty(notes = "판매자명")
    private String partnerNm;
    @ApiModelProperty(notes = "주문일시")
    private String orderDt;
    @ApiModelProperty(notes = "회원번호")
    private String userNo;
    @ApiModelProperty(notes = "회원번호마스킹")
    private String userNoMask;
    @ApiModelProperty(notes = "구매자")
    private String buyerNm;
    @ApiModelProperty(notes = "수령인")
    private String pickReceiverNm;
    @ApiModelProperty(notes = "수령인연락처")
    private String pickMobileNo;
    @ApiModelProperty(notes = "우편번호")
    private String pickZipcode;
    @ApiModelProperty(notes = "주소")
    private String pickAddr;
    @ApiModelProperty(notes = "점포유형")
    private String storeType;
    @ApiModelProperty(notes = "사이트유형")
    private String siteType;
}
