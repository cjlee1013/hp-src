package kr.co.homeplus.shipping.external.model.store;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송 차량 정보 입력")
public class StoreShipVaninfoSetDto extends StoreShipCommonSetDto {
    @ApiModelProperty(notes = "차량번호")
    private String vanNo;

    @ApiModelProperty(notes = "배송기사명")
    private String driverNm;

    @ApiModelProperty(notes = "배송기사전화번호")
    private String driverTelNo;

    @ApiModelProperty(notes = "차량벤더정보")
    private String vanVendorNm;

    @ApiModelProperty(notes = "단축번호")
    private String sordNo;

    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;
}
