package kr.co.homeplus.shipping.partner.model.sell.orderShipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 발주/발송관리 카운트 응답 DTO")
public class OrderShipManageCntGetDto {
    @ApiModelProperty(notes = "신규주문 건수")
    private String newCnt;

    @ApiModelProperty(notes = "상품준비중 건수")
    private String readyCnt;

    @ApiModelProperty(notes = "신규주문지연 건수")
    private String newDelayCnt;

    @ApiModelProperty(notes = "발송지연 건수")
    private String shipDelayCnt;

    @ApiModelProperty(notes = "발송전지연안내 건수")
    private String shipDelayNotiCnt;

    @ApiModelProperty(notes = "오늘발송요청 건수")
    private String shipTodayCnt;

    @ApiModelProperty(notes = "발송전취소요청 건수")
    private String claimCnt;
}