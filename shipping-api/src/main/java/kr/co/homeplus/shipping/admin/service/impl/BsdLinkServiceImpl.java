package kr.co.homeplus.shipping.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.shipping.admin.mapper.BsdLinkMasterMapper;
import kr.co.homeplus.shipping.admin.mapper.BsdLinkSlaveMapper;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdInvoiceSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdLinkListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdLinkListSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdLinkTranSetDto;
import kr.co.homeplus.shipping.admin.service.BsdLinkService;
import kr.co.homeplus.shipping.common.model.history.BundleProcessInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.MultiShippingProcessInsertSetDto;
import kr.co.homeplus.shipping.common.service.HistoryService;
import kr.co.homeplus.shipping.common.service.MessageSendService;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.utils.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class BsdLinkServiceImpl implements BsdLinkService {
    private final BsdLinkMasterMapper bsdLinkMasterMapper;
    private final BsdLinkSlaveMapper bsdLinkSlaveMapper;
    private final MessageSendService messageSendService;
    private final HistoryService historyService;

    /**
     * BSD연동조회
     */
    @Override
    public List<BsdLinkListGetDto> getBsdLinkList(BsdLinkListSetDto bsdLinkListSetDto) {
      List<BsdLinkListGetDto> bsdLinkListGetDtoList = bsdLinkSlaveMapper.selectBsdLinkList(bsdLinkListSetDto);

      for (BsdLinkListGetDto dto : bsdLinkListGetDtoList) {
        dto.setBuyerNm(PrivacyMaskingUtils.maskingUserName(dto.getBuyerNm()));
        dto.setReceiverNm(PrivacyMaskingUtils.maskingUserName(dto.getReceiverNm()));
        dto.setShipAddr(PrivacyMaskingUtils.maskingUserName(dto.getShipAddr()));
      }

      return bsdLinkListGetDtoList;
    }

    /**
     * BSD전송
     */
    @Override
    public ResponseObject<String> setBsdLinkTran(BsdLinkTranSetDto bsdLinkTranSetDto) {
      int returnVal = 0;

      try {
        returnVal = bsdLinkMasterMapper.insertBsdLinkTran(bsdLinkTranSetDto);

        // 전송된 데이터가 있으면..
        if (returnVal > 0) {
          returnVal = bsdLinkMasterMapper.updateBsdLinkResult(bsdLinkTranSetDto);

          // 전송결과 업데이트 성공
          if (returnVal > 0) {
            log.info("setBsdLinkTran result : {}", returnVal);
            return ResponseUtil.getResoponseObject(Integer.toString(returnVal));
          } else {
            return ResponseUtil.getResoponseObject(ExceptionCode.ERROR_CODE_3016);
          }
        } else {
          // 요청한 전송건이 있었다면 INSERT ERROR
          if ( bsdLinkTranSetDto.getBsdLinkNoList().size() > 0 ) {
            return ResponseUtil.getResoponseObject(ExceptionCode.ERROR_CODE_3015);
          } else {
            return ResponseUtil.getResoponseObject(Integer.toString(returnVal));
          }
        }
      } catch (Exception e) {
        log.error("setBsdLinkTran Error : {}", e.getMessage());
        return ResponseUtil.getResoponseObject(ExceptionCode.ERROR_CODE_3015);
      }
    }

  @Override
  public ResponseObject<List<String>> setShippingBsdInvoice(List<BsdInvoiceSetDto> bsdInvoiceSetDtoList) {
      List<String> bundleNoList = new ArrayList<>();
      int retVal;
      String bundleNo;
      for (BsdInvoiceSetDto bsdInvoiceSetDto : bsdInvoiceSetDtoList) {
        bundleNo = bsdInvoiceSetDto.getBundleNo();
        retVal = bsdLinkMasterMapper.updateShippingInvoice(bsdInvoiceSetDto);
        if (retVal > 0) {
          bundleNoList.add(bundleNo);
          historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("선물세트 발송처리 완료","배송중","상품준비중","batch","shipping",bundleNo));
          messageSendService.sendMessageBuyerBsd(bundleNo);
          messageSendService.sendMessageShipBSD(bundleNo);
        } else {
          historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("선물세트 발송처리 실패","상품준비중","상품준비중","batch","shipping",bundleNo));
        }
      }
      return ResponseUtil.getResoponseObject(bundleNoList);
  }

  @Override
  public ResponseObject<List<String>> setMultiOrderBsdInvoice(List<BsdInvoiceSetDto> bsdInvoiceSetDtoList) {
    List<String> bundleNoList = new ArrayList<>();
    int retVal;
    String bundleNo;
    for (BsdInvoiceSetDto bsdInvoiceSetDto : bsdInvoiceSetDtoList) {
      bundleNo = bsdInvoiceSetDto.getBundleNo();
      retVal = bsdLinkMasterMapper.updateMultiOrderInvoice(bsdInvoiceSetDto);
      if (retVal > 0) {
        bundleNoList.add(bundleNo);
        historyService.setMultiShippingProcessHistory(new MultiShippingProcessInsertSetDto("다중배송 발송처리 완료","배송중","상품준비중","batch","shipping",bundleNo));
        messageSendService.sendMessageMultiShipBSD(bundleNo);
      } else {
        historyService.setMultiShippingProcessHistory(new MultiShippingProcessInsertSetDto("다중배송 발송처리 실패","배송중","상품준비중","batch","shipping",bundleNo));
      }
    }
    return ResponseUtil.getResoponseObject(bundleNoList);
  }
}
