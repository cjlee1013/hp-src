package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "배송조회 응답값")
public class TraceInfoGetDto {

    @ApiModelProperty(notes = "발신인 이름")
    private String senderName;

    @ApiModelProperty(notes = "수신자 이름")
    private String exchrName;

    @ApiModelProperty(notes = "상품 이름")
    private String itemName;

    @ApiModelProperty(notes = "운송장 번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "수신자 주소")
    private String exchrAddr;

    @ApiModelProperty(notes = "주문번호")
    private String orderNumber;

    @ApiModelProperty(notes = "광고용 주소")
    private String adUrl;

    @ApiModelProperty(notes = "배송 예정시간")
    private String estimate;

    @ApiModelProperty(notes = "진행단계")
    private String level;

    @ApiModelProperty(notes = "배송완료 여부(Y,N)")
    private String complete;

    @ApiModelProperty(notes = "수령인정보")
    private String recipient;

    @ApiModelProperty(notes = "상품이미지 url")
    private String itemImage;

    @ApiModelProperty(notes = "배송 상세정보", dataType = "List")
    private List<SweettrackerInfoDetailSetDto> trackingDetails;

    @ApiModelProperty(notes = "상품정")
    private String productInfo;

    @ApiModelProperty(notes = "마지막 배송 정보")
    private SweettrackerInfoDetailSetDto lastDetail;

    @ApiModelProperty(notes = "마지막 배송 상태 상세정보")
    private SweettrackerInfoDetailSetDto lastStateDetail;

    @ApiModelProperty(notes = "첫 배송 상태 상세정보")
    private SweettrackerInfoDetailSetDto firstDetail;

    @ApiModelProperty(notes = "배송완료여부(Y,N)")
    private String completeYn;

    class SweettrackerInfoDetailSetDto{
        @ApiModelProperty(notes = "진행시간")
        private String time;

        @ApiModelProperty(notes = "진행시간 문자타입")
        private String timeString;

        @ApiModelProperty(notes = "배송상태코드")
        private String code;

        @ApiModelProperty(notes = "진행위치(지점)")
        private String where;

        @ApiModelProperty(notes = "진행상태")
        private String kind;

        @ApiModelProperty(notes = "진행위치(지점)전화번호")
        private String telno;

        @ApiModelProperty(notes = "배송기사 전화번호")
        private String telno2;

        @ApiModelProperty(notes = "비고")
        private String remark;

        @ApiModelProperty(notes = "진행단계")
        private String level;
    }
}
