package kr.co.homeplus.shipping.tracking.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.common.model.history.CommonHistoryInsertSetDto;
import kr.co.homeplus.shipping.common.service.HistoryService;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.tracking.config.SweettrackerConfig;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdResultSetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceCallbackGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceRequestItemGetDto;
import kr.co.homeplus.shipping.tracking.service.SweettrackerService;
import kr.co.homeplus.shipping.tracking.service.TrackingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/tracking/bsd")
@Api(tags = "배송 추적 및 조회")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class BsdController {
  private final SweettrackerService sweettrackerService;
  private final TrackingService trackingService;
  private final HistoryService historyService;

  private final SweettrackerConfig sweettrackerConfig;

  @ApiOperation(value = "Bsd 배송 추적 요청", response = ResponseObject.class, notes = "필수값 : Bsd일련번호, 송장번호, 택배사코드")
  @RequestMapping(value = "/requestTracking", method = {RequestMethod.POST})
  public ResponseObject<List<BsdRequestGetDto>> requestTracking(@RequestBody @Valid List<BsdRequestSetDto> bsdRequestSetDtoList) {
    if (bsdRequestSetDtoList.size() > 0) {
      Map<String, BsdTrackingGetDto> bsdTrackingGetDtoMap = trackingService.getBsdInvoice(bsdRequestSetDtoList);
      if (bsdTrackingGetDtoMap != null) {
        List<BsdRequestGetDto> bsdRequestGetDtoList = new ArrayList<>();
        BsdResultSetDto bsdResultSetDto;
        String callbackUrl = "/tracking/bsd/callbackTrackingSWT";
        TraceRequestGetDto traceRequestGetDto = sweettrackerService.requestTracking(callbackUrl,
            bsdTrackingGetDtoMap.values());
        if (traceRequestGetDto != null) {
          for (TraceRequestItemGetDto item : traceRequestGetDto.getList()) {
            bsdResultSetDto = new BsdResultSetDto(item.getFid(), item.getSuccess(), item.getDetail(),item.getE_code() +":"+ item.getE_message());
            bsdRequestGetDtoList.addAll(
                trackingService.setBsdTrackingResult(bsdResultSetDto,bsdTrackingGetDtoMap));
          }
          return ResourceConverter.toResponseObject(bsdRequestGetDtoList);
        }
      }
    }
    return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK,
        ExceptionCode.ERROR_CODE_2201.getResponseCode(),
        ExceptionCode.ERROR_CODE_2201.getResponseMessage());
  }

  @ApiOperation(value = "배송 요청 콜백", response = ResponseObject.class, notes = "스윗트래커에서 호출")
  @RequestMapping(value = "/callbackTrackingSWT", method = {RequestMethod.POST})
  public TraceCallbackGetDto callbackTrackingSWT(TraceCallbackSetDto traceCallbackSetDto) {
    CommonHistoryInsertSetDto commonHistoryInsertSetDto = traceCallbackSetDto.toCommonHistoryInsertSetDto(sweettrackerService.getDlvCdMappingStoH());
    if (commonHistoryInsertSetDto == null) {
      log.info(traceCallbackSetDto.toString());
      return new TraceCallbackGetDto("false", "fail - invalid courier code");
    }
    int historyResult = historyService.setCommonHistoryInvoice(commonHistoryInsertSetDto);
    if (historyResult > 0) {
      return new TraceCallbackGetDto("true", "success");
    }
    return new TraceCallbackGetDto("false", "system error");
  }
}