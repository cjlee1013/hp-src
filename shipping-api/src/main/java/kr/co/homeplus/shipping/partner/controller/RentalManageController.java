package kr.co.homeplus.shipping.partner.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.admin.service.ShipManageService;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageCounselCompleteSetDto;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageCounselReservationSetDto;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageListSetDto;
import kr.co.homeplus.shipping.partner.service.RentalManageService;
import kr.co.homeplus.shipping.utils.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/partner/sell")
@Api(tags = "파트너 - 렌탈판매관리")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class RentalManageController {
  private final RentalManageService rentalManageService;
  private final ShipManageService shipManageService;

  @ApiOperation(value = "렌탈판매관리 조회")
  @PostMapping("/getRentalManageList")
  public ResponseObject<List<RentalManageListGetDto>> getRentalManageList(@RequestBody @Valid RentalManageListSetDto rentalManageListSetDto) {
    return ResourceConverter.toResponseObject(rentalManageService.getRentalManageList(rentalManageListSetDto));
  }

  @ApiOperation(value = "렌탈판매관리 카운트 리스트 조회")
  @GetMapping("/getRentalManageCntList")
  public ResponseObject<List<RentalManageListGetDto>> getRentalManageCntList(
        @ApiParam(name = "schType", value = "검색타입", required = true) @RequestParam String schType,
        @ApiParam(name = "partnerId", value = "파트너ID", required = true) @RequestParam String partnerId) {
    return ResourceConverter.toResponseObject(rentalManageService.getRentalManageCntList(schType, partnerId));
  }

  @ApiOperation(value = "렌탈판매관리 카운트 조회")
  @GetMapping("/getRentalManageCnt")
  public ResponseObject<RentalManageCntGetDto> getRentalManageCnt(@ApiParam(name = "partnerId", value = "파트너ID", required = true) @RequestParam String partnerId) {
    return ResourceConverter.toResponseObject(rentalManageService.getRentalManageCnt(partnerId));
  }

  @ApiOperation(value = "상담예약")
  @PostMapping("/setCounselReservation")
  public ResponseObject<String> setCounselReservation(@RequestBody @Valid RentalManageCounselReservationSetDto rentalManageCounselReservationSetDto) {
    try {
      return rentalManageService.setCounselReservation(rentalManageCounselReservationSetDto);
    } catch (Exception e) {
      return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3012.getResponseMessage());
    }
  }

  @ApiOperation(value = "상담완료")
  @PostMapping("/setCounselComplete")
  public ResponseObject<String> setCounselComplete(@RequestBody @Valid RentalManageCounselCompleteSetDto rentalManageCounselCompleteSetDto) {
    try {
      return rentalManageService.setCounselComplete(rentalManageCounselCompleteSetDto);
    } catch (Exception e) {
      return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3013.getResponseMessage());
    }
  }

}