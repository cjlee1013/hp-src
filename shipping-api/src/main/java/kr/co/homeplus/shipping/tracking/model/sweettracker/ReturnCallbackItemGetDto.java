package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송추적 콜백 스윗트래커 응답값")
public class ReturnCallbackItemGetDto {

    @ApiModelProperty(notes = "처리결과(Y:성공,N:실패)")
    private String result;

    @ApiModelProperty(notes = "주문번호")
    private String ordCde;

    public ReturnCallbackItemGetDto(String result, String ordCde) {
        this.result = result;
        this.ordCde = ordCde;
    }
}
