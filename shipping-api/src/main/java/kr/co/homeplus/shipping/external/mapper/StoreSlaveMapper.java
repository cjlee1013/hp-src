package kr.co.homeplus.shipping.external.mapper;

import java.util.List;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shipping.external.model.store.*;
import kr.co.homeplus.shipping.external.model.market.*;

@SlaveConnection
public interface StoreSlaveMapper {
    List<StoreShipInfoGetDto> getShipInfo(String purchaseStoreInfoNo);
    List<StorePickInfoGetDto> getPickInfo(List<StoreClaimCommonSetDto> storeClaimCommonSetDto);
    List<StoreExchInfoGetDto> getExchInfo(List<StoreClaimCommonSetDto> storeClaimCommonSetDto);
    List<MarketOrderItemGetDto> getShippingMarket(String shipNo);
}
