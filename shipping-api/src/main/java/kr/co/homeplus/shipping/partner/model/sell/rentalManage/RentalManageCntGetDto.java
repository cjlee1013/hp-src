package kr.co.homeplus.shipping.partner.model.sell.rentalManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 렌탈판매관리 카운트 응답 DTO")
public class RentalManageCntGetDto {
    @ApiModelProperty(notes = "상담요청 건수")
    private String callCnt;

    @ApiModelProperty(notes = "상담예약 건수")
    private String reservationCnt;

    @ApiModelProperty(notes = "상담완료 건수")
    private String completeCnt;
}