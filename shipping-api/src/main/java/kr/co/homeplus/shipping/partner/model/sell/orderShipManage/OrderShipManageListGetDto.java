package kr.co.homeplus.shipping.partner.model.sell.orderShipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "판매관리 > 발주/발송관리 리스트 응답 DTO")
public class OrderShipManageListGetDto {
    @ApiModelProperty(notes = "배송SEQ")
    private String shipNo;

    @ApiModelProperty(notes = "상품주문번호")
    private String orderItemNo;

    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;

    @ApiModelProperty(notes = "주문일")
    private String orderDt;

    @ApiModelProperty(notes = "주문상태")
    private String shipStatusNm;

    @ApiModelProperty(notes = "주문상태코드")
    private String shipStatus;

    @ApiModelProperty(notes = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "상품번호")
    private String itemNo;

    @ApiModelProperty(notes = "상품명")
    private String itemNm1;

    @ApiModelProperty(notes = "옵션")
    private String txtOptVal;

    @ApiModelProperty(notes = "수량")
    private String itemQty;

    @ApiModelProperty(notes = "상품금액")
    private String itemPrice;

    @ApiModelProperty(notes = "총상품금액")
    private String orderPrice;

    @ApiModelProperty(notes = "수수료")
    private String commissionPrice;

    @ApiModelProperty(notes = "판매자쿠폰할인")
    private String sellerChargePrice;

    @ApiModelProperty(notes = "구매자")
    private String buyerNm;

    @ApiModelProperty(notes = "구매자 연락처")
    private String buyerMobileNo;

    @ApiModelProperty(notes = "수령인")
    private String receiverNm;

    @ApiModelProperty(notes = "수령인 연락처")
    private String shipMobileNo;

    @ApiModelProperty(notes = "배송방법")
    private String shipMethodNm;

    @ApiModelProperty(notes = "배송방법코드")
    private String shipMethod;

    @ApiModelProperty(notes = "택배사")
    private String dlvCdNm;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "배송요청일")
    private String reserveShipDt;

    @ApiModelProperty(notes = "배송예정일")
    private String scheduleShipDt;

    @ApiModelProperty(notes = "우편번호")
    private String zipcode;

    @ApiModelProperty(notes = "주소")
    private String addr;

    @ApiModelProperty(notes = "도로명기본주소")
    private String roadBaseAddr;

    @ApiModelProperty(notes = "도로명상세주소")
    private String roadDetailAddr;

    @ApiModelProperty(notes = "배송메세지")
    private String dlvShipMsg;

    @ApiModelProperty(notes = "배송비지급")
    private String shipPriceType;

    @ApiModelProperty(notes = "배송비")
    private String shipPrice;

    @ApiModelProperty(notes = "도서산간배송비")
    private String islandShipPrice;

    @ApiModelProperty(notes = "결제일")
    private String paymentFshDt;

    @ApiModelProperty(notes = "주문확인일")
    private String confirmDt;

    @ApiModelProperty(notes = "발송기한")
    private String orgShipDt;

    @ApiModelProperty(notes = "2차발송기한")
    private String delayShipDt;

    @ApiModelProperty(notes = "배송완료일")
    private String completeDt;

    @ApiModelProperty(notes = "미수취신고일")
    private String noRcvDeclrDt;

    @ApiModelProperty(notes = "업체상품코드")
    private String sellerItemCd;

    @ApiModelProperty(notes = "개인통관고유번호")
    private String personalOverseaNo;

    @ApiModelProperty(notes = "회원번호")
    private String userNo;

    @ApiModelProperty(notes = "미수취처리유형코드")
    private String noRcvProcessType;

    @ApiModelProperty(notes = "배송완료3개월 경과 여부")
    private String completeAfter3MonthYn;

    @ApiModelProperty(notes = "사이트유형")
    private String siteType;
}
