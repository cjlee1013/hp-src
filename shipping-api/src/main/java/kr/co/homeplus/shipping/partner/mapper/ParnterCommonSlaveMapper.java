package kr.co.homeplus.shipping.partner.mapper;

import java.util.Map;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;
import org.apache.ibatis.annotations.Param;

@SlaveConnection
public interface ParnterCommonSlaveMapper {
  int selectPartnerCheck(@Param("partnerId") String partnerId);
  int selectPartnerItemByShipNo(@Param("shipNo") String shipNo, @Param("partnerId") String partnerId);
  int selectPartnerItemByOrderItemNo(@Param("orderItemNo") String orderItemNo, @Param("partnerId") String partnerId);
  Map<String,String> selectTicketInfoByShipNo(@Param("shipNo") String shipNo);
}
