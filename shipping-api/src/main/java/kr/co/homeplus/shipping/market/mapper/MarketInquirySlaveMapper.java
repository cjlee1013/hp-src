package kr.co.homeplus.shipping.market.mapper;

import java.util.List;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shipping.market.model.MarketInquiryDetailGetDto;
import kr.co.homeplus.shipping.market.model.MarketInquiryDetailSetDto;
import kr.co.homeplus.shipping.market.model.MarketInquiryListGetDto;
import kr.co.homeplus.shipping.market.model.MarketInquiryListSetDto;

@SlaveConnection
public interface MarketInquirySlaveMapper {
  List<MarketInquiryListGetDto> selectMarketInquiryList(
      MarketInquiryListSetDto marketInquiryListSetDto);
  MarketInquiryDetailGetDto selectMarketInquiryDetail(MarketInquiryDetailSetDto marketInquiryDetailSetDto);
}
