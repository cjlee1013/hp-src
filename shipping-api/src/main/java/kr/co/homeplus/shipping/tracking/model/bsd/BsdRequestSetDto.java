package kr.co.homeplus.shipping.tracking.model.bsd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class BsdRequestSetDto {
    @ApiModelProperty(notes = "BSD일련번호")
    private String bsdLinkNo;
}
