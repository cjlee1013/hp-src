package kr.co.homeplus.shipping.constants;

/**
 * resource api 명을 관리
 * apiId의 url은 application.yml의 plus.resource-routes 에 정의 됨
 */
public class ResourceRouteName {
    // admin-web
    public static final String ADMIN = "admin";

    // partner-web
    public static final String PARTNER = "partner";

    // message api
    public static final String MESSAGE = "message";

    // outbound api
    public static final String OUTBOUND = "outbound";
}
