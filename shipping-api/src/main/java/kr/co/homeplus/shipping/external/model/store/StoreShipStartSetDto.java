package kr.co.homeplus.shipping.external.model.store;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송 시작 상태 변경")
public class StoreShipStartSetDto extends StoreShipVaninfoSetDto {
    @ApiModelProperty(notes = "배송예정시간(yyyy-MM-dd HH:mm:ss)")
    private String scheduleShipDt;

    @ApiModelProperty(notes = "차량ID")
    private String vanId;

    @ApiModelProperty(notes = "배송번호")
    private String shipNo;
}
