package kr.co.homeplus.shipping.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipStoreManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipStoreManageListSetDto;
import kr.co.homeplus.shipping.admin.service.ShipStoreManageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/shipManage")
@Api(tags = "어드민 - 점포배송관리")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class ShipStoreManageController {
  private final ShipStoreManageService shipStoreManageService;

  @ApiOperation(value = "점포배송관리 조회")
  @PostMapping("/getShipStoreManageList")
  public ResponseObject<List<ShipStoreManageListGetDto>> getShipStoreManageList(@RequestBody @Valid ShipStoreManageListSetDto shipStoreManageListSetDto) {
    return ResourceConverter.toResponseObject(shipStoreManageService.getShipStoreManageList(shipStoreManageListSetDto));
  }

  @ApiOperation(value = "배송Shift 리스트 조회")
  @GetMapping("/getStoreShiftList")
  public ResponseObject<List<String>> getStoreShiftList(
      @ApiParam(name = "storeId", value = "점포코드", required = true, defaultValue = "") @RequestParam String storeId
      , @ApiParam(name = "storeType", value = "점포유형", required = true, defaultValue = "") @RequestParam String storeType) {
    return ResourceConverter.toResponseObject(shipStoreManageService.getStoreShiftList(storeId,storeType));
  }
}