package kr.co.homeplus.shipping.tracking.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.shipping.tracking.config.SweettrackerConfig;
import kr.co.homeplus.shipping.tracking.model.common.CommonTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.common.DlvCdMappingGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickAddressGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickValidCreditGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickValidCreditSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.InvalidCreditGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.InvalidCreditItemGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.InvalidCreditItemSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.InvalidCreditSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnCancelGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnCancelItemGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnCancelItemSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnCancelSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnRequestItemGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnRequestItemSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceRequestItemGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceRequestItemSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceRequestSetDto;
import kr.co.homeplus.shipping.tracking.service.SweettrackerService;
import kr.co.homeplus.shipping.tracking.service.TrackingService;
import kr.co.homeplus.shipping.utils.HttpConnection;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class SweettrackerServiceImpl implements SweettrackerService {

    private static Map<String, DlvCdMappingGetDto> dlvCdMappingHtoS;
    private static Map<String, DlvCdMappingGetDto> dlvCdMappingStoH;

    private final TrackingService trackingService;

    private final SweettrackerConfig sweettrackerConfig;

    @Override
    public TraceRequestGetDto requestTracking(String callbackUrl, Collection<?> commonTrackingGetDtoList)
    {
        List<TraceRequestItemSetDto> traceRequestItemSetDtoList = new ArrayList<>();
        List<TraceRequestItemGetDto> traceRequestItemGetDtoList = new ArrayList<>();
        Map<String, DlvCdMappingGetDto> dlvCdMappingHtoS = getDlvCdMappingHtoS();
        DlvCdMappingGetDto dlvCdMappingGetDto;
        for (Object commonTrackingGetDto : commonTrackingGetDtoList) {
            CommonTrackingGetDto item = (CommonTrackingGetDto) commonTrackingGetDto;
            dlvCdMappingGetDto = dlvCdMappingHtoS.get(item.getDlvCd());
            if (dlvCdMappingGetDto != null) {
                traceRequestItemSetDtoList.add(new TraceRequestItemSetDto(item.getInvoiceNo(), dlvCdMappingGetDto.getDlvLinkCompanyCd(), item.getNo()));
            } else {
                //택배사 코드 매핑 안되는 애들 별도로 오류처리
                traceRequestItemGetDtoList.add(new TraceRequestItemGetDto(item.getNo(),item.getInvoiceNo(),"false","04","택배사코드오류"));
            }
        }
        if (traceRequestItemSetDtoList.size() > 0) {
            TraceRequestSetDto traceRequestSetDto = new TraceRequestSetDto(
                sweettrackerConfig.getCallback() + callbackUrl,
                sweettrackerConfig.trackingProperties()
                    .getTier(), sweettrackerConfig.trackingProperties()
                .getKey(), traceRequestItemSetDtoList);
            String requestUrl = sweettrackerConfig.trackingProperties()
                .getUrl() + "/add_invoice_list";
            try {
                ObjectMapper mapper = new ObjectMapper();
                String jsonRequest = mapper.writeValueAsString(traceRequestSetDto);
                byte[] params = jsonRequest.getBytes("utf-8");
                String response = HttpConnection.sendPost(requestUrl, params, "UTF-8", 1000, 10000,
                    "application/json");
            /* 샘플데이터
            String response = "{ \"list\" : [\n"
                + "{\"fid\":\"1\",\"num\":\"620984129182\",\"success\":true},\n"
                + "{\"fid\":\"2\",\"num\":\"620976988235\",\"success\":true},\n"
                + "{\"fid\":\"3\",\"num\":\"620957540334\",\"success\":true}],\n"
                + "\"success\":true\n"
                + "}";
             */
                log.info(response);
                TraceRequestGetDto results = mapper.readValue(response, TraceRequestGetDto.class);
                if (results != null) {
                    results.getList().addAll(traceRequestItemGetDtoList);
                    return results;
                }
            } catch (Exception e) {
                for (TraceRequestItemSetDto item : traceRequestItemSetDtoList) {
                    traceRequestItemGetDtoList.add(new TraceRequestItemGetDto(item.getFid(),item.getNum(),"false","99",e.getMessage()));
                }
            }
        }
        return new TraceRequestGetDto(traceRequestItemGetDtoList);
    }

    @Override
    public List<PickValidCreditGetDto> getInvalidCredit(List<PickValidCreditSetDto> pickValidCreditSetDtoList) {
        List<InvalidCreditItemSetDto> invalidCreditItemSetDtoList = new ArrayList<>();
        List<PickValidCreditGetDto> pickValidCreditGetDtoList = new ArrayList<>();
        InvalidCreditItemSetDto invalidCreditItemSetDto;
        Map<String, DlvCdMappingGetDto> dlvCdMappingHtoS = getDlvCdMappingHtoS();
        DlvCdMappingGetDto dlvCdMappingGetDto;
        for (PickValidCreditSetDto item : pickValidCreditSetDtoList){
            dlvCdMappingGetDto = dlvCdMappingHtoS.get(item.getDlvCd());
            if (dlvCdMappingGetDto != null) {
                invalidCreditItemSetDto = new InvalidCreditItemSetDto(item.getCreditCd(),
                    dlvCdMappingGetDto.getDlvLinkCompanyCd());
                invalidCreditItemSetDtoList.add(invalidCreditItemSetDto);
            } else {
                pickValidCreditGetDtoList.add(new PickValidCreditGetDto(item.getCreditCd(),item.getDlvCd(),"N","택배사코드오류"));
            }
        }
        if (invalidCreditItemSetDtoList.size() > 0) {
            InvalidCreditSetDto invalidCreditSetDto = new InvalidCreditSetDto(
                sweettrackerConfig.pickProperties()
                    .getTier(), invalidCreditItemSetDtoList);
            try {
                ObjectMapper mapper = new ObjectMapper();
                String jsonRequest = mapper.writeValueAsString(invalidCreditSetDto);
                byte[] params = jsonRequest.getBytes("utf-8");
                String requestUrl = sweettrackerConfig.pickProperties()
                    .getUrl() + "/get_invalid_credit_renewal";
                String response = HttpConnection.sendPost(requestUrl, params, "UTF-8", 1000,
                    10000, "application/json");
                InvalidCreditGetDto invalidCreditGetDto = mapper.readValue(response,
                    InvalidCreditGetDto.class);
                if (invalidCreditGetDto != null && invalidCreditGetDto.getData() != null) {
                    Map<String, DlvCdMappingGetDto> dlvCdMappingStoH = getDlvCdMappingStoH();
                    String success;
                    for (InvalidCreditItemGetDto item : invalidCreditGetDto.getData()) {
                        success =
                            ("Y".equals(item.getResult()) && "Y".equals(item.getCusUse())) ? "Y"
                                : "N";
                        dlvCdMappingGetDto = dlvCdMappingStoH.get(item.getComCode());
                        if (dlvCdMappingGetDto != null) {
                            pickValidCreditGetDtoList.add(
                                new PickValidCreditGetDto(item.getCusCde(),
                                    dlvCdMappingGetDto.getDlvCd(), success, item.getErrMsg()));
                        } else {
                            pickValidCreditGetDtoList.add(
                                new PickValidCreditGetDto(item.getCusCde(), item.getComCode(),
                                    success, "택배사코드오류 " + item.getErrMsg()));
                        }
                    }
                }
            } catch (Exception e) {
                for (InvalidCreditItemSetDto item : invalidCreditItemSetDtoList) {
                    pickValidCreditGetDtoList.add(new PickValidCreditGetDto(item.getCusCde(),item.getComCode(),"N",e.getMessage()));
                }
            }
        }
        return pickValidCreditGetDtoList;
    }

    @Override
    public ReturnRequestGetDto requestPick(List<PickAddressGetDto> pickAddressGetDtoList) {
        List<ReturnRequestItemSetDto> returnRequestItemSetDtoList = new ArrayList<>();
        List<ReturnRequestItemGetDto> ReturnRequestItemGetDtoList = new ArrayList<>();
        Map<String, DlvCdMappingGetDto> dlvCdMappingHtoS = getDlvCdMappingHtoS();
        DlvCdMappingGetDto dlvCdMapping;
        String cipherType = sweettrackerConfig.pickProperties().getType();
        String cipherKey = sweettrackerConfig.pickProperties().getKey();
        for (PickAddressGetDto address : pickAddressGetDtoList) {
            dlvCdMapping = dlvCdMappingHtoS.get(address.getPickDlvCd());
            if (dlvCdMapping != null && "Y".equals(dlvCdMapping.getAutoReturnYn())) {
                returnRequestItemSetDtoList.add(new ReturnRequestItemSetDto(address,dlvCdMapping.getDlvLinkCompanyCd(),cipherType,cipherKey));
            } else {
                ReturnRequestItemGetDtoList.add(new ReturnRequestItemGetDto("F",address.getClaimPickShippingNo(),"","택배사코드오류",null));
            }
        }
        if (returnRequestItemSetDtoList.size() > 0) {
            ReturnRequestSetDto returnRequestSetDto = new ReturnRequestSetDto(
                sweettrackerConfig.pickProperties()
                    .getTier(), cipherType, returnRequestItemSetDtoList);

            try {
                ObjectMapper mapper = new ObjectMapper();
                String jsonRequest = mapper.writeValueAsString(returnRequestSetDto);
                String requestUrl = sweettrackerConfig.pickProperties()
                    .getUrl() + "/put_order_renewal";
                byte[] params = jsonRequest.getBytes("utf-8");
                String response = HttpConnection.sendPost(requestUrl, params, "UTF-8", 1000,
                    10000, "application/json");
            /* 샘플데이터
            String response = "{\n"
                + "\"data\":[\n"
                + "{\n"
                + "\"result\":\"Y\",\n"
                + "\"ordCde\":\"HPLUS1234567890\",\n"
                + "\"errCde\":\"\",\n"
                + "\"errMsg\":\"\"\n"
                + "}\n"
                + "]";
             */
                ReturnRequestGetDto results = mapper.readValue(response, ReturnRequestGetDto.class);
                if (results != null && results.getData() != null) {
                    return results;
                }
            } catch (Exception e) {
                for (ReturnRequestItemSetDto item : returnRequestItemSetDtoList) {
                    ReturnRequestItemGetDtoList.add(
                        new ReturnRequestItemGetDto("F", item.getOrdCde(), "",
                            e.getMessage(),null));
                }
            }
        }
        return new ReturnRequestGetDto(ReturnRequestItemGetDtoList);
    }

    @Override
    public ReturnCancelGetDto cancelPick(List<PickRequestSetDto> pickRequestSetDtoList) {
        List<ReturnCancelItemSetDto> returnCancelItemSetDtoList = new ArrayList<>();
        for (PickRequestSetDto item : pickRequestSetDtoList) {
            returnCancelItemSetDtoList.add(new ReturnCancelItemSetDto(item.getClaimPickShippingNo()));
        }
        if (returnCancelItemSetDtoList.size() > 0) {
            ReturnCancelSetDto returnCancelSetDto = new ReturnCancelSetDto(
                sweettrackerConfig.pickProperties()
                    .getTier(), returnCancelItemSetDtoList);

            try {
                ObjectMapper mapper = new ObjectMapper();
                String jsonRequest = mapper.writeValueAsString(returnCancelSetDto);
                String requestUrl = sweettrackerConfig.pickProperties()
                    .getUrl() + "/put_order_cancel_renewal";
                byte[] params = jsonRequest.getBytes("utf-8");
                String response = HttpConnection.sendPost(requestUrl, params, "UTF-8", 1000,
                    10000, "application/json");
            /* 샘플데이터
            String response = "{\n"
                + "\"data\":[\n"
                + "{\n"
                + "\"result\":\"Y\",\n"
                + "\"ordCde\":\"HPLUS1234567890\",\n"
                + "\"errCde\":\"\",\n"
                + "\"errMsg\":\"\"\n"
                + "}\n"
                + "]\n"
                + "}";
             */
                ReturnCancelGetDto results = mapper.readValue(response, ReturnCancelGetDto.class);
                if (results != null && results.getData() != null) {
                    return results;
                }
            } catch (Exception e) {
                List<ReturnCancelItemGetDto> returnCancelItemGetDtoList = new ArrayList<>();
                for (ReturnCancelItemSetDto item:returnCancelItemSetDtoList) {
                    returnCancelItemGetDtoList.add(new ReturnCancelItemGetDto("N",item.getOrdCde(),"err_cel_04",e.getMessage()));
                }
                return new ReturnCancelGetDto(returnCancelItemGetDtoList);
            }
        }
        return null;
    }

    @Override
    public Map<String, DlvCdMappingGetDto> getDlvCdMappingStoH(){
        if (dlvCdMappingStoH == null) {
            dlvCdMappingStoH = new HashMap<>();
            List<DlvCdMappingGetDto> mappingCode = trackingService.getDlvCdMapping();
            for (DlvCdMappingGetDto item : mappingCode) {
                if ("SWT".equals(item.getDlvLinkCompany())) {
                    dlvCdMappingStoH.put(item.getDlvLinkCompanyCd(),item);
                }
            }
        }
        return dlvCdMappingStoH;
    }

    @Override
    public Map<String, DlvCdMappingGetDto> getDlvCdMappingHtoS(){
        if (dlvCdMappingHtoS == null) {
            dlvCdMappingHtoS = new HashMap<>();
            List<DlvCdMappingGetDto> mappingCode = trackingService.getDlvCdMapping();
            for (DlvCdMappingGetDto item : mappingCode) {
                if ("SWT".equals(item.getDlvLinkCompany())) {
                    dlvCdMappingHtoS.put(item.getDlvCd(),item);
                }
            }
        }
        return dlvCdMappingHtoS;
    }
}
