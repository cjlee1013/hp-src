package kr.co.homeplus.shipping.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

public class HttpConnection {

    public static final String USER_AGENT = "Mozilla/5.0";

    public static String sendPost(String url, byte[] urlParameters, String charSet,int conTimeOut, int readTimeOut, String mediaType) throws Exception{
        HttpURLConnection con = null;
        DataOutputStream wr = null;
        BufferedReader in = null;
        try {
            URL obj = new URL(url);
            con = (HttpURLConnection) obj.openConnection();
            //Header Setting
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("charset", charSet);
            con.setConnectTimeout(conTimeOut);       //Connection 12초
            con.setReadTimeout(readTimeOut);          //Read 10초
            if (mediaType != null) {
                con.setRequestProperty("Content-Type", mediaType);
            }
            con.setUseCaches(false);            //동적생성결과조회

            //Send post request
            con.setDoOutput(true);
            wr = new DataOutputStream(con.getOutputStream());
            wr.write(urlParameters);
            wr.flush();
            wr.close();

            Charset charset = Charset.forName(charSet);
            in = new BufferedReader(
                new InputStreamReader(con.getInputStream(), charset));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response.toString();
        } catch (Exception e) {
            throw e;
        } finally {
            if (con != null) {
                con.disconnect();
            }
            if (wr != null) {
                try {
                    wr.close();
                } catch (Exception e) {

                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {

                }
            }
        }
    }
}
