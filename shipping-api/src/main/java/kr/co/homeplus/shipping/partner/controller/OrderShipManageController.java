package kr.co.homeplus.shipping.partner.controller;

import static kr.co.homeplus.shipping.constants.ResourceRouteName.PARTNER;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageConfirmOrderSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageNotiDelaySetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllExcelSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllPopListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllPopListSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllSetDto;
import kr.co.homeplus.shipping.admin.service.ShipManageService;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.partner.model.sell.HomeMainCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.OrderDetailGetDto;
import kr.co.homeplus.shipping.partner.model.sell.orderShipManage.OrderShipManageCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.orderShipManage.OrderShipManageListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.orderShipManage.OrderShipManageListSetDto;
import kr.co.homeplus.shipping.partner.model.sell.shipStatus.ShipStatusCntGetDto;
import kr.co.homeplus.shipping.partner.service.OrderShipManageService;
import kr.co.homeplus.shipping.partner.service.ShipStatusService;
import kr.co.homeplus.shipping.utils.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/partner/sell")
@Api(tags = "파트너 - 발주/발송관리")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class OrderShipManageController {
  private final OrderShipManageService orderShipManageService;
  private final ShipStatusService shipStatusService;
  private final ShipManageService shipManageService;

  @ApiOperation(value = "발주/발송관리 조회")
  @PostMapping("/getOrderShipManageList")
  public ResponseObject<List<OrderShipManageListGetDto>> getOrderShipManageList(@RequestBody @Valid OrderShipManageListSetDto orderShipManageListSetDto) {
    return ResourceConverter.toResponseObject(orderShipManageService.getOrderShipManageList(orderShipManageListSetDto));
  }

  @ApiOperation(value = "주문확인")
  @PostMapping("/setConfirmOrder")
  public ResponseObject<String> setConfirmOrder(@RequestBody @Valid ShipManageConfirmOrderSetDto shipManageConfirmOrderSetDto) {
    try {
      return shipManageService.setConfirmOrder(shipManageConfirmOrderSetDto, PARTNER);
    } catch (Exception e) {
      return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3001.getResponseMessage());
    }
  }

  @ApiOperation(value = "발송지연안내")
  @PostMapping("/setNotiDelay")
  public ResponseObject<String> setNotiDelay(@RequestBody @Valid ShipManageNotiDelaySetDto shipManageNotiDelaySetDto) {
    try {
      return shipManageService.setNotiDelay(shipManageNotiDelaySetDto, PARTNER);
    } catch (Exception e) {
      return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3002.getResponseMessage());
    }
  }

  @ApiOperation(value = "일괄발송처리")
  @PostMapping("/setShipAllExcel")
  public ResponseObject<Map<String, Object>> setShipAllExcel(@RequestBody @Valid List<ShipManageShipAllExcelSetDto> shipManageShipAllExcelSetDtoList) {
    try {
      return shipManageService.setShipAllExcel(shipManageShipAllExcelSetDtoList, PARTNER);
    } catch (Exception e) {
      return ResourceConverter.toResponseObject(new HashMap<>(), HttpStatus.OK, "FAIL", ExceptionCode.ERROR_CODE_3003.getResponseMessage());
    }
  }

  @ApiOperation(value = "선택발송처리")
  @PostMapping("/setShipAll")
  public ResponseObject<String> setShipAll(@RequestBody @Valid ShipManageShipAllSetDto shipManageShipAllSetDto) {
    try {
      return shipManageService.setShipAll(shipManageShipAllSetDto, PARTNER);
    } catch (Exception e) {
      return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3004.getResponseMessage());
    }
  }

  @ApiOperation(value = "선택발송처리(주문취소건가능)")
  @PostMapping("/setShipAllExClaim")
  public ResponseObject<String> setShipAllExClaim(@RequestBody @Valid ShipManageShipAllSetDto shipManageShipAllSetDto) {
    try {
      return shipManageService.setShipAllExClaim(shipManageShipAllSetDto, PARTNER);
    } catch (Exception e) {
      return ResponseUtil.getResoponseObjectFail(ExceptionCode.ERROR_CODE_3004.getResponseMessage());
    }
  }

  @ApiOperation(value = "선택발송처리 팝업 리스트 조회")
  @PostMapping("/getShipAllPopList")
  public ResponseObject<List<ShipManageShipAllPopListGetDto>> getShipAllPopList(@RequestBody @Valid ShipManageShipAllPopListSetDto shipManageShipAllPopListSetDto) {
    return ResourceConverter.toResponseObject(shipManageService.getShipAllPopList(shipManageShipAllPopListSetDto));
  }

  @ApiOperation(value = "발주/발송관리 카운트 리스트 조회")
  @GetMapping("/getOrderShipManageCntList")
  public ResponseObject<List<OrderShipManageListGetDto>> getOrderShipManageCntList(
        @ApiParam(name = "schType", value = "검색타입", required = true) @RequestParam String schType,
        @ApiParam(name = "partnerId", value = "파트너ID", required = true) @RequestParam String partnerId) {
    return ResourceConverter.toResponseObject(orderShipManageService.getOrderShipManageCntList(schType, partnerId));
  }

  @ApiOperation(value = "발주/발송관리 카운트 조회")
  @GetMapping("/getOrderShipManageCnt")
  public ResponseObject<OrderShipManageCntGetDto> getOrderShipManageCnt(@ApiParam(name = "partnerId", value = "파트너ID", required = true) @RequestParam String partnerId) {
    return ResourceConverter.toResponseObject(orderShipManageService.getOrderShipManageCnt(partnerId));
  }

  @ApiOperation(value = "상품 상세정보 팝업 조회")
  @GetMapping("/getOrderDetailPop")
  public ResponseObject<OrderDetailGetDto> getOrderDetailPop(@ApiParam(name = "bundleNo", value = "번들번호", required = true) @RequestParam String bundleNo
                                                            , @ApiParam(name = "partnerId", value = "파트너ID", required = true) @RequestParam String partnerId) {
    return ResourceConverter.toResponseObject(orderShipManageService.getOrderDetailPop(bundleNo, partnerId));
  }

  @ApiOperation(value = "홈 메인 판매진행형황 카운트 조회")
  @GetMapping("/getHomeMainCnt")
  public ResponseObject<HomeMainCntGetDto> getHomeMainCnt(@ApiParam(name = "partnerId", value = "파트너ID", required = true) @RequestParam String partnerId) {
    OrderShipManageCntGetDto orderShipManageCntGetDto = orderShipManageService.getOrderShipManageMainCnt(partnerId);
    ShipStatusCntGetDto shipStatusCntGetDto = shipStatusService.getShipStatusMainCnt(partnerId);

    HomeMainCntGetDto homeMainCntGetDto = new HomeMainCntGetDto();

    homeMainCntGetDto.setNewCnt(orderShipManageCntGetDto.getNewCnt());
    homeMainCntGetDto.setNewDelayCnt(orderShipManageCntGetDto.getNewDelayCnt());
    homeMainCntGetDto.setReadyCnt(orderShipManageCntGetDto.getReadyCnt());
    homeMainCntGetDto.setShipDelayCnt(orderShipManageCntGetDto.getShipDelayCnt());
    homeMainCntGetDto.setShippingCnt(shipStatusCntGetDto.getShippingCnt());

    return ResourceConverter.toResponseObject(homeMainCntGetDto);
  }
}