package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.shipping.tracking.model.pick.PickAddressGetDto;
import kr.co.homeplus.shipping.utils.CipherUtil;
import lombok.Data;

@Data
public class ReturnRequestItemSetDto {
  @ApiModelProperty(notes = "주문번호")
  private String ordCde;

  @ApiModelProperty(notes = "택배사 코드")
  private String comCode;

  @ApiModelProperty(notes = "운송장 번호")
  private String invoice;

  @ApiModelProperty(notes = "주문 일자 / 등록 일자 (yyyyMMdd)")
  private String addDat;

  @ApiModelProperty(notes = "신용코드 (우체국의 경우 점포코드)")
  private String cusCde;

  @ApiModelProperty(notes = "서브 신용코드")
  private String cusSub;

  @ApiModelProperty(notes = "송화인 명")
  private String sndNme;

  @ApiModelProperty(notes = "송화인 우편번호")
  private String sndZip;

  @ApiModelProperty(notes = "송화인 주소1")
  private String sndAd1;

  @ApiModelProperty(notes = "송화인 주소2")
  private String sndAd2;

  @ApiModelProperty(notes = "송화인 전화번호")
  private String sndTel;

  @ApiModelProperty(notes = "송화인 전화번호2")
  private String sndMod;

  @ApiModelProperty(notes = "수화인 명")
  private String ownNme;

  @ApiModelProperty(notes = "수화인 우편번호")
  private String ownZip;

  @ApiModelProperty(notes = "수화인 주소1")
  private String ownAd1;

  @ApiModelProperty(notes = "수화인 주소2")
  private String ownAd2;

  @ApiModelProperty(notes = "수화인 전화번호1")
  private String ownTel;

  @ApiModelProperty(notes = "수화인 전화번호2")
  private String ownMod;

  @ApiModelProperty(notes = "화물내역 상품명 (간단하게)")
  private String itmLst;

  @ApiModelProperty(notes = "배송 메모")
  private String adMemo;

  @ApiModelProperty(notes = "운임 구분 (3.신용)")
  private String wipGbn;
/* 우체국용 파라미터 미사용
  @ApiModelProperty(notes = "화물 크기 (1.극소 2.소 3. 중 4.대 5.극대)")
  private String dsoGbn;

  @ApiModelProperty(notes = "화물무게(1:초소형, 2:2Kg이하, 3:5Kg이하, 4:10Kg이하, 5:20Kg이하, 6:30Kg이하)")
  private String dsoWght;

  @ApiModelProperty(notes = "점포코드(우체국) / 협력사코드(CJ대한통운)")
  private String comDivCde;
 */

  public ReturnRequestItemSetDto(PickAddressGetDto pickAddressGetDto, String comCode,String cipherType, String cipherKey) {
    this.ordCde = pickAddressGetDto.getClaimPickShippingNo();
    this.comCode = comCode;
    this.invoice = pickAddressGetDto.getOrgInvoiceNo();
    this.addDat = pickAddressGetDto.getRegDt();
    this.cusCde = pickAddressGetDto.getCreditCd();
    //this.cusSub = pickAddressGetDto.getCreditCdSub();
    if ("1".equals(cipherType)) {
      this.sndNme = CipherUtil.encrypt(pickAddressGetDto.getPickReceiverNm(),"AES",cipherKey);
      this.sndZip = CipherUtil.encrypt(pickAddressGetDto.getPickZipcode(),"AES",cipherKey);
      this.sndAd1 = CipherUtil.encrypt(pickAddressGetDto.getPickBaseAddr(),"AES",cipherKey);
      this.sndAd2 = CipherUtil.encrypt(pickAddressGetDto.getPickDetailAddr(),"AES",cipherKey);
      this.sndTel = CipherUtil.encrypt(pickAddressGetDto.getPickMobileNo(),"AES",cipherKey);
      this.sndMod = CipherUtil.encrypt(pickAddressGetDto.getPickPhone(),"AES",cipherKey);
      this.ownNme = CipherUtil.encrypt(pickAddressGetDto.getBusinessNm(),"AES",cipherKey);
      this.ownZip = CipherUtil.encrypt(pickAddressGetDto.getReturnZipcode(),"AES",cipherKey);
      this.ownAd1 = CipherUtil.encrypt(pickAddressGetDto.getReturnAddr1(),"AES",cipherKey);
      this.ownAd2 = CipherUtil.encrypt(pickAddressGetDto.getReturnAddr2(),"AES",cipherKey);
      this.ownTel = CipherUtil.encrypt(pickAddressGetDto.getPhone1(),"AES",cipherKey);
      this.ownMod = CipherUtil.encrypt(pickAddressGetDto.getPhone2(),"AES",cipherKey);
    } else {
      this.sndNme = pickAddressGetDto.getPickReceiverNm();
      this.sndZip = pickAddressGetDto.getPickZipcode();
      this.sndAd1 = pickAddressGetDto.getPickBaseAddr();
      this.sndAd2 = pickAddressGetDto.getPickDetailAddr();
      this.sndTel = pickAddressGetDto.getPickMobileNo();
      this.sndMod = pickAddressGetDto.getPickPhone();
      this.ownNme = pickAddressGetDto.getBusinessNm();
      this.ownZip = pickAddressGetDto.getReturnZipcode();
      this.ownAd1 = pickAddressGetDto.getReturnAddr1();
      this.ownAd2 = pickAddressGetDto.getReturnAddr2();
      this.ownTel = pickAddressGetDto.getPhone1();
      this.ownMod = pickAddressGetDto.getPhone2();
    }
    this.itmLst = pickAddressGetDto.getItemName();
    this.adMemo = pickAddressGetDto.getPickMemo();
    this.wipGbn = "3";
    /* 우체국용 파라미터 미사용
    this.dsoGbn = pickAddressGetDto.getDsoGbn();
    this.dsoWght = pickAddressGetDto.getDsoWght();
    this.comDivCde = pickAddressGetDto.getComDivCde();
     */
  }

  public String getOrdCde() {
    return ordCde;
  }
}
