package kr.co.homeplus.shipping.external.model.store;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "옴니연동 교환상태값")
public class StoreExchInfoGetDto {
    @ApiModelProperty(notes = "클레임교환번호")
    private String claimExchShippingNo;

    @ApiModelProperty(notes = "클레임번들번호")
    private String claimBundleNo;

    @ApiModelProperty(notes = "교환상태")
    private String exchStatus;

    public String getClaimExchShippingNo() { return claimExchShippingNo; }

    public String getClaimBundleNo() {
        return claimBundleNo;
    }

    public String getExchStatus() { return exchStatus; }

}
