package kr.co.homeplus.shipping.admin.mapper;

import java.util.List;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsPartnerGetDto;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsPartnerSetDto;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsStoreGetDto;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsStoreSetDto;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface StatisticsSlaveMapper {
  List<StatisticsPartnerGetDto> getPartnerPayment(StatisticsPartnerSetDto statisticsPartnerSetDto);
  List<StatisticsStoreGetDto> getStorePayment(StatisticsStoreSetDto statisticsPartnerSetDto);
  List<StatisticsPartnerGetDto> getPartnerDecision(StatisticsPartnerSetDto statisticsPartnerSetDto);
  List<StatisticsStoreGetDto> getStoreDecision(StatisticsStoreSetDto statisticsPartnerSetDto);
}