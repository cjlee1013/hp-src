package kr.co.homeplus.shipping.admin.service;

import java.util.List;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsPartnerGetDto;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsPartnerSetDto;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsStoreGetDto;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsStoreSetDto;

public interface StatisticsService {
  List<StatisticsPartnerGetDto> getPartnerPayment(StatisticsPartnerSetDto statisticsPartnerSetDto);
  List<StatisticsStoreGetDto> getStorePayment(StatisticsStoreSetDto statisticsStoreSetDto);
  List<StatisticsPartnerGetDto> getPartnerDecision(StatisticsPartnerSetDto statisticsPartnerSetDto);
  List<StatisticsStoreGetDto> getStoreDecision(StatisticsStoreSetDto statisticsStoreSetDto);
}
