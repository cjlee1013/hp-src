package kr.co.homeplus.shipping.admin.model.statistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StatisticsPartnerGetDto {
  @ApiModelProperty(notes = "판매업체명")
  private String businessNm;
  @ApiModelProperty(notes = "판매업체ID")
  private String partnerId;
  @ApiModelProperty(notes = "판매업체코드")
  private String ofVendorCd;
  @ApiModelProperty(notes = "주문건수")
  private String orderQty;
  @ApiModelProperty(notes = "주문금액")
  private String orderPrice;
  @ApiModelProperty(notes = "취소건수")
  private String cancelQty;
  @ApiModelProperty(notes = "취소금액")
  private String cancelPrice;
  @ApiModelProperty(notes = "반품건수")
  private String returnQty;
  @ApiModelProperty(notes = "반품금액")
  private String returnPrice;
}
