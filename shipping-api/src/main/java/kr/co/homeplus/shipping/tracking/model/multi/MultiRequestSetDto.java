package kr.co.homeplus.shipping.tracking.model.multi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class MultiRequestSetDto {
    @ApiModelProperty(notes = "다중주문아이템번호")
    private String multiOrderItemNo;
}
