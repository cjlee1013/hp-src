package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "스윗트래커 반품요청 파라미터")
public class ReturnRequestSetDto {

    @ApiModelProperty(notes = "tier코드")
    private String tierCode;

    @ApiModelProperty(notes = "암호화타입")
    private String cipherType;

    @ApiModelProperty(notes = "목록", dataType = "List")
    private List<ReturnRequestItemSetDto> data;

    public ReturnRequestSetDto(String tierCode, String cipherType,List<ReturnRequestItemSetDto> data) {
        this.tierCode = tierCode;
        this.cipherType = cipherType;
        this.data = data;
    }
}
