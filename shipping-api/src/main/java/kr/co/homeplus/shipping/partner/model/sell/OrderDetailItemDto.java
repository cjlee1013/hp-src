package kr.co.homeplus.shipping.partner.model.sell;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문상품정보 DTO")
public class OrderDetailItemDto {
    @ApiModelProperty(notes = "상품번호")
    private String itemNo;

    @ApiModelProperty(notes = "상품주문번호")
    private String orderItemNo;

    @ApiModelProperty(notes = "주문상태")
    private String shipStatusNm;

    @ApiModelProperty(notes = "상품명")
    private String itemNm1;

    @ApiModelProperty(notes = "옵션")
    private String txtOptVal;

    @ApiModelProperty(notes = "상품금액")
    private String itemPrice;

    @ApiModelProperty(notes = "원주문수량")
    private String orgItemQty;

    @ApiModelProperty(notes = "수량")
    private String itemQty;

    @ApiModelProperty(notes = "총상품금액")
    private String orderPrice;

    @ApiModelProperty(notes = "배송방법")
    private String shipMethodNm;

    @ApiModelProperty(notes = "택배사")
    private String dlvCd;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "배송완료일")
    private String completeDt;
}
