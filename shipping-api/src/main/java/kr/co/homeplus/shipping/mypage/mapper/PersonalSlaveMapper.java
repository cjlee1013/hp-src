package kr.co.homeplus.shipping.mypage.mapper;

import java.util.List;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shipping.mypage.model.order.PersonalGetDto;
import kr.co.homeplus.shipping.mypage.model.order.PersonalSetDto;

@SlaveConnection
public interface PersonalSlaveMapper {
  List<PersonalGetDto> getShipInfo(PersonalSetDto personalSetDto);
  int getShipInfoCnt(PersonalSetDto personalSetDto);
}