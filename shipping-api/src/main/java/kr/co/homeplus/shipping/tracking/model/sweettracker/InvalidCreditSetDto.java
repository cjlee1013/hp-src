package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.shipping.tracking.model.pick.PickValidCreditSetDto;
import lombok.Data;

@Data
@ApiModel(description = "신용코드 확인 파라미터")
public class InvalidCreditSetDto {
    @ApiModelProperty(notes = "tier코드")
    private String tierCode;

    @ApiModelProperty(notes = "목록", dataType = "List")
    private List<InvalidCreditItemSetDto> data;

    public InvalidCreditSetDto(String tierCode, List<InvalidCreditItemSetDto> data) {
        this.tierCode = tierCode;
        this.data = data;
    }
}
