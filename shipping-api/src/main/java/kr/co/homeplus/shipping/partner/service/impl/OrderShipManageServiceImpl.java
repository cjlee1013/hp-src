package kr.co.homeplus.shipping.partner.service.impl;

import java.util.List;
import kr.co.homeplus.shipping.common.service.HistoryService;
import kr.co.homeplus.shipping.partner.mapper.OrderShipManageMasterMapper;
import kr.co.homeplus.shipping.partner.mapper.OrderShipManageSlaveMapper;
import kr.co.homeplus.shipping.partner.model.sell.OrderDetailBaseDto;
import kr.co.homeplus.shipping.partner.model.sell.OrderDetailGetDto;
import kr.co.homeplus.shipping.partner.model.sell.OrderDetailItemDto;
import kr.co.homeplus.shipping.partner.model.sell.OrderDetailShipDto;
import kr.co.homeplus.shipping.partner.model.sell.orderShipManage.OrderShipManageCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.orderShipManage.OrderShipManageListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.orderShipManage.OrderShipManageListSetDto;
import kr.co.homeplus.shipping.partner.service.OrderShipManageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderShipManageServiceImpl implements OrderShipManageService {
    private final OrderShipManageSlaveMapper orderShipManageSlaveMapper;
    private final OrderShipManageMasterMapper orderShipManageMasterMapper;
    private final HistoryService historyService;

  /**
     * 발주/발송관리 조회
     */
    @Override
    public List<OrderShipManageListGetDto> getOrderShipManageList(OrderShipManageListSetDto orderShipManageListSetDto) {
      log.info("getOrderShipManageList param : {}", orderShipManageListSetDto);

      List<OrderShipManageListGetDto> orderShipManageListGetDtoList = orderShipManageSlaveMapper.selectOrderShipManageList(orderShipManageListSetDto);

      return orderShipManageListGetDtoList;
    }

    /**
     * 발주/발송관리 카운트 리스트 조회
     */
    @Override
    public List<OrderShipManageListGetDto> getOrderShipManageCntList(String schType, String partnerId) {
      log.info("getOrderShipManageCntList param : {}", partnerId);

      List<OrderShipManageListGetDto> orderShipManageListGetDtoList = orderShipManageSlaveMapper.selectOrderShipManageCntList(schType, partnerId);

      return orderShipManageListGetDtoList;
    }

    /**
     * 발주/발송관리 카운트 조회
     */
    @Override
    public OrderShipManageCntGetDto getOrderShipManageCnt(String partnerId) {
      log.info("getOrderShipManageCnt param : {}", partnerId);

      OrderShipManageCntGetDto orderShipManageCntGetDto = new OrderShipManageCntGetDto();

      orderShipManageCntGetDto.setNewCnt(orderShipManageSlaveMapper.selectOrderShipManageCnt("newCnt", partnerId));
      orderShipManageCntGetDto.setReadyCnt(orderShipManageSlaveMapper.selectOrderShipManageCnt("readyCnt", partnerId));
      orderShipManageCntGetDto.setNewDelayCnt(orderShipManageSlaveMapper.selectOrderShipManageCnt("newDelayCnt", partnerId));
      orderShipManageCntGetDto.setShipDelayCnt(orderShipManageSlaveMapper.selectOrderShipManageCnt("shipDelayCnt", partnerId));
      orderShipManageCntGetDto.setShipDelayNotiCnt(orderShipManageSlaveMapper.selectOrderShipManageCnt("shipDelayNotiCnt", partnerId));
      orderShipManageCntGetDto.setShipTodayCnt(orderShipManageSlaveMapper.selectOrderShipManageCnt("shipTodayCnt", partnerId));
      orderShipManageCntGetDto.setClaimCnt(orderShipManageSlaveMapper.selectOrderShipManageCnt("claimCnt", partnerId));

      return orderShipManageCntGetDto;
    }

    /**
     * 홈 메인 판매진행형황 카운트 조회
     */
    @Override
    public OrderShipManageCntGetDto getOrderShipManageMainCnt(String partnerId) {
      log.info("getOrderShipManageMainCnt param : {}", partnerId);

      OrderShipManageCntGetDto orderShipManageCntGetDto = new OrderShipManageCntGetDto();

      orderShipManageCntGetDto.setNewCnt(orderShipManageSlaveMapper.selectOrderShipManageMainCnt("newCnt", partnerId));
      orderShipManageCntGetDto.setReadyCnt(orderShipManageSlaveMapper.selectOrderShipManageMainCnt("readyCnt", partnerId));
      orderShipManageCntGetDto.setNewDelayCnt(orderShipManageSlaveMapper.selectOrderShipManageMainCnt("newDelayCnt", partnerId));
      orderShipManageCntGetDto.setShipDelayCnt(orderShipManageSlaveMapper.selectOrderShipManageMainCnt("shipDelayCnt", partnerId));
      orderShipManageCntGetDto.setShipDelayNotiCnt(orderShipManageSlaveMapper.selectOrderShipManageMainCnt("shipDelayNotiCnt", partnerId));
      orderShipManageCntGetDto.setShipTodayCnt(orderShipManageSlaveMapper.selectOrderShipManageMainCnt("shipTodayCnt", partnerId));
      orderShipManageCntGetDto.setClaimCnt(orderShipManageSlaveMapper.selectOrderShipManageMainCnt("claimCnt", partnerId));

      return orderShipManageCntGetDto;
    }

    /**
     * 상품 상세정보 팝업 조회
     */
    @Override
    public OrderDetailGetDto getOrderDetailPop(String bundleNo, String partnerId) {
      log.info("getOrderDetailPop param : {}", partnerId);

      OrderDetailGetDto orderDetailGetDto = new OrderDetailGetDto();

      // 주문기본정보 조회
      OrderDetailBaseDto orderDetailBaseDto = orderShipManageSlaveMapper.selectOrderDetailBase(bundleNo, partnerId);
      orderDetailGetDto.setOrderDetailBase(orderDetailBaseDto);

      // 주문상품정보 조회
      List<OrderDetailItemDto> orderDetailItemDtoList = orderShipManageSlaveMapper.selectOrderDetailItem(bundleNo, partnerId);
      orderDetailGetDto.setOrderDetailItemList(orderDetailItemDtoList);

      // 배송정보 조회
      OrderDetailShipDto orderDetailShipDto = orderShipManageSlaveMapper.selectOrderDetailShip(bundleNo, partnerId);
      orderDetailGetDto.setOrderDetailShip(orderDetailShipDto);

      return orderDetailGetDto;
    }
}
