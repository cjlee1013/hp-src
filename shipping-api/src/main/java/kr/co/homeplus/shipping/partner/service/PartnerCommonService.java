package kr.co.homeplus.shipping.partner.service;

public interface PartnerCommonService {
    boolean isPartner(String partnerId);
    boolean isPartnerItemByShipNo(String shipNo, String partnerId);
    boolean isPartnerItemByOrderItemNo(String orderItemNo, String partnerId);
    boolean isCoopTicketByShipNo(String shipNo);
}
