package kr.co.homeplus.shipping.partner.service.impl;

import java.util.Map;
import kr.co.homeplus.shipping.partner.mapper.ParnterCommonSlaveMapper;
import kr.co.homeplus.shipping.partner.service.PartnerCommonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class PartnerCommonServiceImpl implements PartnerCommonService {
  private final ParnterCommonSlaveMapper parnterCommonSlaveMapper;

  /**
   * 존재하는 파트너인지 확인
   */
  public boolean isPartner(String partnerId) {
    return parnterCommonSlaveMapper.selectPartnerCheck(partnerId) > 0;
  }

  /**
   * 파트너 물품이 맞는지 확인 (by shipNo)
   */
  public boolean isPartnerItemByShipNo(String shipNo, String partnerId) {
    if(!this.isPartner(partnerId)) {
      return false;
    }

    return parnterCommonSlaveMapper.selectPartnerItemByShipNo(shipNo, partnerId) > 0;
  }

  /**
   * 파트너 물품이 맞는지 확인 (by orderItemNo)
   */
  public boolean isPartnerItemByOrderItemNo(String orderItemNo, String partnerId) {
    if(!this.isPartner(partnerId)) {
      return false;
    }

    return parnterCommonSlaveMapper.selectPartnerItemByOrderItemNo(orderItemNo, partnerId) > 0;
  }

  /**
   * COOP 티켓인지 확인 (by shipNo)
   */
  public boolean isCoopTicketByShipNo(String shipNo) {
    Map<String,String> ticketInfo = parnterCommonSlaveMapper.selectTicketInfoByShipNo(shipNo);

    if ("COOP".equals(ticketInfo.get("issue_channel"))) {
      return true;
    } else {
      return false;
    }
  }
}
