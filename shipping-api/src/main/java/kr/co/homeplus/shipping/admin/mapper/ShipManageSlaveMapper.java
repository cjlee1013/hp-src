package kr.co.homeplus.shipping.admin.mapper;

import java.util.List;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipInfoPrintGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageListSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipStoreInfoGetDto;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;
import org.apache.ibatis.annotations.Param;

@SlaveConnection
public interface ShipManageSlaveMapper {
  List<ShipManageListGetDto> selectShipManageList(ShipManageListSetDto shipManageListSetDto);
  ShipStoreInfoGetDto selectStoreInfo(@Param("storeId") String storeId);
  String selectStoreNm(@Param("storeId") String storeId);
  List<String> getOrgBundleNo(String bundleNo);
  List<ShipInfoPrintGetDto> selectShipInfoPrintList(String bundleNo);
}
