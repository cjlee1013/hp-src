package kr.co.homeplus.shipping.test.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import kr.co.homeplus.shipping.common.model.history.ExchProcessInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.PickProcessInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.StoreProcessInsertSetDto;
import kr.co.homeplus.shipping.common.service.HistoryService;
import kr.co.homeplus.shipping.external.mapper.StoreMasterMapper;
import kr.co.homeplus.shipping.external.mapper.StoreSlaveMapper;
import kr.co.homeplus.shipping.external.model.store.StoreClaimCommonSetDto;
import kr.co.homeplus.shipping.external.model.store.StoreClaimResultGetDto;
import kr.co.homeplus.shipping.external.model.store.StoreExchCompleteSetDto;
import kr.co.homeplus.shipping.external.model.store.StoreExchInfoGetDto;
import kr.co.homeplus.shipping.external.model.store.StoreExchStartSetDto;
import kr.co.homeplus.shipping.external.model.store.StorePickCompleteSetDto;
import kr.co.homeplus.shipping.external.model.store.StorePickInfoGetDto;
import kr.co.homeplus.shipping.external.model.store.StorePickStartSetDto;
import kr.co.homeplus.shipping.external.model.store.StoreShipCommonSetDto;
import kr.co.homeplus.shipping.external.model.store.StoreShipCompleteSetDto;
import kr.co.homeplus.shipping.external.model.store.StoreShipInfoGetDto;
import kr.co.homeplus.shipping.external.model.store.StoreShipResultGetDto;
import kr.co.homeplus.shipping.external.model.store.StoreShipStartSetDto;
import kr.co.homeplus.shipping.external.service.StoreService;
import kr.co.homeplus.shipping.test.mapper.TestMasterMapper;
import kr.co.homeplus.shipping.test.model.qa.QaShipStatusSetDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TestService {
    private final TestMasterMapper testMasterMapper;

    public int manualShipStatus(QaShipStatusSetDto qaShipStatusSetDto){
        return testMasterMapper.manualShipStatus(qaShipStatusSetDto);
    }

    public int manualPickStatus(QaShipStatusSetDto qaShipStatusSetDto){
        return testMasterMapper.manualPickStatus(qaShipStatusSetDto);
    }

    public int manualExchStatus(QaShipStatusSetDto qaShipStatusSetDto){
        return testMasterMapper.manualExchStatus(qaShipStatusSetDto);
    }
}
