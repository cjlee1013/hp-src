package kr.co.homeplus.shipping.market.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class MarketInquiryListGetDto {
    private Long messageNo; //문의 번호
    private String copOrdGoodSeq; //체결번호
    private String isresponseYn; //처리상태코드
    private String isresponseStatus; //처리상태
    private String marketType; //제휴사코드
    private String marketNm; //제휴사명
    private String messageType; //등록구분코드
    private String messageTypeNm; //등록구분명
    private String contactType; //문의타입
    private String contactNm; //문의타입명
    private String storeId; //점포코드
    private String storeNm; //점포명
    private String requestTitle; //제목
    private String receiveDate; //문의일시
    private String ordNo; //주문번호
    private String copOrdNo; //제휴사주문번호
    private String goodId; //상품번호
    private String copGoodId; //제휴사상품번호
    private String inquirerName; //문의자명
    private String inquirerPhone; //문의자연락처
    private String isresponseDate; //처리일시
    private String responseDiff; //처리소요시간
    private String isresponseNm; //답변작성자
}
