package kr.co.homeplus.shipping.admin.model.statistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StatisticsPartnerSetDto {
  @ApiModelProperty(notes = "판매업체")
  private String partnerId;
  @ApiModelProperty(notes = "시작일")
  private String startDt;
  @ApiModelProperty(notes = "종료일")
  private String endDt;
}
