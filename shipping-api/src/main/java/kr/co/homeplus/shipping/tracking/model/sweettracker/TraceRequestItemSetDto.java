package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TraceRequestItemSetDto {
  @ApiModelProperty(notes = "송장번호")
  private String num;

  @ApiModelProperty(notes = "택배사코드")
  private String code;

  @ApiModelProperty(notes = "배송번호")
  private String fid;

  public TraceRequestItemSetDto(String num, String code, String fid) {
    this.num = num;
    this.code = code;
    this.fid = fid;
  }

  public String getNum() {
    return num;
  }

  public String getCode() {
    return code;
  }

  public String getFid() {
    return fid;
  }
}
