package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModelProperty;
import java.beans.ConstructorProperties;
import kr.co.homeplus.shipping.tracking.model.pick.PickCallbackSetDto;
import lombok.Data;

@Data
public class ReturnRequestItemGetDto {
  @ApiModelProperty(notes = "처리결과")
  private String result;
  @ApiModelProperty(notes = "주문번호")
  private String ordCde;
  @ApiModelProperty(notes = "에러코드(err_ord_01:입력데이터 누락/err_ord_02:중복 주문 번호/err_ord_03:시스템 처리 오류")
  private String errCde;
  @ApiModelProperty(notes = "에러코드명")
  private String errMsg;
  @ApiModelProperty(notes = "스윗트래커 변환 주문번호")
  private String stOrdCde;


  public String getResult() {
    //중복호출은 성공으로 처리
    if ("err_ord_02".equals(errCde)) {
      return "Y";
    }
    return result;
  }

  public String getOrdCde() {
    return ordCde;
  }

  public String getErrCde() {
    return errCde;
  }

  public String getErrMsg() {
    return errMsg;
  }

  @ConstructorProperties({"result","ordCde","errCde","errMsg","stOrdCde"})
  public ReturnRequestItemGetDto(String result, String ordCde, String errCde, String errMsg,String stOrdCde) {
    this.result = result;
    this.ordCde = ordCde;
    this.errCde = errCde;
    this.errMsg = errMsg;
    this.stOrdCde = stOrdCde;
  }
}
