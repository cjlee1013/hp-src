package kr.co.homeplus.shipping.admin.mapper;

import java.util.List;
import kr.co.homeplus.shipping.admin.model.shipManage.ReturnDlvCompanyListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipLinkCompanyCodeListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipLinkCompanySetListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipCompanyManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipCompanyManageListSetDto;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;
import org.apache.ibatis.annotations.Param;

@SlaveConnection
public interface ShipCompanyManageSlaveMapper {
  List<ShipCompanyManageListGetDto> selectShipCompanyManageList(ShipCompanyManageListSetDto shipCompanyManageListSetDto);
  List<ShipLinkCompanyCodeListGetDto> selectLinkCompanyCodeList(@Param("company") String company);
  List<ShipLinkCompanySetListGetDto> selectShipLinkCompanySetList();
  List<ReturnDlvCompanyListGetDto> selectReturnDlvCompanyList();
}
