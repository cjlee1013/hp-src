package kr.co.homeplus.shipping.tracking.model.multi;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.shipping.tracking.model.common.CommonResultSetDto;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class MultiResultSetDto extends CommonResultSetDto {
    @ApiModelProperty(notes = "다중주문아이템번호")
    private String multiOrderItemNo;

    public MultiResultSetDto(String multiOrderItemNo, String success,String result, String detail) {
        super(success,result,detail);
        this.multiOrderItemNo = multiOrderItemNo;
    }

    public String getMultiOrderItemNo() {
        return multiOrderItemNo;
    }
}
