package kr.co.homeplus.shipping.core.exception;

import kr.co.homeplus.shipping.enums.ExceptionCode;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ShippingException extends RuntimeException {

    private final ExceptionCode returnCode;

    public ShippingException(ExceptionCode returnCode, String message) {
        super(message);
        this.returnCode = returnCode;
    }

    public ExceptionCode getReturnCode() {
        return this.returnCode;
    }

}
