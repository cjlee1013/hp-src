package kr.co.homeplus.shipping.tracking.model.exch;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.shipping.tracking.model.common.CommonTrackingGetDto;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class ExchTrackingGetDto implements CommonTrackingGetDto {
    @ApiModelProperty(notes = "클레임번들번호")
    private String claimExchShippingNo;

    @ApiModelProperty(notes = "배송번호")
    private String claimExchShippingNos;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    public String[] getClaimExchShippingNos() { return claimExchShippingNos.split(","); }

    @Override
    public String getNo() {
        return claimExchShippingNo;
    }

    @Override
    public String getInvoiceNo() {
        return invoiceNo;
    }

    @Override
    public String getDlvCd() {
        return dlvCd;
    }
}
