package kr.co.homeplus.shipping.common.model.history;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송 히스토리 입력")
public class CommonHistoryInsertSetDto {

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    @ApiModelProperty(notes = "송장번호")
    private String invoice_no;

    @ApiModelProperty(notes = "배송단계")
    private String level;

    @ApiModelProperty(notes = "택배사 처리시간")
    private String time_trans;

    @ApiModelProperty(notes = "스윗트래커 등록시간")
    private String time_sweet;

    @ApiModelProperty(notes = "택배 위치")
    private String where;

    @ApiModelProperty(notes = "사업소 기반 전화번호")
    private String telno_office;

    @ApiModelProperty(notes = "배송기사 전화번호")
    private String telno_man;

    @ApiModelProperty(notes = "배송상세 정보")
    private String details;

    @ApiModelProperty(notes = "수취인 주소")
    private String recv_addr;

    @ApiModelProperty(notes = "수취인 이름")
    private String recv_name;

    @ApiModelProperty(notes = "발신인 이름")
    private String send_name;

    @ApiModelProperty(notes = "배송기사 이름")
    private String man;

    @ApiModelProperty(notes = "배송예정 시간")
    private String estmate;

    @ApiModelProperty(notes = "등록자")
    private String regId;

    public CommonHistoryInsertSetDto(String dlvCd, String invoice_no, String level, String time_trans
                                    , String time_sweet, String where ,String telno_office ,String telno_man
                                    , String details, String recv_addr ,String recv_name ,String send_name
                                    , String man, String estmate ,String regId) {
        this.dlvCd = dlvCd;
        this.invoice_no = invoice_no;
        this.level = level;
        this.time_trans = time_trans;
        this.time_sweet = time_sweet;
        this.where = where;
        this.telno_office = telno_office;
        this.telno_man = telno_man;
        this.details = details;
        this.recv_addr = recv_addr;
        this.recv_name = recv_name;
        this.send_name = send_name;
        this.man = man;
        this.estmate = estmate;
        this.regId = regId;
    }
}
