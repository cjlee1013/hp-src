package kr.co.homeplus.shipping.tracking.model.shipping;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송완료 상태 변경")
public class ShippingCallbackSetDto {
    @ApiModelProperty(notes = "번들번호")
    private String shipNo;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "수거상태")
    private String callbackStatus;

    @ApiModelProperty(notes = "배송완료시간(yyyy-MM-dd HH:mm:ss)")
    private String callbackDt;

    public ShippingCallbackSetDto(String shipNo ,String invoiceNo, String callbackDt) {
        this.shipNo = shipNo;
        this.invoiceNo = invoiceNo;
        this.callbackDt = callbackDt;
    }

    public ShippingCallbackSetDto(String shipNo, String invoiceNo, String callbackStatus,
        String callbackDt) {
        this.shipNo = shipNo;
        this.invoiceNo = invoiceNo;
        this.callbackStatus = callbackStatus;
        this.callbackDt = callbackDt;
    }

    public String getShipNo() { return shipNo; }

    public void setShipNo(String shipNo) {
        this.shipNo = shipNo;
    }

    public String getCallbackStatus() {
        return callbackStatus;
    }
}
