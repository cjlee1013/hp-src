package kr.co.homeplus.shipping.admin.service.impl;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.admin.mapper.ShipCompanyManageMasterMapper;
import kr.co.homeplus.shipping.admin.mapper.ShipCompanyManageSlaveMapper;
import kr.co.homeplus.shipping.admin.model.shipManage.ReturnDlvCompanyListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipLinkCompanyCodeListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipLinkCompanySetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipLinkCompanySetListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipCompanyManageInfoSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipCompanyManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipCompanyManageListSetDto;
import kr.co.homeplus.shipping.admin.service.ShipCompanyManageService;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipCompanyManageServiceImpl implements ShipCompanyManageService {
    private final ShipCompanyManageSlaveMapper shipCompanyManageSlaveMapper;
    private final ShipCompanyManageMasterMapper shipCompanyManageMasterMapper;

    /**
     * 택배사코드관리 조회
     */
    @Override
    public List<ShipCompanyManageListGetDto> getShipCompanyManageList(ShipCompanyManageListSetDto shipCompanyManageListSetDto) {
        return shipCompanyManageSlaveMapper.selectShipCompanyManageList(shipCompanyManageListSetDto);
    }

    /**
     * 택배사코드관리 상세정보 저장
     */
    @Override
    public ResponseObject<String> setShipCompanyInfo(ShipCompanyManageInfoSetDto shipCompanyManageInfoSetDto) {
      try {
        // 저장
        int result = shipCompanyManageMasterMapper.insertShipCompanyInfo(shipCompanyManageInfoSetDto);

        return ResourceConverter.toResponseObject(Integer.toString(result), HttpStatus.OK);
      } catch (Exception e) {
        log.error("setShipCompanyInfo Error :: {}", e.getMessage());
        return ResourceConverter.toResponseObject("0", HttpStatus.OK, ExceptionCode.ERROR_CODE_3006.getCode(), ExceptionCode.ERROR_CODE_3006.getResponseMessage());
      }
    }

    /**
     * 연동업체설정 리스트 조회
     */
    @Override
    public List<ShipLinkCompanySetListGetDto> getShipLinkCompanySetList() {
      return shipCompanyManageSlaveMapper.selectShipLinkCompanySetList();
    }

    /**
     * 연동업체설정 저장
     */
    @Override
    public ResponseObject<String> setShipLinkCompanySet(ShipLinkCompanySetDto shipLinkCompanySetDto) {
      try {
        // 저장
        int result = shipCompanyManageMasterMapper.insertShipLinkCompanySet(shipLinkCompanySetDto);

        return ResourceConverter.toResponseObject(Integer.toString(result), HttpStatus.OK);
      } catch (Exception e) {
        log.error("setShipLinkCompanySet Error : {}", e.getMessage());
        return ResourceConverter.toResponseObject("0", HttpStatus.OK, ExceptionCode.ERROR_CODE_3007.getCode(), ExceptionCode.ERROR_CODE_3007.getResponseMessage());
      }
    }

    /**
     * 연동업체코드 리스트 조회
     */
    @Override
    public List<ShipLinkCompanyCodeListGetDto> getLinkCompanyCodeList(String company) {
      return shipCompanyManageSlaveMapper.selectLinkCompanyCodeList(company);
    }

    /**
     * 반품택배사 리스트 조회
     */
    @Override
    public List<ReturnDlvCompanyListGetDto> getReturnDlvCompanyList() {
      return shipCompanyManageSlaveMapper.selectReturnDlvCompanyList();
    }
}
