package kr.co.homeplus.shipping.market.model;

import lombok.Data;

@Data
public class MarketInquiryDetailGetDto {
    private Long messageNo; //문의번호
    private String storeNm; //점포명
    private String marketNm; //제휴사명
    private String isresponseStatus; //처리상태
    private String contactNm; //문의유형
    private String ordNo; //주문번호
    private String goodId; //상품번호
    private String copGoodId; //제휴사상품번호
    private String receiveDate; //접수일
    private String inquirerName; //회원명
    private String inquirerPhone; //회원연락처
    private String requestTitle; //문의제목
    private String requestComments; //문의내용
    private String isresponseNm; //답변작성자
    private String isresponseDate; //답변일
    private String responseTitle; //답변제목
    private String responseContents; //답변내용
}