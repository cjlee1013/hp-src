package kr.co.homeplus.shipping.market.mapper;

import kr.co.homeplus.shipping.core.db.annotation.MasterConnection;
import kr.co.homeplus.shipping.market.model.MarketInquiryInfoDto;
import org.apache.ibatis.annotations.Param;

@MasterConnection
public interface NaverInquiryMasterMapper {
  int updateNaverInquiryInfo(
      @Param("dto") MarketInquiryInfoDto naverInquiryGetDto
      , @Param("isresponseId") String isresponseId
      , @Param("isresponseNm") String isresponseNm);
}
