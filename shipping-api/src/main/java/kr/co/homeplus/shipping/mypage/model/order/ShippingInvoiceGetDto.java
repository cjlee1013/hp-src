package kr.co.homeplus.shipping.mypage.model.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "배송 히스토리 정보")
public class ShippingInvoiceGetDto {
    @ApiModelProperty(notes = "택배사")
    private String dlvNm;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "배송상태")
    private String status;

    @ApiModelProperty(notes = "추적결과")
    private String trackingResult;

    @ApiModelProperty(notes = "변경내역")
    private List<ShippingHistoryGetDto> historyList;

    public String getDlvCd() {
        return dlvCd;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setHistoryList(
        List<ShippingHistoryGetDto> historyList) {
        this.historyList = historyList;
    }

    public List<ShippingHistoryGetDto> getHistoryList() {
        return historyList;
    }
}
