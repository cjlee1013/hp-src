package kr.co.homeplus.shipping.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ShipMethod {
	DS_DLV("DS_DLV","택배배송"),
	DS_DRCT("DS_DRCT","직접배송"),
	DS_PICK("DS_PICK","방문수령(픽업)"),
	DS_POST("DS_POST","우편배송"),
	DS_QUICK("DS_QUICK","퀵배송"),

	TD_DLV("TD_DLV","택배배송"),
	TD_DRCT("TD_DRCT","자차"),
	TD_PICK("TD_PICK","픽업"),
	TD_QUICK("TD_QUICK","퀵")
	;

	private String code;
	private String name;

	public static String getShipMethodNm(String shipMethod) {
		return ShipMethod.valueOf(shipMethod).getName();
	}

	/*
	 * 배송방법 존재여부 확인
	 */
	public static boolean isShipMethodNm(String shipMethod) {
		for(ShipMethod shipMethodEnum : ShipMethod.values()) {
			if(shipMethodEnum.getCode().equals(shipMethod)) {
				return true;
			}
		}

		return false;
	}
}
