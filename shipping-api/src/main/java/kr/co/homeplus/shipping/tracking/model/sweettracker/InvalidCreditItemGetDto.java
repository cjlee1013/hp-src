package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.beans.ConstructorProperties;
import lombok.Data;

@Data
@ApiModel(description = "신용코드 확인 아이템")
public class InvalidCreditItemGetDto {
    @ApiModelProperty(notes = "조회 성공여부")
    private String result;
    @ApiModelProperty(notes = "택배사별 신용코드")
    private String cusCde;
    @ApiModelProperty(notes = "택배사코드")
    private String comCode;
    @ApiModelProperty(notes = "신용코드 사용가능여부")
    private String cusUse;
    @ApiModelProperty(notes = "에러코드(err_cre_01:입력데이터 누락/err_cre_02:시스템 처리 오류")
    private String errCde;
    @ApiModelProperty(notes = "에러코드명")
    private String errMsg;

    public String getResult() {
        return result;
    }

    public String getCusCde() {
        return cusCde;
    }

    public String getComCode() {
        return comCode;
    }

    public String getCusUse() {
        return cusUse;
    }

    public String getErrCde() {
        return errCde;
    }

    public String getErrMsg() {
        return errMsg;
    }

    @ConstructorProperties({"result","cusCde","comCode","cusUse","errCde","errMsg"})
    public InvalidCreditItemGetDto(String result, String cusCde, String comCode,
        String cusUse, String errCde, String errMsg) {
        this.result = result;
        this.cusCde = cusCde;
        this.comCode = comCode;
        this.cusUse = cusUse;
        this.errCde = errCde;
        this.errMsg = errMsg;
    }
}
