package kr.co.homeplus.shipping.partner.mapper;

import java.util.List;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageListSetDto;
import org.apache.ibatis.annotations.Param;

@SlaveConnection
public interface RentalManageSlaveMapper {
  List<RentalManageListGetDto> selectRentalManageList(RentalManageListSetDto rentalManageListSetDto);
  List<RentalManageListGetDto> selectRentalManageCntList(@Param("schType") String schType, @Param("partnerId") String partnerId);
  String selectRentalManageCnt(@Param("schType") String schType, @Param("partnerId") String partnerId);
}
