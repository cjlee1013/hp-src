package kr.co.homeplus.shipping.tracking.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.common.model.history.MultiShippingHistoryInsertSetDto;
import kr.co.homeplus.shipping.common.service.HistoryService;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.tracking.model.multi.MultiCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiResultSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceCallbackGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceRequestItemGetDto;
import kr.co.homeplus.shipping.tracking.service.SweettrackerService;
import kr.co.homeplus.shipping.tracking.service.TrackingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/tracking/multi")
@Api(tags = "배송 추적 및 조회")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class MultiController {
  private final SweettrackerService sweettrackerService;
  private final TrackingService trackingService;
  private final HistoryService historyService;

  @ApiOperation(value = "배송 추적 요청", response = ResponseObject.class, notes = "필수값 : 배송번호, 송장번호, 택배사코드")
  @RequestMapping(value = "/requestTracking", method = {RequestMethod.POST})
  public ResponseObject<List<MultiRequestGetDto>> requestTracking(@RequestBody @Valid List<MultiRequestSetDto> multiTrackingGetDtoList) {
    if (multiTrackingGetDtoList.size() > 0) {
      Map<String, MultiTrackingGetDto> multiTrackingGetDtoMap = trackingService.getMultiInvoice(multiTrackingGetDtoList);
      if (multiTrackingGetDtoMap != null) {
        List<MultiRequestGetDto> multiRequestGetDtoList = new ArrayList<>();
        MultiResultSetDto multiResultSetDto;
        String callbackUrl = "/tracking/multi/callbackTrackingSWT";
        TraceRequestGetDto traceRequestGetDto = sweettrackerService.requestTracking(callbackUrl,
            multiTrackingGetDtoMap.values());
        if (traceRequestGetDto != null) {
          for (TraceRequestItemGetDto item : traceRequestGetDto.getList()) {
            multiResultSetDto = new MultiResultSetDto(item.getFid(), item.getSuccess(),
                item.getDetail(),item.getE_code() +":"+ item.getE_message());
            multiRequestGetDtoList.addAll(
                trackingService.setMultiTrackingResult(multiResultSetDto,
                    multiTrackingGetDtoMap));
          }
          return ResourceConverter.toResponseObject(multiRequestGetDtoList);
        }
      }
    }
    return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.ERROR_CODE_2001.getResponseCode(), ExceptionCode.ERROR_CODE_2001.getResponseMessage());
  }

  @ApiOperation(value = "배송 추적 콜백", response = ResponseObject.class, notes = "스윗트래커에서 호출")
  @RequestMapping(value = "/callbackTrackingSWT", method = {RequestMethod.POST})
  public TraceCallbackGetDto callbackTrackingSWT(TraceCallbackSetDto traceCallbackSetDto) {
    MultiShippingHistoryInsertSetDto multiShippingHistoryInsertSetDto = traceCallbackSetDto.toMultiHistoryInsertSetDto(sweettrackerService.getDlvCdMappingStoH());
    if (multiShippingHistoryInsertSetDto == null) {
      log.info(traceCallbackSetDto.toString());
      return new TraceCallbackGetDto("false", "fail - invalid courier code");
    }
    int historyResult = historyService.setMultiShippingHistoryInvoice(multiShippingHistoryInsertSetDto);
    if (historyResult > 0) {
      MultiCallbackSetDto multiCallbackSetDto = traceCallbackSetDto.toMultiCallbackSetDto();
      int callbackResult = 0;
      if (multiCallbackSetDto != null) {
        historyService.setMultiShippingProcessHistory(traceCallbackSetDto.toMultiProcessInsertSetDto());
        callbackResult = trackingService.setMultiTrackingCallback(multiCallbackSetDto);
        if (callbackResult > 0) {
          return new TraceCallbackGetDto("true", "success");
        } else {
          return new TraceCallbackGetDto("false", "fail - invalid fid");
        }
      } else {
        return new TraceCallbackGetDto("true", "success");
      }
    }
    return new TraceCallbackGetDto("false", "system error");
  }
}
