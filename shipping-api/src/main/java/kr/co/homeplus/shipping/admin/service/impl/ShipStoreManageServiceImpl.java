package kr.co.homeplus.shipping.admin.service.impl;

import java.util.Iterator;
import java.util.List;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.shipping.admin.mapper.ShipStoreManageSlaveMapper;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipStoreManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipStoreManageListSetDto;
import kr.co.homeplus.shipping.admin.service.ShipStoreManageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipStoreManageServiceImpl implements ShipStoreManageService {
    private final ShipStoreManageSlaveMapper shipStoreManageSlaveMapper;

    /**
     * 배송관리 조회
     */
    @Override
    public List<ShipStoreManageListGetDto> getShipStoreManageList(ShipStoreManageListSetDto shipStoreManageListSetDto) {
      log.info("getShipStoreManageList param : {}", shipStoreManageListSetDto);

      List<ShipStoreManageListGetDto> shipStoreManageListGetDtoList = shipStoreManageSlaveMapper.selectShipStoreManageList(shipStoreManageListSetDto);

      Iterator<ShipStoreManageListGetDto> iterator = shipStoreManageListGetDtoList.iterator();

      while (iterator.hasNext()) {
        ShipStoreManageListGetDto dto = iterator.next();

        // 취소 데이터 노출 제외
        if(Integer.parseInt(dto.getBundleQty()) < 1) {
          iterator.remove();
        } else {
          dto.setBuyerMobileNo(PrivacyMaskingUtils.maskingPhone(dto.getBuyerMobileNo()));
          dto.setShipMobileNo(PrivacyMaskingUtils.maskingPhone(dto.getShipMobileNo()));
          dto.setBuyerNm(PrivacyMaskingUtils.maskingUserName(dto.getBuyerNm()));
          dto.setReceiverNm(PrivacyMaskingUtils.maskingUserName(dto.getReceiverNm()));
          dto.setAddr(PrivacyMaskingUtils.maskingAddress(dto.getAddr()));
        }
      }

      return shipStoreManageListGetDtoList;
    }

  /**
   * 배송Shift 리스트 조회
   */
  public List<String> getStoreShiftList(String storeId, String storeType) {
    return shipStoreManageSlaveMapper.selectStoreShiftList(storeId, storeType);
  }
}
