package kr.co.homeplus.shipping.common.model.history;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송 히스토리 입력")
public class ProcessHistoryInsertSetDto {
    @ApiModelProperty(notes = "등록자")
    private String historyReason;

    @ApiModelProperty(notes = "등록자")
    private String historyDetailDesc;

    @ApiModelProperty(notes = "등록자")
    private String historyDetailBefore;

    @ApiModelProperty(notes = "등록자")
    private String regChannel;

    @ApiModelProperty(notes = "등록자")
    private String regId;

    public ProcessHistoryInsertSetDto(String historyReason, String historyDetailDesc, String historyDetailBefore, String regChannel, String regId) {
        this.historyReason = historyReason;
        this.historyDetailDesc = historyDetailDesc;
        this.historyDetailBefore = historyDetailBefore;
        this.regChannel = regChannel;
        this.regId = regId;
    }
}
