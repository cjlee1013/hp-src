package kr.co.homeplus.shipping.tracking.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.common.model.history.PickHistoryInsertSetDto;
import kr.co.homeplus.shipping.common.service.HistoryService;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.tracking.config.SweettrackerConfig;
import kr.co.homeplus.shipping.tracking.model.pick.PickCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickAddressGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickResultSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickValidCreditGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickValidCreditSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnCallbackGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnCallbackItemGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnCallbackItemSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnCancelGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnCancelItemGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnRequestItemGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceCallbackGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.TraceRequestItemGetDto;
import kr.co.homeplus.shipping.tracking.service.SweettrackerService;
import kr.co.homeplus.shipping.tracking.service.TrackingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/tracking/pick")
@Api(tags = "배송 추적 및 조회")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class PickController {
  private final SweettrackerService sweettrackerService;
  private final TrackingService trackingService;
  private final HistoryService historyService;

  private final SweettrackerConfig sweettrackerConfig;

  @ApiOperation(value = "반품 배송 추적 요청", response = ResponseObject.class, notes = "필수값 : 반품번호, 송장번호, 택배사코드")
  @RequestMapping(value = "/requestTracking", method = {RequestMethod.POST})
  public ResponseObject<List<PickRequestGetDto>> requestTracking(@RequestBody @Valid List<PickRequestSetDto> pickRequestSetDtoList) {
    if (pickRequestSetDtoList.size() > 0) {
      Map<String, PickTrackingGetDto> pickTrackingGetDtoMap = trackingService.getPickInvoice(pickRequestSetDtoList);
      if (pickTrackingGetDtoMap != null) {
        List<PickRequestGetDto> pickRequestGetDtoList = new ArrayList<>();
        PickResultSetDto pickResultSetDto;
        String callbackUrl = "/tracking/pick/callbackTrackingSWT";
        TraceRequestGetDto traceRequestGetDto = sweettrackerService.requestTracking(callbackUrl,
            pickTrackingGetDtoMap.values());
        if (traceRequestGetDto != null) {
          for (TraceRequestItemGetDto item : traceRequestGetDto.getList()) {
            pickResultSetDto = new PickResultSetDto(item.getFid(), item.getSuccess(), item.getDetail(),item.getE_code() +":"+ item.getE_message());
            pickRequestGetDtoList.addAll(
                trackingService.setPickTrackingResult(pickResultSetDto,pickTrackingGetDtoMap));
          }
          return ResourceConverter.toResponseObject(pickRequestGetDtoList);
        }
      }
    }
    return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK,
        ExceptionCode.ERROR_CODE_2101.getResponseCode(),
        ExceptionCode.ERROR_CODE_2101.getResponseMessage());
  }

  @ApiOperation(value = "배송 요청 콜백", response = ResponseObject.class, notes = "스윗트래커에서 호출")
  @RequestMapping(value = "/callbackTrackingSWT", method = {RequestMethod.POST})
  public TraceCallbackGetDto callbackTrackingSWT(TraceCallbackSetDto traceCallbackSetDto) {
    PickHistoryInsertSetDto pickHistoryInsertSetDto = traceCallbackSetDto.toPickHistoryInsertSetDto(sweettrackerService.getDlvCdMappingStoH());
    if (pickHistoryInsertSetDto == null) {
      log.info(traceCallbackSetDto.toString());
      return new TraceCallbackGetDto("false", "fail - invalid courier code");
    }
    int historyResult = historyService.setPickHistoryInvoice(pickHistoryInsertSetDto);
    if (historyResult > 0) {
      PickCallbackSetDto pickCallbackSetDto = traceCallbackSetDto.toPickCallbackSetDto();
      int callbackResult = 0;
      if (pickCallbackSetDto != null) {
        historyService.setPickProcessHistory(traceCallbackSetDto.toPickProcessInsertSetDto());
        callbackResult = trackingService.setPickTrackingCallback(pickCallbackSetDto);
        if (callbackResult > 0) {
          return new TraceCallbackGetDto("true", "success");
        } else {
          return new TraceCallbackGetDto("false", "fail - invalid fid");
        }
      } else {
        return new TraceCallbackGetDto("true", "success");
      }
    }
    return new TraceCallbackGetDto("false", "system error");
  }


  @ApiOperation(value = "반품 배송 접수 요청", response = ResponseObject.class, notes = "필수값 : 반품번호")
  @RequestMapping(value = "/requestPick", method = {RequestMethod.POST})
  public ResponseObject<List<PickRequestGetDto>> requestPick(@RequestBody @Valid List<PickRequestSetDto> pickTrackingGetDtoList) {
    if (pickTrackingGetDtoList.size() > 0) {
      List<PickAddressGetDto> addressList = trackingService.getPickAddress(pickTrackingGetDtoList);
      ReturnRequestGetDto returnRequestGetDto = sweettrackerService.requestPick(addressList);
      if (returnRequestGetDto == null) {
        return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK,
            ExceptionCode.ERROR_CODE_2101.getResponseCode(),
            ExceptionCode.ERROR_CODE_2101.getResponseMessage());
      } else {
        List<PickRequestGetDto> pickRequestGetDtoList = new ArrayList<>();
        PickResultSetDto pickResultSetDto;
        for (ReturnRequestItemGetDto item : returnRequestGetDto.getData()) {
          pickResultSetDto = new PickResultSetDto(item.getOrdCde(), item.getResult(),
              item.getErrMsg(),item.getErrCde() +":"+ item.getErrMsg());
          pickRequestGetDtoList.add(trackingService.setPickRequestResult(pickResultSetDto));
        }
        return ResourceConverter.toResponseObject(pickRequestGetDtoList);
      }
    }
    return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK,
        ExceptionCode.ERROR_CODE_2101.getResponseCode(),
        ExceptionCode.ERROR_CODE_2101.getResponseMessage());
  }


  @ApiOperation(value = "반품 배송 접수 취소", response = ResponseObject.class, notes = "필수값 : 반품번호")
  @RequestMapping(value = "/cancelPick", method = {RequestMethod.POST})
  public ResponseObject<List<PickRequestGetDto>> cancelPick(@RequestBody @Valid List<PickRequestSetDto> pickRequestSetDtoList) {
    if (pickRequestSetDtoList.size() > 0) {
      ReturnCancelGetDto returnCancelGetDtoList = sweettrackerService.cancelPick(
          pickRequestSetDtoList);
      if (returnCancelGetDtoList == null) {
        return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK,
            ExceptionCode.ERROR_CODE_2101.getResponseCode(),
            ExceptionCode.ERROR_CODE_2101.getResponseMessage());
      } else {
        List<PickRequestGetDto> pickRequestGetDtoList = new ArrayList<>();
        PickResultSetDto pickResultSetDto;
        for (ReturnCancelItemGetDto item : returnCancelGetDtoList.getData()) {
          pickResultSetDto = new PickResultSetDto(item.getOrdCde(), item.getResult(),
              item.getErrMsg(),item.getErrCde() +":"+ item.getErrMsg());
          pickRequestGetDtoList.add(trackingService.setPickRequestCancel(pickResultSetDto));
        }
        return ResourceConverter.toResponseObject(pickRequestGetDtoList);
      }
    }
    return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK,
        ExceptionCode.ERROR_CODE_2101.getResponseCode(),
        ExceptionCode.ERROR_CODE_2101.getResponseMessage());
  }


  @ApiOperation(value = "반품 배송 접수 콜백", response = ResponseObject.class, notes = "스윗트래커에서 호출")
  @RequestMapping(value = "/callbackPickSWT", method = {RequestMethod.POST})
  public ReturnCallbackGetDto callbackPickSWT(@RequestBody @Valid ReturnCallbackSetDto returnCallbackSetDto) {
    log.info(returnCallbackSetDto.toString());
    int callbackResult,historyResult;
    List<ReturnCallbackItemGetDto> returnCallbackItemGetDtoList = new ArrayList<>();
    for (ReturnCallbackItemSetDto item : returnCallbackSetDto.getResult_data()) {
      historyResult = historyService.setPickProcessHistory(item.toPickProcessInsertSetDto());
      callbackResult = trackingService.setPickRequestCallback(item.toPickCallbackSetDto(sweettrackerService.getDlvCdMappingStoH()));
      if (callbackResult > 0 && historyResult > 0) {
        returnCallbackItemGetDtoList.add(new ReturnCallbackItemGetDto("Y", item.getOrdCde()));
      } else {
        returnCallbackItemGetDtoList.add(new ReturnCallbackItemGetDto("N", item.getOrdCde()));
      }
    }
    return new ReturnCallbackGetDto(returnCallbackItemGetDtoList);
  }



  @ApiOperation(value = "택배사 신용코드 유효성체크", response = ResponseObject.class, notes = "필수값 : 신용코드 , 택배사 코드")
  @RequestMapping(value = "/getInvalidCredit", method = {RequestMethod.POST})
  public ResponseObject<List<PickValidCreditGetDto>> getInvalidCredit(@RequestBody @Valid List<PickValidCreditSetDto> pickValidCreditSetDtoList) {
    if (pickValidCreditSetDtoList.size() > 0) {
      List<PickValidCreditGetDto> PickValidCreditGetDtoList = sweettrackerService.getInvalidCredit(
          pickValidCreditSetDtoList);
      if (PickValidCreditGetDtoList != null) {
        return ResourceConverter.toResponseObject(PickValidCreditGetDtoList);
      }
    }
    return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK,
        ExceptionCode.ERROR_CODE_2101.getResponseCode(),
        ExceptionCode.ERROR_CODE_2101.getResponseMessage());
  }
}
