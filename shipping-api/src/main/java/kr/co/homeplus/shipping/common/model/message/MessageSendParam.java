package kr.co.homeplus.shipping.common.model.message;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
public class MessageSendParam {

    @ApiModelProperty(value = "발송 캠페인 명")
    private String workName;

    @ApiModelProperty(value = "발송 캠페인 설명")
    private String description;

    @ApiModelProperty(value = "등록자")
    private String regId;

    @ApiModelProperty(value = "템플릿 코드")
    private String templateCode;

    @ApiModelProperty(value = "전환발송 사용 시 템플릿 코드")
    private String switchTemplateCode;

    @ApiModelProperty(value = "개인별 매핑 데이터")
    private List<MessageSendBodyArgument> bodyArgument;

    public MessageSendParam (String workName, String description, String regId, String templateCode) {
        this.workName = workName;
        this.description = description;
        this.regId = regId;
        this.templateCode = templateCode;
    }

    public MessageSendParam (String workName, String description, String regId, String templateCode, String switchTemplateCode) {
        this.workName = workName;
        this.description = description;
        this.regId = regId;
        this.templateCode = templateCode;
        this.switchTemplateCode = switchTemplateCode;
    }
}
