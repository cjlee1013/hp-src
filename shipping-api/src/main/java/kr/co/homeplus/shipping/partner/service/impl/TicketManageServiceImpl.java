package kr.co.homeplus.shipping.partner.service.impl;

import static kr.co.homeplus.shipping.constants.ResourceRouteName.PARTNER;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.shipping.common.model.history.ShippingProcessInsertSetDto;
import kr.co.homeplus.shipping.common.service.HistoryService;
import kr.co.homeplus.shipping.core.exception.ShippingException;
import kr.co.homeplus.shipping.enums.DateType;
import kr.co.homeplus.shipping.partner.mapper.TicketManageMasterMapper;
import kr.co.homeplus.shipping.partner.mapper.TicketManageSlaveMapper;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageCompleteArgDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageCompleteSetDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageConfirmOrderSetDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageListSetDto;
import kr.co.homeplus.shipping.partner.service.PartnerCommonService;
import kr.co.homeplus.shipping.partner.service.TicketManageService;
import kr.co.homeplus.shipping.utils.DateUtil;
import kr.co.homeplus.shipping.utils.ResponseUtil;
import kr.co.homeplus.shipping.utils.ShipInfoUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class TicketManageServiceImpl implements TicketManageService {
    private final TicketManageSlaveMapper ticketManageSlaveMapper;
    private final TicketManageMasterMapper ticketManageMasterMapper;
    private final HistoryService historyService;
    private final PartnerCommonService partnerCommonService;

    /**
     * 이티켓판매관리 조회
     */
    @Override
    public List<TicketManageListGetDto> getTicketManageList(TicketManageListSetDto ticketManageListSetDto) {
      log.info("getTicketManageList param : {}", ticketManageListSetDto);

      List<TicketManageListGetDto> ticketManageListGetDtoList = ticketManageSlaveMapper.selectTicketManageList(ticketManageListSetDto);

      return this.getMaskingTicketManageList(ticketManageListGetDtoList);
    }

    /**
     * 이티켓판매관리 카운트 리스트 조회
     */
    @Override
    public List<TicketManageListGetDto> getTicketManageCntList(String schType, String partnerId) {
      log.info("getTicketManageCntList param : {}", partnerId);

      List<TicketManageListGetDto> ticketManageListGetDtoList = ticketManageSlaveMapper.selectTicketManageCntList(schType, partnerId);

      return this.getMaskingTicketManageList(ticketManageListGetDtoList);
    }

    /**
     * 이티켓판매관리 리스트 마스킹
     */
    private List<TicketManageListGetDto> getMaskingTicketManageList(List<TicketManageListGetDto> ticketManageList) {
      for (TicketManageListGetDto dto : ticketManageList) {
        // 배송완료 7일이 지나면 마스킹
        if (DateUtil.isAfter(dto.getCompleteDt(), DateType.DAY, 8)) {
          dto.setCompleteAfter3MonthYn("Y");
          dto.setBuyerNm(PrivacyMaskingUtils.maskingUserName(dto.getBuyerNm()));
          dto.setBuyerMobileNo(PrivacyMaskingUtils.maskingPhone(dto.getBuyerMobileNo()));
          dto.setReceiverNm(PrivacyMaskingUtils.maskingUserName(dto.getReceiverNm()));
          dto.setShipMobileNo(PrivacyMaskingUtils.maskingPhone(dto.getShipMobileNo()));
          dto.setAddr(PrivacyMaskingUtils.maskingAddress(dto.getAddr()));
          dto.setPersonalOverseaNo(PrivacyMaskingUtils.maskingCustomsIdNumber(dto.getPersonalOverseaNo()));
        }
      }

      return ticketManageList;
    }

    /**
     * 이티켓판매관리 카운트 조회
     */
    @Override
    public TicketManageCntGetDto getTicketManageCnt(String partnerId) {
      log.info("getTicketManageCnt param : {}", partnerId);

      TicketManageCntGetDto ticketManageCntGetDto = new TicketManageCntGetDto();

      ticketManageCntGetDto.setReadyCnt(ticketManageSlaveMapper.selectTicketManageCnt("readyCnt", partnerId));
      ticketManageCntGetDto.setCallCnt(ticketManageSlaveMapper.selectTicketManageCnt("callCnt", partnerId));
      ticketManageCntGetDto.setFailCnt(ticketManageSlaveMapper.selectTicketManageCnt("failCnt", partnerId));

      return ticketManageCntGetDto;
    }

    /**
     * 주문확인
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, ShippingException.class})
    public ResponseObject<String> setTicketConfirmOrder(TicketManageConfirmOrderSetDto ticketManageConfirmOrderSetDto) {
      log.info("setTicketConfirmOrder param : {}", ticketManageConfirmOrderSetDto);

      int result = 0;
      String chgId = ticketManageConfirmOrderSetDto.getChgId();
      List<String> shipNoList = ticketManageConfirmOrderSetDto.getShipNoList();

      // 주문확인 처리
      for (String shipNo : shipNoList) {
        // 파트너 물품인지 체크
        if(!partnerCommonService.isPartnerItemByShipNo(shipNo, chgId)) {
          historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("티켓주문확인 실패", "파트너ID가 존재하지 않거나 본 계정의 물품이 아닙니다","", PARTNER, chgId, shipNo));
          continue;
        }

        // COOP 티켓인지 확인
        if(partnerCommonService.isCoopTicketByShipNo(shipNo)) {
          historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("티켓주문확인 실패", "COOP티켓은 처리할 수 없습니다","", PARTNER, chgId, shipNo));
          continue;
        }

        int returnVal = ticketManageMasterMapper.updateTicketConfirmOrder(shipNo,chgId);

        if (returnVal == 1){
          historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("티켓주문확인", "","", PARTNER, chgId, shipNo));
          result++;
        } else {
          historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("티켓주문확인 실패", "","", PARTNER, chgId, shipNo));
        }
      }

      return ResponseUtil.getResoponseObject(Integer.toString(result));
    }

    /**
     * 발급완료
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, ShippingException.class})
    public ResponseObject<String> setTicketComplete(TicketManageCompleteSetDto ticketManageCompleteSetDto) {
      log.info("setTicketComplete param : {}", ticketManageCompleteSetDto);

      int result = 0;
      String chgId = ticketManageCompleteSetDto.getChgId();
      List<TicketManageCompleteArgDto> ticketManageCompleteArgDtoList = ticketManageCompleteSetDto.getShipNoList();

      for (TicketManageCompleteArgDto ticketManageCompleteArgDto : ticketManageCompleteArgDtoList) {
        String shipNo = ticketManageCompleteArgDto.getShipNo();
        List<String> orderTicketNoList = ticketManageCompleteArgDto.getOrderTicketNoList();
        int returnVal = 0;
        int updateResult = 0;

        // 파트너 물품인지 체크
        if(!partnerCommonService.isPartnerItemByShipNo(shipNo, chgId)) {
          historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("티켓발급완료처리 실패", "파트너ID가 존재하지 않거나 본 계정의 물품이 아닙니다","", PARTNER, chgId, shipNo));
          continue;
        }

        // COOP 티켓인지 확인
        if(partnerCommonService.isCoopTicketByShipNo(shipNo)) {
          historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("티켓발급완료처리 실패", "COOP티켓은 처리할 수 없습니다","", PARTNER, chgId, shipNo));
          continue;
        }

        // 티켓 상태 업데이트
        for (String orderTicketNo : orderTicketNoList) {
          returnVal = ticketManageMasterMapper.updateOrderTicketComplete(orderTicketNo,chgId);

          if (returnVal == 1) {
            historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("티켓발급완료처리", "티켓상태업데이트:" + orderTicketNo,"", PARTNER, chgId, shipNo));
            updateResult++;
          } else {
            historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("티켓발급완료처리 실패", "티켓상태업데이트:"+orderTicketNo,"", PARTNER, chgId, shipNo));
          }
        }

        // 배송 상태 업데이트
        if (updateResult > 0) {
          // 배송 상태 조회
          String shipStatus = ticketManageSlaveMapper.selectTicketShipStatus(shipNo);

          if ("D2".equals(shipStatus)) {
            returnVal = ticketManageMasterMapper.updateShippingTicketComplete(shipNo,chgId);

            if (returnVal == 1) {
              historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("티켓발급완료처리", "배송상태업데이트","", PARTNER, chgId, shipNo));
            } else {
              historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("티켓발급완료처리 실패", "배송상태업데이트","", PARTNER, chgId, shipNo));
            }
          }
        }

        result =+ updateResult;
      }

      return ResponseUtil.getResoponseObject(Integer.toString(result));
    }
}
