package kr.co.homeplus.shipping.test.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.test.model.qa.QaShipStatusSetDto;
import kr.co.homeplus.shipping.test.service.TestService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/test/qa")
@Api(tags = "QA용 임시API")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
@Profile("local|dev|qa")
public class QaController {

  private final TestService testService;

  @ApiOperation(value = "배송상태 변경", response = ResponseObject.class, notes = "배송상태(D1:결제완료,D2:상품준비중,D3:배송중,D4:배송완료,D5:구매확정)/시간(미입력시 현재시간)/키타입(ship_no,bundle_no,purchase_order_no,order_item_no)/키목록")
  @RequestMapping(value = "/manualShipStatus", method = {RequestMethod.POST})
  public ResponseObject<String> manualShipStatus(@RequestBody @Valid QaShipStatusSetDto qaShipStatusSetDto) {
    int results = testService.manualShipStatus(qaShipStatusSetDto);

    if (results > 0 ) {
      return ResourceConverter.toResponseObject("SUCCESS");
    } else {
      return ResourceConverter.toResponseObject("FAIL");
    }
  }

  @ApiOperation(value = "반품상태 변경", response = ResponseObject.class, notes = "배송상태(P0:수거예약,P1:송장등록,P2:수거중,P3:수거완료,P8:수거실패)/시간(미입력시 현재시간)/키타입(claim_bundle_no,claim_pick_shipping_no)/키목록")
  @RequestMapping(value = "/manualPickStatus", method = {RequestMethod.POST})
  public ResponseObject<String> manualPickStatus(@RequestBody @Valid QaShipStatusSetDto qaShipStatusSetDto) {
    int results = testService.manualPickStatus(qaShipStatusSetDto);

    if (results > 0 ) {
      return ResourceConverter.toResponseObject("SUCCESS");
    } else {
      return ResourceConverter.toResponseObject("FAIL");
    }
  }

  @ApiOperation(value = "교환상태 변경", response = ResponseObject.class, notes = "배송상태(D0:상품출고대기,D1:송장등록,D2:배송중,D3:배송완료)/시간(미입력시 현재시간)/키타입(claim_bundle_no,claim_exch_shipping_no)/키목록")
  @RequestMapping(value = "/manualExchStatus", method = {RequestMethod.POST})
  public ResponseObject<String> manualExchStatus(@RequestBody @Valid QaShipStatusSetDto qaShipStatusSetDto) {
    int results = testService.manualExchStatus(qaShipStatusSetDto);

    if (results > 0 ) {
      return ResourceConverter.toResponseObject("SUCCESS");
    } else {
      return ResourceConverter.toResponseObject("FAIL");
    }
  }
}
