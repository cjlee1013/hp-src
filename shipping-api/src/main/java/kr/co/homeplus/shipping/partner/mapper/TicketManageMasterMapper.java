package kr.co.homeplus.shipping.partner.mapper;

import kr.co.homeplus.shipping.core.db.annotation.MasterConnection;
import org.apache.ibatis.annotations.Param;

@MasterConnection
public interface TicketManageMasterMapper {
  int updateTicketConfirmOrder(@Param("shipNo") String shipNo, @Param("chgId") String chgId);
  int updateOrderTicketComplete(@Param("orderTicketNo") String orderTicketNo, @Param("chgId") String chgId);
  int updateShippingTicketComplete(@Param("shipNo") String shipNo, @Param("chgId") String chgId);
}