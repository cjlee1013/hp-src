package kr.co.homeplus.shipping.tracking.model.pick;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class PickRequestSetDto {
    @ApiModelProperty(notes = "배송번호")
    private String claimPickShippingNo;

    public String getClaimPickShippingNo() {
        return claimPickShippingNo;
    }
}
