package kr.co.homeplus.shipping.tracking.model.shipping;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class ShippingRequestGetDto {
    @ApiModelProperty(notes = "배송번호")
    private String shipNo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "성공여부")
    private String success;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "결과값")
    private String detail;

    public ShippingRequestGetDto(String shipNo, String success, String detail) {
        this.shipNo = shipNo;
        this.success = success;
        this.detail = detail;
    }

    public void appendDetail(String detail) { this.detail = this.detail.concat(" " + detail); }
}
