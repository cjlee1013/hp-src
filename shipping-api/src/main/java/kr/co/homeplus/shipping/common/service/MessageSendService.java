package kr.co.homeplus.shipping.common.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.shipping.common.mapper.MessageMasterMapper;
import kr.co.homeplus.shipping.common.model.history.BundleProcessInsertSetDto;
import kr.co.homeplus.shipping.common.model.message.MessageSendBodyArgument;
import kr.co.homeplus.shipping.common.model.message.MessageSendParam;
import kr.co.homeplus.shipping.common.model.message.info.MessageInfoBsdBuyerGetDto;
import kr.co.homeplus.shipping.common.model.message.info.MessageInfoBsdGetDto;
import kr.co.homeplus.shipping.common.model.message.info.MessageInfoConfirmReceiveGetDto;
import kr.co.homeplus.shipping.common.model.message.info.MessageInfoShipAllGetDto;
import kr.co.homeplus.shipping.common.model.message.info.MessageInfoShipDelayGetDto;
import kr.co.homeplus.shipping.common.model.message.info.MessageInfoTdGetDto;
import kr.co.homeplus.shipping.constants.MessageTemplateCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageSendService {

    private final MessageService messageService;
    private final MessageMasterMapper messageMasterMapper;
    private final HistoryService historyService;

    /**
     * 택배배송 알림톡 (1건씩)
     */
    public void sendMessageShipDlv(String bundleNo) {
        List<String> bundleNoList = Arrays.asList(bundleNo);
        this.sendMessageShipDlv(bundleNoList);
    }

    /**
     * 택배배송 알림톡 (N건)
     */
    public void sendMessageShipDlv(List<String> bundleNoList) {
        try {
            // 메세지 정보
            MessageSendParam messageSendParam =
                new MessageSendParam("배송_택배배송안내","배송_택배배송안내","SYSTEM", MessageTemplateCode.TALK_SHIP_DLV, MessageTemplateCode.SMS_SHIP_DLV);

            // 개인별 메세지 정보 리스트 생성
            List<MessageSendBodyArgument> messageSendBodyArgumentList = new ArrayList<>();
            for (String bundleNo : bundleNoList) {
                // 배송 정보 조회
                MessageInfoShipAllGetDto messageInfoShipAllGetDto = messageMasterMapper.selectShipAllMessageInfo(bundleNo);

                if (Objects.isNull(messageInfoShipAllGetDto)) {
                   continue;
                }

                // 매핑 데이터 설정
                Map<String, String> mappingData = new HashMap<>();
                mappingData.put("주문번호", messageInfoShipAllGetDto.getPurchaseOrderNo());
                mappingData.put("상품명", messageInfoShipAllGetDto.getItemNm());
                mappingData.put("택배명", messageInfoShipAllGetDto.getDlvCdNm());
                mappingData.put("송장번호", messageInfoShipAllGetDto.getInvoiceNo());

                // 개인별 메세지 정보 설정
                MessageSendBodyArgument messageSendBodyArgument = new MessageSendBodyArgument(
                    messageInfoShipAllGetDto.getPurchaseOrderNo(), messageInfoShipAllGetDto.getMobileNo(), mappingData);

                // 개인별 메세지 정보 리스트에 추가
                messageSendBodyArgumentList.add(messageSendBodyArgument);
            }

            // 메세지 정보에 개인별 정보 설정
            messageSendParam.setBodyArgument(messageSendBodyArgumentList);

            long talkWorkSeq = messageService.sendTalk(messageSendParam);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    /**
     * DS직접배송 알림톡 (1건씩)
     */
    public void sendMessageShipDsDrct(String bundleNo) {
        List<String> bundleNoList = Arrays.asList(bundleNo);
        this.sendMessageShipDsDrct(bundleNoList);
    }

    /**
     * DS직접배송 알림톡 (N건)
     */
    public void sendMessageShipDsDrct(List<String> bundleNoList) {
        try {
            // 메세지 정보
            MessageSendParam messageSendParam =
                new MessageSendParam("배송_DS직접배송안내","배송_DS직접배송안내","SYSTEM", MessageTemplateCode.TALK_SHIP_DS_DRCT, MessageTemplateCode.SMS_SHIP_DS_DRCT);

            // 개인별 메세지 정보 리스트 생성
            List<MessageSendBodyArgument> messageSendBodyArgumentList = new ArrayList<>();

            for (String bundleNo : bundleNoList) {
                // 배송 정보 조회
                MessageInfoShipAllGetDto messageInfoShipAllGetDto = messageMasterMapper.selectShipAllMessageInfo(bundleNo);

                if (Objects.isNull(messageInfoShipAllGetDto)) {
                    continue;
                }

                // 매핑 데이터 설정
                Map<String, String> mappingData = new HashMap<>();
                mappingData.put("주문번호", messageInfoShipAllGetDto.getPurchaseOrderNo());
                mappingData.put("상품명", messageInfoShipAllGetDto.getItemNm());
                mappingData.put("배송예정일", messageInfoShipAllGetDto.getScheduleShipDt());

                // 개인별 메세지 정보 설정
                MessageSendBodyArgument messageSendBodyArgument = new MessageSendBodyArgument(
                    messageInfoShipAllGetDto.getPurchaseOrderNo(), messageInfoShipAllGetDto.getMobileNo(), mappingData);

                // 개인별 메세지 정보 리스트에 추가
                messageSendBodyArgumentList.add(messageSendBodyArgument);
            }

            // 메세지 정보에 개인별 정보 설정
            messageSendParam.setBodyArgument(messageSendBodyArgumentList);

            long talkWorkSeq = messageService.sendTalk(messageSendParam);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    /**
     * DS우편배송 알림톡 (1건씩)
     */
    public void sendMessageShipDsPost(String bundleNo) {
        List<String> bundleNoList = Arrays.asList(bundleNo);
        this.sendMessageShipDsPost(bundleNoList);
    }

    /**
     * DS우편배송 알림톡 (N건)
     */
    public void sendMessageShipDsPost(List<String> bundleNoList) {
        try {
            // 메세지 정보
            MessageSendParam messageSendParam =
                new MessageSendParam("배송_DS우편배송안내","배송_DS우편배송안내","SYSTEM", MessageTemplateCode.TALK_SHIP_DS_POST, MessageTemplateCode.SMS_SHIP_DS_POST);

            // 개인별 메세지 정보 리스트 생성
            List<MessageSendBodyArgument> messageSendBodyArgumentList = new ArrayList<>();

            for (String bundleNo : bundleNoList) {
                // 배송 정보 조회
                MessageInfoShipAllGetDto messageInfoShipAllGetDto = messageMasterMapper.selectShipAllMessageInfo(bundleNo);

                if (Objects.isNull(messageInfoShipAllGetDto)) {
                    continue;
                }

                // 매핑 데이터 설정
                Map<String, String> mappingData = new HashMap<>();
                mappingData.put("주문번호", messageInfoShipAllGetDto.getPurchaseOrderNo());
                mappingData.put("상품명", messageInfoShipAllGetDto.getItemNm());

                // 개인별 메세지 정보 설정
                MessageSendBodyArgument messageSendBodyArgument = new MessageSendBodyArgument(
                    messageInfoShipAllGetDto.getPurchaseOrderNo(), messageInfoShipAllGetDto.getMobileNo(), mappingData);

                // 개인별 메세지 정보 리스트에 추가
                messageSendBodyArgumentList.add(messageSendBodyArgument);
            }

            // 메세지 정보에 개인별 정보 설정
            messageSendParam.setBodyArgument(messageSendBodyArgumentList);

            long talkWorkSeq = messageService.sendTalk(messageSendParam);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }


    /**
     * BSD 단일배송 알림톡 (N건)
     */
    public void sendMessageShipBSD(String bundleNo) {
        try {
            // 메세지 정보
            MessageSendParam messageSendParam =
                new MessageSendParam("배송_추석선물세트선물메세지","배송_추석선물세트선물메세지","SYSTEM", MessageTemplateCode.TALK_SHIP_BSD, MessageTemplateCode.SMS_SHIP_BSD);

            // 개인별 메세지 정보 리스트 생성
            List<MessageSendBodyArgument> messageSendBodyArgumentList = new ArrayList<>();

            // 메세지 정보 (선물메세지 없음)
            MessageSendParam messageSendWoMsgParam =
                    new MessageSendParam("명절선물(선물메세지 미입력)","명절선물(선물메세지 미입력)","SYSTEM", MessageTemplateCode.TALK_SHIP_BSD_WO_MSG, MessageTemplateCode.SMS_SHIP_BSD_WO_MSG);

            // 개인별 메세지 정보 리스트 생성 (선물메세지 없음)
            List<MessageSendBodyArgument> messageSendBodyArgumentWoMsgList = new ArrayList<>();

            // 배송 정보 조회
            MessageInfoBsdGetDto messageInfoBsdGetDto = messageMasterMapper.selectBsdShipMessageInfo(bundleNo);

            if (messageInfoBsdGetDto == null) {
                return;
            }

            // 매핑 데이터 설정
            Map<String, String> mappingData = new HashMap<>();
            mappingData.put("수령인", messageInfoBsdGetDto.getReceiverNm());
            mappingData.put("구매자마스킹", PrivacyMaskingUtils.maskingUserName(messageInfoBsdGetDto.getBuyerNm()));
            String giftMsg = messageInfoBsdGetDto.getGiftMsg();
            if (giftMsg == null || "".equals(giftMsg)) {
                // 개인별 메세지 정보 설정
                MessageSendBodyArgument messageSendBodyArgument = new MessageSendBodyArgument(
                        messageInfoBsdGetDto.getPurchaseOrderNo(), messageInfoBsdGetDto.getMobileNo(), mappingData);

                // 개인별 메세지 정보 리스트에 추가
                messageSendBodyArgumentWoMsgList.add(messageSendBodyArgument);
            } else {
                mappingData.put("주문시 작성한 선물 메시지 노출", giftMsg);

                // 개인별 메세지 정보 설정
                MessageSendBodyArgument messageSendBodyArgument = new MessageSendBodyArgument(
                        messageInfoBsdGetDto.getPurchaseOrderNo(), messageInfoBsdGetDto.getMobileNo(), mappingData);

                // 개인별 메세지 정보 리스트에 추가
                messageSendBodyArgumentList.add(messageSendBodyArgument);
            }
            long talkWorkMsgSeq=0;
            if (messageSendBodyArgumentList.size() > 0) {
                // 메세지 정보에 개인별 정보 설정
                messageSendParam.setBodyArgument(messageSendBodyArgumentList);

                talkWorkMsgSeq = messageService.sendTalk(messageSendParam);
            }
            long talkWorkWoMsgSeq=0;
            if (messageSendBodyArgumentWoMsgList.size() > 0) {
                // 메세지 정보에 개인별 정보 설정
                messageSendWoMsgParam.setBodyArgument(messageSendBodyArgumentWoMsgList);

                talkWorkWoMsgSeq = messageService.sendTalk(messageSendWoMsgParam);
            }

            historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("단일배송 문자발송 성공" ,"코드:"+talkWorkMsgSeq+"/"+talkWorkWoMsgSeq,"","batch","shipping",bundleNo));
        } catch (Exception e) {
            log.error(e.getMessage());
            historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("단일배송 문자발송 실패" ,"오류메시지:"+e.getMessage(),"","batch","shipping",bundleNo));
        }
    }

    /**
     * BSD 다중배송 알림톡 (N건)
     */
    public void sendMessageMultiShipBSD(String bundleNo) {
        try {
            // 메세지 정보
            MessageSendParam messageSendParam =
                new MessageSendParam("배송_추석선물세트선물메세지","배송_추석선물세트선물메세지","SYSTEM", MessageTemplateCode.TALK_SHIP_BSD, MessageTemplateCode.SMS_SHIP_BSD);

            // 개인별 메세지 정보 리스트 생성
            List<MessageSendBodyArgument> messageSendBodyArgumentList = new ArrayList<>();

            // 메세지 정보 (선물메세지 없음)
            MessageSendParam messageSendWoMsgParam =
                    new MessageSendParam("명절선물(선물메세지 미입력)","명절선물(선물메세지 미입력)","SYSTEM", MessageTemplateCode.TALK_SHIP_BSD_WO_MSG, MessageTemplateCode.SMS_SHIP_BSD_WO_MSG);

            // 개인별 메세지 정보 리스트 생성 (선물메세지 없음)
            List<MessageSendBodyArgument> messageSendBodyArgumentWoMsgList = new ArrayList<>();
            // 배송 정보 조회
            MessageInfoBsdGetDto messageInfoBsdGetDto = messageMasterMapper.selectBsdMultiShipMessageInfo(bundleNo);

            if (messageInfoBsdGetDto == null) {
                return;
            }

            // 매핑 데이터 설정
            Map<String, String> mappingData = new HashMap<>();
            mappingData.put("수령인", messageInfoBsdGetDto.getReceiverNm());
            mappingData.put("구매자마스킹", PrivacyMaskingUtils.maskingUserName(messageInfoBsdGetDto.getBuyerNm()));
            String giftMsg = messageInfoBsdGetDto.getGiftMsg();
            if (giftMsg == null || "".equals(giftMsg)) {
                // 개인별 메세지 정보 설정
                MessageSendBodyArgument messageSendBodyArgument = new MessageSendBodyArgument(
                        messageInfoBsdGetDto.getPurchaseOrderNo(), messageInfoBsdGetDto.getMobileNo(), mappingData);

                // 개인별 메세지 정보 리스트에 추가
                messageSendBodyArgumentWoMsgList.add(messageSendBodyArgument);
            } else {
                mappingData.put("주문시 작성한 선물 메시지 노출", giftMsg);

                // 개인별 메세지 정보 설정
                MessageSendBodyArgument messageSendBodyArgument = new MessageSendBodyArgument(
                        messageInfoBsdGetDto.getPurchaseOrderNo(), messageInfoBsdGetDto.getMobileNo(), mappingData);

                // 개인별 메세지 정보 리스트에 추가
                messageSendBodyArgumentList.add(messageSendBodyArgument);
            }
            long talkWorkMsgSeq=0;
            long talkWorkWoMsgSeq=0;
            if (messageSendBodyArgumentList.size() > 0) {
                // 메세지 정보에 개인별 정보 설정
                messageSendParam.setBodyArgument(messageSendBodyArgumentList);

                talkWorkMsgSeq = messageService.sendTalk(messageSendParam);
            }
            if (messageSendBodyArgumentWoMsgList.size() > 0) {
                // 메세지 정보에 개인별 정보 설정
                messageSendWoMsgParam.setBodyArgument(messageSendBodyArgumentWoMsgList);

                talkWorkWoMsgSeq = messageService.sendTalk(messageSendWoMsgParam);
            }
            historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("다중배송 문자발송 성공" ,"코드:"+talkWorkMsgSeq+"/"+talkWorkWoMsgSeq,"","batch","shipping",bundleNo));
        } catch (Exception e) {
            log.error(e.getMessage());
            historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("다중배송 문자발송 실패" ,"오류메시지:"+e.getMessage(),"","batch","shipping",bundleNo));
        }
    }


    /**
     * 선물세트 주문자 알림톡 (N건)
     */
    public void sendMessageBuyerBsd(String bundleNo) {
        try {
            // 메세지 정보
            MessageSendParam messageSendParam =
                new MessageSendParam("선물세트_주문자","선물세트_주문자","SYSTEM", MessageTemplateCode.TALK_BUYER_BSD, MessageTemplateCode.SMS_BUYER_BSD);

            // 개인별 메세지 정보 리스트 생성
            List<MessageSendBodyArgument> messageSendBodyArgumentList = new ArrayList<>();
            // 배송 정보 조회
            MessageInfoBsdBuyerGetDto messageInfoBsdBuyerGetDto = messageMasterMapper.selectBsdBuyerMessageInfo(bundleNo);

            if (Objects.isNull(messageInfoBsdBuyerGetDto)) {
                return;
            }

            // 매핑 데이터 설정
            Map<String, String> mappingData = new HashMap<>();
            mappingData.put("주문번호", messageInfoBsdBuyerGetDto.getPurchaseOrderNo());
            mappingData.put("대표상품 외 n건", messageInfoBsdBuyerGetDto.getItemNm());

            // 개인별 메세지 정보 설정
            MessageSendBodyArgument messageSendBodyArgument = new MessageSendBodyArgument(
                messageInfoBsdBuyerGetDto.getPurchaseOrderNo(), messageInfoBsdBuyerGetDto.getMobileNo(), mappingData);

            // 개인별 메세지 정보 리스트에 추가
            messageSendBodyArgumentList.add(messageSendBodyArgument);

            // 메세지 정보에 개인별 정보 설정
            messageSendParam.setBodyArgument(messageSendBodyArgumentList);

            long talkWorkSeq = messageService.sendTalk(messageSendParam);
            historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("주문자 문자발송 성공" ,"코드:"+talkWorkSeq,"","batch","shipping",bundleNo));
        } catch (Exception e) {
            log.error(e.getMessage());
            historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("주문자 문자발송 실패" ,"오류메시지:"+e.getMessage(),"","batch","shipping",bundleNo));
        }
    }

    /**
     * 수취확인 요청 알림톡
     */
    public long sendMessageComfirmReceive(List<String> bundleNoList) {
        try {
            // 메세지 정보
            MessageSendParam messageSendParam =
                new MessageSendParam("배송_수취확인 요청_DS","배송_수취확인 요청_DS","SYSTEM", MessageTemplateCode.TALK_SHIP_CONFIRM_RECEIVE, MessageTemplateCode.SMS_SHIP_CONFIRM_RECEIVE);

            // 개인별 메세지 정보 리스트 생성
            List<MessageSendBodyArgument> messageSendBodyArgumentList = new ArrayList<>();
            for (String bundleNo : bundleNoList) {
                // 배송 정보 조회
                MessageInfoConfirmReceiveGetDto messageInfo = messageMasterMapper.selectConfirmReceiveMessageInfo(bundleNo);

                if (Objects.isNull(messageInfo)) {
                    continue;
                }

                // 매핑 데이터 설정
                Map<String, String> mappingData = new HashMap<>();
                mappingData.put("주문번호", messageInfo.getPurchaseOrderNo());
                mappingData.put("상품명", messageInfo.getItemNm());

                // 개인별 메세지 정보 설정
                MessageSendBodyArgument messageSendBodyArgument = new MessageSendBodyArgument(
                    messageInfo.getPurchaseOrderNo(), messageInfo.getMobileNo(), mappingData);

                // 개인별 메세지 정보 리스트에 추가
                messageSendBodyArgumentList.add(messageSendBodyArgument);
            }

            // 메세지 정보에 개인별 정보 설정
            messageSendParam.setBodyArgument(messageSendBodyArgumentList);

            long talkWorkSeq = messageService.sendTalk(messageSendParam);

            return talkWorkSeq;
        } catch (Exception e) {
            log.error(e.getMessage());
            return -1;
        }
    }

    /**
     * 발송지연안내 알림톡
     */
    public void sendMessageShipDelay(String bundleNo) {
        try {
            // 메세지 정보
            MessageSendParam messageSendParam =
                new MessageSendParam("배송_ DS배송 지연안내","배송_ DS배송 지연안내","SYSTEM", MessageTemplateCode.TALK_SHIP_DELAY, MessageTemplateCode.SMS_SHIP_DELAY);

            // 개인별 메세지 정보 리스트 생성
            List<MessageSendBodyArgument> messageSendBodyArgumentList = new ArrayList<>();

            // 배송 정보 조회
            MessageInfoShipDelayGetDto messageInfo = messageMasterMapper.selectShipDelayMessageInfo(bundleNo);

            if (Objects.isNull(messageInfo)) {
                return;
            }

            // 매핑 데이터 설정
            Map<String, String> mappingData = new HashMap<>();
            mappingData.put("주문번호", messageInfo.getPurchaseOrderNo());
            mappingData.put("대표상품 외 n건", messageInfo.getItemNm());
            mappingData.put("입력사유노출", messageInfo.getDelayShipCd());
            mappingData.put("2차출고기한", messageInfo.getDelayShipDt());

            // 개인별 메세지 정보 설정
            MessageSendBodyArgument messageSendBodyArgument = new MessageSendBodyArgument(
                messageInfo.getPurchaseOrderNo(), messageInfo.getMobileNo(), mappingData);

            // 개인별 메세지 정보 리스트에 추가
            messageSendBodyArgumentList.add(messageSendBodyArgument);

            // 메세지 정보에 개인별 정보 설정
            messageSendParam.setBodyArgument(messageSendBodyArgumentList);

            long talkWorkSeq = messageService.sendTalk(messageSendParam);
        } catch (Exception e) {
            log.error(e.getMessage());
            return;
        }
    }


    /**
     * TD 자차배송 선물메세지 알림톡 (N건)
     */
    public void sendMessageTDDRCT(String bundleNo) {
        try {
            // 메세지 정보
            MessageSendParam messageSendParam =
                new MessageSendParam("자차_선물메세지","자차_선물메세지","SYSTEM", MessageTemplateCode.TALK_SHIP_TD_DRCT, MessageTemplateCode.SMS_SHIP_TD_DRCT);

            // 개인별 메세지 정보 리스트 생성
            List<MessageSendBodyArgument> messageSendBodyArgumentList = new ArrayList<>();
            // 배송 정보 조회
            MessageInfoTdGetDto messageInfoTdGetDto = messageMasterMapper.selectShipTDDRCTMessageInfo(bundleNo);

            if (Objects.isNull(messageInfoTdGetDto)) {
                return;
            }

            // 매핑 데이터 설정
            Map<String, String> mappingData = new HashMap<>();
            mappingData.put("수령인", messageInfoTdGetDto.getReceiverNm());
            mappingData.put("구매자마스킹", PrivacyMaskingUtils.maskingUserName(messageInfoTdGetDto.getBuyerNm()));
            mappingData.put("주문시 작성한 선물 메시지", messageInfoTdGetDto.getGiftMsg());

            // 개인별 메세지 정보 설정
            MessageSendBodyArgument messageSendBodyArgument = new MessageSendBodyArgument(
                messageInfoTdGetDto.getPurchaseOrderNo(), messageInfoTdGetDto.getMobileNo(), mappingData);

            // 개인별 메세지 정보 리스트에 추가
            messageSendBodyArgumentList.add(messageSendBodyArgument);
            // 메세지 정보에 개인별 정보 설정
            messageSendParam.setBodyArgument(messageSendBodyArgumentList);

            long talkWorkSeq = messageService.sendTalk(messageSendParam);
            historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("자차 선물메세지 발송 성공" ,"코드:"+talkWorkSeq,"","batch","shipping",bundleNo));
        } catch (Exception e) {
            log.error(e.getMessage());
            historyService.setShippingProcessHistoryBundle(new BundleProcessInsertSetDto("자차 선물메세지 발송 실패" ,"오류메시지:"+e.getMessage(),"","batch","shipping",bundleNo));
        }
    }
}
