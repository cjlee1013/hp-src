package kr.co.homeplus.shipping.admin.mapper;

import java.util.List;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipReserveManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipReserveManageListSetDto;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;

@SlaveConnection
public interface ShipReserveManageSlaveMapper {
  List<ShipReserveManageListGetDto> selectShipReserveManageList(
      ShipReserveManageListSetDto shipReserveManageListSetDto);
}
