package kr.co.homeplus.shipping.partner.service.impl;

import static kr.co.homeplus.shipping.constants.ResourceRouteName.PARTNER;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import kr.co.homeplus.shipping.common.model.history.ShippingProcessInsertSetDto;
import kr.co.homeplus.shipping.common.service.HistoryService;
import kr.co.homeplus.shipping.core.exception.ShippingException;
import kr.co.homeplus.shipping.enums.DateType;
import kr.co.homeplus.shipping.partner.mapper.RentalManageMasterMapper;
import kr.co.homeplus.shipping.partner.mapper.RentalManageSlaveMapper;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageCounselCompleteSetDto;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageCounselReservationSetDto;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageListSetDto;
import kr.co.homeplus.shipping.partner.service.PartnerCommonService;
import kr.co.homeplus.shipping.partner.service.RentalManageService;
import kr.co.homeplus.shipping.utils.DateUtil;
import kr.co.homeplus.shipping.utils.ResponseUtil;
import kr.co.homeplus.shipping.utils.ShipInfoUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class RentalManageServiceImpl implements RentalManageService {
    private final RentalManageSlaveMapper rentalManageSlaveMapper;
    private final RentalManageMasterMapper rentalManageMasterMapper;
    private final HistoryService historyService;
    private final PartnerCommonService partnerCommonService;

    /**
     * 렌탈판매관리 조회
     */
    @Override
    public List<RentalManageListGetDto> getRentalManageList(RentalManageListSetDto rentalManageListSetDto) {
      log.info("getRentalManageList param : {}", rentalManageListSetDto);

      List<RentalManageListGetDto> rentalManageListGetDtoList = rentalManageSlaveMapper.selectRentalManageList(rentalManageListSetDto);

      return this.getMaskingRentalManageList(rentalManageListGetDtoList);
    }

    /**
     * 렌탈판매관리 카운트 리스트 조회
     */
    @Override
    public List<RentalManageListGetDto> getRentalManageCntList(String schType, String partnerId) {
      log.info("getRentalManageCntList param : {}", partnerId);

      List<RentalManageListGetDto> rentalManageListGetDtoList = rentalManageSlaveMapper.selectRentalManageCntList(schType, partnerId);

      return this.getMaskingRentalManageList(rentalManageListGetDtoList);
    }

    /**
     * 렌탈판매관리 리스트 마스킹
     */
    private List<RentalManageListGetDto> getMaskingRentalManageList(List<RentalManageListGetDto> rentalManageList) {
      for (RentalManageListGetDto dto : rentalManageList) {
        // 배송완료 7일이 지나면 마스킹
        if (DateUtil.isAfter(dto.getCompleteDt(), DateType.DAY, 8)) {
          dto.setCompleteAfter3MonthYn("Y");
          dto.setBuyerNm(PrivacyMaskingUtils.maskingUserName(dto.getBuyerNm()));
          dto.setBuyerMobileNo(PrivacyMaskingUtils.maskingPhone(dto.getBuyerMobileNo()));
          dto.setReceiverNm(PrivacyMaskingUtils.maskingUserName(dto.getReceiverNm()));
          dto.setShipMobileNo(PrivacyMaskingUtils.maskingPhone(dto.getShipMobileNo()));
          dto.setAddr(PrivacyMaskingUtils.maskingAddress(dto.getAddr()));
          dto.setPersonalOverseaNo(PrivacyMaskingUtils.maskingCustomsIdNumber(dto.getPersonalOverseaNo()));
        }
      }

      return rentalManageList;
    }

    /**
     * 렌탈판매관리 카운트 조회
     */
    @Override
    public RentalManageCntGetDto getRentalManageCnt(String partnerId) {
      log.info("getRentalManageCnt param : {}", partnerId);

      RentalManageCntGetDto rentalManageCntGetDto = new RentalManageCntGetDto();

      rentalManageCntGetDto.setCallCnt(rentalManageSlaveMapper.selectRentalManageCnt("callCnt", partnerId));
      rentalManageCntGetDto.setReservationCnt(rentalManageSlaveMapper.selectRentalManageCnt("reservationCnt", partnerId));
      rentalManageCntGetDto.setCompleteCnt(rentalManageSlaveMapper.selectRentalManageCnt("completeCnt", partnerId));

      return rentalManageCntGetDto;
    }

    /**
     * 상담예약
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, ShippingException.class})
    public ResponseObject<String> setCounselReservation(RentalManageCounselReservationSetDto rentalManageCounselReservationSetDto) {
        log.info("setCounselReservation param : {}", rentalManageCounselReservationSetDto);

        int result = 0;
        String chgId = rentalManageCounselReservationSetDto.getChgId();
        List<String> shipNoList = rentalManageCounselReservationSetDto.getShipNoList();

        // 주문확인 처리
        for (String shipNo : shipNoList) {
          // 파트너 물품인지 체크
          if(!partnerCommonService.isPartnerItemByShipNo(shipNo, chgId)) {
            historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("상담예약 실패", "파트너ID가 존재하지 않거나 본 계정의 물품이 아닙니다","", PARTNER, chgId, shipNo));
            continue;
          }

          int returnVal = rentalManageMasterMapper.updateCounselReservation(shipNo, chgId);

          if (returnVal == 1) {
            historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("상담예약", "", "", PARTNER, chgId, shipNo));
            result++;
          } else {
            historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("상담예약 실패", "", "", PARTNER, chgId, shipNo));
          }
        }

        return ResponseUtil.getResoponseObject(Integer.toString(result));
    }

    /**
     * 상담완료
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, ShippingException.class})
    public ResponseObject<String> setCounselComplete(RentalManageCounselCompleteSetDto rentalManageCounselCompleteSetDto) {
      log.info("setCounselComplete param : {}", rentalManageCounselCompleteSetDto);

      int result = 0;
      String chgId = rentalManageCounselCompleteSetDto.getChgId();
      List<String> shipNoList = rentalManageCounselCompleteSetDto.getShipNoList();

      // 상담완료 처리
      for (String shipNo : shipNoList) {
        // 파트너 물품인지 체크
        if(!partnerCommonService.isPartnerItemByShipNo(shipNo, chgId)) {
          historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("상담완료 실패", "파트너ID가 존재하지 않거나 본 계정의 물품이 아닙니다","", PARTNER, chgId, shipNo));
          continue;
        }

        int returnVal = rentalManageMasterMapper.updateCounselComplete(shipNo, chgId);

        if (returnVal == 1) {
          historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("상담완료", "", "", PARTNER, chgId, shipNo));
          result++;
        } else {
          historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("상담완료 실패", "", "", PARTNER, chgId, shipNo));
        }
      }

      return ResponseUtil.getResoponseObject(Integer.toString(result));
    }
}
