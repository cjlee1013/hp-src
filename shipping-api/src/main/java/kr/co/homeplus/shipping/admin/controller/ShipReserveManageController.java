package kr.co.homeplus.shipping.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipReserveManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipReserveManageListSetDto;
import kr.co.homeplus.shipping.admin.service.ShipReserveManageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/shipManage")
@Api(tags = "어드민 - 예약배송관리")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class ShipReserveManageController {
  private final ShipReserveManageService shipReserveManageService;

  @ApiOperation(value = "예약배송관리 조회")
  @PostMapping("/getShipReserveManageList")
  public ResponseObject<List<ShipReserveManageListGetDto>> getShipReserveManageList(@RequestBody @Valid ShipReserveManageListSetDto shipReserveManageListSetDto) {
    return ResourceConverter.toResponseObject(shipReserveManageService.getShipReserveManageList(shipReserveManageListSetDto));
  }
}