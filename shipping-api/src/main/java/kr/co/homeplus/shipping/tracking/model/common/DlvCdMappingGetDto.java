package kr.co.homeplus.shipping.tracking.model.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "택배사 매핑 코드")
public class DlvCdMappingGetDto {
    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    @ApiModelProperty(notes = "연동업체코드")
    private String dlvLinkCompanyCd;

    @ApiModelProperty(notes = "연동업체")
    private String dlvLinkCompany;

    @ApiModelProperty(notes = "자동반품접수")
    private String autoReturnYn;

    @ApiModelProperty(notes = "자동반품지연")
    private String autoReturnDelayYn;

    @ApiModelProperty(notes = "노출여부")
    private String displayYn;

    public String getDlvCd() {
        return dlvCd;
    }

    public String getDlvLinkCompanyCd() {
        return dlvLinkCompanyCd;
    }

    public String getDlvLinkCompany() {
        return dlvLinkCompany;
    }

    public String getAutoReturnYn() {
        return autoReturnYn;
    }

    public String getAutoReturnDelayYn() {
        return autoReturnDelayYn;
    }

    public String getDisplayYn() { return displayYn; }
}
