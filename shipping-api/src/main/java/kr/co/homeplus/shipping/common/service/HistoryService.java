package kr.co.homeplus.shipping.common.service;

import kr.co.homeplus.shipping.common.model.history.*;

public interface HistoryService {
    int setShippingHistoryInvoice(ShippingHistoryInsertSetDto shippingHistoryInsertSetDto);
    int setPickHistoryInvoice(PickHistoryInsertSetDto pickHistoryInsertSetDto);
    int setExchHistoryInvoice(ExchHistoryInsertSetDto exchHistoryInsertSetDto);

    int setShippingProcessHistory(ShippingProcessInsertSetDto historyInsertSetDto);
    int setShippingProcessHistoryBundle(BundleProcessInsertSetDto historyInsertSetDto);
    int setPickProcessHistory(PickProcessInsertSetDto historyInsertSetDto);
    int setExchProcessHistory(ExchProcessInsertSetDto historyInsertSetDto);

    int setMultiShippingHistoryInvoice(MultiShippingHistoryInsertSetDto historyInsertSetDto);
    int setMultiShippingProcessHistory(MultiShippingProcessInsertSetDto historyInsertSetDto);

    int setCommonHistoryInvoice(CommonHistoryInsertSetDto historyInsertSetDto);
}
