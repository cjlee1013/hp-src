package kr.co.homeplus.shipping.tracking.model.bsd;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "BSD 추적 결과")
public class BsdRequestGetDto {
    @ApiModelProperty(notes = "BSD일련번호")
    private String bsdLinkNo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "성공여부")
    private String success;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "결과값")
    private String detail;

    public BsdRequestGetDto(String bsdLinkNo, String success, String detail) {
        this.bsdLinkNo = bsdLinkNo;
        this.success = success;
        this.detail = detail;
    }

    public void appendDetail(String detail) { this.detail = this.detail.concat(" " + detail); }
}
