package kr.co.homeplus.shipping.tracking.mapper;

import kr.co.homeplus.shipping.core.db.annotation.MasterConnection;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdResultSetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchResultSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiResultSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickResultSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingResultSetDto;

@MasterConnection
public interface TrackingMasterMapper {
  int setShippingTrackingResult(ShippingResultSetDto shippingRequestGetDto);
  int setShippingComplete(ShippingCallbackSetDto shippingCallbackSetDto);
  int setPickTrackingResult(PickResultSetDto pickRequestGetDto);
  int setPickComplete(PickCallbackSetDto pickCallbackSetDto);
  int setExchTrackingResult(ExchResultSetDto exchRequestGetDto);
  int setExchComplete(ExchCallbackSetDto exchCallbackSetDto);
  int setPickRequestSuccess(String claimPickShippingNo);
  int setPickRequestFail(String claimPickShippingNo);
  int setPickRequestCancel(PickResultSetDto pickRequestGetDto);
  int setPickRequestCallback(PickCallbackSetDto pickCallbackSetDto);
  int setMultiTrackingResult(MultiResultSetDto shippingRequestGetDto);
  int setMultiComplete(MultiCallbackSetDto shippingCallbackSetDto);
  int setBsdTrackingResult(BsdResultSetDto bsdResultSetDto);
}