package kr.co.homeplus.shipping.tracking.model.common;

public interface CommonTrackingGetDto {

    String getNo();
    String getInvoiceNo();
    String getDlvCd();
}
