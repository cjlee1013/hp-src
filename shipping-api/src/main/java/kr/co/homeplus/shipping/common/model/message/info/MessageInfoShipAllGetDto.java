package kr.co.homeplus.shipping.common.model.message.info;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "메세지 발송 정보 - 일괄 발송")
public class MessageInfoShipAllGetDto extends MessageInfoBaseDto{
    @ApiModelProperty(notes = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "상품명")
    private String itemNm;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "송장번호")
    private String scheduleShipDt;

    @ApiModelProperty(notes = "택배사")
    private String dlvCdNm;
}
