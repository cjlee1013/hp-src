package kr.co.homeplus.shipping.partner.mapper;

import java.util.List;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shipping.partner.model.sell.OrderDetailBaseDto;
import kr.co.homeplus.shipping.partner.model.sell.OrderDetailItemDto;
import kr.co.homeplus.shipping.partner.model.sell.OrderDetailShipDto;
import kr.co.homeplus.shipping.partner.model.sell.orderShipManage.OrderShipManageCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.orderShipManage.OrderShipManageListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.orderShipManage.OrderShipManageListSetDto;
import org.apache.ibatis.annotations.Param;

@SlaveConnection
public interface OrderShipManageSlaveMapper {
  List<OrderShipManageListGetDto> selectOrderShipManageList(OrderShipManageListSetDto orderShipManageListSetDto);
  List<OrderShipManageListGetDto> selectOrderShipManageCntList(@Param("schType") String schType, @Param("partnerId") String partnerId);
  String selectOrderShipManageCnt(@Param("schType") String schType, @Param("partnerId") String partnerId);
  OrderDetailBaseDto selectOrderDetailBase(@Param("bundleNo") String bundleNo, @Param("partnerId") String partnerId);
  List<OrderDetailItemDto> selectOrderDetailItem(@Param("bundleNo") String bundleNo, @Param("partnerId") String partnerId);
  OrderDetailShipDto selectOrderDetailShip(@Param("bundleNo") String bundleNo, @Param("partnerId") String partnerId);
  String selectOrderShipManageMainCnt(@Param("schType") String schType, @Param("partnerId") String partnerId);
}
