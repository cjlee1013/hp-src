package kr.co.homeplus.shipping.admin.model.monitoring;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송모니터링 응답")
public class MonitoringShipGetDto {
    @ApiModelProperty(notes = "판매자명")
    private String partnerNm;

    @ApiModelProperty(notes = "주문번호")
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;

    @ApiModelProperty(notes = "주문일시")
    private String orderDt;

    @ApiModelProperty(notes = "배송상태")
    private String shipStatusNm;

    @ApiModelProperty(notes = "배송상태코드")
    private String shipStatus;

    @ApiModelProperty(notes = "마켓유형")
    private String marketType;

    @ApiModelProperty(notes = "마켓주문번호")
    private String marketOrderNo;

    @ApiModelProperty(notes = "발송처리일시")
    private String shippingDt;

    @ApiModelProperty(notes = "배송완료일시")
    private String completeDt;

    @ApiModelProperty(notes = "미수취신고일")
    private String noRcvDeclrDt;

    @ApiModelProperty(notes = "미수취신고처리")
    private String noRcvProcessNm;

    @ApiModelProperty(notes = "미수취신고사유")
    private String noRcvDetailReason;

    @ApiModelProperty(notes = "배송방법")
    private String shipMethodNm;

    @ApiModelProperty(notes = "배송방법코드")
    private String shipMethod;

    @ApiModelProperty(notes = "택배사")
    private String dlvCdNm;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "송장조회상태")
    private String invoiceStatus;

    @ApiModelProperty(notes = "배송요청일")
    private String reserveShipDt;

    @ApiModelProperty(notes = "배송예정일")
    private String scheduleShipDt;

    @ApiModelProperty(notes = "발송기한")
    private String orgShipDt;

    @ApiModelProperty(notes = "2차발송기한")
    private String delayShipDt;

    @ApiModelProperty(notes = "상품주문번호")
    private String orderItemNo;

    @ApiModelProperty(notes = "회원번호마스킹")
    private String userNoMask;

    @ApiModelProperty(notes = "회원번호")
    private String userNo;

    @ApiModelProperty(notes = "구매자")
    private String buyerNm;

    @ApiModelProperty(notes = "수령인")
    private String receiverNm;

    @ApiModelProperty(notes = "점포유형")
    private String storeType;

    @ApiModelProperty(notes = "사이트유형")
    private String siteType;

    @ApiModelProperty(notes = "배송SEQ")
    private String shipNo;
}
