package kr.co.homeplus.shipping.external.model.store;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "옴니연동 기본 파라미터")
public class StoreShipCommonSetDto {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "구매점포번호", required = true)
    private String purchaseStoreInfoNo;
}
