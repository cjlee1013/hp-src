package kr.co.homeplus.shipping.utils;

import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class CipherUtil {
  public static String encrypt (String value, String cipherType, String cipherKey) {
    try {
      SecretKeySpec keySpec = new SecretKeySpec(cipherKey.getBytes(), cipherType);
      Cipher cipher = Cipher.getInstance(cipherType);
      cipher.init(Cipher.ENCRYPT_MODE, keySpec);
      byte[] encrypted = cipher.doFinal(value.getBytes("UTF-8"));
      return Base64.getEncoder().encodeToString(encrypted);
    } catch (Exception e) {
      return null;
    }
  }
}
