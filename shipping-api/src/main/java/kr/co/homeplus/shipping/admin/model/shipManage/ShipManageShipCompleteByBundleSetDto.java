package kr.co.homeplus.shipping.admin.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "배송완료 Bundle단위 요청 DTO")
public class ShipManageShipCompleteByBundleSetDto {
    @ApiModelProperty(notes = "번들번호")
    private String bundleNo;

    @ApiModelProperty(notes = "수정자")
    private String chgId;
}
