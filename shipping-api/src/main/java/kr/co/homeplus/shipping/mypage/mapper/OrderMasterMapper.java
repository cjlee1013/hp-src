package kr.co.homeplus.shipping.mypage.mapper;

import kr.co.homeplus.shipping.core.db.annotation.MasterConnection;

@MasterConnection
public interface OrderMasterMapper {
  int manualShipComplete(String shipNo);
  int manualPurchaseComplete(String shipNo);
  int manualMultiShipComplete(String multiBundleNo);
  int manualMultiPurchaseComplete(String multiBundleNo);
}