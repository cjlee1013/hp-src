package kr.co.homeplus.shipping.common.model.history;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "클레임 배송 히스토리 입력")
public class ExchHistoryInsertSetDto extends CommonHistoryInsertSetDto {
    @ApiModelProperty(notes = "클레임 번들번호")
    private String claimExchShippingNo;

    public ExchHistoryInsertSetDto(String dlvCd, String invoice_no, String level, String time_trans
                                    , String time_sweet, String where ,String telno_office ,String telno_man
                                    , String details, String recv_addr ,String recv_name ,String send_name
                                    , String man, String estmate ,String regId, String claimExchShippingNo) {
        super(dlvCd, invoice_no, level, time_trans, time_sweet, where, telno_office, telno_man, details, recv_addr, recv_name, send_name, man, estmate, regId);
        this.claimExchShippingNo = claimExchShippingNo;
    }
}
