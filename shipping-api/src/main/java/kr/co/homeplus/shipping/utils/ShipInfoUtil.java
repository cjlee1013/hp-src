package kr.co.homeplus.shipping.utils;

import lombok.extern.slf4j.Slf4j;

/*
 배송 관련 정보 유틸
 */
@Slf4j
public class ShipInfoUtil {
  /*
   * 택배배송 코드값 확인
   */
  public static boolean isDlv(String shipMethod) {
    String[] shipMethodArr = shipMethod.split("_");
    String method = shipMethodArr.length == 1 ? shipMethodArr[0] : shipMethodArr[1];

    return "DLV".equals(method);
  }

  /*
   * 직접배송 코드값 확인
   */
  public static boolean isDrct(String shipMethod) {
    String[] shipMethodArr = shipMethod.split("_");
    String method = shipMethodArr.length == 1 ? shipMethodArr[0] : shipMethodArr[1];

    return "DRCT".equals(method);
  }

  /*
   * 배송방법에서 mall_type 추출
   */
  public static String getMallTypeByShipMethod(String shipMethod) {
    String[] shipMethodArr = shipMethod.split("_");
    String mallType = shipMethodArr[0];

    return mallType;
  }
}
