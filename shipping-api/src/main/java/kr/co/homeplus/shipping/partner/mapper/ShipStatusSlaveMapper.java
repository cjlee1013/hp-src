package kr.co.homeplus.shipping.partner.mapper;

import java.util.List;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shipping.partner.model.sell.shipStatus.ShipStatusCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.shipStatus.ShipStatusListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.shipStatus.ShipStatusListSetDto;
import org.apache.ibatis.annotations.Param;

@SlaveConnection
public interface ShipStatusSlaveMapper {
  List<ShipStatusListGetDto> selectShipStatusList(ShipStatusListSetDto shipStatusListSetDto);
  List<ShipStatusListGetDto> selectShipStatusCntList(@Param("schType") String schType, @Param("partnerId") String partnerId);
  String selectShipStatusCnt(@Param("schType") String schType, @Param("partnerId") String partnerId);
  String selectShipStatusMainCnt(@Param("schType") String schType, @Param("partnerId") String partnerId);
}
