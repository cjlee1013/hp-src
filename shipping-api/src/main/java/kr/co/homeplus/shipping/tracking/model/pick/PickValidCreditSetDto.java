package kr.co.homeplus.shipping.tracking.model.pick;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "신용코드 확인 파라미터")
public class PickValidCreditSetDto {
    @ApiModelProperty(notes = "택배사별 신용코드")
    private String creditCd;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    public String getCreditCd() {
        return creditCd;
    }

    public String getDlvCd() {
        return dlvCd;
    }
}
