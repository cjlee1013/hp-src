package kr.co.homeplus.shipping.tracking.model.bsd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.shipping.tracking.model.common.CommonTrackingGetDto;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class BsdTrackingGetDto implements CommonTrackingGetDto {
    @ApiModelProperty(notes = "BSD일련번호")
    private String bsdLinkNo;

    @ApiModelProperty(notes = "배송번호")
    private String bsdLinkNos;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    public String[] getBsdLinkNos() { return bsdLinkNos.split(","); }

    @Override
    public String getNo() {
        return bsdLinkNo;
    }

    @Override
    public String getInvoiceNo() {
        return invoiceNo;
    }

    @Override
    public String getDlvCd() {
        return dlvCd;
    }
}
