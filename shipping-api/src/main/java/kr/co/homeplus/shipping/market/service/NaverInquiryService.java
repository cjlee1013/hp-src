package kr.co.homeplus.shipping.market.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.constants.ResourceRouteName;
import kr.co.homeplus.shipping.core.exception.ShippingException;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.market.mapper.NaverInquiryMasterMapper;
import kr.co.homeplus.shipping.market.mapper.NaverInquirySlaveMapper;
import kr.co.homeplus.shipping.market.model.NaverInquiryAnswerSetDto;
import kr.co.homeplus.shipping.market.model.MarketInquiryInfoDto;
import kr.co.homeplus.shipping.utils.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Slf4j
@Service
@RequiredArgsConstructor
public class NaverInquiryService {
  private final NaverInquirySlaveMapper naverInquirySlaveMapper;
  private final NaverInquiryMasterMapper naverInquiryMasterMapper;
  private final ResourceClient resourceClient;

  @Transactional(rollbackFor = {Exception.class, ShippingException.class})
  public ResponseObject<String> setNaverInquiryAnswer(NaverInquiryAnswerSetDto dto) {
    int returnVal = 0;

    MarketInquiryInfoDto req = naverInquirySlaveMapper.selectInquiryAnswerInfo(dto);

//    if(ObjectUtils.isEmpty(req)) {
//      return ResponseUtil.getResoponseObject(ExceptionCode.ERROR_CODE_4003);
//    }

    req.setResponseTitle(dto.getResponseTitle());
    req.setResponseContents(dto.getResponseContents());
    req.setIsresponseYn("Y");
    req.setIsresponseDate(
        LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.KOREA)));

    ResponseObject<MarketInquiryInfoDto> inquiryResult = resourceClient.postForResponseObject(
        ResourceRouteName.OUTBOUND,
        req,
        "/market/naver/setInquiryAnswer",
        new ParameterizedTypeReference<ResponseObject<MarketInquiryInfoDto>>() {});

    returnVal = naverInquiryMasterMapper.updateNaverInquiryInfo(inquiryResult.getData(),dto.getIsresponseId(),dto.getIsresponseNm());

    // 전송결과 업데이트 성공
    if (returnVal > 0 && inquiryResult.getReturnCode().equals("SUCCESS")) {
      log.info("setNaverInquiryAnswer result : {}", returnVal);
      return ResponseUtil.getResoponseObject(Integer.toString(returnVal));
    } else {
      return ResponseUtil.getResoponseObject(ExceptionCode.ERROR_CODE_4002);
    }
  }
}
