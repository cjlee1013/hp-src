package kr.co.homeplus.shipping.mypage.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.client.model.ResponsePagination;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.mypage.model.order.PersonalGetDto;
import kr.co.homeplus.shipping.mypage.model.order.PersonalSetDto;
import kr.co.homeplus.shipping.mypage.service.MypageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/mypage/personal")
@Api(tags = "마이페이지 / 개인정보이용내역 API")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class PersonalController {
  private final MypageService mypageService;

  @ApiOperation(value = "운송업체상세내역", response = ResponseObject.class, notes = "")
  @RequestMapping(value = "/getShipInfo", method = {RequestMethod.POST})
  public ResponseObject<List<PersonalGetDto>> getShipInfo(@RequestBody @Valid PersonalSetDto personalSetDto) {
    List<PersonalGetDto> results = mypageService.getShipInfo(personalSetDto);
    int totalCnt = mypageService.getShipInfoCnt(personalSetDto);
    int perPage = personalSetDto.getPerPage();
    int totalPage;
    if (totalCnt % perPage > 0) {
      totalPage = totalCnt / perPage + 1;
    } else {
      totalPage = totalCnt / perPage;
    }
    if (results == null) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.SYS_ERROR_CODE_9999.getResponseCode(), ExceptionCode.SYS_ERROR_CODE_9999.getResponseMessage());
    } else if (results.size() == 0) {
      return ResourceConverter.toResponseObject(new ArrayList<>(), HttpStatus.OK, ExceptionCode.ERROR_CODE_1093.getResponseCode(), ExceptionCode.ERROR_CODE_1093.getResponseMessage());
    }
    return ResourceConverter.toResponseObject(results,ResponsePagination.Builder.builder().setAsPage(personalSetDto.getPage(),perPage,totalPage).totalCount(totalCnt).build());
  }

}
