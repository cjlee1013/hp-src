package kr.co.homeplus.shipping.market.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class NaverInquiryAnswerSetDto {
    private Long messageNo;						//문의 번호
    private String messageType;					//문의 유형 (B : 게시판, N : 쪽지, E : 긴급메세지)
    private String responseTitle;					//답변 제목
    private String responseContents;					//답변 내용
    private String isresponseId;					//답변자ID
    private String isresponseNm;					//답변자명
}
