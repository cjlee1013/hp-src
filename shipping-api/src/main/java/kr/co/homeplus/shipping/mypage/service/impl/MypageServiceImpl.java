package kr.co.homeplus.shipping.mypage.service.impl;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.shipping.common.model.history.MultiShippingProcessInsertSetDto;
import kr.co.homeplus.shipping.common.model.history.ShippingProcessInsertSetDto;
import kr.co.homeplus.shipping.common.service.HistoryService;
import kr.co.homeplus.shipping.mypage.mapper.OrderMasterMapper;
import kr.co.homeplus.shipping.mypage.mapper.PersonalSlaveMapper;
import kr.co.homeplus.shipping.mypage.model.order.PersonalGetDto;
import kr.co.homeplus.shipping.mypage.model.order.PersonalSetDto;
import kr.co.homeplus.shipping.mypage.model.order.ShippingInvoiceGetDto;
import kr.co.homeplus.shipping.mypage.mapper.OrderSlaveMapper;
import kr.co.homeplus.shipping.mypage.model.order.ShippingHistoryGetDto;
import kr.co.homeplus.shipping.mypage.service.MypageService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MypageServiceImpl implements MypageService {

    private final OrderMasterMapper orderMasterMapper;
    private final OrderSlaveMapper orderSlaveMapper;
    private final PersonalSlaveMapper personalSlaveMapper;
    private final HistoryService historyService;

    @Override
    public int manualShipComplete(String shipNo) {
        int result = 0;
        String shipStatus;
        shipStatus = orderSlaveMapper.getShipInfo(shipNo);
        if (shipStatus == null || shipStatus.isEmpty()) {
            result = -1;
        } else if (shipStatus.equals("D3")) {
            try {
                result = orderMasterMapper.manualShipComplete(shipNo);
                if (result > 0) {
                    try {
                        historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("수취확인","","","mypage","mypage",shipNo));
                    } catch (Exception e) {

                    }
                }
            } catch (Exception e) {
                result = -9999;
            }
        } else if (shipStatus.equals("NN") || shipStatus.equals("D1") || shipStatus.equals("D2")) {
            // 아직 배송 출발 전인 경우
            result = -2;
        } else if (shipStatus.equals("D4") || shipStatus.equals("D5")) {
            // 이미 배송 완료된 경우
            result = -3;
        } else {
            // 기타 예외
            result = -9999;
        }
        return result;
    }

    @Override
    public int manualPurchaseComplete(String shipNo) {
        int result = 0;
        String shipStatus;
        shipStatus = orderSlaveMapper.getShipInfo(shipNo);
        if (shipStatus == null || shipStatus.isEmpty()) {
            result = -1;
        } else if (shipStatus.equals("D3") || shipStatus.equals("D4")) {
            try {
                result = orderMasterMapper.manualPurchaseComplete(shipNo);
                if (result > 0) {
                    try {
                        historyService.setShippingProcessHistory(new ShippingProcessInsertSetDto("구매확정","","","mypage","mypage",shipNo));
                    } catch (Exception e) {

                    }
                }
            } catch (Exception e) {
                result = -9999;
            }
        } else if (shipStatus.equals("NN") || shipStatus.equals("D1") || shipStatus.equals("D2")) {
            // 아직 배송 출발 전인 경우
            result = -2;
        } else if (shipStatus.equals("D5")) {
            // 이미 배송 완료된 경우
            result = -3;
        } else {
            // 기타 예외
            result = -9999;
        }
        return result;
    }

    @Override
    public ShippingInvoiceGetDto getShippingHistory(String shipNo){
        ShippingInvoiceGetDto historyGetDto = orderSlaveMapper.getShippingInvoice(shipNo);
        return getHistory(historyGetDto);
    }

    @Override
    public ShippingInvoiceGetDto getPickHistory(String claimPickShippingNo){
        ShippingInvoiceGetDto historyGetDto = orderSlaveMapper.getPickInvoice(claimPickShippingNo);
        return getHistory(historyGetDto);
    }

    @Override
    public ShippingInvoiceGetDto getExchHistory(String claimExchShippingNo){
        ShippingInvoiceGetDto historyGetDto = orderSlaveMapper.getExchInvoice(claimExchShippingNo);
        return getHistory(historyGetDto);
    }

    @Override
    public List<ShippingHistoryGetDto> getInvoiceHistory(String dlvCd, String invoiceNo){
        List<ShippingHistoryGetDto> shippingHistoryGetDtoList = orderSlaveMapper.getInvoiceHistory(dlvCd, invoiceNo);
        for (ShippingHistoryGetDto shippingHistoryGetDto : shippingHistoryGetDtoList) {
            shippingHistoryGetDto.setShipDetail(StringEscapeUtils.unescapeHtml4(shippingHistoryGetDto.getShipDetail()));
        }

        return shippingHistoryGetDtoList;
    }

    @Override
    public List<PersonalGetDto> getShipInfo(
        PersonalSetDto personalSetDto) {
        personalSetDto.setStartPage();
        return personalSlaveMapper.getShipInfo(personalSetDto);
    }

    @Override
    public int getShipInfoCnt(PersonalSetDto personalSetDto) {
        return personalSlaveMapper.getShipInfoCnt(personalSetDto);
    }

    @Override
    public int manualMultiShipComplete(String multiBundleNo) {
        int result = 0;
        try {
            result = orderMasterMapper.manualMultiShipComplete(multiBundleNo);
            if (result > 0) {
                try {
                    historyService.setMultiShippingProcessHistory(new MultiShippingProcessInsertSetDto("수취확인","","","mypage","mypage",multiBundleNo));
                } catch (Exception e) {

                }
            }
        } catch (Exception e) {
            return -9999;
        }
        return result;
    }

    @Override
    public int manualMultiPurchaseComplete(String multiBundleNo) {
        int result = 0;
        try {
            result = orderMasterMapper.manualMultiPurchaseComplete(multiBundleNo);
            if (result > 0) {
                try {
                    historyService.setMultiShippingProcessHistory(new MultiShippingProcessInsertSetDto("구매확정","","","mypage","mypage",multiBundleNo));
                } catch (Exception e) {

                }
            }
        } catch (Exception e) {
            return -9999;
        }
        return result;
    }

    @Override
    public ShippingInvoiceGetDto getMultiShippingHistory(String multiOrderItemNo){
        ShippingInvoiceGetDto historyGetDto = orderSlaveMapper.getMultiShippingInvoice(multiOrderItemNo);
        if (historyGetDto != null) {
            historyGetDto.setHistoryList(getInvoiceHistory(historyGetDto.getDlvCd(), historyGetDto.getInvoiceNo()));
        }
        return historyGetDto;
    }

    private ShippingInvoiceGetDto getHistory (ShippingInvoiceGetDto historyGetDto){
        if (historyGetDto != null) {
            String invoiceNo = historyGetDto.getInvoiceNo();
            String dlvCd = historyGetDto.getDlvCd();
            if (invoiceNo != null && dlvCd != null && !invoiceNo.equals("") && !dlvCd.equals("")) {
                historyGetDto.setHistoryList(getInvoiceHistory(dlvCd, invoiceNo));
            } else {
                historyGetDto.setHistoryList(new ArrayList<>());
            }
        }
        return historyGetDto;
    }
}
