package kr.co.homeplus.shipping.batch.service;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.batch.mapper.ShippingMessageSlaveMapper;
import kr.co.homeplus.shipping.common.service.MessageSendService;
import kr.co.homeplus.shipping.utils.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShippingMessageService {
    private final ShippingMessageSlaveMapper shippingMessageSlaveMapper;
    private final MessageSendService messageSendService;

    /**
     * 수취확인 요청 알림톡
     */
    public ResponseObject<String> sendComfirmReceiveTalk() {
      List<String> bundleNoList = shippingMessageSlaveMapper.selectBundleReceive5day();

      long talkWorkSeq = messageSendService.sendMessageComfirmReceive(bundleNoList);

      if (talkWorkSeq > 0) {
        return ResponseUtil.getResoponseObject(Long.toString(talkWorkSeq));
      } else {
        return ResponseUtil.getResoponseObjectFail("수취확인 요청 알림톡 발송 실패");
      }
    }

}
