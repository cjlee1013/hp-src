package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "배송추적 콜백 스윗트래커 응답값")
public class ReturnCallbackGetDto {
    List<ReturnCallbackItemGetDto> data;

    public ReturnCallbackGetDto(List<ReturnCallbackItemGetDto> data) {
        this.data = data;
    }
}
