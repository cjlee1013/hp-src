package kr.co.homeplus.shipping.external.mapper;

import kr.co.homeplus.shipping.core.db.annotation.MasterConnection;
import kr.co.homeplus.shipping.external.model.market.MarketShipSetDto;
import kr.co.homeplus.shipping.external.model.store.*;

@MasterConnection
public interface StoreMasterMapper {
    int setShipStart(StoreShipStartSetDto storeShipStartSetDto);

    int setShipVanInfo(StoreShipVaninfoSetDto storeShipVaninfoSetDto);

    int setShipComplete(StoreShipCompleteSetDto storeCommonSetDto);

    int setPickStart(StorePickStartSetDto storePickStartSetDto);

    int setPickVanInfo(StoreClaimVaninfoSetDto storeClaimVaninfoSetDto);

    int setPickSchedule(StorePickStartSetDto storePickStartSetDto);

    int setPickComplete(StoreClaimCommonSetDto storeClaimCommonSetDto);

    int setExchStart(StoreExchStartSetDto storeExchStartSetDto);

    int setExchVanInfo(StoreClaimVaninfoSetDto storeClaimVaninfoSetDto);

    int setExchSchedule(StoreExchStartSetDto storeExchStartSetDto);

    int setExchComplete(StoreClaimCommonSetDto storeClaimCommonSetDto);

    int setMarketShipStart(MarketShipSetDto marketShipSetDto);

    int setMarketShipComplete(MarketShipSetDto marketShipSetDto);
}
