package kr.co.homeplus.shipping.tracking.model.multi;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class MultiRequestGetDto {
    @ApiModelProperty(notes = "다중주문아이템번호")
    private String multiOrderItemNo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "성공여부")
    private String success;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "결과값")
    private String detail;

    public MultiRequestGetDto(String multiOrderItemNo, String success, String detail) {
        this.multiOrderItemNo = multiOrderItemNo;
        this.success = success;
        this.detail = detail;
    }

    public void appendDetail(String detail) { this.detail = this.detail.concat(" " + detail); }
}
