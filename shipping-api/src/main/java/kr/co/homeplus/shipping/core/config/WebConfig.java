package kr.co.homeplus.shipping.core.config;

import com.navercorp.lucy.security.xss.servletfilter.XssEscapeServletFilter;
import javax.servlet.Filter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * web config
 */
@Configuration
public class WebConfig {
    @Bean
    public FilterRegistrationBean<Filter> xssEscapeServletFilter() {
        FilterRegistrationBean<Filter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new XssEscapeServletFilter());
        registrationBean.addUrlPatterns("/*");
        return registrationBean;
    }
}
