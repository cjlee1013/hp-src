package kr.co.homeplus.shipping.partner.service;

import java.util.List;
import kr.co.homeplus.shipping.partner.model.sell.shipStatus.ShipStatusCntGetDto;
import kr.co.homeplus.shipping.partner.model.sell.shipStatus.ShipStatusListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.shipStatus.ShipStatusListSetDto;

public interface ShipStatusService {
  List<ShipStatusListGetDto> getShipStatusList(ShipStatusListSetDto shipStatusListSetDto);
  List<ShipStatusListGetDto> getShipStatusCntList(String schType, String partnerId);
  ShipStatusCntGetDto getShipStatusCnt(String partnerId);
  ShipStatusCntGetDto getShipStatusMainCnt(String partnerId);
}