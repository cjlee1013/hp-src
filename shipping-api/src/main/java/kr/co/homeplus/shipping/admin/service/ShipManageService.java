package kr.co.homeplus.shipping.admin.service;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipInfoPrintGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageConfirmOrderSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageListSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageNotiDelaySetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllExcelSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllPopListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllPopListSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipAllSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipCompleteByBundleSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipCompletePopListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipManageShipCompleteSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipStoreInfoGetDto;

/**
 * shipping service interface
 */
public interface ShipManageService {
  List<ShipManageListGetDto> getShipManageList(ShipManageListSetDto shipManageListSetDto);
  ResponseObject<String> setConfirmOrder(ShipManageConfirmOrderSetDto shipManageConfirmOrderSetDto, String route);
  ResponseObject<String> setNotiDelay(ShipManageNotiDelaySetDto shipManageNotiDelaySetDto, String route);
  ResponseObject<Map<String, Object>> setShipAllExcel(List<ShipManageShipAllExcelSetDto> shipManageShipAllExcelSetDtoList, String route);
  ResponseObject<String> setShipAll(ShipManageShipAllSetDto shipManageShipAllSetDto, String route);
  ResponseObject<String> setShipAllExClaim(ShipManageShipAllSetDto shipManageShipAllSetDto, String route);
  List<ShipManageShipAllPopListGetDto> getShipAllPopList(ShipManageShipAllPopListSetDto shipManageShipAllPopListSetDto);
  ResponseObject<String> setShipComplete(ShipManageShipCompleteSetDto shipManageShipCompleteSetDto, String route);
  ResponseObject<String> setPurchaseCompleteByBundle(ShipManageShipCompleteByBundleSetDto shipManageShipCompleteByBundleSetDto, String route);
  List<ShipManageShipCompletePopListGetDto> getShipCompletePopList(ShipManageShipCompleteSetDto shipManageShipCompleteSetDto, String route);
  ShipStoreInfoGetDto getStoreInfo(String storeId);
  String getStoreNm(String storeId);
  List<ShipInfoPrintGetDto> getShipInfoPrintList(String bundleNo);
}

