package kr.co.homeplus.shipping.partner.model.sell;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "홈 메인 판매진행형황 카운트 응답 DTO")
public class HomeMainCntGetDto {
    @ApiModelProperty(notes = "신규주문 건수")
    private String newCnt;

    @ApiModelProperty(notes = "발송대기 건수")
    private String readyCnt;

    @ApiModelProperty(notes = "주문확인지연 건수")
    private String newDelayCnt;

    @ApiModelProperty(notes = "발송지연 건수")
    private String shipDelayCnt;

    @ApiModelProperty(notes = "배송중 건수")
    private String shippingCnt;
}