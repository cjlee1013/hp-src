package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송추적 콜백 스윗트래커 응답값")
public class TraceCallbackGetDto {

    @ApiModelProperty(notes = "성공여부")
    private String code;

    @ApiModelProperty(notes = "실패사유")
    private String message;

    public TraceCallbackGetDto(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
