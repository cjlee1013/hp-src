package kr.co.homeplus.shipping.tracking.model.pick;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "수거 배송 콜백")
public class PickCallbackSetDto {
    @ApiModelProperty(notes = "수거배송번호")
    private String claimPickShippingNo;

    @ApiModelProperty(notes = "송장번호")
    private String pickInvoiceNo;

    @ApiModelProperty(notes = "택배사코드")
    private String pickDlvCd;

    @ApiModelProperty(notes = "수거상태")
    private String callbackStatus;

    @ApiModelProperty(notes = "처리시간")
    private String callbackDt;

    public PickCallbackSetDto(String claimPickShippingNo, String pickInvoiceNo,
        String callbackStatus, String callbackDt) {
        this.claimPickShippingNo = claimPickShippingNo;
        this.pickInvoiceNo = pickInvoiceNo;
        this.callbackStatus = callbackStatus;
        this.callbackDt = callbackDt;
    }

    public PickCallbackSetDto(String claimPickShippingNo, String pickInvoiceNo, String pickDlvCd,
        String callbackStatus, String callbackDt) {
        this.claimPickShippingNo = claimPickShippingNo;
        this.pickInvoiceNo = pickInvoiceNo;
        this.pickDlvCd = pickDlvCd;
        this.callbackStatus = callbackStatus;
        this.callbackDt = callbackDt;
    }

    public String getCallbackStatus() {
        return callbackStatus;
    }
}
