package kr.co.homeplus.shipping.market.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class MarketInquiryDetailSetDto {
    private Long messageNo; //문의 번호
    private String marketType; //제휴사코드
    private String messageType; //등록구분코드
}
