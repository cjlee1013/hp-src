package kr.co.homeplus.shipping.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsPartnerGetDto;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsPartnerSetDto;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsStoreGetDto;
import kr.co.homeplus.shipping.admin.model.statistics.StatisticsStoreSetDto;
import kr.co.homeplus.shipping.admin.service.StatisticsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/statistics")
@Api(tags = "주문결제 통계")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class StatisticsController {
  private final StatisticsService statisticsService;

  @ApiOperation(value = "기간별 판매업체(결제) 현황")
  @PostMapping("/getPartnerPayment")
  public ResponseObject<List<StatisticsPartnerGetDto>> getPartnerPayment(@RequestBody @Valid StatisticsPartnerSetDto statisticsPartnerSetDto) {
    return ResourceConverter.toResponseObject(statisticsService.getPartnerPayment(statisticsPartnerSetDto));
  }

  @ApiOperation(value = "기간별 점포(결제) 현황")
  @PostMapping("/getStorePayment")
  public ResponseObject<List<StatisticsStoreGetDto>> getStorePayment(@RequestBody @Valid StatisticsStoreSetDto statisticsStoreSetDto) {
    return ResourceConverter.toResponseObject(statisticsService.getStorePayment(statisticsStoreSetDto));
  }

  @ApiOperation(value = "기간별 판매업체(구매확정) 현황")
  @PostMapping("/getPartnerDecision")
  public ResponseObject<List<StatisticsPartnerGetDto>> getPartnerDecision(@RequestBody @Valid StatisticsPartnerSetDto statisticsPartnerSetDto) {
    return ResourceConverter.toResponseObject(statisticsService.getPartnerDecision(statisticsPartnerSetDto));
  }

  @ApiOperation(value = "기간별 점포(구매확정) 현황")
  @PostMapping("/getStoreDecision")
  public ResponseObject<List<StatisticsStoreGetDto>> getStoreDecision(@RequestBody @Valid StatisticsStoreSetDto statisticsStoreSetDto) {
    return ResourceConverter.toResponseObject(statisticsService.getStoreDecision(statisticsStoreSetDto));
  }
}