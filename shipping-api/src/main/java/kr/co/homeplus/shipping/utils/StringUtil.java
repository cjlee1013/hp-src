package kr.co.homeplus.shipping.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/*
 문자열 관련 유틸
 */
public class StringUtil {

  /**
   * 문자열 안에 공백이 있는지 확인
   *
   * @param str 문자열
   * @return 공백 여부
   */
  public static boolean spaceCheck(String str)
  {
    for(int i = 0 ; i < str.length() ; i++)
    {
      if(str.charAt(i) == ' ')
        return true;
    }
    return false;
  }
}
