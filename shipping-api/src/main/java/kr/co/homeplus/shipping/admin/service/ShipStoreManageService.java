package kr.co.homeplus.shipping.admin.service;

import java.util.List;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipStoreManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipStoreManageListSetDto;

/**
 * shipping service interface
 */
public interface ShipStoreManageService {
  List<ShipStoreManageListGetDto> getShipStoreManageList(ShipStoreManageListSetDto shipStoreManageListSetDto);
  List<String> getStoreShiftList(String storeId, String storeType);
}

