package kr.co.homeplus.shipping.common.model.message.info;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "메세지 발송 정보 - 기본정보")
public class MessageInfoBaseDto {
    @ApiModelProperty(notes = "휴대폰번호")
    private String mobileNo;
}
