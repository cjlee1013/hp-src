package kr.co.homeplus.shipping.external.model.store;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "교환완료 상태 변경")
public class StoreExchCompleteSetDto extends StoreClaimCommonSetDto {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "교환 완료 시간(yyyy-MM-dd HH:mm:ss)")
    private String exchCompleteDt;

}
