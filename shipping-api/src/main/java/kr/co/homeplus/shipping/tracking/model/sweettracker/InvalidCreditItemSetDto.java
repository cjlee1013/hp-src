package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "신용코드 확인 아이템")
public class InvalidCreditItemSetDto {
    @ApiModelProperty(notes = "택배사별 신용코드")
    private String cusCde;

    @ApiModelProperty(notes = "배송사코드")
    private String comCode;

    public InvalidCreditItemSetDto(String cusCde, String comCode) {
        this.cusCde = cusCde;
        this.comCode = comCode;
    }

    public String getCusCde() {
        return cusCde;
    }

    public String getComCode() {
        return comCode;
    }
}
