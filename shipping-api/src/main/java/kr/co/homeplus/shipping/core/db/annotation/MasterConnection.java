package kr.co.homeplus.shipping.core.db.annotation;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public @interface MasterConnection {

}
