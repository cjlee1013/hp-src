package kr.co.homeplus.shipping.common.mapper;

import kr.co.homeplus.shipping.common.model.message.info.MessageInfoBsdBuyerGetDto;
import kr.co.homeplus.shipping.common.model.message.info.MessageInfoBsdGetDto;
import kr.co.homeplus.shipping.common.model.message.info.MessageInfoConfirmReceiveGetDto;
import kr.co.homeplus.shipping.common.model.message.info.MessageInfoShipAllGetDto;
import kr.co.homeplus.shipping.common.model.message.info.MessageInfoShipDelayGetDto;
import kr.co.homeplus.shipping.common.model.message.info.MessageInfoTdGetDto;
import kr.co.homeplus.shipping.core.db.annotation.MasterConnection;
import org.apache.ibatis.annotations.Param;

@MasterConnection
public interface MessageMasterMapper {
  MessageInfoShipAllGetDto selectShipAllMessageInfo(@Param("bundleNo") String bundleNo);
  MessageInfoBsdGetDto selectBsdShipMessageInfo(String bundleNo);
  MessageInfoBsdGetDto selectBsdMultiShipMessageInfo(String multiBundleNo);
  MessageInfoBsdBuyerGetDto selectBsdBuyerMessageInfo(@Param("bundleNo") String bundleNo);
  MessageInfoConfirmReceiveGetDto selectConfirmReceiveMessageInfo(@Param("bundleNo") String bundleNo);
  MessageInfoShipDelayGetDto selectShipDelayMessageInfo(@Param("bundleNo") String bundleNo);
  MessageInfoTdGetDto selectShipTDDRCTMessageInfo(String bundleNo);
}