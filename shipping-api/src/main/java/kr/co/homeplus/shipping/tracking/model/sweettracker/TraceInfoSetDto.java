package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송조회 스윗트래커 호출 파라미터")
public class TraceInfoSetDto {
    @ApiModelProperty(notes = "식별코드")
    private String t_key;

    @ApiModelProperty(notes = "택배사코드")
    private String t_code;

    @ApiModelProperty(notes = "송장번호")
    private String t_invoice;
}
