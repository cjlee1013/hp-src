package kr.co.homeplus.shipping.admin.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 택배사코드관리 연동업체설정 저장 요청 DTO")
public class ShipLinkCompanySetDto {
    @ApiModelProperty(notes = "연동업체")
    private String dlvLinkCompany;

    @ApiModelProperty(notes = "연동여부")
    private String linkYn;

    @ApiModelProperty(notes = "연동중지기간여부")
    private String stopDtYn;

    @ApiModelProperty(notes = "연동중지시작일")
    private String stopStartDt;

    @ApiModelProperty(notes = "연동중지종료일")
    private String stopEndDt;

    @ApiModelProperty(notes = "등록자(수정자)")
    private String regId;
}
