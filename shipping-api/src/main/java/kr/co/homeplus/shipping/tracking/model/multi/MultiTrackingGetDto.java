package kr.co.homeplus.shipping.tracking.model.multi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.shipping.tracking.model.common.CommonTrackingGetDto;
import lombok.Data;

@Data
@ApiModel(description = "추적요청 파라미터")
public class MultiTrackingGetDto implements CommonTrackingGetDto {
    @ApiModelProperty(notes = "다중주문아이템번호")
    private String multiOrderItemNo;

    @ApiModelProperty(notes = "다중주문아이템번호목록")
    private String multiOrderItemNos;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    public String[] getMultiOrderItemNos() { return multiOrderItemNos.split(","); }

    @Override
    public String getNo() { return multiOrderItemNo; }

    @Override
    public String getInvoiceNo() { return invoiceNo; }

    @Override
    public String getDlvCd() {
        return dlvCd;
    }
}
