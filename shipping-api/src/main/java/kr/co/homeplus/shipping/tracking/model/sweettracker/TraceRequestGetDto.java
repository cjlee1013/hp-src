package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.beans.ConstructorProperties;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "스윗트래커 추적요청 응답값")
public class TraceRequestGetDto {

    @ApiModelProperty(notes = "성공여부")
    private String success;

    @ApiModelProperty(notes = "목록", dataType = "List")
    private List<TraceRequestItemGetDto> list;

    @ApiModelProperty(notes = "에러코드")
    private String e_code;

    @ApiModelProperty(notes = "에러메시지")
    private String e_message;

    public String getSuccess() {
        return success;
    }

    public List<TraceRequestItemGetDto> getList() { return list; }

    public String getE_code() {
        return e_code;
    }

    public String getE_message() {
        return e_message;
    }

    public TraceRequestGetDto(List<TraceRequestItemGetDto> list) {
        this.success = "false";
        this.list = list;
    }

    @ConstructorProperties({"success","list","e_code","e_message"})
    public TraceRequestGetDto(String success,
        List<TraceRequestItemGetDto> list, String e_code, String e_message) {
        this.success = success;
        this.list = list;
        this.e_code = e_code;
        this.e_message = e_message;
    }
}
