package kr.co.homeplus.shipping.common.model.history;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송 히스토리 입력")
public class MultiShippingProcessInsertSetDto extends ProcessHistoryInsertSetDto{
    @ApiModelProperty(notes = "다중번들번호")
    private String multiBundleNo;

    public MultiShippingProcessInsertSetDto(String historyReason,
        String historyDetailDesc, String historyDetailBefore, String regChannel, String regId, String multiBundleNo) {
        super(historyReason,historyDetailDesc,historyDetailBefore,regChannel,regId);
        this.multiBundleNo = multiBundleNo;
    }
}
