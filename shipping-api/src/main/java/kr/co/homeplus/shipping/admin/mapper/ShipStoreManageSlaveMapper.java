package kr.co.homeplus.shipping.admin.mapper;

import java.util.List;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipStoreManageListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.ShipStoreManageListSetDto;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;
import org.apache.ibatis.annotations.Param;

@SlaveConnection
public interface ShipStoreManageSlaveMapper {
  List<ShipStoreManageListGetDto> selectShipStoreManageList(ShipStoreManageListSetDto shipStoreManageListSetDto);
  List<String> selectStoreShiftList(@Param("storeId") String storeId, @Param("storeType") String storeType);
}
