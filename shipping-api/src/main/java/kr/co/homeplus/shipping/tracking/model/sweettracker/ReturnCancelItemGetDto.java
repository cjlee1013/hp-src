package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModelProperty;
import java.beans.ConstructorProperties;
import lombok.Data;

@Data
public class ReturnCancelItemGetDto {
  @ApiModelProperty(notes = "처리결과")
  private String result;
  @ApiModelProperty(notes = "주문번호")
  private String ordCde;
  @ApiModelProperty(notes = "에러코드(err_cel_01:입력데이터 누락/err_cel_02:존재하지 않는 번호/err_cel_03:취소 불가능한 상태/err_cel_03:시스템 처리 오류")
  private String errCde;
  @ApiModelProperty(notes = "에러코드명")
  private String errMsg;

  public String getResult() {
    return result;
  }

  public String getOrdCde() {
    return ordCde;
  }

  public String getErrCde() {
    return errCde;
  }

  public String getErrMsg() {
    return errMsg;
  }

  @ConstructorProperties({"result","ordCde","errCde","errMsg"})
  public ReturnCancelItemGetDto(String result, String ordCde, String errCde, String errMsg) {
    this.result = result;
    this.ordCde = ordCde;
    this.errCde = errCde;
    this.errMsg = errMsg;
  }
}
