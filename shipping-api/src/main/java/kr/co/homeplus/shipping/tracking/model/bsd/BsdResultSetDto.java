package kr.co.homeplus.shipping.tracking.model.bsd;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.shipping.tracking.model.common.CommonResultSetDto;
import lombok.Data;

@Data
@ApiModel(description = "BSD 추적 결과 입력")
public class BsdResultSetDto extends CommonResultSetDto {
    @ApiModelProperty(notes = "BSD일련번호")
    private String bsdLinkNo;

    public BsdResultSetDto(String bsdLinkNo, String success,String result,
        String detail) {
        super(success, result, detail);
        this.bsdLinkNo = bsdLinkNo;
    }

    public String getBsdLinkNo() { return bsdLinkNo; }
}
