package kr.co.homeplus.shipping.common.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.common.model.message.MessageSendParam;
import kr.co.homeplus.shipping.constants.ResourceRouteName;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageService {

    private final ResourceClient resourceClient;

    // 문자 전송
    public long sendSms(MessageSendParam messageSendParam) {
        Map<String, Object> responseMap = this.send(messageSendParam, "/send/sms");

        if (responseMap.size() > 0) {
            if(Integer.parseInt(responseMap.get("sendCount").toString()) > 0) {
                return Long.parseLong(responseMap.get("smsWorkSeq").toString());
            }
        }

        return -1;
    }

    // 알림톡 전송
    public long sendTalk(MessageSendParam messageSendParam) {
        String uri = "/send/alimtalk";

        // 전환발송 사용 시
        if(!Objects.isNull(messageSendParam.getSwitchTemplateCode())) {
            uri = "/send/alimtalk/switch";
        }

        Map<String, Object> responseMap = this.send(messageSendParam, uri);

        if (responseMap.size() > 0) {
            if(Integer.parseInt(responseMap.get("sendCount").toString()) > 0) {
                return Long.parseLong(responseMap.get("alimtalkWorkSeq").toString());
            }
        }

        return -1;
    }

    // 이메일 전송
    public long sendEmail(MessageSendParam messageSendParam) {
        Map<String, Object> responseMap = this.send(messageSendParam, "/send/mail");

        if (responseMap.size() > 0) {
            if(Integer.parseInt(responseMap.get("sendCount").toString()) > 0) {
                return Long.parseLong(responseMap.get("mailWorkSeq").toString());
            }
        }

        return -1;
    }

    // 전송 공통
    private Map<String, Object> send(MessageSendParam messageSendParam, String uri) {
        Map<String, Object> responseMap = new HashMap<>();

        try {
            log.info("messageSendParam : {}", messageSendParam);

            ResponseObject responseObject = resourceClient.postForResponseObject(ResourceRouteName.MESSAGE, messageSendParam, uri);
            responseMap = new ObjectMapper().convertValue(responseObject.getData(), Map.class);

            log.info("responseBodyResult: {}", responseMap);

        } catch (Exception e) {
            log.error("stackTrace: {}", ExceptionUtils.getStackTrace(e));
        }

        return responseMap;
    }

}
