package kr.co.homeplus.shipping.admin.service;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdInvoiceSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdLinkListGetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdLinkListSetDto;
import kr.co.homeplus.shipping.admin.model.shipManage.BsdLinkTranSetDto;

/**
 * shipping service interface
 */
public interface BsdLinkService {
  List<BsdLinkListGetDto> getBsdLinkList(BsdLinkListSetDto bsdLinkListSetDto);
  ResponseObject<String> setBsdLinkTran(BsdLinkTranSetDto bsdLinkTranSetDto);
  ResponseObject<List<String>> setShippingBsdInvoice(List<BsdInvoiceSetDto> bsdInvoiceSetDtoList);
  ResponseObject<List<String>> setMultiOrderBsdInvoice(List<BsdInvoiceSetDto> bsdInvoiceSetDtoList);
}

