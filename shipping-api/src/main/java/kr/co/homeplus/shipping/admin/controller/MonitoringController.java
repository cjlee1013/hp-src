package kr.co.homeplus.shipping.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringDelayGetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringDelaySetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringPickGetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringPickSetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringShipGetDto;
import kr.co.homeplus.shipping.admin.model.monitoring.MonitoringShipSetDto;
import kr.co.homeplus.shipping.admin.service.MonitoringService;
import kr.co.homeplus.shipping.enums.ExceptionCode;
import kr.co.homeplus.shipping.test.model.qa.QaShipStatusSetDto;
import kr.co.homeplus.shipping.test.service.TestService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/admin/monitoring")
@Api(tags = "배송모니터링")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
public class MonitoringController {

  private final MonitoringService monitoringService;

  @ApiOperation(value = "발송지연조회", response = ResponseObject.class, notes = "")
  @RequestMapping(value = "/getMonitoringDelayList", method = {RequestMethod.POST})
  public ResponseObject<List<MonitoringDelayGetDto>> getMonitoringDelayList(@RequestBody @Valid MonitoringDelaySetDto monitoringDelaySetDto) {
    List<MonitoringDelayGetDto> lists = monitoringService.getMonitoringDelayList(monitoringDelaySetDto);

    if (lists != null ) {
      return ResourceConverter.toResponseObject(lists);
    } else {
      return ResourceConverter.toResponseObject(null, HttpStatus.OK,
          ExceptionCode.ERROR_CODE_1093.getResponseCode(),ExceptionCode.ERROR_CODE_1093.getResponseMessage());
    }
  }

  @ApiOperation(value = "배송모니터링", response = ResponseObject.class, notes = "")
  @RequestMapping(value = "/getMonitoringShipList", method = {RequestMethod.POST})
  public ResponseObject<List<MonitoringShipGetDto>> getMonitoringShipList(@RequestBody @Valid MonitoringShipSetDto monitoringShipSetDto) {
    List<MonitoringShipGetDto> lists = monitoringService.getMonitoringShipList(monitoringShipSetDto);

    if (lists != null ) {
      return ResourceConverter.toResponseObject(lists);
    } else {
      return ResourceConverter.toResponseObject(null, HttpStatus.OK,
          ExceptionCode.ERROR_CODE_1093.getResponseCode(),ExceptionCode.ERROR_CODE_1093.getResponseMessage());
    }
  }

  @ApiOperation(value = "회수지연조회", response = ResponseObject.class, notes = "")
  @RequestMapping(value = "/getMonitoringPickList", method = {RequestMethod.POST})
  public ResponseObject<List<MonitoringPickGetDto>> getMonitoringPickList(@RequestBody @Valid MonitoringPickSetDto monitoringPickSetDto) {
    List<MonitoringPickGetDto> lists = monitoringService.getMonitoringPickList(monitoringPickSetDto);

    if (lists != null ) {
      return ResourceConverter.toResponseObject(lists);
    } else {
      return ResourceConverter.toResponseObject(null, HttpStatus.OK,
          ExceptionCode.ERROR_CODE_1093.getResponseCode(),ExceptionCode.ERROR_CODE_1093.getResponseMessage());
    }
  }
}
