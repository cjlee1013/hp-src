package kr.co.homeplus.shipping.tracking.model.sweettracker;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ReturnCancelItemSetDto {
  @ApiModelProperty(notes = "주문번호")
  private String ordCde;

  public ReturnCancelItemSetDto(String ordCde) {
    this.ordCde = ordCde;
  }

  public String getOrdCde() {
    return ordCde;
  }
}
