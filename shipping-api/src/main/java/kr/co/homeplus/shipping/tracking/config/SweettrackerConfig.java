package kr.co.homeplus.shipping.tracking.config;

import kr.co.homeplus.shipping.core.db.properties.DataSourceProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
@Setter
public class SweettrackerConfig {
  private String type;
  private String tier;
  private String key;
  private String url;

  @Value("${sweettracker.callback}")
  private String callback;

  @Bean
  @ConfigurationProperties("sweettracker.tracking")
  public SweettrackerConfig trackingProperties() {
    return new SweettrackerConfig();
  }
  @Bean
  @ConfigurationProperties("sweettracker.pick")
  public SweettrackerConfig pickProperties() {
    return new SweettrackerConfig();
  }

  @Bean
  public String getCallback(){ return callback; }
}
