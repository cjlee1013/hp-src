package kr.co.homeplus.shipping.admin.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 배송관리 배송완료 요청 DTO")
public class ShipManageShipCompleteSetDto {
    @ApiModelProperty(notes = "배송번호 리스트")
    private List<String> bundleNoList;

    @ApiModelProperty(notes = "수정자")
    private String chgId;
}
