package kr.co.homeplus.shipping.tracking.service;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdResultSetDto;
import kr.co.homeplus.shipping.tracking.model.bsd.BsdTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.common.DlvCdMappingGetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchResultSetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiResultSetDto;
import kr.co.homeplus.shipping.tracking.model.multi.MultiTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickAddressGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickResultSetDto;
import kr.co.homeplus.shipping.tracking.model.pick.PickTrackingGetDto;
import kr.co.homeplus.shipping.tracking.model.exch.ExchRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingCallbackSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingRequestSetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingResultSetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnCancelGetDto;
import kr.co.homeplus.shipping.tracking.model.sweettracker.ReturnRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingRequestGetDto;
import kr.co.homeplus.shipping.tracking.model.shipping.ShippingTrackingGetDto;

public interface TrackingService {

  Map<String,ShippingTrackingGetDto> getShippingInvoice(List<ShippingRequestSetDto> shippingRequestSetDtoList);
  List<ShippingRequestGetDto> setShippingTrackingResult(ShippingResultSetDto shippingResultSetDto,Map<String,ShippingTrackingGetDto> shippingTrackingGetDtoMap);
  int setShippingTrackingCallback(ShippingCallbackSetDto shippingCallbackSetDto);

  Map<String,PickTrackingGetDto> getPickInvoice(List<PickRequestSetDto> pickRequestSetDtoList);
  List<PickRequestGetDto> setPickTrackingResult(PickResultSetDto pickResultSetDto,Map<String,PickTrackingGetDto> pickTrackingGetDtoMap);
  int setPickTrackingCallback(PickCallbackSetDto pickCallbackSetDto);

  Map<String,ExchTrackingGetDto> getExchInvoice(List<ExchRequestSetDto> exchRequestSetDtoList);
  List<ExchRequestGetDto> setExchTrackingResult(ExchResultSetDto exchResultSetDto,Map<String,ExchTrackingGetDto> exchTrackingGetDtoMap);
  int setExchTrackingCallback(ExchCallbackSetDto exchCallbackSetDto);

  List<PickAddressGetDto> getPickAddress(List<PickRequestSetDto> pickRequestSetDto);
  PickRequestGetDto setPickRequestResult(PickResultSetDto pickResultSetDto);
  PickRequestGetDto setPickRequestCancel(PickResultSetDto pickResultSetDto);
  int setPickRequestCallback(PickCallbackSetDto returnCallbackSetDto);

  Map<String, MultiTrackingGetDto> getMultiInvoice(List<MultiRequestSetDto> multiRequestSetDtoList);
  List<MultiRequestGetDto> setMultiTrackingResult(MultiResultSetDto multiResultSetDto,Map<String,MultiTrackingGetDto> multiTrackingGetDtoMap);
  int setMultiTrackingCallback(MultiCallbackSetDto multiCallbackSetDto);

  Map<String, BsdTrackingGetDto> getBsdInvoice(List<BsdRequestSetDto> bsdRequestSetDtoList);
  List<BsdRequestGetDto> setBsdTrackingResult(BsdResultSetDto bsdResultSetDto,Map<String,BsdTrackingGetDto> bsdTrackingGetDtoMap);

  List<DlvCdMappingGetDto> getDlvCdMapping();
}
