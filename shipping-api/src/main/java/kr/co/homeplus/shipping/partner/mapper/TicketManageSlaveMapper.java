package kr.co.homeplus.shipping.partner.mapper;

import java.util.List;
import kr.co.homeplus.shipping.core.db.annotation.SlaveConnection;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.rentalManage.RentalManageListSetDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageListGetDto;
import kr.co.homeplus.shipping.partner.model.sell.ticketManage.TicketManageListSetDto;
import org.apache.ibatis.annotations.Param;

@SlaveConnection
public interface TicketManageSlaveMapper {
  List<TicketManageListGetDto> selectTicketManageList(TicketManageListSetDto ticketManageListSetDto);
  List<TicketManageListGetDto> selectTicketManageCntList(@Param("schType") String schType, @Param("partnerId") String partnerId);
  String selectTicketManageCnt(@Param("schType") String schType, @Param("partnerId") String partnerId);
  String selectTicketShipStatus(@Param("shipNo") String shipNo);
}
