package kr.co.homeplus.claimbatch.claim.entity.claim;

public class ClaimTicketEntity {
    // 클레임 티켓번호
    public String claimTicketNo;
    // 클레임번호(환불:클레임번호)
    public String claimNo;
    // 클레임 묶음 배송번호
    public String claimBundleNo;
    // 클레임아이템NO
    public String claimItemNo;
    // 상품주문번호
    public String orderItemNo;
    // 주문티켓번호(발급거래번호)
    public String orderTicketNo;
    // 등록일
    public String regDt;
    // 수정일
    public String chgDt;
}
