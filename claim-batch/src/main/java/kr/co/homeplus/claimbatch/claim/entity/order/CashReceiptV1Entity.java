package kr.co.homeplus.claimbatch.claim.entity.order;

public class CashReceiptV1Entity {
    // 현금영수증 발행 SEQ
    public String seq;
    // 기준일자(배송완료 / 환불완료)
    public String basicDt;
    // 구분 (1:배송완료, 2:환불완료)
    public String gubun;
    // 파트너 아이디
    public String partnerId;
    // 결제번호
    public String paymentNo;
    // 번들번호
    public String bundleNo;
    // 구매주문번호
    public String purchaseOrderNo;
    // 클레임번호
    public String claimNo;
    // PG종류(INICIS,LGU)
    public String pgKind;
    // 결제수단
    public String parentMethodCd;
    // PG사 거래번호
    public String pgTid;
    // PG_oid(주문번호)
    public String pgOid;
    // PG결제 금액
    public String pgAmt;
    // DGV 결제금액
    public String dgvAmt;
    // 주문/클레임 PG결제 전체 금액
    public String realPgAmt;
    // 주문/클레임 DGV 포인트 전체금액
    public String realDgvAmt;
    // 현금영수증 용도(PERS:개인소득공제용,EVID:지출증빙용,NONE:미발행)
    public String cashReceiptUsage;
    // 현금영수증 유형(PHONE:휴대폰번호,CARD:현금영수증카드번호)
    public String cashReceiptType;
    // 현금영수증 신청번호
    public String cashReceiptReqNo;
    // 현금영수증 발행여부
    public String cashReceiptYn;
    // 문화비소득공제 대상여부
    public String cultureCostYn;
    // 과세 여부 (Y:과세, N:면세)
    public String taxYn;
    // PG과세금액(신용카드,간편결제)
    public String pgTaxAmt;
    // PG면세금액(신용카드,간편결제)
    public String pgFreetaxAmt;
    // 과세상품 현금영수증 대상 (PG+DGV) 합계금액
    public String taxSumAmt;
    // 공급가액
    public String supplyAmt;
    // 부가세
    public String vatAmt;
    // 현금영수증 취소후 남은금액(PG+DGV)
    public String claimTaxSumAmt;
    // 사업자 등록번호
    public String partnerNo;
    // 회사 상호
    public String partnerNm;
    // 회사 대표자명
    public String partnerOwner;
    // 업태 (itm_partner.business_conditions)
    public String businessConditions;
    // 업종명 (itm_partner.biz_cate_cd) 코드값 변환
    public String bizCateNm;
    // 지번주소1
    public String addr1;
    // 지번주소2
    public String addr2;
    // 회사 대표번호1
    public String phone1;
    // 우편번호
    public String zipcode;
    // 구매자명
    public String buyerNm;
    // 구매자 이메일
    public String buyerEmail;
    // 구매자 연락처
    public String buyerMobileNo;
    // 상품명
    public String itemNm;
    // 부분취소여부 ( Y: 부분취소, N: 전체취소)
    public String partYn;
    //
    public String referSeq;
    // 등록일시
    public String regDt;
    // 등록아이디
    public String regId;
    // 수정일시
    public String chgDt;
}
