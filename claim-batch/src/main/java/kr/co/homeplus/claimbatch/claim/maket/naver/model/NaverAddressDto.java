package kr.co.homeplus.claimbatch.claim.maket.naver.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class NaverAddressDto {
    @ApiModelProperty(value= "주소타입코드")
    private String addressType;
    @ApiModelProperty(value= "우편번호")
    private String zipCode;
    @ApiModelProperty(value= "기본주소")
    private String baseAddress;
    @ApiModelProperty(value= "상세주소")
    private String detailedAddress;
    @ApiModelProperty(value= "도로명주소여부")
    private String isRoadNameAddress;
    @ApiModelProperty(value= "도시(국내주소에는빈문자열)")
    private String city;
    @ApiModelProperty(value= "주(국내주소에는빈문자열)")
    private String state;
    @ApiModelProperty(value= "국가(국내주소에는빈문자열)")
    private String country;
    @ApiModelProperty(value= "연락처1")
    private String tel1;
    @ApiModelProperty(value= "연락처2")
    private String tel2;
    @ApiModelProperty(value= "이름")
    private String name;
    @ApiModelProperty(value= "수령위치코드")
    private String pickupLocationType;
    @ApiModelProperty(value= "수령위치텍스트입력")
    private String pickupLocationContent;
    @ApiModelProperty(value= "출입방법코드")
    private String entryMethod;
    @ApiModelProperty(value= "출입방법텍스트입력")
    private String entryMethodContent;
}
