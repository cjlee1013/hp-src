package kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MarketClaimCancelInfoDto {
    @ApiModelProperty(value= "마켓클레임취소정보번호")
    private long marketClaimCancelNo;
    @ApiModelProperty(value= "업체주문상품번호")
    private String marketOrderItemNo;
    @ApiModelProperty(value= "클레임처리상태코드")
    private String claimStatus;
    @ApiModelProperty(value= "클레임요청일")
    private String claimRequestDate;
    @ApiModelProperty(value= "접수채널")
    private String requestChannel;
    @ApiModelProperty(value= "취소사유코드")
    private String cancelReason;
    @ApiModelProperty(value= "취소상세사유")
    private String cancelDetailedReason;
    @ApiModelProperty(value= "취소완료일")
    private String cancelCompletedDate;
    @ApiModelProperty(value= "취소승인일")
    private String cancelApprovalDate;
    @ApiModelProperty(value= "환불예정일")
    private String refundExpectedDate;
    @ApiModelProperty(value= "환불대기상태")
    private String refundStandbyStatus;
    @ApiModelProperty(value= "환불대기사유")
    private String refundStandbyReason;
}
