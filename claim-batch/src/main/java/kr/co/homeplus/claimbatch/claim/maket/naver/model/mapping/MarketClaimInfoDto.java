package kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MarketClaimInfoDto {
    @ApiModelProperty(value= "마켓클레임배송정보번호(반품/교환)")
    private long marketClaimInfoNo;
    @ApiModelProperty(value= "마켓클레임번호")
    private long marketClaimNo;
    @ApiModelProperty(value= "마켓클레임번호")
    private long marketClaimItemNo;
    @ApiModelProperty(value= "업체주문상품번호")
    private String marketOrderItemNo;
    @ApiModelProperty(value= "클레임처리상태코드")
    private String claimStatus;
    @ApiModelProperty(value= "클레임요청일")
    private String claimRequestDate;
    @ApiModelProperty(value= "접수채널")
    private String requestChannel;
    @ApiModelProperty(value= "클레임사유코드")
    private String claimReason;
    @ApiModelProperty(value= "반품상세사유")
    private String claimDetailReason;
    @ApiModelProperty(value= "취소완료일")
    private String cancelCompletedDate;
    @ApiModelProperty(value= "취소승인일")
    private String cancelApprovalDate;
    @ApiModelProperty(value= "보류상태코드")
    private String holdbackStatus;
    @ApiModelProperty(value= "보류사유코드")
    private String holdbackReason;
    @ApiModelProperty(value= "보류상세사유")
    private String holdbackDetailReason;
    @ApiModelProperty(value= "수거지기본주소")
    private String collectBaseAddress;
    @ApiModelProperty(value= "수거지상세주소")
    private String collectDetailAddress;
    @ApiModelProperty(value= "수거지우편번호")
    private String collectZipcode;
    @ApiModelProperty(value= "수거자이름")
    private String collectName;
    @ApiModelProperty(value= "수거지연락처1")
    private String collectTelNo1;
    @ApiModelProperty(value= "수거지연락처2")
    private String collectTelNo2;
    @ApiModelProperty(value= "수취지기본주소")
    private String returnBaseAddress;
    @ApiModelProperty(value= "수취지상세주소")
    private String returnDetailAddress;
    @ApiModelProperty(value= "수취지우편번호")
    private String returnZipcode;
    @ApiModelProperty(value= "수취자이름")
    private String returnName;
    @ApiModelProperty(value= "수취지연락처1")
    private String returnTelNo1;
    @ApiModelProperty(value= "수취지연락처2")
    private String returnTelNo2;
    @ApiModelProperty(value= "수거상태")
    private String collectStatus;
    @ApiModelProperty(value= "수거방법코드")
    private String collectDeliveryMethod;
    @ApiModelProperty(value= "수거택배사코드")
    private String collectDeliveryCompany;
    @ApiModelProperty(value= "수거송장번호")
    private String collectTrackingNumber;
    @ApiModelProperty(value= "수거완료일")
    private String collectCompletedDate;
    @ApiModelProperty(value= "배송비청구액")
    private long claimDeliveryFeeDemandAmount;
    @ApiModelProperty(value= "배송비결제방법")
    private String claimDeliveryFeePayMethod;
    @ApiModelProperty(value= "배송비결제수단")
    private String claimDeliveryFeePayMeans;
    @ApiModelProperty(value= "기타비용청구액")
    private String etcFeeDemandAmount;
    @ApiModelProperty(value= "기타비용결제방법")
    private String etcFeePayMethod;
    @ApiModelProperty(value= "기타비용결제수단")
    private String etcFeePayMeans;
    @ApiModelProperty(value= "환불대기상태")
    private String refundStandbyStatus;
    @ApiModelProperty(value= "환불대기사유")
    private String refundStandbyReason;
    @ApiModelProperty(value= "환불예정일")
    private String refundExpectedDate;
    @ApiModelProperty(value= "반품완료일")
    private String returnCompletedDate;
    @ApiModelProperty(value= "보류설정일")
    private String holdbackConfigDate;
    @ApiModelProperty(value= "보류설정자(구매자,판매자,관리자,시스템)")
    private String holdbackConfigurer;
    @ApiModelProperty(value= "보류해제일")
    private String holdbackReleaseDate;
    @ApiModelProperty(value= "보류해제자(구매자,판매자,관리자,시스템)")
    private String holdbackReleaser;
    @ApiModelProperty(value= "배송비묶음청구상품주문번호(다중일시쉼표구분)")
    private String claimDeliveryFeeProductOrderIds;
    @ApiModelProperty(value= "배송비할인액")
    private long claimDeliveryFeeDiscountAmount;
    @ApiModelProperty(value= "재배송상태")
    private String reDeliveryStatus;
    @ApiModelProperty(value= "재배송방법코드")
    private String reDeliveryMethod;
    @ApiModelProperty(value= "재배송택배사코드")
    private String reDeliveryCompany;
    @ApiModelProperty(value= "재배송송장번호")
    private String reDeliveryTrackingNumber;
    @ApiModelProperty(value= "재배송처리일")
    private String reDeliveryOperationDate;
    @ApiModelProperty(value= "등록일")
    private String regDt;
    @ApiModelProperty(value= "등록자ID")
    private String regId;
    @ApiModelProperty(value= "수정일시")
    private String chgDt;
    @ApiModelProperty(value= "수정자ID")
    private String chgId;
}
