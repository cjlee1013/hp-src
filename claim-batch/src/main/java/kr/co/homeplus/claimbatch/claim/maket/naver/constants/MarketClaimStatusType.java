package kr.co.homeplus.claimbatch.claim.maket.naver.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MarketClaimStatusType {
    // 취소요청
    MARKET_NAVER_CLAIM_CANCEL_REQUEST("CANCEL_REQUEST", "C1"),
    // 취소처리중
    MARKET_NAVER_CLAIM_CANCELING("CANCELING", "C2"),
    // 취소완료
    MARKET_NAVER_CLAIM_CANCEL_DONE("CANCEL_DONE", "C3"),
    // 취소철회
    MARKET_NAVER_CLAIM_CANCEL_REJECT("CANCEL_REJECT", "C4"),
    // 반품요청
    MARKET_NAVER_CLAIM_RETURN_REQUEST("RETURN_REQUEST", "C1"),
    // 반품완료
    MARKET_NAVER_CLAIM_RETURN_DONE("RETURN_DONE", "C3"),
    // 반품철회
    MARKET_NAVER_CLAIM_RETURN_REJECT("RETURN_REJECT", "C4"),
    // 교환요청
    MARKET_NAVER_CLAIM_EXCHANGE_REQUEST("EXCHANGE_REQUEST", "C1"),
    // 교환완료
    MARKET_NAVER_CLAIM_EXCHANGE_DONE("EXCHANGE_DONE", "C3"),
    // 교환거부
    MARKET_NAVER_CLAIM_EXCHANGE_REJECT("EXCHANGE_REJECT", "C4"),
    // 교환재배송중
    MARKET_NAVER_CLAIM_EXCHANGE_REDELIVERING("EXCHANGE_REDELIVERING", "C4"),
    // 수거처리중
    MARKET_NAVER_CLAIM_COLLECTING("COLLECTING", "C2"),
    // 수거완료
    MARKET_NAVER_CLAIM_COLLECT_DONE("COLLECT_DONE", "C3"),
    // 직권취소중
    MARKET_NAVER_CLAIM_ADMIN_CANCELING("ADMIN_CANCELING", "C2"),
    // 직권취소완료
    MARKET_NAVER_CLAIM_ADMIN_CANCEL_DONE("ADMIN_CANCEL_DONE", "C3"),
    ;

    private String marketClaimStatus;
    private String refitClaimStatus;

}
