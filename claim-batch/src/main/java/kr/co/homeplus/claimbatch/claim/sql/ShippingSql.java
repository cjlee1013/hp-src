package kr.co.homeplus.claimbatch.claim.sql;

import kr.co.homeplus.claimbatch.claim.constants.CustomConstants;
import kr.co.homeplus.claimbatch.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.ship.ShippingNonRcvEntity;
import kr.co.homeplus.claimbatch.claim.enums.MysqlFunction;
import kr.co.homeplus.claimbatch.claim.factory.EntityFactory;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import kr.co.homeplus.claimbatch.util.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ShippingSql {

    public static String selectRequestWithdrawalOfUnreceivedDelivery(@Param("processDt") String processDt) {
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class);
        SQL query  = new SQL()
            .SELECT(
                shippingNonRcv.purchaseOrderNo,
                shippingNonRcv.bundleNo
            )
            .FROM(EntityFactory.getTableName(shippingNonRcv))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessType, "C"),
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "Y"),
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "N"),
                SqlUtils.customOperator(
                    CustomConstants.ANGLE_BRACKET_LEFT,
                    shippingNonRcv.noRcvProcessDt,
                    SqlUtils.sqlFunction(
                        MysqlFunction.DATE_FULL_LAST,
                        SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray(SqlUtils.appendSingleQuote(processDt), "-8"))
                    )
                )
            );
        log.debug("selectRequestWithdrawalOfUnreceivedDelivery ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateShipNotReceivedWithdrawal(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(shippingNonRcv))
            .SET(
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessType, "W"),
                SqlUtils.equalColumn(shippingNonRcv.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(shippingNonRcv.chgId, "BATCH"),
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessCntnt, "철회요청 장기미처리로 인한 자동철회"),
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "Y")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingNonRcv.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(shippingNonRcv.bundleNo, bundleNo)
            );
        log.debug("updateShipNotReceivedWithdrawal ::: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectNonShipCompleteInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class);
        SQL query = new SQL()
            .SELECT(
                shippingItem.purchaseOrderNo,
                shippingItem.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shipStatus, shippingItem.shipStatus),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_24, SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.completeDt), shippingItem.completeDt)
            )
            .FROM(EntityFactory.getTableName(shippingItem))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo)
            )
            .GROUP_BY(shippingItem.purchaseOrderNo, shippingItem.bundleNo);
        log.debug("selectNonShipCompleteInfo ::: [{}]", query.toString());
        return query.toString();
    }

    public static String selectNonReceivedShippingComplete(@Param("processDt") String processDt) {
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                shippingNonRcv.purchaseOrderNo,
                shippingNonRcv.bundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingNonRcv))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(shippingNonRcv.purchaseOrderNo, shippingNonRcv.bundleNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipStatus, ObjectUtils.toArray("'D4'", "'D5'"))))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "N"),
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "N"),
                SqlUtils.customOperator(CustomConstants.LEFT_UP_EQUAL, shippingItem.completeDt, SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.DATEADD_MON, ObjectUtils.toArray(SqlUtils.appendSingleQuote(processDt), -1)))),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, shippingItem.completeDt, SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, SqlUtils.sqlFunction(MysqlFunction.DATEADD_MON, ObjectUtils.toArray(SqlUtils.appendSingleQuote(processDt), -1))))
            )
            .GROUP_BY(shippingNonRcv.purchaseOrderNo, shippingNonRcv.bundleNo);
        log.debug("selectNonReceivedShippingComplete ::: [{}]", query.toString());
        return query.toString();
    }

    public static String updateNotReceivedShippingCompleteWithdrawal(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        ShippingNonRcvEntity shippingNonRcv = EntityFactory.createEntityIntoValue(ShippingNonRcvEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(shippingNonRcv))
            .SET(
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessType, "W"),
                SqlUtils.equalColumn(shippingNonRcv.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(shippingNonRcv.chgId, "BATCH"),
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessCntnt, "철회요청 장기미처리로 인한 자동철회"),
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvWthdrwYn, "Y"),
                SqlUtils.equalColumnByInput(shippingNonRcv.noRcvProcessYn, "Y"),
                SqlUtils.equalColumn(shippingNonRcv.noRcvProcessDt, "NOW()")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingNonRcv.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(shippingNonRcv.bundleNo, bundleNo)
            );
        log.debug("updateNotReceivedShippingCompleteWithdrawal ::: \n[{}]", query.toString());
        return query.toString();
    }
}
