package kr.co.homeplus.claimbatch.pine.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.claimbatch.history.model.BatchHistoryDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "PINE 영수증 누락 거래정보(TRAN 생성용)")
@Getter
@Setter
@EqualsAndHashCode
public class ClaimPineReceiptOmitTranDto {

    @ApiModelProperty(value = "점포ID", position = 1)
    private String storeId;

    @ApiModelProperty(value = "POS번호", position = 2)
    private String posNo;

    @ApiModelProperty(value = "거래일자(YYYYMMDD)", position = 3)
    private String tradeDt;

    @ApiModelProperty(value = "누락번호", position = 4)
    private String omitNo;

    @ApiModelProperty(value = "다음누락번호", position = 5)
    private String nextOmitNo;

    @ApiModelProperty(value = "본부코드", position = 6)
    private String headOfficeCode;

    @ApiModelProperty(value = "지역코드", position = 7)
    private String areaCode;

    @ApiModelProperty(value = "데이터 전송여부", position = 8)
    private String dataSendYn;

    @ApiModelProperty(value = "클레임번호(PINE 전송용)", position = 9)
    private String claimOmitNo;

    @ApiModelProperty(value = "시작 누락번호", position = 10)
    private String startOmitNo;

    @ApiModelProperty(value = "개별번호 생성유무", position = 11)
    private String issuedYn;

    @ApiModelProperty(value = "배치이력 Dto", position = 12)
    private BatchHistoryDto batchHistoryDto;
}
