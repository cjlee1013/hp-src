package kr.co.homeplus.claimbatch.claim.maket.service;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.NaverReturnInfoDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimItemDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimInfoDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimRegDetailDto;

public interface MarketMapperService {

    boolean addMarketClaim(MarketClaimDto marketClaimDto);

    boolean addMarketClaimItem(MarketClaimItemDto marketClaimItemDto);

    boolean addMarketClaimInfo(MarketClaimInfoDto marketClaimInfoDto);

    boolean addMarketClaimItemCheck(MarketClaimItemDto marketClaimItemDto);

    LinkedHashMap<String, Object> getMarketOrderItemInfo(String marketOrderItemNo);

    List<LinkedHashMap<String, Object>> getCreateMarketClaimData(String marketOrderItemNo);

    boolean setMarketClaimProcessResultSave(long marketClaimNo, String rcvYn);

    boolean setMarketClaimAfterProcessClaimNoSave(long marketClaimNo, long claimNo);

    LinkedHashMap<String, Object> getClaimReasonType(String claimReasonType);

    ClaimRegDetailDto getClaimRegDetailDtoInfo(long marketClaimNo, String marketOrderItemNo);

    boolean getMarketClaimPrevCheck(String marketClaimId);

    boolean modifyPrevDataStatus(String marketClaimId, MarketClaimItemDto marketClaimItemDto, MarketClaimInfoDto marketClaimInfoDto);

    boolean modifyMarketClaimAmount(String marketClaimId, MarketClaimDto marketClaimDto);

    boolean modifyHoldbackStatus(String marketOrderItemNo, String holdbackStatus);

    LinkedHashMap<String, Object> getExistingClaimByMarket(long marketClaimNo);

    List<LinkedHashMap<String, Object>> getMarketClaimList(long marketClaimNo);

    int getMarketOrderItemCnt(String marketOrderNo);

    int getMarketClaimItemCnt(String marketOrderNo);

    boolean isMarketClaimAnotherStatusCheck(long marketClaimNo, String claimStatus);

    List<LinkedHashMap<String, Object>> getMarketClaimingDataCnt(String marketOrderNo);

    int getMarketClaimRequestCnt(long marketClaimNo);

    int getPrevClaimItemCntByMarket(long marketClaimNo);
}
