package kr.co.homeplus.claimbatch.claim.service.impl;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claimbatch.claim.mapper.ShippingReadMapper;
import kr.co.homeplus.claimbatch.claim.mapper.ShippingWriteMapper;
import kr.co.homeplus.claimbatch.claim.service.ShippingMapperService;
import kr.co.homeplus.claimbatch.util.SqlUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ShippingMapperServiceImpl implements ShippingMapperService {

    private final ShippingReadMapper shippingReadMapper;
    private final ShippingWriteMapper shippingWriteMapper;

    @Override
    public List<LinkedHashMap<String, Object>> getRequestWithdrawalOfUnreceivedDeliveryInfo(String processDt) {
        return shippingReadMapper.selectRequestWithdrawalOfUnreceivedDelivery(processDt);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getNonReceivedShippingCompleteInfo(String processDt) {
        return shippingReadMapper.selectNonReceivedShippingComplete(processDt);
    }

    @Override
    public LinkedHashMap<String, Object> getNonShipCompleteInfo(long purchaseOrderNo, long bundleNo) {
        return shippingReadMapper.selectNonShipCompleteInfo(purchaseOrderNo, bundleNo);
    }

    @Override
    public boolean modifyUnreceivedDeliveryWithdrawal(long purchaseOrderNo, long bundleNo) {
        return SqlUtils.isGreaterThanZero(shippingWriteMapper.updateShipNotReceivedWithdrawal(purchaseOrderNo, bundleNo));
    }

    @Override
    public boolean modifyUnReceivedShippingCompleteWithdrawal(long purchaseOrderNo, long bundleNo) {
        return SqlUtils.isGreaterThanZero(shippingWriteMapper.updateNotReceivedShippingCompleteWithdrawal(purchaseOrderNo, bundleNo));
    }
}
