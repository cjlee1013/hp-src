package kr.co.homeplus.claimbatch.claim.entity.claim;

public class ClaimPickShippingEntity {
    // 클레임 반품수거 번호
    public String claimPickShippingNo;
    // 클레임 묶음번호(환불:클레임 번호)
    public String claimBundleNo;
    // 고객번호
    public String userNo;
    // 반품수거_자동접수 여부(자동접수: Y)
    public String isAuto;
    // 구매자 반품발송여부(구매자 발송 : Y)
    public String isPick;
    // 수거대행 여부 - 파트너 등록 정보 참조(Y: 대행가능 / N:대행불가)
    public String isPickProxy;
    // 수거배송수단(C:택배배송 D:업체직배송 N:배송없음)
    public String pickShipType;
    // 수거상태(NN: 요청대기 - 자동접수 미지원 택배사인 경우 N0: 수거예약 - 택배사 수동 예약 P0:자동접수요청  (반품자동접수 요청성공) P1: 요청완료-송장등록 (반품자동접수 후 송장번호 받은경우 / 수거요청 처리로 송장등록) P2: 수거중 P3: 수거완료 P8 :수거실패 (반품자동접수실패))
    public String pickStatus;
    // 수거_택배코드
    public String pickDlvCd;
    // 원 송장번호
    public String orgInvoiceNo;
    // 수거_송장번호
    public String pickInvoiceNo;
    // 수거송장 등록자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)
    public String pickInvoiceRegId;
    // 배송추적요청여부
    public String pickTrackingYn;
    // 배송추적응답여부(R:추적대기,T:추적여부수신(스윗트래커콜백수신시),F1:실패-필수 파라미터가 빈 값이거나 없음,F2:실패-운송장번호 유효성 검사 실패,F3:이미 등록된 운송장번호,F4:지원하지 않는 택배사코드,F9:시스템오류)
    public String pickTrackingResult;
    // 수거_이름
    public String pickReceiverNm;
    // 수거_우편번호
    public String pickZipcode;
    // 수거_주소1
    public String pickBaseAddr;
    // 수거_주소2
    public String pickDetailAddr;
    // 수거_도로명 기본주소
    public String pickRoadBaseAddr;
    // 수거_도로명 상세주소
    public String pickRoadDetailAddr;
    // 수거_휴대전화
    public String pickMobileNo;
    // 수거_연락처
    public String pickPhone;
    // 수거_메모
    public String pickMemo;
    // 수거일자(TD)
    public String shipDt;
    // 수거shiftId
    public String shiftId;
    // 수거slotId
    public String slotId;
    // 수거_예약접수자
    public String pickP0Id;
    // 수거_예약일(업체직배송인 경우 수거예정일 최초등록일)
    public String pickP0Dt;
    // 수거_송장등록일(업체직배송의 경우 수거예정일)
    public String pickP1Dt;
    // 수거_시작일
    public String pickP2Dt;
    // 수거_완료일
    public String pickP3Dt;
    // 안심번호사용유무
    public String safetyUseYn;
    // 파트너ID
    public String partnerId;
    // 등록ID
    public String regId;
    // 등록일
    public String regDt;
    // 수정자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)
    public String chgId;
    // 수정일
    public String chgDt;
}
