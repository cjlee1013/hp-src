package kr.co.homeplus.claimbatch.claim.entity.multi;

public class MultiOrderItemEntity {
    // 다중주문아이템번호
    public String multiOrderItemNo;
    // 구매주문번호
    public String purchaseOrderNo;
    // 번들번호
    public String bundleNo;
    // 다중번들번호
    public String multiBundleNo;
    // 주문상품번호
    public String orderItemNo;
    // 배송상태(NN:대기,D1:주문확인전(결제완료),D2:주문확인(상품준비중),D3:배송중,D4:배송완료,D5:구매확정)
    public String shipStatus;
    // 상품번호
    public String itemNo;
    // 상품가격(상품판매가)
    public String itemPrice;
    // 상품수량
    public String itemQty;
    // 택배코드
    public String dlvCd;
    // 송장번호
    public String invoiceNo;
    // 예약배송일자
    public String reserveShipDt;
    // 확인일자(판매자 확인일자 D1 > D2) (BSD전송일자?)
    public String confirmDt;
    // 배송시작일(D2 > D3)
    public String shippingDt;
    // 배송완료일(D3 > D4) 홈플러스 배송완료일자
    public String completeDt;
    // 택배사배송완료일
    public String completeRegDt;
    // 구매확정일자 (D4->D5)
    public String decisionDt;
    // 등록일시
    public String regDt;
    // 등록자ID
    public String regId;

}
