package kr.co.homeplus.claimbatch.monitoring.mapper;

import kr.co.homeplus.claimbatch.core.db.annotation.SlaveConnection;
import kr.co.homeplus.claimbatch.monitoring.model.ClaimPaymentMonitoringDto;
import kr.co.homeplus.claimbatch.monitoring.sql.ClaimMontoringSql;
import org.apache.ibatis.annotations.SelectProvider;

@SlaveConnection
public interface ClaimMonitoringMapper {
    @SelectProvider(type = ClaimMontoringSql.class, method = "selectClaimPaymentFailMonitoring")
    ClaimPaymentMonitoringDto selectClaimPaymentFailMonitoring();
}
