package kr.co.homeplus.claimbatch.pine.service;

import kr.co.homeplus.claimbatch.history.model.BatchHistoryDto;

/**
 * PINE 전송 후 금액이 잘못 게산되어 다시 재송신을 하는 경우
 * 1. 클레임 번호로 다시 생성(데이터 오류) - claim_no 기준으로 모두 재송신
 * 2. 주문상점번호(purchaseStoreInfoNo)로 클레임 데이터 생성
 * 3. 클레임상점번호(claimStoreInfoNo)로 재송신 - 같은 claim_no 로 TD/DS 상품이 동시에 존재하는데 그중 하나만 다시 재송신 하는 경우
 * 4. 클레임상점번호(claimStoreInfoNo)로 재송신 - 클레임에서 반품배송비의 경우는 주문으로 송신 해당건을 삭제하기 위한 클레임 생성
 * 5. 모든 대상을 PINE으로 송신한다. (확인을 위해 별도로 생성)
 */
public interface PineReSendService {

    // 클레임 번호로 다시 생성(데이터 오류) - claim_no 기준으로 모두 재송신
    void pineReSendClaimNo(BatchHistoryDto batchHistoryDto, String claimNo);

    // 주문상점번호(purchaseStoreInfoNo)로 클레임 데이터 생성
    void pineReSendPurchaseStoreInfoNo(BatchHistoryDto batchHistoryDto, String purchaseStoreInfoNo);

    // 클레임상점번호(claimStoreInfoNo)로 재송신 - 같은 claim_no 로 TD/DS 상품이 동시에 존재하는데 그중 하나만 다시 재송신 하는 경우
    void pineReSendClaimStoreInfoNo(BatchHistoryDto batchHistoryDto, String claimStoreInfoNo);

    // 클레임상점번호(claimStoreInfoNo)로 재송신 - 클레임에서 반품배송비의 경우는 주문으로 송신 해당건을 삭제하기 위한 클레임 생성
    void pineReSendShipClaimStoreInfoNo(BatchHistoryDto batchHistoryDto, String shipClaimStoreInfoNo);

    // 모든 대상을 PINE으로 송신한다. (확인을 위해 별도로 생성)
    void pineReSendExecute(BatchHistoryDto batchHistoryDto);

}
