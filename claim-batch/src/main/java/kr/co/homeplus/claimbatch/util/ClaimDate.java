package kr.co.homeplus.claimbatch.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ClaimDate {

    // LocalDate
    public static final String DIGIT_YMD = "yyyyMMdd";

    public static final String DEFAULT_YMD = "yyyy-MM-dd";

    // LocalDateTime
    public static final String DIGIT_YMD_HIS = "yyyyMMddHHmmss";

    public static final String DIGIT_HI = "HHmm";

    public static final String DEFAULT_YMD_HIS = "yyyy-MM-dd HH:mm:ss";

    public static final String MILL_YMD_HIS = "yyyy-MM-dd HH:mm:ss.SSS";

    ////// Formatter 가져오기 //////
    /**
     * format String to Formatter
     * parameter 로 받아온 format 문자열로 DateTimeFormatter return
     * @param formatStr Pattern String
     * @return formatter by pattern
     * */
    private static DateTimeFormatter getFormatter(String formatStr) {
        return DateTimeFormatter.ofPattern(formatStr);
    }

    /**
     * Default DateTime Formatter 리턴
     * @return DateTimeFormatter - yyyy-MM-dd HH:mm:ss
     * */
    private static DateTimeFormatter getFormatter() {
        return DateTimeFormatter.ofPattern(DEFAULT_YMD_HIS);
    }

    ////// 현재 날짜,시간 가져오기 //////
    /**
     * 현재 날짜 가져오기
     * Default Date Format yyyy-MM-dd
     * @return dateTimeStr
     * */
    public static String getCurrentYmd() {
        return LocalDate.now().format(getFormatter(DEFAULT_YMD));
    }

    /**
     * 현재 날짜 가져오기
     * Default Date Format yyyyMMdd
     * @return dateTimeStr
     * */
    public static String getCurrentDitYmd() {
        return LocalDate.now().format(getFormatter(DIGIT_YMD));
    }

    /**
     * 현재 날짜 가져오기
     * Default Date Format yyyyMMddHHmmss
     * @return dateTimeStr
     * */
    public static String getCurrentDitYmdHis() {
        return LocalDateTime.now().format(getFormatter(DIGIT_YMD_HIS));
    }

    /**
     * 현재 날짜 가져오기
     * Default Date Format HHmm
     * @return dateTimeStr
     * */
    public static String getCurrentDitHi() {
        return LocalDateTime.now().format(getFormatter(DIGIT_HI));
    }

    /**
     * 현재 날짜 가져오기
     * 시간을 갖지 않는 날짜 format 으로 return 가능
     * @return dateTimeStr
     * */
    public static String getCurrentYmd(String dateFormatStr) {
        return LocalDate.now().format(getFormatter(dateFormatStr));
    }

    /**
     * 오늘 날짜 가져오기
     * Default DateTime Format yyyy-MM-dd HH:mm:ss
     * @return dateTimeStr
     * */
    public static String getToday() { //getNow, getCurrent
        return LocalDateTime.now().format(getFormatter(DEFAULT_YMD_HIS));
    }

    /**
     * 현재 날짜/시간 가져오기
     * Default DateTime Format yyyy-MM-dd HH:mm:ss
     * @return dateTimeStr
     * */
    public static String getCurrentYmdHis() {
        return LocalDateTime.now().format(getFormatter(DEFAULT_YMD_HIS));
    }

    /**
     * 현재 날짜/시간 가져오기
     * 날짜, 시간을 가지는 format 으로 return 가능
     * @return dateTimeStr
     * */
    public static String getCurrentYmdHis(String dateTimeFormatStr) {
        return LocalDateTime.now().format(getFormatter(dateTimeFormatStr));
    }

    /**
     * 현재 날짜에서 설정된 일만큼 플러스
     * Default Date Format yyyyMMdd
     * @return dateTimeStr
     * */
    public static String getCurrentDitYmdPlus(long i) {
        return LocalDate.now().plusDays(i).format(getFormatter(DIGIT_YMD));
    }

    /**
     * 현재 날짜에서 설정된 일만큼 마이너스
     * Default Date Format yyyyMMdd
     * @return dateTimeStr
     * */
    public static String getCurrentDitYmdMinus(long i) {
        return LocalDate.now().minusDays(i).format(getFormatter(DIGIT_YMD));
    }

}
