package kr.co.homeplus.claimbatch.claim.model.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(description = "클레임 발생시 재고 환원을 위한 DTO")
public class RestoreStockItemDto {
    @ApiModelProperty(value = "주문번호")
    private long purchaseOrderNo;
    @ApiModelProperty(value = "배송번호")
    private long bundleNo;
    @ApiModelProperty(value = "클레임배송번호")
    private long claimBundleNo;
    @ApiModelProperty(value = "재고환원아이템리스트")
    private List<RestoreStoreItemListDto> orderItemList;
    @ApiModelProperty(value = "고객번호")
    private long userNo;

    @Data
    public static class RestoreStoreItemListDto {
        private String itemNo;
        private int qty;
    }
}
