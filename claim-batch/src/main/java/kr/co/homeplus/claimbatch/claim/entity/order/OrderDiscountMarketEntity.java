package kr.co.homeplus.claimbatch.claim.entity.order;

public class OrderDiscountMarketEntity {
    // 마켓유형
    public String marketType;
    // 마켓주문번호
    public String marketOrderNo;
    // 마켓주문상품번호
    public String marketOrderItemNo;
    // 구매주문번호
    public String purchaseOrderNo;
    // 상품할인금액
    public String itemDiscountAmt;
    // 상품쿠폰할인금액
    public String itemCouponDiscountAmt;
    // 상품즉시할인금액
    public String itemImmDiscountAmt;
    // 상품복수구매할인금액
    public String itemMultiPurchaseDiscountAmt;
    // 판매자부담할인금액
    public String sellerChargeDiscountAmt;
    // 판매자부담쿠폰할인금액
    public String sellerChargeCouponDiscountAmt;
    // 판매자부담즉시할인금액
    public String sellerChargeImmDiscountAmt;
    // 판매자부담복수구매할인금액
    public String sellerChargeMultiPurchaseDiscountAmt;
    // 등록일시
    public String regDt;
    // 등록자ID
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정자ID
    public String chgId;
}
