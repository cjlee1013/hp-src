package kr.co.homeplus.claimbatch.pine.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "PINE 영수증 누락번호 관리(PINE 전송 claim_no 생성용)")
@Getter
@Setter
@EqualsAndHashCode
public class PineReceiptOmitClaimIssueDto {

    @ApiModelProperty(value = "점포ID", position = 1)
    private String storeId;

    @ApiModelProperty(value = "POS번호", position = 2)
    private String posNo;

    @ApiModelProperty(value = "거래일자(YYYYMMDD)", position = 3)
    private String tradeDt;

    @ApiModelProperty(value = "누락번호", position = 4)
    private int omitNo;

    @ApiModelProperty(value = "데이터 전송여부", position = 5)
    private int dataSendYn;

    @ApiModelProperty(value = "클레임번호(PINE 전송용)", position = 6)
    private String claimOmitNo;
}
