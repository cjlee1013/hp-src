package kr.co.homeplus.claimbatch.history.service;

import kr.co.homeplus.claimbatch.history.model.BatchHistoryDto;

/**
 * batch log insert, update
 */
public interface BatchHistoryService {

    String insertBatchHistory(BatchHistoryDto batchHistoryDto);

    int updateBatchHistory(BatchHistoryDto batchHistoryDto);

}
