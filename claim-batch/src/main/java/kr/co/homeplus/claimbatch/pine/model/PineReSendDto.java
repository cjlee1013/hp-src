package kr.co.homeplus.claimbatch.pine.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "PINE 클레임 재전송")
@Getter
@Setter
@EqualsAndHashCode
public class PineReSendDto {

    @ApiModelProperty(value = "클레임번호(재생성)", position = 1)
    private String claimNo;

    @ApiModelProperty(value = "주문상점번호", position = 2)
    private String purchaseStoreInfoNo;

    @ApiModelProperty(value = "클레임상점번호", position = 3)
    private String claimStoreInfoNo;

    @ApiModelProperty(value = "클레임상점번호", position = 4)
    private String shipClaimStoreInfoNo;

    @ApiModelProperty(value = "memo", position = 5)
    private String memo;

}
