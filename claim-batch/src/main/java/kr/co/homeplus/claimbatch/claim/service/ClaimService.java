package kr.co.homeplus.claimbatch.claim.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.claimbatch.claim.model.register.omni.OmniOriginOrderDto;
import kr.co.homeplus.claimbatch.message.enums.ClaimMessageInfo;
import kr.co.homeplus.claimbatch.util.MessageSendUtil;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimService {

    private final ClaimMapperService mapperService;
    private final ClaimCommonService commonService;
    private final ClaimRegisterService registerService;
    private final ClaimPaymentService paymentService;

    public void orderGiftAutoCancel(String processDt) {
        // 취소대상 거래 확인
        // 클레임 등록
        // ClaimRegSetDto 만들어야 함.
        List<LinkedHashMap<String, Object>> autoCancelList = mapperService.getPresentAutoCancelList(processDt);
        // 리스트가 없으면 종료처리.
        if(autoCancelList != null){
            // 거래건수 만큼 수행
            for(LinkedHashMap<String, Object> map : autoCancelList){
                try {
                    // 등록결과 리턴.
                    LinkedHashMap<String, Object> result = registerService.addClaimRegister(commonService.createClaimRegSetDto(map));
                    if (!this.modifyClaimStatus(result)) {
                        log.error("거래번호({}) - 클레임그룹번호({}) 상태 변경 실패", map.get("purchase_order_no"), result.get("claim_no"));
                        continue;
                    }
                    if(!paymentService.paymentCancel(ObjectUtils.toLong(result.get("claim_no")), "batch", Boolean.TRUE)){
                        log.error("거래번호({}) - 클레임그룹번호({}) 선물하기 결제 실패", map.get("purchase_order_no"), result.get("claim_no"));
                        continue;
                    }
                    // 메시지 발송
                    MessageSendUtil.sendPresentAutoCancel(ObjectUtils.toLong(result.get("claim_no")));

                    if(!mapperService.modifyPresentAutoCancelResult(ObjectUtils.toLong(map.get("purchase_order_no")))){
                        log.error("거래번호({}) - 클레임그룹번호({}) 선물취소상태 변경 실패", map.get("purchase_order_no"), result.get("claim_no"));
                    }
                } catch (Exception e){
                    log.error("거래번호({}) 선물받기 자동 취소 중 오류발생 :: {}", map.get("purchase_order_no"), e.getMessage());
                }
            }
        }
    }

    /**
     * 주문취소
     *
     * @param targetClaimNo
     */
    public void claimPaymentCancel(String targetClaimNo) {
        // 취소해야 할 거래 확인
        // 취소처리
        List<LinkedHashMap<String, Object>> claimList = this.getClaimNoList(targetClaimNo);
        log.info("PREV claim_payment_cancel : {}", claimList);
        List<LinkedHashMap<String, Object>> customClaimList = claimList.stream().filter(map -> ObjectUtils.toInt(map.get("pg_cancel_request_cnt")) < 4).collect(Collectors.toList());
        log.info("AFTER claim_payment_cancel : {}", customClaimList);
        this.claimPaymentCancelProcess(customClaimList, Boolean.TRUE);
    }

    public void claimPaymentRetryCancel(String targetClaimNo) {
        List<LinkedHashMap<String, Object>> claimList = this.getClaimNoList(targetClaimNo);
        this.claimPaymentCancelProcess(claimList, Boolean.FALSE);
    }

    public void claimPartnerPaymentCancel(String targetClaimNo) {
        List<LinkedHashMap<String, Object>> claimList = this.getClaimPartnerApprovalNoList();
        this.claimPaymentCancelProcess(claimList, Boolean.FALSE);
    }

    public void claimStatusWithdraw(String targetClaimNo, String passwd) {
        if("chongmoon.cho".equals(passwd)){
            if(targetClaimNo.contains("|")) {
                for(String str : targetClaimNo.split("\\|")){
                    if(!paymentService.setClaimStatus(Long.parseLong(str), "BATCH", "C4")){
                        log.error("클레임그룹번호 ({}) 철회 요청 실패", str);
                    } else {
                        log.error("클레임그룹번호 ({}) 철회 요청 성공", str);
                    }
                }
            } else {
                if(!paymentService.setClaimStatus(Long.parseLong(targetClaimNo), "BATCH", "C4")){
                    log.error("클레임그룹번호 ({}) 철회 요청 실패", targetClaimNo);
                } else {
                    log.error("클레임그룹번호 ({}) 철회 요청 성공", targetClaimNo);
                }
            }

        } else {
            log.error("해당 기능을 사용할 수 없는 사용자입니다.");
        }
    }

    private void claimPaymentCancelProcess(List<LinkedHashMap<String, Object>> claimList, boolean isPgCheck){
        if(claimList != null){
            log.info("클레임 결제취소 대상 건수 {}", claimList.size());
            for(LinkedHashMap<String, Object> map : claimList){
                long claimNo = ObjectUtils.toLong(map.get("claim_no"));
                long purchaseOrderNo = ObjectUtils.toLong(map.get("purchase_order_no"));
                log.info("클레임그룹번호({}) 결제취소 요청 정보 :::: {}", claimNo, map);
                try {
                    if(!paymentService.paymentCancel(claimNo, "batch", isPgCheck)){
                        log.error("클레임그룹번호({}) 결제취소 실패", claimNo);
                        continue;
                    }
                    if(ObjectUtils.isEquals("Y", map.get("order_gift_yn")) && ObjectUtils.isEqualsIgnoreCase("batch", map.get("request_id"))){
                        // 메시지 발송
                        MessageSendUtil.sendPresentAutoCancel(ObjectUtils.toLong(claimNo));
                        if(!mapperService.modifyPresentAutoCancelResult(purchaseOrderNo)){
                            log.error("거래번호({}) - 클레임그룹번호({}) 선물취소상태 변경 실패", map.get("purchase_order_no"), claimNo);
                        }
                    } else {
                        if(isOrderCanceling(ObjectUtils.toString(map.get("request_dt")),ObjectUtils.toString(map.get("approve_dt")))){
                            // 옴니 건 처리.
                            if(ObjectUtils.isEqualsIgnoreCase("Y", map.get("oos_yn"))){
//                                if(ObjectUtils.toLong(map.get("substitution_amt")) > 0){
//                                    MessageSendUtil.sendOmniSubstitution(claimNo);
//                                } else {
//                                    //결품
//                                    boolean isItemSubstitution = this.getOriginOrderItemList(purchaseOrderNo).stream().map(OmniOriginOrderDto::getSubstitutionYn).anyMatch(key -> key.equals("N"));
//                                    if(isItemSubstitution){
//                                        MessageSendUtil.sendOmniOutOfStock(claimNo, ClaimMessageInfo.CLAIM_MESSAGE_HOME_OOS_SUB);
//                                    } else {
//                                        MessageSendUtil.sendOmniOutOfStock(claimNo, ClaimMessageInfo.CLAIM_MESSAGE_HOME_OOS_NOT_SUB);
//                                    }
//                                }
                            } else {
                                MessageSendUtil.sendClaimCancel(claimNo);
                            }
                        } else {
                            if("C,R".contains(ObjectUtils.toString(map.get("claim_type")))){
                                MessageSendUtil.sendClaimCompleteWithoutExchange(claimNo);
                            } else {
                                MessageSendUtil.sendExchangeClaimComplete(claimNo);
                            }
                        }
                    }
                } catch (Exception e) {
                    log.error("클레임그룹번호({}) 결제취소 중 오류발생 :: {}", claimNo, e.getMessage());
                }
            }
        }
    }

    public void claimPaymentInputManual(String claimNo, String paymentAmt) {
        log.info("claimPaymentInputManual input data : claimNo : {} / paymentAmt : {} / paymentType : {}", claimNo, paymentAmt.split("\\|")[0], paymentAmt.split("\\|")[1]);
        String paymentType = paymentAmt.split("\\|")[1];
        LinkedHashMap<String, Object> paymentInfoMap = mapperService.getPaymentCancelManualInfo(Long.parseLong(claimNo), paymentType);
        paymentInfoMap.put("payment_amt", paymentAmt.split("\\|")[0]);
        log.info("claimPaymentInputManual input data : {}", paymentInfoMap);
        try {
            commonService.paymentCancelManual(paymentType, paymentInfoMap);
        } catch (Exception e) {
            log.error("claimPaymentInputManual error :: {}", e.getMessage());
        }
    }

    public void claimMinusRefundInfo(String claimNo) {
        try {
            List<Long> claimTargetList = mapperService.getClaimMinusRefundClaimList(ObjectUtils.toLong(claimNo));
            log.info("쿠폰 할인금액 재처리 대상 리스트 :: {}", claimTargetList);
            if(claimTargetList == null){
                throw new Exception("시럽쿠폰 처리 건이 없습니다.");
            }
            for(long targetClaimNo : claimTargetList){
                long claimMinusTargetAmt = mapperService.getClaimMinusRefundModifyAmt(targetClaimNo);
                LinkedHashMap<String, Object> preClaimMinusRefundMap = mapperService.getClaimMinusRefundPreInfo(targetClaimNo);
                if(registerService.isClaimMinusRefundPossible(claimMinusTargetAmt, preClaimMinusRefundMap)){
                    long totDiscountAmt = ObjectUtils.toLong(preClaimMinusRefundMap.get("tot_discount_amt"));
                    if(!mapperService.setClaimMinusDiscountModify(targetClaimNo, claimMinusTargetAmt, totDiscountAmt)){
                        log.info("클레임그룹번호({})에 대한 시럽쿠폰 처리에 실패하였습니다.", targetClaimNo);
                        continue;
                    }
                    mapperService.callByClaimAdditionReg(targetClaimNo);
                    this.claimPaymentCancel(String.valueOf(targetClaimNo));
//                    if(!paymentService.paymentCancel(targetClaimNo, "batch", Boolean.FALSE)){
//                        log.error("클레임그룹번호({}) 결제취소 실패", targetClaimNo);
//                        continue;
//                    }
                    // 마일리지 발행
                    if(!paymentService.claimMinusRefundMileage(preClaimMinusRefundMap, claimMinusTargetAmt)){
                        log.info("클레임그룹번호({})에 대한 마일리지대체 발생이 실패하였습니다.", targetClaimNo);
                    };
                }
            }
        } catch (Exception e){
            log.error("쿠폰 할인금액 재 처리 실패 :: {}", e.getMessage());
        }
    }

    // 실패건 재실행
    private boolean modifyClaimStatus(LinkedHashMap<String, Object> map){
        return paymentService.setClaimStatus(ObjectUtils.toLong(map.get("claim_no")), "batch", "C2");
    }

    // 원주문 상품리트슽 조회.
    private List<OmniOriginOrderDto> getOriginOrderItemList(long ordNo) {
        return mapperService.getOmniOriginOrderList(ordNo);
    }

    private List<LinkedHashMap<String, Object>> getClaimNoList(String claimNo){
        if(claimNo != null){
            if(claimNo.contains("|")){
                List<LinkedHashMap<String, Object>> claimList = new ArrayList<>();
                for(String str : claimNo.split("\\|")){
                    List<LinkedHashMap<String, Object>> resultList = mapperService.getClaimApprovalList(ObjectUtils.toLong(str));
                    if(resultList != null){
                        claimList.addAll(resultList);
                    }
                }
                return claimList;
            }
        }
        return mapperService.getClaimApprovalList(ObjectUtils.toLong(claimNo));
   }

    private List<LinkedHashMap<String, Object>> getClaimPartnerApprovalNoList(){
        List<LinkedHashMap<String, Object>> claimList = new ArrayList<>();
        List<Long> claimNoList = mapperService.getClaimPartnerApprovalList();
        for(Long str : claimNoList){
            List<LinkedHashMap<String, Object>> resultList = mapperService.getClaimApprovalList(str);
            if(resultList != null){
                claimList.addAll(resultList);
            }
        }
        return claimList;
    }

   private boolean isOrderCanceling(String requestDt, String approveDt) throws Exception {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date approvalDate = format.parse(approveDt);
            Date requestDate = format.parse(requestDt);
            return ((approvalDate.getTime() - requestDate.getTime())/1000) < 3 ? Boolean.TRUE : Boolean.FALSE;
        } catch (Exception e){
            log.error("전체취소,주무취소여부 판단 오류 ::: {}", e.getMessage());
            throw new Exception("전체취소,주무취소여부 판단 오류");
        }
   }

}
