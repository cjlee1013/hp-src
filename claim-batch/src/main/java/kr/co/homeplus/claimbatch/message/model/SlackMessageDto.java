package kr.co.homeplus.claimbatch.message.model;

import java.util.List;
import lombok.Data;

@Data
public class SlackMessageDto {
    List<SlackMessageSectionDto> blocks;

    @Data
    public static class SlackMessageSectionDto {
        String type;
        SlackMessageMarkDownDto text;
    }

    @Data
    public static class SlackMessageMarkDownDto {
        String type;
        String text;
    }
}
