package kr.co.homeplus.claimbatch.claim.service;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claimbatch.claim.enums.ClaimCode;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claimbatch.claim.model.payment.ClaimCouponReIssueSetDto.CouponReIssueDto;
import kr.co.homeplus.claimbatch.claim.model.payment.RestoreStockItemDto;
import kr.co.homeplus.claimbatch.claim.model.payment.RestoreStockItemDto.RestoreStoreItemListDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimAdditionRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimBundleRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimExchShippingRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimInfoRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimItemRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimPaymentRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimReqRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimShippingRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.omni.OmniOriginOrderDto;
import kr.co.homeplus.claimbatch.claim.model.register.safety.ClaimSafetySetDto;
import kr.co.homeplus.claimbatch.claim.model.register.ticket.ClaimTicketModifyDto;
import kr.co.homeplus.claimbatch.claim.model.register.ticket.ClaimTicketRegDto;

public interface ClaimMapperService {

    /**
     * 환불 예정 금액 조회
     *
     * @param calcSetDto 환불 예정 금액 조회 파라 미터
     * @return ClaimPreRefundCalcGetDto 환불 예정 금액 정보
     * @throws Exception 오류 처리
     */
    ClaimPreRefundCalcGetDto getClaimPreRefundCalculation(ClaimPreRefundCalcSetDto calcSetDto) throws Exception;

    /**
     * 환불 예정 금액 조회(마켓)
     *
     * @param calcSetDto 환불 예정 금액 조회 파라 미터
     * @return ClaimPreRefundCalcGetDto 환불 예정 금액 정보
     * @throws Exception 오류 처리
     */
    ClaimPreRefundCalcGetDto getClaimPreRefundCalculationByMarket(ClaimPreRefundCalcSetDto calcSetDto) throws Exception;

    /**
     * 클레임 번들 생성을 위한 데이터 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return 클레임번들 기본정보
     */
    LinkedHashMap<String, Object> getCreateClaimBundleInfo(long purchaseOrderNo, long bundleNo);

    /**
     * 클레임 아이템 생성을 위한 데이터 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param orderItemNo 상품주문번호
     * @return HashMap<String, Object> 클레임 아이템 기본정보
     */
    LinkedHashMap<String, Object> getClaimItemInfo(long purchaseOrderNo, long orderItemNo);

    /**
     * 클레임 마스터 등록
     *
     * @param claimMstRegDto 등록 데이터
     * @return 성공 유무
     */
    Boolean addClaimMstInfo(ClaimMstRegDto claimMstRegDto);

    /**
     * 클레임 번들 등록
     *
     * @param claimBundleRegDto 클레임 번들 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    Boolean addClaimBundleInfo(ClaimBundleRegDto claimBundleRegDto);

    /**
     * 클레임 요청 등록
     *
     * @param claimReqRegDto 클레임 요청 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    Boolean addClaimReqInfo(ClaimReqRegDto claimReqRegDto);

    /**
     * 클레임 아이템 정보 등록
     *
     * @param claimItemRegDto 클레임 아이템 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    Boolean addClaimItemInfo(ClaimItemRegDto claimItemRegDto);

    /**
     * 클레임 결제 취소를 위한 원주문 결제 정보 조회
     *
     * @param paymentNo 결제 번호
     * @param paymentType 결제 타입
     * @return 클레임 결제 취소를 위한 원주문 결제 정보 맵
     */
    LinkedHashMap<String, Object> getOriPaymentInfoForClaim(long paymentNo, String paymentType);

    /**
     * 클레임 결제 취소 정보 등록
     *
     * @param claimPaymentRegDto 클레임 결제 취소 정보
     * @return Boolean 등록 성공 유무
     */
    Boolean addClaimPaymentInfo(ClaimPaymentRegDto claimPaymentRegDto);

    /**
     * 클레임 아이템 옵션별 등록을 위한 데이터 조회
     *
     * @param orderItemNo 아이템주문번호
     * @param orderOptNo 옵션번호
     * @return 클레임 옵션 등록 정보
     */
    ClaimOptRegDto getOriginOrderOptInfoForClaim(long orderItemNo, String orderOptNo);

    /**
     * 클레임 옵션별 등록
     *
     * @param claimOptRegDto 클레임 옵션별 데이터 DTO
     * @return Boolean 등록 성공 유무
     */
    Boolean addClaimOptInfo(ClaimOptRegDto claimOptRegDto);

    /**
     * 클레임 - 결제취소를 위한 데이터 조회
     *
     * @param claimNo 클레임 번호
     * @return LinkedHashMap<String, Object> 결제취소용 데이터
     */
    List<LinkedHashMap<String, Object>> getPaymentCancelInfo(long claimNo);


    LinkedHashMap<String, Object> getPaymentCancelManualInfo(long claimNo, String paymentType);

    /**
     * 클레임 - 클레임상태 변경 업데이트
     *
     *
     * @param parameter@return Boolean 클레임상태변경 업데이트 결과
     */
    Boolean setClaimStatus(LinkedHashMap<String, Object> parameter);

    /**
     * 클레임 - 클레임 결제 취소 상태 업데이트
     *
     * @param claimPaymentNo 클레임 페이먼트 번호
     * @param claimCode 클레임 코드
     * @return Boolean 클레임 결제취소상태 업데이트 결과
     */
    Boolean setClaimPaymentStatus(long claimPaymentNo, String claimApprovalCd, ClaimCode claimCode, String isLastYn);

    /**
     * 클레임 - 클레임 마지막 작업
     *
     * @param claimInfoRegDto
     * @return
     */
    void callByEndClaimFlag(ClaimInfoRegDto claimInfoRegDto);

    /**
     * 클레임 - 전체주문취소에 대한 원 배송번호 취득
     * @param purchaseOrderNo 주문번호
     * @return 배송번호 정보
     */
    List<LinkedHashMap<String, Object>> getOrderBundleByClaim(long purchaseOrderNo);

    /**
     * 클레임 - 환불예정금액 조회 가능 데이터 취득
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return 환불예정금액 조회 가능 데이터
     */
    List<ClaimPreRefundItemList> getClaimPreRefundItemInfo(long purchaseOrderNo, long bundleNo);

    /**
     * 클레임 - 환불예정금액 조회 가능 데이터 취득
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return 환불예정금액 조회 가능 데이터
     */
    List<ClaimPreRefundItemList> getClaimPreRefundItemInfoByMarket(long purchaseOrderNo, long bundleNo, String marketOrderItemNo);

    /**
     * 클레임 취소신청시 부분취소 여부 확인
     *
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return 주문건수, 클레임 건수
     */
    LinkedHashMap<String, Object> getOrderByClaimPartInfo(long purchaseOrderNo, long bundleNo);

    /**
     * addition data
     * @param claimNo 클레임번호
     */
    void callByClaimAdditionReg(long claimNo);

    /**
     * 아이템별 임직원할인금액 조회
     * @param claimNo 클레임번호
     * @param orderItemNo 아이템주문번호
     */
    void callByClaimItemEmpDiscount(long claimNo, long orderItemNo);

    /**
     * 클레임 번들 생성을 위한 데이터 조회
     *
     * @param purchaseOrderNo 주문번호
     * @return 클레임번들 기본정보
     */
    LinkedHashMap<String, Object> getCreateClaimMstInfo(long purchaseOrderNo);

    /**
     * 클레임 가능 여부 판단을 위한 데이터 조회
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return 주문데이터
     */
    LinkedHashMap<String, Object> getClaimPossibleCheck(long purchaseOrderNo, long bundleNo);

    /**
     * 클레임 성공시 기존 쿠폰 재발행
     * @param claimNo 클레임번호
     * @return
     */
    List<CouponReIssueDto> getClaimCouponReIssueInfo(long claimNo, long userNo);

    /**
     * 클레임 마무리 전 클레임데이터 조회
     * @param claimNo 클레임번호
     * @return
     */
    List<LinkedHashMap<String, Object>> getClaimInfo(long claimNo);

    /**
     * 쿠폰재발행 결과 업데이트
     * @param claimAdditionNo additionNo
     * @return
     */
    Boolean modifyClaimCouponReIssueResult(List<Long> claimAdditionNo);

    /**
     * 슬롯마감결과 업데이트
     * @param claimBundleNo 배송번호
     * @return
     */
    Boolean modifyClaimBundleSlotResult(long claimBundleNo);

    /**
     * claim_info insert 를 위한 데이터 조회
     * @param claimNo 클레임번호
     * @return
     */
    ClaimInfoRegDto getClaimRegData(long claimNo);


    /**
     * OCB 취소 거래번호 업데이트
     * @param claimPaymentNo
     * @param pgMid
     * @return
     */
    Boolean modifyClaimPaymentOcbTransactionId(long claimPaymentNo, String pgMid);

    /**
     * PG 취소 거래번호 업데이트
     *
     * @param claimPaymentNo 취소거래번호
     * @param cancelTradeId PG취소거래번호
     * @return 결과
     */
    Boolean modifyClaimPaymentPgCancelTradeId(long claimPaymentNo, String cancelTradeId);

    /**
     * 티켓 취소를 위한 티켓정보 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param orderItemNo 상품주문번호
     * @return 티켓정보
     */
    List<LinkedHashMap<String, Object>> getOrderTicketInfo(long purchaseOrderNo, long orderItemNo, int cancelCnt);

    /**
     * 티켓 클레임 insert
     *
     * @param claimTicketRegDto 티켓클레임 저장 정보 DTO
     * @return 티켓클레임저장결과
     */
    Boolean addClaimTicketInfo(ClaimTicketRegDto claimTicketRegDto);

    /**
     * 티켓 취소 처리정보 조회
     *
     * @param claimNo 클레임번호
     * @return 티켓정보
     */
    List<LinkedHashMap<String, Object>> getClaimTicketInfo(long claimNo);

    /**
     * 클레임 주문 타입 체크
     *
     * @param claimNo 클레임 번호
     * @return
     */
    LinkedHashMap<String, String> getClaimOrderTypeCheck(long claimNo);

    /**
     * 티켓 정보 업데이트
     *
     * @param modifyDto 업데이트 정보
     * @return
     */
    Boolean modifyOrderTicketInfo(ClaimTicketModifyDto modifyDto);

    /**
     * 클레임 - 클레임 결제 취소 상태 업데이트
     *
     * @param claimNo 클레임 번호
     * @param claimCode 클레임 코드
     * @return Boolean 클레임 결제취소상태 업데이트 결과
     */
    Boolean setTotalClaimPaymentStatus(long claimNo, ClaimCode claimCode);

    /**
     * MHC 적립취소 생성데이터
     *
     * @param claimNo 클레임번호
     * @return
     */
    List<LinkedHashMap<String, Object>> getCreateClaimAccumulateInfo(long claimNo);

    /**
     * MHC 적립취소 금액계산을 위한 데이터 조회
     *
     * @param purchaseOrderNo 주문번호
     * @return
     */
    LinkedHashMap<String, Object> getAccumulateCancelInfo(long purchaseOrderNo, String pointKind);

    /**
     * 주문적립취소 등록
     *
     * @param parameterMap 파라미터정보
     * @return
     */
    Boolean addClaimAccumulate(LinkedHashMap<String, Object> parameterMap);

    /**
     * 주문적립취소 전송상태 업데이트
     *
     * @param claimNo 클레임번호
     * @param reqSendYn 전송상태
     * @return
     */
    Boolean modifyClaimAccumulateReqSend(long claimNo, String reqSendYn, String pointKind);

    /**
     * 주문적립취소 등록 결과 업데이트
     *
     * @param parameterMap 파라미터정보
     * @return
     */
    Boolean modifyClaimAccumulateReqResult(LinkedHashMap<String, Object> parameterMap);

    /**
     * 합배송이 있는 주문의 클레임 건수를 가지고 온다.
     *
     * @param purchaseOrderNo 주문번호
     * @return
     */
    List<LinkedHashMap<String, Object>> getOrderCombineClaimCheck(String purchaseOrderNo, String type);

    /**
     * 결제취소 전 환불예정금액 재조회
     *
     * @param claimNo 클레임번호
     * @return 환불예정금액 조회 파라미터
     */
    ClaimPreRefundCalcSetDto getPreRefundReCheckData(long claimNo);

    /**
     * 결제취소 전 환불예정금 재조회를 위한 아이템정보
     *
     * @param claimNo 클레임그룹번호
     * @return 아이템정보
     */
    List<ClaimPreRefundItemList> getPreRefundItemData(long claimNo);

    /**
     * 환불예정금 비교 데이터
     * (기존 등록 데이터)
     *
     * @param claimNo 클레임그룹번호
     * @return 환불금등록데이터
     */
    ClaimMstRegDto getPreRefundCompareData(long claimNo);

    /**
     * 환불예정금액 재조회
     *
     * @param calcSetDto 환불예정금액 조회 파라미터
     * @return 환불예정금재조회결과
     * @throws Exception 오류시 오류처리.
     */
    ClaimPreRefundCalcGetDto getClaimPreRefundReCheckCalculation(ClaimPreRefundCalcSetDto calcSetDto) throws Exception;

    /**
     * 클레임마스터 금액 업데이트
     *
     * @param claimNo 클레임그룹번호
     * @param column 컬럼명
     * @param amt 금액
     * @return 업데이트결과
     */
    boolean modifyClaimMstPaymentAmt(long claimNo, String column, long amt);

    /**
     * 클레임결제취소금액 업데이트
     *
     * @param claimNo 클레임그룹번호
     * @param paymentType 결제취소타입
     * @param paymentAmt 결제취소금액
     * @return 업데이트결과
     */
    boolean modifyClaimPaymentAmt(long claimNo, String paymentType, long paymentAmt);

    /**
     * 선물하기 취소 데이터 조회
     * 입력받은 데이터 기준으로 처리.
     *
     * @param processDt 실행일
     * @return 취소데이터 리스트
     */
    List<LinkedHashMap<String, Object>> getPresentAutoCancelList(String processDt);

    /**
     * 자동취소 결과 업데이트
     *
     * @param purchaseOrderNo 주문번호
     * @return 결과
     */
    boolean modifyPresentAutoCancelResult(long purchaseOrderNo);

    /**
     * 클레임 승인 건 조회
     *
     * @param claimNo 클레임그룹번호
     * @return 클레임그룹번호 리스트
     */
    List<LinkedHashMap<String, Object>> getClaimApprovalList(long claimNo);

    /**
     * 클레임 등록된 TD번들 정보
     *
     * @param claimNo
     * @return
     */
    LinkedHashMap<String, String> getClaimTdBundleInfo(long claimNo);

    /**
     * 마일리지 페이백 여부 체크
     *
     * @param bundleNo
     * @return
     */
    LinkedHashMap<String, Object> getMileagePaybackCheck(long bundleNo);

    /**
     * 페이백 대상 금액
     *
     * @param purchaseOrderNo
     * @return
     */
    LinkedHashMap<String, Object> getOrderCombinePaybackCheck(long purchaseOrderNo);

    /**
     * 페이백 클레임 금액
     *
     * @param bundleNo
     * @param orgBundleNo
     * @return
     */
    LinkedHashMap<String, Object> getClaimCombinePaybackCheck(long bundleNo, long orgBundleNo);

    /**
     * 배송비 페이백 결과 업데이트
     *
     * @param paybackNo
     * @return
     */
    boolean setShipPaybackResult(long paybackNo);

    /**
     * 교환/반품시 자동수거대상여부 확인.
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return
     */
    LinkedHashMap<String, Object> getClaimPickIsAutoInfo(long purchaseOrderNo, long bundleNo);

    /**
     * 클레임 반품(수거) 정보 입력
     *
     * @param shippingRegDto 입력데이터
     * @return 입력결과
     */
    Boolean addClaimPickShippingInfo(ClaimShippingRegDto shippingRegDto);

    /**
     * 클레임 교환 정보 입력
     *
     * @param exchShippingRegDto 입력데이터
     * @return 입력결과
     */
    Boolean addClaimExchShipping(ClaimExchShippingRegDto exchShippingRegDto);

    /**
     * 클레임 안심번호 발행 정보
     * @param claimBundleNo 클레임번들번호
     * @param issueType 발행타입
     * @return
     */
    ClaimSafetySetDto getClaimSafetyData(long claimBundleNo, String issueType);

    /**
     * 옴니 시 원주문 정보 조회
     *
     * @param purchaseOrderNo
     * @return
     */
    List<OmniOriginOrderDto> getOmniOriginOrderList(long purchaseOrderNo);

    List<RestoreStockItemDto> getRestoreStockItemInfo(long claimNo);

    List<RestoreStoreItemListDto> getRestoreStockItemList(long claimBundleNo);

    List<Long> getClaimMinusRefundClaimList(long claimNo);

    Long getClaimMinusRefundModifyAmt(long claimNo);

    boolean setClaimMinusDiscountModify(long claimNo, long correctCouponAmt, long totDiscountAmt);

    boolean marketClaimAmountModify(long claimNo, long paymentAmt, long claimDeliveryFeeDemandAmount, long shipAmt, long promoDiscountAmt);

    boolean addClaimAdditionByMarketShipAmt(ClaimAdditionRegDto claimAdditionRegDto);

    boolean marketClaimAddShipAmtModify(long claimNo, long diffAmt);

    int isClaimAdditionByAddShip(long claimNo);

    List<LinkedHashMap<String, Object>> getClaimAdditionAddDataByMarketAddShip(long claimNo);

    LinkedHashMap<String, Object> getMarketClaimPrevShipAmt(long purchaseOrderNo);

    LinkedHashMap<String, Object> getMarketClaimAmountInfo(long claimNo);

    LinkedHashMap<String, Object> getOrderDiscountMarketPrice(String marketOrderNo);

    LinkedHashMap<String, Object> getPrevMarketClaimDiscount(String marketOrderNo, long claimNo);

    boolean marketClaimBundleEncloseModify(long claimNo);

    LinkedHashMap<String, Object> getClaimMinusRefundPreInfo(long claimNo);

    List<Long> getClaimPartnerApprovalList();
}
