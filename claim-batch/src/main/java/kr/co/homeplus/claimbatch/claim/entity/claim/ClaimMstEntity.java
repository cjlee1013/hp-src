package kr.co.homeplus.claimbatch.claim.entity.claim;

public class ClaimMstEntity {
    // 클레임번호(환불:클레임번호)
    public String claimNo;
    // 결제번호
    public String paymentNo;
    // 주문번호
    public String purchaseOrderNo;
    // 사이트유형(HOME:홈플러스,CLUB:더클럽)
    public String siteType;
    // 완료 금액(전체환불금액)
    public String completeAmt;
    // 총상품금액
    public String totItemPrice;
    // 총배송가격
    public String totShipPrice;
    // 총추가배송가격(도서산간배송비)
    public String totIslandShipPrice;
    // 총배송가격(총 배송비 가격:고객지불금액)
    public String totAddShipPrice;
    // 총추가배송가격(총 도서산간 추가 배송비:고객지불금액)
    public String totAddIslandShipPrice;
    // 총할인금액
    public String totDiscountAmt;
    // 총상품할인금액
    public String totItemDiscountAmt;
    // 총배송비할인금액
    public String totShipDiscountAmt;
    // 총 임직원할인금액
    public String totEmpDiscountAmt;
    // 총장바구니할인금액
    public String totCartDiscountAmt;
    // 총행사 할인금액
    public String totPromoDiscountAmt;
    // 총카드할인 금액
    public String totCardDiscountAmt;
    // PG결제금액
    public String pgAmt;
    // 마일리지결제금액
    public String mileageAmt;
    // MHC결제금액
    public String mhcAmt;
    // DGV결제금액
    public String dgvAmt;
    // OCB결제금액
    public String ocbAmt;
    // 클레임 귀책자(B:Buyer, S:Seller, H:Homeplus)
    public String whoReason;
    // 고객번호
    public String userNo;
    // 결품여부
    public String oosYn;
    // 부분취소여부 ( Y : 부분취소, N : 전체취소)
    public String claimPartYn;
    // 임직원번호
    public String empNo;
    // 임직원할인한도잔액
    public String empRemainAmt;
    // 등록일
    public String regDt;
    // 총추가중복TD할인금액
    public String totAddTdDiscountAmt;
    // 총추가중복DS할인금액
    public String totAddDsDiscountAmt;
    // 회원등급번호
    public String gradeSeq;
    // 마켓연동주문번호
    public String marketOrderNo;
    // 마켓유형
    public String marketType;
}