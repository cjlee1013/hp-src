package kr.co.homeplus.claimbatch.claim.maket.entity;

public class MarketClaimInfoEntity {
    // 마켓클레임배송정보번호(반품/교환)
    public String marketClaimInfoNo;
    // 마켓클레임번호
    public String marketClaimNo;
    // 마켓클레임번호
    public String marketClaimItemNo;
    // 업체주문상품번호
    public String marketOrderItemNo;
    // 클레임처리상태코드
    public String claimStatus;
    // 클레임요청일
    public String claimRequestDate;
    // 접수채널
    public String requestChannel;
    // 클레임사유코드
    public String claimReason;
    // 반품상세사유
    public String claimDetailReason;
    // 취소완료일
    public String cancelCompletedDate;
    // 취소승인일
    public String cancelApprovalDate;
    // 보류상태코드
    public String holdbackStatus;
    // 보류사유코드
    public String holdbackReason;
    // 보류상세사유
    public String holdbackDetailReason;
    // 수거지기본주소
    public String collectBaseAddress;
    // 수거지상세주소
    public String collectDetailAddress;
    // 수거지우편번호
    public String collectZipcode;
    // 수거자이름
    public String collectName;
    // 수거지연락처1
    public String collectTelNo1;
    // 수거지연락처2
    public String collectTelNo2;
    // 수취지기본주소
    public String returnBaseAddress;
    // 수취지상세주소
    public String returnDetailAddress;
    // 수취지우편번호
    public String returnZipcode;
    // 수취자이름
    public String returnName;
    // 수취지연락처1
    public String returnTelNo1;
    // 수취지연락처2
    public String returnTelNo2;
    // 수거상태
    public String collectStatus;
    // 수거방법코드
    public String collectDeliveryMethod;
    // 수거택배사코드
    public String collectDeliveryCompany;
    // 수거송장번호
    public String collectTrackingNumber;
    // 수거완료일
    public String collectCompletedDate;
    // 배송비청구액
    public String claimDeliveryFeeDemandAmount;
    // 배송비결제방법
    public String claimDeliveryFeePayMethod;
    // 배송비결제수단
    public String claimDeliveryFeePayMeans;
    // 기타비용청구액
    public String etcFeeDemandAmount;
    // 기타비용결제방법
    public String etcFeePayMethod;
    // 기타비용결제수단
    public String etcFeePayMeans;
    // 환불대기상태
    public String refundStandbyStatus;
    // 환불대기사유
    public String refundStandbyReason;
    // 환불예정일
    public String refundExpectedDate;
    // 반품완료일
    public String returnCompletedDate;
    // 보류설정일
    public String holdbackConfigDate;
    // 보류설정자(구매자,판매자,관리자,시스템)
    public String holdbackConfigurer;
    // 보류해제일
    public String holdbackReleaseDate;
    // 보류해제자(구매자,판매자,관리자,시스템)
    public String holdbackReleaser;
    // 배송비묶음청구상품주문번호(다중일시쉼표구분)
    public String claimDeliveryFeeProductOrderIds;
    // 배송비할인액
    public String claimDeliveryFeeDiscountAmount;
    // 재배송상태
    public String reDeliveryStatus;
    // 재배송방법코드
    public String reDeliveryMethod;
    // 재배송택배사코드
    public String reDeliveryCompany;
    // 재배송송장번호
    public String reDeliveryTrackingNumber;
    // 재배송처리일
    public String reDeliveryOperationDate;
    // 등록일
    public String regDt;
    // 등록자ID
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정자ID
    public String chgId;
}
