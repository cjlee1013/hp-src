package kr.co.homeplus.claimbatch.claim.entity.multi;

public class MultiBundleEntity {
    // 다중번들번호
    public String multiBundleNo;
    // 구매주문번호
    public String purchaseOrderNo;
    // 번들번호
    public String bundleNo;
    // 원천점포ID(매출용실제점포)
    public String originStoreId;
    // 점포ID(주문서에서 넘어온 가상점포ID)
    public String storeId;
    // 배송정책번호
    public String shipPolicyNo;
    // 배송가격(고객 결제금액)
    public String shipPrice;
    // 도서산간배송가격(고객 결제금액)
    public String islandShipPrice;
    // 배송가능지역(상품 배송가능지역 ALL:전국, EXCLUDE:제주/도서산간 제외)
    public String shipArea;
    // 배송가능지역유형(고객 주소지 배송가능지역 유형)
    public String shipAreaType;
    // 배송유형(ITEM:상품별배송,BUNDLE:번들배송)
    public String shipType;
    // 배송종류(FREE:무료,FIXED:유료,COND:번들배송)
    public String shipKind;
    // 배송방법(DS_DLV:택배배송,DS_DRCT:직접배송,DS_PICK:방문수령/픽업,DS_POST:우편배송,DS_QUICK:퀵배송,TD_DLV:택배배송,TD_DRCT:직접배송,TD_PICK:방문수령,TD_POST:우편배송,TD_QUICK:퀵배송)
    public String shipMethod;
    // 배송정책비(일반배송상품 배송비 기준)
    public String shipFee;
    // 도서산간배송정책비(도서산간 제주권역 추가배송비 기준)
    public String islandShipFee;
    // 무료조건비
    public String freeCondition;
    // 클레임배송정책비
    public String claimShipFee;
    // 등록일시
    public String regDt;
    // 등록자ID
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정자ID
    public String chgId;

}
