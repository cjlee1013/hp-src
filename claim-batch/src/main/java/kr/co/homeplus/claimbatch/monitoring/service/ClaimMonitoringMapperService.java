package kr.co.homeplus.claimbatch.monitoring.service;

import kr.co.homeplus.claimbatch.monitoring.model.ClaimPaymentMonitoringDto;

public interface ClaimMonitoringMapperService {
    ClaimPaymentMonitoringDto getMonitoringData();
}
