package kr.co.homeplus.claimbatch.claim.sql;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claimbatch.claim.constants.CustomConstants;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimAccumulateEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimAdditionEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimExchShippingEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimMstEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimOptEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimPaymentEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimPickShippingEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimReqEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.BundleEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimInfoEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderOptEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.PaymentInfoEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.PurchaseAccumulateEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claimbatch.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claimbatch.claim.enums.ClaimCode;
import kr.co.homeplus.claimbatch.claim.enums.MysqlFunction;
import kr.co.homeplus.claimbatch.claim.factory.EntityFactory;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimBundleRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimExchShippingRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimInfoRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimItemRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimPaymentRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimReqRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimShippingRegDto;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import kr.co.homeplus.claimbatch.util.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ClaimManagementSql {

    /**
     * 클레임 마스터 저장
     *
     * @param claimMstRegDto 클레임 마스터 DTO
     * @return 클레임 마스터 저장 쿼리.
     */
    public static String insertClaimMst(ClaimMstRegDto claimMstRegDto){
        String[] claimMstArray = EntityFactory.getColumnInfo(ClaimMstEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(ClaimMstEntity.class))
            .INTO_COLUMNS(Arrays.copyOfRange(claimMstArray, 1, claimMstArray.length))
            .INTO_VALUES(
                SqlUtils.convertInsertParamToCamelCase(Arrays.copyOfRange(claimMstArray, 1, claimMstArray.length) )
            );

        log.debug("insertClaimMst :: \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 클레임 번들 저장.
     *
     * @param claimBundleRegDto 클레임 번들 DTO
     * @return 클레임 번들 저장 쿼리.
     */
    public static String insertClaimBundle(ClaimBundleRegDto claimBundleRegDto){
        String[] claimBundleArray = EntityFactory.getColumnInfo(ClaimBundleEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(ClaimBundleEntity.class))
            .INTO_COLUMNS(Arrays.copyOfRange(claimBundleArray, 1, claimBundleArray.length))
            .INTO_VALUES(
                SqlUtils.convertInsertParamToCamelCase(Arrays.copyOfRange(claimBundleArray, 1, claimBundleArray.length) )
            );

        log.debug("insertClaimBundle :: \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 클레임 요청 저장
     *
     * @param claimReqRegDto 클레임 요청 DTO
     * @return 클레임 요청 저장 쿼리
     */
    public static String insertClaimReq(ClaimReqRegDto claimReqRegDto){
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(ClaimReqEntity.class))
            .INTO_COLUMNS(
                claimReq.claimBundleNo, claimReq.claimNo, claimReq.claimReasonType, claimReq.claimReasonDetail,
                claimReq.uploadFileName, claimReq.uploadFileName2, claimReq.uploadFileName3,
                claimReq.requestId, claimReq.requestDt, claimReq.regDt, claimReq.chgDt
            )
            .INTO_VALUES(
                String.valueOf(claimReqRegDto.getClaimBundleNo()),
                String.valueOf(claimReqRegDto.getClaimNo()),
                SqlUtils.appendSingleQuote(claimReqRegDto.getClaimReasonType()),
                SqlUtils.appendSingleQuote(claimReqRegDto.getClaimReasonDetail()),
                SqlUtils.appendSingleQuote(claimReqRegDto.getUploadFileName()),
                SqlUtils.appendSingleQuote(claimReqRegDto.getUploadFileName2()),
                SqlUtils.appendSingleQuote(claimReqRegDto.getUploadFileName3()),
                SqlUtils.appendSingleQuote(claimReqRegDto.getRequestId()),
                claimReqRegDto.getRequestDt(),
                "NOW()",
                "NOW()"
            );

        log.debug("insertClaimReq :: \n[{}]", query.toString());
        return query.toString();
    }


    /**
     * 클레임 아이템 저장.
     *
     * @param claimItemRegDto 클레임 아이템 DTO
     * @return 클레임 아이템 저장 쿼리.
     */
    public static String insertClaimItemReq(ClaimItemRegDto claimItemRegDto){
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(ClaimItemEntity.class))
            .INTO_COLUMNS(claimItem.claimNo, claimItem.claimBundleNo, claimItem.paymentNo,
                claimItem.purchaseOrderNo, claimItem.bundleNo, claimItem.originStoreId, claimItem.storeId,
                claimItem.fcStoreId, claimItem.orderItemNo, claimItem.itemNo, claimItem.itemName,
                claimItem.itemPrice, claimItem.claimItemQty, claimItem.storeType, claimItem.mallType,
                claimItem.taxYn, claimItem.partnerId, claimItem.empDiscountRate, claimItem.empDiscountAmt,
                claimItem.regId, claimItem.regDt, claimItem.marketOrderNo)
            .INTO_VALUES(
                String.valueOf(claimItemRegDto.getClaimNo()),
                String.valueOf(claimItemRegDto.getClaimBundleNo()),
                String.valueOf(claimItemRegDto.getPaymentNo()),
                String.valueOf(claimItemRegDto.getPurchaseOrderNo()),
                String.valueOf(claimItemRegDto.getBundleNo()),
                String.valueOf(claimItemRegDto.getOriginStoreId()),
                String.valueOf(claimItemRegDto.getStoreId()),
                String.valueOf(claimItemRegDto.getFcStoreId()),
                String.valueOf(claimItemRegDto.getOrderItemNo()),
                SqlUtils.appendSingleQuote(claimItemRegDto.getItemNo()),
                SqlUtils.appendSingleQuote(claimItemRegDto.getItemName()),
                String.valueOf(claimItemRegDto.getItemPrice()),
                String.valueOf(claimItemRegDto.getClaimItemQty()),
                SqlUtils.appendSingleQuote(claimItemRegDto.getStoreType()),
                SqlUtils.appendSingleQuote(claimItemRegDto.getMallType()),
                SqlUtils.appendSingleQuote(claimItemRegDto.getTaxYn()),
                SqlUtils.appendSingleQuote(claimItemRegDto.getPartnerId()),
                String.valueOf(claimItemRegDto.getEmpDiscountRate()),
                String.valueOf(claimItemRegDto.getEmpDiscountAmt()),
                SqlUtils.appendSingleQuote(claimItemRegDto.getRegId()),
                "NOW()",
                SqlUtils.appendSingleQuote(claimItemRegDto.getMarketOrderNo())
            );

        log.debug("insertClaimBundle :: \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 클레임 결제취소 데이터 저장
     *
     * @param claimPaymentRegDto 클레임 결제취소 데이터 DTO
     * @return 클레임 결제 취소 데이터 저장 쿼리.
     */
    public static String insertClaimPayment(ClaimPaymentRegDto claimPaymentRegDto){
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(ClaimPaymentEntity.class))
            .INTO_COLUMNS(claimPayment.claimNo, claimPayment.paymentNo, claimPayment.purchaseOrderNo, claimPayment.pgKind,
                claimPayment.paymentType, claimPayment.parentPaymentMethod, claimPayment.methodCd, claimPayment.easyMethodCd,
                claimPayment.pgMid, claimPayment.pgOid, claimPayment.paymentTradeId, claimPayment.originApprovalCd,
                claimPayment.paymentStatus, claimPayment.paymentAmt, claimPayment.pgCancelRequestCnt, claimPayment.payToken,
                claimPayment.regDt, claimPayment.regId, claimPayment.chgDt, claimPayment.chgId)
            .INTO_VALUES(
                String.valueOf(claimPaymentRegDto.getClaimNo()),
                String.valueOf(claimPaymentRegDto.getPaymentNo()),
                String.valueOf(claimPaymentRegDto.getPurchaseOrderNo()),
                SqlUtils.appendSingleQuote(claimPaymentRegDto.getPgKind()),
                SqlUtils.appendSingleQuote(claimPaymentRegDto.getPaymentType()),
                SqlUtils.appendSingleQuote(claimPaymentRegDto.getParentPaymentMethod()),
                SqlUtils.appendSingleQuote(claimPaymentRegDto.getMethodCd()),
                SqlUtils.appendSingleQuote(claimPaymentRegDto.getEasyMethodCd()),
                SqlUtils.appendSingleQuote(claimPaymentRegDto.getPgMid()),
                SqlUtils.appendSingleQuote(claimPaymentRegDto.getPgOid()),
                SqlUtils.appendSingleQuote(claimPaymentRegDto.getPaymentTradeId()),
                SqlUtils.appendSingleQuote(claimPaymentRegDto.getOriginApprovalCd()),
                SqlUtils.appendSingleQuote(claimPaymentRegDto.getPaymentStatus()),
                String.valueOf(claimPaymentRegDto.getPaymentAmt()),
                String.valueOf(claimPaymentRegDto.getPgCancelRequestCnt()),
                SqlUtils.appendSingleQuote(claimPaymentRegDto.getPayToken()),
                "NOW()",
                SqlUtils.appendSingleQuote(claimPaymentRegDto.getRegId()),
                "NOW()",
                SqlUtils.appendSingleQuote(claimPaymentRegDto.getRegId())
            );

        log.debug("insertClaimPayment :: \n[{}]", query.toString());
        return query.toString();
    }
    /**
     * 클레임 아이템 옵션 저장
     *
     * @param claimOptRegDto 클레임 아이템 옵션 DTO
     * @return 클레임 아이템 옵션 저장 쿼리.
     */
    public static String insertClaimOpt(ClaimOptRegDto claimOptRegDto){
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(ClaimOptEntity.class))
            .INTO_COLUMNS(claimOpt.claimNo, claimOpt.claimBundleNo, claimOpt.claimItemNo,
                claimOpt.orderOptNo, claimOpt.optNo, claimOpt.optPrice, claimOpt.claimOptQty, claimOpt.regDt,
                claimOpt.marketOrderNo, claimOpt.marketOrderItemNo)
            .INTO_VALUES(
                String.valueOf(claimOptRegDto.getClaimNo()),
                String.valueOf(claimOptRegDto.getClaimBundleNo()),
                String.valueOf(claimOptRegDto.getClaimItemNo()),
                String.valueOf(claimOptRegDto.getOrderOptNo()),
                String.valueOf(claimOptRegDto.getOptNo()),
                String.valueOf(claimOptRegDto.getOptPrice()),
                String.valueOf(claimOptRegDto.getClaimOptQty()),
                "NOW()",
                SqlUtils.appendSingleQuote(claimOptRegDto.getMarketOrderNo()),
                SqlUtils.appendSingleQuote(claimOptRegDto.getMarketOrderItemNo())
            );
        log.debug("insertClaimOpt :: \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 클레임 상태변경.
     *
     * @param parameter 클레임 상태 변경 데이터
     * @return 클레임 상태변경 쿼리
     */
    //@Param("claimNo") long claimNo, @Param("claimBundleNo") long claimBundleNo, @Param("claimStatus") String claimStatus
    public static String updateClaimStatus(LinkedHashMap<String, Object> parameter) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimReq, ObjectUtils.toArray(claimBundle.claimNo)))
            .SET(
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, parameter.get("claimStatus")),
                SqlUtils.nonSingleQuote(claimBundle.chgDt, "NOW()")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, parameter.get("claimNo"))
            );

        switch (ObjectUtils.toString(parameter.get("claimStatus"))) {
            // 승인
            case "C2" :
                query.SET(
                    SqlUtils.equalColumnByInput(claimReq.approveId, parameter.get("regId")),
                    SqlUtils.nonSingleQuote(claimReq.approveDt, "NOW()")
                );
                break;
            // 완료
            case "C3" :
                query.SET(
                    SqlUtils.equalColumn(
                        claimReq.approveId,
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IS_NULL, claimReq.approveId), SqlUtils.appendSingleQuote(parameter.get("regId")), claimReq.approveId)
                        )
                    ),
                    SqlUtils.equalColumn(
                        claimReq.approveDt,
                        SqlUtils.sqlFunction(
                            MysqlFunction.CASE,
                            ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IS_NULL, claimReq.approveDt), "NOW()", claimReq.approveDt)
                        )
                    ),
                    SqlUtils.equalColumnByInput(claimReq.completeId, parameter.get("regId")),
                    SqlUtils.nonSingleQuote(claimReq.completeDt, "NOW()")
                );
                break;
            // 철회
            case "C4" :
                query.SET(
                    SqlUtils.equalColumnByInput(claimReq.withdrawId, parameter.get("regId")),
                    SqlUtils.nonSingleQuote(claimReq.withdrawDt, "NOW()")
                );
                break;
            // 거부
            case "C9" :
                query.SET(
                    SqlUtils.equalColumnByInput(claimReq.rejectId, parameter.get("regId")),
                    SqlUtils.nonSingleQuote(claimReq.rejectDt, "NOW()"),
                    SqlUtils.equalColumnByInput(claimReq.rejectReasonType, parameter.get("reasonType")),
                    SqlUtils.equalColumnByInput(claimReq.rejectReasonDetail, parameter.get("reasonDetail"))
                );
                break;
            case "C8" :
                query.SET(
                    SqlUtils.equalColumnByInput(claimReq.pendingId, parameter.get("regId")),
                    SqlUtils.nonSingleQuote(claimReq.pendingDt, "NOW()"),
                    SqlUtils.equalColumnByInput(claimReq.pendingReasonType, parameter.get("reasonType")),
                    SqlUtils.equalColumnByInput(claimReq.pendingReasonDetail, parameter.get("reasonDetail"))
                );
                break;
            default:
                break;
        }
        String claimStatus = ObjectUtils.toString(parameter.get("claimStatus"));
        if(!"C8".contains(ObjectUtils.toString(claimStatus))) {
            query.WHERE(SqlUtils.sqlFunction(MysqlFunction.IN, claimBundle.claimStatus, ObjectUtils.toArray("'C1'", "'C2'", "'C8'")));
        } else {
            query.WHERE(SqlUtils.equalColumnByInput(claimBundle.claimStatus, ClaimCode.CLAIM_STATUS_C1.getCode()));
        }
        log.debug("updateClaimStatus :: \n[{}]", SqlUtils.orQuery(query.toString()));
        return SqlUtils.orQuery(query.toString());
    }

    public static String updateClaimPaymentStatus(@Param("claimPaymentNo") long claimPaymentNo, @Param("claimApprovalCd") String claimApprovalCd, ClaimCode claimCode, @Param("isLastYn")String isLastYn) {
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(ClaimPaymentEntity.class))
            .SET(
                SqlUtils.nonSingleQuote(claimPayment.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(claimPayment.claimApprovalCd, claimApprovalCd)
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.claimPaymentNo, claimPaymentNo)
            );

        if(claimCode == ClaimCode.CLAIM_PAYMENT_STATUS_P4){
            query.SET(
                SqlUtils.equalColumnByInput(claimPayment.paymentStatus, claimCode.getCode()),
                SqlUtils.nonSingleQuote(claimPayment.pgCancelRequestCnt, claimPayment.pgCancelRequestCnt.concat(" + ").concat("1"))
            );
        } else {
            if(claimCode == ClaimCode.CLAIM_PAYMENT_STATUS_P3){
                query.SET(
                    SqlUtils.nonSingleQuote(claimPayment.payCompleteDt, "NOW()"),
                    SqlUtils.equalColumnByInput(claimPayment.lastCancelYn, isLastYn)
                );
            }
            query.SET(
                SqlUtils.equalColumnByInput(claimPayment.paymentStatus, claimCode.getCode())
            );
        }
        log.debug("updateClaimPaymentStatus :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateClaimPaymentOcbTransactionId(@Param("claimPaymentNo") long claimPaymentNo, @Param("pgMid") String pgMid) {
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimPayment))
            .SET(
                SqlUtils.nonSingleQuote(claimPayment.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(claimPayment.pgMid, pgMid)
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.claimPaymentNo, claimPaymentNo)
            );
        log.debug("updateClaimPaymentOcbTransactionId :: \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * PG취소거래번호 업데이트
     *
     * @param claimPaymentNo 클레임거래취소번호
     * @param cancelTradeId 취소거래번호
     */
    public static String updateClaimPaymentPgCancelTradeId(@Param("claimPaymentNo") long claimPaymentNo, @Param("cancelTradeId") String cancelTradeId){
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimPayment))
            .SET(
                SqlUtils.nonSingleQuote(claimPayment.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(claimPayment.cancelTradeId, cancelTradeId)
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.claimPaymentNo, claimPaymentNo)
            );
        log.debug("updateClaimPaymentPgCancelTradeId :: \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 클레임 옵션정보를 위한 주문 옵션 조회
     *
     * @param orderItemNo 상품주문번호
     * @param orderOptNo 상품옵션번호
     * @return 주문옵션정보 조회 쿼리.
     */
    public static String selectOrderOptInfoForClaim(@Param("orderItemNo") long orderItemNo, @Param("orderOptNo") String orderOptNo){
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class);
        SQL query = new SQL()
            .SELECT(
                orderOpt.orderOptNo,
                orderOpt.optNo,
                orderOpt.marketOrderItemNo
            )
            .FROM(EntityFactory.getTableName(OrderOptEntity.class))
            .WHERE(
                SqlUtils.equalColumnByInput(orderOpt.orderItemNo, orderItemNo),
                SqlUtils.equalColumnByInput(orderOpt.orderOptNo, orderOptNo)
            );
        log.debug("selectOrderOptInfoForClaim :: \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 클레임 결제 취소를 위한 원주문 결제 정보 조회
     *
     * @param paymentNo 결제 번호
     * @param paymentType 결제 타입
     * @return 결제 취소를 위한 원주문 결제 정보 조회 쿼리
     */
    public static String selectOriPaymentForClaim(@Param("paymentNo") long paymentNo, @Param("paymentType") String paymentType){
        PaymentInfoEntity paymentInfo = EntityFactory.createEntityIntoValue(PaymentInfoEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                paymentInfo.paymentType,
                SqlUtils.aliasToRow(paymentInfo.parentMethodCd, "parent_payment_method"),
                paymentInfo.methodCd,
                paymentInfo.easyMethodCd,
                paymentInfo.pgKind,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    paymentInfo.paymentType,
                    ObjectUtils.toArray(
                        "'PG'", paymentInfo.pgMid,
                        "'OCB'", paymentInfo.ocbUniqTradeId,
                        "'DGV'", paymentInfo.dgvCardNo
                    ),
                    paymentInfo.pgMid
                ),
//                paymentInfo.pgMid,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(paymentInfo.paymentType, "DGV"),
                        paymentInfo.dgvPosNo,
                        paymentInfo.pgOid
                    ),
                    paymentInfo.pgOid
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(paymentInfo.paymentType, "DGV"),
                        paymentInfo.dgvApprovalDt,
                        paymentInfo.payToken
                    ),
                    paymentInfo.payToken
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    paymentInfo.paymentType,
                    ObjectUtils.toArray(
                        "'PG'", paymentInfo.pgTid,
                        "'MILEAGE'", purchaseOrder.userNo,
                        "'MHC'", paymentInfo.mhcApprovalDt,
                        "'OCB'", paymentInfo.ocbTranId,
                        "'DGV'", paymentInfo.dgvTradeNo
                    ),
                    "payment_trade_id"
                ),
                SqlUtils.aliasToRow(paymentInfo.approvalNo, "origin_approval_cd")
            )
            .FROM(EntityFactory.getTableNameWithAlias(paymentInfo))
            .INNER_JOIN( SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(paymentInfo.paymentNo)) )
            .WHERE(
                SqlUtils.equalColumnByInput(paymentInfo.paymentNo, paymentNo)
            );
        if(paymentType != null){
            query.WHERE(
                SqlUtils.equalColumnByInput(paymentInfo.paymentType, paymentType)
            );
        } else {
            query.LIMIT(1).OFFSET(0);
        }
        log.debug("selectOriPaymentForClaim :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPaymentCancelInfo(@Param("claimNo") long claimNo) {
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        PaymentInfoEntity paymentInfo = EntityFactory.createEntityIntoValue(PaymentInfoEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.pgKind, claimPayment.pgKind),
                claimPayment.paymentType,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimPayment.paymentType, "DG"),
                        SqlUtils.sqlFunction(MysqlFunction.AES_DECRYPT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.pgMid), "!@homeplus#$")),
                        claimPayment.pgMid
                    ),
                    claimPayment.pgMid
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.pgOid, claimPayment.pgOid),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.easyMethodCd, claimPayment.easyMethodCd),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.payToken, claimPayment.payToken),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.paymentTradeId, claimPayment.paymentTradeId),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.originApprovalCd, claimPayment.originApprovalCd),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(claimPayment.paymentAmt, SqlUtils.sqlFunction(MysqlFunction.ABS, claimPayment.substitutionAmt))), claimPayment.paymentAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.pgCancelRequestCnt, claimPayment.pgCancelRequestCnt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.parentPaymentMethod, claimPayment.parentPaymentMethod),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB"),
                        purchaseOrder.orgPurchaseOrderNo,
                        purchaseOrder.purchaseOrderNo
                    ),
                    purchaseOrder.purchaseOrderNo
                ),
                claimPayment.claimPaymentNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.userNo, claimBundle.userNo),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, paymentInfo.amt))
                        .FROM(EntityFactory.getTableNameWithAlias(paymentInfo))
                        .WHERE(
                            SqlUtils.equalColumn(paymentInfo.parentMethodCd, claimPayment.parentPaymentMethod),
                            SqlUtils.equalColumn(
                                paymentInfo.paymentNo,
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CASE,
                                    ObjectUtils.toArray(
                                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB"),
                                        SqlUtils.subTableQuery(
                                            new SQL().SELECT(SqlUtils.nonAlias(purchaseOrder.paymentNo))
                                                .FROM(EntityFactory.getTableName(purchaseOrder))
                                                .WHERE(
                                                    SqlUtils.equalColumn(SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo), purchaseOrder.orgPurchaseOrderNo)
                                                ).LIMIT(1).OFFSET(0)
                                        ),
                                        purchaseOrder.paymentNo
                                    )
                                )
                            )
                        ),
                    "order_payment_amt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.FN_PREV_CLAIM_AMT, ObjectUtils.toArray(claimPayment.purchaseOrderNo, claimPayment.paymentType), "prev_payment_amt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.claimNo, claimPayment.claimNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.siteType, claimMst.siteType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.regId, claimPayment.regId)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimPayment))
            .INNER_JOIN( SqlUtils.createJoinCondition( claimBundle, ObjectUtils.toArray(claimPayment.claimNo, claimPayment.paymentNo, claimPayment.purchaseOrderNo) ))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimPayment.purchaseOrderNo)))
            .INNER_JOIN( SqlUtils.createJoinCondition( claimMst, ObjectUtils.toArray( claimPayment.claimNo ) ) )
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.claimNo, claimNo),
                SqlUtils.sqlFunction(MysqlFunction.IN, claimBundle.claimStatus, ObjectUtils.toArray("'C1'", "'C2'", "'C8'"))
            );
        query.AND()
            .WHERE(SqlUtils.equalColumnByInput(claimPayment.paymentStatus, ClaimCode.CLAIM_PAYMENT_STATUS_P1.getCode()))
            .OR()
            .WHERE(SqlUtils.equalColumnByInput(claimPayment.paymentStatus, ClaimCode.CLAIM_PAYMENT_STATUS_P4.getCode()));
        query.GROUP_BY(claimPayment.claimPaymentNo, claimPayment.paymentType, claimPayment.purchaseOrderNo);

        log.debug("selectPaymentCancelInfo :: \n[{}]", SqlUtils.orQuery(query.toString()));
        return SqlUtils.orQuery(query.toString());
    }

    public static String callByEndClaimFlag(ClaimInfoRegDto infoRegDto){
        return SqlUtils.sqlFunction(
            MysqlFunction.SP_CLAIM_REG,
            ObjectUtils.toArray(
                String.valueOf(infoRegDto.getPaymentNo()),
                String.valueOf(infoRegDto.getPurchaseOrderNo()),
                String.valueOf(infoRegDto.getClaimNo())
            )
        );
    }

    public static String callByClaimAdditionReg(@Param("claimNo") long claimNo){
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_ADDITION_REG, ObjectUtils.toArray(String.valueOf(claimNo)));
    }

    public static String callByClaimItemEmpDiscount(@Param("claimNo")long claimNo, @Param("orderItemNo") long orderItemNo){
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_EMP_DISCOUNT, ObjectUtils.toArray(String.valueOf(claimNo), String.valueOf(orderItemNo)));
    }

    public static String selectClaimPossibleCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.orderType,
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shipStatus, shippingItem.shipStatus),
                purchaseOrder.purchaseOrderNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo, bundle.bundleNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        if(bundleNo != 0){
            query.WHERE(
                SqlUtils.equalColumnByInput(bundle.bundleNo, bundleNo)
            );
        }

        log.debug("selectClaimPossibleCheck :: \n[{}]", query.toString());
        return query.toString();
    }


    public static String updateClaimCouponReIssueResult(List<Long> claimAdditionNo) {
        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimAddition))
            .SET(
                SqlUtils.equalColumnByInput(claimAddition.isIssue, "Y")
            )
            .WHERE(
                SqlUtils.sqlFunction(MysqlFunction.IN, claimAddition.claimAdditionNo, claimAdditionNo.toArray())
            );
        log.debug("updateClaimCouponReIssueResult \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateClaimBundleSlotResult(@Param("claimBundleNo") long claimBundleNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimBundle))
            .SET(
                SqlUtils.equalColumnByInput(claimBundle.closeSendYn, "Y")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimBundleNo, claimBundleNo)
            );
        log.debug("updateClaimBundleSlotResult \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateTotalClaimPaymentStatus(@Param("claimNo") long claimNo, ClaimCode claimCode) {
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimPayment))
            .SET(
                SqlUtils.equalColumnByInput(claimPayment.paymentStatus, claimCode.getCode()),
                SqlUtils.equalColumn(claimPayment.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(claimPayment.chgId, "SYSTEM")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.claimNo, claimNo)
            );

        if(claimCode.getCode().equals("P1")){
            query.WHERE(
                SqlUtils.equalColumnByInput(claimPayment.paymentStatus, "P0")
            );
        }

        log.debug("updateTotalClaimPaymentStatus \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectCreateClaimAccumulateInfo(@Param("claimNo") long claimNo) {
        PurchaseAccumulateEntity purchaseAccumulate = EntityFactory.createEntityIntoValue(PurchaseAccumulateEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimInfoEntity claimInfo = EntityFactory.createEntityIntoValue(ClaimInfoEntity.class, "info");
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimMst.claimNo,
                claimMst.purchaseOrderNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.userNo, purchaseAccumulate.userNo),
                purchaseAccumulate.pointKind,
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.siteType, purchaseAccumulate.siteType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.orderDt, purchaseAccumulate.orderDt),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.chgDt), "%Y%m%d"), "claim_dt"),
                purchaseAccumulate.originStoreId,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseAccumulate.pointKind, "MHC"),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.mhcBaseAccRate),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.mileageAccRate)
                    ),
                    "base_acc_rate"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseAccumulate.pointKind, "MHC"),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.mhcAddAccRate),
                        0
                    ),
                    "add_acc_rate"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseAccumulate.pointKind, "MHC"),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.mhcBaseAccAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.mileageAccAmt)
                    ),
                    "base_acc_amt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseAccumulate.pointKind, "MHC"),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.mhcAddAccAmt),
                        0
                    ),
                    "add_acc_amt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.accTargetAmt, purchaseAccumulate.accTargetAmt),
                SqlUtils.aliasToRow("0", "claim_bass_acc_amt"),
                SqlUtils.aliasToRow("0", "claim_add_acc_amt"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseAccumulate.pointKind, "MHC"),
                        SqlUtils.customOperator(CustomConstants.PLUS, SqlUtils.sqlFunction(MysqlFunction.SUM, claimInfo.pgAmt), SqlUtils.sqlFunction(MysqlFunction.SUM, claimInfo.dgvAmt)),
                        SqlUtils.customOperator(
                            CustomConstants.MINUS,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.SUM, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimInfo.pgAmt, claimInfo.mhcAmt, claimInfo.dgvAmt, claimInfo.ocbAmt))),
                                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totShipPrice),
                                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totIslandShipPrice)
                            )
                        )
                    ),
                    "claim_acc_target_amt"
                ),
                SqlUtils.aliasToRow("0", "acc_cancel_amt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.totOrderAmt, purchaseAccumulate.totOrderAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.completeAmt, "tot_claim_amt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.mhcCardNoEnc, purchaseAccumulate.mhcCardNoEnc),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.mhcCardNoHmac, purchaseAccumulate.mhcCardNoHmac),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.affiliateCardKind, purchaseAccumulate.affiliateCardKind),
                SqlUtils.aliasToRow("'N'", "req_send_yn"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.resultCd, "org_result_cd"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.approvalDt, "org_approval_dt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseAccumulate.approvalNo, "org_approval_no"),
                SqlUtils.aliasToRow("0", "result_point")
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseAccumulate))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(purchaseAccumulate.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"))))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(claimInfo, "info"),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(claimBundle.claimNo, claimBundle.bundleNo), claimInfo)
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo, claimBundle.claimNo, claimInfo.orderItemNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    orderItem,
                    ObjectUtils.toArray(claimItem.purchaseOrderNo, claimItem.orderItemNo),
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.deptNo, "3106"),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, orderItem.division, "9")
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo),
                SqlUtils.equalColumnByInput(purchaseAccumulate.resultCd, "0000"),
                SqlUtils.OR(
                    Boolean.TRUE,
                    SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, orderItem.orderItemNo),
                    SqlUtils.equalColumnByInput(claimInfo.orderItemNo, 0)
                )
            )
            .GROUP_BY(claimMst.claimNo, claimMst.purchaseOrderNo, purchaseAccumulate.pointKind, purchaseAccumulate.originStoreId);
        log.debug("selectCreateClaimAccumulateInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectAccumulateCancelInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("pointKind") String pointKind) {
        ClaimAccumulateEntity claimAccumulate = EntityFactory.createEntityIntoValue(ClaimAccumulateEntity.class);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimAccumulate.claimBaseAccAmt, claimAccumulate.claimBaseAccAmt),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimAccumulate.claimAddAccAmt, claimAccumulate.claimAddAccAmt),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimAccumulate.claimAccTargetAmt, claimAccumulate.claimAccTargetAmt)
            )
            .FROM(EntityFactory.getTableName(claimAccumulate))
            .WHERE(
                SqlUtils.equalColumnByInput(claimAccumulate.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(claimAccumulate.pointKind, pointKind)
            );
        log.debug("selectAccumulateCancelInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String insertClaimAccumulate(LinkedHashMap<String, Object> parameterMap) {
        ClaimAccumulateEntity claimAccumulate = EntityFactory.createEntityIntoValue(ClaimAccumulateEntity.class);
        String[] intoColumn = EntityFactory.getColumnInfo(ClaimAccumulateEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(claimAccumulate))
            .INTO_COLUMNS(intoColumn)
            .INTO_VALUES(SqlUtils.convertInsertValue(intoColumn));
        log.debug("insertClaimAccumulate \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateClaimAccumulate(LinkedHashMap<String, Object> parameterMap) {
        ClaimAccumulateEntity claimAccumulate = EntityFactory.createEntityIntoValue(ClaimAccumulateEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimAccumulate))
            .SET(
                SqlUtils.equalColumn(claimAccumulate.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(claimAccumulate.resultCd, ObjectUtils.toString(parameterMap.get("returnCode"))),
                SqlUtils.equalColumn(claimAccumulate.resultPoint, ObjectUtils.toString(parameterMap.get("mhcSaveCancelPoint")))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimAccumulate.claimNo, ObjectUtils.toLong(parameterMap.get("claim_no")))
            );

        if(parameterMap.get("mhcAprDate") != null){
            query.SET(
                SqlUtils.equalColumnByInput(claimAccumulate.approvalDt, ObjectUtils.toString(parameterMap.get("mhcAprDate")))
            );
        }

        if(parameterMap.get("mhcAprNumber") != null){
            query.SET(
                SqlUtils.equalColumnByInput(claimAccumulate.approvalNo, ObjectUtils.toString(parameterMap.get("mhcAprNumber")))
            );
        }
        log.debug("updateClaimAccumulate \n[{}]", query.toString());
        return query.toString();
    }


    public static String updateClaimAccumulateReqSend(@Param("claimNo") long claimNo, @Param("reqSendYn") String reqSendYn, @Param("pointKind") String pointKind) {
        ClaimAccumulateEntity claimAccumulate = EntityFactory.createEntityIntoValue(ClaimAccumulateEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimAccumulate))
            .SET(
                SqlUtils.equalColumnByInput(claimAccumulate.reqSendYn, reqSendYn),
                SqlUtils.equalColumn(claimAccumulate.chgDt, "NOW()")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimAccumulate.claimNo, String.valueOf(claimNo)),
                SqlUtils.equalColumnByInput(claimAccumulate.pointKind, pointKind)
            );
        log.debug("updateClaimAccumulateReqSend \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPaymentCancelManualInfo(@Param("claimNo") long claimNo, @Param("paymentType") String paymentType) {
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        PaymentInfoEntity paymentInfo = EntityFactory.createEntityIntoValue(PaymentInfoEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.pgKind, claimPayment.pgKind),
                claimPayment.paymentType,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimPayment.paymentType, "DG"),
                        SqlUtils.sqlFunction(MysqlFunction.AES_DECRYPT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.pgMid), "!@homeplus#$")),
                        claimPayment.pgMid
                    ),
                    claimPayment.pgMid
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.pgOid, claimPayment.pgOid),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.easyMethodCd, claimPayment.easyMethodCd),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.payToken, claimPayment.payToken),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.paymentTradeId, claimPayment.paymentTradeId),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.originApprovalCd, claimPayment.originApprovalCd),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(claimPayment.paymentAmt, SqlUtils.sqlFunction(MysqlFunction.ABS, claimPayment.substitutionAmt))), claimPayment.paymentAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.pgCancelRequestCnt, claimPayment.pgCancelRequestCnt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.parentPaymentMethod, claimPayment.parentPaymentMethod),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB"),
                        purchaseOrder.orgPurchaseOrderNo,
                        purchaseOrder.purchaseOrderNo
                    ),
                    purchaseOrder.purchaseOrderNo
                ),
                claimPayment.claimPaymentNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.userNo, claimBundle.userNo),
                SqlUtils.subTableQuery(
                    new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, paymentInfo.amt))
                        .FROM(EntityFactory.getTableNameWithAlias(paymentInfo))
                        .WHERE(
                            SqlUtils.equalColumn(paymentInfo.parentMethodCd, claimPayment.parentPaymentMethod),
                            SqlUtils.equalColumn(
                                paymentInfo.paymentNo,
                                SqlUtils.sqlFunction(
                                    MysqlFunction.CASE,
                                    ObjectUtils.toArray(
                                        SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_SUB"),
                                        SqlUtils.subTableQuery(
                                            new SQL().SELECT(SqlUtils.nonAlias(purchaseOrder.paymentNo))
                                                .FROM(EntityFactory.getTableName(purchaseOrder))
                                                .WHERE(
                                                    SqlUtils.equalColumn(SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo), purchaseOrder.orgPurchaseOrderNo)
                                                ).LIMIT(1).OFFSET(0)
                                        ),
                                        purchaseOrder.paymentNo
                                    )
                                )
                            )
                        ),
                    "order_payment_amt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.FN_PREV_CLAIM_AMT, ObjectUtils.toArray(claimPayment.purchaseOrderNo, claimPayment.paymentType), "prev_payment_amt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.claimNo, claimPayment.claimNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.siteType, claimMst.siteType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.regId, claimPayment.regId)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimPayment))
            .INNER_JOIN( SqlUtils.createJoinCondition( claimBundle, ObjectUtils.toArray(claimPayment.claimNo, claimPayment.paymentNo, claimPayment.purchaseOrderNo) ))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimPayment.purchaseOrderNo)))
            .INNER_JOIN( SqlUtils.createJoinCondition( claimMst, ObjectUtils.toArray( claimPayment.claimNo ) ) )
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimPayment.paymentType, paymentType)
            );
        query.GROUP_BY(claimPayment.claimPaymentNo, claimPayment.paymentType, claimPayment.purchaseOrderNo);

        log.debug("selectPaymentCancelManualInfo :: \n[{}]", SqlUtils.orQuery(query.toString()));
        return SqlUtils.orQuery(query.toString());
    }

    public static String insertClaimPickShipping(ClaimShippingRegDto shippingRegDto) {
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class);

        //claimPickShipping.pickShipType,
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(claimPickShipping))
            .INTO_COLUMNS(
                claimPickShipping.claimBundleNo, claimPickShipping.isAuto, claimPickShipping.isPick, claimPickShipping.isPickProxy,
                claimPickShipping.userNo, claimPickShipping.pickTrackingYn, claimPickShipping.partnerId, claimPickShipping.orgInvoiceNo,
                claimPickShipping.pickReceiverNm, claimPickShipping.pickZipcode, claimPickShipping.pickBaseAddr, claimPickShipping.pickDetailAddr,
                claimPickShipping.pickRoadBaseAddr, claimPickShipping.pickRoadDetailAddr, claimPickShipping.pickMobileNo, claimPickShipping.pickStatus,
                claimPickShipping.pickShipType, claimPickShipping.safetyUseYn, claimPickShipping.slotId, claimPickShipping.shiftId, claimPickShipping.shipDt,
                claimPickShipping.regId, claimPickShipping.chgId, claimPickShipping.regDt, claimPickShipping.chgDt
            )
            .INTO_VALUES(
                String.valueOf( shippingRegDto.getClaimBundleNo() ),
                SqlUtils.appendSingleQuote(shippingRegDto.getIsAuto()),
                SqlUtils.appendSingleQuote(shippingRegDto.getIsPick()),
                SqlUtils.appendSingleQuote(shippingRegDto.getIsPickProxy()),
                String.valueOf( shippingRegDto.getUserNo() ),
                SqlUtils.appendSingleQuote(shippingRegDto.getPickTrackingYn()),
                SqlUtils.appendSingleQuote(shippingRegDto.getPartnerId()),
                SqlUtils.appendSingleQuote(shippingRegDto.getOrgInvoiceNo()),
                SqlUtils.appendSingleQuote(shippingRegDto.getPickReceiverNm()),
                SqlUtils.appendSingleQuote(shippingRegDto.getPickZipcode()),
                SqlUtils.appendSingleQuote(shippingRegDto.getPickBaseAddr()),
                SqlUtils.appendSingleQuote(shippingRegDto.getPickDetailAddr()),
                SqlUtils.appendSingleQuote(shippingRegDto.getPickRoadBaseAddr()),
                SqlUtils.appendSingleQuote(shippingRegDto.getPickRoadDetailAddr()),
                SqlUtils.appendSingleQuote(shippingRegDto.getPickMobileNo()),
                SqlUtils.appendSingleQuote(shippingRegDto.getPickStatus()),
                SqlUtils.appendSingleQuote(shippingRegDto.getPickShipType()),
                SqlUtils.appendSingleQuote(shippingRegDto.getSafetyUseYn()),
                SqlUtils.appendSingleQuote(shippingRegDto.getSlotId()),
                SqlUtils.appendSingleQuote(shippingRegDto.getShiftId()),
                SqlUtils.appendSingleQuote(shippingRegDto.getShipDt()),
                SqlUtils.appendSingleQuote(shippingRegDto.getRegId()),
                SqlUtils.appendSingleQuote(shippingRegDto.getChgId()),
                "NOW()",
                "NOW()"
            );

        log.debug("insertClaimPickShipping ::: {} ", query.toString());

        // 이미 수거를 보냈을 경우
        // 수거송장 등록자는 pickRegId로
        // 수거예약일은 shipDt
        // 수거 송장 등록일은 접수일로 처리.
        if(shippingRegDto.getIsPick().equalsIgnoreCase("Y")){
            query.INTO_COLUMNS(
                claimPickShipping.pickDlvCd, claimPickShipping.pickInvoiceNo, claimPickShipping.pickInvoiceRegId, claimPickShipping.pickP0Id
            )
                .INTO_VALUES(
                    SqlUtils.appendSingleQuote(shippingRegDto.getPickDlvCd()),
                    SqlUtils.appendSingleQuote(shippingRegDto.getPickInvoiceNo()),
                    SqlUtils.appendSingleQuote(shippingRegDto.getPickInvoiceRegId()),
                    SqlUtils.appendSingleQuote(shippingRegDto.getPickInvoiceRegId())
                );
        }

        log.debug("insertClaimPickShipping ::: {} ", query.toString());

        switch (shippingRegDto.getPickStatus()) {
            case "P1" :
                query.INTO_COLUMNS(claimPickShipping.pickP0Dt, claimPickShipping.pickP1Dt)
                    .INTO_VALUES("NOW()", "NOW()");
                break;
            case "P3" :
                query.INTO_COLUMNS(
                    claimPickShipping.pickP0Dt, claimPickShipping.pickP1Dt,
                    claimPickShipping.pickP2Dt, claimPickShipping.pickP3Dt
                )
                    .INTO_VALUES("NOW()", "NOW()", "NOW()", "NOW()");
                break;
        }

        log.debug("insertClaimPickShipping :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String insertClaimExchShipping(ClaimExchShippingRegDto exchShippingRegDto) {
        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(claimExchShipping))
            .INTO_COLUMNS(
                claimExchShipping.claimPickShippingNo, claimExchShipping.claimBundleNo, claimExchShipping.exchShipType, claimExchShipping.exchStatus,
                claimExchShipping.exchTrackingYn, claimExchShipping.userNo, claimExchShipping.exchReceiverNm, claimExchShipping.exchZipcode,
                claimExchShipping.exchBaseAddr, claimExchShipping.exchDetailAddr, claimExchShipping.exchRoadBaseAddr, claimExchShipping.exchRoadDetailAddr,
                claimExchShipping.exchMobileNo, claimExchShipping.partnerId, claimExchShipping.regId, claimExchShipping.chgId,
                claimExchShipping.regDt, claimExchShipping.chgDt
            )
            .INTO_VALUES(
                SqlUtils.convertInsertParamToCamelCase(claimExchShipping.claimPickShippingNo, claimExchShipping.claimBundleNo, claimExchShipping.exchShipType, claimExchShipping.exchStatus,
                    claimExchShipping.exchTrackingYn, claimExchShipping.userNo, claimExchShipping.exchReceiverNm, claimExchShipping.exchZipcode,
                    claimExchShipping.exchBaseAddr, claimExchShipping.exchDetailAddr, claimExchShipping.exchRoadBaseAddr, claimExchShipping.exchRoadDetailAddr,
                    claimExchShipping.exchMobileNo, claimExchShipping.partnerId, claimExchShipping.regId, claimExchShipping.chgId,
                    claimExchShipping.regDt, claimExchShipping.chgDt)
            );
        log.debug("insertClaimExchShipping :: \n[{}]", query.toString());
        return query.toString();
    }
}
