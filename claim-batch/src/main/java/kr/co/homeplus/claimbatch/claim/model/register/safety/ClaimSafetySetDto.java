package kr.co.homeplus.claimbatch.claim.model.register.safety;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임 안심번호 발급/수정 요청 DTO")
public class ClaimSafetySetDto {
    @ApiModelProperty(value = "클레임번들번호")
    private Long claimBundleNo;
    @ApiModelProperty(value = "클레임타입(R:수거,X:교환)")
    private String claimType;
    @ApiModelProperty(value = "요청연락처번호")
    private String reqPhoneNo;
}
