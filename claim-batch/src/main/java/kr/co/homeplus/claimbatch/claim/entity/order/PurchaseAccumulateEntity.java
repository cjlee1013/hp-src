package kr.co.homeplus.claimbatch.claim.entity.order;

public class PurchaseAccumulateEntity {
    // 구매주문번호
    public String purchaseOrderNo;
    // 고객번호
    public String userNo;
    // 포인트구분
    public String pointKind;
    // 사이트유형(HMP:홈플러스,CLUB:더클럽)
    public String siteType;
    // 주문일자(YYYYMMDD)
    public String orderDt;
    // (매출용 실제점포ID:0024,0003 등)
    public String originStoreId;
    // 마일리지적립율
    public String mileageAccRate;
    // 마일리지적립금액
    public String mileageAccAmt;
    // MHC기본적립율
    public String mhcBaseAccRate;
    // MHC기본적립금액
    public String mhcBaseAccAmt;
    // MHC추가적립율
    public String mhcAddAccRate;
    // MHC추가적립금액
    public String mhcAddAccAmt;
    // 적립대상금액
    public String accTargetAmt;
    // 총주문금액
    public String totOrderAmt;
    // MHC카드번호암호화(MHC 대체카드번호 RKM 암호화)
    public String mhcCardNoEnc;
    // MHC카드번호HMAC(MHC 대체카드번호 RKM HMAC)
    public String mhcCardNoHmac;
    // 제휴카드구분
    public String affiliateCardKind;
    // 요청전송여부(N:미전송,I:요청중,Y:전송성공,E:전송실패)
    public String reqSendYn;
    // 결과코드
    public String resultCd;
    // 승인일자
    public String approvalDt;
    // 승인번호
    public String approvalNo;
    // 결과포인트
    public String resultPoint;
    // 등록일시
    public String regDt;
    // 등록ID
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정ID
    public String chgId;
}
