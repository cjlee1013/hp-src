package kr.co.homeplus.claimbatch.claim.maket.naver.service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import kr.co.homeplus.claimbatch.claim.enums.ClaimCode;
import kr.co.homeplus.claimbatch.claim.maket.naver.constants.NaverMarketClaimReasonType;
import kr.co.homeplus.claimbatch.claim.maket.service.MarketMapperService;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimRegDetailDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.claimbatch.claim.service.ClaimMapperService;
import kr.co.homeplus.claimbatch.claim.service.ClaimPaymentService;
import kr.co.homeplus.claimbatch.claim.service.ClaimRegisterService;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import kr.co.homeplus.claimbatch.util.SlackUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MarketClaimService {

    private final ClaimMapperService claimMapperService;
    private final MarketMapperService mapperService;
    private final ClaimRegisterService registerService;
    private final NaverClaimService naverClaimService;
    private final ClaimPaymentService paymentService;

    // 클레임 데이터 수집
    public void getNaverClaimList(String startDt, String endDt) {
        try {
            naverClaimService.getNaverClaimList(startDt, endDt);
        } catch (Exception e) {
            log.error("{}", e.getMessage());
        }
    }

    public void getNaverClaimListRetry(String marketOrderItemNos) {
        try {
            naverClaimService.getNaverClaimListRetry(marketOrderItemNos);
        } catch (Exception e) {
            log.error("{}", e.getMessage());
        }
    }

    // 클레임 생성
    public void marketClaimProcess(String marketOrderItemNo) {
        List<LinkedHashMap<String, Object>> marketClaimList = new ArrayList<>();
        // 입력받은 값이 있으면 해당 값만 처리
        if(marketOrderItemNo != null){
            if(marketOrderItemNo.contains("|")){
                for(String str : marketOrderItemNo.split("\\|")){
                    List<LinkedHashMap<String, Object>> resultList = mapperService.getCreateMarketClaimData(str);
                    if(resultList != null){
                        marketClaimList.addAll(resultList);
                    }
                }
            }
        }
        // 데이터가 없는 경우에는 전체를 입력분을 찾아서 처
        if(marketClaimList.size() <= 0) {
            marketClaimList = mapperService.getCreateMarketClaimData(marketOrderItemNo);
            // R/X에 대힌 처리가 필요함.
        }

        Map<String, List<LinkedHashMap<String, Object>>> marketClaimGroupMap = this.getGroupbyList(marketClaimList);
        // 클레임 생성 데이터를 판단하여 처리.
        if(marketClaimGroupMap != null) {
            for(String key : marketClaimGroupMap.keySet()) {
                try {
                    // 무조건 처음 정보를 기준으로 처리.
                    LinkedHashMap<String, Object> map = marketClaimGroupMap.get(key).get(0);
                    // 기존 클레임이있는지 확인 후 있으면 상태 업데이트
                    LinkedHashMap<String, Object> existingClaimMap = mapperService.getExistingClaimByMarket(ObjectUtils.toLong(map.get("market_claim_no")));
                    // 요청채널
                    String requestChannel = this.getRegChannel(ObjectUtils.toString(map.get("request_channel")));
                    long claimNo = 0L;
                    // 클레임구분
                    String claimType = ObjectUtils.toString(map.get("claim_type"));
                    // 클레임상태
                    String claimStatus = ObjectUtils.toString(map.get("claim_status"));
                    // 마켓주문번호
                    String marketOrderNo = ObjectUtils.toString(map.get("market_order_no"));

                    // 기등록한 데이터가 아닌 경우
                    if(existingClaimMap == null) {
                        // 등록가능 파라미터 확인
                        ClaimRegSetDto claimRegSetDto = this.createClaimRegSetDtoByMarket(marketClaimGroupMap.get(key));
                        // 부분취소 여부 확인
                        this.isClaimPart(claimRegSetDto);
                        // 클레임 등록
                        LinkedHashMap<String, Object> result = registerService.addClaimRegister(claimRegSetDto);
                        log.info("마켓상품주문번호({}) 에 대한 클레임 생성({})", map.get("market_order_item_no"), result.get("claim_no"));
                        // 클레임 생성되면 해당 마켓클레임 상품에 처리 여부를 저장한다.
                        mapperService.setMarketClaimProcessResultSave(ObjectUtils.toLong(map.get("market_claim_no")), "R");
                        // 클레임 번호 업데이트
                        mapperService.setMarketClaimAfterProcessClaimNoSave(ObjectUtils.toLong(map.get("market_claim_no")), ObjectUtils.toLong(result.get("claim_no")));
                        if("CANCEL,ADMIN_CANCEL".contains(claimType) && !"CANCEL_REQUEST".equals(claimStatus)) {
                            paymentService.setClaimStatus(ObjectUtils.toLong(result.get("claim_no")), "BATCH", "C2");
                        }
                        claimNo = ObjectUtils.toLong(result.get("claim_no"));
                    } else {
                        // 기존클레임이 존재할 경우 클레임아이템 수량과 처리하고자 하는 수량을 비교 하여 처리한다.
                        // 기존 클레임 등록된 아이템 수
                        int prevClaimItemCnt = this.getPrevClaimItemCnt(ObjectUtils.toLong(map.get("market_claim_no")));
                        int requestCnt = this.getRequestClaimItemCnt(ObjectUtils.toLong(map.get("market_claim_no")));

                        if(requestCnt != prevClaimItemCnt) {
                            String message = MessageFormat.format("기존클레임 존재!!!\n클레임그룹번호({0})의 등록된 아이템 수량 : {1}\n마켓클레임 요청 아이템 수량 : {2}\n아이템 수량이 서로 달라 거절처리됨.\n - 확인필요.", ObjectUtils.toString(existingClaimMap.get("claim_no")), prevClaimItemCnt, requestCnt);
                            SlackUtils.sendingForMarketClaimError(message);
                            continue;
                        }

                        if(!"R".equals(ObjectUtils.toString(map.get("rcv_yn")))) {
                            mapperService.setMarketClaimAfterProcessClaimNoSave(ObjectUtils.toLong(map.get("market_claim_no")), ObjectUtils.toLong(existingClaimMap.get("claim_no")));
                            // 클레임 생성되면 해당 마켓클레임 상품에 처리 여부를 저장한다.
                            mapperService.setMarketClaimProcessResultSave(ObjectUtils.toLong(map.get("market_claim_no")), "R");
                        }
                        claimNo = ObjectUtils.toLong(existingClaimMap.get("claim_no"));
                    }
                    // 클레임 접수된 건이 상태가 DONE 일 경우 C3로 처리 하도록 한다.
                    // 반품/교환에서 상태가 HOLDBACK이 있는 경우는 보류처리.
                    // 보류처리 필요.
                    log.info("마켓주문 클레임그룹번호({})에 대한 마켓클레임상태({}) 처리", claimNo, claimStatus);
//                    // 금액 업데이트
//                    log.info("마켓주문 클레임진행 중 금액 업데이트 실시 : {}", paymentService.marketClaimAmountModify(claimNo));

                    if("CANCEL_DONE,RETURN_DONE,EXCHANGE_DONE,ADMIN_CANCEL_DONE".contains(claimStatus)) {
                        // Market_claim_item 해당 주문 번호로 다른 결과 있으면 거절 처리.
                        if(mapperService.isMarketClaimAnotherStatusCheck(ObjectUtils.toLong(map.get("market_claim_no")), claimStatus)) {
                            // 거절처리.
                            paymentService.setClaimStatus(claimNo, "BATCH", "C4");
                            String message = MessageFormat.format("클레임그룹번호({0})의 마켓주문({1})의 상태({2})가 서로 달라 거절처리됨.\n - 수기처리해야함.", String.valueOf(claimNo), marketOrderNo, claimStatus);
                            SlackUtils.sendingForMarketClaimError(message);
                        } else {
                            // C3
                            paymentService.paymentCancelByMarket(claimNo, "BATCH");
                        }
                        // 마켓클레임 처리완료처리
                        mapperService.setMarketClaimProcessResultSave(ObjectUtils.toLong(map.get("market_claim_no")), "Y");
                    } else if("RELEASED".equals(ObjectUtils.toString(map.get("holdback_status")))) {
                        paymentService.setClaimStatus(claimNo, "BATCH", "C8");
                    } else if("RETURN_REJECT,EXCHANGE_REJECT".contains(claimStatus)) {
                        log.info("마켓주문 클레임그룹번호({}) 에 대한 거절처리", claimNo);
                        paymentService.setClaimStatus(claimNo, "BATCH", "C4");
                    } else {
                        if(
                            "CANCEL,ADMIN_CANCEL".contains(claimType) && "CANCELING".equals(claimStatus)
                            || ("COLLECT_DONE".equals(claimStatus) && ("RETURN".equals(claimType) || "EXCHANGE".equals(claimType)))
                        ) {
                            paymentService.setClaimStatus(claimNo, "BATCH", "C2");
                        }
                    }
                } catch (Exception e) {
                    log.error("마켓 상품주문번호({})에 대한 클레임 생성 오류 :: {}", marketClaimGroupMap.get(key).get(0).get("market_order_item_no"), e.getMessage());
                    mapperService.setMarketClaimProcessResultSave(ObjectUtils.toLong(marketClaimGroupMap.get(key).get(0).get("market_claim_no")), "E");
                }
            }
        } else {
            log.error("처리가능한 마켓클레임이 없습니다.");
        }
    }

    /**
     * 취소요청 파라미터 생성.
     *
     * @param map 취소기본정보.
     * @return 취소요청파라미터
     */
    private ClaimRegSetDto createClaimRegSetDtoByMarket(List<LinkedHashMap<String, Object>> mapList) throws Exception {
        // 주문번호
        long purchaseOrderNo = ObjectUtils.toLong(mapList.get(0).get("purchase_order_no"));
        // 배송번호
        long bundleNo = ObjectUtils.toLong(mapList.get(0).get("bundle_no"));
        // 마켓클레임번호
        long marketClaimNo = ObjectUtils.toLong(mapList.get(0).get("market_claim_no"));
        // 마켓주문번호리스트 (마켓클레임번호로 처리 방법 변경)
//        List<String> marketOrderItemNoList = mapList.stream().map(map -> ObjectUtils.toString(map.get("market_order_item_no"))).collect(Collectors.toList());
        List<String> marketOrderItemNoList = this.getMarketClaimList(marketClaimNo);
        // 클레임할 정보가 있는지 확인
        List<ClaimPreRefundItemList> itemInfo = this.getMarketClaimPreRefundItemList(purchaseOrderNo, bundleNo, marketOrderItemNoList);
        log.info("itemInfo :: {}", itemInfo);
        if(itemInfo == null || itemInfo.size() == 0){
            log.error("거래번호({})는 이미 취소된 거래입니다.", purchaseOrderNo);
            throw new Exception("이미 취소된 거래입니다.");
        }
        // 마켓클레임 사유코드 -> refit용으로 전환
        String claimReasonType = this.getRefitClaimReasonType(ObjectUtils.toString(mapList.get(0).get("claim_type")), ObjectUtils.toString(mapList.get(0).get("claim_reason")));
        // 클레임 타입 결과조회
        LinkedHashMap<String, Object> claimTypeMap = mapperService.getClaimReasonType(claimReasonType);

        if(claimTypeMap == null) {
            log.error("클레임 정보 맵핑 실패 :: {}", purchaseOrderNo);
            throw new Exception("클레임 정보 맵핑에 실패했습니다.");
        }
        // 클레임사유타입과 상세 사유는 오는대로 처리.
        return ClaimRegSetDto.builder()
            .purchaseOrderNo(String.valueOf(purchaseOrderNo))
            .bundleNo(String.valueOf(bundleNo))
            .claimItemList(this.getMarketClaimPreRefundItemList(purchaseOrderNo, bundleNo, marketOrderItemNoList))
            .whoReason(ObjectUtils.toString(claimTypeMap.get("who_reason")))
            .cancelType(ObjectUtils.toString(claimTypeMap.get("cancel_type")))
            .claimType(ObjectUtils.toString(claimTypeMap.get("claim_type")))
            .claimReasonType(claimReasonType)
            .claimReasonDetail(ObjectUtils.toString(mapList.get(0).get("claim_reason_detail")))
            .regId(this.getRegChannel(ObjectUtils.toString(mapList.get(0).get("request_channel"))))
            .claimPartYn("N")
            .orderCancelYn(ObjectUtils.toString(claimTypeMap.get("claim_type")).equals("C") ? "Y" : "N")
            .piDeductPromoYn("N")
            .piDeductDiscountYn("N")
            .piDeductShipYn("N")
            .claimDetail(ObjectUtils.toString(claimTypeMap.get("claim_type")).equals("C") ? null : this.createClaimRegDetailDtoByMarket(ObjectUtils.toLong(mapList.get(0).get("market_claim_no")), marketOrderItemNoList.get(0)))
            .build();
    }

    private String getRefitClaimReasonType(String claimType, String marketReasonType) {
        if(StringUtils.isEmpty(marketReasonType)) {
            marketReasonType = "";
        }
        if ("ADMIN_CANCEL".equals(claimType)) {
            claimType = "CANCEL";
        }
        return NaverMarketClaimReasonType.getClaimReasonTypeByRefit(claimType, marketReasonType);
    }

    private ClaimRegDetailDto createClaimRegDetailDtoByMarket(long marketClaimNo, String marketOrderItemNo) {
        ClaimRegDetailDto claimRegDetailDto = mapperService.getClaimRegDetailDtoInfo(marketClaimNo, marketOrderItemNo);
        if(claimRegDetailDto != null) {
            if(StringUtils.isEmpty(claimRegDetailDto.getPickBaseAddr())) {
                claimRegDetailDto = mapperService.getClaimRegDetailDtoInfo(marketClaimNo, marketOrderItemNo);
            }
            // 동봉여부
            claimRegDetailDto.setIsEnclose("N");
            // 구매자 반품발송여부
//            claimRegDetailDto.setIsPick("N");
            // 수거 여뿌(N이면 수거안함)
//            claimRegDetailDto.setIsPickReq("N");
        }
        return claimRegDetailDto;
    }

    private String getRegChannel(String requestChannel) {
        switch (requestChannel) {
            case "구매회원" :
                return "M-FRONT";
            case "판매자" :
            case "판매자API" :
                return "M-ADMIN";
            default:
                return "BATCH";
       }
    }

    private void isClaimPart(ClaimRegSetDto claimRegSetDto) throws Exception {
        // 주문건수, 클레임건수 데이터 가지고 옴
        LinkedHashMap<String, Object> getOrderClaimPartInfoMap = this.getOrderByClaimPartInfo(claimRegSetDto.getPurchaseOrderNo(), claimRegSetDto.getBundleNo());
        // 클레임신청된 건수 + 이미 클레임 신청된 건수 비교
        int totalClaimItemQty = ObjectUtils.toInt(getOrderClaimPartInfoMap.get("claimItemQty")) + claimRegSetDto.getClaimItemList().stream().mapToInt(itemList -> Integer.parseInt(itemList.getClaimQty())).sum();

        if(ObjectUtils.toInt(getOrderClaimPartInfoMap.get("orderItemQty")) == totalClaimItemQty){
            // 전체취소 건수 set
            claimRegSetDto.setClaimPartYn("N");
            // 기 등록된 건이 없으면 전체취소로직
            if(ObjectUtils.toInt(getOrderClaimPartInfoMap.get("claimItemQty")) == 0 && Long.parseLong(claimRegSetDto.getBundleNo()) == 0){
                claimRegSetDto.setClaimPartYn("Y");
            }
        } else {
            claimRegSetDto.setClaimPartYn("Y");
        }
    }

    private LinkedHashMap<String, Object> getOrderByClaimPartInfo(String purchaseOrderNo, String bundleNo) throws Exception {
        LinkedHashMap<String, Object> returnMap = claimMapperService.getOrderByClaimPartInfo(Long.parseLong(purchaseOrderNo), Long.parseLong(bundleNo));
        if(ObjectUtils.toInt(returnMap.get("orderItemQty")) == 0){
            throw new Exception("클레임할 건수가 없습니다.");
        }
        return returnMap;
    }

    private List<ClaimPreRefundItemList> getMarketClaimPreRefundItemList(long purchaseOrderNo, long bundleNo, List<String> marketOrderItemNoList) {
        List<ClaimPreRefundItemList> refundItemList = new ArrayList<>();
        for(String marketOrderItemNo : marketOrderItemNoList) {
            List<ClaimPreRefundItemList> itemList = claimMapperService.getClaimPreRefundItemInfoByMarket(purchaseOrderNo, bundleNo, marketOrderItemNo);
            if(itemList != null && itemList.size() > 0) {
                refundItemList.addAll(itemList);
            }
        }
        return refundItemList;
    }

    private Map<String, List<LinkedHashMap<String, Object>>> getGroupbyList(List<LinkedHashMap<String, Object>> list) {
        try {
            if(list.size() > 0) {
                return list.stream().collect(Collectors.groupingBy(map -> ObjectUtils.toString(map.get("market_claim_no"))));
            }
            return null;
        } catch (Exception e) {
            log.error("마켓클레임 그룹핑 중 오류가 발생되었습니다. :: {}", e.getMessage());
            return null;
        }
    }

    /**
     * 마켓클레임 리스트
     *
     * @param marketClaimNo
     * @return
     */
    private List<String> getMarketClaimList(long marketClaimNo) {
        List<LinkedHashMap<String, Object>> claimList = mapperService.getMarketClaimList(marketClaimNo);
        log.info("마켓클레임번호({}) 상품 리스트 : {}", marketClaimNo, claimList);
        return claimList.stream().map(map -> ObjectUtils.toString(map.get("market_order_item_no"))).collect(Collectors.toList());
    }

    /**
     * 기존클레임 존재시 기클레임 아이템 수량 체크
     *
     * @param marketClaimNo
     * @return
     */
    private int getPrevClaimItemCnt(long marketClaimNo) {
        return mapperService.getPrevClaimItemCntByMarket(marketClaimNo);

    }

    private int getRequestClaimItemCnt(long marketClaimNo) {
        return mapperService.getMarketClaimRequestCnt(marketClaimNo);
    }
}
