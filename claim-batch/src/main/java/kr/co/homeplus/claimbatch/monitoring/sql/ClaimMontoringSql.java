package kr.co.homeplus.claimbatch.monitoring.sql;

import kr.co.homeplus.claimbatch.claim.constants.CustomConstants;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimPaymentEntity;
import kr.co.homeplus.claimbatch.claim.enums.MysqlFunction;
import kr.co.homeplus.claimbatch.claim.factory.EntityFactory;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import kr.co.homeplus.claimbatch.util.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ClaimMontoringSql {

    public static String selectClaimPaymentFailMonitoring() {
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class);
        SQL innerQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimPayment.paymentType, "PG"), "1", "0"
                    ),
                    "pgFailCnt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimPayment.paymentType, "PG"),
                        SqlUtils.customOperator(CustomConstants.MINUS, claimPayment.paymentAmt, claimPayment.substitutionAmt),
                        "0"
                    ),
                    "pgFailAmt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimPayment.paymentType, "MP"), "1", "0"
                    ),
                    "mhcFailCnt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimPayment.paymentType, "MP"),
                        SqlUtils.customOperator(CustomConstants.MINUS, claimPayment.paymentAmt, claimPayment.substitutionAmt),
                        "0"
                    ),
                    "mhcFailAmt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimPayment.paymentType, "PP"), "1", "0"
                    ),
                    "mileageFailCnt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimPayment.paymentType, "PP"),
                        SqlUtils.customOperator(CustomConstants.MINUS, claimPayment.paymentAmt, claimPayment.substitutionAmt),
                        "0"
                    ),
                    "mileageFailAmt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimPayment.paymentType, "OC"), "1", "0"
                    ),
                    "ocbFailCnt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimPayment.paymentType, "OC"),
                        SqlUtils.customOperator(CustomConstants.MINUS, claimPayment.paymentAmt, claimPayment.substitutionAmt),
                        "0"
                    ),
                    "ocbFailAmt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimPayment.paymentType, "DG"), "1", "0"
                    ),
                    "dgvFailCnt"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimPayment.paymentType, "DG"),
                        SqlUtils.customOperator(CustomConstants.MINUS, claimPayment.paymentAmt, claimPayment.substitutionAmt),
                        "0"
                    ),
                    "dgvFailAmt"
                )
            )
            .FROM(EntityFactory.getTableName(claimPayment))
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.paymentStatus, "P4"),
                SqlUtils.between(
                    claimPayment.chgDt,
                    SqlUtils.sqlFunction(
                        MysqlFunction.DATE_FORMAT,
                        ObjectUtils.toArray("NOW() - INTERVAL 1 HOUR", CustomConstants.YYYYMMDD.concat(" ").concat(CustomConstants.HHMISS))
                    ),
                    SqlUtils.sqlFunction(
                        MysqlFunction.DATE_FORMAT,
                        ObjectUtils.toArray("NOW()", CustomConstants.YYYYMMDD.concat(" ").concat(CustomConstants.HHMISS))
                    )
                )
            );

        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray("NOW()", CustomConstants.YYYYMMDD.concat(" ").concat(CustomConstants.HHMISS)),
                    "sendTime"
                ),
                SqlUtils.sqlFunction(MysqlFunction.COUNT, "1", "totalCnt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "pgFailCnt", "pgFailCnt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "pgFailAmt", "pgFailAmt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "mhcFailCnt", "mhcFailCnt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "mhcFailAmt", "mhcFailAmt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "mileageFailCnt", "mileageFailCnt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "mileageFailAmt", "mileageFailAmt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "ocbFailCnt", "ocbFailCnt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "ocbFailAmt", "ocbFailAmt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "dgvFailCnt", "dgvFailCnt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "dgvFailAmt", "dgvFailAmt")
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "Z"));
        log.info("selectClaimPaymentFailMonitoring :: \n[{}]", query.toString());
        return query.toString();
    }

}
