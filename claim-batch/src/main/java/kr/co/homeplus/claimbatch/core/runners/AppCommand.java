package kr.co.homeplus.claimbatch.core.runners;

import kr.co.homeplus.claimbatch.claim.service.ClaimService;
import kr.co.homeplus.claimbatch.claim.service.ShippingService;
import kr.co.homeplus.claimbatch.history.model.BatchHistoryDto;
import kr.co.homeplus.claimbatch.history.service.BatchHistoryService;
import kr.co.homeplus.claimbatch.monitoring.service.ClaimMonitoringService;
import kr.co.homeplus.claimbatch.statistics.service.StatisticsService;
import kr.co.homeplus.claimbatch.pine.service.PineReSendService;
import kr.co.homeplus.claimbatch.pine.service.PineReceiptOmitService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import picocli.CommandLine.*;

import java.util.concurrent.Callable;
import kr.co.homeplus.claimbatch.claim.maket.naver.service.MarketClaimService;

@Component
@RequiredArgsConstructor
@Slf4j
@Command(name = "java -jar claim-batch.jar", mixinStandardHelpOptions = true,
        version = "0.0.1",
        description = "claim-batch command")
public class AppCommand implements Callable<Integer>, IExitCodeExceptionMapper {

    private final PineReceiptOmitService pineReceiptOmitService;

    private final BatchHistoryService batchHistoryService;

    private final ClaimService claimService;

    private final ShippingService shippingService;

    private final StatisticsService statisticsService;

    private final PineReSendService pineReSendService;

    private final MarketClaimService marketClaimService;

    private final ClaimMonitoringService monitoringService;

    @ArgGroup(exclusive = true, multiplicity = "1")
    private Exclusive exclusive;

    static class Exclusive {
        @Option(names = {"-n", "--name"}, description = "service name")
        private boolean isName;
    }
    // Index[0] - 배치 serviceName
    @Parameters(index = "0", paramLabel = "service_name", description = "positional params")
    private String serviceName;
    // Index[1] - 추가 parameter 필요한 경우 사용
    @Parameters(index = "1", paramLabel = "meta1", description = "add param1", defaultValue = "")
    // posNo
    private String meta1;
    // Index[2] - 추가 parameter 필요한 경우 사용
    @Parameters(index = "2", paramLabel = "meta2", description = "add param2", defaultValue = "")
    // storeId
    private String meta2;
    // Index[3] - 추가 parameter 필요한 경우 사용
    @Parameters(index = "3", paramLabel = "meta3", description = "add param3", defaultValue = "")
    // tradeDt
    private String meta3;
    // Index[4] - 추가 parameter 필요한 경우 사용
    @Parameters(index = "4", paramLabel = "meta4", description = "add param4", defaultValue = "")
    // claimOmitNo
    private String meta4;

    @Override
    public Integer call() {
        log.info("exclusive.isName {}, service_name {}", exclusive.isName, serviceName);

        if(exclusive.isName) {
            // log insert
            BatchHistoryDto batchHistoryDto = new BatchHistoryDto();
            batchHistoryDto.setBatchName(serviceName);

            switch (serviceName) {
                case "createClaimPinePosOmit":
                    pineReceiptOmitService.createClaimPineTradeNoOmit(batchHistoryDto);
                    pineReceiptOmitService.sendClaimOmitData(batchHistoryDto);
                    break;
                case "onlySendTradeNoOmit":
                    pineReceiptOmitService.sendClaimOmitData(batchHistoryDto);
                    break;
                case "inputClaimPineOmitIssue":
                    pineReceiptOmitService.inputClaimPineOmitIssue(batchHistoryDto, meta1);
                    break;
                case "onlyCreateTradeNoOmit":
                    pineReceiptOmitService.createClaimPineTradeNoOmit(batchHistoryDto);
                    break;
                case "pineReSendClaimNo":
                    pineReSendService.pineReSendClaimNo(batchHistoryDto, meta1);
                    break;
                case "pineReSendPurchaseStoreInfoNo":
                    pineReSendService.pineReSendPurchaseStoreInfoNo(batchHistoryDto, meta1);
                    break;
                case "pineReSendClaimStoreInfoNo":
                    pineReSendService.pineReSendClaimStoreInfoNo(batchHistoryDto, meta1);
                    break;
                case "pineReSendShipClaimStoreInfoNo":
                    pineReSendService.pineReSendShipClaimStoreInfoNo(batchHistoryDto, meta1);
                    break;
                case "pineReSendExecute":
                    pineReSendService.pineReSendExecute(batchHistoryDto);
                    break;
                case "orderGiftAutoCancel" :
                    claimService.orderGiftAutoCancel(meta1);
                    break;
                case "claimPaymentCancel" :
                    claimService.claimPaymentCancel(meta1);
                    break;
                case "claimPaymentCancelCntRetry" :
                    claimService.claimPaymentRetryCancel(meta1);
                    break;
                case "claimPaymentMonitor" :
                    monitoringService.claimMonitoringProcess();
                    break;
                case "claimWithdraw" :
                    claimService.claimStatusWithdraw(meta1, meta2);
                    break;
                case "claimPartnerPaymentCancel" :
                    claimService.claimPartnerPaymentCancel(meta1);
                    break;
                case "claimPaymentInputManual" :
                    claimService.claimPaymentInputManual(meta1, meta2);
                    break;
                case "couponMinusRefund" :
                    claimService.claimMinusRefundInfo(meta1);
                    break;
                case "notReceivedShippingComplete" :
                    shippingService.shipNotReceivedShippingComplete(meta1);
                    break;
                case "notReceivedProcess" :
                    shippingService.shipNotReceived(meta1);
                    break;
                case "orderSubstitutionStatistics" :
                    statisticsService.orderSubstitutionStatistics(meta1);
                    break;
                case "naverClaimService" :
                    marketClaimService.getNaverClaimList(meta1, meta2);
                    break;
                case "naverManualClaimService" :
                    marketClaimService.getNaverClaimList(meta1, meta2);
                    break;
                case "marketClaimProcess" :
                    marketClaimService.marketClaimProcess(meta1);
                    break;
                case "marketClaimManualProcess" :
                    marketClaimService.marketClaimProcess(meta1);
                    break;
                case "naverClaimRetry" :
                    marketClaimService.getNaverClaimListRetry(meta1);
                    break;
                default:
                    batchHistoryDto.setMessage("service name 오입력");
                    batchHistoryService.insertBatchHistory(batchHistoryDto);
                    break;
            }
        }
        return ExitCode.OK;
    }

    @Override
    public int getExitCode(Throwable exception) {
        Throwable cause = exception.getCause();
        if (cause instanceof ArrayIndexOutOfBoundsException) {
            return 12;
        } else if (cause instanceof NumberFormatException) {
            return 13;
        }
        return 11;
    }

}