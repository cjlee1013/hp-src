package kr.co.homeplus.claimbatch.claim.entity.ship;

public class ShippingNonRcvEntity {

    // 구매주문번호
    public String purchaseOrderNo;

    // 주문상품번호
    public String orderItemNo;

    // 배송번호
    public String shipNo;

    // 번들번호
    public String bundleNo;

    // 고객번호
    public String userNo;

    // 파트너ID
    public String partnerId;

    // 미수취 신고 유형
    public String noRcvDeclrType;

    // 미수취 상세 사유
    public String noRcvDetailReason;

    // 미수취 신고 일자
    public String noRcvDeclrDt;

    // 미수취 처리 일자
    public String noRcvProcessDt;

    // 미수취처리유형(D : 재배송(사용안함) C : 수취확인 요청 W : 고객요청에 의한 미수취신고 철회)
    public String noRcvProcessType;

    // 처리내용
    public String noRcvProcessCntnt;

    // 처리여부
    public String noRcvProcessYn;

    // 철회여부
    public String noRcvWthdrwYn;

    // 미수취 등록자(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  admin : 사번으로 인식하여 사원정보 노출)
    public String regId;

    // 미수취 등록일자
    public String regDt;

    // 미수취 수정자(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  admin : 사번으로 인식하여 사원정보 노출)
    public String chgId;

    // 미수취 수정일자
    public String chgDt;

}
