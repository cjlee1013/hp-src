package kr.co.homeplus.claimbatch.pine.service.impl;

import kr.co.homeplus.claimbatch.history.model.BatchHistoryDto;
import kr.co.homeplus.claimbatch.history.service.BatchHistoryService;
import kr.co.homeplus.claimbatch.pine.mapper.PineReSendMasterMapper;
import kr.co.homeplus.claimbatch.pine.model.PineReSendDto;
import kr.co.homeplus.claimbatch.pine.service.PineReSendService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * PINE 전송 후 금액이 잘못 게산되어 다시 재송신을 하는 경우
 * 1. 클레임 번호로 다시 생성(데이터 오류) - claim_no 기준으로 모두 재송신
 * 2. 주문상점번호(purchaseStoreInfoNo)로 클레임 데이터 생성
 * 3. 클레임상점번호(claimStoreInfoNo)로 재송신 - 같은 claim_no 로 TD/DS 상품이 동시에 존재하는데 그중 하나만 다시 재송신 하는 경우
 * 4. 클레임상점번호(claimStoreInfoNo)로 재송신 - 클레임에서 반품배송비의 경우는 주문으로 송신 해당건을 삭제하기 위한 클레임 생성
 * 5. 모든 대상을 PINE으로 송신한다. (확인을 위해 별도로 생성)
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PineReSendServiceImpl implements PineReSendService {

    private final PineReSendMasterMapper pineReSendMasterMapper;
    private final BatchHistoryService batchHistoryService;

    // 클레임 번호로 다시 생성(데이터 오류) - claim_no 기준으로 모두 재송신
    // 기존의 클레임은 주문으로 상계처리 하고 다시 데이터를 생성한다.(반품배송비등이 추가로 생길수 있다)
    public void pineReSendClaimNo(BatchHistoryDto batchHistoryDto, String claimNo){
        // 1. resend_claim_pine_target insert
        int result = paramCheck(batchHistoryDto, claimNo);
        if( result == 0 ){
            return;
        }
        log.info("Manual pineReSendClaimNo Start [claimNo] ["+claimNo+"] " );

        PineReSendDto pineReSendDto = new PineReSendDto();
        pineReSendDto.setClaimNo(claimNo);

        try {
            pineReSendMasterMapper.insertReSendClaim(pineReSendDto);
            log.info("Manual pineReSendClaimNo 데이터 Success [claimNo] ["+claimNo+"] ");
        } catch( Exception e ){
            log.info("Manual pineReSendClaimNo 데이터 생성시 Error 발생 [claimNo] ["+claimNo+"] "+e.getMessage() );
            batchHistoryDto.setMessage("PineReSend 데이터 생성시 Error 발생 ["+e.getMessage()+"]");
        } finally {
            batchHistoryService.insertBatchHistory(batchHistoryDto);
        }
    }

    // 주문상점번호(purchaseStoreInfoNo)로 클레임 데이터 생성하여 송신
    public void pineReSendPurchaseStoreInfoNo(BatchHistoryDto batchHistoryDto, String purchaseStoreInfoNo){
        // 1. resend_claim_pine_target insert
        int result = paramCheck(batchHistoryDto, purchaseStoreInfoNo);
        if( result == 0 ){
            return;
        }
        log.info("Manual pineReSendPurchaseStoreInfoNo Start [purchaseStoreInfoNo] ["+purchaseStoreInfoNo+"] " );

        PineReSendDto pineReSendDto = new PineReSendDto();
        pineReSendDto.setPurchaseStoreInfoNo(purchaseStoreInfoNo);

        try {
            pineReSendMasterMapper.insertReSendClaim(pineReSendDto);
            log.info("Manual pineReSendPurchaseStoreInfoNo 데이터 Success [purchaseStoreInfoNo] ["+purchaseStoreInfoNo+"] ");
        } catch( Exception e ){
            log.info("Manual pineReSendPurchaseStoreInfoNo 데이터 생성시 Error 발생 [purchaseStoreInfoNo] ["+purchaseStoreInfoNo+"] "+e.getMessage() );
            batchHistoryDto.setMessage("PineReSend 데이터 생성시 Error 발생 ["+e.getMessage()+"]");
        } finally {
            batchHistoryService.insertBatchHistory(batchHistoryDto);
        }
    }

    // 클레임상점번호(claimStoreInfoNo)로 재송신 - 같은 claim_no 로 TD/DS 상품이 동시에 존재하는데 그중 하나만 다시 재송신 하는 경우
    public void pineReSendClaimStoreInfoNo(BatchHistoryDto batchHistoryDto, String claimStoreInfoNo){
        // 1. resend_claim_pine_target insert
        int result = paramCheck(batchHistoryDto, claimStoreInfoNo);
        if( result == 0 ){
            return;
        }
        log.info("Manual pineReSendClaimStoreInfoNo Start [claimStoreInfoNo] ["+claimStoreInfoNo+"] " );
        PineReSendDto pineReSendDto = new PineReSendDto();
        pineReSendDto.setClaimStoreInfoNo(claimStoreInfoNo);

        try {
            pineReSendMasterMapper.insertReSendClaim(pineReSendDto);
            log.info("Manual pineReSendClaimStoreInfoNo 데이터 Success [claimStoreInfoNo] ["+claimStoreInfoNo+"] ");
        } catch( Exception e ){
            log.info("Manual pineReSendClaimStoreInfoNo 데이터 생성시 Error 발생 [claimStoreInfoNo] ["+claimStoreInfoNo+"] "+e.getMessage() );
            batchHistoryDto.setMessage("PineReSend 데이터 생성시 Error 발생 ["+e.getMessage()+"]");
        } finally {
            batchHistoryService.insertBatchHistory(batchHistoryDto);
        }
    }

    // 클레임상점번호(claimStoreInfoNo)로 재송신 - 클레임에서 반품배송비의 경우는 주문으로 송신 해당건을 삭제하기 위한 클레임 생성
    public void pineReSendShipClaimStoreInfoNo(BatchHistoryDto batchHistoryDto, String shipClaimStoreInfoNo){
        // 1. resend_claim_pine_target insert
        int result = paramCheck(batchHistoryDto, shipClaimStoreInfoNo);
        if( result == 0 ){
            return;
        }
        log.info("Manual pineReSendShipClaimStoreInfoNo Start [shipClaimStoreInfoNo] ["+shipClaimStoreInfoNo+"] " );
        PineReSendDto pineReSendDto = new PineReSendDto();
        pineReSendDto.setShipClaimStoreInfoNo(shipClaimStoreInfoNo);

        try {
            pineReSendMasterMapper.insertReSendClaim(pineReSendDto);
            log.info("Manual pineReSendShipClaimStoreInfoNo 데이터 Success [shipClaimStoreInfoNo] ["+shipClaimStoreInfoNo+"] ");
        } catch( Exception e ){
            log.info("Manual pineReSendShipClaimStoreInfoNo 데이터 생성시 Error 발생 [shipClaimStoreInfoNo] ["+shipClaimStoreInfoNo+"] "+e.getMessage() );
            batchHistoryDto.setMessage("PineReSend 데이터 생성시 Error 발생 ["+e.getMessage()+"]");
        } finally {
            batchHistoryService.insertBatchHistory(batchHistoryDto);
        }
     }

     // 재송신 대상을 PINE으로 송신한다.
    public void pineReSendExecute(BatchHistoryDto batchHistoryDto){
        // call up_tx_claim_resend_pine_multi
        // up_tx_claim_resend_pine_multi 에서 resend_claim_pine_target 테이블을 참조하여 각각 데이터 생성하여 송신환다.
        try {
            pineReSendMasterMapper.pineReSendExecute();
            log.info("Manual pineReSendExecute 데이터 Success ");
        } catch( Exception e ){
            log.info("Manual pineReSendExecute 데이터 생성시 Error 발생 "+e.getMessage() );
            batchHistoryDto.setMessage("Manual pineReSendExecute Error 발생 ["+e.getMessage()+"]");
        } finally {
            batchHistoryService.insertBatchHistory(batchHistoryDto);
        }
    }

    // 파라미터 체크
    private int paramCheck(BatchHistoryDto batchHistoryDto, String meta1){
        if( StringUtils.isBlank(meta1) ){
            log.info("Manual PineReSendService Start [meta1] ["+meta1+"] 파라미터 null " );
            batchHistoryDto.setMessage("파라미터 NULL");
            batchHistoryService.insertBatchHistory(batchHistoryDto);
            return 0;
        }
        return 1;
    }
}
