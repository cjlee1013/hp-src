package kr.co.homeplus.claimbatch.claim.maket.naver.model;

import kr.co.homeplus.claimbatch.claim.maket.naver.constants.NaverProductOrderChangeType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NaverClaimListSetDto {
    private String startDt;
    private String endDt;
    private String productOrderChangeType;
}
