package kr.co.homeplus.claimbatch.pine.mapper;

import kr.co.homeplus.claimbatch.core.db.annotation.SlaveConnection;
import kr.co.homeplus.claimbatch.pine.model.ClaimPosReceiptIssueDto;
import kr.co.homeplus.claimbatch.pine.model.ClaimPineReceiptOmitTranDto;

import java.util.List;

@SlaveConnection
public interface PineReceiptOmitSlaveMapper {

    // 영수증채번 테이블에서 금일 조회해야할 POS_NO, STORE_ID를 취득한다.
    List<ClaimPosReceiptIssueDto> selectClaimPosReceiptIssueList(ClaimPosReceiptIssueDto claimPosReceiptIssueDto);

    // 누락된 POS번호에 대해 PINE에 송신할 데이터를 취득한다.
    List<ClaimPineReceiptOmitTranDto> selectClaimPineReceiptOmitMngList(ClaimPineReceiptOmitTranDto claimPineReceiptOmitTranDto);

    // 송신되지 못한 데이터를 취득하여 데이터를 생성한다.
    List<ClaimPineReceiptOmitTranDto> selectClaimPineReceiptOmitIssueList(ClaimPineReceiptOmitTranDto claimPineReceiptOmitTranDto);
}
