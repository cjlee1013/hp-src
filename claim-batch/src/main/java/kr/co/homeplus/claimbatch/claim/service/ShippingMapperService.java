package kr.co.homeplus.claimbatch.claim.service;

import java.util.LinkedHashMap;
import java.util.List;

public interface ShippingMapperService {

    /**
     * 미수취 철회요청 장기미처리건 조회
     *
     * @param processDt
     * @return
     */
    List<LinkedHashMap<String, Object>> getRequestWithdrawalOfUnreceivedDeliveryInfo(String processDt);

    /**
     * 미수취 철회신청 장기 미처리건.
     *
     * @param processDt
     * @return
     */
    List<LinkedHashMap<String, Object>> getNonReceivedShippingCompleteInfo(String processDt);

    /**
     * 미수취 철회시 배송정보 확인
     *
     * @param purchaseOrderNo
     * @param bundleNo
     * @return
     */
    LinkedHashMap<String, Object> getNonShipCompleteInfo(long purchaseOrderNo, long bundleNo);

    /**
     * 장기미처리 자동철회
     *
     * @param purchaseOrderNo
     * @param bundleNo
     * @return
     */
    boolean modifyUnreceivedDeliveryWithdrawal(long purchaseOrderNo, long bundleNo);

    /**
     * 미수취 신고장기미처리 자동철회
     * @param purchaseOrderNo
     * @param bundleNo
     * @return
     */
    boolean modifyUnReceivedShippingCompleteWithdrawal(long purchaseOrderNo, long bundleNo);
}
