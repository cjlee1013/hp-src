package kr.co.homeplus.claimbatch.claim.entity.multi;

public class MultiSafetyIssueEntity {
    // 다중배송주소번호
    public String multiShipAddrNo;
    // 구매주문번호
    public String purchaseOrderNo;
    // 번들번호
    public String bundleNo;
    // 다중번들번호
    public String multiBundleNo;
    // 요청연락처번호(발급요청 전화번호)
    public String reqPhoneNo;
    // 발급연락처번호(발급된 안심번호 0504XXXXXXXX)
    public String issuePhoneNo;
    // 발급유형(N:발급(신규발급),U:변경(착신번호변경))
    public String issueType;
    // 발급상태(R:요청중, C:완료, F:실패)
    public String issueStatus;
    // 발급오류(연동업체 실패코드)
    public String issueErrorCd;
    // 발급일자
    public String issueDt;
    // 만료일자(안심번호유효기간, (개발환경:발급시점+5분 / 운영환경:발급시점 + 3주)
    public String expireDt;
    // 등록일시
    public String regDt;
    // 수정일시(착신전화번호 변경 일자)
    public String chgDt;

}
