package kr.co.homeplus.claimbatch.claim.model.register;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임등록시 교환/반품 상세 정보.")
public class ClaimRegDetailDto {

    @ApiModelProperty(value = "동봉여부", position = 1, required = true)
    private String isEnclose;

    @ApiModelProperty(value = "판매자발송여부", position = 2, required = true)
    private String isPick;

    @ApiModelProperty(value = "수거여부", position = 3, required = true)
    private String isPickReq;

    @ApiModelProperty(value = "수거일자", position = 4, required = true)
    private String shipDt;

    @ApiModelProperty(value = "수거ShiftId", position = 5)
    private String shiftId;

    @ApiModelProperty(value = "수거slotId", position = 6)
    private String slotId;

    @ApiModelProperty(value = "수거송장번호(구매자가직접보낼경우에만)", position = 7)
    private String pickInvoiceNo;

    @ApiModelProperty(value = "수거택배사코드(구매자가직접보낼경우에만)", position = 8)
    private String pickDlvCd;

    @ApiModelProperty(value = "수거고객명", position = 9, required = true)
    private String pickReceiverNm;

    @ApiModelProperty(value = "수거우편번호", position = 10, required = true)
    private String pickZipCode;

    @ApiModelProperty(value = "수거도로명주소", position = 11, required = true)
    private String pickRoadBaseAddr;

    @ApiModelProperty(value = "수거도로명상세주소", position = 12, required = true)
    private String pickRoadDetailAddr;

    @ApiModelProperty(value = "수거일반주소", position = 13)
    private String pickBaseAddr;

    @ApiModelProperty(value = "수거일반상세주소", position = 14)
    private String pickBaseDetailAddr;

    @ApiModelProperty(value = "수거대상연락처", position = 15, required = true)
    private String pickMobileNo;

    @ApiModelProperty(value = "교환수신자명", position = 10)
    private String exchReceiverNm;

    @ApiModelProperty(value = "교환우편번호", position = 10)
    private String exchZipCode;

    @ApiModelProperty(value = "교환도로명주소", position = 11)
    private String exchRoadBaseAddr;

    @ApiModelProperty(value = "교환도로명상세주소", position = 12)
    private String exchRoadDetailAddr;

    @ApiModelProperty(value = "교환일반주소", position = 13)
    private String exchBaseAddr;

    @ApiModelProperty(value = "교환일반상세주소", position = 14)
    private String exchBaseDetailAddr;

    @ApiModelProperty(value = "교환받는사람연락처", position = 15)
    private String exchMobileNo;

    @ApiModelProperty(value = "사진첨부1", position = 16)
    private String uploadFileName;

    @ApiModelProperty(value = "사진첨부2", position = 17)
    private String uploadFileName2;

    @ApiModelProperty(value = "사진첨부3", position = 18)
    private String uploadFileName3;

    @ApiModelProperty(value = "수거등록자아이디(admin:userCd, mypage:mypage, partner:partnerId", position = 19, required = true)
    private String pickRegId;

    @ApiModelProperty(value = "고객번호", position = 20, hidden = true)
    private String userNo;

    @ApiModelProperty(value = "배송타입", position = 21, hidden = true)
    private String shipType;
}
