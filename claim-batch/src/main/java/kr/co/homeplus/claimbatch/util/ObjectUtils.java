package kr.co.homeplus.claimbatch.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.util.ReflectionUtils;

public class ObjectUtils {

    /**
     * Object to Map[bean]
     *
     * @param object DTO 클래스
     * @return HashMap
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public static LinkedHashMap<String, Object> getConvertMap(Object object) throws Exception{
        if(object == null){
            return null;
        }

        LinkedHashMap<String, Object> returnMap = new LinkedHashMap<>();
        Class<?> clz = object.getClass();

        for(Field field : clz.getDeclaredFields()){
            if(Modifier.isPrivate(field.getModifiers())){
                ReflectionUtils.makeAccessible(field);
            }
            returnMap.put(field.getName(), field.get(object));
        }
        return returnMap;
    }

    public static LinkedHashMap<String, String> getConvertStrMap(Object object) throws Exception{
        if(object == null){
            return null;
        }

        LinkedHashMap<String, String> returnMap = new LinkedHashMap<>();
        Class<?> clz = object.getClass();

        for(Field field : clz.getDeclaredFields()){
            if(Modifier.isPrivate(field.getModifiers())){
                ReflectionUtils.makeAccessible(field);
            }
            returnMap.put(field.getName(), String.valueOf(field.get(object)));
        }
        return returnMap;
    }

    public static Long toLong(Object obj) {
        return NumberUtils.toLong(String.valueOf(obj));
    }

    public static Integer toInt(Object obj) {
        return NumberUtils.toInt(String.valueOf(obj));
    }

    public static String toString(Object obj) {
        return toString(obj, null);
    }

    public static String toString(Object obj, String defaultStr) {
        if(obj != null){
            return obj.toString();
        }
        return defaultStr;
    }

    public static Double toDouble(Object obj) {
        return NumberUtils.toDouble(String.valueOf(obj));
    }

    public static Object[] toArray(Object... objects){
        return Arrays.stream(objects).toArray();
    }

    public static List<Object> toList(Object... objects) {
        return Arrays.asList(objects);
    }

    public static Boolean toBoolean(Object obj) {
        return Boolean.getBoolean(String.valueOf(obj));
    }

    public static String[] toStrArray(Map<String, Object> map, String... keys) {
        return Arrays.stream(toArray(map, keys)).map(String::valueOf).toArray(String[]::new);
    }

    public static Object[] toArray(Map<String, Object> map, String... keys){
        List<Object> returnList = new ArrayList<>();
        for(String key : keys){
            if(map.containsKey(key)){
                returnList.add(map.get(key));
            }
        }
        return returnList.toArray(Object[]::new);
    }

    public static LinkedHashMap<String, Object> toMap(Map map, String... keys) {
        return convertMapToMap(Boolean.FALSE, map, keys);
    }

    public static LinkedHashMap<String, Object> toCamelMap(Map map, String... keys){
        return convertMapToMap(Boolean.TRUE, map, keys);
    }

    public static boolean isEquals(Object target, Object compare) {
        if(target == null || compare == null){
            return Boolean.FALSE;
        }
        return String.valueOf(target).equals(String.valueOf(compare));
    }
    public static boolean isEqualsIgnoreCase(Object target, Object compare){
        if(target == null || compare == null){
            return Boolean.FALSE;
        }
        return String.valueOf(target).equalsIgnoreCase(String.valueOf(compare));
    }

    public static boolean isCompareMap(Map sourceMap, Map compareMap, String key){
        if (sourceMap == null || compareMap == null){
            return Boolean.FALSE;
        }
        return isEquals(sourceMap.get(key), compareMap.get(key));
    }

    public static boolean isMapEmpty(Map map){
        if(map == null) {
            return Boolean.TRUE;
        }
        return map.isEmpty();
    }

    private static LinkedHashMap<String, Object> convertMapToMap(boolean isCamel, Map map, String... keys) {
        if(map != null) {
            LinkedHashMap<String, Object> returnMap = new LinkedHashMap<>();
            for(String key : keys){
                if(map.get(key) != null){
                    returnMap.put(isCamel ? snakeCaseToCamelCase(key) : key, map.get(key));
                }
            }
            return returnMap;
        }
        return null;
    }

    private static String snakeCaseToCamelCase(String snakeCase){
        if(StringUtils.isNotEmpty(snakeCase)){
            StringBuilder camelCase = new StringBuilder();
            for(String param : snakeCase.split("_")){
                if(camelCase.length() == 0){
                    camelCase.append(param);
                } else {
                    camelCase.append(StringUtils.capitalize(param));
                }
            }
            return camelCase.toString();
        }
        return null;
    }
}
