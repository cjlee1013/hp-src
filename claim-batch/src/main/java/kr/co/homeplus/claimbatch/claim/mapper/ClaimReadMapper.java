package kr.co.homeplus.claimbatch.claim.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claimbatch.claim.model.payment.ClaimCouponReIssueSetDto.CouponReIssueDto;
import kr.co.homeplus.claimbatch.claim.model.payment.RestoreStockItemDto;
import kr.co.homeplus.claimbatch.claim.model.payment.RestoreStockItemDto.RestoreStoreItemListDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimInfoRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.omni.OmniOriginOrderDto;
import kr.co.homeplus.claimbatch.claim.sql.ClaimManagementSql;
import kr.co.homeplus.claimbatch.claim.sql.ClaimMinusRefundSql;
import kr.co.homeplus.claimbatch.claim.sql.ClaimOrderCancelSql;
import kr.co.homeplus.claimbatch.claim.sql.ClaimOrderTicketSql;
import kr.co.homeplus.claimbatch.claim.sql.OrderDataSql;
import kr.co.homeplus.claimbatch.core.db.annotation.SlaveConnection;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.mapping.StatementType;

@SlaveConnection
public interface ClaimReadMapper {

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "callByClaimPreRefundPriceCalculation")
    ClaimPreRefundCalcGetDto callByClaimPreRefundPriceCalculation(LinkedHashMap<String, Object> parameterMap);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "callByClaimPreRefundPriceMultiCalculation")
    ClaimPreRefundCalcGetDto callByClaimPreRefundPriceMultiCalculation(LinkedHashMap<String, Object> parameterMap);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "callByClaimPreRefundPriceMarketCalculation")
    ClaimPreRefundCalcGetDto callByClaimPreRefundPriceMarketCalculation(LinkedHashMap<String, Object> parameterMap);

    @SelectProvider(type = OrderDataSql.class, method = "selectCreateClaimDataInfo")
    LinkedHashMap<String, Object> selectCreateClaimDataInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = OrderDataSql.class, method = "selectClaimItemInfo")
    LinkedHashMap<String, Object> selectClaimItemInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderItemNo") long orderItemNo);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectOriPaymentForClaim")
    LinkedHashMap<String, Object> selectOriPaymentForClaim(@Param("paymentNo") long paymentNo, @Param("paymentType") String paymentType);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectOrderOptInfoForClaim")
    ClaimOptRegDto selectOrderOptInfoForClaim(@Param("orderItemNo") long orderItemNo, @Param("orderOptNo") String orderOptNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectOrderBundleByClaim")
    List<LinkedHashMap<String, Object>> selectOrderBundleByClaim(@Param("purchaseOrderNo") long purchaseOrderNo);

    @ResultType(value = ClaimPreRefundItemList.class)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectOrderItemInfoByClaim")
    List<ClaimPreRefundItemList> selectOrderItemInfoByClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @ResultType(value = ClaimPreRefundItemList.class)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectOrderItemInfoByMarketClaim")
    List<ClaimPreRefundItemList> selectOrderItemInfoByMarketClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo, @Param("marketOrderItemNo") String marketOrderItemNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectOrderByClaimPartInfo")
    LinkedHashMap<String, Object> selectOrderByClaimPartInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = OrderDataSql.class, method = "selectCreateClaimMstDataInfo")
    LinkedHashMap<String, Object> selectCreateClaimMstDataInfo(@Param("purchaseOrderNo") long purchaseOrderNo);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectClaimPossibleCheck")
    LinkedHashMap<String, Object> selectClaimPossibleCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimCouponReIssue")
    List<CouponReIssueDto> selectClaimCouponReIssue(@Param("claimNo") long claimNo, @Param("userNo") long userNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimAddCouponReIssue")
    List<CouponReIssueDto> selectClaimAddCouponReIssue(@Param("claimNo") long claimNo, @Param("userNo") long userNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimInfo")
    List<LinkedHashMap<String, Object>> selectClaimInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimRegDataInfo")
    ClaimInfoRegDto selectClaimRegDataInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderTicketSql.class, method = "selectOrderTicketInfo")
    List<LinkedHashMap<String, Object>> selectOrderTicketInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderItemNo") long orderItemNo, @Param("cancelCnt") int cancelCnt);

    @SelectProvider(type = ClaimOrderTicketSql.class, method = "selectClaimTicketInfo")
    List<LinkedHashMap<String, Object>> selectClaimTicketInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimOrderTypeCheck")
    LinkedHashMap<String, String> selectClaimOrderTypeCheck(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectCreateClaimAccumulateInfo")
    List<LinkedHashMap<String, Object>> selectCreateClaimAccumulateInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectAccumulateCancelInfo")
    LinkedHashMap<String, Object> selectAccumulateCancelInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("pointKind") String pointKind);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectOriginAndCombineClaimCheck")
    List<LinkedHashMap<String, Object>> selectOriginAndCombineClaimCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("searchType") String searchType);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectPreRefundRecheckData")
    ClaimPreRefundCalcSetDto selectPreRefundRecheckData(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectPreRefundItemData")
    List<ClaimPreRefundItemList> selectPreRefundItemData(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectPreRefundCompareData")
    ClaimMstRegDto selectPreRefundCompareData(@Param("claimNo") long claimNo);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "callByClaimPreRefundReCheckCalculation")
    ClaimPreRefundCalcGetDto callByClaimPreRefundReCheckCalculation(LinkedHashMap<String, Object> parameterMap);

    @SelectProvider(type = OrderDataSql.class, method = "selectPresentAutoCancelInfo")
    List<LinkedHashMap<String, Object>> selectPresentAutoCancelInfo(@Param("processDt") String processDt);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimApprovalData")
    List<LinkedHashMap<String, Object>> selectClaimApprovalData(@Param("claimNo") long claimNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimBundleTdInfo")
    LinkedHashMap<String, String> selectClaimBundleTdInfo(@Param("claimNo") long claimNo);
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectMileagePaybackCheck")
    LinkedHashMap<String, Object> selectMileagePaybackCheck(@Param("bundleNo") long bundleNo);
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectMileagePaybackInfo")
    LinkedHashMap<String, Object> selectMileagePaybackInfo(@Param("purchaseOrderNo") long purchaseOrderNo);
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectMileagePaybackCancelInfo")
    LinkedHashMap<String, Object> selectMileagePaybackCancelInfo(@Param("bundleNo") long bundleNo, @Param("orgBundleNo") long orgBundleNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectRestoreStockItemInfo")
    List<RestoreStockItemDto> selectRestoreStockItemInfo(@Param("claimNo") long claimNo);
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectRestoreStoreItemList")
    List<RestoreStoreItemListDto> selectRestoreStoreItemList(@Param("claimBundleNo") long claimBundleNo);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimPickIsAutoInfo")
    LinkedHashMap<String, Object> selectClaimPickIsAutoInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = ClaimMinusRefundSql.class, method = "selectClaimMinusRefundTarget")
    List<Long> selectClaimMinusRefundTarget(@Param("claimNo") long claimNo);
    @SelectProvider(type = ClaimMinusRefundSql.class, method = "selectMinusRefundCouponCorrect")
    Long selectMinusRefundCouponCorrect(@Param("claimNo") long claimNo);
    @SelectProvider(type = ClaimMinusRefundSql.class, method = "selectClaimMinusRefundPreInfo")
    LinkedHashMap<String, Object> selectClaimMinusRefundPreInfo(@Param("claimNo") long claimNo);

    @SelectProvider(type = OrderDataSql.class, method = "selectOriginOrderListByOmni")
    List<OmniOriginOrderDto> selectOriginOrderListByOmni(@Param("purchaseOrderNo") long purchaseOrderNo);
    //
    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimPartnerApproval")
    List<Long> selectClaimPartnerApproval();

}
