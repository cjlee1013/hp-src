package kr.co.homeplus.claimbatch.claim.maket.entity;

public class MarketClaimItemEntity {
    // 마켓주문클레임상품순번
    public String marketClaimItemNo;
    // 마켓클레임번호
    public String marketClaimNo;
    // 마켓유형
    public String marketType;
    // 업체주문상품번호
    public String marketOrderItemNo;
    // 업체주문번호
    public String marketOrderNo;
    // 클레임타입
    public String claimType;
    // 클레임상태
    public String claimStatus;
    // 클레임사유
    public String claimReason;
    // 클레임상세사유
    public String claimReasonDetail;
    // 상품번호
    public String itemNo;
    // 상품명
    public String itemNm;
    // 상품옵션
    public String itemOpt;
    // 사은품
    public String gift;
    // 업체상품번호
    public String marketItemNo;
    // 상품수량
    public String itemQty;
    // 상품가격
    public String itemPrice;
    // 상품옵션가격
    public String itemOptPrice;
    // 상품할인금액
    public String itemDiscountAmt;
    // 배송메모
    public String shipMemo;
    // 출입방법내용
    public String accesMethodCntnt;
    // 픽업장소내용
    public String pickupPlaceCntnt;
    // 배송비금액
    public String shipAmt;
    // 결제금액
    public String paymentAmt;
    // 등록일시
    public String regDt;
    // 등록자ID
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정자ID
    public String chgId;
    // 거래일시
    public String tradeDt;
    // 대체여부
    public String substitutionYn;
    // 원상품수량
    public String orgItemQty;
    // 이벤트쿠폰번호
    public String eventCouponNo;
    // 쿠폰ID
    public String couponId;
    // 최소사용건수
    public String minUseCnt;
    // 할인유형
    public String discountType;
    // 할인값
    public String discountVal;
    // 수신여부
    public String rcvYn;
    // 쿠폰할인금액
    public String couponDiscountAmt;
    // 프로모션번호
    public String promoNo;
    // 프로모션상세번호
    public String promoDetailNo;
    // rpm프로모션상세번호
    public String rpmPromoDetailNo;
    // 프로모션유형(SIMP:simple, THRES:Threhold, MIX:MixMatch)
    public String promoType;
    // 프로모션종류(BASIC:기본할인, PICK:골라담기, TOGETHER:함께할인, INTERVAL:구간할인)
    public String promoKind;
    // 프로모션할인유형
    public String promoDiscountType;
    // 프로모션할인금액
    public String promoDiscountAmt;
    // 프로모션할인율
    public String promoDiscountRate;
    // 믹스매치유형(1:구매수량, 2:구매금액)
    public String mixMatchType;
    // 쿠폰가격
    public String couponPrice;
    // 할인가격
    public String discountPrice;
    // 판매가격
    public String salePrice;
    // 원플러스원
    public String onePlusOne;
    // 점포가격
    public String storePrice;
    // 커스텀코드1
    public String customCode1;
    // 커스텀코드2
    public String customCode2;
    // 마켓할인가격
    public String marketDiscountPrice;
    // 배송상태
    public String shipStatus;
    // 처리상태
    public String processStatus;
    // 처리실패메시지
    public String processFailMsg;
    // 점포ID
    public String storeId;
    // 배송번호
    public String dlvNo;
    // 출입방법코드
    public String entryMethodCd;
    // 출입방법내용
    public String entryMethodCntnt;
    // 마켓클레임번호
    public String marketClaimId;
    // 구매주문번호
    public String claimNo;
}
