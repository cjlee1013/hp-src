package kr.co.homeplus.claimbatch.history.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@ApiModel(description = "Batch Log 적재")
@Getter
@Setter
@EqualsAndHashCode
public class BatchHistoryDto {

    @ApiModelProperty(value = "배치SEQ", position = 1)
    private String batchSeq;

    @ApiModelProperty(value = "배치명", position = 2)
    private String batchName;

    @ApiModelProperty(value = "메시지", position = 3)
    private String message;

    @ApiModelProperty(value = "추가컬럼1", position = 4)
    private String val1;

    @ApiModelProperty(value = "추가컬럼2", position = 5)
    private String val2;

    @ApiModelProperty(value = "추가컬럼3", position = 6)
    private String val3;

    @ApiModelProperty(value = "추가컬럼4", position = 7)
    private String val4;

    @ApiModelProperty(value = "추가컬럼5", position = 8)
    private String val5;

    @ApiModelProperty(value = "기준일자(조회용)", position = 8)
    private String tradeDate;

    public void setMessage(String message){
        this.message = StringUtils.substring( ( this.message == null ? "" : this.message )
                +" "
                + ( message == null ? "":message )
                , 0 , 300 );
    }

    public void setClearMessage(){
        this.message = null;
    }
}
