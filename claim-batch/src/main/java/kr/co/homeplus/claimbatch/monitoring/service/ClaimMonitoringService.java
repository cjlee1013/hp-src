package kr.co.homeplus.claimbatch.monitoring.service;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.claimbatch.monitoring.model.ClaimPaymentMonitoringDto;
import kr.co.homeplus.claimbatch.util.MessageSendUtil;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimMonitoringService {
    private final ClaimMonitoringMapperService mapperService;

    public boolean claimMonitoringProcess() {
        try {
            ClaimPaymentMonitoringDto dto = mapperService.getMonitoringData();
            List<String> mailSenderList = new ArrayList<>(){{
                add("chongmoon.cho@homeplus.co.kr");
                add("jslee1028@homeplus.co.kr");
                add("kobebry@homeplus.co.kr");
            }};
            dto.setTotalCntCall();
            for(String sender : mailSenderList){
                MessageSendUtil.sendMail(ObjectUtils.getConvertStrMap(dto), sender);
            }
            return Boolean.TRUE;
        } catch (Exception e) {
            log.error("모니터링 메시지 발송 중 오류가 발생되었습니다. :: {}", e.getMessage());
            return Boolean.FALSE;
        }
    }
}
