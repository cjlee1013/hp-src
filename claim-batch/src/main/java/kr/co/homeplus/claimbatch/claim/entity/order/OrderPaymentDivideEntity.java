package kr.co.homeplus.claimbatch.claim.entity.order;

public class OrderPaymentDivideEntity {
    // 구매주문번호
    public String purchaseOrderNo;
    // 결제번호
    public String paymentNo;
    // 구매점포정보번호
    public String purchaseStoreInfoNo;
    // 번들번호
    public String bundleNo;
    // 주문상품번호
    public String orderItemNo;
    // 고객번호
    public String userNo;
    // 상위수단코드(CARD:신용카드)
    public String parentMethodCd;
    // (매출용 실제점포ID:0024,0003 등)
    public String originStoreId;
    // 점포ID(24:영등포,3:가좌점...)
    public String storeId;
    // 사이트유형(HOME:홈플러스,CLUB:더클럽)
    public String siteType;
    // 점포유형(HYPER, CLUB, EXP, AURORA, DS)
    public String storeType;
    // 점포종류(NOR:일반,DLV:택배점)
    public String storeKind;
    // 배송정책번호(배송비 일때만 입력)
    public String shipPolicyNo;
    // 상품번호(상품 일때만 입력)
    public String itemNo;
    // 파트너ID
    public String partnerId;
    // 파트너명(필요여부 확인)
    public String partnerNm;
    // 세금여부(Y:과세,N:면세)
    public String taxYn;
    // 문화비소득공제여부
    public String cultureCostYn;
    // 배송비가격(배송비가격)
    public String shipPrice;
    // 배송비할인금액(배송비 할인금액)
    public String shipDiscountAmt;
    // 상품가격(상품 판매가)
    public String itemPrice;
    // 옵션수량
    public String itemQty;
    // (상품수량 * 상품금액)
    public String orderPrice;
    // PG분할결제금액
    public String pgDivideAmt;
    // 마일리지분할결제금액
    public String mileageDivideAmt;
    // MHC분할결제금액
    public String mhcDivideAmt;
    // DGV분할결제금액
    public String dgvDivideAmt;
    // OCB분할결제금액
    public String ocbDivideAmt;
    // 소수점처리(UP:올림,DOWN:버림,ROUND:반올림)
    public String decpntProcess;
    // 신용카드수수료율
    public String creditCommissionRate;
    // 체크카드수수료율
    public String debitCommissionRate;
    // 수수료금액
    public String commissionAmt;
    // 임직원할인금액(임직원 할인 대상 상품별 분할 금액)
    public String empDiscountAmt;
    // 프로모션할인금액(행사 할인 적용금액, 행사금액 비례배분됨)
    public String promoDiscountAmt;
    // 쿠폰할인금액(상품쿠폰/배송비쿠폰 할인금액)
    public String itemCouponAmt;
    // 홈플러스부담쿠폰율(상품쿠폰,상품할인 분할 율)
    public String couponHomeChargeRate;
    // 홈플러스 부담쿠폰금액(상품쿠폰,상품할인 분할 금액)
    public String couponHomeChargeAmt;
    // 판매자부담쿠폰율(상품쿠폰,상품할인 분할 율)
    public String couponSellerChargeRate;
    // 판매자부담쿠폰금액(상품쿠폰,상품할인 분할 금액)
    public String couponSellerChargeAmt;
    // 카드부담쿠폰율(상품쿠폰,상품할인 분할 율)
    public String couponCardChargeRate;
    // 카드부담쿠폰금액(상품쿠폰,상품할인 분할 금액)
    public String couponCardChargeAmt;
    // 장바구니할인유형(PRICE:정액, RATE:정률)
    public String cartDiscountType;
    // 장바구니할인율
    public String cartDiscountRate;
    // 장바구니할인금액 (주문 장바구니 할인 전체금액)
    public String cartTotDiscountAmt;
    // 장바구니쿠폰금액(장바구니 쿠폰 분할 금액)
    public String cartCouponAmt;
    // 장바구니홈플러스부담율
    public String cartCouponHomeRate;
    // 홈플러스장바구니쿠폰금액(장바구니 쿠폰 분할 금액)
    public String cartCouponHomeAmt;
    // 장바구니판매자부담율
    public String cartCouponSellerRate;
    // 판매자장바구니쿠폰금액(장바구니 쿠폰 분할 금액)
    public String cartCouponSellerAmt;
    // 장바구니카드사부담율
    public String cartCouponCardRate;
}
