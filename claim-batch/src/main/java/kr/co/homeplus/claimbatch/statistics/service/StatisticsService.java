package kr.co.homeplus.claimbatch.statistics.service;

import kr.co.homeplus.claimbatch.statistics.mapper.StatisticsWriteMapper;
import kr.co.homeplus.claimbatch.util.DateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class StatisticsService {
    private final StatisticsWriteMapper statisticsWriteMapper;

    public void orderSubstitutionStatistics(String basicDt) {
        if(basicDt.isEmpty()) {
            basicDt = DateUtils.getCurrentPrevDayYmd(1);
        }
        log.info("입력받은 basic_dt : {}", basicDt);
        statisticsWriteMapper.callByOrderSubstitutionStatistics(basicDt);
    }
}
