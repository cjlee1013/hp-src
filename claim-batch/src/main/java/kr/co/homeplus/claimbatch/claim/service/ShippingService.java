package kr.co.homeplus.claimbatch.claim.service;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claimbatch.claim.model.shipping.ClaimShipCompleteDto;
import kr.co.homeplus.claimbatch.enums.DateType;
import kr.co.homeplus.claimbatch.enums.ExternalUrlInfo;
import kr.co.homeplus.claimbatch.util.DateUtils;
import kr.co.homeplus.claimbatch.util.ExternalUtil;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShippingService {

    private final ShippingMapperService shippingMapperService;

    public void shipNotReceived(String processDt) {
        // 조회일이 없으면 오늘 날짜로
        if(StringUtils.isEmpty(processDt)) {
            processDt = DateUtils.getCurrentYmd();
        }
        // 조회기준보다 전일자 미수취 미철회 요청건을 가지고 온다.
        List<LinkedHashMap<String, Object>> unWithdrawalList = shippingMapperService.getRequestWithdrawalOfUnreceivedDeliveryInfo(processDt);
        log.info("{} 일 기준 미수취 철회요청 장기미처리 건수 :: {}", processDt, unWithdrawalList.size());
        if(unWithdrawalList.size() > 0) {
            for(LinkedHashMap<String, Object> map : unWithdrawalList) {
                long purchaseOrderNo = ObjectUtils.toLong(map.get("purchase_order_no"));
                long bundleNo = ObjectUtils.toLong(map.get("bundle_no"));
                try {
                    // 철회요청
                    if(shippingMapperService.modifyUnreceivedDeliveryWithdrawal(purchaseOrderNo, bundleNo)){
                        LinkedHashMap<String, Object> shipOriginInfo = shippingMapperService.getNonShipCompleteInfo(purchaseOrderNo, bundleNo);
                        log.info("주문번호({})/배송번호({}) 미수취 철회에 따른 배송상태 확인 :: {}", purchaseOrderNo, bundleNo, shipOriginInfo);

                        String shipStatus = ObjectUtils.toString(shipOriginInfo.get("ship_status"));
                        if("D3,D4".contains(shipStatus)){
                            if("D3".equals(shipStatus)){
                                this.setClaimShipComplete(String.valueOf(bundleNo), "CLAIM-BATCH");
                            } else {
                                log.info("주문번호({})/배송번호({}) 미수취 철회에 따른 배송상태 확인(확정) :: {}", purchaseOrderNo, bundleNo, DateUtils.isAfter(ObjectUtils.toString(shipOriginInfo.get("complete_dt")), DateType.DAY, 8));
                                if(DateUtils.isAfter(ObjectUtils.toString(shipOriginInfo.get("complete_dt")), DateType.DAY, 8)){
                                    this.setClaimShipDecision(String.valueOf(bundleNo), "CLAIM-BATCH");
                                }
                            }
                        }
                        continue;
                    }
                } catch (Exception e) {
                    log.error("주문번호({})/배송번호({}) 미수취 철회에 따른 배송상태 업데이트 중 오류발생 ::: {}", purchaseOrderNo, bundleNo, e.getMessage());
                    continue;
                }
                log.info("주문번호({})/배송번호({}) 건은 철회처리가 되지 못했습니다.", purchaseOrderNo, bundleNo);
            }
        }
    }

    public void shipNotReceivedShippingComplete(String processDt) {
        // 조회일이 없으면 오늘 날짜로
        if(StringUtils.isEmpty(processDt)) {
            processDt = DateUtils.getCurrentYmd();
        }
        // 조회기준보다 전일자 미수취 미철회 요청건을 가지고 온다.
        List<LinkedHashMap<String, Object>> unWithdrawalList = shippingMapperService.getNonReceivedShippingCompleteInfo(processDt);
        log.info("{} 일 기준 미수취 철회요청 장기미처리 건수 :: {}", processDt, unWithdrawalList.size());
        if(unWithdrawalList.size() > 0) {
            for(LinkedHashMap<String, Object> map : unWithdrawalList) {
                long purchaseOrderNo = ObjectUtils.toLong(map.get("purchase_order_no"));
                long bundleNo = ObjectUtils.toLong(map.get("bundle_no"));
                try {
                    // 철회요청
                    if(shippingMapperService.modifyUnReceivedShippingCompleteWithdrawal(purchaseOrderNo, bundleNo)){
                        LinkedHashMap<String, Object> shipOriginInfo = shippingMapperService.getNonShipCompleteInfo(purchaseOrderNo, bundleNo);
                        log.info("주문번호({})/배송번호({}) 철회신청 장기미처리에 따른 배송상태 확인 :: {}", purchaseOrderNo, bundleNo, shipOriginInfo);
                        String shipStatus = ObjectUtils.toString(shipOriginInfo.get("ship_status"));
                        if("D4".contains(shipStatus)){
                            this.setClaimShipDecision(String.valueOf(bundleNo), "CLAIM-BATCH");
                        }
                        continue;
                    }
                } catch (Exception e) {
                    log.error("주문번호({})/배송번호({}) 미수취 철회에 따른 배송상태 업데이트 중 오류발생 ::: {}", purchaseOrderNo, bundleNo, e.getMessage());
                    continue;
                }
                log.info("주문번호({})/배송번호({}) 건은 철회처리가 되지 못했습니다.", purchaseOrderNo, bundleNo);
            }
        }
    }

    private void setClaimShipComplete(String bundleNo, String regId) throws Exception{
        ExternalUtil.postTransfer(
            ExternalUrlInfo.SHIPPING_CLAIM_FROM_SHIP_COMPLETE,
            ClaimShipCompleteDto.builder().bundleNo(bundleNo).chgId(regId).build(),
            String.class
        );
    }

    private void setClaimShipDecision(String bundleNo, String regId) throws Exception{
        ExternalUtil.postTransfer(
            ExternalUrlInfo.SHIPPING_CLAIM_FROM_SHIP_DECISION,
            ClaimShipCompleteDto.builder().bundleNo(bundleNo).chgId(regId).build(),
            String.class
        );
    }

}
