package kr.co.homeplus.claimbatch.claim.entity.claim;

public class ClaimAdditionEntity {
    // 일련번호
    public String claimAdditionNo;
    // 클레임 마스터 번호
    public String claimNo;
    // 클레임 묶음 번호
    public String claimBundleNo;
    // 클레임 상품번호
    public String claimItemNo;
    // 주문번호
    public String purchaseOrderNo;
    // 묶음번호
    public String bundleNo;
    // 다중 주문 배송 번호
    public String multiBundleNo;
    // 상품주문번호
    public String orderItemNo;
    // 다중 주문 아이템번호
    public String multiOrderItemNo;
    // 상품번호
    public String itemNo;
    // 고객번호
    public String userNo;
    // 주문 할인번호
    public String orderDiscountNo;
    // 추가금액 유형(D(iscount) : 할인금액(쿠폰/즉시할인), S(hip) : 배송비), G(ift) : 사은품
    public String additionType;
    // 추가금액 유형 상세  (*할인 유형코드   상품별 - 상품쿠폰: item_coupon  상품별 - 상품즉시할인: item_discount  상품별 - 제휴즉시할인: affi_discount  배송번호별 - 배송비쿠폰: ship_coupon  --- 미사용(배송번호별 - 배송비즉시할인: ship_discount)  구매번호별 - 장바구니쿠폰: cart_coupon  구매번호별 - 카드사즉시할인: card_discount  그룹별 - 그룹쿠폰 : group_coupon  프로모션 : promo_discount  사은품 : gift    *배송비 유형코드  cancel : 취소배송비(합포장 박스 감소에 따른 돌려주는 배송비, 전체취소에 따른 배송비 환불) - 100%구매자 부담(- 기록)  delivery : 추가배송비(부분취소 또는 반품시 무료배송 조건깨지면서 발생하는 배송비) - 구매자/판매자 부담  pickup : 반품배송비(수거배송비) - 구매자/판매자 부담    cancel_island : 취소배송비 추가비용(도서산간)  delivery_island : 추가배송비 추가비용(도서산간)  pickup_island : 반품배송비(수거배송비) 추가비용(도서산간))
    public String additionTypeDetail;
    // 할인번호 ((addition_type = D and addition_type_detail = *_discount 인 경우 기록))
    public String additionDiscountNo;
    // 쿠폰번호 ((addition_type = D and addition_type_detail *_coupon 인 경우 기록))
    public String additionCouponNo;
    // 추가 총금액
    public String additionSumAmt;
    // 구매자부담금액
    public String additionAmtBuyer;
    // 판매자부담금액
    public String additionAmtSeller;
    // 홈플러스 부담금액
    public String additionAmtHome;
    // 카드사 부담금액
    public String additionAmtCard;
    // 쿠폰이 적용된 수량
    public String couponApplyQty;
    // 환불차감 대상여부 (클레임배송비 처리방법(claim_bundle. claim_ship_fee_enclose. = Y or claim_bundle. claim_ship_fee_bank. = Y )   claim_addition 항목 중 pickup, delivery is_apply-> N 설정 is_apply-> N 인경우 환불금액 미차감)
    public String isApply;
    // 사은품메시지(영수증 노출 문구)
    public String giftMsg;
    // 재발행여부
    public String isIssue;
    // 등록일
    public String regDt;
    // 쿠폰전체 취소 여부(Y/N)
    public String totalCancelYn;
}
