package kr.co.homeplus.claimbatch.claim.maket.naver.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class NaverExchangeInfoDto {
    @ApiModelProperty(value= "클레임처리상태코드")
    private String claimStatus;
    @ApiModelProperty(value= "클레임요청일")
    private String claimRequestDate;
    @ApiModelProperty(value= "접수채널")
    private String requestChannel;
    @ApiModelProperty(value= "교환사유코드")
    private String exchangeReason;
    @ApiModelProperty(value= "교환상세사유")
    private String exchangeDetailedReason;
    @ApiModelProperty(value= "보류상태코드")
    private String holdbackStatus;
    @ApiModelProperty(value= "보류사유코드")
    private String holdbackReason;
    @ApiModelProperty(value= "보류상세사유")
    private String holdbackDetailedReason;
    @ApiModelProperty(value= "수거지주소(from)")
    private NaverAddressDto collectAddress;
    @ApiModelProperty(value= "수취지주소(to)")
    private NaverAddressDto returnReceiveAddress;
    @ApiModelProperty(value= "수거상태")
    private String collectStatus;
    @ApiModelProperty(value= "수거방법코드")
    private String collectDeliveryMethod;
    @ApiModelProperty(value= "수거택배사코드")
    private String collectDeliveryCompany;
    @ApiModelProperty(value= "수거송장번호")
    private String collectTrackingNumber;
    @ApiModelProperty(value= "수거완료일")
    private String collectCompletedDate;
    @ApiModelProperty(value= "재배송상태")
    private String reDeliveryStatus;
    @ApiModelProperty(value= "재배송방법코드")
    private String reDeliveryMethod;
    @ApiModelProperty(value= "재배송택배사코드")
    private String reDeliveryCompany;
    @ApiModelProperty(value= "재배송송장번호")
    private String reDeliveryTrackingNumber;
    @ApiModelProperty(value= "교환배송비청구액")
    private long claimDeliveryFeeDemandAmount;
    @ApiModelProperty(value= "교환배송비결제방법")
    private String claimDeliveryFeePayMethod;
    @ApiModelProperty(value= "교환배송비결제수단")
    private String claimDeliveryFeePayMeans;
    @ApiModelProperty(value= "기타비용청구액")
    private long etcFeeDemandAmount;
    @ApiModelProperty(value= "기타비용결제방법")
    private String etcFeePayMethod;
    @ApiModelProperty(value= "기타비용결제수단")
    private String etcFeePayMeans;
    @ApiModelProperty(value= "보류설정일")
    private String holdbackConfigDate;
    @ApiModelProperty(value= "보류설정자(구매자,판매자,관리자,시스템)")
    private String holdbackConfigurer;
    @ApiModelProperty(value= "보류해제일")
    private String holdbackReleaseDate;
    @ApiModelProperty(value= "보류해제자(구매자,판매자,관리자,시스템)")
    private String holdbackReleaser;
    @ApiModelProperty(value= "재배송처리일")
    private String reDeliveryOperationDate;
    @ApiModelProperty(value= "교환배송비묶음청구상품주문번호(다중일시쉼표구분)")
    private String claimDeliveryFeeProductOrderIds;
    @ApiModelProperty(value= "교환배송비할인액")
    private long claimDeliveryFeeDiscountAmount;
}
