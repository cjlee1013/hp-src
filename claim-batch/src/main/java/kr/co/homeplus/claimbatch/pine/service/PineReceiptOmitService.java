package kr.co.homeplus.claimbatch.pine.service;

import kr.co.homeplus.claimbatch.history.model.BatchHistoryDto;

/**
 * POS 번호가 누락된 번호를 찾아 dummy-data를 생성하여 PINE에 송신할 데이터를 생성한다.
 */
public interface PineReceiptOmitService {

    // 누락된 POS번호를 취득하여 PINE 전송 데이터를 생성한다.
    void createClaimPineTradeNoOmit(BatchHistoryDto batchHistoryDto);

    // 미전송된 데이터를 취득하여 PINE 전송 데이터를 생성한다.
    void sendClaimOmitData(BatchHistoryDto batchHistoryDto);

    // 특정 누락된 POS번호를 입력받아 PINE 전송 데이터를 생성한다.
    void inputClaimPineOmitIssue(BatchHistoryDto batchHistoryDto, String meta1);
}
