package kr.co.homeplus.claimbatch.claim.entity.order;

public class OrderTicketEntity {
    // 주문티켓번호(발급거래번호)
    public String orderTicketNo;
    // 구매주문번호
    public String purchaseOrderNo;
    // 번들번호
    public String bundleNo;
    // 주문상품번호
    public String orderItemNo;
    // 업체상품코드
    public String sellerItemCd;
    // 티켓코드(발급시스템을 통해 부여)
    public String ticketCd;
    //
    public String ticketStatus;
    // 티켓유효시작일시
    public String ticketValidStartDt;
    // 티켓유효종료일시
    public String ticketValidEndDt;
    // 티켓사용일시
    public String ticketUseDt;
    // 티켓전송횟수(SMS 재전송 횟수)
    public String ticketSendCnt;
    // 미사용환불율(미사용 환불 적용율 : 0~100 / 1부터 미사용환불대상)
    public String noUseRefundRate;
    // 발급유형(DIRECT:즉시발급, RESERVE:예약발급)
    public String issueType;
    // 발급상태 발급대기(I1) / 발급중(I2) / 발급완료(I3) / 발급취소(I4) / 발급실패(I8) / 취소실패(I9)
    public String issueStatus;
    // 발급리턴코드
    public String issueReturnCd;
    // 발급리턴메시지
    public String issueReturnMsg;
    // 발급요청횟수
    public String issueReqCnt;
    // 발급채널(COOP:쿠프마케팅, ETC:미발급업체)
    public String issueChannel;
    // 발급예약일시(발급유형이 예약발급인 경우 사용)
    public String issueReserveDt;
    // 발급일자
    public String issueDt;
    // 상품유형(사용미정), (E:E티켓,그외 정의해서 추가 사용 예정)
    public String itemType;
    // 연동유형(사용미정), (INTERNAL:내부티켓, PARTNER:업체티켓, EXTERNAL:외부연동티켓, DIRECT:직접연동)
    public String linkType;
    // 등록일시
    public String regDt;
    // 등록ID
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정ID
    public String chgId;

}
