package kr.co.homeplus.claimbatch.claim.maket.naver.service;

import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.claimbatch.claim.maket.naver.constants.NaverProductOrderChangeType;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.NaverClaimDataDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.NaverClaimDataDto.MarketClaimProductOrderDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimItemDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimInfoDto;
import kr.co.homeplus.claimbatch.claim.maket.service.MarketMapperService;
import kr.co.homeplus.claimbatch.claim.service.ClaimPaymentService;
import kr.co.homeplus.claimbatch.enums.DateType;
import kr.co.homeplus.claimbatch.enums.ExternalUrlInfo;
import kr.co.homeplus.claimbatch.util.DateUtils;
import kr.co.homeplus.claimbatch.util.ExternalUtil;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import kr.co.homeplus.claimbatch.util.SetParameter;
import kr.co.homeplus.claimbatch.util.SlackUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class NaverClaimService {

    private final MarketMapperService mapperService;
    private final MarketNaverProcess naverProcess;
    private final ClaimPaymentService paymentService;

    public void getNaverClaimListRetry(String marketOrderItemNos) throws Exception {
        List<NaverClaimDataDto> claimList = new ArrayList<>();
        if (StringUtils.isNotEmpty(marketOrderItemNos)) {
            if (marketOrderItemNos.contains("|")) {
                for (String str : marketOrderItemNos.split("\\|")) {
                    if (claimList.size() > 0) {
                        claimList.addAll(this.getNaverMarketOrderItemStatus(str));
                    } else {
                        claimList = this.getNaverMarketOrderItemStatus(str);
                    }
                }
            } else {
                claimList = this.getNaverMarketOrderItemStatus(marketOrderItemNos);
            }
            // 클레임 처리.
            this.getNaverClaimProcess(claimList);
        }
    }

    public void getNaverClaimList(String startDt, String endDt) throws Exception {
        if(StringUtils.isEmpty(startDt)) {
            startDt = DateUtils.setDateTime(DateUtils.getCurrentYmd(DateUtils.DEFAULT_YMD_HIS_ZERO_SEC), DateType.MIN, -45);
            endDt = DateUtils.setDateTime(DateUtils.getCurrentYmd(DateUtils.DEFAULT_YMD_HIS_LAST_SEC), DateType.MIN, -25);
        }
        log.info("getNaverClaimList startDt :: {} // endDt :: {}", startDt, endDt);
        // 클레임 처리.
        this.getNaverClaimProcess(this.getNaverMarketClaimList(startDt, endDt));
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public void getNaverClaimProcess(List<NaverClaimDataDto> dataList) throws Exception {
        try {

            if(dataList != null && dataList.size() > 0) {
                for(NaverClaimDataDto claimDataDto : dataList) {
                    MarketClaimDto marketClaimDto = naverProcess.createMarketClaimDto(claimDataDto.getOrder(), claimDataDto.getMarketClaimProductOrderList().get(0).getProductOrder());

                    List<MarketClaimItemDto> claimItemList = new ArrayList<>();
                    List<MarketClaimInfoDto> claimInfoList = new ArrayList<>();
                    boolean isError = Boolean.FALSE;
                    boolean isUpdate = Boolean.FALSE;
                    for(MarketClaimProductOrderDto dto : claimDataDto.getMarketClaimProductOrderList()){
                        log.info("주문번호 : {} || 상품주문번호 : {}", claimDataDto.getOrder().getOrderId(), dto.getProductOrder().getProductOrderId());
                        LinkedHashMap<String, Object> marketOrderItemInfo = mapperService.getMarketOrderItemInfo(dto.getProductOrder().getProductOrderId());
                        if(marketOrderItemInfo == null || marketOrderItemInfo.size() <= 0) {
                            isError = Boolean.TRUE;
                            log.info("마켓연동 주문번호({})가 주문정보에 없습니다.", dto.getProductOrder().getProductOrderId());
                            continue;
                        }
                        // 정상로직시 에러 FALSE 처리.
                        isError = Boolean.FALSE;
                        // 마켓클레임아이템 저장 DTO 생성.
                        MarketClaimItemDto marketClaimItemDto = naverProcess.createMarketClaimItemDto(dto.getProductOrder(), marketOrderItemInfo);
                        // 마켓주문번호 설정.
                        marketClaimItemDto.setMarketOrderNo(marketClaimDto.getMarketOrderNo());
                        // 마켓거래일자 설정.
                        marketClaimItemDto.setTradeDt(marketClaimDto.getPaymentDt());
                        // 마켓클레임정보 저장 DTO 생성.
                        MarketClaimInfoDto marketClaimInfoDto = null;
                        // 마켓클레임 타입에 따른 구분
                        if(dto.getProductOrder().getClaimType().equals("CANCEL") || dto.getProductOrder().getClaimType().equals("ADMIN_CANCEL")) {
                            marketClaimInfoDto = naverProcess.createMarketClaimInfoDtoByCancel(dto.getCancelInfo());
                        } else if (dto.getProductOrder().getClaimType().equals("RETURN")) {
                            marketClaimInfoDto = naverProcess.createMarketClaimInfoDtoByReturn(dto.getReturnInfo());
                            // 마켓클레임 반품
                        } else if (dto.getProductOrder().getClaimType().equals("EXCHANGE")) {
                            // 마켓클레임 교환
                            marketClaimInfoDto = naverProcess.createMarketClaimInfoDtoByExchange(dto.getExchangeInfo());
                        } else {
                            // 구분 없는 클레임타입인 경우 오류
                            isError = Boolean.TRUE;
                            log.error("마켓아이템번호({})에 대한 클레임 요청구분이 없습니다.", dto.getProductOrder().getProductOrderId());
                            break;
                        }
                        // 마켓클레임아이템 리스트 저장
                        claimItemList.add(marketClaimItemDto);
                        // 업데이트 대상여부
                        if(mapperService.getMarketClaimPrevCheck(marketClaimItemDto.getMarketClaimId())) {
                            mapperService.modifyPrevDataStatus(marketClaimItemDto.getMarketClaimId(), marketClaimItemDto, marketClaimInfoDto);
                            log.info("기존 데이터 존재로 상태값만 업데이트 하여 처리.");
//                            isUpdate = !"CANCEL,ADMIN_CANCEL".contains(marketClaimItemDto.getClaimType()) ? Boolean.TRUE : Boolean.FALSE;
                            isUpdate = Boolean.TRUE;
                            continue;
                        }

                        if(marketClaimInfoDto != null) {
                            // 마켓주문아이템번호 설정.
                            marketClaimInfoDto.setMarketOrderItemNo(dto.getProductOrder().getProductOrderId());
                            // 클레임사유설정.
                            marketClaimItemDto.setClaimReason(marketClaimInfoDto.getClaimReason());
                            marketClaimItemDto.setClaimReasonDetail(marketClaimInfoDto.getClaimDetailReason());
                            claimInfoList.add(marketClaimInfoDto);
                        }
                    }

                    if(!isError) {
                        // 마켓클레임 금액정보 업데이트
                        this.getCalculationPrice(marketClaimDto, claimItemList);
                        if(isUpdate) {
                            // 반품/교환에 의해서 업데이트 된 건은 금액 업데이트만 가능 해야함.
                            // 취소 일 경우는 기존 건들에 대해서는 업데이트 하지 않고 신규 건에 대해서는 insert 해야한다.
                            if(claimItemList.stream().map(MarketClaimItemDto::getClaimType).noneMatch(str -> "CANCEL".equals(str) || "ADMIN_CANCEL".equals(str))) {
                                mapperService.modifyMarketClaimAmount(claimItemList.stream().map(MarketClaimItemDto::getMarketClaimId).collect(Collectors.toList()).get(0), marketClaimDto);
                            }
                            claimItemList = claimItemList.stream().filter(dto -> !StringUtils.isEmpty(dto.getClaimReason())).collect(Collectors.toList());
//                            claimItemList.removeIf(dto -> StringUtils.isEmpty(dto.getClaimReason()));
                        } else {
                            // 기존 클레임이 존재하는지 확인
                            List<LinkedHashMap<String, Object>> prevClaimList = mapperService.getMarketClaimingDataCnt(marketClaimDto.getMarketOrderNo());
                            int marketOrderItemCnt = mapperService.getMarketOrderItemCnt(marketClaimDto.getMarketOrderNo());
                            int marketClaimItemCnt = mapperService.getMarketClaimItemCnt(marketClaimDto.getMarketOrderNo());
                            int requestClaimCnt = claimItemList.size();

                            log.info("prevClaimList : {}", prevClaimList);
                            if(prevClaimList != null && prevClaimList.size() > 0) {
                                int claimingCnt = prevClaimList.stream().map(map -> ObjectUtils.toLong(map.get("claim_item_no"))).distinct().collect(Collectors.toList()).size();
                                if(claimingCnt > claimItemList.size()) {
                                    log.info("마켓주문번호({})에 대한 기취소요청 수량과 취소요청 수량이 다르므로 오류처리", marketClaimDto.getMarketOrderNo());
                                    for(MarketClaimItemDto dto : claimItemList) {
                                        log.info("마켓주문번호({})의 마켓상품주문번호({})에 대한 임시저장 결과 : {}", marketClaimDto.getMarketOrderNo(), dto.getMarketOrderItemNo(), mapperService.addMarketClaimItemCheck(dto));
                                    }
                                    // 해당 테이블 저장시 아이템은 저장하지 않는다.
                                    claimItemList.clear();
                                    SlackUtils.sendingForMarketClaimError(this.setNaverPrevClaimErrorMessage(marketClaimDto.getMarketOrderNo(), claimingCnt, requestClaimCnt));
                                } else if(claimingCnt < claimItemList.size()) {
                                    for(long claimNo : prevClaimList.stream().map(map -> ObjectUtils.toLong(map.get("claim_no"))).distinct().collect(Collectors.toList())) {
                                        log.info("마켓주문번호({})에 대한 기취소요청 수량보다 취소요청 수량이 크므로 기존 클레임그룹번호({}) 거절처리", marketClaimDto.getMarketOrderNo(), claimNo);
                                        paymentService.setClaimStatus(claimNo, "BATCH", "C4");
                                    }
                                }
                            }
                            if(claimItemList.size() > 0) {
                                if(marketClaimDto.getTradeRcvAmt() == 0 && "CANCEL,ADMIN_CANCEL".contains(claimItemList.get(0).getClaimType())) {
                                    log.info("마켓주문번호({}) 주문상품수 : {} / 기존취소상품 수 : {} / 현재취소요청상품수 : {}", marketClaimDto.getMarketOrderNo(), marketOrderItemCnt, marketClaimItemCnt, requestClaimCnt);
                                    if((marketOrderItemCnt != claimItemList.size()) && marketClaimItemCnt == 0) {
                                        log.info("마켓주문번호({})에 대한 주문수량과 취소수량이 다름으로 오류처리", marketClaimDto.getMarketOrderNo());
                                        for(MarketClaimItemDto dto : claimItemList) {
                                            log.info("마켓주문번호({})의 마켓상품주문번호({})에 대한 임시저장 결과 : {}", marketClaimDto.getMarketOrderNo(), dto.getMarketOrderItemNo(), mapperService.addMarketClaimItemCheck(dto));
                                        }
                                        // 해당 테이블 저장시 아이템은 저장하지 않는다.
                                        claimItemList.clear();
                                        // slackMessage Send skip
//                                        SlackUtils.sendingForMarketClaimError(this.setNaverClaimInsertErrorMessage(marketClaimDto.getMarketOrderNo(), marketOrderItemCnt, marketClaimItemCnt, requestClaimCnt));
                                    }
                                }
                            }
                        }
                        // 기존 데이터여부 확인.
                        // 있으면 상태만 업데이트 하도록 한다.
                        if((claimItemList.size() > 0 && claimInfoList.size() > 0)) {
                            // 마켓클레임 저장
                            if(!mapperService.addMarketClaim(marketClaimDto)) {
                                log.error("addMarketClaim Fail");
                            }
                            // 마켓클레임번호 설정
                            claimItemList.forEach(dto -> dto.setMarketClaimNo(marketClaimDto.getMarketClaimNo()));
                            // 마켓클레임번호 설정
                            claimInfoList.forEach(dto -> dto.setMarketClaimNo(marketClaimDto.getMarketClaimNo()));
                            // 마켓클레임아이템 저장
                            for(MarketClaimItemDto claimItemDto : claimItemList) {
                                if(mapperService.addMarketClaimItem(claimItemDto)) {
                                    claimInfoList.stream().filter(dto -> dto.getMarketOrderItemNo().equals(claimItemDto.getMarketOrderItemNo())).forEach(dto -> dto.setMarketClaimItemNo(claimItemDto.getMarketClaimItemNo()));
                                }
                            }
                            if(claimInfoList.size() > 0) {
                                for(MarketClaimInfoDto infoDto : claimInfoList) {
                                    // 마켓클레임취소정보 저장.
                                    mapperService.addMarketClaimInfo(infoDto);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("get Naver Claim List : {}", e.getMessage());
            throw new Exception("마켓클레임(네이버) 데이터 저장중 오류");
        }
    }

    private List<NaverClaimDataDto> getNaverMarketClaimList(String startDt, String endDt) throws Exception {
        List<NaverClaimDataDto> dataList = new ArrayList<>();
        for(NaverProductOrderChangeType changeType : NaverProductOrderChangeType.values()) {
            if(dataList.size() <= 0) {
                dataList = this.getNaverClaimDataDownload(startDt, endDt, changeType);
            } else {
                dataList.addAll(this.getNaverClaimDataDownload(startDt, endDt, changeType));
            }
        }
        return dataList;
    };

    private List<NaverClaimDataDto> getNaverMarketOrderItemStatus(String marketOrderItemNo) throws Exception {
        List<NaverClaimDataDto> dataList = this.getNaverOrderItemStatusDownload(marketOrderItemNo);
        if(dataList != null) {
            String claimType = dataList.get(0).getMarketClaimProductOrderList().get(0).getProductOrder().getClaimType();
            String productOrderStatus = dataList.get(0).getMarketClaimProductOrderList().get(0).getProductOrder().getProductOrderStatus();
            String basicDt;
            if("CANCEL,ADMIN_CANCEL".contains(claimType)) {
                basicDt = dataList.get(0).getMarketClaimProductOrderList().get(0).getCancelInfo().getCancelCompletedDate();
            } else if ("RETURN".equals(claimType)) {
                basicDt = dataList.get(0).getMarketClaimProductOrderList().get(0).getReturnInfo().getReturnCompletedDate();
            } else {
                basicDt = dataList.get(0).getMarketClaimProductOrderList().get(0).getExchangeInfo().getCollectCompletedDate();
            }
            if(basicDt != null) {
                String startDt = DateUtils.customDateStr(DateUtils.setDateTime(basicDt, DateType.MIN, 0), DateUtils.DEFAULT_YMD_HIS_ZERO_SEC);
                String endDt = DateUtils.customDateStr(DateUtils.setDateTime(basicDt, DateType.MIN, 1), DateUtils.DEFAULT_YMD_HIS_ZERO_SEC);
                log.info("{}, {}, {}", startDt, endDt, NaverProductOrderChangeType.getNaverProductOrderChangeType(productOrderStatus));
                return this.getNaverClaimDataDownload(startDt, endDt, NaverProductOrderChangeType.getNaverProductOrderChangeType(productOrderStatus));
            }
        }
        return dataList;
    };

    private List<NaverClaimDataDto> getNaverClaimDataDownload(String startDt, String endDt, NaverProductOrderChangeType changeType) throws Exception {
        return ExternalUtil.getListTransfer(
            ExternalUrlInfo.OUTBOUND_MARKET_NAVER_CLAIM_LIST,
            new ArrayList<SetParameter>(){{
                add(SetParameter.create("startDt", URLEncoder.encode(startDt, "UTF-8")));
                add(SetParameter.create("endDt", URLEncoder.encode(endDt, "UTF-8")));
                add(SetParameter.create("productOrderChangeType", changeType.getRequestParam()));
            }},
            NaverClaimDataDto.class
        );
    }

    private List<NaverClaimDataDto> getNaverOrderItemStatusDownload(String marketOrderItemNo) throws Exception {
        return ExternalUtil.getListTransfer(
            ExternalUrlInfo.OUTBOUND_MARKET_NAVER_ITEM_STATUS_LIST,
            new ArrayList<SetParameter>(){{
                add(SetParameter.create("productOrderItemNo", URLEncoder.encode(marketOrderItemNo, "UTF-8")));
            }},
            NaverClaimDataDto.class
        );
    }

    /**
     * 마켓연동 - 클레임 저장시 금액 계산
     *
     * @param marketClaimDto 마켓클레임 저장 데이터
     * @param marketClaimItemList 마켓클레임 아이템 정보 리스트
     */
    private void getCalculationPrice(MarketClaimDto marketClaimDto, List<MarketClaimItemDto> marketClaimItemList) {
        long itemAmt = 0L;
        long itemDiscountAmt = 0L;
        long promotionDiscountAmt = 0L;
        long discountAmt = 0L;
        long tradeTotAmt = 0L;
        long tradeAmt = 0L;
        long tradeCnt = 0L;
        long couponDiscountAmt = 0L;

        for(MarketClaimItemDto marketClaimItemDto : marketClaimItemList) {
            itemAmt += marketClaimItemDto.getItemPrice() * marketClaimItemDto.getItemQty();
            itemDiscountAmt += marketClaimItemDto.getItemDiscountAmt();
            promotionDiscountAmt += marketClaimItemDto.getPromoDiscountAmt();
            couponDiscountAmt += marketClaimItemDto.getCouponDiscountAmt();
            tradeCnt += marketClaimItemDto.getItemQty();

            log.info("주무번호 : {} / {}, {}", marketClaimItemDto.getMarketOrderItemNo(), marketClaimItemDto.getItemPrice(), marketClaimItemDto.getItemQty());
        }

        discountAmt = promotionDiscountAmt + couponDiscountAmt;
        tradeTotAmt = itemAmt + marketClaimDto.getShipAmt();
        tradeAmt = tradeTotAmt - discountAmt;

        marketClaimDto.setTradeTotAmt(tradeTotAmt);
        marketClaimDto.setDiscountAmt(discountAmt);
        marketClaimDto.setTradeAmt(tradeAmt);
        marketClaimDto.setTradeCnt(tradeCnt + (marketClaimDto.getShipAmt() > 0 ? 1 : 0));
        marketClaimDto.setItemAmt(itemAmt);
        marketClaimDto.setItemDiscountAmt(itemDiscountAmt);
        marketClaimDto.setPromoDiscountAmt(promotionDiscountAmt);
        marketClaimDto.setCouponDiscountAmt(couponDiscountAmt);
    }

    private String setNaverClaimInsertErrorMessage(String marketOrderNo, int orderCnt, int prevCancelCnt, int presentCancelCnt) {
        String pattern = "마켓주문번호({0}) 건수 오류\n주문상품수 : {1}\n기존취소상품 수 : {2}\n현재취소요청상품수 : {3}";
        return MessageFormat.format(pattern, marketOrderNo, orderCnt, prevCancelCnt, presentCancelCnt);
    }

    private String setNaverPrevClaimErrorMessage(String marketOrderNo, int prevCancelCnt, int presentCancelCnt) {
        String pattern = "기존 클레임존재!!\n마켓주문번호({0}) 건수 오류\n기존취소수량 : {1}\n현재취소요청상품수 : {2}";
        return MessageFormat.format(pattern, marketOrderNo, prevCancelCnt, presentCancelCnt);
    }

}
