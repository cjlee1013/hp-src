package kr.co.homeplus.claimbatch.claim.maket.naver.model;

import java.util.List;
import lombok.Data;

@Data
public class NaverClaimDataDto {
    private ClaimOrderDto order;
    private List<MarketClaimProductOrderDto> marketClaimProductOrderList;

    @Data
    public static class MarketClaimProductOrderDto {
        private NaverProductOrderDto productOrder;
        //    private NaverDeliveryDto delivery;
        private NaverCancelInfo cancelInfo;
        private NaverReturnInfoDto returnInfo;
        private NaverExchangeInfoDto exchangeInfo;
    }
}
