package kr.co.homeplus.claimbatch.pine.service.impl;

import kr.co.homeplus.claimbatch.history.model.BatchHistoryDto;
import kr.co.homeplus.claimbatch.history.service.BatchHistoryService;
import kr.co.homeplus.claimbatch.pine.mapper.PineReceiptOmitMasterMapper;
import kr.co.homeplus.claimbatch.pine.mapper.PineReceiptOmitSlaveMapper;
import kr.co.homeplus.claimbatch.pine.model.ClaimPineReceiptOmitTranDto;
import kr.co.homeplus.claimbatch.pine.service.PineReceiptOmitService;
import kr.co.homeplus.claimbatch.util.ClaimDate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * sample service interface 구현체.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PineReceiptOmitServiceImpl implements PineReceiptOmitService {

    private final PineReceiptOmitMasterMapper pineReceiptOmitMasterMapper;
    private final PineReceiptOmitSlaveMapper pineReceiptOmitSlaveMapper;

    private final BatchHistoryService batchHistoryService;

   /**
     * 미송신된 데이터를 다시 재송신한다.
     */
    @Override
    public void sendClaimOmitData(BatchHistoryDto batchHistoryDto){
        try {
            String tradeDate = batchHistoryDto.getTradeDate();
            if (!StringUtils.isBlank(tradeDate)) {
                // 당일자(YYYYMMDD) - 00시 05분에 POS마감 처리 이전에 실행되어야 한다.
                tradeDate = ClaimDate.getCurrentDitHi();
                // 00시 05분 이전이면 전일자 취득
                if( Integer.parseInt(tradeDate) < Integer.parseInt("0005" ) ){
                    tradeDate = ClaimDate.getCurrentDitYmdMinus(1);
                } else {
                    tradeDate = ClaimDate.getCurrentDitYmd();
                }
            } else {
                tradeDate = ClaimDate.getCurrentDitYmd();
            }

            // tradeDate = "20201021";       // test일자
            if( !"onlySendTradeNoOmit".equalsIgnoreCase(batchHistoryDto.getBatchName())) {
                batchHistoryDto.setBatchName(batchHistoryDto.getBatchName() + " : onlySendTradeNoOmit");
            }
            batchHistoryDto.setMessage("tradeDate ["+tradeDate+"]");

            ClaimPineReceiptOmitTranDto claimPineReceiptOmitTranDto = new ClaimPineReceiptOmitTranDto();
            claimPineReceiptOmitTranDto.setTradeDt(tradeDate);

            // 01. claim_pine_omit_issue 미송신 데이터를 조회 , 해당 일자에 data_send_yn != Y 인 데이터를 취득한다.
            List<ClaimPineReceiptOmitTranDto> claimPineReceiptOmitTranDtoList = pineReceiptOmitSlaveMapper.selectClaimPineReceiptOmitIssueList(claimPineReceiptOmitTranDto);
            if( claimPineReceiptOmitTranDtoList.size() > 0 ) {

                String claimOmitNo = null;
                for (ClaimPineReceiptOmitTranDto claimPineReceiptOmitTranRaw : claimPineReceiptOmitTranDtoList) {
                    claimPineReceiptOmitTranRaw.setBatchHistoryDto(batchHistoryDto);
                    // PINE DATA 생성
                    createClaimPineData(claimPineReceiptOmitTranRaw);
                }
            } else {
                batchHistoryDto.setMessage(" SUCCESS : 송신 데이터가 미존재 ");
            }
        } catch ( Exception e ){
            batchHistoryDto.setMessage("Pine DATA 생성시 Error 발생 ["+e.getMessage()+"]");
        } finally {
            batchHistoryService.insertBatchHistory(batchHistoryDto);
        }
    }

    /**
     * 특정 누락본만 송신해야할 경우에 사용한다. (posNo, storeId 는 0 제외)
     * @param meta1         posNo * storeId * tradeDt * omitNo
     */
    @Override
    public void inputClaimPineOmitIssue(BatchHistoryDto batchHistoryDto, String meta1){

        log.info("Manual Batch Start [meta1] ["+meta1+"]");
        if ( StringUtils.isBlank(meta1) || meta1.split("\\*").length != 4 ) {
            batchHistoryDto.setMessage("Pine DATA 생성시 Error 발생 파라미터 오류 ["+meta1+"]");
            batchHistoryService.insertBatchHistory(batchHistoryDto);
            return;
        }
        String[] metaArray =  meta1.split("\\*");
        String posNo = metaArray[0];
        String storeId = metaArray[1];
        String tradeDt = metaArray[2];
        String omitNo= metaArray[3];

        log.info("Manual Batch Param [posNo] ["+posNo+"] [storeId] ["+storeId+"] [tradeDt] ["+tradeDt+"] [omitNo] ["+omitNo+"]");

        ClaimPineReceiptOmitTranDto claimPineReceiptOmitTradeResult = new ClaimPineReceiptOmitTranDto();
        claimPineReceiptOmitTradeResult.setPosNo(StringUtils.leftPad(posNo, 6, "0"));
        claimPineReceiptOmitTradeResult.setStoreId(StringUtils.leftPad(storeId, 4, "0"));
        claimPineReceiptOmitTradeResult.setTradeDt(tradeDt);
        claimPineReceiptOmitTradeResult.setOmitNo(StringUtils.leftPad(omitNo, 4, "0"));
        claimPineReceiptOmitTradeResult.setDataSendYn("N");
        claimPineReceiptOmitTradeResult.setClaimOmitNo(""); // 초기화
        claimPineReceiptOmitTradeResult.setBatchHistoryDto(batchHistoryDto);

        // 수기로 입력한 영수증번호로 claim_omit_no 생성후 tran data 생성
        createClaimPineData(claimPineReceiptOmitTradeResult);

        log.info("Manual Batch End [posNo] ["+posNo+"] [storeId] ["+storeId+"] [tradeDt] ["+tradeDt+"] [omitNo] ["+omitNo+"]");
    }

    /**
     * 해당일자에 발생된 모든 POS번호, 영수증번호를 취득하여 누락된 영수증번호 데이터를 생성한다.
     */
    @Override
    public void createClaimPineTradeNoOmit(BatchHistoryDto batchHistoryDto) {

        StringBuffer batchLogBuffer = new StringBuffer();

        // 당일자(YYYYMMDD) - 00시 05분에 POS마감 처리 이전에 실행되어야 한다.
        String tradeDate = ClaimDate.getCurrentDitHi();
        // 00시 05분 이전이면 전일자 취득
        if( Integer.parseInt(tradeDate) < Integer.parseInt("0005" ) ){
            tradeDate = ClaimDate.getCurrentDitYmdMinus(1);
        } else {
            tradeDate = ClaimDate.getCurrentDitYmd();
        }
        // tradeDate = "20201021";

        log.info("Batch start [tradeDate] ["+tradeDate+"]");
        batchLogBuffer.append("TradeDate ["+tradeDate+"] ");

        try {
            ClaimPineReceiptOmitTranDto claimPineReceiptOmitTranDto = new ClaimPineReceiptOmitTranDto();
            claimPineReceiptOmitTranDto.setTradeDt(tradeDate);
            claimPineReceiptOmitTranDto.setBatchHistoryDto(batchHistoryDto);
            createTradeNoOmitIssue(claimPineReceiptOmitTranDto);
        } catch( Exception e)
        {
            batchLogBuffer.append("Error insertClaimPineReceiptOmitMng ["+e.getMessage()+"]");
            batchHistoryDto.setMessage(batchLogBuffer.toString());
            batchHistoryService.insertBatchHistory(batchHistoryDto);
        } finally {
            log.info("Batch End [tradeDate] ["+tradeDate+"]");
        }
    }

    /**
     * trade_dt 기준으로 영수증 누락번호를 조회하여 모두 insert 한다. 자동배치 (autoInsertClaimPineOmitIssue)
     * @param claimPineReceiptOmitTranDto     ClaimPineReceiptOmitTranDto
     */
    public void createTradeNoOmitIssue(ClaimPineReceiptOmitTranDto claimPineReceiptOmitTranDto){

        log.info(" tradeDt : " + claimPineReceiptOmitTranDto.getTradeDt());

        BatchHistoryDto batchHistoryDto = claimPineReceiptOmitTranDto.getBatchHistoryDto();
        String batchName = batchHistoryDto.getBatchName()+" : createPosNoOmitIssue";

        try {
            // 누락 번호의 범위를 취득한다.
            BatchHistoryDto batchHistoryDtoOmit = new BatchHistoryDto();

            // 해당 일자의 발행된 영수증 번호중에 누락 번호를 모두 취득한다. 변수는 일자만(trade_dt)
            pineReceiptOmitMasterMapper.autoInsertClaimPineOmitIssue(claimPineReceiptOmitTranDto);

            batchHistoryDtoOmit.setMessage("tradeDt [" + claimPineReceiptOmitTranDto.getTradeDt() + "] ");
            batchHistoryDtoOmit.setBatchName(batchName);
            batchHistoryService.insertBatchHistory(batchHistoryDtoOmit);
        } catch (Exception e){
            batchHistoryDto.setMessage("Error insertClaimPineReceiptOmitMng ["+e.getMessage()+"]");
            batchHistoryService.insertBatchHistory(batchHistoryDto);
        }
    }

    /**
     * 누락된 영수증 번호를 기준으로 claim_omit_no 번호를 취득하고 tran data 생성ㅇ
     * 1. insertClaimPineOmitIssue 테이블에 insert , ord_no(claim_omit_no) 생성
     * 2. ord_no(claim_omit_no)  기준으로 pine tran data 생성
     * @param claimPineReceiptOmitTradeResult  pos_no, store_id, trade_dt, omit_no, head_office_code, area_code
     */
    @Transactional(value = "createClaimPineOmitIssue", isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void createClaimPineData(ClaimPineReceiptOmitTranDto claimPineReceiptOmitTradeResult) {

        String claimOmitNo = null;
        StringBuffer stringBuffer = new StringBuffer();

        BatchHistoryDto batchHistoryDto = claimPineReceiptOmitTradeResult.getBatchHistoryDto();
        BatchHistoryDto batchHistoryDtoLog = new BatchHistoryDto();

        try {
            // batchHistoryDto -> batchHistoryDtoLog
            BeanUtils.copyProperties(batchHistoryDtoLog, batchHistoryDto);
            batchHistoryDtoLog.setBatchName(batchHistoryDto.getBatchName()+" : createClaimPineData");

            // 미송신 데이터를 다시 재송신을 위해 claimOmitNo 유무를 체크한다. 없으면 Manual 송신(신규)
            claimOmitNo = claimPineReceiptOmitTradeResult.getClaimOmitNo();
            if (StringUtils.isBlank(claimOmitNo)) {
                // 04. insert claim_pine_omit_issue : pine 데이터 생성을 위해 claim_omit_no 를 취득한다.
                pineReceiptOmitMasterMapper.insertClaimPineOmitIssue(claimPineReceiptOmitTradeResult);
                claimOmitNo = claimPineReceiptOmitTradeResult.getClaimOmitNo();
            }

            if (!StringUtils.isBlank(claimOmitNo)) {
                // 05. insert trade header
                pineReceiptOmitMasterMapper.insertPineTradeInfoHeader(claimPineReceiptOmitTradeResult);
                // 06. insert trade item
                pineReceiptOmitMasterMapper.insertPineTradeInfoItem(claimPineReceiptOmitTradeResult);
                // 07. update claim_pine_omit_issue
                claimPineReceiptOmitTradeResult.setDataSendYn("Y");
                int updateCount = pineReceiptOmitMasterMapper.updateClaimPineOmitIssueSendYn(claimPineReceiptOmitTradeResult);
                if (updateCount <= 0) {
                    // 07-1. update error claim_pine_omit_issue
                    claimPineReceiptOmitTradeResult.setDataSendYn("E");
                    pineReceiptOmitMasterMapper.updateClaimPineOmitIssueSendYn(claimPineReceiptOmitTradeResult);
                    stringBuffer.append("claimOmitNo(OrdNo) [" + claimOmitNo + "] Pine DATA Error 생성실패");
                } else {
                    stringBuffer.append("claimOmitNo(OrdNo) [" + claimOmitNo + "] Pine DATA Success");
                }
            } else {
                stringBuffer.append(" claim_pine_omit_issue 테이블의 claim_omit_no 생성 실패 Pine DATA Error ");
            }

        } catch (Exception e) {
            // 07-1. update error claim_pine_omit_issue
            claimPineReceiptOmitTradeResult.setDataSendYn("E");
            pineReceiptOmitMasterMapper.updateClaimPineOmitIssueSendYn(claimPineReceiptOmitTradeResult);
            stringBuffer.append("Pine DATA Error 발생 claimOmitNo [" + claimOmitNo + "] [" + e.getMessage() + "]");
        } finally {
            batchHistoryDtoLog.setMessage(stringBuffer.toString());
            batchHistoryService.insertBatchHistory(batchHistoryDtoLog);
        }
    }
}
