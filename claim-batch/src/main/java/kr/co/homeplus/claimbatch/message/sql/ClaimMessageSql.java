package kr.co.homeplus.claimbatch.message.sql;

import kr.co.homeplus.claimbatch.claim.constants.CustomConstants;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimMstEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimPaymentEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimPickShippingEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.PaymentEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claimbatch.claim.entity.ship.ShippingAddrEntity;
import kr.co.homeplus.claimbatch.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.ship.StoreSlotMngEntity;
import kr.co.homeplus.claimbatch.claim.enums.MysqlFunction;
import kr.co.homeplus.claimbatch.claim.factory.EntityFactory;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import kr.co.homeplus.claimbatch.util.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ClaimMessageSql {

    public static String selectClaimBasicMessageInfo(@Param("claimNo") long claimNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class, Boolean.TRUE);
        StoreSlotMngEntity storeSlotMng = EntityFactory.createEntityIntoValue(StoreSlotMngEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimBundle.claimNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimType, claimBundle.claimType),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, storeSlotMng.shipDt), CustomConstants.YYYYMMDD), storeSlotMng.shipDt),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderDt), CustomConstants.YYYYMMDD), purchaseOrder.orderDt),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.PLUS,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.MAX, storeSlotMng.shipDt),
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, storeSlotMng.slotShipStartTime), "'00'"))
                            )
                        ),
                        CustomConstants.HHMI
                    ),
                    storeSlotMng.slotShipStartTime
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.DATE_FORMAT,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(
                            CustomConstants.PLUS,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.MAX, storeSlotMng.shipDt),
                                SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, storeSlotMng.slotShipEndTime), "'00'"))
                            )
                        ),
                        CustomConstants.HHMI
                    ),
                    storeSlotMng.slotShipEndTime
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shipMethod, shippingItem.shipMethod),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.sqlFunction(MysqlFunction.COUNT, claimItem.itemNo), "1"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.MAX, claimItem.itemName),
                                "' 외 '",
                                SqlUtils.customOperator(CustomConstants.MINUS, SqlUtils.sqlFunction(MysqlFunction.COUNT, claimItem.itemNo), "1"),
                                "'건'"
                            )
                        ),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimItem.itemName)
                    ),
                    claimItem.itemName
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.IN, SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shipMethod), ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, claimItem.originStoreId), "S")),
                        SqlUtils.sqlFunction(MysqlFunction.FN_STORE_PARTNER_NAME, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, claimItem.partnerId), "P"))
                    ),
                    "partner_nm"
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.shipMobileNo, claimBundle.shipMobileNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.buyerMobileNo, purchaseOrder.buyerMobileNo),
                SqlUtils.sqlFunction(
                    MysqlFunction.FN_SHIP_TYPE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.purchaseOrderNo),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.bundleNo),
                        "'C'"
                    ),
                    "ship_type"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_WHEN,
                    SqlUtils.sqlFunction(MysqlFunction.DAYOFWEEK, SqlUtils.sqlFunction(MysqlFunction.MAX, storeSlotMng.shipDt)),
                    ObjectUtils.toArray(
                        "'1'", "'일'",
                        "'2'", "'월'",
                        "'3'", "'화'",
                        "'4'", "'수'",
                        "'5'", "'목'",
                        "'6'", "'금'",
                        "'7'", "'토'"
                    ),
                    "day_of_week"
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimStatus, claimBundle.claimStatus),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimItem.storeType, claimItem.storeType),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.MAX, claimItem.storeType), "EXP"),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimItem.storeType),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.siteType)
                    ),
                    purchaseOrder.siteType
                ),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimItem.claimItemQty, "item_qty"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.purchaseOrderNo, claimBundle.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimShipFeeEnclose, "enclose_yn"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPickShipping.pickShipType, claimPickShipping.pickShipType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderGiftYn, purchaseOrder.orderGiftYn),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderType, purchaseOrder.orderType)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimBundle.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(claimItem.orderItemNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimPickShipping, ObjectUtils.toArray(claimBundle.claimBundleNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    storeSlotMng,
                    ObjectUtils.toArray(claimPickShipping.shipDt, claimPickShipping.shiftId, claimPickShipping.slotId),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumn(claimItem.originStoreId, storeSlotMng.storeId),
                        SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'"))
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo)
            )
            .GROUP_BY(claimBundle.claimNo)
            ;
        log.debug("selectClaimBasicMessageInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimPaymentMessageInfo(@Param("claimNo") long claimNo) {
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                claimPayment.claimPaymentNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.claimNo, claimMst.claimNo),
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totAddShipPrice),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totAddIslandShipPrice)
                    ),
                    "claim_ship_amt"
                ),
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.paymentAmt),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.substitutionAmt)
                    ),
                    "refund_amt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimType, claimBundle.claimType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.paymentStatus, claimPayment.paymentStatus),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimShipFeeEnclose, claimBundle.claimShipFeeEnclose)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimBundle.claimNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.claimNo, claimNo)
            )
            .GROUP_BY(claimPayment.claimPaymentNo)
            ;

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(claimMst.claimNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "claim_ship_amt", "claim_ship_amt"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "refund_amt", "refund_amt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(claimBundle.claimType), claimBundle.claimType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(claimPayment.paymentStatus), claimPayment.paymentStatus),
                SqlUtils.sqlFunction(MysqlFunction.MAX, SqlUtils.nonAlias(claimBundle.claimShipFeeEnclose), "enclose_yn")
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "ZZ"))
            .GROUP_BY(SqlUtils.nonAlias(claimMst.claimNo));
        log.debug("selectClaimPaymentMessageInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderSubstitutionInfo(@Param("claimNo") long claimNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, SqlUtils.sqlFunction(MysqlFunction.COUNT, orderItem.itemNo), "1"),
                        SqlUtils.sqlFunction(
                            MysqlFunction.CONCAT,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemNm1),
                                "' 외 '",
                                SqlUtils.customOperator(CustomConstants.MINUS, SqlUtils.sqlFunction(MysqlFunction.COUNT, orderItem.itemNo), "1"),
                                "'건'"
                            )
                        ),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemNm1)
                    ),
                    "sub_item_name"
                ),
                SqlUtils.sqlFunction(MysqlFunction.SUM, orderItem.itemQty, "sub_item_qty"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, payment.totAmt, "sub_payment_amt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(EntityFactory.getTableNameWithAlias(purchaseOrder), ObjectUtils.toArray(SqlUtils.equalColumn(claimMst.purchaseOrderNo, purchaseOrder.orgPurchaseOrderNo))))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'")))))
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            );
        log.debug("selectOrderSubstitutionInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectPresentAutoCancelInfo(@Param("claimNo") long claimNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.buyerMobileNo, "sender_mobile_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.shipMobileNo, "receiver_mobile_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.buyerNm, "sender_nm"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.receiverNm, "receiver_nm"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.dsGiftAutoCancelDt, "receive_dt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimMst.purchaseOrderNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(purchaseOrder.orderGiftYn, "Y"))))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            );
        log.debug("selectPresentAutoCancelInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }
}
