package kr.co.homeplus.claimbatch.claim.model.payment.mileage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "마일리지 적립 요청")
public class MileageSaveRequestDto {
    @ApiModelProperty(value = "마일리지코드", position = 1)
    private String mileageCode;

    @ApiModelProperty(value = "점포유형", position = 2)
    private String storeType;

    @ApiModelProperty(value = "요청사유", position = 3)
    private String requestReason;

    @ApiModelProperty(value = "고객번호", position = 4)
    private String userNo;

    @ApiModelProperty(value = "마일리지금액", position = 5)
    private long mileageAmt;

    @ApiModelProperty(value = "거래번호", position = 6)
    private String tradeNo;

    @ApiModelProperty(value = "요청자", position = 7)
    private String regId;

    @ApiModelProperty(value = "유효기간", position = 8)
    private int expireDay;
}
