package kr.co.homeplus.claimbatch.claim.maket.naver.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ClaimOrderDto {
    @ApiModelProperty(value= "주문일시")
    private String orderDate;
    @ApiModelProperty(value= "주문번호")
    private String orderId;
    @ApiModelProperty(value= "주문자아이디")
    private String ordererId;
    @ApiModelProperty(value= "주문자이름")
    private String ordererName;
    @ApiModelProperty(value= "주문자연락처1")
    private String ordererTel1;
    @ApiModelProperty(value= "주문자연락처2")
    private String ordererTel2;
    @ApiModelProperty(value= "결제일시(최종결제)")
    private String paymentDate;
    @ApiModelProperty(value= "결제수단(신용카드,휴대폰,무통장입금,실시간계좌이체,포인트결제,신용카드간편결제,계좌간편결제,휴대폰간편결제,나중에결제,후불결제)")
    private String paymentMeans;
    @ApiModelProperty(value= "결제기한")
    private String paymentDueDate;
    @ApiModelProperty(value= "주문할인액")
    private String orderDiscountAmount;
    @ApiModelProperty(value= "일반결제수단최종결제금액")
    private long generalPaymentAmount;
    @ApiModelProperty(value= "네이버페이포인트최종결제금액")
    private long naverMileagePaymentAmount;
    @ApiModelProperty(value= "충전금최종결제금액")
    private long chargeAmountPaymentAmount;
    @ApiModelProperty(value= "네이버페이적립금최종결제금액")
    private long checkoutAccumulationPaymentAmount;
    @ApiModelProperty(value= "후불결제최종결제금액")
    private long payLaterPaymentAmount;
    @ApiModelProperty(value= "배송메모개별입력여부")
    private String isDeliveryMemoParticularInput;
    @ApiModelProperty(value= "결제위치구분(PC")
    private String payLocationType;
    @ApiModelProperty(value= "주문자번호")
    private String ordererNo;
}
