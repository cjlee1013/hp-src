package kr.co.homeplus.claimbatch.claim.entity.order;

public class OrderPromoItemBuyEntity {

    // 주문프로모션아이템순번
    public String orderPromoItemBuySeq;

    // 구매주문번호
    public String purchaseOrderNo;

    // 주문프로모션번호
    public String orderPromoNo;

    // 프로모션번호
    public String promoNo;

    // 프로모션상세번호
    public String promoDetailNo;

    // 상품번호
    public String itemNo;

}
