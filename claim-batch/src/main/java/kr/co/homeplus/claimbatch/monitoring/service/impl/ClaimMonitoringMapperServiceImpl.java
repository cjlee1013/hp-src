package kr.co.homeplus.claimbatch.monitoring.service.impl;

import kr.co.homeplus.claimbatch.monitoring.mapper.ClaimMonitoringMapper;
import kr.co.homeplus.claimbatch.monitoring.model.ClaimPaymentMonitoringDto;
import kr.co.homeplus.claimbatch.monitoring.service.ClaimMonitoringMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ClaimMonitoringMapperServiceImpl implements ClaimMonitoringMapperService {

    private final ClaimMonitoringMapper mapper;

    @Override
    public ClaimPaymentMonitoringDto getMonitoringData() {
        return mapper.selectClaimPaymentFailMonitoring();
    }
}
