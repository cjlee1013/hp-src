package kr.co.homeplus.claimbatch.claim.entity.order;

public class PurchaseStoreInfoEntity {

    // 구매점포번호
    public String purchaseStoreInfoNo;

    // 결제번호
    public String paymentNo;

    // 구매주문번호
    public String purchaseOrderNo;

    // 원천점포ID(매출용실제점포)
    public String originStoreId;

    // 점포ID(주문서에서 넘어온 가상점포ID)
    public String storeId;

    // 점포종류(NOR:일반점포,DLV:택배점포)
    public String storeKind;

    // 거래일자(YYYYMMDD)
    public String tradeDt;

    // POS번호
    public String posNo;

    // 거래번호(영수증번호 Seq)
    public String tradeNo;

    // 배송일자
    public String shipDt;

    // SHIFT ID
    public String shiftId;

    // SLOT ID
    public String slotId;

    // SHIFT마감시간 (DMS실행시간)
    public String shiftCloseTime;

    // SLOT마감시간
    public String slotCloseTime;

    // 배송방법 (DS_DLV:택배배송,DS_DRCT:직접배송,DS_PICK:방문수령/픽업,DS_POST:우편배송,DS_QUICK:퀵배송,TD_DLV:택배배송,TD_DRCT:직접배송,TD_PICK:방문수령,TD_QUICK:퀵배송)
    public String shipMethod;

    // 합배송 주문여부(Y:합배송,N:일반배송)
    public String combineOrderYn;

    // 픽업여부(점포픽업:Y,자차:N)
    public String pickupYn;

    // 락커여부(락커사용여부 Y:사용,N:미사용)
    public String lockerYn;

    // FC점포ID
    public String fcStoreId;

    // Slot 주문차감 번호(order_slot_stock 테이블 연결고리)
    public String orderSlotStockNo;

}
