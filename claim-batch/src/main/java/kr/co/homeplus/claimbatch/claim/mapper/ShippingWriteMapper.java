package kr.co.homeplus.claimbatch.claim.mapper;

import kr.co.homeplus.claimbatch.claim.sql.ShippingSql;
import kr.co.homeplus.claimbatch.core.db.annotation.MasterConnection;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

@MasterConnection
public interface ShippingWriteMapper {
    @UpdateProvider(type = ShippingSql.class, method = "updateShipNotReceivedWithdrawal")
    int updateShipNotReceivedWithdrawal(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @UpdateProvider(type = ShippingSql.class, method = "updateNotReceivedShippingCompleteWithdrawal")
    int updateNotReceivedShippingCompleteWithdrawal(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);
}
