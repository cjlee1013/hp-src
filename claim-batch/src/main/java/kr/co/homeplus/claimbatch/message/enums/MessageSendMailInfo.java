package kr.co.homeplus.claimbatch.message.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum MessageSendMailInfo {
    CLAIM_MONITORING_MAIL(
        "M00001",
        "#{sendTime} 클레임 실패 건",
        "안녕하세요. 클레임 결제취소 실패건 모니터링 메일입니다.\n\n"
            + "현재 #{sendTime} 기준 1시간 동안 발생하 결제 실패 건은 총 {totalCnt} 입니다.\n"
            + "<table>"
            + "<tr><td>결제수단</td><td>실패건수</td><td>실패금액</td></tr>"
            + "<tr><td>PG</td><td>#{pgFailCnt}</td><td>#{pgFailAmt}</td></tr>"
            + "<tr><td>MHC</td><td>#{mhcFailCnt}</td><td>#{mhcFailAmt}</td></tr>"
            + "<tr><td>MILEAGE</td><td>#{mileageFailCnt}</td><td>#{mileageFailAmt}</td></tr>"
            + "<tr><td>OCB</td><td>#{ocbFailCnt}</td><td>#{ocbFailAmt}</td></tr>"
            + "<tr><td>DGV</td><td>#{dgvFailCnt}</td><td>#{dgvFailAmt}</td></tr>"
            + "</table>",
        "클레임 결제실패 모니터링 메일",
        "sendTime|totalCnt|pgFailCnt|pgFailAmt|mhcFailCnt|mhcFailAmt|mileageFailCnt|mileageFailAmt|ocbFailCnt|ocbFailAmt|dgvFailCnt|ocbFailAmt|dgvFailCnt|dgvFailAmt"),
    ;

    @Getter
    private final String templateCode;

    @Getter
    private final String title;

    @Getter
    private final String body;

    @Getter
    private final String sendTitle;

    @Getter
    private final String mappingParams;
}
