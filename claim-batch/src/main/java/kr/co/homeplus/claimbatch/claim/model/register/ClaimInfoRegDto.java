package kr.co.homeplus.claimbatch.claim.model.register;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClaimInfoRegDto {
    private Long paymentNo;
    private Long purchaseOrderNo;
    private Long claimNo;
}
