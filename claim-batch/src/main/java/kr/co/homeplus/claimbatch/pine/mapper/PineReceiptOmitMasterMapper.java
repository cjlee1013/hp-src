package kr.co.homeplus.claimbatch.pine.mapper;

import kr.co.homeplus.claimbatch.core.db.annotation.MasterConnection;
import kr.co.homeplus.claimbatch.pine.model.ClaimPosReceiptIssueDto;
import kr.co.homeplus.claimbatch.pine.model.ClaimPineReceiptOmitTranDto;

@MasterConnection
public interface PineReceiptOmitMasterMapper {

    // POS 누락 번호를 생성한다.(사용하지 않음)
    int insertClaimPineReceiptOmitMng(ClaimPosReceiptIssueDto claimPosReceiptIssueDto);

    // 누락된 영수증 번호를 취득 한다. (자동, 일자로 한꺼번에 생성)
    void autoInsertClaimPineOmitIssue(ClaimPineReceiptOmitTranDto claimPineReceiptOmitTranDto);

    // 누락된 영수증 번호를 취득한다. PINE 전송용 claim_omit_no를 생성한다.(한건씩, 수동)
    void insertClaimPineOmitIssue(ClaimPineReceiptOmitTranDto claimPineReceiptOmitTranDto);

    // PINE 전송 : header 데이터 생성
    int insertPineTradeInfoHeader(ClaimPineReceiptOmitTranDto claimPineReceiptOmitTranDto);

    // PINE 전송 : item 데이터 생성
    int insertPineTradeInfoItem(ClaimPineReceiptOmitTranDto claimPineReceiptOmitTranDto);

    // PINE 거래정보 전송유무 update(claim_pine_omit_issue)
    int updateClaimPineOmitIssueSendYn(ClaimPineReceiptOmitTranDto claimPineReceiptOmitTranDto);

    // PINE 거래정보 전송유무 update(claim_pine_receipt_omit_mng) - 사용하지 않음
    int updateClaimPineReceiptOmitMngIsseudYn(ClaimPineReceiptOmitTranDto claimPineReceiptOmitTranDto);
}
