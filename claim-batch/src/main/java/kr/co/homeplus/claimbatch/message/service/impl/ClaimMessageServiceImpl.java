package kr.co.homeplus.claimbatch.message.service.impl;

import java.util.LinkedHashMap;
import kr.co.homeplus.claimbatch.message.mapper.ClaimMessageMapper;
import kr.co.homeplus.claimbatch.message.service.ClaimMessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimMessageServiceImpl implements ClaimMessageService {

    private final ClaimMessageMapper mapper;

    @Override
    public LinkedHashMap<String, Object> getClaimBasicMessageInfo(long claimNo) {
        LinkedHashMap<String, Object> returnMap = mapper.selectClaimBasicMessageInfo(claimNo);
        log.info("클레임그룹번호({})에 대한 메시지 발송 기본정보 데이터 ::: {}", claimNo, returnMap);
        return returnMap;
    }

    @Override
    public LinkedHashMap<String, Object> getClaimPaymentMessageInfo(long claimNo) {
        LinkedHashMap<String, Object> returnMap = mapper.selectClaimPaymentMessageInfo(claimNo);
        log.info("클레임그룹번호({})에 대한 메시지 발송 결제정보 데이터 ::: {}", claimNo, returnMap);
        return returnMap;
    }

    @Override
    public LinkedHashMap<String, Object> getOrderSubstitutionMessageInfo(long claimNo) {
        LinkedHashMap<String, Object> returnMap = mapper.selectOrderSubstitutionInfo(claimNo);
        log.info("클레임그룹번호({})에 대한 메시지 발송 대체주문정보 데이터 ::: {}", claimNo, returnMap);
        return returnMap;
    }

    @Override
    public LinkedHashMap<String, Object> getPresentAutoCancelInfo(long claimNo) {
        LinkedHashMap<String, Object> returnMap = mapper.selectPresentAutoCancelInfo(claimNo);
        log.info("클레임그룹번호({})에 대한 메시지 발송 선물자동취소정보 데이터 ::: {}", claimNo, returnMap);
        return returnMap;
    }
}
