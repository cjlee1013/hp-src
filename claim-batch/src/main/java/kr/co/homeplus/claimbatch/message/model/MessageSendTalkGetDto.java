package kr.co.homeplus.claimbatch.message.model;

import io.swagger.annotations.ApiModel;
import kr.co.homeplus.claimbatch.message.enums.ReturnCode;
import lombok.Data;

@Data
@ApiModel(description = "알림톡 응답 DTO")
public class MessageSendTalkGetDto {
    private ReturnCode returnCode;
    private Integer sendCount;
    private long alimtalkWorkSeq;
}
