package kr.co.homeplus.claimbatch.claim.maket.naver.sql;

import java.util.Arrays;
import kr.co.homeplus.claimbatch.claim.constants.CustomConstants;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimAdditionEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimMstEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimOptEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimPaymentEntity;
import kr.co.homeplus.claimbatch.claim.entity.dms.ItmMngCodeEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.BundleEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderDiscountMarketEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderOptEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.PaymentEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claimbatch.claim.entity.ship.ShippingAddrEntity;
import kr.co.homeplus.claimbatch.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claimbatch.claim.enums.MysqlFunction;
import kr.co.homeplus.claimbatch.claim.factory.EntityFactory;
import kr.co.homeplus.claimbatch.claim.maket.entity.MarketClaimEntity;
import kr.co.homeplus.claimbatch.claim.maket.entity.MarketClaimItemCheckEntity;
import kr.co.homeplus.claimbatch.claim.maket.entity.MarketClaimItemEntity;
import kr.co.homeplus.claimbatch.claim.maket.entity.MarketClaimInfoEntity;
import kr.co.homeplus.claimbatch.claim.maket.entity.MarketOrderEntity;
import kr.co.homeplus.claimbatch.claim.maket.entity.MarketOrderItemEntity;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimItemDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimInfoDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimAdditionRegDto;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import kr.co.homeplus.claimbatch.util.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MarketClaimSql {

    public static String insertMarketClaim(MarketClaimDto claimOrderDto) {
        String[] columnArray = EntityFactory.getColumnInfo(MarketClaimEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(MarketClaimEntity.class))
            .INTO_COLUMNS(Arrays.copyOfRange(columnArray, 1, columnArray.length))
            .INTO_VALUES(
                SqlUtils.convertInsertParamToCamelCase(Arrays.copyOfRange(columnArray, 1, columnArray.length))
            );
        log.debug("insertMarketClaim :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String insertMarketClaimItem(MarketClaimItemDto marketClaimItemDto) {
        String[] columnArray = EntityFactory.getColumnInfo(MarketClaimItemEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(MarketClaimItemEntity.class))
            .INTO_COLUMNS(Arrays.copyOfRange(columnArray, 1, columnArray.length))
            .INTO_VALUES(
                SqlUtils.convertInsertParamToCamelCase(Arrays.copyOfRange(columnArray, 1, columnArray.length))
            );
        log.debug("insertMarketClaimItem :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String insertMarketClaimInfo(MarketClaimInfoDto marketClaimInfoDto) {
        String[] columnArray = EntityFactory.getColumnInfo(MarketClaimInfoEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(MarketClaimInfoEntity.class))
            .INTO_COLUMNS(Arrays.copyOfRange(columnArray, 1, columnArray.length))
            .INTO_VALUES(
                SqlUtils.convertInsertParamToCamelCase(Arrays.copyOfRange(columnArray, 1, columnArray.length))
            );
        log.debug("insertMarketClaimInfo :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String insertMarketClaimItemCheck(MarketClaimItemDto marketClaimItemDto) {
        String[] columnArray = EntityFactory.getColumnInfo(MarketClaimItemCheckEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(MarketClaimItemCheckEntity.class))
            .INTO_COLUMNS(Arrays.copyOfRange(columnArray, 1, columnArray.length))
            .INTO_VALUES(
                SqlUtils.convertInsertParamToCamelCase(Arrays.copyOfRange(columnArray, 1, columnArray.length))
            );
        log.debug("insertMarketClaimItem :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateMarketClaimItemStatus(@Param("marketClaimNo") long marketClaimNo, @Param("rcvYn") String rcvYn) {
        MarketClaimItemEntity marketClaimItem = EntityFactory.createEntityIntoValue(MarketClaimItemEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(marketClaimItem))
            .SET(
                SqlUtils.equalColumnByInput(marketClaimItem.rcvYn, rcvYn)
            )
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimItem.marketClaimNo, marketClaimNo),
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, marketClaimItem.rcvYn, "'Y'")
            );
        log.debug("updateMarketClaimItemStatus :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateMarketClaimItemByClaimNo(@Param("marketClaimNo") long marketClaimNo, @Param("claimNo") long claimNo) {
        MarketClaimItemEntity marketClaimItem = EntityFactory.createEntityIntoValue(MarketClaimItemEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(marketClaimItem))
            .SET(
                SqlUtils.equalColumnByInput(marketClaimItem.claimNo, claimNo)
            )
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimItem.marketClaimNo, marketClaimNo)
            );
        log.debug("updateMarketClaimItemByClaimNo :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMarketOrderItemCheck(@Param("marketOrderItemNo") String marketOrderItemNo) {
        MarketOrderItemEntity marketOrderItem = EntityFactory.createEntityIntoValue(MarketOrderItemEntity.class);
        SQL query = new SQL()
            .SELECT(
                marketOrderItem.marketOrderNo,
                marketOrderItem.marketOrderItemNo,
                marketOrderItem.eventCouponNo,
                marketOrderItem.couponId,
                marketOrderItem.discountType,
                marketOrderItem.discountVal,
                marketOrderItem.rcvYn,
                marketOrderItem.couponDiscountAmt,
                marketOrderItem.promoNo,
                marketOrderItem.promoDetailNo,
                marketOrderItem.rpmPromoDetailNo,
                marketOrderItem.promoType,
                marketOrderItem.promoKind,
                marketOrderItem.promoDiscountType,
                marketOrderItem.promoDiscountAmt,
                marketOrderItem.promoDiscountRate,
                marketOrderItem.mixMatchType,
                marketOrderItem.couponPrice,
                marketOrderItem.discountPrice,
                marketOrderItem.onePlusOne,
                marketOrderItem.marketDiscountPrice
            )
            .FROM(EntityFactory.getTableName(marketOrderItem))
            .WHERE(
                SqlUtils.equalColumnByInput(marketOrderItem.marketOrderItemNo, marketOrderItemNo)
            );
        log.debug("selectMarketOrderItemCheck :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectCreateMarketClaimData(@Param("marketOrderItemNo") String marketOrderItemNo) {
        MarketClaimItemEntity marketClaimItem = EntityFactory.createEntityIntoValue(MarketClaimItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        MarketClaimInfoEntity marketClaimInfo = EntityFactory.createEntityIntoValue(MarketClaimInfoEntity.class, "info");
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                orderOpt.orderItemNo,
                shippingItem.bundleNo,
                marketClaimItem.marketOrderItemNo,
                marketClaimItem.marketClaimItemNo,
                marketClaimItem.claimStatus,
                marketClaimItem.claimType,
                marketClaimItem.marketClaimNo,
                marketClaimInfo.holdbackStatus,
                marketClaimItem.rcvYn,
                marketClaimInfo.requestChannel,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(marketClaimItem.claimReason, "'INCORRECT_INFO'"), marketClaimItem.claimReason),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(marketClaimItem.claimReasonDetail, "'클레임사유가 입력되지 않음'"), marketClaimItem.claimReasonDetail),
                marketClaimItem.marketClaimNo,
                marketClaimItem.marketOrderNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(marketClaimItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(marketClaimInfo, "info"),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(marketClaimItem.marketClaimNo, marketClaimItem.marketClaimItemNo), marketClaimInfo)
                    )
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(marketClaimItem.marketType, marketClaimItem.marketOrderNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo, marketClaimItem.marketOrderItemNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderOpt.orderItemNo, orderItem.bundleNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, payment.paymentStatus, ObjectUtils.toArray("'P3'", "'P5'"))))
            )
            .WHERE(
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, marketClaimItem.rcvYn, "'Y'"),
                SqlUtils.notExists(
                    new SQL().SELECT(claimBundle.purchaseOrderNo)
                    .FROM(EntityFactory.getTableNameWithAlias(claimOpt))
                    .INNER_JOIN(
                        SqlUtils.createJoinCondition(
                            claimBundle,
                            ObjectUtils.toArray(claimOpt.claimNo, claimOpt.claimBundleNo),
                            ObjectUtils.toArray(
                                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                            )
                        )
                    )
                    .WHERE(
                        SqlUtils.equalColumn(claimBundle.purchaseOrderNo, purchaseOrder.purchaseOrderNo),
                        SqlUtils.equalColumn(claimOpt.orderOptNo, orderOpt.orderOptNo)
                    )
                )
            );

        if(StringUtils.isEmpty(marketOrderItemNo)) {
            query.WHERE(
                SqlUtils.sqlFunction(
                    MysqlFunction.IN,
                    marketClaimItem.marketClaimNo,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT_DISTINCT(SqlUtils.nonAlias(marketClaimItem.marketClaimNo))
                                .FROM(EntityFactory.getTableName(marketClaimItem))
                                .WHERE(
                                    SqlUtils.sqlFunction(
                                        MysqlFunction.BETWEEN,
                                        ObjectUtils.toArray(
                                            marketClaimItem.chgDt,
                                            SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATEADD_MIN, ObjectUtils.toArray("NOW()", "-25")), "%Y-%m-%d %H:%i:00")),
                                            SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATEADD_MIN, ObjectUtils.toArray("NOW()", "-5")), "%Y-%m-%d %H:%i:00"))
                                        )
                                    )
                                )
                        )
                    )
                )
            );
        } else {
            query.WHERE(
                SqlUtils.sqlFunction(
                    MysqlFunction.IN,
                    marketClaimItem.marketClaimNo,
                    ObjectUtils.toArray(
                        SqlUtils.subTableQuery(
                            new SQL().SELECT(SqlUtils.nonAlias(marketClaimItem.marketClaimNo))
                                .FROM(EntityFactory.getTableName(marketClaimItem))
                                .WHERE(SqlUtils.equalColumnByInput(SqlUtils.nonAlias(marketClaimItem.marketOrderItemNo), marketOrderItemNo))
                        )
                    )
                )
            );
        }
        log.debug("selectCreateMarketClaimData :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimReasonType(@Param("claimReasonType") String claimReasonType) {
        ItmMngCodeEntity itmMngCode = EntityFactory.createEntityIntoValue(ItmMngCodeEntity.class);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.aliasToRow("imc.ref_1", "claim_type"),
                SqlUtils.aliasToRow("imc.ref_2", "who_reason"),
                SqlUtils.aliasToRow("imc.ref_3", "cancel_type")
            )
            .FROM(EntityFactory.getTableNameWithAlias("dms", itmMngCode))
            .WHERE(
                SqlUtils.equalColumnByInput(itmMngCode.gmcCd, "claim_reason_type"),
                SqlUtils.equalColumnByInput(itmMngCode.useYn, "Y"),
                SqlUtils.equalColumnByInput(itmMngCode.mcCd, claimReasonType)
            );
        log.debug("selectClaimReasonType :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimExchangeAndReturnInfo(@Param("marketClaimNo") long marketClaimNo, @Param("marketOrderItemNo") String marketOrderItemNo) {
        MarketClaimInfoEntity marketClaimInfo = EntityFactory.createEntityIntoValue(MarketClaimInfoEntity.class, Boolean.TRUE);
        MarketClaimEntity marketClaim = EntityFactory.createEntityIntoValue(MarketClaimEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.aliasToRow(marketClaim.shipReqDt, "ship_dt"),
                marketClaim.shiftId,
                marketClaim.slotId,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(marketClaimInfo.collectDeliveryMethod, "RETURN_INDIVIDUAL"),
                        "'Y'", "'N'"
                    ),
                    "is_pick"
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(marketClaimInfo.collectDeliveryMethod, "RETURN_INDIVIDUAL"),
                        "'N'", "'Y'"
                    ),
                    "is_pick_req"
                ),
                SqlUtils.sqlFunction( MysqlFunction.IFNULL, ObjectUtils.toArray(marketClaimInfo.collectName, shippingAddr.receiverNm), "pick_receiver_nm" ),
                SqlUtils.sqlFunction( MysqlFunction.IFNULL, ObjectUtils.toArray(marketClaimInfo.collectZipcode, shippingAddr.zipcode), "pick_zip_code" ),
                SqlUtils.sqlFunction( MysqlFunction.IFNULL, ObjectUtils.toArray(marketClaimInfo.collectBaseAddress, shippingAddr.roadBaseAddr), "pick_road_base_addr" ),
                SqlUtils.sqlFunction( MysqlFunction.IFNULL, ObjectUtils.toArray(marketClaimInfo.collectDetailAddress, shippingAddr.roadDetailAddr), "pick_road_detail_addr" ),
                SqlUtils.sqlFunction( MysqlFunction.IFNULL, ObjectUtils.toArray(marketClaimInfo.collectTelNo1, shippingAddr.shipMobileNo), "pick_mobile_no" ),
                SqlUtils.sqlFunction( MysqlFunction.IFNULL, ObjectUtils.toArray(marketClaimInfo.returnName, shippingAddr.receiverNm), "exch_receiver_nm" ),
                SqlUtils.sqlFunction( MysqlFunction.IFNULL, ObjectUtils.toArray(marketClaimInfo.returnZipcode, shippingAddr.zipcode), "exch_zip_code" ),
                SqlUtils.sqlFunction( MysqlFunction.IFNULL, ObjectUtils.toArray(marketClaimInfo.returnBaseAddress, shippingAddr.roadBaseAddr), "exch_road_base_addr" ),
                SqlUtils.sqlFunction( MysqlFunction.IFNULL, ObjectUtils.toArray(marketClaimInfo.returnDetailAddress, shippingAddr.roadDetailAddr), "exch_road_detail_addr" ),
                SqlUtils.sqlFunction( MysqlFunction.IFNULL, ObjectUtils.toArray(marketClaimInfo.returnTelNo1, shippingAddr.shipMobileNo), "exch_mobile_no" ),
                purchaseOrder.userNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(marketClaimInfo))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(marketClaim, ObjectUtils.toArray(marketClaimInfo.marketClaimNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(marketClaim.marketOrderNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_MARKET")))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(marketClaimInfo.marketOrderItemNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo, orderOpt.orderItemNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo, shippingItem.bundleNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimInfo.marketClaimNo, marketClaimNo),
                SqlUtils.equalColumnByInput(marketClaimInfo.marketOrderItemNo, marketOrderItemNo)
            );
        log.debug("selectClaimExchangeAndReturnInfo :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPrevInsertDataCheck(@Param("marketClaimId") String marketClaimId) {
        MarketClaimEntity marketClaim = EntityFactory.createEntityIntoValue(MarketClaimEntity.class, Boolean.TRUE);
        MarketClaimItemEntity marketClaimItem = EntityFactory.createEntityIntoValue(MarketClaimItemEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, marketClaimItem.marketOrderItemNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(marketClaim))
            .INNER_JOIN(SqlUtils.createJoinCondition(marketClaimItem, ObjectUtils.toArray(marketClaim.marketOrderNo, marketClaim.marketClaimNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimItem.marketClaimId, marketClaimId)
            );
        log.debug("selectPrevInsertDataCheck :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updatePrevDataStatus(@Param("marketClaimId") String marketClaimId, MarketClaimItemDto marketClaimItemDto, MarketClaimInfoDto marketClaimInfoDto) {
        MarketClaimInfoEntity marketClaimInfo = EntityFactory.createEntityIntoValue(MarketClaimInfoEntity.class, "info");
        MarketClaimItemEntity marketClaimItem = EntityFactory.createEntityIntoValue(MarketClaimItemEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableNameWithAlias(marketClaimInfo, "info"))
            .INNER_JOIN(SqlUtils.createJoinCondition(marketClaimItem, ObjectUtils.toArray(marketClaimInfo.marketClaimItemNo, marketClaimItem.marketOrderItemNo)))
            .SET(
                // item정보 업데이트
                SqlUtils.equalColumnByInput(marketClaimItem.claimStatus, marketClaimItemDto.getClaimStatus()),
                SqlUtils.equalColumnByInput(marketClaimItem.shipAmt, marketClaimItemDto.getShipAmt()),
                SqlUtils.equalColumnByInput(marketClaimItem.couponDiscountAmt, marketClaimItemDto.getCouponDiscountAmt()),
                SqlUtils.equalColumnByInput(marketClaimItem.itemDiscountAmt, marketClaimItemDto.getItemDiscountAmt()),
                SqlUtils.equalColumnByInput(marketClaimItem.paymentAmt, marketClaimItemDto.getPaymentAmt()),
                SqlUtils.equalColumnByInput(marketClaimItem.promoDiscountAmt, marketClaimItemDto.getPromoDiscountAmt()),
                // info 업데이트
                SqlUtils.equalColumnByInput(marketClaimInfo.claimStatus, marketClaimInfoDto.getClaimStatus()),
                SqlUtils.equalColumnByInput(marketClaimInfo.claimDeliveryFeeDemandAmount, marketClaimInfoDto.getClaimDeliveryFeeDemandAmount()),
                SqlUtils.equalColumnByInput(marketClaimInfo.claimDeliveryFeeDiscountAmount, marketClaimInfoDto.getClaimDeliveryFeeDiscountAmount()),
                SqlUtils.equalColumnByInput(marketClaimInfo.etcFeeDemandAmount, marketClaimInfoDto.getEtcFeeDemandAmount()),
                SqlUtils.equalColumn(marketClaimInfo.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(marketClaimInfo.chgId, "BATCH"),
                SqlUtils.equalColumn(marketClaimItem.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(marketClaimItem.chgId, "BATCH")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimItem.marketClaimId, marketClaimId)
            );
        log.debug("selectPrevInsertDataCheck :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateMarketClaimAmountByRetry(@Param("marketClaimId") String marketClaimId, MarketClaimDto marketClaimDto) {
        MarketClaimEntity marketClaim = EntityFactory.createEntityIntoValue(MarketClaimEntity.class, Boolean.TRUE);
        MarketClaimItemEntity marketClaimItem = EntityFactory.createEntityIntoValue(MarketClaimItemEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableNameWithAlias(marketClaim))
            .INNER_JOIN(SqlUtils.createJoinCondition(marketClaimItem, ObjectUtils.toArray(marketClaim.marketClaimNo, marketClaim.marketOrderNo)))
            .SET(
                SqlUtils.equalColumnByInput(marketClaim.tradeTotAmt, marketClaimDto.getTradeTotAmt()),
                SqlUtils.equalColumnByInput(marketClaim.itemAmt, marketClaimDto.getItemAmt()),
                SqlUtils.equalColumnByInput(marketClaim.discountAmt, marketClaimDto.getDiscountAmt()),
                SqlUtils.equalColumnByInput(marketClaim.couponDiscountAmt, marketClaimDto.getCouponDiscountAmt()),
                SqlUtils.equalColumnByInput(marketClaim.shipAmt, marketClaimDto.getShipAmt()),
                SqlUtils.equalColumnByInput(marketClaim.tradeAmt, marketClaimDto.getTradeAmt()),
                SqlUtils.equalColumnByInput(marketClaim.itemDiscountAmt, marketClaimDto.getItemDiscountAmt()),
                SqlUtils.equalColumnByInput(marketClaim.promoDiscountAmt, marketClaimDto.getPromoDiscountAmt()),
                SqlUtils.equalColumnByInput(marketClaim.shipDiscountAmt, marketClaimDto.getShipDiscountAmt()),
                SqlUtils.equalColumnByInput(marketClaim.tradeRcvAmt, marketClaimDto.getTradeRcvAmt())
            )
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimItem.marketClaimId, marketClaimId)
            );
        log.debug("updateMarketClaimAmountByRetry :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateHoldbackStatus(@Param("marketOrderItemNo") String marketOrderItemNo, @Param("holdbackStatus") String holdbackStatus) {
        MarketClaimInfoEntity marketClaimInfo = EntityFactory.createEntityIntoValue(MarketClaimInfoEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(marketClaimInfo))
            .SET(
                SqlUtils.equalColumnByInput(marketClaimInfo.holdbackStatus, holdbackStatus),
                SqlUtils.equalColumn(marketClaimInfo.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(marketClaimInfo.chgId, "BATCH")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimInfo.marketOrderItemNo, marketOrderItemNo)
            );
        log.debug("updateHoldbackStatus :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectExistingClaimCheck(@Param("marketClaimNo") long marketClaimNo) {
        MarketClaimItemEntity marketClaimItem = EntityFactory.createEntityIntoValue(MarketClaimItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                claimBundle.claimNo,
                claimBundle.claimStatus
            )
            .FROM(EntityFactory.getTableNameWithAlias(marketClaimItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(marketClaimItem.marketType, marketClaimItem.marketOrderNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo, marketClaimItem.marketOrderItemNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimItem.marketClaimNo, marketClaimNo),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C3'", "'C4'", "'C9'"))
            );
        log.debug("selectExistingClaimCheck :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateMarketClaimAmount(@Param("claimNo") long claimNo, @Param("paymentAmt") long paymentAmt, @Param("claimDeliveryFeeDemandAmount") long claimDeliveryFeeDemandAmount, @Param("shipAmt") long shipAmt, @Param("promoDiscountAmt") long promoDiscountAmt) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimMst.claimNo))
            )
            .SET(
                SqlUtils.equalColumn(claimMst.completeAmt, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(paymentAmt, claimDeliveryFeeDemandAmount))),
                SqlUtils.equalColumn(
                    claimMst.totDiscountAmt,
                    SqlUtils.sqlFunction(
                        MysqlFunction.CASE,
                        ObjectUtils.toArray(
                            SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, claimMst.totDiscountAmt, "0"),
                            SqlUtils.customOperator(
                                CustomConstants.PLUS,
                                ObjectUtils.toArray(
                                    claimMst.totDiscountAmt,
                                    SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(promoDiscountAmt, claimMst.totPromoDiscountAmt)),
                                    SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(claimDeliveryFeeDemandAmount, claimMst.totAddShipPrice))
                                )
                            ),
                            SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimMst.totDiscountAmt, claimDeliveryFeeDemandAmount, promoDiscountAmt))
                        )
                    )
                ),
                SqlUtils.equalColumnByInput(claimMst.totShipPrice, shipAmt),
                SqlUtils.equalColumnByInput(claimMst.totAddShipPrice, claimDeliveryFeeDemandAmount),
                SqlUtils.equalColumn(claimMst.pgAmt, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(paymentAmt, claimDeliveryFeeDemandAmount))),
                SqlUtils.equalColumn(claimPayment.paymentAmt, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(paymentAmt, claimDeliveryFeeDemandAmount))),
                SqlUtils.equalColumnByInput(claimMst.totPromoDiscountAmt, promoDiscountAmt)
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            );
        log.debug("updateMarketClaimAmount :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMarketClaimAmountInfoByModify(@Param("claimNo") long claimNo) {
        MarketClaimItemEntity marketClaimItem = EntityFactory.createEntityIntoValue(MarketClaimItemEntity.class, Boolean.TRUE);
        MarketClaimInfoEntity marketClaimInfo = EntityFactory.createEntityIntoValue(MarketClaimInfoEntity.class, "info");
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        OrderDiscountMarketEntity orderDiscountMarket = EntityFactory.createEntityIntoValue(OrderDiscountMarketEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        MarketClaimEntity marketClaim = EntityFactory.createEntityIntoValue(MarketClaimEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        MarketOrderEntity marketOrder = EntityFactory.createEntityIntoValue(MarketOrderEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, marketClaimItem.paymentAmt, marketClaimItem.paymentAmt),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.MAX, marketClaimInfo.claimDeliveryFeeDemandAmount), marketClaimInfo.claimDeliveryFeeDemandAmount),
                SqlUtils.sqlFunction(MysqlFunction.MAX, marketClaimItem.shipAmt, marketClaimItem.shipAmt),
                SqlUtils.customOperator(
                    CustomConstants.PLUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totAddShipPrice),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totAddIslandShipPrice)
                    ),
                    "tot_add_ship_amt"
                ),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, orderDiscountMarket.itemDiscountAmt, orderDiscountMarket.itemDiscountAmt),
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL_SUM_ZERO,
                    SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(orderDiscountMarket.sellerChargeDiscountAmt, orderDiscountMarket.sellerChargeCouponDiscountAmt)),
                    orderDiscountMarket.sellerChargeDiscountAmt
                ),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.shipPrice, bundle.shipPrice),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.claimShipFee, bundle.claimShipFee),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.shipFee, bundle.shipFee),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, marketClaimItem.promoDiscountAmt, marketClaimItem.promoDiscountAmt),
                SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, marketOrder.tradeRcvAmt), SqlUtils.sqlFunction(MysqlFunction.MAX, marketClaim.tradeRcvAmt)), marketClaim.tradeAmt),
                SqlUtils.sqlFunction(MysqlFunction.FN_PREV_CLAIM_AMT, ObjectUtils.toArray(claimMst.purchaseOrderNo, "'PG'"), "prev_payment_amt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.purchaseOrderNo, claimMst.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, marketClaimItem.marketOrderNo, marketClaimItem.marketOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, marketClaimItem.claimType, marketClaimItem.claimType)
            )
            .FROM(EntityFactory.getTableNameWithAlias(marketClaimItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(marketClaimInfo, "info"),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(marketClaimItem.marketClaimNo, marketClaimItem.marketClaimItemNo), marketClaimInfo)
                    )
                )
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(marketClaimItem.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(claimBundle.purchaseOrderNo, claimBundle.bundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(marketClaim, ObjectUtils.toArray(marketClaimItem.marketClaimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimBundle.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(marketOrder, ObjectUtils.toArray(marketClaimItem.marketOrderNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(orderDiscountMarket, ObjectUtils.toArray(purchaseOrder.marketType, marketClaimItem.marketOrderNo, marketClaimItem.marketOrderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimItem.claimNo, claimNo)
            );
        log.debug("selectMarketClaimAmountInfoByModify :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMarketClaimAdditionCheckByAddShip(@Param("claimNo") long claimNo) {
        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimAddition.additionSumAmt)
            )
            .FROM(EntityFactory.getTableName(claimAddition))
            .WHERE(
                SqlUtils.equalColumnByInput(claimAddition.additionType, "S"),
                SqlUtils.equalColumnByInput(claimAddition.claimNo, claimNo)
            );
        log.debug("selectMarketClaimAdditionCheckByAddShip :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateClaimAdditionByMarketShipAmt(@Param("claimNo") long claimNo, @Param("marketShipAmt") long marketShipAmt) {
        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimAddition))
            .SET(
                SqlUtils.equalColumn(
                    claimAddition.additionSumAmt,
                    SqlUtils.sqlFunction(
                        MysqlFunction.CASE,
                        ObjectUtils.toArray(
                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimAddition.additionSumAmt, "0"),
                            SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimAddition.additionSumAmt, marketShipAmt)),
                            "0"
                        )
                    )
                ),
                SqlUtils.equalColumn(
                    claimAddition.additionAmtBuyer,
                    SqlUtils.sqlFunction(
                        MysqlFunction.CASE,
                        ObjectUtils.toArray(
                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimAddition.additionAmtBuyer, "0"),
                            SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimAddition.additionAmtBuyer, marketShipAmt)),
                            "0"
                        )
                    )
                ),
                SqlUtils.equalColumn(
                    claimAddition.additionAmtSeller,
                    SqlUtils.sqlFunction(
                        MysqlFunction.CASE,
                        ObjectUtils.toArray(
                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimAddition.additionAmtSeller, "0"),
                            SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimAddition.additionAmtSeller, marketShipAmt)),
                            "0"
                        )
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimAddition.additionType, "S"),
                SqlUtils.equalColumnByInput(claimAddition.additionTypeDetail, "pickup"),
                SqlUtils.equalColumnByInput(claimAddition.claimNo, claimNo)
            );
        log.debug("updateClaimAdditionByMarketShipAmt :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String insertClaimAdditionByMarketShipAmt(ClaimAdditionRegDto claimAdditionRegDto) {
        String[] claimAddition = EntityFactory.getColumnInfo(ClaimAdditionEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(ClaimAdditionEntity.class))
            .INTO_COLUMNS(Arrays.copyOfRange(claimAddition, 1, claimAddition.length))
            .INTO_VALUES(
                SqlUtils.convertInsertParamToCamelCase(Arrays.copyOfRange(claimAddition, 1, claimAddition.length) )
            );
        log.debug("insertClaimAdditionByMarketShipAmt :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimAdditionAddDataByMarketClaim(@Param("claimNo") long claimNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class);
        SQL query = new SQL()
            .SELECT(
                claimBundle.claimNo,
                claimBundle.claimBundleNo,
                claimBundle.purchaseOrderNo,
                claimBundle.bundleNo,
                claimBundle.userNo
            )
            .FROM(EntityFactory.getTableName(claimBundle))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo)
            );
        log.debug("selectClaimAdditionAddDataByMarketClaim :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPrevMarketClaimShipAmt(@Param("purchaseOrderNo") long purchaseOrderNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.PLUS, claimMst.totShipPrice, claimMst.totIslandShipPrice), "prev_ship_amt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectPrevMarketClaimShipAmt :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderDiscountMarketSumPrice(@Param("marketOrderNo") String marketOrderNo) {
        OrderDiscountMarketEntity orderDiscountMarket = EntityFactory.createEntityIntoValue(OrderDiscountMarketEntity.class);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, orderDiscountMarket.itemDiscountAmt, orderDiscountMarket.itemDiscountAmt),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(orderDiscountMarket.sellerChargeDiscountAmt, orderDiscountMarket.sellerChargeCouponDiscountAmt)), orderDiscountMarket.sellerChargeDiscountAmt)
            )
            .FROM(EntityFactory.getTableName(orderDiscountMarket))
            .WHERE(
                SqlUtils.equalColumnByInput(orderDiscountMarket.marketOrderNo, marketOrderNo)
            );
        log.debug("selectOrderDiscountMarketSumPrice :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMarketClaimList(@Param("marketClaimNo") long marketClaimNo) {
        MarketClaimItemEntity marketClaimItem = EntityFactory.createEntityIntoValue(MarketClaimItemEntity.class);
        SQL query = new SQL()
            .SELECT(
                marketClaimItem.marketClaimNo,
                marketClaimItem.marketClaimItemNo,
                marketClaimItem.marketOrderNo,
                marketClaimItem.marketOrderItemNo
            )
            .FROM(EntityFactory.getTableName(marketClaimItem))
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimItem.marketClaimNo, marketClaimNo)
            );
        log.debug("selectMarketClaimList :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPrevMarketClaimDiscount(@Param("marketOrderNo") String marketOrderNo, @Param("claimNo") long claimNo) {
        MarketClaimItemEntity marketClaimItem = EntityFactory.createEntityIntoValue(MarketClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        OrderDiscountMarketEntity orderDiscountMarket = EntityFactory.createEntityIntoValue(OrderDiscountMarketEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL_SUM_ZERO,
                    SqlUtils.customOperator(
                        CustomConstants.MINUS,
                        orderDiscountMarket.itemDiscountAmt,
                        SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(orderDiscountMarket.sellerChargeDiscountAmt, orderDiscountMarket.sellerChargeCouponDiscountAmt))
                    ),
                    "discount_amt"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(marketClaimItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(marketClaimItem.claimNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                    )
                )
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(orderDiscountMarket, ObjectUtils.toArray(marketClaimItem.marketOrderNo, marketClaimItem.marketOrderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimItem.marketOrderNo, marketOrderNo),
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, marketClaimItem.claimNo, String.valueOf(claimNo))
            );
        log.debug("selectPrevMarketClaimDiscount :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateMarketClaimBundleIsEnclose(@Param("claimNo") long claimNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimBundle))
            .SET(
                SqlUtils.equalColumnByInput(claimBundle.claimShipFeeEnclose, "N")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo)
            );
        log.debug("updateMarketClaimBundleIsEnclose :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMarketOrderItemCnt(@Param("marketOrderNo") String marketOrderNo) {
        MarketOrderItemEntity marketOrderItem = EntityFactory.createEntityIntoValue(MarketOrderItemEntity.class);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, marketOrderItem.marketOrderItemNo, "cnt")
            )
            .FROM(EntityFactory.getTableName(marketOrderItem))
            .WHERE(
                SqlUtils.equalColumnByInput(marketOrderItem.marketOrderNo, marketOrderNo)
            );
        log.debug("selectMarketOrderItemCnt :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMarketClaimItemCnt(@Param("marketOrderNo") String marketOrderNo) {
        MarketClaimItemEntity marketClaimItem = EntityFactory.createEntityIntoValue(MarketClaimItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, marketClaimItem.marketOrderItemNo, "cnt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(marketClaimItem))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(marketClaimItem.marketType, marketClaimItem.marketOrderNo)))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(purchaseOrder.purchaseOrderNo, marketClaimItem.marketOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'"))
                    )
                )
            )
            .INNER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo, marketClaimItem.marketOrderNo, marketClaimItem.marketOrderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimItem.marketOrderNo, marketOrderNo)
            );
        log.debug("selectMarketClaimItemCnt :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMarketClaimStatusCheck(@Param("marketClaimNo") long marketClaimNo, @Param("claimStatus") String claimStatus) {
        MarketClaimItemEntity marketClaimItem = EntityFactory.createEntityIntoValue(MarketClaimItemEntity.class);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, marketClaimItem.claimStatus, "cnt")
            )
            .FROM(EntityFactory.getTableName(marketClaimItem))
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimItem.marketClaimNo, marketClaimNo),
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, marketClaimItem.claimStatus, SqlUtils.appendSingleQuote(claimStatus))
            );
        log.debug("selectMarketClaimStatusCheck :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMarketClaimingDataCheck(@Param("marketOrderNo") String marketOrderNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                claimMst.claimNo,
                claimItem.claimItemNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(claimMst.claimNo),
                    ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C3'", "'C4'", "'C9'")))
                )
            )
            .INNER_JOIN( SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)) )
            .INNER_JOIN( SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimNo, claimItem.claimBundleNo, claimItem.claimItemNo)) )
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.marketType, "NAVER"),
                SqlUtils.equalColumnByInput(claimMst.marketOrderNo, marketOrderNo)
            );
        log.debug("selectMarketClaimingDataCheck :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMarketClaimItemReqCnt(@Param("marketClaimNo") long marketClaimNo) {
        MarketClaimItemEntity marketClaimItem = EntityFactory.createEntityIntoValue(MarketClaimItemEntity.class);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, marketClaimItem.marketClaimItemNo, "cnt")
            )
            .FROM(EntityFactory.getTableName(marketClaimItem))
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimItem.marketClaimNo, marketClaimNo)
            );
        log.debug("selectMarketClaimItemReqCnt :: \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPrevClaimItemCntByMarket(@Param("marketClaimNo") long marketClaimNo) {
        MarketClaimItemEntity marketClaimItem = EntityFactory.createEntityIntoValue(MarketClaimItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.COUNT, claimOpt.claimOptNo, "CNT")
            )
            .FROM(EntityFactory.getTableNameWithAlias(marketClaimItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(marketClaimItem.marketType, marketClaimItem.marketOrderNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo, marketClaimItem.marketOrderItemNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(marketClaimItem.marketClaimNo, marketClaimNo),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C3'", "'C4'", "'C9'"))
            );
        log.debug("selectPrevClaimItemCntByMarket :: \n[{}]", query.toString());
        return query.toString();
    }
}
