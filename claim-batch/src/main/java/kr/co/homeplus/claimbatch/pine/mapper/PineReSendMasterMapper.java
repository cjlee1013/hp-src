package kr.co.homeplus.claimbatch.pine.mapper;

import kr.co.homeplus.claimbatch.core.db.annotation.MasterConnection;
import kr.co.homeplus.claimbatch.pine.model.PineReSendDto;

@MasterConnection
public interface PineReSendMasterMapper {

    // resend_claim_pine_target 테이블에 재송신할 거래번호를 입력한다.
    int insertReSendClaim(PineReSendDto pineReSendDto);

    // resend_claim_pine_target에 있는 거래번호를 기준으로 재송신한다.
    void pineReSendExecute();
}
