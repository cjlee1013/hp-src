package kr.co.homeplus.claimbatch.claim.sql;

import kr.co.homeplus.claimbatch.claim.constants.CustomConstants;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.BundleEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderOptEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claimbatch.claim.entity.ship.ShippingAddrEntity;
import kr.co.homeplus.claimbatch.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claimbatch.claim.enums.MysqlFunction;
import kr.co.homeplus.claimbatch.claim.factory.EntityFactory;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import kr.co.homeplus.claimbatch.util.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class OrderDataSql {

    /**
     * 클레임에 필요한 데이터 생성
     *  -> 클레임 아이템 데이터 생성을 위한 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param orderItemNo 상품주문번호
     * @return 클레임 아이템 데이터 생성을 위한 데이터 쿼리
     */
    public static String selectClaimItemInfo( @Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderItemNo") long orderItemNo ) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);

        SQL query =  new SQL()
            .SELECT(
                orderItem.orderItemNo,
                orderItem.itemNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemNm1, "item_name"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemQty, orderItem.itemQty),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.itemPrice, orderItem.itemPrice),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.storeType, orderItem.storeType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.mallType, orderItem.mallType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.originStoreId, orderItem.originStoreId),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.storeId, orderItem.storeId),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.taxYn, orderItem.taxYn),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.empDiscountRate, orderItem.empDiscountRate),
                SqlUtils.sqlFunction(MysqlFunction.MAX, orderItem.empDiscountAmt, orderItem.empDiscountAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderType, purchaseOrder.orderType),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimItem.claimItemQty, claimItem.claimItemQty),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.marketOrderNo, purchaseOrder.marketOrderNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo, orderItem.purchaseOrderNo, orderItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.orderItemNo, orderItemNo)
            )
            .GROUP_BY(orderItem.orderItemNo, orderItem.itemNo);
        log.debug("selectClaimItemInfo ::: \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 클레임에 필요한 데이터 생성
     *  -> 클레임 번들 생성을 위한 데이터.
     *
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return 클레임 번들 생성을 위한 데이터 쿼리
     */
    public static String selectCreateClaimDataInfo( @Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ShippingAddrEntity shippingAddr = EntityFactory.createEntityIntoValue(ShippingAddrEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.paymentNo,
                purchaseOrder.siteType,
                purchaseOrder.userNo,
                bundle.bundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.shipStatus, shippingItem.shipStatus),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.partnerId, shippingItem.partnerId),
                SqlUtils.sqlFunction(MysqlFunction.FN_PAYMENT_TYPE, purchaseOrder.paymentNo, "payment_type"),
                SqlUtils.sqlFunction(MysqlFunction.FN_PAYMENT_TYPE, purchaseOrder.paymentNo, "refund_method"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingItem.invoiceNo, shippingItem.invoiceNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.empNo, purchaseOrder.empNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.reserveYn, purchaseOrder.reserveYn),
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo,  bundle.bundleNo, "'C'"), "order_category"),
                SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo,  bundle.bundleNo, "'M'")), "TD"), "close_send_yn"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.shipMobileNo, shippingAddr.shipMobileNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, shippingAddr.safetyUseYn, shippingAddr.safetyUseYn),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.marketOrderNo, purchaseOrder.marketOrderNo)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(bundle.bundleNo))
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(shippingAddr, ObjectUtils.toArray(shippingItem.shipAddrNo))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(bundle.bundleNo, bundleNo)
            )
            .GROUP_BY(purchaseOrder.purchaseOrderNo, purchaseOrder.paymentNo, purchaseOrder.siteType, purchaseOrder.userNo, bundle.bundleNo);
        log.debug("selectCreateClaimDataInfo ::: {}", query.toString());
        return query.toString();
    }

    public static String selectCreateClaimMstDataInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.paymentNo,
                purchaseOrder.siteType,
                purchaseOrder.userNo,
                purchaseOrder.empNo,
                purchaseOrder.gradeSeq,
                purchaseOrder.marketType,
                purchaseOrder.marketOrderNo
            )
            .FROM(EntityFactory.getTableName(purchaseOrder))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectClaimMstDtaInfo ::: {}", query.toString());
        return query.toString();
    }

    public static String selectPresentAutoCancelInfo(@Param("processDt") String processDt) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        // 기준데이터
        processDt = ((StringUtils.isNoneEmpty(processDt)) ? SqlUtils.appendSingleQuote(processDt) : SqlUtils.sqlFunction(MysqlFunction.DATEADD_DAY, ObjectUtils.toArray("NOW()", "-1")));
        SQL innerQuery = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                orderItem.bundleNo,
                orderItem.itemQty,
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        orderItem.itemQty,
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, claimItem.claimItemQty)
                    ),
                    "remain_qty"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.orderItemNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(shippingItem.shipStatus, "D0"))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimItem.claimBundleNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.NOT_IN, ObjectUtils.toArray("'C4'", "'C9'")))))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.orderGiftYn, "Y"),
                SqlUtils.equalColumnByInput(purchaseOrder.dsGiftAutoCancelYn, "N"),
                SqlUtils.sqlFunction(
                    MysqlFunction.BETWEEN,
                    ObjectUtils.toArray(
                        purchaseOrder.dsGiftAutoCancelDt,
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, processDt),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_LAST, processDt)
                    )
                )
            );
        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(purchaseOrder.purchaseOrderNo),
                SqlUtils.nonAlias(orderItem.bundleNo),
                "remain_qty",
                SqlUtils.sqlFunction(MysqlFunction.CASE, ObjectUtils.toArray(SqlUtils.equalColumn("item_qty", "remain_qty"), "'N'", "'Y'"), "claim_part_yn")
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "data"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, "remain_qty", "0")
            );
        log.debug("selectPresentAutoCancelInfo ::: {}", query.toString());
        return query.toString();
    }

    public static String updatePresentAutoCancel(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(purchaseOrder))
            .SET(
                SqlUtils.equalColumnByInput(purchaseOrder.dsGiftAutoCancelYn, "Y"),
                SqlUtils.equalColumn(purchaseOrder.chgDt, "NOW()"),
                SqlUtils.equalColumnByInput(purchaseOrder.chgId, "claim_batch")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("updatePresentAutoCancel ::: {}", query.toString());
        return query.toString();
    }

    public static String selectOriginOrderListByOmni(@Param("purchaseOrderNo") long purchaseOrderNo ) {
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                orderItem.itemNo,
                SqlUtils.aliasToRow(orderOpt.optQty, orderItem.itemQty),
                orderItem.orderItemNo,
                orderItem.bundleNo,
                orderOpt.orderOptNo,
                shippingItem.userNo,
                SqlUtils.sqlFunction(
                    MysqlFunction.IFNULL_ZERO,
                    SqlUtils.subTableQuery(
                        new SQL().SELECT(SqlUtils.sqlFunction(MysqlFunction.SUM, claimItem.claimItemQty))
                            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
                            .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo)))
                            .WHERE(
                                SqlUtils.equalColumn(claimBundle.purchaseOrderNo, orderItem.purchaseOrderNo),
                                SqlUtils.equalColumn(claimBundle.bundleNo, orderItem.bundleNo),
                                SqlUtils.equalColumn(claimItem.orderItemNo, orderItem.orderItemNo),
                                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'"))
                            )
                    ),
                    claimItem.claimItemQty
                ),
                orderItem.substitutionYn
            )
            .FROM(EntityFactory.getTableNameWithAlias(orderItem))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo)))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    shippingItem,
                    ObjectUtils.toArray(orderItem.orderItemNo),
                    ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")))
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(orderItem.purchaseOrderNo, purchaseOrderNo)
            )
            .ORDER_BY(SqlUtils.orderByesc(orderItem.itemNo, orderOpt.optQty))
            ;
        log.debug("selectOriginOrderListByOmni :: \n[{}]", query.toString());
        return query.toString();
    }

}
