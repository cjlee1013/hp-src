package kr.co.homeplus.claimbatch.claim.model.register;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimPaymentRegDto {

    @ApiModelProperty(value= "클레임 결제번호(클레임 결제수단별 생성)")
    private long claimPaymentNo;

    @ApiModelProperty(value= "클레임번호(환불:클레임 번호)")
    private long claimNo;

    @ApiModelProperty(value= "결제번호")
    private long paymentNo;

    @ApiModelProperty(value= "주문번호")
    private long purchaseOrderNo;

    @ApiModelProperty(value= "PG종류(INICIS,TOSSPG)")
    private String pgKind;

    @ApiModelProperty(value= "환불수단(PG:PG취소, BK:계좌입금,PP:포인트 지급, PT : 포인트 사용취소,MP:MHC포인트,DG:DGV상품권, OC:OCB포인트)")
    private String paymentType;

    @ApiModelProperty(value= "결제수단 상위코드(CARD:카드, VBANK:무통장입금, PHONE:휴대폰결제, RBANK: 실시간계좌이체, EASY: 간편결제,FREE:0원결제)")
    private String parentPaymentMethod;

    @ApiModelProperty(value= "결제수단코드")
    private String methodCd;

    @ApiModelProperty(value= "간편결제수단코")
    private String easyMethodCd;

    @ApiModelProperty(value= "PG 상점ID")
    private String pgMid;

    @ApiModelProperty(value= "PG 인증Key값")
    private String pgOid;

    @ApiModelProperty(value= "PG 승인Key값")
    private String paymentTradeId;

    @ApiModelProperty(value= "결제토큰(TOSS)")
    private String payToken;

    @ApiModelProperty(value= "원거래 승인번호")
    private String originApprovalCd;

    @ApiModelProperty(value= "클레임 승인번호")
    private String claimApprovalCd;

    @ApiModelProperty(value= "결제 상태(P1: 요청, P2: 진행중, P3: 완료, P4:실패)")
    private String paymentStatus;

    @ApiModelProperty(value= "환불 금액")
    private long paymentAmt;

    @ApiModelProperty(value= "대체주문금액")
    private long substitutionAmt;

    @ApiModelProperty(value= "원 환불요청 결제번호(대체환불수단 등록시 사용됨)")
    private long originClaimPaymentNo;

    @ApiModelProperty(value= "PG 취소 요청 시도횟수(3회까지 재시도)")
    private int pgCancelRequestCnt;

    @ApiModelProperty(value= "등록일")
    private String regDt;

    @ApiModelProperty(value= "등록아이디")
    private String regId;

    @ApiModelProperty(value= "수정일")
    private String chgDt;

    @ApiModelProperty(value= "수정아이디")
    private String chgId;
}
