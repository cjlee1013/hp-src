package kr.co.homeplus.claimbatch.claim.model.register;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClaimReqRegDto {
    @ApiModelProperty(value= "쿨래암 요청번호")
    private long claimReqNo;
    @ApiModelProperty(value= "클레임 묶음 배송번호")
    private long claimBundleNo;
    @ApiModelProperty(value= "클레임번호(환불:클레임번호)")
    private long claimNo;
    @ApiModelProperty(value= "클레임사유코드(claim_code.claim_depth2 = request )")
    private String claimReasonType;
    @ApiModelProperty(value= "클레임사유상세")
    private String claimReasonDetail;
    @ApiModelProperty(value= "클레임 요청자 정보(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  그외에는 사번으로 인식하여 사원정보 노출)")
    private String requestId;
    @ApiModelProperty(value= "요청일자")
    private String requestDt;
    @ApiModelProperty(value= "첨부파일")
    private String uploadFileName;
    @ApiModelProperty(value= "첨부파일명2")
    private String uploadFileName2;
    @ApiModelProperty(value= "첨부파일명3")
    private String uploadFileName3;
    @ApiModelProperty(value= "등록일")
    private String regDt;
    @ApiModelProperty(value= "수정일")
    private String chgDt;
}
