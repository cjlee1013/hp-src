package kr.co.homeplus.claimbatch.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ExternalUrlInfo {
    PAYMENT_CANCEL_PG("payment", "/pg/pgCancel"),
    PAYMENT_CANCEL_MILEAGE("mileage", "/cancel/cancelMileageRestore"),
    PAYMENT_SAVE_MILEAGE("mileage", "/external/external/mileageSave"),
    PAYMENT_SAVE_CANCEL_MILEAGE("mileage", "/external/external/mileageCancel"),
    PAYMENT_CANCEL_OCB("payment", "/ocb/cancelOcbPoint"),
    PAYMENT_CANCEL_MHC_ALL("escrow", "/mhc/external/cancelMhcPoint"),
    PAYMENT_CANCEL_MHC_PART("escrow", "/mhc/external/partCancelMhcPoint"),
    PAYMENT_SAVE_CANCEL_MHC_ALL("escrow", "/mhc/external/saveCancelMhcPoint"),
    PAYMENT_SAVE_CANCEL_MHC_PART("escrow", "/mhc/external/partSaveCancelMhcPoint"),
    PAYMENT_CANCEL_DGV("payment", "/dgv/cancelDgvGiftCard"),
    PAYMENT_CANCEL_DGV_RECEIPT("escrow", "/dgv/external/dgvPosReceiptIssue"),
    RESTORE_SLOT_CLAIM("escrow", "/slot/external/restoreSlotStockForClaim"),
    RESTORE_STOCK_FOR_CLAIM("escrow", "/stock/restoreStockForClaim"),
    RESTORE_SLOT_CLAIM_MARKET("escrow", "/market/stock/restoreStockForClaim"),
    USER_MHC_CARD_INFO("user", "/search/basic/getUserInfo"),
    SHIPPING_CLAIM_FROM_SHIP_COMPLETE("shipping", "/admin/shipManage/setShipCompleteByBundle"),
    SHIPPING_CLAIM_FROM_SHIP_DECISION("shipping", "/admin/shipManage/setPurchaseCompleteByBundle"),
    EMP_DISCOUNT_LIMIT_AMT_INFO("escrow", "/ordinis/external/getEmpInfo"),
    CLAIM_SAFETY_ISSUE_ADD("escrow", "/safetyissue/addClaimSafetyNo"),
    CLAIM_BY_COUPON_RE_ISSUE("front","/coupon/escrow/setCouponReIssued"),
    OUTBOUND_COOP_TICKET_CHECK_FOR_CLAIM("outbound", "/coop/cancelTicketInfo"),
    OUTBOUND_COOP_TICKET_CANCEL_FOR_CLAIM("outbound", "/coop/cancelTicket"),
    OUTBOUND_MARKET_NAVER_CLAIM_LIST("outbound", "/market/naver/getClaimRequestList"),
    OUTBOUND_MARKET_NAVER_ITEM_STATUS_LIST("outbound", "/market/naver/getMarketClaimStatus"),
    MESSAGE_SEND_ALIM_TALK("message", "/send/alimtalk"),
    ;

    @Getter
    private final String apiId;

    @Getter
    private final String uri;

}
