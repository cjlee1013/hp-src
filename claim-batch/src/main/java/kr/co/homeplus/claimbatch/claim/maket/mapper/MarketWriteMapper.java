package kr.co.homeplus.claimbatch.claim.maket.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimItemDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimInfoDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.sql.MarketClaimSql;
import kr.co.homeplus.claimbatch.core.db.annotation.MasterConnection;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

@MasterConnection
public interface MarketWriteMapper {

    @Options(useGeneratedKeys = true, keyProperty="marketClaimNo")
    @InsertProvider(type = MarketClaimSql.class, method = "insertMarketClaim")
    int insertMarketClaim(MarketClaimDto claimOrderDto);

    @Options(useGeneratedKeys = true, keyProperty="marketClaimItemNo")
    @InsertProvider(type = MarketClaimSql.class, method = "insertMarketClaimItem")
    int insertMarketClaimItem(MarketClaimItemDto marketClaimItemDto);

    @InsertProvider(type = MarketClaimSql.class, method = "insertMarketClaimInfo")
    int insertMarketClaimInfo(MarketClaimInfoDto marketClaimInfoDto);

    @InsertProvider(type = MarketClaimSql.class, method = "insertMarketClaimItemCheck")
    int insertMarketClaimItemCheck(MarketClaimItemDto marketClaimItemDto);

    @UpdateProvider(type = MarketClaimSql.class, method = "updateMarketClaimItemStatus")
    int updateMarketClaimItemStatus(@Param("marketClaimNo") long marketClaimNo, @Param("rcvYn") String rcvYn);

    @UpdateProvider(type = MarketClaimSql.class, method = "updateMarketClaimItemByClaimNo")
    int updateMarketClaimItemByClaimNo(@Param("marketClaimNo") long marketClaimNo, @Param("claimNo") long claimNo);

    @UpdateProvider(type = MarketClaimSql.class, method = "updatePrevDataStatus")
    int updatePrevDataStatus(@Param("marketClaimId") String marketClaimId, MarketClaimItemDto marketClaimItemDto, MarketClaimInfoDto marketClaimInfoDto);

    @UpdateProvider(type = MarketClaimSql.class, method = "updateMarketClaimAmountByRetry")
    int updateMarketClaimAmountByRetry(@Param("marketClaimId") String marketClaimId, MarketClaimDto marketClaimDto);

    @UpdateProvider(type = MarketClaimSql.class, method = "updateHoldbackStatus")
    int updateHoldbackStatus(@Param("marketOrderItemNo") String marketOrderItemNo, @Param("holdbackStatus") String holdbackStatus);

    @SelectProvider(type = MarketClaimSql.class, method = "selectMarketClaimList")
    List<LinkedHashMap<String, Object>> selectMarketClaimList(@Param("marketClaimNo") long marketClaimNo);

    @SelectProvider(type = MarketClaimSql.class, method = "selectMarketOrderItemCnt")
    int selectMarketOrderItemCnt(@Param("marketOrderNo") String marketOrderNo);

    @SelectProvider(type = MarketClaimSql.class, method = "selectMarketClaimItemCnt")
    int selectMarketClaimItemCnt(@Param("marketOrderNo") String marketOrderNo);

    @SelectProvider(type = MarketClaimSql.class, method = "selectMarketClaimStatusCheck")
    int selectMarketClaimStatusCheck(@Param("marketClaimNo") long marketClaimNo, @Param("claimStatus") String claimStatus);

    @SelectProvider(type = MarketClaimSql.class, method = "selectMarketClaimingDataCheck")
    List<LinkedHashMap<String, Object>> selectMarketClaimingDataCheck(@Param("marketOrderNo") String marketOrderNo);
}
