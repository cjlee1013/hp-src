package kr.co.homeplus.claimbatch.statistics.mapper;

import java.util.HashMap;
import kr.co.homeplus.claimbatch.core.db.annotation.MasterConnection;
import org.apache.ibatis.annotations.Param;

@MasterConnection
public interface StatisticsWriteMapper {
    void callByOrderSubstitutionStatistics(@Param("basicDt") String basicDt);
}
