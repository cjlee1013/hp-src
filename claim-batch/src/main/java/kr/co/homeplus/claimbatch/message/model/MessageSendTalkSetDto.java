package kr.co.homeplus.claimbatch.message.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
@ApiModel(description = "알람톡 발송 DTO")
public class MessageSendTalkSetDto {
    @ApiModelProperty(value = "템플릿 코드", required = true)
    private String templateCode;
    @ApiModelProperty(value = "전환발송 될 SMS 템플릿 코드", required = true)
    private String switchTemplateCode;
    @ApiModelProperty(value = "발송 캠페인 명", required = true)
    @JsonAlias({"workName", "coupledWorkName"})
    protected String workName;
    @ApiModelProperty(value = "발송 캠페인 설명", required = true)
    protected String description;
    @ApiModelProperty(value = "등록자", required = true)
    protected String regId;
    @ApiModelProperty(value = "개인별 매핑 데이터", position = 2)
    private List<MessageSendBodyDto> bodyArgument;

    @Data
    @Builder
    public static class MessageSendBodyDto {
        @ApiModelProperty(value = "claimNo", required = true)
        private String identifier;
        @ApiModelProperty(value = "전화번호", required = true)
        private String toToken;
        @ApiModelProperty("메시지매핑데이터")
        private Map<String, String> mappingData;
    }
}
