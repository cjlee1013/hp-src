package kr.co.homeplus.claimbatch.claim.sql;

import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimTicketEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderTicketEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claimbatch.claim.enums.MysqlFunction;
import kr.co.homeplus.claimbatch.claim.factory.EntityFactory;
import kr.co.homeplus.claimbatch.claim.model.register.ticket.ClaimTicketModifyDto;
import kr.co.homeplus.claimbatch.claim.model.register.ticket.ClaimTicketRegDto;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import kr.co.homeplus.claimbatch.util.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ClaimOrderTicketSql {

    /**
     * 티켓 취소를 위한 티켓정보 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param orderItemNo 상품주문번호
     * @return 티쳇정보
     */
    public static String selectOrderTicketInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("orderItemNo") long orderItemNo, @Param("cancelCnt") int cancelCnt) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderTicketEntity orderTicket = EntityFactory.createEntityIntoValue(OrderTicketEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                orderTicket.orderTicketNo,
                orderTicket.purchaseOrderNo,
                orderTicket.bundleNo,
                orderTicket.orderItemNo,
                orderTicket.sellerItemCd,
                orderTicket.ticketCd,
                orderTicket.ticketStatus,
                orderTicket.issueType,
                orderTicket.issueStatus,
                orderTicket.issueChannel
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderTicket, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo, orderItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.orderItemNo, orderItemNo),
                SqlUtils.sqlFunction(MysqlFunction.NOT_IN, orderTicket.ticketStatus, ObjectUtils.toArray("'T2'", "'T4'"))
            )
            .ORDER_BY(orderTicket.orderTicketNo)
            .LIMIT(cancelCnt)
            .OFFSET(0);
        log.debug("selectOrderTicketInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String insertClaimTicket(ClaimTicketRegDto claimTicketRegDto) {
        ClaimTicketEntity claimTicket = EntityFactory.createEntityIntoValue(ClaimTicketEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(EntityFactory.getTableName(claimTicket))
            .INTO_COLUMNS(
                claimTicket.claimNo, claimTicket.claimBundleNo, claimTicket.claimItemNo, claimTicket.orderItemNo,
                claimTicket.orderTicketNo, claimTicket.regDt, claimTicket.chgDt
            )
            .INTO_VALUES(
                String.valueOf(claimTicketRegDto.getClaimNo()),
                String.valueOf(claimTicketRegDto.getClaimBundleNo()),
                String.valueOf(claimTicketRegDto.getClaimItemNo()),
                String.valueOf(claimTicketRegDto.getOrderItemNo()),
                String.valueOf(claimTicketRegDto.getOrderTicketNo()),
                String.valueOf(claimTicketRegDto.getRegDt()),
                String.valueOf(claimTicketRegDto.getChgDt())
            );
        log.debug("insertClaimTicket Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimTicketInfo(@Param("claimNo") long claimNo) {
        ClaimTicketEntity claimTicket = EntityFactory.createEntityIntoValue(ClaimTicketEntity.class, Boolean.TRUE);
        OrderTicketEntity orderTicket = EntityFactory.createEntityIntoValue(OrderTicketEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimTicket.orderTicketNo,
                orderTicket.sellerItemCd,
                orderTicket.ticketCd,
                orderTicket.ticketStatus,
                orderTicket.issueChannel,
                orderTicket.issueStatus,
                claimTicket.claimTicketNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimTicket))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderTicket, ObjectUtils.toArray(claimTicket.orderItemNo, orderTicket.orderTicketNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimTicket.claimBundleNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, claimBundle.claimStatus, ObjectUtils.toArray("'C1'", "'C2'", "'C8'")))))
            .WHERE(
                SqlUtils.equalColumnByInput(claimTicket.claimNo, claimNo)
            );
        log.debug("selectClaimTicketInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String updateOrderTicketStatus(ClaimTicketModifyDto modifyDto){
        ClaimTicketEntity claimTicket = EntityFactory.createEntityIntoValue(ClaimTicketEntity.class, Boolean.TRUE);
        OrderTicketEntity orderTicket = EntityFactory.createEntityIntoValue(OrderTicketEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableNameWithAlias(claimTicket))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderTicket, ObjectUtils.toArray(claimTicket.orderTicketNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimTicket.claimBundleNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, claimBundle.claimStatus, ObjectUtils.toArray("'C1'", "'C2'", "'C8'")))))
            .SET(
                SqlUtils.equalColumn(orderTicket.chgDt, "NOW()")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimTicket.claimTicketNo, modifyDto.getClaimTicketNo())
            );
        if(modifyDto.getTicketStatus() != null){
            query.SET(
                SqlUtils.equalColumnByInput(orderTicket.ticketStatus, modifyDto.getTicketStatus())
            );
        }
        if(modifyDto.getIssueStatus() != null){
            query.SET(
                SqlUtils.equalColumnByInput(orderTicket.issueStatus, modifyDto.getIssueStatus())
            );
        }
        log.debug("updateOrderTicketStatus Query :: \n [{}]", query.toString());
        return query.toString();
    }

}
