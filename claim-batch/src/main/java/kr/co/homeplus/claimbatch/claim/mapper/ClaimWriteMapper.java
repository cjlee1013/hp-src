package kr.co.homeplus.claimbatch.claim.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claimbatch.claim.enums.ClaimCode;
import kr.co.homeplus.claimbatch.claim.maket.naver.sql.MarketClaimSql;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimAdditionRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimBundleRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimExchShippingRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimInfoRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimItemRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimPaymentRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimReqRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimShippingRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.safety.ClaimSafetySetDto;
import kr.co.homeplus.claimbatch.claim.model.register.ticket.ClaimTicketModifyDto;
import kr.co.homeplus.claimbatch.claim.model.register.ticket.ClaimTicketRegDto;
import kr.co.homeplus.claimbatch.claim.sql.ClaimManagementSql;
import kr.co.homeplus.claimbatch.claim.sql.ClaimMinusRefundSql;
import kr.co.homeplus.claimbatch.claim.sql.ClaimOrderCancelSql;
import kr.co.homeplus.claimbatch.claim.sql.ClaimOrderTicketSql;
import kr.co.homeplus.claimbatch.claim.sql.OrderDataSql;
import kr.co.homeplus.claimbatch.core.db.annotation.MasterConnection;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.mapping.StatementType;

@MasterConnection
public interface ClaimWriteMapper {

    @Options(useGeneratedKeys = true, keyProperty="claimNo")
    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimMst")
    int insertClaimMst(ClaimMstRegDto claimMstRegDto);

    @Options(useGeneratedKeys = true, keyProperty="claimBundleNo")
    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimBundle")
    int insertClaimBundle(ClaimBundleRegDto claimBundleRegDto);

    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimReq")
    int insertClaimReq(ClaimReqRegDto claimReqRegDto);

    @Options(useGeneratedKeys = true, keyProperty="claimItemNo")
    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimItemReq")
    int insertClaimItemReq(ClaimItemRegDto claimItemRegDto);

    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimPayment")
    int insertClaimPayment(ClaimPaymentRegDto claimPaymentRegDto);

    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimOpt")
    int insertClaimOpt(ClaimOptRegDto claimOptRegDto);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectPaymentCancelInfo")
    List<LinkedHashMap<String, Object>> selectPaymentCancelInfo(@Param("claimNo") long claimNo);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimStatus")
    int updateClaimStatus(LinkedHashMap<String, Object> parameter);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimPaymentStatus")
    int updateClaimPaymentStatus(@Param("claimPaymentNo") long claimPaymentNo, @Param("claimApprovalCd") String claimApprovalCd, ClaimCode claimCode, @Param("isLastYn")String isLastYn);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimManagementSql.class, method = "callByEndClaimFlag")
    LinkedHashMap<String, Object> callByClaimReg(ClaimInfoRegDto infoRegDto);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimManagementSql.class, method = "callByClaimAdditionReg")
    LinkedHashMap<String, Object> callByClaimAdditionReg(@Param("claimNo") long claimNo);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = ClaimManagementSql.class, method = "callByClaimItemEmpDiscount")
    void callByClaimItemEmpDiscount(@Param("claimNo")long claimNo, @Param("orderItemNo") long orderItemNo);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimCouponReIssueResult")
    int updateClaimCouponReIssueResult(List<Long> claimAdditionNo);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimBundleSlotResult")
    int updateClaimBundleSlotResult(@Param("claimBundleNo") long claimBundleNo);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimPaymentOcbTransactionId")
    int updateClaimPaymentOcbTransactionId(@Param("claimPaymentNo") long claimPaymentNo, @Param("pgMid") String pgMid);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimPaymentPgCancelTradeId")
    int updateClaimPaymentPgCancelTradeId(@Param("claimPaymentNo") long claimPaymentNo, @Param("cancelTradeId") String cancelTradeId);

    @Options(useGeneratedKeys = true, keyProperty="claimTicketNo")
    @InsertProvider(type = ClaimOrderTicketSql.class, method = "insertClaimTicket")
    int insertClaimTicket(ClaimTicketRegDto claimTicketRegDto);

    @UpdateProvider(type = ClaimOrderTicketSql.class, method = "updateOrderTicketStatus")
    int updateOrderTicketStatus(ClaimTicketModifyDto modifyDto);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateTotalClaimPaymentStatus")
    int updateTotalClaimPaymentStatus(@Param("claimNo") long claimNo, ClaimCode claimCode);

    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimAccumulate")
    int insertClaimAccumulate(LinkedHashMap<String, Object> parameterMap);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimAccumulateReqSend")
    int updateClaimAccumulateReqSend(@Param("claimNo") long claimNo, @Param("reqSendYn") String reqSendYn, @Param("pointKind") String pointKind);

    @UpdateProvider(type = ClaimManagementSql.class, method = "updateClaimAccumulate")
    int updateClaimAccumulate(LinkedHashMap<String, Object> parameterMap);

    @UpdateProvider(type = ClaimOrderCancelSql.class, method = "updateClaimMstPaymentAmt")
    int updateClaimMstPaymentAmt(@Param("claimNo") long claimNo, @Param("column") String column, @Param("amt") long amt);

    @UpdateProvider(type = ClaimOrderCancelSql.class, method = "updateClaimPaymentAmt")
    int updateClaimPaymentAmt(@Param("claimNo") long claimNo, @Param("paymentType") String paymentType, @Param("paymentAmt") long paymentAmt);

    @UpdateProvider(type = OrderDataSql.class, method = "updatePresentAutoCancel")
    int updatePresentAutoCancel(@Param("purchaseOrderNo") long purchaseOrderNo);

    @UpdateProvider(type = ClaimOrderCancelSql.class, method = "updateMileagePaybackResult")
    int updateMileagePaybackResult(@Param("paybackNo") long paybackNo);

    @UpdateProvider(type = ClaimMinusRefundSql.class, method = "updateMinusRefundCouponCorrect")
    int updateMinusRefundCouponCorrect(@Param("claimNo") long claimNo, @Param("correctCouponAmt") long correctCouponAmt, @Param("totDiscountAmt") long totDiscountAmt);

    @SelectProvider(type = ClaimManagementSql.class, method = "selectPaymentCancelManualInfo")
    LinkedHashMap<String, Object> selectPaymentCancelManualInfo(@Param("claimNo") long claimNo, @Param("paymentType") String paymentType);

    @Options(useGeneratedKeys = true, keyProperty="claimPickShippingNo")
    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimPickShipping")
    int insertClaimPickShipping(ClaimShippingRegDto shippingRegDto);

    @InsertProvider(type = ClaimManagementSql.class, method = "insertClaimExchShipping")
    int insertClaimExchShipping(ClaimExchShippingRegDto exchShippingRegDto);

    @SelectProvider(type = ClaimOrderCancelSql.class, method = "selectClaimSafetyIssueData")
    ClaimSafetySetDto selectClaimSafetyIssueData(@Param("claimBundleNo") long claimBundleNo, @Param("issueType") String issueType);

    @UpdateProvider(type = MarketClaimSql.class, method = "updateMarketClaimAmount")
    int updateMarketClaimAmount(@Param("claimNo") long claimNo, @Param("paymentAmt") long paymentAmt, @Param("claimDeliveryFeeDemandAmount") long claimDeliveryFeeDemandAmount, @Param("shipAmt") long shipAmt, @Param("promoDiscountAmt") long promoDiscountAmt);

    @SelectProvider(type = MarketClaimSql.class, method = "selectMarketClaimAmountInfoByModify")
    LinkedHashMap<String, Object> selectMarketClaimAmountInfoByModify(@Param("claimNo") long claimNo);

    @SelectProvider(type = MarketClaimSql.class, method = "selectOrderDiscountMarketSumPrice")
    LinkedHashMap<String, Object> selectOrderDiscountMarketSumPrice(@Param("marketOrderNo") String marketOrderNo);

    @SelectProvider(type = MarketClaimSql.class, method = "selectPrevMarketClaimDiscount")
    LinkedHashMap<String, Object> selectPrevMarketClaimDiscount(@Param("marketOrderNo") String marketOrderNo, @Param("claimNo") long claimNo);

    @UpdateProvider(type = MarketClaimSql.class, method = "updateMarketClaimBundleIsEnclose")
    int updateMarketClaimBundleIsEnclose(@Param("claimNo") long claimNo);

    @SelectProvider(type = MarketClaimSql.class, method = "selectMarketClaimAdditionCheckByAddShip")
    int selectMarketClaimAdditionCheckByAddShip(@Param("claimNo") long claimNo);

    @InsertProvider(type = MarketClaimSql.class, method = "insertClaimAdditionByMarketShipAmt")
    int insertClaimAdditionByMarketShipAmt(ClaimAdditionRegDto claimAdditionRegDto);

    @UpdateProvider(type = MarketClaimSql.class, method = "updateClaimAdditionByMarketShipAmt")
    int updateClaimAdditionByMarketShipAmt(@Param("claimNo") long claimNo, @Param("marketShipAmt") long marketShipAmt);

    @SelectProvider(type = MarketClaimSql.class, method = "selectClaimAdditionAddDataByMarketClaim")
    List<LinkedHashMap<String, Object>> selectClaimAdditionAddDataByMarketClaim(@Param("claimNo") long claimNo);

    @SelectProvider(type = MarketClaimSql.class, method = "selectPrevMarketClaimShipAmt")
    LinkedHashMap<String, Object> selectPrevMarketClaimShipAmt(@Param("purchaseOrderNo") long purchaseOrderNo);
}
