package kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MarketClaimItemDto {
    @ApiModelProperty(value= "마켓주문클레임상품순번")
    private long marketClaimItemNo;
    @ApiModelProperty(value= "마켓클레임번호")
    private long marketClaimNo;
    @ApiModelProperty(value= "마켓유형")
    private String marketType;
    @ApiModelProperty(value= "업체주문상품번호")
    private String marketOrderItemNo;
    @ApiModelProperty(value= "업체주문번호")
    private String marketOrderNo;
    @ApiModelProperty(value= "클레임타입")
    private String claimType;
    @ApiModelProperty(value= "클레임상태")
    private String claimStatus;
    @ApiModelProperty(value= "클레임사유")
    private String claimReason;
    @ApiModelProperty(value= "클레임상세사유")
    private String claimReasonDetail;
    @ApiModelProperty(value= "상품번호")
    private String itemNo;
    @ApiModelProperty(value= "상품명")
    private String itemNm;
    @ApiModelProperty(value= "상품옵션")
    private String itemOpt;
    @ApiModelProperty(value= "사은품")
    private String gift;
    @ApiModelProperty(value= "업체상품번호")
    private String marketItemNo;
    @ApiModelProperty(value= "상품수량")
    private long itemQty;
    @ApiModelProperty(value= "상품가격")
    private long itemPrice;
    @ApiModelProperty(value= "상품옵션가격")
    private long itemOptPrice;
    @ApiModelProperty(value= "상품할인금액")
    private long itemDiscountAmt;
    @ApiModelProperty(value= "배송메모")
    private String shipMemo;
    @ApiModelProperty(value= "출입방법내용")
    private String accesMethodCntnt;
    @ApiModelProperty(value= "픽업장소내용")
    private String pickupPlaceCntnt;
    @ApiModelProperty(value= "배송비금액")
    private long shipAmt;
    @ApiModelProperty(value= "결제금액")
    private long paymentAmt;
    @ApiModelProperty(value= "등록일시")
    private String regDt;
    @ApiModelProperty(value= "등록자ID")
    private String regId;
    @ApiModelProperty(value= "수정일시")
    private String chgDt;
    @ApiModelProperty(value= "수정자ID")
    private String chgId;
    @ApiModelProperty(value= "거래일시")
    private String tradeDt;
    @ApiModelProperty(value= "대체여부")
    private String substitutionYn;
    @ApiModelProperty(value= "원상품수량")
    private long orgItemQty;
    @ApiModelProperty(value= "이벤트쿠폰번호")
    private String eventCouponNo;
    @ApiModelProperty(value= "쿠폰ID")
    private String couponId;
    @ApiModelProperty(value= "최소사용건수")
    private long minUseCnt;
    @ApiModelProperty(value= "할인유형")
    private String discountType;
    @ApiModelProperty(value= "할인값")
    private long discountVal;
    @ApiModelProperty(value= "수신여부")
    private String rcvYn;
    @ApiModelProperty(value= "쿠폰할인금액")
    private long couponDiscountAmt;
    @ApiModelProperty(value= "프로모션번호")
    private String promoNo;
    @ApiModelProperty(value= "프로모션상세번호")
    private String promoDetailNo;
    @ApiModelProperty(value= "rpm프로모션상세번호")
    private long rpmPromoDetailNo;
    @ApiModelProperty(value= "프로모션유형(SIMP:simple, THRES:Threhold, MIX:MixMatch)")
    private String promoType;
    @ApiModelProperty(value= "프로모션종류(BASIC:기본할인, PICK:골라담기, TOGETHER:함께할인, INTERVAL:구간할인)")
    private String promoKind;
    @ApiModelProperty(value= "프로모션할인유형")
    private String promoDiscountType;
    @ApiModelProperty(value= "프로모션할인금액")
    private long promoDiscountAmt;
    @ApiModelProperty(value= "프로모션할인율")
    private String promoDiscountRate;
    @ApiModelProperty(value= "믹스매치유형(1:구매수량, 2:구매금액)")
    private String mixMatchType;
    @ApiModelProperty(value= "쿠폰가격")
    private long couponPrice;
    @ApiModelProperty(value= "할인가격")
    private long discountPrice;
    @ApiModelProperty(value= "판매가격")
    private long salePrice;
    @ApiModelProperty(value= "원플러스원")
    private long onePlusOne;
    @ApiModelProperty(value= "점포가격")
    private long storePrice;
    @ApiModelProperty(value= "커스텀코드1")
    private String customCode1;
    @ApiModelProperty(value= "커스텀코드2")
    private String customCode2;
    @ApiModelProperty(value= "마켓할인가격")
    private long marketDiscountPrice;
    @ApiModelProperty(value= "배송상태")
    private String shipStatus;
    @ApiModelProperty(value= "처리상태")
    private String processStatus;
    @ApiModelProperty(value= "처리실패메시지")
    private String processFailMsg;
    @ApiModelProperty(value= "점포ID")
    private String storeId;
    @ApiModelProperty(value= "배송번호")
    private String dlvNo;
    @ApiModelProperty(value= "출입방법코드")
    private String entryMethodCd;
    @ApiModelProperty(value= "출입방법내용")
    private String entryMethodCntnt;
    @ApiModelProperty(value= "마켓클레임번호")
    private String marketClaimId;
    @ApiModelProperty(value= "구매주문번호")
    private long claimNo;
}
