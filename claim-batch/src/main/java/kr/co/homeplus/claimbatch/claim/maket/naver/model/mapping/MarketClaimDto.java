package kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MarketClaimDto {
    @ApiModelProperty(value= "마켓제휴클레임순번")
    private long marketClaimNo;
    @ApiModelProperty(value= "마켓유형")
    private String marketType;
    @ApiModelProperty(value= "마켓주문번호")
    private String marketOrderNo;
    @ApiModelProperty(value= "점포ID")
    private String storeId;
    @ApiModelProperty(value= "주문자ID")
    private String buyerId;
    @ApiModelProperty(value= "주문자명")
    private String buyerNm;
    @ApiModelProperty(value= "주문자전화번호")
    private String buyerTelNo;
    @ApiModelProperty(value= "주문자휴대폰번호")
    private String buyerMobileNo;
    @ApiModelProperty(value= "주문자이메일")
    private String buyerEmail;
    @ApiModelProperty(value= "주문자우편번호")
    private String buyerZipcode;
    @ApiModelProperty(value= "주문자기본주소")
    private String buyerBaseAddr;
    @ApiModelProperty(value= "주문자상세주소")
    private String buyerDetailAddr;
    @ApiModelProperty(value= "받는사람명")
    private String receiverNm;
    @ApiModelProperty(value= "받는사람전화번호")
    private String receiverTelNo;
    @ApiModelProperty(value= "받는사람휴대폰번호")
    private String receiverMobileNo;
    @ApiModelProperty(value= "받는사람우편번호")
    private String receiverZipcode;
    @ApiModelProperty(value= "받는사람기본주소")
    private String receiverBaseAddr;
    @ApiModelProperty(value= "받는사람상세주소")
    private String receiverDetailAddr;
    @ApiModelProperty(value= "배송메모")
    private String shipMemo;
    @ApiModelProperty(value= "총상품금액")
    private long itemAmt;
    @ApiModelProperty(value= "총배송비금액")
    private long shipAmt;
    @ApiModelProperty(value= "총상품할인금액")
    private long itemDiscountAmt;
    @ApiModelProperty(value= "총배송비할인금액")
    private long shipDiscountAmt;
    @ApiModelProperty(value= "총프로모션할인금액")
    private long promoDiscountAmt;
    @ApiModelProperty(value= "총쿠폰할인금액")
    private long couponDiscountAmt;
    @ApiModelProperty(value= "할인금액")
    private long discountAmt;
    @ApiModelProperty(value= "거래합계금액")
    private long tradeTotAmt;
    @ApiModelProperty(value= "거래금액")
    private long tradeAmt;
    @ApiModelProperty(value= "받은금액")
    private long tradeRcvAmt;
    @ApiModelProperty(value= "거래건수")
    private long tradeCnt;
    @ApiModelProperty(value= "결제일시")
    private String paymentDt;
    @ApiModelProperty(value= "결제수단")
    private String paymentMethod;
    @ApiModelProperty(value= "등록일시")
    private String regDt;
    @ApiModelProperty(value= "등록자ID")
    private String regId;
    @ApiModelProperty(value= "수정일시")
    private String chgDt;
    @ApiModelProperty(value= "수정자ID")
    private String chgId;
    @ApiModelProperty(value= "거래일시")
    private String tradeDt;
    @ApiModelProperty(value= "수신여부")
    private String rcvYn;
    @ApiModelProperty(value= "패키지번호")
    private String pkgNo;
    @ApiModelProperty(value= "배송요청일시")
    private String shipReqDt;
    @ApiModelProperty(value= "배송요청슬롯시작시간")
    private String shipReqSlotStartTime;
    @ApiModelProperty(value= "배송요청슬롯종료시간")
    private String shipReqSlotEndTime;
    @ApiModelProperty(value= "슬롯ID")
    private String slotId;
    @ApiModelProperty(value= "SHIFTID")
    private String shiftId;
    @ApiModelProperty(value= "슬롯원복여부")
    private String slotRestrYn;
    @ApiModelProperty(value= "출입방법코드")
    private String entryMethodCd;
    @ApiModelProperty(value= "출입방법내용")
    private String entryMethodCntnt;
}
