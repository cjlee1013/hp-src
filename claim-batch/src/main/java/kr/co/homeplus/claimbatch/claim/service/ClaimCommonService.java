package kr.co.homeplus.claimbatch.claim.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.claimbatch.claim.enums.ClaimCode;
import kr.co.homeplus.claimbatch.claim.enums.ExchangeShipType;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claimbatch.claim.model.payment.ClaimCouponReIssueSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.ClaimCouponReIssueSetDto.CouponReIssueDto;
import kr.co.homeplus.claimbatch.claim.model.payment.RestoreSlotStockDto;
import kr.co.homeplus.claimbatch.claim.model.payment.dgv.ClaimDgvCancelSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.dgv.ClaimDgvReceiptResponseDto;
import kr.co.homeplus.claimbatch.claim.model.payment.emp.EmpInfoDto;
import kr.co.homeplus.claimbatch.claim.model.payment.mhc.ClaimMhcCancelSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.mhc.MhcCardInfoDto;
import kr.co.homeplus.claimbatch.claim.model.payment.mhc.SaveCancelMhcPartSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.mhc.SaveCancelMhcSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.mileage.ClaimMileageCancelSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.ocb.ClaimOcbCancelSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.pg.ClaimPgCancelSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.ticket.ClaimCoopTicketCheckGetDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimAdditionRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimBundleRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimExchShippingRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimItemRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimPaymentRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimRegDetailDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimReqRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimShippingRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ticket.ClaimTicketRegDto;
import kr.co.homeplus.claimbatch.claim.model.shipping.ClaimShipCompleteDto;
import kr.co.homeplus.claimbatch.enums.ExternalUrlInfo;
import kr.co.homeplus.claimbatch.util.ExternalUtil;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import kr.co.homeplus.claimbatch.util.SetParameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimCommonService {

    private final ClaimMapperService mapperService;

    /**
     * 클레임 데이터 맵 생성.
     *
     * @param regSetDto 클레임 등록 데이터
     * @return 클레임 데이터 맵
     */
    public LinkedHashMap<String, Object> createClaimDataMap(ClaimRegSetDto regSetDto) {
        // 기존 orderItemNo -> bundleNo 로 변경. (2020.05.21)
        LinkedHashMap<String, Object> createClaimData = mapperService.getCreateClaimBundleInfo(Long.parseLong(regSetDto.getPurchaseOrderNo()), Long.parseLong(regSetDto.getBundleNo()));
        // Claim 타입 설정.
        createClaimData.put("claim_type", regSetDto.getClaimType());
        createClaimData.put("claim_reason_type", regSetDto.getClaimReasonType());
        createClaimData.put("claim_reason_detail", regSetDto.getClaimReasonDetail());
        createClaimData.put("request_id", regSetDto.getRegId());
        createClaimData.put("claim_bundle_qty", regSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getClaimQty).mapToInt(Integer::parseInt).sum());

        return createClaimData;
    }

    /**
     * 환불 예정금액 조회
     *
     * @param regSetDto 등록 DTO
     * @return ClaimPreRefundCalcSetDto 환불예정금액 조회 데이터.
     */
    public ClaimPreRefundCalcSetDto convertCalcSetDto(ClaimRegSetDto regSetDto){
        return ClaimPreRefundCalcSetDto.builder()
            .claimItemList(regSetDto.getClaimItemList())
            .purchaseOrderNo(regSetDto.getPurchaseOrderNo())
            .cancelType(regSetDto.getCancelType())
            .whoReason(regSetDto.getWhoReason())
            .claimPartYn(regSetDto.getClaimPartYn())
            .piDeductPromoYn(regSetDto.getPiDeductPromoYn())
            .piDeductDiscountYn(regSetDto.getPiDeductDiscountYn())
            .piDeductShipYn(regSetDto.getPiDeductShipYn())
            .encloseYn("N")
            .build();
    }

    /**
     * 취소요청 파라미터 생성.
     *
     * @param map 취소기본정보.
     * @return 취소요청파라미터
     */
    public ClaimRegSetDto createClaimRegSetDto(LinkedHashMap<String, Object> map) throws Exception {
        long purchaseOrderNo = ObjectUtils.toLong(map.get("purchase_order_no"));
        long bundleNo = ObjectUtils.toLong(map.get("bundle_no"));
        List<ClaimPreRefundItemList> itemInfo = mapperService.getClaimPreRefundItemInfo(purchaseOrderNo, bundleNo);
        log.info("itemInfo :: {}", itemInfo);
        if(itemInfo == null || itemInfo.size() == 0){
            log.error("거래번호({})는 이미 취소된 거래입니다.", purchaseOrderNo);
            throw new Exception("이미 취소된 거래입니다.");
        }
        return ClaimRegSetDto.builder()
            .purchaseOrderNo(String.valueOf(purchaseOrderNo))
            .bundleNo(String.valueOf(bundleNo))
            .claimItemList(mapperService.getClaimPreRefundItemInfo(purchaseOrderNo, bundleNo))
            .whoReason("B")
            .cancelType("O")
            .claimType("C")
            .claimReasonType("C004")
            .claimReasonDetail("선물하기 배송지 미입력으로 자동취소")
            .regId("BATCH")
            .claimPartYn(ObjectUtils.toString(map.get("claim_part_yn")))
            .orderCancelYn("Y")
            .piDeductPromoYn("N")
            .piDeductDiscountYn("N")
            .piDeductShipYn("N")
            .build();
    }

    /**
     * 클레임 마스터 등록을 위한 DTO 생성.
     *
     * @param calcSetDto 환불예정금액 조회 데이터
     * @return ClaimMstRegDto 클레임 마스터 등록 DTO 정보
     * @throws Exception 오류 처리.
     */
    public ClaimMstRegDto getFusionForClaimMst(ClaimPreRefundCalcSetDto calcSetDto) throws Exception {
        // 클레임 마스터 등록을 위한 purchaseOrder 테이블 조회.
        LinkedHashMap<String, Object> createClaimMstData = mapperService.getCreateClaimMstInfo(Long.parseLong(calcSetDto.getPurchaseOrderNo()));

        // 클레임 환불 예정 금액 조회
        ClaimPreRefundCalcGetDto calcGetDto = new ClaimPreRefundCalcGetDto();
        // 마켓여부 확인
        if(StringUtils.isEmpty(ObjectUtils.toString(createClaimMstData.get("market_type")))) {
            calcGetDto = mapperService.getClaimPreRefundCalculation(calcSetDto);
        } else {
            calcGetDto = mapperService.getClaimPreRefundCalculationByMarket(calcSetDto);
        }
        log.debug("환불예정금액 ::: {}", calcGetDto);
        if(calcGetDto.getReturnValue() != 0){
            throw new Exception("환불예정금액 조회 실패.");
        }

        // 클레임 마스터 등록을 위한 DTO 생성.
        ClaimMstRegDto claimMstRegDto = ClaimMstRegDto.builder()
            .paymentNo(Long.parseLong(createClaimMstData.get("payment_no").toString()))
            .purchaseOrderNo(Long.parseLong(createClaimMstData.get("purchase_order_no").toString()))
            .siteType(createClaimMstData.get("site_type").toString())
            .userNo(createClaimMstData.get("user_no").toString())
            .claimPartYn(calcSetDto.getClaimPartYn())
            .empNo(ObjectUtils.toString(createClaimMstData.get("emp_no")))
            .empRemainAmt(0)
            .oosYn("N")
            .gradeSeq(ObjectUtils.toString(createClaimMstData.get("grade_seq")))
            .marketType(ObjectUtils.toString(createClaimMstData.get("market_type")))
            .marketOrderNo(ObjectUtils.toString(createClaimMstData.get("market_order_no")))
            .build();

        // 데이터 넣기
        claimMstRegDto.putMap(convertClaimRegInfo(claimMstRegDto, calcGetDto));
        // 귀책사유
        claimMstRegDto.setWhoReason(calcSetDto.getWhoReason());

        return claimMstRegDto;
    }

    /**
     * 환불예정금조회결과 -> 등록데이터 생성
     *
     * @param regDto 등록DTO
     * @param calcGetDto 환불예정금액결과 DTO
     * @return 등록데이터 맵.
     * @throws Exception 오류시 오류처리.
     */
    public LinkedHashMap<String, Object> convertClaimRegInfo(ClaimMstRegDto regDto, ClaimPreRefundCalcGetDto calcGetDto) throws Exception {
        LinkedHashMap<String, Object> claimRegInfo = ObjectUtils.getConvertMap(regDto);
        LinkedHashMap<String, Object> refundInfo = ObjectUtils.getConvertMap(calcGetDto);
        // 맵에 데이터가 없으면 오류
        if(refundInfo.isEmpty() || claimRegInfo.isEmpty()){
            throw new Exception("클레임 등록을 위한 데이터 조회에 실패하였습니다.");
        }
        // 키 찾아서 맵핑.
        claimRegInfo.forEach((key, value) -> {
            String getkey = key;
            if(getkey.equals("completeAmt")){
                value = refundInfo.get("refundAmt");
            } else {
                if(getkey.startsWith("tot")){
                    getkey = getkey.replace("tot", "");
                }
                if(refundInfo.get(StringUtils.uncapitalize(getkey)) != null){
                    value = refundInfo.getOrDefault(StringUtils.uncapitalize(getkey), 0);
                }
            }
            claimRegInfo.replace(key, value);
        });
        return claimRegInfo;
    }


    /**
     * 클레임 번들 등록 DTO 생성
     *
     * @param claimData 클레임 생성 데이터 맵
     * @return ClaimBundleRegDto 클레임 번들 등록 DTO
     */
    public ClaimBundleRegDto createClaimBundleRegDto(HashMap<String, Object> claimData) {
        return ClaimBundleRegDto.builder()
            .purchaseOrderNo(Long.parseLong(claimData.get("purchase_order_no").toString()))
            .paymentNo(Long.parseLong(claimData.get("payment_no").toString()))
            .userNo(Long.parseLong(claimData.get("user_no").toString()))
            .bundleNo(Long.parseLong(claimData.get("bundle_no").toString()))
            .partnerId(claimData.get("partner_id").toString())
            .claimType(claimData.get("claim_type").toString())
            .claimStatus("C1")
            .claimNo(Long.parseLong(claimData.get("claim_no").toString()))
            .originClaimBundleNo(0)
            .claimBundleQty(Long.parseLong(claimData.get("claim_bundle_qty").toString()))
            .claimShipFeeBank("N")
            .claimShipFeeEnclose(ObjectUtils.toString(claimData.getOrDefault("isEnclose", "N")))
            .warehouse("N")
            .isNoDiscountDeduct("N")
            .isNoShipDeduct("N")
            .reserveYn(ObjectUtils.toString(claimData.get("reserve_yn")))
            .orderCategory(ObjectUtils.toString(claimData.get("order_category")))
            .closeSendYn(ObjectUtils.toString(claimData.get("close_send_yn")))
            .shipMobileNo(ObjectUtils.toString(claimData.get("ship_mobile_no")))
            .marketOrderNo(ObjectUtils.toString(claimData.get("market_order_no")))
            .build();
    }

    /**
     * 클레임 요청 등록 DTO 생성
     *
     * @param claimData 클레임 생성 데이터 맵
     * @return ClaimReqRegDto 클레임 요청 등록
     */
    public ClaimReqRegDto createClaimReqRegDto(HashMap<String, Object> claimData) {
        return ClaimReqRegDto.builder()
            .claimNo(Long.parseLong(claimData.get("claim_no").toString()))
            .claimBundleNo(Long.parseLong(claimData.get("claim_bundle_no").toString()))
            .claimReasonType(claimData.get("claim_reason_type").toString())
            .claimReasonDetail(ObjectUtils.toString(claimData.get("claim_reason_detail")))
            .requestId(claimData.get("request_id").toString())
            .requestDt("NOW()")
            .build();
    }

    /**
     * 클레임 아이템 등록 DTO 생성
     *
     * @param claimData 클레임 생성 데이터 맵
     * @return ClaimItemRegDto 클레임 아이템 등록 DTO
     */
    public ClaimItemRegDto createClaimItemRegDto(LinkedHashMap<String, Object> claimData) {
        return ClaimItemRegDto.builder()
            .claimNo(ObjectUtils.toLong(claimData.get("claim_no")))
            .claimBundleNo(ObjectUtils.toLong(claimData.get("claim_bundle_no")))
            .paymentNo(ObjectUtils.toLong(claimData.get("payment_no")))
            .purchaseOrderNo(ObjectUtils.toLong(claimData.get("purchase_order_no")))
            .bundleNo(ObjectUtils.toLong(claimData.get("bundle_no")))
            .originStoreId(ObjectUtils.toString(claimData.get("origin_store_id")))
            .storeId(ObjectUtils.toString(claimData.get("store_id")))
            .fcStoreId(ObjectUtils.toString(claimData.get("fc_store_id")))
            .orderItemNo(ObjectUtils.toLong(claimData.get("order_item_no")))
            .itemNo(ObjectUtils.toString(claimData.get("item_no")))
            .itemName(ObjectUtils.toString(claimData.get("item_name")))
            .itemPrice(ObjectUtils.toLong(claimData.get("item_price")))
            .claimItemQty(0)
            .storeType(ObjectUtils.toString(claimData.get("store_type")))
            .mallType(ObjectUtils.toString(claimData.get("mall_type")))
            .taxYn(ObjectUtils.toString(claimData.get("tax_yn")))
            .partnerId(ObjectUtils.toString(claimData.get("partner_id")))
            .empDiscountRate(0)
            .empDiscountAmt(0)
            .regId(ObjectUtils.toString(claimData.get("request_id")))
            .marketOrderNo(ObjectUtils.toString(claimData.get("market_order_no")))
            .build();
    }

    /**
     * 클레임 - 클레임 결제 수단 등록 로직
     *
     * @param claimData 클레임 데이터
     * @param paymentType 결제 타입
     * @param paymentAmt 취소금액
     * @return 클레임 결제 수단 등록 DTO
     */
    public ClaimPaymentRegDto createClaimPaymentRegDto(LinkedHashMap<String, Object> claimData, String paymentType, long paymentAmt) throws Exception {
        // 결제 번호
        long paymentNo = Long.parseLong(claimData.get("payment_no").toString());

        // 원주문 결제 정보 가지고 오기
        LinkedHashMap<String, Object> originPaymentInfo = mapperService.getOriPaymentInfoForClaim(paymentNo, paymentType);

        if(originPaymentInfo == null){
            throw new Exception("취소 가능한 결제 건이 없습니다.");
        }

        return ClaimPaymentRegDto.builder()
            .claimNo(Long.parseLong(claimData.get("claim_no").toString()))
            .paymentNo(Long.parseLong(claimData.get("payment_no").toString()))
            .purchaseOrderNo(Long.parseLong(claimData.get("purchase_order_no").toString()))
            .pgKind(ObjectUtils.toString(originPaymentInfo.get("pg_kind")))
            .paymentType(this.getClaimPaymentTypeCode(paymentType))
            .parentPaymentMethod(ObjectUtils.toString(originPaymentInfo.get("parent_payment_method")))
            .methodCd(ObjectUtils.toString(originPaymentInfo.get("method_cd")))
            .easyMethodCd(ObjectUtils.toString(originPaymentInfo.get("easy_method_cd")))
            .pgMid(ObjectUtils.toString(originPaymentInfo.get("pg_mid")))
            .pgOid(ObjectUtils.toString(originPaymentInfo.get("pg_oid")))
            .paymentTradeId(ObjectUtils.toString(originPaymentInfo.get("payment_trade_id")))
            .originApprovalCd(ObjectUtils.toString(originPaymentInfo.get("origin_approval_cd")))
            .paymentStatus(ClaimCode.CLAIM_PAYMENT_STATUS_P0.getCode())
            .paymentAmt(paymentAmt)
            .payToken(ObjectUtils.toString(originPaymentInfo.get("pay_token")))
            .substitutionAmt(0)
            .pgCancelRequestCnt(0)
            .regId(claimData.get("request_id").toString())
            .chgId(claimData.get("request_id").toString())
            .build();
    }

    /**
     * 클레임 결제 수단을 등록하기 위한 로직
     * 클레임 발생시 취소금액을 처리하기 위하여
     * 환불예정금액에서 계산된 결제 수단에 따라 분리.
     *
     * @param claimMstRegDto 클레임 마스터정보
     * @return LinkedHashMap<String, Long> 클레임 결제수단 등록 결제타입 정보
     */
    public LinkedHashMap<String, Long> getClaimPaymentTypeList(ClaimMstRegDto claimMstRegDto){

        LinkedHashMap<String, Long> paymentTypeMap = new LinkedHashMap<>();

        if(claimMstRegDto.getPgAmt() != 0){
            paymentTypeMap.put("PG", claimMstRegDto.getPgAmt());
        }
        if(claimMstRegDto.getMhcAmt() != 0){
            paymentTypeMap.put("MHC", claimMstRegDto.getMhcAmt());
        }
        if(claimMstRegDto.getOcbAmt() != 0){
            paymentTypeMap.put("OCB", claimMstRegDto.getOcbAmt());
        }
        if(claimMstRegDto.getMileageAmt() != 0){
            paymentTypeMap.put("MILEAGE", claimMstRegDto.getMileageAmt());
        }
        if(claimMstRegDto.getDgvAmt() != 0){
            paymentTypeMap.put("DGV", claimMstRegDto.getDgvAmt());
        }
        if(paymentTypeMap.size() == 0){
            return null;
        }
        return paymentTypeMap;
    }

    public String getClaimPaymentTypeCode(String type){
        return Stream.of(ClaimCode.values())
            .filter(e -> e.name().contains("CLAIM_CONVERT_PAYMENT_TYPE"))
            .collect(Collectors.toMap(ClaimCode::name, ClaimCode::getCode))
            .get("CLAIM_CONVERT_PAYMENT_TYPE_".concat(type.toUpperCase(Locale.KOREA)));
    }

    // ====================================================================================
    // payment cancel dto
    // ====================================================================================

    public ClaimPgCancelSetDto createPaymentPgCancelSetDto(LinkedHashMap<String, Object> paymentData) {

        int orderPaymentAmt = ObjectUtils.toInt(paymentData.get("order_payment_amt"));
        int pgCancelAmt = ObjectUtils.toInt(paymentData.get("payment_amt")) + ObjectUtils.toInt(paymentData.get("prev_payment_amt"));
        int partRemainPrice = orderPaymentAmt - pgCancelAmt;

        if(orderPaymentAmt < pgCancelAmt){
            partRemainPrice = 0;
        }

        return ClaimPgCancelSetDto.builder()
            .pgKind(ObjectUtils.toString(paymentData.get("pg_kind")))
            .pgOid(ObjectUtils.toString(paymentData.get("pg_oid")))
            .pgMid(ObjectUtils.toString(paymentData.get("pg_mid")))
            .pgTid(ObjectUtils.toString(paymentData.get("payment_trade_id")))
            .userNo(ObjectUtils.toLong(paymentData.get("user_no")))
            .parentMethodCd(ObjectUtils.toString(paymentData.get("parent_payment_method")))
            .cancelReason(paymentData.getOrDefault("cancel_message", "").toString())
            .clientIp(paymentData.getOrDefault("client_ip", "0.0.0.0").toString())
            .partCancelPrice(ObjectUtils.toLong(paymentData.getOrDefault("payment_amt", null)))
            .partRemainPrice(partRemainPrice)
            .cancelType(isTotalCancel(paymentData) ? "ALL" : "PART")
            .payToken(ObjectUtils.toString(paymentData.get("pay_token")))
            .build();
    }

    /**
     * MHC 포인트 취소 로직.
     * @param paymentData pg_kind, payment_type, pg_mid, pg_oid, payment_trade_id, origin_approval_cd, payment_amt
     *                    pg_cancel_request_cnt, parent_payment_method, purchase_order_no
     *                    claim_payment_no, order_payment_amt, prev_payment_amt
     * @return ClaimMhcCancelSetDto MHC취소 요청 DTO
     * @throws Exception 오류시 오류처리.
     */
    public ClaimMhcCancelSetDto createPaymentCancelForMhc(LinkedHashMap<String, Object> paymentData) throws Exception {
        // externalService 로 user-api 조회
        MhcCardInfoDto mhcCardInfo = getUserMhcCardInfo(ObjectUtils.toLong(paymentData.get("user_no")));

        ClaimMhcCancelSetDto claimMhcCancelSetDto = ClaimMhcCancelSetDto.builder()
            .userNo(ObjectUtils.toLong(paymentData.get("user_no")))
            .siteType(ObjectUtils.toString(paymentData.get("site_type")))
            .saleDt(new SimpleDateFormat("yyyyMMdd").format(new Date().getTime()))
            .mhcCardNoEnc(mhcCardInfo.getMhcCardnoEnc())
            .mhcCardNoHmac(mhcCardInfo.getMhcCardnoHmac())
            .claimNo(ObjectUtils.toLong(paymentData.get("claim_no")))
            .cancelAmt(ObjectUtils.toLong(paymentData.get("payment_amt")))
            .orgMhcAprDate(ObjectUtils.toString(paymentData.get("payment_trade_id")))
            .orgMhcAprNumber(ObjectUtils.toString(paymentData.get("origin_approval_cd")))
            .partCancelYn(isTotalCancel(paymentData) ? "N" : "Y")
            .build();
        log.debug("createPaymentCancelForMhc :: {} // isTotalCancel :: {}", claimMhcCancelSetDto, isTotalCancel(paymentData));
        return claimMhcCancelSetDto;
    }

    public ClaimOcbCancelSetDto createPaymentCancelForOcb(LinkedHashMap<String, Object> paymentData) throws Exception {
        List<String> mctTransactionIdList = new ArrayList<>();
        mctTransactionIdList.add("CANCEL");
        mctTransactionIdList.add(ObjectUtils.toString(paymentData.get("pg_mid")).split("-")[1]);
        mctTransactionIdList.add(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));

        mapperService.modifyClaimPaymentOcbTransactionId(ObjectUtils.toLong(paymentData.get("claim_payment_no")), String.join("-", mctTransactionIdList));

        return ClaimOcbCancelSetDto.builder()
            .cancelOcbPoint(ObjectUtils.toLong(paymentData.get("payment_amt")))
            .mctTransactionId(String.join("-", mctTransactionIdList))
            .orgOcbTransactionId(ObjectUtils.toString(paymentData.get("payment_trade_id")))
            .partCancelYn(isTotalCancel(paymentData) ? "N" : "Y")
            .claimNo(ObjectUtils.toLong(paymentData.get("claim_no")))
            .userNo(ObjectUtils.toString(paymentData.get("user_no")))
            .build();
    }

    public ClaimMileageCancelSetDto createPaymentCancelForMileageDto(LinkedHashMap<String, Object> paymentData){
        return ClaimMileageCancelSetDto.builder()
            .userNo(ObjectUtils.toString(paymentData.get("payment_trade_id")))
            .tradeNo(Long.parseLong(ObjectUtils.toString(paymentData.get("purchase_order_no"))))
            .cancelMessage(paymentData.getOrDefault("cancel_message", "").toString())
            .regId(ObjectUtils.toString(paymentData.get("reg_id")))
            .requestCancelAmt(ObjectUtils.toString(paymentData.get("payment_amt")))
            .build();
    }

    public ClaimDgvCancelSetDto createPaymentCancelForDgv(LinkedHashMap<String, Object> paymentData) throws Exception {
        ClaimDgvReceiptResponseDto responseDto = ExternalUtil.getTransfer(
            ExternalUrlInfo.PAYMENT_CANCEL_DGV_RECEIPT,
            new ArrayList<SetParameter>(){{
                add(SetParameter.create("siteType", ObjectUtils.toString(paymentData.get("site_type"))));
                add(SetParameter.create("tradeDt", new SimpleDateFormat("yyyyMMdd").format(new Date().getTime())));
            }},
            ClaimDgvReceiptResponseDto.class
        );
        return ClaimDgvCancelSetDto.builder()
            .dgvCardNo(ObjectUtils.toString(paymentData.get("pg_mid")))
            .posNo(responseDto.getPosNo())
            .tradeNo(responseDto.getTradeNo())
            .saleDt(new SimpleDateFormat("yyyyMMdd").format(new Date().getTime()))
            .partCancelYn(isTotalCancel(paymentData) ? "N" : "Y")
            .tradeAmount(ObjectUtils.toLong(paymentData.get("payment_amt")))
            .claimNo(ObjectUtils.toLong(paymentData.get("claim_no")))
            .orgAprNumber(ObjectUtils.toString(paymentData.get("origin_approval_cd")))
            .orgAprDate(ObjectUtils.toString(paymentData.get("pay_token")))
            .orgPosNo(ObjectUtils.toString(paymentData.get("pg_oid")))
            .orgTradeNo(ObjectUtils.toString(paymentData.get("payment_trade_id")))
            .userNo(ObjectUtils.toString(paymentData.get("user_no")))
            .build();
    }

    public RestoreSlotStockDto createRestoreStockSlotDto(LinkedHashMap<String, Object> claimData) {
        return RestoreSlotStockDto.builder()
            .purchaseOrderNo(ObjectUtils.toLong(claimData.get("purchase_order_no")))
            .bundleNo(ObjectUtils.toLong(claimData.get("bundle_no")))
            .claimNo(ObjectUtils.toLong(claimData.get("claim_no")))
            .userNo(ObjectUtils.toLong(claimData.get("user_no")))
            .build();
    }

    public ClaimCouponReIssueSetDto createClaimCouponReIssueDto(List<CouponReIssueDto> issueList, long userNo, long purchaseOrderNo) {
        return ClaimCouponReIssueSetDto.builder()
            .couponReIssueList(issueList)
            .userNo(userNo)
            .purchaseOrderNo(purchaseOrderNo)
            .build();
    }

    public ClaimTicketRegDto createClaimTicketDto(LinkedHashMap<String, Object> ticketInfo) {
        return ClaimTicketRegDto.builder()
            .claimNo(ObjectUtils.toLong(ticketInfo.get("claim_no")))
            .claimBundleNo(ObjectUtils.toLong(ticketInfo.get("claim_bundle_no")))
            .claimItemNo(ObjectUtils.toLong(ticketInfo.get("claim_item_no")))
            .orderItemNo(ObjectUtils.toLong(ticketInfo.get("order_item_no")))
            .orderTicketNo(ObjectUtils.toLong(ticketInfo.get("order_ticket_no")))
            .regDt("NOW()")
            .chgDt("NOW()")
            .build();
    }

    public SaveCancelMhcSetDto createSaveCancelMhcDto(LinkedHashMap<String, Object> pointInfo) {
        return SaveCancelMhcSetDto.builder()
            .userNo(ObjectUtils.toString(pointInfo.get("user_no")))
            .saleDt(ObjectUtils.toString(pointInfo.get("claim_dt")))
            .mhcCardNoEnc(ObjectUtils.toString(pointInfo.get("mhc_card_no_enc")))
            .mhcCardNoHmac(ObjectUtils.toString(pointInfo.get("mhc_card_no_hmac")))
            .claimNo(ObjectUtils.toString(pointInfo.get("claim_no")))
            .orgPurchaseOrderNo(ObjectUtils.toString(pointInfo.get("purchase_order_no")))
            .build();
    }

    public SaveCancelMhcPartSetDto createSaveCancelMhcPartDto(LinkedHashMap<String, Object> pointInfo) {
        return SaveCancelMhcPartSetDto.builder()
            .userNo(ObjectUtils.toString(pointInfo.get("user_no")))
            .saleDt(ObjectUtils.toString(pointInfo.get("claim_dt")))
            .mhcCardNoEnc(ObjectUtils.toString(pointInfo.get("mhc_card_no_enc")))
            .mhcCardNoHmac(ObjectUtils.toString(pointInfo.get("mhc_card_no_hmac")))
            .claimNo(ObjectUtils.toString(pointInfo.get("claim_no")))
            .orgPurchaseOrderNo(ObjectUtils.toString(pointInfo.get("purchase_order_no")))
            .mhcBaseAccAmt(ObjectUtils.toInt(pointInfo.get("claim_base_acc_amt")))
            .mhcAddAccAmt(ObjectUtils.toInt(pointInfo.get("claim_add_acc_amt")))
            .partCancelAmt(ObjectUtils.toInt(pointInfo.get("claim_acc_target_amt")))
            .build();
    }

    public ClaimShippingRegDto createClaimPickShippingDto(ClaimRegDetailDto regDetailDto, LinkedHashMap<String, Object> claimData) {
        LinkedHashMap<String, Object> pickInfo = mapperService.getClaimPickIsAutoInfo(ObjectUtils.toLong(claimData.get("purchase_order_no")), ObjectUtils.toLong(claimData.get("bundle_no")));

        String shipType = "TD,AURORA,PICK,SUM".contains(ObjectUtils.toString(pickInfo.get("ship_type")))? "TD" : ObjectUtils.toString(pickInfo.get("ship_type"));
        String shipMethod = ObjectUtils.toString(pickInfo.get("ship_method"));

        // 기본 데이터 생성.(필수데이터)
        ClaimShippingRegDto shippingRegDto = ClaimShippingRegDto.builder()
            .claimBundleNo(ObjectUtils.toLong(claimData.get("claim_bundle_no")))
            .isPick(regDetailDto.getIsPick())
            .userNo(ObjectUtils.toLong(claimData.get("user_no")))
            .pickTrackingYn("N")
            .partnerId(ObjectUtils.toString(claimData.get("partner_id")))
            .orgInvoiceNo(ObjectUtils.toString(claimData.get("invoice_no")))
            .pickReceiverNm(regDetailDto.getPickReceiverNm())
            .pickZipcode(regDetailDto.getPickZipCode())
            .pickBaseAddr(regDetailDto.getPickBaseAddr())
            .pickDetailAddr(regDetailDto.getPickBaseDetailAddr())
            .pickRoadBaseAddr(regDetailDto.getPickRoadBaseAddr())
            .pickRoadDetailAddr(regDetailDto.getPickRoadDetailAddr())
            .pickMobileNo(regDetailDto.getPickMobileNo())
            .regId(ObjectUtils.toString(claimData.get("request_id")))
            .chgId(ObjectUtils.toString(claimData.get("request_id")))
            .isAuto(ObjectUtils.toString(pickInfo.get("return_delivery_yn")))
            .isPickProxy(ObjectUtils.toString(pickInfo.get("return_delivery_yn")))
            .pickStatus(ObjectUtils.toString(pickInfo.get("return_delivery_yn")).equalsIgnoreCase("Y") ? "NN" : "N0")
            .safetyUseYn(ObjectUtils.toString(claimData.get("safety_use_yn")))
            .shipMethod(ObjectUtils.toString(pickInfo.get("ship_method")))
            .build();

        if(shipType.equalsIgnoreCase("TD")){
            shippingRegDto.setPickShipType("D");
            shippingRegDto.setShipDt(regDetailDto.getShipDt());
            shippingRegDto.setShiftId(regDetailDto.getShiftId());
            shippingRegDto.setSlotId(regDetailDto.getSlotId());
        } else {
            // DS
            if(regDetailDto.getIsPick().equalsIgnoreCase("Y")) {
                shippingRegDto.setIsAuto("N");
                shippingRegDto.setPickDlvCd(regDetailDto.getPickDlvCd());
                shippingRegDto.setPickShipType("C");
                shippingRegDto.setPickInvoiceNo(regDetailDto.getPickInvoiceNo());
                shippingRegDto.setPickInvoiceRegId(ObjectUtils.toString(claimData.get("user_no")));
                shippingRegDto.setPickStatus("P1");
            } else {
                shippingRegDto.setPickDlvCd(ObjectUtils.toString(pickInfo.get("dlvCd")));
                shippingRegDto.setPickShipType(shipMethod.contains("DRCT") ? "D" : shipMethod.contains("POST") ? "P" : "C");
            }
        }

        // 수거안함
        if(regDetailDto.getIsPickReq().equalsIgnoreCase("N")) {
            shippingRegDto.setPickShipType("N");
            shippingRegDto.setPickStatus("P3");
        }
        return shippingRegDto;
    }

    public ClaimExchShippingRegDto createClaimExchShippingRegDto(ClaimShippingRegDto shippingRegDto, ClaimRegDetailDto regDetailDto) {
        return ClaimExchShippingRegDto.builder()
            .claimPickShippingNo(shippingRegDto.getClaimPickShippingNo())
            .claimBundleNo(shippingRegDto.getClaimBundleNo())
            .exchShipType(getExchPickType(shippingRegDto))
            .exchStatus("D0")
            .exchTrackingYn("N")
            .exchReceiverNm(regDetailDto.getExchReceiverNm())
            .exchZipcode(regDetailDto.getExchZipCode())
            .exchRoadBaseAddr(regDetailDto.getExchRoadBaseAddr())
            .exchRoadDetailAddr(regDetailDto.getExchRoadDetailAddr())
            .exchBaseAddr(regDetailDto.getExchBaseAddr())
            .exchDetailAddr(regDetailDto.getExchBaseDetailAddr())
            .exchMobileNo(regDetailDto.getExchMobileNo())
            .userNo(shippingRegDto.getUserNo())
            .partnerId(shippingRegDto.getPartnerId())
            .regId(shippingRegDto.getRegId())
            .chgId(shippingRegDto.getChgId())
            .safetyUseYn(shippingRegDto.getSafetyUseYn())
            .build();
    }

    public ClaimAdditionRegDto createClaimAdditionRegDtoByMarketShipPrice(LinkedHashMap<String, Object> parameterMap) {
        return ClaimAdditionRegDto.builder()
            .claimNo(ObjectUtils.toLong(parameterMap.get("claim_no")))
            .claimBundleNo(ObjectUtils.toLong(parameterMap.get("claim_bundle_no")))
            .purchaseOrderNo(ObjectUtils.toLong(parameterMap.get("purchase_order_no")))
            .bundleNo(ObjectUtils.toLong(parameterMap.get("bundle_no")))
            .userNo(ObjectUtils.toLong(parameterMap.get("user_no")))
            .additionType("S")
            .additionTypeDetail("pickup")
            .additionSumAmt(ObjectUtils.toLong(parameterMap.get("market_ship_amt")))
            .additionAmtBuyer(ObjectUtils.toLong(parameterMap.get("market_ship_amt")))
            .isApply("Y")
            .isIssue("N")
            .build();
    }

    /**
     * 티켓 상태 확인 (쿠프전용)
     *
     * @param parameterMap 요청 파라미터
     * @return 티켓 상태 결과
     * @throws Exception 오류시 오류처리
     */
    public Boolean getCoopTicketCheck(LinkedHashMap<String, Object> parameterMap) throws Exception {
        log.info("티켓 정보 확인(쿠프) 요청 정보 : {}", parameterMap);
        try{
            ClaimCoopTicketCheckGetDto returnDto =
                ExternalUtil.postTransfer(
                    ExternalUrlInfo.OUTBOUND_COOP_TICKET_CHECK_FOR_CLAIM,
//                    new ArrayList<SetParameter>(){{
//                        add(SetParameter.create("sellerItemCd", checkDto.getSellerItemCd()));
//                        add(SetParameter.create("ticketCd", checkDto.getTicketCd()));
//                    }},
                    parameterMap,
                    ClaimCoopTicketCheckGetDto.class
                );
            log.info("티켓 정보 확인(쿠프) 요청 결과 : {}", returnDto);
            return returnDto.getUseYn().equals("N");
        } catch (Exception e){
            log.error("티켓 정보 확인(쿠프) 요청 오류발생 ::: ErrorMsg :: {}", e.getMessage());
            return false;
        }
    }

    /**
     * 티켓 취소처리 (쿠프전용)
     *
     * @return
     * @throws Exception
     */
    public Boolean getCoopTicketCancel(LinkedHashMap<String, Object> parameterMap) throws Exception {
        log.info("티켓 정보 취소(쿠프) 요청 정보 : {}", parameterMap);
        try{
            ClaimCoopTicketCheckGetDto returnDto =
                ExternalUtil.postTransfer(
                    ExternalUrlInfo.OUTBOUND_COOP_TICKET_CANCEL_FOR_CLAIM,
                    parameterMap,
                    ClaimCoopTicketCheckGetDto.class
                );
            log.info("티켓 정보 취소(쿠프) 요청 결과 : {}", returnDto);
            return "00,12,83".contains(returnDto.getResultCode());
        } catch (Exception e){
            log.error("티켓 정보 취소(쿠프) 요청 오류발생 ::: ErrorMsg :: {}", e.getMessage());
            return false;
        }
    }

    public Integer getEmpDiscountLimitAmt(String empNo) throws Exception {
        EmpInfoDto empInfo = ExternalUtil.getTransfer(
            ExternalUrlInfo.EMP_DISCOUNT_LIMIT_AMT_INFO,
            new ArrayList<SetParameter>(){{ add(SetParameter.create("empNo", empNo)); }},
            EmpInfoDto.class
        );
        if(empInfo == null){
            throw new Exception("임직원할인잔액 취득에 실패하였습니다.");
        }
        return empInfo.getRemainPoint();
    }

    public void paymentCancelManual(String cancelType, LinkedHashMap<String, Object> paymentData) throws Exception {
        LinkedHashMap<String, Object> returnMap = new LinkedHashMap<>();
        if(cancelType.equals("MP")){
             returnMap = ExternalUtil.externalProcess(this.createPaymentCancelForMhc(paymentData));
        } else if(cancelType.equals("OC")){
            returnMap = ExternalUtil.externalProcess(this.createPaymentCancelForOcb(paymentData));
        } else if(cancelType.equals("PG")){
            returnMap = ExternalUtil.externalProcess(this.createPaymentPgCancelSetDto(paymentData));
        } else if(cancelType.equals("PP")){
            returnMap = ExternalUtil.externalProcess(this.createPaymentCancelForMileageDto(paymentData));
        }
        log.info("paymentCancelManual : {}", returnMap);
    }

    public void createClaimSafetyIssue(long claimBundleNo, String issueType) {
        try {
            Integer resultData = ExternalUtil.postTransfer(
                ExternalUrlInfo.CLAIM_SAFETY_ISSUE_ADD,
                mapperService.getClaimSafetyData(claimBundleNo, issueType),
                Integer.class
            );
            log.debug("createClaimSafetyIssue result ::: {}", resultData);
        } catch (Exception e) {
            log.error("createClaimSafetyIssue ERROR ::: {}", e.getMessage());
        }
    }

    public void setClaimShipComplete(String bundleNo, String regId) throws Exception{
        ExternalUtil.postTransfer(
            ExternalUrlInfo.SHIPPING_CLAIM_FROM_SHIP_COMPLETE,
            ClaimShipCompleteDto.builder().bundleNo(bundleNo).chgId(regId).build(),
            String.class
        );
    }

    public void setClaimShipDecision(String bundleNo, String regId) throws Exception{
        ExternalUtil.postTransfer(
            ExternalUrlInfo.SHIPPING_CLAIM_FROM_SHIP_DECISION,
            ClaimShipCompleteDto.builder().bundleNo(bundleNo).chgId(regId).build(),
            String.class
        );
    }

    private MhcCardInfoDto getUserMhcCardInfo(long userNo) throws Exception {
        try {
            return ExternalUtil.getTransfer(ExternalUrlInfo.USER_MHC_CARD_INFO, new ArrayList<SetParameter>(){{ add(SetParameter.create("userNo", userNo)); }}, MhcCardInfoDto.class);
        } catch (Exception e) {
            log.info("getUserMhcCardInfo ::: {}", e.getMessage());
            throw new Exception("MHC 카드 정보 취득에 실패하였습니다.");
        }
    }

    /**
     * 전체 취소 여부 체크
     * @param paymentData
     * @return
     */
    private Boolean isTotalCancel(LinkedHashMap<String, Object> paymentData) {
        if(ObjectUtils.toLong(paymentData.get("prev_payment_amt")) > 0) {
            return Boolean.FALSE;
        }
        if(!ObjectUtils.toLong(paymentData.get("payment_amt")).equals(ObjectUtils.toLong(paymentData.get("order_payment_amt")))){
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    private String getExchPickType(ClaimShippingRegDto shippingRegDto){
        if(shippingRegDto.getPickShipType().equals("N")){
            // DB 조회후 원배송에 따른 타입으로 설정.
            String[] shipMethod = shippingRegDto.getShipMethod().split("_");
            if("DRCT,PICK".contains(shipMethod[1])){
                return ExchangeShipType.DIRECT.getExchangeShipType();
            } else if ("POST".contains(shipMethod[1])){
                return ExchangeShipType.POST.getExchangeShipType();
            } else {
                return ExchangeShipType.DELIVERY.getExchangeShipType();
            }
        }
        return shippingRegDto.getPickShipType();
    }

}
