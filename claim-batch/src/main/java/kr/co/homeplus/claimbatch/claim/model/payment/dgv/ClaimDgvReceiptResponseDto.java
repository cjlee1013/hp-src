package kr.co.homeplus.claimbatch.claim.model.payment.dgv;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "취소시 사용할 DGV용 영수증 번호")
public class ClaimDgvReceiptResponseDto {
    @ApiModelProperty(value = "POS번호")
    private String posNo;
    @ApiModelProperty(value = "영수증번호")
    private String tradeNo;
}
