package kr.co.homeplus.claimbatch.history.mapper;

import java.util.LinkedHashMap;
import kr.co.homeplus.claimbatch.core.db.annotation.MasterConnection;
import kr.co.homeplus.claimbatch.history.model.BatchHistoryDto;

@MasterConnection
public interface BatchHistoryMasterMapper {

    // batch log insert
    void insertBatchHistory(BatchHistoryDto batchHistoryDto);

    // batch log insert
    int updateBatchHistory(BatchHistoryDto batchHistoryDto);

    int insertProcessHistory(LinkedHashMap<String, Object> parameterMap);
}
