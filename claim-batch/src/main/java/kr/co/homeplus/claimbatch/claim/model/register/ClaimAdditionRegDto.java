package kr.co.homeplus.claimbatch.claim.model.register;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClaimAdditionRegDto {
    @ApiModelProperty(value= "일련번호")
    private long claimAdditionNo;
    @ApiModelProperty(value= "클레임 마스터 번호")
    private long claimNo;
    @ApiModelProperty(value= "클레임 묶음 번호")
    private long claimBundleNo;
    @ApiModelProperty(value= "클레임 상품번호")
    private long claimItemNo;
    @ApiModelProperty(value= "주문번호")
    private long purchaseOrderNo;
    @ApiModelProperty(value= "묶음번호")
    private long bundleNo;
    @ApiModelProperty(value= "상품주문번호")
    private long orderItemNo;
    @ApiModelProperty(value= "상품번호")
    private String itemNo;
    @ApiModelProperty(value= "고객번호")
    private long userNo;
    @ApiModelProperty(value= "주문 할인번호")
    private long orderDiscountNo;
    @ApiModelProperty(value= "추가금액 유형(D(iscount) : 할인금액(쿠폰/즉시할인), S(hip) : 배송비), G(ift) : 사은품")
    private String additionType;
    @ApiModelProperty(value= "추가금액 유형 상세  (*할인 유형코드   상품별 - 상품쿠폰: item_coupon  상품별 - 상품즉시할인: item_discount  상품별 - 제휴즉시할인: affi_discount  배송번호별 - 배송비쿠폰: ship_coupon  --- 미사용(배송번호별 - 배송비즉시할인: ship_discount)  구매번호별 - 장바구니쿠폰: cart_coupon  구매번호별 - 카드사즉시할인: card_discount  그룹별 - 그룹쿠폰 : group_coupon  프로모션 : promo_discount  사은품 : gift    *배송비 유형코드  cancel : 취소배송비(합포장 박스 감소에 따른 돌려주는 배송비, 전체취소에 따른 배송비 환불) - 100%구매자 부담(- 기록)  delivery : 추가배송비(부분취소 또는 반품시 무료배송 조건깨지면서 발생하는 배송비) - 구매자/판매자 부담  pickup : 반품배송비(수거배송비) - 구매자/판매자 부담    cancel_island : 취소배송비 추가비용(도서산간)  delivery_island : 추가배송비 추가비용(도서산간)  pickup_island : 반품배송비(수거배송비) 추가비용(도서산간))")
    private String additionTypeDetail;
    @ApiModelProperty(value= "할인번호 ((addition_type = D and addition_type_detail = *_discount 인 경우 기록))")
    private long additionDiscountNo;
    @ApiModelProperty(value= "쿠폰번호 ((addition_type = D and addition_type_detail *_coupon 인 경우 기록))")
    private long additionCouponNo;
    @ApiModelProperty(value= "추가 총금액")
    private long additionSumAmt;
    @ApiModelProperty(value= "구매자부담금액")
    private long additionAmtBuyer;
    @ApiModelProperty(value= "판매자부담금액")
    private long additionAmtSeller;
    @ApiModelProperty(value= "홈플러스 부담금액")
    private long additionAmtHome;
    @ApiModelProperty(value= "카드사 부담금액")
    private long additionAmtCard;
    @ApiModelProperty(value= "쿠폰이 적용된 수량")
    private long couponApplyQty;
    @ApiModelProperty(value= "환불차감 대상여부 (클레임배송비 처리방법(claim_bundle. claim_ship_fee_enclose. = Y or claim_bundle. claim_ship_fee_bank. = Y )   claim_addition 항목 중 pickup, delivery is_apply-> N 설정 is_apply-> N 인경우 환불금액 미차감)")
    private String isApply;
    @ApiModelProperty(value= "사은품메시지(영수증 노출 문구)")
    private String giftMsg;
    @ApiModelProperty(value= "재발행여부")
    private String isIssue;
    @ApiModelProperty(value= "등록일")
    private String regDt;
    @ApiModelProperty(value= "쿠폰전체 취소 여부(Y/N)")
    private String totalCancelYn;
    @ApiModelProperty(value= "다중 주문 배송 번호")
    private long multiBundleNo;
    @ApiModelProperty(value= "다중 주문 아이템번호 ")
    private long multiOrderItemNo;
}
