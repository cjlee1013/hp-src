package kr.co.homeplus.claimbatch.claim.model.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "클레임 완료시 쿠폰 재발행 DTO")
public class ClaimCouponReIssueSetDto {
    @ApiModelProperty(value = "쿠폰재발행정보", position = 1)
    List<CouponReIssueDto> couponReIssueList;
    @ApiModelProperty(value = "고객번호", position = 2)
    private long userNo;
    @ApiModelProperty(value = "주문번호", position = 3)
    private long purchaseOrderNo;

    @Data
    @Builder
    @ApiModel(description = "재발행 대상 쿠폰 정보")
    public static class CouponReIssueDto {
        @ApiModelProperty(value = "발행번호")
        private Long issueNo;
        @ApiModelProperty(value = "쿠폰번호")
        private Long couponNo;
        @ApiModelProperty(value = "상점ID")
        private Integer storeId;
        @ApiModelProperty(value = "재발행금액")
        private Long discountAmt;
        @ApiModelProperty(value = "claimAdditionNo", hidden = true)
        private Long claimAdditionNo;
    }
}
