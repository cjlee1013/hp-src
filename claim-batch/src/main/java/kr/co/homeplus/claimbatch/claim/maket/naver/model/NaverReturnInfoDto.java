package kr.co.homeplus.claimbatch.claim.maket.naver.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class NaverReturnInfoDto {
    @ApiModelProperty(value= "클레임처리상태코드")
    private String claimStatus;
    @ApiModelProperty(value= "클레임요청일")
    private String claimRequestDate;
    @ApiModelProperty(value= "접수채널")
    private String requestChannel;
    @ApiModelProperty(value= "반품사유코드")
    private String returnReason;
    @ApiModelProperty(value= "반품상세사유")
    private String returnDetailedReason;
    @ApiModelProperty(value= "보류상태코드")
    private String holdbackStatus;
    @ApiModelProperty(value= "보류사유코드")
    private String holdbackReason;
    @ApiModelProperty(value= "보류상세사유")
    private String holdbackDetailedReason;
    @ApiModelProperty(value= "수거지주소(from)")
    private NaverAddressDto collectAddress;
    @ApiModelProperty(value= "수취지주소(to)")
    private NaverAddressDto returnReceiveAddress;
    @ApiModelProperty(value= "수거상태")
    private String collectStatus;
    @ApiModelProperty(value= "수거방법코드")
    private String collectDeliveryMethod;
    @ApiModelProperty(value= "수거택배사코드")
    private String collectDeliveryCompany;
    @ApiModelProperty(value= "수거송장번호")
    private String collectTrackingNumber;
    @ApiModelProperty(value= "수거완료일")
    private String collectCompletedDate;
    @ApiModelProperty(value= "반품배송비청구액")
    private long claimDeliveryFeeDemandAmount;
    @ApiModelProperty(value= "반품배송비결제방법")
    private String claimDeliveryFeePayMethod;
    @ApiModelProperty(value= "반품배송비결제수단")
    private String claimDeliveryFeePayMeans;
    @ApiModelProperty(value= "기타비용청구액")
    private long etcFeeDemandAmount;
    @ApiModelProperty(value= "기타비용결제방법")
    private String etcFeePayMethod;
    @ApiModelProperty(value= "기타비용결제수단")
    private String etcFeePayMeans;
    @ApiModelProperty(value= "환불대기상태")
    private String refundStandbyStatus;
    @ApiModelProperty(value= "환불대기사유")
    private String refundStandbyReason;
    @ApiModelProperty(value= "환불예정일")
    private String refundExpectedDate;
    @ApiModelProperty(value= "반품완료일")
    private String returnCompletedDate;
    @ApiModelProperty(value= "보류설정일")
    private String holdbackConfigDate;
    @ApiModelProperty(value= "보류설정자(구매자,판매자,관리자,시스템)")
    private String holdbackConfigurer;
    @ApiModelProperty(value= "보류해제일")
    private String holdbackReleaseDate;
    @ApiModelProperty(value= "보류해제자(구매자,판매자,관리자,시스템)")
    private String holdbackReleaser;
    @ApiModelProperty(value= "반품배송비묶음청구상품주문번호(다중일시쉼표구분)")
    private String claimDeliveryFeeProductOrderIds;
    @ApiModelProperty(value= "반품배송비할인액")
    private long claimDeliveryFeeDiscountAmount;
}
