package kr.co.homeplus.claimbatch.claim.model.payment.mhc;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "MHC 결제 취소 DTO")
public class ClaimMhcCancelSetDto {

    @ApiModelProperty(notes = "고객번호", required = true, position = 1)
    private long userNo;

    @ApiModelProperty(notes = "사이트유형(HOME:홈플러스, CLUB:더클럽)", required = true, position = 2)
    private String siteType;

    @ApiModelProperty(notes = "매출일자(YYYYMMDD)", required = true, position = 3)
    private String saleDt;

    @ApiModelProperty(notes = "MHC 대체카드번호_C", required = true, position = 4)
    private String mhcCardNoEnc;

    @ApiModelProperty(notes = "MHC 대체카드번호_H", required = true, position = 5)
    private String mhcCardNoHmac;

    @ApiModelProperty(notes = "클레임번호", required = true, position = 6)
    private long claimNo;

    @ApiModelProperty(notes = "취소금액", required = true, position = 7)
    private long cancelAmt;

    @ApiModelProperty(notes = "원거래승인일자", position = 8)
    private String orgMhcAprDate;

    @ApiModelProperty(notes = "원거래승인번호", position = 9)
    private String orgMhcAprNumber;

    private String partCancelYn;

}
