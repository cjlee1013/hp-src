package kr.co.homeplus.claimbatch.claim.entity.claim;

public class ClaimItemEntity {
    // 클레임 상품번호
    public String claimItemNo;
    // 클레임 번호(환불:클레임번호)
    public String claimNo;
    // 클레임 묶음번호
    public String claimBundleNo;
    // 결제번호
    public String paymentNo;
    // 주문번호
    public String purchaseOrderNo;
    // 배송번호
    public String bundleNo;
    // 원천점포ID(매출용실제점포)
    public String originStoreId;
    // 점포ID(주문서에서 넘어온 점포ID)
    public String storeId;
    // FC점포ID
    public String fcStoreId;
    // 상품주문번호(결제완료:주문번호)
    public String orderItemNo;
    // 상품번호
    public String itemNo;
    // 상품명
    public String itemName;
    // 상품가격
    public String itemPrice;
    // 클레임 상품수량(옵션합계)
    public String claimItemQty;
    // 대체수량
    public String changeQty;
    // 점포유형(Hyper, Club, Exp, DS)
    public String storeType;
    // 몰유형(DS:업체상품,TD:매장상품)
    public String mallType;
    // 세금여부(Y:과세,N:면세)
    public String taxYn;
    // 파트너 아이디
    public String partnerId;
    // 임직원할인율
    public String empDiscountRate;
    // 임직원할인금액
    public String empDiscountAmt;
    // 등록일
    public String regDt;
    // 등록아이디
    public String regId;
    // 마켓연동주문번호
    public String marketOrderNo;
}
