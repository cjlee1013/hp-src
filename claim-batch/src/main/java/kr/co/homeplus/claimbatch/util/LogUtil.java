package kr.co.homeplus.claimbatch.util;

    import java.util.LinkedHashMap;
    import javax.annotation.PostConstruct;
    import kr.co.homeplus.claimbatch.history.mapper.BatchHistoryMasterMapper;
    import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
    import lombok.RequiredArgsConstructor;
    import lombok.extern.slf4j.Slf4j;
    import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class LogUtil {

    private final BatchHistoryMasterMapper batchHistoryMasterMapper;
    private static BatchHistoryMasterMapper historyMapper;

    @PostConstruct
    private void initialize() {
        historyMapper = batchHistoryMasterMapper;
    }

    private static final String RESPONSE_MSG = "[HttpStatus : {}\n응답코드 : {}\n응답메시지 : {}\n데이터 : {}]";

    public static void responsePrint(ResponseObject responseObject) {
        log.info(RESPONSE_MSG, responseObject.getReturnStatus(), responseObject.getReturnCode(), responseObject.getReturnMessage(), responseObject.getData());
    }

    public static boolean addProcessHistoryMapper(LinkedHashMap<String, Object> parameterMap) {
        log.info("프로세스 히스토리에 데이터를 저장합니다. :: [{}]", parameterMap);
        return SqlUtils.isGreaterThanZero(historyMapper.insertProcessHistory(parameterMap));
    }
}
