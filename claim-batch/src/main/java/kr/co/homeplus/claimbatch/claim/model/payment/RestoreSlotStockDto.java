package kr.co.homeplus.claimbatch.claim.model.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@ApiModel(description = "클레임 발생시 슬롯 환원을 위한 DTO(TD일경우만)")
public class RestoreSlotStockDto {

    @ApiModelProperty(value = "구매주문번호", position = 1)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "배송번호", position = 2)
    private long bundleNo;

    @ApiModelProperty(value = "클레임번호", position = 3)
    private long claimNo;

    @ApiModelProperty(value = "회원번호", position = 4)
    private long userNo;

}
