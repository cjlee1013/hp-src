package kr.co.homeplus.claimbatch.claim.maket.naver.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PickupLocationType {
    FRONT_OF_DOOR("FRONT_OF_DOOR", "수령위치:문 앞"),
    MANAGEMENT_OFFICE("MANAGEMENT_OFFICE", "수령위치:경비실 보관"),
    DIRECT_RECEIVE("DIRECT_RECEIVE", "수령위치:직접 수령"),
    OTHER("OTHER", "수령위치:기타"),
    ;

    private String pickupLocationType;
    private String pikcupLocationTypeNm;

}