package kr.co.homeplus.claimbatch.claim.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimBundleRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimItemRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimShippingRegDto;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimRegisterService {

    private final ClaimCommonService commonService;
    private final ClaimMapperService mapperService;

    @Transactional(rollbackFor = {Exception.class}, propagation = Propagation.NESTED)
    public LinkedHashMap<String, Object> addClaimRegister(ClaimRegSetDto regSetDto) throws Exception {
        // Claim_mst 등록
        ClaimMstRegDto claimMstRegDto = insertClaimMstProcess(regSetDto);
        // 클레임 등록을 위한 클레임 데이터 가지고 오기
        LinkedHashMap<String, Object> createClaimData = commonService.createClaimDataMap(regSetDto);
        // claim_mst 에 등록된 claim_no 를 map 넣음.
        createClaimData.put("claim_no", claimMstRegDto.getClaimNo());
        // 클레임 번들 등록
        createClaimData = insertClaimPartBasicFromBundleProcess(createClaimData, regSetDto);
        // 번들등록 후 데이터 확인
        log.info("클레임 등록(번들등록후) : {}", createClaimData);
        // claim addition reg
        mapperService.callByClaimAdditionReg(claimMstRegDto.getClaimNo());
        // 클레임 취소금액 등록
        insertClaimPaymentProcess(createClaimData, claimMstRegDto);
        //안신번호 발행.(반품/수거만)
        if(!regSetDto.getClaimType().equals("C") && ObjectUtils.toString(createClaimData.get("safety_use_yn")).equals("Y")){
            // 수거 등록
            commonService.createClaimSafetyIssue(ObjectUtils.toLong(createClaimData.get("claim_bundle_no")), "R");
            if(regSetDto.getClaimType().equals("X")){
                commonService.createClaimSafetyIssue(ObjectUtils.toLong(createClaimData.get("claim_bundle_no")), "X");
            }
        }

        if(!regSetDto.getClaimType().equals("C")){
            // 기조 배송상태 완료로 변경.
            if("D3".contains(ObjectUtils.toString(createClaimData.get("ship_status")))){
                commonService.setClaimShipComplete(regSetDto.getBundleNo(), regSetDto.getRegId());
            }
        }
        // 클레임 결과 업데이트
        return createClaimData;
    }

    public boolean isClaimMinusRefundPossible(long claimMinusTargetAmt, LinkedHashMap<String, Object> preClaimMinusRefundMap) {
        if(preClaimMinusRefundMap.isEmpty()){
            return Boolean.FALSE;
        }
        long totItemPrice = ObjectUtils.toLong(preClaimMinusRefundMap.get("tot_item_price"));
        long totDiscountAmt = ObjectUtils.toLong(preClaimMinusRefundMap.get("tot_discount_amt"));

        if(totItemPrice - (totDiscountAmt + claimMinusTargetAmt) <= 0){
            log.info("클레입그룹번호({})에 대한 쿠폰할인금액이 0워이하 이므로 처리 종료.(할인계산금액 : {})", preClaimMinusRefundMap.get("claim_no"), (totItemPrice - (totDiscountAmt + claimMinusTargetAmt)));
            return Boolean.FALSE;
        }
        log.info("클레입그룹번호({})에 대한 쿠폰할인금액 금액 정보 : [{}/\n{}]", preClaimMinusRefundMap.get("claim_no"), preClaimMinusRefundMap, claimMinusTargetAmt);
        return Boolean.TRUE;
    }

    /**
     * 클레임 마스터 데이터 생성
     *
     * @param regSetDto 클레임 등록 데이터
     * @return 클레임 마스터 데이터
     * @throws Exception 오류시 오류처리.
     */
    private ClaimMstRegDto insertClaimMstProcess(ClaimRegSetDto regSetDto) throws Exception {
        // 클레임 마스터 등록을 위한 DTO 설정.
        // 환불 예정 금액 조회를 다시 한다. (정합성 체크)
        ClaimMstRegDto claimMstRegDto = commonService.getFusionForClaimMst(commonService.convertCalcSetDto(regSetDto));
        // 취소/반품일 시 클레임금액이 0원미만 이면 클레임 처리 안함.
        if(claimMstRegDto.getCompleteAmt() < 0 && "C,R".contains(regSetDto.getClaimType())){
            throw new Exception("취소/반품시 클레임비용이 0원미만입니다.");
        }
        // 임직원번호가 있으면 입직원할인 잔액 조회.
        if(!StringUtils.isEmpty(claimMstRegDto.getEmpNo())){
            claimMstRegDto.setEmpRemainAmt(commonService.getEmpDiscountLimitAmt(claimMstRegDto.getEmpNo()));
        }
        // 클레임
        if(!mapperService.addClaimMstInfo(claimMstRegDto)){
            throw new Exception("클레임등록이 되지 않았습니다.");
        }
        return claimMstRegDto;
    }

    /**
     * 클레임 번들 등록
     *
     * @param createClaimData 클레임 기본데이터
     * @param regSetDto 클레임 등록데이터
     * @throws Exception 오류시 오류처리
     */
    private LinkedHashMap<String, Object>  insertClaimPartBasicFromBundleProcess(LinkedHashMap<String, Object> createClaimData, ClaimRegSetDto regSetDto) throws Exception {
        if("R,X".contains(regSetDto.getClaimType())){
            createClaimData.put("isEnclose", regSetDto.getClaimDetail().getIsEnclose());
        }
        // 클레임 번들
        ClaimBundleRegDto claimBundleRegDto = commonService.createClaimBundleRegDto(createClaimData);
        // 클레임 번들 등록
        if(!mapperService.addClaimBundleInfo(claimBundleRegDto)){
            throw new Exception("클레임 등록에 실패하였습니다.");
        }
        // claim_bundle_no 를 map 에 넣음.
        createClaimData.put("claim_bundle_no", claimBundleRegDto.getClaimBundleNo());

        if("R,X".contains(regSetDto.getClaimType())){
            log.debug("insert claimReq :: {}", !StringUtils.isEmpty(regSetDto.getClaimDetail().getUploadFileName()));
            if(!StringUtils.isEmpty(regSetDto.getClaimDetail().getUploadFileName())){
                createClaimData.put("upload_file_name", regSetDto.getClaimDetail().getUploadFileName());
            }
            if(!StringUtils.isEmpty(regSetDto.getClaimDetail().getUploadFileName2())){
                createClaimData.put("upload_file_name2", regSetDto.getClaimDetail().getUploadFileName2());
            }
            if(!StringUtils.isEmpty(regSetDto.getClaimDetail().getUploadFileName3())){
                createClaimData.put("upload_file_name3", regSetDto.getClaimDetail().getUploadFileName3());
            }
        }
        // 환불예정금액 재조회를 위한 파라미터 저장 추가.
        // 할인미차감여부
        createClaimData.put("deduct_discount_yn", regSetDto.getPiDeductDiscountYn());
        // 행사미차감여부
        createClaimData.put("deduct_promo_yn", regSetDto.getPiDeductPromoYn());
        // 배송비미차감여부
        createClaimData.put("deduct_ship_yn", regSetDto.getPiDeductShipYn());

        // 클레임 요청
        if(!mapperService.addClaimReqInfo(commonService.createClaimReqRegDto(createClaimData))){
            throw new Exception("클레임 등록에 실패하였습니다.");
        }
        // 클레임 아이템 및 옵션별 저장.
        this.claimItemPartRegProcess(createClaimData, regSetDto);

        if(!regSetDto.getClaimType().equalsIgnoreCase("C")){
            ClaimShippingRegDto shippingRegDto = commonService.createClaimPickShippingDto(regSetDto.getClaimDetail(),  createClaimData);
            // pick insert
            if(!mapperService.addClaimPickShippingInfo(shippingRegDto)){
                throw new Exception("클레임 등록에 실패하였습니다.(반품정보생성중오류)");
            }

            if(regSetDto.getClaimType().equalsIgnoreCase("X")){
                if(!mapperService.addClaimExchShipping(commonService.createClaimExchShippingRegDto(shippingRegDto, regSetDto.getClaimDetail()))){
                    throw new Exception("클레임 등록에 실패하였습니다.(교환정보생성중오류)");
                }
            }
            createClaimData.put("pickStatus", shippingRegDto.getPickStatus());
        }
        return createClaimData;
    }

    /**
     * 클레임 결제취소 데이터 등록
     *
     * @param createClaimData 클레임 기본데이터
     * @param claimMstRegDto 클레임 마스터 등록데이터 (환불예정금액포함)
     * @throws Exception 오류시 오류처리.
     */
    private void insertClaimPaymentProcess(LinkedHashMap<String, Object> createClaimData, ClaimMstRegDto claimMstRegDto) throws Exception {
        LinkedHashMap<String, Long> claimPaymentType = commonService.getClaimPaymentTypeList(claimMstRegDto);

        if(claimPaymentType == null){
            claimPaymentType = new LinkedHashMap<>(){{
                put("FREE", 0L);
            }};
        }

        for(String key : claimPaymentType.keySet()){
            if(!mapperService.addClaimPaymentInfo(commonService.createClaimPaymentRegDto(createClaimData, key, claimPaymentType.get(key)))){
                throw new Exception("클레임 등록에 실패하였습니다.");
            }
        }
    }

    /**
     * 클레임 아이템 등록 Process
     * 클레임 아이템 등록 후 클레임 옵션 등록
     *
     * @param claimData 클레임 생성 데이터 맵
     * @param claimRegSetDto 클레임 등록 데이터
     * @throws Exception 오류시 오류처리
     */
    private void claimItemPartRegProcess(LinkedHashMap<String, Object> claimData, ClaimRegSetDto claimRegSetDto) throws Exception {
        // 이전상품주문번호
        long prevOrderItemNo = 0L;
        // 클레임 상품번호
        long claimItemNo = 0L;
        // 아이템 번호별 loop
        for(ClaimPreRefundItemList itemList : claimRegSetDto.getClaimItemList()){
            LinkedHashMap<String, Object> claimItemInfo = mapperService.getClaimItemInfo(Long.parseLong(claimRegSetDto.getPurchaseOrderNo()), Long.parseLong(itemList.getOrderItemNo()));
            // 클레임 데이터 넣기
            claimItemInfo.putAll(claimData);
            // 클레임 등록 DTO 생성
            ClaimItemRegDto claimItemRegDto = commonService.createClaimItemRegDto(claimItemInfo);
            // 이전 주문번호와 현재 처리 주문번호가 같으면 insert skip
            if(prevOrderItemNo != claimItemRegDto.getOrderItemNo()){
                log.info("주문번호[{}] - 상품주문번호[{}]에 대한 Claim_item 생성 데이터 조회 :: {}", claimRegSetDto.getPurchaseOrderNo(), itemList.getOrderItemNo(), claimItemInfo);
                // 클레임 신청 수량
                int claimItemQty = claimRegSetDto.getClaimItemList().stream().filter(itemList1 -> itemList1.getOrderItemNo().equals(String.valueOf(claimItemRegDto.getOrderItemNo()))).map(ClaimPreRefundItemList::getClaimQty).mapToInt(Integer::parseInt).sum();
                // validation
                if(ObjectUtils.isEquals(claimItemInfo.get("item_qty"), claimItemInfo.get("claim_item_qty")) || claimItemQty <= 0){
                    throw new Exception("클레임 불가능한 상품이 존재합니다.");
                }
                // 클래임 건수 입력
                claimItemRegDto.setClaimItemQty(claimItemQty);
                // 클레임 아이템생성.
                if(!mapperService.addClaimItemInfo(claimItemRegDto)){
                    throw new Exception("클레임 등록에 실패하였습니다.");
                }
                claimItemNo = claimItemRegDto.getClaimItemNo();
                prevOrderItemNo = claimItemRegDto.getOrderItemNo();
                // 티켓 취소시 여기서 처리.
                if(ObjectUtils.toString(claimItemInfo.get("order_type")).equals("ORD_TICKET")){
                    List<LinkedHashMap<String, Object>> orderTicketInfo = mapperService.getOrderTicketInfo(claimItemRegDto.getPurchaseOrderNo(), claimItemRegDto.getOrderItemNo(), claimItemQty);
                    if(orderTicketInfo.stream().map(map -> ObjectUtils.toString(map.get("issue_channel"))).map(String::valueOf).anyMatch(str -> str.equals("COOP"))){
                        List<LinkedHashMap<String, Object>> coopTicketInfoList = orderTicketInfo.stream().filter(map -> ObjectUtils.toString(map.get("issue_channel")).equals("COOP")).collect(
                            Collectors.toList());
                        if(coopTicketInfoList.size() != claimItemQty){
                            throw new Exception("취소수량과 티켓수량이 맞지 않습니다.");
                        }
                        // 티켓 체크
                        for(LinkedHashMap<String, Object> coopTicketInfoMap : coopTicketInfoList){
                            if(!commonService.getCoopTicketCheck(ObjectUtils.toCamelMap(coopTicketInfoMap, "seller_item_cd", "ticket_cd"))){
                                throw new Exception("취소불가능한 티켓이 포함되어 있습니다.");
                            }
                        }
                    }
                    for(LinkedHashMap<String, Object> insertMap : orderTicketInfo){
                        insertMap.putAll(claimItemInfo);
                        insertMap.put("claim_item_no", claimItemNo);
                        if(!mapperService.addClaimTicketInfo(commonService.createClaimTicketDto(insertMap))){
                            throw new Exception("티켓취소정보 저장중 오류가 발생하였습니다.");
                        }
                    }
                }
            }
            // opt 데이터 조회
            // 주문에서 옵션별 데이터를 가지고 온다.
            ClaimOptRegDto claimOptRegDto = mapperService.getOriginOrderOptInfoForClaim(Long.parseLong(itemList.getOrderItemNo()), itemList.getOrderOptNo());
            // 클레임 번호
            claimOptRegDto.setClaimNo((Long) claimItemInfo.get("claim_no"));
            // 클레임 번들 번호
            claimOptRegDto.setClaimBundleNo((Long) claimItemInfo.get("claim_bundle_no"));
            // 클래암 아이템 번호
            claimOptRegDto.setClaimItemNo(claimItemNo);
            // 클레임 수량
            claimOptRegDto.setClaimOptQty(Long.parseLong(itemList.getClaimQty()));
            // 마켓주문번호
            claimOptRegDto.setMarketOrderNo(ObjectUtils.toString(claimItemInfo.get("market_order_no")));
            // 생성한 데이터를 클레임 옵션별에 저장.
            if(!mapperService.addClaimOptInfo(claimOptRegDto)){
                throw new Exception("클레임 등록에 실패하였습니다.");
            }
            // 아이템 임직원 할인
            mapperService.callByClaimItemEmpDiscount(claimItemRegDto.getClaimNo(), claimItemRegDto.getOrderItemNo());
        }
    }
}
