package kr.co.homeplus.claimbatch.pine.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "PINE 영수증 누락 거래정보(claim_pine_receipt_omit_mng)")
@Getter
@Setter
@EqualsAndHashCode
public class PineReceiptOmitMngDto {

    @ApiModelProperty(value = "점포ID", position = 1)
    private String storeId;

    @ApiModelProperty(value = "POS번호", position = 2)
    private String posNo;

    @ApiModelProperty(value = "거래일자(YYYYMMDD)", position = 3)
    private String tradeDt;

    @ApiModelProperty(value = "누락번호", position = 4)
    private int omitNo;

    @ApiModelProperty(value = "다음 누락번호", position = 5)
    private int nextOmitNo;

    @ApiModelProperty(value = "본부코드", position = 6)
    private String headOfficeCode;

    @ApiModelProperty(value = "지역코드", position = 7)
    private String areaCode;
}
