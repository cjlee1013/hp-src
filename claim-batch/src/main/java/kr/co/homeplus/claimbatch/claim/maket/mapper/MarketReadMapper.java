package kr.co.homeplus.claimbatch.claim.maket.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claimbatch.claim.maket.naver.sql.MarketClaimSql;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimRegDetailDto;
import kr.co.homeplus.claimbatch.core.db.annotation.SlaveConnection;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

@SlaveConnection
public interface MarketReadMapper {

    @SelectProvider(type = MarketClaimSql.class, method = "selectMarketOrderItemCheck")
    LinkedHashMap<String, Object> selectMarketOrderItemCheck(@Param("marketOrderItemNo") String marketOrderItemNo);

    @SelectProvider(type = MarketClaimSql.class, method = "selectCreateMarketClaimData")
    List<LinkedHashMap<String, Object>> selectCreateMarketClaimData(@Param("marketOrderItemNo") String marketOrderItemNo);

    @SelectProvider(type = MarketClaimSql.class, method = "selectClaimReasonType")
    LinkedHashMap<String, Object> selectClaimReasonType(@Param("claimReasonType") String claimReasonType);

    @SelectProvider(type = MarketClaimSql.class, method = "selectClaimExchangeAndReturnInfo")
    ClaimRegDetailDto selectClaimExchangeAndReturnInfo(@Param("marketClaimNo") long marketClaimNo, @Param("marketOrderItemNo") String marketOrderItemNo);

    @SelectProvider(type = MarketClaimSql.class, method = "selectPrevInsertDataCheck")
    int selectPrevInsertDataCheck(@Param("marketClaimId") String marketClaimId);

    @SelectProvider(type = MarketClaimSql.class, method = "selectExistingClaimCheck")
    LinkedHashMap<String, Object> selectExistingClaimCheck(@Param("marketClaimNo") long marketClaimNo);

    @SelectProvider(type = MarketClaimSql.class, method = "selectMarketClaimItemReqCnt")
    int selectMarketClaimItemReqCnt(@Param("marketClaimNo") long marketClaimNo);

    @SelectProvider(type = MarketClaimSql.class, method = "selectPrevClaimItemCntByMarket")
    int selectPrevClaimItemCntByMarket(@Param("marketClaimNo") long marketClaimNo);
}