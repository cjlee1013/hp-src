package kr.co.homeplus.claimbatch.claim.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.claimbatch.claim.enums.ClaimCode;
import kr.co.homeplus.claimbatch.claim.mapper.ClaimReadMapper;
import kr.co.homeplus.claimbatch.claim.mapper.ClaimWriteMapper;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import kr.co.homeplus.claimbatch.claim.model.payment.ClaimCouponReIssueSetDto.CouponReIssueDto;
import kr.co.homeplus.claimbatch.claim.model.payment.RestoreStockItemDto;
import kr.co.homeplus.claimbatch.claim.model.payment.RestoreStockItemDto.RestoreStoreItemListDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimAdditionRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimBundleRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimExchShippingRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimInfoRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimItemRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimOptRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimPaymentRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimReqRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimShippingRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.omni.OmniOriginOrderDto;
import kr.co.homeplus.claimbatch.claim.model.register.safety.ClaimSafetySetDto;
import kr.co.homeplus.claimbatch.claim.model.register.ticket.ClaimTicketModifyDto;
import kr.co.homeplus.claimbatch.claim.model.register.ticket.ClaimTicketRegDto;
import kr.co.homeplus.claimbatch.claim.service.ClaimMapperService;
import kr.co.homeplus.claimbatch.util.SqlUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimMapperServiceImpl implements ClaimMapperService {

    private final ClaimReadMapper readMapper;
    private final ClaimWriteMapper writeMapper;

    @Override
    public ClaimPreRefundCalcGetDto getClaimPreRefundCalculation(ClaimPreRefundCalcSetDto calcSetDto) throws Exception {
        // 다중 클레임 건 여부 확인
        boolean isMulti = Boolean.FALSE;
        if(calcSetDto.getClaimItemList().size() > 1) {
            isMulti = Boolean.TRUE;
        }
        if(isMulti){
            log.debug("Claim Pre Refund Calculation is Multi Action....");
            return readMapper.callByClaimPreRefundPriceMultiCalculation(this.createPreRefundMap(calcSetDto, Boolean.TRUE));
        }
        return readMapper.callByClaimPreRefundPriceCalculation(this.createPreRefundMap(calcSetDto, Boolean.FALSE));
    }

    @Override
    public ClaimPreRefundCalcGetDto getClaimPreRefundCalculationByMarket(ClaimPreRefundCalcSetDto calcSetDto) throws Exception {
        return readMapper.callByClaimPreRefundPriceMarketCalculation(this.createPreRefundMap(calcSetDto, Boolean.TRUE));
    }

    /**
     * 클레임 번들 생성을 위한 데이터 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param bundleNo 배송번호
     * @return HashMap<String, Object> 클레임번들 기본정보
     */
    @Override
    public LinkedHashMap<String, Object> getCreateClaimBundleInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectCreateClaimDataInfo(purchaseOrderNo, bundleNo);
    }

    /**
     * 클레임 아이템 생성을 위한 데이터 조회
     *
     * @param purchaseOrderNo 주문번호
     * @param orderItemNo 상품주문번호
     * @return HashMap<String, Object> 클레임 아이템 기본정보
     */
    @Override
    public LinkedHashMap<String, Object> getClaimItemInfo(long purchaseOrderNo, long orderItemNo) {
        return readMapper.selectClaimItemInfo(purchaseOrderNo, orderItemNo);
    }

    /**
     * 클레임 마스터 등록
     *
     * @param claimMstRegDto 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimMstInfo(ClaimMstRegDto claimMstRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimMst(claimMstRegDto));
    }


    /**
     * 클레임 번들 등록
     *
     * @param claimBundleRegDto 클레임 번들 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimBundleInfo(ClaimBundleRegDto claimBundleRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimBundle(claimBundleRegDto));
    }

    /**
     * 클레임 요청 등록
     *
     * @param claimReqRegDto 클레임 요청 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimReqInfo(ClaimReqRegDto claimReqRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimReq(claimReqRegDto));
    }

    /**
     * 클레임 아이템 정보 등록
     *
     * @param claimItemRegDto 클레임 아이템 등록 데이터
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimItemInfo(ClaimItemRegDto claimItemRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimItemReq(claimItemRegDto));
    }

    /**
     * 클레임 결제 취소를 위한 원주문 결제 정보 조회
     *
     * @param paymentNo 결제 번호
     * @param paymentType 결제 타입
     * @return 클레임 결제 취소를 위한 원주문 결제 정보 맵
     */
    @Override
    public LinkedHashMap<String, Object> getOriPaymentInfoForClaim(long paymentNo, String paymentType){
        if(paymentType.equals("FREE")){
            paymentType = null;
        }
        return readMapper.selectOriPaymentForClaim(paymentNo, paymentType);
    }

    /**
     * 클레임 결제 취소 정보 등록
     *
     * @param claimPaymentRegDto 클레임 결제 취소 정보
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimPaymentInfo(ClaimPaymentRegDto claimPaymentRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimPayment(claimPaymentRegDto));
    }

    /**
     * 클레임 아이템 옵션별 등록을 위한 데이터 조회
     *
     * @param orderItemNo 아이템주문번호
     * @param orderOptNo 옵션번호
     * @return 클레임 옵션 등록 정보
     */
    @Override
    public ClaimOptRegDto getOriginOrderOptInfoForClaim(long orderItemNo, String orderOptNo){
        return readMapper.selectOrderOptInfoForClaim(orderItemNo, orderOptNo);
    }

    /**
     * 클레임 옵션별 등록
     *
     * @param claimOptRegDto 클레임 옵션별 데이터 DTO
     * @return Boolean 등록 성공 유무
     */
    @Override
    public Boolean addClaimOptInfo(ClaimOptRegDto claimOptRegDto){
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimOpt(claimOptRegDto));
    }

    /**
     * 클레임 - 결제취소를 위한 데이터 조회
     *
     * @param claimNo 클레임 번호
     * @return LinkedHashMap<String, Object> 결제취소용 데이터
     */
    @Override
    public List<LinkedHashMap<String, Object>> getPaymentCancelInfo(long claimNo) {
        return writeMapper.selectPaymentCancelInfo(claimNo);
    }

    /**
     * 클레임 - 클레임상태 변경 업데이트
     *
     *
     * @param parameter@return Boolean 클레임상태변경 업데이트 결과
     */
    @Override
    public Boolean setClaimStatus(LinkedHashMap<String, Object> parameter) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimStatus(parameter));
    }

    /**
     * 클레임 - 클레임 결제 취소 상태 업데이트
     *
     * @param claimPaymentNo 클레임 페이먼트 번호
     * @param claimCode 클레임 코드
     * @return Boolean 클레임 결제취소상태 업데이트 결과
     */
    @Override
    public Boolean setClaimPaymentStatus(long claimPaymentNo, String claimApprovalCd, ClaimCode claimCode, String isLastYn) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimPaymentStatus(claimPaymentNo, claimApprovalCd, claimCode, isLastYn));
    }

    @Override
    public void callByEndClaimFlag(ClaimInfoRegDto claimInfoRegDto) {
        writeMapper.callByClaimReg(claimInfoRegDto);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getOrderBundleByClaim(long purchaseOrderNo) {
        return readMapper.selectOrderBundleByClaim(purchaseOrderNo);
    }

    @Override
    public List<ClaimPreRefundItemList> getClaimPreRefundItemInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectOrderItemInfoByClaim(purchaseOrderNo, bundleNo);
    }

    @Override
    public List<ClaimPreRefundItemList> getClaimPreRefundItemInfoByMarket(long purchaseOrderNo, long bundleNo, String marketOrderItemNo) {
        return readMapper.selectOrderItemInfoByMarketClaim(purchaseOrderNo, bundleNo, marketOrderItemNo);
    }

    @Override
    public LinkedHashMap<String, Object> getOrderByClaimPartInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectOrderByClaimPartInfo(purchaseOrderNo, bundleNo);
    }

    @Override
    public void callByClaimAdditionReg(long claimNo) {
        LinkedHashMap<String, Object> returnMap = writeMapper.callByClaimAdditionReg(claimNo);
        log.debug("callByClaimAdditionReg ::: {}", returnMap);
    }

    @Override
    public void callByClaimItemEmpDiscount(long claimNo, long orderItemNo) {
        writeMapper.callByClaimItemEmpDiscount(claimNo, orderItemNo);
    }

    @Override
    public LinkedHashMap<String, Object> getCreateClaimMstInfo(long purchaseOrderNo) {
        return readMapper.selectCreateClaimMstDataInfo(purchaseOrderNo);
    }

    @Override
    public LinkedHashMap<String, Object> getClaimPossibleCheck(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectClaimPossibleCheck(purchaseOrderNo, bundleNo);
    }

    @Override
    public List<CouponReIssueDto> getClaimCouponReIssueInfo(long claimNo, long userNo) {
        List<CouponReIssueDto> responseList = new ArrayList<>();
        List<CouponReIssueDto> reIssueList = readMapper.selectClaimCouponReIssue(claimNo, userNo);
        if(reIssueList != null) {
            responseList.addAll(reIssueList);
        }
        List<CouponReIssueDto> addReIssueList = readMapper.selectClaimAddCouponReIssue(claimNo, userNo);
        if(addReIssueList != null) {
            responseList.addAll(addReIssueList);
        }
        return responseList;
    }

    @Override
    public List<LinkedHashMap<String, Object>> getClaimInfo(long claimNo) {
        return readMapper.selectClaimInfo(claimNo);
    }

    @Override
    public Boolean modifyClaimCouponReIssueResult(List<Long> claimAdditionNo) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimCouponReIssueResult(claimAdditionNo));
    }

    @Override
    public Boolean modifyClaimBundleSlotResult(long claimBundleNo){
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimBundleSlotResult(claimBundleNo));
    }

    @Override
    public ClaimInfoRegDto getClaimRegData(long claimNo) {
        return readMapper.selectClaimRegDataInfo(claimNo);
    }

    @Override
    public Boolean modifyClaimPaymentOcbTransactionId(long claimPaymentNo, String pgMid) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimPaymentOcbTransactionId(claimPaymentNo, pgMid));
    }

    @Override
    public Boolean modifyClaimPaymentPgCancelTradeId(long claimPaymentNo, String cancelTradeId) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimPaymentPgCancelTradeId(claimPaymentNo, cancelTradeId));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getOrderTicketInfo(long purchaseOrderNo, long orderItemNo, int cancelCnt) {
        return readMapper.selectOrderTicketInfo(purchaseOrderNo, orderItemNo, cancelCnt);
    }

    @Override
    public Boolean addClaimTicketInfo(ClaimTicketRegDto claimTicketRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimTicket(claimTicketRegDto));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getClaimTicketInfo(long claimNo) {
        return readMapper.selectClaimTicketInfo(claimNo);
    }

    @Override
    public LinkedHashMap<String, String> getClaimOrderTypeCheck(long claimNo) {
        return readMapper.selectClaimOrderTypeCheck(claimNo);
    }

    @Override
    public Boolean modifyOrderTicketInfo(ClaimTicketModifyDto modifyDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateOrderTicketStatus(modifyDto));
    }

    @Override
    public Boolean setTotalClaimPaymentStatus(long claimNo, ClaimCode claimCode){
        return SqlUtils.isGreaterThanZero(writeMapper.updateTotalClaimPaymentStatus(claimNo, claimCode));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getCreateClaimAccumulateInfo(long claimNo){
        return readMapper.selectCreateClaimAccumulateInfo(claimNo);
    }

    @Override
    public LinkedHashMap<String, Object> getAccumulateCancelInfo(long purchaseOrderNo, String pointKind){
        return readMapper.selectAccumulateCancelInfo(purchaseOrderNo, pointKind);
    }

    @Override
    public Boolean addClaimAccumulate(LinkedHashMap<String, Object> parameterMap) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimAccumulate(parameterMap));
    }

    @Override
    public Boolean modifyClaimAccumulateReqSend(long claimNo, String reqSendYn, String pointKind){
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimAccumulateReqSend(claimNo, reqSendYn, pointKind));
    }

    @Override
    public Boolean modifyClaimAccumulateReqResult(LinkedHashMap<String, Object> parameterMap) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimAccumulate(parameterMap));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getOrderCombineClaimCheck(String purchaseOrderNo, String type) {
        return readMapper.selectOriginAndCombineClaimCheck(Long.parseLong(purchaseOrderNo), type);
    }

    @Override
    public ClaimPreRefundCalcSetDto getPreRefundReCheckData(long claimNo) {
        return readMapper.selectPreRefundRecheckData(claimNo);
    }

    @Override
    public List<ClaimPreRefundItemList> getPreRefundItemData(long claimNo) {
        return readMapper.selectPreRefundItemData(claimNo);
    }

    @Override
    public ClaimMstRegDto getPreRefundCompareData(long claimNo) {
        return readMapper.selectPreRefundCompareData(claimNo);
    }

    @Override
    public ClaimPreRefundCalcGetDto getClaimPreRefundReCheckCalculation(ClaimPreRefundCalcSetDto calcSetDto) throws Exception {
        return readMapper.callByClaimPreRefundReCheckCalculation(this.createPreRefundMap(calcSetDto, Boolean.TRUE));
    }

    @Override
    public boolean modifyClaimMstPaymentAmt(long claimNo, String column, long amt) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimMstPaymentAmt(claimNo, column, amt));
    }

    @Override
    public boolean modifyClaimPaymentAmt(long claimNo, String paymentType, long paymentAmt) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimPaymentAmt(claimNo, paymentType, paymentAmt));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getPresentAutoCancelList(String processDt) {
        return readMapper.selectPresentAutoCancelInfo(processDt);
    }

    @Override
    public boolean modifyPresentAutoCancelResult(long purchaseOrderNo) {
        return SqlUtils.isGreaterThanZero(writeMapper.updatePresentAutoCancel(purchaseOrderNo));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getClaimApprovalList(long claimNo) {
        return readMapper.selectClaimApprovalData(claimNo);
    }

    @Override
    public LinkedHashMap<String, String> getClaimTdBundleInfo(long claimNo) {
        return readMapper.selectClaimBundleTdInfo(claimNo);
    }

    @Override
    public LinkedHashMap<String, Object> getMileagePaybackCheck(long bundleNo) {
        return readMapper.selectMileagePaybackCheck(bundleNo);
    }

    @Override
    public LinkedHashMap<String, Object> getOrderCombinePaybackCheck(long purchaseOrderNo) {
        return readMapper.selectMileagePaybackInfo(purchaseOrderNo);
    }

    @Override
    public LinkedHashMap<String, Object> getClaimCombinePaybackCheck(long bundleNo, long orgBundleNo) {
        return readMapper.selectMileagePaybackCancelInfo(bundleNo, orgBundleNo);
    }

    @Override
    public boolean setShipPaybackResult(long paybackNo) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateMileagePaybackResult(paybackNo));
    }

    private LinkedHashMap<String, Object> createPreRefundMap(ClaimPreRefundCalcSetDto calcSetDto, boolean isMulti){
        // 파라미터 용 맵 선언.
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        // 주문번호
        parameterMap.put("purchaseOrderNo", calcSetDto.getPurchaseOrderNo());
        // 상품주문번호
        parameterMap.put("orderItemNo", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderItemNo).collect(Collectors.joining(":")));
        // 상품번호
        parameterMap.put("itemNo", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getItemNo).collect(Collectors.joining(":")));
        // 옵션번호
        parameterMap.put("orderOptNo", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getOrderOptNo).collect(Collectors.joining(":")));
        // 클레임 수량
        parameterMap.put("claimQty", calcSetDto.getClaimItemList().stream().map(ClaimPreRefundItemList::getClaimQty).collect(Collectors.joining(":")));
        // 귀책사유
        parameterMap.put("whoReason", calcSetDto.getWhoReason());
        // 취소타입
        parameterMap.put("cancelType", calcSetDto.getCancelType());
        //단건 클레임만 결품 여부 값 추가
        if(!isMulti){
            //결품여부
            parameterMap.put("piOosYn", "N");
        }
        //행사미차감 여부
        parameterMap.put("piDeductPromoYn", calcSetDto.getPiDeductPromoYn());
        //할인 미차감 여부
        parameterMap.put("piDeductDiscountYn", calcSetDto.getPiDeductDiscountYn());
        //배송비 미차감 여부
        parameterMap.put("piDeductShipYn", calcSetDto.getPiDeductShipYn());
        // 동봉 여부
        parameterMap.put("encloseYn", calcSetDto.getEncloseYn());

        return parameterMap;
    }

    @Override
    public List<OmniOriginOrderDto> getOmniOriginOrderList(long purchaseOrderNo) {
        return readMapper.selectOriginOrderListByOmni(purchaseOrderNo);
    }

    @Override
    public List<Long> getClaimPartnerApprovalList() {
        return readMapper.selectClaimPartnerApproval();
    }

    @Override
    public LinkedHashMap<String, Object> getPaymentCancelManualInfo(long claimNo, String paymentType) {
        return writeMapper.selectPaymentCancelManualInfo(claimNo, paymentType);
    }

    @Override
    public List<RestoreStockItemDto> getRestoreStockItemInfo(long claimNo) {
        return readMapper.selectRestoreStockItemInfo(claimNo);
    }

    @Override
    public List<RestoreStoreItemListDto> getRestoreStockItemList(long claimBundleNo) {
        return readMapper.selectRestoreStoreItemList(claimBundleNo);
    }

    @Override
    public List<Long> getClaimMinusRefundClaimList(long claimNo) {
        return readMapper.selectClaimMinusRefundTarget(claimNo);
    }

    @Override
    public Long getClaimMinusRefundModifyAmt(long claimNo) {
        return readMapper.selectMinusRefundCouponCorrect(claimNo);
    }

    @Override
    public boolean setClaimMinusDiscountModify(long claimNo, long correctCouponAmt, long totDiscountAmt) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateMinusRefundCouponCorrect(claimNo, correctCouponAmt, totDiscountAmt));
    }

    @Override
    public LinkedHashMap<String, Object> getClaimMinusRefundPreInfo(long claimNo) {
        return readMapper.selectClaimMinusRefundPreInfo(claimNo);
    }

    @Override
    public LinkedHashMap<String, Object> getClaimPickIsAutoInfo(long purchaseOrderNo, long bundleNo) {
        return readMapper.selectClaimPickIsAutoInfo(purchaseOrderNo, bundleNo);
    }

    @Override
    public Boolean addClaimPickShippingInfo(ClaimShippingRegDto shippingRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimPickShipping(shippingRegDto));
    }

    @Override
    public Boolean addClaimExchShipping(ClaimExchShippingRegDto exchShippingRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimExchShipping(exchShippingRegDto));
    }

    @Override
    public ClaimSafetySetDto getClaimSafetyData(long claimBundleNo, String issueType) {
        return writeMapper.selectClaimSafetyIssueData(claimBundleNo, issueType);
    }

    @Override
    public boolean marketClaimAmountModify(long claimNo, long paymentAmt, long claimDeliveryFeeDemandAmount, long shipAmt, long promoDiscountAmt) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateMarketClaimAmount(claimNo, paymentAmt, claimDeliveryFeeDemandAmount, shipAmt, promoDiscountAmt));
    }

    @Override
    public LinkedHashMap<String, Object> getMarketClaimAmountInfo(long claimNo) {
        return writeMapper.selectMarketClaimAmountInfoByModify(claimNo);
    }

    @Override
    public boolean addClaimAdditionByMarketShipAmt(ClaimAdditionRegDto claimAdditionRegDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertClaimAdditionByMarketShipAmt(claimAdditionRegDto));
    }

    @Override
    public boolean marketClaimAddShipAmtModify(long claimNo, long diffAmt) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateClaimAdditionByMarketShipAmt(claimNo, diffAmt));
    }

    @Override
    public int isClaimAdditionByAddShip(long claimNo) {
        return writeMapper.selectMarketClaimAdditionCheckByAddShip(claimNo);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getClaimAdditionAddDataByMarketAddShip(long claimNo) {
        return writeMapper.selectClaimAdditionAddDataByMarketClaim(claimNo);
    }

    @Override
    public LinkedHashMap<String, Object> getMarketClaimPrevShipAmt(long purchaseOrderNo) {
        return writeMapper.selectPrevMarketClaimShipAmt(purchaseOrderNo);
    }

    @Override
    public LinkedHashMap<String, Object> getOrderDiscountMarketPrice(String marketOrderNo) {
        return writeMapper.selectOrderDiscountMarketSumPrice(marketOrderNo);
    }

    @Override
    public LinkedHashMap<String, Object> getPrevMarketClaimDiscount(String marketOrderNo, long claimNo) {
        return writeMapper.selectPrevMarketClaimDiscount(marketOrderNo, claimNo);
    }

    @Override
    public boolean marketClaimBundleEncloseModify(long claimNo) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateMarketClaimBundleIsEnclose(claimNo));
    }
}
