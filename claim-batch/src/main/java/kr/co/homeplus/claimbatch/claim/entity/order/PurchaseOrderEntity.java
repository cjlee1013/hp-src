package kr.co.homeplus.claimbatch.claim.entity.order;

public class PurchaseOrderEntity {
    // 구매주문번호
    public String purchaseOrderNo;
    // 결제번호
    public String paymentNo;
    // 원구매주문번호(합배송주문시 원구매주문번호)
    public String orgPurchaseOrderNo;
    // 장바구니통로번호
    public String cartRouteNo;
    // 고객번호(비회원주문시 신규생성)
    public String userNo;
    // (ORD_NOR:일반주문 ,ORD_COMBINE:합주문 ,ORD_SUB:대체주문,ORD_MULTI:다중배송지,ORD_CALL:상담주문)
    public String orderType;
    // 주문일자(주문데이터 추출 기준 Key값)
    public String orderDt;
    // 사이트유형(HOME:홈플러스,CLUB:더클럽)
    public String siteType;
    // 예약여부(예약주문여부 Y:예약,N:일반)
    public String reserveYn;
    // DS선물자동취소일자
    public String dsGiftAutoCancelDt;
    // DS선물인증번호
    public String dsGiftCertifyNo;
    // 총상품금액
    public String totItemPrice;
    // 총배송가격(총 배송비 가격:고객지불금액)
    public String totShipPrice;
    // 총추가배송가격(총 도서산간 추가 배송비:고객지불금액)
    public String totIslandShipPrice;
    // 총상품할인금액(제휴즉시,상품즉시,상품쿠폰)
    public String totItemDiscountAmt;
    // 총배송비할인금액(배송비즉시,배송비쿠폰)
    public String totShipDiscountAmt;
    // 총프로모션할인금액
    public String totPromoDiscountAmt;
    // 총임직원할인금액
    public String totEmpDiscountAmt;
    // 총카드사할인금액(총 카드사즉시할인 금액)
    public String totCardDiscountAmt;
    // 총장바구니할인금액(장바구니쿠폰,카드사즉시할인)
    public String totCartDiscountAmt;
    // PG결제금액
    public String pgAmt;
    // 마일리지결제금액
    public String mileageAmt;
    // MHC결제금액
    public String mhcAmt;
    // DGV결제금액
    public String dgvAmt;
    // OCB결제금액
    public String ocbAmt;
    // 주문자명
    public String buyerNm;
    // 주문자모바일번호
    public String buyerMobileNo;
    // 주문자이메일
    public String buyerEmail;
    // 비회원주문여부(Y:비회원,N:회원)
    public String nomemOrderYn;
    // 플랫폼(PC,APP,MWEB)
    public String platform;
    // OS유형(모바일기기OS:ANDROID,IOS)
    public String osType;
    // 제휴코드
    public String affiliateCd;
    // 제휴수수료율
    public String affiliateCommissionRate;
    // 새벽배송 배송완료메세지알림 (MORNING:오전8시이후, DIRECT:배송완료후바로)
    public String auroraEarlyNotice;
    // 요청IP
    public String reqIp;
    // 임직원번호
    public String empNo;
    // 임직원명
    public String empNm;
    // 임직원종류(00:임직원)
    public String empKind;
    // 임직원할인남은금액
    public String empRemainAmt;
    // 배송완료여부(주문 배송완료여부 Y:완료,N:진행중)
    public String shipFshYn;
    // 배송완료일시(주문 배송완료일시)
    public String shipFshDt;
    // 합배송여부(원주문에 합배송이 있는지 여부 Y:합배송존재, N:없음))
    public String cmbnYn;
    // 대체주문여부(원주문에 대체주문이 있는지 여부 Y:대체주문존재, N:없음)
    public String substitutionOrderYn;
    // 마일리지적립금액
    public String mileageAccAmt;
    // mhc적립급액
    public String mhcAccAmt;
    // 등록일시
    public String regDt;
    // 등록자ID
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정자ID
    public String chgId;
    // 옴니인터페이스여부(S:초기값, Q:영수증발급대상오류, N:OMNI전송대기, I:OMNI처리중, Y:OMNI처리완료, E:OMNI전송에러, X:OMNI전송제외)
    public String omniIfYn;
    // 옴니인터페이스일시
    public String omniIfDt;
    // PINE인터페이스여부(S:초기값, C:영수증발급대상, Q:영수증발급대상오류, N:PINE전송대기, I:PINE처리중, Y:PINE처리완료, E:PINE전송에러, P:PINE전송제외)
    public String pineIfYn;
    // PINE인터페이스일시
    public String pineIfDt;
    // 주문선물여부
    public String orderGiftYn;
    // OCB 적립 카드번호 (ocb 제휴 진입시)
    public String ocbSaveCardNo;
    // 제휴채널 extraCd
    public String affiliateExtraCd;
    // 선물자동취소여부
    public String dsGiftAutoCancelYn;
    // 사이트유형확장(HOME:홈플러스,CLUB:더클럽,EXP:EXPRESS)
    public String siteTypeExtend;
    // 회원등급번호
    public String gradeSeq;
    // 총추가중복TD할인금액
    public String totAddTdDiscountAmt;
    // 총추가중복DS할인금액
    public String totAddDsDiscountAmt;
    // 마켓유형
    public String marketType;
    // 마켓연동주문번호
    public String marketOrderNo;
    // 배송완료알림여부(Y:오전7시이후,N:배송완료후)
    public String deliveryCompleteAlarmYn;
}
