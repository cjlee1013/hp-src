package kr.co.homeplus.claimbatch.claim.entity.claim;

public class ClaimInfoEntity {
    // 클레임번호(환불:클레임번호)
    public String claimNo;
    // 상품주문번호(결제완료:주문번호)
    public String orderItemNo;
    // 상품번호 또는 배송정책번호
    public String itemNo;
    // 사이트유형(HOME:홈플러스,CLUB:더클럽)
    public String siteType;
    // 결제번호
    public String paymentNo;
    // 배송번호
    public String bundleNo;
    // 다중주문 배송번호
    public String multiBundleNo;
    // 다중 주문 아이템 번호
    public String multiOrderItemNo;
    // 클레임배송번호
    public String claimBundleNo;
    // 파트너아이디
    public String partnerId;
    // 원천점포ID(매출용실제점포)
    public String originStoreId;
    // 점포ID(주문서에서 넘어온 점포ID)
    public String storeId;
    // FC 점포ID
    public String fcStoreId;
    // FC상품여부
    public String fcItemYn;
    // 주문번호
    public String purchaseOrderNo;
    // 파트너명(업체명)
    public String partnerNm;
    // 상점유형(HYPER, CLUB, EXP, DS, AURORA)
    public String storeType;
    // 몰유형(DS:업체상품,TD:매장상품)
    public String mallType;
    // 과세여부(Y:과세,N:면세)
    public String taxYn;
    // 상품판매가
    public String itemPrice;
    // 완료수량
    public String completeQty;
    // 완료금액
    public String completeAmt;
    // 공급자(TD상품 납품업체)
    public String supplier;
    // MD사번(주문상품담당MD사번)
    public String mdEmpNo;
    // 홈플러스 부담 쿠폰 할인금액
    public String couponHomeChargeAmt;
    // 판매자 부담 쿠폰 할인금액
    public String couponSellerChargeAmt;
    // 카드 부담 쿠폰 할인금액(광고선전비)
    public String couponCardChargeAmt;
    // 홈플러스 부담 장바구니 할인금액
    public String cartCouponHomeAmt;
    // 판매자 부담 장바구니 할인금액
    public String cartCouponSellerAmt;
    // 카드 부담 장바구니 할인금액
    public String cartCouponCardAmt;
    // 홈플러스 부담 카드즉시할인 금액
    public String cardCouponHomeAmt;
    // 카드사 부담 카드 즉시할인 금액
    public String cardCouponCardAmt;
    // 프로모션 할인금액
    public String promoDiscountAmt;
    // 임직원 할인금액
    public String empDiscountAmt;
    // 배송비
    public String shipAmt;
    // 조건부 무료배송비
    public String condShipAmt;
    // 배송비 할인금액
    public String shipDiscountAmt;
    // 클레임 취소배송비 + 도서산간
    public String claimCancelShipAmt;
    // 클레임 추가배송비 + 도서산간
    public String claimAddShipAmt;
    // 클레임 반품배송비 + 도서산간
    public String claimReturnShipAmt;
    // 결제금액(상품+배송비)-각종할인
    public String claimPrice;
    // 세금부가세금액(주문금액-상품별할인금액 기준)  ROUND((주문-상품별할인금액)*(10/110),0)
    public String taxVatAmt;
    // PG 환불금액(완료금액-할인금액(쿠폰,장바구니,카드사)-마일리지금액-MHC-DGV-OCB)
    public String pgAmt;
    // 포인트 환불금액
    public String mileageAmt;
    // MHC 환불금액
    public String mhcAmt;
    // DGV 환불금액
    public String dgvAmt;
    // OCB 환불금액
    public String ocbAmt;
    // PG결제 전체 금액(주문/클레임 별 총 금액)
    public String realPgAmt;
    // 포인트 결제 전체 금액(클레임 별 포인트 금액(payment_type=PT))
    public String realMileageAmt;
    // MHC 결제 전체 금액
    public String realMhcAmt;
    // DGV 결제 전체 금액
    public String realDgvAmt;
    // OCB 결제 전체 금액
    public String realOcbAmt;
    // 클레임시 포인트 전환금액(payment_type이 PP인 케이스)
    public String claimReturnMileage;
    // PG 승인 Key 값
    public String pgTid;
    // 신용카드수수료율
    public String creditCommissionRate;
    // 체크카드수수료율
    public String debitCommissionRate;
    // 수수료금액
    public String commissionAmt;
    // 고객번호
    public String userNo;
    // 결제방법(CARD:카드, VBANK:무통장입금, PHONE:휴대폰결제, RBANK: 실시간계좌이체, EASY: 간편결제)
    public String paymentTool;
    // 결제수단(W002:산업은행,W003:기업은행...escrow.payment_method참고)
    public String paymethodCd;
    // PG사(LUG,KCP,KICC,INICIS,NICE)
    public String pgKind;
    // 결제완료일시/환불완료시간
    public String paymentCompleteDt;
    // 장바구니 할인 타입(1:정액, 2:정률)
    public String discountType;
    // 장바구니 할인 요율(discount_type이 정률인경우만 사용)
    public String cartDiscountRate;
    // 장바구니 최대 금액(장바구니쿠폰 할인 가능한 최대금액)
    public String cartDiscountMax;
    // 장바구니 구매 최소 금액(장바구니 쿠폰이 사용 가능한 구매 최소 금액(해당금액 이상만 장바구니 쿠폰 사용))
    public String cartPurchaseMin;
    // 장바구니 할인 홈플러스 부담율
    public String cartCouponHomeRate;
    // 장바구니 할인 판매자 부담율
    public String cartCouponSellerRate;
    // 장바구니 할인 카드사 부담율
    public String cartCouponCardRate;
    // 마일리지 사용여부(Y:사용,N:미사용)
    public String mileageYn;
    // 장바구니 사용여부(Y:사용,N:미사용)
    public String cartYn;
    // 결제건 주문건수(금액배분을 위해 필요함)
    public String oppCnt;
    // 환불수단(PG:PG취소, BK:계좌입금,PP:포인트 지급, PT : 포인트 사용취소)
    public String paymentType;
    // 홈플러스 부담 상품쿠폰 요율
    public String couponHomeChargeRate;
    // 판매자 부담 상품쿠폰 요율
    public String couponSellerChargeRate;
    // 카드사 부담 상품쿠폰 요율
    public String couponCardChargeRate;
    // 클레임 발생한 장바구니 쿠폰금액
    public String claimCartCouponAmt;
    // 등록일시
    public String regDt;
    // 등록자
    public String regId;
    // 수정일시
    public String chgDt;
    // 수정자
    public String chgId;
    // 추가중복TD할인금액(쿠폰분할금액)
    public String addTdCouponAmt;
    // 추가중복DS할인금액(쿠폰분할금액)
    public String addDsCouponAmt;
}
