package kr.co.homeplus.claimbatch.claim.model.shipping;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@ApiModel(description = "교환/반품시 배송완료 처리 DTO")
public class ClaimShipCompleteDto {
    @ApiModelProperty(value = "배송번호", position = 1)
    private String bundleNo;
    @ApiModelProperty(value = "변경자ID", position = 2)
    private String chgId;
}
