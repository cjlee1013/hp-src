package kr.co.homeplus.claimbatch.monitoring.model;

import lombok.Data;

@Data
public class ClaimPaymentMonitoringDto {
    private String sendTime;
    private int totalCnt;
    private int pgFailCnt;
    private int pgFailAmt;
    private int mhcFailCnt;
    private int mhcFailAmt;
    private int mileageFailCnt;
    private int mileageFailAmt;
    private int ocbFailCnt;
    private int ocbFailAmt;
    private int dgvFailCnt;
    private int dgvFailAmt;

    public void setTotalCntCall(){
        this.totalCnt = (this.pgFailCnt + this.mhcFailCnt + this.mileageFailCnt + this.ocbFailCnt + this.dgvFailCnt);
    }
}
