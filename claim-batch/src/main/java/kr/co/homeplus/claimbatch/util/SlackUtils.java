package kr.co.homeplus.claimbatch.util;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import kr.co.homeplus.claimbatch.message.model.SlackMessageDto;
import kr.co.homeplus.claimbatch.message.model.SlackMessageDto.SlackMessageMarkDownDto;
import kr.co.homeplus.claimbatch.message.model.SlackMessageDto.SlackMessageSectionDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class SlackUtils {

    private final ResourceClient resourceClient;
    private static ResourceClient messageClient;

    @PostConstruct
    private void initialize() {
        messageClient = resourceClient;
    }

    public static void sendingForMarketClaimError(String message) {
        List<SlackMessageSectionDto> sectionList = new ArrayList<>();
        sectionList.add(createSlackMessageSectionDto(createSlackMessageMrkDwn(message)));
        sendToSlack(createSlackMessageDto(sectionList));
    }

    public static void sendingForMarketClaimErrors(List<String> messages) {
        List<SlackMessageSectionDto> sectionList = new ArrayList<>();
        for(String message : messages) {
            sectionList.add(createSlackMessageSectionDto(createSlackMessageMrkDwn(message)));
        }
        sendToSlack(createSlackMessageDto(sectionList));
    }

    public static SlackMessageDto createSlackMessageDto(List<SlackMessageSectionDto> sectionDto) {
        SlackMessageDto messageDto = new SlackMessageDto();
        messageDto.setBlocks(sectionDto);
        return messageDto;
    }

    public static SlackMessageSectionDto createSlackMessageSectionDto(SlackMessageMarkDownDto text) {
        SlackMessageSectionDto sectionDto = new SlackMessageSectionDto();
        sectionDto.setType("section");
        sectionDto.setText(text);
        return sectionDto;
    }

    public static SlackMessageMarkDownDto createSlackMessageMrkDwn(String text) {
        String serverProfile = System.getProperty("spring.profiles.active");
        SlackMessageMarkDownDto markDownDto = new SlackMessageMarkDownDto();
        markDownDto.setType("mrkdwn");
        markDownDto.setText("(".concat(setMarkdownBold(serverProfile.toUpperCase(Locale.KOREA))).concat(")\n".concat(text)));
        return markDownDto;
    }

    public static String setMarkdownBold(String text) {
        return MessageFormat.format("*{0}*", text);
    }

    public static String setMarkdownLink(String text) {
        return MessageFormat.format("<{0}>", text);
    }

    public static void sendToSlack(SlackMessageDto slackMessageDto) {
        try{
            messageClient.postForResponseEntity("claimslack", slackMessageDto, getSlackUrl(), String.class);
        } catch (Exception e) {
            log.error("Slack Send Message Error ::: {}", e.getMessage());
        }
    }

    private static String getSlackUrl() {
        String serverProfile = System.getProperty("spring.profiles.active");
        switch (serverProfile.toUpperCase(Locale.KOREA)) {
            case "QA" :
                return "/services/TNK6E5RPH/B01P14A1XNK/rLqTgxvDfGU7sg6NL9sUTiZp";
            case "PRD" :
                return "/services/TNK6E5RPH/B0300FN5466/b6DfoQem3duRzAxtXPitblGx";
            default:
                return "/services/TNK6E5RPH/B01P145KB2B/IJBj4PNikfW7F6b90GxznoAF";
        }
    }
}
