package kr.co.homeplus.claimbatch.claim.maket.service.impl;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claimbatch.claim.maket.mapper.MarketReadMapper;
import kr.co.homeplus.claimbatch.claim.maket.mapper.MarketWriteMapper;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimItemDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimInfoDto;
import kr.co.homeplus.claimbatch.claim.maket.service.MarketMapperService;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimRegDetailDto;
import kr.co.homeplus.claimbatch.util.SqlUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MarketMapperServiceImpl implements MarketMapperService {

    private final MarketWriteMapper writeMapper;
    private final MarketReadMapper readMapper;

    @Override
    public boolean addMarketClaim(MarketClaimDto marketClaimDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertMarketClaim(marketClaimDto));
    }

    @Override
    public boolean addMarketClaimItem(MarketClaimItemDto marketClaimItemDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertMarketClaimItem(marketClaimItemDto));
    }

    @Override
    public boolean addMarketClaimInfo(MarketClaimInfoDto marketClaimInfoDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertMarketClaimInfo(marketClaimInfoDto));
    }

    @Override
    public boolean addMarketClaimItemCheck(MarketClaimItemDto marketClaimItemDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.insertMarketClaimItemCheck(marketClaimItemDto));
    }

    @Override
    public LinkedHashMap<String, Object> getMarketOrderItemInfo(String marketOrderItemNo) {
        return readMapper.selectMarketOrderItemCheck(marketOrderItemNo);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getCreateMarketClaimData(String marketOrderItemNo) {
        return readMapper.selectCreateMarketClaimData(marketOrderItemNo);
    }

    @Override
    public boolean setMarketClaimProcessResultSave(long marketClaimNo, String rcvYn) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateMarketClaimItemStatus(marketClaimNo, rcvYn));
    }

    @Override
    public boolean setMarketClaimAfterProcessClaimNoSave(long marketClaimNo, long claimNo) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateMarketClaimItemByClaimNo(marketClaimNo, claimNo));
    }

    @Override
    public LinkedHashMap<String, Object> getClaimReasonType(String claimReasonType) {
        return readMapper.selectClaimReasonType(claimReasonType);
    }

    @Override
    public ClaimRegDetailDto getClaimRegDetailDtoInfo(long marketClaimNo, String marketOrderItemNo) {
        return readMapper.selectClaimExchangeAndReturnInfo(marketClaimNo, marketOrderItemNo);
    }

    @Override
    public boolean getMarketClaimPrevCheck(String marketClaimId) {
        return SqlUtils.isGreaterThanZero(readMapper.selectPrevInsertDataCheck(marketClaimId));
    }

    @Override
    public boolean modifyPrevDataStatus(String marketClaimId, MarketClaimItemDto marketClaimItemDto, MarketClaimInfoDto marketClaimInfoDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.updatePrevDataStatus(marketClaimId, marketClaimItemDto, marketClaimInfoDto));
    }

    @Override
    public boolean modifyMarketClaimAmount(String marketClaimId, MarketClaimDto marketClaimDto) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateMarketClaimAmountByRetry(marketClaimId, marketClaimDto));
    }

    @Override
    public boolean modifyHoldbackStatus(String marketOrderItemNo, String holdbackStatus) {
        return SqlUtils.isGreaterThanZero(writeMapper.updateHoldbackStatus(marketOrderItemNo, holdbackStatus));
    }

    @Override
    public LinkedHashMap<String, Object> getExistingClaimByMarket(long marketClaimNo) {
        return readMapper.selectExistingClaimCheck(marketClaimNo);
    }

    @Override
    public List<LinkedHashMap<String, Object>> getMarketClaimList(long marketClaimNo) {
        return writeMapper.selectMarketClaimList(marketClaimNo);
    }

    @Override
    public int getMarketOrderItemCnt(String marketOrderNo) {
        return writeMapper.selectMarketOrderItemCnt(marketOrderNo);
    }

    @Override
    public int getMarketClaimItemCnt(String marketOrderNo) {
        return writeMapper.selectMarketClaimItemCnt(marketOrderNo);
    }

    @Override
    public boolean isMarketClaimAnotherStatusCheck(long marketClaimNo, String claimStatus) {
        return SqlUtils.isGreaterThanZero(writeMapper.selectMarketClaimStatusCheck(marketClaimNo, claimStatus));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getMarketClaimingDataCnt(String marketOrderNo) {
        return writeMapper.selectMarketClaimingDataCheck(marketOrderNo);
    }

    @Override
    public int getMarketClaimRequestCnt(long marketClaimNo) {
        return readMapper.selectMarketClaimItemReqCnt(marketClaimNo);
    }

    @Override
    public int getPrevClaimItemCntByMarket(long marketClaimNo) {
        return readMapper.selectPrevClaimItemCntByMarket(marketClaimNo);
    }
}
