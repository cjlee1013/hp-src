package kr.co.homeplus.claimbatch.claim.sql;

import kr.co.homeplus.claimbatch.claim.constants.CustomConstants;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimAdditionEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimMstEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimPaymentEntity;
import kr.co.homeplus.claimbatch.claim.entity.dms.ItmMngCodeEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderDiscountEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderPaymentDivideEntity;
import kr.co.homeplus.claimbatch.claim.enums.MysqlFunction;
import kr.co.homeplus.claimbatch.claim.factory.EntityFactory;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import kr.co.homeplus.claimbatch.util.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ClaimMinusRefundSql {

    public static String selectClaimMinusRefundTarget(@Param("claimNo") long claimNo) {
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        OrderDiscountEntity orderDiscount = EntityFactory.createEntityIntoValue(OrderDiscountEntity.class, Boolean.TRUE);
        ItmMngCodeEntity itmMngCode = EntityFactory.createEntityIntoValue(ItmMngCodeEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                claimPayment.claimNo,
                SqlUtils.sqlFunction(MysqlFunction.SUM, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(claimMst.mhcAmt, claimMst.mileageAmt, claimMst.ocbAmt, claimMst.dgvAmt)), "sum")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimPayment))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimPayment.claimNo)))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(claimMst.claimNo, claimMst.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                        SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C2")
                    )
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    orderDiscount,
                    ObjectUtils.toArray(claimBundle.purchaseOrderNo, claimBundle.bundleNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(orderDiscount.discountKind, "cart_coupon")
                    )
                )
            )
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmMngCode),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(itmMngCode.gmcCd, "coupon_ minus_refund"),
                        SqlUtils.equalColumn(orderDiscount.couponNo, itmMngCode.mcNm)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.paymentStatus, "P4"),
                SqlUtils.equalColumnByInput(claimPayment.paymentType, "PG")
            )
            .GROUP_BY(claimMst.claimNo);

        if (claimNo != 0) {
            innerQuery.WHERE(
                SqlUtils.equalColumnByInput(claimPayment.claimNo, claimNo)
            );
        } else {
            innerQuery.WHERE(
                SqlUtils.sqlFunction(
                    MysqlFunction.BETWEEN,
                    ObjectUtils.toArray(
                        claimPayment.chgDt,
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATEADD_HOUR, ObjectUtils.toArray("NOW()", "-1")), "%Y-%m-%d %H:00:00")),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray("NOW()", "%Y-%m-%d %H:00:00"))
                    )
                )
            );
        }
        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(claimMst.claimNo)
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "Z"))
            .WHERE(
                SqlUtils.equalColumnByInput("sum", 0)
            );
        log.debug("selectClaimMinusRefundTarget Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectMinusRefundCouponCorrect(@Param("claimNo") long claimNo) {
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(
                    MysqlFunction.CONCAT,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(
                            MysqlFunction.LEFT,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(MysqlFunction.FLOOR,
                                    SqlUtils.sqlFunction(MysqlFunction.SUM,
                                        SqlUtils.customOperator(
                                            CustomConstants.MULTIPLY, orderPaymentDivide.cartCouponHomeAmt, SqlUtils.customOperator(CustomConstants.DIVISION, ObjectUtils.toArray(claimItem.claimItemQty, orderPaymentDivide.itemQty)
                                            )
                                        )
                                    )
                                ),
                                SqlUtils.customOperator(
                                    CustomConstants.MINUS,
                                    SqlUtils.sqlFunction(MysqlFunction.LENGTH,
                                        SqlUtils.sqlFunction(MysqlFunction.FLOOR,
                                            SqlUtils.sqlFunction(MysqlFunction.SUM,
                                                SqlUtils.customOperator(
                                                    CustomConstants.MULTIPLY, orderPaymentDivide.cartCouponHomeAmt, SqlUtils.customOperator(CustomConstants.DIVISION, ObjectUtils.toArray(claimItem.claimItemQty, orderPaymentDivide.itemQty)
                                                    )
                                                )
                                            )
                                        )
                                    ),
                                    "1"
                                )
                            )
                        ),
                        "0"
                    )
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimItem))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderPaymentDivide, ObjectUtils.toArray(claimItem.purchaseOrderNo, claimItem.bundleNo, claimItem.orderItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimItem.claimNo, claimNo)
            );
        log.debug("selectMinusRefundCouponCorrect Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String updateMinusRefundCouponCorrect(@Param("claimNo") long claimNo, @Param("correctCouponAmt") long correctCouponAmt, @Param("totDiscountAmt") long totDiscountAmt) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    claimPayment,
                    ObjectUtils.toArray(claimMst.claimNo),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(claimPayment.paymentType, "PG"),
                        SqlUtils.equalColumnByInput(claimPayment.paymentStatus, "P4")
                    )
                )
            )
            .SET(
                SqlUtils.equalColumnByInput(claimMst.totCartDiscountAmt, correctCouponAmt),
                SqlUtils.equalColumn(claimMst.totDiscountAmt, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(totDiscountAmt, correctCouponAmt))),
                SqlUtils.equalColumn(claimMst.completeAmt, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(claimMst.totItemPrice, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(totDiscountAmt, correctCouponAmt))))),
                SqlUtils.equalColumn(claimMst.pgAmt, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(claimMst.totItemPrice, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(totDiscountAmt, correctCouponAmt))))),
                SqlUtils.equalColumn(claimPayment.paymentAmt, SqlUtils.customOperator(CustomConstants.MINUS, ObjectUtils.toArray(claimMst.totItemPrice, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(totDiscountAmt, correctCouponAmt)))))
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimMst.mileageAmt, 0),
                SqlUtils.equalColumnByInput(claimMst.mhcAmt, 0),
                SqlUtils.equalColumnByInput(claimMst.ocbAmt, 0),
                SqlUtils.equalColumnByInput(claimMst.dgvAmt, 0)
            );
        log.info("updateMinusRefundCouponCorrect Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimMinusRefundPreInfo(@Param("claimNo") long claimNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimMst.claimNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totItemPrice, claimMst.totItemPrice),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totDiscountAmt, claimMst.totDiscountAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.userNo, claimMst.userNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimItem.storeType, claimItem.storeType)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimMst.claimNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            )
            .GROUP_BY(claimMst.claimNo)
            ;
        log.debug("selectClaimMinusRefundPreInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }
}
