package kr.co.homeplus.claimbatch.claim.maket.naver.constants;

import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NaverProductOrderChangeType {
//    public static final String PAY_WAITING = "PAY_WAITING";
//    public static final String PAYED = "PAYED";
//    public static final String DISPATCHED = "DISPATCHED";
    CANCEL_REQUESTED("CANCEL_REQUESTED"),
    RETURN_REQUESTED("RETURN_REQUESTED"),
    EXCHANGE_REQUESTED("EXCHANGE_REQUESTED"),
    EXCHANGE_REDELIVERY_READY("EXCHANGE_REDELIVERY_READY"),
    HOLDBACK_REQUESTED("HOLDBACK_REQUESTED"),
    CANCELED("CANCELED"),
    RETURNED("RETURNED"),
    EXCHANGED("EXCHANGED"),
    ;
//    public static final String PURCHASE_DECIDED = "PURCHASE_DECIDED";
//    public static final String EXCHANGE_REDELIVERY_READY = "EXCHANGE_REDELIVERY_READY";

    private String requestParam;

    public static NaverProductOrderChangeType getNaverProductOrderChangeType(String claimType) {
        return Stream.of(NaverProductOrderChangeType.values()).filter(e -> e.name().equals(claimType)).iterator().next();
    }
}
