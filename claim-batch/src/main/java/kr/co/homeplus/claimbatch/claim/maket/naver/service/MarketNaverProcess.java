package kr.co.homeplus.claimbatch.claim.maket.naver.service;

import java.util.Arrays;
import java.util.LinkedHashMap;
import kr.co.homeplus.claimbatch.claim.maket.naver.constants.PickupLocationType;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.ClaimOrderDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.NaverCancelInfo;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.NaverExchangeInfoDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.NaverProductOrderDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.NaverReturnInfoDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimItemDto;
import kr.co.homeplus.claimbatch.claim.maket.naver.model.mapping.MarketClaimInfoDto;
import kr.co.homeplus.claimbatch.claim.maket.service.MarketMapperService;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MarketNaverProcess {
    private final MarketMapperService marketMapperService;

    public MarketClaimDto createMarketClaimDto(ClaimOrderDto claimOrderDto, NaverProductOrderDto productOrderList) {
        MarketClaimDto marketClaimDto = MarketClaimDto.builder()
            .marketType("NAVER")
            .marketOrderNo(claimOrderDto.getOrderId())
            .storeId(productOrderList.getBranchId())
            .buyerId(claimOrderDto.getOrdererId())
            .buyerNm(this.getStrLength(claimOrderDto.getOrdererName(), 20))
            .buyerTelNo(claimOrderDto.getOrdererTel1())
            .buyerMobileNo(claimOrderDto.getOrdererTel2())
            .buyerEmail("")       // 네이버 미제공
            .buyerZipcode("")     // 네이버 미제공
            .buyerBaseAddr("")    // 네이버 미제공
            .buyerDetailAddr("")  // 네이버 미제공
            .receiverNm(StringEscapeUtils.unescapeHtml4(productOrderList.getShippingAddress().getName()))
            .receiverTelNo(productOrderList.getShippingAddress().getTel1())
            .receiverMobileNo(productOrderList.getShippingAddress().getTel2())
            .receiverZipcode(productOrderList.getShippingAddress().getZipCode())
            .receiverBaseAddr(productOrderList.getShippingAddress().getBaseAddress())
            .receiverDetailAddr(productOrderList.getShippingAddress().getDetailedAddress())
            .shipMemo(this.getShipMemo(productOrderList))
            .entryMethodCd(productOrderList.getShippingAddress().getEntryMethod())
            .entryMethodCntnt(productOrderList.getShippingAddress().getEntryMethodContent())
            .paymentDt(claimOrderDto.getPaymentDate())
            .paymentMethod(claimOrderDto.getPaymentMeans())
            .tradeDt(claimOrderDto.getPaymentDate() != null ? claimOrderDto.getPaymentDate() : "2999-12-31 23:59:59")
            // 2021-08-11
            //marketClaimDto.ShipReqDt(productOrderList.getDeliveryOperationDate() + " " + productOrderList.getDeliveryStartTime().substring(0,2) + ":" + productOrderList.getDeliveryStartTime().substring(2,4));
            .shipReqDt(productOrderList.getDeliveryOperationDate() + " 00:00:00")
            .rcvYn("N")
            .pkgNo(productOrderList.getPackageNumber())
            .slotId(this.getMarketOrderSlotAndShiftInfo(productOrderList.getSlotId(), Boolean.TRUE))
            .shiftId(this.getMarketOrderSlotAndShiftInfo(productOrderList.getSlotId(), Boolean.FALSE))
            .slotRestrYn("N")
            .shipReqSlotStartTime(productOrderList.getDeliveryStartTime())
            .shipReqSlotEndTime(productOrderList.getDeliveryEndTime())
            .shipAmt(productOrderList.getDeliveryFeeAmount())
            .shipDiscountAmt(productOrderList.getDeliveryDiscountAmount())
            .tradeRcvAmt(claimOrderDto.getGeneralPaymentAmount() + claimOrderDto.getNaverMileagePaymentAmount() + claimOrderDto.getChargeAmountPaymentAmount() + claimOrderDto.getCheckoutAccumulationPaymentAmount() + claimOrderDto.getPayLaterPaymentAmount())
            .chgId("NAVER")
            .regId("NAVER")
            .build();


        if (StringUtils.isNotEmpty(claimOrderDto.getOrdererTel1()) && claimOrderDto.getOrdererTel1().length() >= 3) {
            if (Arrays.asList("010","011","016","017","018","019","050").contains(claimOrderDto.getOrdererTel1().substring(0,3))) {
                marketClaimDto.setBuyerMobileNo(claimOrderDto.getOrdererTel1());
                marketClaimDto.setBuyerTelNo(claimOrderDto.getOrdererTel2());
            }
        }
        if (StringUtils.isNotEmpty(productOrderList.getShippingAddress().getTel1()) && productOrderList.getShippingAddress().getTel1().length() >= 3) {
            if (Arrays.asList("010","011","016","017","018","019","050").contains(productOrderList.getShippingAddress().getTel1().substring(0,3))) {
                marketClaimDto.setReceiverMobileNo(productOrderList.getShippingAddress().getTel1());
                marketClaimDto.setReceiverTelNo(productOrderList.getShippingAddress().getTel2());
            }
        }

        if(marketClaimDto.getReceiverNm().length() > 20) {
            log.info("마켓클레임DTO ReceiverNm Error : {}", marketClaimDto.getReceiverNm());
            marketClaimDto.setReceiverNm(marketClaimDto.getReceiverNm().substring(0, 20));
        }
        
        return marketClaimDto;
    }

    public MarketClaimItemDto createMarketClaimItemDto(NaverProductOrderDto productOrderDto, LinkedHashMap<String, Object> marketOrderItemInfoMap) {
        return MarketClaimItemDto.builder()
            .marketType("NAVER")
            .storeId(productOrderDto.getBranchId())
            .marketOrderItemNo(productOrderDto.getProductOrderId())
            .itemNo(productOrderDto.getSellerProductCode())
            .itemNm(StringUtils.isNotEmpty(productOrderDto.getProductOption()) ? productOrderDto.getProductName() + "=" + productOrderDto.getProductOption() : productOrderDto.getProductName())
            .itemOpt(StringUtils.isNotEmpty(productOrderDto.getProductOption()) ? productOrderDto.getProductOption().replace("상품옵션: ", "") : "")
            .gift(productOrderDto.getFreeGift())
            .marketItemNo(productOrderDto.getProductId())
            .itemQty(productOrderDto.getQuantity() + this.isRangeCheck(productOrderDto.getBranchBenefitValue()))
            .orgItemQty(productOrderDto.getQuantity())
            .itemPrice(productOrderDto.getUnitPrice())
            .itemOptPrice(productOrderDto.getOptionPrice())
            .storePrice(productOrderDto.getUnitPrice())
            .itemDiscountAmt(productOrderDto.getProductDiscountAmount())
            .shipMemo(getShipMemo(productOrderDto))
            .entryMethodCd(productOrderDto.getShippingAddress().getEntryMethod())
            .entryMethodCntnt(productOrderDto.getShippingAddress().getEntryMethodContent())
            .shipAmt(productOrderDto.getDeliveryFeeAmount())
            .paymentAmt(productOrderDto.getTotalPaymentAmount())
            .regId("NAVER")
            .chgId("NAVER")
            .marketDiscountPrice(productOrderDto.getSellerBurdenDiscountAmount())
            .couponPrice(productOrderDto.getProductProductDiscountAmount() < 0 ? 0 : productOrderDto.getProductProductDiscountAmount())
            .discountPrice(productOrderDto.getProductDiscountAmount())
            .salePrice(productOrderDto.getTotalProductAmount())
            .substitutionYn(productOrderDto.isAcceptAlternativeProduct() ? "N" : "Y")    //대체안함 선택여부(Y:대체안함)
            .customCode1(productOrderDto.getSellerCustomCode1())
            .customCode2(productOrderDto.getSellerCustomCode2())
            .marketDiscountPrice(productOrderDto.getSellerBurdenDiscountAmount())
            .claimType(productOrderDto.getClaimType())
            .claimStatus(productOrderDto.getClaimStatus())
            .eventCouponNo(ObjectUtils.toString(marketOrderItemInfoMap.get("event_coupon_no")))
            .couponId(ObjectUtils.toString(marketOrderItemInfoMap.get("coupon_id")))
            .discountType(ObjectUtils.toString(marketOrderItemInfoMap.get("discount_type")))
            .discountVal(ObjectUtils.toLong(marketOrderItemInfoMap.get("discount_val")))
            .rcvYn(ObjectUtils.toString(marketOrderItemInfoMap.get("rcv_yn")))
            .couponDiscountAmt(ObjectUtils.toLong(marketOrderItemInfoMap.get("coupon_discount_amt")))
            .promoNo(ObjectUtils.toString(marketOrderItemInfoMap.get("promo_no")))
            .promoDetailNo(ObjectUtils.toString(marketOrderItemInfoMap.get("promo_detail_no")))
            .rpmPromoDetailNo(ObjectUtils.toLong(marketOrderItemInfoMap.get("rpm_promo_detail_no")))
            .promoType(ObjectUtils.toString(marketOrderItemInfoMap.get("promo_type")))
            .promoKind(ObjectUtils.toString(marketOrderItemInfoMap.get("promo_kind")))
            .promoDiscountType(ObjectUtils.toString(marketOrderItemInfoMap.get("promo_discount_type")))
            .promoDiscountRate(ObjectUtils.toString(marketOrderItemInfoMap.get("promo_discount_rate")))
            .promoDiscountAmt(ObjectUtils.toLong(marketOrderItemInfoMap.get("promo_discount_amt")))
            .mixMatchType(ObjectUtils.toString(marketOrderItemInfoMap.get("mix_match_type")))
            .couponPrice(ObjectUtils.toLong(marketOrderItemInfoMap.get("coupon_price")))
            .discountPrice(ObjectUtils.toLong(marketOrderItemInfoMap.get("discount_price")))
            .onePlusOne(ObjectUtils.toLong(marketOrderItemInfoMap.get("one_plus_one")))
            .marketDiscountPrice(ObjectUtils.toLong(marketOrderItemInfoMap.get("market_discount_price")))
            .marketClaimId(productOrderDto.getClaimId())
            .build();
    }

    private String getMarketOrderSlotAndShiftInfo(String slotId, boolean isSlot) {
        if (StringUtils.isEmpty(slotId)) {
            slotId = "999999999999";
        }
        if(isSlot) {
            return slotId.substring(8);
        } else {
            return slotId.substring(slotId.length()-4, slotId.length()-1);
        }
    }

    public MarketClaimInfoDto createMarketClaimInfoDtoByCancel(NaverCancelInfo cancelInfo) {
        return MarketClaimInfoDto.builder()
            .claimStatus(cancelInfo.getClaimStatus())
            .claimRequestDate(cancelInfo.getClaimRequestDate())
            .requestChannel(cancelInfo.getRequestChannel())
            .claimReason(cancelInfo.getCancelReason())
            .claimDetailReason(cancelInfo.getCancelDetailedReason())
            .cancelCompletedDate(cancelInfo.getCancelCompletedDate())
            .cancelApprovalDate(cancelInfo.getCancelApprovalDate())
            .refundExpectedDate(cancelInfo.getRefundExpectedDate())
            .refundStandbyStatus(cancelInfo.getRefundStandbyStatus())
            .refundStandbyReason(cancelInfo.getRefundStandbyReason())
            .regId("BATCH")
            .chgId("BATCH")
            .build();
    }

    public MarketClaimInfoDto createMarketClaimInfoDtoByReturn(NaverReturnInfoDto returnInfoDto) {
        return MarketClaimInfoDto.builder()
            .claimStatus(returnInfoDto.getClaimStatus())
            .claimRequestDate(returnInfoDto.getClaimRequestDate())
            .requestChannel(returnInfoDto.getRequestChannel())
            .claimReason(returnInfoDto.getReturnReason())
            .claimDetailReason(returnInfoDto.getReturnDetailedReason())
            .holdbackStatus(returnInfoDto.getHoldbackStatus())
            .holdbackReason(returnInfoDto.getHoldbackReason())
            .holdbackDetailReason(returnInfoDto.getHoldbackDetailedReason())
            .collectBaseAddress(returnInfoDto.getCollectAddress().getBaseAddress())
            .collectDetailAddress(returnInfoDto.getCollectAddress().getDetailedAddress())
            .collectZipcode(returnInfoDto.getCollectAddress().getZipCode())
            .collectName(returnInfoDto.getCollectAddress().getName())
            .collectTelNo1(returnInfoDto.getCollectAddress().getTel1())
            .collectTelNo2(returnInfoDto.getCollectAddress().getTel2())
            .returnBaseAddress(returnInfoDto.getReturnReceiveAddress().getBaseAddress())
            .returnDetailAddress(returnInfoDto.getReturnReceiveAddress().getDetailedAddress())
            .returnZipcode(returnInfoDto.getReturnReceiveAddress().getZipCode())
            .returnName(returnInfoDto.getReturnReceiveAddress().getName())
            .returnTelNo1(returnInfoDto.getReturnReceiveAddress().getTel1())
            .returnTelNo2(returnInfoDto.getReturnReceiveAddress().getTel2())
            .collectStatus(returnInfoDto.getCollectStatus())
            .collectDeliveryMethod(returnInfoDto.getCollectDeliveryMethod())
            .collectDeliveryCompany(returnInfoDto.getCollectDeliveryCompany())
            .collectTrackingNumber(returnInfoDto.getCollectTrackingNumber())
            .collectCompletedDate(returnInfoDto.getCollectCompletedDate())
            .claimDeliveryFeeDemandAmount(this.isRangeCheck(returnInfoDto.getClaimDeliveryFeeDemandAmount()))
            .etcFeePayMethod(returnInfoDto.getEtcFeePayMethod())
            .etcFeePayMeans(returnInfoDto.getEtcFeePayMeans())
            .refundStandbyReason(returnInfoDto.getRefundStandbyReason())
            .refundExpectedDate(returnInfoDto.getRefundExpectedDate())
            .returnCompletedDate(returnInfoDto.getReturnCompletedDate())
            .holdbackConfigDate(returnInfoDto.getHoldbackConfigDate())
            .holdbackConfigurer(returnInfoDto.getHoldbackConfigurer())
            .holdbackReleaseDate(returnInfoDto.getHoldbackReleaseDate())
            .holdbackReleaser(returnInfoDto.getHoldbackReleaser())
            .claimDeliveryFeeProductOrderIds(returnInfoDto.getClaimDeliveryFeeProductOrderIds())
            .claimDeliveryFeeDiscountAmount(this.isRangeCheck(returnInfoDto.getClaimDeliveryFeeDiscountAmount()))
            .regId("BATCH")
            .chgId("BATCH")
            .build();
    }

    public MarketClaimInfoDto createMarketClaimInfoDtoByExchange(NaverExchangeInfoDto exchangeInfoDto) {
        return MarketClaimInfoDto.builder()
            .claimStatus(exchangeInfoDto.getClaimStatus())
            .claimRequestDate(exchangeInfoDto.getClaimRequestDate())
            .requestChannel(exchangeInfoDto.getRequestChannel())
            .claimReason(exchangeInfoDto.getExchangeReason())
            .claimDetailReason(exchangeInfoDto.getExchangeDetailedReason())
            .holdbackStatus(exchangeInfoDto.getHoldbackStatus())
            .holdbackReason(exchangeInfoDto.getHoldbackReason())
            .holdbackDetailReason(exchangeInfoDto.getHoldbackDetailedReason())
            .collectBaseAddress(exchangeInfoDto.getCollectAddress().getBaseAddress())
            .collectDetailAddress(exchangeInfoDto.getCollectAddress().getDetailedAddress())
            .collectZipcode(exchangeInfoDto.getCollectAddress().getZipCode())
            .collectTelNo1(exchangeInfoDto.getCollectAddress().getTel1())
            .collectTelNo2(exchangeInfoDto.getCollectAddress().getTel2())
            .returnBaseAddress(exchangeInfoDto.getReturnReceiveAddress().getBaseAddress())
            .returnDetailAddress(exchangeInfoDto.getReturnReceiveAddress().getDetailedAddress())
            .returnZipcode(exchangeInfoDto.getReturnReceiveAddress().getZipCode())
            .returnTelNo1(exchangeInfoDto.getReturnReceiveAddress().getTel1())
            .returnTelNo2(exchangeInfoDto.getReturnReceiveAddress().getTel2())
            .collectStatus(exchangeInfoDto.getCollectStatus())
            .collectDeliveryMethod(exchangeInfoDto.getCollectDeliveryMethod())
            .collectDeliveryCompany(exchangeInfoDto.getCollectDeliveryCompany())
            .collectTrackingNumber(exchangeInfoDto.getCollectTrackingNumber())
            .collectCompletedDate(exchangeInfoDto.getCollectCompletedDate())
            .claimDeliveryFeeDemandAmount(this.isRangeCheck(exchangeInfoDto.getClaimDeliveryFeeDemandAmount()))
            .etcFeePayMethod(exchangeInfoDto.getEtcFeePayMethod())
            .etcFeePayMeans(exchangeInfoDto.getEtcFeePayMeans())
            .holdbackConfigDate(exchangeInfoDto.getHoldbackConfigDate())
            .holdbackConfigurer(exchangeInfoDto.getHoldbackConfigurer())
            .holdbackReleaseDate(exchangeInfoDto.getHoldbackReleaseDate())
            .holdbackReleaser(exchangeInfoDto.getHoldbackReleaser())
            .claimDeliveryFeeProductOrderIds(exchangeInfoDto.getClaimDeliveryFeeProductOrderIds())
            .claimDeliveryFeeDiscountAmount(this.isRangeCheck(exchangeInfoDto.getClaimDeliveryFeeDiscountAmount()))
            .reDeliveryStatus(exchangeInfoDto.getReDeliveryStatus())
            .reDeliveryMethod(exchangeInfoDto.getReDeliveryMethod())
            .reDeliveryCompany(exchangeInfoDto.getReDeliveryCompany())
            .reDeliveryTrackingNumber(exchangeInfoDto.getReDeliveryTrackingNumber())
            .reDeliveryOperationDate(exchangeInfoDto.getReDeliveryOperationDate())
            .regId("BATCH")
            .chgId("BATCH")
            .build();
    }
    
    private String getShipMemo(NaverProductOrderDto productOrder) {
        String pickupLocation = StringUtils.isNotEmpty(productOrder.getShippingAddress().getPickupLocationType()) ? PickupLocationType.valueOf(productOrder.getShippingAddress().getPickupLocationType()).getPikcupLocationTypeNm() : "";
        String pickupContent = StringUtils.isNotEmpty(productOrder.getShippingAddress().getPickupLocationContent()) ? "("+productOrder.getShippingAddress().getPickupLocationContent()+")" : "";
        String shipMemo = StringUtils.isNotEmpty(productOrder.getShippingMemo()) ? productOrder.getShippingMemo() + "//" : "";
        return shipMemo + pickupLocation + pickupContent;
    }

    private long isRangeCheck(long amount) {
        if(amount == -2147483648) {
            return 0;
        }
        return amount;
    }

    private String getStrLength(String str, int position) {
        if(str.length() > position) {
            return str.substring(0, position);
        }
        return str;
    }

}
