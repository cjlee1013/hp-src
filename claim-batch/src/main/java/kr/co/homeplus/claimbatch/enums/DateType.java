package kr.co.homeplus.claimbatch.enums;

public enum DateType {
    YEAR, MONTH, DAY, HOUR, MIN, SEC
}
