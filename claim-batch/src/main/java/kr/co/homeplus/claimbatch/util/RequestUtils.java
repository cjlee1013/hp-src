package kr.co.homeplus.claimbatch.util;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.plus.util.WebUriUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class RequestUtils {

    private static String uriDecode(String uri) {
        return WebUriUtils.decode(uri);
    }

    private static String uriEncode(String uri) {
        return WebUriUtils.encode(uri);
    }

    public static HttpHeaders createHttpHeaders(LinkedHashMap<String, Object> webHeaderMap){
        HttpHeaders httpHeaders = createHttpHeaders();
        webHeaderMap.forEach((key, value) -> {
            httpHeaders.set(key, String.valueOf(value));
        });
        return httpHeaders;
    }

    public static HttpHeaders createHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return httpHeaders;
    }

    /**
     * url parameter 만들기
     *
     * @param setParameterList param 이 담긴 List
     * @see SetParameter
     */
    public static String getParameter(List<SetParameter> setParameterList){
        return setParameterList.stream().map(p -> p.getKey() + "=" + p.getValue())
            .reduce((p1, p2) -> p1 + "&" + p2)
            .map(s -> "?" + s)
            .orElse("");
    }

}
