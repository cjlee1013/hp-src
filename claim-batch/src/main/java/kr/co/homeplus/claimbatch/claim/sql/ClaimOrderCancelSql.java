package kr.co.homeplus.claimbatch.claim.sql;

import java.util.LinkedHashMap;
import kr.co.homeplus.claimbatch.claim.constants.Constants;
import kr.co.homeplus.claimbatch.claim.constants.CustomConstants;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimAdditionEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimBundleEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimExchShippingEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimMstEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimOptEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimPaymentEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimPickShippingEntity;
import kr.co.homeplus.claimbatch.claim.entity.claim.ClaimReqEntity;
import kr.co.homeplus.claimbatch.claim.entity.dms.ItmMngCodeEntity;
import kr.co.homeplus.claimbatch.claim.entity.dms.ItmPartnerSellerDeliveryEntity;
import kr.co.homeplus.claimbatch.claim.entity.dms.ItmPartnerSellerEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.BundleEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderDiscountEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderOptEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.OrderPaymentDivideEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.PaymentEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.PurchaseOrderEntity;
import kr.co.homeplus.claimbatch.claim.entity.order.PurchaseStoreInfoEntity;
import kr.co.homeplus.claimbatch.claim.entity.ship.ShippingItemEntity;
import kr.co.homeplus.claimbatch.claim.entity.ship.ShippingMileagePaybackEntity;
import kr.co.homeplus.claimbatch.claim.entity.ship.StoreSlotMngEntity;
import kr.co.homeplus.claimbatch.claim.enums.MysqlFunction;
import kr.co.homeplus.claimbatch.claim.factory.EntityFactory;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import kr.co.homeplus.claimbatch.util.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ClaimOrderCancelSql {

    /**
     * 환불예정금액 조회 - call procedure
     *
     * @param parameterMap 조회 파라미터
     * @return 환불예정금액
     */
    public static String callByClaimPreRefundPriceCalculation(LinkedHashMap<String, Object> parameterMap){
        log.info("환불예정금 조회(단건) :: \n[{}]", SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION, parameterMap.values().toArray()));
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION, parameterMap.values().toArray());
    }

    /**
     * 환불예정금액 다건 조회 - call procedure
     *
     * @param parameterMap 조회 파라미터
     * @return 환불예정금액
     */
    public static String callByClaimPreRefundPriceMultiCalculation(LinkedHashMap<String, Object> parameterMap){
        log.info("환불예정금 조회(다건) :: \n[{}]", SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_MULTI, parameterMap.values().toArray()));
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_MULTI, parameterMap.values().toArray());
    }

    /**
     * 환불예정금액 마켓건 조회 - call procedure
     *
     * @param parameterMap 조회 파라미터
     * @return 환불예정금액
     */
    public static String callByClaimPreRefundPriceMarketCalculation(LinkedHashMap<String, Object> parameterMap){
        log.info("환불예정금 조회(마켓) :: \n[{}]", SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_MARKET, parameterMap.values().toArray()));
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_MARKET, parameterMap.values().toArray());
    }

    public static String selectOrderBundleByClaim(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                purchaseOrder.purchaseOrderNo,
                bundle.bundleNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(bundle),
                    ObjectUtils.toList(SqlUtils.onClause(ObjectUtils.toArray(purchaseOrder.purchaseOrderNo), bundle))
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
            );
        log.debug("selectOrderBundleByClaim Query :: \n [{}]", query.toString());
        return query.toString();
    }

    /**
     * 클레임 신청시 부분취소 여부 확인을 위한 쿼리
     *
     * @param purchaseOrderNo 주문거래번호
     * @param bundleNo 배송번호
     * @return 부분취소여부 확인 쿼리
     * @throws Exception
     */
    public static String selectOrderByClaimPartInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) throws Exception {
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);

        SQL orderItemQtyQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, ObjectUtils.toArray(orderItem.itemQty), "orderItemQty")
            )
            .FROM(EntityFactory.getTableNameWithAlias(orderItem))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias(purchaseOrder),
                    ObjectUtils.toList(SqlUtils.onClause(ObjectUtils.toArray(orderItem.purchaseOrderNo), purchaseOrder))
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(orderItem.purchaseOrderNo, purchaseOrderNo)
            );

        if(bundleNo != 0){
            orderItemQtyQuery.WHERE(SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo)).GROUP_BY(orderItem.bundleNo);
        }

        SQL claimItemQtyQuery = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, ObjectUtils.toArray(claimItem.claimItemQty))
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(
                    claimItem,
                    ObjectUtils.toArray(claimBundle.claimBundleNo, claimBundle.purchaseOrderNo, claimBundle.bundleNo),
                    ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")))
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimItem.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
            );

        if(bundleNo != 0){
            claimItemQtyQuery.WHERE(SqlUtils.equalColumnByInput(claimBundle.bundleNo, bundleNo)).GROUP_BY(claimBundle.bundleNo);
        }

        SQL query = new SQL()
            .SELECT(
                SqlUtils.subTableQuery(
                    orderItemQtyQuery,
                    "orderItemQty"
                ),
                SqlUtils.subTableQuery(
                    claimItemQtyQuery,
                    "claimItemQty"
                )
            );

        log.debug("getOrderByClaimPartInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderItemInfoByClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                orderItem.itemNo,
                orderOpt.orderOptNo,
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderOpt.optQty),
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.MAX, claimOpt.claimOptQty))
                    ),
                    "claim_qty"
                ),
                orderItem.orderItemNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo, orderItem.purchaseOrderNo, orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimItemNo, orderOpt.orderOptNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo)
            )
            .GROUP_BY(orderItem.itemNo, orderItem.orderItemNo, orderOpt.orderOptNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(orderItem.itemNo),
                SqlUtils.nonAlias(orderOpt.orderOptNo),
                "claim_qty",
                SqlUtils.nonAlias(orderItem.orderItemNo)
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "data"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, "claim_qty", "0")
            );
        log.debug("selectOrderItemInfoByClaim Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectOrderItemInfoByMarketClaim(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo, @Param("marketOrderItemNo") String marketOrderItemNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        OrderOptEntity orderOpt = EntityFactory.createEntityIntoValue(OrderOptEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                orderItem.itemNo,
                orderOpt.orderOptNo,
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.MAX, orderOpt.optQty),
                        SqlUtils.sqlFunction(MysqlFunction.IFNULL_ZERO, SqlUtils.sqlFunction(MysqlFunction.MAX, claimOpt.claimOptQty))
                    ),
                    "claim_qty"
                ),
                orderItem.orderItemNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderOpt, ObjectUtils.toArray(orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    claimBundle,
                    ObjectUtils.toArray(orderItem.purchaseOrderNo),
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C4'", "'C9'")),
                        SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'")
                    )
                )
            )
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimBundleNo, orderItem.purchaseOrderNo, orderItem.orderItemNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimItemNo, orderOpt.orderOptNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(orderItem.bundleNo, bundleNo),
                SqlUtils.equalColumnByInput(orderOpt.marketOrderItemNo, marketOrderItemNo)
            )
            .GROUP_BY(orderItem.itemNo, orderItem.orderItemNo, orderOpt.orderOptNo);

        SQL query = new SQL()
            .SELECT(
                SqlUtils.nonAlias(orderItem.itemNo),
                SqlUtils.nonAlias(orderOpt.orderOptNo),
                "claim_qty",
                SqlUtils.nonAlias(orderItem.orderItemNo)
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "data"))
            .WHERE(
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, "claim_qty", "0")
            );
        log.debug("selectOrderItemInfoByClaim Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimCouponReIssue(@Param("claimNo") long claimNo, @Param("userNo") long userNo) {
        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        OrderDiscountEntity orderDiscount = EntityFactory.createEntityIntoValue(OrderDiscountEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                claimAddition.claimAdditionNo,
                orderDiscount.couponNo,
                orderDiscount.issueNo,
                SqlUtils.aliasToRow(claimAddition.additionSumAmt, "discount_amt"),
                orderDiscount.storeId
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimAddition))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderDiscount, ObjectUtils.toArray(claimAddition.orderDiscountNo, claimAddition.purchaseOrderNo, claimAddition.orderItemNo))
            )
            .WHERE(
                SqlUtils.middleLike(claimAddition.additionTypeDetail, "coupon"),
                SqlUtils.equalColumnByInput(claimAddition.isIssue, "N"),
                SqlUtils.equalColumnByInput(claimAddition.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimAddition.userNo, userNo),
                SqlUtils.equalColumnByInput(claimAddition.totalCancelYn, "Y")
            )
            ;
        log.debug("selectClaimCouponReIssue \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimAddCouponReIssue(@Param("claimNo") long claimNo, @Param("userNo") long userNo) {
        ClaimAdditionEntity claimAddition = EntityFactory.createEntityIntoValue(ClaimAdditionEntity.class, Boolean.TRUE);
        OrderDiscountEntity orderDiscount = EntityFactory.createEntityIntoValue(OrderDiscountEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                claimAddition.claimAdditionNo,
                orderDiscount.couponNo,
                orderDiscount.issueNo,
                SqlUtils.aliasToRow(claimAddition.additionSumAmt, "discount_amt"),
                orderDiscount.storeId
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimAddition))
            .INNER_JOIN(
                SqlUtils.createJoinCondition(orderDiscount, ObjectUtils.toArray(claimAddition.orderDiscountNo, claimAddition.purchaseOrderNo))
            )
            .WHERE(
                SqlUtils.sqlFunction(MysqlFunction.IN, claimAddition.additionTypeDetail, ObjectUtils.toArray("'add_td_coupon'", "'add_ds_coupon'")),
                SqlUtils.equalColumnByInput(claimAddition.isIssue, "N"),
                SqlUtils.equalColumnByInput(claimAddition.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimAddition.userNo, userNo),
                SqlUtils.equalColumnByInput(claimAddition.totalCancelYn, "Y")
            )
            ;
        log.debug("selectClaimCouponReIssue \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimInfo(@Param("claimNo") long claimNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        PurchaseStoreInfoEntity purchaseStoreInfo = EntityFactory.createEntityIntoValue(PurchaseStoreInfoEntity.class, Boolean.TRUE);
        StoreSlotMngEntity storeSlotMng = EntityFactory.createEntityIntoValue(StoreSlotMngEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimMst.claimNo,
                claimMst.userNo,
                claimMst.claimPartYn,
                claimBundle.claimType,
                claimMst.purchaseOrderNo,
                claimBundle.bundleNo,
                claimBundle.claimBundleNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.orderCategory, claimBundle.orderCategory),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, orderItem.itemQty, orderItem.itemQty),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL_SUM_ZERO, claimItem.claimItemQty, claimItem.claimItemQty),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE_N_Y,
                    SqlUtils.customOperator(
                        Constants.EQUAL,
                        SqlUtils.customOperator(
                            CustomConstants.ANGLE_BRACKET_RIGHT,
                            ObjectUtils.toArray(
                                SqlUtils.sqlFunction(
                                    MysqlFunction.DATE_FULL_24,
                                    SqlUtils.customOperator(
                                        CustomConstants.PLUS,
                                        ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, storeSlotMng.shipDt), SqlUtils.sqlFunction(MysqlFunction.CONCAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.MAX, storeSlotMng.slotShipCloseTime), "'00'")))
                                    )
                                ),
                                "NOW()"
                            )
                        ),
                        "1"
                    ),
                    "is_close"
                ),
                purchaseOrder.orderType
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(claimItem.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimMst.purchaseOrderNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(purchaseStoreInfo, ObjectUtils.toArray(bundle.purchaseStoreInfoNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(storeSlotMng, ObjectUtils.toArray( purchaseStoreInfo.storeId, purchaseStoreInfo.slotId, purchaseStoreInfo.shiftId, purchaseStoreInfo.shipDt)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            )
            .GROUP_BY(claimMst.claimNo, claimMst.userNo, claimMst.claimPartYn, claimBundle.claimType, claimMst.purchaseOrderNo, claimBundle.bundleNo, claimBundle.claimBundleNo);

        log.debug("selectClaimInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimRegDataInfo(@Param("claimNo") long claimNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimBundle.paymentNo,
                claimBundle.purchaseOrderNo,
                claimBundle.claimNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .WHERE(SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo))
            .GROUP_BY(claimBundle.paymentNo, claimBundle.purchaseOrderNo, claimBundle.claimNo);
        log.debug("selectClaimRegDataInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimOrderTypeCheck(@Param("claimNo") long claimNo){
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                purchaseOrder.orderType
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo)
            );
        log.debug("selectClaimOrderTypeCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectOriginAndCombineClaimCheck(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("searchType") String searchType) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderItemEntity orderItem = EntityFactory.createEntityIntoValue(OrderItemEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);

        SQL innerQuery = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.cmbnYn,
                orderItem.orderItemNo,
                orderItem.bundleNo,
                orderItem.itemNo,
                SqlUtils.aliasToRow(orderItem.itemNm1, "item_nm"),
                orderItem.orgItemNo,
                orderItem.itemQty,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.AND(
                            SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                            SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3")
                        ),
                        claimItem.claimItemQty,
                        "0"
                    ),
                    claimItem.claimItemQty
                ),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.NOT_IN, claimBundle.claimStatus, ObjectUtils.toArray("'C3'", "'C4'", "'C9'")),
                        claimItem.claimItemQty,
                        "0"
                    ),
                    "process_item_qty"
                ),
                orderItem.itemPrice,
                shippingItem.shipNo,
                orderItem.saleUnit
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderItem, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.orderItemNo, orderItem.bundleNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.IN, bundle.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")))))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(orderItem.purchaseOrderNo, orderItem.bundleNo)))
            .LEFT_OUTER_JOIN(SqlUtils.createJoinCondition(claimItem, ObjectUtils.toArray(claimBundle.claimNo, orderItem.orderItemNo)))
            .ORDER_BY(SqlUtils.orderByesc(orderItem.orderItemNo));

        switch (searchType) {
          case "COMBINE" :
                innerQuery.WHERE(
                    SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo),
                    SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_COMBINE")
                );
                break;
            default:
                innerQuery.WHERE(
                    SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo)
                );
                break;
        }

        SQL query = new SQL()
            .SELECT(
                "purchase_order_no",
                "order_item_no",
                SqlUtils.sqlFunction(MysqlFunction.MAX, "cmbn_yn", "cmbn_yn"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "bundle_no", "bundle_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_no", "item_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_nm", "item_nm"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "org_item_no", "org_item_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_qty", "order_item_qty"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "claim_item_qty", "claim_item_qty"),
                SqlUtils.sqlFunction(MysqlFunction.SUM, "process_item_qty", "process_item_qty"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "item_price", "item_price"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "ship_no", "ship_no"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, "sale_unit", "sale_unit")
            )
            .FROM(SqlUtils.subTableQuery(innerQuery, "itemList"))
            .GROUP_BY("itemList.purchase_order_no", "itemList.order_item_no");

        log.debug("selectOriginAndCombineClaimCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPreRefundRecheckData(@Param("claimNo") long claimNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        ItmMngCodeEntity itmMngCode = EntityFactory.createEntityIntoValue(ItmMngCodeEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(
                claimMst.purchaseOrderNo,
                claimMst.whoReason,
                SqlUtils.aliasToRow("imc.ref_3", "cancel_type"),
                claimMst.claimPartYn,
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(claimReq.deductPromoYn, "'N'"), "pi_deduct_promo_yn"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(claimReq.deductDiscountYn, "'N'"), "pi_deduct_discount_yn"),
                SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(claimReq.deductShipYn, "'N'"), "pi_deduct_ship_yn"),
                SqlUtils.aliasToRow(claimBundle.claimShipFeeEnclose, "enclose_yn")
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimReq, ObjectUtils.toArray(claimBundle.claimNo, claimBundle.claimBundleNo)))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmMngCode),
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(itmMngCode.gmcCd, "claim_reason_type"),
                        SqlUtils.equalColumn(itmMngCode.mcCd, claimReq.claimReasonType)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            )
            .LIMIT(1)
            .OFFSET(0)
            ;
        log.debug("selectPreRefundRecheckData \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPreRefundItemData(@Param("claimNo") long claimNo) {
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class, Boolean.TRUE);
        ClaimOptEntity claimOpt = EntityFactory.createEntityIntoValue(ClaimOptEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimItem.itemNo,
                claimOpt.orderOptNo,
                SqlUtils.aliasToRow(claimOpt.claimOptQty, "claim_qty"),
                claimItem.orderItemNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimItem))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimOpt, ObjectUtils.toArray(claimItem.claimNo, claimItem.claimItemNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimItem.claimNo, claimNo)
            );
        log.debug("selectPreRefundItemData \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectPreRefundCompareData(@Param("claimNo") long claimNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class);
        SQL query = new SQL()
            .SELECT(
                claimMst.claimNo,
                claimMst.completeAmt,
                claimMst.totItemPrice,
                claimMst.totShipPrice,
                claimMst.totIslandShipPrice,
                claimMst.totAddIslandShipPrice,
                claimMst.totDiscountAmt,
                claimMst.totItemDiscountAmt,
                claimMst.totShipDiscountAmt,
                claimMst.totEmpDiscountAmt,
                claimMst.totCartDiscountAmt,
                claimMst.totPromoDiscountAmt,
                claimMst.totCardDiscountAmt,
                claimMst.pgAmt,
                claimMst.mileageAmt,
                claimMst.mhcAmt,
                claimMst.dgvAmt,
                claimMst.ocbAmt
            )
            .FROM(EntityFactory.getTableName(claimMst))
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimMst.oosYn, "N")
            );
        log.debug("selectPreRefundCompareData \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 환불예정금액 다건 조회 - call procedure
     *
     * @param parameterMap 조회 파라미터
     * @return 환불예정금액
     */
    public static String callByClaimPreRefundReCheckCalculation(LinkedHashMap<String, Object> parameterMap){
        log.info("환불예정금 재조회 :: \n[{}]", SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_RECHECK, parameterMap.values().toArray()));
        return SqlUtils.sqlFunction(MysqlFunction.SP_CLAIM_REFUND_CALCULATION_RECHECK, parameterMap.values().toArray());
    }

    public static String updateClaimMstPaymentAmt(@Param("claimNo") long claimNo, @Param("column") String column, @Param("amt") long amt) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimMst))
            .SET(
                SqlUtils.equalColumnByInput(SqlUtils.convertCamelCaseToSnakeCase(column), amt)
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo)
            );
        log.debug("updateClaimMstPaymentAmt \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateClaimPaymentAmt(@Param("claimNo") long claimNo, @Param("paymentType") String paymentType, @Param("paymentAmt") long paymentAmt) {
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class);
        SQL query = new SQL()
            .UPDATE(EntityFactory.getTableName(claimPayment))
            .SET(
                SqlUtils.equalColumnByInput(claimPayment.paymentAmt, paymentAmt)
            )
            .WHERE(
                SqlUtils.equalColumnByInput(claimPayment.paymentType, paymentType),
                SqlUtils.equalColumnByInput(claimPayment.claimNo, claimNo)
            );
        log.debug("updateClaimPaymentAmt \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimApprovalData(@Param("claimNo") long claimNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimMst.claimNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimType, claimBundle.claimType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimReq.requestDt, claimReq.requestDt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimReq.approveDt, claimReq.approveDt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimReq.requestId, claimReq.requestId),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderGiftYn, purchaseOrder.orderGiftYn),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.purchaseOrderNo, claimMst.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.oosYn, claimMst.oosYn),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimPayment.substitutionAmt, claimPayment.substitutionAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.pgCancelRequestCnt, claimPayment.pgCancelRequestCnt)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C2"))))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimReq, ObjectUtils.toArray(claimBundle.claimBundleNo, claimBundle.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimMst.purchaseOrderNo), ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.NOT_IN, purchaseOrder.orderType, ObjectUtils.toArray("'ORD_MARKET'", "'ORD_SUB_MARKET'")))))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimReq.claimNo)))
            .GROUP_BY(claimMst.claimNo)
            ;
        if (claimNo != 0){
            query.WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo),
                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, claimReq.approveDt)
            );
        } else {
            query.WHERE(
                SqlUtils.sqlFunction(
                    MysqlFunction.BETWEEN,
                    ObjectUtils.toArray(
                        claimReq.approveDt,
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATEADD_MIN, ObjectUtils.toArray("NOW()", "-60")), "%Y-%m-%d %H:%i:00")),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATEADD_MIN, ObjectUtils.toArray("NOW()", "-1")), "%Y-%m-%d %H:%i:00"))
                    )
                )
            );
        }

        log.debug("selectClaimApprovalData \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimApprovalDataByMarket(@Param("claimNo") long claimNo) {
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimReqEntity claimReq = EntityFactory.createEntityIntoValue(ClaimReqEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);

        SQL query = new SQL()
            .SELECT(
                claimMst.claimNo,
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimBundle.claimType, claimBundle.claimType),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimReq.requestDt, claimReq.requestDt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimReq.approveDt, claimReq.approveDt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimReq.requestId, claimReq.requestId),
                SqlUtils.sqlFunction(MysqlFunction.MAX, purchaseOrder.orderGiftYn, purchaseOrder.orderGiftYn),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.purchaseOrderNo, claimMst.purchaseOrderNo),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.oosYn, claimMst.oosYn),
                SqlUtils.sqlFunction(MysqlFunction.SUM, claimPayment.substitutionAmt, claimPayment.substitutionAmt),
                SqlUtils.sqlFunction(MysqlFunction.MAX, claimPayment.pgCancelRequestCnt, claimPayment.pgCancelRequestCnt)
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimMst))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimBundle, ObjectUtils.toArray(claimMst.claimNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C2"))))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimReq, ObjectUtils.toArray(claimBundle.claimBundleNo, claimBundle.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(claimMst.purchaseOrderNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(purchaseOrder.orderType, "ORD_MARKET"))))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimReq.claimNo)))
            .GROUP_BY(claimMst.claimNo)
            ;
        if (claimNo != 0){
            query.WHERE(
                SqlUtils.equalColumnByInput(claimMst.claimNo, claimNo),
                SqlUtils.sqlFunction(MysqlFunction.IS_NOT_NULL, claimReq.approveDt)
            );
        } else {
            query.WHERE(
                SqlUtils.sqlFunction(
                    MysqlFunction.BETWEEN,
                    ObjectUtils.toArray(
                        claimReq.approveDt,
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATEADD_MIN, ObjectUtils.toArray("NOW()", "-11")), "%Y-%m-%d %H:%i:00")),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATEADD_MIN, ObjectUtils.toArray("NOW()", "-1")), "%Y-%m-%d %H:%i:00"))
                    )
                )
            );
        }

        log.debug("selectClaimApprovalData \n[{}]", query.toString());
        return query.toString();
    }


    public static String selectClaimBundleTdInfo(@Param("claimNo") long claimNo){
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                claimBundle.bundleNo,
                claimBundle.purchaseOrderNo
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo),
                SqlUtils.equalColumnByInput(claimBundle.orderCategory, "TD")
            );
        log.debug("selectClaimBundleTdInfo Query :: \n [{}]", query.toString());
        return query.toString();
    }

    public static String selectMileagePaybackCheck(@Param("bundleNo") long bundleNo) {
        ShippingMileagePaybackEntity shippingMileagePayback = EntityFactory.createEntityIntoValue(ShippingMileagePaybackEntity.class, Boolean.TRUE);
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                purchaseOrder.purchaseOrderNo,
                purchaseOrder.orgPurchaseOrderNo,
                shippingMileagePayback.orgBundleNo,
                shippingMileagePayback.bundleNo,
                shippingMileagePayback.paybackNo,
                shippingMileagePayback.storeType,
                shippingMileagePayback.userNo,
                shippingMileagePayback.cancelYn,
                shippingMileagePayback.resultYn,
                SqlUtils.aliasToRow(shippingMileagePayback.shipPricePayback, "return_amt")
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingMileagePayback))
            .INNER_JOIN(SqlUtils.createJoinCondition(purchaseOrder, ObjectUtils.toArray(shippingMileagePayback.orgPurchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(payment.paymentStatus, "P3"))))
            .WHERE(
                SqlUtils.equalColumnByInput(shippingMileagePayback.cancelYn, "N"),
                SqlUtils.OR(
                    Boolean.TRUE,
                    SqlUtils.equalColumnByInput(shippingMileagePayback.bundleNo, bundleNo),
                    SqlUtils.equalColumnByInput(shippingMileagePayback.orgBundleNo, bundleNo)
                )
            );
        log.debug("selectMileagePaybackCheck \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileagePaybackInfo(@Param("purchaseOrderNo") long purchaseOrderNo) {
        PurchaseOrderEntity purchaseOrder = EntityFactory.createEntityIntoValue(PurchaseOrderEntity.class, Boolean.TRUE);
        OrderPaymentDivideEntity orderPaymentDivide = EntityFactory.createEntityIntoValue(OrderPaymentDivideEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        PaymentEntity payment = EntityFactory.createEntityIntoValue(PaymentEntity.class, Boolean.TRUE);
        BundleEntity bundle = EntityFactory.createEntityIntoValue(BundleEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.SUM, SqlUtils.customOperator(CustomConstants.PLUS, ObjectUtils.toArray(orderPaymentDivide.pgDivideAmt, orderPaymentDivide.mileageDivideAmt, orderPaymentDivide.mhcDivideAmt, orderPaymentDivide.ocbDivideAmt, orderPaymentDivide.dgvDivideAmt)), "payment_amt"),
                SqlUtils.sqlFunction(MysqlFunction.MAX, bundle.freeCondition, bundle.freeCondition)
            )
            .FROM(EntityFactory.getTableNameWithAlias(purchaseOrder))
            .INNER_JOIN(SqlUtils.createJoinCondition(orderPaymentDivide, ObjectUtils.toArray(purchaseOrder.purchaseOrderNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(shippingItem, ObjectUtils.toArray(orderPaymentDivide.bundleNo, orderPaymentDivide.orderItemNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(payment, ObjectUtils.toArray(purchaseOrder.paymentNo), ObjectUtils.toArray(SqlUtils.equalColumnByInput(payment.paymentStatus, "P3"))))
            .INNER_JOIN(SqlUtils.createJoinCondition(bundle, ObjectUtils.toArray(shippingItem.bundleNo)))
            .WHERE(
                SqlUtils.sqlFunction(MysqlFunction.IN, shippingItem.shipMethod, ObjectUtils.toArray("'TD_DRCT'", "'TD_PICK'", "'TD_QUICK'")),
                SqlUtils.OR(
                    Boolean.TRUE,
                    SqlUtils.equalColumnByInput(purchaseOrder.purchaseOrderNo, purchaseOrderNo),
                    SqlUtils.equalColumnByInput(purchaseOrder.orgPurchaseOrderNo, purchaseOrderNo)
                )
            );
        log.debug("selectMileagePaybackInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectMileagePaybackCancelInfo(@Param("bundleNo") long bundleNo, @Param("orgBundleNo") long orgBundleNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        ClaimMstEntity claimMst = EntityFactory.createEntityIntoValue(ClaimMstEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.customOperator(
                    CustomConstants.MINUS,
                    ObjectUtils.toArray(
                        SqlUtils.sqlFunction(MysqlFunction.SUM, SqlUtils.customOperator(CustomConstants.MINUS, claimPayment.paymentAmt, claimPayment.substitutionAmt)),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totShipPrice),
                        SqlUtils.sqlFunction(MysqlFunction.MAX, claimMst.totIslandShipPrice)
                    ),
                    "cancel_amt"
                )
            )
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimBundle.claimNo)))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimMst, ObjectUtils.toArray(claimPayment.claimNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C3"),
                SqlUtils.customOperator(CustomConstants.NOT_EQUAL, claimBundle.claimType, "'X'"),
                SqlUtils.OR(
                    Boolean.TRUE,
                    SqlUtils.equalColumnByInput(claimBundle.bundleNo, bundleNo),
                    SqlUtils.equalColumnByInput(claimBundle.bundleNo, orgBundleNo)
                )
            );
        log.debug("selectMileagePaybackCancelInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String updateMileagePaybackResult(@Param("paybackNo") long paybackNo) {
        ShippingMileagePaybackEntity shippingMileagePayback = EntityFactory.createEntityIntoValue(ShippingMileagePaybackEntity.class);
        SQL query  = new SQL()
            .UPDATE(EntityFactory.getTableName(shippingMileagePayback))
            .SET(
                SqlUtils.equalColumnByInput(shippingMileagePayback.cancelYn, "Y"),
                SqlUtils.equalColumnByInput(shippingMileagePayback.chgId, "SYSTEM"),
                SqlUtils.equalColumn(shippingMileagePayback.chgDt, "NOW()")
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingMileagePayback.paybackNo, paybackNo)
            );
        log.debug("updateMileagePaybackResult \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimPartnerApproval() {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class, Boolean.TRUE);
        ClaimPaymentEntity claimPayment = EntityFactory.createEntityIntoValue(ClaimPaymentEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT_DISTINCT(claimPayment.claimNo)
            .FROM(EntityFactory.getTableNameWithAlias(claimBundle))
            .INNER_JOIN(SqlUtils.createJoinCondition(claimPayment, ObjectUtils.toArray(claimBundle.claimNo)))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimStatus, "C2"),
                SqlUtils.equalColumnByInput(claimPayment.paymentStatus, "P0"),
                SqlUtils.sqlFunction(
                    MysqlFunction.BETWEEN,
                    ObjectUtils.toArray(
                        claimBundle.chgDt,
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATEADD_HOUR, ObjectUtils.toArray("NOW()", "-3")), "%Y-%m-%d %H:%i:00")),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(SqlUtils.sqlFunction(MysqlFunction.DATEADD_HOUR, ObjectUtils.toArray("NOW()", "-1")), "%Y-%m-%d %H:%i:00"))
                    )
                ),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_LEFT, claimPayment.pgCancelRequestCnt, "4")
            );
        log.debug("selectClaimPartnerApproval \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectRestoreStockItemInfo(@Param("claimNo") long claimNo) {
        ClaimBundleEntity claimBundle = EntityFactory.createEntityIntoValue(ClaimBundleEntity.class);
        SQL query = new SQL()
            .SELECT(
                claimBundle.purchaseOrderNo,
                claimBundle.bundleNo,
                claimBundle.claimBundleNo,
                claimBundle.userNo
            )
            .FROM(EntityFactory.getTableName(claimBundle))
            .WHERE(
                SqlUtils.equalColumnByInput(claimBundle.claimNo, claimNo)
            );
        log.debug("selectRestoreStockItemInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectRestoreStoreItemList(@Param("claimBundleNo") long claimBundleNo) {
        ClaimItemEntity claimItem = EntityFactory.createEntityIntoValue(ClaimItemEntity.class);
        SQL query = new SQL()
            .SELECT(
                claimItem.itemNo,
                SqlUtils.aliasToRow(claimItem.claimItemQty, "qty")
            )
            .FROM(EntityFactory.getTableName(claimItem))
            .WHERE(
                SqlUtils.equalColumnByInput(claimItem.claimBundleNo, claimBundleNo)
            );
        log.debug("selectRestoreStoreItemInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimPickIsAutoInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo) {
        ItmPartnerSellerEntity itmPartnerSeller = EntityFactory.createEntityIntoValue(ItmPartnerSellerEntity.class, Boolean.TRUE);
        ShippingItemEntity shippingItem = EntityFactory.createEntityIntoValue(ShippingItemEntity.class, Boolean.TRUE);
        ItmPartnerSellerDeliveryEntity itmPartnerSellerDelivery = EntityFactory.createEntityIntoValue(ItmPartnerSellerDeliveryEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                SqlUtils.sqlFunction(MysqlFunction.FN_SHIP_TYPE, ObjectUtils.toArray(shippingItem.purchaseOrderNo, shippingItem.bundleNo, "'M'"), "ship_type"),
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                        SqlUtils.equalColumnByInput(SqlUtils.sqlFunction(MysqlFunction.IFNULL, ObjectUtils.toArray(itmPartnerSeller.returnDeliveryYn, "'N'")), "Y"),
                        SqlUtils.sqlFunction(MysqlFunction.CASE_N_Y, SqlUtils.sqlFunction(MysqlFunction.IS_NULL, itmPartnerSellerDelivery.dlvCd)),
                        "'N'"
                    ),
                    itmPartnerSeller.returnDeliveryYn
                ),
                shippingItem.shipMethod,
                shippingItem.dlvCd
            )
            .FROM(EntityFactory.getTableNameWithAlias(shippingItem))
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmPartnerSeller),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(shippingItem.partnerId), itmPartnerSeller)
                    )
                )
            )
            .LEFT_OUTER_JOIN(
                SqlUtils.createJoinCondition(
                    EntityFactory.getTableNameWithAlias("dms", itmPartnerSellerDelivery),
                    ObjectUtils.toList(
                        SqlUtils.onClause(ObjectUtils.toArray(shippingItem.partnerId, shippingItem.dlvCd), itmPartnerSellerDelivery)
                    )
                )
            )
            .WHERE(
                SqlUtils.equalColumnByInput(shippingItem.purchaseOrderNo, purchaseOrderNo),
                SqlUtils.equalColumnByInput(shippingItem.bundleNo, bundleNo)
            )
            .ORDER_BY(SqlUtils.orderByesc(ObjectUtils.toArray(shippingItem.shipNo)))
            .LIMIT(1)
            .OFFSET(0)
            ;
        log.debug("selectClaimPickIsAutoInfo \n[{}]", query.toString());
        return query.toString();
    }

    public static String selectClaimSafetyIssueData(@Param("claimBundleNo") long claimBundleNo, @Param("issueType") String issueType) {
        ClaimPickShippingEntity claimPickShipping = EntityFactory.createEntityIntoValue(ClaimPickShippingEntity.class);
        ClaimExchShippingEntity claimExchShipping = EntityFactory.createEntityIntoValue(ClaimExchShippingEntity.class);
        SQL query = new SQL();
        //수거일 경우
        if(issueType.equals("R")){
            query
                .SELECT(
                    SqlUtils.aliasToRow(claimPickShipping.pickMobileNo, "req_phone_no"),
                    SqlUtils.appendSingleQuoteWithAlias(issueType, "claim_type"),
                    claimPickShipping.claimBundleNo
                )
                .FROM(EntityFactory.getTableName(claimPickShipping))
                .WHERE(SqlUtils.equalColumnByInput(claimPickShipping.claimBundleNo, claimBundleNo));
        } else {
            query
                .SELECT(
                    SqlUtils.aliasToRow(claimExchShipping.exchMobileNo, "req_phone_no"),
                    SqlUtils.appendSingleQuoteWithAlias(issueType, "claim_type"),
                    claimExchShipping.claimBundleNo
                )
                .FROM(EntityFactory.getTableName(claimExchShipping))
                .WHERE(SqlUtils.equalColumnByInput(claimExchShipping.claimBundleNo, claimBundleNo));
        }
        log.debug("selectClaimSafetyIssueData \n[{}]", query.toString());
        return query.toString();
    }

}
