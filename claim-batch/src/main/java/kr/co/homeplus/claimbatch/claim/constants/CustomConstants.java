package kr.co.homeplus.claimbatch.claim.constants;

public class CustomConstants {

    public static final String PLUS = "+";
    public static final String MINUS = "-";
    public static final String MULTIPLY = "*";
    public static final String DIVISION = "/";
    public static final String MOD = "%";
    public static final String SINGLE_QUOTE = "'";
    public static final String DOUBLE_QUOTE = "\"";
    public static final String ROUND_BRACKET_START = "(";
    public static final String ROUND_BRACKET_END = ")";
    public static final String CURLY_BRACKET_START = "{";
    public static final String CURLY_BRACKET_END = "}";
    public static final String SQUARE_BRACKET_START = "{";
    public static final String SQUARE_BRACKET_END = "}";
    public static final String ANGLE_BRACKET_LEFT = "<";
    public static final String ANGLE_BRACKET_RIGHT = ">";
    public static final String RIGTH_UP_EQUAL = "<=";
    public static final String LEFT_UP_EQUAL = ">=";
    public static final String SPLIT_DOT = "\\.";
    public static final String NOT_EQUAL = "!=";
    public static final String IN = "IN";
    public static final String OUT = "OUT";
    public static final String YYYYMMDD = "%Y-%m-%d";
    public static final String YYYYMMDD_COMMA = "%Y.%m.%d";
    public static final String HHMI = "%H:%i";
    public static final String HHMISS = "%H:%i:%s";
    public static final String SQL_BLANK = "' '";
    public static final String AND = "AND";
    public static final String OR = "OR";
    public static final String TILDE = "~";
    public static final String COLON = ":";
}
