package kr.co.homeplus.claimbatch.pine.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.claimbatch.history.model.BatchHistoryDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "PINE 영수증 채번 테이블에서 대상을 조회")
@Getter
@Setter
@EqualsAndHashCode
public class ClaimPosReceiptIssueDto {

    @ApiModelProperty(value = "점포ID", position = 1)
    private String storeId;

    @ApiModelProperty(value = "POS번호", position = 2)
    private String posNo;

    @ApiModelProperty(value = "거래일자(YYYYMMDD)", position = 3)
    private String tradeDt;

    @ApiModelProperty(value = "배치이력 Dto", position = 4)
    private BatchHistoryDto batchHistoryDto;

}
