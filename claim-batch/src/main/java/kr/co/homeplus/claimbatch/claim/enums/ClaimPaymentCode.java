package kr.co.homeplus.claimbatch.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ClaimPaymentCode {
    P0("P0"),
    P1("P1"),
    P2("P2"),
    P3("P3"),
    P4("P4");

    @Getter
    private final String code;
}
