package kr.co.homeplus.claimbatch.claim.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum MysqlFunction {
    COMMON_TABLE("(SELECT IFNULL(imc.mc_nm, '''') AS mc_cd FROM dms.itm_mng_code imc where imc.gmc_cd = ''{0}'' AND imc.mc_cd = {1})"),
    COMMON_ROW_TABLE(
        "(SELECT *, CASE @GROUPING WHEN {1} THEN @RANKT := @RANK + 1 ELSE @RANK := 1 END AS RANKING, @GROUPING := {1}\n"
            + " FROM {0}, (SELECT @GROUPING:= '''', @RANK := 0) XX\n"
            + " ORDER BY {2} )"
    ),
    CLAIM_SUB_TABLE("( SELECT cm.purchase_order_no, cb.bundle_no, MIN(cm.claim_part_yn) AS claim_part_yn\n"
        + "  FROM claim_mst cm\n"
        + " INNER JOIN claim_bundle cb ON cm.claim_no = cb.claim_no AND cm.purchase_order_no = cb.purchase_order_no\n"
        + " WHERE cm.user_no = {0}\n"
        + "   AND cb.claim_status = ''C3''\n"
        + "   AND cb.claim_type != ''X''\n"
        + " GROUP BY cm.purchase_order_no, cb.bundle_no ) claimSubData"),
    CLAIM_ITEM_SUB_TABLE("(SELECT cb.purchase_order_no, cb.bundle_no, ci.item_no, MAX(ci.claim_item_qty) AS claim_item_qty\n"
        + "  FROM claim_bundle cb\n"
        + " INNER JOIN claim_item ci ON cb.claim_bundle_no = ci.claim_bundle_no AND cb.bundle_no = ci.bundle_no\n"
        + " WHERE cb.user_no = {0}\n"
        + "   AND cb.claim_status = ''C3''\n"
        + "   AND cb.claim_type != ''X''\n"
        + " GROUP BY cb.purchase_order_no, cb.bundle_no, ci.item_no) claimItemData"),
    PAYMENT_METHOD_INFO(
        "(SELECT IFNULL(pm.method_nm, '''') AS method_nm\n"
            + " FROM payment_method pm\n"
            + " WHERE pm.site_type = {0} AND pm.method_cd = {1} AND pm.parent_method_cd = {2})"),
    ABS("ABS({0})"),
    AND(" AND {0}"),
    AES_DECRYPT("(SELECT AES_DECRYPT(UNHEX({0}), ''{1}''))"),
    BETWEEN("{0} BETWEEN {1} AND {2}"),
    BETWEEN_DAY("{0} BETWEEN {1} AND DATE_ADD({2}, INTERVAL 1 DAY)"),
    BETWEEN_MIN("{0} BETWEEN {1} AND DATE_ADD({2}, INTERVAL 1 MINUTE)"),
    BETWEEN_NOW_MONTH("{0} BETWEEN DATE_ADD(NOW(), INTERVAL -1 MONTH) AND NOW()"),
    BETWEEN_ORI("{0} BETWEEN {1} AND {2}"),
    CASE("CASE WHEN {0} THEN {1} ELSE {2} END"),
    CASE_N_Y("CASE WHEN {0} THEN ''N'' ELSE ''Y'' END"),
    CASE_Y_N("CASE WHEN {0} THEN ''Y'' ELSE ''N'' END"),
    CASE_WHEN("CASE [columnName] WHEN [...] THEN [...] END"),
    CAST_CHAR("CAST({0} AS CHAR)"),
    CAST_NCHAR("CAST({0} AS NCHAR)"),
    CONCAT("CONCAT([...])"),
    COUNT("COUNT({0})"),
    DAYOFWEEK("DAYOFWEEK({0})"),
    DATEADD_DAY("DATE_ADD({0}, INTERVAL {1} DAY)"),
    DATEADD_MIN("DATE_ADD({0}, INTERVAL {1} MINUTE)"),
    DATEADD_HOUR("DATE_ADD({0}, INTERVAL {1} HOUR)"),
    DATEADD_MON("DATE_ADD({0}, INTERVAL {1} MONTH)"),
    DATEADD_YEAR("DATE_ADD({0}, INTERVAL {1} YEAR)"),
    DATEADD_WEEK("DATE_ADD({0}, INTERVAL {1} WEEK)"),
    DATEDIFF("DATEDIFF({0}, {1})"),
    DATE_FORMAT("DATE_FORMAT({0}, ''{1}'')"),
    DATE_FULL_12("DATE_FORMAT({0}, ''%Y-%m-%d %h:%i:%s'')"),
    DATE_FULL_24("DATE_FORMAT({0}, ''%Y-%m-%d %H:%i:%s'')"),
    DATE_FULL_LAST("DATE_FORMAT({0}, ''%Y-%m-%d 23:59:59'')"),
    DATE_FULL_ZERO("DATE_FORMAT({0}, ''%Y-%m-%d 00:00:00'')"),
    DISTINCT("DISTINCT {0}"),
    ELSE("ELSE {0} END"),
    EXISTS("EXISTS ({0})"),
    FLOOR("FLOOR({0})"),
    IFNULL("IFNULL({0}, {1})"),
    IFNULL_BLANK("IFNULL({0}, '''')"),
    IFNULL_SUM_ZERO("IFNULL(SUM({0}), 0)"),
    IFNULL_ZERO("IFNULL({0}, 0)"),
    IN("{0} IN ({1})"),
    IS_NULL("{0} IS NULL"),
    IS_NOT_NULL("{0} IS NOT NULL"),
    LIKE("{0} LIKE ''{1}''"),
    LEFT("LEFT({0}, {1})"),
    LENGTH("LENGTH({0})"),
    LOWER("LOWER({0})"),
    MIN("MIN({0})"),
    MAX("MAX({0})"),
    NOT_IN("{0} NOT IN ({1})"),
    NOT_LIKE("{0} NOT LIKE ''{1}''"),
    NOT_EXISTS("NOT EXISTS ({0})"),
    OR("{0} OR {1}"),
    RIGHT("RIGHT({0}, {1})"),
    SUM("SUM({0})"),
    SUBSTR("SUBSTR({0}, {1}, {2})"),
    SUB_QUERY("SELECT {0} FROM {1} WHERE {2} "),
    ROUND("ROUND({0})"),
    UPPER("UPPER({0})"),
    SP_CLAIM_REFUND_CALCULATION("up_nt_get_claim_info_order(''{0}'', ''{1}'', ''{2}'', ''{3}'', ''{4}'', ''{5}'', ''{6}'', ''{7}'', ''{8}'', ''{9}'', ''{10}'', ''{11}'')"),
    SP_CLAIM_REFUND_CALCULATION_MULTI("up_nt_get_claim_info_order_multi(''{0}'', ''{1}'', ''{2}'', ''{3}'', ''{4}'', ''{5}'', ''{6}'', ''{7}'', ''{8}'', ''{9}'', ''{10}'')"),
    SP_CLAIM_REFUND_CALCULATION_MARKET("up_nt_get_claim_info_order_multi_m(''{0}'', ''{1}'', ''{2}'', ''{3}'', ''{4}'', ''{5}'', ''{6}'', ''{7}'', ''{8}'', ''{9}'', ''{10}'')"),
    SP_CLAIM_REFUND_CALCULATION_OOS("up_nt_get_claim_info_order_multi_oos(''{0}'', ''{1}'', ''{2}'', ''{3}'', ''{4}'', ''{5}'', ''{6}'')"),
    SP_CLAIM_REFUND_CALCULATION_RECHECK("up_nt_get_claim_info_order_multi_retry(''{0}'', ''{1}'', ''{2}'', ''{3}'', ''{4}'', ''{5}'', ''{6}'', ''{7}'', ''{8}'', ''{9}'', ''{10}'')"),
    SP_CLAIM_EMP_DISCOUNT("up_tx_claim_emp_discount(''{0}'', ''{1}'')"),
    SP_CLAIM_ADDITION_REG("up_tx_claim_addition_reg(''{0}'')"),
    SP_CLAIM_REG("up_tx_claim_reg(''{0}'', ''{1}'', ''{2}'')"),
    FN_STORE_PARTNER_NAME("uf_get_store_partner_name({0}, ''{1}'')"),
    FN_ESTIMATED_DELIVERY("uf_get_estimated_delivery({0}, {1}, {2})"),
    FN_ORDER_OPT_NM("uf_get_order_opt_nm({0})"),
    FN_ORDER_OPTION_NM_MANAGE("uf_get_order_opt_nm_manage({0})"),
    FN_PAYMENT_TYPE("uf_main_payment_type({0})"),
    FN_SHIP_ADDR("uf_shipping_addr({0})"),
    FN_PREV_CLAIM_AMT("uf_get_claim_prev_cancel_amt({0}, {1})"),
    FN_SHIP_TYPE("uf_get_ship_type({0}, {1}, {2})");

    @Getter
    private final String pattern;
}
