package kr.co.homeplus.claimbatch.claim.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.claimbatch.claim.enums.ClaimCode;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.ClaimCouponReIssueGetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.ClaimCouponReIssueSetDto.CouponReIssueDto;
import kr.co.homeplus.claimbatch.claim.model.payment.RestoreSlotStockDto;
import kr.co.homeplus.claimbatch.claim.model.payment.RestoreStockItemDto;
import kr.co.homeplus.claimbatch.claim.model.payment.mileage.MileageSaveCancelDto;
import kr.co.homeplus.claimbatch.claim.model.payment.mileage.MileageSaveRequestDto;
import kr.co.homeplus.claimbatch.claim.model.payment.ticket.ClaimCoopTicketCheckGetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.ticket.ClaimCoopTicketCheckSetDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimInfoRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ClaimMstRegDto;
import kr.co.homeplus.claimbatch.claim.model.register.ticket.ClaimTicketModifyDto;
import kr.co.homeplus.claimbatch.enums.ExternalUrlInfo;
import kr.co.homeplus.claimbatch.util.DateUtils;
import kr.co.homeplus.claimbatch.util.ExternalUtil;
import kr.co.homeplus.claimbatch.util.LogUtil;
import kr.co.homeplus.claimbatch.util.MessageSendUtil;
import kr.co.homeplus.claimbatch.util.ObjectUtils;
import kr.co.homeplus.claimbatch.util.SlackUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimPaymentService {

    private final ClaimMapperService mapperService;

    private final ClaimCommonService commonService;

    @Value("${spring.profiles.active}")
    private String profilesActive;

    public Boolean paymentCancel(long claimNo, String regId, boolean isPgCheck) throws Exception {
        // 티켓정보 취득
        // 주문에 대한 확인 필요.
        if(!this.getTicketCancelProcess(claimNo)){
            throw new Exception("티켓 취소중 오류가 발생하였습니다.");
        }
//        if("dev,local".contains(profilesActive)){
//            // 취소전 결제금액 재조회
//            this.getPrePaymentCancel(claimNo);
//        }
        setClaimPaymentStatus(claimNo, ClaimCode.CLAIM_PAYMENT_STATUS_P1);
        // 취소 처리.
        if (!this.paymentCancelProcess(claimNo, isPgCheck)) {
            log.error("클레임 결제 취소 중 오류가 발생됨. {}", claimNo);
            throw new Exception("클레임 결제 취소 중 오류");
        }
        // 상태 변경 C2-> C3
        if (!this.setClaimStatus(claimNo, regId, "C3")) {
            throw new Exception("클레임 상태 변경중 오류가 발생되었습니다.");
        }

        try {
            // claim_info 생성.
            this.callingClaimReg(claimNo);
            // 재고환원처리
            this.restoreItemForClaim(claimNo);
            // MHC적립취소처리.
            this.saveCancelPointProcess(claimNo);
            // 배송비 페이백 확인 및 회수처리.
            this.shipPaybackProcess(claimNo);
            // 쿠폰 깨진거
            Boolean isCouponAndSlot = this.isClaimCouponAndSlotProcess(claimNo);
        } catch (Exception e){
            log.error("결제 취소 후 작업 오류 :::: {}", e.getMessage());
        }
        return Boolean.TRUE;
    }

    public void paymentCancelByMarket(long claimNo, String regId) throws Exception {
        // 금액업데이트
        log.info("마켓클레임 금액 업데이트(배송) 결과 : {}", this.marketClaimAmountModify(claimNo));
        // 취소 처리를 위한 로직
        setClaimPaymentStatus(claimNo, ClaimCode.CLAIM_PAYMENT_STATUS_P1);
        // 취소가능 정보를 획득
        List<LinkedHashMap<String, Object>> claimPaymentInfo = mapperService.getPaymentCancelInfo(claimNo);
        if(claimPaymentInfo != null && claimPaymentInfo.size() > 0) {
            LinkedHashMap<String, Object> map = claimPaymentInfo.get(0);
            setClaimPaymentStatus(map.get("claim_payment_no"), "0000", ClaimCode.CLAIM_PAYMENT_STATUS_P3, this.isLastPaymentCancelCheck(map) ? "Y" : "N");
        } else {
            // P3
            this.setClaimPaymentStatus(claimNo, ClaimCode.CLAIM_PAYMENT_STATUS_P3);
        }

        // 상태 변경 C2-> C3
        if (!this.setClaimStatus(claimNo, regId, "C3")) {
            throw new Exception("클레임(마켓) 상태 변경중 오류가 발생되었습니다.");
        }

        try {
            // claim_info 생성.
            this.callingClaimReg(claimNo);
            // 재고 환원
            this.restoreStockByMarket(claimNo);
            // MHC적립취소처리.
            this.saveCancelPointProcess(claimNo);
            // 배송비 페이백 확인 및 회수처리.
            this.shipPaybackProcess(claimNo);
            // 쿠폰 깨진거
            this.isClaimCouponAndSlotProcess(claimNo);
        } catch (Exception e){
            log.error("결제 취소 후 작업 오류 :::: {}", e.getMessage());
        }
    }

    /**
     * 결제취소 전 환불예정금액 비교
     *
     */
    public void getPrePaymentCancel(long claimNo) throws Exception {
        // 기존 환불예정금 취득
        ClaimMstRegDto claimMstInfo = mapperService.getPreRefundCompareData(claimNo);
        if(claimMstInfo == null) {
            // 환불예정금액 재조회
            ClaimPreRefundCalcGetDto reCheckRefundDto =  mapperService.getClaimPreRefundReCheckCalculation(getClaimPreRefundData(claimNo));
            if(reCheckRefundDto.getReturnValue() != 0) {
                log.info("결제취소전 환불예정금 조회실패 :: {}", reCheckRefundDto.getReturnMsg());
                throw new Exception(reCheckRefundDto.getReturnMsg());
            }
            LinkedHashMap<String, Object> claimMstInfoMap = ObjectUtils.getConvertMap(claimMstInfo);
            LinkedHashMap<String, Object> reCheckMap = commonService.convertClaimRegInfo(ClaimMstRegDto.builder().build(), reCheckRefundDto);
            if(!getPreRefundAmtCheck(claimMstInfoMap, reCheckMap, "pgAmt", "mileageAmt", "mhcAmt", "dgvAmt", "ocbAmt")){
                // 환불예정금 변경처리.
                throw new Exception("클레임 등록에 실패하였습니다.");
            }
        }
    }

    // 취소 처리.
    // 성공 실패시 업데이트가 되어야 함.취소 회수 중요함.
    public Boolean paymentCancelProcess(long claimNo, boolean isPgCheck) throws Exception {
        // 클레임 결제 정보 리스트
        List<LinkedHashMap<String, Object>> claimPaymentInfo = mapperService.getPaymentCancelInfo(claimNo);

        if(claimPaymentInfo.size() == 0){
            log.info("{} 에 대한 취소거래 건이 없습니다.", claimNo);
            return Boolean.FALSE;
        }
        if(isPgCheck){
            boolean isOverTry = claimPaymentInfo.stream().map(map -> map.get("pg_cancel_request_cnt")).map(ObjectUtils::toInt).anyMatch(value -> value > 3);
            if(isOverTry){
                MessageSendUtil.sendRefundFail(claimNo);
                throw new Exception("재시도 횟수를 초과하였습니다.");
            }
        }
        // return Type
        boolean isNextStep = Boolean.TRUE;
        // 클레임 결제 정보 데이터내 정보로 결제취소처리.
        for(LinkedHashMap<String, Object> map : claimPaymentInfo){
            // 취소로직 SKIP여부
            Boolean isSkip = Boolean.FALSE;
            // 클레임 취소 승인코드
            String claimApprovalCd = null;
            log.info("클레임그룹번호({})에 대한 결제수단({}) 취소정보 : {}", claimNo, map.get("payment_type"), map);

            // 결제금액 확인
            if(ObjectUtils.toLong(map.get("payment_amt")) < 0){
                // 클레임 금액이 0원미만이면 오류 처리.
                log.info("{} 의 결제취소 건 중 {} 의 금액이 0보다 작으므로 오류처리", claimNo, map.get("payment_type"));
                return Boolean.FALSE;
            } else if(ObjectUtils.toLong(map.get("payment_amt")) == 0){
                // 클레임 비용이 0원이면 완료처리.(렌탈 등)
                claimApprovalCd = "0000";
                // PG결제취소금액이 0원일때 원거래번호를 넣어준다.
                if(ObjectUtils.toString(map.get("payment_type")).equals("PG")) {
                    setClaimPaymentCancelTradeId(map.get("claim_payment_no"), map.get("payment_trade_id"));
                }
                isSkip = Boolean.TRUE;
            } else if(ObjectUtils.toLong(map.get("order_payment_amt")) < (ObjectUtils.toLong(map.get("prev_payment_amt")) + ObjectUtils.toLong(map.get("payment_amt")))) {
                isSkip = Boolean.TRUE;
                isNextStep = Boolean.FALSE;
                claimApprovalCd = "9999";
                this.addProcessHistory(
                    ObjectUtils.toLong(map.get("claim_no")),
                    ObjectUtils.toLong(map.get("purchase_order_no")),
                    ObjectUtils.toString(map.get("payment_type")),
                    ObjectUtils.toString(map.get("parent_payment_method")),
                    ObjectUtils.toString("취소금액이 원금액보다 큽니다.")
                );
            }

            if(!isSkip) {
                map.put("cancel_message", "클레임 발생으로 결제 취소");
                // 클레임 결제 상태 변경(P1 -> P2)
                setClaimPaymentStatus(map.get("claim_payment_no"), claimApprovalCd, ClaimCode.CLAIM_PAYMENT_STATUS_P2, null);
                LinkedHashMap<String, Object> returnMap = new LinkedHashMap<>();
                try {
                    log.info("{} 클레임 결제 취소 요청 Map ::: [{}]", claimNo, map);
                    // 특정사용자 오류 처리.
                    switch (ObjectUtils.toString(map.get("payment_type"))) {
                        case "PP" :
                            returnMap = ExternalUtil.externalProcess(commonService.createPaymentCancelForMileageDto(map));
                            returnMap.put("claimApprovalNo", returnMap.get("approvalNo"));
                            returnMap.put("returnCode", "0000");
                            break;
                        case "PG" :
                            returnMap = ExternalUtil.externalProcess(commonService.createPaymentPgCancelSetDto(map));
                            returnMap.put("claimApprovalNo", returnMap.get("resultCode"));
                            returnMap.put("returnCode", returnMap.get("resultCode"));
                            setClaimPaymentCancelTradeId(map.get("claim_payment_no"), returnMap.get("pgPartTid") == null ? returnMap.get("pgTid") : returnMap.get("pgPartTid"));
                            break;
                        case "MP" :
                            returnMap = ExternalUtil.externalProcess(commonService.createPaymentCancelForMhc(map));
                            returnMap.put("claimApprovalNo", returnMap.get("mhcAprNumber"));
                            break;
                        case "OC" :
                            returnMap = ExternalUtil.externalProcess(commonService.createPaymentCancelForOcb(map));
                            returnMap.put("claimApprovalNo", returnMap.get("ocbAprNumber"));
                            break;
                        case "DG" :
                            returnMap = ExternalUtil.externalProcess(commonService.createPaymentCancelForDgv(map));
                            returnMap.put("claimApprovalNo", returnMap.get("aprNumber"));
                            break;
                        default:
                            log.info("{} 의 {} 은 결제취소할 수 없는 프로세스입니다.", claimNo, ObjectUtils.toString(map.get("payment_type")));
                            return Boolean.FALSE;
                    }

                    claimApprovalCd = ObjectUtils.toString(returnMap.get("claimApprovalNo"));
                    String returnCode = ObjectUtils.toString(returnMap.get("returnCode"));
                    if(!"0000,SUCCESS".contains(returnCode)){
                        claimApprovalCd = returnCode;
                        log.info("{} 의 {} 취소 실패 :: [취소실패코드 : {} | 취소실패메시지 : {}]", claimNo, ObjectUtils.toString(map.get("payment_type")), returnCode, ObjectUtils.toString(returnMap.get("returnMessage")));
                        isNextStep = isProcessStep(ObjectUtils.toString(map.get("payment_type")), returnCode);
                        this.addProcessHistory(
                            ObjectUtils.toLong(map.get("claim_no")),
                            ObjectUtils.toLong(map.get("purchase_order_no")),
                            ObjectUtils.toString(map.get("payment_type")),
                            ObjectUtils.toString(map.get("parent_payment_method")),
                            ObjectUtils.toString(returnMap.get("returnMessage")).concat("(").concat(ObjectUtils.toString(returnMap.get("returnCode"))).concat(")")
                        );
                    }
                    log.info("{} 의 {} 결제취소 결과 ::: [{}]", claimNo, ObjectUtils.toString(map.get("payment_type")), claimApprovalCd);
                } catch (Exception e) {
                    // 오류시 오류 카운트!
                    log.error("클레임 결제 취소 Exception ::: [{}]", e.getMessage());
                    // 결제 취소 실패 업데이트 (P2 -> P4)
                    isNextStep = Boolean.FALSE;
                    this.addProcessHistory(
                        ObjectUtils.toLong(map.get("claim_no")),
                        ObjectUtils.toLong(map.get("purchase_order_no")),
                        ObjectUtils.toString(map.get("payment_type")),
                        ObjectUtils.toString(map.get("parent_payment_method")),
                        ObjectUtils.toString(e.getMessage())
                    );
                }
            }

            if(!isNextStep){
                setClaimPaymentStatus(map.get("claim_payment_no"), claimApprovalCd, ClaimCode.CLAIM_PAYMENT_STATUS_P4, null);
                return Boolean.FALSE;
            }
            // 결제 취소 성공 업데이트(P2 -> P3)
            setClaimPaymentStatus(map.get("claim_payment_no"), claimApprovalCd, ClaimCode.CLAIM_PAYMENT_STATUS_P3, this.isLastPaymentCancelCheck(map) ? "Y" : "N");
        }

        return Boolean.TRUE;
    }

    public Boolean getTicketCancelProcess(long claimNo) throws Exception {
        LinkedHashMap<String, String> getOrderType = mapperService.getClaimOrderTypeCheck(claimNo);
        if(getOrderType != null){
            if(getOrderType.get("order_type").equals("ORD_TICKET")){
                List<LinkedHashMap<String, Object>> claimTicketInfo = mapperService.getClaimTicketInfo(claimNo);
                // 데이터 취소요청으로 업데이트
                if(claimTicketInfo.stream().map(map -> map.get("ticket_status")).map(String::valueOf).allMatch("T2,T4"::contains)){
                    return false;
                }
                //update ticket_status T1 -> T3
                for(LinkedHashMap<String, Object> map : claimTicketInfo){
                    if(!mapperService.modifyOrderTicketInfo(ClaimTicketModifyDto.builder().claimTicketNo(ObjectUtils.toLong(map.get("claim_ticket_no"))).ticketStatus("T3").issueStatus(null).build())){
                        throw new Exception("취소 가능한 결제 건이 없습니다.");
                    }
                }

                if(claimTicketInfo.stream().map(map -> map.get("issue_channel")).map(String::valueOf).anyMatch(str -> str.equals("COOP"))){
                    // 취소 요청 AND 취소처리 업데이트
                    for(LinkedHashMap<String, Object> coopMap : claimTicketInfo.stream().filter(map -> ObjectUtils.toString(map.get("issue_channel")).equals("COOP")).collect(Collectors.toList())){
                        if(!commonService.getCoopTicketCancel(ObjectUtils.toCamelMap(coopMap, "seller_item_cd", "ticket_cd"))){
                            throw new Exception("취소불가능한 티켓이 포함되어 있습니다.");
                        }
                    }
                }
                //update ticket_status T1 -> T3
                for(LinkedHashMap<String, Object> map : claimTicketInfo){
                    if(!mapperService.modifyOrderTicketInfo(ClaimTicketModifyDto.builder().claimTicketNo(ObjectUtils.toLong(map.get("claim_ticket_no"))).ticketStatus("T4").issueStatus("I3").build())){
                        throw new Exception("취소 가능한 결제 건이 없습니다.");
                    }
                }
            }
        }
        return Boolean.TRUE;
    }

    public Boolean saveCancelPointProcess(long claimNo) throws Exception {
        List<LinkedHashMap<String, Object>> createAccumulateMap = mapperService.getCreateClaimAccumulateInfo(claimNo);
        if(createAccumulateMap == null || createAccumulateMap.size() <= 0){
            log.info("적립취소할 금액이 존재하지 않음.");
            return Boolean.FALSE;
        }
        for(LinkedHashMap<String, Object> map : createAccumulateMap){
            Boolean isPartCancel = Boolean.FALSE;
            long purchaseOrderNo = ObjectUtils.toLong(map.get("purchase_order_no"));
            String pointKind = ObjectUtils.toString(map.get("point_kind"));
            log.info("주문번호[{}]의 [{}] 적립취소 데이터 ::: [{}] ", purchaseOrderNo, pointKind, map);
            // 이전 적립취소 금액
            LinkedHashMap<String, Object> preClaimAccumulateInfo = mapperService.getAccumulateCancelInfo(purchaseOrderNo, pointKind);
            log.info("주문번호[{}]의 [{}] 이전취소 데이터 ::: [{}] ", purchaseOrderNo, pointKind, preClaimAccumulateInfo);

            if(preClaimAccumulateInfo == null){
                preClaimAccumulateInfo = new LinkedHashMap<>();
                preClaimAccumulateInfo.put("claim_base_acc_amt", 0);
                preClaimAccumulateInfo.put("claim_add_acc_amt", 0);
                preClaimAccumulateInfo.put("claim_acc_target_amt", 0);
            }
            // 누적 취소금액 = 적립취소대상금액의 합
            map.put("acc_cancel_amt", preClaimAccumulateInfo.get("claim_acc_target_amt"));
            // claim_base_acc_amt / base_acc_amt - (TRUNCATE(acc_target_amt - (claim_acc_target_amt + claim_acc_target_amt), -3) *  =
            int accTargetAmt = ObjectUtils.toInt(map.get("acc_target_amt"));
            int claimAccTargetAmt = (ObjectUtils.toInt(preClaimAccumulateInfo.get("claim_acc_target_amt")) + ObjectUtils.toInt(map.get("claim_acc_target_amt")));
            if(accTargetAmt != ObjectUtils.toInt(map.get("claim_acc_target_amt"))){
                isPartCancel = Boolean.TRUE;
            }
            if(accTargetAmt == ObjectUtils.toInt(map.get("claim_acc_target_amt"))){
                // 남은적립대상금액이 0원인 경우
                // 최초적립금액 - 취소한적립금액 이 취소적립금액
                map.put("claim_base_acc_amt", ObjectUtils.toInt(map.get("base_acc_amt")) - ObjectUtils.toInt(preClaimAccumulateInfo.get("claim_base_acc_amt")));
                map.put("claim_add_acc_amt", ObjectUtils.toInt(map.get("add_acc_amt")) - ObjectUtils.toInt(preClaimAccumulateInfo.get("claim_add_acc_amt")));
            } else {
                map.put("claim_base_acc_amt", this.getCalculationSavePoint(pointKind, ObjectUtils.toInt(map.get("base_acc_amt")), ObjectUtils.toInt(preClaimAccumulateInfo.get("claim_base_acc_amt")), (accTargetAmt - claimAccTargetAmt), ObjectUtils.toDouble(map.get("base_acc_rate"))));
                map.put("claim_add_acc_amt", this.getCalculationSavePoint(pointKind, ObjectUtils.toInt(map.get("add_acc_amt")), ObjectUtils.toInt(preClaimAccumulateInfo.get("claim_add_acc_amt")), (accTargetAmt - claimAccTargetAmt), ObjectUtils.toDouble(map.get("add_acc_rate"))));
            }
            map.put("result_point", ObjectUtils.toInt(map.get("claim_base_acc_amt")) + ObjectUtils.toInt(map.get("claim_add_acc_amt")));
            log.info("주문번호({}) 취소포인트 계산 결과 ::: [{}]", purchaseOrderNo, map);
            // insert
            if(mapperService.addClaimAccumulate(map)) {
                LinkedHashMap<String, Object> resultMap = null;
                if(pointKind.equals("MHC")){
                    if(isPartCancel){
                        mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_I.getCode(), pointKind);
                        try{
                            resultMap = ExternalUtil.externalProcess(commonService.createSaveCancelMhcPartDto(map));
                        } catch (Exception e){
                            resultMap = null;
                            mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_E.getCode(), pointKind);
                        }
                    } else {
                        try{
                            resultMap = ExternalUtil.externalProcess(commonService.createSaveCancelMhcDto(map));
                        } catch (Exception e){
                            resultMap = null;
                            mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_E.getCode(), pointKind);
                        }
                    }
                    log.info("주문번호({}) MHC주문적립 취소 결과 :: {}", purchaseOrderNo, resultMap);
                    if(resultMap != null){
                        resultMap.put("claim_no", map.get("claim_no"));
                        mapperService.modifyClaimAccumulateReqResult(resultMap);
                        if("0000".equals(ObjectUtils.toString(resultMap.get("returnCode")))){
                            mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_Y.getCode(), pointKind);
                        } else {
                            mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_N.getCode(), pointKind);
                        }
                    }
                } else {
                    try {
                        LinkedHashMap<String, Object> mileageSaveReturnMap = new LinkedHashMap<>();
                        mileageSaveReturnMap.put("org_purchase_order_no", map.get("purchase_order_no"));
                        mileageSaveReturnMap.put("store_type", map.get("site_type"));
                        mileageSaveReturnMap.put("user_no", map.get("user_no"));
                        mileageSaveReturnMap.put("return_amt", map.get("result_point"));

                        log.info("주문번호({}) 마일리지 적립취소요청 파라미터 ::: [{}]", purchaseOrderNo, mileageSaveReturnMap);
                        ResponseObject<String> mileageSaveReturnResponse =
                            ExternalUtil.postNonDataTransfer(
                                ExternalUrlInfo.PAYMENT_SAVE_CANCEL_MILEAGE,
                                this.createMileageSaveCancelDto(mileageSaveReturnMap, "01"),
                                String.class
                            );
                        log.info("주문번호({}) 마일리지 주문적립 취소 결과 :: {}", purchaseOrderNo, ObjectUtils.getConvertMap(mileageSaveReturnResponse));
                        mapperService.modifyClaimAccumulateReqResult(
                            new LinkedHashMap<>(){{
                                put("returnCode", mileageSaveReturnResponse.getReturnCode());
                                put("mhcSaveCancelPoint", map.get("result_point"));
                                put("claim_no", claimNo);
                                put("mhcAprDate", DateUtils.getCurrentYmd(DateUtils.DIGIT_YMD));
                            }}
                        );
                        if("0000,SUCCESS".contains(mileageSaveReturnResponse.getReturnCode())){
                            mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_Y.getCode(), pointKind);
                        } else {
                            mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_N.getCode(), pointKind);
                        }
                    } catch (Exception e) {
                        log.error("주문번호({}) 마일리지 적립취소 요청 실패 :: {}", purchaseOrderNo, e.getMessage());
                        mapperService.modifyClaimAccumulateReqSend(claimNo, ClaimCode.CLAIM_ACCUMULATE_REQ_SEND_E.getCode(), pointKind);
                    }
                }
            }
        }
        return Boolean.TRUE;
    }

    public void shipPaybackProcess(long claimNo) throws Exception {
        LinkedHashMap<String, String> claimBundleInfo = mapperService.getClaimTdBundleInfo(claimNo);
        log.info("클레임({}) 배송번호 확인 :: {}", claimNo, claimBundleInfo);
        if(claimBundleInfo != null){
            long bundleNo = ObjectUtils.toLong(claimBundleInfo.get("bundle_no"));
            LinkedHashMap<String, Object> paybackInfoMap = mapperService.getMileagePaybackCheck(bundleNo);
            log.info("클레임({}) 합배송 페이백 정보 :: {}", claimNo, paybackInfoMap);
            if(paybackInfoMap != null) {
                LinkedHashMap<String, Object> orderPriceMap = mapperService.getOrderCombinePaybackCheck(ObjectUtils.toLong(paybackInfoMap.get("org_purchase_order_no")));
                LinkedHashMap<String, Object> claimPriceMap = mapperService.getClaimCombinePaybackCheck(ObjectUtils.toLong(paybackInfoMap.get("bundle_no")), ObjectUtils.toLong(paybackInfoMap.get("org_bundle_no")));
                log.info("클레임({}) 합배송 페이백 정보 :: 주문금액[{}] / 클레임금액[{}]", claimNo, orderPriceMap, claimPriceMap);

                long orderPrice = ObjectUtils.toLong(orderPriceMap.get("payment_amt"));
                long claimPrice = ObjectUtils.toLong(claimPriceMap.get("cancel_amt"));
                long bundleFreeCondition = ObjectUtils.toLong(orderPriceMap.get("free_condition"));

                log.info("클레임({}) 페이백 회수 허들 체크 :: 클레임연산금액[{}] / 배송비허들[{}]", claimNo, (orderPrice - claimPrice), bundleFreeCondition);
                if((orderPrice - claimPrice) < bundleFreeCondition) {
                    boolean isUpdateStep = Boolean.TRUE;
                    // 상태에 따른 처리
                    if(ObjectUtils.isEquals(paybackInfoMap.get("result_yn"), "Y")){
                        try {
                            ResponseObject<String> paybackResult =
                                ExternalUtil.postNonDataTransfer(
                                    ExternalUrlInfo.PAYMENT_SAVE_CANCEL_MILEAGE,
                                    this.createMileageSaveCancelDto(paybackInfoMap, "02"),
                                    String.class
                                );
                            if(!"0000,SUCCESS".contains(paybackResult.getReturnCode())){
                                isUpdateStep = Boolean.FALSE;
                            }
                        } catch (Exception e) {
                            log.error("배송비 PAYBACK 요청 실패 :: {}", e.getMessage());
                            isUpdateStep = Boolean.FALSE;
                        }
                    }
                    // 업데이트
                    if(isUpdateStep){
                        mapperService.setShipPaybackResult(ObjectUtils.toLong(paybackInfoMap.get("payback_no")));
                    }
                }
            }
        }
    }

    public boolean claimMinusRefundMileage(LinkedHashMap<String, Object> map, long mileageAmt) {
        try{
            ExternalUtil.postTransfer(ExternalUrlInfo.PAYMENT_SAVE_MILEAGE, this.createMileageSaveRequestDto(map, mileageAmt), String.class);
        } catch (Exception e){
            log.info("마일리지 적립 실패 :: {}", e.getMessage());
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public void restoreItemForClaim(long claimNo) {
        List<RestoreStockItemDto> restoreStockItemInfo = mapperService.getRestoreStockItemInfo(claimNo);
        if(restoreStockItemInfo != null){
            try {
                for(RestoreStockItemDto itemDto : restoreStockItemInfo) {
                    itemDto.setOrderItemList(mapperService.getRestoreStockItemList(itemDto.getClaimBundleNo()));
                    log.info("클레임그룹번호({})에 대한 재고환원 요청 정보 :: {}", claimNo, itemDto);
                    ExternalUtil.postTransfer(ExternalUrlInfo.RESTORE_STOCK_FOR_CLAIM, itemDto, String.class);
                }
            } catch (Exception e) {
                log.error("재고 환원 처리중 오류가 발생되었습니다. :: {}", e.getMessage());
            }
        }
    }

    private MileageSaveRequestDto createMileageSaveRequestDto(LinkedHashMap<String, Object> map, long mileageAmt) {
        return MileageSaveRequestDto.builder()
            .mileageCode("H9007")
            .storeType(ObjectUtils.toString(map.get("store_type")))
            .requestReason("시럽쿠폰사용보상(".concat(String.valueOf(mileageAmt).concat(")")))
            .userNo(ObjectUtils.toString(map.get("user_no")))
            .mileageAmt(mileageAmt)
            .tradeNo(ObjectUtils.toString(map.get("claim_no")))
            .regId("claimBatch")
            .expireDay(0)
            .build();
    }

    private MileageSaveCancelDto createMileageSaveCancelDto(LinkedHashMap<String, Object> map, String type) {
        return MileageSaveCancelDto.builder()
            .tradeNo(ObjectUtils.toString(map.get("org_purchase_order_no")))
            .cancelType(type)
            .storeType(ObjectUtils.toString(map.get("store_type")))
            .requestReason(type.equals("02") ? "클레임에 따른 Payback 회수" : "클레이 따른 주문적립 회수")
            .userNo(ObjectUtils.toString(map.get("user_no")))
            .mileageAmt(ObjectUtils.toLong(map.get("return_amt")))
            .regId("CLAIM-BATCH")
            .build();
    }

    public void setClaimPaymentStatus(Object claimPaymentNo, String claimApprovalCd, ClaimCode claimCode, String isLastYn) throws Exception {
        if(!mapperService.setClaimPaymentStatus(Long.parseLong(String.valueOf(claimPaymentNo)), claimApprovalCd, claimCode, isLastYn)){
            throw new Exception("클레임 상태 변경중 오류가 발생되었습니다.");
        }
    }

    public void setClaimPaymentCancelTradeId(Object claimPaymentNo, Object cancelTradeId ) {
        if(!mapperService.modifyClaimPaymentPgCancelTradeId(ObjectUtils.toLong(claimPaymentNo), ObjectUtils.toString(cancelTradeId))){
            log.error("ClaimPaymentNo({}) - PG 거래취소 업데이트 실패 ", claimPaymentNo);
        }
    }

    public void setClaimPaymentStatus(long claimNo, ClaimCode claimCode) throws Exception {
        if(!mapperService.setTotalClaimPaymentStatus(claimNo, claimCode)){
            if(!claimCode.getCode().equals("P1")){
                throw new Exception("클레임 상태 변경중 오류가 발생되었습니다.");
            }
        }
    }

    public void callingClaimReg(long purchaseOrderNo, long paymentNo, long claimNo){
        mapperService.callByEndClaimFlag(ClaimInfoRegDto.builder().paymentNo(paymentNo).purchaseOrderNo(purchaseOrderNo).claimNo(claimNo).build());
    }

    public void callingClaimReg(long claimNo){
        ClaimInfoRegDto regDto = mapperService.getClaimRegData(claimNo);
        log.info("클레임그룹번호({}) claim_info Register :: {}", claimNo, regDto);
        if(regDto != null){
            mapperService.callByEndClaimFlag(regDto);
        }
    }

    public List<LinkedHashMap<String, Object>> getOrderBundleByClaim(String purchaseOrderNo) {
        return mapperService.getOrderBundleByClaim(Long.parseLong(purchaseOrderNo));
    }

    public Boolean restoreStockSlot(RestoreSlotStockDto restoreDto) throws Exception {
        try {
            LinkedHashMap<String, Object> returnMap = ExternalUtil.externalProcess(restoreDto);
            if("0000,2002,SUCCESS".contains(ObjectUtils.toString(returnMap.get("returnCode")))){
                return Boolean.TRUE;
            }
        } catch (Exception e){
            log.error("슬롯 마감 처리 오류 ::: {}", e.getMessage());
        }
        return Boolean.FALSE;
    }

    public void restoreStockByMarket(long claimNo) {
        List<RestoreStockItemDto> restoreStockItemInfo = mapperService.getRestoreStockItemInfo(claimNo);
        if(restoreStockItemInfo != null){
            try {
                for(RestoreStockItemDto itemDto : restoreStockItemInfo) {
                    itemDto.setOrderItemList(mapperService.getRestoreStockItemList(itemDto.getClaimBundleNo()));
                    log.info("클레임그룹번호({})에 대한 재고환원 요청 정보 :: {}", claimNo, itemDto);
                    ExternalUtil.postTransfer(ExternalUrlInfo.RESTORE_SLOT_CLAIM_MARKET, itemDto, String.class);
                }
            } catch (Exception e) {
                log.error("재고 환원 처리중 오류가 발생되었습니다. :: {}", e.getMessage());
            }
        }
    }

    /**
     * 클레임 상태 변경
     *
     * @param claimNo 클레임번호
     * @param claimStatus 클레임상태
     * @return Boolean 클레임상태변경 결과.
     */
    public Boolean setClaimStatus(long claimNo, String regId, String claimStatus) {
        return mapperService.setClaimStatus(createClaimStatusMap(claimNo, regId, claimStatus));
    }

    /**
     * 클레임 상태 변경
     *
     * @param claimNo 클레임번호
     * @param claimBundleNo 클레임주문번호
     * @param claimCode 클레임상태 Enum
     * @return Boolean 클레임상태변경 결과.
     */
    public Boolean setClaimStatus(long claimNo, long claimBundleNo, String regId, ClaimCode claimCode) {
        return mapperService.setClaimStatus(createClaimStatusMap(claimNo, regId, claimCode.getCode()));
    }

    private boolean getPreRefundAmtCheck(Map mstInfoMap, Map reCheckMap, String... keys){
        for(String key : keys){
            log.info("클레임번호({}) 결제수단({}) 확인 :: [기존 : {} / 재조회 : {}]", mstInfoMap.get("claimNo"), key, mstInfoMap.get(key), reCheckMap.get(key));
            if(!ObjectUtils.isCompareMap(mstInfoMap, reCheckMap, key)){
                log.info("환불예정금이 기존과 다릅니다. 금액 교체를 시도 합니다.");
                if(!mapperService.modifyClaimMstPaymentAmt((Long) mstInfoMap.get("claimNo"), key, (Long) reCheckMap.get(key))){
                    return Boolean.FALSE;
                }
                if(!mapperService.modifyClaimPaymentAmt((Long) mstInfoMap.get("claimNo"), getClaimPaymentType(key), (Long) reCheckMap.get(key))){
                    return Boolean.FALSE;
                }
            }
        }
        return Boolean.TRUE;
    }

    private String getClaimPaymentType(String key){
        return commonService.getClaimPaymentTypeCode(key.replaceAll("Amt", ""));
    }

    /**
     * 결제취소 전 환불예정금재조회
     * 재조회용 데이터
     *
     * @param claimNo 클레임그룹번호
     * @return 환불예정금액 조회 파라미터
     * @throws Exception 오류시 오류처리.
     */
    private ClaimPreRefundCalcSetDto getClaimPreRefundData (long claimNo) throws Exception {
        ClaimPreRefundCalcSetDto calcDto = mapperService.getPreRefundReCheckData(claimNo);
        if(calcDto == null) {
            throw new Exception("환불금재조회 할 데이터가 없습니다.");
        }
        calcDto.setClaimItemList(mapperService.getPreRefundItemData(claimNo));
        return calcDto;
    }

    private LinkedHashMap<String, Object> createClaimStatusMap(long claimNo, String regId, String claimStatus) {
        return new LinkedHashMap<>(){{
            // 클레임 번호
            put("claimNo", claimNo);
            // 등록자(*수정자)
            put("regId", regId);
            // 클레임 상태
            put("claimStatus", claimStatus);
        }};
    }

    /**
     * 결제취소 응답 코드 중
     * 오류가 아닌 건 체크
     * @param paymentType 결제취소타입
     * @param responseCode 응답코드
     * @return Boolean 진행여부
     */
    private Boolean isProcessStep(String paymentType, String responseCode) {
        switch (paymentType) {
            case "PG" :
                return "AV11".contains(responseCode);
            case "MP" :
                return "3013".contains(responseCode);
            case "PP" :
                return "3218".contains(responseCode);
            case "DG" :
                return "8507".contains(responseCode);
            case "OC" :
                return "3042".contains(responseCode);
            default:
                return Boolean.FALSE;
        }
    }

    public Boolean isClaimCouponAndSlotProcess(long claimNo) throws Exception {
        // 클레임 정보
        List<LinkedHashMap<String, Object>> claimInfoMap = mapperService.getClaimInfo(claimNo);
        log.info("클레임 쿠폰재발행 & 슬롯마감 처리 데이터 ::: {}", claimInfoMap);
        // 클레임 정보가 없으면 리턴.
        if(claimInfoMap == null || claimInfoMap.size() == 0){
            return Boolean.FALSE;
        }
        long bundleNo = claimInfoMap.stream().map(map -> map.get("bundle_no")).map(ObjectUtils::toLong).distinct().collect(Collectors.toList()).get(0);
        String claimType = claimInfoMap.stream().map(map -> map.get("claim_type")).map(ObjectUtils::toString).distinct().collect(Collectors.toList()).get(0);
        // userNo
        long userNo = claimInfoMap.stream().map(map -> map.get("user_no")).map(ObjectUtils::toLong).distinct().collect(Collectors.toList()).get(0);
        long purchaseOrderNo = claimInfoMap.stream().map(map -> map.get("purchase_order_no")).map(ObjectUtils::toLong).distinct().collect(Collectors.toList()).get(0);
        // 쿠폰 재발행 건수 확인
        List<CouponReIssueDto> reIssueList = mapperService.getClaimCouponReIssueInfo(claimNo, userNo);
        // 쿠폰 재발행 건수가 있으면, 재발행.
        if(reIssueList != null && reIssueList.size() > 0){
            if(getCouponReIssueInfo(reIssueList, userNo, purchaseOrderNo)){
                mapperService.modifyClaimCouponReIssueResult(reIssueList.stream().map(CouponReIssueDto::getClaimAdditionNo).collect(Collectors.toList()));
            }
        }
        List<LinkedHashMap<String, Object>> slotCloseData = claimInfoMap.stream().filter(map -> ObjectUtils.toString(map.get("order_category")).equals("TD")).filter(map -> ObjectUtils.toString(map.get("claim_part_yn")).equals("N")).filter(map -> ObjectUtils.toString(map.get("is_close")).equals("N")).collect(Collectors.toList());
        log.info("클레임그룹번호({}) 에 대한 슬롯마감정보 :: [{}]", claimNo, slotCloseData);
        // 슬롯마감
        if(slotCloseData.size() > 0) {
            if(slotCloseData.stream().map(map -> map.get("order_type")).map(String::valueOf).noneMatch(str -> str.equals("ORD_COMBINE"))){
                RestoreSlotStockDto slotStockDto = commonService.createRestoreStockSlotDto(claimInfoMap.get(0));
                log.info("클레임그룹번호({}) 에 대한 슬롯환원요청 정보 :: [{}]", claimNo, slotStockDto);
                if(restoreStockSlot(slotStockDto)){
                    mapperService.modifyClaimBundleSlotResult(ObjectUtils.toLong(claimInfoMap.get(0).get("claim_bundle_no")));
                }
            }
        }
        return Boolean.TRUE;
    }

//    public void marketSlotClose(long claimNo) throws Exception {
//        // 클레임 정보
//        List<LinkedHashMap<String, Object>> claimInfoMap = mapperService.getClaimInfo(claimNo);
//        log.info("마켓주문 슬롯마감 처리 데이터 ::: {}", claimInfoMap);
//        List<LinkedHashMap<String, Object>> slotCloseData = claimInfoMap.stream().filter(map -> ObjectUtils.toString(map.get("order_category")).equals("TD")).filter(map -> ObjectUtils.toString(map.get("claim_part_yn")).equals("N")).filter(map -> ObjectUtils.toString(map.get("is_close")).equals("N")).collect(Collectors.toList());
//        log.info("마켓주문 클레임그룹번호({}) 에 대한 슬롯마감정보 :: [{}]", claimNo, slotCloseData);
//        // 슬롯마감
//        if(slotCloseData.size() > 0) {
//            if(slotCloseData.stream().map(map -> map.get("order_type")).map(String::valueOf).noneMatch(str -> str.equals("ORD_COMBINE"))){
//                RestoreSlotStockDto slotStockDto = commonService.createRestoreStockSlotDto(claimInfoMap.get(0));
//                log.info("마켓주문 클레임그룹번호({}) 에 대한 슬롯환원요청 정보 :: [{}]", claimNo, slotStockDto);
//                if(restoreStockByMarket(slotStockDto)){
//                    mapperService.modifyClaimBundleSlotResult(ObjectUtils.toLong(claimInfoMap.get(0).get("claim_bundle_no")));
//                } else {
//                    log.info("마켓주문 클레임그룹번호({}) 에 대한 슬롯환원 실패", claimNo);
//                }
//            }
//        }
//    }

    public Boolean marketClaimAmountModify(long claimNo) throws Exception {
        // 금액조회부 추가.
        LinkedHashMap<String, Object> marketClaimAmountMap = mapperService.getMarketClaimAmountInfo(claimNo);

        if(marketClaimAmountMap != null) {
            log.info("마켓클레임({}) 금액 업데이트 확인 {}", claimNo, marketClaimAmountMap);
            // refit클레임배송비
            long claimShipFee = ObjectUtils.toLong(marketClaimAmountMap.get("claim_ship_fee"));
            // 결제금액
            long paymentAmt = ObjectUtils.toLong(marketClaimAmountMap.get("payment_amt"));
            // 추가배송비
            long claimDeliveryFeeDemandAmount = ObjectUtils.toLong(marketClaimAmountMap.get("claim_delivery_fee_demand_amount"));
            // 마켓초도배송비
            long shipAmt = ObjectUtils.toLong(marketClaimAmountMap.get("ship_amt"));
            // 추가배송비가 존재하는지 확인하여 입력받은 데이터에 대한 보정을 해야함.
            long totAddShipAmt = ObjectUtils.toLong(marketClaimAmountMap.get("tot_add_ship_amt"));
            // 마켓 할인금액
            long itemDiscountAmt = ObjectUtils.toLong(marketClaimAmountMap.get("item_discount_amt"));
            // 초도배송비
            long shipPrice = ObjectUtils.toLong(marketClaimAmountMap.get("ship_price"));
            // 프로모션 금액
            long promoDiscountAmt = ObjectUtils.toLong(marketClaimAmountMap.get("promo_discount_amt"));
            // 마켓최종처리금액
            long tradeAmt = ObjectUtils.toLong(marketClaimAmountMap.get("trade_amt"));
            // 이전 클레임 비용
            long prevPaymentAmt = ObjectUtils.toLong(marketClaimAmountMap.get("prev_payment_amt"));
            // 클레임타입
            String claimType = ObjectUtils.toString(marketClaimAmountMap.get("claim_type"));
            // 판매자부담할인금액
            long sellerChargeDiscountAmt = ObjectUtils.toLong(marketClaimAmountMap.get("seller_charge_discount_amt"));
            // 기본배송비
            long shipFee = ObjectUtils.toLong(marketClaimAmountMap.get("ship_fee"));
            //
            long prevShipAmt = 0L;
            //
            long marketShipAmt = shipAmt;
            // 네이버할인금액
            long totalMarketDiscountAmt = this.getOrderDiscountMarketItemPrice(ObjectUtils.toString(marketClaimAmountMap.get("market_order_no")));

            LinkedHashMap<String, Object> marketClaimShipAmt = mapperService.getMarketClaimPrevShipAmt(ObjectUtils.toLong(marketClaimAmountMap.get("purchase_order_no")));
            // 이전클레임처리(완료)금액비용
            if(marketClaimShipAmt != null ) {
                prevShipAmt = ObjectUtils.toLong(marketClaimShipAmt.get("prev_ship_amt"));
            }

            log.info("[클레임그룹번호({}) / refit초도배송비 : {} / refit추가배송비 : {} / 마켓초도배송비 : {} / 마켓추가배송비 : {} / 마켓최종금액 : {} / 네이버할인금액 : {}]", claimNo, shipPrice, totAddShipAmt, shipAmt, claimDeliveryFeeDemandAmount, tradeAmt, totalMarketDiscountAmt);
            log.info("[클레임그룹번호({}) / 마켓최종금액 : {} / 이전클레임비용 : {} / 마켓추가배송비 : {} / 이전배송취소금액 : {} / 판매자부담할인액 : {}", claimNo, tradeAmt, prevPaymentAmt, claimDeliveryFeeDemandAmount, prevShipAmt, sellerChargeDiscountAmt);
            // 반품배송비 계산
            // ((네이버 주문금액 - 클레임후 네이버잔액) + 네이버반품배송비) - (클레임상품총금액 + 홈플러스부담할인총금액) 으로 배송비를 구한다.
            // 네이버에서는 클레임시 배송금액이 오지 않아 계산필요.
            shipAmt = ((tradeAmt - prevPaymentAmt) + claimDeliveryFeeDemandAmount) - (paymentAmt + sellerChargeDiscountAmt);
            // 이전 할인금액
            long prevDiscountAmt = this.getPrevMarketClaimDiscount(ObjectUtils.toString(marketClaimAmountMap.get("market_order_no")), claimNo);
            log.info("클레임그룹번호({}) 이전 할인금액 : {} / 계산전 배송비 : {} / 할인금액 : {}", claimNo, prevDiscountAmt, shipAmt, totalMarketDiscountAmt);

            if(shipAmt < 0) {
                shipAmt = shipAmt + (totalMarketDiscountAmt + prevDiscountAmt);
                // 반품배송비
                // 기존 반품배송비에서 계산된 배송비를 뺀 금액이 진짜 반품배송비.
                claimDeliveryFeeDemandAmount = claimDeliveryFeeDemandAmount + (shipAmt * -1);
                // 반품배송비금액이 0보다 적은 금액이면 배송비처리.
                // 0보다 크면 반품배송비
                if(claimDeliveryFeeDemandAmount < 0) {
                    shipAmt = (claimDeliveryFeeDemandAmount * -1);
                    claimDeliveryFeeDemandAmount = 0;
                } else {
                    shipAmt = 0;
                }
            } else if (shipAmt != 0 && shipAmt != 3000) {
                // 배송비가 0원 혹은 3000원이 아닌 경우 이전 할인금액을 더해준다.
                shipAmt = shipAmt + prevDiscountAmt;
            }

            if("EXCHANGE".contains(claimType)) {
                shipAmt = 0;
                claimDeliveryFeeDemandAmount = ObjectUtils.toLong(marketClaimAmountMap.get("claim_delivery_fee_demand_amount"));
                paymentAmt = 0;
                itemDiscountAmt = 0;
                promoDiscountAmt = 0;
            }
            log.info("[클레임그룹번호({}) / 마켓최종금액 : {} / 결제금액 : {} / 상품할인금액 : {} / 계산된배송비 : {} / 계산된추가배송비 : {}", claimNo, tradeAmt, paymentAmt, itemDiscountAmt, shipAmt, claimDeliveryFeeDemandAmount);
            // 계산된 금액이 오류일 경우에 슬랙으로 알람.
            boolean isCalculationError = Boolean.FALSE;
            if((shipAmt > 0 && shipAmt != 3000)
                || (claimDeliveryFeeDemandAmount > 0 && !"3000,4000,7000,8000".contains(String.valueOf(claimDeliveryFeeDemandAmount)))) {
                isCalculationError = Boolean.TRUE;
                // 배송비 오류
                SlackUtils.sendingForMarketClaimError(this.getMarketClaimCalculationErrorMessage(claimNo, tradeAmt, prevPaymentAmt, (paymentAmt + sellerChargeDiscountAmt), shipAmt, claimDeliveryFeeDemandAmount));
            }
            // 금액을 가지고 업데이트
            if(mapperService.marketClaimAmountModify(claimNo, ((paymentAmt + itemDiscountAmt) + shipAmt), claimDeliveryFeeDemandAmount, shipAmt, promoDiscountAmt)) {
                if("RETURN,EXCHANGE".contains(claimType)) {
                    mapperService.marketClaimBundleEncloseModify(claimNo);
                }
                // claim addition reg
                mapperService.callByClaimAdditionReg(claimNo);

                // addition에 생긴 금액이 취소하고자 하는 금액과 일치하는지 확인
                // 일치할 경우 업데이트 안함.
                // 일치하지 않을 경우 업데이트
                int claimAdditionAmt = mapperService.isClaimAdditionByAddShip(claimNo);
                log.info("클레임그룹번호({}) / 클레임AdditionAmt : {} / 계산된 추가배송비 : {}", claimNo, claimAdditionAmt, claimDeliveryFeeDemandAmount);
                if(claimAdditionAmt != claimDeliveryFeeDemandAmount) {
                    this.marketAddShipModify(claimNo, claimShipFee, (claimDeliveryFeeDemandAmount - shipFee));
                }
                if(!isCalculationError) {
                    return Boolean.TRUE;
                }
            }
            if(isCalculationError) {
                throw new Exception("계산된금액오류.");
            }
        }
        return Boolean.FALSE;
    }

    public Boolean getCouponReIssueInfo(List<CouponReIssueDto> reIssueList, long userNo, long purchaseOrderNo) throws Exception {
        try {
            ClaimCouponReIssueGetDto result = ExternalUtil.postTransfer(
                ExternalUrlInfo.CLAIM_BY_COUPON_RE_ISSUE,
                commonService.createClaimCouponReIssueDto(reIssueList, userNo, purchaseOrderNo),
                ClaimCouponReIssueGetDto.class
            );
            if(!result.getResultCode().equals("0000")){
                return Boolean.FALSE;
            }
            return Boolean.TRUE;
        } catch (Exception e){
            log.error("Coupon Re Issue Error ::: {}", e.getMessage());
            return Boolean.FALSE;
        }
    }

    private Boolean getCoopTicketCancel(ClaimCoopTicketCheckSetDto checkDto) throws Exception {
        try{
            log.debug("getCoopTicketCancel ::: {}", checkDto);
            ClaimCoopTicketCheckGetDto returnDto =
                ExternalUtil.postTransfer(
                    ExternalUrlInfo.OUTBOUND_COOP_TICKET_CANCEL_FOR_CLAIM,
                    checkDto,
                    ClaimCoopTicketCheckGetDto.class
                );
            return "00,12".contains(returnDto.getResultCode());
        } catch (Exception e){
            log.error("getCoopTicketCancel ::: checkDto :: {} ::: ErrorMsg :: {}", checkDto, e.getMessage());
            return false;
        }
    }

    private Boolean marketAddShipModify(long claimNo, long totAddShipAmt, long claimDeliveryFeeDemandAmount) {
        long diffAmt = (claimDeliveryFeeDemandAmount - totAddShipAmt);
        return mapperService.marketClaimAddShipAmtModify(claimNo, diffAmt);
    }

    private Integer getCalculationSavePoint(String pointKind, int orderSavePoint, int cancelSaveAmt, int claimAccTargetAmt, double rate){
        BigDecimal claimTargetAmt = new BigDecimal(claimAccTargetAmt);
        int wildNumber = 10;
        // 기본적립.
        if(pointKind.equals("MHC")){
            int claimMhcSavePoint = claimTargetAmt.divide(new BigDecimal(100 * wildNumber), 0, RoundingMode.FLOOR).multiply(new BigDecimal(rate * wildNumber)).intValue();
            return (orderSavePoint - cancelSaveAmt) - claimMhcSavePoint;
        }
        return (orderSavePoint - cancelSaveAmt) - claimTargetAmt.divide(new BigDecimal(100), 0, RoundingMode.FLOOR).multiply(new BigDecimal(rate)).setScale(0, RoundingMode.FLOOR).intValue();
    }

    private void addProcessHistory(long claimNo, long purchaseOrderNo, String paymentType, String parenPaymentMethod, String responseMsg) {
        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<>();
        parameterMap.put("claimNo", claimNo);
        parameterMap.put("purchaseOrderNo", purchaseOrderNo);
        parameterMap.put("historyReason", "환불실패");
        parameterMap.put("historyDetailDesc", this.getConvertHistoryReason(paymentType, parenPaymentMethod, responseMsg));
        if(!LogUtil.addProcessHistoryMapper(parameterMap)){
            log.info("클레임그룹번호({})에 대한 결제타입({}) 오류로그 저장에 실패하였습니다.", claimNo, paymentType);
        }
    }

    private String getConvertHistoryReason(String paymentType, String parenPaymentMethod, String responseMsg) {
        StringBuilder sb = new StringBuilder();
        switch (paymentType) {
            case "PG" :
                sb.append("PG").append("(").append(parenPaymentMethod).append(")");
                break;
            case "PP" :
                sb.append("마일리지");
                break;
            case "MP" :
                sb.append("마이홈플러스포인트");
                break;
            case "OC" :
                sb.append("OK캐쉬");
                break;
            case "DG" :
                sb.append("홈플러스상품권");
                break;
            default:
                sb.append("결제취소타입미상");
                break;
        }
        sb.append(" | ").append(responseMsg);
        return sb.toString();
    }

    private Boolean isLastPaymentCancelCheck(LinkedHashMap<String, Object> map) {
        if (ObjectUtils.toLong(map.get("order_payment_amt")) <= (ObjectUtils.toLong(map.get("prev_payment_amt")) + ObjectUtils.toLong(map.get("payment_amt")))){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    private Long getOrderDiscountMarketItemPrice(String marketOrderNo) {
        LinkedHashMap<String, Object> priceMap = mapperService.getOrderDiscountMarketPrice(marketOrderNo);
        if(priceMap != null) {
            return ObjectUtils.toLong(priceMap.get("seller_charge_discount_amt"));
        }
        return 0L;
    }

    private Long getPrevMarketClaimDiscount(String marketOrderNo, long claimNo) {
        LinkedHashMap<String, Object> priceMap = mapperService.getPrevMarketClaimDiscount(marketOrderNo, claimNo);
        if(priceMap != null) {
            return ObjectUtils.toLong(priceMap.get("discount_amt"));
        }
        return 0L;
    }

    private String getMarketClaimCalculationErrorMessage(long claimNo, long tradeAmt, long prevClaimAmt, long paymentAmt, long shipAmt, long claimDeliveryFeeDemandAmount) {
        String pattern = "클레임그룹번호({0})에 대한 클레임 오류발생\n계산된클레임비용 : {1}\n이전취소클레임비용 : {2}\n취소상품금액 : {3}\n계산된 배송비 : {4}\n계산된 추가배송비 : {5}";
        return MessageFormat.format(pattern, String.valueOf(claimNo), tradeAmt, prevClaimAmt, paymentAmt, shipAmt, claimDeliveryFeeDemandAmount);
    }

}
