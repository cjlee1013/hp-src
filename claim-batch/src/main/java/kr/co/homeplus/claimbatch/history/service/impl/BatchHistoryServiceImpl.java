package kr.co.homeplus.claimbatch.history.service.impl;

import kr.co.homeplus.claimbatch.history.mapper.BatchHistoryMasterMapper;
import kr.co.homeplus.claimbatch.history.model.BatchHistoryDto;
import kr.co.homeplus.claimbatch.history.service.BatchHistoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * batch log insert, update
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class BatchHistoryServiceImpl implements BatchHistoryService {

    private final BatchHistoryMasterMapper batchHistoryMsterMapper;

    /**
     * log insert
     * @param batchHistoryDto
     * @return
     */
    public String insertBatchHistory(BatchHistoryDto batchHistoryDto){

        String batchSeq = null;
        batchHistoryMsterMapper.insertBatchHistory(batchHistoryDto);
        batchSeq = batchHistoryDto.getBatchSeq();

        return batchSeq;
    }

    /**
     * log update
     * @param batchHistoryDto
     */
    public int updateBatchHistory(BatchHistoryDto batchHistoryDto){

        return batchHistoryMsterMapper.updateBatchHistory(batchHistoryDto);
    }
}
