package kr.co.homeplus.claimbatch.claim.entity.dms;

public class ItmPartnerSellerShopEntity {
    //
    public String partnerId;
    //
    public String shopNm;
    //
    public String shopUrl;
    //
    public String shopProfileImg;
    //
    public String shopLogoImg;
    //
    public String shopLogoYn;
    //
    public String shopInfo;
    //
    public String csInfo;
    //
    public String csUseYn;
    //
    public String shopDispType;
    //
    public String useYn;
    //
    public String regDt;
    //
    public String regId;
    //
    public String chgDt;
    //
    public String chgId;

}
