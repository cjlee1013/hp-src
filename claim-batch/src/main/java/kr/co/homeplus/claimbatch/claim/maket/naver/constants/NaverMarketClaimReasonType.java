package kr.co.homeplus.claimbatch.claim.maket.naver.constants;

import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum NaverMarketClaimReasonType {

    CLAIM_CANCEL_01("C002", "PRODUCT_UNSATISFIED"),
    CLAIM_CANCEL_02("C009", "DELAYED_DELIVERY"),
    CLAIM_CANCEL_03("C007", "SOLD_OUT"),
    CLAIM_CANCEL_04("C004", "INTENT_CHANGED"),
    CLAIM_CANCEL_05("C008", "COLOR_AND_SIZE"),
    CLAIM_CANCEL_06("C008", "WRONG_ORDER"),
    CLAIM_CANCEL_07("C009", "INCORRECT_INFO"),
    CLAIM_CANCEL_08("C009", "BROKEN"),
    CLAIM_CANCEL_09("C009", "DROPPED_DELIVERY"),
    CLAIM_CANCEL_10("C009", "ETC"),
    CLAIM_CANCEL_11("C009", "WRONG_DELIVERY"),
    CLAIM_CANCEL_12("C009", "WRONG_OPTION"),
    CALIM_RETURN_01("R002", "INTENT_CHANGED"),
    CALIM_RETURN_02("R009", "COLOR_AND_SIZE"),
    CALIM_RETURN_03("R009", "WRONG_ORDER"),
    CALIM_RETURN_04("R010", "PRODUCT_UNSATISFIED"),
    CALIM_RETURN_05("R002", "INTENT_CHANGED"),
    CALIM_RETURN_06("R009", "COLOR_AND_SIZE"),
    CALIM_RETURN_07("R009", "WRONG_ORDER"),
    CALIM_RETURN_08("R010", "PRODUCT_UNSATISFIED"),
    CALIM_RETURN_09("R011", "DELAYED_DELIVERY"),
    CALIM_RETURN_10("R001", "SOLD_OUT"),
    CALIM_RETURN_11("R005", "DROPPED_DELIVERY"),
    CALIM_RETURN_12("R003", "BROKEN"),
    CALIM_RETURN_13("R010", "INCORRECT_INFO"),
    CALIM_RETURN_14("R006", "WRONG_DELIVERY"),
    CALIM_RETURN_15("R006", "WRONG_OPTION"),
    CLAIM_EXCHANGE_01("X005", "COLOR_AND_SIZE"),
    CLAIM_EXCHANGE_02("X005", "WRONG_ORDER"),
    CLAIM_EXCHANGE_03("X006", "PRODUCT_UNSATISFIED"),
    CLAIM_EXCHANGE_04("X006", "DELAYED_DELIVERY"),
    CLAIM_EXCHANGE_05("X006", "SOLD_OUT"),
    CLAIM_EXCHANGE_06("X006", "DROPPED_DELIVERY"),
    CLAIM_EXCHANGE_07("X002", "BROKEN"),
    CLAIM_EXCHANGE_08("X006", "INCORRECT_INFO"),
    CLAIM_EXCHANGE_09("X003", "WRONG_DELIVERY"),
    CLAIM_EXCHANGE_10("X003", "WRONG_OPTION"),
    ;

    private final String refitClaimReasonType;
    private final String naverClaimRequestReasonType;

    public static String getClaimReasonTypeByRefit(String claimType, String naverClaimRequestReasonType) {
        return Stream.of(NaverMarketClaimReasonType.values()).filter(e -> e.name().contains(claimType)).filter(e -> e.naverClaimRequestReasonType.equals(naverClaimRequestReasonType)).iterator().next().refitClaimReasonType;
    }

    public static String getClaimReasonTypeByNaver(String claimType, String refitClaimReasonType) {
        return Stream.of(NaverMarketClaimReasonType.values()).filter(e -> e.name().contains(claimType)).filter(e -> e.refitClaimReasonType.equals(refitClaimReasonType)).iterator().next().naverClaimRequestReasonType;
    }

}
