package kr.co.homeplus.claimbatch.claim.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.claimbatch.claim.sql.ShippingSql;
import kr.co.homeplus.claimbatch.core.db.annotation.SlaveConnection;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

@SlaveConnection
public interface ShippingReadMapper {

    @SelectProvider(type = ShippingSql.class, method = "selectRequestWithdrawalOfUnreceivedDelivery")
    List<LinkedHashMap<String, Object>> selectRequestWithdrawalOfUnreceivedDelivery(@Param("processDt") String processDt);

    @SelectProvider(type = ShippingSql.class, method = "selectNonShipCompleteInfo")
    LinkedHashMap<String, Object> selectNonShipCompleteInfo(@Param("purchaseOrderNo") long purchaseOrderNo, @Param("bundleNo") long bundleNo);

    @SelectProvider(type = ShippingSql.class, method = "selectNonReceivedShippingComplete")
    List<LinkedHashMap<String, Object>> selectNonReceivedShippingComplete(@Param("processDt") String processDt);
}
