package kr.co.homeplus.claimbatch.claim.model.register;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.claimbatch.claim.model.ClaimPreRefundCalcSetDto.ClaimPreRefundItemList;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClaimRegSetDto {

    @ApiModelProperty(value = "주문번호", position = 1)
    private String purchaseOrderNo;
    @ApiModelProperty(value = "배송번호", position = 2)
    private String bundleNo;
    @ApiModelProperty(value = "상품번호", position = 3)
    private List<ClaimPreRefundItemList> claimItemList;
    @ApiModelProperty(value = "귀책사유(B:고객/S:판매자/H:홈플러스)", position = 4)
    private String whoReason;
    @ApiModelProperty(value = "취소타입(O:주문취소/R:반품/X:교환)", position = 5)
    private String cancelType;
    @ApiModelProperty(value = "클레임타입(C:취소/R:반품/X:교환)", position = 6)
    private String claimType;
//    @ApiModelProperty(value = "유저아이디", position = 7)
//    private String userNo;
    @ApiModelProperty(value = "클레임사유코드", position = 8)
    private String claimReasonType;
    @ApiModelProperty(value = "클레임사유상세", position = 9)
    private String claimReasonDetail;
    @ApiModelProperty(value = "등록아이디", position = 10)
    private String regId;
    @ApiModelProperty(value = "부분취소여부", position = 11)
    private String claimPartYn;
    @ApiModelProperty(value = "주문취소여부", position = 12)
    private String orderCancelYn;
    @ApiModelProperty(value = "클레임상세데이터(교환/반품)", position = 13)
    private ClaimRegDetailDto claimDetail;
    @ApiModelProperty(value = "행사미차감 여부(Y:미차감,N:차감)", position = 14)
    private String piDeductPromoYn;
    @ApiModelProperty(value = "할인 미차감 여부(Y:미차감,N:차감)", position = 15)
    private String piDeductDiscountYn;
    @ApiModelProperty(value = "배송비 미차감 여부(Y:미차감,N:차감)", position = 16)
    private String piDeductShipYn;
}
