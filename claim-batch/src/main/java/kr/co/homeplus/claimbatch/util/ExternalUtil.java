package kr.co.homeplus.claimbatch.util;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import kr.co.homeplus.claimbatch.claim.model.payment.RestoreSlotStockDto;
import kr.co.homeplus.claimbatch.claim.model.payment.dgv.ClaimDgvCancelGetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.dgv.ClaimDgvCancelSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.mhc.ClaimMhcCancelGetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.mhc.ClaimMhcCancelSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.mhc.SaveCancelMhcGetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.mhc.SaveCancelMhcPartSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.mhc.SaveCancelMhcSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.mileage.ClaimMileageCancelGetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.mileage.ClaimMileageCancelSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.ocb.ClaimOcbCancelGetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.ocb.ClaimOcbCancelSetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.pg.ClaimPgCancelGetDto;
import kr.co.homeplus.claimbatch.claim.model.payment.pg.ClaimPgCancelSetDto;
import kr.co.homeplus.claimbatch.enums.ExternalUrlInfo;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.exception.ResourceClientException;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.ResolvableType;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class ExternalUtil {

    private final ResourceClient sourceResourceClient;
    private static ResourceClient resourceClient;

    @PostConstruct
    private void initialize(){
        resourceClient = sourceResourceClient;
    }

    /**
     * 마일리지 취소 처리
     *
     * @param claimMileageCancelSetDto 마일리지 취소 DTO
     * @return ClaimMileageCancelGetDto 마일리지 취소 응답 DTO
     * @throws Exception 오류시 오류처리 넘김
     */
    public static LinkedHashMap<String, Object> externalCancelMileage(
        ClaimMileageCancelSetDto claimMileageCancelSetDto) throws Exception {
        return ObjectUtils.getConvertMap(postTransfer(ExternalUrlInfo.PAYMENT_CANCEL_MILEAGE, claimMileageCancelSetDto, ClaimMileageCancelGetDto.class));
    }

    /**
     * 외부 통신용
     *
     * @param dtoObject 보내고자하는 데이터
     * @return 통시결과
     * @throws Exception 오류발생시 오류처리
     */
    public static LinkedHashMap<String, Object> externalProcess(Object dtoObject) throws Exception {
        if(dtoObject instanceof ClaimMileageCancelSetDto) {
            log.info("마일리지 결제취소 요청정보 :: {}", dtoObject);
            return ObjectUtils.getConvertMap(postTransfer(ExternalUrlInfo.PAYMENT_CANCEL_MILEAGE, dtoObject, ClaimMileageCancelGetDto.class));
        } else if (dtoObject instanceof ClaimPgCancelSetDto) {
            log.info("PG 결제취소 요청정보 :: {}", dtoObject);
            return ObjectUtils.getConvertMap(postTransfer(ExternalUrlInfo.PAYMENT_CANCEL_PG, dtoObject, ClaimPgCancelGetDto.class));
        } else if(dtoObject instanceof RestoreSlotStockDto) {
            return ObjectUtils.getConvertMap(getNonDataTransfer(ExternalUrlInfo.RESTORE_SLOT_CLAIM, convertDtoToSetParameter(dtoObject), String.class));
        } else if(dtoObject instanceof ClaimMhcCancelSetDto) {
            if(((ClaimMhcCancelSetDto) dtoObject).getPartCancelYn().equals("Y")){
                log.info("MHC 부분결제취소 요청정보 :: {}", dtoObject);
                return ObjectUtils.getConvertMap(postTransfer(ExternalUrlInfo.PAYMENT_CANCEL_MHC_PART, dtoObject, ClaimMhcCancelGetDto.class));
            }
            log.info("MHC 결제취소 요청정보 :: {}", dtoObject);
            return ObjectUtils.getConvertMap(postTransfer(ExternalUrlInfo.PAYMENT_CANCEL_MHC_ALL, dtoObject, ClaimMhcCancelGetDto.class));
        } else if(dtoObject instanceof ClaimOcbCancelSetDto) {
            log.info("OCB 결제취소 요청정보 :: {}", dtoObject);
            return ObjectUtils.getConvertMap(postTransfer(ExternalUrlInfo.PAYMENT_CANCEL_OCB, dtoObject, ClaimOcbCancelGetDto.class));
        } else if(dtoObject instanceof SaveCancelMhcSetDto) {
            log.info("MHC 적립취소 요청정보 :: {}", dtoObject);
            return ObjectUtils.getConvertMap(postTransfer(ExternalUrlInfo.PAYMENT_SAVE_CANCEL_MHC_ALL, dtoObject, SaveCancelMhcGetDto.class));
        } else if(dtoObject instanceof SaveCancelMhcPartSetDto) {
            log.info("MHC 부분적립취소 요청정보 :: {}", dtoObject);
            return ObjectUtils.getConvertMap(postTransfer(ExternalUrlInfo.PAYMENT_SAVE_CANCEL_MHC_PART, dtoObject, SaveCancelMhcGetDto.class));
        } else if(dtoObject instanceof ClaimDgvCancelSetDto) {
            log.info("DGV 결제취소 요청정보 :: {}", dtoObject);
            return ObjectUtils.getConvertMap(postTransfer(ExternalUrlInfo.PAYMENT_CANCEL_DGV, dtoObject, ClaimDgvCancelGetDto.class));
        } else {
            // 처리 필요asa
            return null;
        }
    }

    /**
     * 외부연동 - METHOD.GET
     * GET방식으로 외부연동을 한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public static <T> T getTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return getResponseObject(getTransfer(apiInfo, parameter, null, createType(returnClass)));
    }

    public static <T> ResponseObject<T> getNonDataTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return getTransfer(apiInfo, parameter, null, createType(returnClass));
    }

    public static <T> ResponseObject<T> postNonDataTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return postTransfer(apiInfo, parameter, null, createType(returnClass));
    }

    /**
     * 외부연동 - METHOD.GET (feat. List)
     * GET방식으로 외부연동을 한다.
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getListTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(getTransfer(apiInfo, parameter, null, createListType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header, List / Feat. ResponseObject)
     * GET 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> ResponseObject<List<T>> getResponseTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) getResponseObject(getTransfer(apiInfo, parameter, null, createResponseType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header)
     * GET방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public static <T> T getTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return getResponseObject(getTransfer(apiInfo, parameter, httpHeaders, createType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header / feat. List)
     * GET방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getListTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(getTransfer(apiInfo, parameter, httpHeaders, createListType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header, List / Feat. ResponseObject)
     * GET 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> ResponseObject<List<T>> getResponseTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) getResponseObject(getTransfer(apiInfo, parameter, httpHeaders, createResponseType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST
     * POST 방식으로 외부연동을 한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public static <T> T postTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return getResponseObject(postTransfer(apiInfo, parameter, null, createType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST (Feat. List)
     * POST 방식으로 외부연동을 한다.
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> postListTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(postTransfer(apiInfo, parameter, null, createListType(returnClass)));
    }

    /**
     /**
     * 외부연동 - METHOD.POST (include. header, List / Feat. ResponseObject)
     * POST 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> ResponseObject<List<T>> postResponseTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) getResponseObject(postTransfer(apiInfo, parameter, null, createResponseType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST (include. header)
     * POST 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public static <T> T postTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return getResponseObject(postTransfer(apiInfo, parameter, httpHeaders, createType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST (include. header / feat. List)
     * POST 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> postListTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(postTransfer(apiInfo, parameter, httpHeaders, createListType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST (include. header, List / Feat. ResponseObject)
     * POST 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> ResponseObject<List<T>> postResponseTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) getResponseObject(postTransfer(apiInfo, parameter, httpHeaders, createResponseType(returnClass)));
    }

    /**
     * Get 방식 통신 모듈
     * 헤더 정보를 입력받아 해당 헤더 정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더정보
     * @param typeReference 응답받고자 하는 클레스의 ParameterizedTypeReference
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    private static <T> ResponseObject<T> getTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, ParameterizedTypeReference<ResponseObject<T>> typeReference) throws Exception {
        if(httpHeaders == null){
            httpHeaders = RequestUtils.createHttpHeaders();
        }
        ResponseObject<T> responseObject = new ResponseObject<>();
        try{
            if(parameter == null) {
                responseObject = resourceClient.getForResponseObject( apiInfo.getApiId(), apiInfo.getUri(), httpHeaders, typeReference );
            } else if(parameter instanceof List){
                responseObject = resourceClient.getForResponseObject( apiInfo.getApiId(), apiInfo.getUri() + RequestUtils.getParameter((List<SetParameter>) parameter), httpHeaders, typeReference );
            } else {
                responseObject = resourceClient.getForResponseObject( apiInfo.getApiId(), apiInfo.getUri().concat("/").concat(ObjectUtils.toString(parameter)), httpHeaders, typeReference );
            }
            log.info("통신 결과({}/{})", apiInfo.getApiId(), apiInfo.getUri());
            LogUtil.responsePrint(responseObject);
        } catch (ResourceClientException rce) {
            log.error("getTransfer Module Error ::: {}", rce.getMessage());
            if (responseObject.getReturnStatus() == 0) {
                responseObject.setReturnStatus(rce.getHttpStatus());
            }
            return (ResponseObject<T>) getResponseObject(responseObject);
        }
        return responseObject;
//        throw new LogicException(ExceptionCode.SYS_ERROR_CODE_9001);
    }

    /**
     * POST 방식 통신 모듈
     * 헤더 정보를 입력받아 해당 헤더 정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더정보
     * @param typeReference 응답받고자 하는 클레스의 ParameterizedTypeReference
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    private static <T> ResponseObject<T> postTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, ParameterizedTypeReference<ResponseObject<T>> typeReference) throws Exception {
        try{
            ResponseObject<T> responseObject = new ResponseObject<>();
            if(httpHeaders == null){
                httpHeaders = RequestUtils.createHttpHeaders();
            }
            if(parameter instanceof List){
                responseObject = resourceClient.postForResponseObject( apiInfo.getApiId(), null, apiInfo.getUri() + RequestUtils.getParameter((List<SetParameter>) parameter), httpHeaders, typeReference );
            }
            responseObject = resourceClient.postForResponseObject( apiInfo.getApiId(), parameter, apiInfo.getUri(), httpHeaders, typeReference );

            log.info("통신 결과({}/{})", apiInfo.getApiId(), apiInfo.getUri());
            LogUtil.responsePrint(responseObject);
            return responseObject;
        } catch (ResourceClientException rce) {
            rce.printStackTrace();
        }
        throw new Exception("시스템 에러");
    }

    private static <T> ParameterizedTypeReference<ResponseObject<T>> createType(Class<T> returnClass){
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, returnClass);
        return ParameterizedTypeReference.forType(resolvableType.getType());
    }

    private static <T> ParameterizedTypeReference<ResponseObject<T>> createListType(Class<T> returnClass){
        ResolvableType resolvableListType = ResolvableType.forClassWithGenerics(List.class, returnClass);
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, resolvableListType);
        return ParameterizedTypeReference.forType(resolvableType.getType());
    }

    private static <T> ParameterizedTypeReference<ResponseObject<T>> createResponseType(Class<T> returnClass){
        ResolvableType resolvableListType = ResolvableType.forClassWithGenerics(List.class, returnClass);
        ResolvableType resolvableResponseType = ResolvableType.forClassWithGenerics(ResponseObject.class, resolvableListType);
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, resolvableResponseType);
        return ParameterizedTypeReference.forType(resolvableType.getType());
    }

    private static <T> T getResponseObject(ResponseObject<T> responseObject) throws Exception {
        // responseObject Null Check
        assert responseObject != null;
        log.info("{} :: {} :: {} :: {}", responseObject.getReturnCode(), responseObject.getReturnMessage(), responseObject.getData(), responseObject.getReturnStatus());
        // httpStatus 가 200이 아닌 경우
        if(responseObject.getReturnStatus() != 200){
            throw new Exception("시스템 에러");
        }
        // Return Code Check
        if(responseObject.getReturnCode().equalsIgnoreCase("0000") || responseObject.getReturnCode().equalsIgnoreCase("SUCCESS")) {
            return responseObject.getData();
        }
        // 정상이 아닐 경우 오류 처리.
        throw new Exception(MessageFormat.format("{0}({1})]", responseObject.getReturnMessage(), responseObject.getReturnCode()));
    }

    public static List<SetParameter> convertDtoToSetParameter(Object dto) throws Exception {
        List<SetParameter> resultList = new ArrayList<>();
        SqlUtils.getConvertMap(dto).forEach((key, value) -> resultList.add(SetParameter.create(key, value)));
        return resultList;
    }

}
