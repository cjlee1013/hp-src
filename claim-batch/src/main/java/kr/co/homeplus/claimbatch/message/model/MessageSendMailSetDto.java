package kr.co.homeplus.claimbatch.message.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.claimbatch.message.model.MessageSendTalkSetDto.MessageSendBodyDto;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@ApiModel(description = "메일 발송 DTO")
public class MessageSendMailSetDto {
    @ApiModelProperty(value = "템플릿 코드", required = true)
    private String templateCode;
    @ApiModelProperty(value = "제목", required = true)
    private String subject;
    @ApiModelProperty(value = "내용", required = true)
    private String body;
    @ApiModelProperty(value = "콜백", required = true)
    private String callback;
    @ApiModelProperty(value = "발송 캠페인 명", required = true)
    @JsonAlias({"workName", "coupledWorkName"})
    protected String workName;
    @ApiModelProperty(value = "발송 캠페인 설명", required = true)
    protected String description;
    @ApiModelProperty(value = "등록자", required = true)
    protected String regId;
    @ApiModelProperty(value = "개인별 매핑 데이터", position = 2)
    private List<MessageSendBodyDto> bodyArgument;
}
