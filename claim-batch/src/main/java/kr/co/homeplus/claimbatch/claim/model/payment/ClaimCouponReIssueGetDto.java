package kr.co.homeplus.claimbatch.claim.model.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "쿠폰 재발행 결과 DTO")
public class ClaimCouponReIssueGetDto {
    @ApiModelProperty(value = "쿠폰번호", position = 1)
    private long couponNo;
    @ApiModelProperty(value = "결과", position = 2)
    private String result;
    @ApiModelProperty(value = "결과(데이터)", position = 3)
    private String data = "";
    @ApiModelProperty(value = "결과(결과코드)", position = 4)
    private String resultCode = "";
    @ApiModelProperty(value = "결과(응답건수)", position = 5)
    private int returnCnt = 0;
}
