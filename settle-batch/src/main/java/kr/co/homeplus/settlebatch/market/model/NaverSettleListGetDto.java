package kr.co.homeplus.settlebatch.market.model;

import lombok.Data;

import java.beans.ConstructorProperties;
import java.util.List;

@Data
public class NaverSettleListGetDto {
  private int totalCount;		//조회 조건에 해당하는 전체 건수
  private int responseCount;	//현재 페이지의 응답 건수
  private int totalPageCount;	//전체 페이지 개수
  private int currentPage;	//현재 페이지 번호
  private List<NaverSettleDetailGetDto> list;

  @ConstructorProperties({"totalCount","responseCount","totalPageCount","currentPage","list"})
  public NaverSettleListGetDto(int totalCount, int responseCount, int totalPageCount, int currentPage, List<NaverSettleDetailGetDto> list) {
    this.totalCount = totalCount;
    this.responseCount = responseCount;
    this.totalPageCount = totalPageCount;
    this.currentPage = currentPage;
    this.list = list;
  }
}
