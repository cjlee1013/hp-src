package kr.co.homeplus.settlebatch.payment.mapper;

import java.util.List;
import kr.co.homeplus.settlebatch.core.db.annotation.SlaveConnection;
import kr.co.homeplus.settlebatch.payment.model.PartnerSettleGetDto;
import kr.co.homeplus.settlebatch.payment.model.PgPaymentGetDto;
import kr.co.homeplus.settlebatch.payment.model.PgPaymentSetDto;
import kr.co.homeplus.settlebatch.payment.model.PgReceivableGetDto;

@SlaveConnection
public interface PaymentSlaveMapper {
    List<PartnerSettleGetDto> getPartnerSettleList(String settlePreDt);
    long getPartnerSettleSum(PgPaymentSetDto pgPaymentSetDto);
    List<PgReceivableGetDto> getPartnerSettleFail(String settleDt);
    List<PgPaymentGetDto> getPgPaymentPg(PgPaymentSetDto pgPaymentSetDto);
    List<PgPaymentGetDto> getPgPaymentCommission(PgPaymentSetDto pgPaymentSetDto);
    String getBusinessDay(String basicDt);
}
