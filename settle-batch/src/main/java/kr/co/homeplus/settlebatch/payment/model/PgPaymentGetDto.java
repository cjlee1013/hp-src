package kr.co.homeplus.settlebatch.payment.model;

import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class PgPaymentGetDto {
    private String paymentTool;
    private String basicDt;
    private long amount;
}
