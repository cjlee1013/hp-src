package kr.co.homeplus.settlebatch.spExecLog.model;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class SpExecLogSetDto {
    private String SpName;
    private int SpStep;
    private String ExecStartDt;
    private String ExecEndDt;
    private int ErrNum;
    private int ErrLine;
    private String ErrDesc;
    private int ProcCnt;
    private String Etc;
    private SimpleDateFormat dateFormat;

    public SpExecLogSetDto(String SpName) {
        this.SpName = SpName;
        this.SpStep = 1;
        this.dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        this.ExecStartDt = dateFormat.format(new Date());
        this.ExecEndDt = dateFormat.format(new Date());
    }

    public void setNextStep() {
        this.SpStep = SpStep + 1;
        this.ExecStartDt = dateFormat.format(new Date());
        this.ExecEndDt = dateFormat.format(new Date());
        this.ErrNum = 0;
        this.ErrLine = 0;
        this.ErrDesc = null;
        ProcCnt = 0;
        Etc = null;
    }

    public void setExecEndDt() {
        this.ExecEndDt = dateFormat.format(new Date());
    }

    public void setError(int ErrNum, int ErrLine, String ErrDesc){
        this.ErrNum = ErrNum;
        this.ErrLine = ErrLine;
        this.ErrDesc = ErrDesc;
    }

    public void setProcCnt(int procCnt) {
        ProcCnt = procCnt;
    }

    public void setEtc(String etc) {
        Etc = etc;
    }
}
