package kr.co.homeplus.settlebatch.payment.service;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settlebatch.payment.mapper.PaymentMasterMapper;
import kr.co.homeplus.settlebatch.payment.mapper.PaymentSlaveMapper;
import kr.co.homeplus.settlebatch.payment.model.PartnerSettleGetDto;
import kr.co.homeplus.settlebatch.payment.model.PartnerSettleSetDto;
import kr.co.homeplus.settlebatch.payment.model.PgPaymentGetDto;
import kr.co.homeplus.settlebatch.payment.model.PgPaymentSetDto;
import kr.co.homeplus.settlebatch.payment.model.PgReceivableGetDto;
import kr.co.homeplus.settlebatch.payment.model.PgReceivableSetDto;
import kr.co.homeplus.settlebatch.spExecLog.model.SpExecLogSetDto;
import kr.co.homeplus.settlebatch.spExecLog.service.SpExecLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

/**
 * settle service interface
 */
@Service
@RequiredArgsConstructor
public class PaymentService {
    private final PaymentMasterMapper paymentMasterMapper;
    private final PaymentSlaveMapper paymentSlaveMapper;
    private final SpExecLogService spExecLogService;

    @Autowired
    ResourceClient resourceClient;

    // 파트너 변경사항 지급대행 등록
    public void setChangePartner(SpExecLogSetDto spExecLogSetDto) {
        int procCnt = resourceClient.postForResponseObject("settle"
                    , null, "/batch/Payment/setChangePartner"
                    , new ParameterizedTypeReference<ResponseObject<Integer>>() {}).getData();
        spExecLogSetDto.setProcCnt(procCnt);
        spExecLogService.setSpExecLog(spExecLogSetDto);
    }

    // 신규파트너 지급대행 등록
    public void setNewPartner(SpExecLogSetDto spExecLogSetDto) {
        int procCnt = resourceClient.postForResponseObject("settle"
            , null, "/batch/Payment/setNewPartner"
            , new ParameterizedTypeReference<ResponseObject<Integer>>() {}).getData();
        spExecLogSetDto.setProcCnt(procCnt);
        spExecLogService.setSpExecLog(spExecLogSetDto);
    }

    // 지급대행 호출
    public void setPaymentRequest(SpExecLogSetDto spExecLogSetDto) {
        int procCnt = resourceClient.postForResponseObject("settle"
            , null, "/batch/Payment/setPaymentRequest"
            , new ParameterizedTypeReference<ResponseObject<Integer>>() {}).getData();
        spExecLogSetDto.setProcCnt(procCnt);
        spExecLogService.setSpExecLog(spExecLogSetDto);
    }

    // 지급결과 확인
    public void setPaymentResult(SpExecLogSetDto spExecLogSetDto) {
        int procCnt = resourceClient.postForResponseObject("settle"
            , null, "/batch/Payment/setPaymentResult"
            , new ParameterizedTypeReference<ResponseObject<Integer>>() {}).getData();
        spExecLogSetDto.setProcCnt(procCnt);
        spExecLogService.setSpExecLog(spExecLogSetDto);
    }

    // PG미수금 STEP.4/5/6 DS예치금설정/DS지급요청/PG정산요청
    public void setPartnerBalanceRequest(String basicDt, SpExecLogSetDto spExecLogSetDto) {
        String businessDay = paymentSlaveMapper.getBusinessDay(basicDt);
        if (businessDay == null) {
            setPartnerBalanceRequest("INICIS", basicDt, spExecLogSetDto);
            setPartnerBalanceRequest("TOSSPG", basicDt, spExecLogSetDto);
        }
    }

    private void setPartnerBalanceRequest(String pgKind, String basicDt, SpExecLogSetDto spExecLogSetDto) {
        long transBalance, transAmt = 0;
        long depositBalance, depositAmt = 0;
        long pgReceivableAmt;
        String mallType;
        PgReceivableGetDto pgBalanceGetDto = paymentMasterMapper.getPgBalanceAmt(pgKind);
        if (pgBalanceGetDto != null) {
            transBalance = pgBalanceGetDto.getTransBalance();
            depositBalance = pgBalanceGetDto.getDepositBalance();
            pgReceivableAmt = pgBalanceGetDto.getReceivableAmt();
        } else {
            transBalance = 0;
            depositBalance = 0;
            pgReceivableAmt = 0;
        }

        if ("INICIS".equals(pgKind)) {
            mallType = "DS";
            PgPaymentSetDto pgPaymentSetDto = new PgPaymentSetDto(pgKind,basicDt);
            long settleAmt = paymentSlaveMapper.getPartnerSettleSum(pgPaymentSetDto);
            transAmt = (-1) * settleAmt;
            depositAmt = settleAmt;
            transBalance = transBalance + transAmt;
            depositBalance = depositBalance + depositAmt;
            // 판매대행 예치금 설정
            paymentMasterMapper.setPgReceivable(
                new PgReceivableSetDto(basicDt, basicDt, pgKind,null, mallType, "3"
                    , null, (transAmt), (transBalance), (depositAmt), (depositBalance),
                    (pgReceivableAmt)));
            List<PartnerSettleGetDto> partnerSettleGetDtoList = paymentSlaveMapper.getPartnerSettleList(
                basicDt);
            PartnerSettleSetDto partnerSettleSetDto;
            String settlePayState;
            for (PartnerSettleGetDto partnerSettleGetDto : partnerSettleGetDtoList) {
                settleAmt = Long.parseLong(partnerSettleGetDto.getSettleAmt());
                if (depositBalance > settleAmt) {
                    transAmt = 0;
                    depositAmt = (-1) * settleAmt;
                } else {
                    transAmt = depositBalance - settleAmt;
                    depositAmt = (-1) * depositBalance;
                }
                if ((pgReceivableAmt) < 0) {
                    settlePayState = "4";
                } else {
                    settlePayState = "2";
                    transBalance = transBalance + transAmt;
                    depositBalance = depositBalance + depositAmt;
                    pgReceivableAmt = pgReceivableAmt + depositAmt;
                    paymentMasterMapper.setPgReceivable(
                        new PgReceivableSetDto(basicDt, basicDt, pgKind,null, mallType, "4",
                            partnerSettleGetDto.getSettleSrl(), transAmt, transBalance, depositAmt,
                            depositBalance, pgReceivableAmt));
                }
                partnerSettleSetDto = new PartnerSettleSetDto(partnerSettleGetDto.getSettleSrl(),
                    settlePayState);
                paymentMasterMapper.setPartnerSettle(partnerSettleSetDto);
            }

            spExecLogSetDto.setProcCnt(partnerSettleGetDtoList.size());
            spExecLogService.setSpExecLog(spExecLogSetDto);
        }

        mallType = "공통";
        if (transBalance > 0) {
            transAmt = (-1) * transBalance;
            transBalance = 0;
            pgReceivableAmt = pgReceivableAmt + transAmt;
        } else {
            transAmt = 0;
        }
        depositAmt = 0;
        paymentMasterMapper.setPgReceivable(new PgReceivableSetDto(basicDt,basicDt,pgKind,null,mallType,"5"
            ,null,(transAmt),(transBalance),(depositAmt),(depositBalance),(pgReceivableAmt)));
        spExecLogSetDto.setProcCnt(1);
        spExecLogService.setSpExecLog(spExecLogSetDto);
    }

    // PG미수금 STEP.1 DS예치금 회수
    public void setPartnerBalanceFail(String basicDt, String paymentDt, SpExecLogSetDto spExecLogSetDto) {
        long transBalance, transAmt = 0;
        long depositBalance, depositAmt = 0;
        long pgReceivableAmt;
        String pgKind = "INICIS";
        String mallType = "DS";
        PgReceivableGetDto pgBalanceGetDto = paymentMasterMapper.getPgBalanceAmt(pgKind);
        if (pgBalanceGetDto != null) {
            transBalance = pgBalanceGetDto.getTransBalance();
            depositBalance = pgBalanceGetDto.getDepositBalance();
            pgReceivableAmt = pgBalanceGetDto.getReceivableAmt();
        } else {
            transBalance = 0;
            depositBalance = 0;
            pgReceivableAmt = 0;
        }
        List<PgReceivableGetDto> pgReceivableGetDtoList = paymentSlaveMapper.getPartnerSettleFail(paymentDt);
        for (PgReceivableGetDto pgReceivableGetDto : pgReceivableGetDtoList) {
            transAmt = (-1) * pgReceivableGetDto.getTransAmt();
            depositAmt = (-1) * pgReceivableGetDto.getDepositAmt();
            transBalance = transBalance + transAmt;
            depositBalance = depositBalance + depositAmt;
            pgReceivableAmt = pgReceivableAmt + transAmt + depositAmt;
            paymentMasterMapper.setPgReceivable(new PgReceivableSetDto(basicDt,paymentDt,pgKind,null,mallType,"6",pgReceivableGetDto.getSettleSrl(),transAmt,transBalance,depositAmt,depositBalance,pgReceivableAmt));
        }

        spExecLogSetDto.setProcCnt(pgReceivableGetDtoList.size());
        spExecLogService.setSpExecLog(spExecLogSetDto);
    }

    public void setPgPaymentBalance(String basicDt, SpExecLogSetDto spExecLogSetDto) {
        setPgPaymentBalance("INICIS",basicDt,spExecLogSetDto);
        setPgPaymentBalance("TOSSPG",basicDt,spExecLogSetDto);
    }
    // PG미수금 STEP.2/3 결제완료/결제수수료
    private void setPgPaymentBalance(String pgKind,String basicDt,SpExecLogSetDto spExecLogSetDto){
        long transBalance, transAmt = 0;
        long depositBalance, depositAmt = 0;
        long pgReceivableAmt;
        String mallType = "공통";
        PgReceivableGetDto pgBalanceGetDto = paymentMasterMapper.getPgBalanceAmt(pgKind);
        if (pgBalanceGetDto != null) {
            transBalance = pgBalanceGetDto.getTransBalance();
            depositBalance = pgBalanceGetDto.getDepositBalance();
            pgReceivableAmt = pgBalanceGetDto.getReceivableAmt();
        } else {
            transBalance = 0;
            depositBalance = 0;
            pgReceivableAmt = 0;
        }
        String balanceCode,paymentTool,paymentDt;
        PgPaymentSetDto pgPaymentSetDto = new PgPaymentSetDto(pgKind,basicDt);
        List<PgPaymentGetDto> pgPaymentGetDtoList = paymentSlaveMapper.getPgPaymentPg(pgPaymentSetDto);
        for (PgPaymentGetDto pgPaymentGetDto : pgPaymentGetDtoList) {
            paymentTool = pgPaymentGetDto.getPaymentTool();
            paymentDt = pgPaymentGetDto.getBasicDt();
            switch (paymentTool) {
                case "CARD":
                    balanceCode = "11";
                    break;
                case "RBANK":
                    balanceCode = "12";
                    break;
                default:
                    balanceCode = "14";
                    break;
            }
            transAmt = pgPaymentGetDto.getAmount();
            transBalance = transBalance + transAmt;
            pgReceivableAmt = pgReceivableAmt + transAmt;
            paymentMasterMapper.setPgReceivable(new PgReceivableSetDto(basicDt,paymentDt,pgKind,paymentTool,mallType,balanceCode,null,transAmt,transBalance,depositAmt,depositBalance,pgReceivableAmt));
        }
        spExecLogSetDto.setProcCnt(pgPaymentGetDtoList.size());
        spExecLogService.setSpExecLog(spExecLogSetDto);

        pgPaymentGetDtoList = paymentSlaveMapper.getPgPaymentCommission(pgPaymentSetDto);
        for (PgPaymentGetDto pgPaymentGetDto : pgPaymentGetDtoList) {
            paymentTool = pgPaymentGetDto.getPaymentTool();
            paymentDt = pgPaymentGetDto.getBasicDt();
            switch (paymentTool) {
                case "CARD":
                    balanceCode = "21";
                    break;
                case "RBANK":
                    balanceCode = "22";
                    break;
                default:
                    balanceCode = "24";
                    break;
            }
            transAmt = (-1) * pgPaymentGetDto.getAmount();
            transBalance = transBalance + transAmt;
            pgReceivableAmt = pgReceivableAmt + transAmt;
            paymentMasterMapper.setPgReceivable(new PgReceivableSetDto(basicDt,paymentDt,pgKind,paymentTool,mallType,balanceCode,null,transAmt,transBalance,depositAmt,depositBalance,pgReceivableAmt));
        }

        spExecLogSetDto.setProcCnt(pgPaymentGetDtoList.size());
        spExecLogService.setSpExecLog(spExecLogSetDto);
    }
}
