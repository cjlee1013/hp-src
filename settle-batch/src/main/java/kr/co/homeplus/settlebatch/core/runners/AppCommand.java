package kr.co.homeplus.settlebatch.core.runners;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import kr.co.homeplus.settlebatch.market.enums.SettlePeriodType;
import kr.co.homeplus.settlebatch.market.service.MarketService;
import kr.co.homeplus.settlebatch.report.service.ReportService;
import kr.co.homeplus.settlebatch.spExecLog.model.SpExecLogSetDto;
import kr.co.homeplus.settlebatch.payment.service.PaymentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.ExitCode;
import picocli.CommandLine.IExitCodeExceptionMapper;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.util.concurrent.Callable;

@Component
@RequiredArgsConstructor
@Slf4j
@Command(name = "java -jar settleBatch.jar", mixinStandardHelpOptions = true,
        version = "0.0.1",
        description = "example command")
public class AppCommand implements Callable<Integer>, IExitCodeExceptionMapper {

    private final PaymentService paymentService;
    private final ReportService reportService;
    private final MarketService marketService;

    @ArgGroup(exclusive = true, multiplicity = "1")
    private Exclusive exclusive;

    static class Exclusive {
        @Option(names = {"-n", "--name"}, description = "service name")
        private boolean isName;
    }

    @Parameters(index = "0", paramLabel = "service_name", description = "positional params")
    private String serviceName;

    @Parameters(index = "1", paramLabel = "basicDt", description = "basicDt", defaultValue = "")
    private String basicDt;

    @Override
    public Integer call() {
        log.info("exclusive.isName :: "+ exclusive.isName);
        if(exclusive.isName) {
            SpExecLogSetDto spExecLogSetDto = new SpExecLogSetDto("SettleBatch-" + serviceName);
            String paymentDt;
            ZoneId zoneid = ZoneId.systemDefault();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
            switch (serviceName) {
                case "setPartnerRegister":
                    // 지급대행 파트너 등록 (2시)
                    paymentService.setNewPartner(spExecLogSetDto);
                    break;
                case "setPartnerChange":
                    // 지급대행 파트너 수정 (1시간 간격)
                    paymentService.setChangePartner(spExecLogSetDto);
                    break;
                case "setPaymentRequest":
                    // 지급대행 요청 (12시 35분)
                    paymentService.setPaymentRequest(spExecLogSetDto);
                    break;
                case "setPaymentResult":
                    // 지급결과 확인 (20시)
                    paymentService.setPaymentResult(spExecLogSetDto);
                    break;
                case "setBalance":
                    basicDt = LocalDateTime.ofInstant(Instant.now(),zoneid).format(formatter);
                    paymentDt = LocalDateTime.ofInstant(Instant.now().minus(1,ChronoUnit.DAYS),zoneid).format(formatter);
                    // STEP.1 DS예치금 회수
                    paymentService.setPartnerBalanceFail(basicDt,paymentDt, spExecLogSetDto);
                    // STEP.2/3 결제완료/결제수수료
                    paymentService.setPgPaymentBalance(basicDt, spExecLogSetDto);
                    // STEP.4/5/6 DS예치금설정/DS지급요청/PG정산요청
                    paymentService.setPartnerBalanceRequest(basicDt, spExecLogSetDto);
                    break;
                case "setMarginReport":
                    // DC/DS 마진 보고서
                    basicDt = LocalDateTime.ofInstant(Instant.now().minus(1,ChronoUnit.DAYS),zoneid).format(formatter);
                    reportService.setMarginReport(basicDt, spExecLogSetDto);
                    break;
                case "setMarketNaverList":
                    if ("".equals(basicDt)) {
                      basicDt = LocalDateTime.ofInstant(Instant.now(),zoneid).format(formatter);
                    }
                    String periodType = SettlePeriodType.SETTLE_COMPLETE_DATE.getCode();
                    marketService.setNaverSettleList(basicDt,periodType,spExecLogSetDto);
                    marketService.setNaverToSettle(basicDt,periodType,spExecLogSetDto);
                    marketService.setMarketSettleDiffAmt(basicDt,"naver",spExecLogSetDto);
                    break;
                case "setMarketElevenList":
                    if ("".equals(basicDt)) {
                        basicDt = LocalDateTime.ofInstant(Instant.now(),zoneid).format(formatter);
                    }
                    marketService.set11stSettleList(basicDt,spExecLogSetDto);
                    marketService.set11stToSettle(basicDt,spExecLogSetDto);
                    marketService.setMarketSettleDiffAmt(basicDt,"eleven",spExecLogSetDto);
                    break;
                case "setMarketSettleDiffCommission":
                    if ("".equals(basicDt)) {
                        basicDt = LocalDateTime.ofInstant(Instant.now().minus(1,ChronoUnit.DAYS),zoneid).format(formatter);
                    }
                    marketService.setMarketSettleDiffCommission(basicDt,spExecLogSetDto);
                    break;
                case "setMarketDiffManual":
                    for (Instant i = Instant.parse("2021-12-20T00:00:00Z");i.compareTo(Instant.now())<1;i=i.plus(1,ChronoUnit.DAYS)) {
                        basicDt = LocalDateTime.ofInstant(i, zoneid).format(formatter);
                        marketService.setMarketSettleDiffAmt(basicDt,"naver",spExecLogSetDto);
                    }
                    break;
                default:
                    break;
            }
        }
        return ExitCode.OK;
    }

    @Override
    public int getExitCode(Throwable exception) {
        Throwable cause = exception.getCause();
        if (cause instanceof ArrayIndexOutOfBoundsException) {
            return 12;
        } else if (cause instanceof NumberFormatException) {
            return 13;
        }
        return 11;
    }

}