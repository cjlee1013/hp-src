package kr.co.homeplus.settlebatch.market.model;

import lombok.Data;

@Data
public class NaverSettleDetailGetDto {
  private String payDate;  								//결제 일자
  private String settleBasisDate;  						//정산 기준일(구매 확정이 되는 일자)
  private String settleExpectDate;						//정산 예정일(정산 금액이 입금될 예정일)
  private String settleCompleteDate;						//정산 완료일(정산 금액에 실제로 은행 시스템을 통해 입금 완료 처리된 일자)
  private String orderId;									//주문 번호
  private String productOrderId;							//상품 주문 번호
  private String productOrderType;						//정산 대상 구분(상품주문, 배송비, 기타비용)
  private String settleType;								//정산 상태 구분(정산 , 정산전 취소 , 정산후 취소)
  private String productId;								//상품 번호
  private String productName;								//상품명
  private String purchaserName;							//구매자명
  private long paySettleAmount;						//결제 정산 금액
  private long totalCommissionAmount;				//총 수수료 금액
  private long totalPayCommissionAmount;			//총 결제 수수료 금액
  private String primaryPayMeans;							//주 결제 수단
  private long primaryPayMeansBasisAmount;			//주 결제 수단 기준 금액
  private long primaryPayMeansPayCommissionAmount;	//주 결제 수단 결제 수수료 금액
  private String subPayMeans;								//보조 결제 수단
  private long subPayMeansBasisAmount;				//보조 결제 수단 기준 금액
  private long subPayMeansPayCommissionAmount;		//보조 결제 수단 결제 수수료 금액
  private String sub2PayMeans;							//보조 결제 수단2
  private long sub2PayMeansBasisAmount;				//보조 결제 수단2 기준 금액
  private long sub2PayMeansPayCommissionAmount;		//보조 결제 수단2 결제 수수료 금액
  private String sub3PayMeans;							//보조 결제 수단3
  private long sub3PayMeansBasisAmount;				//보조 결제 수단3 기준 금액3
  private long sub3PayMeansPayCommissionAmount;		//보조 결제 수단3 결제 수수료 금액
  private long channelCommissionAmount;				//채널 수수료 금액
  private long freeInstallmentCommissionAmount;		//판매자 부담 무이자 할부 수수료 금액
  private long saleCommissionAmount;				//(구)판매 수수료
  private long sellingInterlockCommissionAmount;	//매출 연동 수수료
  private long benefitSettleAmount;					//혜택 정산 금액
  private long settleExpectAmount;					//정산 예정 금액
  private String quickSettleCreated; 					//주문형페이 : 빠른정산여부, 결제형페이 : 해당없음
  private long quickSettleAmount;					// 빠른 정산 금액
  private long quickSettleDeductionAmount;			// 빠른 정산 지금액 공제
}
