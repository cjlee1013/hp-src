package kr.co.homeplus.settlebatch.market.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
public class NaverSettleSetDto {
  private String periodType;
  private String startDate;
  private String endDate;
  private int pageNumber;
}
