package kr.co.homeplus.settlebatch.market.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum SettlePeriodType {
    PAY_DATE("PAY_DATE", "결제완료일/취소완료일"),
    SETTLE_BASIS_DATE("SETTLE_BASIS_DATE", "정산 기준일"),
    SETTLE_EXPECT_DATE("SETTLE_EXPECT_DATE", "정산 예정일"),
    SETTLE_COMPLETE_DATE("SETTLE_COMPLETE_DATE", "정산 완료일");

    private String code;
    private String desc;
}