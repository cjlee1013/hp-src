package kr.co.homeplus.settlebatch.payment.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PartnerSettleSetDto {
    private String settleSrl;
    private String settlePayState;
}
