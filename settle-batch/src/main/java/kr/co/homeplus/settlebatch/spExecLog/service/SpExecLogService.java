package kr.co.homeplus.settlebatch.spExecLog.service;


import kr.co.homeplus.settlebatch.spExecLog.mapper.SpExecLogMasterMapper;
import kr.co.homeplus.settlebatch.spExecLog.model.SpExecLogSetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class SpExecLogService {
  private final SpExecLogMasterMapper spExecLogMasterMapper;

  public void setSpExecLog(SpExecLogSetDto spExecLogSetDto){
    spExecLogSetDto.setExecEndDt();
    spExecLogMasterMapper.setSpExecLog(spExecLogSetDto);
    spExecLogSetDto.setNextStep();
  }
}
