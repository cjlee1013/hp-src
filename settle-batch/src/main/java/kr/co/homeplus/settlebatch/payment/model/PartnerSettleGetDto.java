package kr.co.homeplus.settlebatch.payment.model;

import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class PartnerSettleGetDto {
    private String settleSrl;
    private String settleAmt;
}
