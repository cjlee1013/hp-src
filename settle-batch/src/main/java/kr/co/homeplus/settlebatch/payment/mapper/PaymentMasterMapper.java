package kr.co.homeplus.settlebatch.payment.mapper;

import kr.co.homeplus.settlebatch.core.db.annotation.MasterConnection;
import kr.co.homeplus.settlebatch.payment.model.PartnerSettleSetDto;
import kr.co.homeplus.settlebatch.payment.model.PgReceivableGetDto;
import kr.co.homeplus.settlebatch.payment.model.PgReceivableSetDto;

@MasterConnection
public interface PaymentMasterMapper {
    int setPartnerSettle(PartnerSettleSetDto partnerSettleSetDto);
    int setPgReceivable(PgReceivableSetDto pgReceivableSetDto);
    PgReceivableGetDto getPgBalanceAmt(String pgKind);
}
