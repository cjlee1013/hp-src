package kr.co.homeplus.settlebatch.market.mapper;

import java.util.List;
import kr.co.homeplus.settlebatch.core.db.annotation.SlaveConnection;
import kr.co.homeplus.settlebatch.payment.model.PartnerSettleGetDto;
import kr.co.homeplus.settlebatch.payment.model.PgPaymentGetDto;
import kr.co.homeplus.settlebatch.payment.model.PgPaymentSetDto;
import kr.co.homeplus.settlebatch.payment.model.PgReceivableGetDto;
import org.apache.ibatis.annotations.Param;

@SlaveConnection
public interface MarketSlaveMapper {
    int getNaverSettleCount(@Param("basicDt") String basicDt, @Param("periodType") String periodType);
    int getMarketSettleCount(@Param("basicDt") String basicDt, @Param("siteType") String siteType);
    int getMarketDiffCount(@Param("basicDt") String basicDt, @Param("siteType") String siteType);
    int getMarketCommissionCount(String basicDt);
}
