package kr.co.homeplus.settlebatch.payment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@AllArgsConstructor
public class PgPaymentSetDto {
    private String pgKind;
    private String payScheduleDt;
}
