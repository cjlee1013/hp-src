package kr.co.homeplus.settlebatch.market.mapper;

import kr.co.homeplus.settlebatch.core.db.annotation.MasterConnection;
import kr.co.homeplus.settlebatch.market.model.NaverSettleDetailGetDto;
import kr.co.homeplus.settlebatch.market.model.Sk11stSettleItemGetDto;
import org.apache.ibatis.annotations.Param;

@MasterConnection
public interface MarketMasterMapper {
    int setNaverSettleList(NaverSettleDetailGetDto naverSettleDetailGetDto);
    int setNaverToSettle(@Param("basicDt") String basicDt, @Param("periodType") String periodType);

    int set11stSettleList(Sk11stSettleItemGetDto sk11stSettleItemGetDto);
    int set11stToSettle(String basicDt);
    int delMarketSettleDiffAmt(@Param("basicDt") String basicDt, @Param("siteType") String siteType);
    int setMarketSettleDiffAmt(@Param("basicDt") String basicDt, @Param("siteType") String siteType);
    int setMarketSettleDiffCommission(String basicDt);
}
