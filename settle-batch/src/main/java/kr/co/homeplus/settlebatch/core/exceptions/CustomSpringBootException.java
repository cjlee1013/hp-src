package kr.co.homeplus.settlebatch.core.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ExitCodeGenerator;

@Slf4j
public class CustomSpringBootException extends RuntimeException implements ExitCodeGenerator {

    private static final long serialVersionUID = 1L;

    @Override
    public int getExitCode() {
        log.error("Inside \"getExitCode\" method of CustomSpringBootException");
        return 23;
    }

    public CustomSpringBootException(String message) {
        super(message);
    }
}
