package kr.co.homeplus.settlebatch.market.service;

import java.util.List;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.settlebatch.market.mapper.MarketMasterMapper;
import kr.co.homeplus.settlebatch.market.mapper.MarketSlaveMapper;
import kr.co.homeplus.settlebatch.market.model.NaverSettleDetailGetDto;
import kr.co.homeplus.settlebatch.market.model.NaverSettleListGetDto;
import kr.co.homeplus.settlebatch.market.model.NaverSettleSetDto;
import kr.co.homeplus.settlebatch.market.model.Sk11stSettleItemGetDto;
import kr.co.homeplus.settlebatch.spExecLog.model.SpExecLogSetDto;
import kr.co.homeplus.settlebatch.spExecLog.service.SpExecLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

/**
 * settle service interface
 */
@Service
@RequiredArgsConstructor
public class MarketService {
    private final MarketMasterMapper marketMasterMapper;
    private final MarketSlaveMapper marketSlaveMapper;
    private final SpExecLogService spExecLogService;

    @Autowired
    ResourceClient resourceClient;

    // 네이버 정산대사 (지급완료)
    public void setNaverSettleList(String basicDt, String periodType, SpExecLogSetDto spExecLogSetDto) {
        NaverSettleSetDto naverSettleSetDto = new NaverSettleSetDto();
        naverSettleSetDto.setStartDate(basicDt);
        naverSettleSetDto.setEndDate(basicDt);
        naverSettleSetDto.setPeriodType(periodType);
        NaverSettleListGetDto naverSettleListGetDto;
        List<NaverSettleDetailGetDto> naverSettleDetailGetDtoList;
        int totalCount = 0;
        if (marketSlaveMapper.getNaverSettleCount(basicDt, periodType) == 0) {
            for (int currentPage = 1, totalPageCount = 100; currentPage <= totalPageCount;
                currentPage++) {
                try {
                    naverSettleSetDto.setPageNumber(currentPage);
                    naverSettleListGetDto = resourceClient.postForResponseObject("outbound"
                        , naverSettleSetDto, "/market/naver/getSettleList"
                        , new ParameterizedTypeReference<ResponseObject<NaverSettleListGetDto>>() {
                        })
                        .getData();
                } catch (Exception e) {
                    continue;
                }
                if (naverSettleListGetDto != null) {
                    if (currentPage == 1) {
                        totalPageCount = naverSettleListGetDto.getTotalPageCount();
                        totalCount = naverSettleListGetDto.getTotalCount();
                    }

                    naverSettleDetailGetDtoList = naverSettleListGetDto.getList();
                    try {
                        for (NaverSettleDetailGetDto naverSettleDetailGetDto : naverSettleDetailGetDtoList) {
                            marketMasterMapper.setNaverSettleList(naverSettleDetailGetDto);
                        }
                    } catch (Exception e) {
                        spExecLogSetDto.setEtc("업데이트 오류:" + e.getMessage());
                    }
                } else {
                    break;
                }
            }
        }
        spExecLogSetDto.setProcCnt(totalCount);
        spExecLogService.setSpExecLog(spExecLogSetDto);
    }

    public void setNaverToSettle(String basicDt, String periodType, SpExecLogSetDto spExecLogSetDto){
        try {
            if (marketSlaveMapper.getMarketSettleCount(basicDt,"naver")==0) {
                int ret = marketMasterMapper.setNaverToSettle(basicDt, periodType);
                spExecLogSetDto.setProcCnt(ret);
            }
        } catch (Exception e) {
            spExecLogSetDto.setEtc("업데이트 오류:"+e.getMessage());
        }
        spExecLogService.setSpExecLog(spExecLogSetDto);
    }

    // 11번가 정산대사 (정산완료)
    public void set11stSettleList(String basicDt, SpExecLogSetDto spExecLogSetDto) {
        List<Sk11stSettleItemGetDto> sk11stSettleItemGetDtoList;
        try {
            sk11stSettleItemGetDtoList = resourceClient.getForResponseObject("outbound"
                    , "/market/eleven/getSettleList?startTime=" + basicDt + "&endTime=" + basicDt
                    , new ParameterizedTypeReference<ResponseObject<List<Sk11stSettleItemGetDto>>>() {
                    }).getData();
        } catch (Exception e) {
            spExecLogSetDto.setEtc("연결 오류"+e.getMessage());
            spExecLogService.setSpExecLog(spExecLogSetDto);
            return;
        }
        try {
            for (Sk11stSettleItemGetDto sk11stSettleItemGetDto : sk11stSettleItemGetDtoList) {
                marketMasterMapper.set11stSettleList(sk11stSettleItemGetDto);
            }
        } catch (Exception e) {
            spExecLogSetDto.setEtc("업데이트 오류:"+e.getMessage());
        }
        spExecLogSetDto.setProcCnt(sk11stSettleItemGetDtoList.size());
        spExecLogService.setSpExecLog(spExecLogSetDto);
    }

    public void set11stToSettle(String basicDt, SpExecLogSetDto spExecLogSetDto){
        try {
            int ret = marketMasterMapper.set11stToSettle(basicDt);
            spExecLogSetDto.setProcCnt(ret);
        } catch (Exception e) {
            spExecLogSetDto.setEtc("업데이트 오류:"+e.getMessage());
        }
        spExecLogService.setSpExecLog(spExecLogSetDto);
    }

    public void setMarketSettleDiffAmt(String basicDt, String siteType, SpExecLogSetDto spExecLogSetDto){
        try {
            if (marketSlaveMapper.getMarketDiffCount(basicDt, siteType)==0) {
                int ret = marketMasterMapper.setMarketSettleDiffAmt(basicDt, siteType);
                spExecLogSetDto.setProcCnt(ret);
            }
        } catch (Exception e) {
            spExecLogSetDto.setEtc("업데이트 오류:"+e.getMessage());
        }
        spExecLogService.setSpExecLog(spExecLogSetDto);
    }

    public void setMarketSettleDiffCommission(String basicDt, SpExecLogSetDto spExecLogSetDto) {
        try {
            if (marketSlaveMapper.getMarketCommissionCount(basicDt)==0) {
                int ret = marketMasterMapper.setMarketSettleDiffCommission(basicDt);
                spExecLogSetDto.setProcCnt(ret);
            }
        }
        catch (Exception e) {
            spExecLogSetDto.setEtc("업데이트 오류:"+e.getMessage());
        }
        spExecLogService.setSpExecLog(spExecLogSetDto);
    }
}
