package kr.co.homeplus.settlebatch.report.service;

import kr.co.homeplus.settlebatch.report.mapper.ReportMasterMapper;
import kr.co.homeplus.settlebatch.spExecLog.model.SpExecLogSetDto;
import kr.co.homeplus.settlebatch.spExecLog.service.SpExecLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * settle service interface
 */
@Service
@RequiredArgsConstructor
public class ReportService {
    private final ReportMasterMapper reportMasterMapper;
    private final SpExecLogService spExecLogService;

    public void setMarginReport(String basicDt, SpExecLogSetDto spExecLogSetDto) {
        spExecLogSetDto.setProcCnt(reportMasterMapper.setMarginReport(basicDt));
        spExecLogService.setSpExecLog(spExecLogSetDto);
    }
}
