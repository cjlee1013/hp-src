package kr.co.homeplus.settlebatch.payment.model;

import lombok.Data;

@Data
public class PgReceivableGetDto {
    private String basicDt;
    private String pgKind;
    private String mallType;
    private String code;
    private String settleSrl;
    private long transAmt;
    private long transBalance;
    private long depositAmt;
    private long depositBalance;
    private long receivableAmt;
}
