package kr.co.homeplus.settlebatch.report.mapper;

import kr.co.homeplus.settlebatch.core.db.annotation.MasterConnection;
import kr.co.homeplus.settlebatch.payment.model.PartnerSettleSetDto;
import kr.co.homeplus.settlebatch.payment.model.PgReceivableGetDto;
import kr.co.homeplus.settlebatch.payment.model.PgReceivableSetDto;

@MasterConnection
public interface ReportMasterMapper {
    int setMarginReport(String basicDt);
}
