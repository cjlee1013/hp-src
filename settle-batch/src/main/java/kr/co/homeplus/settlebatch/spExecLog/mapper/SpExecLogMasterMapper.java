package kr.co.homeplus.settlebatch.spExecLog.mapper;

import kr.co.homeplus.settlebatch.core.db.annotation.MasterConnection;
import kr.co.homeplus.settlebatch.spExecLog.model.SpExecLogSetDto;

@MasterConnection
public interface SpExecLogMasterMapper {
    void setSpExecLog(SpExecLogSetDto spExecLogSetDto);
}
