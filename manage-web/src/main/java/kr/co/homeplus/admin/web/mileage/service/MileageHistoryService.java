package kr.co.homeplus.admin.web.mileage.service;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.privacylog.model.PersonalLogMethod;
import kr.co.homeplus.admin.web.mileage.model.history.MileageListDto;
import kr.co.homeplus.admin.web.mileage.model.history.MileageListGetDto;
import kr.co.homeplus.admin.web.mileage.model.history.MileageListSetDto;
import kr.co.homeplus.admin.web.order.utils.OrderLogUtil;
import kr.co.homeplus.admin.web.user.model.user.UserInfoDetailDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MileageHistoryService {

    private final ResourceClient resourceClient;

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 내역 조회 JSON
     */
    public MileageListGetDto getMileageList(MileageListSetDto listParamDto) throws Exception {
        MileageListGetDto getDtoList =
            resourceClient.postForResponseObject(
                ResourceRouteName.MILEAGE,
                listParamDto,
                "/office/history/getMileageHistoryList",
                new ParameterizedTypeReference<ResponseObject<MileageListGetDto>>() {
                })
                .getData();

        // userNo
        List<String> userList = getDtoList.getMileageHistoryList().stream().map(MileageListDto::getUserNo).distinct().collect(Collectors.toList());
        // 결과
        HashMap<String, String> getUserList = new HashMap<>();
        for (String userNo : userList) {
            ResponseObject<UserInfoDetailDto> userInfo =
                resourceClient.getForResponseObject(
                    ResourceRouteName.USERMNG, "/external/api/search/userInfo?userNo=" + userNo,
                    OrderLogUtil.createPrivacySendAndHeader(PersonalLogMethod.SELECT, userNo, "마일리지내역조회"),
                    new ParameterizedTypeReference<>() {}
                );
            if ("0000,SUCCESS".contains(userInfo.getReturnCode())) {
                getUserList.put(userNo, PrivacyMaskingUtils.maskingUserName(userInfo.getData().getUserNm()));
            }
        }
        for(MileageListDto dto : getDtoList.getMileageHistoryList()){
            dto.setUserNm(getUserList.get(dto.getUserNo()));
            dto.setUserNo(PrivacyMaskingUtils.maskingUserId(dto.getUserNo()));
        }
        return getDtoList;
    }
}
