package kr.co.homeplus.admin.web.user.model.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserInfoDetailAgreeInfo {
    @ApiModelProperty(value = "마케팅활용동의여부")
    private String marketingAgreeYn;

    @ApiModelProperty(value = "마케팅활용동의 처리일자")
    private String marketingAgreeDt;

    @ApiModelProperty(value = "이메일수신동의여부")
    private String emailAgreeYn;

    @ApiModelProperty(value = "이메일수신동의 처리일자")
    private String emailTreatDt;

    @ApiModelProperty(value = "SMS수신동의여부")
    private String smsAgreeYn;

    @ApiModelProperty(value = "SMS수신동의 처리일자")
    private String smsTreatDt;

    @ApiModelProperty(value = "우편수신동의여부")
    private String dmAgreeYn;
    @ApiModelProperty(value = "우편수신동의 처리일자")
    private String dmTreatDt;

    @ApiModelProperty(value = "영수증쿠폰수신동의여부")
    private String receiptAgreeYn;
    @ApiModelProperty(value = "영수증쿠폰수신동의 처리일자")
    private String receiptTreatDt;
}
