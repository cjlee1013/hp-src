package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.market.MarketItemBatchSetDto;
import kr.co.homeplus.admin.web.item.model.market.MarketItemSendSetDto;
import kr.co.homeplus.admin.web.item.model.market.MarketItemSettingGetDto;
import kr.co.homeplus.admin.web.item.model.market.MarketItemSettingListGetDto;
import kr.co.homeplus.admin.web.item.model.market.MarketItemSettingListParamDto;
import kr.co.homeplus.admin.web.item.model.market.MarketItemSettingSetDto;
import kr.co.homeplus.admin.web.item.model.market.MarketItemsSettingSetDto;
import kr.co.homeplus.admin.web.item.service.ItemHistService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/market")
public class MarketItemController {

    final private ResourceClient resourceClient;

    final private LoginCookieService cookieService;

    final private CodeService codeService;

    final private ExcelUploadService excelUploadService;

    @Value("${plus.resource-routes.upload.url}")
    private String upload;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    public MarketItemController(ResourceClient resourceClient,
            LoginCookieService cookieService,
            CodeService codeService,
            ItemHistService itemHistService,
            ExcelUploadService excelUploadService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
        this.excelUploadService = excelUploadService;
    }

    /**
     * 상품관리 > 마켓연동상품관리 > 외부연동 설정관리
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/marketItemSettingMain")
    public String marketItemSettingMain(Model model) {

        Map<String, List<MngCodeGetDto>> code = codeService.getCodeModel(model,
                "use_yn"       // 사용여부
                ,	"item_type"         // 상품유형
                ,   "set_yn"            // 설정여부
                ,   "reg_yn"            // 등록여부
        );

        model.addAllAttributes(RealGridHelper.createForGroup("MarketItemSettingListGridBaseInfo", MarketItemSettingListGetDto.class));

        return "/item/marketItemSettingMain";
    }

    /**
     * 상품관리 > 마켓연동상품관리 > 외부연동 설정관리 리스트 조회
     * @param listParamDto
     * @return
     */
    @ResponseBody
    @PostMapping("/getMarketItemSettingList.json")
    public List<MarketItemSettingListGetDto> getMarketItemSettingList(@RequestBody MarketItemSettingListParamDto listParamDto) {
        ResourceClientRequest<List<MarketItemSettingListGetDto>> request = ResourceClientRequest.<List<MarketItemSettingListGetDto>>postBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri("/item/market/getMarketItemSettingList")
                .postObject(listParamDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    @ResponseBody
    @GetMapping("/getMarketItemSettingDetail.json")
    public MarketItemSettingGetDto getMarketItemSettingDetail(@RequestParam(value = "itemNo") String itemNo) {

        ResourceClientRequest<MarketItemSettingGetDto> request = ResourceClientRequest.<MarketItemSettingGetDto>getBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri("/item/market/getMarketItemSettingDetail?itemNo="+itemNo)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }

    @ResponseBody
    @PostMapping("/setMarketItemSetting.json")
    public ResponseResult setMarketItemSetting(@RequestBody MarketItemSettingSetDto paramDto) {
        paramDto.setUserId(cookieService.getUserInfo().getEmpId());

        ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri("/item/market/setMarketItemSetting")
                .postObject(paramDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    @ResponseBody
    @PostMapping("/setMarketItemsSetting.json")
    public ResponseResult setMarketItemsSetting(@RequestBody MarketItemsSettingSetDto paramDto) {
        paramDto.setUserId(cookieService.getUserInfo().getEmpId());

        ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri("/item/market/setMarketItemsSetting")
                .postObject(paramDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    @ResponseBody
    @PostMapping("/setMarketItemSend.json")
    public ResponseResult setMarketItemSend(@RequestBody MarketItemSendSetDto marketItemSendSetDto) {
        ResponseResult result = new ResponseResult();
        result.setReturnMsg("전송요청 처리 되었습니다.");

        marketItemSendSetDto.getItemList().forEach(itemNo -> {
            String apiUri = StringUtil.getRequestString("/item/setItemMarketBatch", MarketItemBatchSetDto.class, MarketItemBatchSetDto.builder().itemNo(itemNo).sendType(2).userId(cookieService.getUserInfo().getEmpId()).build());
            ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>getBuilder()
                    .apiId(ResourceRouteName.ITEM)
                    .uri(apiUri)
                    .typeReference(new ParameterizedTypeReference<>() {})
                    .build();
            resourceClient.get(request, new TimeoutConfig()).getBody();
        });

        return result;
    }
}
