package kr.co.homeplus.admin.web.search.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/***
 * 명사 문법정보 : 용언화접미사 하/되/내 사용 가능 (명사 한정)
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum NounAttrCd {
    DOV("DOV","~하다"),
    BEV("BEV","~되다"),
    NEV("NEV","~내다");

    private String code;
    private String name;

    public static NounAttrCd getType(String code) {
        for(NounAttrCd cd : NounAttrCd.values()) {
            if(cd.getCode().equals(code)) {
                return cd;
            }
        }
        return null;
    }
}
