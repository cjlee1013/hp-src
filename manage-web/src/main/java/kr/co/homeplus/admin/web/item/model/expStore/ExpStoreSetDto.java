package kr.co.homeplus.admin.web.item.model.expStore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("EXPRESS 주문가능시간 관리 Set Entity")
public class ExpStoreSetDto {

    //점포코드ExpStoreSetDto
    private Integer storeId;

    //주문가능여부
    private String orderAvailYn;

    //주문가능시작시간
    private String orderStartHour;

    //주문가능시작시간
    private String orderStartMinute;

    //주문가능종료시간
    private String orderEndHour;

    //주문가능종료시간
    private String orderEndMinute;

    //배달소요시간
    private String deliveryTime;

    //주문불가사유코드
    private String stopReasonCd;

    //주문불가사유(기타입력)
    private String stopReasonTxt;

    //등록자/수정자
    private String userId;

}

