package kr.co.homeplus.admin.web.manage.model.banner;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BannerListParamDto {

    private String searchPeriodType;
    private String searchStartDt;
    private String searchEndDt;
    private String searchSiteType;
    private String searchDevice;
    private String searchLoc1Depth;
    private String searchLoc2Depth;
    private String searchLoc3Depth;
    private String searchDispYn;
    private String searchType;
    private String searchKeyword;

}
