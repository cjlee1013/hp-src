package kr.co.homeplus.admin.web.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * application.yml 설정을 바인딩한 프로퍼티<p>
 * 리얼그리드 설정 정보를 사용합니다.
 */
@Data
@Component
@ConfigurationProperties(prefix = "realgrid")
public class RealGridProperties {
    /**
     * 리얼 그리드 RootPath 경로
     */
    private String rootPath;

    /**
     * 리얼 그리드 License 경로
     */
    private String licensePath;

    /**
     * 리얼 그리드 Main Js 경로
     */
    private String mainJsPath;

    /**
     * 리얼 그리드 API Js 경로
     */
    private String apiJsPath;

    /**
     * 리얼 그리드 Jszip(Excel 관련) Js 경로
     */
    private String jsZipPath;
}
