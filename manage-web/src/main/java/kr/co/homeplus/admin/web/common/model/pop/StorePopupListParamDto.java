package kr.co.homeplus.admin.web.common.model.pop;

import java.util.List;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class StorePopupListParamDto {
	private String storeType;
	private String callBackScript;
	private List <StorePopupListGetDto> appliedStoreList;
}
