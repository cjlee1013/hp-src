package kr.co.homeplus.admin.web.manage.model.dspExpMain;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspExpMainSetParamDto {

    private Long dispId;
    private String dispType;
    private String manageNm;
    private String themeNm;
    private String themeSubNm;
    private String imgUrl;
    private String mimgUrl;
    private String tabNm;
    private int priority;
    private String useYn;
    private String dispStartDt;
    private String dispEndDt;
    private String regId;

    private List<DspExpMainItemSetParamDto> itemList;

}
