package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "검색로그관  > 블랙리스트 설정 정보 조회 수정  ")
public class OperationSearchLogBlackListRuleInfoSetParam {

    @ApiParam(value="rule no")
    private long ruleNo;
    @ApiParam(value = "서비스 유형 ( 홈플러스 : HOMEPLUS, 익스프레스 : EXP  )")
    private String serviceType;
    @ApiParam(value = "블랙리스트 유형")
    private String blacklistType;
    @ApiParam(value = "검색로그 구간(시간)")
    private long ruleHourInterval;
    @ApiParam(value = "시간 단위(분)")
    private long ruleMinuteInterval;
    @ApiParam(value = "최소 검색 빈도")
    private long ruleMinuteFrequency;
    @ApiParam(value = "반복 횟수")
    private long ruleRepeatCount;
    @ApiParam(value = "블랙리스트로 등록되는 횟수")
    private long blacklistAddCount;
    @ApiParam(value = "수정자  ")
    private String chgId;
}
