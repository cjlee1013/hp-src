package kr.co.homeplus.admin.web.item.model.sameClass;

import lombok.Data;
@Data
public class SameClassSetDto {

    private String itemNo;

    private Integer sameClassNo;

    private String useYn;

    private String regId;
}
