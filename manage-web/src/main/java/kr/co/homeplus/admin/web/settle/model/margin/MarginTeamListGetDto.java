package kr.co.homeplus.admin.web.settle.model.margin;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
public class MarginTeamListGetDto {
    @ApiModelProperty(value = "팀명")
    @RealGridColumnInfo(headText = "팀명", width = 120, sortable = true)
    private String name;

    @ApiModelProperty(value = "매출(IN VAT)")
    @RealGridColumnInfo(headText = "매출(IN VAT)", width = 120, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String completeInAmt;

    @ApiModelProperty(value = "매출(IN VAT) 비율")
    @RealGridColumnInfo(headText = "매출(IN VAT) 비율", width = 120, sortable = true, columnType =RealGridColumnType.NUMBER_C)
    private String completeInAmtRate;

    @ApiModelProperty(value = "매출(EXT VAT)")
    @RealGridColumnInfo(headText = "매출(EXT VAT)", width = 120 , sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String completeExAmt;

    @ApiModelProperty(value = "매출(EXT VAT) 비율")
    @RealGridColumnInfo(headText = "매출(EXT VAT) 비율", width = 120, sortable = true, columnType =RealGridColumnType.NUMBER_C)
    private String completeExAmtRate;

    @ApiModelProperty(value = "마진")
    @RealGridColumnInfo(headText = "마진", width = 120, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String marginAmt;

    @ApiModelProperty(value = "마진율")
    @RealGridColumnInfo(headText = "마진율", width = 120, sortable = true, columnType =RealGridColumnType.NUMBER_C)
    private String marginAmtRate;
}
