package kr.co.homeplus.admin.web.search.controller;

import kr.co.homeplus.admin.web.search.model.DeployGridDto;
import kr.co.homeplus.admin.web.search.model.DeployHistoryGetParam;
import kr.co.homeplus.admin.web.search.model.DeployHistoryGridDto;
import kr.co.homeplus.admin.web.search.model.DeploySetParamDto;
import kr.co.homeplus.admin.web.search.service.DeployService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

@Controller
@RequestMapping("/search/deploy")
@RequiredArgsConstructor
public class DeployController {

    private final DeployService deployService;

    @GetMapping(value = "/main")
    public String Main(Model model) {
        model.addAllAttributes(RealGridHelper.create("deployGridBaseInfo", DeployGridDto.DeployServer.class));
        return "/search/deploy";
    }

    @ResponseBody
    @GetMapping(value = {"/getServerList.json"})
    public ResponseObject getServerList(@RequestParam("schServiceCd") String schServiceCd, @RequestParam("schAppType") String appType) {
        return deployService.getServerList(schServiceCd, appType);
    }

    /***
     * 사전 빌드
     */
    @ResponseBody
    @GetMapping(value = {"/makeDictionaryFromTxt.json"})
    public ResponseObject makeDictionaryFromTxt(@RequestParam("schServiceCd") String schServiceCd, @RequestParam(name = "deployDictType",defaultValue = "MORPH") String deployDictType) {
        return deployService.makeDictionaryFromTxt(schServiceCd, deployDictType);
    }

    /***
     * 사전 업로드
     */
    @ResponseBody
    @GetMapping(value = {"/uploadDictionary.json"})
    public ResponseObject uploadDictionary(@RequestParam("schServiceCd") String schServiceCd, @RequestParam(name = "deployDictType",defaultValue = "MORPH") String deployDictType) {
        return deployService.uploadDictionary(schServiceCd, deployDictType);
    }

    /***
     * 배포 진행
     */
    @ResponseBody
    @GetMapping(value = {"/deploy.json"})
    public ResponseObject deployDictionary(@RequestParam("deployId") Long deployId, @RequestParam("rowId") Long rowId, @RequestParam(name = "deployDictType",defaultValue = "MORPH") String deployDictType) {
        return deployService.deployDictionary(deployId, rowId, deployDictType);
    }

    /***
     * 저장
     */
    @ResponseBody
    @RequestMapping(value = {"/setItem.json"}, method = RequestMethod.POST)
    public ResponseObject setItem(DeploySetParamDto param) {
        return deployService.setItem(param);
    }

    /***
     * 검색기 갱신
     */
    @ResponseBody
    @RequestMapping(value = {"/search_analyzer_reload.json"},method = RequestMethod.GET)
    public ResponseObject searchAnalyzerReload(@RequestParam("domain") String domain, @RequestParam("schServiceCd") String schServiceCd) {
        return deployService.searchAnalyzerReload(domain, schServiceCd);
    }

    /***
     * 배포이력조회 메인
     * @param model
     * @return
     */
    @GetMapping(value = "/history")
    public String historyMain(Model model) {
        model.addAllAttributes(RealGridHelper.create("deployHistoryGridBaseInfo", DeployHistoryGridDto.DeployHistory.class));
        return "/search/deployHistory";
    }

    /***
     * 배포이력조회 API
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/history.json"},method = RequestMethod.GET)
    public ResponseObject searchHistory(@Valid DeployHistoryGetParam param) {
        return deployService.searchHistory(param);
    }
}
