package kr.co.homeplus.admin.web.settle.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "MHC 실적조회 > 건별 상세내역")
public class MhcPaymentListGetDto {
  @ApiModelProperty(notes = "기준일")
  @RealGridColumnInfo(headText = "기준일", sortable = true, width = 100)
  private String basicDt;

  @ApiModelProperty(notes = "채널코드")
  @RealGridColumnInfo(headText = "채널코드", sortable = true, width = 100)
  private String chnlCd;

  @ApiModelProperty(notes = "POS번호")
  @RealGridColumnInfo(headText = "POS번호", sortable = true, width = 100)
  private String posNo;

  @ApiModelProperty(notes = "점포코드")
  @RealGridColumnInfo(headText = "점포코드", sortable = true, width = 100)
  private String storeId;

  @ApiModelProperty(notes = "결제번호")
  @RealGridColumnInfo(headText = "결제번호", sortable = true, width = 100)
  private String paymentNo;

  @ApiModelProperty(notes = "주문번호")
  @RealGridColumnInfo(headText = "주문번호", sortable = true, width = 100)
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "클레임그룹번호")
  @RealGridColumnInfo(headText = "클레임그룹번호", sortable = true, width = 100)
  private String claimNo;

  @ApiModelProperty(notes = "승인번호")
  @RealGridColumnInfo(headText = "승인번호", sortable = true, width = 100)
  private String mhcAprNumber;

  @ApiModelProperty(notes = "취소구분")
  @RealGridColumnInfo(headText = "취소구분", sortable = true, width = 100)
  private String gubun;

  @ApiModelProperty(notes = "가용화여부")
  @RealGridColumnInfo(headText = "가용화여부", sortable = true, width = 100)
  private String shipFshYn;

  @ApiModelProperty(notes = "매출액")
  @RealGridColumnInfo(headText = "매출액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String completeAmt;

  @ApiModelProperty(notes = "결제금액")
  @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String orderPrice;

  @ApiModelProperty(notes = "사용 포인트")
  @RealGridColumnInfo(headText = "사용 포인트", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String mhcUsePoint;

  @ApiModelProperty(notes = "승인 포인트")
  @RealGridColumnInfo(headText = "승인 포인트", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String mhcAprPoint;
}
