package kr.co.homeplus.admin.web.manage.model.gnb;

import lombok.Data;

@Data
public class GnbSetParamDto {

    private Integer gnbNo;

    private String siteType;

    private String device;

    private String dispNm;

    private Integer priority;

    private String iconKind;

    private String dispStartDt;

    private String dispEndDt;

    private String linkType;

    private String linkInfo;

    private String useYn;

    private String regId;

}
