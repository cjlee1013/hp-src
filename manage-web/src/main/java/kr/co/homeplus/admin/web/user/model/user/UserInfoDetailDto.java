package kr.co.homeplus.admin.web.user.model.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.admin.web.user.enums.UserMhcCardType;
import kr.co.homeplus.admin.web.user.enums.UserStatus;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ApiModel(description = "회원정보 관리 상세조회 DTO")
public class UserInfoDetailDto {

    @ApiModelProperty(value = "회원일련번호(userCd,회원코드)")
    private Long userNo;

    @ApiModelProperty(value = "회원이름")
    private String userNm;

    @ApiModelProperty(value = "회원아이디(SSO로그인아이디)")
    private String userId;

    public String getUserStatus() {
        return userStatus != null ? userStatus.getDesc() : null;
    }

    @ApiModelProperty(value = "회원상태")
    private UserStatus userStatus;

    @ApiModelProperty(value = "생년월일")
    private String birth;

    @ApiModelProperty(value = "성별(남|여)")
    private String gender;

    @ApiModelProperty(value = "휴대폰번호")
    private String mobile;

    @ApiModelProperty(value = "이메일")
    private String email;

    @ApiModelProperty(value = "회원구분")
    private String isCompany;

    @ApiModelProperty(value = "통합회원여부")
    private String isUnion;

    @ApiModelProperty(value = "회원등급")
    private String gradeNm;

    @ApiModelProperty(value = "본인인증여부(Y|N)")
    private String isCert;

    @ApiModelProperty(value = "본인인증일자")
    private String certDt;

    @ApiModelProperty(value = "성인인증여부(Y|N)")
    private String isAdultCertificate;

    @ApiModelProperty(value = "성인인증일자")
    private String adultCertificateDt;

    @ApiModelProperty(value = "블랙컨슈머여부(등록|해제)")
    private String isBlackConsumer;

    @ApiModelProperty(value = "개인통관부호")
    private String personalOverseaNo;

    @ApiModelProperty(value = "임직원 아이디")
    private String empId;

    @ApiModelProperty(value = "가입일시")
    private String regDt;

    @ApiModelProperty(value = "최종접속일시")
    private String lastLoginDt;

    @ApiModelProperty(value = "MHC 카드발급여부(Y/N)")
    private String isMhcCard;

    public String getMhcCardType() {
        return this.mhcCardType != null ? this.mhcCardType.getDesc() : "";
    }

    @ApiModelProperty(value = "카드발급유형")
    private UserMhcCardType mhcCardType;

    @ApiModelProperty(value = "MHC 대체카드번호 암호화")
    private String mhcCardnoEnc;

    @ApiModelProperty(value = "MHC 대체카드번호 - 마스킹처리")
    private String mhcCardno;

    @ApiModelProperty(value = "MHC 카드발급일자")
    private String mhcCardIssueDt;

    @ApiModelProperty(value = "MHC 대체카드번호 RKM HMAC")
    private String mhcCardnoHmac;

    @ApiModelProperty(value = "OCB 연동여부(Y|N)")
    private String ocbYn;

    @ApiModelProperty(value = "환불계좌 일련번호")
    private String bankSeq;

    @ApiModelProperty(value = "은행명")
    private String bankNm;

    @ApiModelProperty(value = "계좌번호 - 마스킹 처리")
    private String bankAccount;

    @ApiModelProperty(value = "계좌주명")
    private String bankOwner;

    @ApiModelProperty(value = "은행코드")
    private String bankCd;

    @ApiModelProperty(value = "마케팅활용동의여부")
    private String marketingAgreeYn;

    @ApiModelProperty(value = "마케팅활용동의 처리일자")
    private String marketingAgreeDt;

    @ApiModelProperty(value = "이메일수신동의여부")
    private String emailAgreeYn;

    @ApiModelProperty(value = "이메일수신동의 처리일자")
    private String emailTreatDt;

    @ApiModelProperty(value = "SMS수신동의여부")
    private String smsAgreeYn;

    @ApiModelProperty(value = "SMS수신동의 처리일자")
    private String smsTreatDt;

    @ApiModelProperty(value = "우편수신동의여부")
    private String dmAgreeYn;
    @ApiModelProperty(value = "우편수신동의 처리일자")
    private String dmTreatDt;

    @ApiModelProperty(value = "영수증쿠폰수신동의여부")
    private String receiptAgreeYn;
    @ApiModelProperty(value = "영수증쿠폰수신동의 처리일자")
    private String receiptTreatDt;

    @ApiModelProperty(value = "배송지 목록")
    private List<ShippingAddressDto> shippingAddressList;

    @ApiModelProperty(value = "스토어타입별 첫구매정보")
    private FirstOrderInfoDto firstOrderInfo;
}
