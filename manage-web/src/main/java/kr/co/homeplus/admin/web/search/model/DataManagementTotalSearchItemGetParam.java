package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class DataManagementTotalSearchItemGetParam {
    @NonNull
    @NotEmpty(message = "문서번호는 빈 값일 수 없습니다.")
    @ApiParam(value = "문서번호",required = true)
    private String docId;
    @NonNull
    @NotEmpty(message = "검색어는 빈 값일 수 없습니다.")
    @ApiParam(value = "검색어",required = true)
    private String keyword;
}
