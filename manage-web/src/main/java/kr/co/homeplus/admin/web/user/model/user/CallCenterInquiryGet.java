package kr.co.homeplus.admin.web.user.model.user;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * 어드민 > 회원상세 > 등급변경 > 콜센터 문의조회 DTO
 */
@Setter
@Getter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class CallCenterInquiryGet {
    @RealGridColumnInfo(headText = "문의번호", width = 80)
    private String key;

    @RealGridColumnInfo(headText = "문의번호 이력 seq", hidden = true)
    private String advisorHistorySeq;

    @RealGridColumnInfo(headText = "상담구분")
    private String advisorTypeNm1;

    @RealGridColumnInfo(headText = "상담항목", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 120)
    private String advisorTypeNm2;

    @RealGridColumnInfo(headText = "접수채널", width = 60)
    private String channelNm;

    @RealGridColumnInfo(headText = "제목", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 130)
    private String historyTitle;

    @RealGridColumnInfo(headText = "회원번호", width = 70)
    private String custId;

    @RealGridColumnInfo(headText = "회원명", width = 60)
    private String custNm;

    @RealGridColumnInfo(headText = "접수일시", width = 90)
    private String startTime;
}
