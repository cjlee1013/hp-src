package kr.co.homeplus.admin.web.mileage.service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.mileage.model.type.MileageCommonCode;
import kr.co.homeplus.admin.web.mileage.model.type.MileageTypeMngGetDto;
import kr.co.homeplus.admin.web.mileage.model.type.MileageTypeMngSetDto;
import kr.co.homeplus.admin.web.mileage.model.type.MileageTypeModifySetDto;
import kr.co.homeplus.admin.web.mileage.model.type.MileageTypeSearchPopDto;
import kr.co.homeplus.admin.web.mileage.model.type.pop.MileagePromoTypeGetDto;
import kr.co.homeplus.admin.web.mileage.model.type.pop.MileagePromoTypeSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MileageTypeMngService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 적립 타입 관리 > 메인 리스트 조회
     */
    public List<MileageTypeMngGetDto> getMileageRequestList(MileageTypeMngSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            listParamDto,
            "/office/type/getMileageTypeInfo",
            new ParameterizedTypeReference<ResponseObject<List<MileageTypeMngGetDto>>>(){}).getData();
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 타입 상세 정보 조회(Search by MileageTypeNo)
     */
    public MileageTypeModifySetDto getMileageTypeDetail(MileageTypeModifySetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            null,
            "/office/type/getMileageTypeInfo/basic/" + listParamDto.getMileageTypeNo(),
            new ParameterizedTypeReference<ResponseObject<MileageTypeModifySetDto>>(){}).getData();
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 적립 타입 관리 > 타입 등록
     */
    public ResponseObject<String> addMileageType(MileageTypeModifySetDto listParamDto) throws Exception {
        listParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            listParamDto,
            "/office/type/setMileageTypeInfo",
            new ParameterizedTypeReference<ResponseObject<String>>(){});
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 적립 타입 관리 > 타입 수정
     */
    public ResponseObject<String> modifyMileageType(MileageTypeModifySetDto listParamDto) throws Exception {
        listParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            listParamDto,
            "/office/type/modifyMileageTypeInfo",
            new ParameterizedTypeReference<ResponseObject<String>>() {});
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 유형 조회
     */
    public List<MileageTypeSearchPopDto> getMileageTypeByName(MileageTypeSearchPopDto listParamDto) throws Exception {
        listParamDto.setMileageTypeNo("유형ID");
        listParamDto.setMileageTypeName("유형명");

        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            listParamDto,
            "/office/type/getMileageTypeByName",
            new ParameterizedTypeReference<ResponseObject<List<MileageTypeSearchPopDto>>>() {}).getData();
    }

    public List<MileagePromoTypeGetDto> getMileagePromoTypePopInfo(MileagePromoTypeSetDto setDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            setDto,
            "/office/type/getMileagePromoTypeInfo",
            new ParameterizedTypeReference<ResponseObject<List<MileagePromoTypeGetDto>>>() {}).getData();
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 적립 타입 관리 > 타입 등록
     */
    public ResponseObject<String> addMileageEventType(MileageTypeModifySetDto listParamDto) throws Exception {
        listParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        listParamDto.setMileageCategory("01");
        listParamDto.setMileageKind("90");
        listParamDto.setExpireType("TR");
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            listParamDto,
            "/office/type/setMileageTypeInfo",
            new ParameterizedTypeReference<ResponseObject<String>>(){});
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 적립 타입 관리 > 타입 수정
     */
    public ResponseObject<String> modifyMileageEventType(MileageTypeModifySetDto listParamDto) throws Exception {
        listParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        listParamDto.setMileageCategory("01");
        listParamDto.setMileageKind("90");
        listParamDto.setExpireType("TR");
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            listParamDto,
            "/office/type/modifyMileageTypeInfo",
            new ParameterizedTypeReference<ResponseObject<String>>() {});
    }

    /**
     * 공통코드 조회
     *
     * @param gmcCd
     * @return
     */
    public Map<String, List<MileageCommonCode>> getCode(String... codeGroup) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ITEM,
            "/office/type/getMileageCommonCode?codeGroup=" + Arrays.stream(codeGroup).collect(Collectors.joining("&code=")),
            new ParameterizedTypeReference<ResponseObject<Map<String, List<MileageCommonCode>>>>() {}).getData();
    }

}
