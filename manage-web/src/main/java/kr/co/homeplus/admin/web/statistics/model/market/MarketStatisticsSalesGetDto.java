package kr.co.homeplus.admin.web.statistics.model.market;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "마켓별 판매현황")
public class MarketStatisticsSalesGetDto {
  @ApiModelProperty(notes = "결제일")
  @RealGridColumnInfo(headText = "결제일", sortable = true, width = 100)
  private String basicDt;

  @ApiModelProperty(notes = "매출건수_Total")
  @RealGridColumnInfo(headText = "Total", groupName = "매출건수", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String totOrderCnt;

  @ApiModelProperty(notes = "매출건수_Naver")
  @RealGridColumnInfo(headText = "N마트", groupName = "매출건수", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String naverOrderCnt;

  @ApiModelProperty(notes = "매출건수_11번가")
  @RealGridColumnInfo(headText = "11번가", groupName = "매출건수", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String elevenOrderCnt;

  @ApiModelProperty(notes = "매출금액_Total")
  @RealGridColumnInfo(headText = "Total", groupName = "매출금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String totOrderAmt;

  @ApiModelProperty(notes = "매출금액_Naver")
  @RealGridColumnInfo(headText = "N마트", groupName = "매출금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String naverOrderAmt;

  @ApiModelProperty(notes = "매출금액_11번가")
  @RealGridColumnInfo(headText = "11번가", groupName = "매출금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String elevenOrderAmt;

  @ApiModelProperty(notes = "Basket_Size_Total")
  @RealGridColumnInfo(headText = "Total", groupName = "Basket Size", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String totBasketSize;

  @ApiModelProperty(notes = "Basket_Size_Naver")
  @RealGridColumnInfo(headText = "N마트", groupName = "Basket Size", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String naverBasketSize;

  @ApiModelProperty(notes = "Basket_Size_11번가")
  @RealGridColumnInfo(headText = "11번가", groupName = "Basket Size", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String elevenBasketSize;

  @ApiModelProperty(notes = "상품수량_Total")
  @RealGridColumnInfo(headText = "Total", groupName = "상품수량", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String totItemCnt;

  @ApiModelProperty(notes = "상품수량_Naver")
  @RealGridColumnInfo(headText = "N마트", groupName = "상품수량", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String naverItemCnt;

  @ApiModelProperty(notes = "상품수량_11번가")
  @RealGridColumnInfo(headText = "11번가", groupName = "상품수량", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String elevenItemCnt;
}
