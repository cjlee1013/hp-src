package kr.co.homeplus.admin.web.pg.service;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.GridUtil;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.pg.model.ExcelItem;
import kr.co.homeplus.admin.web.pg.model.PaymentPolicyApplyDto;
import kr.co.homeplus.admin.web.pg.model.PaymentPolicyApplySelectDto;
import kr.co.homeplus.admin.web.pg.model.PaymentPolicyManageDto;
import kr.co.homeplus.admin.web.pg.model.PaymentPolicyManageSelectDto;
import kr.co.homeplus.admin.web.pg.model.PaymentPolicyMethodSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PaymentPolicyManageService {
    private final ResourceClient resourceClient;

    /**
     * 결제정책 마스터 조회
     * @param paymentPolicyManageSelectDto
     * @return
     * @throws Exception
     */
    public List<PaymentPolicyManageDto> getPaymentPolicyMngList(PaymentPolicyManageSelectDto paymentPolicyManageSelectDto) throws Exception {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_PAYMENT_POLICY_MNG_LIST, PaymentPolicyManageSelectDto.class, paymentPolicyManageSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PaymentPolicyManageDto>>>() {};

        return (List<PaymentPolicyManageDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 결제정책 결제수단 Tree 구조 조회
     * @param paymentPolicyMethodSelectDto
     * @return
     * @throws Exception
     */
    public List<Object> getPaymentPolicyMethodTree(PaymentPolicyMethodSelectDto paymentPolicyMethodSelectDto) throws Exception {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_PAYMENT_POLICY_METHOD_TREE, PaymentPolicyMethodSelectDto.class, paymentPolicyMethodSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<Map<String, Object>>>>() {};
        ResponseObject<List<Map<String, Object>>> responseObject = resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference);
        List<Map<String, Object>> responseData = responseObject.getData();

        return GridUtil.convertTreeGridData(responseData, null, "methodCode", "parentCode", "depth");
    }

    /**
     * 결제정책 저장
     * @param paymentPolicyManageDto
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> savePaymentPolicyMng(PaymentPolicyManageDto paymentPolicyManageDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_SAVE_PAYMENT_POLICY_MNG;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};

        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, paymentPolicyManageDto, apiUri, typeReference);
    }

    /**
     * 결제정책 적용대상 조회
     * @param paymentPolicyApplySelectDto
     * @return
     * @throws Exception
     */
    public List<PaymentPolicyApplyDto> getPaymentPolicyApplyList(PaymentPolicyApplySelectDto paymentPolicyApplySelectDto) throws Exception {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_PAYMENT_POLICY_APPLY_LIST, PaymentPolicyApplySelectDto.class, paymentPolicyApplySelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PaymentPolicyApplyDto>>>() {};

        return (List<PaymentPolicyApplyDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 결제정책 적용대상 저장
     * @param paymentPolicyApplyDto
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> savePaymentPolicyApply(PaymentPolicyApplyDto paymentPolicyApplyDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_SAVE_PAYMENT_POLICY_APPLY;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};

        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, paymentPolicyApplyDto, apiUri, typeReference);
    }

    /**
     * 상품 유효성 체크
     * -. 결제정책 대상상품 일괄등록 전 상품 유효성 체크
     * @param itemNoList
     * @return
     * @throws Exception
     */
    public List<ExcelItem> getItemValidCheck(List<String> itemNoList) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_ITEM_VALID_CHECK;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ExcelItem>>>() {};
        return (List<ExcelItem>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, itemNoList, apiUri, typeReference).getData();
    }
}
