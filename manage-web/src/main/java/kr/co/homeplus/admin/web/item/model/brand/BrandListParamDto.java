package kr.co.homeplus.admin.web.item.model.brand;

import lombok.Data;

@Data
public class BrandListParamDto {
    private String searchType;
    private String searchKeyword;
    private String schUseYn;
    private String callType;
}
