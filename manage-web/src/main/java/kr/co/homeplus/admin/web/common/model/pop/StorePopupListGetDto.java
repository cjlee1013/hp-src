package kr.co.homeplus.admin.web.common.model.pop;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class StorePopupListGetDto {
	private Integer storeId;
	private String storeNm;
}