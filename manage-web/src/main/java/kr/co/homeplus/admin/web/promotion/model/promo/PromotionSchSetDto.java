package kr.co.homeplus.admin.web.promotion.model.promo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PromotionSchSetDto {
    private String periodType;
    private String startDt;
    private String endDt;
    private String promotionType;
    private String useYn;
    private String dispYn;
    private String schType;
    private String schValue;
}
