package kr.co.homeplus.admin.web.manage.model.clubToday;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
@Getter
@Setter
public class ClubTodayItemListSelectDto {

    @RealGridColumnInfo(headText = "NO", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private String priority;

    @RealGridColumnInfo(headText = "상품번호", width = 120, order = 1, sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 180, order = 2, sortable = true)
    private String itemNm;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 3, sortable = true)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 4, sortable = true)
    private String regDt;

}
