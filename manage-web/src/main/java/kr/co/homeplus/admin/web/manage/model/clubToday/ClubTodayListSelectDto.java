package kr.co.homeplus.admin.web.manage.model.clubToday;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class ClubTodayListSelectDto {

    @RealGridColumnInfo(headText = "번호", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private String todayNo;

    @RealGridColumnInfo(headText = "클럽투데이 명", columnType = RealGridColumnType.NAME, width = 150, order = 1, sortable = true)
    private String todayNm;

    @RealGridColumnInfo(headText = "전시 시작일", columnType = RealGridColumnType.DATE, width = 120, order = 2, sortable = true)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시 종료일", columnType = RealGridColumnType.DATE, width = 120, order = 3, sortable = true)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "우선순위", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, order = 4, sortable = true)
    private int priority;

    @RealGridColumnInfo(headText = "전시여부", width = 50, order = 5, sortable = true)
    private String dispYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 6, sortable = true)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 7, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 8, sortable = true)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 9, sortable = true)
    private String chgDt;

    @RealGridColumnInfo(headText = "todayTitle", order = 10, hidden = true, sortable = true)
    private String todayTitle;
    @RealGridColumnInfo(headText = "pcImgUrl", order = 11, hidden = true, sortable = true)
    private String pcImgUrl;
    @RealGridColumnInfo(headText = "mobileImgUrl", order = 12, hidden = true, sortable = true)
    private String mobileImgUrl;
    @RealGridColumnInfo(headText = "dispYn", order = 13, hidden = true, sortable = true)
    private String dispYn;

}
