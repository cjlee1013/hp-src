package kr.co.homeplus.admin.web.core.certification;

import kr.co.homeplus.admin.web.core.dto.AuthMenuListDto;
import kr.co.homeplus.admin.web.core.dto.UserInfo;

import java.util.List;

/**
 * 세션관련 기능을 제공하는 서비스의 구현체
 */
public interface SessionService {
    /**
     * 사용자 세션 유효성 검증(세션 유효기간)
     *
     * @return
     * @throws Exception
     */
    boolean isCertification() throws Exception;
    boolean setCertification(String rmsToken) throws Exception;
    boolean setCertification(String rmsToken, String sessionKey) throws Exception;

    /**
     * 세션 값 설정
     *
     * @param object 세션에 저장할 값
     * @param sessionKey 세션 키
     * @return
     * @throws Exception
     */
    boolean setCertification(Object object, String sessionKey) throws Exception;
    String getCertification();

    /**
     * 세션에 저장된 메뉴리스트 리턴
     *
     * @return
     * @throws Exception
     */
    @Deprecated
    List<AuthMenuListDto> getAmsMenuList() throws Exception;

    /**
     * 세션에 저장된 사용자정보 리턴
     *
     * @deprecated <p>로그인 처리방식을 쿠키방식으로 변경하면서, 로그인 사용자 정보를 쿠키에 저장하는 방식으로 변경했습니다.</p>
     * {@link LoginCookieService}를 사용하시길 바랍니다.
     * @see LoginCookieService
     * @return
     * @throws Exception
     */
    @Deprecated
    UserInfo getUserInfo() throws Exception;

    /**
     * 세션 제거
     *
     * @return
     * @throws Exception
     */
    boolean removeCertification() throws Exception;

    /**
     * 세션에 저장된 메뉴리스트 리턴<p>
     * 메뉴정보가 없을 경우, 권한 api를 통해 사용자가 접근가능한 메뉴리스트 조회하여 설정
     *
     * @param userInfo
     * @throws Exception
     */
    void confirmAmsMenuList(UserInfo userInfo) throws Exception;
}
