package kr.co.homeplus.admin.web.pg.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.pg.model.PgCompareCostCommissionSetDto;
import kr.co.homeplus.admin.web.pg.model.PgCompareCostDiffGetDto;
import kr.co.homeplus.admin.web.pg.model.PgCompareCostSetDto;
import kr.co.homeplus.admin.web.pg.model.PgCompareCostSumGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PgCompareCostService {

    private final ResourceClient resourceClient;
    /**
     * Admin > 결제관리 > PG 대사 > PG 정산대사 조회
     */
    public List<PgCompareCostSumGetDto> getPgCompareCostSum(PgCompareCostSetDto pgCompareCostSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            pgCompareCostSetDto,
            "/admin/pgCompare/getPgCompareCostSum",
            new ParameterizedTypeReference<ResponseObject<List<PgCompareCostSumGetDto>>>(){}).getData();
    }
    public List<PgCompareCostDiffGetDto> getPgCompareCostDiff(PgCompareCostSetDto pgCompareCostSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            pgCompareCostSetDto,
            "/admin/pgCompare/getPgCompareCostDiff",
            new ParameterizedTypeReference<ResponseObject<List<PgCompareCostDiffGetDto>>>(){}).getData();
    }
    public List<PgCompareCostDiffGetDto> getPgCompareCostCommission(
        PgCompareCostCommissionSetDto pgCompareCostCommissionSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            pgCompareCostCommissionSetDto,
            "/admin/pgCompare/getPgCompareCostCommission",
            new ParameterizedTypeReference<ResponseObject<List<PgCompareCostDiffGetDto>>>(){}).getData();
    }
}
