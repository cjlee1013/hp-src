package kr.co.homeplus.admin.web.promotion.model.event;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EtcPromoItemSetDto {

    // 상품번호
    private String itemNo;

    // 전시여부
    private String dispYn;
}