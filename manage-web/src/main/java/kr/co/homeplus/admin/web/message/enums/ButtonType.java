package kr.co.homeplus.admin.web.message.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.Getter;

public enum ButtonType {

    WL("웹링크"),
    AL("앱링크"),
    DS("배송조회"),
    BK("텍스트전송"),
    MD("본문전송"),
    BC("상담톡"),
    BT("봇");

    private static final Map<String, String> MAP =
        Stream.of(values()).collect(Collectors.toMap(ButtonType::name, ButtonType::getName));

    @Getter
    private final String name;

    ButtonType(String name) {
        this.name = name;
    }

    public static Map<String, String> getMap() {
        return MAP;
    }
}
