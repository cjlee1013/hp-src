package kr.co.homeplus.admin.web.settle.model.partnerBalance;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PartnerBalanceListGetDto {
    @ApiModelProperty(value = "기간")
    @RealGridColumnInfo(headText = "기간", width = 100, sortable = true)
    private String basicDt;

    @ApiModelProperty(value = "판매업체ID")
    @RealGridColumnInfo(headText = "판매업체ID", width = 100, sortable = true)
    private String partnerId;

    @ApiModelProperty(value = "업체코드")
    @RealGridColumnInfo(headText = "업체코드", width = 100, sortable = true)
    private String vendorCd;

    @ApiModelProperty(value = "판매업체명")
    @RealGridColumnInfo(headText = "판매업체명", width = 150, sortable = true)
    private String partnerNm;

    @ApiModelProperty(value = "상호명")
    @RealGridColumnInfo(headText = "상호명", width = 150, sortable = true)
    private String businessNm;

    @ApiModelProperty(value = "발생내역(결제완료)")
    @RealGridColumnInfo(headText = "결제완료", width = 150, sortable = true, groupName="발생 내역", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String paymentSettleAmt;

    @ApiModelProperty(value = "발생내역(매출집계)")
    @RealGridColumnInfo(headText = "매출집계", width = 150, sortable = true, groupName="발생 내역", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String settleSettleAmt;

    @ApiModelProperty(value = "발생내역(지급완료)")
    @RealGridColumnInfo(headText = "지급완료", width = 150, sortable = true, groupName="발생 내역", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String finalSettleAmt;

    @ApiModelProperty(value = "발생내역(역정산환입)")
    @RealGridColumnInfo(headText = "역정산환입", width = 150, sortable = true, groupName="발생 내역", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String minusSettleAmt;

    @ApiModelProperty(value = "누적합계(결제완료)")
    @RealGridColumnInfo(headText = "결제완료", width = 150, sortable = true, groupName="누적 합계", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String paymentSettleSum;

    @ApiModelProperty(value = "누적합계(매출집계)")
    @RealGridColumnInfo(headText = "매출집계", width = 150, sortable = true, groupName="누적 합계", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String settleSettleSum;

    @ApiModelProperty(value = "누적합계(지급완료)")
    @RealGridColumnInfo(headText = "지급완료", width = 150, sortable = true, groupName="누적 합계", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String finalSettleSum;

    @ApiModelProperty(value = "누적합계(역정산환입)")
    @RealGridColumnInfo(headText = "역정산환입", width = 150, sortable = true, groupName="누적 합계", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String minusSettleSum;

    @ApiModelProperty(value = "미확정잔액")
    @RealGridColumnInfo(headText = "미확정잔액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String advanceAmt;

    @ApiModelProperty(value = "미정산잔액")
    @RealGridColumnInfo(headText = "미정산잔액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String balanceAmt;
}
