package kr.co.homeplus.admin.web.statistics.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.statistics.model.RealtimePartnerItemSaleStatisticsDto;
import kr.co.homeplus.admin.web.statistics.model.StatisticsOrderInfoSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RealtimePartnerItemSaleStatisticsService {
    private final ResourceClient resourceClient;

    /**
     * 실시간 판매업체 상품 판매현황 리스트 조회
     * @param statisticsOrderInfoSelectDto
     * @return
     * @throws Exception
     */
    public List<RealtimePartnerItemSaleStatisticsDto> getRealtimePartnerItemSaleStatisticsList(StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_STATISTICS_ORDER_INFO_RANKING_LIST;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<RealtimePartnerItemSaleStatisticsDto>>>() {};
        return (List<RealtimePartnerItemSaleStatisticsDto>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, statisticsOrderInfoSelectDto, apiUri, typeReference).getData();
    }
}
