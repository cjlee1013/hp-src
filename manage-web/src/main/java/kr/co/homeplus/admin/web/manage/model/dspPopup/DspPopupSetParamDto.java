package kr.co.homeplus.admin.web.manage.model.dspPopup;

import lombok.Data;

import java.util.List;

@Data
public class DspPopupSetParamDto {
    private Long popupNo;
    private String useYn;
    private String dispStartDt;
    private String dispEndDt;
    private String popupNm;
    private String imgNm;
    private String imgUrl;
    private int imgWidth;
    private int imgHeight;
    private String closeBtnPeriod;
    private String linkType;
    private String linkInfo;
    private String dispTarget;
    private String regId;

    private List<DspPopupPartnerSetParamDto> partnerList;
}
