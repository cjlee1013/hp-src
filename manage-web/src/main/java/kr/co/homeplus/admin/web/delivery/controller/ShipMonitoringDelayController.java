package kr.co.homeplus.admin.web.delivery.controller;

import java.util.List;
import kr.co.homeplus.admin.web.delivery.model.shipMonitoring.ShipMonitoringDelayGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipMonitoring.ShipMonitoringDelaySetDto;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.delivery.service.ShipMonitoringDelayService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 배송관리 > 배송모니터링 > 발송지연조회
 */
@Controller
@RequestMapping("/ship")
public class ShipMonitoringDelayController {

    @Value("${front.homeplus.url}")
    private String homeplusFrontUrl;

    @Value("${front.theclub.url}")
    private String theclubFrontUrl;

    @Autowired
    private ShipMonitoringDelayService shipMonitoringDelayService;
    @Autowired
    private ShipCommonService shipCommonService;

    /**
     * 발송지연조회 Main Page
     */
    @GetMapping("/shipMonitoring/shipMonitoringDelayMain")
    public String shipMonitoringDelayMain(Model model) throws Exception {

        //SO 권한 데이터 설정
        shipCommonService.setStoreInfo(model);

        model.addAttribute("storeType", shipCommonService.getCode("store_type"));
        model.addAttribute("shipStatus", shipCommonService.getCode("ship_status"));
        model.addAttribute("shipMethod", shipCommonService.getCode("ship_method"));
        model.addAttribute("theclubFrontUrl", theclubFrontUrl);
        model.addAttribute("homeplusFrontUrl", homeplusFrontUrl);
        model.addAllAttributes(RealGridHelper.create("shipMonitoringDelayGridBaseInfo", ShipMonitoringDelayGetDto.class));

        return "/delivery/shipMonitoringDelayMain";
    }

    /**
     * 발송지연조회 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/shipMonitoring/getShipMonitoringDelayList.json", method = RequestMethod.GET)
    public List<ShipMonitoringDelayGetDto> getShipMonitoringDelayList(ShipMonitoringDelaySetDto shipMonitoringDelaySetDto) {
        return shipMonitoringDelayService.getShipMonitoringDelayList(shipMonitoringDelaySetDto);
    }
}
