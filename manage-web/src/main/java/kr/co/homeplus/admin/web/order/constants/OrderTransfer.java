package kr.co.homeplus.admin.web.order.constants;

public class OrderTransfer {

    // 주문관리 메인 조회
    public static final String ORDER_MAIN_SEARCH = "/order/orderSearch";
    // 주문정보 상세 > 상품주문정보 조회
    public static final String ORDER_DETAIL_PRODUCT_SEARCH = "/order/orderSearch/detail/prod/";
    // 주문정보 상세 > 주문금액 조회
    public static final String ORDER_DETAIL_PRICE_SEARCH = "/order/orderSearch/detail/price/";
    // 주문정보 상세 > 주문정보 조회
    public static final String ORDER_DETAIL_PURCHASE_SEARCH = "/order/orderSearch/detail/purchase/";
    // 주문정보 상세 > 결제정보 조회
    public static final String ORDER_DETAIL_PAYMENT_SEARCH = "/order/orderSearch/detail/payment/";
    // 주문정보 상세 > 배송정보 조회
    public static final String ORDER_DETAIL_SHIP_SEARCH = "/order/orderSearch/detail/shipInfo";
    // 주문정보 상세 > 클레임정보 조회
    public static final String ORDER_DETAIL_CLAIM_SEARCH = "/order/orderSearch/detail/claim/";
    // 주문정보 상세 > 배송정보 > 배송정보 수정 조회
    public static final String ORDER_SHIP_ADDR_INFO_SEARCH = "/order/orderSearch/detail/pop/shipAddr/";
    // 주문정보 상세 > 배송정보 > 다중배송정보 수정 조회
    public static final String ORDER_MULTI_SHIP_ADDR_INFO_SEARCH = "/order/orderSearch/detail/pop/multiShipAddr/";
    // 주문정보 상세 > 배송정보 > 송장등록/송장수정/배송완료 조회
    public static final String ORDER_SHIP_TOTAL_INFO_SEARCH = "/order/orderSearch/detail/pop/shipTotal";
    // 주문정보 상세 > 배송정보 > 주문확인
    public static final String ORDER_SHIP_CHECK = "/admin/shipManage/setConfirmOrder";
    // 주문정보 상세 > 배송정보 > 송장등록/수정 (택배)
    public static final String ORDER_INVOICE_REG_DLV = "/admin/shipManage/setShipAll";
    // 주문정보 상세 > 배송정보 > 배송완료
    public static final String ORDER_INVOICE_COMPLETE = "/admin/shipManage/setShipComplete";
    // 주문정보 상세 > 배송정보 > 배송지 수정
    public static final String ORDER_SHIP_ADDR_CHANGE = "/order/orderSearch/ship/orderShipChange";
    // 주문정보 상세 > 배송정보 > 다중 배송지 수정
    public static final String ORDER_MULTI_SHIP_ADDR_CHANGE = "/order/orderSearch/ship/orderMultiShipChange";
    // 주문정보 상세 > 배송정보 > 미수취 신고 등록
    public static final String ORDER_SHIP_NO_RCV_REG = "/order/orderSearch/addNoRcvInfo";
    // 주문정보 상세 > 배송정보 > 미수취 신고 조회
    public static final String ORDER_SHIP_NO_RCV_INFO = "/order/orderSearch/noRcvList";
    // 주문정보 상세 > 배송정보 > 미수취 신고 수정.
    public static final String ORDER_SHIP_NO_RCV_MODIFY = "/order/orderSearch/modifyNoRcvInfo";
    // 주문정보 상세 > 배송정보 > 슬롯정보
    public static final String ORDER_SHIP_SLOT_CHANGE_INFO = "/slot/external/getChangeShipTimeSlot";
    // 주문정보 상세 > 배송정보 > 슬롯변경
    public static final String ORDER_SHIP_SLOT_CHANGE_MODIFY = "/slot/external/changeShipInfoAndOrderSlotStock";
    // 주문정보 상세 > 배송정보 > 발송지연내역
    public static final String ORDER_DELAY_SHIPPING_INFO = "/order/orderSearch/delayShipping/";
    // 주문정보 상세 > 배송정보 > 판매업체정보
    public static final String ORDER_PARTNER_INFO = "/order/orderSearch/orderPartnerInfo";
    // 주문정보 상세 > 배송정보 > 행사할인정보
    public static final String ORDER_PROMOTION_POP_INFO = "/order/orderSearch/orderPromoPopInfo/";
    // 주문정보 상세 > 배송정보 > 쿠폰할인정보
    public static final String ORDER_COUPON_DISCOUNT_POP_INFO = "/order/orderSearch/getOrderCouponDiscountPopInfo/";
    // 주문정보 상세 > 배송정보 > 사은행사정보
    public static final String ORDER_GIFT_POP_INFO = "/order/orderSearch/getOderGiftPopInfo/";
    // 주문정보 상세 > 배송정보 > 대체상품
    public static final String ORDER_DETAIL_SUBSTITUTION_INFO = "/order/orderSearch/getOrderSubsInfo/";
    // 주문정보 상세 > 배송정보 > 주문적립
    public static final String ORDER_SAVE_POP_INFO = "/order/orderSearch/getOrderSavePopInfo/";
    // 주문정보 상세 > 선물하기 재전송
    public static final String ORDER_PRESENT_RE_SEND = "/gift/sendReceiverNotice";
    // 주문정보 상세 > 점포배송상품메모 정보
    public static final String ORDER_STORE_SHIP_MEMO_INFO = "/order/orderSearch/getStoreShipMemo";
    // 주문정보 상세 > 배송정보 > 현금영수증 발행내역
    public static final String ORDER_CASH_RECEIPT_POP_INFO = "/order/orderSearch/getOrderCashReceiptPopInfo/";
    // 주문정보 상세 > 점포배송상품메모 정보 저장/수정
    public static final String ORDER_STORE_SHIP_MEMO_SET = "/order/orderSearch/setStoreShipMemo";
    // 주문정보 상세 > 배송메시지 정보
    public static final String ORDER_STORE_SHIP_MSG_INFO = "/order/orderSearch/getStoreShipMsg";
    // 주문정보 상세 > 배송시간 변경 > 원배송정보
    public static final String ORDER_SHIP_ORIGIN_INFO = "/order/orderSearch/getOrderShipOriginInfo";
    // 주문정보 상세 > 배송정보 > 배송상태변경
    public static final String ORDER_SHIP_STATUS_CHANGE = "/admin/shipManage/setMultiOrderStatus";
}
