package kr.co.homeplus.admin.web.manage.controller;


import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.favicon.FaviconImgListSelectDto;
import kr.co.homeplus.admin.web.manage.model.favicon.FaviconListParamDto;
import kr.co.homeplus.admin.web.manage.model.favicon.FaviconListSelectDto;
import kr.co.homeplus.admin.web.manage.model.favicon.FaviconSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/manage/favicon")
public class FaviconController {

    final private ResourceClient resourceClient;

    final private LoginCookieService cookieService;

    final private CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;


    public FaviconController(ResourceClient resourceClient,
        LoginCookieService cookieService,
        CodeService codeService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
    }

    /**
     * 사이트관리 > 파비콘관리
     *
     * @return String
     */
    @RequestMapping(value = "/faviconMain", method = RequestMethod.GET)
    public String faviconMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn",
            "favicon_device",
            "site_type"
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("faviconDevice", code.get("favicon_device"));
        model.addAttribute("siteType", code.get("site_type"));

        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);

        //그리도
        model.addAllAttributes(
            RealGridHelper.create("faviconListGridBaseInfo", FaviconListSelectDto.class));

        return "/manage/faviconMain";
    }

    /**
     * 사이트관리 > 파비콘관리 > 파비콘 리스트
     */
    @ResponseBody
    @RequestMapping(value = {"/getFaviconList.json"}, method = RequestMethod.GET)
    public List<FaviconListSelectDto> getFaviconList(FaviconListParamDto faviconListParamDto) {

        String apiUri = "/manage/favicon/getFaviconList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<FaviconListSelectDto>>>() {};
        return (List<FaviconListSelectDto>) resourceClient
            .getForResponseObject(ResourceRouteName.MANAGE, StringUtil
                    .getRequestString(apiUri, FaviconListParamDto.class, faviconListParamDto),
                typeReference).getData();
    }

    /**
     * 사이트관리 > 파비콘관리 > 파비콘이미지 조회
     * @param faviconNo
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/getFaviconImgList.json"}, method = RequestMethod.GET)
    public List<FaviconImgListSelectDto> getFaviconImgList(@RequestParam(value="faviconNo") String faviconNo) throws Exception {
        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , "/manage/favicon/getFaviconImgList?faviconNo=" + faviconNo
            , new ParameterizedTypeReference<ResponseObject<List<FaviconImgListSelectDto>>>(){}).getData();
    }

    /**
     * 사이트관리 > 파비콘관리 > 파비콘 등록/수정
     * @param faviconSetParamDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setFavicon.json"}, method = RequestMethod.POST)
    public ResponseResult setFavicon(@RequestBody FaviconSetParamDto faviconSetParamDto)
        throws Exception {

        String apiUri = "/manage/favicon/setFavicon";
        faviconSetParamDto.setUserId(cookieService.getUserInfo().getEmpId());

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {};
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.MANAGE,
            faviconSetParamDto, apiUri, typeReference).getData();
    }

}
