package kr.co.homeplus.admin.web.manage.model.notice;


import java.util.List;
import lombok.Data;

@Data
public class NoticeSetParamDto {

    private Long noticeNo;

    private String dispArea;

    private String noticeKind;

    private String useYn;

    private String topYn;

    private String dispHomeYn;

    private String dispClubYn;

    private String noticeTitle;

    private String noticeDesc;

    private String regId;

    private List<NoticeFileListGetDto> fileSeq;

    private List<NoticeFileListGetDto> fileList;

    private List<NoticePartnerSetParamDto> partnerList;

}
