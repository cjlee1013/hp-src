package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel(description = "결제관리 > PG 대사 > PG 승인대사 조회 > PG 승인대사 합계 DTO")
public class PgCompareApprovalSumDto {

    @ApiModelProperty(notes = "PG종류(TOSSPG,INICIS)", required = true, position = 1)
    @RealGridColumnInfo(headText = "PG사", width = 50, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    @NotEmpty(message = "PG")
    private String pgKind;

    @ApiModelProperty(notes = "승인금액", position = 2)
    @RealGridColumnInfo(headText = "승인금액", sortable = true, width = 100, groupName="홈플러스", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long approvalTotAmt;

    @ApiModelProperty(notes = "취소금액", position = 3)
    @RealGridColumnInfo(headText = "취소금액", sortable = true, width = 100, groupName="홈플러스", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cancelTotAmt;

    @ApiModelProperty(notes = "승인합계", position = 4)
    @RealGridColumnInfo(headText = "승인합계", sortable = true, width = 100, groupName="홈플러스", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long sumAmt;

    @ApiModelProperty(notes = "승인금액(PG)", position = 5)
    @RealGridColumnInfo(headText = "승인금액", sortable = true, width = 100, groupName="PG", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long pgApprovalTotAmt;

    @ApiModelProperty(notes = "취소금액(PG)", position = 6)
    @RealGridColumnInfo(headText = "취소금액", sortable = true, width = 100, groupName="PG", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long pgCancelTotAmt;

    @ApiModelProperty(notes = "승인취소금액합계(PG)", position = 7)
    @RealGridColumnInfo(headText = "승인합계", sortable = true, width = 100, groupName="PG", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long pgSumAmt;

    @ApiModelProperty(notes = "승인차액", position = 8)
    @RealGridColumnInfo(headText = "승인차액", sortable = true, width = 100, groupName="차액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long approvalDiffTotAmt;

    @ApiModelProperty(notes = "취소차액", position = 9)
    @RealGridColumnInfo(headText = "승인취소차액", sortable = true, width = 100, groupName="차액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cancelDiffTotAmt;

    @ApiModelProperty(notes = "승인취소차액합계", position = 10)
    @RealGridColumnInfo(headText = "합계차액", sortable = true, width = 100, groupName="차액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long diffSumAmt;

}
