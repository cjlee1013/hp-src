package kr.co.homeplus.admin.web.promotion.model.event;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RmsPromoSetDto {
    private Long rpmPromoCompDetailId;

    private String useYn;

    private String empNo;
}