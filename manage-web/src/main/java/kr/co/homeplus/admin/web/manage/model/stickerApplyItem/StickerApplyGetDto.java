package kr.co.homeplus.admin.web.manage.model.stickerApplyItem;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StickerApplyGetDto {

    @ApiModelProperty(value = "관리번호", position = 1)
    @RealGridColumnInfo(headText = "관리번호", width = 100)
    private String stickerApplyNo;

    @ApiModelProperty(value = "관리명", position = 1)
    @RealGridColumnInfo(headText = "관리명", width = 100)
    private String stickerApplyNm;

    @ApiModelProperty(value = "스티커번호")
    @RealGridColumnInfo(headText = "스티커번호", hidden = true)
    private String stickerNo;

    @ApiModelProperty(value = "스티커명 ", position = 3)
    @RealGridColumnInfo(headText = "스티커명", width = 100)
    private String stickerNm;

    @ApiModelProperty(value = "노출시작일", position = 4)
    @RealGridColumnInfo(headText = "노출시작일", width = 100)
    private String dispStartDt;

    @ApiModelProperty(value = "노출종료일", position = 5)
    @RealGridColumnInfo(headText = "노출종료일", width = 100)
    private String dispEndDt;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", hidden = true)
    private String useYn;

    @ApiModelProperty(value = "사용여부", position = 6)
    @RealGridColumnInfo(headText = "사용여부", width = 100)
    private String useYnNm;

    @ApiModelProperty(value = "하이퍼사용여부")
    @RealGridColumnInfo(headText = "하이퍼사용여부", hidden = true)
    private String hyperDispYn;

    @ApiModelProperty(value = "클럽사용여부")
    @RealGridColumnInfo(headText = "클럽사용여부", hidden = true)
    private String clubDispYn;

    @ApiModelProperty(value = "익스프레스사용여부")
    @RealGridColumnInfo(headText = "익스프레스사용여부", hidden = true)
    private String expDispYn;

    @ApiModelProperty(value = "등록일" ,position = 7)
    @RealGridColumnInfo(headText = "등록일", width = 150)
    private String regDt;

    @ApiModelProperty(value = "등록자" ,position = 8)
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regNm;

    @ApiModelProperty(value = "수정일",position = 9)
    @RealGridColumnInfo(headText = "수정일", width = 150)
    private String chgDt;

    @ApiModelProperty(value = "수정자" ,position = 10)
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgNm;
}
