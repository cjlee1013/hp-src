package kr.co.homeplus.admin.web.item.model.auroraCategoryItem;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class AuroraCategoryItemExcelDto {

    @EqualsAndHashCode.Include
    private String mCateCd;
    @EqualsAndHashCode.Include
    private String itemNo;
    @EqualsAndHashCode.Include
    private String useYn;

}
