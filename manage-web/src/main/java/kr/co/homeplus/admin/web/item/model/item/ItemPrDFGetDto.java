package kr.co.homeplus.admin.web.item.model.item;

import lombok.Data;

/**
 * 대표셀링포인트
 */
@Data
public class ItemPrDFGetDto {

	//점포ID
	private Integer storeId;

	//점포명
	private String storeNm;

	//점포타입명
	private String storeTypeNm;

	//유형명-말머리(HEAD),구매가이드(GUIDE)
	private String 	prTypeNm;

	//내용
	private String 	prContents;

}
