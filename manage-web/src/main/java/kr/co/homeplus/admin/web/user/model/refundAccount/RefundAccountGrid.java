package kr.co.homeplus.admin.web.user.model.refundAccount;

import kr.co.homeplus.admin.web.user.enums.RefundAccountBankCd;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class RefundAccountGrid {
    @RealGridColumnInfo(headText = "일련번호", hidden = true)
    private long seq;
    @RealGridColumnInfo(headText = "회원번호")
    private long userNo;
    @RealGridColumnInfo(headText = "회원명")
    private String userNm;
    @RealGridColumnInfo(headText = "회원ID")
    private String userId;
    @RealGridColumnInfo(headText = "휴대폰번호")
    private String mobile;
    @RealGridColumnInfo(headText = "은행명")
    private String bankNm;
    @RealGridColumnInfo(headText = "은행코드", columnType = RealGridColumnType.LOOKUP, hidden = true)
    private RefundAccountBankCd bankCd;
    @RealGridColumnInfo(headText = "계좌번호")
    private String bankAccount;
    @RealGridColumnInfo(headText = "예금주")
    private String bankOwner;
}
