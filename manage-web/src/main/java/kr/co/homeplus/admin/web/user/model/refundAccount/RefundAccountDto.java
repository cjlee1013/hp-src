package kr.co.homeplus.admin.web.user.model.refundAccount;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.admin.web.user.enums.RefundAccountBankCd;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RefundAccountDto {
    @ApiModelProperty(value = "일련번호")
    private long seq;
    @ApiModelProperty(value = "회원아이디")
    private String userId;
    @ApiModelProperty(value = "회원번호")
    private long userNo;
    @ApiModelProperty(value = "회원이름")
    private String userNm;
    @ApiModelProperty(value = "휴대폰번호")
    private String mobile;
    @ApiModelProperty(value = "은행명")
    private String bankNm;
    @ApiModelProperty(value = "은행코드")
    private RefundAccountBankCd bankCd;
    @ApiModelProperty(value = "계좌번호")
    private String bankAccount;
    @ApiModelProperty(value = "계좌주명")
    private String bankOwner;
}
