package kr.co.homeplus.admin.web.app.service;

import java.util.List;
import kr.co.homeplus.admin.web.app.model.AppVersionInfoDto;
import kr.co.homeplus.admin.web.app.model.AppVersionModifyParam;
import kr.co.homeplus.admin.web.app.model.AppVersionRegistParam;
import kr.co.homeplus.admin.web.app.model.AppVersionSearchParam;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.dto.UserInfo;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AppVersionMngService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;


    public List<AppVersionInfoDto> getSearchAppVersionList(AppVersionSearchParam searchParam) {
        String apiUri = String.format(
            "/admin/app/version/mng/getAppVersionList?siteType=%s&devicePlatform=%s&isUsed=%s",
            searchParam.getSiteType(), searchParam.getDevicePlatform(),
            searchParam.getIsUsed());

        return resourceClient.getForResponseObject(
            ResourceRouteName.USERMNG,
            apiUri,
            new ParameterizedTypeReference<ResponseObject<List<AppVersionInfoDto>>>() {
            }).getData();
    }

    public AppVersionInfoDto getAppVersion(Integer seq) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.USERMNG,
            "/admin/app/version/mng/getAppVersion?seq=" + seq,
            new ParameterizedTypeReference<ResponseObject<AppVersionInfoDto>>() {
            }).getData();
    }

    public ResponseObject setAppVersion(AppVersionRegistParam appVersionRegistParam) {
        UserInfo loginUserInfo = loginCookieService.getUserInfo();

        appVersionRegistParam.setRegEmpId(loginUserInfo.getEmpId());
        appVersionRegistParam.setRegEmpNm(loginUserInfo.getUserNm());
        log.debug("appVersionRegParam : {}", appVersionRegistParam);
        ResponseObject responseObject = resourceClient
            .postForResponseObject(ResourceRouteName.USERMNG, appVersionRegistParam,
                "/admin/app/version/mng/setAppVersion");
        return responseObject;
    }

    public ResponseObject putAppVersion(AppVersionModifyParam appVersionModifyParam) {
        UserInfo loginUserInfo = loginCookieService.getUserInfo();

        appVersionModifyParam.setModEmpId(loginUserInfo.getEmpId());
        appVersionModifyParam.setModEmpNm(loginUserInfo.getUserNm());
        log.debug("appVersionModParam : {}", appVersionModifyParam);
        ResponseObject responseObject = resourceClient
            .postForResponseObject(ResourceRouteName.USERMNG, appVersionModifyParam,
                "/admin/app/version/mng/putAppVersion");
        return responseObject;
    }

}
