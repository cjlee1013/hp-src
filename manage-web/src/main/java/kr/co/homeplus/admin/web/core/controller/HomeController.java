package kr.co.homeplus.admin.web.core.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 어드민 홈 컨트롤러
 */
@Slf4j
@Controller
public class HomeController {
	/**
	 * 메인화면(홈)
	 *
	 * @return
	 */
	@GetMapping(value =  "/")
	public String home() {
		return "/home/main";
	}
}