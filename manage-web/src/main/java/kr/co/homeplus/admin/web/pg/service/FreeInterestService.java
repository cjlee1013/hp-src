package kr.co.homeplus.admin.web.pg.service;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.pg.model.FreeInterestApplyDto;
import kr.co.homeplus.admin.web.pg.model.FreeInterestCopyDto;
import kr.co.homeplus.admin.web.pg.model.FreeInterestDto;
import kr.co.homeplus.admin.web.pg.model.FreeInterestSelectDto;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FreeInterestService {

    private static final String PARENT_METHOD_CD_CARD = "CARD";

    private final ResourceClient resourceClient;

    /**
     * 신용카드 결제수단을 조회
     * Site_type중 중복된 신용카드 결제수단을 제거한 후 반환
     * @return
     * @throws Exception
     */
    public List<PaymentMethodDto> getDistinctMethodCd() throws Exception {

        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("parentMethodCd",PARENT_METHOD_CD_CARD));

        String apiUri = EscrowConstants.ESCROW_GET_DISTINCT_METHOD_CD + StringUtil.getParameter(setParameterList);
        ResourceClientRequest<List<PaymentMethodDto>> request = ResourceClientRequest.<List<PaymentMethodDto>>getBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 무이자혜택 기본정보 조회
     * @param freeInterestSelectDto
     * @return
     * @throws Exception
     */
    public List<FreeInterestDto> getFreeInterestMngList(FreeInterestSelectDto freeInterestSelectDto) throws Exception {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("freeInterestMngSeq", freeInterestSelectDto.getSchFreeInterestMngSeq()));
        setParameterList.add(SetParameter.create("applyStartDt", freeInterestSelectDto.getSchApplyStartDt()));
        setParameterList.add(SetParameter.create("applyEndDt", freeInterestSelectDto.getSchApplyEndDt()));
        setParameterList.add(SetParameter.create("siteType", freeInterestSelectDto.getSchSiteType()));
        setParameterList.add(SetParameter.create("freeInterestNm", freeInterestSelectDto.getSchFreeInterestNm()));
        setParameterList.add(SetParameter.create("useYn", freeInterestSelectDto.getSchUseYn()));

        String apiUri = EscrowConstants.ESCROW_GET_FREE_INTEREST_MNG_LIST + StringUtil.getParameter(setParameterList);
        ResourceClientRequest<List<FreeInterestDto>> request = ResourceClientRequest.<List<FreeInterestDto>>getBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 무이자혜택 적용대상 조회
     * @param freeInterestMngSeq
     * @return
     * @throws Exception
     */
    public List<FreeInterestApplyDto> getFreeInterestApplyList(long freeInterestMngSeq) throws Exception {

        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("freeInterestMngSeq", freeInterestMngSeq));
        String apiUri = EscrowConstants.ESCROW_GET_FREE_INTEREST_APPLY_LIST + StringUtil.getParameter(setParameterList);
        ResourceClientRequest<List<FreeInterestApplyDto>> request = ResourceClientRequest.<List<FreeInterestApplyDto>>getBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 카드무이자혜택 저장
     * @param freeInterestDto
     * @return
     * @throws Exception
     */
    public Integer saveFreeInterest(FreeInterestDto freeInterestDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_SAVE_FREE_INTEREST;
        ResourceClientRequest<Integer> request = ResourceClientRequest.<Integer>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(freeInterestDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }


    /**
     * 무이자혜택카드정보 저장
     * @param freeInterestApplyDto
     * @return
     * @throws Exception
     */
    public Integer saveFreeInterestApply(FreeInterestApplyDto freeInterestApplyDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_SAVE_FREE_INTEREST_APPLY;
        ResourceClientRequest<Integer> request = ResourceClientRequest.<Integer>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(freeInterestApplyDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 무이자혜택복사 저장
     * @param freeInterestCopyDto
     * @return
     * @throws Exception
     */
    public Integer saveCopyFreeInterest(FreeInterestCopyDto freeInterestCopyDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_SAVE_COPY_FREE_INTEREST;
        ResourceClientRequest<Integer> request = ResourceClientRequest.<Integer>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(freeInterestCopyDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 무이자혜택 카드정보 삭제
     * @param freeInterestApplyDto
     * @return
     * @throws Exception
     */
    public Integer deleteFreeInterestApply(FreeInterestApplyDto freeInterestApplyDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_DELETE_FREE_INTEREST;
        ResourceClientRequest<Integer> request = ResourceClientRequest.<Integer>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(freeInterestApplyDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }
}
