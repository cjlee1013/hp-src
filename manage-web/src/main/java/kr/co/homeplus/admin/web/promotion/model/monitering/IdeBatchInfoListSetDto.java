package kr.co.homeplus.admin.web.promotion.model.monitering;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdeBatchInfoListSetDto {
    private Long seq;

    private String batchNm;

    private String batchMngNm;

    private String batchInfo;

    private String batchSchedule;

    private String useYn;

    private String empNo;
}