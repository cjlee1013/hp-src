package kr.co.homeplus.admin.web.statistics.model;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CounselStatisticsGetDto {
    @ApiModelProperty(value = "상담구분1")
    @RealGridColumnInfo(headText = "상담구분1", width = 100, sortable = true)
    private String advisortype_one;

    @ApiModelProperty(value = "상담구분2")
    @RealGridColumnInfo(headText = "상담구분2", width = 100, sortable = true)
    private String advisortype_two;

    @ApiModelProperty(value = "구분")
    @RealGridColumnInfo(headText = "구분", width = 70, sortable = true)
    private String voc_yn_nm;

    @ApiModelProperty(value = "합계")
    @RealGridColumnInfo(headText = "합계", width = 80, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String adv_total;

    @ApiModelProperty(value = "접수")
    @RealGridColumnInfo(headText = "접수", width = 70, sortable = true, groupName="점포", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String hb2_reception;

    @ApiModelProperty(value = "처리중")
    @RealGridColumnInfo(headText = "처리중", width = 70, sortable = true, groupName="점포", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String hb2_ing_notend;

    @ApiModelProperty(value = "처리완료")
    @RealGridColumnInfo(headText = "처리완료", width = 70, sortable = true, groupName="점포", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String hb2_ing_end;

    @ApiModelProperty(value = "접수")
    @RealGridColumnInfo(headText = "접수", width = 70, sortable = true, groupName="콜센터", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String hb3_reception;

    @ApiModelProperty(value = "콜백")
    @RealGridColumnInfo(headText = "콜백", width = 70, sortable = true, groupName="콜센터", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String hb3_callback;

    @ApiModelProperty(value = "관리자이관")
    @RealGridColumnInfo(headText = "관리자이관", width = 70, sortable = true, groupName="콜센터", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String hb3_tarns;

    @ApiModelProperty(value = "처리완료")
    @RealGridColumnInfo(headText = "처리완료", width = 70, sortable = true, groupName="콜센터", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String hb3_ing_notend;

    @ApiModelProperty(value = "확인요청")
    @RealGridColumnInfo(headText = "확인요청", width = 70, sortable = true, groupName="본사", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String hb1_confirm;

    @ApiModelProperty(value = "클레임제외")
    @RealGridColumnInfo(headText = "클레임제외", width = 70, sortable = true, groupName="본사", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String hb1_clame_except;

    @ApiModelProperty(value = "처리완료")
    @RealGridColumnInfo(headText = "처리완료", width = 70, sortable = true, groupName="본사", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String hb1_advisor_ing_end;
}
