package kr.co.homeplus.admin.web.core.privacylog.service.impl;

import kr.co.homeplus.admin.web.core.exception.PersonalLogServiceException;
import kr.co.homeplus.admin.web.core.privacylog.model.FileStoreResult;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyLogInfo;
import kr.co.homeplus.admin.web.core.privacylog.service.PrivacyLogService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Slf4j
@Service
@RequiredArgsConstructor
public class PrivacyLogServiceImpl implements PrivacyLogService {

    private final WebClient webClient;

    @Value("${plus.resource-routes.logService.url}")
    private String logServiceUrl;

    @Value("${spring.application.name:manage-web}")
    private String sendApiId;

    private static final String LOG_SERVICE_STORE_PERSONAL_URI = "/store/single/personal?id=";

    @Override
    public void send(final PrivacyLogInfo param) {
        sendToPersonalLogServiceByWebClient(sendApiId, param);
    }

    /**
     * 개인정보 접근이력을 log-service api로 발송합니다.
     * @param sendApiId 발송 API ID
     * @param param 개인정보 접근이력 파라미터
     */
    private void sendToPersonalLogServiceByWebClient(final String sendApiId, final PrivacyLogInfo param) {
        final String uri = getPrivacyLogApiUrl(sendApiId);

        webClient.post()
            .uri(uri)
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .body(BodyInserters.fromPublisher(Mono.just(param), PrivacyLogInfo.class))
            .retrieve()
            .onStatus(HttpStatus::is4xxClientError,
                response -> {
                    logErrorResponse(response);
                    return Mono.error(new PersonalLogServiceException("is4xxClientError"));
                })
            .onStatus(HttpStatus::is5xxServerError,
                response -> {
                    logErrorResponse(response);
                    return Mono.error(new PersonalLogServiceException("is5xxServerError"));
                })
            .bodyToMono(new ParameterizedTypeReference<ResponseObject<FileStoreResult>>(){})
            .doOnError(throwable -> {
                log.error("sendToPersonalLogServiceByWebClient error:{}", ExceptionUtils.getStackTrace(throwable));
                throw new PersonalLogServiceException(throwable);
            }).subscribe()
        ;
    }

    private String getPrivacyLogApiUrl(final String sendApiId) {
        return logServiceUrl + LOG_SERVICE_STORE_PERSONAL_URI + sendApiId;
    }

    /**
     * error response logging
     * @see <a href="https://stackoverflow.com/q/44593066/8854954">webclient get body on error</a>
     * @param response
     */
    public static void logErrorResponse(ClientResponse response) {
        log.error("LogServiceByWebClient.logErrorResponse error:<status:{}>,<headers:{}>",
            response.statusCode(), response.headers().asHttpHeaders());
        response.bodyToMono(String.class)
            .publishOn(Schedulers.elastic())
            .subscribe(body -> log.error("LogServiceByWebClient.logErrorResponse error body:{}", body));
    }
}
