package kr.co.homeplus.admin.web.item.enums;

import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.collect.ImmutableBiMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum MallType {

        DS("DS", "셀러상품")
    ,   TD("TD", "점포상품");

    @Getter
    private final String type;

    @Getter
    private final String description;

    @Getter
    private static final ImmutableBiMap<MallType, String> biMap;

    static {
        BiMap<MallType, String> map = EnumHashBiMap.create(MallType.class);

        for (MallType policy : values()) {
            map.put(policy, policy.name());
        }

        biMap = ImmutableBiMap.copyOf(map);
    }

    public static MallType valueFor(final String type) {
        final MallType policy =  biMap.inverse().get(type);

        return policy;
    }

}
