package kr.co.homeplus.admin.web.item.model.market;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//외부연동 설정관리 상품 설정정보 저장
public class MarketItemSettingSetDto {

	//상품번호
	private String itemNo;

	//설정정보
	private List<MarketItemProviderGetDto> providerList;

	//등록/수정자
	private String userId;

}
