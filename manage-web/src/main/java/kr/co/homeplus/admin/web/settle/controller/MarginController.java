package kr.co.homeplus.admin.web.settle.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.settle.model.margin.MarginDetailListGetDto;
import kr.co.homeplus.admin.web.settle.model.margin.MarginDetailListSetDto;
import kr.co.homeplus.admin.web.settle.model.margin.MarginHeadListGetDto;
import kr.co.homeplus.admin.web.settle.model.margin.MarginListSetDto;
import kr.co.homeplus.admin.web.settle.model.margin.MarginPartListGetDto;
import kr.co.homeplus.admin.web.settle.model.margin.MarginTeamListGetDto;
import kr.co.homeplus.admin.web.settle.service.MarginService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/margin")
public class MarginController {

    @Autowired
    private MarginService marginService;
    @Autowired
    private SettleCommonService settleCommonService;

    /**
     * DC/DS 마진조회 Main Page
     */
    @RequestMapping(value = "/marginMain", method = RequestMethod.GET)
    public String marginMain(Model model) throws Exception{
        model.addAllAttributes(RealGridHelper.create("marginTeamGridBaseInfo", MarginTeamListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("marginPartGridBaseInfo", MarginPartListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("marginHeadGridBaseInfo", MarginHeadListGetDto.class));

        return "/settle/marginMain";
    }

    /**
     * DC/DS 마진상세조회 Main Page
     */
    @RequestMapping(value = "/marginDetailMain", method = RequestMethod.GET)
    public String marginDetailMain(Model model) throws Exception{
        model.addAllAttributes(RealGridHelper.create("marginDetailGridBaseInfo", MarginDetailListGetDto.class));

        return "/settle/marginDetailMain";
    }

    /**
     * DC/DS 마진조회 팀별 리스트 조회
     */
    @ResponseBody
    @PostMapping("/getMarginList.json")
    public Map<String,Object> getMarginTeamList(@RequestBody MarginListSetDto marginListSetDto) {
        Map<String,Object> listMap = new HashMap<>();

        listMap.put("teamList",marginService.getMarginTeamList(marginListSetDto));
        listMap.put("partList",marginService.getMarginPartList(marginListSetDto));
        listMap.put("headList",marginService.getMarginHeadList(marginListSetDto));

        return listMap;
    }

    /**
     * DC/DS 마진조회 파트별 리스트 조회
     */
    @ResponseBody
    @PostMapping("/getMarginPartList.json")
    public List<MarginPartListGetDto> getMarginPartList(@RequestBody MarginListSetDto marginListSetDto) {
        return marginService.getMarginPartList(marginListSetDto);
    }

    /**
     * DC/DS 마진조회 유형별 리스트 조회
     */
    @ResponseBody
    @PostMapping("/getMarginHeadList.json")
    public List<MarginHeadListGetDto> getMarginHeadList(@RequestBody MarginListSetDto marginListSetDto) {
        return marginService.getMarginHeadList(marginListSetDto);
    }

    /**
     * DC/DS 마진상세조회 메인 리스트 조회
     */
    @ResponseBody
    @PostMapping("/getMarginDetailList.json")
    public List<MarginDetailListGetDto> getMarginDetailList(@RequestBody MarginDetailListSetDto marginDetailListSetDto) {
        return marginService.getMarginDetailList(marginDetailListSetDto);
    }

}