package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import kr.co.homeplus.admin.web.common.model.pop.StorePopupSearchGetDto;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/common")
public class SettleCommonController {

    private final SettleCommonService settleCommonService;

    public SettleCommonController(SettleCommonService settleCommonService) {
        this.settleCommonService = settleCommonService;
    }

    /**
     * 점포 리스트 조회 (select box용)
     */
    @ResponseBody
    @GetMapping("/getStoreList.json")
    public List<StorePopupSearchGetDto> getStoreList(
        @RequestParam(value = "storeType") String storeType
        ,@RequestParam(value = "storeRange") String storeRange) {
        return settleCommonService.getStoreList(storeType, storeRange);
    }
}