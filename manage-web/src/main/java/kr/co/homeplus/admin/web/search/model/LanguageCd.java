package kr.co.homeplus.admin.web.search.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/***
 * 언어: 외래어(음역)가 유래된 언어
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum  LanguageCd {
    ENGLISH("ENGLISH","영어"),
    CHINESE("CHINESE","중국어"),
    JAPANESE("JAPANESE","일본어"),
    GERMAN("GERMAN","독일어"),
    FRENCH("FRENCH","프랑스어"),
    ITALIAN("ITALIAN","이탈리아어"),
    ETC("ETC","기타");

    private String code;
    private String name;

    public static LanguageCd getType(String code) {
        for(LanguageCd cd : LanguageCd.values()) {
            if(cd.getCode().equals(code)) {
                return cd;
            }
        }
        return null;
    }
}
