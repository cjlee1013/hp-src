package kr.co.homeplus.admin.web.user.model.memberMemo;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class MemberMemo {
    @RealGridColumnInfo(headText = "번호", width = 90, sortable = true)
    private long num;
    @RealGridColumnInfo(headText = "회원메모 일련번호", hidden = true)
    private long seq;
    @RealGridColumnInfo(headText = "회원일련번호", hidden = true)
    private long userNo;
    @RealGridColumnInfo(headText = "내용", width = 550, columnType = RealGridColumnType.NAME)
    private String content;
    @RealGridColumnInfo(headText = "등록자 아이디", hidden = true)
    private String regEmpId;
    @RealGridColumnInfo(headText = "등록자")
    private String regEmpNm;       
    @RealGridColumnInfo(headText = "등록일시", width = 110)
    private String regDt;
}
