package kr.co.homeplus.admin.web.voc.model.callCenter;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 콜센터문의 리스트 응답 DTO")
public class CallCenterListGetDto {

  @ApiModelProperty(notes = "문의번호")
  @RealGridColumnInfo(headText = "문의번호", width = 90, sortable = true)
  @JsonProperty("KEY")
  private String KEY;

  @ApiModelProperty(notes = "상담구분")
  @RealGridColumnInfo(headText = "상담구분", width = 80, sortable = true)
  @JsonProperty("voc_yn_nm")
  private String voc_yn_nm;

  @ApiModelProperty(notes = "접수채널")
  @RealGridColumnInfo(headText = "접수채널", width = 80, sortable = true)
  @JsonProperty("channel_nm")
  private String channel_nm;

  @ApiModelProperty(notes = "점포유형")
  @RealGridColumnInfo(headText = "점포유형", width = 80, sortable = true)
  @JsonProperty("h_channel_nm")
  private String h_channel_nm;

  @ApiModelProperty(notes = "상담구분")
  @RealGridColumnInfo(headText = "상담구분", width = 110, sortable = true)
  @JsonProperty("advisor_type_nm1")
  private String advisor_type_nm1;

  @ApiModelProperty(notes = "상담항목")
  @RealGridColumnInfo(headText = "상담항목", width = 160, sortable = true)
  @JsonProperty("advisor_type_nm2")
  private String advisor_type_nm2;

  @ApiModelProperty(notes = "주문번호")
  @RealGridColumnInfo(headText = "주문번호", width = 90, sortable = true)
  @JsonProperty("h_order_no")
  private String h_order_no;

  @ApiModelProperty(notes = "제목")
  @RealGridColumnInfo(headText = "제목", width = 250, sortable = true, columnType = RealGridColumnType.NONE)
  @JsonProperty("history_title")
  private String history_title;

  @ApiModelProperty(notes = "판매업체")
  @RealGridColumnInfo(headText = "판매업체", width = 100, sortable = true)
  @JsonProperty("store_nm")
  private String store_nm;

  @ApiModelProperty(notes = "마켓연동")
  @RealGridColumnInfo(headText = "마켓연동", width = 90, sortable = true)
  @JsonProperty("market_type_nm")
  private String market_type_nm;

  @ApiModelProperty(notes = "회원번호")
  @RealGridColumnInfo(headText = "회원번호", width = 100, sortable = true, hidden = true)
  @JsonProperty("cust_id")
  private String cust_id;

  @ApiModelProperty(notes = "회원번호")
  @RealGridColumnInfo(headText = "회원번호", width = 90, sortable = true)
  @JsonProperty("cust_id_mask")
  private String cust_id_mask;

  @ApiModelProperty(notes = "회원명")
  @RealGridColumnInfo(headText = "회원명", width = 80, sortable = true)
  @JsonProperty("cust_nm")
  private String cust_nm;

  @ApiModelProperty(notes = "처리부서")
  @RealGridColumnInfo(headText = "처리부서", width = 80, sortable = true)
  @JsonProperty("h_branch_nm")
  private String h_branch_nm;

  @ApiModelProperty(notes = "처리상태")
  @RealGridColumnInfo(headText = "처리상태", width = 90, sortable = true)
  @JsonProperty("advisor_ing_nm")
  private String advisor_ing_nm;

  @ApiModelProperty(notes = "접수자")
  @RealGridColumnInfo(headText = "접수자", width = 80, sortable = true)
  @JsonProperty("account_nm")
  private String account_nm;

  @ApiModelProperty(notes = "접수일시")
  @RealGridColumnInfo(headText = "접수일시", width = 120, sortable = true)
  @JsonProperty("start_time")
  private String start_time;

  @ApiModelProperty(notes = "처리자")
  @RealGridColumnInfo(headText = "처리자", width = 80, sortable = true)
  @JsonProperty("complet_account_nm")
  private String complet_account_nm;

  @ApiModelProperty(notes = "처리완료일시")
  @RealGridColumnInfo(headText = "처리완료일시", width = 120, sortable = true)
  @JsonProperty("complet_dt")
  private String complet_dt;

  @ApiModelProperty(notes = "채널종류")
  @RealGridColumnInfo(headText = "채널종류", hidden = true)
  @JsonProperty("channel_type")
  private String channel_type;
}