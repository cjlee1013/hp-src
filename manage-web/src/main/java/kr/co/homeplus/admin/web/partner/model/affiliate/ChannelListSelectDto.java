package kr.co.homeplus.admin.web.partner.model.affiliate;

import java.math.BigDecimal;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class ChannelListSelectDto {

    @RealGridColumnInfo(headText = "제휴채널ID", width = 50)
    private String channelId;

    @RealGridColumnInfo(headText = "제휴채널명", width = 50, order = 1)
    private String channelNm;

    @RealGridColumnInfo(headText = "제휴업체ID", width = 50, order = 2)
    private String partnerId;

    @RealGridColumnInfo(headText = "제휴업체명", width = 50, order = 3)
    private String partnerNm;

    @RealGridColumnInfo(headText = "사이트 구분", width = 50, order = 4)
    private String siteTypeNm;

    @RealGridColumnInfo(headText = "사용여부", width = 50, order = 5)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 50, order = 6)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 50, order = 7)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 50, order = 8)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 50, order = 9)
    private String chgDt;

    @RealGridColumnInfo(headText = "cookieTime", hidden = true, order = 11)
    private int cookieTime;

    @RealGridColumnInfo(headText = "수수료", hidden = true, order = 12)
    private BigDecimal commissionRate;

    @RealGridColumnInfo(headText = "urlParameter", hidden = true, order = 13)
    private String urlParameter;

    @RealGridColumnInfo(headText = "siteType", hidden = true, order = 14)
    private String siteType;

    @RealGridColumnInfo(headText = "사용여부 코드", hidden = true, order = 15)
    private String useYn;

}
