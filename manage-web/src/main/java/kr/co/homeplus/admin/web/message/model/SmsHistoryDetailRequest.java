package kr.co.homeplus.admin.web.message.model;

import lombok.Data;

@Data
public class SmsHistoryDetailRequest {

    private long smsWorkSeq;
}
