package kr.co.homeplus.admin.web.manage.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.dspCacheManage.DspCacheManageListParamDto;
import kr.co.homeplus.admin.web.manage.model.dspCacheManage.DspCacheManageListSelectDto;
import kr.co.homeplus.admin.web.manage.model.dspCacheManage.DspCacheManageSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage/dspCacheManage")
public class DspCacheManageController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    /**
     * 시스템관리 > 모니터링 > Cache 강제 적용
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dspCacheManageMain", method = RequestMethod.GET)
    public String dspCacheManageMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"            // 사용여부
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAllAttributes(RealGridHelper.create("dspCacheManageListGridBaseInfo", DspCacheManageListSelectDto.class));

        return "/manage/dspCacheManageMain";
    }

    /**
     * 시스템관리 > 모니터링 > Cache 강제 적용 리스트
     * @param
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/getDspCacheManageList.json"}, method = RequestMethod.GET)
    public List<DspCacheManageListSelectDto> getDspCacheManageList(DspCacheManageListParamDto listParamDto) {
        String apiUri = "/manage/dspCacheManage/getCacheManageList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<DspCacheManageListSelectDto>>>() {};
        return (List<DspCacheManageListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, DspCacheManageListParamDto.class, listParamDto), typeReference).getData();
    }


    /**
     * 시스템관리 > 모니터링 > Cache 강제 관리 등록/수정
     * @param setParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setDspCacheManage.json"}, method = RequestMethod.POST)
    public ResponseResult setDspCacheManage(@RequestBody DspCacheManageSetParamDto setParamDto) {
        String apiUri = "/manage/dspCacheManage/setDspCacheManage";
        setParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, setParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 시스템관리 > 모니터링 > Cache 강제 적용 후 처리
     * @param triggerUrl
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setForceCacheUpdate.json"}, method = RequestMethod.POST)
    public ResponseResult setForceCacheUpdate(@RequestParam(name="triggerUrl") String triggerUrl) {
        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE, triggerUrl, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 시스템관리 > 모니터링 > Cache 강제 적용 후 정보 갱신
     * @param setParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setDspCacheInformation.json"}, method = RequestMethod.POST)
    public ResponseResult setCacheInformation(@RequestBody DspCacheManageSetParamDto setParamDto) {
        String apiUri = "/manage/dspCacheManage/setDspCacheInformation";
        setParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, setParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

}
