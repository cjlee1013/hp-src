package kr.co.homeplus.admin.web.message.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.message.model.AlimtalkCancelRequest;
import kr.co.homeplus.admin.web.message.model.AlimtalkWorkDetail;
import kr.co.homeplus.admin.web.message.model.AlimtalkHistoryDetailRequest;
import kr.co.homeplus.admin.web.message.model.AlimtalkHistoryListRequest;
import kr.co.homeplus.admin.web.message.model.AlimtalkSendListGrid;
import kr.co.homeplus.admin.web.message.model.AlimtalkWorkGrid;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Api(tags = "알림톡 어드민용 컨트롤러")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
@Controller
@RequestMapping("/message/admin/alimtalk")
@RequiredArgsConstructor
public class AlimtalkAdminController {

    private final CodeService codeService;
    private final ResourceClient resourceClient;

    private final Map<String, Object> modelMap = RealGridHelper.create("alimtalkWorkGridBaseInfo", AlimtalkWorkGrid.class);
    private final Map<String, Object> alimtalkSendListGridBaseInfoMap = RealGridHelper.create("alimtalkSendListGridBaseInfo", AlimtalkSendListGrid.class);

    @ApiOperation(value = "알림톡 발송 main 화면")
    @ApiResponse(code = 200, message = "알림톡 발송 main 화면")
    @GetMapping("/history/main")
    public String main(final Model model) {
        codeService.getCodeModel(model, "message_work_status");

        model.addAllAttributes(modelMap);
        model.addAllAttributes(alimtalkSendListGridBaseInfoMap);

        return "/message/alimtalkMain";
    }

    @ApiOperation(value = "알림톡 발송 이력 목록 조회")
    @ApiResponse(code = 200, message = "알림톡 발송 이력 목록")
    @PostMapping("/history/getWorks.json")
    @ResponseBody
    public ResponseObject<List<AlimtalkWorkGrid>> historyList(
        @ApiParam(value = "알림톡 발송 이력 조회 조건", required = true) @Valid final AlimtalkHistoryListRequest alimtalkHistoryListRequest) {

        return resourceClient
            .postForResponseObject(ResourceRouteName.MESSAGE, alimtalkHistoryListRequest,
                "/admin/alimtalk/history/list", new ParameterizedTypeReference<>() {
                });
    }

    @ApiOperation(value = "알림톡 발송 상세 조회")
    @ApiResponse(code = 200, message = "알림톡 발송 상세")
    @GetMapping("/history/getDetail.json")
    @ResponseBody
    public ResponseObject<AlimtalkWorkDetail> historyDetail(
        @ApiParam(value = "알림톡 발송 상세 조회 조건", required = true) @Valid final AlimtalkHistoryDetailRequest alimtalkHistoryDetailRequest) {

        return resourceClient
            .postForResponseObject(ResourceRouteName.MESSAGE, alimtalkHistoryDetailRequest,
                "/admin/alimtalk/history/detail", new ParameterizedTypeReference<>() {
                });
    }

    @ApiOperation(value = "알림톡 발송 취소")
    @ApiResponse(code = 200, message = "알림톡 발송 취소 성공 여부")
    @PostMapping("/history/cancel.json")
    @ResponseBody
    public ResponseObject<Object> cancelOne(
        @ApiParam(value = "알림톡 발송 취소 조건", required = true) @Valid final AlimtalkCancelRequest alimtalkCancelRequest) {

        ResponseObject<Object> responseObject = resourceClient
            .postForResponseObject(ResourceRouteName.MESSAGE, alimtalkCancelRequest,
                "/admin/alimtalk/history/cancel", new ParameterizedTypeReference<>() {
                });

        return ResponseObject.Builder.<Object>builder().data(responseObject).build();
    }
}
