package kr.co.homeplus.admin.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//상품정보고시관리 Get Entry
public class MngNoticeListGetDto {

    //고시항목번호
    private int noticeNo;

    //정보고시항목명
    private String noticeNm;

    //정보고시항목명 세부내용
    private String noticeDetail;

    //기본문구
    private String commentNm;

    //기본타이틀
    private String commentTitle;
}
