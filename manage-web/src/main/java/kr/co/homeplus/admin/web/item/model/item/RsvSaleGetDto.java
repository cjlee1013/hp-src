package kr.co.homeplus.admin.web.item.model.item;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class RsvSaleGetDto {
    @RealGridColumnInfo(headText = "No.", sortable = true, width = 70)
    private long rsvSaleSeq;

    @RealGridColumnInfo(headText = "점포유형", sortable = true, width = 70)
    private String storeType;

    @RealGridColumnInfo(headText = "판매유형", sortable = true, width = 70)
    private String saleType;

    @RealGridColumnInfo(headText = "예약판매명", sortable = true, columnType = RealGridColumnType.NAME, width = 150)
    private String rsvSaleNm;

    @RealGridColumnInfo(headText = "사용여부", sortable = true, width = 50)
    private Character useYn;

    @RealGridColumnInfo(headText = "예약판매기간", sortable = true, width = 120)
    private String rsvSalePeriod;

    @RealGridColumnInfo(headText = "배송기간", sortable = true, width = 120)
    private String sendPeriod;

    private String rsvSaleStartDt;
    private String rsvSaleEndDt;
    private String saleWay;
    private int hopeSendDays;

    private String chgYn;

    @RealGridColumnInfo(headText = "등록자", width = 70)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 70)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME, sortable = true)
    private String chgDt;
}
