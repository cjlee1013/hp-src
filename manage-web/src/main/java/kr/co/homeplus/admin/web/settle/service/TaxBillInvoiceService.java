package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.taxBillInvoice.TaxBillInvoiceListGetDto;
import kr.co.homeplus.admin.web.settle.model.taxBillInvoice.TaxBillInvoiceListSetDto;
import kr.co.homeplus.admin.web.settle.model.taxBillInvoice.TaxBillReceiveGetDto;
import kr.co.homeplus.admin.web.settle.model.taxBillInvoice.TaxBillReceiveSetDto;
import kr.co.homeplus.admin.web.settle.model.taxBillInvoice.TaxBillReceiveSupplierGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TaxBillInvoiceService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    /**
     * 세금계산서 관리 > 세금계산서 조회 > 메인 리스트 조회
     */
    public List<TaxBillInvoiceListGetDto> getTaxBillInvoiceList(TaxBillInvoiceListSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/TaxBillInvoice/getTaxBillInvoiceList",
            new ParameterizedTypeReference<ResponseObject<List<TaxBillInvoiceListGetDto>>>(){}).getData();
    }

    /**
     * 세금계산서 관리 > 세금계산서 수취내역 관리
     */
    public List<TaxBillReceiveGetDto> getTaxBillReceiveList(TaxBillInvoiceListSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/TaxBillInvoice/getTaxBillReceiveList",
            new ParameterizedTypeReference<ResponseObject<List<TaxBillReceiveGetDto>>>(){}).getData();
    }

    /**
     * 세금계산서 관리 > 세금계산서 수취내역 관리 > 수취내역 등록
     */
    public ResponseObject<String> setTaxBillReceiveList(TaxBillReceiveSetDto listParamDto) throws Exception {
        listParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/TaxBillInvoice/setTaxBillReceiveList",
            new ParameterizedTypeReference<ResponseObject<String>>() {});
    }

    /**
     * 세금계산서 관리 > 세금계산서 수취내역 관리 > 수취내역 수정
     */
    public ResponseObject<String> updTaxBillReceive(TaxBillReceiveSetDto listParamDto) throws Exception {
        listParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/TaxBillInvoice/updTaxBillReceive",
            new ParameterizedTypeReference<ResponseObject<String>>() {});
    }


    /**
     * 세금계산서 관리 > 세금계산서 수취내역 관리 > 수취내역 확정
     */
    public ResponseObject<String> setTaxBillReceiveConfirm(TaxBillReceiveSetDto listParamDto) throws Exception {
        listParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/TaxBillInvoice/setTaxBillReceiveConfirm",
            new ParameterizedTypeReference<ResponseObject<String>>() {});
    }

    /**
     * 세금계산서 수취내역 > 공급자 정보 조회
     */
    public List<TaxBillReceiveSupplierGetDto> getTaxBillSupplierList(String taxBillReceiveType) throws Exception {
        return resourceClient.getForResponseObject(
            ResourceRouteName.SETTLE,
            "/admin/TaxBillInvoice/getTaxBillSupplierList?taxBillReceiveType=" + taxBillReceiveType,
            new ParameterizedTypeReference<ResponseObject<List<TaxBillReceiveSupplierGetDto>>>() {}).getData();
    }

    /**
     * 세금계산서 관리 > 세금계산서 수취내역 관리 > 수취내역 삭제
     */
    public ResponseObject<String> deleteTaxBillReceive(TaxBillReceiveSetDto listParamDto) throws Exception {
        listParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/TaxBillInvoice/deleteTaxBillReceive",
            new ParameterizedTypeReference<ResponseObject<String>>() {});
    }
}
