package kr.co.homeplus.admin.web.search.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Getter @Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TypoSetDto {
    private Long typoId;
    private String keyword;
    private String correctedTerms;
    private String updateDt;
}
