package kr.co.homeplus.admin.web.promotion.model.event;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class RmsPromoStoreGetDto implements Cloneable {
    @RealGridColumnInfo(headText = "점포ID", sortable = true, width = 40)
    private Integer storeId;

    @RealGridColumnInfo(headText = "점포유형", sortable = true, width = 40)
    private String storeType;

    @RealGridColumnInfo(headText = "점포명", sortable = true, width = 60)
    private String storeNm;
}