package kr.co.homeplus.admin.web.common.model.categoryMng;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class CategoryGetDto {

	@RealGridColumnInfo(headText = "카테고리명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, sortable = true, width = 250)
	private String cateNm;

	@RealGridColumnInfo(headText = "카테고리 ID", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String cateCd;

	@RealGridColumnInfo(headText = "장바구니 제한", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String cartYnNm;

	@RealGridColumnInfo(headText = "19금제한", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String adultLimitYnNm;

	@RealGridColumnInfo(headText = "사용여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String useYnNm;

	@RealGridColumnInfo(headText = "노출범위", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String dispYnNm;

	@RealGridColumnInfo(headText = "우선순위", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String priority;

	@RealGridColumnInfo(headText = "등록자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String regNm;

	@RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String regDt;

	@RealGridColumnInfo(headText = "수정자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String chgNm;

	@RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String chgDt;

	@RealGridColumnInfo(headText = "depth", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String depth;

	@RealGridColumnInfo(headText = "부모카테고리 ID", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String parentCateCd;

	@RealGridColumnInfo(headText = "lcateCd", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String lcateCd;

	@RealGridColumnInfo(headText = "mcateCd", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String mcateCd;

	@RealGridColumnInfo(headText = "scateCd", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String scateCd;

	@RealGridColumnInfo(headText = "cartYn", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String cartYn;

	@RealGridColumnInfo(headText = "useYn", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String useYn;

	@RealGridColumnInfo(headText = "clubDispYn", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String clubDispYn;

	@RealGridColumnInfo(headText = "dispYn", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String dispYn;

	@RealGridColumnInfo(headText = "adultLimitYn", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String adultLimitYn;

	@RealGridColumnInfo(headText = "isbnYn", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String isbnYn;

	@RealGridColumnInfo(headText = "reviewDisplay", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String reviewDisplay;

	@RealGridColumnInfo(headText = "apiUseYn", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String apiUseYn;

	// 레거시 1depth
	@RealGridColumnInfo(headText = "오프라인 매핑 1depth", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String division;

	// 레거시 2depth
	@RealGridColumnInfo(headText = "오프라인 매핑 2depth", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String groupNo;

	// 레거시 3depth
	@RealGridColumnInfo(headText = "오프라인 매핑 3depth", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String dept;

	// 레거시 4depth
	@RealGridColumnInfo(headText = "오프라인 매핑 4depth", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String classCd;

	// 레거시 5depth
	@RealGridColumnInfo(headText = "오프라인 매핑 5depth", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String subclass;

	// 레거시 1depth
	@RealGridColumnInfo(headText = "오프라인 매핑 1depth", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String divisionNm;

	// 레거시 2depth
	@RealGridColumnInfo(headText = "오프라인 매핑 2depth", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String groupNoNm;

	// 레거시 3depth
	@RealGridColumnInfo(headText = "오프라인 매핑 3depth", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String deptNm;

	// 레거시 4depth
	@RealGridColumnInfo(headText = "오프라인 매핑 4depth", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String classCdNm;

	// 레거시 5depth
	@RealGridColumnInfo(headText = "오프라인 매핑 5depth", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
	private String subclassNm;

	private String dcateCd;
	private String lcateNm;
	private String mcateNm;
	private String scateNm;
	private String dcateNm;
	private String cateName;
	private String apiUseYnNm;

	private String regId;
	private String chgId;

	private String ldispStartDt;
	private String ldispEndDt;
	private String mdispStartDt;
	private String mdispEndDt;

}
