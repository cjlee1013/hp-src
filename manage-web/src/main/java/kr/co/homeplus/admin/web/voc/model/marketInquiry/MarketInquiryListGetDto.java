package kr.co.homeplus.admin.web.voc.model.marketInquiry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 제휴사 고객문의 리스트 응답 DTO")
public class MarketInquiryListGetDto {
  @ApiModelProperty(notes = "문의번호")
  @RealGridColumnInfo(headText = "문의번호", width = 100, sortable = true)
  private String messageNo;

  @ApiModelProperty(notes = "체결번호")
  @RealGridColumnInfo(headText = "체결번호", width = 110, sortable = true)
  private String copOrdGoodSeq; //체결번호

  @ApiModelProperty(notes = "처리상태코드")
  @RealGridColumnInfo(headText = "처리상태코드", width = 120, sortable = true, hidden = true)
  private String isresponseYn; //처리상태코드

  @ApiModelProperty(notes = "처리상태")
  @RealGridColumnInfo(headText = "처리상태", width = 70, sortable = true)
  private String isresponseStatus; //처리상태

  @ApiModelProperty(notes = "제휴사코드")
  @RealGridColumnInfo(headText = "제휴사코드", width = 120, sortable = true, hidden = true)
  private String marketType; //제휴사코드

  @ApiModelProperty(notes = "마켓연동")
  @RealGridColumnInfo(headText = "마켓연동", width = 70, sortable = true)
  private String marketNm; //제휴사명

  @ApiModelProperty(notes = "등록구분코드")
  @RealGridColumnInfo(headText = "등록구분코드", width = 120, sortable = true, hidden = true)
  private String messageType; //등록구분코드

  @ApiModelProperty(notes = "등록구분")
  @RealGridColumnInfo(headText = "등록구분", width = 80, sortable = true)
  private String messageTypeNm; //등록구분명

  @ApiModelProperty(notes = "문의타입")
  @RealGridColumnInfo(headText = "문의타입", width = 120, sortable = true, hidden = true)
  private String contactType; //문의타입

  @ApiModelProperty(notes = "문의타입")
  @RealGridColumnInfo(headText = "문의타입", width = 90, sortable = true)
  private String contactNm; //문의타입명

  @ApiModelProperty(notes = "점포코드")
  @RealGridColumnInfo(headText = "점포코드", width = 120, sortable = true, hidden = true)
  private String storeId; //점포코드

  @ApiModelProperty(notes = "점포")
  @RealGridColumnInfo(headText = "점포", width = 100, sortable = true)
  private String storeNm; //점포명

  @ApiModelProperty(notes = "제목")
  @RealGridColumnInfo(headText = "제목", width = 200, columnType = RealGridColumnType.NONE, sortable = true)
  private String requestTitle; //제목

  @ApiModelProperty(notes = "문의일시")
  @RealGridColumnInfo(headText = "문의일시", width = 130, sortable = true)
  private String receiveDate; //문의일시

  @ApiModelProperty(notes = "주문번호")
  @RealGridColumnInfo(headText = "주문번호", width = 100, sortable = true)
  private String ordNo; //주문번호

  @ApiModelProperty(notes = "마켓주문번호")
  @RealGridColumnInfo(headText = "마켓주문번호", width = 110, sortable = true)
  private String copOrdNo; //제휴사주문번호

  @ApiModelProperty(notes = "상품번호")
  @RealGridColumnInfo(headText = "상품번호", width = 100, sortable = true)
  private String goodId; //상품번호

  @ApiModelProperty(notes = "마켓상품번호")
  @RealGridColumnInfo(headText = "마켓상품번호", width = 100, sortable = true)
  private String copGoodId; //제휴사상품번호

  @ApiModelProperty(notes = "문의자명")
  @RealGridColumnInfo(headText = "문의자명", width = 70, sortable = true)
  private String inquirerName; //문의자명

  @ApiModelProperty(notes = "문의자연락처")
  @RealGridColumnInfo(headText = "문의자연락처", width = 100, sortable = true)
  private String inquirerPhone; //문의자연락처

  @ApiModelProperty(notes = "처리일시")
  @RealGridColumnInfo(headText = "처리일시", width = 130, sortable = true)
  private String isresponseDate; //처리일시

  @ApiModelProperty(notes = "처리소요시간")
  @RealGridColumnInfo(headText = "처리소요시간", width = 110, sortable = true)
  private String responseDiff; //처리소요시간

  @ApiModelProperty(notes = "답변작성자")
  @RealGridColumnInfo(headText = "답변작성자", width = 80, sortable = true)
  private String isresponseNm; //답변작성자
}