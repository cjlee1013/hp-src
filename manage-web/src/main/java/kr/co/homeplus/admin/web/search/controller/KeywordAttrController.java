package kr.co.homeplus.admin.web.search.controller;

import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.search.model.KeywordAttrGridDto;
import kr.co.homeplus.admin.web.search.model.KeywordAttrSetDto;
import kr.co.homeplus.admin.web.search.model.KeywordAttrSetParamDto;
import kr.co.homeplus.admin.web.search.service.KeywordAttrService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/keywordAttr")
@RequiredArgsConstructor
public class KeywordAttrController {

    private final KeywordAttrService keywordAttrService;

    @GetMapping(value = "/keywordList")
    public String ExposureMain(Model model) {
        model.addAllAttributes(RealGridHelper.create("keywordAttrListGridBaseInfo", KeywordAttrGridDto.KeywordAttr.class));
        return "/search/keywordAttr";
    }

    @ResponseBody
    @GetMapping(value = {"/getKeywordAttrList.json"})
    @SuppressWarnings("unchecked")
    public List<KeywordAttrSetDto> getKeywordAttrList(@RequestParam String searchKeyword, @RequestParam String searchUseYn) {
        return (List<KeywordAttrSetDto>)keywordAttrService.getKeywordAttrList(searchKeyword, searchUseYn).getData();
    }

    @ResponseBody
    @GetMapping(value = {"/getKeywordAttrMng.json"})
    public Map getKeywordAttrMng(@RequestParam String keywordAttrNo) {
        return (Map)keywordAttrService.getKeywordAttrMng(keywordAttrNo).getData();
    }

    @ResponseBody
    @RequestMapping(value = {"/postKeywordAttr.json"}, method = RequestMethod.POST)
    public Object postKeywordAttr(@RequestBody @Valid KeywordAttrSetParamDto param) {
        return keywordAttrService.postKeywordAttr(param);
    }

    @ResponseBody
    @RequestMapping(value = {"/putKeywordAttr.json"}, method = RequestMethod.POST)
    public Object putKeywordAttr(@RequestBody @Valid KeywordAttrSetParamDto param) {
        return keywordAttrService.putKeywordAttr(param);
    }
}
