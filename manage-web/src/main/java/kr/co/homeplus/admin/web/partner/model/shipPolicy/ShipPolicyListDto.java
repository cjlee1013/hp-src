package kr.co.homeplus.admin.web.partner.model.shipPolicy;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShipPolicyListDto {

    
    //배송정책번호
    @RealGridColumnInfo(headText = "배송정책번호", columnType = RealGridColumnType.NUMBER_CENTER, width = 80, sortable = true)
    private Long shipPolicyNo ;

    //업체아이디
    @RealGridColumnInfo(headText = "업체아이디", hidden = true)
    private String partnerId;

    //업체명
    @RealGridColumnInfo(headText = "업체명", hidden = true)
    private String partnerNm;

    //점포코드
    @RealGridColumnInfo(headText = "점포코드", hidden = true)
    private String storeId;

    //점포명
    @RealGridColumnInfo(headText = "점포명", hidden = true)
    private String storeNm;

    //기본여부
    @RealGridColumnInfo(headText = "기본여부", hidden = true)
    private String defaultYn;

    //기본
    @RealGridColumnInfo(headText = "기본", width = 50, sortable = true)
    private String defaultYnNm;

    //배송정책명
    @RealGridColumnInfo(headText = "배송정책명", width = 200, sortable = true)
    private String shipPolicyNm;

    //배송방법
    @RealGridColumnInfo(headText = "배송방법", hidden = true)
    private String shipMethod;

    //배송방법
    @RealGridColumnInfo(headText = "배송방법", width = 100, sortable = true)
    private String shipMethodNm;

    //출고기한
    @RealGridColumnInfo(headText = "출고기한", hidden = true)
    private String releaseDay;

    //출고기한
    @RealGridColumnInfo(headText = "출고기한", sortable = true)
    private String releaseDayNm;

    //출고시간
    @RealGridColumnInfo(headText = "출고시간", sortable = true)
    private String releaseTime;

    //휴일제외여부
    @RealGridColumnInfo(headText = "휴일제외여부", hidden = true)
    private String holidayExceptYn;

    //배송유형
    @RealGridColumnInfo(headText = "배송유형", hidden = true)
    private String shipType;

    //배송유형
    @RealGridColumnInfo(headText = "배송유형", width = 100, sortable = true)
    private String shipTypeNm;

    //배송비종류
    @RealGridColumnInfo(headText = "배송비종류", hidden = true)
    private String shipKind;

    //배송비종류
    @RealGridColumnInfo(headText = "배송비종류", width = 100, sortable = true)
    private String shipKindNm;

    //배송비
    @RealGridColumnInfo(headText = "배송비", width = 80)
    private int shipFee;

    //무료조건비
    @RealGridColumnInfo(headText = "무료조건비", width = 80)
    private String freeCondition;

    //차등설정
    @RealGridColumnInfo(headText = "차등설정", hidden = true)
    private String diffYn;

    //차등갯수
    @RealGridColumnInfo(headText = "차등갯수", hidden = true)
    private String diffQty;

    //선결제여부
    @RealGridColumnInfo(headText = "선결제여부", hidden = true)
    private String prepaymentYn;

    //선결제여부
    @RealGridColumnInfo(headText = "선결제여부", sortable = true)
    private String prepaymentYnNm;

    //반품/교환비
    @RealGridColumnInfo(headText = "반품/교환비", sortable = true)
    private String claimShipFee;

    //제주추가배송비
    @RealGridColumnInfo(headText = "제주추가배송비", sortable = true)
    private String extraJejuFee;

    //도서산간배송비
    @RealGridColumnInfo(headText = "도서산간배송비", sortable = true)
    private String extraIslandFee;

    //배송가능지역
    @RealGridColumnInfo(headText = "배송가능지역", hidden = true)
    private String shipArea;

    //출고지우편번호
    @RealGridColumnInfo(headText = "출고지우편번호", hidden = true)
    private String releaseZipcode;

    //출고지주소1
    @RealGridColumnInfo(headText = "출고지주소1",hidden = true)
    private String releaseAddr1;

    //출고지주소2
    @RealGridColumnInfo(headText = "출고지주소2",hidden = true)
    private String releaseAddr2;

    //회수지우편번호
    @RealGridColumnInfo(headText = "회수지우편번호",hidden = true)
    private String returnZipcode;

    //회수지주소1
    @RealGridColumnInfo(headText = "회수지주소1",hidden = true)
    private String returnAddr1;

    //회수지주소2
    @RealGridColumnInfo(headText = "회수지주소2",hidden = true)
    private String returnAddr2;

    //안심번호여부
    @RealGridColumnInfo(headText = "안심번호여부",hidden = true)
    private String safeNumberUseYn;

    //사용여부
    @RealGridColumnInfo(headText = "사용여부",hidden = true)
    private String useYn;

    //등록자
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regNm;

    //등록일
    @RealGridColumnInfo(headText = "등록일", columnType= RealGridColumnType.DATETIME, width = 100)
    private String regDt;

    //수정자
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgNm;

    //수정일
    @RealGridColumnInfo(headText = "수정일",columnType= RealGridColumnType.DATETIME,  width = 100)
    private String chgDt;

    //배송비노출여부
    private String dispYn;

}
