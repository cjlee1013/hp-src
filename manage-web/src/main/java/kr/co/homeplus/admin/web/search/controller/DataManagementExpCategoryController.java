package kr.co.homeplus.admin.web.search.controller;

import java.util.Map;
import kr.co.homeplus.admin.web.search.model.DataManagementCategorySearchListGetParam;
import kr.co.homeplus.admin.web.search.model.DataManagementExpCategoryGridDto;
import kr.co.homeplus.admin.web.search.service.DataManagementExpCategoryService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/dataManagement/expCategory")
@RequiredArgsConstructor
public class DataManagementExpCategoryController {

    @Value("${spring.profiles.active}")
    private String profilesActive;

    private final DataManagementExpCategoryService expCategoryService;

    @GetMapping(value = { "/main" })
    public String getMain(Model model) {
        model.addAttribute("profilesActive",profilesActive);
        model.addAllAttributes(RealGridHelper.create("expCategorySearchGridBaseInfo", DataManagementExpCategoryGridDto.ExpCategoryList.class));
        return "/search/dataManagementExpCategoryMain";
    }

    @ResponseBody
    @GetMapping(value = {"/list.json"})
    public ResponseObject<Map<String,Object>> getList(DataManagementCategorySearchListGetParam param) {
        return expCategoryService.getList(param);
    }

    @ResponseBody
    @GetMapping(value = {"/filter.json"})
    public ResponseObject<Map<String,Object>> getFilter(DataManagementCategorySearchListGetParam param) {
        return expCategoryService.getFilter(param);
    }
}
