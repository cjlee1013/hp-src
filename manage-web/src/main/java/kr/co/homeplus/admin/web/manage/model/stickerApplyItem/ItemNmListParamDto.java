package kr.co.homeplus.admin.web.manage.model.stickerApplyItem;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품관리 Get Entry")
public class ItemNmListParamDto {

    private List<String> itemNoList;
}

