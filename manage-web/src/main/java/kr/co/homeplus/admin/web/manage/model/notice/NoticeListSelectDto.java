package kr.co.homeplus.admin.web.manage.model.notice;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class NoticeListSelectDto {

    @RealGridColumnInfo(headText = "번호", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private String noticeNo;

    @RealGridColumnInfo(headText = "노출영역", order = 1)
    private String dispAreaTxt;

    @RealGridColumnInfo(headText = "구분", order = 2)
    private String noticeKindTxt;

    @RealGridColumnInfo(headText = "제목", columnType = RealGridColumnType.NAME, width = 200, order = 3)
    private String noticeTitle;

    @RealGridColumnInfo(headText = "중요공지", order = 4)
    private String topYnTxt;

    @RealGridColumnInfo(headText = "노출사이트", order = 5)
    private String dispTxt;

    @RealGridColumnInfo(headText = "사용여부", width = 50, order = 6, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 7)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 8, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 9)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 10, sortable = true)
    private String chgDt;

    @RealGridColumnInfo(headText = "dispArea", order = 11, hidden = true)
    private String dispArea;
    @RealGridColumnInfo(headText = "noticeKind", order = 12, hidden = true)
    private String noticeKind;
    @RealGridColumnInfo(headText = "noticeDesc", order = 13, hidden = true)
    private String noticeDesc;
    @RealGridColumnInfo(headText = "useYn", order = 14, hidden = true)
    private String useYn;
    @RealGridColumnInfo(headText = "topYn", order = 15, hidden = true)
    private String topYn;
    @RealGridColumnInfo(headText = "dispHomeYn", order = 16, hidden = true)
    private String dispHomeYn;
    @RealGridColumnInfo(headText = "dispClubYn", order = 17, hidden = true)
    private String dispClubYn;


    private String regId;
    private String chgId;

}
