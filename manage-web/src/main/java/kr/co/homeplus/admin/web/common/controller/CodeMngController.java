package kr.co.homeplus.admin.web.common.controller;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGroupGetDto;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGroupSetDto;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeSetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 시스템관리 > 기준정보관리 > 공통코드관리
 */
@Slf4j
@Controller
@RequestMapping("/common/code")
public class CodeMngController {

	final private ResourceClient resourceClient;
	final private LoginCookieService cookieService;

	public CodeMngController(ResourceClient resourceClient,
			LoginCookieService cookieService) {
		this.resourceClient = resourceClient;
		this.cookieService = cookieService;
	}

	/**
	 * 시스템관리 > 기준정보관리 > 공통코드관리
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/codeMain", method = RequestMethod.GET)
	public String codeMain(Model model) {
		// 유형코드정보
		model.addAllAttributes(RealGridHelper.create("codeGGridBaseInfo", MngCodeGroupGetDto.class));
		// 코드정보
		model.addAllAttributes(RealGridHelper.create("codeDGridBaseInfo", MngCodeGetDto.class));

		return "/common/codeMain";
	}

	/**
	 * 유형코드 조회
	 * @param schType
	 * @param schValue
	 * @return CodeMGetDto
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = {"/getTypeCodeList.json"}, method = RequestMethod.GET)
	public List<MngCodeGroupGetDto> getTypeCodeList(@RequestParam(value = "schType") String schType, @RequestParam(value = "schValue") String schValue) {
		List<SetParameter> setParameterList = new ArrayList<>();
		setParameterList.add(SetParameter.create("schType", schType));
		setParameterList.add(SetParameter.create("schValue", schValue));

		return resourceClient.getForResponseObject(
				ResourceRouteName.ITEM,
				"/common/getTypeCodeList" + StringUtil.getParameter(setParameterList),
				new ParameterizedTypeReference<ResponseObject<List<MngCodeGroupGetDto>>>() {}).getData();
	}

	/**
	 * 유형코드 등록/수정
	 * @param codeMSetDto
	 * @return ResponseResult
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = {"/setTypeCode.json"}, method = RequestMethod.POST)
	public ResponseResult setTypeCode(MngCodeGroupSetDto codeMSetDto) {
		codeMSetDto.setUserId(cookieService.getUserInfo().getEmpId());

		return resourceClient.postForResponseObject(
			ResourceRouteName.ITEM,
			codeMSetDto, "/common/setTypeCode",
			new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
			}).getData();
	}

	/**
	 * 코드 조회
	 * @param typeCode
	 * @return CodeDGetDto
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = {"/getCodeList.json"}, method = RequestMethod.GET)
	public List<MngCodeGetDto> getCodeList(@RequestParam(value = "typeCode") String typeCode) {
		List<SetParameter> setParameterList = new ArrayList<>();
		setParameterList.add(SetParameter.create("typeCode", typeCode));

		return resourceClient.getForResponseObject(
				ResourceRouteName.ITEM,
				"/common/getCodeList" + StringUtil.getParameter(setParameterList),
				new ParameterizedTypeReference<ResponseObject<List<MngCodeGetDto>>>() {}).getData();
	}

	/**
	 * 코드 등록/수정
	 * @param codeDSetDto
	 * @return ResponseResult
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = {"/setCode.json"}, method = RequestMethod.POST)
	public ResponseResult setCode(MngCodeSetDto codeDSetDto) {
		codeDSetDto.setUserId(cookieService.getUserInfo().getEmpId());

		return resourceClient.postForResponseObject(
			ResourceRouteName.ITEM,
			codeDSetDto, "/common/setCode",
			new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
			}).getData();
	}
}
