package kr.co.homeplus.admin.web.partner.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.partner.enums.MarketType;
import kr.co.homeplus.admin.web.partner.model.market.MarketCategoryDetailGetDto;
import kr.co.homeplus.admin.web.partner.model.market.MarketCategoryMapListGetDto;
import kr.co.homeplus.admin.web.partner.model.market.MarketCategoryMapParamDto;
import kr.co.homeplus.admin.web.partner.model.market.MarketCategoryMapSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/partner")
public class MarketCategoryController {

    final private ResourceClient resourceClient;
    final private LoginCookieService cookieService;
    final private CodeService codeService;

    public MarketCategoryController(ResourceClient resourceClient,
                                LoginCookieService cookieService,
                                CodeService codeService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
    }

    /**
     * 업체관리 > 마켓연동관리 > 마켓연동 카테고리매핑
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/marketCategoryMain")
    public String MarketCategoryMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "reg_yn"       // 사용여부
            , "operator_type"       // 사업자유형
            , "partner_grade"       // 회원등급
            , "partner_status"      // 회원상태
            , "apply_yn"            // 연동여부
        );

        model.addAttribute("partnerGrade", code.get("partner_grade"));
        model.addAttribute("partnerStatus", code.get("partner_status"));
        model.addAttribute("approvalStatus", code.get("approval_status"));
        model.addAttribute("regYn", code.get("reg_yn"));
        model.addAttribute("applyYn", code.get("apply_yn"));

        model.addAttribute("gmarket", MarketType.GMAREKT.getId());
        model.addAttribute("auction", MarketType.AUCTION.getId());
        model.addAttribute("elevenSt", MarketType.ELEVEN.getId());
        model.addAttribute("naver", MarketType.NAVER.getId());

        //그리드
        model.addAllAttributes(
            RealGridHelper.create("marketCategoryListGridBaseInfo", MarketCategoryMapListGetDto.class));

        return "/partner/marketCategoryMain";
    }

    /**
     * 마켓 카테고리 조회
      * @param depth
     * @param pCateCd
     * @param cateCd
     * @param partnerId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getMarketCategoryForSelectBox.json"}, method = RequestMethod.GET)
    List<Map<String, Object>> getMarketCategoryForSelectBox(
        @RequestParam(name = "depth") String depth,
        @RequestParam(name = "pCateCd") String pCateCd,
        @RequestParam(name = "cateCd") String cateCd,
        @RequestParam(name = "partnerId") String partnerId ) {

        StringBuffer apiUri = new StringBuffer();
        apiUri.append("/partner/marketCategory/getMarketCategory").append("?depth=").append(depth)
            .append("&partnerId=").append(partnerId)
            .append("&pCateCd=").append(pCateCd)
            .append("&cateCd=").append(cateCd);

        List<Map<String, Object>> data = (List<Map<String, Object>>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri.toString(), new ParameterizedTypeReference<ResponseObject<List<Map<String, Object>>>>() {
        }).getData();

        return data;
    }

    /**
     * 마켓카테고리 디테일 조회
     * @param marketCateCd
     * @param partnerId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getMarketCategoryDetail.json"}, method = RequestMethod.GET)
    MarketCategoryDetailGetDto getMarketCategoryDetail(
        @RequestParam(name = "marketCateCd") String marketCateCd,
        @RequestParam(name = "partnerId") String partnerId ) {

        String apiUri = "/partner/marketCategory/getMarketCategoryDetail?";
        apiUri += "partnerId=" + partnerId;
        apiUri += "&marketCateCd=" + marketCateCd;

        return (MarketCategoryDetailGetDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM,
            apiUri, new ParameterizedTypeReference<ResponseObject<MarketCategoryDetailGetDto>>() {}).getData();
    }

    /**
     * 마켓연동 카테고리맵핑 리스트 조회
     * @param marketCategoryMapParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getMarketCategoryMapList.json"})
    public List<MarketCategoryMapListGetDto> getMarketCategoryMapList(
        MarketCategoryMapParamDto marketCategoryMapParamDto) {

        String apiUri = "/partner/marketCategory/getMarketCategoryMapList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<MarketCategoryMapListGetDto>>>(){};
        return (List<MarketCategoryMapListGetDto>)resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil
            .getRequestString(apiUri, MarketCategoryMapParamDto.class, marketCategoryMapParamDto), typeReference).getData();
    }

    /**
     * 마켓연동 카테고리 맵핑 등록/수정
     * @param marketCategoryMapSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/setMarketCategoryMap.json")
    public ResponseResult setMarketCategoryMap(@RequestBody MarketCategoryMapSetDto marketCategoryMapSetDto) throws Exception {
        String apiUrl = "/partner/marketCategory/setMarketCategoryMap";
        marketCategoryMapSetDto.setUserId(cookieService.getUserInfo().getEmpId());

        ResponseObject<ResponseResult> responseObject = resourceClient.postForResponseObject(ResourceRouteName.ITEM,  marketCategoryMapSetDto
            , apiUrl, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {});

        ResponseResult responseData = responseObject.getData();
        return responseData;

    }
}


