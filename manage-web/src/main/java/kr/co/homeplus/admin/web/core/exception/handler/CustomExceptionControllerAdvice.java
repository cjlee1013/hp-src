package kr.co.homeplus.admin.web.core.exception.handler;

import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Custom Exception 헨들러를 관리하는 클래스
 */
@Slf4j
@ControllerAdvice
public class CustomExceptionControllerAdvice {

    @ExceptionHandler(BusinessLogicException.class)
    public ResponseEntity<ResponseObject<String>> handleBusinessLogicException(
        BusinessLogicException ex, HttpServletRequest request) {
        ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.UNPROCESSABLE_ENTITY)
            .returnCode(ex.getErrorCode())
            .returnMessage(CustomExceptionControllerAdvice.getErrorMsgReplace(ex.getDescription(), ex.getErrorMsg(), ex.getErrorMsg2())).build();

        return ResourceConverter.toResponseEntity(responseObject, HttpStatus.UNPROCESSABLE_ENTITY);

    }

    private static String getErrorMsgReplace(String description, String msg, String msg2) {
        try {
            String retMsg;

            if (StringUtils.isBlank(msg)) {
                retMsg = description;
            } else {
                retMsg = description.replace("%d", msg);
            }

            if (!StringUtils.isBlank(msg2)) {
                retMsg = retMsg.replace("%s", msg2);
            }

            return retMsg;
        } catch(Exception e) {
            return StringUtils.isBlank(msg) ? description : "(" + msg + ")" + description;
        }
    }
}