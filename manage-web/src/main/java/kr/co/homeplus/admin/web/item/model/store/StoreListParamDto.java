package kr.co.homeplus.admin.web.item.model.store;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StoreListParamDto {

    //검색어
    private String searchType;

    //검색키워드
    private String searchKeyword;

    //점포유형
    private String searchStoreType;

    //점포종류
    private String searchStoreKind;

    //구분
    private String searchStoreAttr;

    //사용여부
    private String searchUseYn;

}
