package kr.co.homeplus.admin.web.promotion.model.promo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class PromoUploadSetDto {
    //쿠폰 적용대상 상품, 카드사 즉시할인 상품명
    @EqualsAndHashCode.Include
    private String itemNo;
    private String itemNm;

    //쿠폰 적용대상 파트너
    @EqualsAndHashCode.Include
    private String partnerId;
    private String partnerNm;

    //카드사 즉시할인 관련
    private String discountType; // 정률:2, 정액:1
    private String discountRate; // 정률값
    private String discountPrice; // 정액값
    private String feeAmt;
    private String commissionRate;
    @EqualsAndHashCode.Include
    private String couponCd; // 쿠폰코드
    private String issueStartDt;
    private String issueEndDt;
    private String groupCd;

    //쿠폰일괄발급
    private long userNo;
    private String error;

    //예약상품판매관리 > 상품업로드 시 발송지
    private String sendPlace;
}
