package kr.co.homeplus.admin.web.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "일자별 SHIFT 관리 Model")
@Getter
@Setter
@EqualsAndHashCode
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class DailyShiftManageModel {
    @ApiModelProperty(value = "상품번호", position = 1)
    @RealGridColumnInfo(headText = "상품번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 2)
    @RealGridColumnInfo(headText = "상품명", width = 200, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME)
    private String itemNm;

    @ApiModelProperty(value = "점포유형", position = 3)
    @RealGridColumnInfo(headText = "점포유형", width = 80, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeType;

    @ApiModelProperty(value = "주문기간", position = 4)
    @RealGridColumnInfo(headText = "주문기간", width = 150, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String orderPeriod;

    @ApiModelProperty(value = "SHIFT배송기간", position = 5)
    @RealGridColumnInfo(headText = "배송기간", width = 150, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String shiftShipPeriod;

    @ApiModelProperty(value = "수정일시", position = 6)
    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATE, renderFormat = "yyyy-MM-dd")
    private String chgDt;

    @ApiModelProperty(value = "수정자ID", position = 7)
    @RealGridColumnInfo(headText = "수정자", width = 80, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgId;

    @ApiModelProperty(value = "등록일시", position = 8)
    @RealGridColumnInfo(headText = "등록일시", hidden = true, fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATE, renderFormat = "yyyy-MM-dd")
    private String regDt;

    @ApiModelProperty(value = "등록자ID", position = 9)
    @RealGridColumnInfo(headText = "등록자ID", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regId;

    @ApiModelProperty(value = "주문시작일시", position = 10)
    @RealGridColumnInfo(headText = "주문시작일시", hidden = true, fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATE, renderFormat = "yyyy-MM-dd")
    private String orderStartDt;

    @ApiModelProperty(value = "주문종료일시", position = 11)
    @RealGridColumnInfo(headText = "주문종료일시", hidden = true, fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATE, renderFormat = "yyyy-MM-dd")
    private String orderEndDt;

    @ApiModelProperty(value = "SHIFT배송시작일시", position = 12)
    @RealGridColumnInfo(headText = "SHIFT배송시작일시", hidden = true, fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATE, renderFormat = "yyyy-MM-dd")
    private String shiftShipStartDt;

    @ApiModelProperty(value = "SHIFT배송종료일시", position = 13)
    @RealGridColumnInfo(headText = "SHIFT배송종료일시", hidden = true, fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATE, renderFormat = "yyyy-MM-dd")
    private String shiftShipEndDt;

    @ApiModelProperty(value = "사용여부", position = 14)
    @RealGridColumnInfo(headText = "사용여부", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String useYn;

    @ApiModelProperty(value = "일자별SHIFT관리순번", position = 15)
    @RealGridColumnInfo(headText = "일자별SHIFT관리순번", hidden = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.BASIC)
    private Long dailyShiftMngSeq;
}
