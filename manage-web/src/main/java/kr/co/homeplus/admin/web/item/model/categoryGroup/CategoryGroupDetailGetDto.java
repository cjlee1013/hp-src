package kr.co.homeplus.admin.web.item.model.categoryGroup;

import java.util.List;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CategoryGroupDetailGetDto {

	private List<CategoryGroupImgGetDto> categoryGroupImgGetDtoList;

	private List<CategoryGroupLcateGetDto> categoryGroupLcateGetDtoList;

}
