package kr.co.homeplus.admin.web.order.model.orderPopup;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "현금영수증 발급내역")
public class OrderCashReceiptDto {
    @ApiModelProperty(value = "주무번호")
    private long purchaseOrderNo;
    @ApiModelProperty(value = "현금영수증종류")
    private String cashReceiptUsage;
    @ApiModelProperty(value = "현금영수증타입")
    private String cashReceiptType;
    @ApiModelProperty(value = "현금영수증요청번호")
    private String cashReceiptReqNo;
    @ApiModelProperty(value = "현금영수증발행여부")
    private String cashReceiptYn;
}
