package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 배분율 관리 > 결제정책 히스토리 search DTO")
public class PaymentPolicyHistSelectDto {
    private String schFromDt;
    private String schEndDt;
    private String schItemCd;
    private String schLcateCd;
    private String schMcateCd;
    private String schScateCd;
    private String schDcateCd;
    private String schAffiliateType;
    private String schAffiliateValue;
    private Long policyNo;
}
