package kr.co.homeplus.admin.web.common.model.pop;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class CouponPopListGetDto {
    @RealGridColumnInfo(headText = "쿠폰 번호", sortable = true)
    private Long couponNo;

    @RealGridColumnInfo(headText = "점포유형")
    private String storeType;

    @RealGridColumnInfo(headText = "관리쿠폰명", width = 400)
    private String manageCouponNm;

    @RealGridColumnInfo(headText = "전시쿠폰명", width = 400)
    private String displayCouponNm;

    @RealGridColumnInfo(headText = "적용시스템", width = 200)
    private String applySystem;

    @RealGridColumnInfo(headText = "쿠폰종류")
    private String couponType;

    @RealGridColumnInfo(headText = "쿠폰상태")
    private String status;

    @RealGridColumnInfo(headText = "할인액/율")
    private String discount;
}