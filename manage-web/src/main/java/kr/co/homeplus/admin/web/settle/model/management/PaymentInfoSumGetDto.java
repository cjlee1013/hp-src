package kr.co.homeplus.admin.web.settle.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "결제일마감")
public class PaymentInfoSumGetDto {
  @ApiModelProperty(notes = "상품구분")
  @RealGridColumnInfo(headText = "상품구분", sortable = true, width = 100)
  private String mallType;

  @ApiModelProperty(notes = "점포코드")
  @RealGridColumnInfo(headText = "점포코드", sortable = true, width = 100)
  private String originStoreId;

  @ApiModelProperty(notes = "점포명")
  @RealGridColumnInfo(headText = "점포명", sortable = true, width = 100)
  private String originStoreNm;

  @ApiModelProperty(notes = "매출(IN VAT)")
  @RealGridColumnInfo(headText = "매출(IN VAT)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String completeAmt;

  @ApiModelProperty(notes = "정산(IN VAT)")
  @RealGridColumnInfo(headText = "정산(IN VAT)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String settleAmt;

  @ApiModelProperty(notes = "판매수수료")
  @RealGridColumnInfo(headText = "판매수수료", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String saleAgencyFee;

  @ApiModelProperty(notes = "배송비")
  @RealGridColumnInfo(headText = "배송비", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String shipAmt;

  @ApiModelProperty(notes = "반품배송비")
  @RealGridColumnInfo(headText = "반품배송비", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String claimReturnShipAmt;

  @ApiModelProperty(notes = "상품할인(업체)")
  @RealGridColumnInfo(headText = "상품할인(업체)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String couponSellerChargeAmt;

  @ApiModelProperty(notes = "상품할인(자사)")
  @RealGridColumnInfo(headText = "상품할인(자사)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String couponHomeChargeAmt;

  @ApiModelProperty(notes = "상품할인(카드)")
  @RealGridColumnInfo(headText = "상품할인(카드)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String couponCardChargeAmt;

  @ApiModelProperty(notes = "카드할인(자사)")
  @RealGridColumnInfo(headText = "카드할인(자사)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String cardCouponHomeAmt;

  @ApiModelProperty(notes = "카드할인(카드)")
  @RealGridColumnInfo(headText = "카드할인(카드)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String cardCouponCardAmt;

  @ApiModelProperty(notes = "배송비할인")
  @RealGridColumnInfo(headText = "배송비할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String shipDiscountAmt;

  @ApiModelProperty(notes = "임직원할인")
  @RealGridColumnInfo(headText = "임직원할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String empDiscountAmt;

  @ApiModelProperty(notes = "행사할인")
  @RealGridColumnInfo(headText = "행사할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String promoDiscountAmt;

  @ApiModelProperty(notes = "장바구니할인")
  @RealGridColumnInfo(headText = "장바구니할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String cartCouponAmt;

  @ApiModelProperty(notes = "매출(E-KPI)")
  @RealGridColumnInfo(headText = "매출(E-KPI)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String ekpiAmt;

  @ApiModelProperty(notes = "결제금액 합계")
  @RealGridColumnInfo(headText = "결제금액 합계", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String orderPrice;

  @ApiModelProperty(notes = "PG 결제금액")
  @RealGridColumnInfo(headText = "PG", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String pgAmt;

  @ApiModelProperty(notes = "DGV 결제금액")
  @RealGridColumnInfo(headText = "DGV", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String dgvAmt;

  @ApiModelProperty(notes = "MHC 결제금액")
  @RealGridColumnInfo(headText = "MHC", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String mhcAmt;

  @ApiModelProperty(notes = "OCB 결제금액")
  @RealGridColumnInfo(headText = "OCB", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String ocbAmt;

  @ApiModelProperty(notes = "마일리지 결제금액")
  @RealGridColumnInfo(headText = "마일리지", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String mileageAmt;
}
