package kr.co.homeplus.admin.web.manage.model.dspLeaflet;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspLeafletListParamDto {

    private String searchStartDt;
    private String searchEndDt;
    private String searchPeriodType;
    private String searchType;
    private String schKeyword;
    private String schSiteType;

}
