package kr.co.homeplus.admin.web.message.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
@Getter
@Setter
public class SendListGrid {

    @RealGridColumnInfo(headText = "주문자 회원번호")
    private String identifier;

    @RealGridColumnInfo(headText = "수신자 연락처")
    private String maskingToken;

    @RealGridColumnInfo(headText = "마스킹되지 않은 실제 연락처", hidden = true)
    private String toToken;
}
