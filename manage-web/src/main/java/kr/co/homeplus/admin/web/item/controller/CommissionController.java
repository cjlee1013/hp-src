package kr.co.homeplus.admin.web.item.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.commission.CommissionListParamDto;
import kr.co.homeplus.admin.web.item.model.commission.CommissionListSelectDto;
import kr.co.homeplus.admin.web.item.model.commission.CommissionParamDto;
import kr.co.homeplus.admin.web.item.model.commission.CommissionPartnerListSelectDto;
import kr.co.homeplus.admin.web.item.model.commission.CommissionSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item")
public class CommissionController {

    final private ResourceClient resourceClient;
    final private LoginCookieService cookieService;
    final private CodeService codeService;

    /**
     * 판매자관리 > 판매자관리 > 판매자별 기준수수료율
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/commission/commissionMain", method = RequestMethod.GET)
    public String commissionMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                "use_yn"        // 사용여부
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAllAttributes(RealGridHelper.create("commissionListGridBaseInfo", CommissionListSelectDto.class));

        return "/item/commissionMain";
    }

    /**
     * 판매자관리 > 판매자관리 > 판매자별 기준수수료율 > 리스트
     *
     * @param listParamDto
     * @return BannedWordSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/commission/getCommissionList.json"}, method = RequestMethod.GET)
    public List<CommissionListSelectDto> getBannedWordList(CommissionListParamDto listParamDto) {

        String apiUri = "/item/commission/getCommissionList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CommissionListSelectDto>>>() {};
        return (List<CommissionListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, CommissionListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * 판매자관리 > 판매자관리 > 판매자별 기준수수료율 > 파트너 검색 팝업
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/popup/searchPartnerPopup", method = RequestMethod.GET)
    public String searchPopup(Model model) {
        model.addAllAttributes(RealGridHelper.create("partnerSearchPopGridBaseInfo", CommissionPartnerListSelectDto.class));
        return "/item/pop/partnerSearchPop";
    }

    /**
     * 판매자관리 > 판매자관리 > 판매자별 기준수수료율 > 파트너 리스트 ( 팝업 )
     *
     * @param listParamDto
     * @return CommissionPartnerListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/commission/getPartnerList.json", method = RequestMethod.GET)
    public List<CommissionPartnerListSelectDto> getPartnerList(CommissionListParamDto listParamDto) {

        String apiUri = "/item/commission/getPartnerList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CommissionPartnerListSelectDto>>>() {};
        return (List<CommissionPartnerListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, CommissionListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * 판매자관리 > 판매자관리 > 판매자별 기준수수료율 등록/수정
     *
     * @param setParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/commission/setCommission.json"}, method = RequestMethod.POST)
    public ResponseResult getBannedWordList(@Valid @RequestBody CommissionSetParamDto setParamDto) {
        setParamDto.setRegId(cookieService.getUserInfo().getEmpId());
        String apiUri = "/item/commission/setCommission";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {};
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, setParamDto, apiUri, typeReference).getData();
    }

    @ResponseBody
    @GetMapping(value = "/commission/getBaseCommissionRate.json")
    public BigDecimal getBaseCommissionRate(CommissionParamDto paramDto) {

        String apiUri = "/item/commission/getBaseCommissionRate";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<BigDecimal>>() {};
        return (BigDecimal) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, CommissionParamDto.class, paramDto), typeReference).getData();
    }

}
