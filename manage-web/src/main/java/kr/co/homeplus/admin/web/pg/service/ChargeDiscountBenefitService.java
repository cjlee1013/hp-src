package kr.co.homeplus.admin.web.pg.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.pg.model.ChargeDiscountBenefitManageDto;
import kr.co.homeplus.admin.web.pg.model.ChargeDiscountBenefitManageSelectDto;
import kr.co.homeplus.admin.web.pg.model.ParentMethodCd;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ChargeDiscountBenefitService {

    private final ResourceClient resourceClient;

    /**
     * 사이트 별 카드 코드 리스트 조회
     * @return
     * @throws Exception
     */
    public Map<String, List<PaymentMethodDto>> getMethodCdBySiteType() throws Exception {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("parentMethodCd", ParentMethodCd.CARD.getMethodCd()));

        String apiUri = EscrowConstants.ESCROW_GET_METHOD_CD_BY_SITE_TYPE + StringUtil.getParameter(getParameterList);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Map<String, List<PaymentMethodDto>>>>() {};
        Map<String, List<PaymentMethodDto>> paymentMethodListMap = (Map<String, List<PaymentMethodDto>>) resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
        return paymentMethodListMap;
    }

    /**
     * 청구할인 혜택 조회
     * @param chargeDiscountBenefitManageSelectDto
     * @return
     * @throws Exception
     */
    public List<ChargeDiscountBenefitManageDto> getChargeDiscountBenefitManageList(ChargeDiscountBenefitManageSelectDto chargeDiscountBenefitManageSelectDto) throws Exception {

        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_CHARGE_DISCOUNT_BENEFIT_MANAGE, ChargeDiscountBenefitManageSelectDto.class, chargeDiscountBenefitManageSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ChargeDiscountBenefitManageDto>>>() {};
        List<ChargeDiscountBenefitManageDto> chargeDiscountBenefitManageDtoList = (List<ChargeDiscountBenefitManageDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri,
            typeReference).getData();

        return chargeDiscountBenefitManageDtoList;
    }

    /**
     * 청구할인 혜택 등록/수정
     * @param chargeDiscountBenefitManageDto
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> saveChargeDiscountBenefitManage(ChargeDiscountBenefitManageDto chargeDiscountBenefitManageDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_SAVE_CHARGE_DISCOUNT_BENEFIT_MANAGE;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, chargeDiscountBenefitManageDto, apiUri, typeReference);
    }

}
