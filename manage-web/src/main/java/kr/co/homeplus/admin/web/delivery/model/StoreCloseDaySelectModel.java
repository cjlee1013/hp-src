package kr.co.homeplus.admin.web.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 점포배송정보 > 점포별 휴일 조회 SELECT DTO")
public class StoreCloseDaySelectModel {
    @ApiModelProperty(notes = "점포유형(HYPER,EXP,CLUB)", position = 1)
    private String schStoreType;
    @ApiModelProperty(notes = "휴무유형(F:자율휴무,R:정기휴무,S:설,C:추석,E:기타)", position = 2)
    private String schCloseDayType;
    @ApiModelProperty(notes = "검색어유형(SEQ:등록번호,TITLE:등록제목)", position = 3)
    private String schKeywordType;
    @ApiModelProperty(notes = "검색어", position = 4)
    private String schKeyword;
    @ApiModelProperty(notes = "진행상태", position = 5)
    private String schUseYn;
    @ApiModelProperty(notes = "진행기간(To)", position = 6)
    private String schApplyStartDt;
    @ApiModelProperty(notes = "진행기간(From)", position = 7)
    private String schApplyEndDt;
}
