package kr.co.homeplus.admin.web.item.model.mngAttribute;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
public class MngAttributeListSelectDto {
    @RealGridColumnInfo(headText = "속성번호", sortable = true, width = 70, columnType = RealGridColumnType.NUMBER, fieldType = RealGridFieldType.NUMBER)
    private long attrNo;

    @RealGridColumnInfo(headText = "분류번호", hidden = true)
    private long gattrNo;

    @RealGridColumnInfo(headText = "속성명", sortable = true, width = 180)
    private String attrNm;

    @RealGridColumnInfo(headText = "우선순위", sortable = true, width = 70, columnType = RealGridColumnType.NUMBER, fieldType = RealGridFieldType.NUMBER)
    private Integer priority;

    @RealGridColumnInfo(headText = "구분", hidden = true)
    private String gattrTypeTxt;

    @RealGridColumnInfo(headText = "사용여부코드", hidden = true)
    private String useYn;

    @RealGridColumnInfo(headText = "사용여부", sortable = true, width = 70)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "imgHeight", hidden = true)
    private Integer imgHeight;

    @RealGridColumnInfo(headText = "imgWidth", hidden = true)
    private Integer imgWidth;

    @RealGridColumnInfo(headText = "이미지경로", hidden = true)
    private String imgUrl;

    @RealGridColumnInfo(headText = "이미지명", hidden = true)
    private String imgNm;

    @RealGridColumnInfo(headText = "색상타입", hidden = true)
    private String colorType;

    @RealGridColumnInfo(headText = "색상코드", hidden = true)
    private String colorCode;

    @RealGridColumnInfo(headText = "등록자", hidden = true)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, hidden = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자")
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String chgDt;
}
