package kr.co.homeplus.admin.web.user.model.user;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class UserGradeHistoryDto {
    @RealGridColumnInfo(headText = "일련번호", hidden = true)
    private long seq;
    @RealGridColumnInfo(headText = "회원일련번호", hidden = true)
    private long userNo;
    @RealGridColumnInfo(headText = "등급번호", hidden = true)
    private long gradeSeq;
    @RealGridColumnInfo(headText = "등록자", hidden = true)
    private String regStaffId;
    @RealGridColumnInfo(headText = "년/월", width = 30)
    private String gradeApplyYm;
    @RealGridColumnInfo(headText = "등급", width = 30)
    private String gradeNm;
    @RealGridColumnInfo(headText = "구매건수", width = 50, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long orderCnt;
    @RealGridColumnInfo(headText = "구매금액", width = 50, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE)
    private long orderAmount;
    @RealGridColumnInfo(headText = "적용일시", width = 45)
    private String regDt;
    @RealGridColumnInfo(headText = "등급조정사유", width = 110, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME)
    private String reason;
    @RealGridColumnInfo(headText = "콜센터문의번호", width = 30)
    private Long callCenterSeq;
}
