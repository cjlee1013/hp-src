package kr.co.homeplus.admin.web.statistics.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponStatistics {

    private String storeType;
    private String storeNm;
    private long couponNo;
    private String couponNm;
    private String couponKind;
    private String couponKindNm;
    private long orderCnt;
    private String itemNo;
    private String lcateNm;
    private String mcateNm;
    private long orderAmt;
    private int couponUseCnt;
    private long discountAmt;
    private long claimDiscountAmt;
    private int claimCnt;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{")
            .append("storeType:").append(storeType).append(",")
            .append("storeNm:").append(storeNm).append(",")
            .append("couponNo:").append(couponNo).append(",")
            .append("couponNm:").append(couponNm).append(",")
            .append("couponKind:").append(couponKind).append(",")
            .append("orderCnt:").append(orderCnt)
            .append("}");

        return sb.toString();
    }

}
