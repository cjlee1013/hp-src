package kr.co.homeplus.admin.web.claim.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.claim.model.refundFail.RefundFailListGetDto;
import kr.co.homeplus.admin.web.claim.model.refundFail.RefundFailListSetDto;
import kr.co.homeplus.admin.web.claim.model.refundFail.RequestRefundCompleteGetDto;
import kr.co.homeplus.admin.web.claim.model.refundFail.RequestRefundCompleteSetDto;
import kr.co.homeplus.admin.web.claim.service.RefundFailService;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/claim/popup/refundFail")
@RequiredArgsConstructor
public class RefundFailPopupController {

    /**
     * 환불완료 팝업창
     */
    @GetMapping("/getRefundCompletePop")
    public String refundCompletePop(Model model, HttpServletRequest request) throws Exception {
        return "/claim/pop/refundCompletePop";
    }
}
