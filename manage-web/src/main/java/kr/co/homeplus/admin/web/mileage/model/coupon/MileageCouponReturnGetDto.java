package kr.co.homeplus.admin.web.mileage.model.coupon;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마일리지 쿠폰 회수 결과")
public class MileageCouponReturnGetDto {
    private int successCnt;
    private int failCnt;

    public void addSuccessCnt(){
        this.successCnt += 1;
    }
    public void addSuccessCnt(int cnt){
        this.successCnt += cnt;
    }

    public void addFailCnt(){
        this.failCnt += 1;
    }
    public void addFailCnt(int cnt){
        this.failCnt += cnt;
    }
}
