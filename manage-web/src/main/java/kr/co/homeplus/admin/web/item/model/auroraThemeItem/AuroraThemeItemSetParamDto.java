package kr.co.homeplus.admin.web.item.model.auroraThemeItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuroraThemeItemSetParamDto {

    private Long themeId;
    private String itemNo;
    private int priority;
    private String useYn;

}
