package kr.co.homeplus.admin.web.core.certification;

import lombok.Data;

/**
 * 인증토큰 dto
 */
@Data
public class CertificationTokenDto {
    private String expireTime;
    private String createTime;
    private String amsToken;
    private String userInfo;
}
