package kr.co.homeplus.admin.web.search.controller;

import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.search.model.AnalysisPluginGetDto;
import kr.co.homeplus.admin.web.search.model.DicTypeCd;
import kr.co.homeplus.admin.web.search.model.DictionaryComponentGetDto;
import kr.co.homeplus.admin.web.search.model.DictionaryInsertCompoundsSetParamDto;
import kr.co.homeplus.admin.web.search.model.DictionaryInsertMorphExpansionSetParamDto;
import kr.co.homeplus.admin.web.search.model.DictionaryInsertPreAnalysisSetParamDto;
import kr.co.homeplus.admin.web.search.model.DictionaryInsertTypoCorrectSetParamDto;
import kr.co.homeplus.admin.web.search.model.DictionaryInsertWordInfoSetParamDto;
import kr.co.homeplus.admin.web.search.model.DictionaryListGetDto;
import kr.co.homeplus.admin.web.search.model.DictionaryListGetParamDto;
import kr.co.homeplus.admin.web.search.model.DictionaryPreAnalysisGetDto;
import kr.co.homeplus.admin.web.search.model.DictionaryRegGetDto;
import kr.co.homeplus.admin.web.search.model.DictionaryTotalSearchGridDto;
import kr.co.homeplus.admin.web.search.model.DictionaryTypoCorrectGetDto;
import kr.co.homeplus.admin.web.search.model.DictionaryWordHistoryGetDto;
import kr.co.homeplus.admin.web.search.model.DictionaryWordRelationGetDto;
import kr.co.homeplus.admin.web.search.model.DomainCd;
import kr.co.homeplus.admin.web.search.model.LanguageCd;
import kr.co.homeplus.admin.web.search.model.NeTypeCd;
import kr.co.homeplus.admin.web.search.model.NounAttrCd;
import kr.co.homeplus.admin.web.search.model.PosCd;
import kr.co.homeplus.admin.web.search.model.PreAnalysisAttrCd;
import kr.co.homeplus.admin.web.search.model.RelationCd;
import kr.co.homeplus.admin.web.search.model.VerbAttrCd;
import kr.co.homeplus.admin.web.search.service.DictionaryService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search")
@RequiredArgsConstructor
public class DictionaryController {

    private final DictionaryService dictionaryService;

    /***
     * 어휘 조회
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping(value = { "/dictionary/wordList", "/dictionary/wordList/{wordName}", "/dictionary/wordList/{wordName}.json" })
    public String wordList(Model model, @PathVariable(name = "wordName", required = false) String wordName) {
        model.addAttribute("requestWordName",wordName);
        model.addAttribute("pos", PosCd.values());  ///품사정보
        model.addAttribute("dicType", DicTypeCd.values()); ///사전정보
        model.addAttribute("relation", RelationCd.values()); ///어휘관계
        model.addAllAttributes(RealGridHelper.create("dictionaryListGridBaseInfo", DictionaryTotalSearchGridDto.DictionaryWordList.class));
        return "/search/dictionaryWordList";
    }
    /***
     * 이력 조회
     * @param model
     * @param wordId
     * @return
     */
    @GetMapping(value = {"/dictionary/wordHistory/{wordId}","/popup/dictionary/wordHistory/{wordId}"})
    public String wordHistory(Model model, @PathVariable("wordId") Integer wordId)   {
        model.addAttribute("wordId",wordId);
        model.addAllAttributes(RealGridHelper.create("dictionaryHistoryGridBaseInfo", DictionaryTotalSearchGridDto.DictionaryHistoryList.class));
        return "/search/popup/wordHistory";
    }
    /***
     * 어휘 등록
     * @param model
     * @param wordName
     * @return
     */
    @GetMapping(value = { "/dictionary/wordWrite.json","/dictionary/wordWrite/{wordName}.json","/dictionary/{direct}/wordWrite.json"})
    public String wordWrite(Model model, @PathVariable(value = "wordName",required = false) String wordName, @RequestParam(name="popup",required = false) boolean popup, @PathVariable(name="direct", required = false) String direct) {
        model.addAttribute("wordId","");
        model.addAttribute("wordName", wordName);
        model.addAttribute("popup",popup);
        model.addAttribute("direct",direct);
        model.addAttribute("dicType", DicTypeCd.values()); ///사전정보
        model.addAttribute("relation", RelationCd.values()); ///어휘관계
        model.addAttribute("pos", PosCd.values());  ///품사정보
        model.addAttribute("nounAttr", NounAttrCd.values());  ///명사문법정보
        model.addAttribute("verbAttr",VerbAttrCd.values());   ///동사문법정보
        model.addAttribute("neType",NeTypeCd.values()); //개체명 유형
        model.addAttribute("domain",DomainCd.values()); //분야
        model.addAttribute("language",LanguageCd.values()); //언어
        model.addAttribute("modify",false);
        model.addAttribute("isGeneral","N");
        model.addAttribute("useFlag",true);
        model.addAttribute("stopFlag",false);
        model.addAllAttributes(RealGridHelper.create("wordListGridBaseInfo", DictionaryTotalSearchGridDto.DictionarySave.class));
        return "/search/dictionaryWordWrite";
    }

    /***
     * 어휘 수정
     * @param model
     * @param wordId
     * @return
     */
    @GetMapping(value = "/dictionary/wordModify/{wordId}.json")
    public String wordWrite(Model model, @PathVariable("wordId") Integer wordId) {
        model.addAttribute("wordId",wordId);
        model.addAttribute("wordName", "");
        model.addAttribute("popup","false");
        model.addAttribute("direct","false");
        model.addAttribute("dicType", DicTypeCd.values()); ///사전정보
        model.addAttribute("relation", RelationCd.values()); ///어휘관계
        model.addAttribute("pos", PosCd.values());  ///품사정보
        model.addAttribute("nounAttr", NounAttrCd.values());  ///명사문법정보
        model.addAttribute("verbAttr",VerbAttrCd.values());   ///동사문법정보
        model.addAttribute("neType",NeTypeCd.values()); //개체명 유형
        model.addAttribute("domain",DomainCd.values()); //분야
        model.addAttribute("language",LanguageCd.values()); //언어

        DictionaryRegGetDto detail = dictionaryService.getRegWordInfo(wordId);
        model.addAttribute("detail",detail.getInfo());
        model.addAttribute("wordName", detail.getInfo().getWordName());
        model.addAttribute("modify",true);
        model.addAttribute("isGeneral",detail.getInfo().getIsGeneral());
        model.addAttribute("useFlag",detail.getInfo().isUseFlag());
        model.addAttribute("stopFlag",detail.getInfo().isStopFlag());

        model.addAllAttributes(RealGridHelper.create("wordListGridBaseInfo", DictionaryTotalSearchGridDto.DictionarySave.class));
        return "/search/dictionaryWordWrite";
    }

    /***
     * 복합어 구성 정보
     * @param model
     * @return
     */
    @GetMapping(value = "/popup/dictionary/compoundsInfo/{wordId}")
    public String compoundsInfo(Model model, @PathVariable("wordId") Integer wordId) {
        DictionaryRegGetDto detail = dictionaryService.getRegWordInfo(wordId);
        if(detail.getInfo() == null) {
            model.addAttribute("error", "존재하지않는 복합어입니다.");
            model.addAttribute("redirectUrl", "/wordList");
            return "/search/dictionaryCompounds";
        }
        model.addAttribute("wordId", wordId);  ///단어번호
        model.addAttribute("wordName",detail.getInfo().getWordName()); //복합명사명

        model.addAllAttributes(RealGridHelper.create("dictionaryCompoundsSearchBaseInfo", DictionaryTotalSearchGridDto.PopupSearchList.class));
        model.addAllAttributes(RealGridHelper.create("dictionaryCompoundsDictionaryBaseInfo", DictionaryTotalSearchGridDto.CompoundsDictionarySave.class));
        model.addAllAttributes(RealGridHelper.create("dictionaryCompoundsResultBaseInfo", DictionaryTotalSearchGridDto.CompoundsSearchSave.class));
        return "/search/dictionaryCompounds";
    }

    /***
     * 기분석 구성 정보
     * @param model
     * @param wordId
     * @return
     */
    @GetMapping(value = "/popup/dictionary/preAnalysisInfo/{wordId}")
    public String preAnalysisInfo(Model model, @PathVariable("wordId") Integer wordId) {
        DictionaryRegGetDto detail = dictionaryService.getRegWordInfo(wordId);
        if(detail.getInfo() == null) {
            model.addAttribute("error", "존재하지않는 기분석입니다.");
            model.addAttribute("redirectUrl", "/wordList");
            return "/search/dictionaryPreAnalysis";
        }
        model.addAttribute("preAnalysisAttrCd", PreAnalysisAttrCd.values()); //수행 방식
        model.addAttribute("wordId", wordId);  ///단어번호
        model.addAttribute("wordName",detail.getInfo().getWordName()); //기분석명

        model.addAllAttributes(RealGridHelper.create("dictionaryPreAnalysisSearchBaseInfo", DictionaryTotalSearchGridDto.PopupSearchList.class));
        model.addAllAttributes(RealGridHelper.create("dictionaryPreAnalysisResultBaseInfo", DictionaryTotalSearchGridDto.CompoundsPreanalyzedSave.class));
        return "/search/dictionaryPreAnalysis";
    }

    /***
     * 이력 조회 API
     */
    @ResponseBody
    @GetMapping(value = {"/dictionary/{wordId}/getWordHistory.json"})
    @SuppressWarnings("unchecked")
    public List<DictionaryWordHistoryGetDto> getWordHistory(@PathVariable("wordId") Integer wordId) {
        return (List<DictionaryWordHistoryGetDto>)dictionaryService.getWordHistory(wordId).getData();
    }
    /***
     * 어휘 속성 조회 API
     */
    @ResponseBody
    @PostMapping(value = {"/dictionary/getSrchWordInfo.json"})
    @SuppressWarnings("unchecked")
    public List<DictionaryListGetDto> getSrchWordInfo(@RequestBody @Valid DictionaryListGetParamDto param) {
        return (List<DictionaryListGetDto>)dictionaryService.getSrchWordInfo(param).getData();
    }
    /***
     * 단어 등록 API
     */
    @ResponseBody
    @PostMapping(value = { "/dictionary/insertWordInfo.json" })
    public Integer insertWordInfo(@RequestBody @Valid DictionaryInsertWordInfoSetParamDto param) {
        return (Integer)dictionaryService.insertWordInfo(param).getData();
    }
    /***
     * 단어 수정 API
     */
    @ResponseBody
    @PostMapping(value = { "/dictionary/{wordId}/updateWordInfo.json" })
    public Integer updateWordInfo(@RequestBody @Valid DictionaryInsertWordInfoSetParamDto param, @PathVariable(name="wordId") Integer wordId) {
        return (Integer)dictionaryService.updateWordInfo(param, wordId).getData();
    }
    /***
     * 단어 삭제 API
     */
    @ResponseBody
    @PostMapping(value = { "/dictionary/{wordId}/deleteWordInfo.json" })
    public Integer deleteWordInfo(@PathVariable("wordId") Integer wordId) {
        return (Integer)dictionaryService.deleteWordInfo(wordId).getData();
    }
    /***
     * 복합어 구성 정보 조회
     */
    @ResponseBody
    @GetMapping(value = {"/dictionary/{wordId}/getCompoundsInfo.json"})
    public DictionaryComponentGetDto getCompoundsInfo(@PathVariable("wordId") Integer wordId) {
        return dictionaryService.getCompoundsInfo(wordId);
    }
    /***
     * 기분석 조회
     */
    @ResponseBody
    @GetMapping(value = {"/dictionary/{wordId}/getPreAnalysisInfo.json"})
    public DictionaryPreAnalysisGetDto getPreAnalysisInfo(@PathVariable("wordId") Integer wordId) {
        return dictionaryService.getPreAnalysisInfo(wordId);
    }
    /***
     * 연관어 조회
     */
    @ResponseBody
    @GetMapping(value = {"/dictionary/{wordId}/getRelationInfo.json"})
    public DictionaryWordRelationGetDto getRelationInfo(@PathVariable("wordId") Integer wordId) {
        return (DictionaryWordRelationGetDto)dictionaryService.getRelationInfo(wordId).getData();
    }
    /***
     * 복합어 : 검색용 저장
     */
    @ResponseBody
    @PostMapping(value = { "/dictionary/{wordId}/insertCompoundsResult.json" })
    public Integer insertCompoundsResult(@RequestBody @Valid DictionaryInsertCompoundsSetParamDto param, @PathVariable("wordId") Integer wordId) {
        return (Integer)dictionaryService.insertCompoundsResult(param, wordId).getData();
    }
    /***
     * 복합어 : 사전용 저장
     */
    @ResponseBody
    @PostMapping(value = { "/dictionary/{wordId}/insertCompounds.json" })
    public Integer insertCompounds(@RequestBody @Valid DictionaryInsertCompoundsSetParamDto param, @PathVariable("wordId") Integer wordId) {
        return (Integer)dictionaryService.insertCompounds(param, wordId).getData();
    }
    /***
     * 기분석 : 검색용 저장
     */
    @ResponseBody
    @PostMapping(value = { "/dictionary/{wordId}/insertPreAnalysisResult.json" })
    public Integer insertCompoundsResult(@RequestBody @Valid DictionaryInsertPreAnalysisSetParamDto param, @PathVariable("wordId") Integer wordId) {
        return (Integer)dictionaryService.insertCompoundsResult(param, wordId).getData();
    }
    /***
     * 기분석 : 사전용 저장
     */
    @ResponseBody
    @PostMapping(value = { "/dictionary/{wordId}/insertPreAnalysis.json" })
    public Integer insertCompounds(@RequestBody @Valid DictionaryInsertPreAnalysisSetParamDto param, @PathVariable("wordId") Integer wordId) {
        return (Integer)dictionaryService.insertCompounds(param, wordId).getData();
    }
    /***
     * 관련어 저장
     */
    @ResponseBody
    @PostMapping(value = { "/dictionary/insertMorphExpansion.json" })
    public Integer insertMorphExpansion(@RequestBody @Valid DictionaryInsertMorphExpansionSetParamDto param) {
        return (Integer)dictionaryService.insertMorphExpansion(param).getData();
    }
    /***
     * 관련어 삭제
     */
    @ResponseBody
    @GetMapping(value = {"/dictionary/deleteMorphExpansion.json"})
    public Integer deleteMorphExpansion(@RequestParam("wordId") Integer wordId) {
        return (Integer)dictionaryService.deleteMorphExpansion(wordId).getData();
    }

    @ResponseBody
    @GetMapping(value = {"/dictionary/analysis.json"})
    public AnalysisPluginGetDto analysisResult(@RequestParam("keyword") String keyword) {
        return dictionaryService.analysisResult(keyword);
    }


    /***
     * 오탈자 설정 (현재 오탈자기능 메뉴 분리 )
     * @param model
     * @param wordId
     * @return
     */
    @Deprecated
    @GetMapping(value = "/dictionary/typoCorrect/{wordId}")
    public String typoCorrect(Model model, @PathVariable Integer wordId) {
        DictionaryRegGetDto detail = dictionaryService.getRegWordInfo(wordId);
        model.addAttribute("wordId",wordId);
        model.addAttribute("wordName",detail.getInfo().getWordName());

        DictionaryTypoCorrectGetDto correctGetDto = dictionaryService.getTypoCorrect(wordId);
        model.addAttribute("correctedTerms",correctGetDto.getTypoCorrect() != null ? correctGetDto.getTypoCorrect().getCorrectedTerms() : null);

        return "/search/popup/typoCorrect";
    }

    /***
     * 오타 등록 API (현재 오탈자기능 메뉴 분리 )
     */
    @Deprecated
    @ResponseBody
    @PostMapping(value = { "/dictionary/{wordId}/insertTypoCorrect.json" })
    public Integer insertTypoCorrect(@RequestBody @Valid DictionaryInsertTypoCorrectSetParamDto param, @PathVariable(name="wordId") Integer wordId) {
        return (Integer)dictionaryService.insertTypoCorrect(param, wordId).getData();
    }

}