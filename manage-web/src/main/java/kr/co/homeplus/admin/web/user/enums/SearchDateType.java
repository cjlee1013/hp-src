package kr.co.homeplus.admin.web.user.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum SearchDateType {
    REG_DATE( "등록일")
    , MOD_DATE( "수정일")
    ;
    @Getter
    private final String desc;
}
