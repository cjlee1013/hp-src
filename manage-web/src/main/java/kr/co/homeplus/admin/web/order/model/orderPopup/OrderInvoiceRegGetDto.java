package kr.co.homeplus.admin.web.order.model.orderPopup;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "주문정보 상세 > 배송관리 > 송장등록 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class OrderInvoiceRegGetDto {

    @ApiModelProperty(value = "배송번호")
    @RealGridColumnInfo(headText = "배송번호")
    private long bundleNo;

    @ApiModelProperty(value = "상품주문번호")
    @RealGridColumnInfo(headText = "상품주문번호")
    private long orderItemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명")
    private String itemNm1;

    @ApiModelProperty(value = "운송번호")
    @RealGridColumnInfo(headText = "운송번호", hidden = true)
    private long shipNo;

    @ApiModelProperty(value = "송장정보")
    @RealGridColumnInfo(headText = "송장정보", hidden = true)
    private long invoiceNo;
}
