package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiParam;
import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class DataManagementExpSearchListGetParam {
    @NonNull
    @NotEmpty(message = "검색어는 빈 값일 수 없습니다.")
    @ApiParam(value = "검색어",required = true,defaultValue = "나이키")
    private String keyword;
    @ApiParam(value = "페이지번호",required = true, defaultValue = "1")
    private Integer page = 1;
    @ApiParam(value = "페이지사이즈",required = true, defaultValue = "100")
    private Integer perPage = 100;
    @ApiParam(value = "정렬 ( 추천순: RANK, 최신순: NEW, 낮은 가격순: PRICE_DOWN, 높은 가격순: PRICE_UP )", required = true, allowableValues = "RANK, NEW, PRICE_DOWN, PRICE_UP")
    private String sort = DataManagementTotalSearchSortCode.RANK.name();
    @ApiParam(value = "점포ID ( ex : 37, 38, 90163")
    private String storeIds;
    @ApiParam(value = "검색 유형 ( 일반: NONE, 오타: TYPO, 한/영: HANENG, 대체검색: SUB )", allowableValues = "NONE,TYPO,HANENG,SUB")
    private String searchType;
    @ApiParam(value = "카테고리 뎁스")
    private String categoryDepth;
    @ApiParam(value = "카테고리 ID")
    private String categoryId;
    @ApiParam(value = "브랜드")
    private String brand;
    @ApiParam(value = "혜택 ( 행사상품: BASIC, 골라담기: PICK, 함께할인: TOGETHER, 사은품: GIFT, 무료배송: FREE)", allowableValues = "BASIC, PICK, TOGETHER, GIFT, FREE")
    private String benefit;

}
