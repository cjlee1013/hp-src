package kr.co.homeplus.admin.web.core.privacylog.model;

public class PrivacyHeaderKey {
    private PrivacyHeaderKey() {
        throw new IllegalArgumentException("Constant Class");
    }

    /** 개인정보 접근 아이피 **/
    public static final String PRIVACY_ACCESS_IP = "privacy-access-ip";
    /** 개인정보 접근 계정 **/
    public static final String PRIVACY_ACCESS_USER_ID = "privacy-access-user-id";
}
