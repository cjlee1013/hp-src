package kr.co.homeplus.admin.web.statistics.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class ZipcodeOrderStatistics {
    @RealGridColumnInfo(headText = "점포유형", width = 80, sortable = true)
    private String storeType;

    @RealGridColumnInfo(headText = "점포명", width = 80, sortable = true)
    private String storeNm;

    @RealGridColumnInfo(headText = "우편번호", width = 80, sortable = true)
    private String zipcode;

    @RealGridColumnInfo(headText = "시/도", width = 80, sortable = true)
    private String sidoNm;

    @RealGridColumnInfo(headText = "시군구", width = 90, sortable = true)
    private String sigunguNm;

    @RealGridColumnInfo(headText = "주문건수", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long orderCnt;

    @RealGridColumnInfo(headText = "주문금액", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long orderPrice;

    @RealGridColumnInfo(headText = "1 Shift 주문건수", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift1OrderCnt;

    @RealGridColumnInfo(headText = "1 Shift 주문금액", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift1OrderPrice;

    @RealGridColumnInfo(headText = "2 Shift 주문건수", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift2OrderCnt;

    @RealGridColumnInfo(headText = "2 Shift 주문금액", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift2OrderPrice;

    @RealGridColumnInfo(headText = "3 Shift 주문건수", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift3OrderCnt;

    @RealGridColumnInfo(headText = "3 Shift 주문금액", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift3OrderPrice;

    @RealGridColumnInfo(headText = "4 Shift 주문건수", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift4OrderCnt;

    @RealGridColumnInfo(headText = "4 Shift 주문금액", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift4OrderPrice;

    @RealGridColumnInfo(headText = "5 Shift 주문건수", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift5OrderCnt;

    @RealGridColumnInfo(headText = "5 Shift 주문금액", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift5OrderPrice;

    @RealGridColumnInfo(headText = "6 Shift 주문건수", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift6OrderCnt;

    @RealGridColumnInfo(headText = "6 Shift 주문금액", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift6OrderPrice;

    @RealGridColumnInfo(headText = "7 Shift 주문건수", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift7OrderCnt;

    @RealGridColumnInfo(headText = "7 Shift 주문금액", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift7OrderPrice;

    @RealGridColumnInfo(headText = "8 Shift 주문건수", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift8OrderCnt;

    @RealGridColumnInfo(headText = "8 Shift 주문금액", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift8OrderPrice;

    @RealGridColumnInfo(headText = "9 Shift 주문건수", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift9OrderCnt;

    @RealGridColumnInfo(headText = "9 Shift 주문금액", width = 90, sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private long shift9OrderPrice;
}
