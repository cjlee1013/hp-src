package kr.co.homeplus.admin.web.search.controller;

import java.util.Map;
import kr.co.homeplus.admin.web.search.model.DataManagementCategorySearchGridDto;
import kr.co.homeplus.admin.web.search.model.DataManagementCategorySearchListGetParam;
import kr.co.homeplus.admin.web.search.service.DataManagementCategorySearchService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/dataManagement/category")
@RequiredArgsConstructor
public class DataManagementCategorySearchController {

    @Value("${spring.profiles.active}")
    private String profilesActive;

    private final DataManagementCategorySearchService categorySearchService;

    @GetMapping(value = { "/main" })
    public String getMain(Model model) {
        model.addAttribute("profilesActive",profilesActive);
        model.addAllAttributes(RealGridHelper.create("categorySearchGridBaseInfo", DataManagementCategorySearchGridDto.CategorySearchList.class));
        return "/search/dataManagementCategorySearchMain";
    }

    @ResponseBody
    @GetMapping(value = {"/list.json"})
    public ResponseObject<Map<String,Object>> getList(DataManagementCategorySearchListGetParam param) {
        return categorySearchService.getList(param);
    }

    @ResponseBody
    @GetMapping(value = {"/filter.json"})
    public ResponseObject<Map<String,Object>> getFilter(DataManagementCategorySearchListGetParam param) {
        return categorySearchService.getFilter(param);
    }
}
