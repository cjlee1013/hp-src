package kr.co.homeplus.admin.web.statistics.controller;

import java.util.List;
import kr.co.homeplus.admin.web.statistics.model.CouponPeriodStatisticsDetailGetDto;
import kr.co.homeplus.admin.web.statistics.model.CouponPeriodStatisticsGetDto;
import kr.co.homeplus.admin.web.statistics.model.CouponPeriodStatisticsSetDto;
import kr.co.homeplus.admin.web.statistics.service.CouponPeriodStatisticService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/escrow/statistics")
public class CouponPeriodStatisticsController {

    @Autowired
    private CouponPeriodStatisticService couponPeriodStatisticService;

    /**
     * 기간별 쿠폰통계 Main Page
     */
    @RequestMapping(value = "/couponPeriodMain", method = RequestMethod.GET)
    public String couponPeriodMain(Model model) throws Exception{
        model.addAllAttributes(RealGridHelper.create("couponPeriodGridBaseInfo", CouponPeriodStatisticsGetDto.class));
        model.addAllAttributes(RealGridHelper.create("couponPeriodDetailGridBaseInfo", CouponPeriodStatisticsDetailGetDto.class));

        return "/statistics/couponPeriodMain";
    }

    /**
     * 기간별 쿠폰통계 메인 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getCouponPeriodStatisticsList.json", method = RequestMethod.GET)
    public List<CouponPeriodStatisticsGetDto> getCouponPeriodStatisticsList(CouponPeriodStatisticsSetDto couponPeriodStatisticsSetDto) {
        return couponPeriodStatisticService.getCouponPeriodStatisticsList(couponPeriodStatisticsSetDto);
    }

    /**
     * 기간별 쿠폰통계 상세 리스트 조회
     */
    @ResponseBody
    @PostMapping(value = "/getCouponPeriodStatisticsDetailList.json")
    public List<CouponPeriodStatisticsDetailGetDto> getCouponPeriodStatisticsDetailList(@RequestBody CouponPeriodStatisticsSetDto couponPeriodStatisticsSetDto){
        return couponPeriodStatisticService.getCouponPeriodStatisticsDetailList(couponPeriodStatisticsSetDto);
    }
}