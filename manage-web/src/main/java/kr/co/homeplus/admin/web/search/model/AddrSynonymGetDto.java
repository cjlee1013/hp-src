package kr.co.homeplus.admin.web.search.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddrSynonymGetDto {
    @ApiModelProperty(notes = "동의어명",required = true, position = 1)
    private String synonymName;
}
