package kr.co.homeplus.admin.web.user.model.user;

import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class GradeStatChangeDto {

    private int gradeChangeCd;
    private String gradeChangeStr;

    private Integer cols5;
    private Integer cols4;
    private Integer cols3;
    private Integer cols2;
    private Integer cols1;

}
