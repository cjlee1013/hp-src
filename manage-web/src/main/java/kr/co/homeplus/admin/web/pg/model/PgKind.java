package kr.co.homeplus.admin.web.pg.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum PgKind {
    INICIS("INICIS", "INICIS"),
    TOSSPG("TOSSPG", "TOSSPG")
    ;

    private String pgCd;
    private String pgNm;
}
