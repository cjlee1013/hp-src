package kr.co.homeplus.admin.web.search.controller;

import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.search.model.AddrDictionaryGridDto;
import kr.co.homeplus.admin.web.search.model.AddrDictionaryListGetDto;
import kr.co.homeplus.admin.web.search.model.AddrDictionaryListParamDto;
import kr.co.homeplus.admin.web.search.model.AddrDictionarySetDto;
import kr.co.homeplus.admin.web.search.model.AddrSynonymGetDto;
import kr.co.homeplus.admin.web.search.service.AddrDictionaryService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/addr-dictionary")
@RequiredArgsConstructor
public class AddrDictionaryController {

    private final AddrDictionaryService addrDictionaryService;

    @GetMapping(value = "/getNounList")
    public String getNounList(Model model) {
        model.addAllAttributes(RealGridHelper.create("dictionaryListGridBaseInfo", AddrDictionaryGridDto.DictionaryWordList.class));
        model.addAllAttributes(RealGridHelper.create("synonymListGridBaseInfo", AddrDictionaryGridDto.DictionarySynonymList.class));
        return "/search/addrDictionaryMain";
    }

    /***
     * 어휘 속성 조회 API
     */
    @ResponseBody
    @PostMapping(value = {"/getSrchNounInfo.json"})
    public AddrDictionaryListGetDto getSrchNounInfo(@RequestBody @Valid AddrDictionaryListParamDto param) {
        return (AddrDictionaryListGetDto)addrDictionaryService.getSrchNounInfo(param).getData();
    }

    /***
     * 동의어 조회 API
     */
    @ResponseBody
    @GetMapping(value = {"/{wordId}/getSrchSynonymInfo.json"})
    @SuppressWarnings("unchecked")
    public List<AddrSynonymGetDto> getSrchSynonymInfo(@PathVariable(name = "wordId") Integer wordId) {
        return (List<AddrSynonymGetDto>)addrDictionaryService.getSrchSynonymInfo(wordId).getData();
    }

    /***
     * 명사 등록 API
     */
    @ResponseBody
    @PostMapping(value = { "/insertNounInfo.json" })
    public Integer insertNounInfo(@RequestBody @Valid AddrDictionarySetDto param) {
        return (Integer)addrDictionaryService.insertNounInfo(param).getData();
    }

    /***
     * 명사 수정 API
     */
    @ResponseBody
    @PutMapping(value = { "/{wordId}/updateNounInfo.json" })
    public Integer updateNounInfo(@RequestBody @Valid AddrDictionarySetDto param, @PathVariable(name="wordId") Integer wordId) {
        return (Integer)addrDictionaryService.updateNounInfo(param, wordId).getData();
    }
}
