package kr.co.homeplus.admin.web.core.privacylog.config;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

@Slf4j
@Configuration
public class WebClientConfig {

    private static final int CONNECT_TIMEOUT_MILLIS = 5000;
    private static final int READ_TIMEOUT_MILLIS = 10000;
    private static final int WRITE_TIMEOUT_MILLIS = 10000;

    @Bean
    public WebClient webClient() {
        return WebClient.builder()
            .filters(exchangeFilterFunctions -> {
                exchangeFilterFunctions.add(logRequest());
                exchangeFilterFunctions.add(logResponse());
            })
            .clientConnector(
                new ReactorClientHttpConnector(HttpClient.create()
                    .tcpConfiguration(
                        tcpClient -> tcpClient
                            .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, CONNECT_TIMEOUT_MILLIS)
                            .doOnConnected(connection -> {
                                connection.addHandlerLast(
                                    new ReadTimeoutHandler(READ_TIMEOUT_MILLIS,
                                        TimeUnit.MILLISECONDS));
                                connection.addHandlerLast(
                                    new WriteTimeoutHandler(WRITE_TIMEOUT_MILLIS,
                                        TimeUnit.MILLISECONDS));
                            }))))
            .build();
    }

    private ExchangeFilterFunction logRequest() {
        return ExchangeFilterFunction.ofRequestProcessor(request -> {
            log.debug("webClient.Request:<httpMethod:{}>,<url:{}>", request.method(), request.url());
            return Mono.just(request);
        });
    }

    private ExchangeFilterFunction logResponse() {
        return ExchangeFilterFunction.ofResponseProcessor(response -> {
            log.debug("webClient.Response:<status:{}>", response.statusCode());
            return Mono.just(response);
        });
    }
}
