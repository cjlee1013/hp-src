package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "결제관리 > 결제수단 관리 > 카드 PREFIX 관리 search DTO")
@Getter
@Setter
@EqualsAndHashCode
public class CardPrefixManageSelectDto {
    private String schCardPrefixNo;
    private String schMethodCd;
    private String schCardDetailKind;
    private String schCardIdenNm;
}
