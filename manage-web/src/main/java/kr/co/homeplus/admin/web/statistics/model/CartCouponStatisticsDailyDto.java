package kr.co.homeplus.admin.web.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "통계 > 프로모션통계 > 장바구니쿠폰통계(일자별) DTO")
public class CartCouponStatisticsDailyDto {

    @ApiModelProperty(notes = "점포ID", required = true, position = 1)
    @RealGridColumnInfo(headText = "점포ID", width = 70, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String storeId;

    @ApiModelProperty(notes = "점포명", required = true, position = 2)
    @RealGridColumnInfo(headText = "점포명", width = 100, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String storeNm;

    @ApiModelProperty(notes = "주문일자", required = true, position = 3)
    @RealGridColumnInfo(headText = "주문일자", width = 100, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String orderDt;

    @ApiModelProperty(notes = "사용건수", required = true, position = 4)
    @RealGridColumnInfo(headText = "사용건수", width = 80, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long useCnt;

    @ApiModelProperty(notes = "사용금액", required = true, position = 5)
    @RealGridColumnInfo(headText = "사용금액", width = 80, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long useAmt;

    @ApiModelProperty(notes = "취소건수", required = true, position = 6)
    @RealGridColumnInfo(headText = "취소건수", width = 80, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimCnt;

    @ApiModelProperty(notes = "취소금액", required = true, position = 7)
    @RealGridColumnInfo(headText = "취소금액", width = 80, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimAmt;

    @ApiModelProperty(notes = "합계금액", required = true, position = 8)
    @RealGridColumnInfo(headText = "합계금액", width = 80, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long sumAmt;
}
