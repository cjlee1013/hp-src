package kr.co.homeplus.admin.web.user.model.user;

import lombok.Data;

@Data
public class FirstOrderInfoDto {

    private String hyperFirstOrderYn = "N";
    private String hyperFirstOrderDt;

    private String clubFirstOrderYn = "N";
    private String clubFirstOrderDt;

    private String expFirstOrderYn = "N";
    private String expFirstOrderDt;

}
