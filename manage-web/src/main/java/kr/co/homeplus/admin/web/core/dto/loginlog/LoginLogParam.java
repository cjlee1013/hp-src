package kr.co.homeplus.admin.web.core.dto.loginlog;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 로그인 로그 저장 파라미터
 */
@Setter
@Getter
@ToString
@AllArgsConstructor
public class LoginLogParam {
    /** 로그인 아아디 **/
    private String userId;
    /** 로그인 사번 **/
    private String empId;
    /** 로그인 아이피 **/
    private String clientIp;
}
