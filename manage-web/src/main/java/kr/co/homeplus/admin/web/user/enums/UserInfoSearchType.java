package kr.co.homeplus.admin.web.user.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserInfoSearchType {
    USER_NM("회원명")
    , USER_NO("회원번호")
    , USER_ID("회원 아이디")
    , MOBILE("휴대폰 번호")
    ;

    @Getter
    private final String name;
}
