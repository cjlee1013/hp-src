package kr.co.homeplus.admin.web.search.controller;

import java.util.List;
import javax.validation.Valid;

import kr.co.homeplus.admin.web.search.model.*;
import kr.co.homeplus.admin.web.search.service.TypoService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/typo")
@RequiredArgsConstructor
public class TypoController {

    private final TypoService typoService;

    @GetMapping(value = "/main")
    public String Main(Model model) {
        model.addAllAttributes(RealGridHelper.create("typoGridBaseInfo", TypoGridDto.Typo.class));
        model.addAllAttributes(RealGridHelper.create("candidateGridBaseInfo", StatisticsUnknownMorphologyGridDto.UnknownMorphology.class));
        return "/search/typo";
    }

    @ResponseBody
    @PostMapping(value = {"/search.json"})
    @SuppressWarnings("unchecked")
    public ResponseObject<List<TypoSetDto>> getTypoSearch(@RequestBody @Valid TypoGetParamDto param) {
        return (ResponseObject<List<TypoSetDto>>)typoService.getTypoSearch(param);
    }

    @ResponseBody
    @PostMapping(value = {"/candidate.json"})
    public ResponseObject getCandidateList() {
        return typoService.getCandidateList();
    }

    @ResponseBody
    @RequestMapping(value = {"/insert.json"}, method = RequestMethod.POST)
    public Object setTypoItem(@RequestBody @Valid TypoSetParamDto param) {
        return typoService.setTypoItem(param);
    }

    @ResponseBody
    @RequestMapping(value = {"/{typoId}/delete.json"}, method = RequestMethod.GET)
    public Object setTypoItem(@PathVariable("typoId") long typoId) {
        return typoService.setTypoItem(typoId);
    }
}
