package kr.co.homeplus.admin.web.promotion.model.event;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class RmsPromoItemGetDto implements Cloneable {
    @RealGridColumnInfo(headText = "상품번호", sortable = true, width = 40)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", sortable = true, width = 60)
    private String itemNm;
}