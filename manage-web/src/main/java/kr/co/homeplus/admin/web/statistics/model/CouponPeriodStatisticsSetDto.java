package kr.co.homeplus.admin.web.statistics.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CouponPeriodStatisticsSetDto {
    @ApiModelProperty(value = "조회기간(시작일)")
    private String schStartDt;

    @ApiModelProperty(value = "조회기간(종료일)")
    private String schEndDt;

    @ApiModelProperty(value = "점포유형")
    private String schSiteType;

    @ApiModelProperty(value = "주문유형")
    private String schDiscountKind;

    @ApiModelProperty(value = "검색타입")
    private String schKeywordType;

    @ApiModelProperty(value = "검색어")
    private String schKeyword;

    @ApiModelProperty(value = "쿠폰번호")
    private String schCouponNo;

    @ApiModelProperty(value = "바코드")
    private String schBarcode;

    @ApiModelProperty(value = "마켓유형")
    private String schMarketType;
}
