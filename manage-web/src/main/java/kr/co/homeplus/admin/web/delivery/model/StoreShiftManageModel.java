package kr.co.homeplus.admin.web.delivery.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class StoreShiftManageModel {
    @RealGridColumnInfo(headText = "storeId", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeId;

    @RealGridColumnInfo(headText = "일자", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATE)
    private String shipDt;

    @RealGridColumnInfo(headText = "요일", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String shipWeekday;

    @RealGridColumnInfo(headText = "Shift번호", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String shiftId;

    @RealGridColumnInfo(headText = "VAN조", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String vanNm;

    @RealGridColumnInfo(headText = "Shift시작시간", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String shiftShipStartTime;

    @RealGridColumnInfo(headText = "Shift종료시간", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String shiftShipEndTime;

    @RealGridColumnInfo(headText = "Shift실행일", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String shiftExecDay;

    @RealGridColumnInfo(headText = "Shift마감시간", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String shiftCloseTime;

    @RealGridColumnInfo(headText = "Shift사전마감시간", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String shiftBeforeCloseTime;

    @RealGridColumnInfo(headText = "배정slot수", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private int slotCnt;

    @RealGridColumnInfo(headText = "storeNm", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeNm;

    @RealGridColumnInfo(headText = "반품/교환", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String returnYn;

}