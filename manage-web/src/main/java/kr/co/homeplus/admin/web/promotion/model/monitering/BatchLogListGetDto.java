package kr.co.homeplus.admin.web.promotion.model.monitering;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class BatchLogListGetDto {
    @RealGridColumnInfo(headText = "일련번호", width = 35)
    private Long seq;

    @RealGridColumnInfo(headText = "배치명", width = 80)
    private String batchNm;

    @RealGridColumnInfo(headText = "상태", width = 30)
    private String status;

    @RealGridColumnInfo(headText = "테이블명", width = 80)
    private String tableNm;

    @RealGridColumnInfo(headText = "메세지")
    private String msg;

    @RealGridColumnInfo(headText = "등록일", width = 60)
    private String regDt;
}