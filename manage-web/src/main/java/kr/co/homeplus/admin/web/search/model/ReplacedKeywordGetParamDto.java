package kr.co.homeplus.admin.web.search.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ReplacedKeywordGetParamDto {
    private String keyword;
    @NotEmpty
    @NotNull(message = "sdate is null")
    @Pattern(regexp = "(19[0-9]{2}|[0-9]{4})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)", message = "시작일")
    private String sdate;
    @NotEmpty
    @NotNull(message = "edate is null")
    @Pattern(regexp = "(19[0-9]{2}|[0-9]{4})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)", message = "종료일")
    private String edate;
    @NotNull(message = "innerYn is null")
    private char innerYn;

    private int offset;
    private int limit;
    private String schDateType;
    public int getLimit() {
        return this.limit == 0 ? 100 : this.limit;
    }
}
