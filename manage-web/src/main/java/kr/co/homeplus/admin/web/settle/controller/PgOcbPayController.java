package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.settle.model.ocb.PgOcbListSetDto;
import kr.co.homeplus.admin.web.settle.model.ocb.PgOcbStoreListGetDto;
import kr.co.homeplus.admin.web.settle.model.ocb.PgOcbDailyListGetDto;
import kr.co.homeplus.admin.web.settle.model.ocb.PgOcbTidListGetDto;
import kr.co.homeplus.admin.web.settle.service.PgOcbPayService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/ocb")
public class PgOcbPayController {

    private final PgOcbPayService pgOcbPayService;
    private final SettleCommonService settleCommonService;
    private final CodeService codeService;

    public PgOcbPayController(PgOcbPayService pgOcbPayService, SettleCommonService settleCommonService, CodeService codeService) {
        this.pgOcbPayService = pgOcbPayService;
        this.settleCommonService = settleCommonService;
        this.codeService = codeService;
    }

    /**
     * 정산관리 > 운영관리 > OCB실적조회
     * @param model
     * @return String
     */
    @RequestMapping(value = "/pgOcbPayListMain", method = RequestMethod.GET)
    public String pgOcbPayListMain(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type"              // 점포유형
        );
        model.addAttribute("storeType", code.get("store_type"));
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.createForGroup("ocbStoreBaseInfo", PgOcbStoreListGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("ocbDailyBaseInfo", PgOcbDailyListGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("ocbTidBaseInfo", PgOcbTidListGetDto.class));

        return "/settle/pgOcbPayList";
    }

    /**
     * 정산관리 > 운영관리 > OCB실적조회 > 지점별 실적조회
     */
    @ResponseBody
    @RequestMapping(value = "/getStorePgOcbPay.json", method = RequestMethod.GET)
    public List<PgOcbStoreListGetDto> getStorePgOcbPay(PgOcbListSetDto listParamDto) throws Exception {
        return pgOcbPayService.getStorePgOcbPay(listParamDto);
    }

    /**
     * 정산관리 > 운영관리 > OCB실적조회 > 일별 실적조회
     */
    @ResponseBody
    @RequestMapping(value = "/getDailyPgOcbPay.json", method = RequestMethod.GET)
    public List<PgOcbDailyListGetDto> getDailyPgOcbPay(PgOcbListSetDto listParamDto) throws Exception {
        return pgOcbPayService.getDailyPgOcbPay(listParamDto);
    }

    /**
     * 정산관리 > 운영관리 > OCB실적조회 > 건별 실적조회
     */
    @ResponseBody
    @RequestMapping(value = "/getTidPgOcbPay.json", method = RequestMethod.GET)
    public List<PgOcbTidListGetDto> getTidPgOcbPay(PgOcbListSetDto listParamDto) throws Exception {
        return pgOcbPayService.getTidPgOcbPay(listParamDto);
    }
}