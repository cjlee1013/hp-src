package kr.co.homeplus.admin.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//셀러상품 등록/수정 조회
public class ItemListParamDto {

    //검색 날짜유형
    private String schDate;

    //검색 시작일
    private String schStartDate;

    //검색 종류일
    private String schEndDate;

    //대분류 카테고리
    private String schLcateCd;

    //증분류 카테고리
    private String schMcateCd;

    //소분류 카테고리
    private String schScateCd;

    //세분류 카테고리
    private String schDcateCd;

    //판매업체ID
    private String schPartnerId;

    //싱상품유형 (B:반품(리세일), N:새상품, R:리퍼, U:중고)
    private String schItemType;

    //검색 조건
    private String schType;

    //검색 키워드
    private String schKeyword;

    //거래유형 (DS : 업체상품 , TD : 매장상품)
    private String mallType;

    //점포구분 - Hyper,Club,Exp,DS
    private String schStoreType;

    //점포ID
    private String schStoreId;

    //상품상태(T: 임시저장, A:판매중, S:판매중지)
    private String schItemStatus;

    //필수정보 미입력(category: 카테고리)
    private String schRequiredCateEmpty;

    //필수정보 미입력(itemNm: 상품명)
    private String schRequiredItemNmEmpty;

    //필수정보 미입력(img: 대표이미지)
    private String schRequiredMainImgEmpty;

    //필수정보 미입력(img: 리스트이미지)
    private String schRequiredListImgEmpty;

    //필수정보 미입력(img: 라벨이미지)
    private String schRequiredLabelImgEmpty;

    //필수정보 미입력(originTxt : 원산지)
    private String schRequiredOriginEmpty;

    //배송속성 (직택배상품: DRCT_DLV, 직배전용상품: DRCT, 픽업전용상품: PICK, 퀵전용상품: QUICK)
    private String schItemShipAttr;

    //연관상품 조회
    private String schRelationItem;

    //재고설정
    private String schStockType;
}
