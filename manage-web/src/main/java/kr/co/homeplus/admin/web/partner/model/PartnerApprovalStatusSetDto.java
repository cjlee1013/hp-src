package kr.co.homeplus.admin.web.partner.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 업체관리 > 파트너관리 > 판매업체관리 > 판매업체 승인상태 Set Entry
 */
@Getter
@Setter
public class PartnerApprovalStatusSetDto {

	private String partnerId;
	private String approvalStatus;
	private String chgReason;
	private String userId;
}
