package kr.co.homeplus.admin.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RsvSaleListParamDto {
    private String schDateType;
    private String schStartDt;
    private String schEndDt;
    private String schRsvSaleNm;
    private String schStoreType;
    private String schSaleType;
    private Character schUseYn;
}
