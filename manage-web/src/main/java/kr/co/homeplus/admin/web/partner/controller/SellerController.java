package kr.co.homeplus.admin.web.partner.controller;

import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.partner.model.sellerBank.SellerBankCheckListGetDto;
import kr.co.homeplus.admin.web.partner.model.sellerApiKey.SellerApiKeyListGetDto;
import kr.co.homeplus.admin.web.partner.model.sellerApiKey.SellerApiKeyListParamDto;
import kr.co.homeplus.admin.web.partner.model.sellerApiKey.SellerUseYnSetDto;
import kr.co.homeplus.admin.web.partner.model.sellerBank.SellerBankCheckListParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/partner")
public class SellerController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;

    public SellerController(ResourceClient resourceClient, LoginCookieService loginCookieService, CodeService codeService) {
        this.resourceClient = resourceClient;
        this.loginCookieService = loginCookieService;
        this.codeService = codeService;

    }

    /**
     * 업체관리 > 셀러연동관리 (in) > 셀러연동 API Key 관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/sellerApiKeyMain", method = RequestMethod.GET)
    public String sellerApiKeyMain(Model model) {

        codeService.getCodeModel(model,
            "use_yn"    //사용여부
        );

        model.addAllAttributes(RealGridHelper.createForGroup("sellerApiKeyListGridBaseInfo", SellerApiKeyListGetDto.class));

        return "/partner/sellerApiKeyMain";
    }

    /**
     * 업체관리 > 셀러연동관리 (in) > 셀러연동 API Key 조회
     * @param listParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getSellerApiKeyList.json"}, method = RequestMethod.GET)
    public List<SellerApiKeyListGetDto> getSellerApiKeyList(SellerApiKeyListParamDto listParamDto) {

        String apiUri = "/partner/getSellerApiKeyList";

        ResourceClientRequest<List<SellerApiKeyListGetDto>> request = ResourceClientRequest.<List<SellerApiKeyListGetDto>>getBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri(StringUtil.getRequestString(apiUri, SellerApiKeyListParamDto.class, listParamDto))
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 업체관리 > 셀러연동관리 (in) > 셀러연동 API Key 사용안함처리
     * @param sellerUseYnSetDto
     * @return
     */
    @ResponseBody
    @PostMapping("/setSellerApiKeyUseYn.json")
    public Object setItemDispYn(@ModelAttribute SellerUseYnSetDto sellerUseYnSetDto) {

        sellerUseYnSetDto.setUserId(loginCookieService.getUserInfo().getEmpId());

        String apiUri = "/partner/setSellerApiKeyUseYn";

        ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri(apiUri)
                .postObject(sellerUseYnSetDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 업체관리 > 판매업체관리 > 판매업체 정산계좌 검증
     * @param model
     * @return
     */
    @RequestMapping(value = "/seller/bankCheckMain", method = RequestMethod.GET)
    public String sellerBankCheckMain(Model model) {

        model.addAllAttributes(RealGridHelper.createForGroup("bankCheckListGridBaseInfo", SellerBankCheckListGetDto.class));

        return "/partner/sellerBankCheckMain";
    }

    /**
     * 업체관리 > 판매업체관리 > 판매업체 정산계좌 검증 조회
     * @param listParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getSellerBankCheckList.json"}, method = RequestMethod.GET)
    public List<SellerBankCheckListGetDto> getSellerBankCheckList(SellerBankCheckListParamDto listParamDto) {

        String apiUri = "/partner/getSellerBankCheckList";

        ResourceClientRequest<List<SellerBankCheckListGetDto>> request = ResourceClientRequest.<List<SellerBankCheckListGetDto>>getBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri(StringUtil.getRequestString(apiUri, SellerBankCheckListParamDto.class, listParamDto))
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }
}