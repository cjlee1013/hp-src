package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class DataManagementExpCategoryGridDto {
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
    public static class ExpCategoryList{
        @RealGridColumnInfo(headText = "상품이미지", width = 50, columnType = RealGridColumnType.IMAGE)
        private String img;
        @RealGridColumnInfo(headText = "DOCID", columnType = RealGridColumnType.BASIC)
        private String docId;
        @RealGridColumnInfo(headText = "상품번호", width = 70, columnType = RealGridColumnType.BASIC)
        private String itemNo;
        @RealGridColumnInfo(headText = "성인상품", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private String adultType;
        @RealGridColumnInfo(headText = "브랜드(한글)", width = 70, columnType = RealGridColumnType.BASIC)
        private String brandNmKor;
        @RealGridColumnInfo(headText = "브랜드(영문)", width = 70, columnType = RealGridColumnType.BASIC)
        private String brandNmEng;
        @RealGridColumnInfo(headText = "상품명", width = 230, columnType = RealGridColumnType.BASIC)
        private String itemNm;
        @RealGridColumnInfo(headText = "검색상품명", columnType = RealGridColumnType.NAME, hidden = true)
        private String searchItemNm;
        @RealGridColumnInfo(headText = "총 가중치", width = 50, columnType = RealGridColumnType.NUMBER)
        private Long weight;
        @RealGridColumnInfo(headText = "구매지수", width = 50, columnType = RealGridColumnType.NUMBER)
        private Long weightBuyer;
        @RealGridColumnInfo(headText = "2시간", width = 50, columnType = RealGridColumnType.NUMBER)
        private Long weightHour;
        @RealGridColumnInfo(headText = "24시간", width = 50, columnType = RealGridColumnType.NUMBER)
        private Long weightDay;
        @RealGridColumnInfo(headText = "가격 가중치", width = 0, columnType = RealGridColumnType.NUMBER, hidden = true)
        private Long weightPrice;
        @RealGridColumnInfo(headText = "점포유형", width = 0, columnType = RealGridColumnType.NUMBER, hidden = true)
        private Long weightStoreType;
        @RealGridColumnInfo(headText = "오픈일", width = 0, columnType = RealGridColumnType.NUMBER, hidden = true)
        private Long weightOpenDay;
        @RealGridColumnInfo(headText = "배송유형", width = 0, columnType = RealGridColumnType.NUMBER, hidden = true)
        private Long weightShipMethod;
        @RealGridColumnInfo(headText = "행사", width = 0, columnType = RealGridColumnType.NUMBER, hidden = true)
        private Long weightPromotion;
        @RealGridColumnInfo(headText = "혜택", width = 0, columnType = RealGridColumnType.NUMBER, hidden = true)
        private Long weightBenefit;
        @RealGridColumnInfo(headText = "묶음하위상품가중치합", width = 0, columnType = RealGridColumnType.NUMBER, hidden = true)
        private Long weightBunddleSum;
        @RealGridColumnInfo(headText = "가중치 수식", width = 0, columnType = RealGridColumnType.NUMBER, hidden = true)
        private Long weightCalc;
        @RealGridColumnInfo(headText = "행사상품", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private String eventItemYn;
        @RealGridColumnInfo(headText = "골라담기", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private String pickYn;
        @RealGridColumnInfo(headText = "함께할인", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private String togetherYn;
        @RealGridColumnInfo(headText = "사은품", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private String giftYn;
        @RealGridColumnInfo(headText = "점포ID", width = 50, columnType = RealGridColumnType.BASIC)
        private String storeId;
        @RealGridColumnInfo(headText = "배송방식", width = 50, columnType = RealGridColumnType.BASIC)
        private String shipMethod;
        @RealGridColumnInfo(headText = "판매수량", width = 50, columnType = RealGridColumnType.BASIC)
        private String salesCnt;
        @RealGridColumnInfo(headText = "판매가격", width = 50, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
        private Long salePrice;
        @RealGridColumnInfo(headText = "상품등록일", width = 130, columnType = RealGridColumnType.DATETIME, hidden = true)
        private String regDt;
        @RealGridColumnInfo(headText = "대분류", width = 0, columnType = RealGridColumnType.NAME, hidden = true)
        private String lcateCd;
        @RealGridColumnInfo(headText = "중분류", width = 0, columnType = RealGridColumnType.NAME, hidden = true)
        private String mcateCd;
        @RealGridColumnInfo(headText = "대분류", columnType = RealGridColumnType.NAME, sortable = true)
        private String lcateNm;
        @RealGridColumnInfo(headText = "중분류", columnType = RealGridColumnType.NAME, sortable = true)
        private String mcateNm;
    }
}
