package kr.co.homeplus.admin.web.manage.model.dspMainThemePeriod;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspMainThemePeriodSetParamDto {

    private Long themeId;
    private String locType;
    private String themeNm;
    private String themeMngNm;
    private int priority;
    private String useYn;
    private String dispStartDt;
    private String dispEndDt;
    private String linkType;
    private String linkInfo;
    private String regId;

    private List<DspMainThemePeriodItemSetParamDto> listItem;

}
