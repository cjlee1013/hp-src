package kr.co.homeplus.admin.web.user.controller;

import static kr.co.homeplus.admin.web.core.constants.DateTimeFormat.DATE_FORMAT_DEFAULT_YMD;
import static kr.co.homeplus.admin.web.user.enums.ConsumerReturnCode.CONSUMER_ADD_NOT_MODIFICATION_PERIOD;
import static kr.co.homeplus.admin.web.user.enums.ConsumerReturnCode.CONSUMER_ADD_NOT_RELEASE_DETAIL;
import static kr.co.homeplus.admin.web.user.enums.ConsumerReturnCode.CONSUMER_NOT_MODIFIED_ALREADY_RELEASE;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.DateTimeFormat;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.dto.UserInfo;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyHeaderKey;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyLogInfo;
import kr.co.homeplus.admin.web.core.privacylog.model.PersonalLogMethod;
import kr.co.homeplus.admin.web.core.privacylog.service.PrivacyLogService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.user.exception.ConsumerManageException;
import kr.co.homeplus.admin.web.user.model.consumer.ConsumerAddParam;
import kr.co.homeplus.admin.web.user.model.consumer.ConsumerDto;
import kr.co.homeplus.admin.web.user.model.consumer.ConsumerListGrid;
import kr.co.homeplus.admin.web.user.model.consumer.ConsumerModifyParam;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.util.ServletUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 블랙컨슈머 관리 메뉴
 */
@Slf4j
@Controller
@RequestMapping("/user/blackConsumer")
@RequiredArgsConstructor
public class ConsumerMainController {
    private final ResourceClient resourceClient;
    private final LoginCookieService cookieService;
    private final CodeService codeService;
    private final PrivacyLogService privacyLogService;

    private final Map<String, Object> consumerHistoryGridBaseInfo = RealGridHelper
        .create("consumerHistoryGridBaseInfo", ConsumerListGrid.class);

    //공통코드 컨슈머 사유
    private static final String COMMON_CODE_REASON_TYPE = "reason_type";

    /**
     * 블랙컨슈머 관리 Main Page
     */
    @GetMapping(value = "/main")
    public String consumerMainController(final Model model) {
        codeService.getCodeModel(model, COMMON_CODE_REASON_TYPE);

        model.addAllAttributes(consumerHistoryGridBaseInfo);
        return "/user/consumerMain";
    }

    /**
     * 컨슈머 목록조회
     * @param userNo 회원번호
     * @return 컨슈머 이력목록
     */
    @ResponseBody
    @GetMapping("/getConsumerList.json")
    public ResponseObject<List<ConsumerListGrid>> getConsumerList(final HttpServletRequest request,
        @RequestParam final long userNo) {

        final String privacyAccessIp = ServletUtils.clientIP(request);
        final String privacyAccessUserId = cookieService.getUserInfo().getUserId();

        final PrivacyLogInfo logInfo = PrivacyLogInfo.builder()
            .txTime(DateTimeUtil.getNowMillYmdHis())
            .accessIp(ServletUtils.clientIP(request))
            .userId(cookieService.getUserInfo().getUserId())
            .txCodeUrl(request.getRequestURI())
            .txMethod(PersonalLogMethod.SELECT.name())
            .txCodeName("컨슈머관리 목록조회")
            .processor(String.valueOf(userNo))
            .processorTaskSql("")
            .build();

        privacyLogService.send(logInfo);

        final HttpHeaders headers = new HttpHeaders();
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_IP, privacyAccessIp);
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_USER_ID, privacyAccessUserId);

        return resourceClient.getForResponseObject(ResourceRouteName.USERMNG,
            "/admin/consumer/getConsumerList?userNo=" + userNo, headers,
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 컨슈머 상세조회
     * @param seq 컨슈머 일련번호
     * @return 컨슈머 상세내역
     */
    @ResponseBody
    @GetMapping("/getConsumer.json")
    public ResponseObject<ConsumerDto> getConsumer(@RequestParam final long seq){
        return resourceClient.getForResponseObject(ResourceRouteName.USERMNG,
            "/admin/consumer/getConsumer?seq=" + seq,
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 컨슈머 등록
     * @param param 컨슈머 등록 파라미터
     * @return boolean
     */
    @ResponseBody
    @PostMapping("/addConsumer.json")
    public ResponseObject<Boolean> addBlackConsumer(
        @RequestBody @Valid final ConsumerAddParam param) {

        final UserInfo userInfo = cookieService.getUserInfo();

        param.setRegId(userInfo.getEmpId());
        param.setRegNm(userInfo.getUserNm());

        return resourceClient.postForResponseObject(ResourceRouteName.USERMNG, param,
            "/admin/consumer/addConsumer",
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 컨슈머 수정
     * @param param 컨슈머 수정 파라미터
     * @return boolean
     */
    @ResponseBody
    @PostMapping("/modifyConsumer.json")
    public ResponseObject<Boolean> modifyBlackConsumer(
        @RequestBody @Valid final ConsumerModifyParam param) {
        invalidModifyParam(param);

        final UserInfo userInfo = cookieService.getUserInfo();

        param.setModId(userInfo.getEmpId());
        param.setModNm(userInfo.getUserNm());

        return resourceClient.postForResponseObject(ResourceRouteName.USERMNG, param,
            "/admin/consumer/modifyConsumer",
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 컨슈머 수정시 유효성 체크를 수행합니다.
     * @param param 컨슈머 수정 파라미터
     */
    private void invalidModifyParam(final ConsumerModifyParam param) {
        if (param.isRelease() && isNotBeforeNow(param.getStartDt())) {
            throw new ConsumerManageException(CONSUMER_ADD_NOT_MODIFICATION_PERIOD);
        }

        if (param.isRelease() && StringUtils
            .isNotBlank(param.getReleaseDetail())) {
            throw new ConsumerManageException(CONSUMER_ADD_NOT_RELEASE_DETAIL);
        }

        if (!param.isRelease() && StringUtils.isNotBlank(param.getReleaseDt())) {
            throw new ConsumerManageException(CONSUMER_NOT_MODIFIED_ALREADY_RELEASE);
        }

        if (!param.isRelease() && isAfterNow(param.getEndDt())) {
            throw new ConsumerManageException(CONSUMER_NOT_MODIFIED_ALREADY_RELEASE);
        }
    }

    /**
     * 현재 날짜를 반환합니다.
     * @return {@link LocalDate}
     */
    private static LocalDate getNow() {
        return LocalDate.now(ZoneId.of(DateTimeFormat.TIME_ZONE_KST));
    }

    /**
     * 변경할 포맷패턴 문자열을 받아 {@link DateTimeFormatter} 로 변환하여 리턴합니다.
     * @return {@link DateTimeFormatter}
     */
    private static DateTimeFormatter getFormatterDefaultYmd() {
        return DateTimeFormatter.ofPattern(DATE_FORMAT_DEFAULT_YMD);
    }

    /**
     * 현재 날짜가 오늘 날짜 이전 날짜가 아닌지 확인
     * @param sourceDateStr 체크할 날짜 형식 string
     */
    public static boolean isNotBeforeNow(final String sourceDateStr) {
        if (StringUtils.isBlank(sourceDateStr)) {
            return false;
        }
        final LocalDate sourceDate = LocalDate.parse(sourceDateStr, getFormatterDefaultYmd());
        return getNow().compareTo(sourceDate) >= 0;
    }

    /**
     * 현재 날짜가 오늘 날짜 이후 날짜인지 확인
     * sourceDateTimeFormatStr 으로 parsing
     * @param sourceDateStr 체크할 날짜 형식 string
     */
    public static boolean isAfterNow(final String sourceDateStr) {
        if (StringUtils.isBlank(sourceDateStr)) {
            return false;
        }
        final LocalDate sourceDate = LocalDate.parse(sourceDateStr, getFormatterDefaultYmd());
        return getNow().isAfter(sourceDate) ;
    }

    /**
     * 현재 날짜가 오늘 날짜보다 이전 인지 확인
     * @param sourceDateStr 체크할 날짜 형식 string
     */
    public static boolean isBeforeNow(final String sourceDateStr) {
        if (StringUtils.isBlank(sourceDateStr)) {
            return false;
        }
        final LocalDate sourceDate = LocalDate.parse(sourceDateStr, getFormatterDefaultYmd());
        return sourceDate.isBefore(getNow());
    }
}
