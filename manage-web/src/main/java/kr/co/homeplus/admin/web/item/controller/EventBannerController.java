package kr.co.homeplus.admin.web.item.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.banner.EventBannerSetParamDto;
import kr.co.homeplus.admin.web.item.model.banner.ItemBannerDetailListSelectDto;
import kr.co.homeplus.admin.web.item.model.banner.ItemBannerDetailParamDto;
import kr.co.homeplus.admin.web.item.model.banner.ItemBannerListParamDto;
import kr.co.homeplus.admin.web.item.model.banner.ItemBannerListSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/banner")
public class EventBannerController {

    final private ResourceClient resourceClient;
    final private LoginCookieService cookieService;
    final private CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    public EventBannerController(ResourceClient resourceClient,
                                  LoginCookieService cookieService,
                                  CodeService codeService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
    }

    /**
     * 상품관리 > 상품상세 일괄공지 > 판매자공지
     *®
     * @param model
     * @return String
     */
    @RequestMapping(value = "/eventBannerMain", method = RequestMethod.GET)
    public String eventBannerMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                "banner_period_type"   // 검색일자타입
                , "disp_yn"                     // 노출여부
                , "disp_type_event"             // 배송방법
                , "priority"                    // 우선순위
                , "link_type"                   // 링크타입
                , "banner_store_type"           // 상품상세공지 점포 타입
        );

        //코드정보
        model.addAttribute("bannerPeriodType", code.get("banner_period_type"));
        model.addAttribute("dispYn", code.get("disp_yn"));
        model.addAttribute("dispType", code.get("disp_type_event"));
        model.addAttribute("priority", code.get("priority"));
        model.addAttribute("linkType", code.get("link_type"));
        model.addAttribute("bannerStoreType", code.get("banner_store_type"));
        model.addAttribute("hmpImgUrl", this.hmpImgUrl);

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("dispTypeJson", om.writeValueAsString(code.get("disp_type_event")));

        //그리드
        model.addAllAttributes(
            RealGridHelper.create("eventBannerListGridBaseInfo", ItemBannerListSelectDto.class));

        model.addAllAttributes(
            RealGridHelper.create("bannerDetailListGridBaseInfo", ItemBannerDetailListSelectDto.class));

        return "/item/eventBannerMain";
    }

    /**
     * 상품관리> 상품상세 일괄등록 > 이벤트/마케팅 리스트
     * @param itemBannerListParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getEventBannerList.json"}, method = RequestMethod.POST)
    public List<ItemBannerListSelectDto> getEventBannerList(
        @RequestBody ItemBannerListParamDto itemBannerListParamDto) {
        String apiUri = "/item/banner/getEventBannerList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemBannerListSelectDto>>>() {};
        return (List<ItemBannerListSelectDto>) resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemBannerListParamDto , apiUri, typeReference).getData();
    }

    /**
     * 상품관리> 상품상세 일괄등록 > 이벤트/마케팅 Detail 리스트
     * @param itemBannerDetailParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getEventBannerDetailList.json"}, method = RequestMethod.GET)
    public List<ItemBannerDetailListSelectDto> getEventBannerDetailList(
        ItemBannerDetailParamDto itemBannerDetailParamDto) {
        String apiUri = "/item/banner/getItemBannerDetailList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemBannerDetailListSelectDto>>>() {};
        return (List<ItemBannerDetailListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, ItemBannerDetailParamDto.class, itemBannerDetailParamDto), typeReference).getData();
    }

    /**
     * 마케팅/이벤트 상품상세 등록/수정
     * @param eventBannerSetParamDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setEventBanner.json"}, method = RequestMethod.POST)
    public ResponseResult setEventBanner (@RequestBody EventBannerSetParamDto eventBannerSetParamDto) throws Exception {
        String apiUri = "/item/banner/setEventBanner";

        eventBannerSetParamDto.setUserId(cookieService.getUserInfo().getEmpId());
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {};
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM,
            eventBannerSetParamDto, apiUri, typeReference).getData();
    }

}
