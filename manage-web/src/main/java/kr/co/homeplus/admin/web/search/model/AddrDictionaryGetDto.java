package kr.co.homeplus.admin.web.search.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddrDictionaryGetDto {
    @ApiModelProperty(hidden = true)
    private int wordId;
    @ApiModelProperty(notes = "명사",required = true, position = 1)
    @NotNull
    @NotEmpty
    private String wordName;
    @ApiModelProperty(notes = "사용여부(Y|N)",required = true, position = 2)
    private char useFlag = 'Y';
    @ApiModelProperty(hidden = true)
    private String regDate;
    @ApiModelProperty(notes = "간략설명", position = 3)
    private String note;
    @ApiModelProperty(notes = "의미태그", position = 4)
    private String senseTag;
    @ApiModelProperty(hidden = true)
    private String synonyms;
}
