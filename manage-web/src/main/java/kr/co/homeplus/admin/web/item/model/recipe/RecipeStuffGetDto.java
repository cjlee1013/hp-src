package kr.co.homeplus.admin.web.item.model.recipe;

import lombok.Data;

@Data
public class RecipeStuffGetDto {

    private String stuffType;

    private String stuffDesc;

}