package kr.co.homeplus.admin.web.item.model.commission;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommissionSetParamDto {
    private Long commissionSeq;
    private String partnerId;
    private String cateKind;
    private String cateCd;
    private String useYn;
    private BigDecimal commissionRate;
    private String regId;
}
