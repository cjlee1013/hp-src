package kr.co.homeplus.admin.web.common.model.pop;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PartnerPopupListGetDto {
	private String partnerId;
	private String partnerNm;
	private String businessNm;
	private String partnerOwner;
	private String partnerStatusNm;
	private String regDt;
}
