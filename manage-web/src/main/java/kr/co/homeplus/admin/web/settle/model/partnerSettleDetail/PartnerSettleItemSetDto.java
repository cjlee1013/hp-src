package kr.co.homeplus.admin.web.settle.model.partnerSettleDetail;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PartnerSettleItemSetDto {
    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "지급SRL")
    private long settleSrl;
}
