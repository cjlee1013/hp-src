package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridButtonType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class DictionaryTotalSearchGridDto {
    /* 단어 조회 */
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
    public static class DictionaryWordList{
        @RealGridColumnInfo(headText = "번호", columnType = RealGridColumnType.BASIC, sortable = true)
        private Integer wordId;
        @RealGridColumnInfo(headText = "단어", width = 150, columnType = RealGridColumnType.BASIC, sortable = true)
        private String wordName;
        @RealGridColumnInfo(headText = "의미 태그", columnType = RealGridColumnType.BASIC, sortable = true)
        private String senseTag;
        @RealGridColumnInfo(headText = "사전 종류", columnType = RealGridColumnType.BASIC, sortable = true)
        private String dicType;
        @RealGridColumnInfo(headText = "품사", columnType = RealGridColumnType.BASIC, sortable = true)
        private String pos;
        @RealGridColumnInfo(headText = "간략설명", width = 200, columnType = RealGridColumnType.BASIC, sortable = true)
        private String note;
        @RealGridColumnInfo(headText = "동의어", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private String synonym;
        @RealGridColumnInfo(headText = "하위어", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private String hyponym;
        @RealGridColumnInfo(headText = "연관어", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private String relatednym;
        @RealGridColumnInfo(headText = "등록자", columnType = RealGridColumnType.BASIC, sortable = true)
        private String regUserId;
        @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.BASIC, sortable = true)
        private String regDate;
        @RealGridColumnInfo(headText = "수정자", columnType = RealGridColumnType.BASIC, sortable = true)
        private String modUserId;
        @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.BASIC, sortable = true)
        private String modDate;
        @RealGridColumnInfo(headText = "범용여부", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private String isGeneral;
        @RealGridColumnInfo(headText = "사전등록", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private String useFlagNm;
        @RealGridColumnInfo(headText = "불용어", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private String stopFlagNm;
    }
    /* 이력 조회 */
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
    public static class DictionaryHistoryList {
        @RealGridColumnInfo(headText = "번호", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private Long historyId;
        @RealGridColumnInfo(headText = "유형", width = 25, columnType = RealGridColumnType.BASIC, sortable = true)
        private String updateTypeNm;
        @RealGridColumnInfo(headText = "수정자", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String updateUserid;
        @RealGridColumnInfo(headText = "수정일", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private String updateDate;
        @RealGridColumnInfo(headText = "내역", width = 150, columnType = RealGridColumnType.BASIC, sortable = true)
        private String note;
    }
    /* 어휘 저장 */
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
    public static class DictionarySave {
        @RealGridColumnInfo(headText = "번호", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private Integer wordId;
        @RealGridColumnInfo(headText = "단어", width = 55, columnType = RealGridColumnType.BASIC, sortable = true)
        private String wordName;
        @RealGridColumnInfo(headText = "의미 태그", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private String senseTag;
        @RealGridColumnInfo(headText = "사전 종류", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private String dicType;
        @RealGridColumnInfo(headText = "품사", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private String pos;
        @RealGridColumnInfo(headText = "간략설명", width = 70, columnType = RealGridColumnType.BASIC, sortable = true)
        private String note;
        @RealGridColumnInfo(headText = "등록일", width = 40, columnType = RealGridColumnType.BASIC)
        private String regDate;
    }
    /* 팝업(복합명사, 기분석) : 단어조회 */
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
    public static class PopupSearchList {
        @RealGridColumnInfo(headText = "번호", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private Integer wordId;
        @RealGridColumnInfo(headText = "단어", width = 40, columnType = RealGridColumnType.BASIC, sortable = true)
        private String wordName;
        @RealGridColumnInfo(headText = "품사", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String pos;
        @RealGridColumnInfo(headText = "사전 종류",width = 50,  columnType = RealGridColumnType.BASIC, sortable = true)
        private String dicType;
        @RealGridColumnInfo(headText = "설명", width = 50, columnType = RealGridColumnType.BASIC, sortable = true)
        private String description;
    }
    /* 복합명사 : 사전용 저장 */
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
    public static class CompoundsDictionarySave {
        @RealGridColumnInfo(headText = "번호", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private Integer compId;
        @RealGridColumnInfo(headText = "단어", width = 40, columnType = RealGridColumnType.BASIC, sortable = true)
        private String compName;
        @RealGridColumnInfo(headText = "품사", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String pos;
        @RealGridColumnInfo(headText = "사전 종류",width = 50,  columnType = RealGridColumnType.BASIC, sortable = true)
        private String dicType;
        @RealGridColumnInfo(headText = "설명", width = 50, columnType = RealGridColumnType.BASIC, sortable = true)
        private String description;
        @RealGridColumnInfo(headText = "순서", width = 20, columnType = RealGridColumnType.BUTTON, sortable = true, editable = true, alwaysShowButton = true, buttonType = RealGridButtonType.POPUP, buttonPopupMenu = "upDown")
        private Integer compOrder;
        @RealGridColumnInfo(headText = "제거", width = 20, columnType = RealGridColumnType.BUTTON, sortable = true, editable = true, alwaysShowButton = true, buttonType = RealGridButtonType.ACTION)
        private Integer delete;
    }
    /* 복합명사 : 검색용 저장 */
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
    public static class CompoundsSearchSave {
        @RealGridColumnInfo(headText = "번호", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private Integer compId;
        @RealGridColumnInfo(headText = "단어", width = 40, columnType = RealGridColumnType.BASIC, sortable = true)
        private String compName;
        @RealGridColumnInfo(headText = "품사", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String pos;
        @RealGridColumnInfo(headText = "사전 종류",width = 50,  columnType = RealGridColumnType.BASIC, sortable = true)
        private String dicType;
        @RealGridColumnInfo(headText = "설명", width = 50, columnType = RealGridColumnType.BASIC, sortable = true)
        private String description;
        @RealGridColumnInfo(headText = "순서", width = 20, columnType = RealGridColumnType.BUTTON, sortable = true, editable = true, alwaysShowButton = true, buttonType = RealGridButtonType.POPUP, buttonPopupMenu = "upDown")
        private Integer compOrder;
        @RealGridColumnInfo(headText = "제거", width = 20, columnType = RealGridColumnType.BUTTON, sortable = true, editable = true, alwaysShowButton = true, buttonType = RealGridButtonType.ACTION)
        private Integer delete;
    }
    /* 기분석 : 검색용 저장 */
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
    public static class CompoundsPreanalyzedSave {
        @RealGridColumnInfo(headText = "번호", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private Integer compId;
        @RealGridColumnInfo(headText = "단어", width = 40, columnType = RealGridColumnType.BASIC, sortable = true)
        private String compName;
        @RealGridColumnInfo(headText = "품사", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String pos;
        @RealGridColumnInfo(headText = "사전 종류",width = 50,  columnType = RealGridColumnType.BASIC, sortable = true)
        private String dicType;
        @RealGridColumnInfo(headText = "설명", width = 50, columnType = RealGridColumnType.BASIC, sortable = true)
        private String description;
        @RealGridColumnInfo(headText = "분해여부", width = 0, hidden = true, editable = true)
        private boolean doAnalysis;
        @RealGridColumnInfo(headText = "분해", width = 20, columnType = RealGridColumnType.BUTTON, sortable = true, editable = true, alwaysShowButton = true, buttonType = RealGridButtonType.POPUP, buttonPopupMenu = "doAnalysis")
        private String doAnalysisYn;
        @RealGridColumnInfo(headText = "순서", width = 20, columnType = RealGridColumnType.BUTTON, sortable = true, editable = true, alwaysShowButton = true, buttonType = RealGridButtonType.POPUP, buttonPopupMenu = "upDown")
        private Integer compOrder;
        @RealGridColumnInfo(headText = "제거", width = 20, columnType = RealGridColumnType.BUTTON, sortable = true, editable = true, alwaysShowButton = true, buttonType = RealGridButtonType.ACTION)
        private Integer delete;
    }
}
