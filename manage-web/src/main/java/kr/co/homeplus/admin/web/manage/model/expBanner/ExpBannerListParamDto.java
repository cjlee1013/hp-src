package kr.co.homeplus.admin.web.manage.model.expBanner;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpBannerListParamDto {

    private String searchPeriodType;
    private String searchStartDt;
    private String searchEndDt;
    private String searchDevice;
    private String searchLoc;
    private String searchDispYn;
    private String searchType;
    private String searchKeyword;

}
