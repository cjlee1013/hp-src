package kr.co.homeplus.admin.web.message.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MailWorkDetail extends MailWorkGrid {

    private String subject;

    private String body;

    private String applicationId;

    private List<MailSendListGrid> sendList;
}
