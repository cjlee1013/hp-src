package kr.co.homeplus.admin.web.search.controller;

import java.util.List;
import kr.co.homeplus.admin.web.search.model.PopularKeywordGridDto;
import kr.co.homeplus.admin.web.search.model.PopularKeywordSetDto;
import kr.co.homeplus.admin.web.search.model.PopularManagementSetDto;
import kr.co.homeplus.admin.web.search.service.PopularKeywordService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/popular")
@RequiredArgsConstructor
public class PopularKeywordController {

    private final PopularKeywordService popularKeywordService;

    @GetMapping(value = { "/history" })
    public String getPopularHistory(Model model) {
        model.addAllAttributes(RealGridHelper.create("basicPopularGridBaseInfo", PopularKeywordGridDto.PopularKeyword.class));
        model.addAllAttributes(RealGridHelper.create("nowPopularGridBaseInfo", PopularKeywordGridDto.PopularKeyword.class));
        model.addAllAttributes(RealGridHelper.create("fixedGridBaseInfo", PopularKeywordGridDto.PopularFixed.class));
        model.addAllAttributes(RealGridHelper.create("exceptGridBaseInfo", PopularKeywordGridDto.PopularExcept.class));
        return "/search/popularHistory";
    }

    @GetMapping(value = { "/date" })
    public String getPopularStatistics(Model model) {
        model.addAllAttributes(RealGridHelper.create("dayPopularGridBaseInfo", PopularKeywordGridDto.PopularKeyword.class));
        model.addAllAttributes(RealGridHelper.create("weekPopularGridBaseInfo", PopularKeywordGridDto.PopularKeyword.class));
        model.addAllAttributes(RealGridHelper.create("monthPopularGridBaseInfo", PopularKeywordGridDto.PopularKeyword.class));
        return "/search/popularStatistics";
    }

    @GetMapping(value = { "/keyword" })
    public String getPopularKeyword(Model model) {
        model.addAllAttributes(RealGridHelper.create("fixedGridBaseInfo", PopularKeywordGridDto.PopularFullFixed.class));
        model.addAllAttributes(RealGridHelper.create("exceptGridBaseInfo", PopularKeywordGridDto.PopularFullExcept.class));
        return "/search/popularManagement";
    }

    @ResponseBody
    @GetMapping(value= {"/getPopularDisplayNm.json"})
    public Object getPopularDisplayNm(@RequestParam(name="siteType") String siteType, @RequestParam(name="popularType") String popularType, @RequestParam(name = "dateType", required = false) String dateType) {
       return popularKeywordService.getPopularDisplayNm(siteType, popularType, dateType).getData();
    }
    @ResponseBody
    @GetMapping(value= {"/getPopularDisplayNmGroup.json"})
    public Object getPopularDisplayNmGroup(@RequestParam(name="siteType") String siteType, @RequestParam(name="popularType") String popularType, @RequestParam(name = "dateType", required = false) String dateType, @RequestParam(name="applyDt") String applyDt) {
        return popularKeywordService.getPopularDisplayNmGroup(siteType, popularType, dateType, applyDt).getData();
    }

    @ResponseBody
    @GetMapping(value= {"/getPopularKeyword.json"})
    @SuppressWarnings("unchecked")
    public List<PopularKeywordSetDto> getPopularKeyword(@RequestParam(name="popularType") String popularType, @RequestParam(name="groupNo") String groupNo) {
        return (List<PopularKeywordSetDto>)popularKeywordService.getPopularKeyword(popularType, groupNo).getData();
    }

    @ResponseBody
    @GetMapping(value= {"/getPopularManagement.json"})
    @SuppressWarnings("unchecked")
    public List<PopularManagementSetDto> getPopularManagement(@RequestParam(name="siteType") String siteType, @RequestParam(name="type") String type, @RequestParam(name="date", required = false) String date) {
       return (List<PopularManagementSetDto>)popularKeywordService.getPopularManagement(siteType, type, date).getData();
    }

    @ResponseBody
    @PostMapping(value= {"/putPopularKeyword.json"})
    public Integer putPopularManagement(@RequestBody PopularManagementSetDto param) {
        return (Integer)popularKeywordService.putPopularManagement(param).getData();
    }

    @ResponseBody
    @GetMapping(value= {"/deletePopularManagement.json"})
    public Integer deletePopularManagement(@RequestParam(name="managementNo") String managementNo) {
        return (Integer)popularKeywordService.deletePopularManagement(managementNo).getData();
    }
}
