package kr.co.homeplus.admin.web.statistics.model.storePeriodEKPI;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StorePeriodListGetDto {
    @ApiModelProperty(value = "일자")
    @RealGridColumnInfo(headText = "일자", sortable = true, width = 100)
    private String basicDt;

    @ApiModelProperty(value = "점포")
    @RealGridColumnInfo(headText = "점포", sortable = true, width = 100)
    private String storeNm;

    @ApiModelProperty(value = "점포코드")
    @RealGridColumnInfo(headText = "점포코드", sortable = true, width = 100)
    private String originStoreId;

    @ApiModelProperty(value = "주문건수")
    @RealGridColumnInfo(headText = "주문건수", groupName = "전체 주문", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long orderCnt;

    @ApiModelProperty(value = "매출금액(E-KPI)")
    @RealGridColumnInfo(headText = "매출금액(E-KPI)", groupName = "전체 주문", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long orderAmt;

    @ApiModelProperty(value = "상품개수")
    @RealGridColumnInfo(headText = "상품개수", groupName = "전체 주문", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long orderItemCnt;

    @ApiModelProperty(value = "주문건수")
    @RealGridColumnInfo(headText = "주문건수", groupName = "유효 주문", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long effectiveOrderCnt;

    @ApiModelProperty(value = "매출금액(E-KPI)")
    @RealGridColumnInfo(headText = "매출금액(E-KPI)", groupName = "유효 주문", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long effectiveOrderAmt;

    @ApiModelProperty(value = "상품개수")
    @RealGridColumnInfo(headText = "상품개수", groupName = "유효 주문", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long effectiveOrderItemCnt;

    @ApiModelProperty(value = "신청건수")
    @RealGridColumnInfo(headText = "신청건수", groupName = "당일 전체취소", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimCnt;

    @ApiModelProperty(value = "신청금액")
    @RealGridColumnInfo(headText = "신청금액", groupName = "당일 전체취소", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimAmt;

    @ApiModelProperty(value = "상품개수")
    @RealGridColumnInfo(headText = "상품개수", groupName = "당일 전체취소", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimItemCnt;

    @ApiModelProperty(value = "신청건수")
    @RealGridColumnInfo(headText = "신청건수", groupName = "부분취소", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimPartCnt;

    @ApiModelProperty(value = "신청금액")
    @RealGridColumnInfo(headText = "신청금액", groupName = "부분취소", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimPartAmt;

    @ApiModelProperty(value = "상품개수")
    @RealGridColumnInfo(headText = "상품개수", groupName = "부분취소", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimPartItemCnt;

    @ApiModelProperty(value = "신청건수")
    @RealGridColumnInfo(headText = "신청건수", groupName = "부분반품", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long refundPartCnt;

    @ApiModelProperty(value = "신청금액")
    @RealGridColumnInfo(headText = "신청금액", groupName = "부분반품", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long refundPartAmt;

    @ApiModelProperty(value = "상품개수")
    @RealGridColumnInfo(headText = "상품개수", groupName = "부분반품", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long refundPartItemCnt;

    @ApiModelProperty(value = "신청건수")
    @RealGridColumnInfo(headText = "신청건수", groupName = "대체주문", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long subOrderCnt;

    @ApiModelProperty(value = "신청금액")
    @RealGridColumnInfo(headText = "신청금액", groupName = "대체주문", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long subOrderAmt;

}
