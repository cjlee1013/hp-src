package kr.co.homeplus.admin.web.app.model;

import lombok.Data;

@Data
public class AppStoreReviewControlInfo {

    private String siteType;
    private String version;
    private String controlYn;
}
