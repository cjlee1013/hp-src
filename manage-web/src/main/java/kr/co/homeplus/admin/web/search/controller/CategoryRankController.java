package kr.co.homeplus.admin.web.search.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.search.model.CategoryRankGridDto;
import kr.co.homeplus.admin.web.search.model.CategoryRankInsertParamDto;
import kr.co.homeplus.admin.web.search.model.CategoryRankListGetDto;
import kr.co.homeplus.admin.web.search.model.CategoryRankListParamDto;
import kr.co.homeplus.admin.web.search.model.CategoryRankRelationGetDto;
import kr.co.homeplus.admin.web.search.model.CategoryRankUpdateParamDto;
import kr.co.homeplus.admin.web.search.service.CategoryRankService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/categoryRank")
@RequiredArgsConstructor
public class CategoryRankController {

    private final CategoryRankService categoryRankService;

    @GetMapping(value = { "/rankList" })
    public String getCategoryRanksMain(Model model) {
        String[] scoreList = (String[])categoryRankService.getScoreList().getData();
        model.addAttribute("scoreList", scoreList);
        model.addAllAttributes(RealGridHelper.create("categoryRankGridBaseInfo", CategoryRankGridDto.CategoryRankList.class));
        model.addAllAttributes(RealGridHelper.create("categoryRankRelationGridBaseInfo", CategoryRankGridDto.CategoryRankRelation.class));
        return "/search/categoryRank";
    }

    @ResponseBody
    @PostMapping(value = {"/rankList.json"})
    public CategoryRankListGetDto getRankList(@RequestBody @Valid CategoryRankListParamDto param) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("keyword", param.getKeyword()));
        setParameterList.add(SetParameter.create("offset", param.getOffset()));
        setParameterList.add(SetParameter.create("limit", param.getLimit()));
        setParameterList.add(SetParameter.create("searchType",param.getSearchType()));
        setParameterList.add(SetParameter.create("schUseFlag",param.getSchUseFlag()));
        setParameterList.add(SetParameter.create("schResetFlag",param.getSchResetFlag()));
        return (CategoryRankListGetDto)categoryRankService.getRankList(setParameterList).getData();
    }

    @ResponseBody
    @GetMapping(value= {"/{rankingId}/relation.json"})
    public CategoryRankRelationGetDto getRankRelation(@PathVariable(name="rankingId") long rankingId) {
        return (CategoryRankRelationGetDto)categoryRankService.getRankRelation(rankingId).getData();
    }

    @ResponseBody
    @GetMapping(value = { "/{rankingId}/all/delete.json" })
    public Integer deleteAll(@PathVariable(name="rankingId") long rankingId) {
        return (Integer)categoryRankService.deleteAll(rankingId).getData();
    }

    @ResponseBody
    @GetMapping(value = { "/{rankingId}/{relationId}/relation/delete.json" })
    public Integer deleteRelation(@PathVariable(name="rankingId") long rankingId, @PathVariable(name="relationId") long relationId) {
        return (Integer)categoryRankService.deleteRelation(rankingId, relationId).getData();
    }

    @ResponseBody
    @PostMapping(value = { "/insert.json" })
    public Integer insertRank(@RequestBody @Valid CategoryRankInsertParamDto param) {
        return (Integer)categoryRankService.insertRank(param).getData();
    }

    @ResponseBody
    @PostMapping(value = { "/update.json" })
    public Integer updateRank(@RequestBody @Valid CategoryRankUpdateParamDto param) {
        return (Integer)categoryRankService.updateRank(param).getData();
    }

    @ResponseBody
    @GetMapping(value = { "/sync.json" })
    public Object sync() {
        return categoryRankService.sync();
    }
}
