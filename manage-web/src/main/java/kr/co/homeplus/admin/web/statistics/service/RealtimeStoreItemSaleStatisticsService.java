package kr.co.homeplus.admin.web.statistics.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.statistics.model.RealTimeOneItemSaleDto;
import kr.co.homeplus.admin.web.statistics.model.RealTimeOneItemSaleSearchSetDto;
import kr.co.homeplus.admin.web.statistics.model.RealtimeStoreItemSaleStatisticsDto;
import kr.co.homeplus.admin.web.statistics.model.StatisticsOrderInfoSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RealtimeStoreItemSaleStatisticsService {
    private final ResourceClient resourceClient;

    /**
     * 실시간 점포 상품 판매현황 리스트 조회
     * @param statisticsOrderInfoSelectDto
     * @return
     * @throws Exception
     */
    public List<RealtimeStoreItemSaleStatisticsDto> getRealtimeStoreItemSaleStatisticsList(StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_STATISTICS_ORDER_INFO_STORE_ITEM_RANKING_LIST;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<RealtimeStoreItemSaleStatisticsDto>>>() {};
        return (List<RealtimeStoreItemSaleStatisticsDto>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, statisticsOrderInfoSelectDto, apiUri, typeReference).getData();
    }

    /**
     * 실시간 특정 상품 판매현황 리스트
     * @param realTimeOneItemSaleSearchSetDto
     * @return
     * @throws Exception
     */
    public List<RealTimeOneItemSaleDto> getRealTimeOneItemSaleList(RealTimeOneItemSaleSearchSetDto realTimeOneItemSaleSearchSetDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_ONE_ITEM_SALE_LIST;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<RealTimeOneItemSaleDto>>>() {};
        return (List<RealTimeOneItemSaleDto>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, realTimeOneItemSaleSearchSetDto, apiUri, typeReference).getData();
    }
}
