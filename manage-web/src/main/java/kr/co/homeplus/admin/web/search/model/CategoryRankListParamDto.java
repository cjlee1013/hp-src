package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "카테고리랭킹 > 리스트 조회")
public class CategoryRankListParamDto {
    @ApiParam(value = "검색어")
    private String keyword;
    @ApiParam(value = "검색유형 ( 키워드 : KEYWORD, 카테고리번호 : CATEGORYID )", defaultValue = "KEYWORD", required = true, allowableValues = "KEYWORD, CATEGORYID")
    private String searchType;
    @ApiParam(value = "사용여부 (사용 : 1, 미사용 : 0)", allowableValues = "1, 0")
    private String schUseFlag;
    @ApiParam(value = "재설정대상 (Y : 1, N : 0)", allowableValues = "1, 0")
    private String schResetFlag;

    private int offset = 0;
    private int limit = 100;
}
