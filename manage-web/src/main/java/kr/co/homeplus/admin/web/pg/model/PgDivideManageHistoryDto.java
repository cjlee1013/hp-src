package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class PgDivideManageHistoryDto {
    @ApiModelProperty(value = "사이트유형명")
    @RealGridColumnInfo(headText = "사이트", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String siteTypeNm;

    @ApiModelProperty(value = "플랫폼(PC,모바일)")
    @RealGridColumnInfo(headText = "디바이스", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String platform;

    @ApiModelProperty(value = "상위결제수단명")
    @RealGridColumnInfo(headText = "결제수단", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String parentMethodNm;

    @ApiModelProperty(value = "inicis비율")
    @RealGridColumnInfo(headText = "INICIS", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Integer inicisRate;

    @ApiModelProperty(value = "tosspg비율")
    @RealGridColumnInfo(headText = "TOSSPG", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Integer tosspgRate;

    @ApiModelProperty(value = "수정일자")
    @RealGridColumnInfo(headText = "수정일", width = 140, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgDt;

    @ApiModelProperty(value = "수정자id")
    @RealGridColumnInfo(headText = "수정자", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgId;

    @ApiModelProperty(value = "상위결제수단코드(신용카드,휴대폰결제등)")
    @RealGridColumnInfo(headText = "parentMethodCd", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String parentMethodCd;
}