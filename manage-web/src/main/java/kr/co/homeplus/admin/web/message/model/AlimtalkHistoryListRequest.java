package kr.co.homeplus.admin.web.message.model;

import lombok.Data;

@Data
public class AlimtalkHistoryListRequest {

    /**
     * 검색날짜 필드
     */
    private String schDate;
    /**
     * 검색시작일
     */
    private String schStartDate;
    /**
     * 검색종료일
     */
    private String schEndDate;
    /**
     * 검색어
     */
    private String schKeyword;
    /**
     * 상태
     */
    private String schWorkStatus;
}
