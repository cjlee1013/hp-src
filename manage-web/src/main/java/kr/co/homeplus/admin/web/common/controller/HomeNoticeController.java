package kr.co.homeplus.admin.web.common.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.cnotice.AdminNoticeListParamDto;
import kr.co.homeplus.admin.web.common.model.cnotice.AdminNoticeListSelectDto;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.harmful.AdminHarmfulItemListParamDto;
import kr.co.homeplus.admin.web.item.model.harmful.AdminHarmfulItemListSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;


@Slf4j
@Controller
public class HomeNoticeController {
    private final ResourceClient resourceClient;

    @Value("${plus.resource-routes.image.url}")
    private String homeImgUrl;

    private final Map<String, Object> cnoticeGridHeadBaseInfo = RealGridHelper
        .create("cnoticeGridHeadBaseInfo", AdminNoticeListSelectDto.class);
    private final Map<String, Object> harmGridHeadBaseInfo = RealGridHelper
        .create("harmGridHeadBaseInfo", AdminHarmfulItemListSelectDto.class);

    public HomeNoticeController(ResourceClient resourceClient) {
        this.resourceClient = resourceClient;
    }

    @GetMapping(value =  "/home")
    public String home(final Model model) {
        // 일반공지 그리드
        model.addAllAttributes(cnoticeGridHeadBaseInfo);
        // 위해상품공지 그리드
        model.addAllAttributes(harmGridHeadBaseInfo);
        model.addAttribute("homeImgUrl", this.homeImgUrl);

        return "/common/homeMain";
    }

    @ResponseBody
    @GetMapping(value =  "/home/getAdminNoticeList.json")
    private List<AdminNoticeListSelectDto> getNoticeListType(String searchType, String searchKeyword) throws Exception {

        String apiUri = "/manage/notice/getAdminNoticeList";

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        String endDate = df.format(c.getTime());
        c.add(Calendar.MONTH, -6);
        String startDate = df.format(c.getTime());

        AdminNoticeListParamDto adminNoticeListParamDto = new AdminNoticeListParamDto();

        adminNoticeListParamDto.setSearchEndDt(endDate);
        adminNoticeListParamDto.setSearchStartDt(startDate);
        adminNoticeListParamDto.setSearchKeyword(searchKeyword);
        adminNoticeListParamDto.setSearchType(searchType);

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<AdminNoticeListSelectDto>>>() {};

        return (List<AdminNoticeListSelectDto>) resourceClient.getForResponseObject(
            ResourceRouteName.MANAGE, StringUtil
                .getRequestString(apiUri, AdminNoticeListParamDto.class, adminNoticeListParamDto), typeReference).getData();
    }

    private  long diffOfDate(String begin, String end) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        Date beginDate = formatter.parse(begin);
        Date endDate = formatter.parse(end);

        long diff = endDate.getTime() - beginDate.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);

        return diffDays;
    }

    /**
     * 위해상품 조회(검색)
     *
     * @param adminHarmfulItemListParamDto
     */
    @ResponseBody
    @GetMapping(value = "/home/getHarmProductList.json")
    public List<AdminHarmfulItemListSelectDto> getHarmProductList(@ModelAttribute AdminHarmfulItemListParamDto adminHarmfulItemListParamDto) throws Exception{

        String apiUri = "/item/harmful/getAdminHarmfulItemList";

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        String endDate = df.format(c.getTime());
        c.add(Calendar.MONTH, -6);
        String startDate = df.format(c.getTime());

        adminHarmfulItemListParamDto.setSearchStartDt(startDate);
        adminHarmfulItemListParamDto.setSearchEndDt(endDate);


        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<AdminHarmfulItemListSelectDto>>>() {};

        return (List<AdminHarmfulItemListSelectDto>) resourceClient.getForResponseObject(
            ResourceRouteName.ITEM, StringUtil
                .getRequestString(apiUri, AdminHarmfulItemListParamDto.class, adminHarmfulItemListParamDto), typeReference).getData();


    }

}