package kr.co.homeplus.admin.web.message.model;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import kr.co.homeplus.admin.web.message.enums.WorkStatus;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import lombok.Data;

@Data
public class RecipientInfo {

    @ApiModelProperty(value = "채널타입")
    private WorkType workType;

    @ApiModelProperty(value = "alimtalkWorkSeq")
    private Long alimtalkWorkSeq;

    @ApiModelProperty(value = "alimtalkListSeq")
    private Long alimtalkListSeq;

    @ApiModelProperty(value = "alimtalkHistorySeq")
    private Long alimtalkHistorySeq;

    @ApiModelProperty(value = "알림톡 템플릿 코드")
    private String alimtalkTemplateCode;

    @ApiModelProperty(value = "알림톡 발송일시")
    private LocalDateTime alimtalkSendDt;

    @ApiModelProperty(value = "알림톡 발송 결과")
    private WorkStatus alimtalkSendResultCd;

    @ApiModelProperty(value = "smsWorkSeq")
    private Long smsWorkSeq;

    @ApiModelProperty(value = "smsListSeq")
    private Long smsListSeq;

    @ApiModelProperty(value = "smsHistorySeq")
    private Long smsHistorySeq;

    @ApiModelProperty(value = "SMS 템플릿 코드")
    private String smsTemplateCode;

    @ApiModelProperty(value = "SMS 발송일시")
    private LocalDateTime smsSendDt;

    @ApiModelProperty(value = "SMS 발송 결과")
    private WorkStatus smsSendResultCd;

    @ApiModelProperty(value = "전화번호 또는 이메일")
    private String recipient;

    @ApiModelProperty(value = "전환발송 여부")
    private String switchTargetYn;

    @ApiModelProperty(value = "최초 발송 시도시간")
    private LocalDateTime sendDt;

    @ApiModelProperty(value = "최초 발송 등록일")
    private LocalDateTime regDt;
}
