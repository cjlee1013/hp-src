package kr.co.homeplus.admin.web.search.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReplacedKeywordSetDto {
    private long totalCount;
    private List<ReplacedKeywordSetParamDto> list;

    @Getter
    @Setter
    static class ReplacedKeyword {
        private Long replacedId;
        private String keyword;
        private String replacedKeyword;
        private char useFlag;
        private String useFlagNm;
        private String sdate;
        private String edate;
        private String updateDt;
        private String regDt;
        private String creator;
        private String modifier;
        private String administratorId;
    }
}
