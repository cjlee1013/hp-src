package kr.co.homeplus.admin.web.settle.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.settle.model.SettleMallType;
import kr.co.homeplus.admin.web.settle.model.dailyPayment.DailyPaymentGetDto;
import kr.co.homeplus.admin.web.settle.model.dailyPayment.DailyPaymentSetDto;
import kr.co.homeplus.admin.web.settle.model.dailyPayment.DailyPaymentSumGetDto;
import kr.co.homeplus.admin.web.settle.model.dailyPayment.PaymentInfoGetDto;
import kr.co.homeplus.admin.web.settle.model.dailyPayment.PaymentInfoSetDto;
import kr.co.homeplus.admin.web.settle.service.DailyPaymentService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/dailyPayment")
public class DailyPaymentController {

    private final DailyPaymentService dailyPaymentService;
    private final SettleCommonService settleCommonService;
    private final CodeService codeService;

    public DailyPaymentController(DailyPaymentService dailySettleService, SettleCommonService settleCommonService, CodeService codeService) {
        this.dailyPaymentService = dailySettleService;
        this.settleCommonService = settleCommonService;
        this.codeService = codeService;
    }

    /**
     * 정산관리 > 정산관리 > 판매현황
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dailyPaymentList", method = RequestMethod.GET)
    public String dailyPaymentList(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type"              // 점포유형
        );
        model.addAttribute("storeType", code.get("store_type"));
        model.addAttribute("mallType", Arrays.asList(SettleMallType.values()));
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2021));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.create("dailyPaymentListBaseInfo", DailyPaymentGetDto.class));
        model.addAllAttributes(RealGridHelper.create("dailyPaymentDetailBaseInfo", PaymentInfoGetDto.class));
        model.addAllAttributes(RealGridHelper.create("dailyPaymentSumBaseInfo", DailyPaymentSumGetDto.class));

        return "/settle/dailyPaymentList";
    }

    /**
     * 정산관리 > 정산관리 > 판매현황 > 메인 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getDailyPaymentList.json", method = RequestMethod.GET)
    public List<DailyPaymentGetDto> getDailyPaymentList(DailyPaymentSetDto listParamDto) throws Exception {
        return dailyPaymentService.getDailyPaymentList(listParamDto);
    }

    /**
     * 정산관리 > 정산관리 > 판매현황 > 상세 매출 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getDailyPaymentInfo.json", method = RequestMethod.GET)
    public List<PaymentInfoGetDto> getDailyPaymentInfo(PaymentInfoSetDto listParamDto) throws Exception {
        return dailyPaymentService.getDailyPaymentInfo(listParamDto);
    }

    /**
     * 정산관리 > 정산관리 > 판매현황 > 점별 판매금액 합계
     */
    @ResponseBody
    @RequestMapping(value = "/getDailyPaymentSum.json", method = RequestMethod.GET)
    public List<DailyPaymentSumGetDto> getDailyPaymentSum(DailyPaymentSetDto listParamDto) throws Exception {
        return dailyPaymentService.getDailyPaymentSum(listParamDto);
    }
}