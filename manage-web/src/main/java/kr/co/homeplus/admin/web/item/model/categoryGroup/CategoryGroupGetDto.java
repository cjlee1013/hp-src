package kr.co.homeplus.admin.web.item.model.categoryGroup;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CategoryGroupGetDto {

	private Integer gcateCd;

	private String gcateNm;

	private Integer priority;

	private Integer largeCount;

	private String useYnTxt;

	private String useYn;

	private String regDt;

	private String regNm;

	private String chgDt;

	private String chgNm;
}
