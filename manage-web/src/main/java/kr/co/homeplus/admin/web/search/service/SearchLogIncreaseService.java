package kr.co.homeplus.admin.web.search.service;

import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SearchLogIncreaseService {

    private final ResourceClient resourceClient;

    public ResponseObject getIncreaseList( String dateType, String siteType, String startDt, String endDt) {
        String apiUri = "/searchlog/getIncreaseSearchLogList?dateType=" + dateType + "&siteType=" + siteType
            +  "&startDt=" + startDt + "&endDt=" + endDt;
        return getResponseObject(apiUri);
    }

    public ResponseObject getSearchLogCount(String siteType, String startDt, String endDt) {
        String apiUri = "/searchlog/getSearchLogCount?siteType=" + siteType
                +  "&startDt=" + startDt + "&endDt=" + endDt;
        return getResponseObject(apiUri);
    }

    private ResponseObject getResponseObject(String apiUri){
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }
}
