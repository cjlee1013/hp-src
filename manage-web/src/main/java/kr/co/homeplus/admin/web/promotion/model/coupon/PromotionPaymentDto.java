package kr.co.homeplus.admin.web.promotion.model.coupon;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class PromotionPaymentDto {
    @RealGridColumnInfo(headText = "코드", width = 100, sortable = true)
    private String methodCd;
    @RealGridColumnInfo(headText = "카드명", width = 150, columnType = RealGridColumnType.NAME, sortable = true)
    private String methodNm;
    @RealGridColumnInfo(headText = "그룹코드", width = 100, sortable = true)
    private String groupCd;
    @RealGridColumnInfo(headText = "우선순위", width = 0, columnType = RealGridColumnType.NUMBER, hidden = true, sortable = true)
    private int sortSeq;
    private String useYn;
}