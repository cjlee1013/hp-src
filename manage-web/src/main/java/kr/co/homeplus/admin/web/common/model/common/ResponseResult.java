package kr.co.homeplus.admin.web.common.model.common;


import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseResult {

	private String returnCode;
	private String returnMsg;
	private String returnKey;
	private int returnCnt;
	private List<Integer> returnCnts;
	private int returnTotalCnt;
	private int returnSuccessCnt;
	private int returnFailCnt;

	private Object resultObj;
	private List<?> resultList;

	public void setResultList(List<?> resultList) {
		this.resultList = resultList;
	}

}
