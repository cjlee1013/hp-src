package kr.co.homeplus.admin.web.partner.model.affiliate;


import java.math.BigDecimal;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChannelSetParamDto {

    private String channelId;
    private String partnerId;
    private String channelNm;
    private String siteType;
    private BigDecimal commissionRate;
    private String urlParameter;
    private int cookieTime;
    private String useYn;
    private String userId;
    private List<String> salesCdList;

}
