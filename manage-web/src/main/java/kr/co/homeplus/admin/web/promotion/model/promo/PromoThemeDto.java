package kr.co.homeplus.admin.web.promotion.model.promo;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class PromoThemeDto {

    @RealGridColumnInfo(headText = "분류번호")
    private Long themeNo;

    @RealGridColumnInfo(headText = "분류명", width = 300, columnType = RealGridColumnType.NAME)
    private String themeNm;

    @RealGridColumnInfo(headText = "전시시작일시")
    private String themeDispStartDt;

    @RealGridColumnInfo(headText = "전시종료일시")
    private String themeDispEndDt;

    @RealGridColumnInfo(headText = "전시여부")
    private String themeDispYn;

    @RealGridColumnInfo(headText = "점포유형")
    private String storeType;

    @RealGridColumnInfo(headText = "상품 수")
    private Integer itemCnt;

    @RealGridColumnInfo(headText = "정렬방식")
    private String sortKind;

    @RealGridColumnInfo(headText = "분류 임시 번호", hidden = true)
    private String tempThemeNo;

    @RealGridColumnInfo(headText = "우선순위", hidden = true)
    private Integer priority;

    @RealGridColumnInfo(headText = "PC 템플릿", hidden = true)
    private String pcTemplate;

    @RealGridColumnInfo(headText = "MOBILE 템플릿", hidden = true)
    private String mobileTemplate;
}