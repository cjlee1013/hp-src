package kr.co.homeplus.admin.web.promotion.model.event;

import java.util.List;
import kr.co.homeplus.admin.web.promotion.model.coupon.PromotionPaymentDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MileagePromoDetailSetDto {
    private Long mileageNo;

    private String siteType;

    private String mileageMngNm;

    private String promoStartDt;

    private String promoEndDt;

    private String earnType;

    private Float earnPercent;

    private Integer earnAmount;

    private Integer purchaseMin;

    private Integer earnAmountMax;

    private String storeType;

    private String mileageCode;

    private String displayMessage;

    private Integer expireDayInp;

    private String dispStoreType;

    private String useYn;

    private String empNo;

    private String departCd;

    private String purchaseGuide;

    // 노출 점포 리스트
    private List<Integer> storeIdList;

    private List<String> createItemList;    // 추가된 상품 리스트
    private List<String> deleteItemList;    // 삭제된 상품 리스트

    // 결제수단 리스트 (카드)
    private List<PromotionPaymentDto> paymentList;
}