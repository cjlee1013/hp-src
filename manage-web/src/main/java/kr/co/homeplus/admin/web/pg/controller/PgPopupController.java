package kr.co.homeplus.admin.web.pg.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.pg.model.ExcelItem;
import kr.co.homeplus.admin.web.pg.model.PaymentPolicyHistDto;
import kr.co.homeplus.admin.web.pg.model.PaymentPolicyHistSelectDto;
import kr.co.homeplus.admin.web.pg.service.PaymentPolicyManageService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 어드민 결제관리 팝업 컨트롤러
 */
@Controller
@RequestMapping("/pg/popup")
public class PgPopupController {
    private final ResourceClient resourceClient;
    private final ExcelUploadService excelUploadService;
    private final PaymentPolicyManageService paymentPolicyManageService;

    public PgPopupController(ResourceClient resourceClient, ExcelUploadService excelUploadService, PaymentPolicyManageService paymentPolicyManageService) {
        this.resourceClient = resourceClient;
        this.excelUploadService = excelUploadService;
        this.paymentPolicyManageService = paymentPolicyManageService;
    }

    /**
     * 결제정책 히스토리 팝업
     * @param model
     * @param applyTarget
     * @return
     * @throws Exception
     */
    @GetMapping("/paymentPolicyHistoryPop")
    public String popPaymentPolicyHistory(Model model, @RequestParam(value="policyNo") String policyNo,
                                                       @RequestParam(value="applyTarget") String applyTarget) throws Exception {
        model.addAllAttributes(RealGridHelper.create("paymentPolicyHistoryPopBaseInfo", PaymentPolicyHistDto.class));
        model.addAttribute("policyNo", policyNo);
        model.addAttribute("applyTarget", applyTarget);
        return "/pg/pop/paymentPolicyHistoryPop";
    }

    /**
     * 결제정책 히스토리 조회
     * @return
     */
    @ResponseBody
    @GetMapping("/getPaymentPolicyHistory.json")
    public List<PaymentPolicyHistDto> getPaymentPolicyHistory(PaymentPolicyHistSelectDto paymentPolicyHistSelectDto) throws Exception {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_PAYMENT_POLICY_HISTORY, PaymentPolicyHistSelectDto.class, paymentPolicyHistSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PaymentPolicyHistDto>>>() {};

        return (List<PaymentPolicyHistDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 결제정책 적용대상 일괄등록 팝업
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/paymentPolicyApplyUploadPop")
    public String popPaymentPolicyApplyUpload(Model model, @RequestParam(value = "callback") String callBackScript) throws Exception {
        model.addAttribute("callBackScript", callBackScript);
        return "/pg/pop/paymentPolicyApplyUploadPop";
    }

    /**
     * 엑셀 상품 적용
     * @param httpServletRequest
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/excelItemApply.json")
    public List<ExcelItem> excelItemApply(HttpServletRequest httpServletRequest) throws Exception {
        String fileName = "uploadFile";
        MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) httpServletRequest;
        MultipartFile multipartFile = multipartHttpServletRequest.getFile(fileName);

        /**
         * 1. 엑셀 메타정보 입력
         *
         * 엑셀의 시작/마지막 행, 시작/마지막열, 읽을 대상시트, 맵핑 할 DTO필드 정보입력
         * 시작행(row), 시작열(cell), 대상시트(sheet 1번째)
         * endRow, lastCell 생략가능(default로 자동설정)
         */
        ExcelHeaders headers = new ExcelHeaders.Builder().header(0, "상품번호", "itemNo", true).build();
        ExcelUploadOption<ExcelItem> excelUploadOption = new ExcelUploadOption.Builder<>(ExcelItem.class, headers, 1, 0).build();
//        excelUploadOption.setBeginRow(1);   //시작행(1 부터 시작, 0은 헤더)
//        excelUploadOption.setFirstCell(0); //시작열(0 부터 시작)
//        excelUploadOption.setActiveSheetIndex(0); //대상sheet(0 부터 시작)

        /**
         * 1.1 엑셀 헤더와 맵핑 될 DTO필드정보 입력
         *
         * 작성시 대상DTO와 맵핑할 셀의 수, 셀의 시작/끝 정보가 일치해야 한다.(순서 무관)
         * e.g.)header.put(index, 필드명);
         * - index는 엑셀 헤더 위치, 0부터 시작
         * - 헤더 명은 String으로 헤더 위치에 맵핑할 DTO의 필드명을 입력한다.(필드명은 camelCase로 입력
         */
//        ExcelUploadHeaderInfo header = new ExcelUploadHeaderInfo();
//
//        String headerNm = "itemNo";
//        header.put(0, headerNm);

        /**
         * 1.2 엑셀 메타정보에 맵핑정보 입력
         */
/*
        excelUploadOption.setHeaderInfo(header);
        excelUploadOption.set
*/

        /**
         * 2. 엑셀 조회
         * multipartFile과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.
         */
        List<ExcelItem> result = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        List<String> itemNoList = result.stream().map(ExcelItem::getItemNo).distinct().collect(Collectors.toList());

        int prevSize = itemNoList.size();
        if (prevSize == 0){
            return null;
        }
        //최대등록가능 갯수는 1,000개로 제한
        if (prevSize > 1000) {
            List<ExcelItem> limitItemList = new ArrayList<>();
            ExcelItem excelItem = new ExcelItem();
            excelItem.setErrorCount(prevSize);
            excelItem.setItemNm("일괄 등록은 최대 1,000개까지 가능합니다");
            excelItem.setItemNo("error");
            limitItemList.add(excelItem);
            return limitItemList;
        }

        // 일괄등록 요청상품 itemNo로 검증 : escrow DB 상품 동기화 테이블 조회
        List<ExcelItem> validItemList = paymentPolicyManageService.getItemValidCheck(itemNoList);
        int validSize = validItemList.size();

        if (validSize == 0) {
            validItemList = new ArrayList<>();
            ExcelItem excelItem = new ExcelItem();
            excelItem.setErrorCount(prevSize);
            excelItem.setItemNm("등록가능한 상품이 없습니다");
            excelItem.setItemNo("error");
            validItemList.add(excelItem);
        }
        if (prevSize != validSize) {
            validItemList.get(0).setErrorCount(prevSize - validSize);
        }
        return validItemList;
    }
}
