package kr.co.homeplus.admin.web.settle.model.accounting;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AccountSubjectListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;
}
