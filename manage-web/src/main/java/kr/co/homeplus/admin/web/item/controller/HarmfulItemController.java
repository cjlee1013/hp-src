package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.harmful.HarmfulDSItemListSelectDto;
import kr.co.homeplus.admin.web.item.model.harmful.HarmfulItemDetailParamDto;
import kr.co.homeplus.admin.web.item.model.harmful.HarmfulItemDetailSelectDto;
import kr.co.homeplus.admin.web.item.model.harmful.HarmfulItemListParamDto;
import kr.co.homeplus.admin.web.item.model.harmful.HarmfulItemListSelectDto;
import kr.co.homeplus.admin.web.item.model.harmful.HarmfulItemSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item")
public class HarmfulItemController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;

    /**
     * 심사관리 > 위해상품관리 > 위해상품관리
     * @param model
     * @return String
     */
    @RequestMapping(value = "/harmful/harmfullItemMain", method = RequestMethod.GET)
    public String categoryAttributeMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                "harmful_institute"        // 위해상품 검사기관
                ,"harmful_period_type"  //위해상품조회기간
                ,"harmful_search_type" //위해상품검색어구분
                ,"harmful_status" //위해상품 처리상태
                ,"harmful_type" //위해상품유형
        );

        model.addAttribute("searchPeriodType", code.get("harmful_period_type"));
        model.addAttribute("searchItemStatus", code.get("harmful_status"));
        model.addAttribute("searchItemType", code.get("harmful_type"));
        model.addAttribute("searchInstitute", code.get("harmful_institute"));
        model.addAttribute("searchType", code.get("harmful_search_type"));

        model.addAllAttributes(RealGridHelper.create("harmfulItemListGridBaseInfo", HarmfulItemListSelectDto.class));
        model.addAllAttributes(RealGridHelper.create("harmfulDSItemListGridBaseInfo", HarmfulDSItemListSelectDto.class));

        return "/item/harmfulItemMain";
    }
    /**
     * 심사관리 > 위해상품관리 >  위해상품관리
     * @return HarmfulItemListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/harmful/getHarmfulItemList.json", method = RequestMethod.GET)
    public List<HarmfulItemListSelectDto> getHarmfulItemList(HarmfulItemListParamDto listParamDto) throws Exception {

        String apiUri = "/item/harmful/getHarmfulItemList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<HarmfulItemListSelectDto>>>() {};
        return (List<HarmfulItemListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, HarmfulItemListParamDto.class, listParamDto), typeReference).getData();
    }
    /**
     * 심사관리 > 위해상품관리 >  위해상품상세
     * @return HarmfulItemListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/harmful/getHarmfulItemDetail.json", method = RequestMethod.GET)
    public HarmfulItemDetailSelectDto getHarmfulItemDetail(HarmfulItemDetailParamDto listParamDto) throws Exception {

        String apiUri = "/item/harmful/getHarmfulItemDetail";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<HarmfulItemDetailSelectDto>>() {};
        return (HarmfulItemDetailSelectDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, HarmfulItemDetailParamDto.class, listParamDto), typeReference).getData();
    }
    /**
     * 심사관리 > 위해상품관리 >  상품정보조회
     * @return HarmfulItemListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/harmful/getDSItemList.json", method = RequestMethod.GET)
    public List<HarmfulDSItemListSelectDto> getDSItemList(@RequestParam(name = "itemList") String itemList) throws Exception {

        String apiUri = "/item/harmful/getDSItemList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<HarmfulDSItemListSelectDto>>>() {};
        return (List<HarmfulDSItemListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?itemList=" + itemList , typeReference).getData();
    }
    /**
     * 심사관리 > 위해상품관리 >  위해상품 저장
     * @param harmfulItemSetDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/harmful/setHarmfulItem.json"}, method = RequestMethod.POST)
    public ResponseResult setHarmfulItem(@RequestBody HarmfulItemSetDto harmfulItemSetDto) throws Exception {

        String apiUri = "/item/harmful/setHarmfulItem";
        harmfulItemSetDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, harmfulItemSetDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();

    }
    /**
     * 심사관리 > 위해상품관리 >  이미지팝업
     * @param
     * @return ResponseResult
     * @throws Exception
     */
    @RequestMapping(value = {"/popup/getImagePopUp"}, method = RequestMethod.GET)
    public String getPartnerListPop(Model model, @RequestParam(value = "imgUrl") String imgUrl) throws Exception {

        model.addAttribute("imgUrl" , imgUrl );
        return "/item/pop/harmfullImagePop";
    }
}
