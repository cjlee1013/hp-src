package kr.co.homeplus.admin.web.escrow.model;

import java.util.List;
import lombok.Data;

/**
 * 캘린더 출력 할 날짜 Dto (월단위)
 */
@Data
public class CalendarDateDto {
    // 출력 년도
    Integer year;
    // 출력 월
    Integer month;
    // 출력 1일자 요일
    Integer firstDayOfWeek;
    // 오늘
    String today;
    // 출력 일자
    List<CalendarDayDto> dayList;
}