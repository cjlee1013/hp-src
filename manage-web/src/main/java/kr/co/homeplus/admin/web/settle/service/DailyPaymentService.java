package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.dailyPayment.DailyPaymentGetDto;
import kr.co.homeplus.admin.web.settle.model.dailyPayment.DailyPaymentSetDto;
import kr.co.homeplus.admin.web.settle.model.dailyPayment.DailyPaymentSumGetDto;
import kr.co.homeplus.admin.web.settle.model.dailyPayment.PaymentInfoGetDto;
import kr.co.homeplus.admin.web.settle.model.dailyPayment.PaymentInfoSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DailyPaymentService {

    private final ResourceClient resourceClient;

    /**
     * 정산관리 > 판매현황 > 메인 리스트 조회
     */
    public List<DailyPaymentGetDto> getDailyPaymentList(DailyPaymentSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/dailyPayment/getDailyPaymentList",
            new ParameterizedTypeReference<ResponseObject<List<DailyPaymentGetDto>>>(){}).getData();
    }

    /**
     * 정산관리 > 판매현황 > 상세 판매내역 조회
     */
    public List<PaymentInfoGetDto> getDailyPaymentInfo(PaymentInfoSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/dailyPayment/getDailyPaymentInfo",
            new ParameterizedTypeReference<ResponseObject<List<PaymentInfoGetDto>>>(){}).getData();
    }

    /**
     * 정산관리 > 판매현황 > 점별 매출금액 합계
     */
    public List<DailyPaymentSumGetDto> getDailyPaymentSum(DailyPaymentSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/dailyPayment/getDailyPaymentSum",
            new ParameterizedTypeReference<ResponseObject<List<DailyPaymentSumGetDto>>>(){}).getData();
    }

}
