package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimProcessDto {


    @ApiModelProperty(value = "구분")
    @RealGridColumnInfo(headText = "구분")
    private String processType;

    @ApiModelProperty(value = "처리일시")
    @RealGridColumnInfo(headText = "처리일시", columnType = RealGridColumnType.DATETIME, width = 150)
    private String processDt;

    @ApiModelProperty(value = "처리채널")
    @RealGridColumnInfo(headText = "처리채널")
    private String regChannel;

    @ApiModelProperty(value = "처리자")
    @RealGridColumnInfo(headText = "처리자")
    private String regId;

}
