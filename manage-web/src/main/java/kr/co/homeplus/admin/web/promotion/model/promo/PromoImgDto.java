package kr.co.homeplus.admin.web.promotion.model.promo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PromoImgDto {
    private String deviceType;

    // 이미지 타입
    private String imgType;

    // 이미지 URL
    private String imgUrl;

    private Integer imgWidth;

    private Integer imgHeight;
}