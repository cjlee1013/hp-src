package kr.co.homeplus.admin.web.message.model;

import java.time.LocalDateTime;
import kr.co.homeplus.admin.web.message.enums.WorkStatus;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class ManualWorkGrid {

    @RealGridColumnInfo(hidden = true)
    private long manualSendSeq;
    @RealGridColumnInfo(hidden = true)
    private long manualSendWorkSeq;
    @RealGridColumnInfo(hidden = true)
    private long coupledWorkSeq;
    @RealGridColumnInfo(headText = "작업종류")
    private String workType;
    @RealGridColumnInfo(headText = "작업명")
    private String coupledWorkName;
    @RealGridColumnInfo(headText = "설명")
    private String description;
    @RealGridColumnInfo(headText = "상태", columnType = RealGridColumnType.LOOKUP)
    private WorkStatus workStatus;
    @RealGridColumnInfo(headText = "예약발송여부")
    private String delayYn;
    @RealGridColumnInfo(headText = "예약발송일시", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME)
    private LocalDateTime workDt;
    @RealGridColumnInfo(headText = "발송종료일시", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME)
    private LocalDateTime endDt;
    @RealGridColumnInfo(headText = "등록자")
    private String regId;
    @RealGridColumnInfo(headText = "등록일시", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME)
    private LocalDateTime regDt;
    @RealGridColumnInfo(headText = "수정자")
    private String chgId;
    @RealGridColumnInfo(headText = "수정일시", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME)
    private LocalDateTime chgDt;
    @RealGridColumnInfo(headText = "삭제여부", hidden = true)
    private String delYn;
}
