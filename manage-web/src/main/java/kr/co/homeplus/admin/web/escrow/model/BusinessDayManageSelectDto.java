package kr.co.homeplus.admin.web.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "시스템관리 > 영업일관리 > 영업일관리 Search DTO")
public class BusinessDayManageSelectDto {
    @ApiModelProperty(notes = "조회 년도")
    private Integer schYear;
    @ApiModelProperty(notes = "조회 월")
    private Integer schMonth;
}