package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DictionaryInsertMorphExpansionSetParamDto {
    @NotNull
    private Integer wordId;
    private String synonym;
    private String hyponym;
    private String relatednym;
    private String userId;
}
