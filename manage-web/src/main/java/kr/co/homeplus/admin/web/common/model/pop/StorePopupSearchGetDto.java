package kr.co.homeplus.admin.web.common.model.pop;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * 점포 정보 Get_entry
 */
@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class StorePopupSearchGetDto {
    // 점포ID
    @RealGridColumnInfo(headText = "점포코드", sortable = true, width = 80, columnType = RealGridColumnType.NUMBER_CENTER, fieldType = RealGridFieldType.NUMBER)
    private String 	storeId;

    // 점포명
    @RealGridColumnInfo(headText = "점포명", sortable = true, width = 100)
    private String 	storeNm;

    // 점포유형명
    @RealGridColumnInfo(headText = "점포유형", sortable = true, width = 100)
    private String 	storeTypeNm;

    // 점포종류명
    @RealGridColumnInfo(headText = "점포구분", sortable = true, width = 100)
    private String 	storeKindNm;

    //사용여부
    @RealGridColumnInfo(headText = "사용여부", sortable = true, width = 100)
    private String  useYnNm;

    //온라인여부
    @RealGridColumnInfo(headText = "온라인여부", sortable = true, width = 100)
    private String  onlineYnNm;

    // 점포유형
    @RealGridColumnInfo(headText = "점포유형코드", hidden = true)
    private String 	storeType;

    // 점포종류
    @RealGridColumnInfo(headText = "점포구분코드", hidden = true)
    private String 	storeKind;

    // 원점포코드
    @RealGridColumnInfo(headText = "원점포코드", hidden = true)
    private String 	originStoreId;
}
