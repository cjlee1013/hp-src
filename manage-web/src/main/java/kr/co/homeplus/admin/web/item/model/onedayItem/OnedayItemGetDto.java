package kr.co.homeplus.admin.web.item.model.onedayItem;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class OnedayItemGetDto {

    @RealGridColumnInfo(headText = "상품번호")
    private String itemNo;

    @RealGridColumnInfo(headText = "상품구분", order = 1)
    private String mallTypeNm;

    @RealGridColumnInfo(headText = "상품명", width = 250, order = 2)
    private String itemNm;

    @RealGridColumnInfo(headText = "전시여부", width = 70, order = 3)
    private String dispYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 70, order = 4)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 70, order = 5)
    private String regDt;

    @RealGridColumnInfo(headText = "전시여부코드", hidden = true, order = 6)
    private String dispYn;

    @RealGridColumnInfo(headText = "사이트코드", hidden = true, order = 7)
    private String siteType;

    @RealGridColumnInfo(headText = "상품유형( HYPER : 하이퍼, AURORA : 새벽배송, EXP : 익스프레스, CLUB : 더클럽 )", hidden = true, order = 8)
    private String itemStoreType;

    @RealGridColumnInfo(headText = "상품구분", hidden = true, order = 9)
    private String mallType;

    private int priority;
    private String dispDt;
    private String chgDt;
    private String regId;
    private String chgId;
    private String chgNm;

}
