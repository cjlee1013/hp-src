package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.priceChangeHist.ItemDetailGetDto;
import kr.co.homeplus.admin.web.item.model.priceChangeHist.PriceChangeHistGetDto;
import kr.co.homeplus.admin.web.item.model.priceChangeHist.PriceChangeHistParamDto;
import kr.co.homeplus.admin.web.item.model.priceChangeHist.PriceChangeHistSetParamDto;
import kr.co.homeplus.admin.web.item.model.priceChangeHist.PriceChangeListParamDto;
import kr.co.homeplus.admin.web.item.model.priceChangeHist.SalePriceDetailListGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item")
public class PriceChangeHistController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;

    /**
     * 상품관리 > 점포상품관리 > 판매가변경관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/priceChangeHist/priceChangeHistMain", method = RequestMethod.GET)
    public String priceChangeHistMain(Model model) {

        model.addAllAttributes(RealGridHelper.create("itemGridBaseInfo", ItemDetailGetDto.class));
        model.addAllAttributes(RealGridHelper.create("itemStoreGridBaseInfo", SalePriceDetailListGetDto.class));

        return "/item/priceChangeHistMain";
    }

    /**
     * 상품관리 > 점포상품관리 > 판매가변경관리 상품 조회
     *
     * @param priceChangeListParamDto
     * @return List<ItemDetailGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/priceChangeHist/getItemList.json"}, method = RequestMethod.GET)
    public List<ItemDetailGetDto> getItemList(@Valid PriceChangeListParamDto priceChangeListParamDto) {
        String apiUri = "/item/priceChangeHist/getItemList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemDetailGetDto>>>() {};
        return (List<ItemDetailGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, PriceChangeListParamDto.class, priceChangeListParamDto), typeReference).getData();
    }

    /**
     * 상품관리 > 점포상품관리 > 판매가변경관리 점포 조회
     *
     * @param priceChangeHistParamDto
     * @return List<SalePriceDetailListGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/priceChangeHist/getItemStoreList.json"}, method = RequestMethod.GET)
    public List<SalePriceDetailListGetDto> getItemStoreList(@Valid PriceChangeHistParamDto priceChangeHistParamDto) {
        String apiUri = "/item/priceChangeHist/getItemStoreList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<SalePriceDetailListGetDto>>>() {};
        return (List<SalePriceDetailListGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, PriceChangeHistParamDto.class, priceChangeHistParamDto), typeReference).getData();
    }

    /**
     * 상품관리 > 점포상품관리 > 판매가변경관리 변경이력 (팝업)
     * @param model
     * @param itemNo
     * @param storeType
     * @return
     * @throws Exception
     */
    @RequestMapping("/popup/priceChangeHistPop")
    public String priceChangeHistPop(Model model, @RequestParam(value="itemNo") String itemNo, @RequestParam(value="storeType") String storeType) {
        model.addAllAttributes(RealGridHelper.create("priceChangeHistGridBaseInfo", PriceChangeHistGetDto.class));
        model.addAttribute("itemNo", itemNo);
        model.addAttribute("storeType", storeType);
        return "/item/pop/priceChangeHistPop";
    }

    /**
     * 상품관리 > 점포상품관리 > 판매가변경관리 변경이력 조회
     *
     * @param priceChangeHistParamDto
     * @return List<ItemDetailGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/priceChangeHist/getPriceChangeHist.json"}, method = RequestMethod.GET)
    public List<PriceChangeHistGetDto> getPriceChangeHist(@Valid PriceChangeHistParamDto priceChangeHistParamDto) {
        String apiUri = "/item/priceChangeHist/getPriceChangeHist";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PriceChangeHistGetDto>>>() {};
        return (List<PriceChangeHistGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, PriceChangeHistParamDto.class, priceChangeHistParamDto), typeReference).getData();
    }

    /**
     * 상품관리 > 점포상품관리 > 판매가변경관리 변경(등록)
     *
     * @param priceChangeHistSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/priceChangeHist/setPriceChangeHist.json"}, method = RequestMethod.POST)
    public ResponseResult setPriceChangeHist(@Valid @RequestBody List<PriceChangeHistSetParamDto> priceChangeHistSetParamDto) {
        String apiUri = "/item/priceChangeHist/setPriceChangeHist";
        priceChangeHistSetParamDto.get(0).setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, priceChangeHistSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

}
