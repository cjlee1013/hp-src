package kr.co.homeplus.admin.web;

import java.nio.charset.StandardCharsets;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;

@Slf4j
@Configuration
@SpringBootApplication
@ComponentScan({"kr.co.homeplus", "kr.co.homeplus.plus"})
public class AdminWebApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(AdminWebApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AdminWebApplication.class);
    }

    @Bean
    public HttpMessageConverter<String> responseBodyConverter() {
        log.info("loaded StringHttpMessageConverter(Charset.forName(\"UTF-8\"))");
        return new StringHttpMessageConverter(StandardCharsets.UTF_8);
    }
}
