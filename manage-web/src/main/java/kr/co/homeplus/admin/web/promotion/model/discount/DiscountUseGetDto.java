package kr.co.homeplus.admin.web.promotion.model.discount;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class DiscountUseGetDto {
    @RealGridColumnInfo(headText = "회원번호", sortable = true, width = 100)
    private String userNo;

    @RealGridColumnInfo(headText = "회원명", sortable = true, width = 100)
    private String userNm;

    @RealGridColumnInfo(headText = "주문번호", sortable = true, width = 100)
    private Long purchaseOrderNo;

    @RealGridColumnInfo(headText = "사용금액", sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER, width = 100)
    private long discountAmt;

    @RealGridColumnInfo(headText = "사용일", sortable = true, columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String orderDt;

    @RealGridColumnInfo(headText = "할인구분", hidden = true, width = 100)
    private String discountKind;

    private int discountNo;
}
