package kr.co.homeplus.admin.web.partner.model.market;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MarketListGetDto {

    @ApiModelProperty(value = "연동업체ID")
    @RealGridColumnInfo(headText = "연동업체ID", width = 120, sortable = true)
    private String partnerId;

    @ApiModelProperty(value = "연동업체명")
    @RealGridColumnInfo(headText = "연동업체명", width = 150, sortable = true)
    private String businessNm;

    @ApiModelProperty(value = "상호")
    @RealGridColumnInfo(headText = "상호", hidden = true)
    private String siteNm;

    @ApiModelProperty(value = "연동업체명")
    @RealGridColumnInfo(headText = "연동업체명", hidden = true)
    private String partnerNm;

    @ApiModelProperty(value = "사업자등록번호")
    @RealGridColumnInfo(headText = "사업자등록번호", width = 120, sortable = true)
    private String partnerNo;

    @ApiModelProperty(value = "사업자유형")
    @RealGridColumnInfo(headText = "사업자유형")
    private String operatorTypeNm;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", hidden = true)
    private String useYn;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", width = 120, sortable = true)
    private String useYnNm;

    @ApiModelProperty(value = "등록자")
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regNm;

    @ApiModelProperty(value = "등록일")
    @RealGridColumnInfo(headText = "등록일", columnType= RealGridColumnType.DATETIME, width = 100)
    private String regDt;

    @ApiModelProperty(value = "수정자")
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgNm;

    @ApiModelProperty(value = "수정일")
    @RealGridColumnInfo(headText = "수정일",columnType= RealGridColumnType.DATETIME,  width = 100)
    private String chgDt;

}
