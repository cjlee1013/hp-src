package kr.co.homeplus.admin.web.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "선물세트 발송지 관리")
@Getter
@Setter
@EqualsAndHashCode
public class ReserveShipPlaceManageSelectModel {
    @ApiModelProperty(value = "검색시작일자(YYYY-MM-DD)", position = 1)
    private String schFromDt;

    @ApiModelProperty(value = "검색종료일자(YYYY-MM-DD)", position = 2)
    private String schEndDt;
}
