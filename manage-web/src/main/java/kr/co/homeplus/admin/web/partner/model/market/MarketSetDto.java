package kr.co.homeplus.admin.web.partner.model.market;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarketSetDto {

    /**
     * 기본정보
     */
    private String partnerId;
    private String partnerType = "COOP";
    private String operatorType;
    private String affiliateType;
    private String businessNm;
    private String affiliateNm;
    private String partnerStatus = "NORMAL";
    private String partnerGrade = "NORMAL";
    private String vendorCd;
    private String useYn;
    private String userId;
    private String isMod;

    /**
     * 사업자정보
     */
    private String partnerNm;
    private String partnerOwner;
    private String partnerNo;
    private String businessConditions;
    private String bizCateCd;
    private String zipcode;
    private String addr1;
    private String addr2;

    /**
     * 사이트정보
     */
    private String siteNm;
    private String siteUrl;
    private String itemCertKey;
    private String orderCertKey;

    /**
     * 수수료정보
     */
    private List<MarketCommissionSetDto> commissionList = new ArrayList<>();

}
