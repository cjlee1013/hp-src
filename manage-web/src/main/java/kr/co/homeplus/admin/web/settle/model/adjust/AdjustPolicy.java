package kr.co.homeplus.admin.web.settle.model.adjust;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum AdjustPolicy {

    TAX("1", "itemNo", "상품번호", "세금계산서 대상"),
    NO_TAX("2", "itemNo", "상품번호", "세금계산서 미대상");

    @Getter
    private final String code;

    @Getter
    private final String noType;

    @Getter
    private final String noTypeName;

    @Getter
    private final String description;

}
