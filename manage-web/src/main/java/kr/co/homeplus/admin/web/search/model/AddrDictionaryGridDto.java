package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class AddrDictionaryGridDto {
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
    public static class DictionaryWordList{
        @RealGridColumnInfo(headText = "번호", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private Integer wordId;
        @RealGridColumnInfo(headText = "단어", width = 45, columnType = RealGridColumnType.BASIC, sortable = true)
        private String wordName;
        @RealGridColumnInfo(headText = "동의어", width = 500, columnType = RealGridColumnType.BASIC, hidden = true)
        private String synonym;
        @RealGridColumnInfo(headText = "사용여부", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private String useFlag;
        @RealGridColumnInfo(headText = "등록일", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private String regDate;
    }
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
    public static class DictionarySynonymList{
        @RealGridColumnInfo(headText = "동의어", width = 90, columnType = RealGridColumnType.BASIC, sortable = true)
        private String synonymName;
    }
}
