package kr.co.homeplus.admin.web.manage.controller;

import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.dspMainTheme.DspMainThemeGetDto;
import kr.co.homeplus.admin.web.manage.model.dspMainTheme.DspMainThemeListParamDto;
import kr.co.homeplus.admin.web.manage.model.dspMainTheme.DspMainThemeSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage/dspMain")
public class DspMainThemeController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    /**
     * 사이트관리 > 전시관리 > 홈테마 관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dspMainThemeMain", method = RequestMethod.GET)
    public String dspMainThemeMain(Model model) {

        codeService.getCodeModel(model
            , "use_yn"		            // 사용여부
            , "dsk_site_gubun"          // 사이트 구분
            , "dsp_main_site_type"      // 사이트 구분 (HOME,CLUB)
            , "dsp_main_period_type"    // 조회기간 기준
        );

        model.addAllAttributes(RealGridHelper.create("dspMainThemeGridBaseInfo", DspMainThemeGetDto.class));

        return "/manage/dspMainThemeMain";
    }

    /**
     * 사이트관리 > 전시관리 > 홈테마 조회
     *
     * @param dspMainThemeListParamDto
     * @return DspMainThemeGetDto
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/getDspMainThemeList.json"}, method = RequestMethod.GET)
    public List<DspMainThemeGetDto> getDspMainThemeList(DspMainThemeListParamDto dspMainThemeListParamDto) {
        String apiUri = "/manage/dspMain/getDspMainThemeList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<DspMainThemeGetDto>>>() {};
        return (List<DspMainThemeGetDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, DspMainThemeListParamDto.class, dspMainThemeListParamDto), typeReference).getData();
    }

    /**
     * 사이트관리 > 전시관리 > 홈테마 등록/수정
     *
     * @param dspMainThemeSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/setDspMainTheme.json"}, method = RequestMethod.POST)
    public ResponseResult setDspMainTheme(@RequestBody DspMainThemeSetParamDto dspMainThemeSetParamDto) {
        String apiUri = "/manage/dspMain/setDspMainTheme";
        dspMainThemeSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, dspMainThemeSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();
    }

}
