package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.partnerPayment.PartnerPaymentAccountSetDto;
import kr.co.homeplus.admin.web.settle.model.partnerPayment.PartnerPaymentListGetDto;
import kr.co.homeplus.admin.web.settle.model.partnerPayment.PartnerPaymentListSetDto;
import kr.co.homeplus.admin.web.settle.model.partnerPayment.PartnerPaymentStateSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PartnerPaymentService {

    private final ResourceClient resourceClient;

    /**
     * 정산관리 > 지급관리 > 메인 리스트 조회
     */
    public List<PartnerPaymentListGetDto> getPartnerPaymentList(PartnerPaymentListSetDto listParamDto) throws Exception {
        List<PartnerPaymentListGetDto> partnerPaymentListGetDtoList = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/partnerPayment/getPartnerPaymentList",
            new ParameterizedTypeReference<ResponseObject<List<PartnerPaymentListGetDto>>>(){}).getData();
        for (PartnerPaymentListGetDto partnerPaymentListGetDto : partnerPaymentListGetDtoList) {
            partnerPaymentListGetDto.setPartnerId(PrivacyMaskingUtils.maskingUserId(partnerPaymentListGetDto.getPartnerId()));
        }
        return partnerPaymentListGetDtoList;
    }

    /**
     * 정산관리 > 지급관리 > 지급상태변경
     */
    public int setPartnerPaymentState(PartnerPaymentStateSetDto partnerPaymentStateSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            partnerPaymentStateSetDto,
            "/admin/partnerPayment/setPartnerPaymentState",
            new ParameterizedTypeReference<ResponseObject<Integer>>(){}).getData();
    }

    /**
     * 정산관리 > 지급관리 > 지급계좌변경
     */
    public int setPartnerPaymentAccount(PartnerPaymentAccountSetDto partnerPaymentAccountSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            partnerPaymentAccountSetDto,
            "/admin/partnerPayment/setPartnerPaymentAccount",
            new ParameterizedTypeReference<ResponseObject<Integer>>(){}).getData();
    }
}
