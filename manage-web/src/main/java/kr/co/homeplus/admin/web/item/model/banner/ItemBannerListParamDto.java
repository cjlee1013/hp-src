package kr.co.homeplus.admin.web.item.model.banner;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemBannerListParamDto {

    //검색일 타입(등록일/노출일)
    private String schDateType;

    //검색시작일
    private String schStartDt;

    //검색종료일
    private String schEndDt;

    //구분
    private String schDispType;

    //노출여부
    private String schDispYn;

    //대카테고리
    private String schCateCd1;

    //중카테고리
    private String schCateCd2;

    //소카테고리
    private String schCateCd3;

    //세카테고리
    private String schCateCd4;

    //등록자
    private String schRegNm;

    //검색타입
    private String schType;

    //검색키워드
    private String schKeyword;

}
