package kr.co.homeplus.admin.web.core.constants;

public class Constants {

    /** 물음표 **/
    public static final String QUESTION = "?";
    /** Y **/
    public static final String USE_Y = "Y";
    /** N **/
    public static final String USE_N = "N";
    /** 0 **/
    public static final String ZERO = "0";
    /** 1 **/
    public static final String ONE = "1";
}
