package kr.co.homeplus.admin.web.item.model.itemGroup;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true, indicator = true)
public class ItemGroupDetailListGetDto {

    @ApiModelProperty(value = "묶음번호")
    @RealGridColumnInfo(headText = "묶음번호", hidden = true)
    private Long groupNo;

    @ApiModelProperty(value = "대표")
    @RealGridColumnInfo(headText = "대표", width = 30)
    private String representYn;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 100, sortable = true)
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", width = 150)
    private String itemNm;

    @ApiModelProperty(value = "상품용도옵션")
    @RealGridColumnInfo(headText = "상품용도옵션", hidden = true)
    private String optSelUseYn;

    @ApiModelProperty(value = "상품용도옵션")
    @RealGridColumnInfo(headText = "상품용도옵션", width = 100)
    private String optSelUseYnNm;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", hidden = true)
    private String useYn;

    @ApiModelProperty(value = "1단계 옵션값")
    @RealGridColumnInfo(headText = "1단계 옵션값", width = 100, sortable = true)
    private String opt1Val;

    @ApiModelProperty(value = "2단계 옵션값")
    @RealGridColumnInfo(headText = "2단계 옵션값", width = 100, sortable = true)
    private String opt2Val;

    @ApiModelProperty(value = "parent ID")
    @RealGridColumnInfo(headText = "parent ID", width = 100, sortable = true)
    private String itemParent;

}
