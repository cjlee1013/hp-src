package kr.co.homeplus.admin.web.delivery.service;

import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_GET_STORE_ZIPCODE_List;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.delivery.model.StoreZipcodeModel;
import kr.co.homeplus.admin.web.delivery.model.StoreZipcodeSelectModel;
import kr.co.homeplus.admin.web.escrow.service.ZipcodeManageService;
import kr.co.homeplus.admin.web.item.model.store.StoreListParamDto;
import kr.co.homeplus.admin.web.item.model.store.StoreListSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StoreZipcodeService {

    private final ResourceClient resourceClient;
    private final ZipcodeManageService zipcodeManageService;

    /**
     * 시도명 리스트 조회
     * @return
     */
    public List<String> getSidoList() {
        return zipcodeManageService.getZipcodeSidoList();
    }

    /**
     * 점포별 우편번호 조회
     * @param storeZipcodeSelectModel
     * @return
     */
    public List<StoreZipcodeModel> getStoreZipcodeList(StoreZipcodeSelectModel storeZipcodeSelectModel) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("schStoreType", storeZipcodeSelectModel.getSchStoreType()));
        setParameterList.add(SetParameter.create("schStoreId", StringUtil.nvl(storeZipcodeSelectModel.getSchStoreId())));
        setParameterList.add(SetParameter.create("schRemoteShipYn", storeZipcodeSelectModel.getSchRemoteShipYn()));
        setParameterList.add(SetParameter.create("schZipcode", storeZipcodeSelectModel.getSchZipcode()));
        setParameterList.add(SetParameter.create("schSidoType", storeZipcodeSelectModel.getSchSidoType()));
        setParameterList.add(SetParameter.create("schSigunguType", storeZipcodeSelectModel.getSchSigunguType()));
        setParameterList.add(SetParameter.create("schAddrKeyword", storeZipcodeSelectModel.getSchAddrKeyword()));
        setParameterList.add(SetParameter.create("schBuildNo", storeZipcodeSelectModel.getSchBuildNo()));

        String apiUri = ESCROW_GET_STORE_ZIPCODE_List + StringUtil.getParameter(setParameterList);

        ResourceClientRequest<List<StoreZipcodeModel>> request = ResourceClientRequest.<List<StoreZipcodeModel>>getBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }
}
