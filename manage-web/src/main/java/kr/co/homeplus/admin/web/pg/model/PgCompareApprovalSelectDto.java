package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 대사 > PG 승인대사 조회 search DTO")
public class PgCompareApprovalSelectDto {
    private String schPgKind;
    private String schFromDt;
    private String schEndDt;
    private String schTradeTypeCd;
    private String schPaymentMethod;
    private String schDiffReasonCd;
}