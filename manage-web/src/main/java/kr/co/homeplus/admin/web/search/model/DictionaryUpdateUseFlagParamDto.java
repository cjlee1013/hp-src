package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "어휘속성 > 사용여부")
public class DictionaryUpdateUseFlagParamDto {
    @ApiModelProperty(notes = "단어번호", position = 1, required = true)
    private Integer wordId;
    @ApiModelProperty(notes = "사용여부", position = 2, required = true)
    private boolean flag;
}
