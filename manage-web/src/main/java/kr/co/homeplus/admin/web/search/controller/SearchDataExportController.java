package kr.co.homeplus.admin.web.search.controller;

import kr.co.homeplus.admin.web.search.model.SearchDataExportParamDto;
import kr.co.homeplus.admin.web.search.service.SearchDataExportService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/search/operation/searchDataExport")
@RequiredArgsConstructor
public class SearchDataExportController {
    private final SearchDataExportService searchDataExportService;

    @GetMapping(value = { "/main" })
    public String main(Model model) {
        return "/search/searchDataExport";
    }

    @GetMapping(value= {"/excel.json"})
    public String excel(Model model, SearchDataExportParamDto param) {
        model.addAttribute("excelBindInfo", searchDataExportService.getExcelData(param));
        return "excelDataDownloadView";
    }
}
