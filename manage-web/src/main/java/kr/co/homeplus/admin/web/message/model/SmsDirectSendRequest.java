package kr.co.homeplus.admin.web.message.model;

import java.util.List;
import lombok.Data;

@Data
public class SmsDirectSendRequest {

    private List<BasicRecipient> bodyArgument;

    private String subject;

    private String body;

    private String callback;

    private long workSeq;

    private String workName;

    private String description;

    private String regId;
}
