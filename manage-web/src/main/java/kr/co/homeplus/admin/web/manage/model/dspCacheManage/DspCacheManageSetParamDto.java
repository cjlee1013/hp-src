package kr.co.homeplus.admin.web.manage.model.dspCacheManage;

import lombok.Data;

@Data
public class DspCacheManageSetParamDto {
    private Long manageSeq;
    private String cacheUrl;
    private String cacheNm;
    private String useYn;
    private String latestDt;
    private String regId;
}
