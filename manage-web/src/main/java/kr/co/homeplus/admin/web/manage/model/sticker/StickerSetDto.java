package kr.co.homeplus.admin.web.manage.model.sticker;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StickerSetDto {

    //스티커번호
    private Long stickerNo;

    //스티커명
    private String stickerNm;

    //사용여부
    private String useYn;

    //등록/수정자
    private String userId;

    //이미지정보
    List<StickerImgSetDto> stickerImgList;
}
