package kr.co.homeplus.admin.web.item.model.item;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RsvSaleSetDto {
    private Long rsvSaleSeq;
    private String storeType;
    private String rsvSaleNm;
    private String saleType;
    private Character useYn;
    private String rsvSaleStartDt;
    private String rsvSaleEndDt;
    private String saleWay;
    private int hopeSendDays;

    private List<RsvSaleSendPlaceGetDto> sendPlaceList;

    private List<RsvSaleItemGetDto> createList;
    private List<RsvSaleItemGetDto> deleteList;
    private List<RsvSaleItemGetDto> noneList;

    private String regId;
}