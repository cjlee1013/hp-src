package kr.co.homeplus.admin.web.item.model.harmful;

import lombok.Data;
import java.util.List;

@Data
public class HarmfulItemSetDto {

    private String cdInsptmachi;

    private String noDocument;

    private String seq;

    private String statusResult;

    private List<String> itemNo;

    private String regId;
}
