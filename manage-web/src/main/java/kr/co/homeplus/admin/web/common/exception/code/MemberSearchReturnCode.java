package kr.co.homeplus.admin.web.common.exception.code;

import lombok.Getter;

/**
 * 어드민 회원조회 팝업 관련
 */
public enum MemberSearchReturnCode {
    WRONG_MOBILE_PATTERN("핸드폰 번호 형식이 아닙니다."),
    MEMBER_SEARCH_AT_LEAST_ONE_SEARCH_VALUE("최소 1개의 검색 값을 입력해주세요."),
    MEMBER_SEARCH_PARAMETER_AT_LEAST_TWO_LETTERS("두 글자 이상 입력해주세요.")
    ;

    @Getter
    private final String msg;

    MemberSearchReturnCode(String msg) {
        this.msg = msg;
    }
}
