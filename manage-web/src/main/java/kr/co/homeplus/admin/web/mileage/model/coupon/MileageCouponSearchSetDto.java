package kr.co.homeplus.admin.web.mileage.model.coupon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마일리지 난수번호 관리 조회 DTO")
public class MileageCouponSearchSetDto {
    @ApiModelProperty(notes = "시작일자", position = 1)
    private String schStartDt;
    @ApiModelProperty(notes = "종료일자", position = 2)
    private String schEndDt;
    @ApiModelProperty(notes = "난수종류", position = 3)
    private String schCouponType;
    @ApiModelProperty(notes = "사용여부", position = 4)
    private String schIssueYn;
    @ApiModelProperty(notes = "상세검색", position = 5)
    private String schDetailCode;
    @ApiModelProperty(notes = "상세검색내용", position = 6)
    private String schDetailDesc;
}
