package kr.co.homeplus.admin.web.settle.model.partnerPayment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PartnerPaymentStateSetDto {
    @ApiModelProperty(value = "지급SRL")
    private String settleSrl;

    @ApiModelProperty(value = "지급상태")
    private String settlePayState;

    @ApiModelProperty(value = "처리자")
    private String chgId;

    public void setChgId(String chgId) {
        this.chgId = chgId;
    }
}
