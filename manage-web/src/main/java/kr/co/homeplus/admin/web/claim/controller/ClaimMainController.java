package kr.co.homeplus.admin.web.claim.controller;


import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.claim.model.ClaimHistoryListDto;
import kr.co.homeplus.admin.web.claim.model.ClaimImageInfoDto;
import kr.co.homeplus.admin.web.claim.model.ClaimProcessListDto;
import kr.co.homeplus.admin.web.claim.model.ClaimReasonInfoGetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimDetailInfoGetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimDetailInfoListDto;
import kr.co.homeplus.admin.web.claim.model.ClaimDetailInfoSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimHistoryDto;
import kr.co.homeplus.admin.web.claim.model.ClaimOrderGetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimOrderSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimProcessDto;
import kr.co.homeplus.admin.web.claim.model.ClaimReasonInfoSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimRegisterPreRefundInfoDto;
import kr.co.homeplus.admin.web.claim.model.StoreInfoGetDto;
import kr.co.homeplus.admin.web.claim.service.ClaimMainService;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.privacylog.service.PrivacyLogService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/claim/info")
@RequiredArgsConstructor
public class ClaimMainController {

    @Value("${front.homeplus.url}")
    private String frontUrl;

    @Value("${front.theclub.url}")
    private String clubUrl;

    private final ClaimMainService claimMainService;
    private final CodeService codeService;
    private final LoginCookieService loginCookieService;

    /**
     * 클레임 Main Page
     */
    @GetMapping("/claimMain")
    public String claimMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type",                // 점포유형
            "claim_status",      // 클레임상태
            "claim_type",        // 클레임 타입
            "pickup_status",        // 수거상태
            "claim_date_type",    //검색기간
            "claim_search_type",   //검색어
            "exch_shipping_status"

        );
        model.addAttribute("storeType", code.get("store_type"));
        model.addAttribute("claimStatus", code.get("claim_status"));
        model.addAttribute("claimType", code.get("claim_type"));
        model.addAttribute("pickupStatus", code.get("pickup_status"));
        model.addAttribute("exchShippingStatus", code.get("exch_shipping_status"));
        model.addAttribute("claimDateType", code.get("claim_date_type"));
        model.addAttribute("claimSearchType", code.get("claim_search_type"));

        String storeId = loginCookieService.getUserInfo().getStoreId();
        if(StringUtils.isNotEmpty(storeId)){
            StoreInfoGetDto storeInfoGetDto = claimMainService.getStoreInfo(storeId);
            model.addAttribute("storeId", loginCookieService.getUserInfo().getStoreId());
            model.addAttribute("storeNm", storeInfoGetDto.getStoreNm());
            model.addAttribute("soStoreType", storeInfoGetDto.getStoreType());
        }

        model.addAllAttributes(RealGridHelper.create("claimListGridBaseInfo", ClaimOrderGetDto.class));

        return "/claim/claimMain";
    }

    /**
     * 클레임 상세 Tab Page
     */
    @GetMapping("/getClaimDetailInfoTab")
    public String getClaimDetailInfoTab(Model model, HttpServletRequest request) throws Exception {

        String url = "/claim";
        model.addAttribute("purchaseOrderNo", request.getParameter("purchaseOrderNo"));
        model.addAttribute("claimNo", request.getParameter("claimNo"));
        model.addAttribute("claimBundleNo", request.getParameter("claimBundleNo"));
        model.addAttribute("claimType", request.getParameter("claimType"));
        model.addAttribute("frontUrl", frontUrl);
        model.addAttribute("clubUrl", clubUrl);

        model.addAllAttributes(
            RealGridHelper.create("claimDetailInfoListGridBaseInfo", ClaimDetailInfoListDto.class));
        model.addAllAttributes(
            RealGridHelper.create("claimHistoryGridBaseInfo", ClaimHistoryDto.class));
        model.addAllAttributes(
            RealGridHelper.create("claimProcessGridBaseInfo", ClaimProcessDto.class));

        switch (request.getParameter("claimType")){
            case "C" :
                url += "/claimCancelDetail";
                break;
            case "R" :
                url += "/claimReturnDetail";
                break;
            case "X" :
                url += "/claimExchangeDetail";
                break;
        }
        return url;
    }

    /**
     * 클레임관리 > 클레임관리 >  클레임 리스트 조회
     *
     * @param claimOrderSetDto
     * @return String
     */
    @ResponseBody
    @PostMapping("/getClaimList.json")
    public List<ClaimOrderGetDto> getClaimList(@RequestBody ClaimOrderSetDto claimOrderSetDto) {
        return claimMainService.getClaimList(claimOrderSetDto);
    }

    /**
     * 클레임 상세내역 조회
     * @return ClaimDetailInfoGetDto
     */
    @PostMapping("/getClaimMainDetailInfo.json")
    @ResponseBody
    public ClaimDetailInfoGetDto getClaimDetailInfo(final HttpServletRequest request, @RequestBody ClaimDetailInfoSetDto claimDetailInfoSetDto) throws Exception {
        return claimMainService.getClaimDetailInfo(request, claimDetailInfoSetDto);
    }

    /**
     * 클레임 처리내역 리스트 조회
     * **/
    @GetMapping("/getClaimProcessList.json")
    @ResponseBody
    public List<ClaimProcessListDto> getClaimProcessList(@RequestParam(value = "claimNo") long claimNo, @RequestParam(value = "claimType") String claimType){
        return claimMainService.getClaimProcessList(claimNo, claimType);
    }

    /**
     * 클레임 히스토리 리스트 조회
     * **/
    @GetMapping("/getClaimHistoryList.json")
    @ResponseBody
    public List<ClaimHistoryListDto> getClaimHistoryList(@RequestParam(value = "claimNo") long claimNo){
        return claimMainService.getClaimHistoryList(claimNo);
    }

    /**
     * 클레임 상세 Page 주문상세 내역 list
     * @return List<ClaimDetailInfoListDto>
     */
    @PostMapping("/getClaimDetailInfoList.json")
    @ResponseBody
    public List<ClaimDetailInfoListDto> getClaimDetailInfoList(@RequestBody ClaimDetailInfoSetDto claimDetailInfoSetDto) {
        return claimMainService.getClaimDetailInfoList(claimDetailInfoSetDto);
    }

    /**
     * 클레임 상세 Page 클레임 사유 조회
     * @return CancelReasonInfoGetDto
     */
    @PostMapping("/getClaimReasonInfo.json")
    @ResponseBody
    public ClaimReasonInfoGetDto getCancelReasonInfo(@RequestBody ClaimReasonInfoSetDto claimReasonInfoSetDto) {
        return claimMainService.getClaimReasonInfo(claimReasonInfoSetDto);
    }

    /**
     *
     * **/
    @GetMapping("/getRegisterPreRefund.json")
    @ResponseBody
    public ClaimRegisterPreRefundInfoDto getRegisterPreRefund(@RequestParam(value = "claimNo") long claimNo){
        return claimMainService.getRegisterPreRefund(claimNo);
    }

    /**
     * 이미지 정보 조회
     * **/
    @GetMapping("/getClaimImageInfo.json")
    @ResponseBody
    public ClaimImageInfoDto getClaimImageInfo(@RequestParam(value = "fileId") String fileId){
        return claimMainService.getClaimImageInfo(fileId);
    }

}
