package kr.co.homeplus.admin.web.manage.model.favicon;

import lombok.Data;

@Data
public class FaviconImgSetParamDto {

    //파비콘 이미지 고유번호
    private Long imgNo;

    //파비콘 관리 번호
    private Long faviconNo;

    //이미지타입
    private String imgType;

    //이미지명
    private String imgNm;

    //이미지경로
    private String imgUrl;

    //사용여부
    private String useYn;

    //등록/수정자
    private String userId;

}
