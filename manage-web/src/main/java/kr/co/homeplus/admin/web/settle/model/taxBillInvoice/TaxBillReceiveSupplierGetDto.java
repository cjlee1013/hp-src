package kr.co.homeplus.admin.web.settle.model.taxBillInvoice;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TaxBillReceiveSupplierGetDto {
    @ApiModelProperty(value = "공급처")
    private String supplier;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "상호")
    private String partnerNm;

    @ApiModelProperty(value = "사업자번호")
    private String partnerNo;

    @ApiModelProperty(value = "대표자명")
    private String partnerOwner;

}