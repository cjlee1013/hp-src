package kr.co.homeplus.admin.web.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JsonInclude(value = Include.NON_EMPTY)
public class AuthorizedMenus implements Serializable {
    // 메뉴관련 정보
    @ApiModelProperty(notes = "메뉴고유번호")
    private int menuSeq;

    @ApiModelProperty(notes = "메뉴뎁스")
    private int menuDepth;

    // 상위부서
    @ApiModelProperty(notes = "상위메뉴 고유번호")
    private int parentsMenuSeq;

    @ApiModelProperty(notes = "메뉴순서")
    private int menuOrder;

    // 메뉴 출력 및 사용관련 정보
    @ApiModelProperty(notes = "메뉴 url")
    private String menuUrl;

    @ApiModelProperty(notes = "메뉴명")
    private String menuName;

    @ApiModelProperty(notes = "개인정보 포함여부")
    private String isPersonal;

    @ApiModelProperty(notes = "엑셀다운기능 여부")
    private String isExcel;

    // 메뉴 출력관련 정보
    @ApiModelProperty(notes = "소유한 RolesDto 고유번호")
    private String roleCd;

    @ApiModelProperty(notes = "메뉴접근 가능여부 ( y | n )")
    private String isHold;

    // 하위메뉴 리스트
    @ApiModelProperty(notes = "하위메뉴 리스트")
    private List<AuthorizedMenus> childMenuList;

    @ApiModelProperty(notes = "업무용 VDESK 접속 여부(y|n)")
    private String isAccessVdi;

    @ApiModelProperty(notes = "고객만족센터 접속 여부(y|n)")
    private String isAccessCustomerCenter;
}
