package kr.co.homeplus.admin.web.statistics.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DiscountStatistics {

    private String storeType;
    private String mallType;
    private long discountNo;
    private String discountNm;

    private String orderDt;
    private int orderCnt;
    private long payAmt;
    private long discountAmt;
    private int claimCnt;
    private long claimAmt;
    private long claimDiscountAmt;


}
