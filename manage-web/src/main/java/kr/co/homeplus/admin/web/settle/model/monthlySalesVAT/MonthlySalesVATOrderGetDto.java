package kr.co.homeplus.admin.web.settle.model.monthlySalesVAT;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MonthlySalesVATOrderGetDto {
    @ApiModelProperty(value = "매출일")
    @RealGridColumnInfo(headText = "매출일", sortable = true, width = 100)
    private String basicDt;

    @ApiModelProperty(value = "주문구분")
    @RealGridColumnInfo(headText = "주문구분", sortable = true, width = 100)
    private String gubun;

    @ApiModelProperty(value = "주문번호")
    @RealGridColumnInfo(headText = "주문번호", sortable = true, width = 100)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "배송번호")
    @RealGridColumnInfo(headText = "배송번호", sortable = true, width = 100)
    private long bundleNo;

    @ApiModelProperty(value = "상품주문번호")
    @RealGridColumnInfo(headText = "주문번호", sortable = true, width = 100)
    private long orderItemNo;

    @ApiModelProperty(value = "구분") //1:상품, 2:배송비
    @RealGridColumnInfo(headText = "구분", sortable = true, width = 100)
    private String orderType;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", sortable = true, width = 100)
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", sortable = true, width = 200)
    private String itemNm;

    @ApiModelProperty(value = "과면세")
    @RealGridColumnInfo(headText = "과면세", sortable = true, width = 100)
    private String taxYn;

    @ApiModelProperty(value = "상품금액")
    @RealGridColumnInfo(headText = "상품금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long itemSaleAmt;

    @ApiModelProperty(value = "주문수량")
    @RealGridColumnInfo(headText = "주문수량", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long completeQty;

    @ApiModelProperty(value = "주문금액")
    @RealGridColumnInfo(headText = "주문금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long completeAmt;

    @ApiModelProperty(value = "수수료율")
    @RealGridColumnInfo(headText = "수수료율", sortable = true, width = 100)
    private String feeRate;

    @ApiModelProperty(value = "판매수수료")
    @RealGridColumnInfo(headText = "판매수수료", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long saleAgencyFee;

    @ApiModelProperty(value = "배송비")
    @RealGridColumnInfo(headText = "배송비", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipAmt;

    @ApiModelProperty(value = "반품배송비")
    @RealGridColumnInfo(headText = "반품배송비", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimReturnShipAmt;

    @ApiModelProperty(value = "파트너부담 쿠폰할인금액")
    @RealGridColumnInfo(headText = "업체", sortable = true, width = 120, groupName="상품할인", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponSellerChargeAmt;

    @ApiModelProperty(value = "홈플러스부담 쿠폰할인금액")
    @RealGridColumnInfo(headText = "자사", sortable = true, width = 120, groupName="상품할인", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponHomeChargeAmt;

    @ApiModelProperty(value = "카드부담 쿠폰할인금액")
    @RealGridColumnInfo(headText = "카드사", sortable = true, width = 120, groupName="상품할인", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponCardChargeAmt;

    @ApiModelProperty(value = "홈플러스부담 카드할인금액")
    @RealGridColumnInfo(headText = "자사", sortable = true, width = 120, groupName="카드할인", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cardCouponHomeAmt;

    @ApiModelProperty(value = "카드부담 카드할인금액")
    @RealGridColumnInfo(headText = "카드사", sortable = true, width = 120, groupName="카드할인", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cardCouponCardAmt;

    @ApiModelProperty(value = "배송비할인")
    @RealGridColumnInfo(headText = "배송비할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipDiscountAmt;

    @ApiModelProperty(value = "임직원할인")
    @RealGridColumnInfo(headText = "임직원할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long empDiscountAmt;

    @ApiModelProperty(value = "장바구니할인")
    @RealGridColumnInfo(headText = "장바구니할인", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cartCouponAmt;

    @ApiModelProperty(value = "총결제금액")
    @RealGridColumnInfo(headText = "합계", groupName = "결제 금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long totalPaidAmt;

    @ApiModelProperty(value = "PG결제금액")
    @RealGridColumnInfo(headText = "PG", groupName = "결제 금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long pgAmt;

    @ApiModelProperty(value = "DGV금액")
    @RealGridColumnInfo(headText = "DGV", groupName = "결제 금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long dgvAmt;

    @ApiModelProperty(value = "MHC금액")
    @RealGridColumnInfo(headText = "MHC", groupName = "결제 금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long mhcAmt;

    @ApiModelProperty(value = "OCB금액")
    @RealGridColumnInfo(headText = "OCB", groupName = "결제 금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long ocbAmt;

    @ApiModelProperty(value = "마일리지금액")
    @RealGridColumnInfo(headText = "마일리지", groupName = "결제 금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long mileageAmt;

    @ApiModelProperty(value = "주문자명")
    @RealGridColumnInfo(headText = "주문자명", sortable = true, width = 100)
    private String userNm;

    @ApiModelProperty(value = "결제일")
    @RealGridColumnInfo(headText = "결제일", sortable = true, width = 100)
    private String paymentCompleteDt;

}
