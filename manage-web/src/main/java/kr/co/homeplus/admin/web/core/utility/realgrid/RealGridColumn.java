package kr.co.homeplus.admin.web.core.utility.realgrid;


import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Deprecated(forRemoval = true)
@Getter
@Setter
@Slf4j
public class RealGridColumn {
    private final String COMMA = ",";
    private String name;
    private String fieldName;
    private String fieldType; // "text", "bool", "number", "datetime"
    private String headerText;
    private RealGridColumnType realGridColumnType;
    private int width;
    private boolean visible;
    private boolean sortable;
    private boolean editable;
    private String zeroText; // fieldType 숫자형일 경우만 적용됨.
    private List<String> values;
    private List<String> labels;
    private RealGridButtonProperty buttonProperty;

    public RealGridColumn(String name, String fieldName, String headerText, RealGridColumnType realGridColumnType, int width, boolean visible, boolean sortable, String zeroText) {
        this.name = name;
        this.fieldName = fieldName;
        this.fieldType = this.getFieldType(realGridColumnType);
        this.headerText = headerText;
        this.realGridColumnType = realGridColumnType;
        this.visible = visible;
        this.width = width;
        this.sortable = sortable;
        this.zeroText = zeroText;
    }

    public RealGridColumn(String name, String fieldName, String headerText, RealGridColumnType realGridColumnType,  int width, boolean visible, boolean sortable, boolean editable) {
        this(name, fieldName, headerText, realGridColumnType, width, visible, sortable, "");
        this.editable = editable;
    }

    public RealGridColumn(String name, String fieldName, String headerText, RealGridColumnType realGridColumnType,  int width, boolean visible, boolean sortable) {
        this(name, fieldName, headerText, realGridColumnType, width, visible, sortable, "");
    }

    public RealGridColumn(String name, String fieldName, RealGridFieldType fieldType, String headerText, RealGridColumnType realGridColumnType, int width, boolean visible, boolean sortable, boolean editable, String zeroText) {
        this.name = name;
        this.fieldName = fieldName;
        this.fieldType = fieldType.getType();
        this.headerText = headerText;
        this.realGridColumnType = realGridColumnType;
        this.width = width;
        this.visible = visible;
        this.sortable = sortable;
        this.editable = editable;
        this.zeroText = zeroText.isEmpty() ? "" : zeroText;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"name\" : \"" + this.name + "\"").append(COMMA);
        sb.append("\"fieldName\" : \"" + this.fieldName + "\"").append(COMMA);
        sb.append("\"header\" : { \"text\": \"" + this.headerText + "\" }").append(COMMA);
        sb.append("\"styles\" : " + this.getStyleJson(this.realGridColumnType)).append(COMMA);
        sb.append("\"dynamicStyles\" : " + this.getDynamicStyle(this.realGridColumnType)).append(COMMA);
        sb.append("\"renderer\" : " + this.getRenderer(this.realGridColumnType)).append(COMMA);
        sb.append("\"visible\" : " + Boolean.valueOf(this.visible).toString()).append(COMMA);
        sb.append("\"sortable\" : " + Boolean.valueOf(this.sortable).toString()).append(COMMA);
        sb.append("\"editable\" : " + Boolean.valueOf(this.editable).toString()).append(COMMA);

        if(this.realGridColumnType == RealGridColumnType.BUTTON)
        {
            assert this.buttonProperty != null;
            sb.append("\"button\" : \""+this.buttonProperty.getButton().getValue()+"\"").append(COMMA);
            if(this.buttonProperty.getButton() == RealGridButtonType.POPUP)
                sb.append("\"popupMenu\" : \""+this.buttonProperty.getPopupMenu()+"\"").append(COMMA);
            sb.append("\"alwaysShowButton\" : \""+this.buttonProperty.isAlwaysShowButton()+"\"").append(COMMA);
        }

        if (StringUtils.isNotEmpty(this.zeroText)) {
            sb.append("\"zeroText\" : \"" + this.zeroText + "\"").append(COMMA);
        }

        sb.append("\"width\" : " + this.width);
        sb.append("}");

        return sb.toString();
    }

    public void setRealGridColumnType(RealGridColumnType realGridColumnType){
        this.realGridColumnType = realGridColumnType;
        this.fieldType = this.getFieldType(realGridColumnType);
    }

    private String getFieldType(RealGridColumnType realGridColumnType) {
        String result = "";

        switch (realGridColumnType) {
            case NONE:
            case BASIC:
            case NAME:
            case NUMBER_STR:
            case IMAGE:
            case BUTTON:
                result = "text";
                break;
            case NUMBER:
            case NUMBER_C:
            case NUMBER_CENTER:
            case SERIAL:
            case PRICE:
            case COMMISSION:
            case COMMISSION_C:
                result = "number";
                break;
            case DATE:
            case DATETIME:
                result = "datetime";
                break;
        }

        return result;
    }

    private String getStyleJson(RealGridColumnType realGridColumnType) {
        String result = "";

        switch (realGridColumnType) {
            case NONE:
                result = "{}";
                break;
            case BASIC:
            case BUTTON:
            case SERIAL:
                result = "{\"textAlignment\" : \"center\"}";
                break;
            case NAME:
                result = "{\"textAlignment\" : \"near\", \"paddingLeft\" : 5}";
                break;
            case NUMBER:
                result = "{\"textAlignment\" : \"far\", \"paddingRight\" : 5}";
                break;
            case NUMBER_C:
                result = "{\"textAlignment\" : \"far\", \"numberFormat\" : \"#,##0\"}";
                break;
            case NUMBER_CENTER:
                result = "{\"textAlignment\" : \"center\", \"numberFormat\" : \"#,##0\"}";
                break;
            case NUMBER_STR:
                result = "{\"textAlignment\" : \"far\", \"paddingRight\": 5}";
                break;
            case PRICE:
                result = "{\"textAlignment\" : \"far\", \"paddingRight\": 5, \"numberFormat\" : \"#,##0\", \"suffix\":\"원\"}";
                break;
            case COMMISSION:
                result = "{\"textAlignment\" : \"far\", \"paddingRight\": 5, \"numberFormat\" : \"#,##0.##\"}";
                break;
            case COMMISSION_C:
                result = "{\"textAlignment\" : \"center\", \"paddingRight\": 5, \"numberFormat\" : \"#,##0.##\"}";
                break;
            case DATE:
                result = "{\"textAlignment\" : \"center\", \"datetimeFormat\" : \"yyyy-MM-dd\"}";
                break;
            case DATETIME:
                result = "{\"textAlignment\" : \"center\", \"datetimeFormat\" : \"yyyy-MM-dd HH:mm:ss\"}";
                break;
            case IMAGE:
                result = "{\"contentFit\" : \"auto\"}";
                break;
        }

        return result;
    }

    private String getDynamicStyle(RealGridColumnType realGridColumnType) {
        String result = "";

        switch (realGridColumnType) {
            case NUMBER:
                result = !StringUtils.isEmpty(zeroText) ? "[{\"criteria\" : \"value = 0\", \"styles\": \"textAlignment=center\"}]" : "[{}]";
                break;
            case COMMISSION:
            case COMMISSION_C:
                result = "[{\"criteria\" : \"value != 0\", \"styles\": \"suffix=%\"}]";
                break;
            default:
                result = "[{}]";
                break;
        }

        return result;
    }

    private String getRenderer(RealGridColumnType realGridColumnType) {
        String result = "";

        switch (realGridColumnType) {
            case IMAGE:
                result = "{\"type\" : \"image\"}";
                break;
            default:
                result = "[{}]";
                break;
        }

        return result;
    }
}
