package kr.co.homeplus.admin.web.item.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.GridUtil;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.brand.BrandListParamDto;
import kr.co.homeplus.admin.web.item.model.brand.BrandListSelectDto;
import kr.co.homeplus.admin.web.item.model.expCategory.ExpCategoryGetDto;
import kr.co.homeplus.admin.web.item.model.expCategory.ExpCategorySetParamDto;
import kr.co.homeplus.admin.web.item.model.expCategory.ExpRmsCategoryMappingGetDto;
import kr.co.homeplus.admin.web.item.model.expCategory.ExpRmsCategorySetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item/expCategory")
public class ExpCategoryController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    /**
     * 사이트관리 > 전문관관리 > EXPRESS 샵 카테고리 관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/expCategoryMain", method = RequestMethod.GET)
    public String expCategoryMain(Model model) {

        codeService.getCodeModel(model
            , "use_yn"				        // 사용여부
            , "category_disp_yn"			// 노출범위
        );

        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);

        model.addAllAttributes(RealGridHelper.create("expCategoryGridBaseInfo", ExpCategoryGetDto.class));
        model.addAllAttributes(RealGridHelper.create("expRmsCategoryMappingGridBaseInfo", ExpRmsCategoryMappingGetDto.class));

        return "/item/expCategoryMain";
    }

    /**
     * 사이트관리 > 전문관관리 > EXPRESS 샵 카테고리 리스트
     *
     * @param lCateCd
     * @return Object
     */
    @ResponseBody
    @RequestMapping(value = {"/getExpCategory.json"}, method = RequestMethod.GET)
    public List<Object> getExpCategory(@RequestParam(value = "lCateCd", required = false) String lCateCd
        , @RequestParam(value = "searchUseYn", required = false) String searchUseYn
        , @RequestParam(value = "itemDisplayYn", required = false) String itemDisplayYn) throws Exception {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("lCateCd", lCateCd));
        getParameterList.add(SetParameter.create("searchUseYn", searchUseYn));
        getParameterList.add(SetParameter.create("itemDisplayYn", itemDisplayYn));

        String apiUriRequest = "/item/expCategory/getExpCategory" + StringUtil.getParameter(getParameterList);

        return GridUtil.convertTreeGridData(
            resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<Map<String, Object>>>>() {}).getData()
            , null
            , "cateCd"
            , "parentCateCd"
            , "depth");
    }

    /**
     * 사이트관리 > 전문관관리 > EXPRESS 샵 카테고리 리스트 ( 전체노출 )
     *
     * @param depth
     * @param parentCateCd
     * @return Object
     */
    @ResponseBody
    @RequestMapping(value = {"/getExpCategoryByDisplay.json"}, method = RequestMethod.GET)
    public List<Object> getExpCategoryByDisplay(@RequestParam(value = "depth", required = false) String depth, @RequestParam(value = "parentCateCd", required = false) String parentCateCd) throws Exception {
        String apiUri = "/item/expCategory/getExpCategoryByDisplay?lCateCd=" + parentCateCd;

        if( "1".equals(depth) ){
            return GridUtil.convertTreeGridData(resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<Map<String, Object>>>>() {}).getData(), null, "cateCd", "parentCateCd", "depth");
        }else{
            List<Map<String, Object>> reseponse = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<Map<String, Object>>>>() {}).getData();
            List<Object> treeList   = new ArrayList();

            for( Map<String, Object> val : reseponse ){
                if(val.get("depth").equals(depth)){
                    treeList.add(val);
                }
            }

            return treeList;
        }
    }

    /**
     * 사이트관리 > 전문관관리 > EXPRESS RMS 카테고리 중복 체크
     *
     * @param expRmsCategorySetParamDto
     * @return ResponseResult
     */
    @ResponseBody @GetMapping(value = {"/validationRmsCategory.json"})
    public ResponseResult validationRmsCategory(@Valid ExpRmsCategorySetParamDto expRmsCategorySetParamDto) {
        String apiUri = "/item/expCategory/validationRmsCategory";
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, expRmsCategorySetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 사이트관리 > 전문관관리 > EXPRESS 샵 카테고리 등록/수정
     *
     * @param expCategorySetParamDto
     * @return ResponseResult
     */
    @ResponseBody @PostMapping(value = {"/setExpCategory.json"})
    public ResponseResult setExpCategory(@Valid @RequestBody ExpCategorySetParamDto expCategorySetParamDto) {
        String apiUri = "/item/expCategory/setExpCategory";
        expCategorySetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, expCategorySetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 사이트관리 > 전문관관리 > EXPRESS 샵 카테고리 리스트 ( RMS )
     *
     * @param mcateCd
     * @return Object
     */
    @ResponseBody
    @RequestMapping(value = {"/getExpRmsCategory.json"}, method = RequestMethod.GET)
    public List<ExpRmsCategoryMappingGetDto> getExpRmsCategory(@RequestParam(value = "mcateCd", required = false) String mcateCd) {
        String apiUri = "/item/expCategory/getExpRmsCategory?mcateCd=" + mcateCd;
        return (List<ExpRmsCategoryMappingGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri).getData();
    }

}
