package kr.co.homeplus.admin.web.item.model.itemRelation;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemRelationSetDto {

    //연관상품관리번호
    private Long relationNo;

    //연관명
    private String relationNm;

    //노출여부
    private String dispYn;

    //PC노출기준
    private String dispPcType;

    //모바일노출기준
    private String dispMobileType;

    //소분류 카테고리
    private String scateCd;

    //판매업체ID
    private String partnerId;

    //인트로이미지URL
    private String imgUrl;

    //인트로이미지명
    private String imgNm;

    //인트로이미지 사용여부
    private String imgUseYn;

    //설정여부
    private String useYn;

    //등록/수정자
    private String userId;

    //연관상품 리스트
    List<String> relationItemList;

}
