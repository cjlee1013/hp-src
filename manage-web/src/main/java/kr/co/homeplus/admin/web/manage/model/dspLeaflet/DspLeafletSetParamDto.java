package kr.co.homeplus.admin.web.manage.model.dspLeaflet;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspLeafletSetParamDto {

    private Long leafletNo;
    private String siteType;
    private String martLeafletNm;
    private String dispStartDt;
    private String dispEndDt;
    private String regId;

    @JsonProperty("itemCategoryList")
    private List<DspLeafletItemCategorySetParamDto> itemCategoryList;

    @JsonProperty("itemList")
    private List<DspLeafletItemInfoSetParamDto> itemList;

}
