package kr.co.homeplus.admin.web.user.model.user;

import kr.co.homeplus.admin.web.user.enums.UserShippingAddress;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class ShippingAddressDto {
    @RealGridColumnInfo(headText = "배송지 관리 일련번호", hidden = true)
    private long saSeq;

    @RealGridColumnInfo(headText = "회원 일련번호", hidden = true)
    private Long userNo;

    @RealGridColumnInfo(headText = "배송지명")
    private String shippingNm;

    @RealGridColumnInfo(headText = "받는사람")
    private String receiver;

    @RealGridColumnInfo(headText = "휴대폰")
    private String mobile;

    @RealGridColumnInfo(headText = "우편번호")
    private String zipcode;

    @RealGridColumnInfo(headText = "주소")
    private String addr1;

    @RealGridColumnInfo(headText = "상세주소")
    private String addr2;

    @RealGridColumnInfo(headText = "비고")
    private int isBasic;

    @RealGridColumnInfo(headText = "지번주소", hidden = true)
    private String parcelAddr1;

    @RealGridColumnInfo(headText = "지번주소 상세", hidden = true)
    private String parcelAddr2;


    public String getIsBasic() {
        return UserShippingAddress
            .getUserShippingAddressPolicy(this.isBasic)
            .getDesc();
    }
}
