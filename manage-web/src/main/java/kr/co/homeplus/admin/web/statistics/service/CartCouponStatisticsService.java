package kr.co.homeplus.admin.web.statistics.service;

import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.statistics.model.CartCouponStatisticsDailyDto;
import kr.co.homeplus.admin.web.statistics.model.CartCouponStatisticsDto;
import kr.co.homeplus.admin.web.statistics.model.CartCouponStatisticsSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CartCouponStatisticsService {

    private final ResourceClient resourceClient;

    /**
     * 장바구니쿠폰통계(점포별) 조회
     * @param cartCouponStatisticsSelectDto
     * @return
     */
    public List<CartCouponStatisticsDto> getCartCouponStatistics(CartCouponStatisticsSelectDto cartCouponStatisticsSelectDto) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("schFromDt", cartCouponStatisticsSelectDto.getSchFromDt()));
        setParameterList.add(SetParameter.create("schEndDt", cartCouponStatisticsSelectDto.getSchEndDt()));
        setParameterList.add(SetParameter.create("schStoreType", cartCouponStatisticsSelectDto.getSchStoreType()));
        setParameterList.add(SetParameter.create("schCouponKind", cartCouponStatisticsSelectDto.getSchCouponKind()));

        String apiUri = EscrowConstants.STATISTICS_GET_CART_COUPON_STATISTICS + StringUtil.getParameter(setParameterList);

        List<CartCouponStatisticsDto> cartCouponStatisticsList = resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, new ParameterizedTypeReference<ResponseObject<List<CartCouponStatisticsDto>>>() {}).getData();

        cartCouponStatisticsList.stream().forEach(cartCouponStatisticsDto -> {
            cartCouponStatisticsDto.setSchFromDt(cartCouponStatisticsSelectDto.getSchFromDt());
            cartCouponStatisticsDto.setSchEndDt(cartCouponStatisticsSelectDto.getSchEndDt());
            cartCouponStatisticsDto.setSchCouponKind(cartCouponStatisticsSelectDto.getSchCouponKind());
        });

        return cartCouponStatisticsList;
    }

    /**
     * 장바구니쿠폰통계(일자별) 조회
     * @param cartCouponStatisticsSelectDto
     * @return
     */
    public List<CartCouponStatisticsDailyDto> getCartCouponStatisticsByStoreId(CartCouponStatisticsSelectDto cartCouponStatisticsSelectDto) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("schFromDt", cartCouponStatisticsSelectDto.getSchFromDt()));
        setParameterList.add(SetParameter.create("schEndDt", cartCouponStatisticsSelectDto.getSchEndDt()));
        setParameterList.add(SetParameter.create("schStoreType", cartCouponStatisticsSelectDto.getSchStoreType()));
        setParameterList.add(SetParameter.create("schCouponKind", cartCouponStatisticsSelectDto.getSchCouponKind()));
        setParameterList.add(SetParameter.create("schStoreId", cartCouponStatisticsSelectDto.getSchStoreId()));

        String apiUri = EscrowConstants.STATISTICS_GET_CART_COUPON_STATISTICS_BY_STOREID + StringUtil.getParameter(setParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, new ParameterizedTypeReference<ResponseObject<List<CartCouponStatisticsDailyDto>>>() {}).getData();
    }
}
