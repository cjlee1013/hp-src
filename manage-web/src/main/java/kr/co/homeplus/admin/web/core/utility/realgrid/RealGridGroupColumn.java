package kr.co.homeplus.admin.web.core.utility.realgrid;

import lombok.Getter;
import lombok.Setter;

@Deprecated(forRemoval = true)
@Setter
@Getter
public class RealGridGroupColumn extends RealGridColumn {

    private String groupName;

    public RealGridGroupColumn(String name, String fieldName, String headerText, RealGridColumnType realGridColumnType, int width, boolean visible, boolean sortable, String groupName) {
        super(name, fieldName, headerText, realGridColumnType, width, visible, sortable);
        this.groupName = groupName;
    }
}
