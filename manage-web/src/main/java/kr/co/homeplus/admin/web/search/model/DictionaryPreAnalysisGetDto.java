package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DictionaryPreAnalysisGetDto {
    private PreAnalysisAttrDto attr;
    private DictionaryComponentGetDto<PreAnalysisDto> list = new DictionaryComponentGetDto<>();
}
