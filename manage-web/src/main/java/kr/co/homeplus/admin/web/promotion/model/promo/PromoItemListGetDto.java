package kr.co.homeplus.admin.web.promotion.model.promo;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PromoItemListGetDto {
    // 분류 번호
    private Long themeNo;

    // 상품 분류 임시 번호
    private String tempThemeNo;

    // 상품 내역
    private List<PromoItemGetDto> itemList;
}