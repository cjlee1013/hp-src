package kr.co.homeplus.admin.web.search.service;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.search.model.AnalysisPluginGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequestRaw;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AnalysisService {

    private final ResourceClient resourceClient;

    public AnalysisPluginGetDto getAnalysis(String keyword, String mode, String service) {
        String apiUri = "/analysis/plugin?keyword="+ URLEncoder.encode(keyword, StandardCharsets.UTF_8)+"&analysisMode="+mode+"&service=" + service;
        ResourceClientRequestRaw<AnalysisPluginGetDto> request = ResourceClientRequestRaw.<AnalysisPluginGetDto>getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request,new TimeoutConfig()).getBody();
    }
}
