package kr.co.homeplus.admin.web.app.controller;

import kr.co.homeplus.admin.web.app.model.AppStoreReviewControlInfo;
import kr.co.homeplus.admin.web.app.model.AppStoreReviewControlParam;
import kr.co.homeplus.admin.web.app.service.AppStoreReviewControlService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * iOS 앱 심사 관리
 */
@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping(value = "/app/mng/appstore/review")
public class AppStoreReviewControlController {

    private final AppStoreReviewControlService appStoreReviewControlService;

    @GetMapping(value = "/main")
    public String appleReviewControlMain() {
        return "/app/appStoreReviewControlMain";
    }

    @ResponseBody
    @GetMapping(value = "/getAppStoreReviewControlData.json", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<AppStoreReviewControlInfo> getAppStoreReviewControlInfo(@RequestParam("seq") int seq) {
        return ResourceConverter.toResponseObject(appStoreReviewControlService.getAppStoreReviewControlInfo(seq));
    }

    @ResponseBody
    @PostMapping(value = "/setAppStoreReviewControlData.json", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> setAppStoreReviewControlData(@RequestBody AppStoreReviewControlParam appStoreReviewControlParam) {
        return ResourceConverter.toResponseObject(appStoreReviewControlService.setAppStoreReviewControl(appStoreReviewControlParam));
    }
}
