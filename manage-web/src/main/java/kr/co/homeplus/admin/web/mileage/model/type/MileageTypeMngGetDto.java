package kr.co.homeplus.admin.web.mileage.model.type;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MileageTypeMngGetDto {

    @ApiModelProperty(value = "마일리지코드", position = 1)
    @RealGridColumnInfo(headText = "마일리지코드", width = 100)
    private String mileageCode;

    @ApiModelProperty(value = "마일리지번호", position = 2)
    @RealGridColumnInfo(headText = "마일리지번호", width = 100)
    private String mileageTypeNo;

    @ApiModelProperty(value = "점포유형", position = 3)
    @RealGridColumnInfo(headText = "점포유형", width = 100)
    private String storeType;

    @ApiModelProperty(value = "마일리지유형", position = 4)
    @RealGridColumnInfo(headText = "마일리지유형", width = 200)
    private String mileageTypeName;

    @ApiModelProperty(value = "마일리지종류", position = 5)
    @RealGridColumnInfo(headText = "마일리지 종류", width = 200)
    private String mileageKind;

    @ApiModelProperty(value = "사용", position = 6)
    @RealGridColumnInfo(headText = "사용", width = 80)
    private String useYn;

    @ApiModelProperty(value = "등록아이디", position = 7)
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regId;

    @ApiModelProperty(value = "등록일", position = 8)
    @RealGridColumnInfo(headText = "등록일", width = 120)
    private String regDt;

    @ApiModelProperty(value = "수정아이디", position = 9)
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgId;

    @ApiModelProperty(value = "수정일", position = 10)
    @RealGridColumnInfo(headText = "수정일", width = 120)
    private String chgDt;
}
