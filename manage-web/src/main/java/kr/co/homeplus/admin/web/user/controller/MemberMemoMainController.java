package kr.co.homeplus.admin.web.user.controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.privacylog.model.PersonalLogMethod;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyHeaderKey;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyLogInfo;
import kr.co.homeplus.admin.web.core.privacylog.service.PrivacyLogService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.user.model.memberMemo.MemberMemo;
import kr.co.homeplus.admin.web.user.model.memberMemo.MemberMemoAdd;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.util.ServletUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 회원메모관리 메뉴
 */
@Slf4j
@Controller
@RequestMapping("/user/memberMemo")
@RequiredArgsConstructor
public class MemberMemoMainController {

    private final ResourceClient resourceClient;
    private final LoginCookieService cookieService;
    private final PrivacyLogService privacyLogService;

    //grid
    private final Map<String, Object> memberMemoGridBaseInfo = RealGridHelper
        .create("memberMemoGridBaseInfo", MemberMemo.class);

    /**
     * 회원메모관리 Main Page
     */
    @GetMapping(value = "/main")
    public String memberMemoMainController(final Model model) {
        model.addAllAttributes(memberMemoGridBaseInfo);
        return "/user/memberMemoMain";
    }

    /**
     * 회원메모관리 목록 조회(검색)
     */
    @ResponseBody
    @GetMapping(value = "/getMemberMemoList.json")
    public ResponseObject<List<MemberMemo>> getMemberMemoList(final HttpServletRequest request,
        @RequestParam final Long userNo) {

        final String privacyAccessIp = ServletUtils.clientIP(request);
        final String privacyAccessUserId = cookieService.getUserInfo().getUserId();

        final PrivacyLogInfo logInfo = PrivacyLogInfo.builder()
            .txTime(DateTimeUtil.getNowMillYmdHis())
            .accessIp(privacyAccessIp)
            .userId(privacyAccessUserId)
            .txCodeUrl(request.getRequestURI())
            .txMethod(PersonalLogMethod.SELECT.name())
            .txCodeName("회원메모관리 목록조회")
            .processor(String.valueOf(userNo))
            .processorTaskSql("")
            .build();

        privacyLogService.send(logInfo);

        final HttpHeaders headers = new HttpHeaders();
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_IP, privacyAccessIp);
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_USER_ID, privacyAccessUserId);

        return resourceClient.getForResponseObject(
            ResourceRouteName.USERMNG, "/admin/memberMemo/getMemberMemoList?userNo=" + userNo,
            headers, new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 회원메모관리 상세 조회
     */
    @Validated
    @ResponseBody
    @GetMapping(value = "/getMemberMemo.json")
    public ResponseObject<MemberMemo> getMemberMemo(@RequestParam @NotNull final Long seq) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.USERMNG,
            "/admin/memberMemo/getMemberMemo?seq=" + seq,
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 회원메모관리 등록
     */
    @ResponseBody
    @PostMapping(value = "/addMemberMemo.json")
    public ResponseObject<Boolean> addMemberMemo(@RequestBody @Valid final MemberMemoAdd param) {
        param.setRegEmpId(cookieService.getUserInfo().getEmpId());
        param.setRegEmpNm(cookieService.getUserInfo().getUserNm());

        return resourceClient.postForResponseObject(
            ResourceRouteName.USERMNG,
            param,
            "/admin/memberMemo/addMemberMemo",
            new ParameterizedTypeReference<>() {
            });
    }
}
