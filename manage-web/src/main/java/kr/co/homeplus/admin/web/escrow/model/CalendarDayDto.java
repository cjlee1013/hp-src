package kr.co.homeplus.admin.web.escrow.model;

import lombok.Data;

/**
 * 캘린더 출력 할 날짜 Dto (일단위)
 */
@Data
public class CalendarDayDto {
    // 일 (1~29|30|31)
    Integer day;
    // 요일 (1~7)
    Integer dayOfWeek;
    // 일자 (YYYY-MM-DD)
    String date;

    /**
     * 요일을 1~7 로 정의하기 위해 setDayOfWeek() 재정의
     * @param dayOfWeek
     */
    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek%7 == 0 ? 7 : dayOfWeek%7;
    }
}