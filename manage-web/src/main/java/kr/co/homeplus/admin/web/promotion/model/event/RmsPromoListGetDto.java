package kr.co.homeplus.admin.web.promotion.model.event;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class RmsPromoListGetDto {
    @RealGridColumnInfo(headText = "행사번호", sortable = true)
    private String promoDisplayId;

    @RealGridColumnInfo(headText = "행사구성번호", sortable = true)
    private String compDisplayId;

    @RealGridColumnInfo(headText = "행사상세번호", sortable = true)
    private Long rpmPromoCompDetailId;

    @RealGridColumnInfo(headText = "행사명", width = 200)
    private String name;

    @RealGridColumnInfo(headText = "행사종류")
    private String eventKind;

    @RealGridColumnInfo(headText = "최소구매")
    private String purchaseMin;

    @RealGridColumnInfo(headText = "할인유형")
    private String changeType;

    @RealGridColumnInfo(headText = "할인액(율)")
    private String discount;

    @RealGridColumnInfo(headText = "시작일시", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String startDate;

    @RealGridColumnInfo(headText = "종료일시", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String endDate;

    @RealGridColumnInfo(headText = "행사상태")
    private String state;

    @RealGridColumnInfo(headText = "thresholdId", hidden = true)
    private Integer thresholdId;

    @RealGridColumnInfo(headText = "온라인 사용여부")
    private String useYn;

    @RealGridColumnInfo(headText = "이중가격표기", sortable = true)
    private String xoutYn;

    @RealGridColumnInfo(headText = "등록자")
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자")
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String chgDt;
}