package kr.co.homeplus.admin.web.delivery.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.delivery.model.ReserveShipPlaceManageModel;
import kr.co.homeplus.admin.web.delivery.model.ReserveShipPlaceManageSelectModel;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ReserveShipPlaceManageService {
    private final ResourceClient resourceClient;

    /**
     * 선물세트 발송지 리스트 조회
     * @param reserveShipPlaceManageSelectModel
     * @return
     * @throws Exception
     */
    public List<ReserveShipPlaceManageModel> getReserveShipPlaceMngList(ReserveShipPlaceManageSelectModel reserveShipPlaceManageSelectModel) throws Exception {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_RESERVE_SHIP_PLACE_MNG_LIST, ReserveShipPlaceManageSelectModel.class, reserveShipPlaceManageSelectModel);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ReserveShipPlaceManageModel>>>() {};
        return (List<ReserveShipPlaceManageModel>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 선물세트 발송지 저장
     * @param reserveShipPlaceManageModel
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> saveReserveShipPlace(ReserveShipPlaceManageModel reserveShipPlaceManageModel) throws Exception {
        String apiUri = EscrowConstants.ESCROW_SAVE_RESERVE_SHIP_PLACE;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, reserveShipPlaceManageModel, apiUri, typeReference);
    }

    /**
     * 선물세트 발송지 리스트 저장 (다중건)
     * @param reserveShipPlaceManageModelList
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> saveReserveShipPlaceList(List<ReserveShipPlaceManageModel> reserveShipPlaceManageModelList) throws Exception {
        String apiUri = EscrowConstants.ESCROW_SAVE_RESERVE_SHIP_PLACE_LIST;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, reserveShipPlaceManageModelList, apiUri, typeReference);
    }

    /**
     * 선물세트 발송지 리스트 삭제
     * @param reserveShipPlaceManageModelList
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> deleteReserveShipPlaceList(List<ReserveShipPlaceManageModel> reserveShipPlaceManageModelList) throws Exception {
        String apiUri = EscrowConstants.ESCROW_DELETE_RESERVE_SHIP_PLACE_LIST;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, reserveShipPlaceManageModelList, apiUri, typeReference);
    }
}
