package kr.co.homeplus.admin.web.item.model.expCategory;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
@Getter
@Setter
public class ExpRmsCategoryMappingGetDto {

    @RealGridColumnInfo(headText = "1depth(DIVISION)")
    private String division;

    @RealGridColumnInfo(headText = "카테고리명", columnType = RealGridColumnType.NAME, width = 200, order = 1)
    private String divisionNm;

    @RealGridColumnInfo(headText = "2depth(GROUP)", order = 2)
    private String groupNo;

    @RealGridColumnInfo(headText = "카테고리명", columnType = RealGridColumnType.NAME, width = 200, order = 3)
    private String groupNoNm;

    @RealGridColumnInfo(headText = "3depth(SELECTION)", order = 4)
    private String dept;

    @RealGridColumnInfo(headText = "카테고리명", columnType = RealGridColumnType.NAME, width = 200, order = 5)
    private String deptNm;

    @RealGridColumnInfo(headText = "4depth(CLASS)", order = 6)
    private String classCd;

    @RealGridColumnInfo(headText = "카테고리명", columnType = RealGridColumnType.NAME, width = 200, order = 7)
    private String classCdNm;

    @RealGridColumnInfo(headText = "5depth(SUBCLASS)", order = 8)
    private String subclass;

    @RealGridColumnInfo(headText = "카테고리명", columnType = RealGridColumnType.NAME, width = 200, order = 9)
    private String subclassNm;

}
