package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단관리 > 결제정책관리 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class PaymentPolicyManageDto {
    @ApiModelProperty(value = "정책번호")
    @RealGridColumnInfo(headText = "관리번호", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Long policyNo;

    @ApiModelProperty(value = "사이트유형(HOME:홈플러스,CLUB:더클럽)")
    @RealGridColumnInfo(headText = "사이트", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String siteType;

    @ApiModelProperty(value = "정책명")
    @RealGridColumnInfo(headText = "관리명", width = 200, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String policyNm;

    @ApiModelProperty(value = "적용대상(상품번호:ITEM,카테고리:CATE,제휴:AFFI)")
    @RealGridColumnInfo(headText = "적용대상", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String applyTarget;

    @ApiModelProperty(value = "우선순위")
    @RealGridColumnInfo(headText = "우선순위", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Integer priority;

    @ApiModelProperty(value = "pg상점설정(N:해당없음,C:PG카드할인,P:PG배분)")
    @RealGridColumnInfo(headText = "PG상점설정", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String pgStoreSet;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String useYn;

    @ApiModelProperty(value = "적용시작일자")
    @RealGridColumnInfo(headText = "적용시작일", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String applyStartDt;

    @ApiModelProperty(value = "적용종료일자")
    @RealGridColumnInfo(headText = "적용종료일", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String applyEndDt;

    @ApiModelProperty(value = "등록자id")
    @RealGridColumnInfo(headText = "등록자", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regId;

    @ApiModelProperty(value = "등록일자")
    @RealGridColumnInfo(headText = "등록일", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regDt;

    @ApiModelProperty(value = "수정자id")
    @RealGridColumnInfo(headText = "수정자", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgId;

    @ApiModelProperty(value = "수정일자")
    @RealGridColumnInfo(headText = "수정일", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgDt;

    @ApiModelProperty(value = "결제수단안내여부")
    @RealGridColumnInfo(headText = "paymentMethodGuideYn", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String paymentMethodGuideYn;

    @ApiModelProperty(value = "포인트사용여부")
    @RealGridColumnInfo(headText = "pointUseYn", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String pointUseYn;

    @ApiModelProperty(value = "현금영수증사용여부")
    @RealGridColumnInfo(headText = "cashReceiptUseYn", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String cashReceiptUseYn;

    @ApiModelProperty(value = "카드할부사용여부")
    @RealGridColumnInfo(headText = "cardInstallmentUseYn", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String cardInstallmentUseYn;

    @ApiModelProperty(value = "무이자사용여부")
    @RealGridColumnInfo(headText = "freeInterestUseYn", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String freeInterestUseYn;

    private List<PaymentPolicyMethodDto> paymentPolicyMethodDtoList;
    private List<PgDivideManageDto> pgDivideManageDtoList;
}