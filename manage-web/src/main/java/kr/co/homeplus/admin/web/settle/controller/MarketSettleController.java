package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import kr.co.homeplus.admin.web.settle.model.market.MarketCompareListGetDto;
import kr.co.homeplus.admin.web.settle.model.market.MarketCompareSetDto;
import kr.co.homeplus.admin.web.settle.model.market.MarketCompareDailyGetDto;
import kr.co.homeplus.admin.web.settle.model.market.MarketCompareSumGetDto;
import kr.co.homeplus.admin.web.settle.model.market.MarketSettleListGetDto;
import kr.co.homeplus.admin.web.settle.model.market.MarketSettleListSetDto;
import kr.co.homeplus.admin.web.settle.service.MarketSettleService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/settle/market")
public class MarketSettleController {
    private final SettleCommonService settleCommonService;
    private final MarketSettleService marketSettleService;

    @RequestMapping(value = "/settleList", method = RequestMethod.GET)
    public String settleList(Model model) throws Exception {
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));

        model.addAllAttributes(RealGridHelper.create("marketSettleListBaseInfo", MarketSettleListGetDto.class));
        return "/settle/marketSettleList";
    }

    @ResponseBody
    @RequestMapping(value = "/getSettleList.json", method = RequestMethod.GET)
    public List<MarketSettleListGetDto> getSettleList(MarketSettleListSetDto marketSettleListSetDto) throws Exception {
        return marketSettleService.getMarketSettleList(marketSettleListSetDto);
    }

    @RequestMapping(value = "/compareList", method = RequestMethod.GET)
    public String compareList(Model model) throws Exception {
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));

        model.addAllAttributes(RealGridHelper.createForGroup("marketCompareSumBaseInfo", MarketCompareSumGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("marketCompareDailyBaseInfo", MarketCompareDailyGetDto.class));
        model.addAllAttributes(RealGridHelper.create("marketCompareListBaseInfo", MarketCompareListGetDto.class));
        return "/settle/marketCompareList";
    }

    @ResponseBody
    @RequestMapping(value = "/getCompareSum.json", method = RequestMethod.GET)
    public List<MarketCompareDailyGetDto> getCompareSum(MarketCompareSetDto marketCompareSetDto) throws Exception {
        return marketSettleService.getMarketCompareSum(marketCompareSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getCompareList.json", method = RequestMethod.GET)
    public List<MarketCompareListGetDto> getCompareList(MarketCompareSetDto marketCompareSetDto) throws Exception {
        return marketSettleService.getMarketCompareList(marketCompareSetDto);
    }
}
