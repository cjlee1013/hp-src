package kr.co.homeplus.admin.web.item.controller;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.brand.BrandListParamDto;
import kr.co.homeplus.admin.web.item.model.brand.BrandListSelectDto;
import kr.co.homeplus.admin.web.item.model.brand.BrandSelectDto;
import kr.co.homeplus.admin.web.item.model.brand.BrandSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/brand")
public class BrandController {

    final private ResourceClient resourceClient;

    final private LoginCookieService cookieService;

    final private CodeService codeService;

    public BrandController(ResourceClient resourceClient,
            LoginCookieService cookieService,
            CodeService codeService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;

    }

    /**
     * 상품관리 > 상품관리 > 브랜드관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/brandMain", method = RequestMethod.GET)
    public String brandMain(Model model) {

        codeService.getCodeModel(model,
                "use_yn"           // 사용여부
                ,	"brand_item_disp_yn"    // 상품명 노출 여부
                ,   "initial_kor"           // 한글 초성
                ,   "initial_eng"           // 영문 초성
        );

        model.addAllAttributes(RealGridHelper.createForGroup("brandListGridBaseInfo", BrandListSelectDto.class));

        return "/item/brandMain";
    }

    /**
     * 상품관리 > 상품관리 > 브랜드 > 리스트
     *
     * @param listParamDto
     * @return BrandListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getBrandList.json"}, method = RequestMethod.GET)
    public List<BrandListSelectDto> getBrandList(BrandListParamDto listParamDto) {

        String apiUri = "/item/brand/getBrandList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<BrandListSelectDto>>>() {};
        return (List<BrandListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, BrandListParamDto.class, listParamDto), typeReference).getData();
    }

    @ResponseBody
    @RequestMapping(value = {"/getBrand.json"}, method = RequestMethod.GET)
    public BrandSelectDto getBrand(Long brandNo) {

        String apiUri = "/item/brand/getBrand?brandNo=" + brandNo;

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<BrandSelectDto>>() {};
        return (BrandSelectDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, typeReference).getData();
    }

    /**
     * 브래드명으로 리스트 조회
     * @param brandNm
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getBrandListByNm.json"}, method = RequestMethod.GET)
    public List<BrandListSelectDto> getBrandListByNm(@RequestParam(value = "brandNm") String brandNm) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("brandNm", brandNm));

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<BrandListSelectDto>>>() {};
        return (List<BrandListSelectDto>) resourceClient
                .getForResponseObject(
                        ResourceRouteName.ITEM,
                        "/item/brand/getBrandListByNm" + StringUtil.getParameter(setParameterList),
                        typeReference).getData();
    }

    /**
     * 상품관리 > 상품관리 > 브랜드관리
     *
     * @param setParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/setBrand.json"}, method = RequestMethod.POST)
    public Object setBrand(@ModelAttribute BrandSetParamDto setParamDto) {

        setParamDto.setRegId(cookieService.getUserInfo().getEmpId());

        String apiUri = "/item/brand/setBrand";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {};
        ResponseObject<ResponseResult> responseObject = resourceClient.postForResponseObject(ResourceRouteName.ITEM, setParamDto, apiUri, typeReference);

        ResponseResult responseData = responseObject.getData();

        return responseData;
    }
}
