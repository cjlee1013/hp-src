package kr.co.homeplus.admin.web.promotion.model.event;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("마일리지행사 노출 점포 조회")
public class MileagePromoStoreGetDto {

    @ApiModelProperty(value = "점포ID")
    private String storeId;

    @ApiModelProperty(value = "점포명")
    private String storeNm;
}