package kr.co.homeplus.admin.web.item.model.brand;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BrandSetParamDto {

    private Long brandNo;
    private String brandNm;
    private String brandNmEng;
    private String brandDesc;
    private String useYn;
    private String itemDispYn;
    private String dispNm;
    private String initial;
    private String regId;
    private String isImageUpdate;
    private String imgUrl;
    private String siteUrl;
}
