package kr.co.homeplus.admin.web.item.model.item;

import lombok.Data;

/**
 * 추가구성 정보
 */
@Data
public class ItemOptionAdditionGetDto {
	private long   addOptNo;
	private long   itemNo;
	private String addOptTitle;
	private String addOptVal;
	private int    addOptPrice;
	private int    priority;
	private int    stockQty;
	private String sellerAddOptCd;
	private String useYn;
	private String syncStock;
	private String regId;
	private String regDt;
	private String chgId;
	private String chgDt;

	private String regNm;
	private String chgNm;
	private String useYnNm;

	/**
	 * 판매된 수량
	 */
	private int salesQty;
}
