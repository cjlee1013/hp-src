package kr.co.homeplus.admin.web.partner.model;

import lombok.Data;

@Data
//판매자 주소
public class PartnerSellerAddressDto {

    //국내 우편번호
    private String zipCode;

    //국내 지번 주소1
    private String addr1;

    //국내 지번 주소2
    private String addr2;
}
