package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.unshipOrder.TdUnshipOrderListGetDto;
import kr.co.homeplus.admin.web.settle.model.unshipOrder.TdUnshipOrderListSetDto;
import kr.co.homeplus.admin.web.settle.model.unshipOrder.UnshipOrderListGetDto;
import kr.co.homeplus.admin.web.settle.model.unshipOrder.UnshipOrderListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UnshipOrderService {

    private final ResourceClient resourceClient;

    /**
     * 미배송 주문조회 리스트 조회
     */
    public List<UnshipOrderListGetDto> getUnshipOrderList(UnshipOrderListSetDto unshipOrderListSetDto) throws Exception{
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            unshipOrderListSetDto,
            "/admin/unshipOrder/getUnshipOrderList",
            new ParameterizedTypeReference<ResponseObject<List<UnshipOrderListGetDto>>>(){}).getData();
    }

    /**
     * TD미배송 주문조회 리스트 조회
     */
    public List<TdUnshipOrderListGetDto> getTdUnshipOrderList(TdUnshipOrderListSetDto tdUnshipOrderListSetDto) throws Exception{
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            tdUnshipOrderListSetDto,
            "/admin/unshipOrder/getTdUnshipOrderList",
            new ParameterizedTypeReference<ResponseObject<List<TdUnshipOrderListGetDto>>>(){}).getData();
    }
}
