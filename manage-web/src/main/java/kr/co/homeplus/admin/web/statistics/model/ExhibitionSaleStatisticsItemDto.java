package kr.co.homeplus.admin.web.statistics.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class ExhibitionSaleStatisticsItemDto {
    @RealGridColumnInfo(headText = "상품번호", sortable = true)
    String itemNo;

    @RealGridColumnInfo(headText = "상품명", sortable = true)
    String itemNm;

    @RealGridColumnInfo(headText = "판매수량", sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long saleCount;

    @RealGridColumnInfo(headText = "취소수량", sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long claimCount;

    @RealGridColumnInfo(headText = "판매금액", sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long saleAmt;

    @RealGridColumnInfo(headText = "취소금액", sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long claimAmt;

    @RealGridColumnInfo(headText = "순판매수량", sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long netSaleCount;

    @RealGridColumnInfo(headText = "순판매금액", sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long netSaleAmt;
}
