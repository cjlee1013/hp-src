package kr.co.homeplus.admin.web.promotion.model.promo;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PromoItemParamDto {

    private List<String> itemNoList;
    private String storeType;
}