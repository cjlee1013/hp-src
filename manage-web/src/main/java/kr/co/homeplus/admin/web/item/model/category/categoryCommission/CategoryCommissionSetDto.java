package kr.co.homeplus.admin.web.item.model.category.categoryCommission;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryCommissionSetDto {
    private String updateType;
    private String cateKind;
    private String changeCateCd;
    private String cateCd;
    private String commissionRate;
    private String useYn;
    private String regId;
    private String partnerId;
    private long prodNo;
    private String commissionKind;
    private String commissionType;
}
