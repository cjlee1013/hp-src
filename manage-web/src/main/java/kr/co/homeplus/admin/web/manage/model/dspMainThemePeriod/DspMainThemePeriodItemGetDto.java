package kr.co.homeplus.admin.web.manage.model.dspMainThemePeriod;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class DspMainThemePeriodItemGetDto {

    @RealGridColumnInfo(headText = "상품번호", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", columnType = RealGridColumnType.NAME, fieldType = RealGridFieldType.TEXT, width = 150, order = 1)
    private String itemNm;

    @RealGridColumnInfo(headText = "전시여부", columnType = RealGridColumnType.NAME, fieldType = RealGridFieldType.TEXT, width = 50, order = 2, sortable = true)
    private String dispYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 3)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 4, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "itemSeq", order = 5, hidden = true)
    private Long itemSeq;
    @RealGridColumnInfo(headText = "themeId", order = 6, hidden = true)
    private Long themeId;
    @RealGridColumnInfo(headText = "dispYn", order = 7, hidden = true)
    private String dispYn;
    @RealGridColumnInfo(headText = "priority", order = 8, hidden = true)
    private int priority;

    private String regId;
    private String chgId;
    private String chgNm;
    private String chgDt;

}
