package kr.co.homeplus.admin.web.item.model.auroraBanner;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuroraBannerListParamDto {

    private String searchPeriodType;
    private String searchStartDt;
    private String searchEndDt;
    private String schKeyword;

}
