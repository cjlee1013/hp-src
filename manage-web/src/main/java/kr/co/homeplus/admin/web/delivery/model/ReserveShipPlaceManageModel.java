package kr.co.homeplus.admin.web.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "선물세트 발송지 관리")
@Getter
@Setter
@EqualsAndHashCode
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class ReserveShipPlaceManageModel {
    @ApiModelProperty(value = "발송일자(YYYY-MM-DD)", position = 1)
    @RealGridColumnInfo(headText = "발송일", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String shipDt;

    @ApiModelProperty(value = "발송장소점포여부", position = 2)
    @RealGridColumnInfo(headText = "점포", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String shipPlaceStoreYn;

    @ApiModelProperty(value = "발송장소안성여부", position = 3)
    @RealGridColumnInfo(headText = "중앙(안성)", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String shipPlaceAnseongYn;

    @ApiModelProperty(value = "발송장소함안여부", position = 4)
    @RealGridColumnInfo(headText = "중앙(함안)", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String shipPlaceHamanYn;

    @ApiModelProperty(value = "등록자ID", position = 5)
    private String regId;

    @ApiModelProperty(value = "등록일자", position = 6)
    private String regDt;

    @ApiModelProperty(value = "수정자ID", position = 7)
    private String chgId;

    @ApiModelProperty(value = "수정일자", position = 8)
    private String chgDt;
}
