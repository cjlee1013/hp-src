package kr.co.homeplus.admin.web.item.model.recipe;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
@Getter
@Setter
public class RecipeItemGetDto {

    @RealGridColumnInfo(headText = "NO", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_CENTER, sortable = true)
    private int priority;

    @RealGridColumnInfo(headText = "상품번호", width = 120, order = 1, sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 150, order = 2)
    private String itemNm1;

    @RealGridColumnInfo(headText = "옵션번호", width = 150, order = 3)
    private String optNo;

    @RealGridColumnInfo(headText = "옵션명", width = 150, order = 4)
    private String opt1Val;

}