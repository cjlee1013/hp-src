package kr.co.homeplus.admin.web.promotion.model.coupon;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CouponListParamDto {
    private String schDateType;
    private String schStartDt;
    private String schEndDt;
    private String schStoreType;
    private String schCouponType;
    private String schRegNm;
    private String schCouponStatus;
    private String schPurpose;
    private String schShareYn;
    private String schType;
    private String schValue;
    private String schDepartCd;
    private String schGradeCouponYn;

    private char schExceptTempStatusYn;
    private char schIssueCouponOnlyYn;
}