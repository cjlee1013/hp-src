package kr.co.homeplus.admin.web.escrow.model.extract;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "픽업주문 추출용 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class ExtractPickupOrderDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    @RealGridColumnInfo(headText = "주문번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "점포명", position = 2)
    @RealGridColumnInfo(headText = "점포명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeNm;

    @ApiModelProperty(value = "주문일", position = 3)
    @RealGridColumnInfo(headText = "주문일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String orderDt;

    @ApiModelProperty(value = "픽업일", position = 4)
    @RealGridColumnInfo(headText = "픽업일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String shipDt;

    @ApiModelProperty(value = "픽업시간", position = 5)
    @RealGridColumnInfo(headText = "픽업시간", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String pickupTime;

    @ApiModelProperty(value = "주문금액", position = 6)
    @RealGridColumnInfo(headText = "주문금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long orderAmt;

    @ApiModelProperty(value = "장바구니할인금액", position = 7)
    @RealGridColumnInfo(headText = "장바구니할인금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cartDiscountAmt;

    @ApiModelProperty(value = "중복쿠폰할인금액", position = 8)
    @RealGridColumnInfo(headText = "중복쿠폰 할인금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long addDiscountAmt;

    @ApiModelProperty(value = "결제금액", position = 9)
    @RealGridColumnInfo(headText = "결제금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long payAmt;

    @ApiModelProperty(value = "주문상태", position = 10)
    @RealGridColumnInfo(headText = "주문상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String paymentStatus;

    @ApiModelProperty(value = "배송상태", position = 11)
    @RealGridColumnInfo(headText = "배송상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String shipStatus;
}
