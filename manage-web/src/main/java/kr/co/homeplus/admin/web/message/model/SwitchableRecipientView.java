package kr.co.homeplus.admin.web.message.model;

import java.time.LocalDateTime;
import kr.co.homeplus.admin.web.message.enums.WorkStatus;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import lombok.Data;

@Data
public class SwitchableRecipientView {

    private long workSeq;

    private long listSeq;

    private long historySeq;

    private WorkType workType;

    private String templateCode;

    private LocalDateTime sendDt;

    private String recipient;

    private WorkStatus alimtalkStatus;

    private WorkStatus smsStatus;
}
