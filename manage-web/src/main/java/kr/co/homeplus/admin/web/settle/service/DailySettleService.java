package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.dailySettle.DailySettleInfoGetDto;
import kr.co.homeplus.admin.web.settle.model.dailySettle.DailySettleInfoSetDto;
import kr.co.homeplus.admin.web.settle.model.dailySettle.DailySettleListGetDto;
import kr.co.homeplus.admin.web.settle.model.dailySettle.DailySettleListSetDto;
import kr.co.homeplus.admin.web.settle.model.dailySettle.DailySettleSumGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DailySettleService {

    private final ResourceClient resourceClient;

    /**
     * 정산관리 > 매출조회 > 메인 리스트 조회
     */
    public List<DailySettleListGetDto> getDailySettleList(DailySettleListSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/dailySettle/getDailySettleList",
            new ParameterizedTypeReference<ResponseObject<List<DailySettleListGetDto>>>(){}).getData();
    }

    /**
     * 정산관리 > 매출조회 > 파트너 상세 매출 조회
     */
    public List<DailySettleInfoGetDto> getDailySettleInfo(DailySettleInfoSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/dailySettle/getDailySettleInfo",
            new ParameterizedTypeReference<ResponseObject<List<DailySettleInfoGetDto>>>(){}).getData();
    }

    /**
     * 정산관리 > 매출조회 > 점별 매출금액 합계
     */
    public List<DailySettleSumGetDto> getDailySettleSum(DailySettleListSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/dailySettle/getDailySettleSum",
            new ParameterizedTypeReference<ResponseObject<List<DailySettleSumGetDto>>>(){}).getData();
    }

}
