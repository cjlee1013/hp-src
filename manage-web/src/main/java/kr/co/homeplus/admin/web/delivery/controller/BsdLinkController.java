package kr.co.homeplus.admin.web.delivery.controller;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumn;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.admin.web.delivery.model.shipManage.BsdLinkListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.BsdLinkListSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.BsdLinkTranSetDto;
import kr.co.homeplus.admin.web.delivery.service.BsdLinkService;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 배송관리 > 배송관리 > BSD연동조회
 */
@Controller
@RequestMapping("/ship")
public class BsdLinkController {

    @Autowired
    private BsdLinkService bsdLinkService;
    @Autowired
    private ShipCommonService shipCommonService;

    // BSD연동조회 그리드
    private static final List<RealGridColumn> BSD_LINK_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption BSD_LINK_GRID_OPTION = new RealGridOption("fill", false, true, false);

    public BsdLinkController() {
        // BSD연동조회 그리드
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("bsdLinkNo", "bsdLinkNo", "순번", RealGridColumnType.BASIC, 120, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("purchaseOrderNo", "purchaseOrderNo", "주문번호", RealGridColumnType.BASIC, 120, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("orderDt", "orderDt", "주문일자", RealGridColumnType.BASIC, 120, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("reserveShipDt", "reserveShipDt", "희망발송일", RealGridColumnType.BASIC, 100, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("sendPlaceNm", "sendPlaceNm", "배송유형", RealGridColumnType.BASIC, 100, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("sendPlace", "sendPlace", "배송유형코드", RealGridColumnType.BASIC, 100, false, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("linkDt", "linkDt", "BSD연동일자", RealGridColumnType.BASIC, 150, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("linkResultNm", "linkResultNm", "BSD연동결과", RealGridColumnType.BASIC, 100, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("linkResult", "linkResult", "BSD연동결과코드", RealGridColumnType.BASIC, 100, false, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("checkResultNm", "checkResultNm", "BSD체크결과", RealGridColumnType.BASIC, 100, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("checkResult", "checkResult", "BSD체크결과코드", RealGridColumnType.BASIC, 100, false, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("shipStatusNm", "shipStatusNm", "배송상태", RealGridColumnType.BASIC, 100, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("shipStatus", "shipStatus", "배송상태코드", RealGridColumnType.BASIC, 100, false, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("buyerNm", "buyerNm", "주문자명", RealGridColumnType.BASIC, 100, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("receiverNm", "receiverNm", "수취인명", RealGridColumnType.BASIC, 100, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("itemNm1", "itemNm1", "상품명", RealGridColumnType.NONE, 350, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("shipAddr", "shipAddr", "주소", RealGridColumnType.NONE, 350, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("dlvRefitSeq", "dlvRefitSeq", "박스번호", RealGridColumnType.BASIC, 120, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("dlvCdNm", "dlvCdNm", "택배사", RealGridColumnType.BASIC, 120, true, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("dlvCd", "dlvCd", "택배사코드", RealGridColumnType.BASIC, 120, false, true));
        BSD_LINK_GRID_HEAD.add(new RealGridColumn("invoiceNo", "invoiceNo", "송장번호", RealGridColumnType.BASIC, 150, true, true));
    }

    /**
     * BSD연동조회 Main Page
     */
    @GetMapping("/shipManage/bsdLinkMain")
    public String bsdLinkMain(Model model) throws Exception {
        model.addAttribute("shipStatus", shipCommonService.getCode("ship_status"));
        model.addAttribute("sendPlace", shipCommonService.getCode("send_place"));
        model.addAttribute("bsdLinkGridBaseInfo", new RealGridBaseInfo("bsdLinkGridBaseInfo", BSD_LINK_GRID_HEAD, BSD_LINK_GRID_OPTION).toString());

        return "/delivery/bsdLinkMain";
    }

    /**
     * BSD연동조회 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/shipManage/getBsdLinkList.json", method = RequestMethod.GET)
    public List<BsdLinkListGetDto> getBsdLinkList(BsdLinkListSetDto bsdLinkListSetDto) {
        return bsdLinkService.getBsdLinkList(bsdLinkListSetDto);
    }

    /**
     * BSD전송
     */
    @ResponseBody
    @PostMapping(value = "/shipManage/setBsdLinkTran.json")
    public ResponseObject<Object> setBsdLinkTran(@RequestBody BsdLinkTranSetDto bsdLinkTranSetDto) throws Exception {
        return bsdLinkService.setBsdLinkTran(bsdLinkTranSetDto);
    }

}
