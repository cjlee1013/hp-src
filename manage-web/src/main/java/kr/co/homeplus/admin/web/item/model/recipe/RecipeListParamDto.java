package kr.co.homeplus.admin.web.item.model.recipe;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("레시피리스트")
public class RecipeListParamDto {

    private String searchStartDt;

    private String searchEndDt;

    private String searchDispYn;

    private String searchType;

    private String searchKeyword;
}
