package kr.co.homeplus.admin.web.common.model.pop;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AffiliateChannelPopListSelectDto {
    @RealGridColumnInfo(headText = "제휴채널ID", sortable = true, width = 50)
    private String channelId;

    @RealGridColumnInfo(headText = "제휴채널ID", sortable = true, width = 50)
    private String channelNm;

    @RealGridColumnInfo(headText = "제휴업체ID", sortable = true, width = 50)
    private String partnerId;

    @RealGridColumnInfo(headText = "제휴업체명", sortable = true, width = 50)
    private String partnerNm;

    @RealGridColumnInfo(headText = "사이트 구분", sortable = true, width = 50)
    private String siteTypeNm;

    @RealGridColumnInfo(headText = "사용여부", sortable = true, width = 50)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "등록자", sortable = true, width = 50)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", sortable = true, width = 50)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", sortable = true, width = 50)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", sortable = true, width = 50)
    private String chgDt;
}
