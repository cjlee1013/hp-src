package kr.co.homeplus.admin.web.promotion.model.event;

import java.util.List;
import kr.co.homeplus.admin.web.promotion.model.coupon.PromotionPaymentDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MileagePromoDetailGetDto {
    private Long mileageNo;

    private String siteType;

    private String mileageMngNm;

    private String promoStartDt;

    private String promoEndDt;

    private String earnType;

    private Float earnPercent;

    private Integer earnAmount;

    private Integer purchaseMin;

    private Integer earnAmountMax;

    private String storeType;

    private String mileageCode;

    private String displayMessage;

    private Integer expireDayInp;

    private String dispStoreType;

    private String departCd;

    private String useYn;

    private String purchaseGuide;

    // 노출 점포 리스트
    private List<MileagePromoStoreGetDto> storeList;

    // 적용상품 리스트
    private List<MileagePromoItemGetDto> itemList;

    // 결제수단 리스트 (카드)
    private List<PromotionPaymentDto> paymentList;
}