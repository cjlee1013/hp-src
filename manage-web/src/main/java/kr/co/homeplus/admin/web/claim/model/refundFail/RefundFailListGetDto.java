package kr.co.homeplus.admin.web.claim.model.refundFail;


import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class RefundFailListGetDto {

    @ApiModelProperty(value = "환불요청일", position = 1)
    @RealGridColumnInfo(headText = "환불요청일", width = 80)
    private String payRegDt;

    @ApiModelProperty(value = "클레임 타입", position = 2)
    @RealGridColumnInfo(headText = "클레임 타입", width = 80, hidden = true)
    private String claimType;

    @ApiModelProperty(value = "그룹클레임번호", position = 3)
    @RealGridColumnInfo(headText = "그룹클레임번호", width = 80)
    private String claimNo;

    @ApiModelProperty(value = "주문번호", position = 4)
    @RealGridColumnInfo(headText = "주문번호", width = 80)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "처리상태", position = 5)
    @RealGridColumnInfo(headText = "처리상태", width = 80)
    private String paymentStatus;

    @ApiModelProperty(value = "환불금액", position = 6)
    @RealGridColumnInfo(headText = "환불금액", width = 80, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE)
    private String paymentAmt;

    @ApiModelProperty(value = "결제수단", position = 7)
    @RealGridColumnInfo(headText = "결제수단", width = 80)
    private String paymentType;

    @ApiModelProperty(value = "환불수단", position = 8)
    @RealGridColumnInfo(headText = "환불수단", width = 80)
    private String refundType;

    @ApiModelProperty(value = "연동 주문번호", position = 8)
    @RealGridColumnInfo(headText = "연동 주문번호", width = 100)
    private String pgOid;

    @ApiModelProperty(value = "PG사", position = 8)
    @RealGridColumnInfo(headText = "PG사", width = 80)
    private String pgKind;

    @ApiModelProperty(value = "주문완료일", position = 9)
    @RealGridColumnInfo(headText = "주문완료일", width = 120)
    private String orderDt;

    @ApiModelProperty(value = "환불실패 사유", position = 10)
    @RealGridColumnInfo(headText = "환불실패 사유", width = 200)
    private String refundDesc;

    @ApiModelProperty(value = "회원번호", position = 11)
    @RealGridColumnInfo(headText = "회원번호", width = 80)
    private String userNo;

    @ApiModelProperty(value = "구매자명", position = 12)
    @RealGridColumnInfo(headText = "구매자명", width = 80)
    private String buyerNm;

    @ApiModelProperty(value = "구매자 연락처", position = 13)
    @RealGridColumnInfo(headText = "구매자 연락처", width = 80)
    private String buyerMobileNo;

    @ApiModelProperty(value = "클레임환불번호", position = 14)
    @RealGridColumnInfo(headText = "클레임환불번호", width = 80, hidden = true)
    private String claimPaymentNo;

    @ApiModelProperty(value = "클레임번호", position = 15)
    @RealGridColumnInfo(headText = "클레임번호", width = 80, hidden = true)
    private String claimBundleNo;

}
