package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class AnalysisGridDto {

    @Getter
    @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
    public static class AnalysisPlugin{
        @RealGridColumnInfo(headText = "term", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private String term;
        @RealGridColumnInfo(headText = "type", width = 15, columnType = RealGridColumnType.BASIC, sortable = true)
        private String type;
        @RealGridColumnInfo(headText = "startOffset", width = 15, columnType = RealGridColumnType.BASIC, sortable = true)
        private int startOffset;
        @RealGridColumnInfo(headText = "endOffset", width = 15, columnType = RealGridColumnType.BASIC, sortable = true)
        private int endOffset;
        @RealGridColumnInfo(headText = "position", width = 15, columnType = RealGridColumnType.BASIC, sortable = true)
        private int position;
    }
}
