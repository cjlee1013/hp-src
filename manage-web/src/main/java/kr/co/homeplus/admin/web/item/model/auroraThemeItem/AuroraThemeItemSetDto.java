package kr.co.homeplus.admin.web.item.model.auroraThemeItem;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuroraThemeItemSetDto {

    List<AuroraThemeItemSetParamDto> itemList;

}
