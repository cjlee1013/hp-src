package kr.co.homeplus.admin.web.statistics.controller;


import java.util.List;
import kr.co.homeplus.admin.web.statistics.model.AverageOrderItemQtyListGetDto;
import kr.co.homeplus.admin.web.statistics.model.AverageOrderItemQtyListSetDto;
import kr.co.homeplus.admin.web.statistics.service.AverageOrderItemQtyService;
import kr.co.homeplus.admin.web.statistics.service.StatisticsCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/statistics/order")
@RequiredArgsConstructor
@Slf4j
public class AverageOrderItemQtyController {

    private final StatisticsCommonService statisticsCommonService;

    private final AverageOrderItemQtyService averageOrderItemQtyService;

    /**
     * 통계 > 주문결제통계 > 주문당 평균 주문 상품수
     */
    @GetMapping("/averageOrderItemQtyMain")
    public String averageOrderItemQtyMain(Model model) throws Exception {
        //년도 조회
        model.addAttribute("getYear", statisticsCommonService.getDateCalculate("Y", null, null, 2020));
        //월 조회
        model.addAttribute("getMonth", statisticsCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.create("averageItemQtyListGridBaseInfo", AverageOrderItemQtyListGetDto.class));

        return "/statistics/averageOrderItemQtyMain";
    }

    /**
     * 통계 > 주문결제통계 > 주문당 평균 주문 상품수 리스트 조회
     *
     * @param averageOrderItemQtyListSetDto
     * @return String
     */
    @ResponseBody
    @PostMapping("/getAverageOrderItemQtyList.json")
    public List<AverageOrderItemQtyListGetDto> getAverageOrderItemQtyList(@RequestBody AverageOrderItemQtyListSetDto averageOrderItemQtyListSetDto) {
        return averageOrderItemQtyService.getAverageOrderItemQtyList(averageOrderItemQtyListSetDto);
    }
}
