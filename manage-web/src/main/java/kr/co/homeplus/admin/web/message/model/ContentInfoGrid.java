package kr.co.homeplus.admin.web.message.model;

import java.time.LocalDateTime;
import kr.co.homeplus.admin.web.message.enums.StoreType;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import kr.co.homeplus.admin.web.message.enums.AffiliationType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class ContentInfoGrid {

    @RealGridColumnInfo(headText = "번호", width = 20)
    private long contentSeq;
    @RealGridColumnInfo(headText = "템플릿명", columnType = RealGridColumnType.NAME)
    private String name;
    @RealGridColumnInfo(headText = "설명", columnType = RealGridColumnType.NAME)
    private String description;
    @RealGridColumnInfo(headText = "점포유형", width = 35)
    private StoreType storeType;
    @RealGridColumnInfo(headText = "발송주체", width = 35, columnType = RealGridColumnType.LOOKUP)
    private AffiliationType affiliationType;
    @RealGridColumnInfo(headText = "발송채널", width = 35)
    private WorkType workType;
    @RealGridColumnInfo(headText = "발신정보")
    private String callback;
    @RealGridColumnInfo(hidden = true)
    private String contentSubject;
    @RealGridColumnInfo(hidden = true)
    private String contentBody;
    @RealGridColumnInfo(headText = "템플릿코드", width = 40, sortable = true)
    private String templateCode;
    @RealGridColumnInfo(headText = "기타정보1", width = 55, columnType = RealGridColumnType.NAME)
    private String etc1;
    @RealGridColumnInfo(headText = "기타정보2", width = 55, columnType = RealGridColumnType.NAME)
    private String etc2;
    @RealGridColumnInfo(headText = "기타정보3", width = 55, columnType = RealGridColumnType.NAME)
    private String etc3;
    @RealGridColumnInfo(headText = "기타정보4", width = 55, columnType = RealGridColumnType.NAME)
    private String etc4;
    @RealGridColumnInfo(headText = "기타정보5", width = 55, columnType = RealGridColumnType.NAME)
    private String etc5;
    @RealGridColumnInfo(headText = "작성자", width = 30)
    private String regId;
    @RealGridColumnInfo(headText = "작성일시", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME, width = 80)
    private LocalDateTime regDt;
    @RealGridColumnInfo(headText = "수정자", width = 30)
    private String chgId;
    @RealGridColumnInfo(headText = "변경일시", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME, width = 80)
    private LocalDateTime chgDt;
}
