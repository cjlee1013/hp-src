package kr.co.homeplus.admin.web.manage.model.banner;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BannerLinkGetDto {

    private Long bannerNo;
    private Long linkNo;
    private String linkType;
    private String linkInfo;
    private String linkInfoNm;
    private String linkOptions;
    private String textYn;
    private String title;
    private String subTitle;
    private String imgUrl;
    private int imgWidth;
    private int imgHeight;
    private String useYn;
    private String regId;
    private String regNm;
    private String regDt;
    private String chgId;
    private String chgNm;
    private String chgDt;

}
