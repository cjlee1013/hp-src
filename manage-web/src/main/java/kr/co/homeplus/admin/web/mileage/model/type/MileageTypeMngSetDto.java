package kr.co.homeplus.admin.web.mileage.model.type;

import lombok.Data;

@Data
public class MileageTypeMngSetDto {
    //점포 유형
    private String storeType;

    //마일리지 종류
    private String mileageKind;

    //사용 여부
    private String useYn;

    //상세검색
    private String schDetailCode;

    //마일리지 종류
    private String schDetailDesc;

    //마일리지 타입번호
    private String mileageTypeNo;
}
