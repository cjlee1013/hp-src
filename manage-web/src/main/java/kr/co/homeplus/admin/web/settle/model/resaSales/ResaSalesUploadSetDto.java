package kr.co.homeplus.admin.web.settle.model.resaSales;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "SALES 자료조회 파일업로드")
public class ResaSalesUploadSetDto {
  @ApiModelProperty(notes = "일자")
  private String business_date;

  @ApiModelProperty(notes = "점포코드")
  private String store;

  @ApiModelProperty(notes = "계정과목")
  private String type;

  @ApiModelProperty(notes = "코스트센터")
  private String cost_center;

  @ApiModelProperty(notes = "금액")
  private String amt;

  @ApiModelProperty(notes = "등록자")
  private String regId;
}
