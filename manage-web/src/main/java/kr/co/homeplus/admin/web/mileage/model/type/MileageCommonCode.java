package kr.co.homeplus.admin.web.mileage.model.type;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MileageCommonCode {
    private String codeGroup;
    private String code;
    private String upperCodeGroup;
    private String upperCode;
    private String codeName;
    private String priority;
    private String useYn;
    private String etc1;
    private String etc2;
}
