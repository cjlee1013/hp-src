package kr.co.homeplus.admin.web.core.privacylog.model;

public enum PersonalLogMethod {
    SELECT, INSERT, DELETE, UPDATE;
}
