package kr.co.homeplus.admin.web.core.exception;

public class LoginIdSaveCookieServiceException extends RuntimeException {

    public LoginIdSaveCookieServiceException(String message, Exception e) {
        super(message, e);
    }

    public LoginIdSaveCookieServiceException(String message) {
        super(message);
    }
}
