package kr.co.homeplus.admin.web.settle.model.dcOnlineReceivePay;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DcOnlineReceivePayListGetDto {
    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 120, sortable = true)
    private String itemNo;

    @ApiModelProperty(value = "RMS상품코드")
    @RealGridColumnInfo(headText = "RMS상품코드", width = 120, sortable = true)
    private String rmsItemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", width = 450, sortable = true)
    private String itemName;

    @ApiModelProperty(value = "과면세구분")
    @RealGridColumnInfo(headText = "과면세구분", width = 120, sortable = true)
    private String taxYn;

    @ApiModelProperty(value = "카테고리")
    @RealGridColumnInfo(headText = "카테고리", width = 120, sortable = true)
    private String deptNo;

    @ApiModelProperty(value = "점포코드")
    @RealGridColumnInfo(headText = "점포코드", width = 120, sortable = true)
    private String storeId;

    @ApiModelProperty(value = "점포카테고리")
    @RealGridColumnInfo(headText = "점포카테고리", width = 120, sortable = true)
    private String deptStoreCd;

    @ApiModelProperty(value = "기초잔고수량")
    @RealGridColumnInfo(headText = "기초잔고수량", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String basicQty;

    @ApiModelProperty(value = "기초잔고금액")
    @RealGridColumnInfo(headText = "기초잔고금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String basicAmt;

    @ApiModelProperty(value = "매입수량")
    @RealGridColumnInfo(headText = "매입수량", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String purchaseQty;

    @ApiModelProperty(value = "매입금액")
    @RealGridColumnInfo(headText = "매입금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String purchaseAmt;

    @ApiModelProperty(value = "재고이동수량")
    @RealGridColumnInfo(headText = "재고이동수량", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String tranQty;

    @ApiModelProperty(value = "재고이동금액")
    @RealGridColumnInfo(headText = "재고이동금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String tranAmt;

    @ApiModelProperty(value = "손실수량")
    @RealGridColumnInfo(headText = "손실수량", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String lossQty;

    @ApiModelProperty(value = "손실금액")
    @RealGridColumnInfo(headText = "손실금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String lossAmt;

    @ApiModelProperty(value = "미확인손실수량")
    @RealGridColumnInfo(headText = "미확인손실수량", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String unknownLossQty;

    @ApiModelProperty(value = "미확인손실금액")
    @RealGridColumnInfo(headText = "미확인손실금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String unknownLossAmt;

    @ApiModelProperty(value = "반품수량")
    @RealGridColumnInfo(headText = "반품수량", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String refundQty;

    @ApiModelProperty(value = "반품금액")
    @RealGridColumnInfo(headText = "반품금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String refundAmt;

    @ApiModelProperty(value = "매출수량")
    @RealGridColumnInfo(headText = "매출수량", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String salesQty;

    @ApiModelProperty(value = "매출금액")
    @RealGridColumnInfo(headText = "매출금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String salesAmt;

    @ApiModelProperty(value = "기말잔고수량")
    @RealGridColumnInfo(headText = "기말잔고수량", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String termEndQty;

    @ApiModelProperty(value = "기말잔고금액")
    @RealGridColumnInfo(headText = "기말잔고금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String termEndAmt;

    @ApiModelProperty(value = "매입원가")
    @RealGridColumnInfo(headText = "매입원가", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String orgPrice;

    @ApiModelProperty(value = "업체코드")
    @RealGridColumnInfo(headText = "업체코드", width = 120, sortable = true)
    private String vendorCd;

    @ApiModelProperty(value = "주문수량")
    @RealGridColumnInfo(headText = "주문수량", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String orderQty;

    @ApiModelProperty(value = "주문금액")
    @RealGridColumnInfo(headText = "주문금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String orderAmt;

    @ApiModelProperty(value = "이동평균원가")
    @RealGridColumnInfo(headText = "이동평균원가", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String tranAvgAmt;

    @ApiModelProperty(value = "할인금액")
    @RealGridColumnInfo(headText = "할인금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String discountAmt;

    @ApiModelProperty(value = "판매금액")
    @RealGridColumnInfo(headText = "판매금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String sellAmt;

    @ApiModelProperty(value = "순매출금액")
    @RealGridColumnInfo(headText = "순매출금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String realSaleAmt;

    @ApiModelProperty(value = "공급가")
    @RealGridColumnInfo(headText = "공급가", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String supplyAmt;

    @ApiModelProperty(value = "마진")
    @RealGridColumnInfo(headText = "마진", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String margin;
}
