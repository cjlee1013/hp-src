package kr.co.homeplus.admin.web.message.model;

import lombok.Data;

@Data
public class RecipientDetailRequest {

    private String listSeq;

    private String historySeq;

    private String workType;
}
