package kr.co.homeplus.admin.web.message.service;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import kr.co.homeplus.admin.web.message.model.RecipientDetailResponse;
import kr.co.homeplus.admin.web.message.model.RecipientInfo;
import kr.co.homeplus.admin.web.message.model.RecipientSearchRequest;
import kr.co.homeplus.admin.web.message.model.RecipientSearchResponse;
import kr.co.homeplus.admin.web.message.model.SmsResendRequest;
import kr.co.homeplus.admin.web.message.model.SmsResendResponse;
import kr.co.homeplus.admin.web.message.model.SwitchableRecipientView;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

@RequiredArgsConstructor
@Service
public class RecipientSupportService {

    private final ResourceClient resourceClient;

    public List<SwitchableRecipientView> search(final RecipientSearchRequest request) {
        String addUrl = "";
        if (StringUtils.isNotEmpty(request.getWorkType())) {
            addUrl = "/work?workType=" + request.getWorkType();
        }

        if (StringUtils.isEmpty(request.getWorkStatus())) {
            request.setWorkStatus(null);
        }

        RecipientSearchResponse response = resourceClient.postForResponseObject(
            ResourceRouteName.MESSAGE, request, "/recipient/list/switch" + addUrl,
            new ParameterizedTypeReference<ResponseObject<RecipientSearchResponse>>() {
            }).getData();

        return response.getSwitchableRecipientInfos()
            .stream()
            .map(this::mapper)
            .collect(Collectors.toList());
    }

    public ResponseObject<RecipientDetailResponse> getDetail(long listSeq, long historySeq,
        WorkType workType) {
        String url = UriComponentsBuilder.newInstance()
            .path("/recipient/detail")
            .queryParam("listSeq", listSeq)
            .queryParam("historySeq", historySeq)
            .queryParam("workType", workType)
            .build().encode(StandardCharsets.UTF_8).toString();
        return resourceClient.getForResponseObject(ResourceRouteName.MESSAGE, url,
            new ParameterizedTypeReference<>() {
            });
    }

    public String resend(final SmsResendRequest smsResendRequest, WorkType workType) {
        if (WorkType.SMS != workType) {
            return "SMS만 재발송 가능합니다.";
        }
        String url = UriComponentsBuilder.newInstance()
            .path("/resend/sms/row")
            .build().encode(StandardCharsets.UTF_8).toString();

        ResponseObject<SmsResendResponse> responseObject = resourceClient.postForResponseObject(
            ResourceRouteName.MESSAGE, smsResendRequest, url, new ParameterizedTypeReference<>() {
            });
        return responseObject.getData().getSmsResendWorkSeq() > 0
            ? "재발송 되었습니다."
            : "재발송 실패하였습니다.";
    }

    private SwitchableRecipientView mapper(RecipientInfo recipientInfo) {
        SwitchableRecipientView switchableRecipientView = new SwitchableRecipientView();

        boolean singleType = "N".equalsIgnoreCase(recipientInfo.getSwitchTargetYn());
        if (recipientInfo.getWorkType() == WorkType.ALIMTALK && singleType) {
            switchableRecipientView.setWorkType(recipientInfo.getWorkType());
            switchableRecipientView.setWorkSeq(recipientInfo.getAlimtalkWorkSeq());
            switchableRecipientView.setListSeq(recipientInfo.getAlimtalkListSeq());
            switchableRecipientView.setHistorySeq(recipientInfo.getAlimtalkHistorySeq());
            switchableRecipientView.setTemplateCode(recipientInfo.getAlimtalkTemplateCode());
            switchableRecipientView.setAlimtalkStatus(recipientInfo.getAlimtalkSendResultCd());
            switchableRecipientView.setSmsStatus(null);
            switchableRecipientView.setRecipient(recipientInfo.getRecipient());
            switchableRecipientView.setSendDt(recipientInfo.getAlimtalkSendDt());
        } else {
            switchableRecipientView.setWorkType(WorkType.SMS);
            switchableRecipientView.setWorkSeq(recipientInfo.getSmsWorkSeq());
            switchableRecipientView.setListSeq(recipientInfo.getSmsListSeq());
            switchableRecipientView.setHistorySeq(recipientInfo.getSmsHistorySeq());
            switchableRecipientView.setTemplateCode(recipientInfo.getSmsTemplateCode());
            switchableRecipientView.setSmsStatus(recipientInfo.getSmsSendResultCd());
            switchableRecipientView.setRecipient(recipientInfo.getRecipient());
            switchableRecipientView.setSendDt(recipientInfo.getSmsSendDt());
            switchableRecipientView.setAlimtalkStatus(
                singleType ? null : recipientInfo.getAlimtalkSendResultCd());
        }
        return switchableRecipientView;
    }
}
