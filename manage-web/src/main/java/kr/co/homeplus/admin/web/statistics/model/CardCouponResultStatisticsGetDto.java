package kr.co.homeplus.admin.web.statistics.model;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardCouponResultStatisticsGetDto {
    @ApiModelProperty(value = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", width = 80, sortable = true)
    private String storeType;

    @ApiModelProperty(value = "쿠폰번호")
    @RealGridColumnInfo(headText = "쿠폰번호", width = 80, sortable = true)
    private String couponNo;

    @ApiModelProperty(value = "쿠폰명")
    @RealGridColumnInfo(headText = "쿠폰명", width = 200, sortable = true)
    private String couponNm;
}
