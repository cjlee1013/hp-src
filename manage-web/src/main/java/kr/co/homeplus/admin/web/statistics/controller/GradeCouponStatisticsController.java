package kr.co.homeplus.admin.web.statistics.controller;


import java.util.List;
import kr.co.homeplus.admin.web.statistics.model.AverageOrderItemQtyListGetDto;
import kr.co.homeplus.admin.web.statistics.model.AverageOrderItemQtyListSetDto;
import kr.co.homeplus.admin.web.statistics.model.GradeCouponStatisticsListGetDto;
import kr.co.homeplus.admin.web.statistics.model.GradeCouponStatisticsListSetDto;
import kr.co.homeplus.admin.web.statistics.service.AverageOrderItemQtyService;
import kr.co.homeplus.admin.web.statistics.service.GradeCouponStatisticsService;
import kr.co.homeplus.admin.web.statistics.service.StatisticsCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/statistics/promotion")
@RequiredArgsConstructor
@Slf4j
public class GradeCouponStatisticsController {

    private final GradeCouponStatisticsService gradeCouponStatisticsService;

    /**
     * 통계 > 프로모션통계 > 등급별 쿠폰 통계
     */
    @GetMapping("/gradeCouponStatisticsMain")
    public String gradeCouponStatisticsMain(Model model) throws Exception {

        model.addAllAttributes(RealGridHelper.create("gradeCouponStatisticsListGridBaseInfo", GradeCouponStatisticsListGetDto.class));
        return "/statistics/gradeCouponStatisticsMain";
    }

    /**
     * 통계 > 프로모션통계 > 등급별 쿠폰 통계 리스트 조회
     *
     * @param gradeCouponStatisticsListSetDto
     * @return String
     */
    @ResponseBody
    @PostMapping("/getGradeCouponStatisticsList.json")
    public List<GradeCouponStatisticsListGetDto> getGradeCouponStatisticsList(@RequestBody GradeCouponStatisticsListSetDto  gradeCouponStatisticsListSetDto) {
        return gradeCouponStatisticsService.getGradeCouponStatisticsList(gradeCouponStatisticsListSetDto);
    }

    //기간별 판매업체 판매 현황(E-KPI)
    @RequestMapping(value = "/popup/gradeCouponStatisticsPop", method = RequestMethod.GET)
    public String DSPeriodEKPIPop(Model model) throws Exception {
        return "/statistics/pop/gradeCouponStatisticsPop";
    }
}