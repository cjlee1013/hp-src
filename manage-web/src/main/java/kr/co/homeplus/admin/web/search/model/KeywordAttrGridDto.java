package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class KeywordAttrGridDto {
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
    public static class KeywordAttr {
        @RealGridColumnInfo(headText = "키워드번호", width = 30, sortable = true, columnType = RealGridColumnType.NUMBER)
        private Long keywordAttrNo;
        @RealGridColumnInfo(headText = "키워드", width = 150, sortable = true, columnType = RealGridColumnType.BASIC)
        private String keyword;
        @RealGridColumnInfo(headText = "사용여부", width = 80, sortable = true, columnType = RealGridColumnType.BASIC)
        private String useYn;
        @RealGridColumnInfo(headText = "등록자", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String regId;
        @RealGridColumnInfo(headText = "등록일", width = 70, sortable = true, columnType = RealGridColumnType.BASIC)
        private String regDt;
        @RealGridColumnInfo(headText = "수정자", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String chgId;
        @RealGridColumnInfo(headText = "수정일", width = 70, sortable = true, columnType = RealGridColumnType.BASIC)
        private String chgDt;
    }
}
