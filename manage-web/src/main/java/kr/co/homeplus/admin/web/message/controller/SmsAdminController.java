package kr.co.homeplus.admin.web.message.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.message.model.SmsCancelRequest;
import kr.co.homeplus.admin.web.message.model.SmsHistoryDetailRequest;
import kr.co.homeplus.admin.web.message.model.SmsHistoryListRequest;
import kr.co.homeplus.admin.web.message.model.SmsSendListGrid;
import kr.co.homeplus.admin.web.message.model.SmsWorkDetail;
import kr.co.homeplus.admin.web.message.model.SmsWorkGrid;
import kr.co.homeplus.admin.web.message.service.CommonMessageSupportService;
import kr.co.homeplus.admin.web.message.service.MessageLogService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Api(tags = "SMS 어드민용 컨트롤러")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
@Controller
@RequestMapping("/message/admin/sms")
@RequiredArgsConstructor
public class SmsAdminController {

    private final MessageLogService messageLogService;
    private final CommonMessageSupportService commonMessageSupportService;
    private final CodeService codeService;
    private final ResourceClient resourceClient;

    private final Map<String, Object> modelMap = RealGridHelper.create("smsWorkGridBaseInfo", SmsWorkGrid.class);
    private final Map<String, Object> smsSendListGridBaseInfoMap = RealGridHelper.create("smsSendListGridBaseInfo", SmsSendListGrid.class);

    @ApiOperation(value = "SMS 발송 main 화면")
    @ApiResponse(code = 200, message = "SMS 발송 main 화면")
    @GetMapping("/history/main")
    public String main(final Model model) {
        codeService.getCodeModel(model, "message_work_status");

        model.addAllAttributes(modelMap);
        model.addAllAttributes(smsSendListGridBaseInfoMap);

        return "/message/smsMain";
    }

    @ApiOperation(value = "SMS 발송 이력 목록 조회")
    @ApiResponse(code = 200, message = "SMS 발송 이력 목록")
    @PostMapping("/history/getWorks.json")
    @ResponseBody
    public ResponseObject<List<SmsWorkGrid>> historyList(
        @ApiParam(value = "SMS 발송 이력 조회 조건", required = true) @Valid final SmsHistoryListRequest smsHistoryListRequest) {

        return resourceClient
            .postForResponseObject(ResourceRouteName.MESSAGE, smsHistoryListRequest,
                "/admin/sms/history/list", new ParameterizedTypeReference<>() {
                });
    }

    @ApiOperation(value = "SMS 발송 상세 조회")
    @ApiResponse(code = 200, message = "SMS 발송 상세")
    @GetMapping("/history/getDetail.json")
    @ResponseBody
    public ResponseObject<SmsWorkDetail> historyDetail(final HttpServletRequest httpServletRequest,
        @ApiParam(value = "SMS 발송 상세 조회 조건", required = true) @Valid final SmsHistoryDetailRequest smsHistoryDetailRequest) {
        return messageLogService.smsHistoryDetail(httpServletRequest, smsHistoryDetailRequest);
    }

    @ApiOperation(value = "SMS 발송 취소")
    @ApiResponse(code = 200, message = "SMS 발송 취소 성공 여부")
    @PostMapping("/history/cancel.json")
    @ResponseBody
    public ResponseObject<Object> cancelOne(
        @ApiParam(value = "SMS 발송 취소 조건", required = true) @Valid final SmsCancelRequest smsCancelRequest) {

        ResponseObject<Object> responseObject = resourceClient
            .postForResponseObject(ResourceRouteName.MESSAGE, smsCancelRequest,
                "/admin/sms/history/cancel", new ParameterizedTypeReference<>() {
                });

        return ResponseObject.Builder.<Object>builder().data(responseObject).build();
    }

    @GetMapping("/resend.json")
    @ResponseBody
    public ResponseObject<String> resend(@RequestParam("smsWorkSeq") long smsWorkSeq) {
        return ResourceConverter.toResponseObject(commonMessageSupportService.resend(smsWorkSeq));
    }
}
