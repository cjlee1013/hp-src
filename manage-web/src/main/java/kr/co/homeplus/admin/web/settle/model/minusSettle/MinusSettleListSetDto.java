package kr.co.homeplus.admin.web.settle.model.minusSettle;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MinusSettleListSetDto {
    @ApiModelProperty(value = "검색기간타입")
    private String schDateType;

    @ApiModelProperty(value = "검색기간(Start)")
    private String schStartDt;

    @ApiModelProperty(value = "검색기간(End)")
    private String schEndDt;

    @ApiModelProperty(value = "검색조건 타입")
    private String schKeywordType;

    @ApiModelProperty(value = "검색어")
    private String schKeyword;
}
