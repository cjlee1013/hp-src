package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 배분율 관리 > 결제정책 관리-적용대상 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class PaymentPolicyApplyDto {
    @ApiModelProperty(value = "적용코드(상품코드,카테고리번호,제휴코드)")
    @RealGridColumnInfo(headText = "applyCd", width = 50, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String applyCd;

    @ApiModelProperty(value = "적용대상명(상품명,카테고리명,채널명)")
    @RealGridColumnInfo(headText = "applyTargetNm", width = 200, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String applyTargetNm;

    @ApiModelProperty(value = "정책번호")
    @RealGridColumnInfo(headText = "policyNo", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Long policyNo;

    @ApiModelProperty(value = "적용우선순위(1:채널,2:상품,3:카테고리(subString...),4:그이외...)")
    @RealGridColumnInfo(headText = "applyPriority", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Integer applyPriority;

    @ApiModelProperty(value = "적용순번")
    @RealGridColumnInfo(headText = "applySeq", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Long applySeq;

    @ApiModelProperty(value = "등록자id")
    private String regId;
    @ApiModelProperty(value = "등록일자")
    private String regDt;

    private List<PaymentPolicyApplyDto> paymentPolicyApplyDtoList;
}
