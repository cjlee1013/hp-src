package kr.co.homeplus.admin.web.common.model.pop;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Address {
    /**
     * 우편 번호
     */
    private String zipcode;
    /**
     * 시/도, 시군구 이름
     **/
    private String sidoSigungu;
    /**
     * 지번 - 동 이름
     **/
    private String gibunDongNm;
    /**
     * 지번 - 리 이름
     **/
    private String gibunRiNm;
    /**
     * 지번 - 본번(번지), 부번(호)
     **/
    private String gibunNo;
    /**
     * 지번 - 전체 주소
     **/
    private String gibunAddrFullTxt;
    /**
     * 지번 - 관련지번주소
     **/
    private String gibunRepTxt;
    /**
     * 도로명
     **/
    private String roadAddrNm;
    /**
     * 건물명
     */
    private String roadBuildNm;
    /**
     * 도로명 - 전체 주소
     **/
    private String roadAddrFullTxt;
    /**
     * 도서 산간 유형 ALL:전국, JEJU:제주, ISLAND:산간
     */
    private String islandType;
}
