package kr.co.homeplus.admin.web.order.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum OrderDlvMsgCode {

    AURORA_SHIP_MSG_01("ASM01", "자유출입가능"),
    AURORA_SHIP_MSG_02("ASM02", "출입비밀번호"),
    AURORA_SHIP_MSG_03("ASM03", "경비실호출"),
    AURORA_SHIP_MSG_04("ASM04", "기타사항"),

    DS_MSG_01("DSM01","문앞에 놓아주세요."),
    DS_MSG_02("DSM05","문앞에 놓은 장바구니에 담아주세요."),
    DS_MSG_03("DSM02","경비(관리)실에 맡겨주세요."),
    DS_MSG_04("DSM03","도착 전 휴대폰으로 연락주세요."),
    DS_MSG_05("DSM04","벨 누르지 마시고 노크해 주세요."),
    DS_MSG_06("SSM07","직접 받겠습니다."),
    DS_MSG_07("DSM06","기타 직접입력"),

    TD_MSG_01("SSM01", "문앞에 놓아주세요."),
    TD_MSG_02("SSM08", "문앞 배송 후, 벨 눌러주세요."),
    TD_MSG_03("SSM05", "문앞에 놓은 장바구니에 담아주세요."),
    TD_MSG_04("SSM02", "경비(관리)실에 맡겨주세요."),
    TD_MSG_05("SSM03", "도착 전 휴대폰으로 연락주세요."),
    TD_MSG_06("SSM04", "벨 누르지 마시고 노크해 주세요."),
    TD_MSG_07("SSM07","직접 받겠습니다."),
    TD_MSG_08("SSM06", "기타 직접입력");

    @Getter
    private final String code;

    @Getter
    private final String name;

}
