package kr.co.homeplus.admin.web.order.utils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.admin.web.order.enums.OrderDlvMsgCode;
import kr.co.homeplus.admin.web.order.enums.OrderNoRcvCode;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OrderUtil {

    public static HashMap<String, String> getDlvCodeMap(String category) {
        List<OrderDlvMsgCode> list = Stream.of(OrderDlvMsgCode.values()).filter(e -> e.name().contains(category.toUpperCase(Locale.KOREA))).collect(Collectors.toList());
        LinkedHashMap<String, String> returnMap = new LinkedHashMap<>();
        for(OrderDlvMsgCode code : list){
            returnMap.put(code.getCode(), code.getName());
        }
        return returnMap;
    }

    public static List<OrderDlvMsgCode> getDlvCodeList(String category) {
        return Stream.of(OrderDlvMsgCode.values())
            .filter(e -> e.name().contains(category.toUpperCase(Locale.KOREA)))
            .collect(Collectors.toList());
    }

    public static HashMap<String, String> getNoRcvCodeMap(String category) {
        return (HashMap<String, String>) Stream.of(OrderNoRcvCode.values())
            .filter(e -> e.name().contains(category.toUpperCase(Locale.KOREA)))
            .collect(Collectors.toMap(OrderNoRcvCode::getCode, OrderNoRcvCode::getName));
    }

}
