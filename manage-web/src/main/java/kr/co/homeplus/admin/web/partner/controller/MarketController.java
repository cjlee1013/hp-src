package kr.co.homeplus.admin.web.partner.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.privacylog.model.PersonalLogMethod;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyHeaderKey;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyLogInfo;
import kr.co.homeplus.admin.web.core.privacylog.service.PrivacyLogService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.partner.model.market.MarketCommissionListGetDto;
import kr.co.homeplus.admin.web.partner.model.market.MarketGetDto;
import kr.co.homeplus.admin.web.partner.model.market.MarketListGetDto;
import kr.co.homeplus.admin.web.partner.model.market.MarketParamDto;
import kr.co.homeplus.admin.web.partner.model.market.MarketSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.util.ServletUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Slf4j
@RequestMapping("/partner")
public class MarketController {



    private final ResourceClient resourceClient;
    private final LoginCookieService cookieService;
    private final CodeService codeService;


    private final PrivacyLogService privacyLogService;

    public MarketController(ResourceClient resourceClient,
        LoginCookieService cookieService,
        CodeService codeService,
        PrivacyLogService privacyLogService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
        this.privacyLogService = privacyLogService;
    }

    /**
     * 업체관리 > 마켓연동관리 > 마켓연동 업체 관리
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/marketMain")
    public String marketMain(Model model) throws Exception {

        codeService.getCodeModel(model,
            "use_yn"       // 사용여부
        );

        //그리드
        model.addAllAttributes(
            RealGridHelper.create("marketListGridBaseInfo", MarketListGetDto.class));

        model.addAllAttributes(
            RealGridHelper.create("marketCommissionGridBaseInfo", MarketCommissionListGetDto.class));

        return "/partner/marketMain";

    }

    /**
     * 마켓연동업체 리스트 조회
     * @param marketParamDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = {"/getMarketList.json"})
    public List<MarketListGetDto> getMarketList(@RequestBody MarketParamDto marketParamDto) {
        String apiUrl = "/partner/market/getMarketList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<MarketListGetDto>>>(){};
        return (List<MarketListGetDto>)resourceClient.postForResponseObject(ResourceRouteName.ITEM, marketParamDto, apiUrl, typeReference).getData();
    }

    /**
     * 마켓연동업체 조회
     * @param partnerId
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getMarket.json"})
    public MarketGetDto getMarket(final HttpServletRequest request,
        @RequestParam(name = "partnerId") String partnerId) {
        String apiUri = "/partner/market/getMarket";

        final String privacyAccessIp = ServletUtils.clientIP(request);
        final String privacyAccessUserId = cookieService.getUserInfo().getUserId();

        final PrivacyLogInfo logInfo = PrivacyLogInfo.builder()
            .txTime(DateTimeUtil.getNowMillYmdHis())
            .accessIp(privacyAccessIp)
            .userId(privacyAccessUserId)
            .txCodeUrl(request.getRequestURI())
            .txMethod(PersonalLogMethod.SELECT.name())
            .txCodeName("회원정보관리 목록조회")
            .processor(partnerId)
            .processorTaskSql("")
            .build();

        privacyLogService.send(logInfo);

        final HttpHeaders headers = new HttpHeaders();
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_IP, privacyAccessIp);
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_USER_ID, privacyAccessUserId);

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<MarketGetDto>>() {};
        return (MarketGetDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?partnerId=" + partnerId
            , headers, typeReference).getData();
    }

    /**
     * 마켓연동업체 등록/수정
     * @param marketSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/setMarket.json")
    public ResponseResult setMarket(@RequestBody MarketSetDto marketSetDto) throws Exception {
        String apiUrl = "/partner/market/setMarket";
        marketSetDto.setUserId(cookieService.getUserInfo().getEmpId());

        ResponseObject<ResponseResult> responseObject = resourceClient.postForResponseObject(ResourceRouteName.ITEM,  marketSetDto
            , apiUrl, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {});

        ResponseResult responseData = responseObject.getData();
        return responseData;
    }

    /**
     * 마켓연동업체 아이디 체크
     * @param partnerId
     * @param partnerType
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = {"/getMarketIdCheck.json"})
    public ResponseResult getPartnerId(
            @RequestParam(value = "partnerId") String partnerId,
            @RequestParam(value = "partnerType") String partnerType) throws Exception {
        StringBuffer uri = new StringBuffer();
        uri.append("/partner/market/getMarketIdCheck?partnerId=").append(partnerId)
            .append("&partnerType=").append(partnerType);

        ResponseObject<ResponseResult> responseObject  = resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , uri.toString()
            , new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {});
        return responseObject.getData();
    }

    /**
     * 마켓연동 수수료 리스트 조회
     * @param partnerId
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = {"/getMarketCommissionList.json"})
    public List<MarketCommissionListGetDto> getMarketCommissionList(
        @RequestParam(value = "partnerId") String partnerId) throws Exception {
        String apiUri = "/partner/market/getMarketCommissionList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<MarketCommissionListGetDto>>>() {};
        return (List<MarketCommissionListGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?partnerId=" + partnerId , typeReference).getData();
    }


    private PrivacyLogInfo write(final HttpServletRequest request, final String partnerId) {
        PrivacyLogInfo personalLogWrapper = PrivacyLogInfo.builder()
            .userId(cookieService.getUserInfo().getUserId())
            .accessIp(ServletUtils.clientIP(request))
            .processor(String.valueOf(partnerId))
            .processorTaskSql("")
            .txCodeName("마켓연동 업체관리")
            .txCodeUrl("/partner/getMarket.json")
            .txMethod(PersonalLogMethod.SELECT.name())
            .txTime(DateTimeUtil.getNowMillYmdHis())
            .build();
        privacyLogService.send(personalLogWrapper);
        return personalLogWrapper;
    }

}


