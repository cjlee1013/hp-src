package kr.co.homeplus.admin.web.item.model.itemGroup;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ItemGroupValidGetDto {

    private String itemNo;

    private String optSelUseYn;

    private String optSelUseYnNm;

    private String itemParent;

}
