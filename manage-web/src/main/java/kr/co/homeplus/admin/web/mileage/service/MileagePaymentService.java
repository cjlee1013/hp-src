package kr.co.homeplus.admin.web.mileage.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.privacylog.model.PersonalLogMethod;
import kr.co.homeplus.admin.web.order.utils.OrderLogUtil;
import kr.co.homeplus.admin.web.user.model.user.UserInfoDto;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import kr.co.homeplus.admin.web.mileage.model.payment.MileagePaymentModifySetDto;
import kr.co.homeplus.admin.web.mileage.model.payment.MileagePaymentSearchGetDto;
import kr.co.homeplus.admin.web.mileage.model.payment.MileagePaymentSearchSetDto;
import kr.co.homeplus.admin.web.mileage.model.payment.MileagePaymentSetDto;
import kr.co.homeplus.admin.web.mileage.model.payment.MileageReqDetailDto;
import kr.co.homeplus.admin.web.mileage.model.payment.MileageRequestCode;
import kr.co.homeplus.admin.web.mileage.model.payment.MileageRequestDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;


@Service
@RequiredArgsConstructor
public class MileagePaymentService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final ExcelUploadService excelUploadService;

    private int readTimeout = 1000*60*3;
    private int connectTimeout = 1000*60*3;
    private int connectionRequestTimeout = 1000*60*1000;

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 메인 리스트 조회
     */
    public List<MileagePaymentSearchGetDto> getMileageRequestList(MileagePaymentSearchSetDto listParamDto) throws Exception {
        ResourceClientRequest<List<MileagePaymentSearchGetDto>> request = ResourceClientRequest.<List<MileagePaymentSearchGetDto>>postBuilder()
            .apiId(ResourceRouteName.MILEAGE)
            .uri("/office/payment/getSearchMileagePaymentInfo")
            .postObject(listParamDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<List<MileagePaymentSearchGetDto>>> responseEntity = resourceClient.post(request, new TimeoutConfig());

        return responseEntity.getBody().getData();

        /*
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,ㅡ
            listParamDto,
            "/office/payment/getSearchMileagePaymentInfo",
            new ParameterizedTypeReference<ResponseObject<List<MileagePaymentSearchGetDto>>>(){}).getData();
        */
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 요청 등록
     */
    public ResponseResult setMileageRequest(MileagePaymentSetDto listParamDto) throws Exception {
        String regId = loginCookieService.getUserInfo().getEmpId();
        listParamDto.getMileageRequestDto().setRegId(regId);

        for(MileageReqDetailDto dto : listParamDto.getMileageReqDetailList()) {
            dto.setRegId(regId);
        }

        ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
            .apiId(ResourceRouteName.MILEAGE)
            .uri("/office/payment/setMileageRequestInfo")
            .postObject(listParamDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        TimeoutConfig timeoutConfig = new TimeoutConfig(readTimeout, connectTimeout, connectionRequestTimeout);
        ResponseEntity<ResponseObject<ResponseResult>> responseEntity = resourceClient.post(request, timeoutConfig);

        return responseEntity.getBody().getData();
/*
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            listParamDto,
            "/office/payment/setMileageRequestInfo",
            new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();

 */
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 요청 수정
     */
    public ResponseResult modifyMileageRequest(MileagePaymentModifySetDto listParamDto) throws Exception {
        String regId = loginCookieService.getUserInfo().getEmpId();
        listParamDto.getMileageRequestDto().setRegId(regId);

        for(MileageReqDetailDto dto : listParamDto.getMileageReqDetailList()) {
            dto.setRegId(regId);
            dto.setMileageDetailStatus(MileageRequestCode.MILEAGE_REQUEST_DETAIL_STATUS_WAIT.getResponseMessage());
        }

        ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
            .apiId(ResourceRouteName.MILEAGE)
            .uri("/office/payment/modifyMileageRequestInfo")
            .postObject(listParamDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        TimeoutConfig timeoutConfig = new TimeoutConfig(readTimeout, connectTimeout, connectionRequestTimeout);
        ResponseEntity<ResponseObject<ResponseResult>> responseEntity = resourceClient.post(request, timeoutConfig);

        return responseEntity.getBody().getData();

        /*
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            listParamDto,
            "/office/payment/modifyMileageRequestInfo",
            new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();
         */
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 기본 정보 조회
     */
    public MileageRequestDto getMileageRequestBasic(MileagePaymentModifySetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            listParamDto,
            "/office/payment/getSearchMileagePaymentInfo/basic/"+ listParamDto.getMileageReqNo(),
            new ParameterizedTypeReference<ResponseObject<MileageRequestDto>>(){}).getData();
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 대상 회원 조회
     */
    public List<MileageReqDetailDto> getMileageRequestDetail(MileagePaymentModifySetDto listParamDto) throws Exception {
        List<MileageReqDetailDto> mileageReqDetailDto = resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            listParamDto,
            "/office/payment/getSearchMileagePaymentInfo/user/"+ listParamDto.getMileageReqNo(),
            new ParameterizedTypeReference<ResponseObject<List<MileageReqDetailDto>>>(){}).getData();

        for (MileageReqDetailDto dto : mileageReqDetailDto) {
            dto.setMaskingUserNo(PrivacyMaskingUtils.maskingUserId(dto.getUserNo()));
            /*if (dto.getDetailMessage() != MileageRequestCode.MILEAGE_DETAIL_CODE_02.getResponseMessage()) {
                ResponseObject<UserInfoDto> returnDto = getUserNm(dto.getUserNo());
                if (returnDto.getData() == null) {
                    dto.setUserNm("");
                } else {
                    dto.setUserNm(PrivacyMaskingUtils.maskingUserName(returnDto.getData().getUserNm()));
                }
            }*/
        }

        return mileageReqDetailDto;
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 엑셀 업로드
     */
    public ResponseObject<Map<String, Object>> uploadExcel(HttpServletRequest httpServletRequest, String requestType) throws Exception {
        String fileName = "uploadFile";
        final MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) httpServletRequest;
        final MultipartFile multipartFile = multipartHttpServletRequest.getFile(fileName);

        List<MileageReqDetailDto> excelResultList; // 엑셀에 넣을 결과값 list
        ResponseObject<Map<String, Object>> responseResult = new ResponseObject<>();
        Map<String, Object> dataMap = new HashMap<>();

        ExcelHeaders headers = new ExcelHeaders.Builder()
            .header(0, "회원번호", "userNo", true) // 엑셀 헤더명과 POJO 필드명이 다를 경우 별개로 입력
            .header(1, "금액", "mileageAmt", true) // 엑셀 헤더명과 POJO 필드명이 다를 경우 별개로 입력
            .build();

        kr.co.homeplus.plus.excel.support.model.ExcelUploadOption<MileageReqDetailDto> excelUploadOption = new ExcelUploadOption.Builder<>(
            MileageReqDetailDto.class, headers, 1, 0)
            .maxRows(5000)
            .build();

        int successCnt = 0; int failCnt = 0; int totalCnt = 0;
        //Map<String, Long> userNos = new HashMap<String, Long>();
        String regId = loginCookieService.getUserInfo().getEmpId();
        String status = MileageRequestCode.MILEAGE_REQUEST_DETAIL_STATUS_WAIT.getResponseMessage();

        try {
            /**
             * 2. 엑셀 조회
             * multipartFile과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.
             */
            excelResultList = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
            totalCnt = excelResultList.size();

            for (MileageReqDetailDto dto : excelResultList) {
                /*
                if (StringUtils.isBlank(dto.getUserNo())) {
                    dto.setDetailMessage(MileageRequestCode.MILEAGE_DETAIL_CODE_02.getResponseMessage());
                    dto.setMileageDetailStatus(MileageRequestCode.MILEAGE_REQUEST_DETAIL_STATUS_FAIL.getResponseMessage());
                    failCnt++;
                    continue;
                } else {
                    try {
                        Long.parseLong(dto.getUserNo());
                        if (userNos.containsKey(dto.getUserNo())) {
                            dto.setDetailMessage(MileageRequestCode.MILEAGE_DETAIL_CODE_01.getResponseMessage());
                            dto.setMileageDetailStatus(MileageRequestCode.MILEAGE_REQUEST_DETAIL_STATUS_FAIL.getResponseMessage());
                            dto.setUseYn("N");
                            failCnt++;
                            continue;
                        }

                        if (dto.getMileageAmt() < 0 || dto.getMileageAmt() >= 10000000) {
                            dto.setDetailMessage(MileageRequestCode.MILEAGE_DETAIL_CODE_01.getResponseMessage());
                            dto.setMileageDetailStatus(MileageRequestCode.MILEAGE_DETAIL_CODE_04.getResponseMessage());
                            dto.setUseYn("N");
                            failCnt++;
                            continue;
                        }

                        userNos.put(dto.getUserNo(), dto.getMileageAmt());
                        ResponseObject<UserInfoDto> userInfo = getUserNm(dto.getUserNo());

                        if (userInfo.getData() == null) { // || userInfo.getData().getUserStatus() != "NORMAL"
                            dto.setDetailMessage(MileageRequestCode.MILEAGE_DETAIL_CODE_02.getResponseMessage());
                            dto.setMileageDetailStatus(MileageRequestCode.MILEAGE_REQUEST_DETAIL_STATUS_FAIL.getResponseMessage());
                            failCnt++;
                            continue;
                        } else {
                            dto.setUserNm(PrivacyMaskingUtils.maskingUserName(userInfo.getData().getUserNm()));
                        }
                    } catch (NumberFormatException e) {
                        dto.setDetailMessage(MileageRequestCode.MILEAGE_DETAIL_CODE_02.getResponseMessage());
                        dto.setMileageDetailStatus(MileageRequestCode.MILEAGE_REQUEST_DETAIL_STATUS_FAIL.getResponseMessage());
                        failCnt++;
                        continue;
                    }
                }
                dto.setUseYn("Y");
                */
                successCnt++;
                dto.setUseYn("Y");
                dto.setMaskingUserNo(dto.getUserNo());
                dto.setRegId(regId);
                dto.setMileageDetailStatus(status);
            }

            ObjectMapper mapper = new ObjectMapper();
            List<MileageReqDetailDto> excelFailList = mapper.convertValue(excelResultList, new TypeReference<List<MileageReqDetailDto>>() {})
                .stream()
                .filter(x -> ObjectUtils.isNotEmpty(x.getDetailMessage()))
                .collect(Collectors.toList());

            List<MileageReqDetailDto> excelSuccessList = mapper.convertValue(excelResultList, new TypeReference<List<MileageReqDetailDto>>() {})
                .stream()
                .filter(x -> ObjectUtils.isEmpty(x.getDetailMessage()))
                .collect(Collectors.toList());

            dataMap.put("failList", excelFailList);
            dataMap.put("successList", excelSuccessList);
            dataMap.put("failCnt", failCnt);
            dataMap.put("successCnt", successCnt);
            dataMap.put("totalCnt", totalCnt);
            dataMap.put("successList", excelSuccessList);
            responseResult.setData(dataMap);

            responseResult.setReturnCode("6000");
            responseResult.setReturnMessage("일괄 등록 완료되었습니다.");

        } catch (Exception e) {
            excelResultList = new ArrayList<>();
            MileageReqDetailDto dto = new MileageReqDetailDto();
            responseResult.setReturnCode("5000");
            responseResult.setReturnMessage("일괄 등록에 실패했습니다.");
            excelResultList.add(dto);
        }
        return responseResult;
    }

    /**
     * 결제관리 > 마일리지 관리 > 회원이름 조회
     */
    public ResponseObject<UserInfoDto> getUserNm(String userNo) throws Exception {new ArrayList<>();
        ResponseObject<UserInfoDto> responseDto =  resourceClient.getForResponseObject(
            ResourceRouteName.USERMNG, "/external/api/search/userInfo?userNo=" + userNo,
            OrderLogUtil.createPrivacySendAndHeader(PersonalLogMethod.SELECT, userNo, "마일리지 관리>회원조회"),
            new ParameterizedTypeReference<>() {}
        );
        UserInfoDto _userInfo = new UserInfoDto();
        if (responseDto.getData() == null) {
            responseDto.setData(_userInfo);
        } else {
            responseDto.getData().setUserNm(responseDto.getData().getUserNm());
        }
        return responseDto;
    }
}
