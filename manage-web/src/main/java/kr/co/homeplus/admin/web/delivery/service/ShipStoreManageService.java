package kr.co.homeplus.admin.web.delivery.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipStoreInfoGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipStoreManageListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipStoreManageListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipStoreManageService {

  private final ResourceClient resourceClient;
  private final ShipCommonService shipCommonService;

  /**
   * 점포배송관리 리스트 조회
   */
  public List<ShipStoreManageListGetDto> getShipStoreManageList(ShipStoreManageListSetDto shipStoreManageListSetDto) {

    if (shipCommonService.isStoreOffice()) {
      ShipStoreInfoGetDto storeInfo = shipCommonService.getStoreInfo(shipCommonService.getStoreId());

      shipStoreManageListSetDto.setSchStoreId(shipCommonService.getStoreId());
      shipStoreManageListSetDto.setSchStoreType(storeInfo.getStoreType());
    }

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        shipStoreManageListSetDto,
        "/admin/shipManage/getShipStoreManageList",
        new ParameterizedTypeReference<ResponseObject<List<ShipStoreManageListGetDto>>>(){}).getData();
  }
}
