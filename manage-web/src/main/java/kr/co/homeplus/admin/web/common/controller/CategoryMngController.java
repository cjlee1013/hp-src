package kr.co.homeplus.admin.web.common.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.categoryMng.CategoryGetDto;
import kr.co.homeplus.admin.web.common.model.categoryMng.CategoryMappingSelectBoxGetDto;
import kr.co.homeplus.admin.web.common.model.categoryMng.CategorySelectBoxGetDto;
import kr.co.homeplus.admin.web.common.model.categoryMng.CategorySelectDto;
import kr.co.homeplus.admin.web.common.model.categoryMng.CategorySetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.constants.RoleConstants;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.core.utility.GridUtil;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.category.categoryCommission.CategoryCommissionSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 카테고리 관리 시스템 > WEB
 */
@Slf4j
@Controller
public class CategoryMngController {

	final private ResourceClient resourceClient;

	final private LoginCookieService cookieService;

	final private CodeService codeService;

	final private AuthorityService authorityService;

	public CategoryMngController(ResourceClient resourceClient,
			LoginCookieService cookieService,
			CodeService codeService,
			AuthorityService authorityService) {
		this.resourceClient = resourceClient;
		this.cookieService = cookieService;
		this.codeService = codeService;

		this.authorityService = authorityService;
	}

	/**
	 * 카테고리관리 > 카테고리 등록/수정 > 페이지
	 *
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = { "/common/categoryMain"}, method = RequestMethod.GET)
	public String categoryMain(Model model) {

		codeService.getCodeModel(model,
				"use_yn"				// 사용여부
				, 	"limit_yn"					// 제한여부
				, 	"category_disp_yn"			// 노출범위
				, 	"isbn_yn"					// ISBN 노출여부
				, 	"review_display"			// 리뷰노출여부
		);

		model.addAttribute("CATEGORY_VIEW", authorityService.hasRole(RoleConstants.CATEGORY_VIEW) ? "Y" : "N");
		model.addAllAttributes(RealGridHelper.createForGroup("categoryGridBaseInfo", CategoryGetDto.class));

		return "/common/categoryMain";
	}

	/**
	 * 카테고리관리 > 카테고리 등록/수정 > 카테고리 리스트
	 *
	 * @param categorySelectDto
	 * @return Object
	 */
	@ResponseBody @RequestMapping(value = {"/common/category/getCategory.json"}, method = RequestMethod.GET)
	public List<Object> getCategory(CategorySelectDto categorySelectDto) throws Exception {

		String apiUri = "/common/CategoryMng/getCategory";
		apiUri += "?lcateCd=" + categorySelectDto.getLcateCd();
		apiUri += "&mcateCd=" + categorySelectDto.getMcateCd();
		apiUri += "&scateCd=" + categorySelectDto.getScateCd();
		apiUri += "&dcateCd=" + categorySelectDto.getDcateCd();

		ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<Map<String, Object>>>>() {};

		ResponseObject<List<Map<String, Object>>> responseObject = resourceClient.getForResponseObject(
				ResourceRouteName.ITEM, apiUri, typeReference);
		List<Map<String, Object>> responseData = responseObject.getData();

		return GridUtil.convertTreeGridData(responseData, null, "cateCd", "parentCateCd", "depth");
	}

	/**
	 * 카테고리관리 > 카테고리 등록/수정 > 카테고리 등록/수정 selectBox
	 *
	 * @param depth
	 * @param parentCateCd
	 * @return Map<String, Object>
	 */
	@ResponseBody @RequestMapping(value = {"/common/category/getAllCategoryForSelectBox.json"}, method = RequestMethod.GET)
	public List<CategorySelectBoxGetDto> getAllCategoryForSelectBox(
			@RequestParam(name = "depth") String depth,
			@RequestParam(name = "parentCateCd") String parentCateCd
			) {

		String apiUri = "/common/CategoryMng/getCategoryForSelectBox";
		apiUri += "?depth=" + depth;
		apiUri += "&parentCateCd=" + parentCateCd;
		List<CategorySelectBoxGetDto> data = (List<CategorySelectBoxGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<CategorySelectBoxGetDto>>>() {
		}).getData();

		return data;
	}

	/**
	 * 카테고리관리 > 카테고리 등록/수정 > 카테고리 등록/수정 mapping selectBox
	 *
	 * @param depth
	 * @param division
	 * @param groupNo
	 * @param dept
	 * @param classCd
	 * @return
	 */
	@ResponseBody @RequestMapping(value = {"/common/category/getCategoryForMappingSelectBox.json"}, method = RequestMethod.GET)
	public List<CategoryMappingSelectBoxGetDto> getCategoryForMappingSelectBox(
				@RequestParam(name = "depth") String depth
			,	@RequestParam(required = false, defaultValue = "") String division
			,	@RequestParam(required = false, defaultValue = "") String groupNo
			,	@RequestParam(required = false, defaultValue = "") String dept
			,	@RequestParam(required = false, defaultValue = "") String classCd
	) {

		String apiUri = "/common/CategoryMng/getCategoryForMappingSelectBox";
		apiUri += "?depth=" + depth;
		apiUri += "&division=" + division;
		apiUri += "&groupNo=" + groupNo;
		apiUri += "&dept=" + dept;
		apiUri += "&classCd=" + classCd;

		List<CategoryMappingSelectBoxGetDto> data = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<CategoryMappingSelectBoxGetDto>>>() {
		}).getData();

		return data;
	}

	/**
	 * 카테고리관리 > 카테고리 등록/수정 > 정적카테고리 생성
	 * @return
	 */
	@ResponseBody @RequestMapping(value = {"/common/category/setStaticCategory.json"}, method = RequestMethod.GET)
	public ResponseResult setStaticCategory( ) {

		String apiUri = "/common/CategoryMng/setStaticCategory";
		ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {};
		return (ResponseResult) resourceClient
				.getForResponseObject(ResourceRouteName.ITEM, apiUri, typeReference)
				.getData();

	}


	/**
	 * 공통 카테고리 리스트 (selectbox)
	 *
	 * @param depth
	 * @param parentCateCd
	 * @return Map<String, Object>
	 */
	@ResponseBody @RequestMapping(value = {"/common/category/getCategoryForSelectBox.json"}, method = RequestMethod.GET)
	public List<CategorySelectBoxGetDto> getCategoryForSelectBox(
			@RequestParam(name = "depth") String depth,
			@RequestParam(name = "parentCateCd") String parentCateCd,
			@RequestParam(name = "dispYnArr", required = false, defaultValue = "Y#A") String dispYnArr
			) {

		List<SetParameter> getParameterList = new ArrayList<>();
		getParameterList.add(SetParameter.create("depth", depth));
		getParameterList.add(SetParameter.create("parentCateCd", parentCateCd));
		getParameterList.add(SetParameter.create("dispYnArr", dispYnArr));
		getParameterList.add(SetParameter.create("useYn", "Y"));

		String apiUri = "/common/CategoryMng/getCategoryForSelectBox" + StringUtil.getParameter(getParameterList);

		List<CategorySelectBoxGetDto> data = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<CategorySelectBoxGetDto>>>() {
		}).getData();

		return data;
	}

	/**
	 * 카테고리관리 > 카테고리 등록/수정 > 카테고리 등록/수정
	 *
	 * @param categorySetDto
	 * @return Object
	 */
	@ResponseBody @RequestMapping(value = {"/common/category/setCategory.json"}, method = RequestMethod.GET)
	public ResponseResult setCategory(@ModelAttribute CategorySetDto categorySetDto) {
		categorySetDto.setRegId(cookieService.getUserInfo().getEmpId());
		categorySetDto.setCategoryWriteRole(true);

		String apiUri = "/common/CategoryMng/setCategory";
		ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
		};
		return (ResponseResult) resourceClient
			.postForResponseObject(ResourceRouteName.ITEM, categorySetDto, apiUri, typeReference)
			.getData();
	}

	/**
	 * 공통 카테고리 세분류 정보 조회
	 *
	 * @param dcateCd
	 * @return String
	 */
	@ResponseBody @RequestMapping(value = {"/common/category/getCategoryDivide.json"}, method = RequestMethod.GET)
	public CategoryGetDto getCategoryForSelectBox(
			@RequestParam(name = "dcateCd") String dcateCd) {
		List<SetParameter> setParameterList = new ArrayList<>();
		setParameterList.add(SetParameter.create("dcateCd", dcateCd));

		return resourceClient.getForResponseObject(
				ResourceRouteName.ITEM,
				"/common/CategoryMng/getCategoryDivide" + StringUtil.getParameter(setParameterList),
				new ParameterizedTypeReference<ResponseObject<CategoryGetDto>>() {
				}).getData();
	}

	/**
	 * 카테고리 뎁스별 상세조회
	 *
	 * @param depth
	 * @param cateCd
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = {"/common/category/getCategoryDetail.json"}, method = RequestMethod.GET)
	public CategoryGetDto getCategoryDetail(@RequestParam(value = "depth") String depth,
											@RequestParam(value = "cateCd") String cateCd) {
		List<SetParameter> setParameterList = new ArrayList<>();
		setParameterList.add(SetParameter.create("depth", depth));
		setParameterList.add(SetParameter.create("cateCd", cateCd));

		return resourceClient.getForResponseObject(
				ResourceRouteName.ITEM,
				"/common/CategoryMng/getCategoryDetail" + StringUtil.getParameter(setParameterList),
				new ParameterizedTypeReference<ResponseObject<CategoryGetDto>>() {}).getData();
	}

	/**
	 * 카테고리 수수료율 등록
	 *
	 * @param commissionSetDto
	 * @return ResponseResult
	 */
	@ResponseBody @RequestMapping(value = {"/common/category/setCategoryCommission.json"}, method = RequestMethod.GET)
	public ResponseResult setCategoryCommission(@ModelAttribute CategoryCommissionSetDto commissionSetDto) {
		commissionSetDto.setRegId(cookieService.getUserInfo().getEmpId());
		String apiUri = "/common/CategoryMng/setCategoryCommission";
		ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
		};
		return (ResponseResult) resourceClient
			.postForResponseObject(ResourceRouteName.ITEM, commissionSetDto, apiUri, typeReference)
			.getData();
	}

}
