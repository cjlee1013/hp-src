package kr.co.homeplus.admin.web.mileage.model.history;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class MileageListDto {
    @ApiModelProperty(value = "발생일시", position = 1)
    @RealGridColumnInfo(headText = "발생일시", width = 150)
    private String actDt;

    @ApiModelProperty(value = "마일리지타입번호", position = 2)
    @RealGridColumnInfo(headText = "마일리지타입번호", hidden = true)
    private String mileageTypeNo;

    @ApiModelProperty(value = "마일리지유형", position = 3)
    @RealGridColumnInfo(headText = "마일리지 유형")
    private String mileageTypeName;

    @ApiModelProperty(value = "마일리지종류", position = 4)
    @RealGridColumnInfo(headText = "마일리지 종류")
    private String mileageKind;

    @ApiModelProperty(value = "구분", position = 5)
    @RealGridColumnInfo(headText = "구분")
    private String mileageCategory;

    @ApiModelProperty(value = "내역", position = 6)
    @RealGridColumnInfo(headText = "내역", width = 200)
    private String historyMessage;

    @ApiModelProperty(value = "회원번호", position = 7)
    @RealGridColumnInfo(headText = "회원번호")
    private String userNo;

    @ApiModelProperty(value = "회원", position = 8)
    @RealGridColumnInfo(headText = "회원")
    private String userNm;

    @ApiModelProperty(value = "금액", position = 9)
    @RealGridColumnInfo(headText = "금액", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_CENTER)
    private long mileageAmt;
    
    @ApiModelProperty(value = "처리자", position = 10)
    @RealGridColumnInfo(headText = "처리자")
    private String chgId;

    @ApiModelProperty(value = "유효기간", position = 11)
    @RealGridColumnInfo(headText = "유효기간", width = 120)
    private String expireDt;
}
