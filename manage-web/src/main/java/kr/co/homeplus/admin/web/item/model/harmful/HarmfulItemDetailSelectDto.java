package kr.co.homeplus.admin.web.item.model.harmful;

import lombok.Data;

import java.util.List;

@Data
public class HarmfulItemDetailSelectDto {


    private String noDocument;

    private String seq;

    private String cdInsptmachi;

    private String nmProd;

    private String barcode;

    private String nmProdtype;

    private String unitPack;

    private String dtMake;

    private String prdValid;

    private String nmManufaccntr;

    private String nmManufacupso;

    private String telManufacupso;

    private String addrManufac;

    private String nmSalerupso;

    private String telSalerupso;

    private String addrSaler;

    private String nmRpttype;

    private String instituteTxt;

    private String nmInspttype;

    private String nmTakemachi;

    private String plcTake;

    private String dtTake;

    private String nmRptrmachi;

    private String nmReporter;

    private String telReporter;

    private String dtReport;

    private String recvDt;

    private String sendDt;

    private String statusResult;

    private String statusResultCode;

    private String sendStatus;

    private String imgAttach01;

    private String imgAttach02;

    private String imgAttach03;

    private String imgAttach04;

    private String imgAttach05;

    private List<HarmfulDSItemListSelectDto> harmfulDSItemListSelectDtoList;
}