package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 배분율 관리 > 결제정책 관리-결제수단 DTO")
public class PaymentPolicyMethodDto {
    @ApiModelProperty(value = "결제수단순번")
    private Long methodSeq;
    @ApiModelProperty(value = "정책번호")
    private Long policyNo;
    @ApiModelProperty(value = "결제수단코드(신한카드,현대카드,무통장등)")
    private String methodCd;
    @ApiModelProperty(value = "등록자id")
    private String regId;
    @ApiModelProperty(value = "등록일자")
    private String regDt;
}
