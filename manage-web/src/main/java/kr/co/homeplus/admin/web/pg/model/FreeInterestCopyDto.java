package kr.co.homeplus.admin.web.pg.model;

import java.util.List;
import lombok.Data;

@Data
public class FreeInterestCopyDto {
    private FreeInterestDto freeInterestDto;
    private List<FreeInterestApplyDto> freeInterestApplyDtoList;
    private String regId;
    private String regDt;
    private String chgId;
    private String chgDt;

}
