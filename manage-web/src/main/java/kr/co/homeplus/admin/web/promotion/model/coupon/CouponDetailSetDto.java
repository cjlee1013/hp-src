package kr.co.homeplus.admin.web.promotion.model.coupon;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CouponDetailSetDto {
    private Long couponNo;
    private String storeType;
    private String status;
    private Character withdrawYn;
    private Character useYn;
    private String manageCouponNm;
    private String displayCouponNm;
    private String departCd;
    private String departNm;
    private String couponType;
    private String purpose;
    private String purchaseStdDt;
    private Character displayItYn;
    private Character displayCzYn;
    private Character itemDuplYn;
    private String discountType;
    private Integer discountPrice;
    private Integer discountRate;
    private Integer discountMax;
    private Integer purchaseMin;
    private Character shareYn;
    private Integer shareHome;
    private Integer shareSeller;
    private Integer shareCard;
    private Character applyPcYn;
    private Character applyAppYn;
    private Character applyMwebYn;
    private String applyScope;
    private String issueStartDt;
    private String issueEndDt;
    private String issueLimitType;
    private Integer issueMaxCnt;
    private Integer limitPerCnt;
    private Character limitPerDayYn;
    private Integer limitPerDayCnt;
    private Character issueTimeYn;
    private String issueStartTime;
    private String issueEndTime;
    private String validType;
    private Integer validDay;
    private Integer validTime;
    private String validStartDt;
    private String validEndDt;
    private Character shipSlotYn;
    private String slotStartTime;
    private String slotEndTime;
    private Character validMon;
    private Character validTue;
    private Character validWed;
    private Character validThu;
    private Character validFri;
    private Character validSat;
    private Character validSun;
    private Character combineOrderAvailYn;
    private Character pickupAvailYn;
    private String barcode;
    private String cartBarcode;
    private String barcodeCouponSeq;
    private String randomNoIssueYn;
    private String randomNoType;
    private String randomNo;
    private String gradeCouponYn;
    private String gradeCouponMonth;
    private Integer userGradeSeq;
    private String paymentType;

    //점포리스트
    private List<PromotionStoreDto> storeList;

    //결제수단리스트
    private List<PromotionPaymentDto> paymentList;

    //적용대상
    private List<String> createApplyList;
    private List<String> updateApplyList;
    private List<String> deleteApplyList;

    //제외상품
    private List<String> createExceptItemList;
    private List<String> deleteExceptItemList;

    private String empNo;
    private String empNm;
}
