package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClaimDetailInfoSetDto {


  @NotNull(message = "그룹클레임번호가 없습니다.")
  @ApiModelProperty(value = "그룹클레임번호", position = 1)
  private String claimBundleNo;

  @NotNull(message = "클레임번호가 없습니다.")
  @ApiModelProperty(value = "클레임번호", position = 2)
  public String claimNo;

  @NotNull(message = "주문번호가 없습니다.")
  @ApiModelProperty(value = "주문번호", position = 3)
  public String purchaseOrderNo;

  @ApiModelProperty(value = "클레임 타입", position = 4)
  public String claimType;
}
