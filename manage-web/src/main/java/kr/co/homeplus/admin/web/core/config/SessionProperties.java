package kr.co.homeplus.admin.web.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * application.yml 설정을 바인딩 하는 프로퍼티<p>
 * session 설정을 관리하는 프로퍼티로  {@link ConfigurationProperties} 사용으로 추후 spring cloud config 사용 가능하도록 변경.
 *
 * @see ConfigurationProperties
 */
@Data
@Component
@ConfigurationProperties(prefix = "session")
public class SessionProperties {
    private int expirationHour;
}
