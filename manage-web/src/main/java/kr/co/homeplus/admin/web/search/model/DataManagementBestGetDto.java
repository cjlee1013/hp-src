package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class DataManagementBestGetDto {
    private Object query;
    private long totalCount;
    private List<Map<String, Object>> dataList;
}
