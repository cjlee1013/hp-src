package kr.co.homeplus.admin.web.manage.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.brandZone.BrandZoneItemListGetDto;
import kr.co.homeplus.admin.web.manage.model.brandZone.BrandZoneListGetDto;
import kr.co.homeplus.admin.web.manage.model.brandZone.BrandZoneParamDto;
import kr.co.homeplus.admin.web.manage.model.brandZone.BrandZoneSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/manage/brandZone")
@Slf4j
public class BrandZoneController {

    private final CodeService codeService;
    private final ResourceClient resourceClient;
    private final LoginCookieService cookieService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    public BrandZoneController(ResourceClient resourceClient,
        LoginCookieService cookieService,
        CodeService codeService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;

    }

    @RequestMapping(value = "/brandZoneMain", method = RequestMethod.GET)
    public String BrandZoneMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"    // 사용여부
            ,"site_type"
            ,"mall_type"
        );
        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("siteType", code.get("site_type"));
        model.addAttribute("mallType" , code.get("mall_type"));
        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);
        model.addAllAttributes(RealGridHelper.create("brandZoneListGridBaseInfo", BrandZoneListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("brandZoneItemListGridBaseInfo", BrandZoneItemListGetDto.class));

        return "/manage/brandZoneMain";
    }

    @ResponseBody
    @RequestMapping(value = {"/getBrandZoneList.json"}, method = RequestMethod.GET)
    public List<BrandZoneListGetDto> getBrandZoneList(BrandZoneParamDto brandZoneParamDto) throws Exception {
        String apiUri = "/manage/brandZone/getBrandZoneList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<BrandZoneListGetDto>>>() {};
        return (List<BrandZoneListGetDto>) resourceClient
            .getForResponseObject(ResourceRouteName.MANAGE, StringUtil
                    .getRequestString(apiUri, BrandZoneParamDto.class, brandZoneParamDto),
                typeReference).getData();
    }

    @ResponseBody
    @RequestMapping(value = {"/getBrandZoneItemList.json"}, method = RequestMethod.GET)
    public List<BrandZoneItemListGetDto> getBrandZoneItemList (@RequestParam(value="brandzoneNo") Long brandzoneNo) throws Exception {
        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , "/manage/brandZone/getBrandZoneItemList?brandzoneNo=" + brandzoneNo
            , new ParameterizedTypeReference<ResponseObject<List<BrandZoneItemListGetDto>>>(){}).getData();
    }

    @ResponseBody
    @RequestMapping(value = {"/setBrandZone.json"}, method = RequestMethod.POST)
    public ResponseResult setBrandZone(@RequestBody BrandZoneSetDto brandZoneSetDto) throws Exception {
        String apiUri = "/manage/brandZone/setBrandZone";
        brandZoneSetDto.setUserId(cookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, brandZoneSetDto,
            apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();

    }

}