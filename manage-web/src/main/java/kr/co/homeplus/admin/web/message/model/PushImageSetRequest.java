package kr.co.homeplus.admin.web.message.model;

import lombok.Data;

@Data
public class PushImageSetRequest {

    /**
     * 이미지 이름
     */
    private String imageName;
    /**
     * 이미지 원본 이름
     */
    private String originName;
    /**
     * 이미지 url
     */
    private String url;
    /**
     * 등록자
     */
    private String regId;
}
