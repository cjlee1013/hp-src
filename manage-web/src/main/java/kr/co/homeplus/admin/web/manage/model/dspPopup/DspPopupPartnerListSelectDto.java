package kr.co.homeplus.admin.web.manage.model.dspPopup;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
@Getter
@Setter
public class DspPopupPartnerListSelectDto {

    @RealGridColumnInfo(headText = "판매자 ID", columnType = RealGridColumnType.NAME, width = 120, sortable = true)
    private String partnerId;

    @RealGridColumnInfo(headText = "상호", columnType = RealGridColumnType.NAME, width = 150, order = 1)
    private String partnerNm;

    @RealGridColumnInfo(headText = "업체명", columnType = RealGridColumnType.NAME, width = 150, order = 2)
    private String businessNm;

    @RealGridColumnInfo(headText = "사업자 등록번호", width = 120, order = 3)
    private String partnerNo;

    @RealGridColumnInfo(headText = "회원상태", columnType = RealGridColumnType.NAME, width = 80, order = 5)
    private String partnerStatusNm;


    private String useYn;

}