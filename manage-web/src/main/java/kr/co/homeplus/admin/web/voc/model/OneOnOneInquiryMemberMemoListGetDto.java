package kr.co.homeplus.admin.web.voc.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 1:1문의 회원메모 응답 DTO")
public class OneOnOneInquiryMemberMemoListGetDto {
  @ApiModelProperty(notes = "번호")
  @RealGridColumnInfo(headText = "번호", width = 80, sortable = true)
  private long num;

  @ApiModelProperty(notes = "회원메모 일련번호")
  @RealGridColumnInfo(headText = "회원메모 일련번호", width = 80, sortable = true, hidden = true)
  private long seq;

  @ApiModelProperty(notes = "회원번호")
  @RealGridColumnInfo(headText = "회원번호", width = 100, sortable = true, hidden = true)
  private long userNo;

  @ApiModelProperty(notes = "내용")
  @RealGridColumnInfo(headText = "내용", width = 300, sortable = true, columnType = RealGridColumnType.NAME)
  private String content;

  @ApiModelProperty(notes = "등록자ID")
  @RealGridColumnInfo(headText = "등록자ID", width = 100, sortable = true, hidden = true)
  private String regEmpId;

  @ApiModelProperty(notes = "등록자")
  @RealGridColumnInfo(headText = "등록자", width = 100, sortable = true)
  private String regEmpNm;

  @ApiModelProperty(notes = "등록일")
  @RealGridColumnInfo(headText = "등록일", width = 150, sortable = true)
  private String regDt;
}