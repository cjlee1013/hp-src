package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeywordAttrMngDto {
    private Long keywordAttrMngNo;
    private Long keywordAttrNo;
    private Long gattrNo;
    private Long priority;
    private Long attrNo;
}
