package kr.co.homeplus.admin.web.pg.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum SiteType {
    HOME("HOME","홈플러스"),
//    [ESCROW-343, CLUB 노출제외]
//    CLUB("CLUB","더클럽"),
    ;

    private String code;
    private String name;
}
