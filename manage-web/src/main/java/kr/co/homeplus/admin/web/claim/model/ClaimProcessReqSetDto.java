package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimProcessReqSetDto {

    @NotNull(message = "클레임번호가 없습니다.")
    @ApiModelProperty(value = "클레임번호", position = 1)
    private long claimNo;

    @NotNull(message = "클레임번들번호가 없습니다.")
    @ApiModelProperty(value = "클레임번들번호", position = 2)
    private long claimBundleNo;

    @NotNull(message = "클레임요청번호가 없습니다.")
    @ApiModelProperty(value = "클레임요청번호", position = 3)
    private long claimReqNo;

    @NotNull(message = "클레임 상태가 없습니다.")
    @ApiModelProperty(value = "클레임 상태(CA:승인, CW:철회, CH:보류, CD:거부)", position = 4)
    private String claimReqType;

    @NotNull(message = "클레임타입이 없습니다.")
    @ApiModelProperty(value = "클레임타입(C:취소/R:반품/X:교환)", position = 5)
    private String claimType;

    @ApiModelProperty(value = "사유코드", position = 6)
    private String reasonType;

    @ApiModelProperty(value = "상세사유", position = 7)
    private String reasonTypeDetail;

    @ApiModelProperty(value = "등록자", position = 8)
    private String regId;

}
