package kr.co.homeplus.admin.web.manage.model.recomSellerShop;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class RecomSellerShopListParamDto {


    private String searchStartDt;

    private String searchEndDt;

    private String searchType;

    private String searchKeyword;

}
