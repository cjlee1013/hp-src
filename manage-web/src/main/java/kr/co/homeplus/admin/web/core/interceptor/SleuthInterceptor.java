package kr.co.homeplus.admin.web.core.interceptor;

import brave.Tracer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class SleuthInterceptor extends HandlerInterceptorAdapter {

	private Tracer tracer;

	public SleuthInterceptor(Tracer tracer) {
		this.tracer = tracer;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		final String traceId = tracer.currentSpan().context().traceIdString();
		final String spanId = tracer.currentSpan().context().spanIdString();
		sleuthTraceGuidVoidCall(traceId);
		sleuthSpanGuidVoidCall(spanId);
		return super.preHandle(request, response, handler);
	}

	private void sleuthTraceGuidVoidCall(String traceId) {
	}

	private void sleuthSpanGuidVoidCall(String spanId) {
	}
}
