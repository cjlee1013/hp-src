package kr.co.homeplus.admin.web.common.model.pop;

import kr.co.homeplus.admin.web.message.enums.ShipStatus;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class OrderSendListInfo {

    private long purchaseOrderNo;

    private String bundleNo;

    private String storeId;

    private String storeNm;

    private String userNo;
    
    private String buyerNm;

    private String buyerMobileNo;

    private String receiverNm;

    private String shipMobileNo;

    private String orderDt;

    private String shipDt;

    private String shiftId;

    private String slotId;

    private ShipStatus shipStatus;

    // 마스킹 처리가 안 된 데이터
    private String rawBuyerNm;

    private String rawBuyerMobileNo;

    private String rawReceiverNm;

    private String rawShipMobileNo;

    public void setMasking() {
        String buyerNm = getBuyerNm();
        String buyerMobileNo = getBuyerMobileNo();
        String receiverNm = getReceiverNm();
        String shipMobileNo = getShipMobileNo();

        if (StringUtils.isNotEmpty(buyerNm)) {
            this.rawBuyerNm = buyerNm;
            setBuyerNm(PrivacyMaskingUtils.maskingUserName(buyerNm));
        }
        if (StringUtils.isNotEmpty(buyerMobileNo)) {
            this.rawBuyerMobileNo = buyerMobileNo;
            setBuyerMobileNo(PrivacyMaskingUtils.maskingPhone(buyerMobileNo));
        }
        if (StringUtils.isNotEmpty(receiverNm)) {
            this.rawReceiverNm = receiverNm;
            setReceiverNm(PrivacyMaskingUtils.maskingUserName(receiverNm));
        }
        if (StringUtils.isNotEmpty(shipMobileNo)) {
            this.rawShipMobileNo = shipMobileNo;
            setShipMobileNo(PrivacyMaskingUtils.maskingPhone(shipMobileNo));
        }
    }
}
