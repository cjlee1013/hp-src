package kr.co.homeplus.admin.web.order.model.ship;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "주문 배송 원정보 DTO")
public class OrderShipOriginInfo {
    @ApiModelProperty(value = "주문번호")
    private long purchaseOrderNo;
    @ApiModelProperty(value = "배송번호")
    private long bundleNo;
    @ApiModelProperty(value = "주문타입")
    private String orderType;
    @ApiModelProperty(value = "원주문번호")
    private long orgPurchaseOrderNo;
    @ApiModelProperty(value = "원배송번호")
    private long orgBundleNo;
}
