package kr.co.homeplus.admin.web.core.certification;

import kr.co.homeplus.admin.web.core.constants.AdminConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.dto.loginlog.LoginLogParam;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoginLogService {

    private final ResourceClient resourceClient;

    /**
     * 어드민 로그인 로그 저장
     * @param userId 로그인 아이디
     * @param clientIp 로그인 아이피
     */
    public void insertLoginLog(final String userId, final String empId, final String clientIp) {
        ResourceClientRequest<Boolean> request = ResourceClientRequest.<Boolean>postBuilder()
            .apiId(ResourceRouteName.AUTHORITY)
            .uri(AdminConstants.ADMIN_LOG_INSERT_LOGIN_LOG)
            .postObject(new LoginLogParam(userId, empId, clientIp))
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        log.info("admin insert login log:<{}>,<{}>,<{}>", userId, empId, clientIp);

        ResponseObject<Boolean> responseObject = resourceClient
            .post(request, AdminConstants.ADMIN_DEFAULT_TIMEOUT_CONFIG).getBody();

        if (responseObject != null && isNotResponseSuccess(responseObject.getReturnCode())) {
            log.error("admin insert login log failed:<{}>,<{}>,<{}>", userId, clientIp,
                responseObject.getReturnMessage());
        }
    }

    /**
     * 응답이 성공이 아닐경우 true
     * @param returnCode 리턴코드
     * @return 응답이 성공이 아닐 경우 true, 성공일 경우 false를 리턴합니다.
     */
    private boolean isNotResponseSuccess(final String returnCode) {
        return !returnCode.equals(ResponseObject.RETURN_CODE_SUCCESS);
    }
}
