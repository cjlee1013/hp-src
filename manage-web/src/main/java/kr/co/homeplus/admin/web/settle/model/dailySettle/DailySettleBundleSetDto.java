package kr.co.homeplus.admin.web.settle.model.dailySettle;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DailySettleBundleSetDto {
    @ApiModelProperty(value = "정산번호")
    private long settleNo;

    @ApiModelProperty(value = "파트너ID")
    private String partnerId;

    @ApiModelProperty(value = "정산주기")
    private String settleCycleType;

    @ApiModelProperty(value = "매출일")
    private String basicDt;

    @ApiModelProperty(value = "상품번호")
    private long itemNo;
}
