package kr.co.homeplus.admin.web.promotion.model.event;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EtcPromoDetailGetDto {
    private Long etcNo;

    private String promoType;

    private String storeType;

    private String etcMngNm;

    private String etcInfo;

    private String promoStartDt;

    private String promoEndDt;

    private String useYn;

    private String dispStoreType;

    // 노출 점포 리스트
    private List<EtcPromoStoreGetDto> storeList;

    // 적용상품 리스트
    private List<EtcPromoItemGetDto> itemList;
}