package kr.co.homeplus.admin.web.item.model.substitute;


import lombok.Data;

@Data
public class RestrictionItemListParamDto {

    private Integer searchCateCd1;

    private Integer searchCateCd2;

    private Integer searchCateCd3;

    private Integer searchCateCd4;

    private String searchStartDt;

    private String searchEndDt;

    private String searchUseYn;

    private String searchType;

    private String searchKeyword;

}
