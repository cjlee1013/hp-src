package kr.co.homeplus.admin.web.order.model.orderSearch;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "주문정보 상세 - 택배배송정보 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class OrderSearchDlvShipDto {

    @ApiModelProperty(value = "점포유형")
    @RealGridColumnInfo(headText = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "배송번호")
    @RealGridColumnInfo(headText = "배송번호")
    private long bundleNo;

    @ApiModelProperty(value = "상품 주문번호")
    @RealGridColumnInfo(headText = "상품 주문번호")
    private long orderItemNo;

    @ApiModelProperty(value = "다중배송번호")
    @RealGridColumnInfo(headText = "박스번호", hidden = true)
    private String multiBundleNo;

    @ApiModelProperty(value = "배송상태")
    @RealGridColumnInfo(headText = "배송상태", width = 120)
    private String shipStatusNm;

    @ApiModelProperty(value = "미수취신고상태")
    @RealGridColumnInfo(headText = "미수취신고상태")
    private String notReceiveType;

    @ApiModelProperty(value = "수령인")
    @RealGridColumnInfo(headText = "수령인")
    private String receiverNm;

    @ApiModelProperty(value = "수령인연락처")
    @RealGridColumnInfo(headText = "수령인연락처", width = 110)
    private String shipMobileNo;

    @ApiModelProperty(value = "안심번호")
    @RealGridColumnInfo(headText = "안심번호", width = 110)
    private String issuePhoneNo;

    @ApiModelProperty(value = "주소")
    @RealGridColumnInfo(headText = "주소", width = 300, columnType = RealGridColumnType.NONE)
    private String shippingAddr;

    @ApiModelProperty(value = "판매업체ID", hidden = true)
    @RealGridColumnInfo(headText = "판매업체ID", hidden = true)
    private String partnerId;

    @ApiModelProperty(value = "판매업체")
    @RealGridColumnInfo(headText = "판매업체", width = 120)
    private String partnerNm;

    @ApiModelProperty(value = "배송방법")
    @RealGridColumnInfo(headText = "배송방법")
    private String shipMethodNm;

    @ApiModelProperty(value = "택배사코드", hidden = true)
    @RealGridColumnInfo(headText = "택배사코드", hidden = true)
    private String dlvCd;

    @ApiModelProperty(value = "택배사")
    @RealGridColumnInfo(headText = "택배사")
    private String dlvNm;

    @ApiModelProperty(value = "송장번호")
    @RealGridColumnInfo(headText = "송장번호", width = 120)
    private String invoiceNo;

    @ApiModelProperty(value = "발송기한")
    @RealGridColumnInfo(headText = "발송기한", width = 110)
    private String orgShipDt;

    @ApiModelProperty(value = "2차발송기한")
    @RealGridColumnInfo(headText = "2차발송기한", width = 110)
    private String delayShipDt;

    @ApiModelProperty(value = "배송요청일")
    @RealGridColumnInfo(headText = "배송요청일", width = 110)
    private String shipDt;

    @ApiModelProperty(value = "배송예정일")
    @RealGridColumnInfo(headText = "배송예정일", width = 110)
    private String scheduleShipDt;

    @ApiModelProperty(value = "주문확인일")
    @RealGridColumnInfo(headText = "주문확인일", width = 110)
    private String confirmDt;

    @ApiModelProperty(value = "발송처리일")
    @RealGridColumnInfo(headText = "발송처리일", width = 110)
    private String shippingDt;

    @ApiModelProperty(value = "배송완료일")
    @RealGridColumnInfo(headText = "배송완료일", width = 110, columnType = RealGridColumnType.NAME)
    private String completeDt;

    @ApiModelProperty(value = "배송상태코드", hidden = true)
    @RealGridColumnInfo(hidden = true, headText = "배송상태코드")
    private String shipStatus;

    @ApiModelProperty(value = "배송방법코드", hidden = true)
    @RealGridColumnInfo(headText = "배송방법코드", hidden = true)
    private String shipMethod;

    @ApiModelProperty(value = "클레임상태", hidden = true)
    @RealGridColumnInfo(headText = "클레임상태", hidden = true)
    private String claimStatus;

    @ApiModelProperty(value = "점포종류(NOR:일반점,DLV:택배점)", hidden = true)
    @RealGridColumnInfo(headText = "점포종류", hidden = true)
    private String storeKind;

    @ApiModelProperty(value = "고객번호", hidden = true)
    @RealGridColumnInfo(headText = "고객번호", hidden = true)
    private String userNo;

    @ApiModelProperty(value = "운송번호", hidden = true)
    @RealGridColumnInfo(headText = "운송번호", hidden = true)
    private String shipNo;

    @ApiModelProperty(value = "주문수량(클레임건차감)", hidden = true)
    @RealGridColumnInfo(headText = "주문수량(클레임건차감)", hidden = true)
    private String itemQty;

    @ApiModelProperty(value = "상품배송가능지역", hidden = true)
    @RealGridColumnInfo(headText = "상품배송가능지역", hidden = true)
    private String shipAreaType;

}
