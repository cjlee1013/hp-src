package kr.co.homeplus.admin.web.manage.model.dspMainTheme;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspMainThemeListParamDto {

    private String searchPeriodType;
    private String searchStartDt;
    private String searchEndDt;
    private String searchSiteType;
    private String searchUseYn;
    private String searchType;
    private String searchKeyword;

}
