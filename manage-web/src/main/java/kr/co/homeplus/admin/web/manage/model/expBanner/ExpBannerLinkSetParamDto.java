package kr.co.homeplus.admin.web.manage.model.expBanner;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpBannerLinkSetParamDto {

    private Long bannerNo;
    private Long linkNo;
    private String linkType;
    private String linkInfo;
    private String linkOptions;
    private String dlvStoreId;
    private String dlvStoreYn;
    private String storeType;
    private String textYn;
    private String title;
    private String subTitle;
    private String imgUrl;
    private int imgWidth;
    private int imgHeight;
    private String useYn = "Y";
    private String regId;

}
