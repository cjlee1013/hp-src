package kr.co.homeplus.admin.web.pg.controller;

import java.util.Arrays;
import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.pg.model.FreeInterestApplyDto;
import kr.co.homeplus.admin.web.pg.model.FreeInterestCopyDto;
import kr.co.homeplus.admin.web.pg.model.FreeInterestDto;
import kr.co.homeplus.admin.web.pg.model.FreeInterestSelectDto;
import kr.co.homeplus.admin.web.pg.model.SiteType;
import kr.co.homeplus.admin.web.pg.service.FreeInterestService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/pg/paymentMethod")
public class FreeInterestController {
    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    private final FreeInterestService freeInterestService;

    public FreeInterestController(LoginCookieService loginCookieService, AuthorityService authorityService, FreeInterestService freeInterestService) {
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;
        this.freeInterestService = freeInterestService;
    }

    /**
     * 무이자 혜택 관리 메인페이지 호출
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/freeInterestMngMain")
    public String freeInterestMain(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_PAY_COMMON)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        //SiteType Enum
        model.addAttribute("siteTypeList", Arrays.asList(SiteType.values()));
        model.addAttribute("paymentMethodList", freeInterestService.getDistinctMethodCd());
        model.addAllAttributes(RealGridHelper.create("freeInterestGridBaseInfo", FreeInterestDto.class));
        model.addAllAttributes(RealGridHelper.create("freeInterestApplyGridBaseInfo", FreeInterestApplyDto.class));

        return "/pg/freeInterestMngMain";
    }

    /**
     * 무이자혜택 기본정보 조회
     * @param freeInterestSelectDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/getFreeInterestMngList.json")
    public List<FreeInterestDto> getFreeInterestMngList(@ModelAttribute FreeInterestSelectDto freeInterestSelectDto) throws Exception {
        return freeInterestService.getFreeInterestMngList(freeInterestSelectDto);
    }


    /**
     * 무이자혜택 적용대상 조회
     * @param freeInterestMngSeq
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/getFreeInterestApplyList.json")
    public List<FreeInterestApplyDto> getFreeInterestApplyList(@RequestParam long freeInterestMngSeq) throws Exception {
        return freeInterestService.getFreeInterestApplyList(freeInterestMngSeq);
    }

    /**
     * 카드무이자혜택 저장
     * @param freeInterestDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/saveFreeInterest.json")
    public Integer saveFreeInterest(@RequestBody FreeInterestDto freeInterestDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        freeInterestDto.setRegId(userCd);
        freeInterestDto.setChgId(userCd);
        return freeInterestService.saveFreeInterest(freeInterestDto);
    }

    /**
     * 카드무이자혜택 저장
     * @param freeInterestApplyDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/saveFreeInterestApply.json")
    public Integer saveFreeInterestApply(@RequestBody FreeInterestApplyDto freeInterestApplyDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        freeInterestApplyDto.setRegId(userCd);
        return freeInterestService.saveFreeInterestApply(freeInterestApplyDto);
    }

    /**
     * 무이자혜택복사 저장
     * @param freeInterestCopyDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/saveCopyFreeInterest.json")
    public Integer saveCopyCardNointerest(@RequestBody FreeInterestCopyDto freeInterestCopyDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        freeInterestCopyDto.setRegId(userCd);
        freeInterestCopyDto.setChgId(userCd);
        return freeInterestService.saveCopyFreeInterest(freeInterestCopyDto);
    }

    /**
     * 카드무이자혜택 삭제
     * @param freeInterestApplyDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/deleteFreeInterestApply.json")
    public Integer deleteFreeInterestApply(@RequestBody FreeInterestApplyDto freeInterestApplyDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        freeInterestApplyDto.setRegId(userCd);
        return freeInterestService.deleteFreeInterestApply(freeInterestApplyDto);
    }


}
