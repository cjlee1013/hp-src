package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class DeployHistoryGetParam {
    @ApiModelProperty(notes = "검색 시작일", required = true)
    @NotEmpty
    private String startDeployDt;
    @ApiModelProperty(notes = "검색 종료일", required = true)
    @NotEmpty
    private String endDeployDt;
    @ApiModelProperty(notes = "기능유형")
    private String actionType;
    @ApiModelProperty(notes = "사전유형")
    private String dictType;
    @ApiModelProperty(notes = "성공여부")
    private String successYn;
    @ApiModelProperty(notes = "서비스유형", required = true)
    private String service;
    @ApiModelProperty(notes = "플랫폼유형")
    private String appType;
}
