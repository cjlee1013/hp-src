package kr.co.homeplus.admin.web.user.model.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ApiModel(description = "회원정보 상세 복호화 및 마스킹 처리대상 파라미터")
public class UserDecryptParam {
    @ApiModelProperty(value = "계좌번호 - 마스킹 처리")
    private String bankAccount;
    @ApiModelProperty(value = "MHC 대체카드번호 암호화")
    private String mhcCardnoEnc;
}
