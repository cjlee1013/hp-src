package kr.co.homeplus.admin.web.item.model.item;

import lombok.Data;

@Data
//상품관리 동영상 Get Entry
public class ItemVideoGetDto {
    //파일시퀀스
    private String fileSeq;

    //파일유형
    private String fileType;

    //파일명
    private String fileNm;

    //파일URL
    private String fileUrl;

    //확장자
    private String fileExt;
}
