package kr.co.homeplus.admin.web.core.exception;

import kr.co.homeplus.admin.web.core.dto.LoginParam;

public class CertificationException extends RuntimeException {
    private LoginParam loginParam;

    public CertificationException(Exception e) {
        super(e);
    }

    public CertificationException(String message) {
        super(message);
    }

    public CertificationException(LoginParam loginParam, String message) {
        super(message);
        this.loginParam = loginParam;
    }

    public LoginParam getLoginParam() {
        return loginParam;
    }
}
