package kr.co.homeplus.admin.web.item.model.recomMsg;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemRecomMsgListParamDto {
    private String schDateType;

    private String schStartDt;

    private String schEndDt;

    private String schStoreType;

    private String schUseYn;

    private String schType;

    private String schValue;
}