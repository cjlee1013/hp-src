package kr.co.homeplus.admin.web.partner.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerSellerShipListGetDto {

    private Long shipPolicyNo;
    private String defaultYn;
    private String shipPolicyNm;
    private Integer shipFee;
    private Integer claimShipFee;
    private String regNm;
    private String regDt;
    private String chgNm;
    private String chgDt;
}
