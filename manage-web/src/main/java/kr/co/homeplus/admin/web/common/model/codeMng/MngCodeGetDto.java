package kr.co.homeplus.admin.web.common.model.codeMng;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class MngCodeGetDto {
	@RealGridColumnInfo(headText = "유형코드", width = 130, hidden = true, sortable = true)
	private String gmcCd;
	@RealGridColumnInfo(headText = "코드", width = 130, sortable = true)
	private String mcCd;
	@RealGridColumnInfo(headText = "코드명", width = 130, sortable = true)
	private String mcNm;
	@RealGridColumnInfo(headText = "참조1", width = 130, sortable = true)
	private String ref1;
	@RealGridColumnInfo(headText = "참조2", width = 130, sortable = true)
	private String ref2;
	@RealGridColumnInfo(headText = "참조3", width = 130, sortable = true)
	private String ref3;
	@RealGridColumnInfo(headText = "참조4", width = 130, sortable = true)
	private String ref4;
	@RealGridColumnInfo(headText = "이전 시스템 상세코드", width = 130, sortable = true)
	private String lgcCodeCd;
	@RealGridColumnInfo(headText = "이전 시스템 상위코드", width = 130, sortable = true)
	private String lgcUpcodeCd;
	@RealGridColumnInfo(headText = "우선순위", width = 130, sortable = true)
	int	priority;
	@RealGridColumnInfo(headText = "사용여부", width = 130, sortable = true)
	private String useYn;
}
