package kr.co.homeplus.admin.web.mileage.controller;


import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponInfoDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponModifyDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponOneTimeDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponRequestDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponReturnGetDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponReturnSetDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponUserInfoDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponSearchGetDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponSearchSetDto;
import kr.co.homeplus.admin.web.mileage.model.payment.MileagePaymentSetDto;
import kr.co.homeplus.admin.web.mileage.service.MileageCouponService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping("/mileage/coupon")
@RequiredArgsConstructor
public class MileageCouponController {

    private final MileageCouponService mileageCouponService;

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 난수번호 관리 Main
     * @param model
     * @return String
     */
    @GetMapping(value = "/mileageCouponMain")
    public String mileageListMain(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.create("mileageCouponGridBaseInfo", MileageCouponSearchGetDto.class));
        model.addAllAttributes(RealGridHelper.create("mileageCouponUserGridBaseInfo", MileageCouponUserInfoDto.class));
        model.addAllAttributes(RealGridHelper.create("mileageCouponDownGridBaseInfo", MileageCouponOneTimeDto.class));
        return "/mileage/mileageCouponMain";
    }

    /**
     * 마일리지 난수관리 - 조회
     *
     * @param searchSetDto 조회파라미터
     * @return 조회결과
     * @throws Exception 오류시 오류처리
     */
    @ResponseBody
    @PostMapping(value = "/getMileageCouponManageList.json")
    public ResponseObject<List<MileageCouponSearchGetDto>> getMileageCouponManageList (@RequestBody MileageCouponSearchSetDto searchSetDto) throws Exception {
        return mileageCouponService.getMileageCouponManageList(searchSetDto);
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 난수관리 - 쿠폰발급유저
     *
     * @param couponManageNo 마일리지쿠폰관리번호
     * @return 조회결과
     * @throws Exception 오류시 오류처리
     */
    @ResponseBody
    @GetMapping(value = "/getMileageCouponUserList.json")
    public ResponseObject<List<MileageCouponUserInfoDto>> getMileageCouponUserList (@RequestParam("couponManageNo") long couponManageNo) throws Exception {
        return mileageCouponService.getMileageCouponUserList(couponManageNo);
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 난수관리 > 기본정보
     *
     * @param couponManageNo 마일리지쿠폰관리번호
     * @return 조회결과
     * @throws Exception 오류시 오류처리
     */
    @ResponseBody
    @GetMapping(value = "/getMileageCouponManageInfo.json")
    public ResponseObject<MileageCouponInfoDto> getMileageCouponManageInfo (@RequestParam("couponManageNo") long couponManageNo) throws Exception {
        return mileageCouponService.getMileageCouponManageInfo(couponManageNo);
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 요청 등록
     * @return MileageRequestGetDto
     */
    @ResponseBody
    @PostMapping(value = "/setMileageCoupon.json")
    public ResponseObject<String> setMileageCoupon(@RequestBody MileageCouponRequestDto requestDto) throws Exception {
        return mileageCouponService.setMileageCoupon(requestDto);
    }

    @ResponseBody
    @PostMapping(value = "/modifyMileageCoupon.json")
    public ResponseObject<String> modifyMileageCoupon(@RequestBody MileageCouponModifyDto modifyDto) throws Exception {
        return mileageCouponService.modifyMileageCoupon(modifyDto);
    }

    @ResponseBody
    @PostMapping(value = "/returnMileageCoupon.json")
    public ResponseObject<MileageCouponReturnGetDto> returnMileageCoupon(@RequestBody MileageCouponReturnSetDto returnSetDto) throws Exception {
        return mileageCouponService.returnMileageCoupon(returnSetDto);
    }

    @ResponseBody
    @GetMapping(value = "/getMileageCouponDownList.json")
    public ResponseObject<List<MileageCouponOneTimeDto>> getMileageCouponDownList (@RequestParam("couponManageNo") long couponManageNo) throws Exception {
        return mileageCouponService.getMileageCouponDownList(couponManageNo);
    }
}
