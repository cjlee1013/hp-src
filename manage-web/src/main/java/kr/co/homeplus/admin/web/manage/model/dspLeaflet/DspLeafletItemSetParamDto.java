package kr.co.homeplus.admin.web.manage.model.dspLeaflet;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspLeafletItemSetParamDto {

    private Integer cateNo;
    private String itemNm1;
    private String itemNo;
    private String itemTypeNm;
    private Integer leafletNo;
    private int priority;
    private String useYn;
    private String useYnTxt;

}
