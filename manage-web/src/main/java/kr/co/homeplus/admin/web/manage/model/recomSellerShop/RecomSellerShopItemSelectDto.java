package kr.co.homeplus.admin.web.manage.model.recomSellerShop;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class RecomSellerShopItemSelectDto {

    @RealGridColumnInfo(headText = "우선순위", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private int priority;

    @RealGridColumnInfo(headText = "상품번호", width = 120, order = 1, sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 180, order = 2)
    private String itemNm;

    @RealGridColumnInfo(headText = "사용여부", width = 50, order = 3, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "useYn", order = 4, hidden = true)
    private String useYn;

}
