package kr.co.homeplus.admin.web.item.model.item.hist;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.ObjectUtils;

@Getter
@Setter
public class ItemHistBasicGetDto {
    // 히스토리번호
    private String id;
    // 히스토리 등록일
    private String itemHistDt;
    // 히스토리 등록자ID
    private String itemHistUserId;
    // 히스토리 등록자명
    private String itemHistUserNm;

    /**
     * 기본정보
     */
    // 상품번호
    private String itemNo;
    // 상품명
    private String itemNm;
    // 상품유형 (B:반품(리세일), N:새상품, O:위메프 사외창고, R:리퍼, U:중고)
    private String itemType;
    // 판매유형 (NOR:일반,HDN:히든프라이스,BIZ:비즈)
    private String mallType;
    // 상품상태 (T: 임시저장, B:진행대기, E:수정대기, A:판매중, S:판매중지)
    private String itemStatus;
    // 판매상태 (A:판매가능, S:품절)
    private String saleStatus;
    // 장바구니 담기 제한 (Y:제한설정, N:제한없음)
    private String cartLimitYn;
    // 파트너ID
    private String partnerId;
    // 판매중지처리일시
    private String saleStatusDt;
    // 성인상품유형명
    private String adultTypeNm;
    // 노출정보
    private String dispYn;
    // 노출정보명
    private String dispYnNm;
    // 상세설명타입 (IMG:이미지, HTML:html)
    private String detailType;
    // 선택형옵션사용여부 (사용:Y, 미사용:N)
    private String optSelUseYn;
    // 텍스트옵션사용여부 (사용:Y, 미사용:N)
    private String optTxtUseYn;
    // 등록자
    private String regId;
    // 등록일시
    private String regDt;
    // 수정자
    private String chgId;
    // 수정일시
    private String chgDt;

    // 대카테고리
    private String lcateCd;
    // 대카테고리명
    private String lcateNm;
    // 중카테고리
    private String mcateCd;
    // 중카테고리명
    private String mcateNm;
    // 소카테고리명
    private String scateNm;
    // 소카테고리
    private String scateCd;
    // 세카테고리
    private String dcateCd;
    // 세카테고리명
    private String dcateNm;

    // 등록자 이름
    private String regNm;
    // 수정자 이름
    private String chgNm;
    // 상품유형명
    private String itemTypeNm;
    // 판매유형명
    private String mallTypeNm;
    // 상품상태명
    private String itemStatusNm;
    // 판매상태명
    private String saleUnit;
    // 선택형옵션사용여부명
    private String optSelUseYnNm;
    // 텍스트옵션사용여부명
    private String optTxtUseYnNm;
    // 파트너명
    private String partnerBusinessNm;
    // 파트너 회원상태 (NORMAL:정상, WITHDRAW:탈퇴)
    private String partnerStatus;
    // 브랜드명
    private String brandNm;
    // 제조사명
    private String makerNm;

    /**
     * 노출상태
     */
    // 상품노출상태
    private String itemShowStatus;
    // 정보변경요청상태
    private String itemInfoChgStatus;
    // 정보변경요청상태명
    private String itemInfoChgStatusNm;

    /**
     * 부가정보
     */
    // 판매자 상품코드
    private String sellerItemCd;
    // 부가정보 > 가격비교 등록여부 (Y,N)
    private String epYn;
    // 외부연동 사이트 등록여부
    private String linkageYn;
    // 이미지 노출여부
	private String imgDispYn;
    // 선물하기사용여부 (Y: 사용, N: 사용안함)
    private String giftYn;
    // 검색 키워드
    private String srchKeyword;
    // 해외배송 대행여부
    private String globalAgcyYn;
    // 인증 리스트
    private Map<String, ItemCertHistGetDto> certListMap;
    private List<ItemCertHistGetDto> certList;

    /**
     * 판매정보
     */
    // 부가세 여부 (Y:과세, N:비과세)
    private String taxYn;

    /**
     * 상품 상세정보
     */
    // 상품상세정보
    private String itemDesc;

    /**
     * 옵션타이틀 정보
     */
    // 옵션타이틀 > type
    private String optTitleType;
    // 옵션타이틀 > depth
    private String optTitleDepth;
    // 옵션타이틀 > 제목1
    private String opt1Title;
    // 옵션타이틀 > 제목2
    private String opt2Title;
    //옵션정보 리스트
    Map<String, ItemOptionValueHistGetDto> optListMap;
    List<ItemOptionValueHistGetDto> optList;

    /**
     * 텍스트옵션 정보
     */
    // 텍스트옵션 > type
    private String optTextType;
    // 텍스트옵션 > depth
    private String optTextDepth;
    // 텍스트옵션 > 제목1
    private String opt1Text;
    // 텍스트옵션 > 제목2
    private String opt2Text;
    // 텍스트옵션 > 등록자
    private String optTextRegId;
    // 텍스트옵션 > 등록일시
    private String optTextRegDt;
    // 텍스트옵션 > 수정자
    private String optTextChgId;
    // 텍스트옵션 > 수정일시
    private String optTextChgDt;

    /**
     * 속성정보
     */
    // 브랜드번호
    private String brandNo;
    // 제조사번호
    private String makerNo;
    // 단위가격 프론트 노출여부
    private String unitDispYn;
    // 원산지
    private String originTxt;

    /**
     * 이미지정보
     */
    private Map<String, ItemImgHistGetDto> imgListMap;

    private List<ItemImgHistGetDto> imgList;

    private String videoFileUrl;

    /**
     * 파일정보
     */
    private Map<String, ItemFileHistGetDto> proofFileListMap;


    private <T> String getMapToString(Map<String, T> obj) {
        if (ObjectUtils.isEmpty(obj)) {
            return "";
        }

        StringBuilder toChangeStr = new StringBuilder();

        for (Map.Entry<String, T> entry : obj.entrySet()) {
            toChangeStr.append(entry.toString());
        }

        return toChangeStr.toString();
    }

    private <T> String getListToString(List<T> obj) {
        if (ObjectUtils.isEmpty(obj)) {
            return "";
        }

        StringBuilder toChangeStr = new StringBuilder();

        for (T entry : obj) {
            toChangeStr.append(entry.toString());
        }

        return toChangeStr.toString();
    }


    // 기본정보
    public Optional<String> toChangeBasicString() {
        return Optional.of(
                  this.itemShowStatus
                + this.itemStatusNm
                + this.itemNm
                + this.itemType
                + this.lcateNm
                + this.mcateNm
                + this.scateNm
                + this.dcateCd
                + this.dispYn
                + this.cartLimitYn
        );
    }

    // 판매정보
    public Optional<String> toChangeSaleString() {
        return Optional.of(
                  this.taxYn
        );
    }

    // 용도옵션 정보
    public Optional<String> toChangeOptItemString() {
        return Optional.of(
                  this.optSelUseYn
                + this.optTitleDepth
                + this.opt1Title
                + this.opt2Title
                + this.getListToString(this.optList)
        );
    }

    // 이미지정보
    public Optional<String> toChangeImgString() {
        return Optional.of(
                  this.videoFileUrl
                + this.getListToString(this.imgList)
        );
    }

    // 속성정보
    public Optional<String> toChangeAttrString() {
        return Optional.of(
                          this.getListToString(this.certList)
                        + this.brandNo
                        + this.makerNo
                        + this.unitDispYn
                        + this.originTxt
        );
    }

    // 부가정보
    public Optional<String> toChangeEtcString() {
        return Optional.of(
                  this.adultTypeNm
                + this.imgDispYn
                + this.giftYn
                + this.epYn
                + this.linkageYn
                + this.srchKeyword
        );
    }

    // group data
    public ItemHistGroupBasicGetDto setItemHistGroupData(final ItemHistGroupStringBasicGetDto prevItemHistGroupStringGetDto) {
        ItemHistGroupBasicGetDto itemHistGroupGetDto = new ItemHistGroupBasicGetDto();
        final String CHANGE_MSG = "수정";

        itemHistGroupGetDto.setId(this.id);
        itemHistGroupGetDto.setChgDt(this.itemHistDt);
        itemHistGroupGetDto.setChgNm(this.itemHistUserNm);

        if (prevItemHistGroupStringGetDto.getBasic().isPresent()
                && !prevItemHistGroupStringGetDto.getBasic().equals(this.toChangeBasicString())) {
            itemHistGroupGetDto.setChangeBasic(CHANGE_MSG);
        }

        if (prevItemHistGroupStringGetDto.getSale().isPresent()
                && !prevItemHistGroupStringGetDto.getSale().equals(this.toChangeSaleString())) {
            itemHistGroupGetDto.setChangeSale(CHANGE_MSG);
        }

        if (prevItemHistGroupStringGetDto.getOptItem().isPresent()
                && !prevItemHistGroupStringGetDto.getOptItem().equals(this.toChangeOptItemString())) {
            itemHistGroupGetDto.setChangeOptItem(CHANGE_MSG);
        }

        if (prevItemHistGroupStringGetDto.getImg().isPresent()
                && !prevItemHistGroupStringGetDto.getImg().equals(this.toChangeImgString())) {
            itemHistGroupGetDto.setChangeImg(CHANGE_MSG);
        }

        if (prevItemHistGroupStringGetDto.getAttr().isPresent()
                && !prevItemHistGroupStringGetDto.getAttr().equals(this.toChangeAttrString())) {
            itemHistGroupGetDto.setChangeAttr(CHANGE_MSG);
        }

        if (prevItemHistGroupStringGetDto.getEtc().isPresent()
                && !prevItemHistGroupStringGetDto.getEtc().equals(this.toChangeEtcString())) {
            itemHistGroupGetDto.setChangeEtc(CHANGE_MSG);
        }

        return itemHistGroupGetDto;
    }

}
