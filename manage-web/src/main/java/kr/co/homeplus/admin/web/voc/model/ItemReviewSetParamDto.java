package kr.co.homeplus.admin.web.voc.model;

import java.util.List;
import lombok.Data;

@Data
public class ItemReviewSetParamDto {

    private String blockYn;
    private String blockContents;
    private List<Long> reviewNos;
    private String regId;

}
