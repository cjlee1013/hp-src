package kr.co.homeplus.admin.web.manage.model.dspPcMain;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcMainGetDto {

    private Integer mainNo;

    private String loc1Depth;

    private String dispStoreType;

    private String bgColor;

    private String closeType;

    private String division;

    private String template;

    private String templateDetail;

    private List<PcMainStoreGetDto> storeList;

    private List<PcMainLinkGetDto> linkList;

}