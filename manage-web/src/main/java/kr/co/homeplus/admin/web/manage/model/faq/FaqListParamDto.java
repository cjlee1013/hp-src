package kr.co.homeplus.admin.web.manage.model.faq;


import lombok.Data;

@Data
public class FaqListParamDto {

    private String searchDispArea;
    private String searchClassCd;
    private String searchUseYn;
    private String searchSiteType;
    private String searchType;
    private String searchKeyword;


}
