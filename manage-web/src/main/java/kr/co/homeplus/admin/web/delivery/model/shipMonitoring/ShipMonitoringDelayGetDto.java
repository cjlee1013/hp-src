package kr.co.homeplus.admin.web.delivery.model.shipMonitoring;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.Data;

@Data
@ApiModel(description = "발송지연조회 응답")
public class ShipMonitoringDelayGetDto {
    @ApiModelProperty(notes = "판매업체")
    @RealGridColumnInfo(headText = "판매업체", width = 150, sortable = true)
    private String partnerNm;

    @ApiModelProperty(notes = "주문번호")
    @RealGridColumnInfo(headText = "주문번호", width = 120, sortable = true)
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "배송번호")
    @RealGridColumnInfo(headText = "배송번호", width = 120, sortable = true)
    private String bundleNo;

    @ApiModelProperty(notes = "주문일시")
    @RealGridColumnInfo(headText = "주문일시", width = 150, sortable = true)
    private String orderDt;

    @ApiModelProperty(notes = "배송상태")
    @RealGridColumnInfo(headText = "배송상태", width = 120, sortable = true)
    private String shipStatusNm;

    @ApiModelProperty(notes = "배송상태코드")
    @RealGridColumnInfo(headText = "배송상태코드", width = 120, sortable = true, hidden = true)
    private String shipStatus;

    @ApiModelProperty(notes = "마켓연동")
    @RealGridColumnInfo(headText = "마켓연동", width = 80, sortable = true)
    private String marketType;

    @ApiModelProperty(notes = "마켓주문번호")
    @RealGridColumnInfo(headText = "마켓주문번호", width = 120, sortable = true)
    private String marketOrderNo;

    @ApiModelProperty(notes = "주문확인일")
    @RealGridColumnInfo(headText = "주문확인일", width = 120, sortable = true)
    private String confirmDt;

    @ApiModelProperty(notes = "주문확인지연일")
    @RealGridColumnInfo(headText = "주문확인지연일", width = 120, sortable = true)
    private String delayConfirmDay;

    @ApiModelProperty(notes = "배송요청일")
    @RealGridColumnInfo(headText = "배송요청일", width = 120, sortable = true)
    private String reserveShipDt;

    @ApiModelProperty(notes = "배송예정일")
    @RealGridColumnInfo(headText = "배송예정일", width = 120, sortable = true)
    private String scheduleShipDt;

    @ApiModelProperty(notes = "발송기한")
    @RealGridColumnInfo(headText = "발송기한", width = 120, sortable = true)
    private String orgShipDt;

    @ApiModelProperty(notes = "2차발송기한")
    @RealGridColumnInfo(headText = "2차발송기한", width = 120, sortable = true)
    private String delayShipDt;

    @ApiModelProperty(notes = "발송지연처리일")
    @RealGridColumnInfo(headText = "발송지연처리일", width = 120, sortable = true)
    private String delayShipRegDt;

    @ApiModelProperty(notes = "발송지연일수")
    @RealGridColumnInfo(headText = "발송지연일수", width = 120, sortable = true)
    private String delayShipDay;

    @ApiModelProperty(notes = "상품주문번호")
    @RealGridColumnInfo(headText = "상품주문번호", width = 120, sortable = true, hidden = true)
    private String orderItemNo;

    @ApiModelProperty(notes = "배송방법")
    @RealGridColumnInfo(headText = "배송방법", width = 120, sortable = true)
    private String shipMethodNm;

    @ApiModelProperty(notes = "배송방법코드")
    @RealGridColumnInfo(headText = "배송방법코드", width = 120, sortable = true, hidden = true)
    private String shipMethod;

    @ApiModelProperty(notes = "택배사")
    @RealGridColumnInfo(headText = "택배사", width = 150, sortable = true)
    private String dlvCdNm;

    @ApiModelProperty(notes = "택배사코드")
    @RealGridColumnInfo(headText = "택배사코드", width = 120, sortable = true, hidden = true)
    private String dlvCd;

    @ApiModelProperty(notes = "송장번호")
    @RealGridColumnInfo(headText = "송장번호", width = 150, sortable = true)
    private String invoiceNo;

    @ApiModelProperty(notes = "회원번호")
    @RealGridColumnInfo(headText = "회원번호", width = 120, sortable = true)
    private String userNoMask;

    @ApiModelProperty(notes = "회원번호")
    @RealGridColumnInfo(headText = "회원번호", width = 120, sortable = true, hidden = true)
    private String userNo;

    @ApiModelProperty(notes = "구매자")
    @RealGridColumnInfo(headText = "구매자", width = 100, sortable = true)
    private String buyerNm;

    @ApiModelProperty(notes = "수령인")
    @RealGridColumnInfo(headText = "수령인", width = 100, sortable = true)
    private String receiverNm;

    @ApiModelProperty(notes = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", width = 120, sortable = true, hidden = true)
    private String storeType;

    @ApiModelProperty(notes = "사이트유형")
    @RealGridColumnInfo(headText = "사이트유형", width = 120, sortable = true, hidden = true)
    private String siteType;
}
