package kr.co.homeplus.admin.web.search.service;

import java.util.Collections;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.search.model.AddrDictionaryListGetDto;
import kr.co.homeplus.admin.web.search.model.AddrDictionaryListParamDto;
import kr.co.homeplus.admin.web.search.model.AddrDictionarySetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.ResourceRouteConfigure;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AddrDictionaryService {

    private final ResourceClient resourceClient;
    private final ResourceRouteConfigure resourceRouteConfigure;

    public ResponseObject getSrchNounInfo(AddrDictionaryListParamDto param) {
        String apiUri = "/dictionary/getAddrSrchNounInfo";
        ResourceClientRequest<AddrDictionaryListGetDto> request = ResourceClientRequest.<AddrDictionaryListGetDto>postBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .postObject(param)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request,new TimeoutConfig()).getBody();
    }

    public ResponseObject getSrchSynonymInfo(Integer wordId) {
        String apiUri = "/dictionary/getAddrSynonymInfo/" + wordId;
        return getResponseObject(apiUri);
    }

    public ResponseObject insertNounInfo(AddrDictionarySetDto param) {
        String apiUri = "/dictionary/insertAddrNounInfo";
        return getResponseObject(apiUri, param);
    }

    public ResponseObject updateNounInfo(AddrDictionarySetDto param, Integer wordId) {
        String apiUri = "/dictionary/updateAddrNounInfo/" + wordId;
        return getResponseObject(apiUri, param, HttpMethod.PUT);
    }

    private ResponseObject getResponseObject(String apiUri){
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    private <E> ResponseObject getResponseObject(String apiUri, E elements){
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .postObject(elements)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }

    public <E> ResponseObject getResponseObject(String apiUri, E elements, HttpMethod method){
        HttpEntity<Object> httpEntity = new HttpEntity<>(elements, new HttpHeaders(){{
            setContentType(MediaType.APPLICATION_JSON);
            setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        }});
        String resourceUrl = resourceRouteConfigure.getUrl(ResourceRouteName.SEARCHMNG) + apiUri;
        return resourceClient.exchange(resourceUrl, method, httpEntity, new ParameterizedTypeReference<>(){}, new TimeoutConfig()).getBody();
    }
}
