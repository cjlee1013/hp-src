package kr.co.homeplus.admin.web.partner.model.affiliate;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class AffiliateListGetDto {

    @RealGridColumnInfo(headText = "제휴업체ID", width = 120)
    private String partnerId;

    @RealGridColumnInfo(headText = "제휴업체명", width = 150, order = 1)
    private String affiliateNm;

    @RealGridColumnInfo(headText = "상호", width = 150, order = 2)
    private String partnerNm;

    @RealGridColumnInfo(headText = "사업자유형", width = 150, order = 3)
    private String operatorTypeNm;

    @RealGridColumnInfo(headText = "사업자등록번호", order = 4)
    private String partnerNo;

    @RealGridColumnInfo(headText = "제휴유형", order = 5, sortable = true)
    private String affiliateTypeNm;

    @RealGridColumnInfo(headText = "회원상태", order = 6, sortable = true)
    private String partnerStatusNm;

    @RealGridColumnInfo(headText = "등록자", order = 7)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, width = 150, order = 8, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", order = 9)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, width = 150, order = 10, sortable = true)
    private String chgDt;


    private String partnerStatus;
    private String regId;
    private String chgId;

}
