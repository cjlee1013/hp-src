package kr.co.homeplus.admin.web.delivery.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum CloseDayType {
    F("F","자율휴무"),
    R("R","정기휴무"),
    S("S","설"),
    C("C","추석"),
    E("E","기타"),
    T("T","임시"),
    ;

    private String mcCd;
    private String mcNm;
}