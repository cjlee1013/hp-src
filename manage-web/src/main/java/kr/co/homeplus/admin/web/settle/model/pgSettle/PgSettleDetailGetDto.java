package kr.co.homeplus.admin.web.settle.model.pgSettle;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "전표조회 목록")
public class PgSettleDetailGetDto {
  @ApiModelProperty(notes = "일자")
  @RealGridColumnInfo(headText = "일자", width = 100)
  private String basicDt;

  @ApiModelProperty(notes = "PG사")
  @RealGridColumnInfo(headText = "PG사", width = 100)
  private String pgKind;

  @ApiModelProperty(notes = "결제수단")
  @RealGridColumnInfo(headText = "결제수단", width = 100)
  private String paymentTool;

  @ApiModelProperty(notes = "결제금액")
  @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 150, groupName="결제기준", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String basicPg;

  @ApiModelProperty(notes = "결제수수료")
  @RealGridColumnInfo(headText = "결제수수료", sortable = true, width = 150, groupName="결제기준", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String basicCommission;

  @ApiModelProperty(notes = "DS 정산대상금액")
  @RealGridColumnInfo(headText = "DS 정산대상금액", sortable = true, width = 150, groupName="결제기준", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String basicSettle;

  @ApiModelProperty(notes = "입금예정액")
  @RealGridColumnInfo(headText = "입금예정액", sortable = true, width = 150, groupName="결제기준", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String basicSum;

  @ApiModelProperty(notes = "결제금액")
  @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 150, groupName="입금기준", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String payPg;

  @ApiModelProperty(notes = "결제수수료")
  @RealGridColumnInfo(headText = "결제수수료", sortable = true, width = 150, groupName="입금기준", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String payCommission;

  @ApiModelProperty(notes = "DS 정산대상금액")
  @RealGridColumnInfo(headText = "DS 정산대상금액", sortable = true, width = 150, groupName="입금기준", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String paySettle;

  @ApiModelProperty(notes = "입금예정액")
  @RealGridColumnInfo(headText = "입금예정액", sortable = true, width = 150, groupName="입금기준", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String paySum;

  @ApiModelProperty(notes = "이전 예치금")
  @RealGridColumnInfo(headText = "이전 예치금", sortable = true, width = 100, groupName="지급대행", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String depositBalance;

  @ApiModelProperty(notes = "예치금")
  @RealGridColumnInfo(headText = "예치금", sortable = true, width = 100, groupName="지급대행", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String depositAmt;

  @ApiModelProperty(notes = "지급완료")
  @RealGridColumnInfo(headText = "지급완료", sortable = true, width = 100, groupName="지급대행", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String settleAmt;

  @ApiModelProperty(notes = "예치금 잔액")
  @RealGridColumnInfo(headText = "예치금 잔액", sortable = true, width = 100, groupName="지급대행", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String depositLeft;

  @ApiModelProperty(notes = "PG미수금")
  @RealGridColumnInfo(headText = "PG미수금", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String receivableAmt;
}
