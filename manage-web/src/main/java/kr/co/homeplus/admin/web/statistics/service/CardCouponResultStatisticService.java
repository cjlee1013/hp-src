package kr.co.homeplus.admin.web.statistics.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.statistics.model.CardCouponResultStatisticsDetailGetDto;
import kr.co.homeplus.admin.web.statistics.model.CardCouponResultStatisticsGetDto;
import kr.co.homeplus.admin.web.statistics.model.CardCouponResultStatisticsSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CardCouponResultStatisticService {

    private final ResourceClient resourceClient;

    /**
     * 카드사별쿠폰실적조회 메인조회
     * @param
     * @return
     */
    public List<CardCouponResultStatisticsGetDto> getCardCouponResultStatisticsList(
        CardCouponResultStatisticsSetDto cardCouponResultStatisticsSetDto) {

        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            cardCouponResultStatisticsSetDto,
            "/admin/statistics/getCardCouponResultStatisticsList",
            new ParameterizedTypeReference<ResponseObject<List<CardCouponResultStatisticsGetDto>>>(){}).getData();
    }

    /**
     * 카드사별쿠폰실적조회 상세조회
     * @param
     * @return
     */
    public List<CardCouponResultStatisticsDetailGetDto> getCardCouponResultStatisticsDetailList(CardCouponResultStatisticsSetDto cardCouponResultStatisticsSetDto) {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            cardCouponResultStatisticsSetDto,
            "/admin/statistics/getCardCouponResultStatisticsDetailList",
            new ParameterizedTypeReference<ResponseObject<List<CardCouponResultStatisticsDetailGetDto>>>(){}).getData();
    }
}
