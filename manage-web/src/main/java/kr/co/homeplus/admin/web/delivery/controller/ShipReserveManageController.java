package kr.co.homeplus.admin.web.delivery.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipReserveManageListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipReserveManageListSetDto;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.delivery.service.ShipReserveManageService;
import kr.co.homeplus.admin.web.order.model.slot.FrontStoreSlot;
import kr.co.homeplus.admin.web.order.model.slot.ShipSlotStockChangeDto;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 배송관리 > 배송관리 > 예약배송관리
 */
@Controller
@RequestMapping("/ship")
public class ShipReserveManageController {

    @Value("${front.homeplus.url}")
    private String homeplusFrontUrl;

    @Value("${front.theclub.url}")
    private String theclubFrontUrl;

    private final ShipReserveManageService shipReserveManageService;
    private final ShipCommonService shipCommonService;
    private final AuthorityService authorityService;

    public ShipReserveManageController(ShipReserveManageService shipReserveManageService
        , ShipCommonService shipCommonService
        , AuthorityService authorityService) {
        this.shipReserveManageService = shipReserveManageService;
        this.shipCommonService = shipCommonService;
        this.authorityService = authorityService;
    }

    /**
     * 예약배송관리 Main Page
     */
    @GetMapping("/shipManage/shipReserveManageMain")
    public String shipReserveManageMain(Model model) throws Exception {

        //SO 권한 데이터 설정
        shipCommonService.setStoreInfo(model);

        model.addAttribute("packageExcel", authorityService.hasRole("package_excel"));
        model.addAttribute("storeType", shipCommonService.getCode("store_type"));
        model.addAttribute("shipStatus", shipCommonService.getCode("ship_status"));
        model.addAttribute("shipMethod", shipCommonService.getCode("ship_method"));
        model.addAttribute("theclubFrontUrl", theclubFrontUrl);
        model.addAttribute("homeplusFrontUrl", homeplusFrontUrl);

        model.addAllAttributes(RealGridHelper.create("shipReserveManageGridBaseInfo", ShipReserveManageListGetDto.class));

        return "/delivery/shipReserveManageMain";
    }

    /**
     * 예약배송관리 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/shipManage/getShipReserveManageList.json", method = RequestMethod.GET)
    public List<ShipReserveManageListGetDto> getShipReserveManageList(
        ShipReserveManageListSetDto shipReserveManageListSetDto) {
        return shipReserveManageService.getShipReserveManageList(shipReserveManageListSetDto);
    }

    /**
     * 슬롯시간 정보 조회
     */
    @ResponseBody
    @GetMapping("/getShipSlotInfo.json")
    public FrontStoreSlot getShipSlotInfo(@RequestParam("storeId") String storeId) throws Exception {
        return shipReserveManageService.getShipSlotInfo(storeId);
    }

    /**
     * 배송시간 수정 팝업
     */
    @GetMapping("/popup/shipChangeSlotPop")
    public String shipChangeSlotPop() throws Exception {
        return "/delivery/pop/shipChangeSlotPop";
    }
    /**
     * 배송시간 일괄 수정
     */
    @ResponseBody
    @PostMapping("/shipManage/setShipSlotInfo.json")
    public int setShipSlotInfo(@RequestBody List<ShipSlotStockChangeDto> stockChangeDtoList) throws Exception {
        return shipReserveManageService.setShipSlotInfo(stockChangeDtoList);
    }
}
