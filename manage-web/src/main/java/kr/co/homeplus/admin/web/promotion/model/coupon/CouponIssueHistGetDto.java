package kr.co.homeplus.admin.web.promotion.model.coupon;

import java.util.List;
import lombok.Data;

@Data
public class CouponIssueHistGetDto {
    private int couponIssueHistListCount;
    private List<CouponIssueHistListGetDto> couponIssueHistList;

    private int issueCount;
    private int useIssueCount;
    private int sumDiscountprice;
}
