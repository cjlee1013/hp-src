package kr.co.homeplus.admin.web.message.model;

import java.util.List;
import lombok.Data;

@Data
public class RecipientSearchResponse {

    private List<RecipientInfo> switchableRecipientInfos;
}
