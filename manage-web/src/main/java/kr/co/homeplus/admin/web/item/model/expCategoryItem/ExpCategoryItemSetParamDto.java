package kr.co.homeplus.admin.web.item.model.expCategoryItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpCategoryItemSetParamDto {

    private Integer mcateCd;
    private String itemNo;
    private String status = "Y";
    private String regId;

}
