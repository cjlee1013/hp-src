package kr.co.homeplus.admin.web.item.model.categoryGroup;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CategoryGroupLcateGetDto {

	private Integer gcateCd;

	private String gcateNm;

	private String lcateNm;

	private Integer lcateCd;

	private String useYnTxt;

	private String useYn;

	private String dispYnTxt;

	private String dispYn;

	private String regDt;

	private String regNm;

	private String chgDt;

	private String chgNm;

}
