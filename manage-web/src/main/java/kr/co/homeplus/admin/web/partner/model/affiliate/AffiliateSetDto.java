package kr.co.homeplus.admin.web.partner.model.affiliate;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.partner.model.PartnerManagerSetDto;
import lombok.Data;

@Data
public class AffiliateSetDto {

    /**
     * 기본정보
     */
    private String partnerId;
    private String partnerType = "AFFI";
    private String operatorType;
    private String affiliateType;
    private String affiliateNm;
    private String partnerStatus = "NORMAL";
    private String partnerGrade = "NORMAL";
    private String userId;
    private String isMod;

    /**
     * 사업자정보
     */
    private String partnerNm;
    private String partnerOwner;
    private String partnerNo;
    private String businessConditions;
    private String bizCateCd;
    private String zipcode;
    private String addr1;
    private String addr2;

    /**
     * 정산정보
     */
    private AffiliateSettleSetDto settleInfo;

    /**
     * 담당자정보
     */
    private List<PartnerManagerSetDto> managerList = new ArrayList<>();

}
