package kr.co.homeplus.admin.web.item.model.market;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("외부연동현황관리 parameter")
public class MarketItemParamDto {

    //외부연동 사이트
    private String schPartner;

    //대카테고리
    private String schLcateCd;

    //중카테고리
    private String schMcateCd;

    //소카테고리
    private String schScateCd;

    //세카테고리
    private String schDcateCd;

    //검색타입
    private String schType;

    //검색키워드
    private String schKeyword;

    //상품상태(T: 임시저장, A:판매중, S:판매중지)
    private String schItemStatus;

    //연동사이트 상품상태
    private String schMarketItemStatus;

    //연동여부
    private String schSendYn;

}