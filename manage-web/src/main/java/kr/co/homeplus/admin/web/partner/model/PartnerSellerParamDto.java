package kr.co.homeplus.admin.web.partner.model;

import lombok.Data;

@Data
public class PartnerSellerParamDto {

    // 등록 시작일
    private String schRegStartDate;

    // 등록 종료일
    private String schRegEndDate;

    // 사업자유형
    private String schOperatorType;

    // 회원등급
    private String schPartnerGrade;

    // 회원상태
    private String schPartnerStatus;

    // 승인상태
    private String schApprovalStatus;

    // OF 승인상태
    private String schOfVendorStatus;

    // 검색 조건
    private String schType;

    // 검색 키워드
    private String schKeyword;

}
