package kr.co.homeplus.admin.web.user.model.refundAccount;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.admin.web.user.enums.RefundAccountSearchType;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ApiModel(description = "환불계좌 관리 조회 파라미터")
public class RefundAccountSearchParam {

    @ApiModelProperty(value = "검색유형")
    private RefundAccountSearchType searchType;

    @ApiModelProperty(value = "키워드")
    private String keyword;
}
