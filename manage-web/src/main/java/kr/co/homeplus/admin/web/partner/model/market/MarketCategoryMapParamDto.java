package kr.co.homeplus.admin.web.partner.model.market;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarketCategoryMapParamDto {

    //등록여부
    private String schRegYn;

    //대카테고리
    private String schCateCd1;

    //중카테고리
    private String schCateCd2;

    //소카테고리
    private String schCateCd3;

    //세카테고리
    private String schCateCd4;
}
