package kr.co.homeplus.admin.web.partner.model.shipPolicy;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShipPolicyParamDto {

    //판매업체ID
    private String partnerId;

    //점포ID
    private String storeId;

    //배송정책번호
    private Long shipPolicyNo;
}
