package kr.co.homeplus.admin.web.core.utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MobileUtil {

    private static final String MOBILE_CHECK_REG = "^01([0|1|6|7|8|9]?)-([0-9]{3,4})-([0-9]{4})$";
    private static final String CONVERT_MOBILE_NO_REG = "(\\d{3})(\\d{3,4})(\\d{4})";

    private MobileUtil() {
       throw new IllegalStateException("Utility class");
    }

    /**
     * 핸드폰번호 형식인지 체크
     *
     * @param text 대상 번호 문자열
     * @return 핸드폰 번호 형식일 경우 true, 아닐 경우 false
     */
    public static boolean isMobile(final String text) {
        final Pattern pattern = Pattern.compile(MOBILE_CHECK_REG);
        final Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    /**
     * 핸드폰번호 형식이 아닌지 체크
     * @param text 대상 번호 문자열
     * @return 핸드폰 번호 형식이 아닐 경우 true, 핸드폰 번호 형식일 경우 false
     */
    public static boolean isNotMobileType(final String text) {
        return !MobileUtil.isMobile(text);
    }

    /**
     * 핸드폰번호 형식으로 만듬
     *
     * @param text 대상 문자열
     * @return 핸드폰 번호 형식으로 변환 된 문자열
     */
    public static String convertMobilePattern(String text) {
        text = text.replace("-", "");

        if (!Pattern.matches(CONVERT_MOBILE_NO_REG, text)) {
            return text;
        }

        return text.replaceAll(CONVERT_MOBILE_NO_REG, "$1-$2-$3");
    }


}
