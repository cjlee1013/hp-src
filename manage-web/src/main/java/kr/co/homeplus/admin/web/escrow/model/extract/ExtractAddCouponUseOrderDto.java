package kr.co.homeplus.admin.web.escrow.model.extract;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "중복쿠폰 사용주문 추출용 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class ExtractAddCouponUseOrderDto {
    @ApiModelProperty(value = "고객번호", position = 1)
    @RealGridColumnInfo(headText = "고객번호", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String userNo;

    @ApiModelProperty(value = "주문번호", position = 2)
    @RealGridColumnInfo(headText = "주문번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "SALE_DATE", position = 3)
    @RealGridColumnInfo(headText = "SALE_DATE", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String orderDt;

    @ApiModelProperty(value = "STORE_CD", position = 4)
    @RealGridColumnInfo(headText = "STORE_CD", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String originStoreId;

    @ApiModelProperty(value = "POS_NO", position = 5)
    @RealGridColumnInfo(headText = "POS_NO", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String posNo;

    @ApiModelProperty(value = "영수증번호", position = 6)
    @RealGridColumnInfo(headText = "영수증번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String tradeNo;

    @ApiModelProperty(value = "CPON-CD", position = 7)
    @RealGridColumnInfo(headText = "CPON-CD", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String couponNo;

    @ApiModelProperty(value = "상품코드", position = 8)
    @RealGridColumnInfo(headText = "상품코드", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 9)
    @RealGridColumnInfo(headText = "상품명", width = 200, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String itemNm;

    @ApiModelProperty(value = "상품매출", position = 10)
    @RealGridColumnInfo(headText = "상품매출", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long orderPrice;

    @ApiModelProperty(value = "SALE_QTY", position = 11)
    @RealGridColumnInfo(headText = "SALE_QTY", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long itemQty;

    @ApiModelProperty(value = "면세/과세", position = 12)
    @RealGridColumnInfo(headText = "면세/과세", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String taxYn;

    @ApiModelProperty(value = "SECTION_CD", position = 13)
    @RealGridColumnInfo(headText = "SECTION_CD", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String deptNo;

    @ApiModelProperty(value = "CLASS_CD", position = 14)
    @RealGridColumnInfo(headText = "CLASS_CD", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String classNo;

    @ApiModelProperty(value = "SUB_CLASS_CD", position = 15)
    @RealGridColumnInfo(headText = "SUB_CLASS_CD", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String subClassNo;

    @ApiModelProperty(value = "쿠폰 Cost1", position = 16)
    @RealGridColumnInfo(headText = "쿠폰 Cost1", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long totAddDiscountAmt;

    @ApiModelProperty(value = "쿠폰 Cost2", position = 17)
    @RealGridColumnInfo(headText = "쿠폰 Cost2", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long addDiscountAmt;
}
