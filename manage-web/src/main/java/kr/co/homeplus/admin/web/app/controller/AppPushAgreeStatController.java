package kr.co.homeplus.admin.web.app.controller;

import io.swagger.annotations.ApiParam;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import kr.co.homeplus.admin.web.app.model.AppPushAgreeDto;
import kr.co.homeplus.admin.web.app.model.DateCalInfo;
import kr.co.homeplus.admin.web.app.service.AppPushAgreeStatService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 앱 광고 푸시 수신동의 통계
 */
@Slf4j
@Controller
@Validated
@RequiredArgsConstructor
@RequestMapping(value = "/app/mng/adver/push/")
public class AppPushAgreeStatController {

    private final AppPushAgreeStatService appPushAgreeStatService;

    private final Map<String, Object> appPushAgreeStatGridBaseInfo = RealGridHelper
        .create("appPushAgreeStatGridBaseInfo", AppPushAgreeDto.class);


    @GetMapping(value = "/main")
    public String AppPushAgreeStatMain(final Model model) {
        model.addAllAttributes(appPushAgreeStatGridBaseInfo);
        //검색조건 조회기간 년도 조회
        model.addAttribute("getYear",
            appPushAgreeStatService.getCalculateYears(2021));
        return "/app/appPushAgreeStatMain";
    }

    /**
     * 검색조건 > 조회기간 > 월별 셀렉트 박스 조회용도
     * @param year 조회년도
     * @return {@link DateCalInfo}
     */
    @ResponseBody
    @GetMapping(value = "/getMonths.json", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DateCalInfo> getMonths(
        @RequestParam @Size(min = 4, max = 4, message = "올바른 형식(yyyy)이 아닙니다.")
        @Pattern(regexp = "^[0-9]+$", message = "숫자만 허용합니다.")
        @ApiParam(value = "조회년도(yyyy)") final String year) {

        return appPushAgreeStatService.getCalculateMonth(Integer.parseInt(year));
    }

    @ResponseBody
    @GetMapping(value = "/getAgreeStatList.json", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AppPushAgreeDto> getAgreeStatList(
        @RequestParam @Size(min = 4, max = 4, message = "올바른 형식(yyyy)이 아닙니다.")
        @Pattern(regexp = "^[0-9]+$", message = "숫자만 허용합니다.")
        @ApiParam(value = "조회년도(yyyy)") final String year,
        @RequestParam @Size(min = 2, max = 2, message = "올바른 형식(mm)이 아닙니다.")
        @Pattern(regexp = "^[0-9]+$", message = "숫자만 허용합니다.")
        @ApiParam(value = "조회월(mm)") final String month) {

        return appPushAgreeStatService.getAgreeStatList(year, month);
    }
}
