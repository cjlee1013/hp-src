package kr.co.homeplus.admin.web.promotion.model.promo;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PromoDetailSetDto {
    // 기획전, 이벤트 번호
    private Long promoNo;

    // 기획전, 이벤트 유형
    private String promoType;

    // 관리기획전명
    private String mngPromoNm;

    // 전시기획전명
    private String dispPromoNm;

    // 전시시작일시
    private String dispStartDt;

    // 전시종료일시
    private String dispEndDt;

    // 사이트 구분
    private String siteType;

    // 사이트 구분 - express 여부
    private String expYn;

    // 적용시스템 - PC
    private String pcUseYn;

    // 적용시스템 - APP
    private String appUseYn;

    // 적용시스템 - MWEB
    private String mwebUseYn;

    // 전시여부
    private String dispYn;

    // 사용여부
    private String useYn;

    // 앱 상단 타이틀
    private String appTopTitle;

    // 노출영역 - 메인
    private String dispMainYn;

    // 노출영역 - 카테고리
    private String dispCategoryYn;

    // 노출영역 - 검색
    private String dispSearchYn;

    // 카테고리 기획전 노출 우선순위
    private Integer priority;

    // 검색키워드
    private String searchKeyword;

    // 검색키워드 노출 우선순위
    private Integer searchPriority;

    // 혜택존 기획전 노출 우선순위
    private Integer mainPriority;

    // 쿠폰사용여부
    private String couponUseYn;

    // 상단영역 - PC
    private String pcType;

    // 상단영역 - MOBILE
    private String mobileType;

    // 배경색상
    private String bgColor;

    // PC 상단 에디터 입력 Text
    private String pcText;

    // MOBILE 상단 에디터 입력 Text
    private String mobileText;

    // 쿠폰 템플릿(1:1단, 2:2단)
    private String couponTemplate;

    // 쿠폰 주의사항 사용 여부(Y:사용, N:미사용)
    private String couponCautionYn;

    // 쿠폰 주의사항 에디터 입력 Text
    private String couponCautionText;

    // 사용자 번호
    private String empNo;

    // 관련 카테고리 리스트
    private List<Integer> cateList;

    // 이미지 리스트
    private List<PromoImgDto> imgList;

    // 쿠폰 리스트
    private List<PromoCouponSetDto> couponList;

    // 상품 분류 리스트
    private List<PromoThemeDto> themeList;

    // 상품 리스트
    private List<PromoItemListSetDto> themeItemList;
}