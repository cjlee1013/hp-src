package kr.co.homeplus.admin.web.manage.controller;

import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.store.StoreListParamDto;
import kr.co.homeplus.admin.web.item.model.store.StoreListSelectDto;
import kr.co.homeplus.admin.web.manage.model.dspSearchBar.DspSearchBarListParamDto;
import kr.co.homeplus.admin.web.manage.model.dspSearchBar.DspSearchBarListSelectDto;
import kr.co.homeplus.admin.web.manage.model.dspSearchBar.DspSearchBarSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage/dspSearchBar")
public class DspSearchBarController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    /**
     * 사이트관리 > 전시관리 > 검색창키워드관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dspSearchBarMain", method = RequestMethod.GET)
    public String dspSearchBarMain(Model model)  {

        codeService.getCodeModel(model
            ,   "use_yn"                // 사용여부
            ,   "dsk_site_gubun"        // 사이트 구분
            ,   "dsk_device_gubun"      // 디바이스 구분
            ,   "dsp_popup_link_type"   // 링크타입
            ,   "dsp_search_loc_cd"     // 노출위치
            ,   "dsp_main_link_type"    // 링크 타입
            ,   "dsp_main_store_type"   // 스토어 타입
        );

        //택배점 리스트 조회
        String apiUri = "/item/store/getStoreList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject< List<StoreListSelectDto>>>() {};
        List<StoreListSelectDto> dlvStoreList =  (List<StoreListSelectDto>) resourceClient.postForResponseObject
            (ResourceRouteName.ITEM, StoreListParamDto.builder().searchStoreKind("DLV").searchUseYn("Y").build(), apiUri, typeReference).getData();

        model.addAttribute("dlvStoreList", dlvStoreList);
        model.addAllAttributes(RealGridHelper.create("dspSearchBarListGridBaseInfo", DspSearchBarListSelectDto.class));

        return "/manage/dspSearchBarMain";
    }

    /**
     * 사이트관리 > 전시관리 > 검색창키워드관리 리스트 조회
     *
     * @param listParamDto
     * @return List<DspSearchBarListSelectDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/getDspSearchBarList.json"}, method = RequestMethod.GET)
    public List<DspSearchBarListSelectDto> getDspSearchBarList(DspSearchBarListParamDto listParamDto) {
        String apiUri = "/manage/dspSearchBar/getDspSearchBarList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<DspSearchBarListSelectDto>>>() {};
        return (List<DspSearchBarListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, DspSearchBarListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * 사이트관리 > 전시관리 > 검색창키워드관리 등록/수정
     *
     * @param dspSearchBarSetDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/setDspSearchBar.json"}, method = RequestMethod.POST)
    public ResponseResult setDspSearchBar(@RequestBody DspSearchBarSetDto dspSearchBarSetDto) {
        String apiUri = "/manage/dspSearchBar/setDspSearchBar";
        dspSearchBarSetDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, dspSearchBarSetDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();
    }

}