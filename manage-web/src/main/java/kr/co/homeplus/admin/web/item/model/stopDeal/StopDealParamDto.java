package kr.co.homeplus.admin.web.item.model.stopDeal;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
public class StopDealParamDto {

    private String storeType;

    // private List<String> itemNoList;

    private String schKeyword;
}