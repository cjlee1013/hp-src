package kr.co.homeplus.admin.web.common.model.pop;

import java.util.List;
import lombok.Data;

/**
 * 수동 발송 > 주문 검색 결과 response dto
 */
@Data
public class OrderSendListResponse {

    private List<OrderSendListInfo> list;
}
