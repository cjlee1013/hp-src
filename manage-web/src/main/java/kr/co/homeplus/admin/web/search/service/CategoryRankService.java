package kr.co.homeplus.admin.web.search.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.search.model.CategoryRankInsertParamDto;
import kr.co.homeplus.admin.web.search.model.CategoryRankListGetDto;
import kr.co.homeplus.admin.web.search.model.CategoryRankRelationGetDto;
import kr.co.homeplus.admin.web.search.model.CategoryRankUpdateParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CategoryRankService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    public ResponseObject getScoreList() {
        String apiUri = "/categoryRank/getScoreList";
        ResourceClientRequest<String[]> request = ResourceClientRequest.<String[]>getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    public ResponseObject getRankList(List<SetParameter> paramList) {
        String apiUri = "/categoryRank/getList" + StringUtil.getParameter(paramList);
        ResourceClientRequest<CategoryRankListGetDto> request = ResourceClientRequest.<CategoryRankListGetDto>getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    public ResponseObject getRankRelation(long rankingId) {
        String apiUri = "/categoryRank/relation/" + rankingId;
        ResourceClientRequest<CategoryRankRelationGetDto> request = ResourceClientRequest.<CategoryRankRelationGetDto>getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    public ResponseObject deleteAll(long rankingId) {
        String apiUri = "/categoryRank/delete/all/" + rankingId;
        return getResponseObject(apiUri);
    }

    public ResponseObject deleteRelation(long rankingId, long relationId) {
        String apiUri = "/categoryRank/delete/relation/" + rankingId + "/" + relationId;
        return getResponseObject(apiUri);
    }

    public ResponseObject insertRank(CategoryRankInsertParamDto param) {
        String apiUri = "/categoryRank/insert";
        param.setAdministratorId(loginCookieService.getUserInfo().getEmpId());
        return getResponseObject(apiUri, param);
    }

    public ResponseObject updateRank(CategoryRankUpdateParamDto param) {
        String apiUri = "/categoryRank/update";
        param.setAdministratorId(loginCookieService.getUserInfo().getEmpId());
        return getResponseObject(apiUri, param);
    }

    public ResponseObject sync() {
        String apiUri = "/categoryRank/sync";
        return getResponseObject(apiUri);
    }

    private ResponseObject getResponseObject(String apiUri){
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    private <E> ResponseObject getResponseObject(String apiUri, E elements){
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .postObject(elements)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }
}
