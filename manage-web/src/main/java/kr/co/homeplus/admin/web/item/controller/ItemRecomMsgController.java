package kr.co.homeplus.admin.web.item.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessExceptionCode;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.admin.web.item.model.recomMsg.ItemRecomMsgItemParamDto;
import kr.co.homeplus.admin.web.item.model.recomMsg.ItemRecomMsgListGetDto;
import kr.co.homeplus.admin.web.item.model.recomMsg.ItemRecomMsgListParamDto;
import kr.co.homeplus.admin.web.item.model.recomMsg.ItemRecomMsgMatchExcelDto;
import kr.co.homeplus.admin.web.item.model.recomMsg.ItemRecomMsgMatchGetDto;
import kr.co.homeplus.admin.web.item.model.recomMsg.ItemRecomMsgSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item")
public class ItemRecomMsgController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;
    private final ExcelUploadService excelUploadService;

    /**
     * 사이트관리 > 전시관리 > 상품 추천문구 관리
     *
     * @param model
     * @return String
     */
    @ApiOperation(value = "상품 추천문구 관리 main 화면")
    @ApiResponse(code = 200, message = "상품 추천문구 관리 main 화면")
    @GetMapping(value = "/recomMsg/recomMsgMain")
    public String recomMsgMain(Model model) throws Exception {
        codeService.getCodeModel(model
            , "use_yn"		// 사용여부
            , "store_type"		    // 점포유형
        );

        model.addAllAttributes(RealGridHelper.create("recomMsgGridBaseInfo", ItemRecomMsgListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("recomItemGridBaseInfo", ItemRecomMsgMatchGetDto.class));

        return "/item/recomMsgMain";
    }

    /**
     * 사이트관리 > 전시관리 > 상품 추천문구 리스트
     *
     * @param itemRecomMsgListParamDto
     * @return Object
     */
    @ResponseBody
    @GetMapping(value = {"/recomMsg/getRecomMsgList.json"})
    public List<ItemRecomMsgListGetDto> getRecomMsgList(ItemRecomMsgListParamDto itemRecomMsgListParamDto) throws Exception {
        String apiUri = "/item/recomMsg/getRecomMsgList";

        ResourceClientRequest<List<ItemRecomMsgListGetDto>> requestClient = ResourceClientRequest.<List<ItemRecomMsgListGetDto>>postBuilder().apiId(ResourceRouteName.ITEM)
            .uri(apiUri)
            .postObject(itemRecomMsgListParamDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<List<ItemRecomMsgListGetDto>>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     *  사이트관리 > 전시관리 > 상품 추천문구 상세 조회 (대상 상품 조회)
     * @param recomNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/recomMsg/getRecomMsgDetail.json"})
    public List<ItemRecomMsgMatchGetDto> getRecomMsgDetail(@RequestParam(name = "recomNo") Long recomNo) {
        String apiUri = "/item/recomMsg/getRecomMsgMatchList?recomNo=" + recomNo;

        ResourceClientRequest<List<ItemRecomMsgMatchGetDto>> requestClient = ResourceClientRequest.<List<ItemRecomMsgMatchGetDto>>getBuilder().apiId(ResourceRouteName.ITEM)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<List<ItemRecomMsgMatchGetDto>>> responseEntity = resourceClient.get(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 사이트관리 > 전시관리 > 상품 추천문구관리 상품 일괄등록 팝업
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/popup/recomMsgExcelUploadPop")
    public String uploadLeafletItemPop(Model model, @RequestParam(value = "storeType") String storeType, @RequestParam(value = "callback") String callback) {
        model.addAttribute("storeType", storeType);
        model.addAttribute("callBackScript", callback);
        return "/item/pop/recomMsgExcelUploadPop";
    }

    /**
     * 추천문구 상품 엑셀 일괄등록 적용
     */
    @ResponseBody
    @PostMapping("/recomMsg/recomMsgItemExcel.json")
    public List<ItemRecomMsgMatchGetDto> recomMsgItemExcel(MultipartHttpServletRequest multipartHttpServletRequest) throws BusinessLogicException {
        // 파일을 읽기위해 input type file 태그 name 작성
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");
        List<ItemRecomMsgMatchExcelDto> excelData;

        // 1. 엑셀 메타정보에 매핑정보 입력
        ExcelHeaders headers = new ExcelHeaders.Builder().header(0, "상품번호", "itemNo", true).build();

        //  1-1. 엑셀 메타정보 입력 (엑셀의 시작/마지막 행, 시작/마지막열, 읽을 대상시트, 맵핑 할 DTO 필드 정보입력)
        ExcelUploadOption<ItemRecomMsgMatchExcelDto> excelUploadOption = new ExcelUploadOption.Builder<>(ItemRecomMsgMatchExcelDto.class, headers, 1, 0).build();

        // 2. 엑셀 조회 (multipartFile 과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.)
        try {
            excelData = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            //파일 양식이 잘못되었거나 오류로 반영에 실패했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1005);
        }

        // 3. 매핑된 데이터에 대한 검증
        Integer maxCnt = 1000;

        // 3-1. 최대개수 확인
        if (excelData.size() > maxCnt) {
            //일괄등록 1회 수행시 최대 상품 1,000개를 초과했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1007, 1000);
        }

        List<String> itemNoList = excelData.stream().map(ItemRecomMsgMatchExcelDto::getItemNo).distinct().collect(Collectors.toList());
        String storeType = multipartHttpServletRequest.getParameter("storeType");

        ItemRecomMsgItemParamDto itemParams = new ItemRecomMsgItemParamDto();
        itemParams.setItemNoList(itemNoList);
        itemParams.setStoreType(storeType);

        // 일괄등록 요청상품 itemNo로 검증
        String apiUri = "/item/recomMsg/getItemList";

        ResourceClientRequest<List<ItemRecomMsgMatchGetDto>> requestClient = ResourceClientRequest.<List<ItemRecomMsgMatchGetDto>>postBuilder().apiId(ResourceRouteName.ITEM)
            .uri(apiUri)
            .postObject(itemParams)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<List<ItemRecomMsgMatchGetDto>>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 사이트관리 > 전시관리 > 상품 추천문구 등록/수정
     *
     * @param itemRecomMsgSetDto
     * @return Long
     */
    @ResponseBody
    @PostMapping(value = {"/recomMsg/setRecomMsg.json"})
    public Long setRecomMsg(@Valid @RequestBody ItemRecomMsgSetDto itemRecomMsgSetDto) {
        itemRecomMsgSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        String apiUri = "/item/recomMsg/setRecomMsg";

        ResourceClientRequest<Long> requestClient = ResourceClientRequest.<Long>postBuilder().apiId(ResourceRouteName.ITEM)
            .uri(apiUri)
            .postObject(itemRecomMsgSetDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<Long>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }
}
