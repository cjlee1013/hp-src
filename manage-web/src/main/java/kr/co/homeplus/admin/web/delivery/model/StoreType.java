package kr.co.homeplus.admin.web.delivery.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum StoreType {

    HYPER("HYPER","Hyper"),
    CLUB("CLUB","Club"),
    AURORA("AURORA","Aurora"),
    EXP("EXP","Express"),
    ;

    private String code;
    private String name;
}
