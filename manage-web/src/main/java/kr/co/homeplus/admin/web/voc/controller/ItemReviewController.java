package kr.co.homeplus.admin.web.voc.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.voc.model.ItemReviewHistListGetDto;
import kr.co.homeplus.admin.web.voc.model.ItemReviewImgListGetDto;
import kr.co.homeplus.admin.web.voc.model.ItemReviewListParamDto;
import kr.co.homeplus.admin.web.voc.model.ItemReviewListSelectDto;
import kr.co.homeplus.admin.web.voc.model.ItemReviewSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/voc")
public class ItemReviewController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    @Value("${front.homeplus.url}")
    private String frontUrl;

    /**
     *  회원관리 > 고객문의관리 > 상품평관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/itemReview/itemReviewMain", method = RequestMethod.GET)
    public String itemReviewMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type"   // 스토어 타입
            , "block_yn"            // 미노출여부
            , "reserve_yn"          // 적립여부
        );

        model.addAttribute("storeType", code.get("store_type"));
        model.addAttribute("blockYn", code.get("block_yn"));
        model.addAttribute("reserveYn", code.get("reserve_yn"));

        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);
        model.addAttribute("frontUrl", frontUrl);

        model.addAllAttributes(RealGridHelper.create("itemReviewListGridBaseInfo", ItemReviewListSelectDto.class));

        return "/voc/itemReviewMain";
    }

    /**
     * 회원관리 > 고객문의관리 > 상품평관리 리스트 조회
     *
     * @param itemReviewListParamDto
     * @return List<ItemReviewListSelectDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/itemReview/getItemReviewList.json"}, method = RequestMethod.GET)
    public List<ItemReviewListSelectDto> getItemReviewList(ItemReviewListParamDto itemReviewListParamDto) {
        String apiUri = "/voc/itemReview/getItemReviewList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemReviewListSelectDto>>>() {};
        return (List<ItemReviewListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, ItemReviewListParamDto.class, itemReviewListParamDto), typeReference).getData();
    }

    /**
     * 회원관리 > 고객문의관리 > 상품평관리 이미지 조회
     *
     * @param reviewNo
     * @return List<ItemReviewImgListGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/itemReview/getItemReviewImg.json"}, method = RequestMethod.GET)
    public List<ItemReviewImgListGetDto> getItemReviewImg(@RequestParam(name = "reviewNo") String reviewNo) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("reviewNo", reviewNo));

        String apiUriRequest = "/voc/itemReview/getItemReviewImg" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<ItemReviewImgListGetDto>>>() {})
            .getData();
    }

    /**
     * 회원관리 > 고객문의관리 > 상품평관리 미노출 등록
     * @param itemReviewSetParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/itemReview/setBlockContents.json"}, method = RequestMethod.POST)
    public ResponseResult setBlockContents(@RequestBody ItemReviewSetParamDto itemReviewSetParamDto) throws Exception {
        String apiUri = "/voc/itemReview/setBlockContents";
        itemReviewSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, itemReviewSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 회원관리 > 고객문의관리 > 상품평관리 미노출 히스토리 (팝업)
     * @param model
     * @param reviewNo
     * @return
     * @throws Exception
     */
    @RequestMapping("/popup/itemReviewBlockHistoryPop")
    public String itemReviewBlockHistory(Model model, @RequestParam(value="reviewNo") Long reviewNo) {
        model.addAllAttributes(RealGridHelper.create("itemReviewBlockHistoryPopBaseInfo", ItemReviewHistListGetDto.class));
        model.addAttribute("reviewNo", reviewNo);
        return "/voc/pop/itemReviewBlockHistoryPop";
    }

    /**
     * 회원관리 > 고객문의관리 > 상품평관리 미노출 히스토리 조회
     *
     * @param reviewNo
     * @return List<ItemReviewListSelectDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/itemReview/getItemReviewHist.json"}, method = RequestMethod.GET)
    public List<ItemReviewHistListGetDto> getItemReviewHist(Long reviewNo) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("reviewNo", reviewNo));

        String apiUriRequest = "/voc/itemReview/getItemReviewHist" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<ItemReviewHistListGetDto>>>() {})
            .getData();
    }

}
