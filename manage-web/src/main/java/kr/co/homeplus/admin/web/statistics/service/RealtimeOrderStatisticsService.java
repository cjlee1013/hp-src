package kr.co.homeplus.admin.web.statistics.service;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import kr.co.homeplus.admin.web.statistics.model.RealtimePartnerOrderStatisticsDto;
import kr.co.homeplus.admin.web.statistics.model.RealtimeStoreOrderStatisticsDto;
import kr.co.homeplus.admin.web.statistics.model.StatisticsOrderInfoSelectDto;
import kr.co.homeplus.admin.web.statistics.model.StoreBasicInfoDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RealtimeOrderStatisticsService {
    private final ResourceClient resourceClient;

    /**
     * 실시간 점포별 판매현황 리스트 조회
     * @param statisticsOrderInfoSelectDto
     * @return
     * @throws Exception
     */
    public List<RealtimeStoreOrderStatisticsDto> getRealtimeStoreOrderStatisticsList(StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_STATISTICS_ORDER_INFO_LIST_BY_STORE;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<RealtimeStoreOrderStatisticsDto>>>() {};
        return (List<RealtimeStoreOrderStatisticsDto>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, statisticsOrderInfoSelectDto, apiUri, typeReference).getData();
    }

    /**
     * 실시간 판매업체별 판매현황 리스트 조회
     * @param statisticsOrderInfoSelectDto
     * @return
     * @throws Exception
     */
    public List<RealtimePartnerOrderStatisticsDto> getRealtimePartnerOrderStatisticsList(StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_STATISTICS_ORDER_INFO_LIST_BY_PARTNER;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<RealtimePartnerOrderStatisticsDto>>>() {};
        return (List<RealtimePartnerOrderStatisticsDto>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, statisticsOrderInfoSelectDto, apiUri, typeReference).getData();
    }

    /**
     * 점포 기본정보 조회
     * @param storeId
     * @return
     * @throws Exception
     */
    public StoreBasicInfoDto getStoreBasicInfo(String storeId) throws Exception {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("storeId", storeId));
        String apiUri = EscrowConstants.ESCROW_GET_STORE_BASIC_INFO + StringUtil.getParameter(setParameterList);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<StoreBasicInfoDto>>() {};
        return (StoreBasicInfoDto) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 추출이력 저장
     * @param extractCommonSetDto
     */
    public void saveExtractHistory(ExtractCommonSetDto extractCommonSetDto) {
        resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, extractCommonSetDto, EscrowConstants.EXTRACT_SAVE_EXTRACT_HISTORY);
    }
}
