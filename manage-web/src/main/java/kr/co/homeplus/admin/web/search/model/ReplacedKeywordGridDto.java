package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class ReplacedKeywordGridDto {
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
    public static class ReplacedKeyword {
        @RealGridColumnInfo(headText = "번호", width = 0, hidden = true, columnType = RealGridColumnType.BASIC)
        private Long replacedId;
        @RealGridColumnInfo(headText = "검색어", width = 40, sortable = true, columnType = RealGridColumnType.BASIC)
        private String keyword;
        @RealGridColumnInfo(headText = "대체검색어", width = 40, sortable = true, columnType = RealGridColumnType.BASIC)
        private String replacedKeyword;
        @RealGridColumnInfo(headText = "사용여부", width = 0, hidden = true, columnType = RealGridColumnType.BASIC)
        private String useFlag;
        @RealGridColumnInfo(headText = "사용여부", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String useFlagNm;
        @RealGridColumnInfo(headText = "시작일", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String sdate;
        @RealGridColumnInfo(headText = "종료일", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String edate;
        @RealGridColumnInfo(headText = "등록자", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String creator;
        @RealGridColumnInfo(headText = "등록일", width = 30, sortable = true, columnType = RealGridColumnType.BASIC)
        private String regDt;
        @RealGridColumnInfo(headText = "수정자", width = 30, sortable = true, columnType = RealGridColumnType.BASIC)
        private String modifier;
        @RealGridColumnInfo(headText = "수정일", width = 30, sortable = true, columnType = RealGridColumnType.BASIC)
        private String updateDt;
        @RealGridColumnInfo(headText = "사용기간 설정여부", width = 0, hidden = true, columnType = RealGridColumnType.BASIC)
        private String operationPeriodYn;
    }
}
