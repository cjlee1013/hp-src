package kr.co.homeplus.admin.web.item.model.item;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
public class ItemStoreListGetDto {

    //점포ID
    @RealGridColumnInfo(headText = "점포ID", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.SERIAL, sortable = true)
    private String storeId;

    //점포명
    @RealGridColumnInfo(headText = "점포명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String storeNm;

    //점포타입명
    @RealGridColumnInfo(headText = "점포유형", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String storeTypeNm;

    @RealGridColumnInfo(headText = "온라인재고수량", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
    @Builder.Default
    private String stockQtyUI = "-";

    //온라인기초재고수량
    private String stockQtyBasic;

    //온라인재고수량
    @RealGridColumnInfo(headText = "온라인재고수량", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C, sortable = true)
    private String stockQty;

    //N마트 온라인기초재고수량
    private String stockQtyNaver;

    //N마트 온라인재고수량
    private String remainCntNaver;

    //11번가 온라인기초재고수량
    private String stockQtyEleven;

    //11번가 온라인재고수량
    private String remainCntEleven;

    //오프라인재고수량
    private String stockQtyOff;

    @RealGridColumnInfo(headText = "오프라인재고수량", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    @Builder.Default
    private String offStockQtyUI = "-";

    //재고수량 변경여부 (Y: 변경, N|NULL: 미변경)
    @RealGridColumnInfo(headText = "재고수량변경여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String chgStockQtyYn;

    private String chgStockQtyYnNaver;

    private String chgStockQtyYnEleven;

    //판매가격
    @RealGridColumnInfo(headText = "판매가", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, sortable = true)
    private String salePrice;

    @RealGridColumnInfo(headText = "FC상품", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String fcStatusFlag;

    //FC 상태구분(00:미등록,10:IN승인대기,15:IN승인거부,20:IN승인완료,30:IN확정,40:FC판매,50:OUT확정대기,60:OUT확정,99:PACK미등록)-GOD_FC_MST.STATUS_FLAG
    @RealGridColumnInfo(headText = "FC상품", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String fcStatusFlagNm;

    //점포택배 설정명
    @RealGridColumnInfo(headText = "점포택배", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String storeDlvNm;

    //점포구분명(일반점:NOR, 택배점:DLV)
    @RealGridColumnInfo(headText = "점포구분", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String storeKindNm;

    //온라인취급여부명
    @RealGridColumnInfo(headText = "온라인취급상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String pfrYnNm;

    //가상점 단독판매여부
    @RealGridColumnInfo(headText = "가상점 단독판매여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String virtualStoreOnlyYn;

    //가상점 단독판매
    @RealGridColumnInfo(headText = "가상점 단독판매", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String virtualStoreOnlyYnNm;

    //취급중지
    @RealGridColumnInfo(headText = "취급중지코드", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String stopDealYn;

    //취급중지명
    @RealGridColumnInfo(headText = "취급중지", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String stopDealYnNm;

    //중지제한여부 (Y: 무제한, N: 기간제한)
    @RealGridColumnInfo(headText = "중지제한여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String stopLimitYn;

    //중지시작일
    @RealGridColumnInfo(headText = "중지시작일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String stopStartDt;

    //중지종료일
    @RealGridColumnInfo(headText = "중지종료일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String stopEndDt;

    //중지사유
    @RealGridColumnInfo(headText = "중지사유", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String stopReason;

    //자차배송여부명
    @RealGridColumnInfo(headText = "자차", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String drctYnNm;

    //택배여부명
    @RealGridColumnInfo(headText = "택배", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String dlvYnNm;

    //퀵배송여부명
    @RealGridColumnInfo(headText = "퀵", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String quickYnNm;

    //픽업여부명
    @RealGridColumnInfo(headText = "픽업", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String pickYnNm;

    //자차배송여부
    @RealGridColumnInfo(headText = "자차배송여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String drctYn;

    //택배여부
    @RealGridColumnInfo(headText = "택배여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String dlvYn;

    //퀵배송여부
    @RealGridColumnInfo(headText = "퀵배송여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String quickYn;

    //픽업
    @RealGridColumnInfo(headText = "방문수령여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String pickYn;

    //자차배송 배송정택번호
    @RealGridColumnInfo(headText = "자차배송 배송정택번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private Long drctShipPolicyNo;

    //택배 배송정책번호
    @RealGridColumnInfo(headText = "택배 배송정책번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private Long dlvShipPolicyNo;

    //퀵배송 배송정책번호
    @RealGridColumnInfo(headText = "퀵배송 배송정책번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private Long quickShipPolicyNo;

    //픽업 배송정책번호
    @RealGridColumnInfo(headText = "픽업 배송정책번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private Long pickShipPolicyNo;

    //점포구분(일반점:NOR, 택배점:DLV)
    @RealGridColumnInfo(headText = "점포구분코드", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String storeKind;

    //점포타입
    @RealGridColumnInfo(headText = "점포타입코드", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String storeType;

    //안심번호 사용여부명
    @RealGridColumnInfo(headText = "안심번호 사용여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String safeNumberUseYnNm;

    //등록일
    private String regDt;

    //등록자
    private String regNm;

}
