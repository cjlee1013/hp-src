package kr.co.homeplus.admin.web.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 점포배송정보 > 점포별 우편번호 조회 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class StoreZipcodeModel {
    @ApiModelProperty(notes = "우편번호", position = 1)
    @RealGridColumnInfo(headText = "우편번호", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String zipcode;

    @ApiModelProperty(notes = "시도", position = 2)
    @RealGridColumnInfo(headText = "시도", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String sidoNm;

    @ApiModelProperty(notes = "시군구", position = 3)
    @RealGridColumnInfo(headText = "시군구", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String sigunguNm;

    @ApiModelProperty(notes = "도로명", position = 4)
    @RealGridColumnInfo(headText = "도로명", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String roadAddrNm;

    @ApiModelProperty(notes = "건물번호", position = 5)
    @RealGridColumnInfo(headText = "건물번호", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String buildNo;

    @ApiModelProperty(notes = "건물명", position = 6)
    @RealGridColumnInfo(headText = "건물명", width = 150, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME)
    private String sigunguBuildNm;

    @ApiModelProperty(notes = "법정동", position = 7)
    @RealGridColumnInfo(headText = "법정동", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String courtEubmyndnNm;

    @ApiModelProperty(notes = "점포명", position = 8)
    @RealGridColumnInfo(headText = "점포명", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeNm;

    @ApiModelProperty(notes = "원거리배송여부", position = 9)
    @RealGridColumnInfo(headText = "원거리배송여부", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String remoteShipYn;

    @ApiModelProperty(notes = "사용여부", position = 10)
    @RealGridColumnInfo(headText = "사용여부", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String useYn;

    @ApiModelProperty(notes = "도서산간 가능여부", position = 11)
    @RealGridColumnInfo(headText = "도서산간 가능여부", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String islandTypeYn;
}