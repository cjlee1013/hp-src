package kr.co.homeplus.admin.web.manage.model.dspExpMain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspExpMainItemExcelDto {

    @EqualsAndHashCode.Include
    private String itemNo;
    @EqualsAndHashCode.Include
    private String itemNm;
    @EqualsAndHashCode.Include
    private String dispYn;
    @EqualsAndHashCode.Include
    private String dispYnTxt;

}
