package kr.co.homeplus.admin.web.core.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Setter
@Getter
public class LoginParam {

    @ApiModelProperty(value = "로그인아이디", required = true)
    @NotNull(message = "아이디를 입력하세요.")
    @Length(min = 3, max = 20, message = "아이디는 3에서 20자리 이내로 입력해주세요.")
    private String userId;

    @ApiModelProperty(value = "로그인비밀번호", required = true)
    @NotNull(message = "비밀번호를 입력하세요.")
    private String userPw;

    @ApiModelProperty(value = "아이디 저장여부", required = true)
    private boolean chkSaveUserId;
}
