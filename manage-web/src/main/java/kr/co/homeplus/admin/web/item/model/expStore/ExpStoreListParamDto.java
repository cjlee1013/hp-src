package kr.co.homeplus.admin.web.item.model.expStore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpStoreListParamDto {

    //검색어
    private String searchType;

    //검색키워드
    private String searchKeyword;

    //주문가능여부
    private String searchOrderAvailYn;
}
