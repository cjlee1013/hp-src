package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.admin.web.claim.model.ClaimPreRefundSetDto.ClaimPreRefundItemList;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClaimReasonChangeSetDto {

    @NotNull(message = "클레임요청번호 없습니다.")
    @ApiModelProperty(value = "클레임요청번호", position = 1)
    private String claimReqNo;

    @NotNull(message = "클레임사유코드가 없습니다.")
    @ApiModelProperty(value = "클레임사유코드", position = 2)
    @Length(max = 4, message = "클레임사유코드")
    private String claimReasonType;

    @ApiModelProperty(value = "클레임사유상세", position = 3)
    private String claimReasonDetail;

}
