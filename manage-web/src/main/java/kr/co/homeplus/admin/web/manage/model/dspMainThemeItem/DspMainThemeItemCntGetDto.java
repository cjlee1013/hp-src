package kr.co.homeplus.admin.web.manage.model.dspMainThemeItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspMainThemeItemCntGetDto {

    private String dispDt;
    private int itemCnt;

}
