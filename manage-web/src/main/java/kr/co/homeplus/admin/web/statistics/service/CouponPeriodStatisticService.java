package kr.co.homeplus.admin.web.statistics.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.statistics.model.CouponPeriodStatisticsDetailGetDto;
import kr.co.homeplus.admin.web.statistics.model.CouponPeriodStatisticsGetDto;
import kr.co.homeplus.admin.web.statistics.model.CouponPeriodStatisticsSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CouponPeriodStatisticService {

    private final ResourceClient resourceClient;

    /**
     * 기간별 쿠폰통계 메인조회
     * @param
     * @return
     */
    public List<CouponPeriodStatisticsGetDto> getCouponPeriodStatisticsList(CouponPeriodStatisticsSetDto couponPeriodStatisticsSetDto) {

        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            couponPeriodStatisticsSetDto,
            "/admin/statistics/getCouponPeriodStatisticsList",
            new ParameterizedTypeReference<ResponseObject<List<CouponPeriodStatisticsGetDto>>>(){}).getData();
    }

    /**
     * 기간별 쿠폰통계 상세조회
     * @param
     * @return
     */
    public List<CouponPeriodStatisticsDetailGetDto> getCouponPeriodStatisticsDetailList(CouponPeriodStatisticsSetDto couponPeriodStatisticsSetDto) {

        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            couponPeriodStatisticsSetDto,
            "/admin/statistics/getCouponPeriodStatisticsDetailList",
            new ParameterizedTypeReference<ResponseObject<List<CouponPeriodStatisticsDetailGetDto>>>(){}).getData();
    }
}
