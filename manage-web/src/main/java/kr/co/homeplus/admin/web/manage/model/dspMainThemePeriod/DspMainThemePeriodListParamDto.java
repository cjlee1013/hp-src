package kr.co.homeplus.admin.web.manage.model.dspMainThemePeriod;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspMainThemePeriodListParamDto {

    private String searchPeriodType;
    private String searchStartDt;
    private String searchEndDt;
    private String searchLocType;
    private String searchUseYn;
    private String searchType;
    private String searchKeyword;

}
