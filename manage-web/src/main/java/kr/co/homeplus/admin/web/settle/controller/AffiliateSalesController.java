package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.settle.model.affiliateSales.AffiliateDetailGetDto;
import kr.co.homeplus.admin.web.settle.model.affiliateSales.AffiliateDetailSetDto;
import kr.co.homeplus.admin.web.settle.model.affiliateSales.AffiliateSalesGetDto;
import kr.co.homeplus.admin.web.settle.model.affiliateSales.AffiliateSalesSetDto;
import kr.co.homeplus.admin.web.settle.service.AffiliateSalesService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/settle/affiliate")
public class AffiliateSalesController {

  private final AffiliateSalesService affiliateSalesService;
  private final CodeService codeService;

  @RequestMapping(value = "/affiliateList", method = RequestMethod.GET)
  public String affiliateList(Model model) throws Exception {

    Map<String, List<MngCodeGetDto>> code = codeService.getCode(
        "site_type"
    );
    model.addAttribute("siteType", code.get("site_type"));
    model.addAllAttributes(RealGridHelper.create("affiliateSalesBaseInfo", AffiliateSalesGetDto.class));
    model.addAllAttributes(RealGridHelper.create("affiliateDetailBaseInfo", AffiliateDetailGetDto.class));
    model.addAllAttributes(RealGridHelper.create("affiliateDetailAllBaseInfo", AffiliateDetailGetDto.class));
    return "/settle/affiliateList";
  }

  @ResponseBody
  @RequestMapping(value = "/getSales.json", method = RequestMethod.GET)
  public List<AffiliateSalesGetDto> getAffiliateSales(AffiliateSalesSetDto affiliateSalesSetDto) throws Exception {
    return affiliateSalesService.getAffiliateSales(affiliateSalesSetDto);
  }

  @ResponseBody
  @RequestMapping(value = "/getDetail.json", method = RequestMethod.GET)
  public List<AffiliateDetailGetDto> getAffiliateDetail(
      AffiliateDetailSetDto affiliateDetailSetDto) throws Exception {
    return affiliateSalesService.getAffiliateDetail(affiliateDetailSetDto);
  }

}
