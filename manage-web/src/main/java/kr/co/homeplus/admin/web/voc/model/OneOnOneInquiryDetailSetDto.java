package kr.co.homeplus.admin.web.voc.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 1:1문의 상세 요청 DTO")
public class OneOnOneInquiryDetailSetDto {
  @ApiModelProperty(notes = "문의번호")
  private String inqryNo;

  @ApiModelProperty(notes = "문의종류")
  private String inqryKind;

  @ApiModelProperty(notes = "처리상태코드")
  private String inqryStatus;

  @ApiModelProperty(notes = "구분코드")
  private String inqryCategory;

  @ApiModelProperty(notes = "문의구분코드")
  private String inqryType;

  @ApiModelProperty(notes = "문의항목코드")
  private String inqryDetailType;

  @ApiModelProperty(notes = "답변내용")
  private String inqryAnswrCntnt;

  @ApiModelProperty(notes = "재문의답변")
  private String reInqryAnswrCntnt;

  @ApiModelProperty(notes = "직원메모")
  private String empMemo;

  @ApiModelProperty(notes = "직원메모여부")
  private String empMemoYn;

  @ApiModelProperty(notes = "등록자")
  private String regId;

  @ApiModelProperty(notes = "등록자명")
  private String regNm;
}