package kr.co.homeplus.admin.web.search.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DictionaryListGetDto {

    ///어휘번호
    private Integer wordId;
    ///어휘명
    private String wordName;
    ///의미태그
    private String senseTag;
    ///사전
    private String dicType;
    ///품사
    private String pos;
    ///등록일
    private String regDate;
    private String regUserId;
    private String modDate;
    private String modUserId;
    ///사용여부
    private boolean useFlag;
    private String useFlagNm;
    private String note;
    ///불용어여부
    private boolean stopFlag;
    private String stopFlagNm;
    private String synonym;
    private String hyponym;
    private String relatednym;
    private String isGeneral;

}
