package kr.co.homeplus.admin.web.item.model.sameClass;

import lombok.Data;


@Data
public class SameClassGroupSetDto {

    private Integer sameClassNo;

    private String sameClassNm;

    private String hyperLimitYn;

    private Integer hyperLimitQty;

    private String clubLimitYn;

    private Integer clubLimitQty;

    private String expLimitYn;

    private Integer expLimitQty;

    private String useYn;


    private String regId;
}
