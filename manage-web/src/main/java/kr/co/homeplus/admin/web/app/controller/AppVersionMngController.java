package kr.co.homeplus.admin.web.app.controller;


import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.app.model.AppVersionInfoDto;
import kr.co.homeplus.admin.web.app.model.AppVersionModifyParam;
import kr.co.homeplus.admin.web.app.model.AppVersionRegistParam;
import kr.co.homeplus.admin.web.app.model.AppVersionSearchParam;
import kr.co.homeplus.admin.web.app.service.AppVersionMngService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 앱 버전관리
 */
@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping(value = "/app/version/mng")
public class AppVersionMngController {

    private final Map<String, Object> appVersionMngGridBaseInfo = RealGridHelper
        .create("appVersionMngGridBaseInfo", AppVersionInfoDto.class);

    private final ResourceClient resourceClient;
    private final AppVersionMngService appVersionMngService;


    @GetMapping(value = "/main")
    public String getAppVersionMain(final Model model) {
        model.addAllAttributes(appVersionMngGridBaseInfo);
        return "/app/appVersionInfoMain";
    }

    @ResponseBody
    @GetMapping(value = "/getAppVersionList.json", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AppVersionInfoDto> getSearchAppVersionList(
        AppVersionSearchParam searchParam) {
        return appVersionMngService.getSearchAppVersionList(searchParam);
    }

    @ResponseBody
    @GetMapping(value = "/getAppVersion.json", produces = MediaType.APPLICATION_JSON_VALUE)
    public AppVersionInfoDto getAppVersion(@RequestParam Integer seq) {
        return appVersionMngService.getAppVersion(seq);
    }

    @ResponseBody
    @PostMapping(value = "/setAppVersion.json")
    public ResponseObject setAppVersion(
        @Valid @RequestBody AppVersionRegistParam appVersionRegistParam) {
        ResponseObject responseObject = appVersionMngService.setAppVersion(appVersionRegistParam);
        return ResourceConverter.toResponseObject(responseObject);
    }

    @ResponseBody
    @PostMapping(value = "/putAppVersion.json")
    public ResponseObject putAppVersion(
        @Valid @RequestBody AppVersionModifyParam appVersionModifyParam) {
        ResponseObject responseObject = appVersionMngService.putAppVersion(appVersionModifyParam);
        return ResourceConverter.toResponseObject(responseObject);
    }
}
