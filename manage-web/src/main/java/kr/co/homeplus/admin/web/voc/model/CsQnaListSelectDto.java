package kr.co.homeplus.admin.web.voc.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class CsQnaListSelectDto {

    @RealGridColumnInfo(headText = "문의번호", width = 70)
    private String qnaNo;

    @RealGridColumnInfo(headText = "문의일시", width = 120, order = 1)
    private String regDt;

    @RealGridColumnInfo(headText = "상품번호", width = 120, order = 2)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 200, order = 3)
    private String itemNm;

    @RealGridColumnInfo(headText = "판매업체ID", width = 80, order = 4)
    private String partnerId;

    @RealGridColumnInfo(headText = "판매업체명", width = 120, order = 5)
    private String partnerNm;

    @RealGridColumnInfo(headText = "구매여부", width = 50, order = 6)
    private String purchaseYn;

    @RealGridColumnInfo(headText = "작성자ID", width = 80, order = 7)
    private String userId;

    @RealGridColumnInfo(headText = "작성자", width = 80, order = 8)
    private String userNm;

    @RealGridColumnInfo(headText = "비밀글여부", width = 50, order = 9)
    private String secretYn;

    @RealGridColumnInfo(headText = "노출여부", width = 50, order = 10)
    private String displayStatusTxt;

    @RealGridColumnInfo(headText = "답변여부", width = 50, order = 11)
    private String qnaStatusTxt;

    @RealGridColumnInfo(headText = "qnaContents", hidden = true, order = 12)
    private String qnaContents;
    @RealGridColumnInfo(headText = "purchaseOrderNo", hidden = true, order = 13)
    private String purchaseOrderNo;
    @RealGridColumnInfo(headText = "displayStatus", hidden = true, order = 14)
    private String displayStatus;

}
