package kr.co.homeplus.admin.web.message.model;

import java.util.HashMap;
import java.util.Map;
import lombok.Data;

@Data
public class BasicRecipient {

    public static final String IDENTIFIER = "회원번호";

    public static final String TO_TOKEN = "연락처";

    private String identifier;

    private String toToken;

    private Map<String, String> mappingData;

    public void setMappingData(String k, String v) {
        if (mappingData == null) {
            mappingData = new HashMap<>();
        }
        mappingData.put(k, v);
    }
}
