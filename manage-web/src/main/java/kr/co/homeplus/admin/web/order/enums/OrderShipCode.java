package kr.co.homeplus.admin.web.order.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum OrderShipCode {

    ORDER_CHECK("D1"),
    ORDER_INVOICE_REG("D2"),
    ORDER_INVOICE_EDIT("D3,D4"),
    ORDER_SHIP_COMPLETE("D4");

    @Getter
    private final String code;
}
