package kr.co.homeplus.admin.web.claim.service;

import java.util.List;
import kr.co.homeplus.admin.web.claim.model.ClaimGiftItemListDto;
import kr.co.homeplus.admin.web.claim.model.ClaimProcessReqSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimShipCompleteSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimShippingSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimShippingStatusSetDto;
import kr.co.homeplus.admin.web.claim.model.DiscountInfoListDto;
import kr.co.homeplus.admin.web.claim.model.PromoInfoListDto;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimPopupService {

    private final ResourceClient resourceClient;

    private final LoginCookieService loginCookieService;

    /**
     * 클레임 승인/반품/교환 상태변경 요청
     *
     * @param claimProcessReqSetDto 클레임 승인/반품/교환 요청 dto
     * @return ClaimProcessRegGetDto 클레임 승인/반품/교환 응답 dto
     */
    public ResponseObject<String> reqClaimProcess(ClaimProcessReqSetDto claimProcessReqSetDto) throws Exception {
        claimProcessReqSetDto.setRegId(loginCookieService.getUserInfo().getEmpId());//요청자id
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            claimProcessReqSetDto,
            "/claim/reqChange",
            new ParameterizedTypeReference<ResponseObject<String>>() {
            }
        );
    }

    /**
     * 차감 할인정보 조회
     *
     * @param claimNo
     * @return DiscountInfoListDto
     */
    public List<DiscountInfoListDto> getDiscountInfoList(long claimNo){

        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
             null,
            "/claim/getDiscountInfoList?claimNo="+claimNo,
            new ParameterizedTypeReference<ResponseObject<List<DiscountInfoListDto>>>() {}
        ).getData();
    }

    /**
     * 취소사은행사 조회
     *
     * @param claimNo
     * @return ClaimGiftItemListDto
     */
    public List<ClaimGiftItemListDto> claimGiftInfoList(String claimNo){

        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
             null,
            "/claim/claimGiftInfoList?claimNo="+claimNo,
            new ParameterizedTypeReference<ResponseObject<List<ClaimGiftItemListDto>>>() {}
        ).getData();
    }

    /**
     * 취소사은행사 조회
     *
     * @param claimNo
     * @return PromoInfoListDto
     */
    public List<PromoInfoListDto> getPromoInfoList(long claimNo){

        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
             null,
            "/claim/getPromoInfoList?claimNo="+claimNo,
            new ParameterizedTypeReference<ResponseObject<List<PromoInfoListDto>>>() {}
        ).getData();
    }


    /**
     * 수거상태 변경 변경
     *
     * @param
     */
    public ResponseObject<Boolean> reqChgPickStatus(ClaimShippingStatusSetDto setDto){
        setDto.setChgId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            setDto,
            "/claim/reqClaimShippingStatus",
            new ParameterizedTypeReference<ResponseObject<Boolean>>() {
            }
        );
    }


     /**
     * 번들 배송완료 요청 변경
     *
     * @param
     */
    public ResponseObject<String> reqPickShipComplete(ClaimShipCompleteSetDto setDto){
        setDto.setChgId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            setDto,
            "/admin/shipManage/setShipCompleteByBundle",
            new ParameterizedTypeReference<ResponseObject<String>>() {
            }
        );
    }
    /**
     * 클레임 상태변경 요청
     * **/
    public ResponseObject<String> chgClaimShipping(ClaimShippingSetDto claimShippingSetDto){
        claimShippingSetDto.setChgId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, claimShippingSetDto, "/claim/reqClaimShippingChange",
            new ParameterizedTypeReference<ResponseObject<String>>() {});
    }
}
