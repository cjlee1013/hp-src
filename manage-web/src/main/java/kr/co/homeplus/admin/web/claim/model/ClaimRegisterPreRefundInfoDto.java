package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClaimRegisterPreRefundInfoDto {

    @ApiModelProperty(value = "클레임번호", position = 1)
    private long claimNo;

    @ApiModelProperty(value = "환불금액", position = 2)
    private long refundAmt;

    @ApiModelProperty(value = "DGV금액", position = 3)
    private long dgvAmt;

    @ApiModelProperty(value = "MHC금액", position = 4)
    private long mhcAmt;

    @ApiModelProperty(value = "마일리지금액", position = 5)
    private long mileageAmt;

    @ApiModelProperty(value = "OCB금액", position = 6)
    private long ocbAmt;

    @ApiModelProperty(value = "PG결제금액", position = 7)
    private long pgAmt;

    @ApiModelProperty(value = "추가도서산간배송비", position = 8)
    private long addIslandShipPrice;

    @ApiModelProperty(value = "추가배송비", position = 9)
    private long addShipPrice;

    @ApiModelProperty(value = "장바구니할인금액", position = 10)
    private long cartDiscountAmt;

    @ApiModelProperty(value = "임직원할인금액", position = 11)
    private long empDiscountAmt;

    @ApiModelProperty(value = "도서산간배송금액", position = 12)
    private long islandShipPrice;

    @ApiModelProperty(value = "상품할인금액", position = 13)
    private long itemDiscountAmt;

    @ApiModelProperty(value = "상품금액", position = 14)
    private long itemPrice;

    @ApiModelProperty(value = "배송비할인금액", position = 15)
    private long shipDiscountAmt;

    @ApiModelProperty(value = "배송금액", position = 16)
    private long shipPrice;

    @ApiModelProperty(value = "할인금액", position = 17)
    private long discountAmt;

    @ApiModelProperty(value = "주문금액", position = 18)
    private long purchaseAmt;

    @ApiModelProperty(value = "행사할인금액", position = 19)
    private long promoDiscountAmt;

    @ApiModelProperty(value = "카드할인금액", position = 20)
    private long cardDiscountAmt;

    @ApiModelProperty(value = "대체주문금액", position = 21)
    private long substitutionAmt;

    @ApiModelProperty(value = "중복쿠폰금액", position = 22)
    private long addCouponDiscountAmt;
}
