package kr.co.homeplus.admin.web.common.model.codeMng;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MngCodeSetDto {
	private String gmcCd;
	private String mcCd;
	private String mcNm;
	private int priority;
	private String useYn;
	private String ref1;
	private String ref2;
	private String ref3;
	private String ref4;
	private String lgcCodeCd;
	private String lgcUpcodeCd;
	private String userId;

}

