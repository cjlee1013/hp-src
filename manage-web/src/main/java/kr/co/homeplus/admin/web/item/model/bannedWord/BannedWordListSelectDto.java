package kr.co.homeplus.admin.web.item.model.bannedWord;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class BannedWordListSelectDto {

    @RealGridColumnInfo(headText = "bannedSeq", hidden = true)
    private long bannedSeq;

    @RealGridColumnInfo(headText = "유형", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 80, order = 1)
    private String bannedTypeTxt;

    @RealGridColumnInfo(headText = "bannedType", order = 2, hidden = true)
    private String bannedType;

    @RealGridColumnInfo(headText = "키워드", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, width = 250, order = 3)
    private String keyword;

    @RealGridColumnInfo(headText = "keywordDesc", order = 4, hidden = true)
    private String keywordDesc;

    @RealGridColumnInfo(headText = "useYn", order = 5, hidden = true)
    private String useYn;

    @RealGridColumnInfo(headText = "사용여부", width = 80, sortable = true, order = 6)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 7)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 120, order = 8)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 9)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 120, order = 10)
    private String chgDt;

    @RealGridColumnInfo(headText = "cateKind", order = 11, hidden = true)
    private String cateKind;
    @RealGridColumnInfo(headText = "cateCd", order = 12, hidden = true)
    private String cateCd;
    @RealGridColumnInfo(headText = "lCateNm", order = 13, hidden = true)
    private String lCateNm;
    @RealGridColumnInfo(headText = "mCateNm", order = 14, hidden = true)
    private String mCateNm;
    @RealGridColumnInfo(headText = "sCateNm", order = 15, hidden = true)
    private String sCateNm;

    private String regId;
    private String chgId;

}
