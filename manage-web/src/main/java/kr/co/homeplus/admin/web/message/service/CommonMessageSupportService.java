package kr.co.homeplus.admin.web.message.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.message.model.SmsResendListRequest;
import kr.co.homeplus.admin.web.message.model.SmsResendListResponse;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class CommonMessageSupportService {

    private final ResourceClient resourceClient;

    public String resend(long smsWorkSeq) {
        SmsResendListRequest smsResendListRequest = new SmsResendListRequest();
        smsResendListRequest.setSmsWorkSeqList(List.of(smsWorkSeq));
        ResponseObject<SmsResendListResponse> responseObject = resourceClient.postForResponseObject(
            ResourceRouteName.MESSAGE, smsResendListRequest, "/resend/sms/list",
            new ParameterizedTypeReference<>() {
            });
        SmsResendListResponse smsResendListResponse = responseObject.getData();
        return smsResendListResponse.getSmsResendWorkSeqList().size() > 0
            ? "재발송 처리 되었습니다."
            : "실패 대상자가 없어 발송처리 되지 않았습니다.";
    }

}
