package kr.co.homeplus.admin.web.item.model.item;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OnlineStockItemSetDto {
    private List<String> itemNoList;

    @Builder.Default
    private String onlineStockYn = "Y";

    private String onlineStockStartDt;

    private String onlineStockEndDt;

    private String setType;

    private String setKind;

    private String setEvenNumber;

    private int stockQty;

    private int stockQtyNaver;

    private int stockQtyEleven;

    private String userId;
}
