package kr.co.homeplus.admin.web.message.model;

import java.util.List;
import lombok.Data;

@Data
public class UploadResult {

    private List<BasicRecipient> list;

    private String message;
}
