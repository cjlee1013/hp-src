package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "카테고리랭킹 > 등록")
public class CategoryRankInsertParamDto {
    @ApiModelProperty(notes = "검색어", position = 1)
    private String keyword;
    @ApiModelProperty(notes = "순위", position = 2)
    private String boostType;
    @ApiModelProperty(notes = "카테고리번호", position = 3)
    private int categoryId;
    @ApiModelProperty(notes = "카테고리명", position = 4)
    private String categoryNm;

    private String administratorId;
}
