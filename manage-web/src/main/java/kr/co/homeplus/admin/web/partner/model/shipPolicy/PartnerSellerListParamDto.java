package kr.co.homeplus.admin.web.partner.model.shipPolicy;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerSellerListParamDto {

    @ApiModelProperty(value = "조회 기간 기준(REG_DT/CHG_DT)")
    private String schPartnerPeriodType;

    @ApiModelProperty(value = "조회시작일")
    private String schStartDate;

    @ApiModelProperty(value = "조회종료일")
    private String schEndDate;

    @ApiModelProperty(value = "구분")
    private String schPartnerType;

    @ApiModelProperty(value = "회원구분")
    private String schPartnerStatus;

    @ApiModelProperty(value = "사업자유형")
    private String schOperatorType;

    @ApiModelProperty(value = "회원등급")
    private String schPartnerGrade;

    @ApiModelProperty(value = "검색어 분류")
    private String schType;

    @ApiModelProperty(value = "검색 키워드")
    private String schKeyword;

    @ApiModelProperty(value = "점포ID")
    private String schStoreId;

    @ApiModelProperty(value = "점포명")
    private String schStoreNm;

}
