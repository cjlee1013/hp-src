package kr.co.homeplus.admin.web.settle.model.affiliateSales;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "전표조회 상세")
public class AffiliateDetailGetDto {
  @ApiModelProperty(notes = "구분")
  @RealGridColumnInfo(headText = "구분", sortable = true, width = 100)
  private String gubun;

  @ApiModelProperty(notes = "배송/구매완료시간")
  @RealGridColumnInfo(headText = "배송/구매완료시간", sortable = true, width = 150)
  private String basicDt;

  @ApiModelProperty(notes = "취소완료시간")
  @RealGridColumnInfo(headText = "취소완료시간", sortable = true, width = 150)
  private String claimDt;

  @ApiModelProperty(notes = "주문시간")
  @RealGridColumnInfo(headText = "주문시간", sortable = true, width = 150)
  private String paymentCompleteDt;

  @ApiModelProperty(notes = "디바이스구분")
  @RealGridColumnInfo(headText = "디바이스구분", sortable = true, width = 100)
  private String platform;

  @ApiModelProperty(notes = "주문번호")
  @RealGridColumnInfo(headText = "주문번호", sortable = true, width = 100)
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "클레임번호")
  @RealGridColumnInfo(headText = "클레임번호", sortable = true, width = 100)
  private String claimNo;

  @ApiModelProperty(notes = "상품번호")
  @RealGridColumnInfo(headText = "상품번호", sortable = true, width = 100)
  private String itemNo;

  @ApiModelProperty(notes = "상품명")
  @RealGridColumnInfo(headText = "상품명", sortable = true, width = 100)
  private String itemName;

  @ApiModelProperty(notes = "결제금액")
  @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String orderPrice;

  @ApiModelProperty(notes = "배송비")
  @RealGridColumnInfo(headText = "배송비", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String shipPrice;

  @ApiModelProperty(notes = "부가세")
  @RealGridColumnInfo(headText = "부가세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String vatPrice;

  @ApiModelProperty(notes = "실적대상금액")
  @RealGridColumnInfo(headText = "실적대상금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String supplyAmt;

  @ApiModelProperty(notes = "수수료율")
  @RealGridColumnInfo(headText = "수수료율", sortable = true, width = 100)
  private String commissionRate;

  @ApiModelProperty(notes = "수수료액")
  @RealGridColumnInfo(headText = "수수료액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String commissionAmt;

  @ApiModelProperty(notes = "제휴연동정보1")
  @RealGridColumnInfo(headText = "제휴연동정보1", sortable = true, width = 150)
  private String affiliateExtraCd;

  @ApiModelProperty(notes = "제휴연동정보2")
  @RealGridColumnInfo(headText = "제휴연동정보2", sortable = true, width = 150)
  private String ocbSaveCardNo;
}
