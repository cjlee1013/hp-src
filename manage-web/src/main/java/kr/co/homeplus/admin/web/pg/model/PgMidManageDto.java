package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단관리 > PG상점ID관리 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class PgMidManageDto {
    @ApiModelProperty(notes = "PG종류(INICIS,TOSSPG)", required = true)
    @RealGridColumnInfo(headText = "PG", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String pgKind;

    @ApiModelProperty(notes = "PG상점ID", required = true)
    @RealGridColumnInfo(headText = "상점ID", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String pgMid;

    @ApiModelProperty(notes = "사이트키(PG사에서 발급한 key)")
    @RealGridColumnInfo(headText = "사이트Key", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String siteKey;

    @ApiModelProperty(notes = "사용목적(NORMAL:일반,GIFT:상품권,GOLD:순금,CULTURE:문화비소득공제,PROMOTION:프로모션)")
    @RealGridColumnInfo(headText = "사용목적", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String usePurpose;

    @ApiModelProperty(notes = "무이자여부(무이자:Y,이자:N)")
    @RealGridColumnInfo(headText = "무이자사용여부", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String freeInterestYn;

    @ApiModelProperty(notes = "노출순서", required = true)
    @RealGridColumnInfo(headText = "노출순서", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Integer dispSeq;

    @ApiModelProperty(notes = "간편결제 적용대상(SAMSUNGPAY:삼성페이,KAKAOPAY:카카오카드,KAKAOMONEY:카카오머니,NAVERPAY:네이버페이,TOSS:토스,PAYCO:페이코)")
    @RealGridColumnInfo(headText = "간편결제 적용대상", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String easyPayTarget;

    @ApiModelProperty(notes = "사용여부(사용:Y,미사용:N)", required = true)
    @RealGridColumnInfo(headText = "사용여부", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String useYn;

    @ApiModelProperty(notes = "비고")
    @RealGridColumnInfo(headText = "parentMethodCd", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String note;

    @ApiModelProperty(notes = "등록자ID", required = true)
    @RealGridColumnInfo(headText = "등록자", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regId;

    @ApiModelProperty(notes = "등록일자", required = true, hidden = true)
    @RealGridColumnInfo(headText = "등록일", sortable = true, fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME)
    private String regDt;

    @ApiModelProperty(notes = "수정자ID", required = true)
    @RealGridColumnInfo(headText = "수정자", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgId;

    @ApiModelProperty(notes = "수정일자", required = true, hidden = true)
    @RealGridColumnInfo(headText = "수정일", sortable = true, fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME)
    private String chgDt;
}
