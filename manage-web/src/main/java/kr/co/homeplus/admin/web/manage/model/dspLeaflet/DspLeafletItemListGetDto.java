package kr.co.homeplus.admin.web.manage.model.dspLeaflet;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class DspLeafletItemListGetDto {

    @RealGridColumnInfo(headText = "상품번호", width = 70, sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 200, order = 1, sortable = true)
    private String itemNm1;

    @RealGridColumnInfo(headText = "상품유형", order = 2, hidden = true)
    private String itemTypeNm;

    @RealGridColumnInfo(headText = "사용여부", order = 3, hidden = true)
    private String useYn;

    @RealGridColumnInfo(headText = "사용여부", width = 50, order = 4, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 5, sortable = true)
    private String chgNm;


    private Long leafletNo;
    private Long cateNo;
    private String itemType;
    private int priority;
    private String regId;
    private String regNm;
    private String regDt;
    private String chgId;
    private String chgDt;

}
