package kr.co.homeplus.admin.web.item.model.sameClass;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SameClassGroupListSelectDto {

    @ApiModelProperty(value = "그룹번호")
    @RealGridColumnInfo(headText = "그룹번호", width = 120, columnType = RealGridColumnType.NUMBER_CENTER, sortable = true)
    private Integer sameClassNo;

    @ApiModelProperty(value = "그룹명")
    @RealGridColumnInfo(headText = "그룹명", width = 120, columnType = RealGridColumnType.BASIC, sortable = true)
    private String sameClassNm;

    @ApiModelProperty(value = "Hyper")
    @RealGridColumnInfo(headText = "Hyper", width = 120, columnType = RealGridColumnType.BASIC, sortable = true)
    private String hyperLimitTxt;

    @ApiModelProperty(value = "Hyper 제한여부")
    @RealGridColumnInfo(headText = "Hyper 제한여부", width = 0, hidden = true)
    private String hyperLimitYn;

    @ApiModelProperty(value = "Hyper 제한 수량")
    @RealGridColumnInfo(headText = "Hyper 제한 수량", width = 0, hidden = true)
    private Integer hyperLimitQty;

    @ApiModelProperty(value = "Club")
    @RealGridColumnInfo(headText = "Club", width = 120, columnType = RealGridColumnType.BASIC, sortable = true)
    private String clubLimitTxt;

    @ApiModelProperty(value = "Club 제한여부")
    @RealGridColumnInfo(headText = "Club 제한여부", width = 0, hidden = true)
    private String clubLimitYn;

    @ApiModelProperty(value = "Club 제한 수량")
    @RealGridColumnInfo(headText = "Club 제한수량", width = 0, hidden = true)
    private Integer clubLimitQty;

    @ApiModelProperty(value = "Express")
    @RealGridColumnInfo(headText = "Express", width = 120, columnType = RealGridColumnType.BASIC, sortable = true)
    private String expLimitTxt;

    @ApiModelProperty(value = "Express 제한여부")
    @RealGridColumnInfo(headText = "Express 제한여부", width = 0, hidden = true)
    private String expLimitYn;

    @ApiModelProperty(value = "Express 제한수량")
    @RealGridColumnInfo(headText = "Express 제한수량", width = 0, hidden = true)
    private Integer expLimitQty;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", width = 0, hidden = true)
    private String useYn;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", width = 120, columnType = RealGridColumnType.BASIC, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "수정자")
    private String chgNm;

    @ApiModelProperty(value = "수정일")
    @RealGridColumnInfo(headText = "수정일",columnType= RealGridColumnType.DATETIME,  width = 150)
    private String chgDt;

}
