package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.partnerSettle.PartnerSettleListGetDto;
import kr.co.homeplus.admin.web.settle.model.partnerSettle.PartnerSettleListSetDto;
import kr.co.homeplus.admin.web.settle.model.partnerSettle.PartnerSettleStateSetDto;
import kr.co.homeplus.admin.web.settle.model.partnerSettleDetail.PartnerSettleDetailGetDto;
import kr.co.homeplus.admin.web.settle.model.partnerSettleDetail.PartnerSettleItemGetDto;
import kr.co.homeplus.admin.web.settle.model.partnerSettleDetail.PartnerSettleItemSetDto;
import kr.co.homeplus.admin.web.settle.model.partnerSettleDetail.PartnerSettlePreDtSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PartnerSettleService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    /**
     * 정산관리 > 정산관리 > 메인 리스트 조회
     */
    public List<PartnerSettleListGetDto> getPartnerSettleList(PartnerSettleListSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/partnerSettle/getPartnerSettleList",
            new ParameterizedTypeReference<ResponseObject<List<PartnerSettleListGetDto>>>(){}).getData();
    }

    /**
     * 정산관리 > 정산관리 > 졍산완료
     */
    public ResponseObject<String> setSettleComplete(PartnerSettleStateSetDto listParamDto) throws Exception {
        listParamDto.setChgId(loginCookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/partnerSettle/setSettleComplete",
            new ParameterizedTypeReference<ResponseObject<String>>() {}
        );
    }

    /**
     * 정산관리 > 정산관리 > 졍산보류
     */
    public ResponseObject<String> setSettlePostpone(PartnerSettleStateSetDto listParamDto) throws Exception {
        listParamDto.setChgId(loginCookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/partnerSettle/setSettlePostpone",
            new ParameterizedTypeReference<ResponseObject<String>>() {}
        );
    }

    /**
     * 정산관리 > 정산관리 > 졍산보류해제
     */
    public ResponseObject<String> setPostponeCancel(PartnerSettleStateSetDto listParamDto) throws Exception {
        listParamDto.setChgId(loginCookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/partnerSettle/setPostponeCancel",
            new ParameterizedTypeReference<ResponseObject<String>>() {}
        );
    }

    /**
     * 정산관리 > 정산관리 > 지급예정일 변경
     */
    public ResponseObject<String> setSettlePreDt(PartnerSettlePreDtSetDto listParamDto) throws Exception {
        listParamDto.setChgId(loginCookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/partnerSettle/setSettlePreDt",
            new ParameterizedTypeReference<ResponseObject<String>>() {}
        );
    }

    /**
     * 정산관리 > 판매자 정산정보
     */
    public List<PartnerSettleDetailGetDto> getPartnerSettleDetail(PartnerSettleListSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/partnerSettle/getPartnerSettleDetail",
            new ParameterizedTypeReference<ResponseObject<List<PartnerSettleDetailGetDto>>>(){}).getData();
    }

    /**
     * 정산관리 > 정산관리 > 상품별 조회
     */
    public List<PartnerSettleItemGetDto> getPartnerSettleItem(PartnerSettleItemSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/partnerSettle/getPartnerSettleItem",
            new ParameterizedTypeReference<ResponseObject<List<PartnerSettleItemGetDto>>>(){}).getData();
    }
}
