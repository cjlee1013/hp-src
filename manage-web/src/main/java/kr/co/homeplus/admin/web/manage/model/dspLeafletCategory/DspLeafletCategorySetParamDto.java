package kr.co.homeplus.admin.web.manage.model.dspLeafletCategory;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspLeafletCategorySetParamDto {

    private Long cateNo;
    private String cateNm;
    private int priority;
    private String pcImg;
    private int pcImgWidth;
    private int pcImgHeight;
    private String mobileImg;
    private int mobileImgWidth;
    private int mobileImgHeight;
    private String useYn;
    private String regId;

}
