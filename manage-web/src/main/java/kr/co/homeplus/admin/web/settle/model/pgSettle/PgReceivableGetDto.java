package kr.co.homeplus.admin.web.settle.model.pgSettle;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
public class PgReceivableGetDto {
    @ApiModelProperty(notes = "집계일")
    @RealGridColumnInfo(headText = "집계일", width = 100)
    private String basicDt;

    @ApiModelProperty(notes = "거래일")
    @RealGridColumnInfo(headText = "거래일", width = 100)
    private String paymentDt;

    @ApiModelProperty(notes = "PG사")
    @RealGridColumnInfo(headText = "PG사", width = 100)
    private String pgKind;

    @ApiModelProperty(notes = "상품구분")
    @RealGridColumnInfo(headText = "상품구분", width = 100)
    private String mallType;

    @ApiModelProperty(notes = "집계코드")
    @RealGridColumnInfo(headText = "집계코드", width = 100)
    private String code;

    @ApiModelProperty(notes = "설명")
    @RealGridColumnInfo(headText = "설명", width = 100)
    private String description;

    @ApiModelProperty(notes = "지급SRL")
    @RealGridColumnInfo(headText = "지급SRL", width = 100)
    private String settleSrl;

    @ApiModelProperty(notes = "거래구분")
    @RealGridColumnInfo(headText = "거래구분", width = 100)
    private String gubun;

    @ApiModelProperty(notes = "거래금액")
    @RealGridColumnInfo(headText = "거래금액", sortable = true, width = 150, groupName="거래내역", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long transAmt;

    @ApiModelProperty(notes = "거래잔액")
    @RealGridColumnInfo(headText = "거래잔액", sortable = true, width = 150, groupName="거래내역", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long transBalance;

    @ApiModelProperty(notes = "예치금액")
    @RealGridColumnInfo(headText = "예치금액", sortable = true, width = 150, groupName="지급대행", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long depositAmt;

    @ApiModelProperty(notes = "예치잔액")
    @RealGridColumnInfo(headText = "예치잔액", sortable = true, width = 150, groupName="지급대행", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long depositBalance;

    @ApiModelProperty(notes = "PG미수금")
    @RealGridColumnInfo(headText = "PG미수금", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long receivableAmt;

    @ApiModelProperty(notes = "처리일시")
    @RealGridColumnInfo(headText = "처리일시", width = 100)
    private String regDt;
}
