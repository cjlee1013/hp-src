package kr.co.homeplus.admin.web.delivery.model.shipMonitoring;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.Data;

@Data
@ApiModel(description = "회수지연조회 응답")
public class ShipMonitoringPickGetDto {
    @ApiModelProperty(notes = "그룹클레임번호")
    @RealGridColumnInfo(headText = "그룹클레임번호", width = 120, sortable = true)
    private String claimNo;

    @ApiModelProperty(notes = "클레임번호")
    @RealGridColumnInfo(headText = "클레임번호", width = 120, sortable = true)
    private String claimBundleNo;

    @ApiModelProperty(notes = "클레임구분")
    @RealGridColumnInfo(headText = "클레임구분", width = 150, sortable = true)
    private String claimTypeNm;

    @ApiModelProperty(notes = "클레임구분코드")
    @RealGridColumnInfo(headText = "클레임구분코드", width = 150, sortable = true, hidden = true)
    private String claimType;

    @ApiModelProperty(notes = "처리상태")
    @RealGridColumnInfo(headText = "처리상태", width = 150, sortable = true)
    private String claimStatusNm;

    @ApiModelProperty(notes = "처리상태코드")
    @RealGridColumnInfo(headText = "처리상태코드", width = 150, sortable = true, hidden = true)
    private String claimStatus;

    @ApiModelProperty(notes = "마켓연동")
    @RealGridColumnInfo(headText = "마켓연동", width = 80, sortable = true)
    private String marketType;

    @ApiModelProperty(notes = "마켓주문번호")
    @RealGridColumnInfo(headText = "마켓주문번호", width = 120, sortable = true)
    private String marketOrderNo;

    @ApiModelProperty(notes = "클레임신청일시")
    @RealGridColumnInfo(headText = "클레임신청일시", width = 150, sortable = true)
    private String requestDt;

    @ApiModelProperty(notes = "수거상태")
    @RealGridColumnInfo(headText = "수거상태", width = 150, sortable = true)
    private String pickStatusNm;

    @ApiModelProperty(notes = "수거상태코드")
    @RealGridColumnInfo(headText = "수거상태코드", width = 150, sortable = true, hidden = true)
    private String pickStatus;

    @ApiModelProperty(notes = "수거예정일")
    @RealGridColumnInfo(headText = "수거예정일", width = 120, sortable = true)
    private String shipDt;

    @ApiModelProperty(notes = "수거지연일")
    @RealGridColumnInfo(headText = "수거지연일", width = 120, sortable = true)
    private String pickDelayDay;

    @ApiModelProperty(notes = "수거방법")
    @RealGridColumnInfo(headText = "수거방법", width = 150, sortable = true)
    private String pickShipType;

    @ApiModelProperty(notes = "택배사")
    @RealGridColumnInfo(headText = "택배사", width = 150, sortable = true)
    private String pickDlvNm;

    @ApiModelProperty(notes = "택배사코드")
    @RealGridColumnInfo(headText = "택배사코드", width = 150, sortable = true, hidden = true)
    private String pickDlvCd;

    @ApiModelProperty(notes = "송장번호")
    @RealGridColumnInfo(headText = "송장번호", width = 150, sortable = true)
    private String pickInvoiceNo;

    @ApiModelProperty(notes = "주문번호")
    @RealGridColumnInfo(headText = "주문번호", width = 150, sortable = true)
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "판매업체")
    @RealGridColumnInfo(headText = "판매업체", width = 150, sortable = true)
    private String partnerNm;

    @ApiModelProperty(notes = "주문일시")
    @RealGridColumnInfo(headText = "주문일시", width = 150, sortable = true)
    private String orderDt;

    @ApiModelProperty(notes = "회원번호")
    @RealGridColumnInfo(headText = "회원번호", width = 120, sortable = true)
    private String userNoMask;

    @ApiModelProperty(notes = "회원번호")
    @RealGridColumnInfo(headText = "회원번호", width = 120, sortable = true, hidden = true)
    private String userNo;

    @ApiModelProperty(notes = "구매자")
    @RealGridColumnInfo(headText = "구매자", width = 100, sortable = true)
    private String buyerNm;

    @ApiModelProperty(notes = "수령인")
    @RealGridColumnInfo(headText = "수령인", width = 100, sortable = true)
    private String pickReceiverNm;

    @ApiModelProperty(notes = "수령인연락처")
    @RealGridColumnInfo(headText = "수령인연락처", width = 120, sortable = true)
    private String pickMobileNo;

    @ApiModelProperty(notes = "우편번호")
    @RealGridColumnInfo(headText = "우편번호", width = 100, sortable = true)
    private String pickZipcode;

    @ApiModelProperty(notes = "주소")
    @RealGridColumnInfo(headText = "주소", width = 350, sortable = true, columnType = RealGridColumnType.NONE)
    private String pickAddr;

    @ApiModelProperty(notes = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", width = 150, sortable = true, hidden = true)
    private String storeType;

    @ApiModelProperty(notes = "사이트유형")
    @RealGridColumnInfo(headText = "사이트유형", width = 150, sortable = true, hidden = true)
    private String siteType;
}
