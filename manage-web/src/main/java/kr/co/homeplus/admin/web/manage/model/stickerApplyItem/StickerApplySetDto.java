package kr.co.homeplus.admin.web.manage.model.stickerApplyItem;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StickerApplySetDto {

    private Long stickerApplyNo;
    private String stickerApplyNm;
    private Long stickerNo;
    private String hyperDispYn;
    private String clubDispYn;
    private String expDispYn;
    private String dispStartDt;
    private String dispEndDt;
    private String useYn;
    private String userId;

    //등록상품리스트
    List<StickerApplyItemGetDto> itemList;
}
