package kr.co.homeplus.admin.web.core.dto;

import lombok.Data;

/**
 * 사용자 롤권한 체크를 위한 Dto
 */
@Data
public class AmsRoleDto {
    private String scope;
    private String userCd;
    private Integer roleSeq;
    private String roleName;
    private boolean hasRole;

}
