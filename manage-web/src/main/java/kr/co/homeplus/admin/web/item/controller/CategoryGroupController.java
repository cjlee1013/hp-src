package kr.co.homeplus.admin.web.item.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumn;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.admin.web.item.model.categoryGroup.CategoryGroupDetailGetDto;
import kr.co.homeplus.admin.web.item.model.categoryGroup.CategoryGroupGetDto;
import kr.co.homeplus.admin.web.item.model.categoryGroup.CategoryGroupLcateGetDto;
import kr.co.homeplus.admin.web.item.model.categoryGroup.CategoryGroupLcateParamDto;
import kr.co.homeplus.admin.web.item.model.categoryGroup.CategoryGroupParamDto;
import kr.co.homeplus.admin.web.item.model.categoryGroup.CategoryGroupSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/category")
public class CategoryGroupController {

    final private ResourceClient resourceClient;

    final private LoginCookieService loginCookieService;

    final private CodeService codeService;


    @Value("${plus.resource-routes.image.url}")
    private String homeImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    /**
     * 대대분류관리 리스트 Grid 설정 Start
     **/
    private static final List<RealGridColumn> CATEGORY_GROUP_LIST_GRID_COLUMN = new ArrayList<>();
    private static final RealGridOption CATEGORY_GROUP_LIST_GRID_OPTION = new RealGridOption("fill", false, false, false);

    /**
     * 대대분류관리 대 카테고리 리스트 Grid 설정 Start
     **/
    private static final List<RealGridColumn> CATEGORY_GROUP_LCATE_LIST_GRID_COLUMN = new ArrayList<>();
    private static final RealGridOption CATEGORY_GROUP_LCATE_LIST_GRID_OPTION = new RealGridOption("fill", false, false, false);

    public CategoryGroupController(ResourceClient resourceClient,
        LoginCookieService loginCookieService,
        CodeService codeService) {
        this.resourceClient = resourceClient;
        this.loginCookieService = loginCookieService;
        this.codeService = codeService;

        CATEGORY_GROUP_LIST_GRID_COLUMN.add(new RealGridColumn("gcateCd", "gcateCd", "대대분류ID", RealGridColumnType.NUMBER_CENTER, 80, true, true));
        CATEGORY_GROUP_LIST_GRID_COLUMN.add(new RealGridColumn("gcateNm", "gcateNm", "대대분류명", RealGridColumnType.BASIC, 120, true, true));
        CATEGORY_GROUP_LIST_GRID_COLUMN.add(new RealGridColumn("priority", "priority", "우선순위", RealGridColumnType.NUMBER_CENTER, 80, true, true));
        CATEGORY_GROUP_LIST_GRID_COLUMN.add(new RealGridColumn("largeCount", "largeCount", "소속대분류수", RealGridColumnType.NUMBER_CENTER, 100, true, true));
        CATEGORY_GROUP_LIST_GRID_COLUMN.add(new RealGridColumn("useYn", "useYn", "사용여부코드", RealGridColumnType.BASIC, 0, false, true));
        CATEGORY_GROUP_LIST_GRID_COLUMN.add(new RealGridColumn("useYnTxt", "useYnTxt", "사용여부", RealGridColumnType.BASIC, 80, true, true));
        CATEGORY_GROUP_LIST_GRID_COLUMN.add(new RealGridColumn("regNm", "regNm", "등록자", RealGridColumnType.BASIC, 80, true, true));
        CATEGORY_GROUP_LIST_GRID_COLUMN.add(new RealGridColumn("regDt", "regDt", "등록일", RealGridColumnType.DATETIME, 100, true, true));
        CATEGORY_GROUP_LIST_GRID_COLUMN.add(new RealGridColumn("chgNm", "chgNm", "수정자", RealGridColumnType.BASIC, 80, true, true));
        CATEGORY_GROUP_LIST_GRID_COLUMN.add(new RealGridColumn("chgDt", "chgDt", "수정일", RealGridColumnType.DATETIME, 100, true, true));


        CATEGORY_GROUP_LCATE_LIST_GRID_COLUMN.add(new RealGridColumn("lcateNm", "lcateNm", "대분류명", RealGridColumnType.BASIC, 100, true, true));
        CATEGORY_GROUP_LCATE_LIST_GRID_COLUMN.add(new RealGridColumn("lcateCd", "lcateCd", "대분류ID", RealGridColumnType.BASIC, 100, true, true));
        CATEGORY_GROUP_LCATE_LIST_GRID_COLUMN.add(new RealGridColumn("useYnTxt", "useYnTxt", "사용여부", RealGridColumnType.BASIC, 100, true, true));
        CATEGORY_GROUP_LCATE_LIST_GRID_COLUMN.add(new RealGridColumn("useYn", "useYn", "사용여부코드", RealGridColumnType.BASIC, 0, false, true));
        CATEGORY_GROUP_LCATE_LIST_GRID_COLUMN.add(new RealGridColumn("dispYnTxt", "dispYnTxt", "노출범위", RealGridColumnType.BASIC, 100, true, true));
        CATEGORY_GROUP_LCATE_LIST_GRID_COLUMN.add(new RealGridColumn("dispYn", "dispYn", "노출범위코드", RealGridColumnType.BASIC, 0, false, true));
        CATEGORY_GROUP_LCATE_LIST_GRID_COLUMN.add(new RealGridColumn("regNm", "regNm", "등록자", RealGridColumnType.BASIC, 100, true, true));
        CATEGORY_GROUP_LCATE_LIST_GRID_COLUMN.add(new RealGridColumn("regDt", "regDt", "등록일", RealGridColumnType.DATETIME, 100, true, true));
        CATEGORY_GROUP_LCATE_LIST_GRID_COLUMN.add(new RealGridColumn("chgNm", "chgNm", "수정자", RealGridColumnType.BASIC, 100, true, true));
        CATEGORY_GROUP_LCATE_LIST_GRID_COLUMN.add(new RealGridColumn("chgDt", "chgDt", "수정일", RealGridColumnType.DATETIME, 100, true, true));

    }

    /**
     * 카테고리관리 > 카테고리관리 > 대대분류관리
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = {"/groupMain"}, method = RequestMethod.GET)
    public String categoryCommissionMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "cate_group_sch_type"        // 카테고리그룹 검색 타입
            ,   "use_yn"    //사용여부
        );


        String apiUri = "/item/category/getLargeCategoryInfoList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CategoryGroupLcateGetDto>>>() {};
        List<CategoryGroupLcateGetDto> lcategoryList =  (List<CategoryGroupLcateGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM,apiUri, typeReference).getData();

        model.addAttribute("lcategoryList", lcategoryList);
        model.addAttribute("schType", code.get("cate_group_sch_type"));
        model.addAttribute("schUseYn", code.get("use_yn"));
        model.addAttribute("homeImgUrl", this.homeImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);
        model.addAttribute("categoryGroupListGridBaseInfo", new RealGridBaseInfo("categoryGroupListGridBaseInfo", CATEGORY_GROUP_LIST_GRID_COLUMN, CATEGORY_GROUP_LIST_GRID_OPTION).toString());
        model.addAttribute("categoryGroupLcateListGridBaseInfo", new RealGridBaseInfo("categoryGroupLcateListGridBaseInfo", CATEGORY_GROUP_LCATE_LIST_GRID_COLUMN, CATEGORY_GROUP_LCATE_LIST_GRID_OPTION).toString());
        return "/item/categoryGroupMain";
    }
    /**
     * 카테고리관리 > 카테고리관리 > 대대분류관리 > 대분류 리스트
     *
     * @param listParamDto
     * @return List<CategoryGroupGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/getGroupList.json"}, method = RequestMethod.GET)
    public List<CategoryGroupGetDto> getGroupList(CategoryGroupParamDto listParamDto) {

        String apiUri = "/item/category/getGroupList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CategoryGroupGetDto>>>() {};

        return  (List<CategoryGroupGetDto>) resourceClient.getForResponseObject(
            ResourceRouteName.ITEM, StringUtil
                .getRequestString(apiUri, CategoryGroupParamDto.class, listParamDto), typeReference).getData();
    }
    /**
     * 카테고리관리 > 카테고리관리 > 대대분류관리 > 대분류 상세
     *
     * @param gcateCd
     * @return CategoryGroupDetailGetDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getGroupDetail.json"}, method = RequestMethod.GET)
    public CategoryGroupDetailGetDto getGroupDetail(String gcateCd) {

        String apiUri = "/item/category/getGroupDetail?gcateCd=";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<CategoryGroupDetailGetDto>>() {};
        return (CategoryGroupDetailGetDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM,apiUri+gcateCd, typeReference).getData();
    }
    /**
     * 카테고리관리 > 카테고리관리 > 대대분류관리 > 대분류 등록
     * @param categoryGroupSetParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setCategoryGroup.json"}, method = RequestMethod.POST)
    public ResponseResult setCategoryGroup(@RequestBody CategoryGroupSetParamDto categoryGroupSetParamDto) throws Exception {

        String apiUri = "/item/category/setCategoryGroup";
        categoryGroupSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, categoryGroupSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();

    }
    /**
     * 카테고리관리 > 카테고리관리 > 대대분류관리 > 대카테고리 등록 여부 조회
     * @param categoryGroupLcateParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/checkLargeCategory.json"}, method = RequestMethod.POST)
    public List<CategoryGroupLcateGetDto> checkLargeCategory(@RequestBody CategoryGroupLcateParamDto categoryGroupLcateParamDto) throws Exception {

        String apiUri = "/item/category/checkLargeCategory";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CategoryGroupLcateGetDto>>>() {};
        return (List<CategoryGroupLcateGetDto>) resourceClient.postForResponseObject(
            ResourceRouteName.ITEM, categoryGroupLcateParamDto,apiUri, typeReference).getData();
    }

}
