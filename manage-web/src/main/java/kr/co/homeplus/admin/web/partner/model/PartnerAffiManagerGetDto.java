package kr.co.homeplus.admin.web.partner.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//판매자관리 > 영업 담당자정보 Get Entry
public class PartnerAffiManagerGetDto {

    //계정 상태 (잠금, 정상)
    private String loginStatusNm;

    //계정 상태 사유
    private String loginRegReason;

    //담당자 이름
    private String mngNm;

    //담당자 휴대폰번호
    private String mngMobile;

    //담당자 이메일
    private String mngEmail;


}
