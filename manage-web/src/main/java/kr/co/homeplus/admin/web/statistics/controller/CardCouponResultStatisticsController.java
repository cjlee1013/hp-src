package kr.co.homeplus.admin.web.statistics.controller;

import java.util.List;
import kr.co.homeplus.admin.web.statistics.model.CardCouponResultStatisticsDetailGetDto;
import kr.co.homeplus.admin.web.statistics.model.CardCouponResultStatisticsGetDto;
import kr.co.homeplus.admin.web.statistics.model.CardCouponResultStatisticsSetDto;
import kr.co.homeplus.admin.web.statistics.service.CardCouponResultStatisticService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/escrow/statistics")
public class CardCouponResultStatisticsController {

    @Autowired
    private CardCouponResultStatisticService cardCouponResultStatisticService;

    /**
     * 카드사별쿠폰실적조회 Main Page
     */
    @RequestMapping(value = "/cardCouponResultMain", method = RequestMethod.GET)
    public String cardCouponResultMain(Model model) throws Exception{
        model.addAllAttributes(RealGridHelper.create("cardCouponResultGridBaseInfo", CardCouponResultStatisticsGetDto.class));
        model.addAllAttributes(RealGridHelper.create("cardCouponResultDetailGridBaseInfo", CardCouponResultStatisticsDetailGetDto.class));

        return "/statistics/cardCouponResultMain";
    }

    /**
     * 카드사별쿠폰실적조회 메인 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getCardCouponResultStatisticsList.json", method = RequestMethod.GET)
    public List<CardCouponResultStatisticsGetDto> getCardCouponResultStatisticsList(
        CardCouponResultStatisticsSetDto cardCouponResultStatisticsSetDto) {
        return cardCouponResultStatisticService.getCardCouponResultStatisticsList(cardCouponResultStatisticsSetDto);
    }

    /**
     * 카드사별쿠폰실적조회 상세 리스트 조회
     */
    @ResponseBody
    @PostMapping(value = "/getCardCouponResultStatisticsDetailList.json")
    public List<CardCouponResultStatisticsDetailGetDto> getCardCouponResultStatisticsDetailList(@RequestBody CardCouponResultStatisticsSetDto cardCouponResultStatisticsSetDto){
        return cardCouponResultStatisticService.getCardCouponResultStatisticsDetailList(cardCouponResultStatisticsSetDto);
    }

    /**
     * 카드사별쿠폰실적조회 도움말 팝업
     */
    @RequestMapping(value = "/popup/cardCouponResultStatisticsPop", method = RequestMethod.GET)
    public String cardCouponResultStatisticsPop(Model model) throws Exception {
        return "/statistics/pop/cardCouponResultStatisticsPop";
    }
}