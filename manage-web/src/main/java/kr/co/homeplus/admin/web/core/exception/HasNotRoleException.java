package kr.co.homeplus.admin.web.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * role 권한이 없을 때 반환하는 예외.
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class HasNotRoleException extends RuntimeException {
    private int roleSeq = 0;
    private String message;
    private String code;

    public HasNotRoleException(int roleSeq, String message){
        this.roleSeq = roleSeq;
        this.message = message;
    }

    public HasNotRoleException(int roleSeq, String message, String code){
        this.roleSeq = roleSeq;
        this.message = message;
        this.code = code;
    }

    public HasNotRoleException(int roleSeq){
        this.roleSeq = roleSeq;
    }

    public HasNotRoleException(String message){
        this.message = message;
    }
}
