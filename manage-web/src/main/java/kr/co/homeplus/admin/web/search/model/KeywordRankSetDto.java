package kr.co.homeplus.admin.web.search.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeywordRankSetDto {
    @NotNull
    @NotEmpty
    private String itemNo;
    @NotNull
    @NotEmpty
    private String keywordList;
    @NotNull
    @NotEmpty
    @Pattern(regexp = "(19[0-9]{2}|[0-9]{4})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)", message = "format is yyyy-MM-dd")
    private String startDt;
    @NotNull
    @NotEmpty
    @Pattern(regexp = "(19[0-9]{2}|[0-9]{4})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)", message = "format is yyyy-MM-dd")
    private String endDt;
}
