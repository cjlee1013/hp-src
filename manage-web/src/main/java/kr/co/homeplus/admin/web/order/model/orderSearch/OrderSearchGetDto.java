package kr.co.homeplus.admin.web.order.model.orderSearch;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.admin.web.pg.model.ParentMethodCd;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "주문관리 조회 응답 DTO")
@RealGridOptionInfo
public class OrderSearchGetDto {

    @ApiModelProperty(value = "결제상태", position = 1)
    @RealGridColumnInfo(headText = "결제상태", width = 80, sortable = true)
    private String paymentStatus;

    @ApiModelProperty(value = "주문일시", position = 2)
    @RealGridColumnInfo(headText = "주문일시", width = 130, sortable = true)
    private String orderDt;

    @ApiModelProperty(value = "결제일시", position = 3)
    @RealGridColumnInfo(headText = "결제일시", width = 130, sortable = true)
    private String paymentFshDt;

    @ApiModelProperty(value = "주문번호", position = 4)
    @RealGridColumnInfo(headText = "주문번호", width = 80, sortable = true)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "배송번호", position = 5)
    @RealGridColumnInfo(headText = "배송번호", width = 80, sortable = true)
    private String bundleNo;

    @ApiModelProperty(value = "점포유형", position = 6)
    @RealGridColumnInfo(headText = "점포유형", width = 70, sortable = true)
    private String storeType;

    @ApiModelProperty(value = "합배송 주문번호", position = 7)
    @RealGridColumnInfo(headText = "합배송 주문번호", width = 90, sortable = true)
    private String orgPurchaseOrderNo;

    @ApiModelProperty(value = "대체주문여부", position = 7)
    @RealGridColumnInfo(headText = "대체주문여부", width = 90, sortable = true)
    private String substitutionOrderYn;

    @ApiModelProperty(value = "상품 주문번호", position = 8)
    @RealGridColumnInfo(headText = "상품 주문번호", width = 80, sortable = true)
    private String orderItemNo;

    @ApiModelProperty(value = "상품번호", position = 9)
    @RealGridColumnInfo(headText = "상품번호", width = 110, sortable = true)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 10)
    @RealGridColumnInfo(headText = "상품명", width = 300, columnType = RealGridColumnType.NONE, sortable = true)
    private String itemNm1;

    @ApiModelProperty(value = "총상품금액", position = 11)
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "총상품금액", columnType = RealGridColumnType.PRICE, width = 80, sortable = true)
    private String orderPrice;

    @ApiModelProperty(value = "배송비", position = 12)
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "배송비", columnType = RealGridColumnType.PRICE, width = 80, sortable = true)
    private long shipPrice;

    @ApiModelProperty(value = "도서산간 배송비", position = 13)
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "도서산간배송비", columnType = RealGridColumnType.PRICE, width = 110, sortable = true)
    private String islandShipPrice;

    @ApiModelProperty(value = "클레임배송비", position = 14)
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "클레임배송비", columnType = RealGridColumnType.PRICE, width = 80, sortable = true)
    private String claimPrice;

    @ApiModelProperty(value = "할인금액", position = 15)
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "할인금액", columnType = RealGridColumnType.PRICE, width = 90, sortable = true)
    private String discountAmt;

    @ApiModelProperty(value = "결제금액", position = 16)
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "결제금액", columnType = RealGridColumnType.PRICE, width = 80, sortable = true)
    private String totAmt;

    @ApiModelProperty(value = "판매업체", position = 17)
    @RealGridColumnInfo(headText = "판매업체", width = 90, sortable = true)
    private String partnerNm;

    @ApiModelProperty(value = "마켓연동", position = 18)
    @RealGridColumnInfo(headText = "마켓연동", width = 60, sortable = true)
    private String marketType;

    @ApiModelProperty(value = "마켓주문번호", position = 19)
    @RealGridColumnInfo(headText = "마켓주문번호", width = 90, sortable = true)
    private String marketOrderNo;

    @ApiModelProperty(value = "영수증번호", position = 30)
    @RealGridColumnInfo(headText = "영수증번호", width = 80)
    private String tradeNo;

    @ApiModelProperty(value = "구매자", position = 20)
    @RealGridColumnInfo(headText = "구매자", width = 50, sortable = true)
    private String buyerNm;

    @ApiModelProperty(value = "구매자연락처", position = 21)
    @RealGridColumnInfo(headText = "구매자연락처", sortable = true)
    private String buyerMobileNo;

    @ApiModelProperty(value = "수령인", position = 22)
    @RealGridColumnInfo(headText = "수령인", width = 50, sortable = true)
    private String receiverNm;

    @ApiModelProperty(value = "수령인연락처", position = 23)
    @RealGridColumnInfo(headText = "수령인연락처", sortable = true)
    private String receiverMobileNo;

    @ApiModelProperty(value = "PG사", position = 24)
    @RealGridColumnInfo(headText = "PG사", width = 70, sortable = true)
    private String pgKind;

    @ApiModelProperty(value = "주결제수단", position = 25)
    @RealGridColumnInfo(headText = "주결제수단", columnType = RealGridColumnType.LOOKUP, width = 80, sortable = true)
    private ParentMethodCd parentMethodCd;

    @ApiModelProperty(value = "사이트타입", position = 26, hidden = true)
    @RealGridColumnInfo(hidden = true, headText = "사이트타입", sortable = true)
    private String siteType;

    @ApiModelProperty(value = "결제번호", position = 27, hidden = true)
    @RealGridColumnInfo(hidden = true, headText = "결제번호", sortable = true)
    private String paymentNo;

    private String setBuyerNm(String buyerNm) {
        return this.buyerNm = buyerNm;
    }

}
