package kr.co.homeplus.admin.web.mileage.model.payment;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MileagePaymentSearchSetDto {
    //조회구분(실행예정일/실행일)
    private String schType;

    //조회시작일
    private String schStartDt;

    //조회종료일
    private String schEndDt;

    //구분
    private String requestType;

    //마일리지종류
    private String mileageKind;

    //사용유무
    private String useYn;

    //상세검색
    private String schDetailCode;

    //상세검색내용 - 마일리지 요청번호
    private String schDetailDesc;
}
