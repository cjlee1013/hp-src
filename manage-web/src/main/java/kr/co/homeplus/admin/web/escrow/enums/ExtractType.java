package kr.co.homeplus.admin.web.escrow.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ExtractType {
    SEARCH, EXCEL_DOWN;
}
