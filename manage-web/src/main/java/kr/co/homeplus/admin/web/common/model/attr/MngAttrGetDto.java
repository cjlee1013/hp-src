package kr.co.homeplus.admin.web.common.model.attr;

import lombok.Data;


@Data
//속성정보 Get Entry
public class MngAttrGetDto {

	//그룹속성명
	private String gattrNm;

	//속성명
	private String attrNm;

}