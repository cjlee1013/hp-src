package kr.co.homeplus.admin.web.core.exception;

/**
 * 인가 된 메뉴인지을 검증하는 인터셉터에서 발생하는 Exception 입니다.
 */
public class ValidateAuthorizedMenuException extends RuntimeException {

    public ValidateAuthorizedMenuException(final Exception e) {
        super(e);
    }

    public ValidateAuthorizedMenuException(final String message) {
        super(message);
    }
}
