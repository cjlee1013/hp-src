package kr.co.homeplus.admin.web.search.controller;


import kr.co.homeplus.admin.web.search.model.OperationSearchLogBlackListGetParam;
import kr.co.homeplus.admin.web.search.model.OperationSearchLogBlackListGridDto;
import kr.co.homeplus.admin.web.search.model.OperationSearchLogBlackListRuleInfoSetParam;
import kr.co.homeplus.admin.web.search.model.OperationSearchLogBlackListSetParam;
import kr.co.homeplus.admin.web.search.service.OperationFilteringService;
import kr.co.homeplus.admin.web.search.service.OperationSearchLogService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/search/operation/filtering")
@RequiredArgsConstructor
public class OperationFilteringController {

    private final OperationFilteringService operationFilteringService;

    @GetMapping(value = { "/setting" })
    public String getMain(Model model) {
        model.addAllAttributes(RealGridHelper.create("blackListRuleInfoGridBaseInfo", OperationSearchLogBlackListGridDto.ruleInfo.class));
        return "/search/operationFilteringSetting";
    }

    @ResponseBody
    @PostMapping(value = {"/getBlackListRuleInfo.json"})
    public List<OperationSearchLogBlackListGridDto> getBlackListRuleInfo(@RequestBody @Valid OperationSearchLogBlackListRuleInfoSetParam param) {
        return (List<OperationSearchLogBlackListGridDto>)operationFilteringService.getSearchLogBlackListRuleInfo(param).getData();
    }

    @ResponseBody
    @PostMapping(value = {"/modifyBlackListRuleInfo.json"})
    public Integer modifyBlackListRuleInfo(@RequestBody @Valid OperationSearchLogBlackListRuleInfoSetParam param) {
        return (Integer)operationFilteringService.modifyBlackListRuleInfo(param).getData();
    }
}
