package kr.co.homeplus.admin.web.pg.service;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodDto;
import kr.co.homeplus.admin.web.pg.model.PgCommissionManageDto;
import kr.co.homeplus.admin.web.pg.model.PgCommissionManageSelectDto;
import kr.co.homeplus.admin.web.pg.model.PgCommissionTargetDto;
import kr.co.homeplus.admin.web.pg.model.PgCommissionTargetSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PgCommissionManageService {
    private final ResourceClient resourceClient;

    /**
     * 중복제거 결제수단 조회
     * 상위결제수단코드 기준 중복된 결제수단을 제거한 후 반환
     * @return
     * @throws Exception
     */
    public List<PaymentMethodDto> getDistinctMethodCd(String parentMethodCd) throws Exception {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("parentMethodCd", parentMethodCd));
        String apiUri = EscrowConstants.ESCROW_GET_DISTINCT_METHOD_CD + StringUtil.getParameter(setParameterList);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PaymentMethodDto>>>() {};
        return (List<PaymentMethodDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 모든 중복제거 결제수단 조회
     * 상위결제수단코드 관계 없이 모든 결제수단코드를 중복제거 후 반환
     * @return
     * @throws Exception
     */
    public List<PaymentMethodDto> getAllDistinctMethodCd() throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_ALL_DISTINCT_METHOD_CD;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PaymentMethodDto>>>() {};
        return (List<PaymentMethodDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * PG 수수료 마스터 조회
     * @param pgCommissionManageSelectDto
     * @return
     * @throws Exception
     */
    public List<PgCommissionManageDto> getPgCommissionMngList(PgCommissionManageSelectDto pgCommissionManageSelectDto) throws Exception {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_PG_COMMISSION_MNG_LIST, PgCommissionManageSelectDto.class, pgCommissionManageSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PgCommissionManageDto>>>() {};
        return (List<PgCommissionManageDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * PG 수수료 대상 조회
     * @param pgCommissionTargetSelectDto
     * @return
     * @throws Exception
     */
    public List<PgCommissionTargetDto> getPgCommissionTargetList(PgCommissionTargetSelectDto pgCommissionTargetSelectDto) throws Exception {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_PG_COMMISSION_TARGET_LIST, PgCommissionTargetSelectDto.class, pgCommissionTargetSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PgCommissionTargetDto>>>() {};
        return (List<PgCommissionTargetDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 중복 수수료 정책 조회
     * @param pgKind
     * @param parentMethodCd
     * @param startDt
     * @param endDt
     * @return
     * @throws Exception
     */
    public ResponseObject<Integer> checkDuplicateCommission(String pgKind, String parentMethodCd, String startDt, String endDt, String pgCommissionMngSeq) throws Exception {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("pgKind", pgKind));
        setParameterList.add(SetParameter.create("parentMethodCd", parentMethodCd));
        setParameterList.add(SetParameter.create("startDt", startDt));
        setParameterList.add(SetParameter.create("endDt", endDt));
        setParameterList.add(SetParameter.create("pgCommissionMngSeq", pgCommissionMngSeq));
        String apiUri = EscrowConstants.ESCROW_CHECK_DUPLICATE_COMMISSION + StringUtil.getParameter(setParameterList);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Integer>>() {};
        return (ResponseObject<Integer>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference);
    }

    /**
     * PG 수수료 저장
     */
    public ResponseObject<Object> savePgCommission(PgCommissionManageDto pgCommissionManageDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_SAVE_PG_COMMISSION;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, pgCommissionManageDto, apiUri, typeReference);
    }
}
