package kr.co.homeplus.admin.web.manage.model.sticker;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StickerListGetDto {

    @ApiModelProperty(value = "스티커번호", position = 1)
    @RealGridColumnInfo(headText = "스티커번호", width = 100 , sortable = true)
    private Long stickerNo;

    @ApiModelProperty(value = "스티커명", position = 2)
    @RealGridColumnInfo(headText = "스티커명", width = 150)
    private String stickerNm;

    @ApiModelProperty(value = "사용여부", position = 3)
    @RealGridColumnInfo(headText = "사용여부", width = 100 , sortable = true)
    private String useYnNm;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", hidden = true)
    private String useYn;

    @ApiModelProperty(value = "등록일", position = 4)
    @RealGridColumnInfo(headText = "등록일", width = 150, sortable = true)
    private String regDt;

    @ApiModelProperty(value = "등록자", position = 5)
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regNm;

    @ApiModelProperty(value = "수정일", position = 6)
    @RealGridColumnInfo(headText = "수정일", width = 150 , sortable = true)
    private String chgDt;

    @ApiModelProperty(value = "수정자", position = 7)
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgNm;
}
