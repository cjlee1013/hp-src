package kr.co.homeplus.admin.web.pg.controller;

import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.COMMON_CARD_DETAIL_KIND;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.pg.model.CardPrefixManageDto;
import kr.co.homeplus.admin.web.pg.model.CardPrefixManageSelectDto;
import kr.co.homeplus.admin.web.pg.service.CardPrefixManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Slf4j
@RequestMapping("/pg/paymentMethod")
public class CardPrefixManageController {

    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    private final CardPrefixManageService cardPrefixManageService;
    private final CodeService codeService;

    public CardPrefixManageController(LoginCookieService loginCookieService, AuthorityService authorityService,
            CardPrefixManageService cardPrefixManageService, CodeService codeService) {
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;
        this.cardPrefixManageService = cardPrefixManageService;
        this.codeService = codeService;
    }

    /**
     * 카드 PREFIX 관리 메인 화면
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/cardPrefixManageMain")
    public String cardPrefixManageMain(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_PAY_COMMON)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        // 공통코드 cardDetailKind 리스트 가져와서 model 에 담기
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(COMMON_CARD_DETAIL_KIND);

        model.addAllAttributes(RealGridHelper.create("cardPrefixManageGridBaseInfo", CardPrefixManageDto.class));
        model.addAttribute("paymentMethodList", cardPrefixManageService.getDistinctMethodCd());
        model.addAttribute("commCardDetailKindList", code.get(COMMON_CARD_DETAIL_KIND));

        return "/pg/cardPrefixManageMain";
    }

    /**
     * 카드 PREFIX 리스트 조회
     * @param cardPrefixManageSelectDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/getCardPrefixManageList.json")
    public List<CardPrefixManageDto> getCardPrefixManageList(CardPrefixManageSelectDto cardPrefixManageSelectDto) throws Exception {
        return cardPrefixManageService.getCardPrefixManageList(cardPrefixManageSelectDto);
    }

    /**
     * 카드 PREFIX 저장/수정
     * @param cardPrefixManageDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/saveCardPrefixManage.json")
    public ResponseObject<Object> saveCardPrefixManage(@RequestBody CardPrefixManageDto cardPrefixManageDto) throws Exception {
//        String userCd = sessionService.getUserInfo().getUserCd(); // 등록자, 수정자 저장할 경우 사용
        return cardPrefixManageService.saveCardPrefixManage(cardPrefixManageDto);
    }

    /**
     * 카드 PREFIX 결제수단 일괄변경
     * @param cardPrefixManageDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/saveBundleCardPrefixMethod.json")
    public ResponseObject<Object> saveBundleCardPrefixMethod(@RequestBody CardPrefixManageDto cardPrefixManageDto) throws Exception {
        return cardPrefixManageService.saveBundleCardPrefixMethod(cardPrefixManageDto);
    }
}
