package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataManagementNewItemGetParam {
    @ApiParam(value = "페이지번호",required = true, defaultValue = "1")
    private Integer page = 1;
    @ApiParam(value = "페이지사이즈",required = true, defaultValue = "100")
    private Integer perPage = 100;
    @ApiParam(value = "점포ID ( ex : 37, 38, 90163")
    private String storeIds;
    @ApiParam(value = "사이트 유형 ( 홈플러스 : HOME, 더클럽 : CLUB)", allowableValues = "HOME,CLUB")
    private String siteType;
    @ApiParam(value = "카테고리번호", required = true, defaultValue = "100001")
    private String categoryId;
    @ApiParam(value = "어드민모드", hidden = true)
    private String adminMode = "Y";
}
