package kr.co.homeplus.admin.web.item.model.priceChangeHist;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceChangeHistParamDto {

    private String itemNo;
    private String storeType;

}
