package kr.co.homeplus.admin.web.item.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.item.ShipPolicyGetDto;
import kr.co.homeplus.admin.web.item.model.item.VirtualStoreGetDto;
import kr.co.homeplus.admin.web.item.model.item.VirtualStoreItemGetDto;
import kr.co.homeplus.admin.web.item.model.item.VirtualStoreItemListSetDto;
import kr.co.homeplus.admin.web.item.model.item.VirtualStoreItemSetDto;
import kr.co.homeplus.admin.web.item.model.item.VirtualStoreListParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/virtualStoreItem")
public class VirtualStoreItemController {
    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;

    public VirtualStoreItemController(ResourceClient resourceClient,
            LoginCookieService loginCookieService,
            CodeService codeService) throws Exception {
        this.resourceClient = resourceClient;
        this.loginCookieService = loginCookieService;
        this.codeService = codeService;
    }

    /**
     * 상품관리 > 점포상품관리 > 택배점 취급상품관리
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/main")
    public String virtualStoreItemMain(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "deal_yn"    // 취급상태여부
            ,	"use_yn"         // 사용여부
            ,	"store_type"     // 점포구분 - Hyper,Club,Exp,DS
            ,	"store_kind"     // 점포종류
            ,	"set_yn"         // 설정여부
        );

        model.addAttribute("storeType", code.get("store_type"));
        model.addAttribute("storeKind", code.get("store_kind"));
        model.addAttribute("useYn", code.get("use_yn"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("store_type")));
        model.addAttribute("useYnJson", om.writeValueAsString(code.get("use_yn")));
        model.addAttribute("dealYnJson", om.writeValueAsString(code.get("deal_yn")));
        model.addAttribute("virtualStoreOnlyYnJson", om.writeValueAsString(code.get("set_yn")));

        model.addAllAttributes(RealGridHelper.create("storeListGridBaseInfo", VirtualStoreGetDto.class));
        model.addAllAttributes(RealGridHelper.create("itemListGridBaseInfo", VirtualStoreItemGetDto.class));

        return "/item/virtualStoreItemMain";
    }

    /**
     * 상품관리 > 점포상품관리 > 택배점 취급상품관리 > 점포리스트 조회
     * @param storeId
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = "/getShipPolicyList.json")
    public List<ShipPolicyGetDto> getShipPolicyList(@RequestParam ("storeId") String storeId) {
        String apiUri = "/item/virtualStoreItem/getShipPolicyList?storeId=" + storeId;
        List<ShipPolicyGetDto> shipPolicyGetDtoList = resourceClient.getForResponseObject(ResourceRouteName.ITEM,  apiUri,
            new ParameterizedTypeReference<ResponseObject<List<ShipPolicyGetDto>>>() {}).getData();

        return  shipPolicyGetDtoList;
    }

    /**
     * 상품관리 > 점포상품관리 > 택배점 취급상품관리 > 점포리스트 조회
     * @param virtualStoreListParamDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = "/getVirtualStoreList.json")
    public List<VirtualStoreGetDto> getVirtualStoreList(VirtualStoreListParamDto virtualStoreListParamDto) {
        return resourceClient.getForResponseObject(ResourceRouteName.ITEM,
            StringUtil.getRequestString("/item/virtualStoreItem/getVirtualStoreList", VirtualStoreListParamDto.class, virtualStoreListParamDto),
            new ParameterizedTypeReference<ResponseObject<List<VirtualStoreGetDto>>>() { }).getData();
    }

    /**
     * 상품관리 > 점포상품관리 > 택배점 취급상품관리 > 취급상품정보 리스트 조회
     * @param storeId
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = "/getVirtualStoreItemList.json")
    public List<VirtualStoreItemGetDto> getVirtualStoreItemList(@RequestParam("storeId") int storeId) {
        String apiUri = "/item/virtualStoreItem/getVirtualStoreItemList?storeId=" + storeId;
        List<VirtualStoreItemGetDto> itemList = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri,
            new ParameterizedTypeReference<ResponseObject<List<VirtualStoreItemGetDto>>>() {}).getData();

        return itemList;
    }

    /**
     * 상품관리 > 점포상품관리 > 택배점 취급상품관리 > 취급상품정보 단건 조회
     * @param storeId
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = "/getVirtualStoreItemInfo.json")
    public VirtualStoreItemGetDto getVirtualStoreItemInfo(@RequestParam("storeId") int storeId, @RequestParam("itemNo") String itemNo) {
        String apiUri = "/item/virtualStoreItem/getVirtualStoreItemInfo?storeId=" + storeId +"&itemNo=" + itemNo;
        VirtualStoreItemGetDto itemInfo = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri,
            new ParameterizedTypeReference<ResponseObject<VirtualStoreItemGetDto>>() {}).getData();

        return itemInfo;
    }


    /**
     * 상품관리 > 점포상품관리 > 택배점 취급상품관리 > 취급상품정보 저장(입력/수정)
     * @param virtualStoreItemListSetDto
     * @return
     */
    @Deprecated
    @ResponseBody
    @PostMapping(value = "/setVirtualStoreItemList.json")
    public Object setVirtualStoreItemList(@RequestBody VirtualStoreItemListSetDto virtualStoreItemListSetDto) {
        virtualStoreItemListSetDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        String apiUri = "/item/virtualStoreItem/setVirtualStoreItemList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Long>>() {};
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, virtualStoreItemListSetDto, apiUri, typeReference).getData();
    }

    /**
     * 상품관리 > 점포상품관리 > 택배점 취급상품관리 > 취급상품정보 저장(입력/수정)
     * @param virtualStoreItemSetDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/setVirtualStoreItem.json")
    public Object setVirtualStoreItem(VirtualStoreItemSetDto virtualStoreItemSetDto) {

        virtualStoreItemSetDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        String apiUri = "/item/virtualStoreItem/setVirtualStoreItem";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Long>>() {};
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, virtualStoreItemSetDto, apiUri, typeReference).getData();
    }

    /**
     * 상품관리 > 점포상품관리 > 택배점 취급상품관리 > 상품번호로 상품명 조회(TD상품만)
     * @param itemNo
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = "/getItemNm.json")
    public ResponseObject<String> getItemNm(@RequestParam("itemNo") String itemNo) {
        String apiUri = "/item/virtualStoreItem/getItemNm?itemNo=" + itemNo;
        ResponseObject<String> itemNmObj = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri,
            new ParameterizedTypeReference<ResponseObject<String>>() {});

        return itemNmObj;
    }

    /**
     * 상품관리 > 점포상품관리 > 택배점 취급상품관리 > 택배점 단독판매여부
     * * @param itemNo
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = "/getVirtualStoreOnlyYn.json")
    public ResponseObject<String> getVirtualStoreOnlyYn(@RequestParam("itemNo") String itemNo, @RequestParam("storeId") int storeId) {
        String apiUri = "/item/virtualStoreItem/getVirtualStoreOnlyYn?itemNo=" + itemNo +"&storeId=" + storeId;
        ResponseObject<String> obj = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<String>>() {});
        return obj;
    }
}
