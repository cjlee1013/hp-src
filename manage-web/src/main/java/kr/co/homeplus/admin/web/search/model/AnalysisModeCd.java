package kr.co.homeplus.admin.web.search.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum AnalysisModeCd {
    INDEX("index_analyzer"),
    SEARCH("search_analyzer");

    private String analysisModeNm;

    public static AnalysisModeCd getAnalysisMode(String analysisModeNm) {
        for(AnalysisModeCd type : AnalysisModeCd.values()) {
            if(type.getAnalysisModeNm().equals(analysisModeNm)) {
                return type;
            }
        }
        return null;
    }
}
