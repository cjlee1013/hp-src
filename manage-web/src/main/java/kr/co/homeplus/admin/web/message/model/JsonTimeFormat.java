package kr.co.homeplus.admin.web.message.model;

public class JsonTimeFormat {

    public static final String T_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public static final String DEFAULT_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String TIME_ZONE_KST = "Asia/Seoul";
}
